﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_LongtextNotif', {
    extend: 'AssetManagement.customer.view.pages.OxPage',
        
    inheritableStatics: {
        _uniqueId: 'UIICP_LN001',
        _controller: null,

        getUid: function() {
            return this._uniqueId;
        },

        getUiDefinition: function (controller) {
            var retval = null;
            try {
                this._controller = controller;
                retval = Ext.create('Ext.container.Container', {
                    id: 'UIIntegratedConfPanel_LongtextNotifContainer',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    margin: '20 5 0 5',
                    items: [{
                            xtype: 'label',
                            cls: 'oxHeaderLabelBlue',
                            text: Locale.getMsg('remarks')
                        },{
                            xtype: 'container',
                            id: this._uniqueId +'longtextNotifPanel',
                            //hideContextMenuColumn: true,
                            margin: '5 0 0 0',
                            width: 580,
                            layout: {
                                type: 'hbox',
                                align: 'left'
                            },
                            items: [
                            //leftContainer,
                            {
                                xtype: 'component',
                                maxWidth: 10,
                                minWidth: 10
                            }, {
                                xtype: 'container',
                                maxWidth: 590,
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },
                                items: [
                                //upper container
                                {
                                    xtype: 'component',
                                    height: 15
                                },{
                                    xtype: 'textarea',
                                    id: this._uniqueId +'longtextNotifTextArea',
                                    scollable: 'vertical',
                                    height: 120,
                                    width: 590,
                                    labelStyle: 'display: none'
                                }]
                            },{
                                xtype: 'container',
                                width: 60,
                                padding: '60 0 0 0',
                                items: [{
                                    xtype: 'button',
                                    id: this._uniqueId + 'addLongtext_button',
                                    flex: 1,
                                    bind: {
                                        html: '<div><img width="100%" src="resources/icons/save.png"></img></div>'
                                    },
                                    padding: '0 5 0 5',
                                    listeners: {
                                        click: this._controller.mixins.mixinUIICP_LongtextNotifController.onAddLongtextClick,
                                        scope: this._controller
                                    }
                                }]
                            },
                            {
                                xtype: 'component',
                                flex: 1,
                                minWidth: 10
                            }]
                        }
                    ]
                });
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUiDefinition of BaseUIIntegratedConfPage_LongtextNotif', ex);
            }
            return retval;
        },
        
        transferModelStateIntoView: function() {
            try {                
                var myModel = this._controller.getViewModel();
                if(myModel.get('notif') !== null && myModel.get('notif') !== 'undefined' && 
                    myModel.get('notif').get('localLongtext') !== null && myModel.get('notif').get('localLongtext') !== 'undefined') {
                    var longtext = myModel.get('notif').get('localLongtext');

                    //set general data
                    Ext.getCmp(this._uniqueId + 'longtextNotifTextArea').setValue(longtext);
                }
                else {
                    Ext.getCmp(this._uniqueId + 'longtextNotifTextArea').setValue('');
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseUIIntegratedConfPage_LongtextNotif', ex);
            }
        },
        
        transferViewStateIntoModel: function() {
            try {                
                var myModel = this._controller.getViewModel();
                
		        myModel.set('notifLongtextClient', Ext.getCmp(this._uniqueId + 'longtextNotifTextArea').getValue()); 
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseUIIntegratedConfPage_LongtextNotif', ex);
            }
        }
    }

}
);