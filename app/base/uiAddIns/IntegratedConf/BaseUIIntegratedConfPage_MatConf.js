﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_MatConf', {
    extend: 'AssetManagement.customer.view.pages.OxPage',
        
        inheritableStatics: {
        _uniqueId: 'UIICP_MC001',
        _controller: null,

        getUid: function() {
            return this._uniqueId;
        },

        getUiDefinition: function (controller) {
            var retval = null;
            try {
                this._controller = controller;
                retval = Ext.create('Ext.container.Container', {
                        id: 'UIIntegratedConfPanel_MatConfContainer',
                        flex: 1,
                        layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                        margin: '20 5 0 5',
                        items: [{
                                    xtype: 'label',
                                    cls: 'oxHeaderLabelBlue',
                                    text: Locale.getMsg('materialConfirmations')
                                },{
                                    xtype: 'container',
                                    id: this._uniqueId + 'matConfPanel',
                                    //hideContextMenuColumn: true,
                                    flex: 1,           
                                    margin: '5 0 0 0',
                                    layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },   
                                                items: [
                                                    //leftContainer,
                                                    {
                                                        xtype: 'component',
                                                        maxWidth: 10,
                                                        minWidth: 10
                                                    },{
                                                        xtype: 'container',
                                                        flex: 1,
                                                        layout: {
                                                            type: 'vbox',
                                                            align: 'stretch'
                                                        },
                                                        items: [
                                                            //upper container
                                                            {
                                                                xtype: 'component',
                                                                height: 15
                                                            },{
                                                            //upper section - form 
                                                            xtype: 'container',
                                                            flex: 1,
                                                            items: [{
                                                                xtype: 'container',
                                                                items: [{
                                                                    xtype: 'container',
                                                                    id: this._uniqueId + 'createContainer',
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                items: [{
                                                                    //hbox with 3 columns
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'left'
                                                                    },
                                                                items: [{//first column 
                                                                    xtype: 'container',
                                                                    width: 190,
                                                                    layout: {
                                                                        type: 'vbox',
                                                                        align: 'stretch'
                                                                    },
                                                                items: [{
                                                                    //material text box label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId + 'materialNumber_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('material'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{                                                                            
                                                                    xtype: 'container',
                                                                    layout: {
                                                                    type: 'hbox',
                                                                    align: 'left'
                                                                },
                                                                items: [{
                                                                    xtype: 'oxtextfield',
                                                                    id:this._uniqueId + 'materialNumber_textbox',
                                                                    maxLength: 18,
                                                                    width: 155
                                                                },{
                                                                    xtype: 'container',
                                                                    width: 50,
                                                                    height: 28,
                                                                    top: 0, 
                                                                    items: [{
                                                                            xtype: 'button',
                                                                            id: this._uniqueId + 'materialSelect_button',
                                                                            bind: {
                                                                                html: '<div><img width="66%" src="resources/icons/search.png"></img></div>'                                                               
                                                                            },
                                                                            padding: '0 5 0 5',
                                                                            listeners: {
                                                                                click: this._controller.mixins.mixinUIICP_MatConfController.onSelectMaterialClick
                                                                            }
									                
                                                                        }
                                                                    ]}            
                                                                ]
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 9
                                                                },{
                                                                    //account indication dropdown label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId + 'accountIndicationDropDownBox_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('accountIndiShort'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 2
                                                                },{
                                                                    //account indication dropdown
                                                                    xtype: 'oxcombobox',
                                                                    displayField: 'text',
                                                                    queryMode: 'local',
                                                                    valueField: 'value',
                                                                    height: 10,
                                                                    id: this._uniqueId + 'accountIndicationDropDownBox',
                                                                    editable: false
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 15
                                                                },{
                                                                    //serial number textfield label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId + 'serialNumberTextbox_label',
                                                                    editable: true,
                                                                    text: Locale.getMsg('serialNumber'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    //serial number textfield
                                                                    xtype: 'oxtextfield',
                                                                    id:this._uniqueId + 'serialNumber_textbox',
                                                                    maxLength: 10
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                }
                                                            ]
                                                            },{
                                                            //spacer
                                                            xtype: 'component',
                                                            width: 10
                                                            },{
                                                            //second column 
                                                            xtype: 'container',
                                                            width: 190,
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [{
                                                                //quantity label
                                                                xtype: 'label',
                                                                id: this._uniqueId + 'quantity_label',
                                                                text: Locale.getMsg('quantity'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },{
                                                                //quantity textfield
                                                                xtype: 'oxnumberfield',
                                                                maxLength: 40,
                                                                id: this._uniqueId + 'quantity_textfield'		                                                                    
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },{
                                                                //plant dropdown label
                                                                xtype: 'label',
                                                                id: this._uniqueId + 'plantTextField_label',
                                                                editable: false,
                                                                text: Locale.getMsg('plant'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },{
                                                                //plant dropdown
                                                                xtype: 'oxtextfield',
                                                                height: 10,
                                                                id: this._uniqueId + 'plantTextField',
                                                                editable: true
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 15
                                                            },{
                                                                //batch number text box label
                                                                xtype: 'label',
                                                                id: this._uniqueId + 'batchNumber_label',
                                                                editable: false,
                                                                text: Locale.getMsg('batch'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },{
                                                                xtype: 'oxtextfield',
                                                                id:this._uniqueId + 'batchNumber_textbox',
                                                                maxLength: 10
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 20
                                                            }
                                                        ]
                                                        },{
                                                        //spacer
                                                        xtype: 'component',
                                                        width: 10
                                                        },{
                                                        //third column 
                                                        xtype: 'container',
                                                        width: 190,
                                                        layout: {
                                                            type: 'vbox',
                                                            align: 'stretch'
                                                        },
                                                        items: [{
                                                            //unit text box label
                                                            xtype: 'label',
                                                            id: this._uniqueId + 'unit_label',
                                                            editable: false,
                                                            text: Locale.getMsg('unit'),
                                                            cls: 'oxDescLabelBlue'
                                                        },{
                                                            //spacer
                                                            xtype: 'component',
                                                            height: 5
                                                        },{
                                                            xtype: 'oxtextfield',
                                                            id:this._uniqueId + 'unit_textbox',
                                                            maxLength: 10
                                                        },{
                                                            //spacer
                                                            xtype: 'component',
                                                            height: 8
                                                        },{
                                                            //storage location dropdown label
                                                            xtype: 'label',
                                                            id: this._uniqueId + 'storLocDownBox_label',
                                                            editable: false,
                                                            text: Locale.getMsg('storLoc'),
                                                            cls: 'oxDescLabelBlue'
                                                        },{
                                                            //spacer
                                                            xtype: 'component',
                                                            height: 2
                                                        },{
                                                            //storage location dropdown
                                                            xtype: 'oxcombobox',
                                                            displayField: 'text',
                                                            queryMode: 'local',
                                                            valueField: 'value',
                                                            height: 15,
                                                            id: this._uniqueId + 'storLocDropDownBox',
                                                            editable: true
                                                        }, {
                                                            //spacer
                                                            xtype: 'component',
                                                            height: 5
                                                        }
                                                    ]
                                                    },{
                                                        xtype: 'container',
                                                        width: 60,
                                                        padding: '70 0 0 0',
                                                        items: [{
                                                            xtype: 'button',
                                                            id: this._uniqueId + 'addMatConf_button12345',   
                                                            flex: 1,
                                                            bind: {
                                                                html: '<div><img width="100%" src="resources/icons/save.png"></img></div>'                                                               
                                                            },
                                                            padding: '0 5 0 5',
                                                            listeners: {                                                                            
                                                                click: this._controller.mixins.mixinUIICP_MatConfController.onAddMatConfClick,
                                                                scope: this._controller   
                                                            }
                                                        }
                                                    ]}
                                                ]}
                                            ]}
                                        ]
                                        },{
                                            //spacer
                                            xtype: 'component',
                                            flex: 1,
                                            height: 5
                                        },{
                                            xtype: 'label',
                                            margin: '0 0 0 0',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('matConf_RecordedMatConfs')
                                        },{
                                            //spacer
                                            xtype: 'component',
                                            height: 10
                                        },{
                                            xtype: 'container',
                                            flex: 1,           
                                            id: this._uniqueId + 'matConfsContainer',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            items: [{
                                                xtype: 'oxgridpanel',
                                                id: this._uniqueId + 'matConfGrid',
                                                disableSelection: true,
                                                columns: [{
                                                    xtype: 'actioncolumn',
                                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                        return '<img src="resources/icons/matconf.png"  />';
                                                    },
                                                    id: this._uniqueId + 'MatConfGridActionColumn',
                                                    maxWidth: 80,
                                                    minWidth: 80,
                                                    align: 'center'
                                                },{
                                                    xtype: 'gridcolumn',
                                                    id: this._uniqueId + 'matConfGridMaterialColumn',
                                                    text: Locale.getMsg('material'),
                                                    dataIndex: 'matnr',
                                                    flex: 2,
                                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                        var material = record.get('material');
                                                        var maktx = material ? material.get('maktx') : '';
			                	                    
                                                        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
                                                    }
                                                },{
                                                    //column account Indication 
                                                    xtype: 'gridcolumn',
                                                    dataIndex: 'bemot',
                                                    flex: 1,
                                                    text: Locale.getMsg('accountIndication'),
                                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                        var retval = '';
			                                        
                                                        try {
                                                            var accountIndication = record.get('accountIndication');
		                                        		
                                                            retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                                                        }
                                                        catch (ex) {
                                                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of matConfGrid/bemot-column in MatConfPage', ex);
                                                        }
		                                        		
                                                        return retval;
                                                    }
                                                },{
                                                    xtype: 'datecolumn',
                                                    id: this._uniqueId + 'matConfGridDateColumn',
                                                    text: Locale.getMsg('date'),
                                                    maxWidth: 125,
                                                    minWidth: 125,
                                                    format: 'd.m.Y',
                                                    dataIndex: 'isdd'
                                                },{
                                                    xtype: 'gridcolumn',
                                                    id: this._uniqueId + 'matConfGridAmountColumn',
                                                    text: Locale.getMsg('orderComponents_Qty'),
                                                    dataIndex: 'quantity',
                                                    maxWidth: 160,
                                                    minWidth: 160,
                                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                        var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('quantity'));
                                                        var unit = record.get('unit'); 
			                                    		
                                                        return quantity + ' ' + unit; 
                                                    }
                                                },{
                                                    xtype: 'gridcolumn',
                                                    id: this._uniqueId + 'matConfGridWerkColumn',
                                                    dataIndex: 'plant',
                                                    maxWidth: 160,
                                                    minWidth: 160,
                                                    text: Locale.getMsg('plantStorLoc'),
                                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                        var plant = record.get('plant'); 
                                                        var storloc = record.get('storloc'); 
			                                    		
                                                        return AssetManagement.customer.utils.StringUtils.concatenate([ plant, storloc ], '/');
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                cellcontextmenu: this._controller.onCellContextMenu,
                                                scope: this._controller
                                            }
                                        }
                                    ]
                                    },{
                                        //lower container
                                        xtype: 'component',
                                        height: 30
                                    }
                                ]}
                            ]}
                        ]}
                    ]
                    });
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUiDefinition of BaseUIIntegratedConfPage_MatConf', ex);
            }
            return retval;
        },                  
	
        fillMaterialSpecFieldsFromComponent: function(component) {
            try {
                if (component) {
                    this.fillStorageLocationComboBox(component.get('werks'));		  
                    this.fillPlantTextfield(component.get('werks'));	
			
                    Ext.getCmp(this._uniqueId + 'materialNumber_textbox').setValue(component.get('matnr'));
                    Ext.getCmp(this._uniqueId + 'quantity_textfield').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(component.get('bdmng')));
                    Ext.getCmp(this._uniqueId + 'unit_textbox').setValue(component.get('meins'));
				
                    this.trySetStorageLocationComboBoxByKeyField(component.get('werks'), component.get('lgort'));
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromComponent of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },
	
        fillMaterialSpecFieldsFromStpo: function(stpo) {
            try {
                if (stpo) {                	  
                    this.fillPlantTextfield(stpo.get('werks'));	
                    Ext.getCmp(this._uniqueId + 'materialNumber_textbox').setValue(stpo.get('idnrk'));
                    Ext.getCmp(this._uniqueId + 'quantity_textfield').setValue(1);    
                    Ext.getCmp(this._uniqueId + 'unit_textbox').setValue(stpo.get('meins'));
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromStpo of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },
	
        fillMaterialSpecFieldsFromMatStock: function(matStock) {
            try {
                if (matStock) {
                    this.fillStorageLocationComboBox(matStock.get('werks'));	  
                    this.fillPlantTextfield(matStock.get('werks'));	
			
                    Ext.getCmp(this._uniqueId + 'materialNumber_textbox').setValue(matStock.get('matnr'));
                    Ext.getCmp(this._uniqueId + 'quantity_textfield').setValue(1); 
                    Ext.getCmp(this._uniqueId + 'unit_textbox').setValue(matStock.get('meins'));
				
                    this.trySetStorageLocationComboBoxByKeyField(matStock.get('werks'), matStock.get('lgort'));
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromMatStock of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },

        fillPlantTextfield: function(werks) {
            try {
                //TODO: Integrate    
                Ext.getCmp(this._uniqueId + 'plantTextField').setValue(werks);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillPlantTextfield of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },

        getCurrentInputValues: function() {
            var retVal = null;
            try { 
                var storLocObject = Ext.getCmp(this._uniqueId + 'storLocDropDownBox').getValue();
                var storLoc = '';
			
                if (storLocObject)
                    storLoc = storLocObject.get('lgort');   
                                                                                                      
                var plantLoc = '';
                var plantObject = Ext.getCmp(this._uniqueId + 'plantTextField').getValue();
                if (plantObject !== null) { 
                    plantLoc = plantObject;
                }

                retVal = {
                    //vornr: operation ? operation.get('vornr') : '',
                    matnr: Ext.getCmp(this._uniqueId + 'materialNumber_textbox').getValue(),
                    quantity: Ext.getCmp(this._uniqueId + 'quantity_textfield').getValue(),
                    unit: Ext.getCmp(this._uniqueId + 'unit_textbox').getValue(),
                    plant: plantLoc,
                    storLoc: storLoc,
                    accountIndication: Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').getValue()
                };
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseUIIntegratedConfPage_MatConf', ex);
            } 
            return retVal;
        },
                   	
        fillDropDownBoxes: function() {
            try {
                var myModel = this._controller.getViewModel();
                var accountIndications = myModel.get('accountIndications');
		
                //filling accountIndication dropdownbox
                var bemotComboBoxSource = Ext.create('Ext.data.Store', {
                        fields: ['text', 'value']
                    });
	
                if (accountIndications && accountIndications.getCount() > 0) {
                    accountIndications.each(function(accountIndication) {
                        bemotComboBoxSource.add({ text: (accountIndication.get('bemot') + ' - ' + accountIndication.get('bemottxt')), value: accountIndication });
                    }, this);
                }
			
                var bemotComboBox = Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox');
                bemotComboBox.clearValue();
                bemotComboBox.setStore(bemotComboBoxSource);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },

        fillStorageLocationComboBox: function(plant) {
            try {		
                var store = Ext.create('Ext.data.Store', {
                        fields: ['text', 'value']
                    });
			
                var myModel = this._controller.getViewModel();

                var defaultStorLoc = null; 
                var matConfsStorLoc = null;
                var matConfsLgort = myModel.get('matConf').get('storloc');
                var storLocs = myModel.get('storLocs');
			
                if (storLocs && storLocs.getCount() > 0) {
                    storLocs.each(function(storLoc) {
                        if (storLoc.get('werks') === plant) {
                            store.add({ text: storLoc.get('lgort') + " - " + storLoc.get('lgobe'), value: storLoc});
						
                            if (storLoc.get('default_lgort') === 'X')
                                defaultStorLoc = storLoc;
							
                            if (storLoc.get('lgort') === matConfsLgort)
                                matConfsStorLoc = storLoc;
                        }
                    }, this);
				
                    if (!defaultStorLoc)
                        defaultStorLoc = storLocs.getAt(0);
                }
			
                var comboBox = Ext.getCmp(this._uniqueId + 'storLocDropDownBox');
                comboBox.clearValue();
                comboBox.setStore(store);
			
                var storLocToSet = matConfsStorLoc;

                if (!storLocToSet)
                    storLocToSet = defaultStorLoc;
				
                if (storLocToSet) {
                    var storLocRecord = storLocToSet ? comboBox.findRecordByValue(storLocToSet) : null;	
				
                    if (storLocRecord)
                        comboBox.setValue(storLocRecord);
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillStorageLocationComboBox of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },
	
        transferModelStateIntoView: function() {
            try {
                var myModel = this._controller.getViewModel();
                var matConf = myModel.get('matConf');

                //set general data
                Ext.getCmp(this._uniqueId + 'materialNumber_textbox').setValue(matConf.get('matnr'));
                Ext.getCmp(this._uniqueId + 'quantity_textfield').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(matConf.get('quantity')));
                Ext.getCmp(this._uniqueId + 'unit_textbox').setValue(matConf.get('unit')); 
			
                //set drop down box values
                this.setDropDownBoxesConvenientToCurrentModel();
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },
	
        setDropDownBoxesConvenientToCurrentModel: function() {
            try {
                var myModel = this._controller.getViewModel();
                var order = myModel.get('order');
                var accountIndications = myModel.get('accountIndications');
                var matConf = myModel.get('matConf');
			
                //account indication
                var matConfsAccountIndi = null;
			
                if (accountIndications && accountIndications.getCount() > 0) {
                    accountIndications.each(function(accountIndication) {
                        if (accountIndication.get('bemot') === matConf.get('bemot')) {
                            matConfsAccountIndi = accountIndication;
                            return false;
                        }
                    }, this);
                }
			
                var matConfsAccountIndiRecord = matConfsAccountIndi ? Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').findRecordByValue(matConfsAccountIndi) : null;	
			
                if (matConfsAccountIndiRecord)
                    Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').setValue(matConfsAccountIndiRecord);
			
                //storage location
                this.fillStorageLocationComboBox(matConf.get('plant'));
                this.fillPlantTextfield(matConf.get('plant'));
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDropDownBoxesConvenientToCurrentModel of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },     
	
        //will try to set stor loc identified by the key values - if not found a message will be triggered and the value will be reset
        //passing incomplete values will just clear the current value
        trySetStorageLocationComboBoxByKeyField: function(werks, lgort) {
            try {
                var storLocComboBox = Ext.getCmp(this._uniqueId + 'storLocDropDownBox');
		
                storLocComboBox.clearValue();

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
                    var targetValue = null;

                    var targetStorLoc = null;
                    var storLocs = this._controller.getViewModel().get('storLocs');
				
                    if (storLocs && storLocs.getCount() > 0) {
                        storLocs.each(function(storLoc) {
                            if (storLoc.get('werks') === werks && storLoc.get('lgort') === lgort) {
                                targetStorLoc = storLoc;
                                return true;
                            }
                        }, this);
                    }
				
                    var targetValue = targetStorLoc ? storLocComboBox.findRecordByValue(targetStorLoc) : null;	
				
                    if (targetValue) {
                        //value found, set it
                        storLocComboBox.setValue(targetValue);
                    }
                    else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('storageLocationNotFound'), true, 1);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySetStorageLocationComboBoxByKeyField of BaseUIIntegratedConfPage_MatConf', ex);
            }
        },
	
        //transfer current values from screen into model
        transferViewStateIntoModel: function() {
            try {
                var matConf = this._controller.getViewModel().get('matConf');    
                var operation = this._controller.getViewModel().get('operation')         
			    var accountIndication = Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').getValue();
			
                matConf.set('vornr', operation ? operation.get('vornr') : '');
			
                //set bemot
                if (accountIndication) {
                    matConf.set('accountIndication', accountIndication);
                    matConf.set('bemot', accountIndication.get('bemot'));
                }
                else {
                    matConf.set('accountIndication', null);
                    matConf.set('bemot', '');
                }

                matConf.set('matnr', Ext.getCmp(this._uniqueId + 'materialNumber_textbox').getValue());
                matConf.set('quantity', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp(this._uniqueId + 'quantity_textfield').getValue()));
                matConf.set('unit', Ext.getCmp(this._uniqueId + 'unit_textbox').getValue());
                
                var storLocObject = Ext.getCmp(this._uniqueId + 'storLocDropDownBox').getValue();
                var storLoc = '';                    			
                if (storLocObject)
                    storLoc = storLocObject.get('lgort');   
                                                                                                      
                var plantLoc = '';
                var plantObject = Ext.getCmp(this._uniqueId + 'plantTextField').getValue();
                if (plantObject !== null) {                   
                    plantLoc = plantObject;
                }

                matConf.set('plant', plantLoc);
                matConf.set('storloc', storLoc);
            
                var accountIndication = Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').getValue();
                matConf.set('bemot', accountIndication ? accountIndication.get('bemot') : '');
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseUIIntegratedConfPage_MatConf', ex);
            }
        }
    }


}
);