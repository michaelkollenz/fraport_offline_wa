﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_LongtextNotifController', {
        mixinId: 'mixinUIICP_LongtextNotifController',
	
        requires: [
            'AssetManagement.customer.helper.OxLogger'
        ],
        _saveRunning: false,

        //context should be IntegratedConfPage
        loadLongtext: function(completeCallback) {
            try {
                var eventId = -1;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var me = this;
                AssetManagement.customer.controller.ClientStateController.enterLoadingState();
				var longtextSuccessCallback = function(localLT, backendLT) {
					var erroroccurred = localLT === undefined || localLT === backendLT;
                    
					me.controller.getViewModel().set('notifLongtextClient', localLT);
					me.controller.getViewModel().set('notifLongtextBackend', backendLT);
                    
                    me.getLongtextNotifView().transferModelStateIntoView();
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				};
                
                if (this.controller.getViewModel().get('notif') !== null && this.controller.getViewModel().get('notif') !== 'undefined') {
                    

                    eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(this.controller.getViewModel().get('notif'), true);
                    eventController.registerOnEventForOneTime(eventId, longtextSuccessCallback);
                }
                else {
                    me.getLongtextNotifView().transferModelStateIntoView();
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLongtext of BaseUIIntegratedConfPage_LongtextNotifController', ex);
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
        },
    
        //context should be IntegratedConfPage
        initializeNotifLongtext: function() {
            try {
                this.getController().mixins.mixinUIICP_LongtextNotifController.loadLongtext.call(this);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNotifLongtext of BaseUIIntegratedConfPage_LongtextNotifController', ex);
            }
        },

        onAddLongtextClick: function() {
            try {
                this.trySaveLongtext()
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddLongtextClick of BaseUIIntegratedConfPage_LongtextNotifController', ex);
            }
        },
        
        //context: integratedConfPageController
        trySaveLongtext: function() {
            try {                
                this._saveRunning = true;		
                AssetManagement.customer.controller.ClientStateController.enterLoadingState();
                this.getView().getLongtextNotifView().transferViewStateIntoModel();
				//get busobjkey from specific busobject
				var busobjkey = this.getView().getViewModel().get('notif').get('qmnum');

                var saveCallBack = function(success) {
					if(success === true) {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('longtextSaved'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingLongtext'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
                }

                var longtext = this.getView().getViewModel().get('notifLongtextClient');
                var eventId = AssetManagement.customer.manager.NotifManager.saveNotifLongtext(longtext, this.getView().getViewModel().get('notif'));
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallBack);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveLongtext of BaseUIIntegratedConfPage_LongtextNotifController', ex);
                this._saveRunning = false;
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
        }
});