﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_GeneralTimeConf', {
    extend: 'AssetManagement.customer.view.pages.OxPage',

        inheritableStatics: {
	       _uniqueId: 'UIICP_GTC001',
        _controller: null,

        getUid: function() {
            return this._uniqueId;
        },

        getUiDefinition: function (controller) {
            var retval = null;
            try {
                this._controller = controller;
                retval = Ext.create('Ext.container.Container', {
                    id: 'UIIntegratedConfPanel_GeneralTimeConfContainer',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    margin: '20 5 0 5',
                    items: [{
                            xtype: 'label',
                            cls: 'oxHeaderLabelBlue',
                            text: Locale.getMsg('timeConfirmations')
                        },{
                            xtype: 'container',
                            id: this._uniqueId +'timeConfPanel',
                            //hideContextMenuColumn: true,
                            flex: 1,
                            margin: '5 0 0 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                            //leftContainer
                            {
                                xtype: 'component',
                                maxWidth: 10,
                                minWidth: 10
                            },{
                                xtype: 'container',
                                flex: 1,
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },
                                items: [
                                //upper container
                                {
                                    xtype: 'component',
                                    height: 15
                                },{
                                    //upper section - form
                                    xtype: 'container',
                                    flex: 1,
                                    items: [
                                    {
                                        xtype: 'container',
                                        items: [{
                                            xtype: 'container',
                                            id: 'createContainer',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    //hbox with 3 columns
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'left'
                                                    },
                                                    items: [
                                                        {//first column
                                                            xtype: 'container',
                                                            width: 190,
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                            {
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'activityType_container',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //activity type dropdown label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'activityTypeDropDownBox_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('timeConfs_Ilart'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    //activity type dropdown
                                                                    xtype: 'oxcombobox',
                                                                    displayField: 'text',
                                                                    queryMode: 'local',
                                                                    valueField: 'value',
                                                                    id: this._uniqueId + 'activityTypeDropDownBox',
                                                                    editable: false,
                                                                    listeners: {
                                                                        select: this._controller.mixins.mixinUIICP_GeneralTimeConfController.onLearrSelected,
                                                                        scope: this
                                                                    }
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'personalNumber_Container',
                                                                padding: '7 0 0 0',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //personal number textfield label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId + 'personalNumberTextField_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('personalNumber'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    //personal number textfield
                                                                    xtype: 'oxnumberfield',
                                                                    minValue: 0,
                                                                    maxValue: 99999999,
                                                                    // Remove spinner buttons, and arrow key and mouse wheel listeners
                                                                    hideTrigger: true,
                                                                    keyNavEnabled: false,
                                                                    mouseWheelEnabled: false,
                                                                    id: this._uniqueId + 'personalNumberTextField'
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'breakBegin_Container',
                                                                padding: '5 0 0 0',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //start break date field label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'breakBegin_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('breakStart'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                }, {
                                                                    //start break date field
                                                                    xtype: 'timefield',
                                                                    flex: 1,
                                                                    format: 'H:i',
                                                                    id: this._uniqueId + 'breakBeginTimeField',
                                                                    editable: true
                                                                }]
                                                            }]
                                                        },{
                                                            //spacer
                                                            xtype: 'component',
                                                            width: 10
                                                        },{
                                                            //second column
                                                            xtype: 'container',
                                                            width: 190,
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                            {
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'ktxt_container',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //shorttext label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'ktxt_label',
                                                                    text: Locale.getMsg('shorttext'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    //shorttext textfield
                                                                    xtype: 'oxtextfield',
                                                                    maxLength: 40,
                                                                    id: this._uniqueId + 'ltxa1_textfield'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 2
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'activityBeginContainer',
                                                                padding: '5 0 0 0',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //activity begin date field label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'activityBegin_label',
                                                                    text: Locale.getMsg('activityStart'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    xtype: 'container',
                                                                    padding: '5 0 0 0',
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'left'
                                                                    },
                                                                    items: [{
                                                                            //activity begin date field
                                                                            xtype: 'datefield',
                                                                            format: 'd.m.Y',
                                                                            flex: 3,
                                                                            id: this._uniqueId + 'activityBeginDateField',
                                                                            editable: false,
                                                                            listeners: {
                                                                                select: this._controller.mixins.mixinUIICP_GeneralTimeConfController.onActivityTimeChanged,
                                                                                scope: this
                                                                            }
                                                                        },{
                                                                            //spacer
                                                                            xtype: 'component',
                                                                            width: 5
                                                                        },{
                                                                            //activity begin time field
                                                                            xtype: 'timefield',
                                                                            format: 'H:i',
                                                                            flex: 2,
                                                                            id: this._uniqueId + 'activityBeginTimeField',
                                                                            editable: true,
                                                                            listeners: {
                                                                                select: this._controller.mixins.mixinUIICP_GeneralTimeConfController.onActivityTimeChanged,
                                                                                scope: this
                                                                            }
                                                                        }
                                                                    ]
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'breakEndContainer',
                                                                padding: '11 0 0 0',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //break end date field label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'breakEnd_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('breakEnd'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    //break end date field
                                                                    xtype: 'timefield',
                                                                    flex: 1,
                                                                    format: 'H:i',
                                                                    id: this._uniqueId + 'breakEndTimeField',
                                                                    editable: true
                                                                }]
                                                            }]
                                                        },{
                                                            //spacer
                                                            xtype: 'component',
                                                            width: 10
                                                        },{
                                                            //third column
                                                            xtype: 'container',
                                                            width: 190,
                                                            layout: {
                                                                type: 'vbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                            {
                                                                xtype: 'container',
                                                                width: 190,
                                                                id: this._uniqueId +'accountIndicationContainer',
                                                                layout: {
                                                                    type: 'vbox',
                                                                    align: 'stretch'
                                                                },
                                                                items: [
                                                                {
                                                                    //activity type dropdown label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'accountIndicationDropDownBox_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('accountIndiShort'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    //activity type dropdown
                                                                    xtype: 'oxcombobox',
                                                                    flex: 1,
                                                                    displayField: 'text',
                                                                    queryMode: 'local',
                                                                    valueField: 'value',
                                                                    id: this._uniqueId + 'accountIndicationDropDownBox',
                                                                    editable: false
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                id: this._uniqueId +'activityEndContainer',
                                                                padding: '7 0 0 0',
                                                                width: 190,
                                                                layout: {
                                                                    type: 'vbox'
                                                                },
                                                                items: [
                                                                {
                                                                    //activity begin date field label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'activityEnd_label',
                                                                    text: Locale.getMsg('activityEnd'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },
                                                                {
                                                                    xtype: 'container',
                                                                    padding: '5 0 0 0',
                                                                    width: 190,
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'left'
                                                                    },
                                                                    items: [
                                                                    {
                                                                        //activity begin date field
                                                                        xtype: 'datefield',
                                                                        format: 'd.m.Y',
                                                                        flex: 3,
                                                                        id: this._uniqueId + 'activityEndDateField',
                                                                        editable: false,
                                                                        listeners: {
                                                                            select: this._controller.mixins.mixinUIICP_GeneralTimeConfController.onActivityTimeChanged,
                                                                            scope: this
                                                                        }
                                                                    },{
                                                                        //spacer
                                                                        xtype: 'component',
                                                                        width: 5
                                                                    },{
                                                                        //activity begin time field
                                                                        xtype: 'timefield',
                                                                        format: 'H:i',
                                                                        flex: 2,
                                                                        id: this._uniqueId + 'activityEndTimeField',
                                                                        editable: true,
                                                                        listeners: {
                                                                            select: this._controller.mixins.mixinUIICP_GeneralTimeConfController.onActivityTimeChanged,
                                                                            scope: this
                                                                        }
                                                                    }]
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                id: this._uniqueId +'worktimeContainer',
                                                                padding: '11 0 0 0',
                                                                hidden: false,
                                                                items: [
                                                                {
                                                                    //workTime Field label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'workTime_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('worktime'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'left'
                                                                    },
                                                                    items: [{
                                                                            //workTime Field
                                                                            xtype: 'timefield',
                                                                            flex: 1,
                                                                            format: 'H:i',
                                                                            increment: 1,
                                                                            id: this._uniqueId + 'workTimeField',
                                                                            editable: true
                                                                        }
                                                                    ]
                                                                }]
                                                            },{
                                                                xtype: 'container',
                                                                id: this._uniqueId +'amountContainer',
                                                                padding: '8 0 0 0',
                                                                hidden: true,
                                                                items: [
                                                                {
                                                                    //amount Field label
                                                                    xtype: 'label',
                                                                    id: this._uniqueId +'amount_label',
                                                                    editable: false,
                                                                    text: Locale.getMsg('amount'),
                                                                    cls: 'oxDescLabelBlue'
                                                                },{
                                                                    //spacer
                                                                    xtype: 'component',
                                                                    height: 5
                                                                },{
                                                                    xtype: 'container',
                                                                    layout: {
                                                                        type: 'hbox',
                                                                        align: 'left'
                                                                    },
                                                                    items: [{
                                                                            //amount Field
                                                                            xtype: 'oxtextfield',
                                                                            flex: 1,
                                                                            format: 'H:i',
                                                                            increment: 1,
                                                                            id: this._uniqueId + 'amountTextField',
                                                                            editable: true
                                                                        }
                                                                    ]
                                                                }]
                                                            }]
                                                        },{
                                                            xtype: 'container',
                                                            width: 60,
                                                            padding: '75 0 0 0',
                                                            items: [{
                                                                xtype: 'button',
                                                                id: this._uniqueId + 'addTimeConf_button',
                                                                flex: 1,
                                                                bind: {
                                                                    html: '<div><img width="100%" src="resources/icons/save.png"></img></div>'
                                                                },
                                                                padding: '0 5 0 5',
                                                                listeners: {
                                                                    click: this._controller.mixins.mixinUIICP_GeneralTimeConfController.onAddTimeConfClick,
                                                                    scope: this._controller
                                                                }
                                                            }
                                                        ]}
                                                    ]}
                                                ]
                                            }
                                        ]
                                    },{
                                        //spacer
                                        xtype: 'component',
                                        flex: 1,
                                        height: 30
                                    },{
                                        //header label timeconfs container
                                        xtype: 'label',
                                        cls: 'oxHeaderLabel',
                                        width: 100,
                                        text: Locale.getMsg('timeConfs_RecTimeConfs')
                                    },{
                                        //spacer
                                        xtype: 'component',
                                        height: 10
                                    },{
                                        //container with created timeconfs
                                        xtype: 'container',
                                        flex: 1,
                                        id: this._uniqueId + 'timeConfsContainer',
                                        items: [
                                            {
                                                xtype: 'oxgridpanel',
                                                id: this._uniqueId + 'timeConfGrid',
                                                disableSelection: true,
                                                columns: [
                                                    {
                                                        //column with timconf image
                                                        xtype: 'gridcolumn',
                                                        dataIndex: 'string',
                                                        text: '',
                                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                            return '<img src="resources/icons/timeconf.png"  />';
                                                        },
                                                        maxWidth: 80,
                                                        minWidth: 80,
                                                        align: 'center'
                                                    },{
                                                        //column activity type and description
                                                        xtype: 'gridcolumn',
                                                        dataIndex: 'learr',
                                                        flex: 1,
                                                        text: Locale.getMsg('activityType'),
                                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                            var retval = '';

                                                            try {
                                                                var learr = '';
                                                                var learrTxt = '';

                                                                var activityType = record.get('activityType');

                                                                if (activityType) {
                                                                    learr = activityType.get('lstar');
                                                                    learrTxt = activityType.get('ktext');
                                                                }
                                                                else {
                                                                    learr = record.get('learr');
                                                                }

                                                                retval = AssetManagement.customer.utils.StringUtils.concatenate([ learr, learrTxt ], '-', true);
                                                            }
                                                            catch (ex) {
                                                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfGrid/learr-column in TimeConfPage', ex);
                                                            }

                                                            return retval;
                                                        }
                                                        },{
                                                            //column account Indication
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'bemot',
                                                            flex: 1,
                                                            text: Locale.getMsg('accountIndication'),
                                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                                var retval = '';

                                                                try {
                                                                    var accountIndication = record.get('accountIndication');

                                                                    retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                                                                }
                                                                catch (ex) {
                                                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfGrid/bemot-column in TimeConfPage', ex);
                                                                }

                                                                return retval;
                                                            }
                                                        },{
                                                            //column startdate
                                                            xtype: 'datecolumn',
                                                            dataIndex: 'isd',
                                                            maxWidth: 125,
                                                            minWidth: 125,
                                                            text: Locale.getMsg('date'),
                                                            format: 'd.m.Y'
                                                        },{
                                                            //column work time
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'idaur',
                                                            maxWidth: 125,
                                                            minWidth: 125,
                                                            text: Locale.getMsg('worktime'),
                                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                                return record.getWorkValueForDisplay();
                                                            }
                                                        },{
                                                            //column personalno.
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'ltxa1',
                                                            flex: 1.5,
                                                            text: Locale.getMsg('specification'),
                                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                                return record.get('ltxa1');
                                                            }
                                                        },{
                                                            //column final conf
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'aueru',
                                                            flex: 0.5,
                                                            text: Locale.getMsg('finalConfirmation'),
                                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                                var aueru = record.get('aueru');

                                                                if (aueru === 'X')
                                                                    return '[END]';

                                                                return '';
                                                            }
                                                        }
                                                    ],
                                                    listeners: {
                                                        cellcontextmenu: controller.onCellContextMenu,
                                                        scope: controller
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]

                        }]
                        }
                    ]
                });
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUiDefinition of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }
            return retval;
        },
        
        transferModelStateIntoView: function() {
            try {
                //set general data
                //var order = this._controller.getViewModel().get('order');
                //var operation = this._controller.getViewModel().get('operation');
                var timeConf = this._controller.getViewModel().get('timeConf');  


                //select the first 
                //init times

                //set start/end 
	    	    Ext.getCmp(this._uniqueId + 'activityBeginDateField').setValue(timeConf.get('isd'));   
	    	    Ext.getCmp(this._uniqueId + 'activityBeginTimeField').setValue(timeConf.get('isd'));
	    	    Ext.getCmp(this._uniqueId + 'activityEndDateField').setValue(timeConf.get('ied'));     
	    	    Ext.getCmp(this._uniqueId + 'activityEndTimeField').setValue(timeConf.get('ied'));

	    	    Ext.getCmp(this._uniqueId + 'workTimeField').setValue(timeConf.get('idaur'));
                
	    	
	    	    Ext.getCmp(this._uniqueId + 'breakBeginTimeField').setValue(timeConf.get('pausesz'));
	    	    Ext.getCmp(this._uniqueId + 'breakEndTimeField').setValue(timeConf.get('pauseez'));

	    	    //set description
	    	    Ext.getCmp(this._uniqueId + 'ltxa1_textfield').setValue(timeConf.get('ltxa1'));
	    	                                                                                           
	    	    //set description
	    	    Ext.getCmp(this._uniqueId + 'personalNumberTextField').setValue(timeConf.get('pernr'));
	    	    //set final flag
	    	    //Ext.getCmp('finalConf').setValue(timeConf.get('aueru') === 'X');
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }
        },
        
        //fill drop down box for activities and bemot
        fillDropDownBoxes: function() {
            try {
                
			    var accountIndications = this._controller.getViewModel().get('accountIndications');
			    //filling accountIndication dropdownbox
			    bemotComboBoxSource = Ext.create('Ext.data.Store', {
			        fields: ['text', 'value']
			    });
	
			    if(accountIndications && accountIndications.getCount() > 0) {
				    accountIndications.each(function(accountIndication) {
					    bemotComboBoxSource.add({ text: (accountIndication.get('bemot') + ' - ' + accountIndication.get('bemottxt')), value: accountIndication });
				    }, this);
			    }
			
			    var bemotComboBox = Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox');
			    bemotComboBox.clearValue();
			    bemotComboBox.setStore(bemotComboBoxSource);
                
				bemotComboBox.select(bemotComboBoxSource.data.items[0]);
				bemotComboBox.fireEvent('select', bemotComboBox, bemotComboBoxSource.data.items[0]);

                //check if operation is relevant for general costs or work costs
                var work = this._controller.isWorkCostOperation();

                //filling activity type combo 
                var activitySource = this._controller.mixins.mixinUIICP_GeneralTimeConfController.getCust_041Store(this._controller.getViewModel().get('cust_041'), this._controller.getViewModel().get('cust_043'), work);

			    var actComboBox = Ext.getCmp(this._uniqueId + 'activityTypeDropDownBox');
			    actComboBox.clearValue();
			    actComboBox.setStore(activitySource);
				actComboBox.select(activitySource.data.items[0]);
				actComboBox.fireEvent('select', actComboBox, activitySource.data.items[0]);

		    } catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
		    }
	    },   
        
        showBreakTimes: function(show) {
            try {
                var hidden = !show;
                Ext.getCmp(this._uniqueId + 'breakBeginTimeField').setHidden(hidden);
                Ext.getCmp(this._uniqueId + 'breakEndTimeField').setHidden(hidden);
                Ext.getCmp(this._uniqueId + 'breakBegin_label').setHidden(hidden);
                Ext.getCmp(this._uniqueId + 'breakEnd_label').setHidden(hidden);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showBreakTimes of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }    
        },   
        
        showActivityTimes: function(show) {
            try {
                var hidden = !show;
                Ext.getCmp(this._uniqueId + 'activityBeginDateField').setHidden(hidden);
                Ext.getCmp(this._uniqueId + 'activityBeginTimeField').setHidden(hidden);
                Ext.getCmp(this._uniqueId + 'activityBegin_label').setHidden(hidden);
                Ext.getCmp(this._uniqueId + 'activityEndContainer').setHidden(hidden);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showActivityTimes of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }    
        },   
        
        showWork: function(show) {
            try {
                var hidden = !show;
                Ext.getCmp(this._uniqueId +'worktimeContainer').setHidden(hidden);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showWork of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }    
        },  
        
        showAmount: function(show) {
            try {
                var hidden = !show;
                Ext.getCmp(this._uniqueId +'amountContainer').setHidden(hidden);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAmount of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }    
        },   
            
        setTimeCalculation: function(flag) {
            try {
                if(flag === 'X' || flag === false) 
                    Ext.getCmp(this._uniqueId + 'workTimeField').setEditable(false);
                else
                    Ext.getCmp(this._uniqueId + 'workTimeField').setEditable(true);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setTimeCalculation of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
            }
        },        
	
	    getCurrentInputValues: function() {
		    var retval = null;
			
		    try {                
                var startDateHelper = Ext.getCmp(this._uniqueId + 'activityBeginDateField').getValue();
                var startTimeHelper = Ext.getCmp(this._uniqueId + 'activityBeginTimeField').getValue();                  
                startDateHelper.setHours(startTimeHelper.getHours());           
                startDateHelper.setMinutes(startTimeHelper.getMinutes()); 
                
                if(Ext.getCmp(this._uniqueId + 'activityBeginDateField').hidden === true)
                    startDateHelper === null;                

                var endDateHelper = Ext.getCmp(this._uniqueId + 'activityEndDateField').getValue();
                var endTimeHelper = Ext.getCmp(this._uniqueId + 'activityEndTimeField').getValue();                
                endDateHelper.setHours(endTimeHelper.getHours());           
                endDateHelper.setMinutes(endTimeHelper.getMinutes());  

                if(Ext.getCmp(this._uniqueId + 'activityEndDateField').hidden === true)
                    endDateHelper === null;
                                                                                     
                var breakBeginHelper = null;
                if (Ext.getCmp(this._uniqueId + 'breakBeginTimeField').getValue() !== null &&
                        Ext.getCmp(this._uniqueId + 'breakBeginTimeField').hidden === false) {                                        
                    breakBeginHelper = startDateHelper;                
                    breakBeginHelper.setHours(Ext.getCmp(this._uniqueId + 'breakBeginTimeField').getValue().getHours());           
                    breakBeginHelper.setMinutes(Ext.getCmp(this._uniqueId + 'breakBeginTimeField').getValue().getMinutes()); 
                }
                     
                var breakEndHelper = null;
                if (Ext.getCmp(this._uniqueId + 'breakEndTimeField').getValue() !== null &&
                        Ext.getCmp(this._uniqueId + 'breakEndTimeField').hidden === false) {  
                    breakEndHelper = startDateHelper;
                    breakEndHelper.setHours(Ext.getCmp(this._uniqueId + 'breakEndTimeField').getValue().getHours());           
                    breakEndHelper.setMinutes(Ext.getCmp(this._uniqueId + 'breakEndTimeField').getValue().getMinutes()); 
                } 
			    retval = {
                    activityType: Ext.getCmp(this._uniqueId + 'activityTypeDropDownBox').getValue(),
				    accountIndication: Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').getValue(),
				    work: Ext.getCmp(this._uniqueId + 'workTimeField').getValue(), 
				    amount: Ext.getCmp(this._uniqueId + 'amountTextField').getValue(),                                                                                                                                           
                    startDate: startDateHelper,
				    endDate: endDateHelper,
                    beginBreak: breakBeginHelper,
                    endBreak: breakEndHelper
                };
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseUIIntegratedConfPage_GeneralTimeConf', ex);
			    retval = null;
		    }
		
		    return retval;
	    },
            
	    transferViewStateIntoModel: function() {
		    try {
			    var myModel = this._controller.getViewModel();
			    var timeConf = myModel.get('timeConf');
			    var operation = myModel.get('operation');
			
			    var activityType = Ext.getCmp(this._uniqueId + 'activityTypeDropDownBox').getValue(); 
			    var accountIndication = Ext.getCmp(this._uniqueId + 'accountIndicationDropDownBox').getValue();
			
			    //set operation data
			    if(operation) {
				    timeConf.set('vornr', operation.get('vornr'));
				    timeConf.set('split', operation.get('split'));
			    } else {
				    timeConf.set('vornr', '');
				    timeConf.set('split', '');
			    }
			
			    //set activity type
			    if(activityType) {
				    timeConf.set('activityType', activityType);
				    timeConf.set('learr', activityType.get('lstar'));
			    } else {
				    timeConf.set('activityType', null);
				    timeConf.set('learr', '');
			    }
			
			    //set bemot
			    if(accountIndication) {
				    timeConf.set('accountIndication', accountIndication);
				    timeConf.set('bemot', accountIndication.get('bemot'));
			    } else {
				    timeConf.set('accountIndication', null);
				    timeConf.set('bemot', '');
			    }
			
			    //set unit
			    var unit = '';	
			
			    //if(activityType)
				//    unit = activityType.get('leinh');
			
			    if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit) && operation)
				    unit = operation.get('arbeh');
				
			    if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit))
			        unit = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('default_timeunit');
				
			    timeConf.set('ismne', unit);
			    timeConf.set('idaue', unit);
			
			    //set start/end date
                var startDateHelper = Ext.getCmp(this._uniqueId + 'activityBeginDateField').getValue();
                var startTimeHelper = Ext.getCmp(this._uniqueId + 'activityBeginTimeField').getValue();                  
                startDateHelper.setHours(startTimeHelper.getHours());           
                startDateHelper.setMinutes(startTimeHelper.getMinutes()); 
                
                if(Ext.getCmp(this._uniqueId + 'activityBeginDateField').hidden === true)
                    startDateHelper === null;                

                var endDateHelper = Ext.getCmp(this._uniqueId + 'activityEndDateField').getValue();
                var endTimeHelper = Ext.getCmp(this._uniqueId + 'activityEndTimeField').getValue();                
                endDateHelper.setHours(endTimeHelper.getHours());           
                endDateHelper.setMinutes(endTimeHelper.getMinutes());  

                if(Ext.getCmp(this._uniqueId + 'activityEndDateField').hidden === true)
                    endDateHelper === null;

                var endDate = endDateHelper;
                var startDate = startDateHelper;
			
			    timeConf.set('isd', startDate);
			    timeConf.set('ied', endDate);
			
			    //set work value
			    if(unit.toUpperCase() === 'H' || unit.toUpperCase() === 'STD') {
				    var minutes = Ext.getCmp(this._uniqueId + 'workTimeField').getValue().getMinutes(); 
				    var hours = Ext.getCmp(this._uniqueId + 'workTimeField').getValue().getHours();
				
				    hours = hours + '.' + (minutes*100)/60;
			    } else {
				    hours = Ext.getCmp(this._uniqueId + 'workTimeField').getValue();
			    }

			    timeConf.set('ismnw', hours);
			    timeConf.set('idaur', hours);
			
			    //set description
			    timeConf.set('ltxa1', Ext.getCmp(this._uniqueId + 'ltxa1_textfield').getValue());

			    timeConf.set('childKey', operation.get('childKey'));
		    } catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of TimeConfPage', ex);
		    }
	   }    	   
	}
}
);