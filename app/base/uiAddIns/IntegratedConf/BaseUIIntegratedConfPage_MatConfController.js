﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_MatConfController', {
    extend: 'Ext.Mixin',
    mixinId: 'mixinUIICP_MatConfController',
	
    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],

    onAddMatConfClick: function() {
        try {
            this.trySaveMatConf();    
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddMatConfClick of BaseUIIntegratedConfPage_MatConfController', ex);
        }    
    }, 
    
	//will perform a set of checks on the current input values before those values will be transferred into the current matConf and saved to the database
        trySaveMatConf: function() {
            try {
                this._saveRunning = true;
			
                AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
                var me = this;
			
                var afterInputCheckAction = function(inputCheckWasSuccessfull) {
                    try {
                        if (inputCheckWasSuccessfull === true) {
                            var afterStorLocQuantityCheckAction = function(doContinue) {
                                try {
                                    if (doContinue === true) {
                                        //get matconf to save
                                        var matConf = me.getView().getController().getViewModel().get('matConf');
									
                                        me.getView().getMatConfView().transferViewStateIntoModel();
                                        matConf.set('unit', matConf.get('unit').toUpperCase());
									
                                        var material = me.getView().getController().getViewModel().get('material');
                                        matConf.set('material', material ? material : null);
								
                                        var callback = function(success) {
                                            try {
                                                if (success === true) {
                                                    me.addMatConfToListStore(matConf);
                                                    me.initializeNewMatConf.call(me.getView());
												
                                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedMatConf'), true, 0);
                                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                                }
                                                else {
                                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingMatConf'), false, 2);
                                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                                }
                                            }
                                            catch (ex) {
                                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of MatConfPageController', ex)
                                            }
                                            finally {
                                                me._saveRunning = false;
                                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                            }
                                        };
									
                                        var eventId = AssetManagement.customer.manager.MatConfManager.saveMatConf(matConf);
									
                                        if (eventId > 0) {
                                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                                        }
                                        else {
                                            me._saveRunning = false;
                                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingMatConf'), false, 2);
                                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                        }
                                    }
                                    else {
                                        var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('saveCancelled'), true, 1);
                                        AssetManagement.customer.core.Core.getMainView().showNotification(message);
								
                                        me._saveRunning = false;
                                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    }
                                }
                                catch (ex) {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of MatConfPageController', ex)
                                }
                            };
					
                            //second perform a check against the selected storlocs quantity
                            me.performStorLocsQuantityCheck(afterStorLocQuantityCheckAction, me);
                        }
                        else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        }
                    }
                    catch (ex) {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of MatConfPageController', ex)
                    }
                };
			
                //first check inpit values
                this.mixins.mixinUIICP_MatConfController.checkInputValues.call(this, afterInputCheckAction, this);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of MatConfPageController', ex);
                this._saveRunning = false;
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
        },
	
	addMatConfToListStore: function(matConf) {
		try {
			var matConfs = this.getView().getController().getViewModel().get('operation').get('matConfs');
			
			if(!matConfs)
			{
				matConfs = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.MaterialConf',
					autoLoad: false
				});
				this.getView().getController().getViewModel().get('operation').set('matConfs', matConfs); 
			}
			
			var temp = matConfs.getById(matConf.get('id'));
			
			if(temp) {
				var index = matConfs.indexOf(temp);
				matConfs.removeAt(index);
				matConfs.insert(index, matConf);
			} else {
				matConfs.insert(0, matConf);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addMatConfToListStore of MatConfPageController', ex);
		}
	}, 
	
	//performs a set of checks, returning the overall result by the provided callback
	checkInputValues: function(callback, callbackScope)  {
		try {
			var retval = true; 
			var errorMessage = '';
			
			var values = this.getView().getMatConfView().getCurrentInputValues(); 
		
			var vornr = values.vornr;
			var isdd = values.isdd;
			var matnr = values.matnr;
			var unit = values.unit;
		    var quantity = values.quantity;
			var plant = values.plant;
			var storLoc = values.storLoc;	
		    var accountIndication = values.accountIndication;
		
			var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
		 
			if(isNullOrEmpty(plant)) {
				errorMessage = Locale.getMsg('providePlant');
				retval = false;
			} else if(isNullOrEmpty(matnr)) {
				errorMessage = Locale.getMsg('selectMaterial');
				retval = false;
			} else if(isNullOrEmpty(storLoc)) {
				errorMessage = Locale.getMsg('provideStorageLocation');
				retval = false;
			} else if(quantity === null || quantity === undefined || quantity <= 0) {
	            errorMessage = Locale.getMsg('provideValidQuantity');
	            retval = false;
			} else if(!accountIndication) {
				errorMessage = Locale.getMsg('chooseAccountIndication');
				retval = false;
			}
			
            var me = this;
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				if(callback)
					callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			if(retval === false) {
				returnFunction.call(this);
			} else {
				//last do the material check - this means:
				//if the chosen material does no longer match the matnr from the input value, try to load the now connected material
				//if this material exist and has a diffrent unit, return false, inform the user and tell him, what the correct unit is
				//if the material could not be loaded, assume that it exists at the sap system and pass the check
				var chosenMaterial = this.getView().getController().getViewModel().get('material');
				
				var unitCheckFunction = function(material) {
					try {
						if(material) {
							if(material.get('meins').toUpperCase() !== unit.toUpperCase()) {
								errorMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('unitDoesNotMatchMaterial'), [ material.get('meins') ]);
								retval = false;
							}
						}
						
						me.getView().getController().getViewModel().set('material', material);
						returnFunction.call(me);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of MatConfPageController', ex)
						
						if(callback) {
							callback.call(callbackScope ? callbackScope : me, false);
						}
					}
				};
				
				if(chosenMaterial && chosenMaterial.get('matnr') === matnr) {
					unitCheckFunction.call(this, chosenMaterial);
				} else {
					//load material from database
					var eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr);
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, unitCheckFunction);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of MatConfPageController', ex);
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	//checks the current set amount in the view against the available of the current selected storage Location
	//returns a boolean, indicating, if this check has been passed
	performStorLocsQuantityCheck: function(callback, callbackScope) {
		try {
			var values = this.getView().getMatConfView().getCurrentInputValues(); 
			
			var matnr = values.matnr;
			var quantity = values.quantity;
			var plant = values.plant;
			var storLoc = values.storLoc;
			var material = this.getView().getController().getViewModel().get('chosenMaterial');
			
			var returnFunction = function(retval) {
				if(callback)
					callback.call(callbackScope ? callbackScope : me, retval);
			};
		
			var me = this;
				
			var quanitityCheckResultCallback = function(quantityResult) {
				try {
					if(quantityResult == Number.NaN) {
						var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorWhileCheckingAvailableStorLocQuant'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(message);
						
						returnFunction.call(me, false);
					} else if(quantityResult == 0 || quantityResult == 2) {
						//user has to be informed and asked for proceeding
						var innerCallback = function(confirmed) {
							try {
								if(confirmed === false) {
									returnFunction.call(me, false);
								} else {
									AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
									returnFunction.call(me, true);
								}
							} catch(ex) {
					        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of MatConfPageController', ex);
					        	
					        	returnFunction.call(me, false);
					       	}    				
						};
						
						var dialogMessage = quantityResult == 0 ? Locale.getMsg('storLocQuantLessRequested') : Locale.getMsg('storLocEmpty');
						dialogMessage += '<br>' +  Locale.getMsg('postAnywayQuestion');
						
						var dialogArgs = {
						    title: Locale.getMsg('caution'),
						    icon: 'resources/icons/alert.png',
							message: dialogMessage,
							asHtml: true,
							callback: innerCallback,
							scope: this
						};
						
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
					} else {
						returnFunction.call(me, true);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of MatConfPageController', ex)
					returnFunction.call(me, false);
				}
			};
			
			var matnrToUse = material ? material.get('matnr') : matnr;
			var requestedQuantity = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(quantity);

			var eventId = AssetManagement.customer.manager.MaterialStockageManager.checkForAvailableQuantity(matnrToUse, plant, storLoc, requestedQuantity);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, quanitityCheckResultCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performStorLocsQuantityCheck of MatConfPageController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	onMaterialSelected: function(selectedObject) {
		try {
			var material = null;
			var viewTransferMethod = null;
			
			var typeOfSelectedObject = Ext.getClassName(selectedObject);
			
			//distinguish the type (diffrent methods)
			if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.Stpo') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().getMatConfView().fillMaterialSpecFieldsFromStpo;
			} else if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.MatStock') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().getMatConfView().fillMaterialSpecFieldsFromMatStock;
			} else if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.OrdComponent') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().getMatConfView().fillMaterialSpecFieldsFromComponent;
			}
			
			 this.getView().getController().getViewModel().set('material', material);
			
			if(viewTransferMethod) {
				viewTransferMethod.call(this.getView().getMatConfView(), selectedObject);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectMaterialClick of BaseUIIntegratedConfPage_MatConfController', ex);
		}
	},    
	
	onSelectMaterialClick: function(){
		try {
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.MATERIALPICKER_DIALOG, { order: order });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectMaterialClick of BaseUIIntegratedConfPage_MatConfController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			//set the operation according to the current data
			var myModel = this._controller.getViewModel();
			
			var curMatConf = myModel.get('matConf');
		
			//if no matconf is currently set, initialize a new one
			if(!curMatConf)
				this.initializeNewMatConf(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseUIIntegratedConfPage_MatConfController', ex);
		}
	}, 
	
	//will initialize a new material conf. and transfer it into the model
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewMatConf: function(skipRefreshView) {
		try {
			if(skipRefreshView === true)
				skipRefreshView = true;
			else
				skipRefreshView = false;
		
			var myModel = this.controller.getViewModel();
			var order = myModel.get('order');
			var operation = myModel.get('operation');
		
			var newMatConf = Ext.create('AssetManagement.customer.model.bo.MaterialConf', { order: order, operation: operation });
			newMatConf.set('moveType', '261');
			newMatConf.set('isdd', AssetManagement.customer.utils.DateTimeUtils.getCurrentDate());
			newMatConf.set('unit', 'ST');
			newMatConf.set('quantity', '1');
			
			//set a default werk, if non yet set - using first entry of the storage locations
			//this will work, as long as only one plant is customized
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(newMatConf.get('werks'))) {
				var storLocs = myModel.get('storLocs');
				
				if(storLocs && storLocs.getCount() > 0) {
					newMatConf.set('werks', storLocs.getAt(0).get('werks'));
				}
			}
		
			myModel.set('matConf', newMatConf);
			myModel.set('chosenMaterial', null);
			myModel.set('isEditMode', false);
			
			if(skipRefreshView === false)
				this.refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewMatConf of MatConfPageController', ex);
		}
	}
});