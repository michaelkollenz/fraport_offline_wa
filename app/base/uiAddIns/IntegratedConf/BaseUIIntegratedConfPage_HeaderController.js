﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_HeaderController', {
        mixinId: 'mixinUIICP_HeaderController',
	
    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],
    
	
    onTimeConfNavigate: function(menu, item, e, eOpts) {
        try {
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfNavigate of BaseUIIntegratedConfPage_HeaderController', ex);
        }
    },

    onMatConfNavigate: function(menu, item, e, eOpts) {
        try {	
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMatConfNavigate of BaseUIIntegratedConfPage_HeaderController', ex);
        }
    },

    onNotifTextNavigate: function(menu, item, e, eOpts) {
        try {	
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTextNavigate of BaseUIIntegratedConfPage_HeaderController', ex);
        }
    },
    
	
	onEquipmentSelected: function(comboBox, record) {
		try {
			var myModel = this.getViewModel();
			var currentOperation = myModel.get('operation');
			var selectedOperation = comboBox.getValue();
			
			if(selectedOperation !== currentOperation) {
				myModel.set('operation', selectedOperation);
                this.loadOperationNotification();
				//this.requestPage(this._pageRequestCallback, this._pageRequestCallbackScope, { order: myModel.get('order'), operation: myModel.get('operation') })
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseUIIntegratedConfPage_HeaderController', ex);
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	}
});