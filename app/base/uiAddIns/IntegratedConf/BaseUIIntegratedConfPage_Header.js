﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_Header', {
    extend: 'AssetManagement.customer.view.pages.OxPage',
        
    
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_HeaderController'
    ],
    inheritableStatics: {
        _uniqueId: 'UIICP_HEAD001',
        _controller: null,

        getUid: function() {
            return this._uniqueId;
        },

        getUiDefinition: function (controller) {
            var retval = null;
            this._controller = controller;//.mixins.mixinUIICP_HeaderController.fooBar();
            //var vm = controller.getViewModel();
            try {
                retval = Ext.create('Ext.container.Container', {
                                        id: 'UIIntegratedConfPanel_HeaderPanel',
                                        layout: {
                                            type: 'vbox',
                                            align: 'stretch'
                                        },     
	                                    margin: '10 5 0 5',
                                        items: [
                                            {
                                                xtype: 'container',
		                                        layout: {
		                                            type: 'hbox',
		                                            align: 'stretch'
		                                        },
                                                items: [
                                                    {
                                                        xtype: 'component',
                                                        width: 12
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        width: 150,
		                                                layout: {
		                                                    type: 'vbox',
		                                                    align: 'stretch'
		                                                },
		                                                items: [
                                                        //leftContainer  
                                                            {
                                                                xtype: 'component',
                                                                height: 10
                                                            },
                                                            {
                                                                //order label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'orderDesc_label',
                                                                editable: false,
                                                                text: Locale.getMsg('order'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },
                                                            {
                                                                //notification label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'notificationDesc_label',
                                                                editable: false,
                                                                text: Locale.getMsg('notification'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },
                                                            {
                                                                //material label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'materialDesc_label',
                                                                editable: false,
                                                                text: Locale.getMsg('material'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },
                                                            {
                                                                //equipment label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'equipmentDesc_label',
                                                                editable: false,
                                                                text: Locale.getMsg('equipment'),
                                                                cls: 'oxDescLabelBlue'
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            }
		                                                ]
                                                    },{
                                                        //spacer
                                                        xtype: 'component',
                                                        width: 10
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        width: 250,
		                                                layout: {
		                                                    type: 'vbox',
		                                                    align: 'stretch'
		                                                },
		                                                items: [                                                    
                                                        //rightContainer 
                                                            {
                                                                xtype: 'component',
                                                                height: 10
                                                            },
                                                            {
                                                                //order label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'order_label',
                                                                editable: false
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },
                                                            {
                                                                //notification label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'notification_label',
                                                                editable: false
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },
                                                            {
                                                                //material label
                                                                xtype: 'label',
		                                                        height: 20,
                                                                id: this._uniqueId +'material_label',
                                                                editable: false
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            },
                                                            {
		                                                        //equipment combobox
		                                                        xtype: 'oxcombobox',
		                                                        flex: 1,
		                                                        border: 5,
		                                                        height: 20,
		                                                        id: this._uniqueId +'equipmentDropDownBox',
		                                                        labelWidth: 110,
		                                                        displayField: 'text',
		                                                        queryMode: 'local',
		                                                        valueField: 'value',
		                		                                editable: false,
		                                                        listeners: {                                                         
                                                                    select: this._controller.mixins.mixinUIICP_HeaderController.onEquipmentSelected,
                                                                    scope: this._controller   
		                                                        }
                                                            },{
                                                                //spacer
                                                                xtype: 'component',
                                                                height: 5
                                                            }
		                                                ]
                                                    }
                                                ]
                                            }
                                        ]
                                    });
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUiDefinition of UIIntegratedConfPage_GeneralTimeConf', ex);
            }
            return retval;
        },

        transferModelStateIntoView: function() {
            try {
                //set general data
                var order = this._controller.getViewModel().get('order');
                var operation = this._controller.getViewModel().get('operation');
                if (operation !== null) {
                    var objListItem = AssetManagement.customer.manager.ObjectListManager.getObjectListItemForOperation(order, operation);
                }
                
                Ext.getCmp(this._uniqueId + 'order_label').setText(AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0') + " " + order.get('ktext'));

                if(objListItem !== null) {
			        if(objListItem.get('qmnum') !== null && order.get('qmnum') !== undefined)
				        Ext.getCmp(this._uniqueId +'notification_label').setText(AssetManagement.customer.utils.StringUtils.trimStart(objListItem.get('ihnum'), '0') + " " + objListItem.get('qmtxt'));
			        else
			            Ext.getCmp(this._uniqueId +'notification_label').setText(AssetManagement.customer.utils.StringUtils.trimStart(''));
                    
			        if(objListItem.get('matnr') !== null && order.get('matnr') !== undefined)
				        Ext.getCmp(this._uniqueId +'material_label').setText(AssetManagement.customer.utils.StringUtils.trimStart(objListItem.get('matnr'), '0') + " " + objListItem.get('maktx'));
			        else
			            Ext.getCmp(this._uniqueId +'material_label').setText(AssetManagement.customer.utils.StringUtils.trimStart(''));
                }



			    /*
			    if(order.get('equipment') !== null && order.get('equipment') !== undefined)
				    Ext.getCmp('timeConfEquiTF').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0') + " " + order.get('equipment').get('eqktx'));
			    else
				    Ext.getCmp('timeConfEquiTF').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0'));*/
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseUIIntegratedConfPage_Header', ex);
            }
        },
        
        //fill drop down box for operations/equipments
        fillDropDownBoxes: function() {
            try {                
			    //filling equi dropdownbox
			    var equiComboBoxSource = Ext.create('Ext.data.Store', {
			        fields: ['text', 'value']
			    });
			    
                
                var order = this._controller.getViewModel().get('order');
                var operations = order.get('operations');
                
			    if(operations && operations.getCount() > 0) {
				    operations.each(function(oper) {
				        var objListItem = AssetManagement.customer.manager.ObjectListManager.getObjectListItemForOperation(order, oper);
                        if(objListItem !== null && objListItem !== 'undefined'){
					        equiComboBoxSource.add({ text: AssetManagement.customer.utils.StringUtils.trimStart(objListItem.get('equnr'), '0') + ' ' + objListItem.get('eqtxt'), value: oper });
                        }						
				    }, this);
			    }
                
			    var equiCb = Ext.getCmp(this._uniqueId + 'equipmentDropDownBox');
			    equiCb.clearValue();
			    equiCb.setStore(equiComboBoxSource);

			    var operationRecord = this._controller.getViewModel().get('operation') ? equiCb.findRecordByValue(this._controller.getViewModel().get('operation')) : null;				
                if (operationRecord) {
                    Ext.getCmp(this._uniqueId + 'equipmentDropDownBox').setValue(operationRecord);
				    Ext.getCmp(this._uniqueId + 'equipmentDropDownBox').fireEvent('select', Ext.getCmp(this._uniqueId + 'equipmentDropDownBox'), operationRecord);
                }
		    } catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseUIIntegratedConfPage_Header', ex);
		    }
	    }
    }

}
);