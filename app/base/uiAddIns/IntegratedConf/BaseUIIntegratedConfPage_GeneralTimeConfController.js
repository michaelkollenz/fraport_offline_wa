﻿Ext.define('AssetManagement.base.uiAddIns.IntegratedConf.BaseUIIntegratedConfPage_GeneralTimeConfController', {
    extend: 'Ext.Mixin',
    mixinId: 'mixinUIICP_GeneralTimeConfController',       

    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],

        getCust_041Store: function (cust_041Complete, cust_043Complete, work) {
            var retVal = null;
            try {           
                if (cust_041Complete !== null && cust_041Complete !== 'undefined' && cust_043Complete !== null && cust_043Complete !== 'undefined') {
                    var c043Array = [];
                    var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
                    cust_043Complete.each(function (c043) {
                        if (work) {
                            if (c043.get('acgroup') === funcParaInstance.getValue1For('work_exp_group'))
                                c043Array.push(c043);
                        }
                        else {
                            if (c043.get('acgroup') === funcParaInstance.getValue1For('general_exp_group'))
                                c043Array.push(c043);
                        }
                    }, this);

                    var cust_041Store = Ext.create('Ext.data.Store', {
                            fields: ['text', 'value']
                        });
                    cust_041Complete.each(function (c041) {
                        for (var i = 0; i < c043Array.length; i++) {
                            //here: always use time confirmations
                            if (c041.get('actype') === c043Array[i].get('actype') && c041.get('target') === '01') {
                                cust_041Store.add({ text: c041.get('description'), value: c041 });
                            }
                        }
                    }, this);
                    retVal = cust_041Store;
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_041List of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
            }
            return retVal;
        },
	
        //event executed on changing of the activity type combo box
        onLearrSelected: function(comboBox, newValue, oldValue) {
            try {	
                var cust041 = null;
                if(comboBox !== null)
                    cust041 = comboBox.getValue();

                if(cust041 === null || cust041 === 'undefined') {
                    //Show all but amount
                    this.showBreakTimes(true);
                    this.showActivityTimes(true);
                    this.showWork(true);
                    this.showAmount(false);
                    this.setTimeCalculation('');
                }
                else {
                    switch(cust041.get('regtyp')) {
                        case '01': //Hours/QTY
                            this.showBreakTimes(false);
                            this.showActivityTimes(false);
                            this.showWork(true);
                            this.showAmount(false);
                            this.setTimeCalculation(cust041.get('durationcalc'));
                            break;
                        case '02': //Hours/Times
                            this.showBreakTimes(false);
                            this.showActivityTimes(true);
                            this.showWork(true);
                            this.showAmount(false);
                            this.setTimeCalculation(cust041.get('durationcalc'));
                            break;
                        case '03': //Hours/Times/Breaks
                            this.showBreakTimes(true);
                            this.showActivityTimes(true);
                            this.showWork(true);
                            this.showAmount(false);
                            this.setTimeCalculation(cust041.get('durationcalc'));
                            break;
                        case '04': //TODO: CATS => not implemented 
                            this.showBreakTimes(false);
                            this.showActivityTimes(false);
                            this.showWork(false);
                            this.showAmount(false);
                            this.setTimeCalculation(cust041.get('durationcalc'));
                            break;
                        case '05': //Amount
                            this.showBreakTimes(false);
                            this.showActivityTimes(false);
                            this.showWork(false);
                            this.showAmount(true);
                            this.setTimeCalculation(cust041.get('durationcalc'));
                            break;
                        default: //Show all but amount
                            this.showBreakTimes(true);
                            this.showActivityTimes(true);
                            this.showWork(true);
                            this.showAmount(false);
                            this.setTimeCalculation(cust041.get('durationcalc'));
                            break;
                    }
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLearrSelected of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
            }
        },

        onActivityTimeChanged: function(comboBox, newValue, oldValue) {
            try {                
                var startDateHelper = Ext.getCmp(this._uniqueId + 'activityBeginDateField').getValue();
                var startTimeHelper = Ext.getCmp(this._uniqueId + 'activityBeginTimeField').getValue();                  
                startDateHelper.setHours(startTimeHelper.getHours());           
                startDateHelper.setMinutes(startTimeHelper.getMinutes()); 
                

                var endDateHelper = Ext.getCmp(this._uniqueId + 'activityEndDateField').getValue();
                var endTimeHelper = Ext.getCmp(this._uniqueId + 'activityEndTimeField').getValue();                
                endDateHelper.setHours(endTimeHelper.getHours());           
                endDateHelper.setMinutes(endTimeHelper.getMinutes());  

                var duration = endDateHelper.getTime() - startDateHelper.getTime();
                var notification= "";
                if(duration < 0) {                    
			        notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noWorkTime'), true, 1);
			        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                }
                else if(duration >= 86400000) {
			        notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('timeToLarge'), true, 1);
			        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                }
                else {
                    var timeStampStartDateDay = new Date(startDateHelper.getYear(), startDateHelper.getMonth(), startDateHelper.getDay())
                    var workTime = new Date(timeStampStartDateDay.getTime() + duration);
                    Ext.getCmp(this._uniqueId + 'workTimeField').setValue(workTime);
                }

            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onActivityTimeChanged of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
            }
        },
    
	    //will initialize a new timeConf and transfer it into the model
	    //the view will be updated by default - use skipRefreshView to prevent this
        initializeNewTimeConf: function(skipRefreshView) {
            try {
                if (skipRefreshView === true)
                    skipRefreshView = true;
                else
                    skipRefreshView = false;
		
                var myModel = this.getViewModel();
                var order = myModel.get('order');
                var operation = myModel.get('operation');
		
                var newTimeConf = Ext.create('AssetManagement.customer.model.bo.TimeConf', { order: order, operation: operation });
			
                //set default unit, if time conf did not draw one from operation
                var timeConfsUnit = newTimeConf.get('ismne');
			
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsUnit)) {
                    timeConfsUnit = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('default_timeunit');
                    newTimeConf.set('ismne', timeConfsUnit);
                    newTimeConf.set('idaue', timeConfsUnit);
                }
			
                //set default activityType, if time conf did not draw any from operation
                var timeConfsLearr = newTimeConf.get('learr');
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsLearr)) {
                    var activityTypes = myModel.get('activityTypes');
			
                    if (activityTypes && activityTypes.getCount() > 0) {
                        timeConfsLearr = activityTypes.getAt(0).get('lstar');
                        newTimeConf.set('learr', timeConfsLearr);
                    }
                }
			
                //set default account indication, if time conf did not draw any from order
                var timeConfsBemot = newTimeConf.get('bemot');
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsBemot)) {
                    var accountIndications = myModel.get('accountIndications');
			
                    if (accountIndications && accountIndications.getCount() > 0) {
                        timeConfsBemot = accountIndications.getAt(0).get('bemot');
                        newTimeConf.set('bemot', timeConfsBemot);
                    }
                }
			
                //set default time value, matching to it's unit
                var defaultTimeInMinutes = 0;
                var asHours = true;
                var toTest = timeConfsUnit.toUpperCase();
                var toSet = 0;
			
                if (toTest === 'M' || toTest === 'MIN')
                    asHours = false;
			
                if (asHours) {
                    toSet = defaultTimeInMinutes / 60.0;
                }
                else {
                    toSet = defaultTimeInMinutes;
                }
			
                //bring into backend format
                var toSet = AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(toSet);
			
                newTimeConf.set('ismnw', toSet);
                newTimeConf.set('idaur', toSet);

                newTimeConf.set('ersda', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
                newTimeConf.set('isd', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
                newTimeConf.set('ied', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
                newTimeConf.set('budat', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
                newTimeConf.set('ernam', AssetManagement.customer.core.Core.getAppConfig().getUserId());
		
                myModel.set('timeConf', newTimeConf);
			
                if (skipRefreshView === false)
                    this.refreshView();
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewTimeConf of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
            }
        },         

        onAddTimeConfClick: function() {
            try {
                this.trySaveTimeConf();    
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddTimeConfClick of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
            }    
        },

        /**
	     * will try to save the current timeconf inside the view
	     * a set of checks will be run before the values are extracted from the view and saved to the database
	     */
        trySaveTimeConf: function() {
            try {
                this._saveRunning = true;
		
                AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
                var me = this;
                var saveAction = function(checkWasSuccessfull) {
                    try {
                        if (checkWasSuccessfull === true) {
                            var timeConf = me.getView().getController().getViewModel().get('timeConf'); 
						
                            me.getView().getTimeConfView().transferViewStateIntoModel();

                            var callback = function(success) {
                                try {
                                    if (success === true) {
                                        me.addTimeConfToListStore(timeConf);
                                        me.mixins.mixinUIICP_GeneralTimeConfController.initializeNewTimeConf.call(me);
									
                                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedTimeConf'), true, 0);
                                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                    }
                                    else {
                                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingTimeConf'), false, 2);
                                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                    }
                                }
                                catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
                                }
                                finally {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                }
                            };
						
                            var eventId = AssetManagement.customer.manager.TimeConfManager.saveTimeConf(timeConf);
						
                            if (eventId > 0) {
                                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                            }
                            else {
                                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingTimeConf'), false, 2);
                                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        }
                        else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        }
                    }
                    catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                };
			
                //check if data is complete first
                this.mixins.mixinUIICP_GeneralTimeConfController.checkInputValues.call(this, saveAction, this);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
                this._saveRunning = false;
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
        },
	 
	    /**
	     * checks if the data of the timeconf object is complete 
	     * returns null if the data is complete or an error message if data is missing
	     */
        checkInputValues: function(callback, callbackScope) {
            try {
                var retval = true;
                var errorMessage = '';
					
                var values = this.getView().getTimeConfView().getCurrentInputValues(); 
		
                var activityType = values.activityType;
                var accountIndication = values.accountIndication;
                var work = values.work;
                var startDate = values.startDate;	
                var endDate = values.endDate;
                var amount = values.amount;
		
                var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
		 
                var value = AssetManagement.customer.utils.DateTimeUtils.getTimeStringWithTimeZoneForDisplay(work);

                if (!activityType) {
                    errorMessage = Locale.getMsg('chooseActivityType');
                    retval = false;
                }
                else if (!accountIndication) {
                    errorMessage = Locale.getMsg('chooseAccountIndication');
                    retval = false;
                }
                if (Ext.getCmp(this.getView().getTimeConfView()._uniqueId + 'worktimeContainer').hidden === false) {
                    if (value ==='00:00') {
                        errorMessage = Locale.getMsg('noWorkTimeOper');
                        retval = false;
                    }
                }
                else if(isNullOrEmpty(amount)) {                    
                    errorMessage = Locale.getMsg('noAmount');
                    retval = false;
                }
                if (retval === true && startDate > endDate) {
                    errorMessage = Locale.getMsg('startAfterEndTime');
                    retval = false;
                }
			
                var returnFunction = function() {
                    if (retval === false) {
                        var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(message);
                    }
				
                    callback.call(callbackScope ? callbackScope : me, retval);
                };
			
                returnFunction.call(this);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseUIIntegratedConfPage_GeneralTimeConfController', ex)
			
                if (callback) {
                    callback.call(callbackScope ? callbackScope : this, false);
                }
            }
        },
	 
	/**
	 * if it's a new timeconf add it to store
	 */
        addTimeConfToListStore: function(timeConf) {
            try {
                var myModel = this.getView().getController().getViewModel();
                var order = myModel.get('order');
                var operation = myModel.get('operation');
                var curOpersTimeConfs = operation.get('timeConfs');
			
                if (!curOpersTimeConfs) {
                    timeConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.TimeConf',
                            autoLoad: false
                        });	
				
                    operation.set('timeConfs', timeConfs)
                }
			
                var temp = curOpersTimeConfs.getById(timeConf.get('id'));
			
                if (temp) {
                    var index = curOpersTimeConfs.indexOf(temp);
                    curOpersTimeConfs.removeAt(index);
                    curOpersTimeConfs.insert(index, timeConf);
                }
                else {
                    curOpersTimeConfs.insert(0, timeConf);
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addTimeConfToListStore of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
            }
        }
});