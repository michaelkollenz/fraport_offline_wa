Ext.define('AssetManagement.base.manager.BaseTimeConfManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.TimeConf',
        'AssetManagement.customer.manager.ActivityTypeManager',
        'AssetManagement.customer.manager.CustBemotManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.model.bo.Cust_043',
        'AssetManagement.customer.manager.Cust_043Manager',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		EVENTS: {
			TIMECONF_ADDED: 'timeConfAdded',
			TIMECONF_CHANGED: 'timeConfChanged',
			TIMECONF_DELETED: 'timeConfDeleted'
		},
	
	    //public methods
		getAllTimeConfs: function (withDependendData, useBatchProcessing) {
		    var retval = -1;

		    try {
		        //CAUTION: Currently using no cache - there may be conflicts when using this list for a time confirmation overview (!)
                //redesign may be necessary for this case

		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        retval = eventController.getNextEventId();

		        var allTimeConfs = Ext.create('Ext.data.Store', {
		            model: 'AssetManagement.customer.model.bo.TimeConf',
		            autoLoad: false
		        });

		        var me = this;
		        var successCallback = function (eventArgs) {
		            try {
		                var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

		                me.buildTimeConfStoreFromDataBaseQuery.call(me, allTimeConfs, eventArgs);

		                if (done) {
		                    if (!withDependendData) {
		                        eventController.requestEventFiring(retval, allTimeConfs);
		                    } else {
		                        me.loadDependendDataForTimeConfs(allTimeConfs, retval);
		                    }
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllTimeConfs of BaseTimeConfManager', ex);
		                eventController.requestEventFiring(retval, undefined);
		            }
		        };

		        var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_TIMECONF', null);
		        AssetManagement.customer.core.Core.getDataBaseHelper().query('D_TIMECONF', keyRange, successCallback, null, useBatchProcessing);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllTimeConfs of BaseTimeConfManager', ex);
		        retval = -1;
		    }

		    return retval;
		},

		getTimeConfsForOrder: function(aufnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.TimeConf',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildTimeConfStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							me.loadDependendDataForTimeConfs.call(me, theStore, retval);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeConfsForOrder of BaseTimeConfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', aufnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_TIMECONF', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_TIMECONF', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeConfsForOrder of BaseTimeConfManager', ex);
			}
	
			return retval;
		},
		
		//save a timeconf - if it is neccessary it will get a new counter first
		saveTimeConf: function(timeConf) {
			var retval = -1;
		
			try {
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!timeConf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = timeConf.get('updFlag');
				
				//update flag management
				this.manageUpdateFlagForSaving(timeConf);

				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConf.get('rmzhl'));
				
				var saveLongtextFunction = function(eventType, timeConf) {
					try {
						var saveLongtextCallback = function(success){
							try {
								if(success) {
									eventController.fireEvent(eventType, timeConf);
								} else {
									eventController.fireEvent(retval, false);
								}
							} catch(ex) {
								eventController.fireEvent(retval, false);
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveTimeConf of BaseTimeConfManager', ex);
							}
						};

						var saveLocalLongtextRequestId = AssetManagement.customer.manager.LongtextManager.saveLocalLongtext(timeConf.get('localLongtext'), timeConf, true);
						if(saveLocalLongtextRequestId > -1) {
							eventController.registerOnEventForOneTime(saveLocalLongtextRequestId, saveLongtextCallback);
						} else {
							eventController.fireEvent(retval, false);
						}
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveTimeConf of BaseTimeConfManager', ex);
					}
				};

				var saveTimeConfCallback = function(nextCounterValue) {
					try {

						if(requiresNewCounterValue && nextCounterValue !== -1)
							timeConf.set('rmzhl', nextCounterValue);
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConf.get('rmzhl'))) {
							eventController.fireEvent(retval, false);
							return;
						}

						var key = AssetManagement.customer.manager.KeyManager.createKey([ timeConf.get('aufnr'), timeConf.get('vornr'), timeConf.get('rmzhl')]);

						timeConf.set('childKey2', key);

						//set the id
						timeConf.set('id', timeConf.get('aufnr') + timeConf.get('vornr') + timeConf.get('split') + timeConf.get('rmzhl'));

						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								
								if(success) {
									var eventType = requiresNewCounterValue ? me.EVENTS.TIMECONF_ADDED : me.EVENTS.TIMECONF_CHANGED;
									//eventController.fireEvent(eventType, timeConf);
									saveLongtextFunction(eventType, timeConf);
								}
								
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveTimeConf of BaseTimeConfManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForTimeConf(timeConf);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_TIMECONF', toSave, callback, callback);
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveTimeConf of BaseTimeConfManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveTimeConfCallback();
				} else {
					eventId = me.getNextRmzhl(timeConf);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveTimeConfCallback);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveTimeConf of BaseTimeConfManager', ex);
			}
			
			return retval;
		},
		
		deleteTimeConf: function(timeConf, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!timeConf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this; 

				var deleteLocalLongtext = function(eventType, timeConf) {
					try {
						var callbackAfterDelete = function(success) {
							try {
								if(success) {
									eventController.fireEvent(eventType, timeConf);
								} else {
									eventController.fireEvent(retval, false);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTimeConf of BaseTimeConfManager', ex);
							}
						};

						var deleteLocalLongtextRequestId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(timeConf);
						if(deleteLocalLongtextRequestId > -1) {
							eventController.registerOnEventForOneTime(deleteLocalLongtextRequestId, callbackAfterDelete);
						} else {
							eventController.fireEvent(retval, false);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTimeConf of BaseTimeConfManager', ex);
					}
				}
				
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(timeConf);
							//eventController.fireEvent(me.EVENTS.TIMECONF_DELETED, timeConf);
							deleteLocalLongtext(me.EVENTS.TIMECONF_DELETED, timeConf);
						}
						
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTimeConf of BaseTimeConfManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the timeConf to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_TIMECONF', timeConf);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_TIMECONF', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTimeConf of BaseTimeConfManager', ex);
			}
			
			return retval;
		},
	
		//private methods
		loadDependendDataForTimeConfs: function(timeConfs, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(timeConfs.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, timeConfs);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var activityTypes = null;
				var accountIndications = null;
				var custActGroups = null;
	
				var pendingRequests = 0;			
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccured === false) {
						me.assignDependendDataForTimeConfs.call(me, eventIdToFireWhenComplete, timeConfs, activityTypes, accountIndications, custActGroups);
					}
				};
				
				//get activityTypes
				pendingRequests++;
				
				var activityTypeSuccessCallback = function(activityTypeStore) {
					errorOccured = activityTypeStore === undefined;
					
					activityTypes = activityTypeStore;
					pendingRequests--;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.ActivityTypeManager.getActivityTypes(true);
				eventController.registerOnEventForOneTime(eventId, activityTypeSuccessCallback);
				
				//get account indications
				pendingRequests++;
				
				var accountIndicationsSuccessCallback = function(accountIndicationsStore) {
					errorOccured = accountIndicationsStore === undefined;
					
					accountIndications = accountIndicationsStore;
					pendingRequests--;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
				eventController.registerOnEventForOneTime(eventId, accountIndicationsSuccessCallback);

			    //get cust. act. groups
				pendingRequests++;

				var custActGroupsSuccessCallback = function (custActGroupsStore) {
				    errorOccured = custActGroupsStore === undefined;

				    custActGroups = custActGroupsStore;
				    pendingRequests--;

				    completeFunction();
				};

				eventId = AssetManagement.customer.manager.Cust_043Manager.getCust_043(true);
				eventController.registerOnEventForOneTime(eventId, custActGroupsSuccessCallback);
				
				if(pendingRequests > 0)
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				else
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, timeConfs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForTimeConfs of BaseTimeConfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForTimeConfs: function(eventIdToFireWhenComplete, timeConfs, activityTypes, accountIndications, custActGroups) {
			try {
				timeConfs.each(function (timeConf, index, length) {
					//assign activityType
					if(activityTypes && activityTypes.getCount() > 0) {
						var learr = timeConf.get('learr');
					
						activityTypes.each(function(activityType) {
							if(activityType.get('lstar') === learr) {
								timeConf.set('activityType', activityType);
								return false;
							}
						});
					}
					
					//assign accountIndication
					if(accountIndications && accountIndications.getCount() > 0) {
						var bemot = timeConf.get('bemot');
					
						accountIndications.each(function(accountIndication) {
							if(accountIndication.get('bemot') === bemot) {
								timeConf.set('accountIndication', accountIndication);
								return false;
							}
						});
					}
				    //assign cust. act. groups
					if (custActGroups && custActGroups.getCount() > 0) {
					    var actype = timeConf.get('actype');

					    custActGroups.each(function (cust043) {
					        if (cust043.get('actype') === actype) {
					            timeConf.set('custActGroup', cust043);
					            return false;
					        }
					    });
					}
				});
				
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, timeConfs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForTimeConfs of BaseTimeConfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		buildTimeConfStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var timeConf = this.buildTimeConfFromDbResultObject(cursor.value);
						store.add(timeConf);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTimeConfStoreFromDataBaseQuery of BaseTimeConfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//builds a single time conf. for a database record
		buildTimeConfFromDbResultObject: function(dbResult) {
			var retval = null;
			
			try {	
			    retval = Ext.create('AssetManagement.customer.model.bo.TimeConf', {
				        aufnr: dbResult['AUFNR'],
				        vornr: dbResult['VORNR'],
				        split: dbResult['SPLIT'],
				        rmzhl: dbResult['RMZHL'],
				        ltxa1: dbResult['LTXA1'],
				        learr: dbResult['LEARR'],
				        txtsp: dbResult['TXTSP'],
				        ismnw: dbResult['ISMNW'],
				        ismne: dbResult['ISMNE'],
				        aueru: dbResult['AUERU'],
				        pernr: dbResult['PERNR'],
				        bemot: dbResult['BEMOT'],
				        grund: dbResult['GRUND'],
				        ernam: dbResult['ERNAM'],
				        arbpl: dbResult['ARBPL'],
				        werks: dbResult['WERKS'],
				        idaur: dbResult['IDAUR'],
				        idaue: dbResult['IDAUE'],
				        ausor: dbResult['AUSOR'],
				        ofmnw: dbResult['OFMNW'],
				        ofmne: dbResult['OFMNE'],
				        stagr: dbResult['STAGR'],

				        ersda: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERSDA']),
				        budat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BUDAT']),
				        isd: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ISDD'], dbResult['ISDZ']),
				        ied: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['IEDD'], dbResult['IEDZ']),
				                     
				        updFlag: dbResult['UPDFLAG'],
				        mobileKey: dbResult['MOBILEKEY'],
				        childKey: dbResult['CHILDKEY'],
				        childKey2: dbResult['CHILDKEY2'],
				        actype: dbResult['ACTYPE'],
				        usr1: dbResult['USR1'],
				        usr2: dbResult['USR2'],
				        pausesz: dbResult['PAUSESZ'],
				        pauseez: dbResult['PAUSEEZ'],
				        kstar: dbResult['KSTAR'],
				        betrag: dbResult['BETRAG'],
				        tcurr: dbResult['TCURR']
			    });
			
			    retval.set('id', retval.get('aufnr') + retval.get('vornr') + retval.get('split') + retval.get('rmzhl'));
	
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTimeConfFromDbResultObject of BaseTimeConfManager', ex);
			}
			
			return retval;
		},
		
		getWork: function(timeconf) {
			var retval = 0.0;
		
			try {
				retval = this.parseWork(timeconf.get('ismnw')); 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWork of BaseTimeConfManager', ex);
			}
			
			return retval;
		}, 
		
		parseWork: function(work) {
			var retval = 0.0;
		
			try {
				var parsedValue
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(work)) {
					var parsedValue = AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(work);
					
					if(parsedValue === Number.NaN) {
						parsedValue = 0.0;
					}
					
					retval = parsedValue;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseWork of BaseTimeConfManager', ex);
			}
			
			return retval;
		},
		
		updateTimeConfSumOperation: function (operation) {
		    var retval = 0;

		    try {
		        var timeConfs = operation.get('timeConfs');
		        var total = 0;

		        if (timeConfs && timeConfs.getCount() > 0) {
		            timeConfs.each(function (timeconf) {
		                var unit = timeconf.get('ismne').toUpperCase();

		                if (unit === 'H' || unit === 'STD') {
		                    total += this.getWork(timeconf);
		                } else if (unit === 'M' || unit === 'MIN') {
		                    total += this.getWork(timeconf) / 60.0;
		                }
		            }, this);
		        }

		        operation.set('tConfSum', total);
		        retval = total;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateTimeConfSumOperation of BaseTimeConfManager', ex);
		    }

		    return retval;
		},
		
		//builds the raw object to store in the database out of a timeConf model
		buildDataBaseObjectForTimeConf: function(timeConf) {
			var retval = null;
			var ac = null;
		
			try {
			    ac = AssetManagement.customer.core.Core.getAppConfig();
		
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['AUFNR'] = timeConf.get('aufnr');
				retval['VORNR'] = timeConf.get('vornr');
				retval['SPLIT'] = timeConf.get('split');
				retval['RMZHL'] = timeConf.get('rmzhl');
				retval['ERNAM'] = timeConf.get('ernam');
				retval['ARBPL'] = timeConf.get('arbpl');
				retval['WERKS'] = timeConf.get('werks');
				retval['LTXA1'] = timeConf.get('ltxa1');
				retval['TXTSP'] = timeConf.get('txtsp');
				retval['ISMNW'] = timeConf.get('ismnw');
				retval['ISMNE'] = timeConf.get('ismne');
				retval['LEARR'] = timeConf.get('learr');
				retval['IDAUR'] = timeConf.get('idaur');
				retval['IDAUE'] = timeConf.get('idaue');
				retval['PERNR'] = timeConf.get('pernr');
				retval['AUERU'] = timeConf.get('aueru');
				retval['AUSOR'] = timeConf.get('ausor');
				retval['OFMNW'] = timeConf.get('ofmnw');
				retval['OFMNE'] = timeConf.get('ofmne');
				retval['BEMOT'] = timeConf.get('bemot');
				retval['GRUND'] = timeConf.get('grund');
				retval['STAGR'] = timeConf.get('stagr');
				retval['STATUS'] = '0';
				
				retval['ISDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(timeConf.get('isd'));
				retval['ISDZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(timeConf.get('isd'));
				retval['IEDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(timeConf.get('ied'));
				retval['IEDZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(timeConf.get('ied'));
				retval['BUDAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(timeConf.get('budat'));
				retval['ERSDA'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(timeConf.get('ersda'));
				
				retval['MOBILEKEY'] = timeConf.get('mobileKey');
				retval['CHILDKEY'] = timeConf.get('childKey');
				retval['CHILDKEY2'] = timeConf.get('childKey2');
				retval['UPDFLAG'] = timeConf.get('updFlag');
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForTimeConf of BaseTimeConfManager', ex);
			}
			
			return retval;
		},
		
		getNextRmzhl: function(timeConf) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!timeConf) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxRmzhl = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
                            var rmzhlValue = cursor.value['RMZHL'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(rmzhlValue, '%')) {
                                //cut of the percent sign
							    rmzhlValue = rmzhlValue.substr(1);
                                var curRmzhlValue = parseInt(rmzhlValue);
							
							    if(!isNaN(curRmzhlValue))
							    {
								    if(curRmzhlValue > maxRmzhl)
									    maxRmzhl = curRmzhlValue;
							    }
						    }

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxRmzhl + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 7);
                            eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextRmzhl of BaseTimeConfManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', timeConf.get('aufnr'));
				keyMap.add('VORNR', timeConf.get('vornr'));
				keyMap.add('SPLIT', timeConf.get('split'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_TIMECONF', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_TIMECONF', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextRmzhl of BaseTimeConfManager', ex);
			}
			
			return retval;			
		}
	}
});