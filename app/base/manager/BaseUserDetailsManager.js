Ext.define('AssetManagement.base.manager.BaseUserDetailsManager', {
	requires: [
	    'AssetManagement.customer.manager.KeyManager',
	    'AssetManagement.customer.model.bo.UserDetails',
	    'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {

		getUsersDetails: function(useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				var baseStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.UserDetails',
					autoLoad: false
				});

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

						me.buildUserDetailsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

						if(done) {
							eventController.requestEventFiring(retval, baseStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrders of BaseOrderManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_USERMASTER', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_USERMASTER', keyRange, successCallback, null, useBatchProcessing);
				
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUsersDetails of BaseUserDetailsManager', ex);
			}

			return retval;
		},

		buildUserDetailsStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;

				    if (cursor) {
				    	var userDetailsInstance = this.buildUserDetailsFromDbResultObject(cursor.value);
						store.add(userDetailsInstance);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserDetailsStoreFromDataBaseQuery of BaseUserDetailsManager', ex);
			}
		},

		buildUserDetailsFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.UserDetails', {
					updFlag: dbResult['UPDFLAG'],
					uname: dbResult['UNAME'],
					email: dbResult['E_MAIL'],
					usersTitle: dbResult['TITLE_P'],
					firstname: dbResult['FIRSTNAME'],
					lastname: dbResult['LASTNAME'],
					fullName: dbResult['FULLNAME'],
					nickname: dbResult['NICKNAME'],
					telNumber: dbResult['TEL1_NUMBR'],
					telExtension: dbResult['TEL1_EXT']

			    });

			    retval.set('id', dbResult['UNAME']);

            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOrderFromDbResultObject of BaseOrderManager', ex);
            }

			return retval;
		}
	}
});