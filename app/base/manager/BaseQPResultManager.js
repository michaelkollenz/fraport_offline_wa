﻿Ext.define('AssetManagement.base.manager.BaseQPResultManager', {

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'Ext.util.HashMap',
        'AssetManagement.customer.manager.cache.QPResultCache',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.MobileIndexHelper',
        'AssetManagement.customer.model.bo.QPResult'
    ],

    inheritableStatics: {

        EVENTS: {
            QPRESULT_ADDED: 'qresultAdded',
            QPRESULTS_DELETED: 'qresultDeleted'
        },

        //public
        getQPResultsForQplos: function (insplot, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(insplot)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var qpResults = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPResult',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildQPResultStoreFromDataBaseQuery.call(me, qpResults, eventArgs);

                        if (done) {
                            if (qpResults && qpResults.getCount() > 0)
                                eventController.fireEvent(retval, qpResults);
                            else
                                eventController.fireEvent(retval, null);

                        }

                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPResultsForQplos of BaseQPResultManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('INSPLOT', insplot);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_QPRESULT', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_QPRESULT', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPResultsForQplos of BaseQPResultManager', ex);
            }
            return retval;
        },

        //private methods
        buildQPResultFromDataBaseQuery: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildQPResultFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPResultFromDataBaseQuery of BaseQPResultManager', ex);
            }

            return retval;
        },

        buildQPResultStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var qpResult = this.buildQPResultFromDbResultObject(cursor.value);
                        store.add(qpResult);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPResultStoreFromDataBaseQuery of BaseQPResultManager', ex);
            }
        },


        buildQPResultFromDbResultObject: function (dbResult) {
            try {

                var retval = Ext.create('AssetManagement.customer.model.bo.QPResult', {
                    insplot: dbResult['INSPLOT'],
                    inspoper: dbResult['INSPOPER'],
                    inspchar: dbResult['INSPCHAR'],
                    equnr: dbResult['EQUNR'],
                    tplnr: dbResult['TPLNR'],
                    closed: dbResult['CLOSED'],
                    codegrp1: dbResult['CODE_GRP1'],
                    codegrp2: dbResult['CODE_GRP2'],
                    code1: dbResult['CODE1'],
                    code2: dbResult['CODE2'],
                    inspector: dbResult['INSPECTOR'],
                    meanValue: AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(dbResult['MEAN_VALUE'], true),
                    qmnum: dbResult['QMNUM'],
                    remark: dbResult['REMARK'],
                    evaluation: dbResult['EVALUATION'],
                    endDate: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['END_DATE'], dbResult['END_TIME']),
                    endTime: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['END_DATE'], dbResult['END_TIME']),
                    startDate: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['START_DATE'], dbResult['START_TIME']),
                    startTime: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['START_DATE'], dbResult['START_TIME']),
                    matnr:  dbResult['MATNR'],
                    werks:  dbResult['WERKS'],
                    plnty:  dbResult['PLNTY'],
                    plnnr:  dbResult['PLNNR'],
                    plnal:  dbResult['PLNAL'],
                    zkriz:  dbResult['ZKRIZ'],
                    zaehl:  dbResult['ZAEHL'],
                    datuv:  dbResult['DATUV'],
                    char_attr:  dbResult['CHAR_ATTR'],
                    usr01flag:  dbResult['USR01FLAG'],
                    usr02flag:  dbResult['USR02FLAG'],
                    usr03code:  dbResult['USR03CODE'],
                    usr04code:  dbResult['USR04CODE'],
                    usr05txt:  dbResult['USR05TXT'],
                    usr06txt:  dbResult['USR06TXT'],
                    usr07txt:  dbResult['USR07TXT'],
                    usr08date:  dbResult['USR08DATE'],
                    usr09num:  dbResult['USR09NUM'],
                    updFlag: dbResult['UPDFLAG'],
                    mobileKey: dbResult['MOBILEKEY'],
                    childKey: dbResult['CHILDKEY'],
                    childKey_char: dbResult['CHILDKEY_CHAR']
                });

                retval.set('id', retval.get('insplot') + retval.get('inspoper') + retval.get('inspchar') + retval.get('equnr') + retval.get('tplnr'));

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPResultFromDbResultObject of BaseQPResultManager', ex);
            }

            return retval;
        },
 //       /*
 //       * creates a new qpresult object and saves it to the database
 //       */
        saveQPResult: function (qpresult) { //changed for store TODO
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qpresult) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }
                //var insplot = qpresults.first().get('insplot')
                var me = this;

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qpresult.get('childKey')))
                    qpresult.set('childKey', AssetManagement.customer.manager.KeyManager.createKey([qpresult.get('insplot'), qpresult.get('inspoper'), qpresult.get('inspchar'), qpresult.get('insplot'), qpresult.get('equnr'), qpresult.get('tplnr')]));
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        eventController.requestEventFiring(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPResult of BaseQPResultManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var toSave = me.buildDataBaseObjectFromQPResult(qpresult);
                AssetManagement.customer.core.Core.getDataBaseHelper().put('D_QPRESULT', toSave, callback, callback);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPResult of BaseQPResultManager', ex);
            }

            return retval;
        },

        saveQPResults: function (qpresults) {
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qpresults || qpresults.getCount() === 0) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }

                var pendingSaves = 0;
                var errorReported = false;

                var successCallback = function (success) {
                    try {
                        errorOccurred = success === false;

                        if (errorOccured && !errorReported) {
                            eventController.requestEventFiring(retval, false);
                            errorReported = true;
                        } else if (!errorOccured) {

                            pendingSaves--;

                            if (pendingSaves === 0) {
                                eventController.requestEventFiring(retval, true);
                            }
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosCharStore of BaseQPlosCharManager', ex);
                    }
                };

                var me = this;
                //begin saving the first item
                qpresults.each(function (qpresult) {
                    var eventId = me.saveQPResult(qpresult);

                    if (eventId > -1) {
                        pendingSaves++;
                        eventController.registerOnEventForOneTime(eventId, successCallback);
                    } else {
                        errorOccurred === true;
                        return false;
                    }
                });
                if (pendingSaves > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    eventController.requestEventFiring(retval, false);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPResults of BaseQPResultManager', ex);
            }
            return retval;
        },

        deleteQPResults: function (insplot, useBatchProcessing) {                            
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(insplot)) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";
                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPResults of BaseQPResultManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('INSPLOT', insplot);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_QPRESULT', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_QPRESULT', keyRange, callback, callback, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPResults of BaseQPResultManager', ex);
            }

            return retval;
        },

        /*
        * creates the databaseobject for the qpResult object
        */
        buildDataBaseObjectFromQPResult: function (qpresult) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = {};

                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['UPDFLAG'] = qpresult.get('updFlag');
                retval['QMNUM'] = qpresult.get('qmnum');
                retval['INSPLOT'] = qpresult.get('insplot');
                retval['INSPOPER'] = qpresult.get('inspoper');
                retval['INSPCHAR'] = qpresult.get('inspchar');
                retval['INSPECTOR'] = qpresult.get('inspector');
                retval['END_DATE'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(qpresult.get('endDate'));
                retval['END_TIME'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(qpresult.get('endTime'));
                retval['START_DATE'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(qpresult.get('startDate'));
                retval['START_TIME'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(qpresult.get('startTime'));
                retval['MEAN_VALUE'] = AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(qpresult.get('meanValue'), false);
                retval['REMARK'] = qpresult.get('remark');
                retval['CODE1'] = qpresult.get('code1');
                retval['CODE_GRP1'] = qpresult.get('codegrp1');
                retval['CODE2'] = qpresult.get('code2');
                retval['CODE_GRP2'] = qpresult.get('codegrp2');
                retval['CLOSED'] = qpresult.get('closed');
                retval['EVALUATION'] = qpresult.get('evaluation');
                retval['EQUNR'] = qpresult.get('equnr');
                retval['TPLNR'] = qpresult.get('tplnr');
                retval["MATNR"] = qpresult.get('matnr');
                retval["WERKS"] = qpresult.get('werks');
                retval["PLNTY"] = qpresult.get('plnty');
                retval["PLNNR"] = qpresult.get('plnnr');
                retval["PLNAL"] = qpresult.get('plnal');
                retval["ZKRIZ"] = qpresult.get('zkriz');
                retval["ZAEHL"] = qpresult.get('zaehl');
                retval["DATUV"] = qpresult.get('datuv');
                retval["CHAR_ATTR"] = qpresult.get('char_attr');
                retval["USR01FLAG"] = qpresult.get('usr01flag');
                retval["USR02FLAG"] = qpresult.get('usr02flag');
                retval["USR03CODE"] = qpresult.get('usr03code');
                retval["USR04CODE"] = qpresult.get('usr04code');
                retval["USR05TXT"] = qpresult.get('usr05txt');
                retval["USR06TXT"] = qpresult.get('usr06txt');
                retval["USR07TXT"] = qpresult.get('usr07txt');
                retval["USR08DATE"] = qpresult.get('usr08date');
                retval["USR09NUM"]  = qpresult.get('usr09num');
                retval['MOBILEKEY'] = qpresult.get('mobileKey');
                retval['CHILDKEY'] = qpresult.get('childKey');
                retval["CHILDKEY_CHAR"] = qpresult.get('childKey_char');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForQPResult of BaseQPResultManager', ex);
            }

            return retval;
        }
    }
});