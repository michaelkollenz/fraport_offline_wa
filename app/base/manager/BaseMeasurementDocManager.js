Ext.define('AssetManagement.base.manager.BaseMeasurementDocManager', {
     extend: 'AssetManagement.customer.manager.OxBaseManager',
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.MeasurementDoc',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

    inheritableStatics: {
        EVENTS: {
            MEASDOC_ADDED: 'measDocAdded',
            MEASDOC_CHANGED: 'measDocChanged',
            MEASDOC_DELETED: 'measDocDeleted'
        },

        //public
        getMeasDocs: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();


                if (this._cacheComplete === true) {
                    var completeStore = this.getAllFromCache();
                    eventController.requestEventFiring(retval, completeStore);

                    return retval;
                }

                var toFill = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementDoc',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildMeasDocStoreFromDataBaseQuery.call(me, toFill, eventArgs);

                        if (done) {
                            me.addAllToCache(toFill, true);
                            eventController.fireEvent(retval, toFill);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasDocs of BaseMeasurementDocManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MEASDOC', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MEASDOC', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasDocs of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        getMeasDocsForPoint: function (point, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(point)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var fromCache = this.getFromCacheForPoint(point);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);

                    return retval;
                }

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementDoc',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildMeasDocStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            me.addToCache(point, theStore);

                            eventController.fireEvent(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasDocsForPoint of BaseMeasurementDocManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('POINT', point);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MEASDOC', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MEASDOC', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasDocsForPoint of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        getMeasDoc: function (point, mdocm, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(point)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mdocm)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var fromCache = this.getFromCache(point, mdocm);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);

                    return retval;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var measDoc = me.buildMeasDocDataBaseQuery.call(me, eventArgs);
                        eventController.requestEventFiring(retval, measDoc);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasDoc of BaseMeasurementDocManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('POINT', point);
                keyMap.add('MDOCM', mdocm);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_MEASDOC', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MEASDOC', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasDoc of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        //private
        buildMeasDocDataBaseQuery: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildMeasDocFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMeasDocDataBaseQuery of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        buildMeasDocStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var measDoc = this.buildMeasDocFromDbResultObject(cursor.value);
                        store.add(measDoc);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMeasDocStoreFromDataBaseQuery of BaseMeasurementDocManager', ex);
            }
        },

        buildMeasDocFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.MeasurementDoc', {
                    point: dbResult['POINT'],
                    mdocm: dbResult['MDOCM'],
                    mdtext: dbResult['MDTXT'],
                    readr: dbResult['READR'],
                    recdc: dbResult['RECDC'],
                    unitr: dbResult['UNITR'],
                    codct: dbResult['CODCT'],
                    codgr: dbResult['CODGR'],
                    vlcod: dbResult['VLCOD'],
                    idate: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['IDATE']),
                    itime: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ITIME']),
                    mobileKey: dbResult['MOBILEKEY'],
                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['POINT'] + dbResult['MDOCM']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMeasDocFromDbResultObject of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        //cache administration
        EVENTS: {
            MEASDOC_ADDED: 'measDocAdded',
            MEASDOC_CHANGED: 'measDocChanged',
            MEASDOC_DELETED: 'measDocDeleted'
        },

        _measDocsCache: null,
        _cacheComplete: false,

        getCache: function () {
            try {
                if (!this._measDocsCache) {
                    this._measDocsCache = Ext.create('Ext.util.HashMap');

                    this.initializeCacheHandling();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseMeasurementDocManager', ex);
            }

            return this._measDocsCache;
        },

        clearCache: function () {
            try {
                this.getCache().clear();
                this._cacheComplete = false;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseMeasurementDocManager', ex);
            }
        },

        initializeCacheHandling: function () {
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
                eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseMeasurementDocManager', ex);
            }
        },

        addToCache: function (point, measDocs) {
            try {
                this.getCache().add(point, measDocs);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseMeasurementDocManager', ex);
            }
        },

        addAllToCache: function (measDocs, completeDataFromDatabase) {
            try {
                if (completeDataFromDatabase === true)
                    this.clearCache();

                measDocs.each(function (measDoc) {
                    var myTargetStore = this.getFromCacheForPoint(measDoc.get('point'));

                    if (!myTargetStore) {
                        myTargetStore = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.MeasurementDoc',
                            autoLoad: false
                        });

                        this.addToCache(measDoc.get('point'), myTargetStore);
                    }

                    myTargetStore.add(measDoc);
                }, this);

                if (completeDataFromDatabase === true)
                    this._cacheComplete = true;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseMeasurementDocManager', ex);
            }
        },

        getFromCache: function (point, mdocm) {
            var retval = null;

            try {
                var id = point + mdocm;

                var currentCached = this.getFromCacheForPoint(point);

                if (currentCached) {
                    currentCached.each(function (measDoc) {
                        if (measDoc.get('id') === id) {
                            retval = measDoc;
                            return false;
                        }
                    }, this);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        getAllFromCache: function () {
            var retval = null;

            try {
                retval = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementDoc',
                    autoLoad: false
                });

                this.getCache().each(function (key, value) {
                    value.each(function (measDoc) {
                        retval.add(measDoc);
                    }, this);
                }, this);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllFromCache of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        getFromCacheForPoint: function (point) {
            var retval = null;

            try {
                retval = this.getCache().get(point);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForPoint of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        //save a measdoc - if it is neccessary it will get a new counter first
        saveMeasDoc: function (measDoc) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!measDoc) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                //update flag management
                this.manageUpdateFlagForSaving(measDoc);

                //check if the measDoc already has a counter value
                var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measDoc.get('mdocm'));

                var me = this;
                var saveFunction = function (nextCounterValue) {
                    try {
                        if (requiresNewCounterValue && nextCounterValue !== -1)
                            measDoc.set('mdocm', nextCounterValue);

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measDoc.get('mdocm'))) {
                            eventController.fireEvent(retval, false);
                            return;
                        }

                        //set the id
                        measDoc.set('id', measDoc.get('point') + measDoc.get('mdocm'));

                        //set mobileKey - only get a new mobilekey if the mobilekey was not set before
                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measDoc.get('mobileKey')))
                            measDoc.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([measDoc.get('id')]));

                        var callback = function (eventArgs) {
                            try {
                                var success = eventArgs.type === "success";

                                if (success) {
                                    var eventType = me.EVENTS.MEASDOC_ADDED;
                                    eventController.fireEvent(eventType, measDoc);
                                }

                                eventController.fireEvent(retval, success);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveMeasDoc of BaseMeasurementDocManager', ex);
                                eventController.fireEvent(retval, false);
                            }
                        };

                        var toSave = me.buildDataBaseObjectForMeasDoc(measDoc);
                        AssetManagement.customer.core.Core.getDataBaseHelper().put('D_MEASDOC', toSave, callback, callback);
                    } catch (ex) {
                        eventController.fireEvent(retval, false);
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveMeasDoc of BaseMeasurementDocManager', ex);
                    }
                };

                if (!requiresNewCounterValue) {
                    saveFunction();
                } else {
                    eventId = this.getNextMdocm(measDoc);

                    if (eventId > 1)
                        eventController.registerOnEventForOneTime(eventId, saveFunction);
                    else
                        eventController.fireEvent(retval, false);
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveMeasDoc of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        deleteMeasDoc: function (measDoc, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!measDoc) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }
                var me = this;
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(measDoc);
                            eventController.fireEvent(me.EVENTS.MEASDOC_DELETED, measDoc);
                        }

                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMeasDoc of BaseMeasurementDocManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                //get the key of the measDoc to delete
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_MEASDOC', measDoc);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_MEASDOC', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMeasDoc of BaseMeasurementDocManager', ex);
            }

            return retval;
        },

        //builds the raw object to store in the database out of a measDoc model
        buildDataBaseObjectForMeasDoc: function (measDoc) {
            var retval = null;
            var ac = null;

            try {
                ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['POINT'] = measDoc.get('point');
                retval['MDOCM'] = measDoc.get('mdocm');
                retval['MDTXT'] = measDoc.get('mdtext');
                retval['READR'] = measDoc.get('readr');
                retval['RECDC'] = measDoc.get('recdc');
                retval['UNITR'] = measDoc.get('unitr');
                retval['CODCT'] = measDoc.get('codct');
                retval['CODGR'] = measDoc.get('codgr');
                retval['VLCOD'] = measDoc.get('vlcod');
                retval['IDATE'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(measDoc.get('idate'));
                retval['ITIME'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(measDoc.get('itime'));

                retval['MOBILEKEY'] = measDoc.get('mobileKey');

                retval['UPDFLAG'] = measDoc.get('updFlag');

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForMeasDoc of BaseMeasurementDocManager', ex);
            }

            return retval;
        },


        getNextMdocm: function (measDoc) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!measDoc) {
                    eventController.requestEventFiring(retval, -1);
                    return retval;
                }

                var maxMdocm = 0;

                var successCallback = function (eventArgs) {
                    try {
                        if (eventArgs && eventArgs.target && eventArgs.target.result) {
                            var cursor = eventArgs.target.result;

                            
                            var mdocmValue = cursor.value['MDOCM'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(mdocmValue, '%')) {
                                //cut of the percent sign
							    mdocmValue = mdocmValue.substr(1);
                                var curMdocmValue = parseInt(mdocmValue);
							
							    if (!isNaN(curMdocmValue)) {
                                    if (curMdocmValue > maxMdocm)
                                        maxMdocm = curMdocmValue;
                                }
						    }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            var nextLocalCounterNumberValue = maxMdocm + 1;
                            var nextLocalCounterValue = '%' + nextLocalCounterNumberValue;
						    
                            eventController.requestEventFiring(retval, nextLocalCounterValue);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMdocm of BaseMeasurementDocManager', ex);
                        eventController.requestEventFiring(retval, -1);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('POINT', measDoc.get('point'));

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MEASDOC', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MEASDOC', keyRange, successCallback, null);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMdocm of BaseMeasurementDocManager', ex);
            }

            return retval;
        }
    }
});