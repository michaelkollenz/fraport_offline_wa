﻿/*
 * Manager Class for handling om COMPL-objects.
 * Creates and saves Compl-Objects in the database 
 */
Ext.define('AssetManagement.base.manager.BaseCompletionManager', {
        requires: [
            'AssetManagement.customer.controller.EventController'
        ],
    
        inheritableStatics: {
            /*
             * creates a new compl objects and saves it to the database
             * requires the objectsnumber of the buisnessobject to be completed and the mobilekey of that object
             */
            completeObject: function (objnr, mobilekey, callback) {
                var retval = -1;
                try {
                    var compl = Ext.create('AssetManagement.customer.model.bo.Compl', { objnr: objnr, mobilekey: mobilekey });
                    var eventController = AssetManagement.customer.controller.EventController.getInstance();
                    retval = eventController.getNextEventId();

                    var me = this;
                    var saveFunction = function() {
                        try {	
						
                            var toSave = me.buildDataBaseObjectForCompl(compl);
                            AssetManagement.customer.core.Core.getDataBaseHelper().put('D_COMPL', toSave, callback, callback);
                        }
                        catch (ex) {
                            eventController.fireEvent(retval, false);
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside completeObject of BaseCompletionManager', ex);
                        }
                    };
				
                    saveFunction();
                }
                catch(ex){
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside completeObject of BaseCompletionManager', ex);
                }
                return retval;
            },

            /*
             * creates the databaseobject for the Compl object
             */
            buildDataBaseObjectForCompl: function (dbResult) {                    
			    var retval = null;
			    var ac = null;
		
			    try {
			        ac = AssetManagement.customer.core.Core.getAppConfig();
		
			        retval = { };
			        retval['MANDT'] =  ac.getMandt();
			        retval['USERID'] =  ac.getUserId();

			        retval['OBJNR'] =  dbResult.get('objnr');
			        retval['MOBILEKEY'] =  dbResult.get('mobilekey');
                
			        retval['UPDFLAG'] =  dbResult.get('updFlag');
                }
                catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForCompl of BaseCompletionManager', ex);
                }
                return retval;
            }
        }
}
);