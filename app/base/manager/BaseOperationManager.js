Ext.define('AssetManagement.base.manager.BaseOperationManager', {
    extend: 'AssetManagement.customer.manager.OxBaseManager',
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.OperationCache',
        'AssetManagement.customer.model.bo.Operation',
        'AssetManagement.customer.model.bo.TimeConf',
        'AssetManagement.customer.model.bo.MaterialConf',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.manager.TimeConfManager',
        'AssetManagement.customer.manager.MatConfManager',
        'AssetManagement.customer.helper.MobileIndexHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

    inheritableStatics: {
        EVENTS: {
            OPERATION_ADDED: 'operationAdded',
            OPERATION_CHANGED: 'operationChanged',
            OPERATION_DELETED: 'operationDeleted'
        },

        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.OperationCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseOperationManager', ex);
            }

            return retval;
        },

        //public methods
        getOperations: function (withDependendData, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var cache = this.getCache();
                var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
                var fromCache = cache.getStoreForAll(requestedDataGrade);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var baseStore = null;

                //if with dependend data is requested, check, if the cache may deliver the base store
                //if so, pull all instances from cache, with a too low data grade
                if (withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
                    baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
                    this.loadListDependendDataForOperations(retval, baseStore);
                } else {
                    baseStore = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.Operation',
                        autoLoad: false
                    });

                    var me = this;
                    var successCallback = function (eventArgs) {
                        try {
                            var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                            me.buildOperationsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

                            if (done) {
                                cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);

                                if (withDependendData) {
                                    //before proceeding pull all instances from cache, with a too low data grade
                                    //else the wrong instances would be filled with data
                                    baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
                                    me.loadListDependendDataForOperations.call(me, retval, baseStore);
                                } else {
                                    //return the store from cache to eliminate duplicate objects
                                    var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
                                    eventController.requestEventFiring(retval, toReturn);
                                }
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOperations of BaseOperationManager', ex);
                            eventController.requestEventFiring(retval, undefined);
                        }
                    };

                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ORDOPER', null);
                    AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ORDOPER', keyRange, successCallback, null, useBatchProcessing);
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOperations of BaseOperationManager', ex);
            }

            return retval;
        },

        getOperationsForOrder: function (aufnr, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var cache = this.getCache();
                var fromCache = cache.getFromCacheForOrder(aufnr, cache.self.DATAGRADES.FULL);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                if (cache.getFromCacheForOrder(aufnr, cache.self.DATAGRADES.BASE)) {
                    baseStore = cache.getAllUpToGradeForOrder(aufnr, cache.self.DATAGRADES.MEDIUM);
                    this.loadDependendDataForOperations(baseStore, retval);
                } else {
                    baseStore = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.Operation',
                        autoLoad: false
                    });

                    var me = this;
                    var successCallback = function (eventArgs) {
                        try {
                            var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                            me.buildOperationsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

                            if (done) {
                                cache.addToCacheForOrder(aufnr, baseStore, cache.self.DATAGRADES.BASE);

                                //before proceeding pull all instances from cache, with a too low data grade
                                //else the wrong instances would be filled with data
                                baseStore = cache.getAllUpToGradeForOrder(aufnr, cache.self.DATAGRADES.MEDIUM);
                                me.loadDependendDataForOperations.call(me, baseStore, retval);
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOperationsForOrder of BaseOperationManager', ex);
                            eventController.requestEventFiring(retval, undefined);
                        }
                    };

                    var keyMap = Ext.create('Ext.util.HashMap');
                    keyMap.add('AUFNR', aufnr);

                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ORDOPER', keyMap);
                    AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ORDOPER', keyRange, successCallback, null, useBatchProcessing);
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOperationsForOrder of BaseOperationManager', ex);
            }

            return retval;
        },


        /**
		 * saves an operation to the database
		 */
        saveOperation: function (operation) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!operation) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var priorUpdateFlag = operation.get('updFlag');

                var isNewOperation = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('aplzl'));
                var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;

                this.manageUpdateFlagForSaving(operation);

                var me = this;

                //manage key fields: vornr, aufpl and aplzl may be set
                var afterKeyFieldsManagement = function (keyFieldsReady) {
                    try {
                        if (keyFieldsReady === true) {
                            //childKey management
                            if (isInsert && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('childKey')))
                                operation.set('childKey', AssetManagement.customer.manager.KeyManager.createKey([operation.get('aufnr'), operation.get('vornr'), operation.get('aufpl'), operation.get('aplzl')]));

                            var saveCallback = function (eventArgs) {
                                try {
                                    var success = eventArgs.type === "success";

                                    if (success) {
                                        var dependedDataSaveCallback = function (dependedDataSaveSuccess) {
                                            try {
                                                if (dependedDataSaveSuccess) {
                                                    if (isNewOperation) {
                                                        //do not specify a datagrade, so all depenend data will loaded, when necessary
                                                        me.getCache().addToCache(operation);
                                                    }

                                                    var eventType = isNewOperation ? me.EVENTS.OPERATION_ADDED : me.EVENTS.OPERATION_CHANGED;
                                                    eventController.requestEventFiring(eventType, operation);
                                                }

                                                eventController.requestEventFiring(retval, dependedDataSaveSuccess);
                                            } catch (ex) {
                                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOperation of BaseOperationManager', ex);
                                                eventController.requestEventFiring(retval, false);
                                            }
                                        };

                                        //save dependend data like object status
                                        me.saveDependendDataOfOperation(operation, dependedDataSaveCallback);
                                    } else {
                                        eventController.requestEventFiring(retval, false);
                                    }
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOperation of BaseOperationManager', ex);
                                    eventController.requestEventFiring(retval, false);
                                }
                            };

                            var toSave = me.buildDataBaseObjectForOperation(operation);

                            if (isInsert) {
                                AssetManagement.customer.core.Core.getDataBaseHelper().put('D_ORDOPER', toSave, saveCallback, saveCallback);
                            } else {
                                AssetManagement.customer.core.Core.getDataBaseHelper().update('D_ORDOPER', null, toSave, saveCallback, saveCallback);
                            }
                        } else {
                            eventController.requestEventFiring(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOperation of BaseOperationManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                this.manageKeyFieldsBeforeSave(operation, afterKeyFieldsManagement);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOperation of BaseOperationManager', ex);
            }

            return retval;
        },

        deleteOperation: function (operation, useBatchProcessing) {
            /**
            * deletes a operation with all it's dependant data from the database
            * dependant data:  longtext, timeconfs, matconf
            */
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!operation) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var deleteLevel = 0;
                var maxLevel = 4;


                var deleteMethod = function () {
                    //save the header
                    if (deleteLevel === 0) {
                        var deleteOperationHeadCallBack = function (success) {
                            deleteLevel++;
                            deleteMethod();
                        };

                        var eventId = AssetManagement.customer.manager.OperationManager.deleteOperationHead(operation, true);
                        eventController.registerOnEventForOneTime(eventId, deleteOperationHeadCallBack);
                    }  else if (deleteLevel === 1) {
                        //delete the timeConfs
                        var timeConfs = operation.get('timeConfs');
                        var timeConfsCount = timeConfs ? timeConfs.getCount() : 0;

                        if (timeConfsCount === 0) {
                            //no items for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteTimeConfsCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.TimeConfManager.deleteTimeConf(timeConfs.getAt(timeConfsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteTimeConfsCallback);
                        }
                    } else if (deleteLevel === 2) {
                        //delete the matConfs
                        var matConfs = operation.get('matConfs');
                        var matConfsCount = matConfs ? matConfs.getCount() : 0;

                        if (matConfsCount === 0) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteMatConfsCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.MatConfManager.deleteMatConf(matConfs.getAt(matConfsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteMatConfsCallback);
                        }
                    } else if (deleteLevel === 3) {
                        //delete the longtext
                        var longtext = operation.get('localLongtext');

                        if (!longtext) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteLongtextCallback = function (success) {
                                deleteLevel++;
                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(operation, true);
                            eventController.registerOnEventForOneTime(eventId, deleteLongtextCallback);
                        } 
                    } 

                    if (deleteLevel >= maxLevel) {
                        eventController.requestEventFiring(retval, true);
                    } else {
                        AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                    }
                }

                deleteMethod();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOperation of BaseOperationManager', ex);
                retval = -1;
            }

            return retval;

        },


        deleteOperationHead: function (operation, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!operation) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(operation);
                            me.getCache().removeFromCache(operation);
                            eventController.requestEventFiring(me.EVENTS.OPERATION_DELETED, operation);
                        }

                        eventController.requestEventFiring(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOperationHead of BaseOperationManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                //distinguish if we have to update the database record or can delete it directly
                //this depends on it's former updateflag
                var hasToBeUpdated = operation.get('updFlag') !== 'I';

                if (hasToBeUpdated) {
                    operation.set('updFlag', 'D')
                    var toUpdate = me.buildDataBaseObjectForOperation(operation);
                    AssetManagement.customer.core.Core.getDataBaseHelper().update('D_ORDOPER', null, toUpdate, callback, callback);
                } else {
                    //get the key of the operation to delete
                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_ORDOPER', operation);
                    AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_ORDOPER', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOperationHead of BaseOperationManager', ex);
            }

            return retval;
        },

        getEarliestOperation: function (order) {
            var retval = null;
            try {
                var operations = order.get('operations');

                if (operations && operations.getCount() > 0) {
                    var curEarliest = operations.getAt(0);
                    var earliestTime = !AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(curEarliest.get('ntan')) ? curEarliest.get('ntan') : curEarliest.get('fsav');

                    operations.each(function (operation) {
                        var checkTime = null;
                        if (!AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(operation.get('ntan')))
                            checkTime = operation.get('ntan');
                        else
                            checkTime = operation.get('fsav');

                        if (!AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(checkTime) && (AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(earliestTime) || (checkTime < earliestTime))) {
                            curEarliest = operation;
                            earliestTime = checkTime;
                        }
                    }, this);

                    retval = curEarliest;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEarliestOperation of BaseOperationManager', ex);
            }

            return retval;
        },

        //private methods	
        buildOperationsStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var operation = this.buildOperationFromDbResultObject(cursor.value);
                        store.add(operation);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOperationsStoreFromDataBaseQuery of BaseOperationManager', ex);
            }
        },

        buildOperationFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.Operation', {
                    aufnr: dbResult['AUFNR'],
                    vornr: dbResult['VORNR'],
                    aufpl: dbResult['AUFPL'],
                    aplzl: dbResult['APLZL'],
                    steus: dbResult['STEUS'],
                    ltxa1: dbResult['LTXA1'],
                    priok: dbResult['PRIOK'],
                    larnt: dbResult['LARNT'],
                    pernr: dbResult['PERNR'],
                    dauno: dbResult['DAUNO'],
                    daune: dbResult['DAUNE'],
                    anzma: dbResult['ANZMA'],
                    arbei: dbResult['ARBEI'],
                    arbeh: dbResult['ARBEH'],
                    arbpl: dbResult['ARBPL'],
                    werks: dbResult['WERKS'],
                    istru: dbResult['ISTRU'],
                    stsma: dbResult['STSMA'],
                    objnr: dbResult['OBJNR'],
                    ktsch: dbResult['KTSCH'],
                    obknr: dbResult['OBKNR'],
                    obzae: dbResult['OBZAE'],
                    txtsp: dbResult['TXTSP'],
                    ablad: dbResult['ABLAD'],
                    sumnr: dbResult['SUMNR'],
                    
					usr01Flag: dbResult['USR01FLAG'],
					usr02Flag: dbResult['USR02FLAG'],

					usr03Code: dbResult['USR03CODE'],
					usr04Code: dbResult['USR04CODE'],

					usr05Txt: dbResult['USR05TXT'],
					usr06Txt: dbResult['USR06TXT'],
					usr07Txt: dbResult['USR07TXT'],

					usr08Date: dbResult['USR08DATE'],
					usr09Num: dbResult['USR09NUM'],

                    fsav: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['FSAVD'], dbResult['FSAVZ']),
                    fsed: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['FSEDD'], dbResult['FSEDZ']),
                    ntan: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['NTANF'], dbResult['NTANZ']),
                    nten: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['NTEND'], dbResult['NTENZ']),
                    isd: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ISDD'], dbResult['ISDZ']),
                    ied: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['IEDD'], dbResult['IEDZ']),

                    updFlag: dbResult['UPDFLAG'],
                    mobileKey: dbResult['MOBILEKEY'],
                    childKey: dbResult['CHILDKEY']
                });

                retval.set('id', retval.get('aufnr') + retval.get('vornr') + retval.get('aufpl') + retval.get('aplzl'));

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOperationFromDbResultObject of BaseOperationManager', ex);
            }

            return retval;
        },

        loadListDependendDataForOperations: function (eventIdToFireWhenComplete, operations) {
            try {
                //do not continue if there is no data
                if (operations.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, operations);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //before the data can be assigned, the lists have to be loaded by all other managers
                //load these data flat!
                //register on the corresponding events
                var timeConfs = null;
                var matConfs = null;

                var counter = 0;

                //TO-DO make dependend of customizing parameters --- this definitely will increase performance

                var done = 0;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignListDependendDataForOperations.call(me, eventIdToFireWhenComplete, operations, timeConfs, matConfs);
                    }
                };

                //get timeConfs
                if (true) {
                    done++;

                    var timeConfSuccessCallback = function (times) {
                        errorOccurred = times === undefined;

                        timeConfs = times;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.TimeConfManager.getAllTimeConfs(false, true);
                    eventController.registerOnEventForOneTime(eventId, timeConfSuccessCallback);
                }

                //get matConfs
                if (true) {
                    done++;

                    var matConfSuccessCallback = function (mConfs) {
                        errorOccurred = mConfs === undefined;

                        matConfs = mConfs;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.MatConfManager.getAllMatConfs(true, true);
                    eventController.registerOnEventForOneTime(eventId, matConfSuccessCallback);
                }

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForOperations of BaseOperationManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        //call this function, when all necessary data has been collected
        assignListDependendDataForOperations: function (eventIdToFireWhenComplete, operations, timeConfs, matConfs) {
            try {
                operations.each(function (operation, index, length) {
                    var aufnr = operation.get('aufnr');
                    var vornr = operation.get('vornr');
                    //var split = operation.get('split');

                    //assign timeConfs
                    if (timeConfs) {
                        var opersTimeConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.TimeConf',
                            autoLoad: false
                        });

                        operation.set('timeConfs', opersTimeConfs);

                        timeConfs.each(function (timeConf) {
                            if (timeConf.get('aufnr') === aufnr && timeConf.get('vornr') === vornr) //&& timeConf.get('split') === split)
                                opersTimeConfs.add(timeConf);
                        });

                        //remove assigned operations
                        opersTimeConfs.each(function (timeConf) {
                            timeConfs.remove(timeConf);
                        });
                    }

                    //assign matConfs
                    if (matConfs) {
                        var opersMatConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.MaterialConf',
                            autoLoad: false
                        });

                        operation.set('matConfs', opersMatConfs);

                        matConfs.each(function (matConf) {
                            if (matConf.get('aufnr') === aufnr && matConf.get('vornr') === vornr) //&& timeConf.get('split') === split)
                                opersMatConfs.add(matConf);
                        });

                        //remove assigned operations
                        opersMatConfs.each(function (matConf) {
                            matConfs.remove(matConf);
                        });
                    }
                });

                var cache = this.getCache();
                cache.addStoreForAllToCache(operations, cache.self.DATAGRADES.LOW);

                //return the store from cache to eliminate duplicate objects
                var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForOperations of BaseOperationManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        loadDependendDataForOperations: function (operations, eventIdToFireWhenComplete) {
            try {
                //do not continue if there is no data
                if (operations.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, operations);
                    return;
                }

                var aufnr = operations.getAt(0).get('aufnr');

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var timeConfs = null;
                var matConfs = null;
                var objectStatusList = null;
                var completion = null;

                var counter = 0;
                var done = 0;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignDependendDataForOperations.call(me, eventIdToFireWhenComplete, operations, timeConfs, matConfs, objectStatusList, completion);
                    }
                };

                //Longtext - pass the items to LT Manager and let it set all textes. Fire an event on completition
                operations.each(function (oper) {
                    //getLocalLongtext
                    if (true) {
                        done++;

                        var localLongtextSuccessCallback = function (localLT, backendLT) {
                            errorOccurred = localLT === undefined || backendLT === undefined;
                            oper.set('localLongtext', localLT);
                            oper.set('backendLongtext', backendLT);
                            counter++;

                            completeFunction();
                        };
                        eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(oper, true);
                        eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
                    }
                    ////completion object
                    //if (true) {
                    //    done++;
                    //    var order = null;
                    //    //var completionSuccessCallback = function (compl) {
                    //    //    erroroccurred = compl === undefined;

                    //    //    oper.set('compl', compl)
                    //    //    counter++;

                    //    //    completeFunction();
                    //    //};

                    //    //var loadedNotifCallback = function (notif) {
                    //    //    try {
                    //    //        if (notif) {
                    //    //            eventId = AssetManagement.customer.manager.CompletionManager.getCompletionsForObject(notif.get('objnr'), true);
                    //    //            eventController.registerOnEventForOneTime(eventId, completionSuccessCallback);
                    //    //        } else {
                    //    //            counter++;
                    //    //            completeFunction();
                    //    //        }
                    //    //    } catch (ex) {
                    //    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForOperations of BaseOperationManager', ex);
                    //    //    }
                    //    //};

                    //    //var loadedObjectListItemCallback = function (objListItem) {
                    //    //    try {
                    //    //        if (objListItem) {
                    //    //            var eventNotifId = AssetManagement.customer.manager.NotifManager.getNotif(objListItem.get('ihnum'), true);
                    //    //            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventNotifId, loadedNotifCallback);
                    //    //        } else {
                    //    //            counter++;
                    //    //            completeFunction();
                    //    //        }
                    //    //    } catch (ex) {
                    //    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForOperations of BaseOperationManager', ex);
                    //    //    }
                    //    //};

                    //    //var eventObjListId = AssetManagement.customer.manager.ObjectListManager.getObjectListItem(oper.get('aufnr'), oper.get('obknr'), oper.get('obzae'), true);
                    //    //AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventObjListId, loadedObjectListItemCallback);

                    //}
                });

                //OxToDo make dependend of customizing parameters --- this definitely will increase performance

                //get timeConfs
                if (true) {
                    done++;

                    var timeConfSuccessCallback = function (times) {
                        errorOccurred = times === undefined;

                        timeConfs = times;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.TimeConfManager.getTimeConfsForOrder(aufnr, true);
                    eventController.registerOnEventForOneTime(eventId, timeConfSuccessCallback);
                }

                //get matConfs
                if (true) {
                    done++;

                    var matConfSuccessCallback = function (mConfs) {
                        errorOccurred = mConfs === undefined;

                        matConfs = mConfs;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.MatConfManager.getMatConfsForOrder(aufnr, true);
                    eventController.registerOnEventForOneTime(eventId, matConfSuccessCallback);
                }

                //get objectStatus
                if (true) {
                    done++;
                    var objectStatusSuccessCallback = function (objStatusList) {
                        errorOccurred = objStatusList === undefined;

                        objectStatusList = objStatusList;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.ObjectStatusManager.getStatusStoreForOrdersOperations(operations, true);
                    eventController.registerOnEventForOneTime(eventId, objectStatusSuccessCallback);
                }

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForOperations of BaseOperationManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        assignDependendDataForOperations: function (eventIdToFireWhenComplete, operations, timeConfs, matConfs, objectStatusList, completion) {
            try {
                //remove all objects from the stores, which have been assigned to it's operation
                //therefore following loops will have less iterations
                operations.each(function (operation, index, length) {
                    //assign timeConfs
                    if (timeConfs) {
                        var vornr = operation.get('vornr');
                        //var split = operation.get('split');

                        var opersTimeConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.TimeConf',
                            autoLoad: false
                        });

                        operation.set('timeConfs', opersTimeConfs);

                        timeConfs.each(function (timeConf) {
                            if (timeConf.get('vornr') === vornr) //&& timeConf.get('split') === split)
                                opersTimeConfs.add(timeConf);
                        });

                        //remove assigned operations
                        opersTimeConfs.each(function (timeConf) {
                            timeConfs.remove(timeConf);
                        });
                    }

                    //getTimeconf sums
                    if (true) {
                        operation.set('tConfSum', AssetManagement.customer.manager.TimeConfManager.updateTimeConfSumOperation(operation));
                    }

                    //assign matConfs
                    if (matConfs) {
                        var vornr = operation.get('vornr');
                        //var split = operation.get('split');

                        var opersMatConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.MaterialConf',
                            autoLoad: false
                        });

                        operation.set('matConfs', opersMatConfs);

                        matConfs.each(function (matConf) {
                            if (matConf.get('vornr') === vornr) //&& timeConf.get('split') === split)
                                opersMatConfs.add(matConf);
                        });

                        //remove assigned operations
                        opersMatConfs.each(function (matConf) {
                            matConfs.remove(matConf);
                        });
                    }

                    //assign objectStatus
                    if (objectStatusList) {
                        var aufpl = operation.get('aufpl');
                        var aplzl = operation.get('aplzl');

                        var objnr = "OV" + aufpl + aplzl;

                        var opersObjectStatusList = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.ObjectStatus',
                            autoLoad: false
                        });

                        operation.set('objectStatusList', opersObjectStatusList);

                        if (objectStatusList.getCount() > 0) {
                            objectStatusList.each(function (objectStatus) {
                                if (objectStatus.get('objnr') == objnr) {
                                    opersObjectStatusList.add(objectStatus);
                                }
                            });

                            if (opersObjectStatusList.getCount() > 0) {
                                opersObjectStatusList.each(function (objectStatus) {
                                    objectStatusList.remove(objectStatus);
                                });
                            }
                        }
                    }
                });

                var cache = this.getCache();
                var aufnr = operations.getAt('0').get('aufnr');
                cache.addToCacheForOrder(aufnr, operations, cache.self.DATAGRADES.FULL);

                //return the store from cache to eliminate duplicate objects
                var toReturn = cache.getFromCacheForOrder(aufnr, cache.self.DATAGRADES.FULL);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForOperations of BaseOperationManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        //builds the raw object to store in the database out of a operation model
        buildDataBaseObjectForOperation: function (operation) {
            var retval = null;
            var ac = null;

            try {
                ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['UPDFLAG'] = operation.get('updFlag');
                retval['AUFNR'] = operation.get('aufnr');
                retval['VORNR'] = operation.get('vornr');
                retval['AUFPL'] = operation.get('aufpl');
                retval['APLZL'] = operation.get('aplzl');
                retval['STEUS'] = operation.get('steus') === '' ? operation.get('order').get('orderType').get('steus') : operation.get('steus');
                retval['LTXA1'] = operation.get('ltxa1');
                retval['LARNT'] = operation.get('larnt');
                retval['PERNR'] = operation.get('pernr');
                retval['DAUNO'] = operation.get('dauno');
                retval['DAUNE'] = operation.get('daune');
                retval['ANZMA'] = operation.get('anzma');
                retval['ARBEI'] = operation.get('arbei');
                retval['ARBEH'] = operation.get('arbeh');
                retval['FSAVD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(operation.get('fsav'));
                retval['FSAVZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(operation.get('fsav'));
                retval['FSEDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(operation.get('fsed'));
                retval['FSEDZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(operation.get('fsed'));
                retval['ARBPL'] = operation.get('arbpl');
                retval['WERKS'] = operation.get('werks');
                retval['NTANF'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(operation.get('ntan'));
                retval['NTANZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(operation.get('ntan'));
                retval['NTEND'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(operation.get('nten'));
                retval['NTENZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(operation.get('nten'));
                retval['ISDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(operation.get('isd'));
                retval['ISDZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(operation.get('isd'));
                retval['IEDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(operation.get('ied'));
                retval['IEDZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(operation.get('ied'));
                retval['ISTRU'] = operation.get('istru');
                retval['STSMA'] = operation.get('stsma');
                retval['OBJNR'] = operation.get('objnr');
                retval['KTSCH'] = operation.get('ktsch');
                retval['OBKNR'] = operation.get('obknr');
                retval['OBZAE'] = operation.get('obzae');
                retval['SUMNR'] = operation.get('sumnr');
                retval['ABLAD'] = operation.get('ablad');
                retval['USR01FLAG'] = operation.get('usr01Flag'),
                retval['USR02FLAG'] = operation.get('usr01Flag'),
                retval['USR03CODE'] = operation.get('usr01Flag'),
                retval['USR04CODE'] = operation.get('usr01Flag'),
                retval['USR05TXT'] = operation.get('usr01Flag'),
                retval['USR06TXT'] = operation.get('usr01Flag'),
                retval['USR07TXT'] = operation.get('usr01Flag'),
                retval['USR08DATE'] = operation.get('usr01Flag'),
                retval['USR09NUM'] = operation.get('usr01Flag'),


                retval['MOBILEKEY'] = operation.get('mobileKey');
                retval['CHILDKEY'] = operation.get('childKey');

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForOperation of BaseOperationManager', ex);
            }

            return retval;
        },

        //will prepare a operation's keyfields for saving
        //returns a boolean via callback, indicating, if the operation keyfields are ready, so the operation can be saved
        manageKeyFieldsBeforeSave: function (operation, callback) {
            try {
                if (!callback)
                    return;

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('aufnr')))
                    callback.call(this, false);

                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //check if the operation requires a new vornr
                var requiresNewVornr = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('vornr'));

                //check if the operation requires a new aufpl
                var requiresNewAufpl = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('aufpl'));

                //check if the operation requires a new aplzl
                var requiresNewAplzl = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('aplzl'));

                var vornrToSet = '';
                var aufplToSet = '';
                var aplzlToSet = '';

                var pendingRequests = 0;
                var errorOccurred = false;
                var reported = false;

                //checks for errors and still pending requests
                var completeFunction = function () {
                    try {
                        if (errorOccurred === true && reported === false) {
                            reported = true;

                            callback.call(this, false);
                        } else if (pendingRequests == 0 && errorOccurred === false) {
                            operation.set('vornr', vornrToSet);
                            operation.set('aufpl', aufplToSet);
                            operation.set('aplzl', aplzlToSet);

                            operation.set('id', operation.get('aufnr') + vornrToSet + aufplToSet + aplzlToSet);

                            callback.call(this, true);
                        }
                    } catch (ex) {
                        if (!reported) {
                            reported = true;

                            callback.call(this, false);
                        }
                    }
                };

                if (requiresNewVornr) {
                    pendingRequests++;

                    var nextVornrCallback = function (nextVornr) {
                        vornrToSet = (nextVornr + '');
                        errorOccurred = vornrToSet === '-1';

                        pendingRequests--;

                        completeFunction();
                    };

                    eventId = this.getNextVornr(operation, true);
                    if (eventId > -1) {
                        eventController.registerOnEventForOneTime(eventId, nextVornrCallback);
                    } else {
                        errorOccurred = true;
                    }
                } else {
                    vornrToSet = operation.get('vornr');
                }

                if (requiresNewAufpl) {
                    pendingRequests++;

                    var nextAufplCallback = function (nextAufpl) {
                        aufplToSet = nextAufpl + '';
                        errorOccurred = aufplToSet === '-1';

                        pendingRequests--;

                        completeFunction();
                    };

                    eventId = this.getNextAufpl(operation, true);
                    if (eventId > -1) {
                        eventController.registerOnEventForOneTime(eventId, nextAufplCallback);
                    } else {
                        errorOccurred = true;
                    }
                } else {
                    aufplToSet = operation.get('aufpl');
                }

                if (requiresNewAplzl) {
                    pendingRequests++;

                    var nextAplzlCallback = function (nextAplzl) {
                        aplzlToSet = nextAplzl + '';
                        errorOccurred = aplzlToSet === '-1';

                        pendingRequests--;

                        completeFunction();
                    };

                    eventId = this.getNextAplzl(operation, true);
                    if (eventId > -1) {
                        eventController.registerOnEventForOneTime(eventId, nextAplzlCallback);
                    } else {
                        errorOccurred = true;
                    }
                } else {
                    aplzlToSet = operation.get('aplzl');
                }

                if (pendingRequests > 0 && !errorOccurred) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageKeyFieldsBeforeSave of BaseOperationManager', ex);
                callback.call(this, false);
            }
        },

        getNextVornr: function (operation, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!operation) {
                    eventController.requestEventFiring(retval, -1);
                    return retval;
                }

                var maxVornr = '';

                var successCallback = function (eventArgs) {
                    try {
                        if (eventArgs && eventArgs.target && eventArgs.target.result) {
                            var cursor = eventArgs.target.result;

                            var vornrValue = cursor.value['VORNR'];
                            maxVornr = cursor.value['VORNR'];
                            //check if the counter value is a local one
                            if (AssetManagement.customer.utils.StringUtils.startsWith(vornrValue, '%')) {
                                //cut of the percent sign
                                vornrValue = vornrValue.substr(1);
                                var curVornrValue = parseInt(vornrValue);

                                if (isNaN(curVornrValue)) {
                                    if (curVornrValue > maxVornr)
                                        maxVornr = curVornrValue;
                                }
                            }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            if (maxVornr === "") {
                                maxVornr = '0000';
                            }
                            var nextLocalCounterNumberValue = parseInt(maxVornr) + 10;
                            var nextLocalCounterValue = AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 4);

                            eventController.requestEventFiring(retval, nextLocalCounterValue);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextVornr of BaseOperationManager', ex);
                        eventController.requestEventFiring(retval, -1);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('AUFNR', operation.get('aufnr'));

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ORDOPER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ORDOPER', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextVornr of BaseOperationManager', ex);
            }

            return retval;
        },

        getNextAufpl: function (operation, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!operation) {
                    eventController.requestEventFiring(retval, -1);
                    return retval;
                }

                var indexCallback = function (nextIndex) {
                    try {
                        var nextAufpl = -1;

                        if (nextIndex) {
                            nextAufpl = AssetManagement.customer.utils.StringUtils.padLeft(nextIndex, '0', 10);
                        }

                        eventController.requestEventFiring(retval, nextAufpl);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextAufpl of BaseOperationManager', ex);
                        eventController.requestEventFiring(retval, -1);
                    }
                };

                eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_ORDOPER', useBatchProcessing);

                if (eventId > 1)
                    eventController.registerOnEventForOneTime(eventId, indexCallback);
                else
                    eventController.requestEventFiring(retval, false);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextAufpl of BaseOperationManager', ex);
            }

            return retval;
        },

        getNextAplzl: function (operation, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!operation) {
                    eventController.requestEventFiring(retval, -1);
                    return retval;
                }

                var maxAplzl = 0;

                var successCallback = function (eventArgs) {
                    try {
                        if (eventArgs && eventArgs.target && eventArgs.target.result) {
                            var cursor = eventArgs.target.result;

                            var aplzlValue = cursor.value['APLZL'];

                            //check if the counter value is a local one
                            if (AssetManagement.customer.utils.StringUtils.startsWith(aplzlValue, '%')) {
                                //cut of the percent sign
                                aplzlValue = aplzlValue.substr(1);
                                var curAplzlValue = parseInt(aplzlValue);

                                if (!isNaN(curAplzlValue)) {
                                    if (curAplzlValue > maxAplzl)
                                        maxAplzl = curAplzlValue;
                                }
                            }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            var nextLocalCounterNumberValue = maxAplzl + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 7);

                            eventController.requestEventFiring(retval, nextLocalCounterValue);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextAplzl of BaseOperationManager', ex);
                        eventController.requestEventFiring(retval, -1);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('AUFNR', operation.get('aufnr'));
                //aufpl is not needed, because aufpl is a unique value for each order

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ORDOPER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ORDOPER', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextAplzl of BaseOperationManager', ex);
            }

            return retval;
        },

        reorderOperations: function (operations) {
            try {
                //sort the store by the vornr
                operations.sort('vornr', 'ASC');
                var lastVornr = 0;
                for (var i = 0; i < operations.length; i++) {
                    var vornrString = operations.get('vornr');
                    var vornr = lastVornr;
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornrString))
                        vornr += 10;
                    else {
                        vornr = parseInt(vornrString);
                    }

                    var newVornrString = AssetManagement.customer.utils.StringUtils.padLeft('' + vornr, '', 4);
                    if (operations.get('vornr') != newVornrString)
                        operations.set('vornr', newVornrString);

                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reorderOperations of BaseOperationManager', ex);
            }
        },

        saveDependendDataOfOperation: function (operation, callback) {
            try {
                if (!operation) {
                    if (callback)
                        callback.call(this, false);

                    return;
                }

                var operationsObjectStatusStore = operation.get('objectStatusList');

                var pendingRequests = 0;
                var errorOccurred = false;
                var reported = false;

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        if (callback)
                            callback.call(this, false);

                        reported = true;
                    } else if (pendingRequests == 0 && errorOccurred === false) {
                        if (callback)
                            callback.call(this, true);
                    }
                };

                //drop the save request for object status
                if (operationsObjectStatusStore && operationsObjectStatusStore.getCount() > 0) {
                    pendingRequests++;

                    var objectStatusSaveCallback = function (success) {
                        try {
                            errorOccurred = !success;

                            pendingRequests--;

                            completeFunction();
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDependendDataOfOperation of BaseOperationManager', ex);

                            if (callback)
                                callback.call(this, false);
                        }
                    };

                    var eventId = AssetManagement.customer.manager.ObjectStatusManager.saveObjectStatusList(operationsObjectStatusStore);
                    eventController.registerOnEventForOneTime(eventId, objectStatusSaveCallback);
                }

                if (pendingRequests > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDependendDataOfOperation of BaseOperationManager', ex);

                if (callback)
                    callback.call(this, false);
            }
        },

        loadDependendDataForCalendarOperations: function (operations, eventIdToFireWhenComplete) {
            try {
                //do not continue if there is no data
                if (operations.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, operations);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var objectStatusList = null;

                var counter = 0;
                var done = 0;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignDependendDataForOperations.call(me, eventIdToFireWhenComplete, operations, null, null, objectStatusList);
                    }
                };

                //get objectStatus
                if (true) {
                    done++;
                    var objectStatusSuccessCallback = function (objStatusList) {
                        errorOccurred = objStatusList === undefined;

                        objectStatusList = objStatusList;
                        counter++;

                        completeFunction();
                    };
                    eventId = AssetManagement.customer.manager.ObjectStatusManager.getStatusStoreForOrdersOperations(operations, true);
                    eventController.registerOnEventForOneTime(eventId, objectStatusSuccessCallback);
                }

                if (done > 0)
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                else
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, operations);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForOperations of BaseOperationManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        }
    }
});