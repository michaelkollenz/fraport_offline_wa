Ext.define('AssetManagement.base.manager.BaseBillOfMaterialManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.model.bo.Mast',
	    'AssetManagement.customer.model.bo.Tpst',
        'AssetManagement.customer.model.bo.Stas',
        'AssetManagement.customer.model.bo.Stpo',
        'AssetManagement.customer.manager.MastManager',
        'AssetManagement.customer.manager.EqstManager',
        'AssetManagement.customer.manager.TpstManager',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.StoreHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public methods
		getBillOfMaterialForOrder: function(order, includeFuncLocBom, includeEquiBom) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(!order || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('iwerk'))) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var iwerk = order.get('iwerk');
				var me = this;
				
				//first step is, to get the mast
				//if also the bom of orders funcloc may be included, start an request on getBillOfMaterialForFuncLoc
				//do the same for equipment
				var orderMaterialBom = null;
				var orderFuncLocBom = null;
				var orderEquiBom = null;

				var pendingRequests = 0;
				
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccured === false) {
						//merge the boms to one store
						var allToGether = AssetManagement.customer.helper.StoreHelper.mergeStores([ orderMaterialBom, orderFuncLocBom, orderEquiBom ]);
						eventController.fireEvent(retval, allToGether);
					}
				};
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('sermat'))) {
					pendingRequests++;

					var orderMaterialBomCallback = function(materialBom) {
						errorOccured = materialBom === undefined;
						
						orderMaterialBom = materialBom;
						
						pendingRequests--;								

						completeFunction();
					};

					var orderMaterialBomRequest = this.getBillOfMaterialForMaterial(order.get('sermat'), iwerk);
					eventController.registerOnEventForOneTime(orderMaterialBomRequest, orderMaterialBomCallback);
				}
				
				//also drop a request for func. loc. if requested and the order has a func. loc.
				if(includeFuncLocBom === true && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('tplnr'))) {
					pendingRequests++;
					
					var funcLocBomCallback = function(funcLocBom) {
						errorOccured = funcLocBom === undefined;

						orderFuncLocBom = funcLocBom;
						
						pendingRequests--;								
						
						completeFunction();
					};
					
					var funcLocBomRequest = this.getBillOfMaterialForFuncLoc(order.get('tplnr'), iwerk);
					eventController.registerOnEventForOneTime(funcLocBomRequest, funcLocBomCallback);
				}
				
				//do the same for equi
				if(includeEquiBom === true && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('equnr'))) {
					pendingRequests++;
					
					var equiBomCallback = function(equiBom) {
						errorOccured = equiBom === undefined;

						orderEquiBom = equiBom;
						
						pendingRequests--;								
						
						completeFunction();
					};
					
					var equiBomRequest = this.getBillOfMaterialForEquipment(order.get('equnr'), iwerk);
					eventController.registerOnEventForOneTime(equiBomRequest, equiBomCallback);
				}

				if(pendingRequests === 0)
					eventController.requestEventFiring(retval, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForOrder of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterialForFuncLoc: function(tplnr, iwerk) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(iwerk)) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var me = this;
				var tpstCallback = function(tpst) {
					try {
						if(!tpst) {
							eventController.fireEvent(retval, tpst);
							return;
						}
					
						var bomCallback = function(bom) {
							try {
								eventController.fireEvent(retval, bom);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForFuncLoc of BaseBillOfMaterialManager', ex);
								eventController.fireEvent(retval, undefined);
							}
						};
					
						var bomRequest = me.getBillOfMaterialForTpst(tpst);
						eventController.registerOnEventForOneTime(bomRequest, bomCallback);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForFuncLoc of BaseBillOfMaterialManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var tpstRequest = AssetManagement.customer.manager.TpstManager.getTpst(tplnr, iwerk);
				eventController.registerOnEventForOneTime(tpstRequest, tpstCallback);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForFuncLoc of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterialForEquipment: function(equnr, iwerk) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(iwerk)) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var me = this;
				var eqstCallback = function(eqst) {
					try {
						if(!eqst) {
							eventController.fireEvent(retval, eqst);
							return;
						}
					
						var bomCallback = function(bom) {
							try {
								eventController.fireEvent(retval, bom);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForEquipment of BaseBillOfMaterialManager', ex);
								eventController.fireEvent(retval, undefined);
							}
						};
					
						var bomRequest = me.getBillOfMaterialForEqst(eqst);
						eventController.registerOnEventForOneTime(bomRequest, bomCallback);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForEquipment of BaseBillOfMaterialManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var eqstRequest = AssetManagement.customer.manager.EqstManager.getEqst(equnr, iwerk);
				eventController.registerOnEventForOneTime(eqstRequest, eqstCallback);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForEquipment of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterialForMaterial: function(matnr, iwerk) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(iwerk)) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var me = this;
				var mastCallback = function(mast) {
					try {
						if(!mast) {
							eventController.fireEvent(retval, mast);
							return;
						}
					
						var bomCallback = function(bom) {
							try {
								eventController.fireEvent(retval, bom);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForMaterial of BaseBillOfMaterialManager', ex);
								eventController.fireEvent(retval, undefined);
							}
						};
					
						var bomRequest = me.getBillOfMaterialForMast(mast);
						eventController.registerOnEventForOneTime(bomRequest, bomCallback);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForMaterial of BaseBillOfMaterialManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var mastRequest = AssetManagement.customer.manager.MastManager.getMast(matnr, iwerk);
				eventController.registerOnEventForOneTime(mastRequest, mastCallback);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForMaterial of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getAllFuncLocBasedBillOfMaterial: function() {
			var retval = -1;
			
			try {
				retval = this.getBillOfMaterial('T', "", "");
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllFuncLocBasedBillOfMaterial of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getAllEquipmentBasedBillOfMaterial: function() {
			var retval = -1;
		
			try {
				retval = this.getBillOfMaterial('E', "", "");
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllEquipmentBasedBillOfMaterial of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getAllMaterialBasedBillOfMaterial: function() {
			var retval = -1;
		
			try {
				retval = this.getBillOfMaterial('M', "", "");
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMaterialBasedBillOfMaterial of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterialForTpst: function(tpst) {
			var retval = -1;
		
			try {
				retval = this.getBillOfMaterial('T', tpst.get('stlnr'), tpst.get('stlal'));
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForTpst of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterialForEqst: function(eqst) {
			var retval = -1;
		
			try {
				retval = this.getBillOfMaterial('E', eqst.get('stlnr'), eqst.get('stlal'));
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForEqst of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterialForMast: function(mast) {
			var retval = -1;
			
			try {
				retval = this.getBillOfMaterial('M', mast.get('stlnr'), mast.get('stlal'));
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterialForMast of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},
		
		getBillOfMaterial: function(stlty, stlnr, stlal) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stlty)) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var stasStore = this.getFromStasCache(stlty, stlnr, stlal);
					
				if(stasStore) {
					this.loadStposForStass(stasStore, retval);
				
					return retval;
				}
				
				stasStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Stas',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
												
						me.buildStassStoreFromDataBaseQuery.call(me, stasStore, eventArgs);
						
						if(done) {
							me.addToStasCache(stlty, stlnr, stlal, stasStore);
							me.loadStposForStass.call(me, stasStore, retval);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterial of BaseBillOfMaterialManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('STLTY', stlty);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stlnr))
					keyMap.add('STLNR', stlnr);
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stlal))
					keyMap.add('STLAL', stlal);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_STAS', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_STAS', keyRange, successCallback, null, false);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBillOfMaterial of BaseBillOfMaterialManager', ex);
			}
			
			return retval;
		},

		//private methods
		loadStposForStass: function(stass, eventIdToFireWhenBomLoaded) {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var listenTo = eventController.getNextEventId();
				
				if(!stass) {
					eventController.requestEventFiring(eventIdToFireWhenBomLoaded, undefined);
					return;
				}
				
				var allsInOneStpoStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Stpo',
					autoLoad: false
				});
				
				var counter = 0;				
				var done = stass.getCount();
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.loadDependendDataForStpos.call(me, allsInOneStpoStore, eventIdToFireWhenBomLoaded);
					}
				};
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
												
						me.buildStposStoreFromDataBaseQuery.call(me, allsInOneStpoStore, eventArgs);
						
						if(done) {
							counter++;
							completeFunction();
						}
					} catch(ex) {
						errorOccured = true;
						completeFunction();
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadStposForStass of BaseBillOfMaterialManager', ex);
					}
				};
				
				stass.each(function(stas) {
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('STLTY', stas.get('stlty'));
					keyMap.add('STLNR', stas.get('stlnr'));
					keyMap.add('STLKN', stas.get('stlkn'));
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_STPO', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_STPO', keyRange, successCallback, null, true);
				 });
				
				 AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
			} catch(ex) {
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, undefined);
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadStposForStass of BaseBillOfMaterialManager', ex);
			}
		},
		
		loadDependendDataForStpos: function(stpos, eventIdToFireWhenBomLoaded) {
			try {
				//do not continue if there is no data
				if(stpos.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenBomLoaded, stpos);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var materials = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignDependendDataForStpos.call(me, eventIdToFireWhenBomLoaded, stpos, materials);
					}
				};
				
				//get materials
				done++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccured = materialStore === undefined;
					
					materials = materialStore;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				if(done > 0)
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				else
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, stpos);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForStpos of BaseBillOfMaterialManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, undefined);
			}
		},
		
		assignDependendDataForStpos: function(eventIdToFireWhenBomLoaded, stpos, materials) {
			try {
				stpos.each(function (stpo, index, length) {
					//assign materials
					if(materials) {
						var matnr = stpo.get('idnrk');
						
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								stpo.set('material', material);
								return false;
							}
						});
					}
				});
				
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, stpos);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForStpos of BaseBillOfMaterialManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenBomLoaded, undefined);
			}
		},
		
		buildStassStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var stas = this.buildStasFromDbResultObject(cursor.value);
						store.add(stas);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
	        } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStassStoreFromDataBaseQuery of BaseBillOfMaterialManager', ex);
			}	
		},
		
		buildStposStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var stpo = this.buildStpoFromDbResultObject(cursor.value);
						store.add(stpo);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
	        } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStposStoreFromDataBaseQuery of BaseBillOfMaterialManager', ex);
			}		
		},
		
		buildStasFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Stas', {
					stlty: dbResult['STLTY'],
					stlnr: dbResult['STLNR'],
					stlal: dbResult['STLAL'],
					stlkn: dbResult['STLKN'],
					stasz: dbResult['STASZ'],
				
				    updFlag: dbResult['UPDFLAG']

				});
				
				retval.set('id', dbResult['STLTY'] + dbResult['STLNR'] + dbResult['STLAL'] + dbResult['STLKN'] + dbResult['STASZ']);
				
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStasFromDbResultObject of BaseBillOfMaterialManager', ex);
			}			
			return retval;
		},
		
		buildStpoFromDbResultObject: function(dbResult) {
	        var retval = null;   
		
	        try {
	             retval = Ext.create('AssetManagement.customer.model.bo.Stpo', {
					stlty: dbResult['STLTY'],
					stlnr: dbResult['STLNR'],
					stlkn: dbResult['STLKN'],
					stpoz: dbResult['STPOZ'],
					idnrk: dbResult['IDNRK'],
					postp: dbResult['POSTP'],
					posnr: dbResult['POSNR'],
					sortf: dbResult['SORTF'],
					meins: dbResult['MEINS'],
					menge: dbResult['MENGE'],
					erskz: dbResult['ERSKZ'],
					sanin: dbResult['SANIN'],
					stkkz: dbResult['STKKZ'],
					datuv: dbResult['DATUV'],
					
				    updFlag: dbResult['UPDFLAG']

				 });		
				    retval.set('id', dbResult['STLTY'] + dbResult['STLNR'] + dbResult['STLKN']
				           		+ dbResult['STPOZ'] + dbResult['IDNRK'] + dbResult['POSTP'] + dbResult['POSNR']);
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStpoFromDbResultObject of BaseBillOfMaterialManager', ex);
			}
			return retval;
		},
		
		//cache administration
		_stasCache: null,
		
		getStasCache: function() {
	        try {
	        	if(!this._stasCache) {
					this._stasCache = Ext.create('Ext.util.HashMap');
					
					this.initializeCacheHandling();
				}
			} catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStasCache of BaseBillOfMaterialManager', ex);
			}
			return this._stasCache;
		},
	
		clearCache: function() {
	        try {
			    this.getStasCache().clear();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseBillOfMaterialManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseBillOfMaterialManager', ex);
			} 
		},
		
		addToStasCache: function(stlty, stlnr, stlal, stasStore) {	
	        try {
			    var temp = this.getStasCache().add(stlty + stlnr + stlal, stasStore);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToStasCache of BaseBillOfMaterialManager', ex);
			}    
		},
		
		getFromStasCache: function(stlty, stlnr, stlal) {
	        var retval = null;

	        try {
			    retval = this.getStasCache().get(stlty, stlnr, stlal);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromStasCache of BaseBillOfMaterialManager', ex);
			}
			return retval;
		}
	}
});