Ext.define('AssetManagement.base.manager.BaseOrderReportManager', {
	requires: [
	    'AssetManagement.customer.controller.EventController',
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
//      'AssetManagement.customer.manager.NotifManager',
        'AssetManagement.customer.manager.BanfManager',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.manager.CustBemotManager',
        'AssetManagement.customer.manager.TextComponentManager',
		'AssetManagement.customer.model.bo.ObjectListItem',
		'AssetManagement.customer.model.bo.TimeConf',
		'AssetManagement.customer.model.bo.MaterialConf',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
	    HEADERLOGO_RESSOURCE: 'maquetLogo.png',
		_headerLogoCache: null,
		
		loadHeaderLogo: function() {
			var retval = -1;
			
			try {
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				retval = eventController.getNextEventId();
				
				if(this._headerLogoCache) {
					eventController.requestEventFiring(retval, this._headerLogoCache);
					return retval;
				}
				
				var innerCallback = function(options, success, response) {
					try {
						//the passed success parameter is unreliable - evaluate, if there is something inside the response!
						var success = response.responseBytes && response.responseBytes.length > 0;
					
						if(!success) {
							eventController.fireEvent(retval, undefined);
						} else {
							//get the image as data url
							//me._headerLogo = response.responseBytes;
							me._headerLogoCache = 'data:image/png;base64,' + btoa(String.fromCharCode.apply(null, response.responseBytes));
							eventController.fireEvent(retval, me._headerLogoCache);
						}
					} catch (ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadHeaderLogo of BaseOrderReportManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				Ext.Ajax.request({
		            url: 'resources/icons/' + this.HEADERLOGO_RESSOURCE,
		            callback: innerCallback,
		            binary: true,
		            scope: this
				});
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadHeaderLogo of BaseOrderReportManager', ex)
				retval = -1;
			}
			
			return retval;
		},
	
		loadHeaderTextContent: function(order) {
			var retval = -1;
		
		    try {
		        var tdName = 'Z_' + order.get('iwerk') + '_HEADER';
			    retval = AssetManagement.customer.manager.TextComponentManager.getTextComponent('TEXT', 'ADRS', tdName, AssetManagement.customer.core.Core.getAppConfig().getCurrentLanguageShort());
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadHeaderTextContent of BaseOrderReportManager', ex)
				retval = -1;
			}
			
			return retval;
		},
		
		loadFooterTextContent: function(order) {
			var retval = -1;
		
			try {
			    var tdName = 'Z_' + order.get('iwerk') + '_FOOTER';
			    retval = AssetManagement.customer.manager.TextComponentManager.getTextComponent('TEXT', 'ADRS', tdName, AssetManagement.customer.core.Core.getAppConfig().getCurrentLanguageShort());
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadFooterTextContent of BaseOrderReportManager', ex)
				retval = -1;
			}
			
			return retval;
		},
	
		loadPreparedObjectList: function(order) {
			var retval = -1;
		
			try {
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
			
				retval = eventController.getNextEventId();
				
				if(!order) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
			
				var objectList = order.get('objectList');
				
				if(!objectList || objectList.getCount() === 0) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var dataBaseRequests = 0;
				var completeCounter = 0;
				var errorOccured = false;
				var aborted = false;
				
				var preparedObjectList = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ObjectListItem',
					autoLoad: false
				});
				
				var requestReturned = function() {
					if(errorOccured === false && completeCounter === dataBaseRequests) {
						eventController.fireEvent(retval, preparedObjectList);
					} else if(errorOccured === true && aborted === false) {
						aborted = true;
						eventController.fireEvent(retval, undefined);
					}
				};
				
				objectList.each(function(objectListItem) {
					var clone = Ext.create('AssetManagement.customer.model.bo.ObjectListItem', {
					    equnr: objectListItem.get('equnr'),
					    eqtxt: objectListItem.get('eqtxt'),
					    matnr: objectListItem.get('matnr'),
					    maktx: objectListItem.get('maktx'),
                        ihnum: objectListItem.get('ihnum'),
					    vornr: ''
					});
					
					preparedObjectList.add(clone);
				
					var equnr = clone.get('equnr');
					var matnr = clone.get('matnr');
					var maktx = clone.get('maktx');
					var qmnumOfOli = objectListItem.get('ihnum');
					
					var loadEquiDataFromNotif = false;
					var loadMatDataFromNotif = false;
					
					//get the connected operation
					var operations = order.get('operations');
					
					if(operations && operations.getCount() > 0) {
						var obknr = objectListItem.get('obknr').trim();
						var obznr = objectListItem.get('obzae').trim();
					
						operations.each(function(operation) {
							if(operation.get('obknr') === obknr && operation.get('obzae') === obzae) {
								clone.set('vornr', operation.get('vornr'));
								return false;
							}
						});
					}
					
					//callback for database request on notif dependend information
					var notifDataCallback = function(notif) {
						try {
							errorOccured = notif === undefined;
								
							if(notif) {
								if(loadEquiDataFromNotif === true) {
									clone.set('equnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(notif.get('equnr')));
								}
								
								if(loadMatDataFromNotif === true) {
									clone.set('matnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(notif.get('matnr')));
									clone.set('maktx', notif.get('maktx'));
								}
							}
							
							completeCounter++;
							requestReturned();
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedObjectList of BaseOrderReportManager', ex);
							errorOccured = true;
							requestReturned();
						}
					};
					
					//callback for material shorttext databse request
					var maktxDataCallback = function(material) {
						try {
							errorOccured = material === undefined;
								
							if(material) {
								clone.set('maktx', material.get('maktx'));
							}
							
							completeCounter++;
							requestReturned();
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedObjectList of BaseOrderReportManager', ex);
							errorOccured = true;
							requestReturned();
						}
					};
						
					//get equnr
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
						clone.set('equnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(equnr));
					} else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnumOfOli)) {
						//try to get the equnr from the notif
						loadEquiDataFromNotif = true;
						dataBaseRequests++;
						
						var notifRequestId = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnumOfOli, true);
						eventController.registerOnEventForOneTime(notifRequestId, notifDataCallback);
					}
					
					//get material infos
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
						clone.set('matnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(matnr));
						
						//check, if there is any material shorttext given
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(maktx)) {
							//get it from the database
							dataBaseRequests++;
							
							var maktxRequestId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr, true);
							eventController.registerOnEventForOneTime(maktxRequestId, maktxDataCallback);
						}
					} else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnumOfOli)) {
						//try to get the material data from the notif
						loadMatDataFromNotif = true;
						
						//only 
						if(loadEquiDataFromNotif === false) {
							dataBaseRequests++;
							
							var notifRequestId = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnumOfOli, true);
							eventController.registerOnEventForOneTime(notifRequestId, notifDataCallback);
						}
					}
				});
				
				if(dataBaseRequests === 0) {
					eventController.requestEventFiring(retval, preparedObjectList);
				} else {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedObjectList of BaseOrderReportManager', ex)
				retval = -1;
			}
			
			return retval;
		},
		
		loadPreparedTimeConfs: function(order) {
			var retval = -1;
		
		    try {
		        //ext_scen is customer specific!!
			    // if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ext_scen_active') === 'X') {
			    //	retval = this.loadPreparedTimeConfsForExternals(order);
			    // }
			   // else {
					var me = this;
					var eventController = AssetManagement.customer.controller.EventController.getInstance();
					
					retval = eventController.getNextEventId();
					
					if(!order) {
						eventController.requestEventFiring(retval, null);
						return retval;
					}
					
					var timeConfs = null;
					var operations = order.get('operations');
				
					if(operations && operations.getCount() > 0) {
						//extract timeConfs
						var arrayOfTimeConfStores = [];
						
						operations.each(function(operation) {
							arrayOfTimeConfStores.push(operation.get('timeConfs'));
						});
						
						timeConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfTimeConfStores);
					}
					
					if(!timeConfs || timeConfs.getCount() === 0) {
						eventController.requestEventFiring(retval, null);
						return retval;
					}
					
					var bemotAssignmentCallback = function(bemots) {
						try {
							if(bemots === undefined) {
								eventController.fireEvent(retval, undefined);
								return;
							}
						
							var preparedTimeConfs = null;
		
							if(bemots) {
								var preparedTimeConfs = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.TimeConf',
									autoLoad: false
								});
								
								timeConfs.each(function(timeConf) {
								    var clone = Ext.create('AssetManagement.customer.model.bo.TimeConf', {
								        vornr: timeConf.get('vornr'),
								        ismne: timeConf.get('ismne'),
								        ismnw: timeConf.get('ismnw'),
								        bemot: timeConf.get('bemot'),
								        learr: timeConf.get('learr'),
								        activityType: timeConf.get('activityType'),
								        ltxa1: timeConf.get('ltxa1'),
								        ied: timeConf.get('ied'),
								        isd: timeConf.get('isd'),
                                        ersda: timeConf.get('ersda')
									});
									
									preparedTimeConfs.add(clone);
									
									bemots.each(function(bemot) {
										if(bemot.get('bemot') === timeConf.get('bemot')) {
											clone.set('bemotRef', bemot);
											return false;
										}
									}, this);
								});
							} else {
								preparedTimeConfs = timeConfs;
							}
							
							eventController.fireEvent(retval, preparedTimeConfs);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfs of BaseOrderReportManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
					
					var bemotRequestId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(false);
					eventController.registerOnEventForOneTime(bemotRequestId, bemotAssignmentCallback);
				//}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfs of BaseOrderReportManager', ex)
				retval = -1;
			}
			
			return retval;
		},
		
		loadPreparedTimeConfsForExternals: function(order) {
			var retval = -1;
				
			try {
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				retval = eventController.getNextEventId();
				
				if(!order) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				//external users does not use time confs, but banfs for confirmations
				var banfsSuccessCallback = function(banfItems) {
					try {
						if(banfItems && banfItems.getCount() > 0) {
							//map the banf positions into timeconf objects for the orderreport
							var preparedTimeConfs = Ext.create('Ext.data.Store', {
								model: 'AssetManagement.customer.model.bo.TimeConf',
								autoLoad: false
							});
							
							banfItems.each(function(banfItem) {
								var mapped = Ext.create('AssetManagement.customer.model.bo.TimeConf', {
								    //vornr: timeConf.get('vornr'),
								    ismne: banfItem.get('meins'),
								    ismnw: banfItem.get('menge'),
								    //bemot: timeConf.get('bemot'),
								    learr: banfItem.get('matnr'),
								    ltxa1: banfItem.get('txz01'),
								    ied: banfItem.get('badat')
								});
								
								if(banfItem.get('material')) {
									var banfsMaterial = banfItem.get('material');
								
									mapped.set('activityType', Ext.create('AssetManagement.customer.model.bo.ActivityType', {
										lstar: banfsMaterial.get('matnr'),
										ktext: banfsMaterial.get('maktx')
									}));
								}
								
								preparedTimeConfs.add(mapped);
							}, this);
							
							eventController.fireEvent(retval, preparedTimeConfs);
						} else {
							eventController.fireEvent(retval, null);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfsForExternals of BaseOrderReportManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var banfRequest = AssetManagement.customer.manager.BanfManager.getBanfPositions(order.get('aufnr'));
				eventController.registerOnEventForOneTime(banfRequest, banfsSuccessCallback);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfsForExternals of BaseOrderReportManager', ex);
				retval = -1;
			}
			
			return retval;
		},

		loadPreparedGeneralTimeConfs: function (order) {
		    var retval = -1;

		    try {
		        var me = this;
		        var eventController = AssetManagement.customer.controller.EventController.getInstance();

		        retval = eventController.getNextEventId();

		        if (!order) {
		            eventController.requestEventFiring(retval, null);
		            return retval;
		        }

		        //get default act. group for timeconfs
		        var custActGroupGeneral = AssetManagment.model.bo.FuncPara.getInstance().get('general_exp_group');

		        //get all cust. act. group with act. types
		        var cust043List = AssetManagement.customer.manager.Cust_043Manager.getCust_043(true);

		        var custGeneralGroupList = null;

		        if (cust043List && cust043List.getCount > 0) {
		            cust043List.each(function (cust_043) {
		                if (cust_043.get('acgroup') === custActGroupGeneral) {
		                    var clone = Ext.create('AssetManagement.customer.model.bo.Cust_043', {
		                        scenario: cust_043.get('scenario'),
		                        acgroup: cust_043.get('acgroup'),
		                        actype: cust_043.get('actype')
		                    });

		                    custGeneralGroupList.add(clone);
		                }
		            }, this);
		        }

		        var preparedGeneralTimeConfs = null;

		        var timeConfs = null;
		        var operations = order.get('operations');

		        if (operations && operations.getCount() > 0) {
		            //extract timeConfs
		            var arrayOfTimeConfStores = [];

		            operations.each(function (operation) {
		                arrayOfTimeConfStores.push(operation.get('timeConfs'));
		            });

		            timeConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfTimeConfStores);
		        }

		        if (!timeConfs || timeConfs.getCount() === 0) {
		            eventController.requestEventFiring(retval, null);
		            return retval;
		        }

		        var bemotAssignmentCallback = function (bemots) {
		            try {
		                if (bemots === undefined) {
		                    eventController.fireEvent(retval, undefined);
		                    return;
		                }

		                var preparedTimeConfs = null;

		                if (bemots) {
		                    var preparedTimeConfs = Ext.create('Ext.data.Store', {
		                        model: 'AssetManagement.customer.model.bo.TimeConf',
		                        autoLoad: false
		                    });

		                    timeConfs.each(function (timeConf) {
		                        var clone = Ext.create('AssetManagement.customer.model.bo.TimeConf', {
		                            vornr: timeConf.get('vornr'),
		                            ismne: timeConf.get('ismne'),
		                            ismnw: timeConf.get('ismnw'),
		                            bemot: timeConf.get('bemot'),
		                            learr: timeConf.get('learr'),
		                            activityType: timeConf.get('activityType'),
		                            ltxa1: timeConf.get('ltxa1'),
		                            ied: timeConf.get('ied'),
		                            isd: timeConf.get('isd'),
		                            ersda: timeConf.get('ersda'),
		                            actType: timeConf.get('actype')
		                        });

		                        preparedTimeConfs.add(clone);

		                        bemots.each(function (bemot) {
		                            if (bemot.get('bemot') === timeConf.get('bemot')) {
		                                clone.set('bemotRef', bemot);
		                                return false;
		                            }
		                        }, this);
		                    });
		                } else {
		                    preparedTimeConfs = timeConfs;
		                }



		                if (preparedTimeConfs && preparedTimeConfs.getCount > 0) {
		                    preparedTimeConfs.each(function (timeConf) {
		                        custGeneralGroupList.each(function (cust_043) {
		                            if (timeConf.get('actype') === cust_043.get('actype')) {
		                                preparedGeneralTimeConfs.add(timeConf);
		                                return false;
		                            }
		                        }, this);
		                    }, this);
		                }

		                preparedTimeConfs = preparedGeneralTimeConfs;

		                eventController.fireEvent(retval, preparedTimeConfs);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfs of BaseOrderReportManager', ex);
		                eventController.fireEvent(retval, undefined);
		            }
		        };

		        var bemotRequestId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(false);
		        eventController.registerOnEventForOneTime(bemotRequestId, bemotAssignmentCallback);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfs of BaseOrderReportManager', ex)
		        retval = -1;
		    }

		    return retval;
		},
		
		loadPreparedMatConfs: function(order) {
			var retval = -1;
		
			try {
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				retval = eventController.getNextEventId();
				
				if(!order) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var matConfs = null;
				var operations = order.get('operations');
			
				if(operations && operations.getCount() > 0) {
					//extract matConfs
					var arrayOfMatConfStores = [];
					
					operations.each(function(operation) {
						arrayOfMatConfStores.push(operation.get('matConfs'));
					});
					
					matConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfMatConfStores);
				}
			
				if(!matConfs || matConfs.getCount() === 0) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var bemotAssignmentCallback = function(bemots) {
					try {
						if(bemots === undefined) {
							eventController.fireEvent(retval, undefined);
							return;
						}
					
						var preparedMatConfs = null;
	
						if(bemots) {
							var preparedMatConfs = Ext.create('Ext.data.Store', {
								model: 'AssetManagement.customer.model.bo.MaterialConf',
								autoLoad: false
							});
							
							matConfs.each(function(matConf) {
								var clone = Ext.create('AssetManagement.customer.model.bo.MaterialConf', {
									matnr: matConf.get('matnr'),
								    vornr: matConf.get('vornr'),
								    quantity: matConf.get('quantity'),
								    unit: matConf.get('unit'),
								    bemot: matConf.get('bemot'),
								    material: matConf.get('material'),
								    isdd: matConf.get('isdd')
								});
								
								preparedMatConfs.add(clone);
								
								bemots.each(function(bemot) {
									if(bemot.get('bemot') === matConf.get('bemot')) {
										clone.set('bemotRef', bemot);
										return false;
									}
								}, this);
							});
						} else {
							preparedMatConfs = matConfs;
						}
						
						eventController.fireEvent(retval, preparedMatConfs);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedMatConfs of BaseOrderReportManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var bemotRequestId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(false);
				eventController.registerOnEventForOneTime(bemotRequestId, bemotAssignmentCallback);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedMatConfs of BaseOrderReportManager', ex)
				retval = -1;
			}
			
			return retval;
		}
	}
});