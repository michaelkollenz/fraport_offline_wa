Ext.define('AssetManagement.base.manager.BaseDocUploadManager', {
	 extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.DocUpload',
		'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.FileHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

	inheritableStatics: {
		EVENTS: {
			DOCUPLOAD_ADDED: 'docUploadAdded',
			DOCUPLOAD_CHANGED: 'docUploadChanged',
			DOCUPLOAD_DELETED: 'docUploadDeleted'
		},

		//public methods
		//returns the data amount for files currently stored inside the database in bytes
		getSizeOfStoredFilesForUpload: function() {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				var sizeSum = 0;

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						if(!eventArgs || !eventArgs.target || !eventArgs.target.result) {
							eventController.fireEvent(retval, sizeSum);
						} else {
							var cursor = eventArgs.target.result;
						    if (cursor) {
						    	try {
						    		var fileSize = parseInt(cursor.value['DOC_SIZE']);
						    		sizeSum += fileSize;
						    	} catch(ex) {
						    	}

						    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						    }
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSizeOfStoredFilesForUpload of BaseDocUploadManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				//use doctype index, to get all (user independend) records
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('S_DOCUPLOAD', 'DOCTYPE', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('S_DOCUPLOAD', 'DIRTY', indexRange, successCallback, null, false);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSizeOfStoredFilesForUpload of BaseDocUploadManager', ex);
			}

			return retval;
		},

		//returns all doc upload records for a specific type - if left empty all records will be returned
		//caution: user independend
		getDocUploads: function(fileType, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				var docUploads = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.DocUpload',
					autoLoad: false
				});

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

						me.buildDocUploadsStoreFromDataBaseQuery.call(me, docUploads, eventArgs);

						if(done) {
							eventController.fireEvent(retval, docUploads);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocUploads of BaseDocUploadManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var indexMap = null;

				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fileType)) {
					indexMap = Ext.create('Ext.util.HashMap');
					indexMap.add('DOC_TYPE', fileType);
				}

				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('S_DOCUPLOAD', 'DOCTYPE', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('S_DOCUPLOAD', 'DOCTYPE', indexRange, successCallback, null, false);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocUploads of BaseDocUploadManager', ex);
			}

			return retval;
		},

	    //getter for files to be sent to SAP
	    //only files with update flag of I are included
		getDocUploadsForSync: function (fileType, useBatchProcessing) {
		    var retval = -1;

		    try {
		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        retval = eventController.getNextEventId();

		        var allDocUploadsCallback = function (allDocUploads) {
		            try {
		                var toReturn = allDocUploads === undefined ? undefined : null;

		                if (allDocUploads) {
		                    var docUploadsForSync = Ext.create('Ext.data.Store', {
		                        model: 'AssetManagement.customer.model.bo.DocUpload',
		                        autoLoad: false
		                    });

		                    if (allDocUploads.getCount() > 0) {
		                        allDocUploads.each(function (docUpload) {
		                            if (docUpload.get('updFlag') === 'I') {
		                                docUploadsForSync.add(docUpload);
		                            }
		                        });
		                    }

		                    toReturn = docUploadsForSync;
		                }

		                eventController.requestEventFiring(retval, toReturn);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocUploadsForSync of BaseDocUploadManager', ex);
		                eventController.requestEventFiring(retval, undefined);
		            }
		        };

		        var getAllId = this.getDocUploads(fileType, useBatchProcessing);

		        if (getAllId > -1) {
		            eventController.registerOnEventForOneTime(getAllId, allDocUploadsCallback);

		            retval = eventController.getNextEventId();
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocUploadsForSync of BaseDocUploadManager', ex);
		    }

		    return retval;
		},

		getDocUploadsForObject: function(objnr, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var keyArray = this.getRefTypeAndRefObjectKeys(objnr);
				var refType = keyArray[0];
				var refObject = keyArray[1];

				if(keyArray.length !== 2 || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(refType) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(refObject)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var docUploads = this.getFromCache(refType + refObject);

				if(docUploads) {
					eventController.requestEventFiring(retval, docUploads);
					return retval;
				}

				docUploads = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.DocUpload',
					autoLoad: false
				});

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

						me.buildDocUploadsStoreFromDataBaseQuery.call(me, docUploads, eventArgs);

						if(done) {
							me.addToCache(refType + refObject, docUploads);
							eventController.fireEvent(retval, docUploads);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocUploadsForObject of BaseDocUploadManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('REF_TYPE', refType);
				indexMap.add('REF_OBJECT', refObject);

				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('S_DOCUPLOAD', 'REFTYPE-REFOBJECT', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('S_DOCUPLOAD', 'REFTYPE-REFOBJECT', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocUploadsForObject of BaseDocUploadManager', ex);
			}

			return retval;
		},

		//deletes a docUpload record from the database
		deleteDocUpload: function(docUpload, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!docUpload) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

				var me = this;

				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";

						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(docUpload);	//includes removing from cache
							eventController.fireEvent(me.EVENTS.DOCUPLOAD_DELETED, docUpload);
						}

						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUpload of BaseDocUploadManager', ex);
						eventController.fireEvent(retval, false);
					}
				};

				//get the key of the docUpload to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('S_DOCUPLOAD', docUpload);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('S_DOCUPLOAD', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUpload of BaseDocUploadManager', ex);
			}

			return retval;
		},

		//will try to open the content of the doc uploads record
		openDocument: function(docUpload) {
			try {
				AssetManagement.customer.helper.FileHelper.openFile(docUpload);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openDocument of BaseDocUploadManager', ex);
			}
		},

		//will try to generate a docupload for the passed order out of the passed file
		//if this succeeds, a new docUpload record will be added to the orders documents
		addNewDocUploadForOrder: function(file, order) {
			var retval = -1;

			try {
				if(!order || !file) {
					return retval;
				}

				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				var me = this;

				//create a new docupload record from file
				var docUpload = me.generateDocUploadFromFile(file);

				if(!docUpload) {
					eventController.requestEventFiring(retval, undefined);;
				}

				docUpload.set('refType', 'OR');
				docUpload.set('refObject', order.get('aufnr'));
				docUpload.set('mobileKey', order.get('mobileKey'));

				var newDesc = me.generateDescriptionForDocUpload(docUpload);

				docUpload.set('description', newDesc);
				docUpload.set('fileDescription', newDesc);

				//email fields currently missing in appconfig
				//docUpload.set('email', AssetManagement.customer.core.Core.getAppConfig().getEmail1(););

				//save it to the database
				//register on the save event, if it returns success fire returned event with doc upload record
				var saveCallback = function(success) {
					try {
						if(success === true) {
							var ordersDocuments = order.get('documents');

							if(!ordersDocuments) {
								ordersDocuments = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.File',
									autoLoad: false
								});

								order.set('documents', ordersDocuments);
							}

							ordersDocuments.add(docUpload);

							eventController.fireEvent(retval, docUpload);
						} else {
							eventController.fireEvent(retval, undefined);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForOrder of BaseDocUploadManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var saveEvent = me.saveDocUpload(docUpload);

				if(saveEvent > 0)
					eventController.registerOnEventForOneTime(saveEvent, saveCallback);
				else
					eventController.requestEventFiring(retval, undefined);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForOrder of BaseDocUploadManager', ex);
				retval = -1;
			}

			return retval;
		},

		//works the analogue as addNewDocUploadForOrder
		addNewDocUploadForNotif: function(file, notif) {
			var retval = -1;

			try {
				if(!file || !notif) {
					return retval;
				}

				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				var me = this;

				//create a new docupload record
				var docUpload = me.generateDocUploadFromFile(file);

				if(!docUpload) {
					eventController.requestEventFiring(retval, undefined);
					return;
				}

				docUpload.set('refType', 'NO');
				docUpload.set('refObject', notif.get('qmnum'));
				docUpload.set('mobileKey', notif.get('mobileKey'));

				docUpload.set('description', me.generateDescriptionForDocUpload(docUpload));
				docUpload.set('fileDescription', me.generateDescriptionForDocUpload(docUpload));

				//email fields currently missing in appconfig
				//docUpload.set('email', AssetManagement.customer.core.Core.getAppConfig().getEmail1(););

				//save it to the database
				//register on the save event, if it returns success fire returned event with doc upload record
				var saveCallback = function(success) {
					try {
						if(success === true) {
							var notifsDocuments = notif.get('documents');

							if(!notifsDocuments) {
								notifsDocuments = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.File',
									autoLoad: false
								});

								notif.set('documents', notifsDocuments);
							}

							notifsDocuments.add(docUpload);

							eventController.fireEvent(retval, docUpload);
						} else {
							eventController.fireEvent(retval, undefined);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForNotif of BaseDocUploadManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var saveEvent = me.saveDocUpload(docUpload);

				if(saveEvent > 0)
					eventController.registerOnEventForOneTime(saveEvent, saveCallback);
				else
					eventController.requestEventFiring(retval, undefined);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForNotif of BaseDocUploadManager', ex);
				retval = -1;
			}

			return retval;
		},

		//works the analogue as addNewDocUploadForOrder
		addNewDocUploadForEqui: function(file, equi) {
			var retval = -1;

			try {
				if(!file || !equi) {
					return retval;
				}

				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				var me = this;

				//create a new docupload record
				var docUpload = me.generateDocUploadFromFile(file);

				if(!docUpload) {
					eventController.requestEventFiring(retval, undefined);
					return;
				}

				docUpload.set('refType', 'EQ');
				docUpload.set('refObject', equi.get('equnr'));
				docUpload.set('mobileKey', equi.get('mobileKey'));

				docUpload.set('description', me.generateDescriptionForDocUpload(docUpload));
				docUpload.set('fileDescription', me.generateDescriptionForDocUpload(docUpload));

				//email fields currently missing in appconfig
				//docUpload.set('email', AssetManagement.customer.core.Core.getAppConfig().getEmail1(););

				//save it to the database
				//register on the save event, if it returns success fire returned event with doc upload record
				var saveCallback = function(success) {
					try {
						if(success === true) {
							var equisDocuments = equi.get('documents');

							if(!equisDocuments) {
								equisDocuments = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.File',
									autoLoad: false
								});

								equi.set('documents', equisDocuments);
							}

							equisDocuments.add(docUpload);

							eventController.fireEvent(retval, docUpload);
						} else {
							eventController.fireEvent(retval, undefined);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForEqui of BaseDocUploadManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var saveEvent = me.saveDocUpload(docUpload);

				if(saveEvent > 0)
					eventController.registerOnEventForOneTime(saveEvent, saveCallback);
				else
					eventController.requestEventFiring(retval, undefined);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForEqui of BaseDocUploadManager', ex);
				retval = -1;
			}

			return retval;
		},

		//works the analogue as addNewDocUploadForOrder
		addNewDocUploadForFuncLoc: function(file, funcLoc) {
			var retval = -1;

			try {
				if(!file || !funcLoc) {
					return retval;
				}

				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				var me = this;

				//create a new docupload record
				var docUpload = me.generateDocUploadFromFile(file);

				if(!docUpload) {
					eventController.requestEventFiring(retval, undefined);
					return;
				}

				docUpload.set('refType', 'IF');
				docUpload.set('refObject', funcLoc.get('tplnr'));
				docUpload.set('mobileKey', funcLoc.get('mobileKey'));

				docUpload.set('description', me.generateDescriptionForDocUpload(docUpload));
				docUpload.set('fileDescription', me.generateDescriptionForDocUpload(docUpload));

				//email fields currently missing in appconfig
				//docUpload.set('email', AssetManagement.customer.core.Core.getAppConfig().getEmail1(););

				//save it to the database
				//register on the save event, if it returns success fire returned event with doc upload record
				var saveCallback = function(success) {
					try {
						if(success === true) {
							var funcLocsDocuments = funcLoc.get('documents');

							if(!funcLocsDocuments) {
								funcLocsDocuments = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.File',
									autoLoad: false
								});

								funcLoc.set('documents', funcLocsDocuments);
							}

							funcLocsDocuments.add(docUpload);

							eventController.fireEvent(retval, docUpload);
						} else {
							eventController.fireEvent(retval, undefined);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForFuncLoc of BaseDocUploadManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var saveEvent = me.saveDocUpload(docUpload);

				if(saveEvent > 0)
					eventController.registerOnEventForOneTime(saveEvent, saveCallback);
				else
					eventController.requestEventFiring(retval, undefined);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewDocUploadForFuncLoc of BaseDocUploadManager', ex);
				retval = -1;
			}

			return retval;
		},

    //will add an order report to an order (including saving to the database)
    //when done, it will fire an event with the generated order report as docUpload
    addOrderReport: function(reportAsFile, order, customerEmail) {
      var retval = -1;

      try {
        if (!order || !reportAsFile) {
          return retval;
        }

        var eventController = AssetManagement.customer.controller.EventController.getInstance();
        var retval = eventController.getNextEventId();

        var me = this;

        //create a new docupload record & save it to the database
        var docUpload = me.generateDocUploadFromFile(reportAsFile);

        if(!docUpload) {
          eventController.requestEventFiring(retval, undefined);
          return;
        }

        docUpload.set('ordReport', 'X');

        docUpload.set('refType', 'OR');
        docUpload.set('refObject', order.get('aufnr'));
        docUpload.set('mobileKey', order.get('mobileKey'));

        //set the email address field, if provided
        if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customerEmail))
          docUpload.set('email', customerEmail);

        //register on the save event, if it returns success fire returned event with doc upload record
        var saveCallback = function(success) {
          try {
            if(success === true) {
              var ordersDocuments = order.get('documents');

              if(!ordersDocuments) {
                ordersDocuments = Ext.create('Ext.data.Store', {
                  model: 'AssetManagement.customer.model.bo.File',
                  autoLoad: false
                });

                order.set('documents', ordersDocuments);
              }

              ordersDocuments.add(docUpload);

              eventController.fireEvent(retval, docUpload);
            } else {
              eventController.fireEvent(retval, undefined);
            }
          } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addOrderReport of BaseDocUploadManager', ex);
            eventController.requestEventFiring(retval, undefined);
          }
        };

        var saveEvent = me.saveDocUpload(docUpload);

        if(saveEvent > 0)
          eventController.registerOnEventForOneTime(saveEvent, saveCallback);
        else
          eventController.requestEventFiring(retval, undefined);

      } catch(ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addOrderReport of BaseDocUploadManager', ex);
        eventController.requestEventFiring(retval, undefined);
      }

      return retval;
    },


    //will add an order report to an order (including saving to the database)
    //when done, it will fire an event with the generated order report as docUpload
    addDeliveryReport: function(reportAsFile, delivery, customerEmail) {
      var retval = -1;

      try {
        if (!delivery || !reportAsFile) {
          return retval;
        }

        var eventController = AssetManagement.customer.controller.EventController.getInstance();
        var retval = eventController.getNextEventId();

        var me = this;

        //create a new docupload record & save it to the database
        var docUpload = me.generateDocUploadFromFile(reportAsFile);

        if(!docUpload) {
          eventController.requestEventFiring(retval, undefined);
          return;
        }

        docUpload.set('ordReport', 'X');

        docUpload.set('refType', 'OR');
        docUpload.set('refObject', delivery.data.deliveryStore.data.items[0].get('gebnr'));
        docUpload.set('mobileKey', delivery.data.deliveryStore.data.items[0].get('mobilekey'));

        //set the email address field, if provided
        if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customerEmail))
          docUpload.set('email', customerEmail);

        //register on the save event, if it returns success fire returned event with doc upload record
        var saveCallback = function(success) {
          try {
            if(success === true) { //TODO check if necessary
              // var ordersDocuments = order.get('documents');
              //
              // if(!ordersDocuments) {
              //   ordersDocuments = Ext.create('Ext.data.Store', {
              //     model: 'AssetManagement.customer.model.bo.File',
              //     autoLoad: false
              //   });
              //
              //   order.set('documents', ordersDocuments);
              // }
              //
              // ordersDocuments.add(docUpload);

              eventController.fireEvent(retval, docUpload);
            } else {
              eventController.fireEvent(retval, undefined);
            }
          } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addOrderReport of BaseDocUploadManager', ex);
            eventController.requestEventFiring(retval, undefined);
          }
        };

        var saveEvent = me.saveDocUpload(docUpload);

        if(saveEvent > 0)
          eventController.registerOnEventForOneTime(saveEvent, saveCallback);
        else
          eventController.requestEventFiring(retval, undefined);

      } catch(ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addOrderReport of BaseDocUploadManager', ex);
        eventController.requestEventFiring(retval, undefined);
      }

      return retval;
    },

		//private methods
		getRefTypeAndRefObjectKeys: function(objnr) {
			var retval = new Array();

			try {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr) && objnr.length > 3) {
					var type = objnr.substring(0, 2);
					var key = objnr.substring(2);

					if(type === 'QM')
						type = 'NO';
					else if(type === 'IE')
						type = 'EQ';

					retval.push(type);
					retval.push(key);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRefTypeAndRefObjectKeys of BaseDocUploadManager', ex);
			}

			return retval;
		},

		buildDocUploadsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
					    var docUpload = this.buildDocUploadFromDbResultObject(cursor.value);
						store.add(docUpload);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDocUploadsStoreFromDataBaseQuery of BaseDocUploadManager', ex);
			}
		},

		buildDocUploadFromDbResultObject: function(dbResult) {
	        var date = null;
			var retval = null;

		    try {
		    	date = AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['TIMESTAMP']);

			    retval = Ext.create('AssetManagement.customer.model.bo.DocUpload', {
			    	//base class properties (file)
					fileId: 'local' + dbResult['ID'],
					fileName: dbResult['FILENAME'],
					fileType: dbResult['DOC_TYPE'],
					fileOrigin: 'local',
					fileDescription: dbResult['DESCRIPTION'],
					createdAt: date,
					lastModified: date,

					//own properties
					userId: dbResult['USERID'], // neccessary for upload procedure
					scenario: dbResult['SCENARIO'], // neccessary for upload procedure
					docId: dbResult['ID'],
					refType: dbResult['REF_TYPE'],
					refObject: dbResult['REF_OBJECT'],
					docType: dbResult['DOC_TYPE'],
					docSize: dbResult['DOC_SIZE'],
					description: dbResult['DESCRIPTION'],
					ordReport: dbResult['ORD_REPORT'],
					spras: dbResult['SPRAS'],
					filename: dbResult['FILENAME'],
					email: dbResult['EMAIL'],
					recordname: dbResult['RECORDNAME'],
					recordnameBin: dbResult['RECORDNAME_BIN'],
					orgFilename: dbResult['ORG_FILENAME'],

					timestamp: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['TIMESTAMP']),
					syncTimestamp: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['SYNCTIMESTAMP']),

				    updFlag: dbResult['UPDFLAG'],
				    mobileKey: dbResult['MOBILEKEY']

			    });
			    retval.set('id', dbResult['ID']);

			    //set the file size in kB
			    var fileSize = parseInt(dbResult['DOC_SIZE']);

			    if(fileSize !== NaN) {
				   retval.set('fileSize', fileSize / 1024.0)
			    }

			    //set the file content
			    var contentAsArrayBuffer = null;
			    if(!AssetManagement.customer.helper.DataBaseHelper.usesNativeIndexedDbAPI()) {
			    	//content has to be decoded from it's ascii representation back to array buffer, because websql is not capable of storing array buffers
			        contentAsArrayBuffer = AssetManagement.customer.helper.FileHelper.decodeArrayBufferFromAsciiString(dbResult['CONTENT']);
			    } else {
			    	contentAsArrayBuffer = dbResult['CONTENT'];
			    }

			    retval.set('contentAsArrayBuffer', contentAsArrayBuffer);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDocUploadFromDbResultObject of BaseDocUploadManager', ex);
			}

			return retval;
		},

		generateDocUploadFromFile: function(file) {
			var retval = null;

			try {
				if(file) {
				    var locale = AssetManagement.customer.core.Core.getAppConfig().getLocale();
				    var language = locale ? locale.charAt(0).toUpperCase() : 'E';

				    var retval = Ext.create('AssetManagement.customer.model.bo.DocUpload', {
				        fileName: file.get('fileName'),
				        fileType: file.get('fileType'),
				        fileSize: file.get('fileSize'),
				        fileOrigin: file.get('fileOrigin'),
				        fileDescription: file.get('fileDescription'),
				        createdAt: file.get('createdAt'),
				        lastModified: file.get('lastModified'),
				        contentAsArrayBuffer: file.get('contentAsArrayBuffer'),

				        //own properties
				        docId: '',
				        docType: file.get('fileType'),
				        docSize: file.get('fileSize') * 1024,
				        description: file.get('fileDescription'),
				        ordReport: '',
				        spras: language,
				        filename: file.get('fileName'),
				        recordname: 'DOCH',
				        recordnameBin: 'AM_DOCD_BIN',
				        orgFilename: file.get('fileName'),

				        timestamp: file.get('lastModified')
				    });
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDocUploadFromFileHandle of BaseDocUploadManager', ex);
				retval = null;
			}

			return retval;
		},

		generateDescriptionForDocUpload: function(docUpload) {
			var retval = '';

			try {
				//build a description
				var description = '';
				var type = docUpload.get('fileType');

				type = type.toLowerCase();

				if('jpg' === type || 'bmp' === type || 'gif' === type || 'png' === type) {
					description += Locale.getMsg('image');
				} else {
					description += Locale.getMsg('file');
				}

				description += '_';

				if(docUpload.get('refType') === 'IF')
					description += docUpload.get('refObject');
				else
					description += AssetManagement.customer.utils.StringUtils.trimStart(docUpload.get('refObject'), '0');

				description += '_';

				description += docUpload.get('orgFilename');

				retval = description;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDescriptionForDocUpload of BaseDocUploadManager', ex);
				retval = '';
			}

			return retval;
		},

		saveDocUpload: function(docUpload) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!docUpload) {
					eventController.requestEventFiring(retval, false);
					return;
				}

				var priorUpdateFlag = docUpload.get('updFlag');
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;

				//update flag management
				this.manageUpdateFlagForSaving(docUpload);

				//check if the docUpload already has an id value
				var requiresNewId = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(docUpload.get('docId'));

				var me = this;
				var saveFunction = function(nextIdValue) {
					try {
						if(requiresNewId && nextIdValue !== -1) {
							docUpload.set('fileId', 'local' + nextIdValue);
							docUpload.set('docId', nextIdValue);
						}

						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(docUpload.get('docId'))) {
							eventController.fireEvent(retval, false);
							return;
						}

						//set the id
						docUpload.set('id', docUpload.get('docId'));

						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";

								if(success) {
									me.addSingleRecordToCache(docUpload);

									var eventType = isInsert ? me.EVENTS.DOCUPLOAD_ADDED : me.EVENTS.DOCUPLOAD_CHANGED;

									eventController.fireEvent(eventType, docUpload);
								}

								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDocUpload of BaseDocUploadManager', ex);
								eventController.fireEvent(retval, false);
							}
						};

						var toSave = me.buildDataBaseObjectForDocUpload(docUpload);

						//if it is an insert perform an insert
						if(isInsert) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('S_DOCUPLOAD', toSave, callback, callback);
						} else {
							//else it is an update
							AssetManagement.customer.core.Core.getDataBaseHelper().update('S_DOCUPLOAD', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDocUpload of BaseDocUploadManager', ex);
					}
				};

				if(!requiresNewId) {
					saveFunction();
				} else {
					eventId = this.getNextId();

					if(eventId > 0)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDocUpload of BaseDocUploadManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, false);
			}

			return retval;
		},

		//builds the raw object to store in the database out of a doc upload record
		buildDataBaseObjectForDocUpload: function(docUpload) {
			var retval = { };

			try {
				retval['ID'] = docUpload.get('docId');
				retval['REF_TYPE'] = docUpload.get('refType');
				retval['REF_OBJECT'] = docUpload.get('refObject');
				retval['DOC_TYPE'] = docUpload.get('docType');
				retval['DOC_SIZE'] = docUpload.get('docSize');
				retval['DESCRIPTION'] = docUpload.get('description');
				retval['ORD_REPORT'] = docUpload.get('ordReport');
				retval['SPRAS'] = docUpload.get('spras');
				retval['FILENAME'] = docUpload.get('filename');
				retval['EMAIL'] = docUpload.get('email');
				retval['RECORDNAME'] = docUpload.get('recordname');
				retval['RECORDNAME_BIN'] = docUpload.get('recordnameBin');
				retval['ORG_FILENAME'] = docUpload.get('orgFilename');

				if(!AssetManagement.customer.helper.DataBaseHelper.usesNativeIndexedDbAPI()) {
					//content has to be put as string, because websql is not capable of storing array buffers - string will be encoded with ascii
				    retval['CONTENT'] = AssetManagement.customer.helper.FileHelper.encodeArrayBufferAsAsciiString(docUpload.get('contentAsArrayBuffer'));
				} else {
					retval['CONTENT'] = docUpload.get('contentAsArrayBuffer');
				}

				retval['TIMESTAMP'] = AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(docUpload.get('timestamp'));
				retval['SYNCTIMESTAMP'] = AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(docUpload.get('syncTimestamp'));

				retval['MOBILEKEY'] = docUpload.get('mobileKey');
				retval['UPDFLAG'] = docUpload.get('updFlag');
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForDocUpload of BaseDocUploadManager', ex);
		    }

			return retval;
		},

		getNextId: function() {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				var maxId = 0;

				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;

							var curId = parseInt(cursor.value['ID']);

							if(!isNaN(curId)) {
								if(curId > maxId)
									maxId = curId;
							}

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
							eventController.fireEvent(retval, maxId + 1 + '');
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextId of BaseDocUploadManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};


                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('S_DOCUPLOAD', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_DOCUPLOAD', keyRange, successCallback, null, null);

            } catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextId of BaseDocUploadManager', ex);
			}

			return retval;
		},

	    //transforms an array buffer into an extended ascii representation
		encodeArrayBufferAsAsciiString: function (arrayBuffer) {
		    var retval = '';

		    try {
		        if (arrayBuffer && arrayBuffer.byteLength > 0) {
		            var bufView = new Uint8Array(arrayBuffer);

		            for (var i = 0; i < bufView.length; i++) {
		                var uint8 = bufView[i];

		                retval += String.fromCharCode(uint8);
		            }
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside encodeArrayBufferAsAsciiString of BaseDocUploadManager', ex);
		        retval = null;
		    }

		    return retval;
		},

	    //return value will be an uint 8 array - works with ascii strings only
	    //null will be returned, if any char with charcode larger than 255 is encountered
		decodeArrayBufferFromAsciiString: function (stringToDecode) {
		    var retval = null;

		    try {
		        if (typeof (stringToDecode) === 'string' && stringToDecode.length > 0) {
		            var arrayBuffer = new ArrayBuffer(stringToDecode.length);
		            var view = new Uint8Array(arrayBuffer);

		            var cancelled = false;

		            for (var i = 0, strLen = stringToDecode.length; i < strLen; i++) {
		                var charCode = stringToDecode.charCodeAt(i);

		                if (charCode < 256) {
		                    view[i] = charCode;
		                } else {
		                    cancelled = false;
		                    break;
		                }
		            }

		            if (!cancelled)
		                retval = arrayBuffer;
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside decodeArrayBufferFromAsciiString of BaseDocUploadManager', ex);
		        retval = null;
		    }

		    return retval;
		},

		//cache administration
		_docUploadsCache: null,

		getCache: function() {
	        try {
	        	if(!this._docUploadsCache) {
					this._docUploadsCache = Ext.create('Ext.util.HashMap');

					this.initializeCacheHandling();
				}
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseDocUploadManager', ex);
		    }

			return this._docUploadsCache;
		},

		//public methods
		clearCache: function() {
			try {
			    this.getCache().clear();
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseDocUploadManager', ex);
		    }
		},

		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseDocUploadManager', ex);
			}
		},

		addToCache: function(key, docUploadsStore) {
			try {
				var temp = this.getCache().add(key, docUploadsStore);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseDocUploadManager', ex);
			}
		},

		addSingleRecordToCache: function(docUpload) {
			try {
				if(!docUpload || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(docUpload.get('refType'))
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(docUpload.get('refObject'))) {
					return;
				}

				var key = docUpload.get('refType') + docUpload.get('refObject');

				var temp = this.getFromCache(key);

				if(temp) {
					temp.add(docUpload);
				} else {
					//create a new store
					var newStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.DocUpload',
						autoLoad: false
					});

					newStore.add(docUpload);

					this.addToCache(key, newStore);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseDocUploadManager', ex);
			}
		},

		getFromCache: function(key) {
	        var retval = null;

	        try {
			    retval = this.getCache().get(key);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseDocUploadManager', ex);
			}

			return retval;
		}
	}
});