Ext.define('AssetManagement.base.manager.BaseMatConfManager', {
	 extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.MaterialConf',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.manager.CustBemotManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		EVENTS: {
			MATCONF_ADDED: 'matConfAdded',
			MATCONF_CHANGED: 'matConfChanged',
			MATCONF_DELETED: 'matConfDeleted'
		},
	
	    //public methods
		getAllMatConfs: function (withDependendData, useBatchProcessing) {
		    var retval = -1;

		    try {
		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        retval = eventController.getNextEventId();

		        var allMatConfs = Ext.create('Ext.data.Store', {
		            model: 'AssetManagement.customer.model.bo.MaterialConf',
		            autoLoad: false
		        });

		        var me = this;
		        var successCallback = function (eventArgs) {
		            try {
		                var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

		                me.buildMatConfStoreFromDataBaseQuery.call(me, allMatConfs, eventArgs);

		                if (done) {
		                    if (!withDependendData) {
		                        eventController.requestEventFiring(retval, allMatConfs);
		                    } else {
		                        me.loadDependendDataForMatConfs(allMatConfs, retval);
		                    }
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMatConfs of BaseMatConfManager', ex);
		                eventController.requestEventFiring(retval, undefined);
		            }
		        };

		        var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MATCONF', null);
		        AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MATCONF', keyRange, successCallback, null, useBatchProcessing);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMatConfs of BaseMatConfManager', ex);
		        retval = -1;
		    }

		    return retval;
		},

		getMatConfsForOrder: function(aufnr, useBatchProcessing) {
			var retval = -1;
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.MaterialConf',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildMatConfStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							me.loadDependendDataForMatConfs.call(me, theStore, retval);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatConfsForOrder of BaseMatConfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', aufnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MATCONF', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MATCONF', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatConfsForOrder of BaseMatConfManager', ex);
			}
	
			return retval;
		},
		
		//save a matconf - if it is neccessary it will get a new counter first
		saveMatConf: function(matConf) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!matConf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				//update flag management
				this.manageUpdateFlagForSaving(matConf);
				
				//check if the matconf already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matConf.get('mblnr'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1)
							matConf.set('mblnr', nextCounterValue);
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matConf.get('mblnr'))) {
							eventController.fireEvent(retval, false);
							return;
						}
						
						//set the id
						matConf.set('id', matConf.get('aufnr') + matConf.get('mblnr') + matConf.get('mjahr'));
						
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								
								if(success) {
									var eventType = me.EVENTS.MATCONF_ADDED;
									eventController.fireEvent(eventType, matConf);
								}
								
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveMatConf of BaseMatConfManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForMatConf(matConf);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_MATCONF', toSave, callback, callback);
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveMatConf of BaseMatConfManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextMblnr(matConf);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveMatConf of BaseMatConfManager', ex);
			}
			
			return retval;
		},
		
		deleteMatConf: function(matConf, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!matConf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				var me  = this;
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(matConf);
							eventController.fireEvent(me.EVENTS.MATCONF_DELETED, matConf);
						}
						
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConf of BaseMatConfManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the matConf to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_MATCONF', matConf);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_MATCONF', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConf of BaseMatConfManager', ex);
			}
			
			return retval;
		},
	
		//private methods
		loadDependendDataForMatConfs: function(matConfs, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(matConfs.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, matConfs);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var materials = null;
				var accountIndications = null;
	
				var pendingRequests = 0;			
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccured === false) {
						me.assignDependendDataForMatConfs.call(me, eventIdToFireWhenComplete, matConfs, materials, accountIndications);
					}
				};
				
				//get materials
				pendingRequests++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccured = materialStore === undefined;
					
					materials = materialStore;
					pendingRequests--;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				//get account indications
				pendingRequests++;
				
				var accountIndicationsSuccessCallback = function(accountIndicationsStore) {
					errorOccured = accountIndicationsStore === undefined;
					
					accountIndications = accountIndicationsStore;
					pendingRequests--;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
				eventController.registerOnEventForOneTime(eventId, accountIndicationsSuccessCallback);
				
				if(pendingRequests > 0)
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				else
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, matConfs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForMatConfs of BaseMatConfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForMatConfs: function(eventIdToFireWhenComplete, matConfs, materials, accountIndications) {
			try {
				matConfs.each(function (matConf, index, length) {
					//assign material
					if(materials) {
						var matnr = matConf.get('matnr');
						
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								matConf.set('material', material);
								return false;
							}
						});
					}
					
					//assign accountIndication
					if(accountIndications && accountIndications.getCount() > 0) {
						var bemot = matConf.get('bemot');
					
						accountIndications.each(function(accountIndication) {
							if(accountIndication.get('bemot') === bemot) {
								matConf.set('accountIndication', accountIndication);
								return false;
							}
						});
					}
				});
				
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, matConfs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForMatConfs of BaseMatConfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		buildMatConfStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var matConf = this.buildMatConfFromDbResultObject(cursor.value);
						store.add(matConf);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMatConfStoreFromDataBaseQuery of BaseMatConfManager', ex);
			}
		},
		
		//builds a single material conf. for a database record
		buildMatConfFromDbResultObject: function(dbResult) {
			var retval = null;
			
			try {
			    retval = Ext.create('AssetManagement.customer.model.bo.MaterialConf', {
			    	aufnr: dbResult['AUFNR'],
				    mblnr: dbResult['MBLNR'],
				    mjahr: dbResult['MJAHR'],
				    move_type: dbResult['MOVE_TYPE'],
				    spec_stock: dbResult['SPEC_STOCK'],
				    stck_type: dbResult['STCK_TYPE'],
				    plant: dbResult['PLANT'],
				    storloc: dbResult['STORLOC'],
				    customer: dbResult['CUSTOMER'],
				    matnr: dbResult['MATNR'],
				    quantity: dbResult['QUANTITY'],
				    unit: dbResult['UNIT'],
				    rsnum: dbResult['RSNUM'],
				    rspos: dbResult['RSPOS'],
				    kzear: dbResult['KZEAR'],
				    batch: dbResult['BATCH'],
				    equnr: dbResult['EQUNR'],
				    serialno: dbResult['SERIALNO'],
				    maktx: dbResult['MAKTX'],
				    ref_doc_no: dbResult['REF_DOC_NO'],
				    gr_rcpt: dbResult['GR_RCPT'],
				    bemot: dbResult['BEMOT'],
				    vornr: dbResult['VORNR'],
				    valtype: dbResult['VAL_TYPE'],
				    isdd: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ISDD']),
				                     
			        updFlag: dbResult['UPDFLAG'],
				    mobileKey: dbResult['MOBILEKEY']
			    });
			
			    retval.set('id', retval.get('aufnr') + retval.get('mblnr') + retval.get('mjahr'));
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMatConfFromDbResultObject of BaseMatConfManager', ex);
			}
			
			return retval;
		},
		
		//builds the raw object to store in the database out of a matConf model
		buildDataBaseObjectForMatConf: function(matConf) {
			var retval = null;
			var ac = null;
		
			try {
				ac = AssetManagement.customer.core.Core.getAppConfig();
			
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
			    retval['USERID'] =  ac.getUserId();
			    retval['AUFNR'] = matConf.get('aufnr');
			    retval['MBLNR'] = matConf.get('mblnr');
			    retval['MJAHR'] = matConf.get('mjahr');
			    retval['MOVE_TYPE'] = matConf.get('move_type');
			    retval['SPEC_STOCK'] = matConf.get('spec_stock');
			    retval['STCK_TYPE'] = matConf.get('stck_type');
			    retval['PLANT'] = matConf.get('plant');
			    retval['STORLOC'] = matConf.get('storloc');
			    retval['CUSTOMER'] = matConf.get('customer');
			    retval['MATNR'] = matConf.get('matnr');
			    retval['QUANTITY'] = matConf.get('quantity');
			    retval['UNIT'] = matConf.get('unit');
			    retval['RSNUM'] = matConf.get('rsnum');
			    retval['RSPOS'] = matConf.get('rspos');
			    retval['KZEAR'] = matConf.get('kzear');
			    retval['BATCH'] = matConf.get('batch');
			    retval['EQUNR'] = matConf.get('equnr');
			    retval['SERIALNO'] = matConf.get('serialno');
			    retval['MAKTX'] = matConf.get('maktx');
			    retval['REF_DOC_NO'] = matConf.get('ref_doc_no');
			    retval['GR_RCPT'] = matConf.get('gr_rcpt');
			    retval['BEMOT'] = matConf.get('bemot');
			    retval['VORNR'] = matConf.get('vornr');
			    retval['VAL_TYPE'] = matConf.get('valtype');
			    retval['ISDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(matConf.get('isdd'));
			
			    retval['UPDFLAG'] = matConf.get('updFlag');
			    retval['MOBILEKEY'] = matConf.get('mobileKey');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForMatConf of BaseMatConfManager', ex);
			}
			
			return retval;
		},
		
		getNextMblnr: function(matConf) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!matConf) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxMblnr = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;

							var mblnrValue = cursor.value['MBLNR'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(mblnrValue, '%')) {
                                //cut of the percent sign
							    mblnrValue = mblnrValue.substr(1);
                                var curMblnrValue = parseInt(mblnrValue);
							
							    if(!isNaN(curMblnrValue))
							    {
								    if(curMblnrValue > maxMblnr)
									    maxMblnr = curMblnrValue;
							    } 
                            }
				
							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxMblnr + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 9);
						    eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMblnr of BaseMatConfManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', matConf.get('aufnr'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MATCONF', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().rawQuery('D_MATCONF', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMblnr of BaseMatConfManager', ex);
			}
			
			return retval;			
		}
	}
});