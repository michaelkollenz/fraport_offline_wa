Ext.define('AssetManagement.base.manager.BaseMastManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Mast',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {	
		//public methods
		getMasts: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._cacheComplete === true) {
					eventController.requestEventFiring(retval, this._mastCache);
					
					return retval;
				}
				
				var toFill = this.getCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildMastStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me._cacheComplete = true;
							
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMasts of BaseMastManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_MAST', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_MAST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMasts of BaseMastManager', ex);
			}
	
			return retval;
		}, 
		
		getMast: function(matnr, werks, useBatchProcessing) {
			var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr) ||
						AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var mast = this.getFromCache(werks, matnr);
					
				if(mast) {
					eventController.requestEventFiring(retval, mast);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var mast = me.buildMastFromDataBaseQuery.call(me, eventArgs);
						
						if(mast)
							me.addToCache(mast);
							
						eventController.requestEventFiring(retval, mast);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMast of BaseMastManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('MATNR', matnr);
				keyMap.add('WERKS', werks);
		
	            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_MAST', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_MAST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMast of BaseMastManager', ex);
			}
			
			return retval;
		},
	
	    //private methods
		buildMastFromDataBaseQuery: function(eventArgs) {
	    	var retval = null;
	    	
	    	try {
	    		if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildMastFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMastFromDataBaseQuery of BaseMastManager', ex);
			}
			
			return retval;
		},
		
		buildMastStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var mast = this.buildMastFromDbResultObject(cursor.value);
						store.add(mast);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMastStoreFromDataBaseQuery of BaseMastManager', ex);
			}
		}, 
		
		buildMastFromDbResultObject: function(dbResult) {
	        var retval = null;
		
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Mast', {
					matnr: dbResult['MATNR'],
					werks: dbResult['WERKS'],
				    stlan: dbResult['STLAN'],
				    stlnr: dbResult['STLNR'],
				    stlal: dbResult['STLAL'],
				    
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['MATNR'] + dbResult['WERKS']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMastFromDbResultObject of BaseMastManager', ex);
			}
			
			return retval;
		},
		
		//cache administration
		_mastCache: null,
		_cacheComplete: false,
	
		getCache: function() {
	        try {
	        	if(!this._mastCache) {
					this._mastCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Mast',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseMastManager', ex);
			}
			
			return this._mastCache;
		},
		
		clearCache: function() {
	        try {
			    this.getCache().removeAll();
			    this._cacheComplete = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseMastManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseMastManager', ex);
			} 
		},
		
		addToCache: function(mast) {
			try {
				var temp = this.getCache().getById(mast.get('id'));
				
				if(temp)
					this._mastCache.remove(temp);
					
				this._mastCache.add(mast);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseMastManager', ex);
			}
		},
		
		getFromCache: function(matnr, werks) {
			var retval = null;
			
			try {
				retval = this.getCache().getById(matnr + werks);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseMastManager', ex);
			}
			
			return retval;
		}
	}
});