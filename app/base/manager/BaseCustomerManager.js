Ext.define('AssetManagement.base.manager.BaseCustomerManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.CustomerCache',
        'AssetManagement.customer.model.bo.Customer',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.CustomerCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustomerManager', ex);
			}
			
			return retval;
		},
		
		//public
		getCustomers: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.Customer',
			        autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCustomerStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustomers of BaseCustomerManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_CUSTOMER', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_CUSTOMER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustomers of BaseCustomerManager', ex);
			}
	
			return retval;
		},
		
		getCustomer: function(kunnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(kunnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(kunnr);
		
		        if(fromCache) {
		        	eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var customer = me.buildCustomerFromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(customer);
						eventController.requestEventFiring(retval, customer);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustomer of BaseCustomerManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('KUNNR', kunnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_CUSTOMER', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_CUSTOMER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAddress of BaseCustomerManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCustomerFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCustomerFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustomerFromDataBaseQuery of BaseCustomerManager', ex);
			}
			
			return retval;
		},
		
		buildCustomerStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust = this.buildCustomerFromDbResultObject(cursor.value);
						store.add(cust);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustomerStoreFromDataBaseQuery of BaseCustomerManager', ex);
			}
		},
		
		buildCustomerFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Customer', {
					kunnr: dbResult['KUNNR'],
					vkorg: dbResult['VKORG'],   	
			     	vtweg: dbResult['VTWEG'],
				    spart: dbResult['SPART'],
				    kdgrp: dbResult['KDGRP'],
				    bzirk: dbResult['BZIRK'],
				    vkgrp: dbResult['VKGRP'],
				    vkbur: dbResult['VKBUR'],
				    kvgr1: dbResult['KVGR1'],
				    kvgr2: dbResult['KVGR2'],
				    kvgr3: dbResult['KVGR3'],
				    kvgr4: dbResult['KVGR4'],
				    kvgr5: dbResult['KVGR5'],
				    titlep: dbResult['TITLE_P'],
					firstname: dbResult['FIRSTNAME'],
					lastname: dbResult['LASTNAME'],
				    city: dbResult['CITY'],
			     	postlcod1: dbResult['POSTL_COD1'],
				    street: dbResult['STREET'],
			    	telnumbermob: dbResult['TEL_NUMBER_MOB'],
				    tel1numbr: dbResult['TEL1_NUMBR'],
				    faxnumber: dbResult['FAX_NUMBER'],
	                funktion: dbResult['FUNCTION'],
	                email: dbResult['E_MAIL'],
	                telextens: dbResult['TEL_EXTENS'],
	                faxextens: dbResult['FAX_EXTENS'],
	                user1: dbResult['USR01'],
                    user2: dbResult['USR02'],
                    timeZone: dbResult['TIME_ZONE'],
                    str_suppl1: dbResult['STR_SUPPL1'],
	                updFlag: dbResult['UPDFLAG']
	                
				});
				
				retval.set('id', dbResult['KUNNR'] + dbResult['VKORG'] + dbResult['VTWEG'] + dbResult['SPART']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustomerFromDbResultObject of BaseCustomerManager', ex);
			}
			
			return retval;
		}
	}
});