Ext.define('AssetManagement.base.manager.BaseObjectStatusManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.ObjectStatus',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//private members
		REQUEST_ALL: '0',
		REQUEST_SET: '1',
		REQUEST_INACTIVE: '2',
	
		//public methods
		getStatusStoreForOrdersOperations: function(operations, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!operations || operations.getCount() === 0) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ObjectStatus',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
						
						var objectStatus = me.buildObjectStatusFromDataBaseQuery.call(me, eventArgs);
						
						if(objectStatus) {
							theStore.add(objectStatus);
							AssetManagement.customer.helper.CursorHelper.doCursorContinue(eventArgs.target.result);
						}
					
						if(done) {
							theStore.sort('inact', 'DESC');
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusStoreForOrdersOperations of BaseObjectStatusManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};				
				
				//build select for index - clone and sort operations store before
				var clone = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Operation',
					autoLoad: false
				});
				
				operations.each(function(operation) {
					clone.add(operation);
				}, this);
				
				clone.sort([
				    {
				        property : 'aufpl',
				        direction: 'ASC'
				    },
				    {
				        property : 'aplzl',
				        direction: 'ASC'
				    }
				]);
				
				var lowerObjnr = "OV" + clone.getAt(0).get('aufpl') + clone.getAt(0).get('aplzl');
				var indexMapLower = Ext.create('Ext.util.HashMap');
				indexMapLower.add('OBJNR', lowerObjnr);

				var upperObjnr = "OV" + clone.getAt(clone.getCount() - 1).get('aufpl') + clone.getAt(clone.getCount() - 1).get('aplzl');
				var indexMapUpper = Ext.create('Ext.util.HashMap');
				indexMapUpper.add('OBJNR', upperObjnr);
				
				clone.removeAll(true);
						
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_OBJECT_STATUS', 'OBJNR', indexMapLower, indexMapUpper);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_OBJECT_STATUS', 'OBJNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusStoreForOrdersOperations of BaseObjectStatusManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		getAllStatusStoreForObject: function(objnr, useBatchProcessing) {
	        var retval = -1;
	        	        
	        try {
			    retval = this.getStatusStoreForObject(objnr, this.REQUEST_ALL, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllStatusStoreForObject of BaseObjectStatusManager', ex);
				retval = -1;
			}  
			
			return retval;
		},
		
		getSetStatusStoreForObject: function(objnr, useBatchProcessing) {
			var retval = -1;
	        
            try {
                retval = this.getStatusStoreForObject(objnr, this.REQUEST_SET, useBatchProcessing);
            } catch(ex) {
	            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSetStatusStoreForObject of BaseObjectStatusManager', ex);
	            retval = -1;
            }  

            return retval;
		},
		
		getInactiveStatusStoreForObject: function(objnr, useBatchProcessing) {
			var retval = -1;
	        
            try {
                retval = this.getStatusStoreForObject(objnr, this.REQUEST_INACTIVE, useBatchProcessing);
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInactiveStatusStoreForObject of BaseObjectStatusManager', ex);
                retval = -1;
            }  

            return retval;
		},
		
		//saves the passed object status
		saveSingleObjectStatus: function(objectStatus, useBatchProcessing) {
			var retval = -1;
    
		    try {
				if(!objectStatus) {
				    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, false);
					return retval;
				}
			
				var objectStatusList = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ObjectStatus',
					autoLoad: false
				});
				
				objectStatusList.add(objectStatus);
			
				retval = this.saveObjectStatusList(objectStatusList, useBatchProcessing);
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSingleObjectStatus of BaseObjectStatusManager', ex);
			    retval = -1;
			}  
			
			return retval;
		},
		
		//saves the passed object status list
		saveObjectStatusList: function(objectStatusList, useBatchProcessing) {			
			var retval = -1;
	        
            try {
            	var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!objectStatusList) {
					eventController.requestEventFiring(retval, false);
					return retval;
				} else if(objectStatusList.getCount() == 0) {
					eventController.requestEventFiring(retval, true);
					return retval;
				}
				
				var objnr = objectStatusList.getAt(0).get('objnr');
				
				//first get the currents status set from the affected object from database
				var me = this;
				
				var objectStatusCurrentStatusCallback = function(curStatusList) {
					try {
						if(curStatusList === undefined) {
							eventController.requestEventFiring(retval, false);
							return;
						}
						
						//next step is to evaluate, what database action have to be performed for each entry inside objectStatusList
						var pendingRequests = 0;
						var errorOccurred = false;
						var reported = false;
						
						var completeFunction = function(success) {
							try {
								errorOccurred = !success;
							
								if(errorOccurred === true && reported === false) {
									reported = true;
									
									eventController.requestEventFiring(retval, false);
								} else if(pendingRequests == 0 && errorOccurred === false) {
									eventController.requestEventFiring(retval, true);
								}
							} catch(ex) {
								if(errorOccurred === true && reported === false) {
									reported = true;
									
									eventController.requestEventFiring(retval, false);
								}
							}
						};
						
						var dataBaseRequestCallback = function(eventArgs) {
							try {
								pendingRequests--;
								
								completeFunction(eventArgs.type === "success");
							} catch(ex) {
								if(errorOccurred === true && reported === false) {
									reported = true;
									
									eventController.requestEventFiring(retval, false);
								}
							}
						};
						
						//determine the database request to drop, if even necessary
						objectStatusList.each(function(objectStatus) {
							try {
								var counterPart = null;
								
								if(curStatusList) {
									counterPart = curStatusList.getById(objectStatus.get('id'));
								}
								
								if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objectStatus.get('updFlag')))
								     me.manageUpdateFlagForSaving(objectStatus);

								if (objectStatus.get('updFlag') === 'I' && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objectStatus.get('childKey2')))
								    objectStatus.set('childKey2', AssetManagement.customer.manager.KeyManager.createKey([objectStatus.get('objnr'), objectStatus.get('stat')]));

								
								if(counterPart) {
									//status is part of current the status list
								
									//next check is, if the status objects differ at the inactive flag
									if(objectStatus.get('inact') !== counterPart.get('inact')) {
										//a difference has been found
										//next check, if the status may be deleted (status is local only an set to inactive) 
										//or it has to be updated (it's already known at backend)
										if(objectStatus.get('updFlag') === 'I' && objectStatus.get('inact') === 'X') {
											//get the key of the object status to delete
											var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_OBJECT_STATUS', objectStatus);
											AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_OBJECT_STATUS', keyRange, dataBaseRequestCallback, dataBaseRequestCallback, true, me.requiresManualCompletion());
										} else if(objectStatus.get('updFlag') !== 'I') {
											me.manageUpdateFlagForSaving(objectStatus);
											
											var toUpdate = me.buildDataBaseObjectForObjectStatus(objectStatus);
											AssetManagement.customer.core.Core.getDataBaseHelper().update('D_OBJECT_STATUS', null, toUpdate, dataBaseRequestCallback, dataBaseRequestCallback, true);
										}
										
										pendingRequests++;
									}
								} else {
									//status is not part of current the status list
									//only set it, when the objectStatus is active
									if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objectStatus.get('inact'))) {
										var toSave = me.buildDataBaseObjectForObjectStatus(objectStatus);						
										AssetManagement.customer.core.Core.getDataBaseHelper().put('D_OBJECT_STATUS', toSave, dataBaseRequestCallback, dataBaseRequestCallback, true);
										
										pendingRequests++;
									}
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectStatusList of BaseObjectStatusManager', ex);
								errorOccurred = true;
								
								//break the loop
								return false;
							}
						}, this);
						
						if(pendingRequests > 0 && !errorOccurred) {
							AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
						} else {
							completeFunction(true);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectStatusList of BaseObjectStatusManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				};
				
				var eventId = this.getAllStatusStoreForObject(objnr, useBatchProcessing);
				
				if(eventId > -1) {
					eventController.registerOnEventForOneTime(eventId, objectStatusCurrentStatusCallback);
				} else {
					eventController.requestEventFiring(retval, false);
				}
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectStatusList of BaseObjectStatusManager', ex);
                retval = -1;
            }  

            return retval;
		},
		
		//private methods
		getStatusStoreForObject: function(objnr, requestType, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
					eventController.requestEventFiring(retval, undefined);
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ObjectStatus',
					autoLoad: false
				});
				
				var me = this;
				var addActive = requestType === this.REQUEST_ALL|| requestType === this.REQUEST_SET;
				var addInactive = requestType === this.REQUEST_ALL || requestType === this.REQUEST_INACTIVE;
				
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						if(done) {
							if(requestType === '0')
								theStore.sort('inact', 'DESC');
							
							eventController.fireEvent(retval, theStore);
						} else {
							var cursor = eventArgs.target.result;
							var active = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(cursor.value['INACT']);

							if(addActive && active || addInactive && !active) {
								var objectStatus = me.buildObjectStatusFromDataBaseQuery.call(me, eventArgs);
								theStore.add(objectStatus);
							}
							
							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						}						
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusForObject of BaseObjectStatusManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('OBJNR', objnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_OBJECT_STATUS', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_OBJECT_STATUS', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusForObject of BaseObjectStatusManager', ex);
			}
	
			return retval;
		},
		
		buildObjectStatusFromDataBaseQuery: function(eventArgs) {
	        var retval = null;

	        try {
	        	if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildObjectStatusFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectStatusFromDataBaseQuery of BaseObjectStatusManager', ex);
			}
			
			return retval;
		},
		
		buildObjectStatusFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.ObjectStatus', {
					objnr: dbResult['OBJNR'],
					stat: dbResult['STAT'],
					inact: dbResult['INACT'],
					chgnr: dbResult['CHNGR'],
					changed: dbResult['CHANGED'],
					txt04: dbResult['TXT04'],
					txt30: dbResult['TXT30'],
					status_int: dbResult['STATUS_INT'],
					user_status_code: dbResult['USER_STATUS_CODE'],
					user_status_desc: dbResult['USER_STATUS_DESC'],
					
				    updFlag: dbResult['UPDFLAG'],
				    mobileKey: dbResult['MOBILEKEY'],
				    childKey: dbResult['CHILDKEY'],
                    childKey2: dbResult['CHILDKEY2']
				});
				
				retval.set('id', dbResult['OBJNR'] + dbResult['STAT']);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectStatusFromDbResultObject of BaseObjectStatusManager', ex);
			}
			
			return retval;
		},
		
		//builds the raw object to store in the database out of a object status model
		buildDataBaseObjectForObjectStatus: function(objectStatus) {
			var retval = null;
			var ac = null;
		
			try {
				ac = AssetManagement.customer.core.Core.getAppConfig();
			
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
			    retval['USERID'] =  ac.getUserId();
			    retval['OBJNR'] = objectStatus.get('objnr');
			    retval['STAT'] = objectStatus.get('stat');
			    retval['INACT'] = objectStatus.get('inact');
			    retval['CHNGR'] = objectStatus.get('chgnr');
			    retval['CHANGED'] = objectStatus.get('changed');
			    retval['TXT04'] = objectStatus.get('txt04');
			    retval['TXT30'] = objectStatus.get('txt30');
			    retval['STATUS_INT'] = objectStatus.get('status_int');
			    retval['USER_STATUS_CODE'] = objectStatus.get('user_status_code');
			    retval['USER_STATUS_DESC'] = objectStatus.get('user_status_desc');
			
			    retval['UPDFLAG'] = objectStatus.get('updFlag');
			    retval['MOBILEKEY'] = objectStatus.get('mobileKey');
			    retval['CHILDKEY'] = objectStatus.get('childKey');
			    retval['CHILDKEY2'] = objectStatus.get('childKey2');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForObjectStatus of BaseObjectStatusManager', ex);
			}   
			   
			return retval;
		}
	}
});