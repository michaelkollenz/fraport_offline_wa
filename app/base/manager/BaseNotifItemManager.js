Ext.define('AssetManagement.base.manager.BaseNotifItemManager', {
    extend: 'AssetManagement.customer.manager.OxBaseManager',

	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.NotifItemCache',
        'AssetManagement.customer.model.bo.NotifItem',
        'AssetManagement.customer.model.bo.NotifTask',
        'AssetManagement.customer.model.bo.NotifActivity',
        'AssetManagement.customer.model.bo.NotifCause',
        'AssetManagement.customer.manager.NotifCauseManager',
        'AssetManagement.customer.manager.NotifActivityManager',
        'AssetManagement.customer.manager.NotifTaskManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.StoreHelper',
        'Ext.data.Store'
    ],
    
    inheritableStatics: {
		EVENTS: {
			ITEM_ADDED: 'notifItemAdded',
			ITEM_CHANGED: 'notifItemChanged',
			ITEM_DELETED: 'notifItemDeleted'
		},
		
		//protected
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.NotifItemCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getNotifItems: function(withDependendData, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForNotifItems(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifItem',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
								
							me.buildNotifItemsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
								
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForNotifItems.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifItems of BaseNotifItemManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
						
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIITEM', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIITEM', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifItems of BaseNotifItemManager', ex);
			}
				
			return retval;
		},
		
		getNotifItemsForNotif: function(qmnum, useBatchProcessing) {
			var retval = -1;
			
		    try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.FULL);
					this.loadDependendDataForNotifItems(baseStore, retval);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifItem',
						autoLoad: false
					});
				
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifItemsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotif(qmnum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
								me.loadDependendDataForNotifItems.call(me, baseStore, retval);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifItemsForNotif of BaseNotifItemManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIITEM', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIITEM', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifItemsForNotif of BaseNotifItemManager', ex);
			}
						
			return retval;
		},
		
		getNotifItem: function(qmnum, fenum, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCache(qmnum + fenum, cache.self.DATAGRADES.FULL);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				//to avoid duplicate objects, check if there already exists an instance for the requested notifItem and if so, use it
				var notifItem = cache.getFromCache(qmnum + fenum);
				
				if(notifItem) {
					this.loadDependendDataForNotifItem(notifItem, retval);
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var notifItem = me.buildNotifItemFromDataBaseQuery.call(me, eventArgs);

							if(notifItem) {
								me.loadDependendDataForNotifItem.call(me, notifItem, retval);
							} else {
								eventController.fireEvent(retval, null);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifItem of BaseNotifItemManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);
					keyMap.add('FENUM', qmnum);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_NOTIITEM', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIITEM', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifItem of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
   
		//save a notifItem - if it is necessary it will get a new counter first
		saveNotifItem: function(notifItem) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifItem) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = notifItem.get('updFlag');
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;

				this.manageUpdateFlagForSaving(notifItem);
				
				//check if the notifItem already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifItem.get('fenum'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1)
							notifItem.set('fenum', nextCounterValue);
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifItem.get('fenum'))) {
							eventController.requestEventFiring(retval, false);
							return;
						}
						
						notifItem.set('posnr', nextCounterValue);
						
						if(isInsert && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifItem.get('childKey')))
							notifItem.set('childKey', AssetManagement.customer.manager.KeyManager.createKey([ notifItem.get('qmnum'), notifItem.get('fenum') ]));
						
						//set the id
						notifItem.set('id', notifItem.get('qmnum') + notifItem.get('fenum'));
	
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
							
								if(success) {
									if(isInsert === true) {
										//do not specify a datagrade, so all depenend data will loaded, when necessary
										me.getCache().addToCache(notifItem);
									}
									
									var eventType = isInsert ? me.EVENTS.ITEM_ADDED : me.EVENTS.ITEM_CHANGED;
									eventController.requestEventFiring(eventType, notifItem);
								}
							
								eventController.requestEventFiring(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifItem of BaseNotifItemManager', ex);
								eventController.requestEventFiring(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForNotifItem(notifItem);						

						//if it is an insert perform an insert
						if(isInsert) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_NOTIITEM', toSave, callback, callback);
						} else {
							//else it is an update
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_NOTIITEM', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.requestEventFiring(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifItem of BaseNotifItemManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextFenum(notifItem);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifItem of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
	
		deleteNotifItem: function(notifItem, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifItem) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(notifItem);
							me.getCache().removeFromCache(notifItem);
							
							//do not trigger delete event before subitems have been deleted
							me.deleteNotifItemsSubItems(notifItem, retval);
						} else {
							eventController.requestEventFiring(retval, false);
						}

					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifItem of BaseNotifItemManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				};
				
				//get the key of the notifItem to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_NOTIITEM', notifItem);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_NOTIITEM', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifItem of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
		
		//private methods		
		buildNotifItemFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(event.target.result) {
						retval = this.buildNotifItemFromDbResultObject(event.target.result.value);			
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifItemFromDataBaseQuery of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
		
		buildNotifItemsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var notifItem = this.buildNotifItemFromDbResultObject(cursor.value);
						store.add(notifItem);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifItemsStoreFromDataBaseQuery of BaseNotifItemManager', ex);
			}
		},
		
		buildNotifItemFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.NotifItem', {
					qmnum: dbResult['QMNUM'],
					fenum: dbResult['FENUM'],
					bautl: dbResult['BAUTL'],
					fetxt: dbResult['FETXT'],
					fecod: dbResult['FECOD'],
					fegrp: dbResult['FEGRP'],
					fekat: dbResult['FEKAT'],
					fever: dbResult['FEVER'],
					oteil: dbResult['OTEIL'],
					otgrp: dbResult['OTGRP'],
					otkat: dbResult['OTKAT'],
					otver: dbResult['OTVER'],
					posnr: dbResult['POSNR'],
					anzfehler: dbResult['ANZFEHLER'],
					kzmla: dbResult['KZMLA'],
					axoenh: dbResult['AXOENH'],

					erzeit: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERZEIT']),
		
				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY'],
					childKey: dbResult['CHILDKEY']
			     });
			
			     retval.set('id', dbResult['QMNUM'] + dbResult['FENUM']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifItemFromDbResultObject of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
		
		loadListDependendDataForNotifItems: function(eventIdToFireWhenComplete, notifItems) {
			try {
				//do not continue if there is no data
				if(notifItems.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifItems);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var causes = null;
				var activities = null
				var tasks = null;
				
				var counter = 0;
				
				//TO-DO make dependend of customizing parameters --- this definitely will increase performance
				
				var done = 0;
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccurred === false) {
						me.assignListDependendDataForNotifItems.call(me, eventIdToFireWhenComplete, notifItems, causes, activities, tasks);
					}
				};
				
//				//get notif items' causes
//				if(true) {
//					done++;
//				
//					var causesSuccessCallback = function(itemCauses) {
//						errorOccured = itemCauses === undefined;
//					
//						causes = itemCauses;				
//						counter++;
//						
//						completeFunction();
//					};
//					
//					var eventId = AssetManagement.customer.manager.NotifCauseManager.getNotifCauses(true);
//					eventController.registerOnEventForOneTime(eventId, causesSuccessCallback);
//				}
				
				//get notif items' activities
				if(true) {
					done++;
				
					var activitySuccessCallback = function(itemActivities) {
						errorOccured = itemActivities === undefined;
					
						activities = itemActivities;
						counter++;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.NotifActivityManager.getNotifActivities(true);
					eventController.registerOnEventForOneTime(eventId, activitySuccessCallback);
				}
				
				//get notif items' tasks
				if(true) {
					done++;
				
					var taskSuccessCallback = function(itemTasks) {
						errorOccured = itemTasks === undefined;
					
						tasks = itemTasks;
						counter++;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.NotifTaskManager.getNotifTasks(true);
					eventController.registerOnEventForOneTime(eventId, taskSuccessCallback);
				}
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForNotifItems of BaseNotifItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all necessary data has been collected
		assignListDependendDataForNotifItems: function(eventIdToFireWhenComplete, notifItems, causes, activities, tasks) {
			try {
				notifItems.each(function(notifItem, index, length) {
					//begin causes assignment
					var myCauses = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifCause',
						autoLoad: false
					});
					
					//fetch all my causes
					if(causes && causes.getCount() > 0) {
						causes.each(function(cause, index, length) {
							if(task.get('qmnum') === notifItem.get('qmnum')) {
								if(cause.get('fenum') === notifItem.get('fenum')) {
									myCauses.add(cause);
								}
							}
						});
					}
					
					//sorting by urnum
					myCauses.sort('urnum', 'ASC');
					notifItem.set('notifItemCauses', myCauses);
					//end notif items assignment
					
					//begin activities assignment
					var myActivities = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
					
					//fetch all my activites
					if(activities && activities.getCount() > 0) {
						activities.each(function(activity, index, length) {
							if(activity.get('qmnum') === notifItem.get('qmnum')) {
								if(activity.get('fenum') === notifItem.get('fenum')) {
									myActivities.add(activity);
								}
							}
						});
					}
				
					//sorting by manum
					myActivities.sort('manum', 'ASC');
					notifItem.set('notifItemActivities', myActivities);
					//end notif activities assignment
					
					//begin notif tasks assignment
					var myTasks = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});
					
					//fetch all my tasks
					if(tasks && tasks.getCount() > 0) {
						tasks.each(function(task, index, length) {
							if(task.get('qmnum') === notifItem.get('qmnum')) {
								if(task.get('fenum') === notifItem.get('fenum')) {
									myTasks.add(task);
								}
							}
						});
					}
					
					//sorting by manum
					myTasks.sort('manum', 'ASC');
					notifItem.set('notifItemTasks', myTasks);
					//end notif tasks assignment
				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(notifItems, cache.self.DATAGRADES.LOW);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForNotifItems of BaseNotifItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForNotifItems: function(notifItems, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(notifItems.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifItems);
					return;
				}
				
				var qmnum = notifItems.getAt(0).get('qmnum');
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the data has to be loaded by all other managers
				//register on the corresponding events
				var causes = null;
				var activities = null;
				var tasks = null;
				var custCodes = null;
				
				var counter = 0;
	
				//TODO make dependend of customizing parameters --- this definitely will increase performance
				
				var done = 0;
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignDependendDataForNotifItems.call(me, eventIdToFireWhenComplete, notifItems, causes, activities, tasks, custCodes);
					}
				};
				
				//Longtext - pass the items to LT Manager and let it set all textes. Fire an event on completition
				notifItems.each(function (item){
					//getLocalLongtext
					if(true) {
						done++;
						
						var localLongtextSuccessCallback = function(localLT, backendLT) {
							errorOccured = localLT === undefined || backendLT === undefined;
							item.set('localLongtext', localLT);
							item.set('backendLongtext', backendLT);
							counter++;
							
							completeFunction();
						};
						eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(item, true);
						eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
					}
				}); 
				
				//get notif items' causes
				if(true) {
					done++;
				
					var causesSuccessCallback = function(itemCauses) {
						errorOccured = itemCauses === undefined;
					
						causes = itemCauses;				
						counter++;
						
						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.NotifCauseManager.getNotifCausesForNotif(qmnum, true);
					eventController.registerOnEventForOneTime(eventId, causesSuccessCallback);
				}
				
				//get notif items' activities
				if(true) {
					done++;
				
					var activitySuccessCallback = function(itemActivities) {
						errorOccured = itemActivities === undefined;
					
						activities = itemActivities;
						counter++;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.NotifActivityManager.getNotifActivitiesForNotif(qmnum, true);
					eventController.registerOnEventForOneTime(eventId, activitySuccessCallback);
				}
				
				//get notif items' tasks
				if(true) {
					done++;
				
					var taskSuccessCallback = function(itemTasks) {
						errorOccured = itemTasks === undefined;
					
						tasks = itemTasks;
						counter++;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.NotifTaskManager.getNotifTasksForNotif(qmnum, true);
					eventController.registerOnEventForOneTime(eventId, taskSuccessCallback);
				}
				
				//get notif item custCodes
				done++;
				
				var custCodesSuccessCallback = function(cCodes) {
					errorOccured = cCodes === undefined;
					
					custCodes = cCodes;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
				eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForNotifItems of BaseNotifItemManager', ex);
				eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForNotifItems: function(eventIdToFireWhenComplete, notifItems, causes, activities, tasks, custCodes) {
			try {
				notifItems.each(function(notifItem, index, length) {
					//begin causes assignment
					var myCauses = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifCause',
						autoLoad: false
					});
					
					//fetch items' causes
					if(causes && causes.getCount() > 0) {
						causes.each(function(cause, index, length) {
							if(cause.get('fenum') === notifItem.get('fenum')) {
								myCauses.add(cause);
							}
						});
					}
					
					//sorting by urnum
					myCauses.sort('urnum', 'ASC');
					notifItem.set('notifItemCauses', myCauses);
					//end notif items assignment
					
					//begin activities assignment
					var myActivities = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
					
					//fetch items' activites
					if(activities && activities.getCount() > 0) {
						activities.each(function(activity, index, length) {
							if(activity.get('fenum') === notifItem.get('fenum')) {
								myActivities.add(activity);
							}
						});
					}
					
					//sorting by manum
					myActivities.sort('manum', 'ASC');
					notifItem.set('notifItemActivities', myActivities);
					//end notif activities assignment
					
					//begin notif tasks assignment
					var myTasks = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});
					
					//fetch items' tasks
					if(tasks && tasks.getCount() > 0) {
						tasks.each(function(task, index, length) {
							if(task.get('fenum') === notifItem.get('fenum')) {
								myTasks.add(task);
							}
						});
					}
					
					//sorting by manum
					myTasks.sort('manum', 'ASC');
					notifItem.set('notifItemTasks', myTasks);
					//end notif tasks assignment
					
					if(custCodes) {
						//assign codes
						var katalogArt = notifItem.get('fekat');
						var codeGruppe = notifItem.get('fegrp');
						var code = notifItem.get('fecod');
						
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
							var groupHashMapForCatalogue = custCodes.get(katalogArt);
							
							if(groupHashMapForCatalogue) {
								var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
								
								if(codesGroupStore) {
									codesGroupStore.each(function(custCode) {
										if(custCode.get('code') === code) {
											notifItem.set('damageCustCode', custCode);
											return false;
										}
									}, this);
								}
							}
						}
						
						katalogArt = notifItem.get('otkat');
						codeGruppe = notifItem.get('otgrp');
						code = notifItem.get('oteil');
						
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
							var groupHashMapForCatalogue = custCodes.get(katalogArt);
							
							if(groupHashMapForCatalogue) {
								var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
								
								if(codesGroupStore) {
									codesGroupStore.each(function(custCode) {
										if(custCode.get('code') === code) {
											notifItem.set('objectPartCustCode', custCode);
											return false;
										}
									}, this);
								}
							}
						}
					}
				});
				
				var cache = this.getCache();
				var qmnum = notifItems.getAt('0').get('qmnum');
				cache.addToCacheForNotif(qmnum, notifItems, cache.self.DATAGRADES.MEDIUM);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForNotifItems of BaseNotifItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForNotifItem: function(notifItem, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(!notifItem) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifItem);
					return;
				}
				
				var qmnum = notifItem.get('qmnum');
				var fenum = notifItem.get('fenum');
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the data has to be loaded by all other managers
				//register on the corresponding events
				var counter = 0;
				var causes = null;
				var activities = null;
				var tasks = null;
				var custCodes = null;
				var localLongtext = null;
				var backendLongtext = null;
				//TODO make dependend of customizing parameters --- this definitely will increase performance
			
				var errorOccured = false;
				var reported = false;
				
				var done = 3;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignDependendDataForNotifItem.call(me, eventIdToFireWhenComplete, notifItem, causes, activities, tasks, localLongtext, backendLongtext, custCodes);
					}
				};
				
				var causesSuccessCallback = function(itemCauses) {
					errorOccured = itemCauses === undefined;
					
					causes = itemCauses;
					counter++;
					
					completeFunction();
				};
				
				var eventId = AssetManagement.customer.manager.NotifCauseManager.getNotifCausesForNotifItem(qmnum, fenum, true);
				eventController.registerOnEventForOneTime(eventId, causesSuccessCallback);
				
				var activitySuccessCallback = function(itemActivities) {
					errorOccured = itemActivities === undefined;
					
					activities = itemActivities;
					counter++;
					
					completeFunction();
				};
				
				var eventId = AssetManagement.customer.manager.NotifActivityManager.getNotifActivitiesForNotifItem(qmnum, fenum, true);
				eventController.registerOnEventForOneTime(eventId, activitySuccessCallback);
				
				var taskSuccessCallback = function(itemTasks) {
					errorOccured = itemTasks === undefined;
					
					tasks = itemTasks;
					counter++;
					
					completeFunction();
				};
				
				var eventId = AssetManagement.customer.manager.NotifTaskManager.getNotifTasksForNotifItem(qmnum, fenum, true);
				eventController.registerOnEventForOneTime(eventId, taskSuccessCallback);
				
				done++;
				
				var custCodesSuccessCallback = function(cCodes) {
					errorOccured = cCodes === undefined;
					
					custCodes = cCodes;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
				eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);
				
				if(true) {
					done++;
					
					var localLongtextSuccessCallback = function(localLT, backendLT) {
						errorOccured = localLT === undefined || backendLT === undefined;
						localLongtext = localLT; 
						backendLongtext = backendLT;
						
						counter++;
						
						completeFunction();
					};
					eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(notifItem, true);
					eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
				}
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForNotifItem of BaseNotifItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForNotifItem: function(eventIdToFireWhenComplete, notifItem, causes, activities, tasks, localLT, backendLT, custCodes) {
			try {	
				if(causes)
					causes.sort('urnum', 'ASC');
					
				notifItem.set('notifItemCauses', causes);
				
				if(activities)
					activities.sort('manum', 'ASC');
					
				notifItem.set('notifItemActivities', activities);
				
				if(tasks)
					tasks.sort('manum', 'ASC');
					
				notifItem.set('notifItemTasks', tasks);
				
				notifItem.set('localLongtext', localLT);
				notifItem.set('backendLongtext', backendLT);
					
				if(custCodes) {
					//assign codes
					var katalogArt = notifItem.get('fekat');
					var codeGruppe = notifItem.get('fegrp');
					var code = notifItem.get('fecod');
					
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
						var groupHashMapForCatalogue = custCodes.get(katalogArt);
						
						if(groupHashMapForCatalogue) {
							var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
							
							if(codesGroupStore) {
								codesGroupStore.each(function(custCode) {
									if(custCode.get('code') === code) {
										notifItem.set('damageCustCode', custCode);
										return false;
									}
								}, this);
							}
						}
					}
					
					katalogArt = notifItem.get('otkat');
					codeGruppe = notifItem.get('otgrp');
					code = notifItem.get('oteil');
					
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
						var groupHashMapForCatalogue = custCodes.get(katalogArt);
						
						if(groupHashMapForCatalogue) {
							var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
							
							if(codesGroupStore) {
								codesGroupStore.each(function(custCode) {
									if(custCode.get('code') === code) {
										notifItem.set('objectPartCustCode', custCode);
										return false;
									}
								}, this);
							}
						}
					}
				}
				
				var cache = this.getCache();
				cache.addToCache(notifItem, cache.self.DATAGRADES.FULL);
				
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifItem);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForNotifItem of BaseNotifItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		deleteNotifItemsSubItems: function(notifItem, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(!notifItem) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, false);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//delete all subitems (causes, tasks and activities)
				var counter = 0;
				var done = 3;
				
				var completeFunction = function() {
					counter++;
				
					if(counter === done) {
						eventController.requestEventFiring(me.EVENTS.ITEM_DELETED, notifItem);
						eventController.requestEventFiring(eventIdToFireWhenComplete, true);
					}
				};
				
				//delete the subitems
				var qmnum = notifItem.get('qmnum');
				var fenum = notifItem.get('fenum');

				var localLongtext = notifItem.get('localLongtext');

				if (localLongtext) {
				     done++;
                     eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(notifItem, true);
                     eventController.registerOnEventForOneTime(eventId, completeFunction);
				}
				
				var keyAndIndexMap = Ext.create('Ext.util.HashMap');
				keyAndIndexMap.add('QMNUM', qmnum);
				keyAndIndexMap.add('FENUM', fenum);

				var useLocalUpdateFlag = this.requiresManualCompletion();
				
				var keyRangeCauses = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTICAUSE', keyAndIndexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_NOTICAUSE', keyRangeCauses, completeFunction, completeFunction, true, useLocalUpdateFlag);

				var indexRangeTasks = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_NOTITASK', 'QMNUM-FENUM', keyAndIndexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3teUsingAnIndex('D_NOTITASK', 'QMNUM-FENUM', indexRangeTasks, completeFunction, completeFunction, true, useLocalUpdateFlag);
				
				var indexRangeActivities = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_NOTIACTIVITY', 'QMNUM-FENUM', keyAndIndexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3teUsingAnIndex('D_NOTIACTIVITY', 'QMNUM-FENUM', indexRangeActivities, completeFunction, completeFunction, true, useLocalUpdateFlag);
				
				AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifItemsSubItems of BaseNotifItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, false);
			}
		},
		
		//builds the raw object to store in the database out of a notifItem model
		buildDataBaseObjectForNotifItem: function(notifItem) {
			var retval = { };
			
			try {
				retval['QMNUM'] = notifItem.get('qmnum');
				retval['FENUM'] = notifItem.get('fenum');
				retval['FETXT'] = notifItem.get('fetxt');
				retval['FEKAT'] = notifItem.get('fekat');
				retval['FEGRP'] = notifItem.get('fegrp');
				retval['FECOD'] = notifItem.get('fecod');
				retval['FEVER'] = notifItem.get('fever');
				retval['OTKAT'] = notifItem.get('otkat');
				retval['OTGRP'] = notifItem.get('otgrp');
				retval['OTVER'] = notifItem.get('otver');
				retval['OTEIL'] = notifItem.get('oteil');
				retval['BAUTL'] = notifItem.get('bautl');
				retval['POSNR'] = notifItem.get('posnr');
				retval['ANZFEHLER'] = notifItem.get('anzfehler');
				retval['AXOENHT'] = notifItem.get('axoenh');
				
				retval['ERZEIT'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifItem.get('erzeit'));
				
				retval['MOBILEKEY'] = notifItem.get('mobileKey');
				retval['CHILDKEY'] = notifItem.get('childKey');
				retval['UPDFLAG'] = notifItem.get('updFlag');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForNotifItem of BaseNotifItemManager', ex);
			}
			
			return retval;
		},
		
		getNextFenum: function(notifItem) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifItem) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxFenum = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
                            var fenumValue = cursor.value['FENUM'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(fenumValue, '%')) {
                                //cut of the percent sign
							    fenumValue = fenumValue.substr(1);
                                var curFenumValue = parseInt(fenumValue);
							
							    if(!isNaN(curFenumValue))
							    {
								    if(curFenumValue > maxFenum)
									    maxFenum = curFenumValue;
							    }
						    }

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxFenum + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
						    eventController.requestEventFiring(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextFenum of BaseNotifItemManager', ex);
						eventController.requestEventFiring(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QMNUM', notifItem.get('qmnum'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIITEM', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIITEM', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextFenum of BaseNotifItemManager', ex);
			}
			
			return retval;			
		}
	}
});   