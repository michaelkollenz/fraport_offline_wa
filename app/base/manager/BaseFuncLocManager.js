Ext.define('AssetManagement.base.manager.BaseFuncLocManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.FuncLocCache',
        'AssetManagement.customer.model.bo.FuncLoc',
        'AssetManagement.customer.manager.AddressManager',
        'AssetManagement.customer.manager.PartnerManager',
        'AssetManagement.customer.manager.ObjectStatusManager',
        'AssetManagement.customer.manager.DMSDocumentManager',
        'AssetManagement.customer.manager.DocUploadManager',
        'AssetManagement.customer.manager.HistOrderManager',
        'AssetManagement.customer.manager.HistNotifManager',
        'AssetManagement.customer.manager.MeasurementPointManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.manager.ClassificationManager'
    ],

	inheritableStatics: {
		EVENTS: {
			FUNCLOC_ADDED: 'funcLocAdded',
			FUNCLOC_CHANGED: 'funcLocChanged'
		},

		//private
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.FuncLocCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseFuncLocManager', ex);
			}

			return retval;
		},

		//public methods
		getFuncLocs: function(withDependendData, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}

				var baseStore = null;

				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForFuncLocs(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.FuncLoc',
						autoLoad: false
					});

					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

							me.buildFuncLocsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);

								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForFuncLocs.call(me, retval, baseStore);
								} else {
									//return a store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLocs of BaseFuncLocManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};

					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_FLOC', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_FLOC', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLocs of BaseFuncLocManager', ex);
			}

			return retval;
		},

		getFuncLoc: function(tplnr, useBatchProcessing) {
			var retval = -1;

			try {
				retval = this.getFuncLocInternal(tplnr, false, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLoc of BaseFuncLocManager', ex);
			}

			return retval;
		},

		getFuncLocLightVersion: function(tplnr, useBatchProcessing) {
			var retval = -1;

			try {
				retval = this.getFuncLocInternal(tplnr, true, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLocLightVersion of BaseFuncLocManager', ex);
			}

			return retval;
		},

		//protected
		getFuncLocInternal: function(tplnr, lightVersion, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var cache = this.getCache();
				var requestedDataGrade = lightVersion ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				var fromCache = cache.getFromCache(tplnr, requestedDataGrade);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}

				//to avoid duplicate objects, check if there already exists an instance for the requested equipment and if so, use it
				var funcLoc = cache.getFromCache(tplnr);

				if(funcLoc) {
					this.loadDependendDataForFuncLoc(funcLoc, retval, lightVersion);
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var funcLoc = me.buildFuncLocFromDataBaseQuery.call(me, eventArgs);

							if(funcLoc)
								me.loadDependendDataForFuncLoc.call(me, funcLoc, retval, lightVersion);
							else
								eventController.requestEventFiring(retval, null);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLocInternal of BaseFuncLocManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};

					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('TPLNR', tplnr);

					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_FLOC', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_FLOC', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLocInternal of BaseFuncLocManager', ex);
			}

			return retval;
		},

		//save funcLoc
		saveFuncLoc: function(funcLoc) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!funcLoc) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

				//update flag management
				this.manageUpdateFlagForSaving(funcLoc);

				var me = this;

				//set the id
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";

						if(success) {
							eventController.requestEventFiring(me.EVENTS.FUNCLOC_CHANGED, funcLoc);
						}

						eventController.requestEventFiring(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFuncLoc of BaseFuncLocManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				};

				var toSave = me.buildDataBaseObjectForFuncLoc(funcLoc);
				if(funcLoc.get('updFlag') === 'U')
					AssetManagement.customer.core.Core.getDataBaseHelper().update('D_FLOC', null, toSave, callback, callback);
				else
					AssetManagement.customer.core.Core.getDataBaseHelper().put('D_FLOC', toSave, callback, callback);


			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFuncLoc of BaseFuncLocManager', ex);
			}

			return retval;
		},

		//private methods
		buildFuncLocFromDataBaseQuery: function(eventArgs) {
			var retval = null;

			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildFuncLocFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFuncLocFromDataBaseQuery of BaseFuncLocManager', ex);
			}

			return retval;
		},

		buildFuncLocsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {

				    	var funcLoc = this.buildFuncLocFromDbResultObject(cursor.value);
						store.add(funcLoc);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFuncLocsStoreFromDataBaseQuery of BaseFuncLocManager', ex);
			}
		},

		buildFuncLocFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {
					tplnr: dbResult['TPLNR'],
					pltxt: dbResult['PLTXT'],
					objnr: dbResult['OBJNR'],
					submt: dbResult['SUBMT'],
					iwerk: dbResult['IWERK'],
					baujj: dbResult['BAUJJ'],
					baumm: dbResult['BAUMM'],
					beber: dbResult['BEBER'],
					eqart: dbResult['EQART'],
					eqfnr: dbResult['EQFNR'],
					fltyp: dbResult['FLTYP'],
					herld: dbResult['HERLD'],
					herst: dbResult['HERST'],
					posnr: dbResult['POSNR'],
					invnr: dbResult['INVNR'],
					kostl: dbResult['KOSTL'],
					stort: dbResult['STORT'],
					msgrp: dbResult['MSGRP'],
					rbnr: dbResult['RBNR'],
					tplma: dbResult['TPLMA'],
					typbz: dbResult['TYPBZ'],
					gewrk: dbResult['GEWRK'],
					wergw: dbResult['WERGW'],
					txtGewrk: dbResult['TXT_GEWRK'],
					adrnr: dbResult['ADRNR'],
					strno: dbResult['STRNO'],
					stsma: dbResult['STSMA'],

                    abckz: dbResult['ABCKZ'],
                    groes: dbResult['GROES'],
                    brgew: dbResult['BRGEW'],
                    gewei: dbResult['GEWEI'],
                    usr01flag: dbResult['USR01FLAG'],
                    usr02flag: dbResult['USR02FLAG'],
                    usr03code: dbResult['USR03CODE'],
                    usr04code: dbResult['USR04CODE'],
                    usr05txt: dbResult['USR05TXT'],
                    usr06txt: dbResult['USR06TXT'],
                    usr07txt: dbResult['USR07TXT'],
                    usr09num: dbResult['USR09NUM'],

					ansdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ANSDT']),
					datab: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['DATAB']),
                    usr08date: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['USR08DATE']),

				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY']
			    });

			    retval.set('id', dbResult['TPLNR']);

			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFuncLocFromDbResultObject of BaseFuncLocManager', ex);
			}

			return retval;
		},

		loadListDependendDataForFuncLocs: function(eventIdToFireWhenComplete, funcLocs) {
			try {
				//do not continue if there is no data
				if(funcLocs.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, funcLocs);
					return;
				}

				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var partners = null;		//currently disabled
				var addresses = null;
				var statusList = null;		//currently disabled

				var counter = 0;
				var done = 0;
				var erroroccurred = false;
				var reported = false;

				var completeFunction = function() {
					if(erroroccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && erroroccurred === false) {
						me.assignListDependendDataForFuncLocs.call(me, eventIdToFireWhenComplete, funcLocs, partners, addresses, statusList);
					}
				};

				//TO-DO make dependend of customizing parameters --- this definitely will increase performance

	//			//get partner data
	//			done++;
	//
	//			var partnerSuccessCallback = function(partnerList) {
	//				erroroccurred = partnerList === undefined;
	//
	//				partners = partnerList;
	//				counter++;
	//
	//				completeFunction();
	//			};
	//
	//			eventId = AssetManagement.customer.manager.PartnerManager.getPartners(false);
	//			eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);

				//get address data
				done++;

				var addressSuccessCallback = function(addressList) {
					erroroccurred = addressList === undefined;

					addresses = addressList;
					counter++;

					completeFunction();
				};

				eventId = AssetManagement.customer.manager.AddressManager.getAddresses(true);
				eventController.registerOnEventForOneTime(eventId, addressSuccessCallback);

	//			//get status data
	//			done++;
	//
	//			var statusSuccessCallback = function(statuslist) {
    //				erroroccurred = statuslist === undefined;
	//
	//				statusList = statuslist;
	//				counter++;
	//
	//				completeFunction();
	//			};
	//
	//			eventId = AssetManagement.customer.manager.StatusManager.getStatusList(false);
	//			eventController.registerOnEventForOneTime(eventId, statusSuccessCallback);

				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForFuncLocs of BaseFuncLocManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		//call this function, when all neccessary data has been collected
		assignListDependendDataForFuncLocs: function(eventIdToFireWhenComplete, funcLocs, partners, addresses, statusList) {
			try {
				funcLocs.each(function(floc, index, length) {
					//DISBALED
	//
	//				//begin partners assignment
	//				var myPartners = Ext.create('Ext.data.Store', {
	//					model: 'AssetManagement.customer.model.bo.Partner',
	//					autoLoad: false
	//				});
	//
	//				partners.each(function(partner, index, length) {
	//					if(partner.get('objnr') === order.get('objnr'))
	//						myPartners.add(partner);
	//				});
	//
	//				//sorting is not required yet for funcloc list
	//				order.set('partners', myPartners);
	//				//end partners assignment

					//begin address assignment
					if(addresses) {
						var addressNoToLookFor = floc.get('adrnr');

						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressNoToLookFor)) {
							addresses.each(function(address, index, length) {
								if(address.get('adrnr') === addressNoToLookFor) {
									floc.set('address', address);
									return false;
								}
							});
						}
					}
					//end address assignment

					//DISBALED

	//				//begin status assignment
	//				var myStatusList = Ext.create('Ext.data.Store', {
	//					model: 'AssetManagement.customer.model.bo.Status',
	//					autoLoad: false
	//				});
	//
	//				statusList.each(function(status, index, length) {
	//					if(status.get('objnr') === order.get('objnr'))
	//						myStatusList.add(status);
	//				});
	//
	//				order.set('statusList', myStatusList);
	//				//end status assignment
				});

				var cache = this.getCache();
				cache.addStoreForAllToCache(funcLocs, cache.self.DATAGRADES.LOW);

				//return a store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForFuncLocs of BaseFuncLocManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		loadDependendDataForFuncLoc: function(funcLoc, eventIdToFireWhenComplete, lightVersion) {
			try {
				if(funcLoc) {
					var tplnr = funcLoc.get('tplnr');
					var objnr = funcLoc.get('objnr');

					//all of assignListDependendDataForNotifs but with more details
					//additional longtext, checklist status, files ...

					if(lightVersion === undefined)
						lightVersion = false;

					//lightversion currently contains: address

					var partners = null;
					var address = null;
					var objectStatusList = null;
					var classifications = null;
					var documents = null;
					var measPoints = null;
					var histOrders = null;
					var histNotifs = null;
					var backendLongtext = null;
					var localLongtext = null;

					var counter = 0;
					var done = 0;
					var erroroccurred = false;
					var reported = false;

					var me = this;
					var eventController = AssetManagement.customer.controller.EventController.getInstance();

					var completeFunction = function() {
						if(erroroccurred === true && reported === false) {
							AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
							reported = true;
						} else if(counter === done && erroroccurred === false) {
							me.assignDependendDataForFuncLoc.call(me, eventIdToFireWhenComplete, funcLoc, lightVersion,
								partners, address, objectStatusList, classifications, documents, measPoints,
									histOrders, histNotifs, backendLongtext, localLongtext);
						}
					};

				    //TO-DO make dependend of customizing parameters --- this definitely will increase performance

				    //get classifications
					if (lightVersion === false) {
					    done++;

					    var classificationSuccessCallback = function (classificationList) {
					        erroroccurred = classificationList === undefined;

				            classifications = classificationList;
					        counter++;

					        completeFunction();
					    };

					    eventId = AssetManagement.customer.manager.ClassificationManager.getObjClasses(objnr, true); //getObjClasses(objnr, true);
					    eventController.registerOnEventForOneTime(eventId, classificationSuccessCallback);
					}

					//get partners
					if(lightVersion === false) {
						done++;

						var partnerSuccessCallback = function(partnerList) {
							erroroccurred = partnerList === undefined;

							partners = partnerList;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.PartnerManager.getAllPartnersForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
					}

				    //get measPoints
					if (lightVersion === false) {
					    done++;

					    var measPointsSuccessCallback = function (measPointList) {
					        erroroccurred = measPointList === undefined;

					        measPoints = measPointList;
					        counter++;

					        completeFunction();
					    };

					    eventId = AssetManagement.customer.manager.MeasurementPointManager.getMeasPointsForFuncLoc(objnr, true);
					    eventController.registerOnEventForOneTime(eventId, measPointsSuccessCallback);
					}

					//get address
					if(true) {
						var adrnr = funcLoc.get('adrnr');

						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(adrnr)) {
							done++;

							var addressSuccessCallback = function(addresss) {
								erroroccurred = addresss === undefined;

								address = addresss;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.AddressManager.getAddress(adrnr, true);
							eventController.registerOnEventForOneTime(eventId, addressSuccessCallback);
						}
					}

					//get objectStatus
					if(lightVersion === false) {
						done++;

						var objectStatusSuccessCallback = function(objStatusList) {
							erroroccurred = objStatusList === undefined;

							objectStatusList = objStatusList;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, objectStatusSuccessCallback);
					}

					//get documents
					if(lightVersion === false) {
						documents = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.File',
							autoLoad: false
						});

						var idToUse = 'IF' + tplnr;

						//get dms documents
						done++;

						var dmsDocsSuccessCallback = function(dmsDocs) {
							erroroccurred = dmsDocs === undefined;

							if(erroroccurred) {
								documents = null;
							} else if(documents && dmsDocs && dmsDocs.getCount() > 0) {
								dmsDocs.each(function(dmsDoc) {
									documents.add(dmsDoc);
								}, this);
							}

							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.DMSDocumentManager.getDmsDocumentsForObject(idToUse, true);
						eventController.registerOnEventForOneTime(eventId, dmsDocsSuccessCallback);

						//get doc upload records
						done++;

						var docUploadsSuccessCallback = function(docUploads) {
							erroroccurred = docUploads === undefined;

							if(erroroccurred) {
								documents = null;
							} else if(documents && docUploads && docUploads.getCount() > 0) {
								docUploads.each(function(docUpload) {
									documents.add(docUpload);
								}, this);
							}

							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.DocUploadManager.getDocUploadsForObject(idToUse, true);
						eventController.registerOnEventForOneTime(eventId, docUploadsSuccessCallback);
					}

					//get hist orders
					if(lightVersion === false) {
						done++;

						var histOrdersSuccessCallback = function(histOrderss) {
							errorOccurred = histOrderss === undefined;

							histOrders = histOrderss;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.HistOrderManager.getHistOrdersForFuncLoc(tplnr, true);
						eventController.registerOnEventForOneTime(eventId, histOrdersSuccessCallback);
					}

					//get hist notifs
					if(lightVersion === false) {
						done++;

						var histNotifsSuccessCallback = function(histNotifss) {
							errorOccurred = histNotifss === undefined;

							histNotifs = histNotifss;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.HistNotifManager.getHistNotifsForFuncLoc(tplnr, true);
						eventController.registerOnEventForOneTime(eventId, histNotifsSuccessCallback);
					}

                    //get longtext
					if (lightVersion === false) {
					    done++;

					    var longtextSuccessCallback = function (localLT, backendLT) {
					        erroroccurred = localLT === undefined || backendLT === undefined;
					        localLongtext = localLT;
					        backendLongtext = backendLT;
					        counter++;

					        completeFunction();
					    };

					    eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(funcLoc, true);
					    eventController.registerOnEventForOneTime(eventId, longtextSuccessCallback);
					}

					if(done > 0) {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					} else {
						completeFunction();
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForFuncLoc of BaseFuncLocManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		assignDependendDataForFuncLoc: function(eventIdToFireWhenComplete, funcLoc, lightVersion, partners, address,
													objectStatusList, classifications, documents, measPoints,
														histOrders, histNotifs, backendLongtext, localLongtext)
		{
			try
			{
				if(!funcLoc) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
					return;
				}

				funcLoc.set('classifications', classifications);
				funcLoc.set('address', address);
				funcLoc.set('objectStatusList', objectStatusList);
				funcLoc.set('measPoints', measPoints);
				funcLoc.set('documents', documents);
				funcLoc.set('partners', partners);
				funcLoc.set('backendLongtext', backendLongtext);
				funcLoc.set('localLongtext', localLongtext);
				funcLoc.set('historicalOrders', histOrders);
				funcLoc.set('historicalNotifs', histNotifs);

				var cache = this.getCache();
				var dataGrade = lightVersion === true ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				cache.addToCache(funcLoc, dataGrade);

				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, funcLoc);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForFuncLoc of BaseFuncLocManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		//builds the raw object to store in the database out of a timeConf model
		buildDataBaseObjectForFuncLoc: function(funcLoc) {
			var retval = null;
			var ac = null;

		    try {
			    ac = AssetManagement.customer.core.Core.getAppConfig();

			    retval = { };
				retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['TPLNR'] = funcLoc.get('tplnr');
				retval['PLTXT'] = funcLoc.get('pltxt');
				retval['OBJNR'] = funcLoc.get('objnr');
				retval['SUBMT'] = funcLoc.get('submt');
				retval['IWERK'] = funcLoc.get('iwerk');
				retval['BAUJJ'] = funcLoc.get('baujj');
				retval['BAUMM'] = funcLoc.get('baumm');
				retval['BEBER'] = funcLoc.get('beber');
				retval['EQART'] = funcLoc.get('eqart');
				retval['EQFNR'] = funcLoc.get('eqfnr');
				retval['FLTYP'] = funcLoc.get('fltyp');
				retval['HERLD'] = funcLoc.get('herld');
				retval['HERST'] = funcLoc.get('herst');
				retval['POSNR'] = funcLoc.get('posnr');
				retval['INVNR'] = funcLoc.get('invnr');
				retval['KOSTL'] = funcLoc.get('kostl');
				retval['STORT'] = funcLoc.get('stort');
				retval['MSGRP'] = funcLoc.get('msgrp');
				retval['RBNR'] = funcLoc.get('rbnr');
				retval['TPLMA'] = funcLoc.get('tplma');
				retval['TYPBZ'] = funcLoc.get('typbz');
				retval['GEWRK'] = funcLoc.get('gewrk');
				retval['WERGW'] = funcLoc.get('wergw');
				retval['TXT_GEWRK'] = funcLoc.get('txtGewrk');
				retval['ADRNR'] = funcLoc.get('adrnr');
				retval['STRNO'] = funcLoc.get('strno');
				retval['STSMA'] = funcLoc.get('stsma');
        retval['ABCKZ'] = funcLoc.get('abckz');
        retval['GROES'] = funcLoc.get('groes');
        retval['BRGEW'] = funcLoc.get('brgew');
        retval['GEWEI'] = funcLoc.get('gewei');
        retval['USR01FLAG'] = funcLoc.get('usr01flag');
        retval['USR02FLAG'] = funcLoc.get('usr02flag');
        retval['USR03CODE'] = funcLoc.get('usr03code');
        retval['USR04CODE'] = funcLoc.get('usr04code');
        retval['USR05TXT'] = funcLoc.get('usr05txt');
        retval['USR06TXT'] = funcLoc.get('usr06txt');
        retval['USR07TXT'] = funcLoc.get('usr07txt');
        retval['USR09NUM'] = funcLoc.get('usr09num');

				retval['ANSDT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(funcLoc.get('ansdt'));
				retval['DATAB'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(funcLoc.get('datab'));

				retval['UPDFLAG'] = funcLoc.get('updFlag');
				retval['MOBILEKEY'] = funcLoc.get('mobileKey');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForFuncLoc of BaseFuncLocManager', ex);
			}

			return retval;
		}
	}
});