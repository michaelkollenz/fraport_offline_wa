Ext.define('AssetManagement.base.manager.BaseSDOrderTypeManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.SDOrderTypeCache',
        'AssetManagement.customer.model.bo.SDOrderType',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.SDOrderTypeCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseSDOrderTypeManager', ex);
			}
			
			return retval;
		},

		//public methods
		getSDOrderTypes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			       //it can, so return just this store
			       eventController.requestEventFiring(retval, fromCache);
			       return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			       model: 'AssetManagement.customer.model.bo.SDOrderType',
			       autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildSDOrderTypeStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrderTypes of BaseSDOrderTypeManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('SCENARIO', AssetManagement.customer.model.bo.UserInfo.getInstance().get('mobileScenario'));
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_SD_ORD_TYP', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_SD_ORD_TYP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderTypes of BaseSDOrderTypeManager', ex);
			}
	
			return retval;
		},
		
        getSDOrderType: function(auart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(auart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(auart);
		
		        if(fromCache) {
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var sdOrderType = me.buildSDOrderTypeFromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(sdOrderType);
						eventController.requestEventFiring(retval, sdOrderType);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderType of BaseSDOrderTypeManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('SCENARIO', AssetManagement.customer.model.bo.UserInfo.getInstance().get('mobileScenario'));
				keyMap.add('AUART', auart);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_SD_ORD_TYP', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_SD_ORD_TYP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderType of BaseSDOrderTypeManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildSDOrderTypeFromDataBaseQuery: function(eventArgs) {
			var retval = null;
								
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildSDOrderTypeFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderTypeFromDataBaseQuery of BaseSDOrderTypeManager', ex);
			}
			
			return retval;
		},
		
		buildSDOrderTypeStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var sdOrderType = this.buildSDOrderTypeFromDbResultObject(cursor.value);
						store.add(sdOrderType);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderTypeStoreFromDataBaseQuery of BaseSDOrderTypeManager', ex);
			}
		},
		
		buildSDOrderTypeFromDbResultObject: function(dbResult) {
	        var retval = null;
	        	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.SDOrderType', {
					scenario: dbResult['SCENARIO'],
			     	auart: dbResult['AUART'],
				    augru: dbResult['AUGRU'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['SCENARIO'] + dbResult['AUART']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderTypeFromDbResultObject of BaseSDOrderTypeManager', ex);
			}
						
			return retval;
		}
	}
});