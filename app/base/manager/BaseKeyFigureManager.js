Ext.define('AssetManagement.base.manager.BaseKeyFigureManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.KeyFigureCache',
        'AssetManagement.customer.model.bo.KeyFigure',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.KeyFigureCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseKeyFigureManager', ex);
			}
			
			return retval;
		},
		
		//public
		getKeyFigures: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
				    //it can, so return just this store
				    eventController.requestEventFiring(retval, fromCache);
				    return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.KeyFigure',
			        autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildKeyFigureStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFigures of BaseKeyFigureManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_KEYFIGURE', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_KEYFIGURE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFigures of BaseKeyFigureManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		getKeyFigure: function(stagr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stagr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
	        	var fromCache = cache.getFromCache(stagr);
		
		        if(fromCache) {
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var keyFigure = me.buildKeyFigureDataBaseQuery.call(me, eventArgs);
						cache.addToCache(keyFigure);
						eventController.requestEventFiring(retval, keyFigure);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFigure of BaseKeyFigureManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('STAGR', stagr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_KEYFIGURE', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_KEYFIGURE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFigure of BaseKeyFigureManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildKeyFigureDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildKeyFigureFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildKeyFigureDataBaseQuery of BaseKeyFigureManager', ex);
			}
			
			return retval;
		},
		
		buildKeyFigureStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var keyFigure = this.buildKeyFigureFromDbResultObject(cursor.value);
						store.add(keyFigure);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildKeyFigureStoreFromDataBaseQuery of BaseKeyFigureManager', ex);
			}
		},
		
		buildKeyFigureFromDbResultObject: function(dbResult) {
	        var retval = null;
	        	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.KeyFigure', {
					stagr: dbResult['STAGR'],
					msehi: dbResult['MSEHI'],
					bezei: dbResult['BEZEI'],
					kokrs: dbResult['KOKRS'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['STAGR']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildKeyFigureFromDbResultObject of BaseKeyFigureManager', ex);
			}
			
			return retval;
		}
	}
});