Ext.define('AssetManagement.base.manager.BasePriorityManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Priority',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

	inheritableStatics: {
		//public methods
		getPriorities: function(artpr, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(artpr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var fromCache = this.getPriorityStoreFromCache(artpr);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);

					return retval;
				}

				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Priority',
					autoLoad: false
				});

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

						me.buildPrioritiesStoreFromDataBaseQuery.call(me, theStore, eventArgs);

						if(done) {
							if(theStore.getCount() === 0)
								theStore = null;

							me.addToCache(artpr, theStore);
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPriorities of BasePriorityManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('ARTPR', artpr);

				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_PRIORITIES', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_PRIORITIES', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPriorities of BasePriorityManager', ex);
			}

			return retval;
		},

		getPriority: function(artpr, priok, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(artpr) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priok)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var fromCache = this.getSinglePriorityFromCache(artpr, priok);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var priority = me.buildPriorityFromDataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, priority);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPriority of BasePriorityManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('ARTPR', artpr);
				keyMap.add('PRIOK', priok);

				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_PRIORITIES', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_PRIORITIES', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPriority of BasePriorityManager', ex);
			}

			return retval;
		},

		//private methods
		buildPriorityFromDataBaseQuery: function(eventArgs) {
			var retval = null;

			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildPriorityFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPriorityFromDataBaseQuery of BasePriorityManager', ex);
			}

			return retval;
		},

		buildPrioritiesStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var priority = this.buildPriorityFromDbResultObject(cursor.value);
						store.add(priority);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPrioritiesStoreFromDataBaseQuery of BasePriorityManager', ex);
			}
		},

		buildPriorityFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Priority', {
					artpr: dbResult['ARTPR'],
					priok: dbResult['PRIOK'],
					priokx: dbResult['PRIOKX'],

				    updFlag: dbResult['UPDFLAG']
				});

				retval.set('id', dbResult['ARTPR'] + dbResult['PRIOK']);

			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPriorityFromDbResultObject of BasePriorityManager', ex);
			}

			return retval;
		},

		//cache administration
		_prioritiesCache: null,

		getCache: function() {
	        try {
	        	if(!this._prioritiesCache) {
					this._prioritiesCache = Ext.create('Ext.util.HashMap');

					this.initializeCacheHandling();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BasePriorityManager', ex);
			}

			return this._prioritiesCache;
		},

		clearCache: function() {
	        try {
			    this.getCache().clear();
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BasePriorityManager', ex);
		    }
		},

		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BasePriorityManager', ex);
			}
		},

		addToCache: function(artpr, priorityStore) {
			try {
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(artpr))
					return;

				this.getCache().add(artpr, priorityStore);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BasePriorityManager', ex);
			}
		},

		getPriorityStoreFromCache: function(artpr) {
			var retval = null;

			try {
				retval = this.getCache().get(artpr);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPriorityStoreFromCache of BasePriorityManager', ex);
			}

			return  retval;
		},

		getSinglePriorityFromCache: function(artpr, priok) {
			var priority = null;

			try {
				var artprStore = this.getCache().get(artpr);

				if(artprStore)
					priority = artprStore.getById(artpr + priok);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSinglePriorityFromCache of BasePriorityManager', ex);
			}

			return priority;
		}
	}
});