Ext.define('AssetManagement.base.manager.BaseCustReasonManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.CustReasonCache',
        'AssetManagement.customer.model.bo.CustReason',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.CustReasonCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustReasonManager', ex);
			}
			
			return retval;
		},
		
		//public
		getCustReasons: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
		        	//it can, so return just this store
		        	eventController.requestEventFiring(retval, fromCache);
		        	return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
		        	model: 'AssetManagement.customer.model.bo.CustReason',
		        	autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCustReasonStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustReasons of BaseCustReasonManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CUSTREASONS', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTREASONS', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustReasons of BaseCustReasonManager', ex);
			}
	
			return retval;
		}, 
		
		
	    getCustReason: function(werk, grund, useBatchProcessing) {
			var retval = -1;
			
		    try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werk) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(grund)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(werk +  grund);
		
		        if(fromCache) {
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var custReason = me.buildCustReasonFromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(custReason);
						eventController.requestEventFiring(retval, custReason);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustReason of BaseCustReasonManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
	
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('WERK', werk);
				keyMap.add('GRUND', grund);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_CUSTREASONS', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTREASONS', keyRange, successCallback, null, useBatchProcessing);
		    } catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustReason of BaseCustReasonManager', ex);
		    }
	
		    return retval;
	    },
		
	    //private
	    buildCustReasonFromDataBaseQuery: function(eventArgs) {
	    	var retval = null;
	    	
	    	try {
	    		if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCustReasonFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustReasonFromDataBaseQuery of BaseCustReasonManager', ex);
		    }
			
			return retval;
	    },
	
	    buildCustReasonStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var custReason = this.buildCustReasonFromDbResultObject(cursor.value);
						store.add(custReason);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustReasonStoreFromDataBaseQuery of BaseCustReasonManager', ex);
		    }
	    },
	
	    buildCustReasonFromDbResultObject: function(dbResult) {
	        var retval = null;
	        	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.CustReason', {
					werk: dbResult['WERKS'],
					grund: dbResult['GRUND'],
				    grdtx: dbResult['GRDTX'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['WERKS'] + dbResult['GRUND']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustReasonFromDbResultObject of BaseCustReasonManager', ex);
		    }			
			
			return retval;
		}
	}
});