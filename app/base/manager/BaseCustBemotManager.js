Ext.define('AssetManagement.base.manager.BaseCustBemotManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.CustBemotCache',
        'AssetManagement.customer.model.bo.CustBemot',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//private
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.CustBemotCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustBemotManager', ex);
			}
			
			return retval;
		},
	
		//public
		getCustBemots: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
				var cache = this.getCache();
				var fromCache = cache.getStoreForAll();
				
				if(fromCache) {
					//it can, so return just this store
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.CustBemot',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCustBemotStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustBemots of BaseCustBemotManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CUSTBEMOT', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTBEMOT', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustBemots of BaseCustBemotManager', ex);
			}
	
			return retval;
		},
		
		getCustBemot: function(bemot, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(bemot)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCache(bemot);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var custBemot = me.buildCustBemotFromDataBaseQuery.call(me, eventArgs);
						
						cache.addToCache(custBemot);
						eventController.requestEventFiring(retval, custBemot);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustBemot of BaseCustBemotManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('BEMOT', bemot);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_CUSTBEMOT', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTBEMOT', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustBemot of BaseCustBemotManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCustBemotFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
			    if(eventArgs) {
				   if(eventArgs.target.result) {
					  retval = this.buildCustBemotFromDbResultObject(eventArgs.target.result.value);					
				   }
			    }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustBemotFromDataBaseQuery of BaseCustBemotManager', ex);
			}
			return retval;
		},
		
		buildCustBemotStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var custBemot = this.buildCustBemotFromDbResultObject(cursor.value);
						store.add(custBemot);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustBemotStoreFromDataBaseQuery of BaseCustBemotManager', ex);
			}
		},
		
		buildCustBemotFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.CustBemot', {
					bemot: dbResult['BEMOT'],
					bemottxt: dbResult['BEMOT_TXT'],
					
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['BEMOT']);
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustBemotFromDbResultObject of BaseCustBemotManager', ex);
		    }
			return retval;
		}
	}
});