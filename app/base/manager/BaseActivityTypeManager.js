Ext.define('AssetManagement.base.manager.BaseActivityTypeManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.ActivityTypeCache',
        'AssetManagement.customer.model.bo.ActivityType',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//private
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.ActivityTypeCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseActivityTypeManager', ex);
			}
			
			return retval;
		},
	
		//public methods
		getActivityTypes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can deliver the complete dataset
				var cache = this.getCache();
				var fromCache = cache.getStoreForAll();
				
				if(fromCache) {
					//it can, so return just this store
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ActivityType',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildActivityTypeStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActivityTypes of BaseActivityTypeManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_ACTIVITY_TYPE', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ACTIVITY_TYPE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActivityTypes of BaseActivityTypeManager', ex);
			}
	
			return retval;
		}, 
		
		getActivityType: function(arbpl, werks, kokrs, lstar, useBatchProcessing) {
			var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(arbpl) ||
						AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks) ||
							AssetManagement.customer.utils.StringUtils.isNullOrEmpty(kokrs) ||
								AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lstar)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCache(arbpl + werks + kokrs + lstar);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						activityType = me.buildActivityTypeFromDataBaseQuery.call(me, eventArgs);
						
						cache.addToCache(activityType);
						eventController.requestEventFiring(retval, activityType);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActivityType of BaseActivityTypeManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('ARBPL', arbpl);
				keyMap.add('WERKS', werks);
				keyMap.add('KOKRS', kokrs);
				keyMap.add('LSTAR', lstar);
		
	            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_ACTIVITY_TYPE', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ACTIVITY_TYPE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActivityType of BaseActivityTypeManager', ex);
			}
			
			return retval;
		},
	
	    //private methods
		buildActivityTypeFromDataBaseQuery: function(eventArgs) {
	    	var retval = null;
	    	
	    	try {
	    		if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildActivityTypeFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildActivityTypeFromDataBaseQuery of BaseActivityTypeManager', ex);
			}
			return retval;
		},
		
		buildActivityTypeStoreFromDataBaseQuery: function(store, eventArgs) {
	       try {
	    	   if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var activityType = this.buildActivityTypeFromDbResultObject(cursor.value);
						store.add(activityType);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildActivityTypeStoreFromDataBaseQuery of BaseActivityTypeManager', ex);
			}
		}, 
		
		buildActivityTypeFromDbResultObject: function(dbResult) {
	       var retval = null;

	       try {
			   retval = Ext.create('AssetManagement.customer.model.bo.ActivityType', {
				  arbpl: dbResult['ARBPL'],
				  werks: dbResult['WERKS'],
			      kokrs: dbResult['KOKRS'],
			      lstar: dbResult['LSTAR'],
			      ktext: dbResult['KTEXT'],
			      leinh: dbResult['LEINH'],
			      updFlag: dbResult['UPDFLAG']
			   });
			
			   retval.set('id', dbResult['ARBPL'] + dbResult['WERKS'] + dbResult['KOKRS'] + dbResult['LSTAR']);
		   } catch(ex) {
			  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildActivityTypeFromDbResultObject of BaseActivityTypeManager', ex);
		   }	
		   return retval;
		}
	}
});