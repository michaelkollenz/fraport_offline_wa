Ext.define('AssetManagement.base.manager.BaseSDOrderItemManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.SDOrderItemCache',
        'AssetManagement.customer.model.bo.SDOrderItem',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		EVENTS: {
			SDORDER_ITEM_ADDED: 'SDOrderItemAdded',
			SDORDER_ITEM_CHANGED: 'SDOrderItemChanged',
			SDORDER_ITEM_DELETED: 'SDOrderItemDeleted'
		},
		
		//protected
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.SDOrderItemCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getSDOrderItems: function(withDependendData, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForSDOrderItems(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.SDOrderItem',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildSDOrderItemStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
								
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForSDOrderItems.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrderItems of BaseSDOrderItemManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_SDITEM', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_SDITEM', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrderItems of BaseSDOrderItemManager', ex);
			}
		
			return retval;
		},
		
		getSDOrderItemsForSDOrder: function(vbeln, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();		
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForSDOrder(vbeln, cache.self.DATAGRADES.FULL);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForSDOrder(vbeln, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForSDOrder(vbeln, cache.self.DATAGRADES.MEDIUM);
					this.loadDependendDataForSDOrderItems(baseStore, retval);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.SDOrderItem',
						autoLoad: false
					});
				
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildSDOrderItemStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForSDOrder(vbeln, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForSDOrder(vbeln, cache.self.DATAGRADES.MEDIUM);
								me.loadDependendDataForSDOrderItems.call(me, baseStore, retval);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrderItemsForSDOrder of BaseSDOrderItemManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('VBELN', vbeln);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_SDITEM', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_SDITEM', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrderItemsForSDOrder of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		getNewSDOrderItem: function(sdOrder) {
			var retval = null;

			try {
				retval = Ext.create('AssetManagement.customer.model.bo.SDOrderItem', {
					 vbeln: sdOrder ? sdOrder.get('vbeln') : '',
					 posnr: '',
					 matnr: '',
					 zmeng: '1',
					 zieme: 'ST',
					 arktx: '',
					 abgru: '',
					 lfgsa: 'A',
					 sernr: '',
					 charge: '',
					 werks: '',
					 lgort: '',
					                     
				     updFlag: 'I',
					 mobileKey: sdOrder ? sdOrder.get('mobileKey') : '',
					 childKey: ''
				});
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewSDOrderItem of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		saveSDOrderItem: function(sdOrderItem) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrderItem) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = sdOrderItem.get('updFlag');
				
				var isNewSDItem = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrderItem.get('posnr'));
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;
	
				//update flag management
				this.manageUpdateFlagForSaving(sdOrderItem);
			
				//check if the sd order item already has a posnr
				var requiresNewPosnr = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrderItem.get('posnr'));
				
				var me = this;
				var saveFunction = function(nextPosnr) {
					try {
						if(requiresNewPosnr && nextPosnr !== -1) {
							posnr = AssetManagement.customer.utils.StringUtils.padLeft(nextPosnr, '0', 6);
							sdOrderItem.set('posnr', posnr);
							
							sdOrderItem.set('id', sdOrderItem.get('vbeln') + posnr);
						}											
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrderItem.get('posnr'))) {
							eventController.fireEvent(retval, false);
							return;
						}
						
						//mobilekey management
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrderItem.get('childKey')))
							sdOrderItem.set('childKey', AssetManagement.customer.manager.KeyManager.createKey([ sdOrderItem.get('vbeln'), sdOrderItem.get('posnr') ]));
							
						//set the id
						sdOrderItem.set('id', sdOrderItem.get('vbeln') + sdOrderItem.get('posnr'));
							
						//status management
						if(isInsert === true)
							sdOrderItem.set('lfgsa', 'A');
						
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								
								if(success === true) {
									//try to find the selected material for this item
									var materialCallback = function(material) {
										try {
											sdOrderItem.set('material', material ? material : null)
										
											if(isNewSDItem) {
												//do not specify a datagrade, so all depenend data will loaded, when necessary
												me.getCache().addToCache(sdOrderItem);
											}
								
											var eventType = isNewSDItem ? me.EVENTS.SDORDER_ITEM_ADDED : me.EVENTS.SDORDER_ITEM_CHANGED;
											eventController.fireEvent(eventType, sdOrderItem);
											
											eventController.fireEvent(retval, true);
										} catch(ex) {
											AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItem of BaseSDOrderItemManager', ex);
										}
									};
									
									var eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(sdOrderItem.get('matnr'));
									
									if(eventId > 0) {
										AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, materialCallback);
									} else {
										eventController.fireEvent(retval, true);
									}
								} else {
									eventController.fireEvent(retval, false);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItem of BaseSDOrderItemManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForSDOrderItem(sdOrderItem);						

						if(sdOrderItem.get('updFlag') == 'I') {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_SDITEM', toSave, callback, callback);
						} else if(sdOrderItem.get('updFlag') == 'U') {
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_SDITEM', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItem of BaseSDOrderItemManager', ex);
					}
				};
				
				if(!requiresNewPosnr) {
					saveFunction();
				} else {
					eventId = this.getNextPosnr(sdOrderItem);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItem of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		saveSDOrderItems: function(sdOrderItems) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrderItems || sdOrderItems.getCount() === 0) {
					eventController.requestEventFiring(retval, true);
					return retval;
				}
				
				var me = this;
				
				//save each sd order item
				var numberOfItemsToSave = sdOrderItems.getCount();
				var pendingSaveLoops = numberOfItemsToSave;
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					try {
						if(errorOccurred === true && reported === false) {
							eventController.fireEvent(retval, false);
							reported = true;
						} else if(pendingSaveLoops === 0 && errorOccurred === false) {
							//reorder the items
							sdOrderItems.sort('posnr', 'ASC');
							
							eventController.fireEvent(retval, true);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItems of BaseSDOrderItemManager', ex);
					}
				};

				//passing all requests without waiting for the former one to complete won't work because the posnr will be the same for all
				var saveCallback = function(success) {
					pendingSaveLoops--;
				
					try {
						errorOccurred = !success;
						
						if(success === true && pendingSaveLoops > 0) {
							//pass the next save request
							var eventId = me.saveSDOrderItem(sdOrderItems.getAt(numberOfItemsToSave - pendingSaveLoops));
							
							if(eventId > 0) {
								eventController.registerOnEventForOneTime(eventId, saveCallback);
							} else {
								errorOccurred === true;
							}
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItems of BaseSDOrderItemManager', ex);
					} finally {
						completeFunction();
					}
				};

				//begin saving the first item
				var eventId = this.saveSDOrderItem(sdOrderItems.getAt(0));
							
				if(eventId > 0) {
					eventController.registerOnEventForOneTime(eventId, saveCallback);
				} else {
					errorOccurred === true;
					completeFunction();
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItems of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		deleteSDOrderItem: function(sdOrderItem, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrderItem) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(sdOrderItem);
							me.getCache().removeFromCache(sdOrderItem);
							
							eventController.fireEvent(me.EVENTS.SDORDER_ITEM_DELETED, sdOrderItem);
						}
						
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItem of BaseSDOrderItemManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the sdItem to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_SDITEM', sdOrderItem);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_SDITEM', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItem of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		deleteSDOrderItems: function(sdOrderItems, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrderItems || sdOrderItems.getCount() === 0) {
					eventController.requestEventFiring(retval, true);
					return retval;
				}
				
				//delete each sd order item
				var pendingRequests = 0;			
				var reported = false;
				
				var deleteCallback = function(success) {
					try {
						pendingRequests--;
					
						if(success === false && reported === false) {
							eventController.fireEvent(retval, false);
							reported = true;
						} else if(pendingRequests === 0 && success === true) {
							eventController.fireEvent(retval, true);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItems of BaseSDOrderItemManager', ex);
					}
				};
				
				var eventId = '';
				sdOrderItems.each(function(sdOrderItem) {
					eventId = this.deleteSDOrderItem(sdOrderItem);
					
					if(eventId > 0) {
						pendingRequests++;
						eventController.registerOnEventForOneTime(eventId, deleteCallback);
					} else {
						deleteCallback(false);
					}
				}, this);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItems of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		//private methods	
		buildSDOrderItemStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var sdOrderItem = this.buildSDOrderItemFromDbResultObject(cursor.value);
						store.add(sdOrderItem);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderItemStoreFromDataBaseQuery of BaseSDOrderItemManager', ex);
			}
		},
		
		buildSDOrderItemFromDbResultObject: function(dbResult) {
			var retval = null;
		
			try {
				retval = Ext.create('AssetManagement.customer.model.bo.SDOrderItem', {
					 vbeln: dbResult['VBELN'],
					 posnr: dbResult['POSNR'],
					 matnr: dbResult['MATNR'],
					 zmeng: dbResult['ZMENG'],
					 zieme: dbResult['ZIEME'],
					 arktx: dbResult['ARKTX'],
					 abgru: dbResult['ABGRU'],
					 lfgsa: dbResult['LFGSA'],
					 sernr: dbResult['SERNR'],
					 charge: dbResult['CHARGE'],
					 werks: dbResult['WERKS'],
					 lgort: dbResult['LGORT'],
					 usr1: dbResult['USR1'],
					 usr2: dbResult['USR2'],
					                     
				     updFlag: dbResult['UPDFLAG'],
					 mobileKey: dbResult['MOBILEKEY'],
					 childKey: dbResult['CHILDKEY']
				});
				
				retval.set('id', retval.get('vbeln') + retval.get('posnr'));
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderItemFromDbResultObject of BaseSDOrderItemManager', ex);
			}
			
			return retval;
		},
		
		loadListDependendDataForSDOrderItems: function(eventIdToFireWhenComplete, sdItems) {
			try {
				//do not continue if there is no data
				if(sdItems.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, sdItems);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var materials = null;
				
				var pendingRequests = 0;			
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccurred === false) {
						me.assignListDependendDataForSDOrderItems.call(me, eventIdToFireWhenComplete, sdItems, materials);
					}
				};
				
				//get materials
				pendingRequests++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccurred = materialStore === undefined;
					
					materials = materialStore;
					pendingRequests--;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				if(pendingRequests > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForSDOrderItems of BaseSDOrderItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all neccessary data has been collected
		assignListDependendDataForSDOrderItems: function(eventIdToFireWhenComplete, sdItems, materials) {
			try {
				sdItems.each(function (item, index, length) {
					//assign materials
					if(materials) {
						var matnr = item.get('matnr');
						
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								item.set('material', material);
								return false;
							}
						});
					}
				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(sdItems, cache.self.DATAGRADES.LOW);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForSDOrderItems of BaseSDOrderItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForSDOrderItems: function(sdOrderItems, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(sdOrderItems.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, sdOrderItems);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var materials = null;
				var backendLongtexts = null;
				var localLongtexts = null;
				
				var pendingRequests = 0;			
				var errorOccurred = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccurred === false) {
						me.assignDependendDataForSDOrderItems.call(me, eventIdToFireWhenComplete, sdOrderItems,
								materials, backendLongtexts, localLongtexts);
					}
				};
				
				//get materials
				pendingRequests++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccurred = materialStore === undefined;
					
					materials = materialStore;
					pendingRequests--;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				//TODO make dependend of customizing parameters --- this definitely will increase performance
				
				//DISABLED
//				//get local and backendlongtext (for all sd items)
//				if(true) {
//					done++;
//					
//					var localLongtextSuccessCallback = function(localLT, backendLT) {
//						errorOccurred = localLT === undefined || localLT === backendLT;
//						localLongtext = localLT;
//						backendLongtext = backendLT;
//						counter++;
//						
//						completeFunction();
//					};
//					eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(order, true);
//					eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
//				}
				
				if(pendingRequests > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForSDOrderItems of BaseSDOrderItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForSDOrderItems: function(eventIdToFireWhenComplete, sdOrderItems, materials,
														backendLongtexts, localLongtexts) {
			try {
				//remove all objects from the stores, which have been assigned to it's sd order item
				//therefore following loops will have less iterations
				sdOrderItems.each(function (item, index, length) {
					//assign materials
					if(materials) {
						var matnr = item.get('matnr');
						
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								item.set('material', material);
								return false;
							}
						});
					}
				});
				
				var cache = this.getCache();
				var vbeln = sdOrderItems.getAt('0').get('vbeln');
				cache.addToCacheForSDOrder(vbeln, sdOrderItems, cache.self.DATAGRADES.FULL);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getFromCacheForSDOrder(vbeln, cache.self.DATAGRADES.FULL); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForSDOrderItems of BaseSDOrderItemManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//builds the raw object to store in the database out of a sd order item model
		buildDataBaseObjectForSDOrderItem: function(sdOrderItem) {
			var retval = null;
			var ac = null;
			
			try {
			    ac = AssetManagement.customer.core.Core.getAppConfig();
		
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['VBELN'] = sdOrderItem.get('vbeln');
				retval['POSNR'] = sdOrderItem.get('posnr');
				retval['MATNR'] = sdOrderItem.get('matnr');
				retval['ZMENG'] = sdOrderItem.get('zmeng');
				retval['ZIEME'] = sdOrderItem.get('zieme');
				retval['ARKTX'] = sdOrderItem.get('arktx');
				retval['ABGRU'] = sdOrderItem.get('abgru');
				retval['LFGSA'] = sdOrderItem.get('lfgsa');
				retval['SERNR'] = sdOrderItem.get('sernr');
				retval['CHARGE'] = sdOrderItem.get('charge');
				retval['WERKS'] = sdOrderItem.get('werks');
				retval['LGORT'] = sdOrderItem.get('lgort');
				retval['USR1'] = sdOrderItem.get('usr1');
				retval['USR2'] = sdOrderItem.get('usr2');
				
				retval['UPDFLAG'] = sdOrderItem.get('updFlag');
				retval['MOBILEKEY'] = sdOrderItem.get('mobileKey');
				retval['CHILDKEY'] = sdOrderItem.get('childKey');
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForSDOrderItem of BaseSDOrderItemManager', ex);
			}
				
			return retval;
		},
		
		getNextPosnr: function(sdOrderItem) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrderItem) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxPosnr = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
						    var cursor = eventArgs.target.result;
                            
						    var posnrValue = cursor.value['POSNR'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(posnrValue, '%')) {
                                //cut of the percent sign
							    posnrValue = posnrValue.substr(1);
                                var curPosnrValue = parseInt(posnrValue);
							
							    if(!isNaN(curPosnrValue))
							    {
								    if(curPosnrValue > maxPosnr)
									    maxPosnr = curPosnrValue;
							    } 
                            }

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
						    var nextLocalCounterNumberValue = maxPosnr + 10;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 5);
						    eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextPosnr of BaseSDOrderItemManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('VBELN', sdOrderItem.get('vbeln'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_SDITEM', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_SDITEM', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextPosnr of BaseSDOrderItemManager', ex);
			}
			
			return retval;			
		}
	}
});