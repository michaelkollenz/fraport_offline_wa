Ext.define('AssetManagement.base.manager.BaseDMSDocumentManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.DMSDocument',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.NetworkHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

	inheritableStatics: {
		//public methods
		getDmsDocumentsForObject: function(objnr, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var keyArray = this.getRefTypeAndRefObjectKeys(objnr);
				var refType = keyArray[0];
				var refObject = keyArray[1];

				if(keyArray.length !== 2 || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(refType) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(refObject)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var dmsDocs = this.getFromCache(refType + refObject);

				if(dmsDocs) {
					eventController.requestEventFiring(retval, dmsDocs);
					return retval;
				}

				dmsDocs = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.DMSDocument',
					autoLoad: false
				});

				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

						me.buildDmsDocsStoreFromDataBaseQuery.call(me, dmsDocs, eventArgs);

						if(done) {
							me.addToCache(objnr, dmsDocs);
							eventController.fireEvent(retval, dmsDocs);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDmsDocumentsForObject of BaseDMSDocumentManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('REF_TYPE', refType);
				keyMap.add('REF_OBJECT', refObject);

				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_DMS_DOCUMENT', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_DMS_DOCUMENT', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDmsDocumentsForObject of BaseDMSDocumentManager', ex);
			    retval = -1;
			}

			return retval;
		},

		downloadDMSDocument: function(dmsDoc) {
		    try {
		        var url = this.getDMSDownloadUrl();

		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(url)) {
		            var requestPostValues = this.getRequestPostValues(dmsDoc);

		            if (requestPostValues)
		                AssetManagement.customer.helper.NetworkHelper.downloadFile(url, requestPostValues);
		        }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside downloadDMSDocument of BaseDMSDocumentManager', ex);
			}
		},

		//private methods
		getRefTypeAndRefObjectKeys: function(objnr) {
			var retval = new Array();

			try {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr) && objnr.length > 3) {
					var type = objnr.substring(0, 2);

					if(type === 'QM')
						type = 'NO';
					else if(type === 'IE')
						type = 'EQ';

					var key = objnr.substring(2);

					retval.push(type);
					retval.push(key);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRefTypeAndRefObjectKeys of BaseDMSDocumentManager', ex);
			}

			return retval;
		},

		buildDmsDocsStoreFromDataBaseQuery: function(store, eventArgs) {
            try {
            	if(eventArgs && eventArgs.target) {
    				var cursor = eventArgs.target.result;
    			    if (cursor) {
    				    var dmsDoc = this.buildDmsDocFromDbResultObject(cursor.value);
    					store.add(dmsDoc);

    			    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
    			    }
    			}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDmsDocsStoreFromDataBaseQuery of BaseDMSDocumentManager', ex);
			}
		},

		buildDmsDocFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.DMSDocument', {
					//base class properties (file)
					fileId: dbResult['FILE_ID'],
					fileName: dbResult['DOCFILE'],
					fileType: dbResult['WSAPPLICATION'],
					fileOrigin: 'online',
					fileDescription: dbResult['FILE_DESCR'],
					contentAsArrayBuffer: null,
					createdAt: undefined,
					lastModified: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['VALIDFROMDATE']),

					//own properties
					refType: dbResult['REF_TYPE'],
					refObject: dbResult['REF_OBJECT'],
					documentType: dbResult['DOCUMENTTYPE'],
					documentNumber: dbResult['DOCUMENTNUMBER'],
					docId: dbResult['FILE_ID'],
					storageCategory: dbResult['STORAGECATEGORY'],
					documentVersion: dbResult['DOCUMENTVERSION'],
					documentPart: dbResult['DOCUMENTPART'],
					validFromDate: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['VALIDFROMDATE']),
					wsApplication: dbResult['WSAPPLICATION'],
					docFile: dbResult['DOCFILE'],
					fileDescr: dbResult['FILE_DESCR'],
					docDescr: dbResult['DOC_DESCR'],
					language: dbResult['LANGUAGE'],
					fileSize: dbResult['FILE_SIZE'],
					laboratory: dbResult['LABORATORY'],

				    updFlag: dbResult['UPDFLAG']

				});
				retval.set('id', dbResult['REF_TYPE'] + dbResult['REF_OBJECT'] + dbResult['DOCUMENTTYPE'] +
					dbResult['DOCUMENTNUMBER'] + dbResult['FILE_ID'] + dbResult['STORAGECATEGORY']);

				//set the file size in kB
				var fileSize = parseInt(dbResult['FILE_SIZE']);

				if(fileSize !== NaN) {
					retval.set('fileSize', fileSize / 1024.0);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDmsDocFromDbResultObject of BaseDMSDocumentManager', ex);
			}

			return retval;
		},

		getDMSDownloadUrl: function () {
			var retval = '';

			try {
			    var ac = AssetManagement.customer.core.Core.getAppConfig();

			    var protocol = ac.getSyncProtocol();
			    var getSyncGateway = ac.getSyncGateway();
			    var gatewayPort = ac.getSyncGatewayPort();
			    var gatewayIdent = ac.getSyncGatewayIdent();

                var gatewayService = null;
				var syncAliasService = ac.getGatewayAliasSync();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAliasService)) {
                    gatewayService = ac.getGatewayServiceSync();
                } else {
                    gatewayService = syncAliasService;
                }

			    var controller = ac.getGatewayControllerFileDownload();

			    var anythingMissing = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(protocol) ||
                                        AssetManagement.customer.utils.StringUtils.isNullOrEmpty(getSyncGateway) ||
                                            //AssetManagement.customer.utils.StringUtils.isNullOrEmpty(gatewayPort) || COmmenting port due to the fact that the port might be missing
                                                AssetManagement.customer.utils.StringUtils.isNullOrEmpty(gatewayIdent) ||
                                                    AssetManagement.customer.utils.StringUtils.isNullOrEmpty(gatewayService) ||
                                                        AssetManagement.customer.utils.StringUtils.isNullOrEmpty(controller);
                
			    if (!anythingMissing) {

                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAliasService)) {
                        retval = protocol + "://" + getSyncGateway + ":" + gatewayPort + "/sap" + gatewayIdent + gatewayService + controller;
                    } else {
                        retval = protocol + "://" + getSyncGateway + ":" + gatewayPort + gatewayService + controller;
                    }


			    }
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDMSDownloadUrl of BaseDMSDocumentManager', ex);
			}

			return retval;
		},

		getRequestPostValues: function (dmsDoc) {
		    var retval = null;

		    try {
		        if (dmsDoc) {
		            var ac = AssetManagement.customer.core.Core.getAppConfig();

		            var requestPostValues = {
		                'sap-client': ac.getMandt(),
		                'sap-user': ac.getUserId(),
		                'sap-password': ac.getUserPassword()
		            };

		            requestPostValues['sap-client'] = ac.getMandt();

		            if (dmsDoc.get('documentType') === 'ARC') {
		                requestPostValues['pa_arc_doc_id'] = dmsDoc.get('docId');
		                requestPostValues['pa_obj_key'] = dmsDoc.get('refObject');
		                requestPostValues['pa_objecttype'] = dmsDoc.get('docDescr');
		                requestPostValues['pa_documenttype'] = dmsDoc.get('documentType');
		            } else {
		                requestPostValues['pa_file_id'] = dmsDoc.get('docId');
		                requestPostValues['pa_storage_cat'] = dmsDoc.get('storageCategory');
		            }

		            retval = requestPostValues;
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRequestPostValues of BaseDMSDocumentManager', ex);
		    }

		    return retval;
		},

		//cache administration
		_dmsDocsCache: null,

		getCache: function() {
	        try {
	        	if(!this._dmsDocsCache) {
					this._dmsDocsCache = Ext.create('Ext.util.HashMap');

					this.initializeCacheHandling();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseDMSDocumentManager', ex);
			}

			return this._dmsDocsCache;
		},

		//public methods
		clearCache: function() {
			try {
			    this.getCache().clear();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseDMSDocumentManager', ex);
			}
		},

		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseDMSDocumentManager', ex);
			}
		},

		addToCache: function(key, dmsDocsStore) {
	        try {
			     this.getCache().add(key, dmsDocsStore);
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseDMSDocumentManager', ex);
		    }
		},

		getFromCache: function(key) {
	        var retval = null;

	        try {
			    retval = this.getCache().get(key);
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseDMSDocumentManager', ex);
		    }

		    return retval;
		}
	}
});