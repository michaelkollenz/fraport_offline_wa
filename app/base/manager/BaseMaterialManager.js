Ext.define('AssetManagement.base.manager.BaseMaterialManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.MaterialCache',
        'AssetManagement.customer.model.bo.Material',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {	
	    //protected
        getCache: function() {
	       var retval = null;

	       try {
		       retval = AssetManagement.customer.manager.cache.MaterialCache.getInstance();
	       } catch(ex) {
	           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseMaterialManager', ex);
	       }
	
	       return retval;
        },

	    //public
        includeToCache: function (material) {
            try {
                this.getCache().addToCache(material);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside includeToCache of BaseMaterialManager', ex);
            }
        },
	
		//public methods
		getMaterials: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
				    //it can, so return just this store
				    eventController.requestEventFiring(retval, fromCache);
				    return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.Material',
			        autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildMaterialStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterials of BaseMaterialManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MATERIAL', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MATERIAL', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterials of BaseMaterialManager', ex);
			}
	
			return retval;
		},
		
		getMaterial: function(matnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(matnr);
		
		        if(fromCache) {
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var material = me.buildMaterialFromDataBaseQuery.call(me, eventArgs);
						
						//if nothing was found, and the passed material number had less than 18 chars
						//do another query with a padded value
						if(!material && matnr.length !== 18) {
							matnr = AssetManagement.customer.utils.StringUtils.padLeft(matnr, '0', 18);
							
							var secondTryCallback = function(materialOfSecondTry) {
								try {
									cache.addToCache(materialOfSecondTry);
									eventController.requestEventFiring(retval, materialOfSecondTry);
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterial of BaseMaterialManager', ex);
									eventController.requestEventFiring(retval, undefined);
								}
							};
						
							var eventId = me.getMaterial(matnr);
							eventController.registerOnEventForOneTime(eventId, secondTryCallback);
						} else {
							cache.addToCache(material);
							eventController.requestEventFiring(retval, material);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterial of BaseMaterialManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('MATNR', matnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MATERIAL', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MATERIAL', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterial of BaseMaterialManager', ex);
			}
			
			return retval;
		},

	    //cuts off leading zeros, if the passed material number contains numeric characters only
		trimMaterialNumber: function (matnr) {
		    var retval = '';

		    try {
		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
		            //cut off leading zeros, if the passed material number contains numeric characters only
		            if (matnr.match(/^\d+$/))
		                retval = AssetManagement.customer.utils.StringUtils.trimStart(matnr, '0');
		            else
		                retval = matnr;
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trimMaterialNumber of BaseMaterialManager', ex);
		    }

		    return retval;
		},

	    //brings a passed material number into format used by sap
		getFormattedMaterialNumber: function (materialNumberToFormat) {
		    var retval = '';

		    try {
		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(materialNumberToFormat)) {
		            //check, if the passed string contains number characters only
		            var containsNumCharsOnly = materialNumberToFormat.search(/^\d+$/) > -1;

		            if (containsNumCharsOnly) {
		                retval = AssetManagement.customer.utils.StringUtils.padLeft(materialNumberToFormat, '0', 18);
		            } else {
		                retval = materialNumberToFormat;
		            }
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFormattedMaterialNumber of BaseMaterialManager', ex);
		    }

		    return retval;
		},
		
		/**
		 * get materials from customizing for external confirmations
		 */
		getExtConfMaterialList: function() {
				var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				var extConfMaterialStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Material',
					autoLoad: false
				});
			
				var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
				var materials = funcPara.get('ext_conf_material'); 
				
				//next get all materials
				if(materials && materials.getCount() > 0) {
					var me = this;
					
					var materialAssignmentCallback = function(allMaterials) {
						try {
							if(allMaterials === undefined) {
								eventController.requestEventFiring(retval, undefined);
								return;
							}
						
							if(allMaterials && allMaterials.getCount() > 0) {
								if(materials && materials.getCount() > 0) {
									materials.each(function(extMaterial) {
										var matnr = extMaterial.get('fieldname');
										var material = materials.getById(id); 
										
										extConfMaterialStore.add(material);
									}, this);
								}
							}
							
							eventController.requestEventFiring(retval, extConfMaterialStore);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getExtConfMaterialList of BaseMaterialManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					}
									
					var eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(false);
					
					if(eventId > -1) {
						eventController.registerOnEventForOneTime(eventId, materialAssignmentCallback);
					} else {
						eventController.requestEventFiring(retval, undefined);
					}
				} else {
					eventController.requestEventFiring(retval, extConfMaterialStore);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getExtConfMaterialList of BaseMaterialManager', ex);
				retval = -1;
			}
			
			return retval;
		},

		//private
		buildMaterialFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildMaterialFromDbResultObject(eventArgs.target.result.value);
						this.addToCache(retval);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialFromDataBaseQuery of BaseMaterialManager', ex);
			}
			
			return retval;
		},
		
		buildMaterialStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var material = this.buildMaterialFromDbResultObject(cursor.value);
						store.add(material);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialStoreFromDataBaseQuery of BaseMaterialManager', ex);
			}
		},
		
		buildMaterialFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Material', {
					matnr: dbResult['MATNR'],
					meins: dbResult['MEINS'],
				    maktx: dbResult['MAKTX'],
				    labst: dbResult['LABST'],
				    brprs: dbResult['BRPRS'],
				    ean11: dbResult['EAN11'],
				    bismt: dbResult['BISMT'],
				    xchpf: dbResult['XCHPF'],
                    mtpos_mara: dbResult['MTPOS_MARA'],
                    sernp: dbResult['SERNP'],
                    prdha: dbResult['PRDHA'],
                    mtart: dbResult['MTART'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['MATNR']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialFromDbResultObject of BaseMaterialManager', ex);
			}
			
			return retval;
		}			
	}
});