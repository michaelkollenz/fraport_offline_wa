Ext.define('AssetManagement.base.manager.BaseStructureManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.model.bo.StructureElement',
        'Ext.data.TreeModel'
    ],
    
	currentEqui: null, 
	currentFloc: null,
				
	inheritableStatics: {
		equiList: null,
		flocList: null,
		mastList: null,
		materialList: null,
	
		/**
		 * 
		 * calls assignData for creating treeStore of the specific object
		 * loads all neccessary objectstores
		 */
		getStructure: function(object) {
			var retval = -1;
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var treeStore = Ext.create('Ext.data.TreeStore', {
					model: 'Ext.data.TreeModel',
					root: {
				        expanded: true
					},
					proxy: {
						type : 'memory'
					}
				});
				
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Equipment')
					this.currentEqui = object; 
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.FuncLoc')
					this.currentFloc = object; 
				
				//do not continue if there is no data
				if(!object) {
					eventController.fireEvent(retval, treeStore);
					return;
				}
				
				var me = this;
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Equipment')
							me.assignDependendDataForEqui.call(me, retval, object, treeStore);
						if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.FuncLoc')
							me.assignDependendDataForFloc.call(me, retval, object,treeStore);
					}
				};
				
				//get all masts
				done++;
				var materialsSuccessCallback = function(materialStore) {
					errorOccured = materialStore === undefined;
					
					mastList = materialStore;
					counter++;
					
					completeFunction();
				};
				
				var eventId = AssetManagement.customer.manager.MastManager.getMasts(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				//get all material
				done++;
				var materialsSuccessCallback = function(materialsStore) {
					errorOccured = materialsStore === undefined;
					
					materialList = materialsStore;
					counter++;
					
					completeFunction();
				};
				
				var eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);

				//get all equis
				done++;
				var equiSuccessCallback = function(equipments) {
					errorOccured = equipments=== undefined;
					
					equiList = equipments;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.EquipmentManager.getEquipments(false, true);
				eventController.registerOnEventForOneTime(eventId, equiSuccessCallback);
				
				//get all funcLocs
				done++;
				var flocSuccessCallback = function(funcLocs) {
					errorOccured = funcLocs === undefined;
					
					flocList = funcLocs;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(false, true);
				eventController.registerOnEventForOneTime(eventId, flocSuccessCallback);

				if(done > 0)
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				else
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, treeStore);
			} catch(ex) {
				retval = -1; 
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStructure of BaseStructureManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
			
			return retval;
		},
		
		/**
		 * 
		 * creates treestore nodes for equi object structure
		 */		
		assignDependendDataForEqui: function(eventIdToFireWhenComplete, equi, treeStore) {
			try {
				flocList.clearFilter(); 
			
				//first get superior funcLoc
				var currentElement = this.createNewStructure(equi); 
				var previousElement = currentElement; 
				
				var tempEqui = equi; 
				var tmpFloc = null; 
				//get superior equis
				while(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tempEqui.get('hequi')))
				{
					equiList.filterBy(function (record) {
						try {
						    if (record.get('equnr') === tempEqui.get('hequi')) {
						    	return true; 
						    }
						    else 
						    	return false; 
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseStructureManager', ex)
						}
					});
					
					if(equiList.getCount() < 1)
						break; 
						
					tempEqui = equiList.first(); 
					equiList.clearFilter();
					
					previousElement = currentElement; 
					currentElement = this.createNewStructure(tempEqui);
					currentElement.addChild(previousElement); 
				}
				
				//get superior funcLoc of equi
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tempEqui.get('tplnr')))
				{
					flocList.filterBy(function (record) {
						try {
						    if (record.get('tplnr') === tempEqui.get('tplnr')) {
						    	return true; 
						    }
						    else 
						    	return false; 
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseStructureManager', ex);
						}
					});
					
					if(flocList.getCount() > 0)
						tmpFloc = flocList.first();
					
					flocList.clearFilter(); 
						
					previousElement = currentElement;
					currentElement = this.createNewStructure(tmpFloc);
					currentElement.addChild(previousElement); 
				}
				
				//get superior funcLocs of func Loc
				while(tmpFloc != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tmpFloc.get('tplma')))
				{
					flocList.filterBy(function (record) {
						try {
						    if (record.get('tplnr') === tmpFloc.get('tplma')) {
						    	return true; 
						    }
						    else 
						    	return false; 
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseStructureManager', ex);
						}
					});
					
					if(flocList.getCount() != 1)
						break; 
					
					tmpFloc = flocList.first(); 
					flocList.clearFilter(); 
					previousElement = currentElement;
					currentElement = this.createNewStructure(tmpFloc);
					currentElement.addChild(previousElement); 
				}

				var lastNode = null; 
				var treeNodes = this.newNode(); 
				var _itemNode = this.newNode(); 
				 //==> all parent elements to the provided equipment are now in the structure. Create nodes for these elements
                while (currentElement.hasChild())
                {
                	var node = this.newNode(currentElement.getElement()); 
                	
                    if (lastNode == null)
                    {
                        treeNodes = node;
                        lastNode = node;
                    }
                    else
                    {
                        lastNode.appendChild(node);
                        lastNode = node;
                    }
                    
                    if(Ext.getClassName(currentElement.getElement()) === 'AssetManagement.customer.model.bo.Equipment')
						this.getChildNodes(currentElement.getElement(), node);
					if(Ext.getClassName(currentElement.getElement()) === 'AssetManagement.customer.model.bo.FuncLoc')
						this.getChildNodesFloc(currentElement.getElement(), node);
					
                    if(currentElement.hasChild())
                    	currentElement = currentElement.get('childElements');
                }
                var newNode = this.newNode(currentElement.getElement());

                if (lastNode == null)
                    treeNodes = newNode;
                else
                    lastNode.appendChild(newNode);

                _itemNode = newNode;
                //find all childelements for the equipment
                var currentEqui = equi;
                this.getChildNodes(currentEqui, _itemNode);
                
                /*var rootNode = treeStore.getRootNode(); 
                rootNode.appendChild(treeNodes);*/
                treeStore.setRoot(treeNodes);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, treeStore);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForEqui of BaseStructureManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		}, 
		
		/**
		 * 
		 * clears all filters on funcLocList, equiList and mastList
		 */		
		clearFilters: function(){
			try {
				if(flocList)
					flocList.clearFilter(); 
				if(equiList)
					equiList.clearFilter(); 
				if(mastList)
					mastList.clearFilter(); 
				if(materialList)
					materialList.clearFilter(); 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilters of BaseStructureManager', ex);
			}
		},
		
		/**
		 * 
		 * creates treestore nodes for funcLoc object structure
		 */	
		assignDependendDataForFloc: function(eventIdToFireWhenComplete, funcLoc, treeStore) {
			try {
				this.clearFilters(); 
			
				//first get superior funcLoc
				var currentElement = this.createNewStructure(funcLoc); 
				var previousElement = currentElement; 
				var tmpFloc = null; 

				//get superior funcLocs of func Loc
				while(tmpFloc != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tmpFloc.get('tplma')))
				{
					flocList.filterBy(function (record) {
						try {
						    if (record.get('tplnr') === tmpFloc.get('tplma')) {
						    	return true; 
						    }
						    else 
						    	return false; 
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseStructureManager', ex);
						}
					});
					
					if(flocList.getCount() != 1)
						break; 
					
					tmpFloc = flocList.first(); 
					
					flocList.clearFilter(); 
					previousElement = currentElement;
					currentElement = this.createNewStructure(tmpFloc);
					currentElement.addChild(previousElement); 
				}
				
				var lastNode = null; 
				var treeNodes = this.newNode(); 
				var _itemNode = this.newNode(); 
				 //==> all parent elements to the provided equipment are now in the structure. Create nodes for these elements
                while (currentElement.hasChild())
                {
                	var node = this.newNode(currentElement.getElement()); 
                	
                    if (lastNode == null)
                    {
                        treeNodes = node;
                        lastNode = node;
                    }
                    else
                    {
                        lastNode.appendChild(node);
                        lastNode = node;
                    }

                    if(currentElement.hasChild())
                    	currentElement = currentElement.get('childElements');
                }
                var newNode = this.newNode(currentElement.getElement());

                if (lastNode == null)
                    treeNodes = newNode;
                else
                    lastNode.appendChild(newNode);

                _itemNode = newNode;
                //find all childelements for the equipment
                var currentFloc = funcLoc;
                this.getChildNodesFloc(currentFloc, _itemNode);
                
                //var rootNode = treeStore.getRootNode(); 
                //rootNode.appendChild(treeNodes);
                treeStore.setRoot(treeNodes);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, treeStore);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForFloc of BaseStructureManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		}, 
		
		
		/**
		 * 
		 * gets all children of a functional Location and appends these children to the parentnode
		 */	
		getChildNodesFloc:function(funcLoc, parentNode){
			try 
			{
				var flocs = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Equipment',
							autoLoad: false
				});
				
				flocList.each(function(floc, index, length){
					if(floc.get('tplma') === funcLoc.get('tplnr'))
						flocs.add(floc); 
				}); 
				
				if(flocs.getCount() > 0)
				{
					flocs.each(function(floc, index, length) {
						if(parentNode.findChild('id', floc.get('tplnr')) === null)
						{
							var element = this.createNewStructure(floc);
			                var childNode = this.newNode(element.getElement());
			                parentNode.appendChild(childNode);
			                
			                this.getChildNodesFloc(floc, childNode);
		                }
					}, this);
				}

				//equis
				var equis = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Equipment',
							autoLoad: false
				});

				equiList.each(function(eq, index, length){
					if(eq.get('tplnr') === funcLoc.get('tplnr') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(eq.get('hequi')))
						equis.add(eq); 
				}); 
				
				if(equis.getCount() > 0)
				{
					equis.each(function(eq, index, length) {
						if(parentNode.findChild('id', eq.get('equnr')) === null)
						{
							var element = this.createNewStructure(eq);
			                var childNode = this.newNode(element.getElement());
			                parentNode.appendChild(childNode);
			                
			                //equis.each(function(e, index, length) {
			                	this.getChildNodes(eq, childNode); 
			                //}, this);
		                }
					}, this);
				}
				
				//material
				materialList.filterBy(function (record) {
					try {
					    if (record.get('matnr') === funcLoc.get('submt')) {
					    	return true; 
					    }
					    else 
					    	return false; 
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured in getChildNodesFloc in BaseStructureManager', ex);
					}
				});
				
				if(materialList.getCount() > 0)
				{
					materialList.each(function(mat, index, length) {
						var element = this.createNewStructure(mat);
	                    var childNode = this.newNode(element.getElement());
	                    parentNode.appendChild(childNode);
	                    parentNode = childNode;
					}, this);
				}
				
				materialList.clearFilter(); 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getChildNodesFloc of BaseStructureManager', ex);
			}
		},

		/**
		 * 
		 * gets all children of a equi and appends these children to the parentnode
		 */	
		getChildNodes:function(equi, parentNode){
			try {
				var equis = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Equipment',
							autoLoad: false
				});
				
				equiList.each(function(eq, index, length){
					if(eq.get('hequi') === equi.get('equnr'))
						equis.add(eq); 
				}); 
				
				if(equis.getCount() > 0)
				{
					equis.each(function(eq, index, length) {
						if(parentNode.findChild('id', eq.get('equnr')))
						{
							var element = this.createNewStructure(eq);
			                var childNode = this.newNode(element.getElement());
			                parentNode.appendChild(childNode);
			                
			                //equis.each(function(e, index, length) {
			                	this.getChildNodes(eq, childNode); 
			                //}, this);
		                }
					}, this);
				}

				equiList.clearFilter(); 
				
				materialList.filterBy(function (record) {
					try {
					    if (record.get('matnr') === equi.get('submt')) {
					    	return true; 
					    }
					    else 
					    	return false; 
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured in getChildNodes in BaseStructureManager', ex);
					}
				});
				
				if(materialList.getCount() > 0)
				{
					materialList.each(function(mat, index, length) {
						var element = this.createNewStructure(mat);
	                    var childNode = this.newNode(element.getElement());
	                    parentNode.appendChild(childNode);
	                    parentNode = childNode;
					}, this);
				}
				
				materialList.clearFilter(); 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getChildNodes of BaseStructureManager', ex);
			}
		},

		/**
		 * 
		 * creates new 'AssetManagement.customer.model.bo.StructureElement' object
		 */	
		createNewStructure: function(object){
			try {
				var retval = Ext.create('AssetManagement.customer.model.bo.StructureElement', {});
				
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Equipment')
					retval.set('equipment', object);
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.FuncLoc')
					retval.set('floc', object); 
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Mast')
					retval.set('material', object);
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Material')
					retval.set('material', object);
					
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createNewStructure of BaseStructureManager', ex);
			}
			
			return retval;
		},

		/**
		 * 
		 * creates a new node with the specific values of the given structureElement
		 */	
		newNode: function(object){
			try {
				var retval = Ext.create('Ext.data.TreeModel', {
				});
				
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Equipment')
				{
					if(!AssetManagement.customer.utils.StringUtils.trimStart(object.get('equnr'), '0'))
						AssetManagement.customer.helper.OxLogger.logException('equi: '  +AssetManagement.customer.utils.StringUtils.trimStart(object.get('equnr'), '0'));
					
				
					retval.set('text',AssetManagement.customer.utils.StringUtils.trimStart(object.get('equnr'), '0'));
					if(object.get('eqktx'))
						retval.set('text', AssetManagement.customer.utils.StringUtils.trimStart(object.get('equnr'), '0') + ' ' + object.get('eqktx'));
					retval.set('iconCls', 'icon-treestore-equi');
					
					retval.set('cls', 'treestore-row');
					//needed to be able to jum in detail view from the page
					retval.set('object', object);

					if(this.currentEqui === object)
						retval.set('cls', 'treestore-blue-row');
					
					retval.set('leaf', true); 
					retval.set('id', object.get('equnr')); 
					retval.set('expanded', true); 
				}
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.FuncLoc')
				{
					retval.set('text', object.get('tplnr'));
					if(object.get('pltxt'))
						retval.set('text', object.get('tplnr') + ' ' +object.get('pltxt'));
						
					if(!object.get('tplnr'))
						AssetManagement.customer.helper.OxLogger.logException('tplnr'  +object.get('tplnr'));
					retval.set('iconCls', 'icon-treestore-tp');
					retval.set('cls', 'treestore-row');

				    //needed to be able to jum in detail view from the page
					retval.set('object', object);
					
					if(this.currentFloc === object)
						retval.set('cls', 'treestore-blue-row');
					
					retval.set('leaf', true); 
					retval.set('id', object.get('tplnr'));
					retval.set('expanded', true); 
				}
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Mast')
				{
					if(!AssetManagement.customer.utils.StringUtils.trimStart(object.get('matnr'), '0'))
						AssetManagement.customer.helper.OxLogger.logException('matnr: ' +AssetManagement.customer.utils.StringUtils.trimStart(object.get('matnr'), '0'));
					
					if(object.get('maktx'))
						retval.set('text',AssetManagement.customer.utils.StringUtils.trimStart(object.get('matnr'), '0') + ' ' + object.get('maktx'));
					else
						retval.set('text','')
						
					retval.set('iconCls', 'icon-treestore-mat');
					retval.set('leaf', true); 
					retval.set('cls', 'treestore-row');
					//retval.set('id', object.get('matnr')); 
					retval.set('expanded', true); 
				}
				if(Ext.getClassName(object) === 'AssetManagement.customer.model.bo.Material')
				{
					if(!AssetManagement.customer.utils.StringUtils.trimStart(object.get('matnr'), '0'))
						AssetManagement.customer.helper.OxLogger.logException('matnr: ' +AssetManagement.customer.utils.StringUtils.trimStart(object.get('matnr'), '0'));
					
					if(object.get('maktx'))
						retval.set('text',AssetManagement.customer.utils.StringUtils.trimStart(object.get('matnr'), '0') + ' ' + object.get('maktx'));
					else
						retval.set('text','')
					retval.set('iconCls', 'icon-treestore-mat');
					retval.set('leaf', true); 
					retval.set('cls', 'treestore-row');
					//retval.set('id', object.get('matnr')); 
					retval.set('expanded', true); 
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newNode of BaseStructureManager', ex);
			}
			
			return retval;
		}
	}
});