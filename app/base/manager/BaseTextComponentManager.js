Ext.define('AssetManagement.base.manager.BaseTextComponentManager', {
    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.helper.DbKeyRangeHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        //public methods
        getTextComponent: function (tdobject, tdid, tdname, tdspras, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tdobject)
                        || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tdid)
                            || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tdname)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var textComponent = '';

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        textComponent = me.buildTextFromDataBaseQuery.call(me, textComponent, eventArgs);

                        if (done === true) {
                            eventController.fireEvent(retval, textComponent);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTextComponent of BaseTextComponentManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tdspras)) {
                    var clientLocale = ac.getLocale();
                    tdspras = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(clientLocale) ? clientLocale.charAt(0).toUpperCase() : '';
                }

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', tdobject);
                keyMap.add('TDNAME', tdname);
                keyMap.add('TDID', tdid);
                keyMap.add('TDSPRAS', tdspras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_ST_TEXT_LINE', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ST_TEXT_LINE', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTextComponent of BaseTextComponentManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        buildTextFromDataBaseQuery: function (textString, eventArgs) {
            var retval = '';

            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        textString = this.extendTextStringFromDatabaseObject(textString, cursor.value);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }

                    retval = textString;
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTextFromDataBaseQuery of BaseTextComponentManager', ex);
            }

            return retval;
        },

        //private
        extendTextStringFromDatabaseObject: function (textString, object) {
            var retval = '';

            try {
                var lineFormat = object['TDFORMAT'];
                var lineContent = object['TDLINE'];

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(textString) && (lineFormat === '*')) {
                    retval = lineContent;
                } else {
                    switch (lineFormat) {
                        case '*':
                            retval = textString+ '\r\n' + lineContent;
                            break;
                        case '=':
                            retval = textString + lineContent;
                            break;
                        case '':
                            retval = textString + ' ' + lineContent;
                            break;
                        case '/':
                            retval = textString + '\r\n\t' + lineContent;
                            break;
                        case '/=':
                            retval = textString + '\t' + lineContent;
                            break;
                        default:
                            retval = textString + ' ' + lineContent;
                            break;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extendTextStringFromDatabaseObject of BaseTextComponentManager', ex);
            }

            return retval;
        }
    }
});
