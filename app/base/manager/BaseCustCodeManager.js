Ext.define('AssetManagement.base.manager.BaseCustCodeManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.CustCode',
        'AssetManagement.customer.manager.CustBersManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
        _fillCacheRunning: false,

		//public
		//caution finally return value is a HASHMAP not a store (faster lookups, due to the huge amount of data)
		getCustCodes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var me = this;
				
				if (this._cacheComplete === true) {
					eventController.requestEventFiring(retval, this.getCache());
					
					return retval;
				} else {
				    var cacheFilledCallback = function (successfullyFilled) {
				        try {
				            var toReturn = successfullyFilled ? me.getCache() : undefined;
				            eventController.requestEventFiring(retval, toReturn);
				        } catch (ex) {
				            AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
				            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCodes of BaseCustCodeManager', ex);
				        }
				    };

				    this.fillCache(useBatchProcessing, cacheFilledCallback);
				}
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCodes of BaseCustCodeManager', ex);
			    retval = -1;
			}
	
			return retval;
		},
	
		//caution finally return value is a HASHMAP not a store (faster lookups, due to the huge amount of data)
		getCustCodesGroups: function(rbnr, cataloguetype, useBatchProcessing) {
			var retval = -1;
	
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(rbnr)
			    	|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(cataloguetype)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				
				var custBersSuccessCallback = function(custBers) {
					try {
						me.buildCustCodesGroupHashMap(retval, custBers, useBatchProcessing);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCodesGroup of BaseCustCodeManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var eventId = AssetManagement.customer.manager.CustBersManager.getCustBers(rbnr, cataloguetype);
				eventController.registerOnEventForOneTime(eventId, custBersSuccessCallback);
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCodesGroup of BaseCustCodeManager', ex);
			    retval = -1;
			}
	
			return retval;
		},
		
		getCustCodesForGroup: function(katalogart, codegruppe, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(katalogart)
			    	|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(codegruppe)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				if(this._cacheInitialized === true) {
					var codes = this.getCodesFromCacheForGroup(katalogart, codegruppe);
					eventController.requestEventFiring(retval, codes);
					
					return retval;
				} else {
					var cacheFilledCallback = function(successfillyFilled) {
					    try {
					        var toReturn = successfillyFilled ? this.getCodesFromCacheForGroup(katalogart, codegruppe) : undefined
					        eventController.requestEventFiring(retval, toReturn);
						} catch(ex) {
						    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCodesForGroup of BaseCustCodeManager', ex);
						    eventController.fireEvent(retval, undefined);
						}
					}
				
					this.fillCache(useBatchProcessing, cacheFilledCallback);
				}
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCodesGroup of BaseCustCodeManager', ex);
			    retval = -1;
			}
	
			return retval;
		},
		
		getCustCode: function(katalogart, codegruppe, code, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(katalogart)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(codegruppe)
							|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
					
				if(this._cacheInitialized === true) {
					var custCode = this.getFromCache(katalogart, codegruppe, code);
					
					eventController.requestEventFiring(retval, custCode);
					return retval;
				} else {
				    var cacheFilledCallback = function (successfullyFilled) {
				        try {
				            var toReturn = successfullyFilled ? this.getFromCache(katalogart, codegruppe, code) : undefined;
				            eventController.requestEventFiring(retval, toReturn);
				        } catch (ex) {
				            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCode of BaseCustCodeManager', ex);
				            eventController.fireEvent(retval, undefined);
				        }
				    };
				
					this.fillCache(useBatchProcessing, cacheFilledCallback);
				}
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustCode of BaseCustCodeManager', ex);
			    retval = -1;
			}
			
			return retval;
		},

		//private methods
		buildCustCodesGroupHashMap: function(retval, custBers, useBatchProcessing) {
			try
			{
				if(!custBers) {
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
					return;
				}
				
				var me = this; 
				var fillFunction = function(successfullyFilled) {
				    try {
				        var toReturn = undefined;

				        if (successfullyFilled) {
				            var hashMap = Ext.create('Ext.util.HashMap');

				            if (custBers.getCount() > 0) {
				                var cache = me.getCache();
				                var cataloqueType = custBers.getAt(0).get('qkatart');
				                var catalogueTypeHashMap = cache.get(cataloqueType);

				                if (catalogueTypeHashMap) {
				                    custBers.each(function (custBer) {
				                        var codeGroup = custBer.get('qcodegrp');
				                        var codesGroupStore = catalogueTypeHashMap.get(codeGroup);

				                        if (codesGroupStore && codesGroupStore.getCount() > 0) {
				                            hashMap.add(codeGroup, codesGroupStore);
				                        }
				                    }, this);
				                }
				            }

				            toReturn = hashMap;
				        }
						
				        AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, toReturn);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustCodesGroupHashMap of BaseCustCodeManager', ex);
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
					}
				};
				
				if(this._cacheInitialized === true) {
					fillFunction(true);
				} else {
				    this.fillCache(useBatchProcessing, fillFunction);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustCodesGroupHashMap of BaseCustCodeManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
			} 
		},
		
		buildCustCodeFromDataBaseQuery: function(eventArgs) {
	        var retval = null;

	        try {
	        	if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCustCodeFromDbResultObject(eventArgs.target.result.value);					
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustCodeFromDataBaseQuery of BaseCustCodeManager', ex);
			}
			return retval;
		},
		
		buildCustCodeStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var custCode = this.buildCustCodeFromDbResultObject(cursor.value);
						store.add(custCode);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustCodeStoreFromDataBaseQuery of BaseCustCodeManager', ex);
			}
		},
		
		buildCustCodeFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.CustCode', {
					katalogart: dbResult['KATALOGART'],
					codegruppe: dbResult['CODEGRUPPE'],
				    code: dbResult['CODE'],
				    codegrkurztext: dbResult['CODEGR_KURZTEXT'],
				    kurztext: dbResult['KURZTEXT'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['KATALOGART'] + dbResult['CODEGRUPPE'] + dbResult['CODE']);
			
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustCodeFromDbResultObject of BaseCustCodeManager', ex);
		    }
			return retval;
		},
		
		//cache administration
		_custCodeCache: null,
		_cacheInitialized: false,
	
		getCache: function() {
	        try {
	        	if(!this._custCodeCache) {
					this._custCodeCache = Ext.create('Ext.util.HashMap');
					
					this.initializeCacheHandling();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustCodeManager', ex);
			}
			
			return this._custCodeCache;
		},
	
		clearCache: function() {
	        try {
			    this.getCache().clear();
			    this._cacheComplete = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseCustCodeManager', ex);
			}    
		},
				
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseCustCodeManager', ex);
			} 
		},
		
		fillCache: function (useBatchProcessing, callback) {
		    if (!callback)
		        return;

			try {
				if (this._fillCacheRunning) {
				    //prevent cache override: Stop the request and poll for the current cache filling completion
				    Ext.defer(this.fillCache, 200, this, [useBatchProcessing, callback]);
				    return;
				} else if (this._cacheComplete) {
                    //cache is already complete, so just return the filled cache (this usually happens on polling scenario)
				    callback.call(this, true);
				    return;
				}

				this._fillCacheRunning = true;
				
				this.clearCache();
				var cacheRef = this.getCache();
				
				var me = this;

				var count = 0;

				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						var custCode = me.buildCustCodeFromDataBaseQuery.call(me, eventArgs);
						
						if (custCode) {
							var myTargetCatalogueHashMap = cacheRef.get(custCode.get('katalogart'));
							
							if(!myTargetCatalogueHashMap) {
								myTargetCatalogueHashMap = Ext.create('Ext.util.HashMap');
								
								cacheRef.add(custCode.get('katalogart'), myTargetCatalogueHashMap);
							}
							
							var myTargetGroupStore = myTargetCatalogueHashMap.get(custCode.get('codegruppe'));
							
							if(!myTargetGroupStore) {
								myTargetGroupStore = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.CustCode',
									autoLoad: false
								});
								
								myTargetCatalogueHashMap.add(custCode.get('codegruppe'), myTargetGroupStore);
							}
							
							myTargetGroupStore.add(custCode);
						}
						
						if (done) {
						    me._fillCacheRunning = false;

							me._cacheInitialized = true;
							me._cacheComplete = true;
							callback.call(me, true);
						} else {
						    count++;
							AssetManagement.customer.helper.CursorHelper.doCursorContinue(eventArgs.target.result);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillCache of BaseCustCodeManager', ex);
						callback.call(me, false);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('G_CUSTCODES', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('G_CUSTCODES', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillCache of BaseCustCodeManager', ex);
			    callback.call(this, false);
			}
		
			return;
		},
		
		getCodesFromCacheForGroup: function(katalogart, codegruppe) {
			var retval = null;
		
			try {
				var catalogueHashMap = this.getCache().get(katalogart);
				
				if(catalogueHashMap) {
					retval = catalogueHashMap.get(codegruppe);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCodesFromCacheForGroup of BaseCustCodeManager', ex);
			}
		
			return retval;
		},
		
		getFromCache: function(katalogart, codegruppe, code) {
			var retval = null;
		
			try {
				var groupStore = this.getCodesFromCacheForGroup(katalogart, codegruppe);
			
				if(groupStore) {
					groupStore.each(function(custCode) {
						if(custCode.get('code') === code) {
							retval = custCode;
							return false;
						}
					}, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseCustCodeManager', ex);
			}
		
			return retval;
		}
	}
});