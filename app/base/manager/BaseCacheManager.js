Ext.define('AssetManagement.base.manager.BaseCacheManager', {
	requires: [
	    'AssetManagement.customer.controller.EventController'
    ],
	
	inheritableStatics: {
		//public
		EVENTS: { 
			GLOBAL_CACHE_RESET: 'globalCacheReset',
			CUST_DATA_CACHE_RESET: 'customizingDataCacheReset',
			MOV_DATA_CACHE_RESET: 'movingDataCacheReset'
		},
		
		clearCaches: function(movingData, customizingData) {
			try {
				if(arguments.length === 0) {
					movingData = true;
					customizingData = true;
				}
				
				var eventToFire = this.EVENTS.GLOBAL_CACHE_RESET;
				
				if(movingData === true && !customizingData)
					eventToFire = this.EVENTS.CUST_DATA_CACHE_RESET;
				else if(!movingData && customizingData === true)
					eventToFire = this.EVENTS.MOV_DATA_CACHE_RESET;
					
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventToFire, null);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCaches of BaseCacheManager', ex);
			}
		}
	}
});