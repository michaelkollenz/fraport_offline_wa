Ext.define('AssetManagement.base.manager.BasePartnerManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Partner',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public methods
		getPartners: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._cacheComplete === true) {
					eventController.requestEventFiring(retval, this.getCache());
					
					return retval;
				}
				
				var toFill = this.getCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildPartnersStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me._cacheComplete = true;
							
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPartners of BasePartnerManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PARTNER', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PARTNER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAddresses of BasePartnerManager', ex);
			}
	
			return retval;
		},
		
		getAllPartnersForObject: function(objnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)){
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				if(this._cacheComplete === true) {
					var fromCache = this.extractFromCache(objnr);
					eventController.requestEventFiring(retval, fromCache);
					
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Partner',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildPartnersStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllPartnersForObject of BasePartnerManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('OBJNR', objnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PARTNER', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PARTNER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllPartnersForObject of BasePartnerManager', ex);
			}
	
			return retval;
		},
		
		getSpecificPartnersForObject: function(objnr, parvw, useBatchProcessing) {
			var retval = -1;
			
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parvw)){
				return this.getAllPartnersForObject(objnr, useBatchProcessing);
			}
			
			try {		
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._cacheComplete === true) {
					var fromCache = this.extractFromCache(objnr);
					eventController.requestEventFiring(retval, fromCache);
					
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Partner',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildPartnersStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSpecificPartnersForObject of BasePartnerManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('OBJNR', objnr);
				keyMap.add('PARVW', parvw);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PARTNER', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PARTNER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSpecificPartnersForObject of BasePartnerManager', ex);
			}
	
			return retval;
		},
		
		//private
		buildPartnerFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildPartnerFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPartnerFromDataBaseQuery of BasePartnerManager', ex);
			}
			
			return retval;
		},
		
		buildPartnersStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var partner = this.buildPartnerFromDbResultObject(cursor.value);
						store.add(partner);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPartnersStoreFromDataBaseQuery of BasePartnerManager', ex);
			}
		},
		
		buildPartnerFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Partner', {
					objnr: dbResult['OBJNR'],
					parvw: dbResult['PARVW'],
					counter: dbResult['COUNTER'],
					parnr: dbResult['PARNR'],
					adrnr: dbResult['ADRNR'],
					name1: dbResult['NAME1'],
					name2: dbResult['NAME2'],
					name3: dbResult['NAME3'],
					name4: dbResult['NAME4'],
					postcode1: dbResult['POST_CODE1'],
					city1: dbResult['CITY1'],
					city2: dbResult['CITY2'],
					street: dbResult['STREET'],
					housenum1: dbResult['HOUSE_NUM1'],
					country: dbResult['COUNTRY'],
					region: dbResult['REGION'],
					telnumber: dbResult['TEL_NUMBER'],
					telextens: dbResult['TEL_EXTENS'],
					faxnumber: dbResult['FAX_NUMBER'],
					faxextens: dbResult['FAX_EXTENS'],
					aufnr: dbResult['AUFNR'],
					qmnum: dbResult['QMNUM'],
					equnr: dbResult['EQUNR'],
					tplnr: dbResult['TPLNR'],
					telnumbermob: dbResult['TEL_NUMBER_MOB'],
					email: dbResult['EMAIL'],
					timeZone: dbResult['TZONSP'],
                    str_suppl1: dbResult['STR_SUPPL1'],
					title: dbResult['TITLE'],

				    updFlag: dbResult['UPDFLAG'],
				    mobileKey: dbResult['MOBILEKEY']
				});
				
				retval.set('id', dbResult['OBJNR'] + dbResult['PARVW'] + dbResult['COUNTER']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPartnerFromDbResultObject of BasePartnerManager', ex);
			}
			
			return retval;
		},
		
		//cache administration
		_partnersCache: null,
		_cacheComplete: false,
	
		getCache: function() {
	        try {
	        	if(!this._partnersCache) {
					this._partnersCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Partner',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BasePartnerManager', ex);
			}
			
			return this._partnersCache;
		},
	
		clearCache: function() {
	        try {
			    this.getCache().removeAll();
			    this._cacheComplete = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BasePartnerManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BasePartnerManager', ex);
			} 
		},
		
		extractFromCache: function(objnr, parvw) {
			var retval = Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.Partner',
				autoLoad: false
			});
			
			try {
				var checkForRole = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parvw);
				
				if(checkForRole)
					parvw = parvw.toUpperCase();
			
				this.getCache().each(function(partner) {
					if(partner.get('objnr') === objnr) {
						if(!checkForRole || partner.get('parvw') === parvw) {
							retval.add(partner);
						}
					}
				}, this);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractFromCache of BasePartnerManager', ex);
			}
				
			return retval;
		},
		
		/**
		 * saves the partner of an order or a notification to the database
		 */
		savePartnerForObject: function(partner) {
			var retval = -1;
			try {

				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partner)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				var me = this;	
				
				var callback = function(eventArgs) {
					try {
						eventController.fireEvent(retval, partner); 
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside savePartnerForObject of BasePartnerManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				var toSave = me.buildDataBaseObjectForObjectPartner(partner);
				AssetManagement.customer.core.Core.getDataBaseHelper().put('D_PARTNER', toSave, callback, callback);

			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside savePartnerForObject of BasePartnerManager', ex);
			}
			return retval;
		},

		
		/**
		 * create the databaseobject for saving the partner associated with an order or a notification
		 */
		buildDataBaseObjectForObjectPartner: function(partner) {
			var retval = null;
			var ac = null;
		
			try {
			    ac = AssetManagement.customer.core.Core.getAppConfig();
						
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['UPDFLAG'] =  partner.get('updFlag'); 
				retval['OBJNR'] = partner.get('objnr');
				retval['PARVW'] = partner.get('parvw');
				retval['COUNTER'] = partner.get('counter');
				retval['PARNR'] = partner.get('parnr');
				retval['PARTNER_TYPE'] = partner.get('partnertype');
				retval['ADRNR'] = partner.get('adrnr');
				retval['NAME1'] = partner.get('name1');
				retval['NAME2'] = partner.get('name2');
				retval['NAME3'] = partner.get('name3');
				retval['NAME4'] = partner.get('name4');
				retval['POST_CODE1'] = partner.get('postcode1');
				retval['CITY1'] = partner.get('city1');
				retval['CITY2'] = partner.get('city2');
				retval['STREET'] = partner.get('street');
				retval['TITLE'] = partner.get('title');
				retval['COUNTRY'] = partner.get('country');
				retval['REGION'] = partner.get('region');
				retval['TEL_NUMBER'] = partner.get('telnumber');
				retval['FAX_NUMBER'] = partner.get('faxnumber');
				retval['HOUSE_NUM1'] = partner.get('housenum1');
				retval['TEL_EXTENS'] = partner.get('telextens');
				retval['FAX_EXTENS'] = partner.get('faxextens');
				retval['AUFNR'] = partner.get('aufnr');
				retval['QMNUM'] = partner.get('qmnum');
				retval['EQUNR'] = partner.get('equnr');
				retval['TPLNR'] = partner.get('tplnr');
				retval['TEL_NUMBER_MOB'] = partner.get('telnumbermob');
				retval['EMAIL'] = partner.get('email');
		        retval['TZONSP'] = partner.get('timeZone');
		        retval['STR_SUPPL1'] = partner.get('str_suppl1');
				retval['TITLE'] = partner.get('title');
				retval['MOBILEKEY'] = partner.get('mobileKey');
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForObjectPartner of BasePartnerManager', ex);
			}
			
			return retval;
		},


	    /*
        * returns the first partner object from a provided partner list matching the provided parvw
        * used for finding singular partner roles like AG, WE, RE...
        */
		getSingularPartnerFromPartnerList: function (partnerList, parvw) {
		    var retVal = null;
		    try {
		        var partnerFilterList = AssetManagement.customer.manager.PartnerManager.getPartnerByTypeFromList(partnerList, parvw);
		        if (partnerFilterList !== null && partnerFilterList.data.items.length > 0)
		            retVal = partnerFilterList.data.items[0];
		    }
		    catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSingularPartnerFromPartnerList of PartnerManager', ex);
		    }
		    return retVal;
		},

	    /*
*returns a list of partner objects of a single type from a list of partners
*/
		getPartnerByTypeFromList: function (partnerList, parvw) {
		    var retVal = null;
		    try {
		        var partnerStore = Ext.create('Ext.data.Store', {
		            model: 'AssetManagement.customer.model.bo.Partner',
		            autoLoad: false
		        });
		        partnerList.each(function (partner) {
		            if (partner.get('parvw') === parvw) {
		                partnerStore.add(partner);
		            }
		        }, this);
		        retVal = partnerStore;
		    }
		    catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPartnerByTypeFromList of BasePartnerManager', ex);
		    }
		    return retVal;
		}
		 
	}
});