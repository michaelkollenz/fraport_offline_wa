﻿Ext.define('AssetManagement.base.manager.BaseMultiValueCheckboxManager', {
    extend: 'AssetManagement.customer.manager.OxBaseManager',
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.manager.cache.ClassificationCache',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.MobileIndexHelper',
        'AssetManagement.customer.model.bo.CharactValue',
        'AssetManagement.customer.model.bo.ObjClass',
        'AssetManagement.customer.model.bo.Charact',
        'AssetManagement.customer.model.bo.ObjClassValue'
    ],

    inheritableStatics: {
        EVENTS: {
            CLASS_ADDED: 'classAdded',
            CLASS_CHANGED: 'classChanged',
            CLASS_DELETED: 'classDeleted'
        },

        //private
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.ClassificationCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        getObjClasses: function (objnr, useBatchProcessing) {
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                //check if the cache can deliver func. loc.'s hist orders
                var cache = this.getCache();
                var fromCache = cache.getFromCacheForFuncLoc(objnr);

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClass',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildObjClassStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            //add func. loc.'s meas points to cache
                            cache.addToCacheForFuncLoc(objnr, fromDataBase);

                            //return a store from cache to eliminate duplicate objects
                            var toReturn = cache.getFromCacheForFuncLoc(objnr);
                            me.loadDependendDataForObjClass.call(me, retval, toReturn);

                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjClass of BaseMultiValueCheckboxManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };
                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('OBJNR', objnr);

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_OBJCLASSAS', 'OBJNR', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_OBJCLASSAS', 'OBJNR', indexRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjClass of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        buildObjClassStoreFromDataBaseQuery: function (store, eventArgs) {
            try {

                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var measpoint = this.buildObjClassFromDbResultObject(cursor.value);
                        store.add(measpoint);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjClassStoreFromDataBaseQuery of BaseMultiValueCheckboxManager', ex);
            }
        },

        buildObjClassFromDbResultObject: function (dbResult) {
            var retval = null;
            try {

                retval = Ext.create('AssetManagement.customer.model.bo.ObjClass', {
                    objnr: dbResult['OBJNR'],
                    atinn: dbResult['ATINN'],
                    atzhl: dbResult['ATZHL'],
                    atwrt: dbResult['ATWRT'],
                    atflv: dbResult['ATFLV'],
                    atawe: dbResult['ATAWE'],
                    atflb: dbResult['ATFLB'],
                    ataw1: dbResult['ATAW1'],
                    atcod: dbResult['ATCOD'],
                    attlv: dbResult['ATTLV'],
                    attlb: dbResult['ATTLB'],
                    mafid: dbResult['MAFID'],
                    klart: dbResult['KLART'],
                    clint: dbResult['CLINT'],
                    adzhl: dbResult['ADZHL'],
                    updFlag: dbResult['UPDFLAG'],

                    mobileKey: dbResult['MOBILEKEY']
                });

                retval.set('id', dbResult['OBJNR'] + dbResult['ATINN'] + dbResult['ATZHL'] + dbResult['MAFID'] + dbResult['ADZHL']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjClassFromDbResultObject of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        /// GET CHARACTS ///

        getCharacts: function (clint, useBatchProcessing) {

            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var toFill = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.Charact',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildCharactStoreFromDataBaseQuery.call(me, toFill, eventArgs);

                        if (done) {
                            eventController.fireEvent(retval, toFill);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCharacts of BaseMultiValueCheckboxManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CLASSCHARACT', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CLASSCHARACT', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCharacts of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        addAllToCache: function (characts, completeDataFromDatabase) {
            try {

                if (completeDataFromDatabase === true)
                    this.clearCache();

                characts.each(function (charact) {
                    var myTargetStore = this.getFromCacheForObjClass(charact.get('point'));

                    if (!myTargetStore) {
                        myTargetStore = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.Charact',
                            autoLoad: false
                        });

                        this.addToCache(charact.get('point'), myTargetStore);
                    }

                    myTargetStore.add(charact);
                }, this);

                if (completeDataFromDatabase === true)
                    this._cacheComplete = true;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseMultiValueCheckboxManager', ex);
            }
        },

        getFromCacheForObjClass: function (point) {
            var retval = null;

            try {
                retval = this.getCache().get(point);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForPoint of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        buildCharactStoreFromDataBaseQuery: function (store, eventArgs) {
            try {

                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var measpoint = this.buildCharactsFromDbResultObject(cursor.value);
                        store.add(measpoint);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCharactStoreFromDataBaseQuery of BaseMultiValueCheckboxManager', ex);
            }
        },

        buildCharactsFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.Charact', {
                    clint: dbResult['CLINT'],
                    atinn: dbResult['ATINN'],
                    adzhl: dbResult['ADZHL'],
                    atnam: dbResult['ATNAM'],
                    atfor: dbResult['ATFOR'],
                    anzst: dbResult['ANZST'],
                    anzdz: dbResult['ANZDZ'],
                    atkle: dbResult['ATKLE'],
                    aterf: dbResult['ATERF'],
                    atein: dbResult['ATEIN'],
                    atint: dbResult['ATINT'],
                    atson: dbResult['ATSON'],
                    atinp: dbResult['ATINP'],
                    atvie: dbResult['ATVIE'],
                    atbez: dbResult['ATBEZ'],
                    msehi: dbResult['MSEHI'],
                    atwme: dbResult['ATWME'],
                    mseh6: dbResult['MSEH6'],
                    mseht: dbResult['MSEHT']
                });

                retval.set('id', dbResult['CLINT'] + dbResult['ATINN'] + dbResult['ADZHL']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCharactsFromDbResultObject of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        /// Get Object Class Value (D_OBJCLASS) ///
        getObjectClassValue: function (clint, useBatchProcessing) {

            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var toFill = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildObjectClassValueStoreFromDataBaseQuery.call(me, toFill, eventArgs);

                        if (done) {
                            eventController.fireEvent(retval, toFill);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectClassValue of BaseMultiValueCheckboxManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_OBJCLASS', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_OBJCLASS', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectClassValue of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        buildObjectClassValueStoreFromDataBaseQuery: function (store, eventArgs) {
            try {

                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var ObjectClassValue = this.buildObjClassFromDbResultObject(cursor.value);
                        store.add(ObjectClassValue);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectClassValueStoreFromDataBaseQuery of BaseMultiValueCheckboxManager', ex);
            }
        },

        /// GET CHARACT VALUE (C_CHARACTVALUE) ///
        getCharactValue: function (clint, useBatchProcessing) {

            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var toFill = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.CharactValue',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildCharactValueStoreFromDataBaseQuery.call(me, toFill, eventArgs);

                        if (done) {
                            eventController.fireEvent(retval, toFill);

                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCharactValue of BaseMultiValueCheckboxManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CHARACTVALUE', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CHARACTVALUE', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCharactValue of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        buildCharactValueStoreFromDataBaseQuery: function (store, eventArgs) {
            try {

                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var ObjectClassValue = this.buildCharactValueFromDbResultObject(cursor.value);
                        store.add(ObjectClassValue);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCharactValueStoreFromDataBaseQuery of BaseMultiValueCheckboxManager', ex);
            }
        },

        buildCharactValueFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.CharactValue', {
                    atinn: dbResult['ATINN'],
                    atzhl: dbResult['ATZHL'],
                    adzhl: dbResult['ADZHL'],
                    atwrt: dbResult['ATWRT'],
                    atstd: dbResult['ATSTD'],
                    atwtb: dbResult['ATWTB'],
                    atflv: dbResult['ATFLV'],
                    atflb: dbResult['ATFLB'],
                    atcod: dbResult['ATCOD'],
                    atawe: dbResult['ATAWE'],
                    ataw1: dbResult['ATAW1'],
                    attlv: dbResult['ATTLV'],
                    attlb: dbResult['ATTLB'],
                    updFlag: dbResult['UPDFLAG']

                });

                retval.set('id', dbResult['ATINN'] + dbResult['ATZHL'] + dbResult['ADZHL']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCharactValueFromDbResultObject of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        /// GET D_OBJCLASSAS, C_CLASSCHARACT, D_OBJCLASS, C_CHARACTVALUE
        loadDependendDataForObjClass: function (eventIdToFireWhenComplete, objClasses) {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //before the data can be assigned, the lists have to be loaded by all other managers
                //load these data flat!
                //register on the corresponding events
                var characts = null;
                var objectClassValue = null;
                var charactValue = null;

                var counter = 0;
                var done = 0;
                var erroroccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (erroroccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && erroroccurred === false) {
                        me.assignDependendDataForObjClass.call(me, eventIdToFireWhenComplete, objClasses, characts, objectClassValue, charactValue);
                    }
                };

                //TO-DO make dependend of customizing parameters --- this definitely will increase performance

                //get charact (C_CLASSCHARACT)
                if (true) {
                    done++;

                    var charactSuccessCallback = function (chars) {
                        erroroccurred = chars === undefined;

                        characts = chars;
                        counter++;

                        completeFunction();
                    };

                    var eventId = this.getCharacts(false, true);
                    eventController.registerOnEventForOneTime(eventId, charactSuccessCallback);
                }

                // get ObjectClass Value (D_OBJCLASS)
                if (true) {
                    done++;
                    var objectClassValueSuccessCallback = function (objClassValue) {
                        erroroccurred = objClassValue === undefined;

                        objectClassValue = objClassValue;
                        counter++;

                        completeFunction();
                    };

                    var eventId = this.getObjectClassValue(false, true);
                    eventController.registerOnEventForOneTime(eventId, objectClassValueSuccessCallback);
                }

                // get Charact Value (C_CHARACTVALUE)
                if (true) {
                    done++;

                    var charactValueSuccessCallback = function (charValue) {
                        erroroccurred = charValue === undefined;

                        charactValue = charValue;
                        counter++;

                        completeFunction();
                    };

                    var eventId = this.getCharactValue(false, true);
                    eventController.registerOnEventForOneTime(eventId, charactValueSuccessCallback);
                }


                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForOrders of BaseMultiValueCheckboxManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        assignDependendDataForObjClass: function (eventIdToFireWhenComplete, objClasses, characts, objectClassValue, charactValue) {
            try {

                // Save the right characts in Object Classes
                objClasses.each(function (objClass) {
                    if (characts && characts.getCount() > 0) {
                        var objClassClint = objClass.get('clint');

                        characts.each(function (charact) {
                            if (charact.get('clint') === objClassClint) {

                                var charsForObjClass = Ext.create('Ext.data.Store', {
                                    model: 'AssetManagement.customer.model.bo.Charact',
                                    autoLoad: false
                                });

                                // CHECK IF OBJECTCLASS EXISTS and add its data as an ObjClassValue to the ClassValues-List
                                if (objectClassValue && objectClassValue.getCount() > 0) {
                                    var objClassObjnr = objClass.get('objnr');
                                    var objClassMafid = objClass.get('mafid');
                                    var objClassKlart = objClass.get('klart');
                                    var objClassAdzhl = objClass.get('adzhl');
                                    var charactAtinn = charact.get('atinn');

                                    objectClassValue.each(function (objClassValue) {
                                        if (objClassValue.get('objnr') === objClassObjnr)
                                            if (objClassValue.get('mafid') === objClassMafid)
                                                if (objClassValue.get('klart') === objClassKlart)
                                                    if (objClassValue.get('adzhl') === objClassAdzhl)
                                                        if (objClassValue.get('atinn') === charactAtinn) {

                                                            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(charact.get('classValues'))) {
                                                                var clValue = Ext.create('Ext.data.Store', {
                                                                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                                                                    autoLoad: false
                                                                });
                                                                charact.set('classValues', clValue);
                                                            }

                                                            charact.get('classValues').add(objClassValue);
                                                        }
                                    });
                                }

                                // CHECK IF CHARACTVALUES EXIST
                                charactValue.each(function (charValue) {
                                    var charactAtinn = charact.get('atinn');
                                    var charactAdzhl = charact.get('adzhl');

                                    if (charValue.get('atinn') === charactAtinn)
                                        if (charValue.get('adzhl') === charactAdzhl) {

                                            // Prüfen, ob charactValues im charact vorhanden ist
                                            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(charact.get('charactValues'))) {
                                                var chValue = Ext.create('Ext.data.Store', {
                                                    model: 'AssetManagement.customer.model.bo.CharactValue',
                                                    autoLoad: false
                                                });
                                                charact.set('charactValues', chValue);
                                            }

                                            charact.get('charactValues').add(charValue);
                                        }
                                });

                                // Charact in der objClass speichern
                                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objClass.get('characts'))) {
                                    var char = Ext.create('Ext.data.Store', {
                                        model: 'AssetManagement.customer.model.bo.Charact',
                                        autoLoad: false
                                    });
                                    objClass.set('characts', char);
                                }

                                objClass.get('characts').add(charact);
                            }
                        });
                    }
                });

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, objClasses);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForObjClass of BaseMultiValueCheckboxManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
            }
        },

        CreateValueString: function (value, charact) {
            try {
                var retVal = value;

                // Format date Values
                if (charact.get('atfor') === 'DATE' && value.indexOf('.') === -1) {
                    var tepm = AssetManagement.customer.utils.DateTimeUtils.parseTime(value);
                    retVal = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(tepm);
                }
                else if ((charact.get('atfor') === ("TIME")) && value.indexOf(':') === -1) {
                    var time = AssetManagement.customer.utils.DateTimeUtils.parseTime(value);
                    retVal = AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(time, true);
                }

                // Add unit
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
                    retVal += ' ' + charact.get('mseht');

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside CreateValueString of BaseMultiValueCheckboxManager', ex);
            }

            return retVal;
        },

        updateClassValues: function (classValues,newValues) {
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!classValues) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                } else if (classValues.getCount() === 0) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }

                var delVal = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClass',
                    autoLoad: false
                });

                var saveVal = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClass',
                    autoLoad: false
                });

                //zulöschende werte bestimmen und in delVal speichern
                classValues.each(function (classValue) {
                    var objnr = classValue.get('objnr');
                    var atinn = classValue.get('atinn');
                    var atzhl = classValue.get('atzhl');
                    var mafid = classValue.get('mafid');
                    var adzhl = classValue.get('adzhl');

                    var matchFound = false;

                    newValues.each(function (nValue) {
                        if (nValue.get('objnr') === objnr && nValue.get('atinn') === atinn && nValue.get('atzhl') === atzhl && nValue.get('mafid') === mafid && nValue.get('adzhl') === adzhl) {
                            matchFound = true;
                            return false;
                        }
                    });

                    if (!matchFound) {
                        delVal.add(classValue);
                    }
                }, this);

                //zuspeichernde werte bestimmen und in saveVal speichern
                newValues.each(function (nValue) {
                    var objnr = nValue.get('objnr');
                    var atinn = nValue.get('atinn');
                    var atzhl = nValue.get('atzhl');
                    var mafid = nValue.get('mafid');
                    var adzhl = nValue.get('adzhl');

                    var matchFound = false;

                    classValues.each(function (classValue) {
                        if (classValue.get('objnr') === objnr && classValue.get('atinn') === atinn && classValue.get('atzhl') === atzhl && classValue.get('mafid') === mafid && classValue.get('adzhl') === adzhl) {
                            matchFound = true;
                            return false;
                        }
                    });

                    if (!matchFound) {
                        saveVal.add(nValue);
                    }
                }, this);


                var me = this;
                var pendingRequest = 0;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    try {

                        if (pendingRequest === 0) {
                            if (errorOccurred === true && reported === false) {
                                eventController.requestEventFiring(retval, false);
                                reported = true;
                            } else if (errorOccurred === false) {
                                eventController.requestEventFiring(retval, true);

                            }
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateClassValues of BaseMultiValueCheckboxManager', ex);
                    }
                };
                
                if (delVal.getCount() > 0) {
                    delVal.each(function (toDelete) {
                        pendingRequest++;

                        var deleteCallback = function (success) {
                            try {
                                if (success) {
                                    pendingRequest--;
                                } else {
                                    errorOccurred = true;
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateClassValues of BaseMultiValueCheckboxManager', ex);
                            } finally {
                                completeFunction();
                            }
                        }

                        var eventId = this.deleteClassValue(toDelete, false);

                        if (eventId > 0) {
                            eventController.registerOnEventForOneTime(eventId, deleteCallback);
                        } else {
                            errorOccurred = true;
                            retval = -1;
                        }
                    }, this);
                }

                if (!errorOccurred && saveVal.getCount() > 0) {
                    saveVal.each(function (toSave) {
                        pendingRequest++;

                        var saveCallback = function (success) {
                            try {
                                if (success) {
                                    pendingRequest--;
                                } else {
                                    errorOccurred = true;
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateClassValues of BaseMultiValueCheckboxManager', ex);
                            } finally {
                                completeFunction();
                            }
                        }

                        var eventId = this.saveClassValue(toSave, true);

                        if (eventId > 0) {
                            eventController.registerOnEventForOneTime(eventId, saveCallback);
                        } else {
                            errorOccurred = true;
                            retval = -1;
                        }
                    }, this);
                }

                if (pendingRequest > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.Oxlogger.logException('Exception occurred inside updateClassValues of BaseMultiValueCheckboxManager', ex);
            }

            return retval;

        },

        //save a classvalue - if it is neccessary it will get a new counter first
        saveClassValue: function (classValue) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!classValue) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var priorUpdateFlag = classValue.get('updFlag');

                //update flag management
                this.manageUpdateFlagForSaving(classValue);
               
                //check if the timeconf already has a counter value
                var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValue.get('atzhl'));

                var me = this;
                var saveFunction = function (nextCounterValue) {
                    try {
                        if (requiresNewCounterValue && nextCounterValue !== -1)
                            classValue.set('atzhl', nextCounterValue);

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValue.get('atzhl'))) {
                            eventController.fireEvent(retval, false);
                            return;
                        }

                        //set the id
                        classValue.set('id', classValue.get('objnr') + classValue.get('atinn')+ classValue.get('atzhl'));

                        var callback = function (eventArgs) {
                            try {
                                var success = eventArgs.type === "success";

                                if (success) {
                                    var eventType = requiresNewCounterValue ? me.EVENTS.CLASS_ADDED : me.EVENTS.CLASS_CHANGED;
                                    //me.getCache().addToCache(classValue);
                                    eventController.fireEvent(eventType, classValue);
                                }

                                eventController.fireEvent(retval, success);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveClassValue of BaseTimeConfManager', ex);
                                eventController.fireEvent(retval, false);
                            }
                        };

                        var toSave = me.buildDataBaseObjectForObjClassValue(classValue);

                        if(classValue.get('updFlag')==='I')
                            AssetManagement.customer.core.Core.getDataBaseHelper().put('D_OBJCLASS', toSave, callback, callback);
                        else if(classValue.get('updFlag')==='U')
                            AssetManagement.customer.core.Core.getDataBaseHelper().update('D_OBJCLASS', null, toSave, callback, callback);
                        else
                            eventController.fireEvent(retval, false);
                    } catch (ex) {
                        eventController.fireEvent(retval, false);
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveClassValue of BaseTimeConfManager', ex);
                    }
                };

                if (!requiresNewCounterValue) {
                    saveFunction();
                } else {
                    eventId = this.getNextAtzhl(classValue);

                    if (eventId > 1)
                        eventController.registerOnEventForOneTime(eventId, saveFunction);
                    else
                        eventController.fireEvent(retval, false);
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveTimeConf of BaseTimeConfManager', ex);
            }

            return retval;
        },

        getNextAtzhl: function(classValue) {
            var retval = -1;
			
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
				
                if(!classValue) {
                    eventController.requestEventFiring(retval, -1);
                    return retval;
                }
				
                var maxAtzhl = 0;
								
                var successCallback = function(eventArgs) {
                    try {
                        if(eventArgs && eventArgs.target && eventArgs.target.result) {
                            var cursor = eventArgs.target.result;
							var atzhlValue = cursor.value['ATZHL'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(atzhlValue, '%')) {
                                //cut of the percent sign
							    atzhlValue = atzhlValue.substr(1);
                                var curAtzhlValue = parseInt(atzhlValue);
							
							    if (!isNaN(curAtzhlValue))
                                {
                                    if (curAtzhlValue > maxAtzhl)
                                        maxAtzhl = curAtzhlValue;
                                } 
                            }
				
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            var nextLocalCounterNumberValue = maxAtzhl + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 2);
						    eventController.fireEvent(retval, nextLocalCounterValue);
                        }
                    } catch(ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextAtzhl of BaseTimeConfManager', ex);
                        eventController.fireEvent(retval, -1);
                    }
                };
				
                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('OBJNR', classValue.get('objnr'));
                keyMap.add('ATINN', classValue.get('atinn'));
				
                //drop a raw query to include records flagged for deletion
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_OBJCLASS', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().rawQuery('D_OBJCLASS', keyRange, successCallback, null);
            } catch(ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextAtzhl of BaseTimeConfManager', ex);
            }
			
            return retval;			
        },

        deleteClassValue: function (classValue, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!classValue) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";
                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(classValue);
                            //me.getCache().removeFromCache(classValue);
                            eventController.fireEvent(me.EVENTS.CLASS_DELETED, classValue);
                        }

                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClassValue of BaseMultiValueCheckboxManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var toSave = me.buildDataBaseObjectForObjClassValue(classValue);

                if (classValue.get('updFlag') === 'X')
                    AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_OBJCLASS', toSave, callback, callback, useBatchProcessing, this.requiresManualCompletion());
             
                else
                    eventController.fireEvent(retval, false);
                
                //get the key of the classValue to delete
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_OBJCLASS', classValue);
                 AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_OBJCLASS', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClassValue of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        buildDataBaseObjectForObjClassValue: function (objClassValue) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();
                var retval = {};

                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();

                retval['OBJNR'] = objClassValue.get('objnr');
                retval['ATINN'] = objClassValue.get('atinn');
                retval['CLINT'] = objClassValue.get('clint');
                retval['ATZHL'] = objClassValue.get('atzhl');
                retval['MAFID'] = objClassValue.get('mafid');
                retval['KLART'] = objClassValue.get('klart');
                retval['ADZHL'] = objClassValue.get('adzhl');
                retval['ATWRT'] = objClassValue.get('atwrt');
                retval['ATFLV'] = objClassValue.get('atflv');
                retval['ATAWE'] = objClassValue.get('atawe');
                retval['ATFLB'] = objClassValue.get('atflb');
                retval['ATAW1'] = objClassValue.get('ataw1');
                retval['ATCOD'] = objClassValue.get('atcod');
                retval['ATTLV'] = objClassValue.get('attlv');
                retval['ATTLB'] = objClassValue.get('attlb');

                retval['MOBILEKEY'] = objClassValue.get('mobileKey');
                retval['UPDFLAG'] = objClassValue.get('updFlag');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForObjClassValue of BaseMultiValueCheckboxManager', ex);
            }
            return retval;

        },
        CreateNewClassValue: function (charact, objClass) {
            var retval = null;
            try {
                retval = Ext.create('AssetManagement.customer.model.bo.ObjClassValue', {
                    objnr: objClass.get('objnr'),
                    mafid: objClass.get('mafid'),
                    klart: objClass.get('klart'),
                    clint: '000000000',
                    adzhl: objClass.get('adzhl'),
                    atinn: charact.get('atinn'),
                    atzhl: objClass.get('atzhl'),
                    atwrt: '',
                    atflv: '',
                    atawe: '',
                    atflb: '',
                    ataw1: '',
                    atcod: '',
                    attlv: '',
                    attlb: '',
                    updFlag: 'I',
                    mobileKey: objClass.get('mobileKey')
                });

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside CreateNewClassValue of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        CreateNewClassValueSingular: function (charact, objClass, atwrt, atflv) {
            var retval = null;
            try {
                retval = Ext.create('AssetManagement.customer.model.bo.ObjClassValue', {
                    objnr: objClass.get('objnr'),
                    mafid: objClass.get('mafid'),
                    klart: objClass.get('klart'),
                    clint: '000000000',
                    adzhl: objClass.get('adzhl'),
                    atinn: charact.get('atinn'),
                    atzhl: objClass.get('atzhl'),
                    atwrt: atwrt,
                    atflv: atflv,
                    atawe: '',
                    atflb: '',
                    ataw1: '',
                    atcod: '',
                    attlv: '',
                    attlb: '',
                    updFlag: 'I',
                    mobileKey: objClass.get('mobileKey')
                });

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside CreateNewClassValueSingular of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        deleteClass: function (objClassValue, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objClassValue) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(objClassValue);
                            eventController.fireEvent(me.EVENTS.CLASS_DELETED, objClassValue);
                        }

                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClass of BaseMultiValueCheckboxManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                //get the key of the banf to delete
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_OBJCLASS', objClassValue);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_OBJCLASS', keyRange, callback, callback, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClass of BaseMultiValueCheckboxManager', ex);
            }

            return retval;
        },

        cloneClassValue: function (classValue) {
            var retval = null;

            try {

                retval = Ext.create('AssetManagement.customer.model.bo.ObjClass', {
                    objnr: classValue.get('objnr'),
                    atinn: classValue.get('atinn'),
                    atzhl: classValue.get('atzhl'),
                    atwrt: classValue.get('atwrt'),
                    atflv: classValue.get('atflv'),
                    atawe: classValue.get('atawe'),
                    atflb: classValue.get('atflb'),
                    ataw1: classValue.get('ataw1'),
                    atcod: classValue.get('atcod'),
                    attlv: classValue.get('attlv'),
                    attlb: classValue.get('attlb'),
                    mafid: classValue.get('mafid'),
                    klart: classValue.get('klart'),
                    clint: classValue.get('clint'),
                    adzhl: classValue.get('adzhl'),

                    updFlag: classValue.get('updFlag'),
                    mobileKey: classValue.get('mobileKey')

                });

            } catch (ex) {
                AssetManagement.customer.helper.Oxlogger.logException('Exception occurred inside cloneClassValue of BaseMultiValueCheckboxManager', ex);
            }
            return retval;
        },

        updateClassValuesForCharact: function (charact,newValues){
            try{
                var newValues = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });

                var classValues = charact.get('classValues');
                var charactValues = charact.get('chararactValues');
                
                charactValues.each(function (values) {
                   
                });
            } catch (ex) {

            }
        }
    }
});