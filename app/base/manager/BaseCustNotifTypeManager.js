Ext.define('AssetManagement.base.manager.BaseCustNotifTypeManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.CustNotifTypeCache',
        'AssetManagement.customer.model.bo.CustNotifType',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.CustNotifTypeCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustNotifTypeManager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getNotifTypes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
				
		        var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.CustNotifType',
					autoLoad: false
		        });
		        
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCustNotifTypeStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTypes of BaseCustNotifTypeManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CUSTNOTIFTYPE', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTNOTIFTYPE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTypes of BaseCustNotifTypeManager', ex);
			}
	
			return retval;
		},
		
		getNotifType: function(qmart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(qmart);
		
		        if(fromCache) {
			         eventController.requestEventFiring(retval, fromCache);
			          return retval;
		         }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var custNotifType = me.buildCustNotifTypeFromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(custNotifType);
						eventController.requestEventFiring(retval, custNotifType);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifType of BaseCustNotifTypeManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QMART', qmart);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CUSTNOTIFTYPE', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTNOTIFTYPE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifType of BaseCustNotifTypeManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCustNotifTypeFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCustNotifTypeFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustNotifTypeFromDataBaseQuery of BaseCustNotifTypeManager', ex);
			}
			
			return retval;
		},
		
		buildCustNotifTypeStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var CustNotifType = this.buildCustNotifTypeFromDbResultObject(cursor.value);
						store.add(CustNotifType);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustNotifTypeStoreFromDataBaseQuery of BaseCustNotifTypeManager', ex);
		    }
		},
		
		buildCustNotifTypeFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.CustNotifType', {
					qmart: dbResult['QMART'],
					rbnr: dbResult['RBNR'],
			     	auart: dbResult['AUART'],
					stsma: dbResult['STSMA'],
					fekat: dbResult['FEKAT'],
					urkat: dbResult['URKAT'],
					makat: dbResult['MAKAT'],
					mfkat: dbResult['MFKAT'],
					otkat: dbResult['OTKAT'],
					sakat: dbResult['SAKAT'],
					artpr: dbResult['ARTPR'], //not in databasestructure.json 
					createAllowed: dbResult['CREATE_ALLOWED'],
					qmartx: dbResult['QMARTX'],
					smstsma: dbResult['SMSTSMA'],
					partnerTransfer: dbResult['PARTNER_TRANSFER'],
                    artpr: dbResult['ARTPR'],
                    close_amer: dbResult['CLOSE_AMER'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['QMART']);
						
			} catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustNotifTypeFromDbResultObject of BaseCustNotifTypeManager', ex);
		    }
			
			return retval;
		}				
	}
});