Ext.define('AssetManagement.base.manager.BaseStatusManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Stsma',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public methods
		getStatusListForSchema: function(stsma, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stsma)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				var me = this;
				if(this._cacheInitialized === true) {
					var statusSchemaList = me.getStatusListForSchemaFromCache(stsma);

					statusSchemaList = statusSchemaList ? statusSchemaList : null;
					eventController.requestEventFiring(retval, statusSchemaList);
					return retval;
				} else {
					var cacheFilledCallback = function() {
						try {
						    var statusSchemaList = me.getStatusListForSchemaFromCache(stsma);
						    statusSchemaList = statusSchemaList ? statusSchemaList : null;
							eventController.requestEventFiring(retval, statusSchemaList);
						} catch(ex) {
							AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusListForSchema of BaseStatusManager', ex);
						}
					}
				
					var requestToWaitFor = this.fillCache(useBatchProcessing);
					eventController.registerOnEventForOneTime(requestToWaitFor, cacheFilledCallback);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusListForSchema of BaseStatusManager', ex);
			}
			
			return retval;
		},
		
		getStatus: function(stsma, estat, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stsma) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(estat)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				if(this._cacheInitialized === true) {
					var status = this.getStatusFromCache(stsma);
				
					eventController.requestEventFiring(retval, status);
					return retval;
				} else {
					var cacheFilledCallback = function() {
						try {
							var status = this.getStatusFromCache(stsma);
							eventController.requestEventFiring(retval, status);
						} catch(ex) {
							AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatus of BaseStatusManager', ex);
						}
					}
				
					var requestToWaitFor = this.fillCache(useBatchProcessing);
					eventController.registerOnEventForOneTime(requestToWaitFor, cacheFilledCallback);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatus of BaseStatusManager', ex);
			}
			
			return retval;
		},
		
		loadAllStatusIntoCache: function(useBatchProcessing, prefix) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var toFill = this.getListCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
						
						me.buildStatusStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me._listCacheComplete = true; 
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrders of BaseOrderManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};				
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STSMA', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STSMA', keyRange, successCallback, null, useBatchProcessing);

			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrders of BaseOrderManager', ex);
			}
			
			return retval;
		},
		
		buildStatusStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var order = this.buildStatusFromDbResultObject(cursor.value);
						store.add(order);
				
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStatusStoreFromDataBaseQuery of BaseOrderManager', ex);
			}
		},
		
		//private methods
		fillCache: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				this.clearCache();
				var cacheRef = this.getCache();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						var status = me.buildStatusFromDataBaseQuery.call(me, eventArgs);
						
						if(status) {
							var schemaStore = cacheRef.get(status.get('stsma'));
							
							if(!schemaStore) {
								schemaStore = Ext.create('Ext.data.Store', {
									model: 'AssetManagement.customer.model.bo.Stsma',
									autoLoad: false
								});
								
								cacheRef.add(status.get('stsma'), schemaStore);
							}
							
							schemaStore.add(status);
						}
						
						if(done) {
							me._cacheInitialized = true;
							eventController.fireEvent(retval);
						} else {
						    AssetManagement.customer.helper.CursorHelper.doCursorContinue(eventArgs.target.result);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillCache of BaseStatusManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STSMA', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STSMA', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillCache of BaseStatusManager', ex);
			}
	
			return retval;
		},
		
		buildStatusFromDataBaseQuery: function(eventArgs) {
	        var retval = null;
		
	        try {
	        	if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildStatusFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStatusFromDataBaseQuery of BaseStatusManager', ex);
			}
			
			return retval;
		},
		
		buildStatusFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Stsma', {
					stsma: dbResult['STSMA'],
					estat: dbResult['ESTAT'],
				    txt04: dbResult['TXT04'],
				    txt30: dbResult['TXT30'],
				    inist: dbResult['INIST'],
				    stonr: dbResult['STONR'],
				    hsonr: dbResult['HSONR'],
				    nsonr: dbResult['NSONR'],
				    linep: dbResult['LINEP'],
				    statp: dbResult['STATP'],
				    bersl: dbResult['BERSL'],
	                updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['STSMA'] + dbResult['ESTAT']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildStatusFromDbResultObject of BaseStatusManager', ex);
			}
			
			return retval;
		},
		
		//cache administration
		_schemaCache: null,
		_cacheInitialized: false,
	
		getCache: function() {
	        try {
	        	if(!this._schemaCache) {
					this._schemaCache = Ext.create('Ext.util.HashMap');
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseStatusManager', ex);
			}
			
			return this._schemaCache;
		},
		
		//cache administration	
		_statusListCache: null,
		_listCacheComplete: false,
		
		getListCache: function() {
	        try {
	        	if(!this._statusListCache) {
					this._statusListCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Stsma',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getListCache of BaseStatusManager', ex);
			}
			
			return this._statusListCache;
		},
		
		clearCache: function() {
	        try {
			    this.getCache().clear();
			    this._cacheInitialized = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseStatusManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseStatusManager', ex);
			} 
		},
		
		getStatusListForSchemaFromCache: function(stsma) {
			var retval = null;

			try {
				if(this._cacheInitialized === false)
					retval = undefined;
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stsma))
					retval = this.getCache().get(stsma);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusListForSchemaFromCache of BaseStatusManager', ex);
			}
			
			return retval;
		},
		
		getStatusFromCache: function(stsma, estat) {
			var retval = null;

			try {
				if(this._cacheInitialized === false)
					retval = undefined;
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(stsma) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(estat)) {
					var statusSchemaStore = this.getCache().get(stsma);
					
					if(statusSchemaStore)
						retval = statusSchemaStore.getById(stsma + estat);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusFromCache of BaseStatusManager', ex);
			}
			
			return retval;
		}
	}
});