Ext.define('AssetManagement.base.manager.BaseCust_043Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Cust_043',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
        /*
         * returns a list of cust_043 objects for the current user and scenario
         */
		//@public		
		getCust_043: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Cust_043',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_043StoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							eventController.requestEventFiring(retval, fromDataBase);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_043 of BaseCust_043Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_043', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_043', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_043 of BaseCust_043Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_043DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {							
			     if(eventArgs) {
				    if(eventArgs.target.result) {
					   retval = this.buildCust_043FromDbResultObject(eventArgs.target.result.value);					
				    }
			     }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_043DataBaseQuery of BaseCust_043Manager', ex);
			}
			return null;
		},
		
		buildCust_043StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_043 = this.buildCust_043FromDbResultObject(cursor.value);
						store.add(cust_043);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_043StoreFromDataBaseQuery of BaseCust_043Manager', ex);
			}
		},
		
		buildCust_043FromDbResultObject: function(dbResult) {
            var retval = null;
	
            try {
            	retval = Ext.create('AssetManagement.customer.model.bo.Cust_043', {
    				updflag: dbResult['UPDFLAG'],
            	    scenario: dbResult['SCENARIO'],
    				acgroup: dbResult['ACGROUP'],
    				actype: dbResult['ACTYPE']
    			});
    			
    			retval.set('id', dbResult['SCENARIO'] + dbResult['ACGROUP'] + dbResult['ACTYPE'] );
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_043StoreFromDataBaseQuery of BaseCust_043Manager', ex);
		    }
			return retval;
		}
	}
});