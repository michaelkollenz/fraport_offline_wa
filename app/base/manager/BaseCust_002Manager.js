Ext.define('AssetManagement.base.manager.BaseCust_002Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Cust_002',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public
		getCust_002s: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Cust_002',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_002StoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_002s of BaseCust_002Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_002', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_002', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_002s of BaseCust_002Manager', ex);
			}
	
			return retval;
		},
		
		getCust_002: function(mobileuser, werks, lgort, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobileuser)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_002 = me.buildCust_002DataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, cust_002);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_002 of BaseCust_002Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('MOBILEUSER', mobileuser);
				keyMap.add('WERKS', werks);
				keyMap.add('LGORT', lgort);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_002', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_002', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_002 of BaseCust_002Manager', ex);
			}
			
			return retval;
		},

	    //returns the default storage location for a user
		getDefaultLgortForUser: function (mobileuser, useBatchProcessing) {
		    var retval = -1;

		    try {
		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        var retval = eventController.getNextEventId();

		        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobileuser)) {
		            eventController.requestEventFiring(retval, null);
		            return retval;
		        }

		        var defaultLgort = null;

		        var me = this;
		        var successCallback = function (eventArgs) {
		            try {
		                var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
		                var found = !done && eventArgs.target.result.value['DEFAULT_LGORT'] === 'X';

		                if (found || !defaultLgort) {
		                    defaultLgort = me.buildCust_002DataBaseQuery.call(me, eventArgs);
		                }

		                if (done || found) {
		                    eventController.fireEvent(retval, defaultLgort);
		                } else {
		                    AssetManagement.customer.helper.CursorHelper.doCursorContinue(eventArgs.target.result);
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDefaultLgortForUser of BaseCust_002Manager', ex);
		                eventController.fireEvent(retval, undefined);
		            }
		        };

		        var keyMap = Ext.create('Ext.util.HashMap');
		        keyMap.add('MOBILEUSER', mobileuser);

		        var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_002', keyMap);
		        AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_002', keyRange, successCallback, null, useBatchProcessing);
		    } catch (ex) {
		        retval = -1;
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDefaultLgortForUser of BaseCust_002Manager', ex);
		    }

		    return retval;
		},
		
		//private
		buildCust_002DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCust_002FromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_002DataBaseQuery of BaseCust_002Manager', ex);
			}
			return retval;
		},
		
		buildCust_002StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_002 = this.buildCust_002FromDbResultObject(cursor.value);
						store.add(cust_002);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_002StoreFromDataBaseQuery of BaseCust_002Manager', ex);
			}
		},
		
		buildCust_002FromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Cust_002', {
					mobileuser: dbResult['MOBILEUSER'],
			     	werks: dbResult['WERKS'],
				    lgort: dbResult['LGORT'],
				    default_lgort: dbResult['DEFAULT_LGORT'],
				    wrkbe: dbResult['WRKBE'],
				    lgobe:dbResult['LGOBE'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['MOBILEUSER'] + dbResult['WERKS'] + dbResult['LGORT']);				
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_002FromDbResultObject of BaseCust_002Manager', ex);
			}
			return retval;
		}
	}
});