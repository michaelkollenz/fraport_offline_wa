Ext.define('AssetManagement.base.manager.BaseCust_007Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.Cust_007Cache',
        'AssetManagement.customer.model.bo.Cust_007',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.Cust_007Cache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCust_007Manager', ex);
			}
			
			return retval;
		},
		
		//public
		getCust_007s: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			       //it can, so return just this store
			       eventController.requestEventFiring(retval, fromCache);
			       return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
		        	model: 'AssetManagement.customer.model.bo.Cust_007',
		        	autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_007StoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_007s of BaseCust_007Manager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_007', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_007', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_007s of BaseCust_007Manager', ex);
			}
	
			return retval;
		},
		
		getCust_007: function(qmart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(qmart);
		
		        if(fromCache) {
			       eventController.requestEventFiring(retval, fromCache);
		 	       return retval;
		        }
		
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_007 = me.buildCust_007DataBaseQuery.call(me, eventArgs);
						cache.addToCache(cust_007);
						eventController.requestEventFiring(retval, cust_007);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_007 of BaseCust_007Manager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QMART', qmart);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_007', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_007', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_007 of BaseCust_007Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_007DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCust_007FromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_007DataBaseQuery of BaseCust_007Manager', ex);
			}
			
			return retval;
		},
		
		buildCust_007StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_007 = this.buildCust_007FromDbResultObject(cursor.value);
						store.add(cust_007);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_007StoreFromDataBaseQuery of BaseCust_007Manager', ex);
			}
		},
		
		buildCust_007FromDbResultObject: function(dbResult) {
            var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Cust_007', {
					qmart: dbResult['QMART'],
			     	qherk: dbResult['QHERK'],
				    qpart: dbResult['QPART'],
				    insplotauto: dbResult['INSPLOT_AUTO'],
				    refobject: dbResult['REF_OBJECT'],
				    detplant:dbResult['DET_PLANT'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['QMART']);
					    
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_007FromDbResultObject of BaseCust_007Manager', ex);
			}
			return retval;
		}
	}
});