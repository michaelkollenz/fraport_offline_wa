Ext.define('AssetManagement.base.manager.BaseComponentManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.OrdComponent',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		EVENTS: {
			COMPONENT_ADDED: 'componentAdded',
			COMPONENT_CHANGED: 'componentChanged',
			COMPONENT_DELETED: 'componentDeleted'
		},
	
		//public methods
		getComponentsForOrder: function(aufnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var ordersComponents = this.getFromCache(aufnr);
					
				if(ordersComponents) {
					eventController.requestEventFiring(retval, ordersComponents);
					return retval;
				}
				
				ordersComponents = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.OrdComponent',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
												
						me.buildComponentsStoreFromDataBaseQuery.call(me, ordersComponents, eventArgs);
						
						if(done) {
							me.addToCache(aufnr, ordersComponents);
							
							me.loadDependendDataForComponents.call(me, ordersComponents, retval);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getComponentsForOrder of BaseComponentManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('AUFNR', aufnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_ORDCOMP', 'AUFNR-VORNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_ORDCOMP', 'AUFNR-VORNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getComponentsForOrder of BaseComponentManager', ex);
			}
			
			return retval;
		},
		
		//saves a component - if it is neccessary it will get a new counter first
		saveComponent: function(component) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!component || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(component.get('rsnum'))) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = component.get('updFlag');
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;
	
				this.manageUpdateFlagForSaving(component);
			
				//check if the component already has a rsnum
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(component.get('rspos'));
				
				var me = this;
				var saveFunction = function(nextRspos) {
					try {
						if(requiresNewCounterValue && nextRspos !== -1)
						{
							counter = AssetManagement.customer.utils.StringUtils.padLeft(nextRspos, '0', 4);
							component.set('rspos', counter);
						}											
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(component.get('rspos'))) {
							eventController.fireEvent(retval, false);
							return;
						}
						
						//mobilekey management
						if(isInsert === true && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(component.get('childKey')))
							component.set('childKey', AssetManagement.customer.manager.KeyManager.createKey([ component.get('rsnum'), component.get('rspos') ]));
						
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								
								if(success) {
									if(isInsert)
										me.addSingleComponentToCache(component);
								
									var eventType = isInsert ? me.EVENTS.COMPONENT_ADDED : me.EVENTS.COMPONENT_CHANGED;
									eventController.fireEvent(eventType, component);
								}
							
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveComponent of BaseComponentManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForComponent(component);

						if(isInsert) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_ORDCOMP', toSave, callback, callback);
						} else {
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_ORDCOMP', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveComponent of BaseComponentManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextRspos(component);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveComponent of BaseComponentManager', ex);
			}
			
			return retval;
		},
		
		deleteComponent: function(component, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!component) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(component);	//includes removing from cache
							eventController.fireEvent(me.EVENTS.COMPONENT_DELETED, component);
						}
						
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponent of BaseComponentManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//distinguish if we have to update the database record or can delete it directly
				//this depends on it's former updateflag
				var hasToBeUpdated = component.get('updFlag') !== 'I';
				
				if(hasToBeUpdated) {
					component.set('updFlag', 'D')
					var toUpdate = me.buildDataBaseObjectForComponent(component);
					AssetManagement.customer.core.Core.getDataBaseHelper().update('D_ORDCOMP', null, toUpdate, callback, callback);
				} else {
					//get the key of the component to delete
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_ORDCOMP', component);
					AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_ORDCOMP', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponent of BaseComponentManager', ex);
			}
			
			return retval;
		},

		//private methods
		loadDependendDataForComponents: function(ordersComponents, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(ordersComponents.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, ordersComponents);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var materials = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignDependendDataForComponents.call(me, eventIdToFireWhenComplete, ordersComponents, materials);
					}
				};
				
				//get materials
				done++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccured = materialStore === undefined;
					
					materials = materialStore;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				if(done > 0)
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				else
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, ordersComponents);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForComponents of BaseComponentManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForComponents: function(eventIdToFireWhenComplete, components, materials) {
			try {
				components.each(function (component, index, length) {
					//assign materials
					if(materials) {
						var matnr = component.get('matnr');
						
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								component.set('material', material);
								return false;
							}
						});
					}
				});
				
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, components);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForComponents of BaseComponentManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		buildComponentFromDataBaseQuery: function(eventArgs) {
	        var retval = null;
	        
	        try {
	        	if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildComponentFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildComponentFromDataBaseQuery of BaseComponentManager', ex);
			}
			return retval;
		},
		
		buildComponentsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	//exclude components already flagged for delete
				    	if("D" !== cursor.value['UPDFLAG']) {
					    	var component = this.buildComponentFromDbResultObject(cursor.value);
							store.add(component);
				    	}
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildComponentsStoreFromDataBaseQuery of BaseComponentManager', ex);
			}
		},

		buildComponentFromDbResultObject: function(dbResult) {
	       var retval = null;
	       
	       try {
			   retval = Ext.create('AssetManagement.customer.model.bo.OrdComponent', {
				   rsnum: dbResult['RSNUM'],
				   rspos: dbResult['RSPOS'],
				   aufnr: dbResult['AUFNR'],
				   vornr: dbResult['VORNR'],
				   xloek: dbResult['XLOEK'],
				   kzear: dbResult['KZEAR'],
				   matnr: dbResult['MATNR'],
				   werks: dbResult['WERKS'],
				   lgort: dbResult['LGORT'],
				   bdmng: dbResult['BDMNG'],
				   meins: dbResult['MEINS'],
				   enmng: dbResult['ENMNG'],
				   erfmg: dbResult['ERFMG'],
				   erfme: dbResult['ERFME'],
				   bwart: dbResult['BWART'],
				   sgtxt: dbResult['SGTXT'],
				   postp: dbResult['POSTP'],
				   vmeng: dbResult['VMENG'],
			       ltxsp: dbResult['LTXSP'],
                   idnlf: dbResult['IDNLF'],
                   infnr: dbResult['INFNR'],

				   matxt: dbResult['MATXT'],
				   ekorg: dbResult['EKORG'],
				   ekrgp: dbResult['EKGRP'],
				   afnam: dbResult['AFNAM'],
				   saknr: dbResult['SAKNR'],
				   lifnr: dbResult['LIFNR'],
				   gpreis:dbResult['GPREIS'],
				   waers: dbResult['WAERS'],
				   wempf: dbResult['WEMPF'],
				   ablad: dbResult['ABLAD'],
				   charg: dbResult['CHARG'],

				   mobileKey: dbResult['MOBILEKEY'],
				   childKey: dbResult['CHILDKEY'],
				
			       updFlag: dbResult['UPDFLAG']

			    });		
			   
			    retval.set('id', dbResult['RSNUM'] + dbResult['RSPOS']);
					
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildComponentFromDbResultObject of BaseComponentManager', ex);
			}
			return retval;
		},
		
		//builds the raw object to store in the database out of a component model
		buildDataBaseObjectForComponent: function(component) {
			var retval = null;
			var ac = null;
		
			try {
				ac = AssetManagement.customer.core.Core.getAppConfig();
			
		        retval = { };
		        retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['RSNUM'] = component.get('rsnum');
				retval['RSPOS'] = component.get('rspos');
				retval['AUFNR'] = component.get('aufnr');
				retval['VORNR'] = component.get('vornr');
				retval['XLOEK'] = component.get('xloek');
				retval['KZEAR'] = component.get('kzear');
				retval['MATNR'] = component.get('matnr');
				retval['WERKS'] = component.get('werks');
				retval['LGORT'] = component.get('lgort');
				retval['BDMNG'] = component.get('bdmng');
				retval['MEINS'] = component.get('meins');
				retval['ENMNG'] = component.get('enmng');
				retval['ERFMG'] = component.get('erfmg');
				retval['ERFME'] = component.get('erfme');
				retval['BWART'] = component.get('bwart');
				retval['SGTXT'] = component.get('sgtxt');
				retval['POSTP'] = component.get('postp');
				retval['VMENG'] = component.get('vmeng');

				retval['MATXT'] = component.get('matxt');
				retval['EKORG'] = component.get('ekorg');
				retval['EKGRP'] = component.get('ekrgp');
				retval['AFNAM'] = component.get('afnam');
				retval['SAKNR'] = component.get('saknr');
				retval['LIFNR'] = component.get('lifnr');
				retval['GPREIS'] = component.get('gpreis');
				retval['WAERS'] = component.get('waers');
				retval['WEMPF'] = component.get('wempf');
				retval['ABLAD'] = component.get('ablad');
				retval['CHARG'] = component.get('charg');
				
				retval['UPDFLAG'] = component.get('updFlag');
				retval['MOBILEKEY'] = component.get('mobileKey');
				retval['CHILDKEY'] = component.get('childKey');
						
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForComponent of BaseComponentManager', ex);
			}
			
			return retval;
		},
		
		getNextRspos: function(component) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!component) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxRspos = 0;
				
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
							var rsposValue = cursor.value['RSPOS'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(rsposValue, '%')) {
                                //cut of the percent sign
							    rsposValue = rsposValue.substr(1);
                                var curRsposValue = parseInt(rsposValue);
							
							    if(!isNaN(curRsposValue))
							    {
								    if (curRsposValue > maxRspos)
                                        maxRspos = curRsposValue;
							    } 
                            }
				
							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {

                            var nextLocalCounterNumberValue = maxRspos + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
						    eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextRspos of BaseComponentManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('RSNUM', component.get('rsnum'));
				
                //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ORDCOMP', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().rawQuery('D_ORDCOMP', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextRspos of BaseComponentManager', ex);
			}
			
			return retval;			
		},
		
		//cache administration
		_componentsCache: null,
	
		getCache: function() {
	        try {
	        	if(!this._componentsCache) {
					this._componentsCache = Ext.create('Ext.util.HashMap');
					
					this.initializeCacheHandling();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseComponentManager', ex);
			}
			
			return this._componentsCache;
		},
	
		//public methods
		clearCache: function() {
			try {
			    this.getCache().clear();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseComponentManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseComponentManager', ex);
			} 
		},
		
		addToCache: function(aufnr, componentStore) {
			try {
				this.getCache().add(aufnr, componentStore);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseComponentManager', ex);
			} 
		},
		
		getFromCache: function(aufnr) {
			var retval = null;
		
			try {
				retval = this.getCache().get(aufnr);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseComponentManager', ex);
			}
			
			return retval;
		},
		
		addSingleComponentToCache: function(component) {
			try {
				var targetStore = this.getCache().get(component.get('aufnr'));
				
				if(!targetStore) {
					targetStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.OrdComponent',
						autoLoad: false
					});
					
					this.getCache().add(component.get('aufnr'), targetStore);
				}
				
				targetStore.add(component);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addSingleComponentToCache of BaseComponentManager', ex);
			}
		}
	}
});