Ext.define('AssetManagement.base.manager.BaseScheduleManager', {
	requires: [
	    'AssetManagement.customer.controller.EventController',
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
//        'AssetManagement.customer.manager.OrderManager',		//CAUSES RING DEPENDENCY
//        'AssetManagement.customer.manager.NotifManager',		//CAUSES RING DEPENDENCY
//        'AssetManagement.customer.manager.EquipmentManager',	//CAUSES RING DEPENDENCY
        'AssetManagement.customer.manager.PartnerAPManager',
        'AssetManagement.customer.model.bo.Notif',
        'AssetManagement.customer.model.bo.NotifTask',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public
		//upgrades schedule items with further details
		//this process is complex and may be heavy
		prepareScheduleItemsForPrint: function(scheduleItems) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				retval = eventController.getNextEventId();
				
				if(!scheduleItems) {
					eventController.requestEventFiring(retval, undefined);
					return;
				}
				
				var toPrepareCount = scheduleItems.getCount();
				
				if(toPrepareCount === 0) {
					eventController.requestEventFiring(retval, scheduleItems);
					return;
				}
				
				var currentPendingEvent = 0;
				var preparedCount = 0;
			
				var me = this;
				//get all detail data for each schedule item
				var itemPreparedCallback = function(preparedItem) {
					try {
						preparedCount++;	
					
						if(preparedItem) {
							//check if the work completed
							if(preparedCount === toPrepareCount) {
								eventController.requestEventFiring(retval, scheduleItems);
							} else {
								//pass the next request
								currentPendingEvent = me.prepareScheduleItemForPrint(scheduleItems.getAt(preparedCount));
				
								if(currentPendingEvent > 0) {
									eventController.registerOnEventForOneTime(currentPendingEvent, itemPreparedCallback);
								} else {
									eventController.requestEventFiring(retval, undefined);
								}
							}
						} else {
							//an error occured, abort process and signalize the fail
							eventController.requestEventFiring(retval, undefined);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareScheduleItemsForPrint of BaseScheduleManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var firstItem = scheduleItems.getAt(0);
				currentPendingEvent = this.prepareScheduleItemForPrint(firstItem);
				
				if(currentPendingEvent > 0) {
					eventController.registerOnEventForOneTime(currentPendingEvent, itemPreparedCallback);
				} else {
					eventController.requestEventFiring(retval, undefined);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareScheduleItemsForPrint of BaseScheduleManager', ex);
				retval = -1;
			}
			
			return retval;
		},
	
		prepareScheduleItemForPrint: function(scheduleItem) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!scheduleItem) {
					eventController.requestEventFiring(retval, undefined);
					return;
				}
				
				//depenend database requests have to be performed to get all detail data
				//so there will be different steps, start with the first
				this.prepareScheduleItemForPrintStep1(scheduleItem, retval);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareScheduleItemForPrint of BaseScheduleManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		//will generate a store for all operations
		getScheduleItemsStoreForAllOperations: function() {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var me = this;

				var ordersCallback = function(ordersList) {
					try {
						if(ordersList === undefined) {
							eventController.fireEvent(retval, undefined);
						} else {
							var scheduleItems = me.buildScheduleItemsStoreForOrders(ordersList);
							eventController.fireEvent(retval, scheduleItems);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScheduleItemsStoreForAllOperations of BaseScheduleManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				eventId = AssetManagement.customer.manager.OrderManager.getOrders(true, false);
				if(eventId > 0) {
					eventController.registerOnEventForOneTime(eventId, ordersCallback);
				} else {
					eventController.requestEventFiring(retval, undefined);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScheduleItemsStoreForAllOperations of BaseScheduleManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		buildScheduleItemsStoreForOrders: function(orders) {
			var retval = null;
		
			try {
				//extract all operations from the orders
				var retval = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ScheduleItem',
					autoLoad: false
				});
	
				if(orders) {
					orders.each(function(order) {
						var orderOperations = order.get('operations');
						
						if(orderOperations) {
							orderOperations.each(function(operation) {
								var scheduleItem = Ext.create('AssetManagement.customer.model.bo.ScheduleItem', {
									selected: true,
									operation: operation,
									order: order,
									equipment: order.get('equipment'),
									address: order.get('address')
								});
								
								retval.add(scheduleItem);
							}, this);
						}
					}, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildScheduleItemsStore of BaseScheduleManager', ex);
			}
			
			return retval;
		},
		
		//private
		prepareScheduleItemForPrintStep1: function(scheduleItem, eventIdToFireWhenComplete) {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var me = this;
				
				//this step will pass requests for the following: 
				// - order in rich state
				var connectedOperation = scheduleItem.get('operation');
				var aufnr = connectedOperation ? connectedOperation.get('aufnr') : '';
				
				var order = null;
				
				var pendingRequests = 0;			
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccured === false) {
						me.prepareScheduleItemForPrintStep2.call(me, eventIdToFireWhenComplete, scheduleItem, order);
					}
				};
				
				//get order
				var eventId = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
				if(eventId > 0) {
					pendingRequests++;
					
					var orderCallback = function(itemsOrder) {
						errorOccured = itemsOrder === undefined;
						
						order = itemsOrder;
						pendingRequests--;
						
						completeFunction();
					};
					
					eventController.registerOnEventForOneTime(eventId, orderCallback);
				} else {
					errorOccured = true;
				}
				
				if(errorOccured === true) {
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
				} else {
					if(pendingRequests > 0)
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					else
						this.prepareScheduleItemForPrintStep2(eventIdToFireWhenComplete, scheduleItem, order, operationsObjectListItem);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareScheduleItemForPrintStep1 of BaseScheduleManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		prepareScheduleItemForPrintStep2: function(eventIdToFireWhenComplete, scheduleItem, order) {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var me = this;
				var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
				
				//before passing step 2s requests, process the passed data
				scheduleItem.set('order', order);
				
				var ordersOperations = order.get('operations');
				var scheduleItemsVornr = scheduleItem.get('operation').get('vornr');
				var richOperation = null;
				
				if(ordersOperations && ordersOperations.getCount() > 0) {
					ordersOperations.each(function(ordOper) {
						if(scheduleItemsVornr === ordOper.get('vornr')) {
							richOperation = ordOper;
							return false;
						}
					}, this);
				}
				
				if(richOperation)
					scheduleItem.set('operation', richOperation);
				
				//get the objectlistItem which is connected to the schedule item's operation
				var operationsObjectListItem = null;
				var ordersObjectList = order ? order.get('objectList') : null;
				
				if(ordersObjectList && ordersObjectList.getCount() > 0) {
					var operation = scheduleItem.get('operation');
					var obknrToCheckFor = operation.get('obknr');
					var obzaeToCheckFor = operation.get('obzae');
				
					ordersObjectList.each(function(objectListItem) {
						if(objectListItem.get('obknr') === obknrToCheckFor && objectListItem.get('obzae')  === obzaeToCheckFor) {
							operationsObjectListItem = objectListItem;
							return false;
						}
					}, this);
				}
				
				var drawEquipmentFromObjectListItem = false;
				var drawEquipmentFromObjectListItemsNotification = false;
				
				//the equipment will be set if there is no reference found in the first objectlist entry for an equipment or a notification
				if(operationsObjectListItem) {
					var drawEquipmentFromObjectListItem = !shortHand(operationsObjectListItem.get('eqnur'));
					var drawEquipmentFromObjectListItemsNotification = drawEquipmentFromObjectListItem === false && !shortHand(operationsObjectListItem.get('ihnum'));
				}
				
				if(order && drawEquipmentFromObjectListItem === false && drawEquipmentFromObjectListItemsNotification === false) {
					scheduleItem.set('equipment', order.get('equipment'));
				}
				
				//this step will pass requests for the the following: 
				// - specific partner ap
				// - a set of specific notifs
				// - optionally another specific notif (from operations objectlist)
				// - optionally a specific equipment (from operations objectlist)
				
				var partnerAP = null;
				var connectedNotifsStore = null;
				var objectListNotif = null;
				var equipmentFromObjectList = null;
				
				var pendingRequests = 0;			
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccured === false) {
						me.prepareScheduleItemForPrintStep3.call(me, eventIdToFireWhenComplete, scheduleItem, partnerAP, connectedNotifsStore, objectListNotif, equipmentFromObjectList);
					}
				};
				
				//begin get partnerAP
				var ordersPartners = order ? order.get('partners') : null;
				
				if(ordersPartners && ordersPartners.getCount() > 0) {
					var partnerAPToFind = null;
				
					ordersPartners.each(function(partner) {
						if(partner.get('parvw') === 'AP') {
							partnerAPToFind = partner.get('parnr');
							return false;
						}
					}, this);
					
					if(!shortHand(partnerAPToFind)) {
						var eventId = AssetManagement.customer.manager.PartnerAPManager.getPartnerAP(partnerAPToFind, true);
						if(eventId > 0) {
							pendingRequests++;
							
							var partnerAPCallback = function(itemsOrder) {
								errorOccured = itemsOrder === undefined;
								
								order = itemsOrder;
								pendingRequests--;
								
								completeFunction();
							};
							
							eventController.registerOnEventForOneTime(eventId, partnerAPCallback);
						} else {
							errorOccured = true;
						}
					}
				}
				//end get partnerAP
				
				//begin get notifs
				var ordersObjectList = order ? order.get('objectList') : null;
				
				if(errorOccured === false && ordersObjectList && ordersObjectList.getCount() > 0) {
					var qmnumsToLookFor = new Array();
				
					ordersObjectList.each(function(objectListItem) {
						var ihnum = objectListItem.get('ihnum');
					
						if(!shortHand(ihnum))
							qmnumsToLookFor.push(ihnum);
					}, this);
					
					if(qmnumsToLookFor.length > 0) {
						connectedNotifsStore = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Notif',
							autoLoad: false
						});
					
						//drop a request for each qmnum
						Ext.Array.each(qmnumsToLookFor, function(qmnum) {
							var eventId = AssetManagement.customer.manager.NotifManager.getNotif(qmnum, true);
							if(eventId > 0) {
								pendingRequests++;
								
								var notifCallback = function(notification) {
									errorOccured = notification === undefined;
									
									if(errorOccured === false)
										connectedNotifsStore.add(notification);
									
									pendingRequests--;
									
									completeFunction();
								};
								
								eventController.registerOnEventForOneTime(eventId, notifCallback);
							} else {
								errorOccured = true;
							}
						}, this);
					}
				}
				//end get notifs
				
				//begin get equipment('s source)
				if(errorOccured === false) {
					if(drawEquipmentFromObjectListItem === true) {
						var eventId = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(operationsObjectListItem.get('equnr'), true);
						if(eventId > 0) {
							pendingRequests++;
							
							var equipmentCallback = function(equi) {
								errorOccured = equi === undefined;
								
								equipmentFromObjectList = equi;
								pendingRequests--;
								
								completeFunction();
							};
							
							eventController.registerOnEventForOneTime(eventId, equipmentCallback);
						} else {
							errorOccured = true;
						}
					} else if(drawEquipmentFromObjectListItem === true) {
						var eventId = AssetManagement.customer.manager.NotifManager.getNotif(operationsObjectListItem.get('ihnum'), true);
						if(eventId > 0) {
							pendingRequests++;
							
							var objectListNotifCallback = function(notification) {
								errorOccured = notification === undefined;
								
								objectListNotif = notification;
								pendingRequests--;
								
								completeFunction();
							};
							
							eventController.registerOnEventForOneTime(eventId, objectListNotifCallback);
						} else {
							errorOccured = true;
						}
					}
				}
				//end get equipment('s source)
				
				if(errorOccured === true) {
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
				} else {
					if(pendingRequests > 0)
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					else
						this.prepareScheduleItemForPrintStep3(eventIdToFireWhenComplete, scheduleItem, partnerAP, connectedNotifsStore, objectListNotif, equipmentFromObjectList);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareScheduleItemForPrintStep2 of BaseScheduleManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
	
		prepareScheduleItemForPrintStep3: function(eventIdToFireWhenComplete, scheduleItem, partnerAP, connectedNotifsStore, objectListNotif, equipmentFromObjectList) {
			try {
				//process the passed data
				scheduleItem.set('partnerAP', partnerAP);
				
				//set the equipment (keep in mind, not to set the orders one because this would behave like a fallback)
				var drawEquipmentFromObjectListItemsNotification = objectListNotif !== null && objectListNotif !== undefined;
				
				if(drawEquipmentFromObjectListItemsNotification === true) {
					scheduleItem.set('equipment', objectListNotif.get('equipment'));
				} else if(equipmentFromObjectList) {
					scheduleItem.set('equipment', objectListNotif.get('equipmentFromObjectList'));
				}
				
				//set the notifTasks store
				if(connectedNotifsStore && connectedNotifsStore.getCount() > 0) {
					var notifTasksStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});
					
					connectedNotifsStore.each(function(notif) {
						var notifsTasks = notif.get('notifTasks');
						
						if(notifsTasks && notifsTasks.getCount() > 0) {
							notifsTasks.each(function(notifTask) {
								notifTasksStore.add(notifTask);
							}, this);
						}
					}, this);
					
					scheduleItem.set('notifTasks', notifTasksStore);
				}
				
				//this step will pass no more requests
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, scheduleItem);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareScheduleItemForPrintStep3 of BaseScheduleManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		}	
	}
});