Ext.define('AssetManagement.base.manager.BaseInventoryItemsManager', {
    extend: 'AssetManagement.customer.manager.OxBaseManager',
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.InventoryItem',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.manager.cache.InventoryItemCache',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.model.bo.InventorySerialNo'
    ],

    inheritableStatics: {

        EVENTS: {
            INV_ADDED: 'invAdded',
            INV_CHANGED: 'invChanged',
            INV_SERIALNO_DELETE: 'invSerialNoDelete',
            INV_SERIALNO_ADDED: 'invSerialNoAdded'
        },
        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.InventoryItemCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseInventoryItemsManager', ex);
            }

            return retval;
        },


        //public methods
        getAllInventoryItems: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.InventoryItem',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildInventoryItemStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            me.loadListDependendDataForInventoryItems(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllInventoryItems of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MI_INV_ITEM', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_ITEM', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllInventoryItems of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        getInventoryItems: function (physinventory, fiscalyear, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(physinventory) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fiscalyear)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var inventoryItem = me.buildInventoryItemDataBaseQuery(eventArgs);
                        eventController.requestEventFiring(retval, inventoryItem);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventoryItems of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PHYSINVENTORY', physinventory);
                keyMap.add('FISCALYEAR', fiscalyear);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_MI_INV_ITEM', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_ITEM', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventoryItems of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        //private
        buildInventoryItemDataBaseQuery: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildInventoryItemFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryItemDataBaseQuery of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        buildInventoryItemStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var inventoryitem = this.buildInventoryItemFromDbResultObject(cursor.value);
                        store.add(inventoryitem);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryItemStoreFromDataBaseQuery of BaseInventoryItemsManager', ex);
            }
        },

        buildInventoryItemFromDbResultObject: function (dbResult) {
            var retval = null;
            try {
                retval = Ext.create('AssetManagement.customer.model.bo.InventoryItem', {
                    updFlag: dbResult['UPDFLAG'] ? dbResult['UPDFLAG'] : '',
                    physinventory: dbResult['PHYSINVENTORY'] ? dbResult['PHYSINVENTORY'] : '',
                    fiscalyear: dbResult['FISCALYEAR'] ? dbResult['FISCALYEAR'] : '',
                    item: dbResult['ITEM'] ? dbResult['ITEM'] : '',
                    material: dbResult['MATERIAL'] ? dbResult['MATERIAL'] : '',
                    plant: dbResult['PLANT'] ? dbResult['PLANT'] : '',
                    stgeLoc: dbResult['STGE_LOC'] ? dbResult['STGE_LOC'] : '',
                    batch: dbResult['BATCH'] ? dbResult['BATCH'] : '',
                    specStock: dbResult['SPEC_STOCK'] ? dbResult['SPEC_STOCK'] : '',
                    stockType: dbResult['STOCK_TYPE'] ? dbResult['STOCK_TYPE'] : '',
                    salesOrd: dbResult['SALES_ORD'] ? dbResult['SALES_ORD'] : '',
                    sOrdItem: dbResult['S_ORD_ITEM'] ? dbResult['S_ORD_ITEM'] : '',
                    schedLine: dbResult['SCHED_LINE'] ? dbResult['SCHED_LINE'] : '',
                    vendor: dbResult['VENDOR'] ? dbResult['VENDOR'] : '',
                    customer: dbResult['CUSTOMER'] ? dbResult['CUSTOMER'] : '',
                    probinloc: dbResult['PRODBINLOC'] ? dbResult['PRODBINLOC'] : '',
                    changeUser: dbResult['CHANGE_USER'] ? dbResult['CHANGE_USER'] : '',
		            changeDate: dbResult['CHANGE_DATE'] ? dbResult['CHANGE_DATE'] : '',
                    countUser: dbResult['COUNT_USER'] ? dbResult['COUNT_USER'] : '',
                    countDate: dbResult['COUNT_DATE'] ? dbResult['COUNT_DATE'] : '',
                    pstngName: dbResult['PSTNG_NAME'] ? dbResult['PSTNG_NAME'] : '',
                    pstngDate: dbResult['PSTNG_DATE'] ? dbResult['PSTNG_DATE'] : '',
                    phInvRef: dbResult['PH_INV_REF'] ? dbResult['PH_INV_REF'] : '',
                    counted: dbResult['COUNTED'] ? dbResult['COUNTED'] : '',
                    diffPosted: dbResult['DIFF_POSTED'] ? dbResult['DIFF_POSTED'] : '',
                    recount: dbResult['RECOUNT'] ? dbResult['RECOUNT'] : '',
                    deleteInd: dbResult['DELETE_IND'] ? dbResult['DELETE_IND'] : '',
                    altUnit: dbResult['ALT_UNIT'] ? dbResult['ALT_UNIT'] : '',
                    bookQty: dbResult['BOOK_QTY'] ? dbResult['BOOK_QTY'] : '',
                    zerocount: dbResult['ZERO_COUNT'] === 'X' ? true : false,
                    quantity: dbResult['QUANTITY'] ? dbResult['QUANTITY'] : '',
                    baseUom: dbResult['BASE_UOM'] ? dbResult['BASE_UOM'] : '',
                    baseUomIso: dbResult['BASE_UOM_ISO'] ? dbResult['BASE_UOM_ISO'] : '',
                    entryQnt: dbResult['ENTRY_QNT'] ? dbResult['ENTRY_QNT'] : '',
                    entryUom: dbResult['ENTRY_UOM'] ? dbResult['ENTRY_UOM'] : '',
                    entryUomIso: dbResult['ENTRY_UOM_ISO'] ? dbResult['ENTRY_UOM_ISO'] : '',
                    matDoc: dbResult['MAT_DOC'] ? dbResult['MAT_DOC'] : '',
                    docYear: dbResult['DOC_YEAR'] ? dbResult['DOC_YEAR'] : '',
                    matdocItm: dbResult['MATDOC_ITM'] ? dbResult['MATDOC_ITM'] : '',
                    recountDoc: dbResult['RECOUNTDOC'] ? dbResult['RECOUNTDOC'] : '',
                    difference: dbResult['DIFFERENCE'] ? dbResult['DIFFERENCE'] : '',
                    currency: dbResult['CURRENCY'] ? dbResult['CURRENCY'] : '',
                    currencyIso: dbResult['CURRENCY_ISO'] ? dbResult['CURRENCY_ISO'] : '',
                    ccPhInv: dbResult['CC_PH_INV'] ? dbResult['CC_PH_INV'] : '',
                    wbsElement: dbResult['WBS_ELEMENT'] ? dbResult['WBS_ELEMENT'] : '',
                    svIncVat: dbResult['SV_INC_VAT'] ? dbResult['SV_INC_VAT'] : '',
                    salesVal: dbResult['SALES_VAL'] ? dbResult['SALES_VAL'] : '',
                    bookValue: dbResult['BOOK_VALUE'] ? dbResult['BOOK_VALUE'] : '',
                    voMat: dbResult['VO_MAT'] ? dbResult['VO_MAT'] : '',
                    svWOVat: dbResult['SV_W_O_VAT'] ? dbResult['SV_W_O_VAT'] : '',
                    diffsalcntvat: dbResult['DIFFSALCNTVAT'] ? dbResult['DIFFSALCNTVAT'] : '',
                    diffsalcnt: dbResult['DIFFSALCNT'] ? dbResult['DIFFSALCNT'] : '',
                    valcountQnt: dbResult['VALCOUNTQNT'] ? dbResult['VALCOUNTQNT'] : '',
                    valBookAmt: dbResult['VALBOOKAMT'] ? dbResult['VALBOOKAMT'] : '',
                    diffValue: dbResult['DIFF_VALUE'] ? dbResult['DIFF_VALUE'] : '',
                    reason: dbResult['REASON'] ? dbResult['REASON'] : '',
                    materialExternal: dbResult['MATERIAL_EXT'] ? dbResult['MATERIAL_EXT'] : '',
                    materialGuid: dbResult['MATERIAL_GUID'] ? dbResult['MATERIAL_GUID'] : '',
                    materialVersion: dbResult['MATERIAL_VERSION'] ? dbResult['MATERIAL_VERSION'] : '',
                    phInvRefLong: dbResult['PH_INV_REF_LONG'] ? dbResult['PH_INV_REF_LONG'] : '',
                    maktx: dbResult['MAKTX'] ? dbResult['MAKTX'] : '',
                    lgpbe: dbResult['LGPBE'] ? dbResult['LGPBE'] : '',
                    serpr: dbResult['SERPR'] ? dbResult['SERPR'] : '',
                    x_sernr: dbResult['X_SERNR'] ? dbResult['X_SERNR'] : '',
                    mobileKey: dbResult['MOBILEKEY'] ? dbResult['MOBILEKEY'] : '',
                    plan: dbResult['PLAN_DATE'] ?  AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['PLAN_DATE'], dbResult['DOC_DATE']) : '',
                    localStatus: 'open'
                });
                retval.set('id', dbResult['PHYSINVENTORY'] + dbResult['FISCALYEAR'] + dbResult['ITEM']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryItemFromDbResultObject of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        // * save normal item from internal technician
        // * returns success value
        // */
        saveInventoryItems: function (inventoryItems, isAllCompleted) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!inventoryItems) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                } else if (inventoryItems.getCount() === 0) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }
                //save each inventory item
                var pendingSaveLoops = 0;
                var maxLevel = 1;
                var serialNoSaveCount = 0;
                var me = this;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    try {
                        if (errorOccurred === true && reported === false) {
                            eventController.fireEvent(retval, false);
                            reported = true;
                        } else if (pendingSaveLoops === 0 && errorOccurred === false) {

                            eventController.fireEvent(retval, true);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
                    }
                };

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        errorOccurred = errorOccurred || !success;

                        pendingSaveLoops--;
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
                        errorOccurred = true;
                    } finally {
                        completeFunction();
                    }
                };

                inventoryItems.each(function (inventoryItem) {
                    //if (parseInt(item.get('entryQnt')) > 0 || item.get('zerocount') === true /*|| !isAllCompleted */ || item.get('updFlag') === 'I') {
                        pendingSaveLoops++;

                        inventoryItem.set('updFlag', 'A');

                        var serialList = inventoryItem.get('seriallist');

                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(inventoryItem.get('serpr'))) {
                            if (inventoryItem.get('zerocount') === true) {
                                inventoryItem.set('localStatus', 'completed');

                                var emptySerialNo = Ext.create('Ext.data.Model', {
                                    item: inventoryItem.get('item'),
                                    fiscalyear: inventoryItem.get('fiscalyear'),
                                    physinventory: inventoryItem.get('physinventory')
                                });

                                var deleteCallback = function () {
                                    try {
                                        completeFunction();
                                    } catch (ex) {
                                        retval = -1;
                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
                                    }
                                };

                                var eventId = me.tryDeleteSerialNoForItem(emptySerialNo);
                                eventController.registerOnEventForOneTime(eventId, deleteCallback);

                            } else if (serialList && serialList.getCount() > 0) {
                                if (serialList.getCount() === parseInt(inventoryItem.get('entryQnt'))) {
                                    inventoryItem.set('localStatus', 'completed');
                                } else if (inventoryItem.get('entryQnt') > 0) {
                                    inventoryItem.set('localStatus', 'in process');
                                }
                                serialList.each(function (serialNo) {
                                    pendingSaveLoops++;
                                    if (isAllCompleted)
                                        serialNo.set('updFlag', 'I');
                                    else
                                        serialNo.set('updFlag', 'A');

                                    serialNo.set('item', inventoryItem.get('item'));
                                    serialNo.set('fiscalyear', inventoryItem.get('fiscalyear'));
                                    serialNo.set('physinventory', inventoryItem.get('physinventory'));

                                    var deleteCallback = function () {
                                        try {
                                            var toSave = me.buildDataBaseObjectForInventoryItemSerialNo(serialNo);
                                            AssetManagement.customer.core.Core.getDataBaseHelper().put('D_MI_INV_SERIAL', toSave, callback, callback);
                                            completeFunction();
                                        } catch (ex) {
                                            retval = -1;
                                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
                                        }
                                    };

                                    var eventId = me.tryDeleteSerialNoForItem(serialNo);
                                    eventController.registerOnEventForOneTime(eventId, deleteCallback);


                                }, this);
                            }
                        } else {
                            if (inventoryItem.get('entryQnt') > 0 || inventoryItem.get('zerocount') === true) {
                                inventoryItem.set('localStatus', 'completed');
                            }
                        }

                        if (isAllCompleted) {
                            inventoryItem.set('updFlag','I');
                            inventoryItem.set('counted', 'X');
                            inventoryItem.set('localStatus', 'completed');
                            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(inventoryItem.get('serpr'))) {
                                inventoryItem.set('x_sernr', 'X');
                            }
                        }
                        var toSave = this.buildDataBaseObjectForInventoryItem(inventoryItem);
                        AssetManagement.customer.core.Core.getDataBaseHelper().put('D_MI_INV_ITEM', toSave, callback, callback);
                        //AssetManagement.customer.core.Core.getDataBaseHelper().update('D_MI_INV_ITEM', null, toSave, callback, callback);
                        completeFunction();
                    //}
                }, this);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
            }

            return retval;
        },
        //private
        //will try to delete serial number
        //returns true, when the processing was successful
        tryDeleteSerialNoForItem: function (objectToDeleteFor) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToDeleteFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var parameters = {
                    item: objectToDeleteFor.get('item'),
                    fiscalyear: objectToDeleteFor.get('fiscalyear'),
                    physinventory: objectToDeleteFor.get('physinventory')
                };

                if (!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.item)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                //query database for serial number
                var successCallback = function (eventArgs) {
                    try {
                        if (eventArgs && eventArgs.target && eventArgs.target.result && eventArgs.target.result.value) {
                            var cursor = eventArgs.target.result;
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            eventController.fireEvent(retval, true);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteSerialNoForItem of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                //add keys to delete from database
                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PHYSINVENTORY', parameters.physinventory);
                keyMap.add('ITEM', parameters.item);
                keyMap.add('FISCALYEAR', parameters.fiscalyear);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MI_INV_SERIAL', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_SERIAL', keyRange, successCallback, null);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteSerialNoForItem of BaseInventoryItemsManager', ex);
                retval = -1;
            }

            return retval;
        },

        getItemsForInventory: function (inventoryHead, useBatchProcessing) {
            var retval = -1;
            try {

                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (!inventoryHead) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var me = this;

                var itemsStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.InventoryItem',
                    autoLoad: false
                });
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildInventoryItemStoreFromDataBaseQuery.call(me, itemsStore, eventArgs);

                        if (done) {
                            me.loadDependendDataForInventoryItems(itemsStore, retval);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventoryItems of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('PHYSINVENTORY', inventoryHead.get('physinventory'));
                indexMap.add('FISCALYEAR', inventoryHead.get('fiscalyear'));

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_MI_INV_ITEM', 'PHYSINVENTORY', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_MI_INV_ITEM', 'PHYSINVENTORY', indexRange, successCallback, null, useBatchProcessing);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getItemsForInventory of BaseInventoryItemsManager', ex);
            }
            return retval;
        },

        //private
        //@override
        loadListDependendDataForInventoryItems: function (eventIdToFireWhenComplete, inventoryItems) {
            try {
                //do not continue if there is no data
                if (inventoryItems && inventoryItems.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventoryItems);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var serialNumbers = null;

                var counter = 0;
                var done = 0;
                var erroroccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (erroroccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && erroroccurred === false) {
                        me.assignListDependendDataForInventoryItems.call(me, eventIdToFireWhenComplete, inventoryItems, serialNumbers);
                    }
                };

                if (true) {
                    done++;

                    var serialNoSuccessCallback = function (serialNos) {
                        erroroccurred = serialNos === undefined;

                        serialNumbers = serialNos;
                        counter++;

                        completeFunction();
                    };

                    var eventId = me.getSerialNumbers(true);
                    eventController.registerOnEventForOneTime(eventId, serialNoSuccessCallback);
                }
                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForInventoryItems of BaseInventoryItemsManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        getSerialNumbers: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
                var baseStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.InventorySerialNo',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildInventoryItemSerialNoStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, baseStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSerialNumbers of BaseInventoryItemsManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MI_INV_SERIAL', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_SERIAL', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSerialNumbers of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        //call this function, when all necessary data has been collected
        assignListDependendDataForInventoryItems: function (eventIdToFireWhenComplete, inventoryItems, serialNumbers) {
            try {
                if(inventoryItems && inventoryItems.getCount() > 0 && serialNumbers && serialNumbers.getCount() > 0) {
                    inventoryItems.each(function (inventoryItem) {
                        var physinventory = inventoryItem.get('physinventory');
                        var fiscalyear = inventoryItem.get('fiscalyear');
                        var itemNo = inventoryItem.get('item');

                        var serialNoStore = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.InventorySerialNo',
                            autoLoad: false
                        });

                        serialNumbers.each(function (serialNo) {
                            var physinv = serialNo.get('physinventory');
                            var fiscalyr = serialNo.get('fiscalyear');
                            var pos = serialNo.get('item');

                            if (physinv === physinventory && fiscalyear === fiscalyr && itemNo === pos) {
                                serialNoStore.add(serialNo);
                            }
                        });
                        inventoryItem.set('seriallist', serialNoStore);
                    });
                }
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventoryItems);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForInventoryItems of BaseInventoryItemsManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        buildDataBaseObjectForInventoryItem: function (inventoryItem) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                var zeroCountValue = inventoryItem.get('zerocount') === true ? 'X' : '';
                var retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['SCENARIO'] = ac.getScenario();
                retval['PHYSINVENTORY'] = inventoryItem.get('physinventory');
                retval['UPDFLAG'] = inventoryItem.get('updFlag');
                retval['PHYSINVENTORY'] = inventoryItem.get('physinventory');
                retval['FISCALYEAR'] = inventoryItem.get('fiscalyear');
                retval['ITEM'] = inventoryItem.get('item');
                retval['MATERIAL'] = inventoryItem.get('material');
                retval['PLANT'] = inventoryItem.get('plant');
                retval['STGE_LOC'] = inventoryItem.get('stgeLoc');
                retval['BATCH'] = inventoryItem.get('batch');
                retval['SPEC_STOCK'] = inventoryItem.get('specStock');
                retval['STOCK_TYPE'] = inventoryItem.get('stockType');
                retval['SALES_ORD'] = inventoryItem.get('salesOrd');
                retval['S_ORD_ITEM'] = inventoryItem.get('sOrdItem');
                retval['SCHED_LINE'] = inventoryItem.get('schedLine');
                retval['VENDOR'] = inventoryItem.get('vendor');
                retval['CUSTOMER'] = inventoryItem.get('customer');
                retval['PRODBINLOC'] = inventoryItem.get('probinloc')
                retval['CHANGE_USER'] = inventoryItem.get('changeUser');
                retval['CHANGE_DATE'] = inventoryItem.get('changeDate');
                retval['COUNT_USER'] = inventoryItem.get('countUser');
                retval['COUNT_DATE'] = inventoryItem.get('countDate');
                retval['PSTNG_NAME'] = inventoryItem.get('pstngName');
                retval['PSTNG_DATE'] = inventoryItem.get('pstngDate');
                retval['PH_INV_REF'] = inventoryItem.get('phInvRef');
                retval['COUNTED'] = inventoryItem.get('counted');
                retval['DIFF_POSTED'] = inventoryItem.get('diffPosted');
                retval['RECOUNT'] = inventoryItem.get('recount');
                retval['DELETE_IND'] = inventoryItem.get('deleteInd');
                retval['ALT_UNIT'] = inventoryItem.get('altUnit');
                retval['BOOK_QTY'] = inventoryItem.get('bookQty');
                retval['ZERO_COUNT'] = zeroCountValue;
                retval['QUANTITY'] = inventoryItem.get('quantity');
                retval['BASE_UOM'] = inventoryItem.get('baseUom');
                retval['BASE_UOM_ISO'] = inventoryItem.get('baseUomIso');
                retval['ENTRY_QNT'] = inventoryItem.get('entryQnt');
                retval['ENTRY_UOM'] = inventoryItem.get('entryUom');
                retval['ENTRY_UOM_ISO'] = inventoryItem.get('entryUomIso');
                retval['MAT_DOC'] = inventoryItem.get('matDoc');
                retval['DOC_YEAR'] = inventoryItem.get('docYear');
                retval['MATDOC_ITM'] = inventoryItem.get('matdocItm');
                retval['RECOUNTDOC'] = inventoryItem.get('recountDoc');
                retval['DIFFERENCE'] = inventoryItem.get('difference');
                retval['CURRENCY'] = inventoryItem.get('currency');
                retval['CURRENCY_ISO'] = inventoryItem.get('currencyIso');
                retval['CC_PH_INV'] = inventoryItem.get('ccPhInv');
                retval['WBS_ELEMENT'] = inventoryItem.get('wbsElement');
                retval['SV_INC_VAT'] = inventoryItem.get('svIncVat');
                retval['SALES_VAL'] = inventoryItem.get('salesVal');
                retval['BOOK_VALUE'] = inventoryItem.get('bookValue');
                retval['VO_MAT'] = inventoryItem.get('voMat');
                retval['SV_W_O_VAT'] = inventoryItem.get('svWOVat');
                retval['DIFFSALCNTVAT'] = inventoryItem.get('diffsalcntvat');
                retval['DIFFSALCNT'] = inventoryItem.get('diffsalcnt');
                retval['VALCOUNTQNT'] = inventoryItem.get('valcountQnt');
                retval['VALBOOKAMT'] = inventoryItem.get('valBookAmt');
                retval['DIFF_VALUE'] = inventoryItem.get('diffValue');
                retval['REASON'] = inventoryItem.get('reason');
                retval['MATERIAL_EXT'] = inventoryItem.get('materialExternal');
                retval['MATERIAL_GUID'] = inventoryItem.get('materialGuid');
                retval['MATERIAL_VERSION'] = inventoryItem.get('materialVersion');
                retval['PH_INV_REF_LONG'] = inventoryItem.get('phInvRefLong');
                retval['MAKTX'] = inventoryItem.get('maktx');
                retval['LGPBE'] = inventoryItem.get('lgpbe');
                retval['SERPR'] = inventoryItem.get('serpr');
                retval['X_SERNR'] = inventoryItem.get('x_sernr');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForInventoryItem of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        buildInventoryItemSerialNoStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var inventoryitem = this.buildInventoryItemSerialNoFromDbResultObject(cursor.value);
                        store.add(inventoryitem);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryItemSerialNoStoreFromDataBaseQuery of BaseInventoryItemsManager', ex);
            }
        },

        buildInventoryItemSerialNoFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.InventorySerialNo', {
                    physinventory: dbResult['PHYSINVENTORY'],
                    fiscalyear: dbResult['FISCALYEAR'],
                    item: dbResult['ITEM'],
                    dirty: dbResult['DIRTY'],
                    mandt: dbResult['MANDT'],
                    serialNo: dbResult['SERIALNO'],
                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['PHYSINVENTORY'] + dbResult['FISCALYEAR'] + dbResult['ITEM'] + dbResult['SERIALNO']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryItemSerialNoFromDbResultObject of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        buildDataBaseObjectForInventoryItemSerialNo: function (serialNo) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                var retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['SCENARIO'] = ac.getScenario();
                retval['ITEM'] = serialNo.get('item');
                retval['PHYSINVENTORY'] = serialNo.get('physinventory');
                retval['FISCALYEAR'] = serialNo.get('fiscalyear');
                retval['SERIALNO'] = serialNo.get('serialNo');
                retval['UPDFLAG'] = serialNo.get('updFlag');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForInventoryItemSerialNo of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        loadDependendDataForInventoryItems: function (inventoryItems, eventIdToFireWhenComplete) {
            try {
                if(!inventoryItems) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, null);
                    return;
                } else if(inventoryItems.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventoryItems);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var errorOccurred = false;
                var counter = 0;
                var done = 0;

                var completeFunction = function () {
                    if (errorOccurred === true) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventoryItems);
                    }
                };


                inventoryItems.each(function(inventoryItem) {
                    counter++;

                    var serialNumbersCallback = function (serialNos) {
                        errorOccurred = serialNos === undefined;

                        inventoryItem.set('seriallist', serialNos);

                        done++;

                        completeFunction();

                    };

                    var eventId = me.getSerialNumbersForInventoryItem(inventoryItem, true);
                    eventController.registerOnEventForOneTime(eventId, serialNumbersCallback);
                });

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForInventoryItems of BaseInventoryItemsManager', ex);
            }
        },

        getSerialNumbersForInventoryItem: function (inventoryItem, useBatchProcessing) {
            var retval = -1;
            try {

                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (!inventoryItem) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var me = this;

                var serialNumbersStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.InventorySerialNo',
                    autoLoad: false
                });

                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildInventoryItemSerialNoStoreFromDataBaseQuery.call(me, serialNumbersStore, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, serialNumbersStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSerialNumbersForInventoryItem of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('PHYSINVENTORY', inventoryItem.get('physinventory'));
                indexMap.add('FISCALYEAR', inventoryItem.get('fiscalyear'));
                indexMap.add('ITEM', inventoryItem.get('item'));

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MI_INV_SERIAL', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_SERIAL', indexRange, successCallback, null, useBatchProcessing);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSerialNumbersForInventoryItem of BaseInventoryItemsManager', ex);
            }
            return retval;
        },

        saveInventoryItem: function (inventoryItem, withDependentData) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!inventoryItem) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }
                var me = this;

                inventoryItem.set('updFlag', 'X');

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(inventoryItem.get('serpr'))) {
                    var serialList = inventoryItem.get('seriallist');
                    if (serialList && serialList.getCount() > 0) {
                        if (serialList.getCount() === parseInt(inventoryItem.get('entryQnt'))) {
                            inventoryItem.set('localStatus', 'completed');
                        } else if (inventoryItem.get('entryQnt') > 0) {
                            inventoryItem.set('localStatus', 'in process');
                        }
                    }
                } else {
                    if (inventoryItem.get('entryQnt') > 0 || inventoryItem.get('zerocount') === true) {
                        inventoryItem.set('localStatus', 'completed');
                    }
                }

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if(success) {
                            if(withDependentData) {
                                var serialList = inventoryItem.get('seriallist');
                                var serialListCount = serialList.getCount();
                                var saveSerialNoCallback = function (success) {
                                    if(success) {
                                        eventController.fireEvent(retval, true);
                                    } else {
                                        eventController.fireEvent(retval, false);
                                    }
                                };
                                var eventId = me.saveSerialNoForInventoryItem(inventoryItem, serialList.getAt(serialListCount));
                                eventController.registerOnEventForOneTime(eventId, saveSerialNoCallback);
                            } else {
                                eventController.fireEvent(retval, true);
                            }
                        } else {
                            eventController.fireEvent(retval, false);
                        }

                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var toSave = this.buildDataBaseObjectForInventoryItem(inventoryItem);
                AssetManagement.customer.core.Core.getDataBaseHelper().update('D_MI_INV_ITEM', null, toSave, callback, callback);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveInventoryItems of BaseInventoryItemsManager', ex);
            }

            return retval;
        },

        saveSerialNoForInventoryItem: function (inventoryItem, serialNo) {
            var retval = -1;
            try {

                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (!serialNo) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }
                var me = this;

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";
                        if(success) {
                            eventController.fireEvent(retval, true);
                        } else {
                            eventController.fireEvent(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSerialNoForInventoryItem of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };


                serialNo.set('item', inventoryItem.get('item'));
                serialNo.set('fiscalyear', inventoryItem.get('fiscalyear'));
                serialNo.set('physinventory', inventoryItem.get('physinventory'));

                var toSave = me.buildDataBaseObjectForInventoryItemSerialNo(serialNo);
                AssetManagement.customer.core.Core.getDataBaseHelper().update('D_MI_INV_SERIAL', null, toSave, callback, callback);

            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSerialNoForInventoryItem of BaseInventoryItemsManager', ex);
            }
            return retval;
        },


        deleteSerialNoOfInventoryItem: function (inventoryItem, serialNo, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!inventoryItem) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                serialNo.set('item', inventoryItem.get('item'));
                serialNo.set('fiscalyear', inventoryItem.get('fiscalyear'));
                serialNo.set('physinventory', inventoryItem.get('physinventory'));

                var toDelete = this.buildDataBaseObjectForInventoryItemSerialNo(serialNo);

                var me = this;
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(toDelete);
                            eventController.fireEvent(me.EVENTS.INV_SERIALNO_DELETE, toDelete);
                        }

                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSerialNoOfItem of BaseInventoryItemsManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                //get the key of the serialNo to delete
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_MI_INV_SERIAL', toDelete);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_MI_INV_SERIAL', keyRange, callback, callback, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSerialNoOfItem of BaseInventoryItemsManager', ex);
            }
            return retval;
        }
    }
});
