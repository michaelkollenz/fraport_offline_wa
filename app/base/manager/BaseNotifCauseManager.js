Ext.define('AssetManagement.base.manager.BaseNotifCauseManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.NotifCauseCache',
        'AssetManagement.customer.model.bo.NotifCause',
        'AssetManagement.customer.manager.CustCodeManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.StoreHelper',
        'Ext.data.Store'
    ],
    
    inheritableStatics: {
		EVENTS: {
			CAUSE_ADDED: 'notifCauseAdded',
			CAUSE_CHANGED: 'notifCauseChanged',
			CAUSE_DELETED: 'notifCauseDeleted'
		},
		
		//protected
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.NotifCauseCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},
		
		//public methods	
		getNotifCauses: function(withDependendData, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForNotifCauses(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifCause',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
								
							me.buildNotifTasksStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
								
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForNotifCauses.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifCauses of BaseNotifCauseManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
						
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTICAUSE', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTICAUSE', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifCauses of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getNotifCausesForNotif: function(qmnum, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();		
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
					this.loadDependendDataForNotifCauses(baseStore, retval, true);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifCause',
						autoLoad: false
					});
				
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifCausesStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotif(qmnum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
								me.loadDependendDataForNotifCauses.call(me, baseStore, retval, true);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifCausesForNotif of BaseNotifCauseManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTICAUSE', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTICAUSE', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifCausesForNotif of BaseNotifCauseManager', ex);
			}
				
			return retval;
		},
		
		getNotifCausesForNotifItem: function(qmnum, fenum, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();		
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.FULL);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotifItem(qmnum, fenum, cache.self.DATAGRADES.MEDIUM);
					this.loadDependendDataForNotifCauses(baseStore, retval);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifCause',
						autoLoad: false
					});
				
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifCausesStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotifItem(qmnum, fenum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotifItem(qmnum, fenum, cache.self.DATAGRADES.MEDIUM);
								me.loadDependendDataForNotifCauses.call(me, baseStore, retval);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifCausesForNotifItem of BaseNotifCauseManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
			
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);
					keyMap.add('FENUM', fenum);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTICAUSE', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTICAUSE', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifCausesForNotifItem of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},
		
		//save a notifCause - if it is neccessary it will get a new counter first
		saveNotifCause: function(notifCause) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifCause) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = notifCause.get('updFlag');
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;
				
				if(isInsert && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifCause.get('fenum')))
					notifCause.set('fenum', '0000');

				//update flag management
				this.manageUpdateFlagForSaving(notifCause);
				
				//check if the notifCause already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifCause.get('urnum'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1)
							notifCause.set('urnum', nextCounterValue);
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifCause.get('urnum'))) {
							eventController.fireEvent(retval, false);
							return;
						}


						if(isInsert) {
							var key = AssetManagement.customer.manager.KeyManager.createKey([ notifCause.get('qmnum'), notifCause.get('fenum'), notifCause.get('urnum') ]);
                            if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifCause.get('childKey'))){
                                notifCause.set('childKey', key);
							} else {
                                notifCause.set('childKey2', key);
							}

						}

						//set the id
						notifCause.set('id', notifCause.get('qmnum') + notifCause.get('fenum') + notifCause.get('urnum'));
	
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
							
								if(success) {
									if(isInsert === true) {
										//do not specify a datagrade, so all depenend data will loaded, when necessary
										me.getCache().addToCache(notifCause);
									}
								
									var eventType = isInsert ? me.EVENTS.CAUSE_ADDED : me.EVENTS.CAUSE_CHANGED;
									eventController.fireEvent(eventType, notifCause);
								}
							
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifCause of BaseNotifCauseManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForNotifCause(notifCause);						

						//if it is an insert perform an insert
						if(isInsert) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_NOTICAUSE', toSave, callback, callback);
						} else {
							//else it is an update
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_NOTICAUSE', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifCause of BaseNotifCauseManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextUrnum(notifCause);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifCause of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},
	
		deleteNotifCause: function(notifCause, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifCause) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(notifCause);
							me.getCache().removeFromCache(notifCause);
							
							eventController.fireEvent(me.EVENTS.CAUSE_DELETED, notifCause);
						}
					
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifCause of BaseNotifCauseManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the notifCause to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_NOTICAUSE', notifCause);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_NOTICAUSE', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifCause of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},

		//private
		buildNotifCausesStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var notifCause = this.buildNotifCauseFromDbResultObject(cursor.value);
						store.add(notifCause);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifCausesStoreFromDataBaseQuery of BaseNotifCauseManager', ex);
			}
		},
		
		buildNotifCauseFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.NotifCause', {
					qmnum: dbResult['QMNUM'],
					fenum: dbResult['FENUM'],
					urnum: dbResult['URNUM'],
					bautl: dbResult['BAUTL'],
					urtxt: dbResult['URTXT'],
					qurnum: dbResult['QURNUM'],
					urcod: dbResult['URCOD'],
					urgrp: dbResult['URGRP'],
					urkat: dbResult['URKAT'],
					urver: dbResult['URVER'],
					stsma: dbResult['STSMA'],
			        kzmla: dbResult['KZMLA'],
					erzeit: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERZEIT']),
			
				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY'],
					childKey: dbResult['CHILDKEY'],
					childKey2: dbResult['CHILDKEY2']
				});
				
				retval.set('id', dbResult['QMNUM'] + dbResult['FENUM'] + dbResult['URNUM']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifCauseFromDbResultObject of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},
		
		loadListDependendDataForNotifCauses: function(eventIdToFireWhenComplete, notifCauses) {
			try {
				//do not continue if there is no data
				if(notifCauses.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifCauses);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				
				var counter = 0;
				
				var done = 0;
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccurred === false) {
						me.assignListDependendDataForNotifTasks.call(me, eventIdToFireWhenComplete, notifCauses);
					}
				};
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForNotifCauses of BaseNotifCauseManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all necessary data has been collected
		assignListDependendDataForNotifCauses: function(eventIdToFireWhenComplete, notifCauses) {
			try {
//				notifItems.each(function(notifItem, index, length) {
//					
//				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(notifCauses, cache.self.DATAGRADES.LOW);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForNotifCauses of BaseNotifCauseManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForNotifCauses: function(notifCauses, eventIdToFireWhenComplete, allOfOneNotif) {
			try {
				//do not continue if there is no data
				if(notifCauses.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifCauses);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var custCodes = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done) {
						me.assignDependendDataForNotifCauses.call(me, eventIdToFireWhenComplete, notifCauses, custCodes, allOfOneNotif);
					}
				};
				
				if(allOfOneNotif) {
					//get custCodes
					done++;
					
					var custCodesSuccessCallback = function(cCodes) {
						errorOccured = cCodes === undefined;
						
						custCodes = cCodes;
						counter++;
						
						completeFunction();
					};
					
					eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
					eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);
				}
				
				if(allOfOneNotif) {
					notifCauses.each(function(cause){
						//getLocalLongtext
						if(true) {
							done++;
							
							var localLongtextSuccessCallback = function(localLT, backendLT) {
								errorOccured = localLT === undefined || backendLT === undefined;
								cause.set('localLongtext', localLT);
								cause.set('backendLongtext', backendLT);
								counter++;
								
								completeFunction();
							};
							eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(cause, true);
							eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
						}
					});
				}
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForNotifCauses of BaseNotifCauseManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForNotifCauses: function(eventIdToFireWhenComplete, notifCauses, custCodes, allOfOneNotif) {
			try {
				notifCauses.each(function(cause, index, length) {
					//assign codes
					if(custCodes) {
						var katalogArt = cause.get('urkat');
						var codeGruppe = cause.get('urgrp');
						var code = cause.get('urcod');
						
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
							var groupHashMapForCatalogue = custCodes.get(katalogArt);
							
							if(groupHashMapForCatalogue) {
								var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
								
								if(codesGroupStore) {
									codesGroupStore.each(function(custCode) {
										if(custCode.get('code') === code) {
											cause.set('causeCustCode', custCode);
											return false;
										}
									}, this);
								}
							}
						}
					}
				});
				
				notifCauses.sort('urnum', 'ASC');
				
				var cache = this.getCache();
				var qmnum = notifCauses.getAt('0').get('qmnum');
				var toReturn = null;
				
				if(allOfOneNotif) {
					cache.addToCacheForNotif(qmnum, notifCauses, cache.self.DATAGRADES.MEDIUM);
					toReturn = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM); 
				} else {
					var fenum = notifCauses.getAt('0').get('fenum');
					cache.addToCacheForNotifItem(qmnum, fenum, notifCauses, cache.self.DATAGRADES.FULL);
					toReturn = cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.FULL); 
				}
				
				//return the store from cache to eliminate duplicate objects
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForNotifCauses of BaseNotifCauseManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//builds the raw object to store in the database out of a notifCause model
		buildDataBaseObjectForNotifCause: function(notifCause) {
			var retval = { };
			
			try {
				retval['QMNUM'] = notifCause.get('qmnum');
				retval['FENUM'] = notifCause.get('fenum');
				retval['URNUM'] = notifCause.get('urnum');
				retval['URTXT'] = notifCause.get('urtxt');
				retval['URKAT'] = notifCause.get('urkat');
				retval['URGRP'] = notifCause.get('urgrp');
				retval['URCOD'] = notifCause.get('urcod');
				retval['URVER'] = notifCause.get('urver');
				retval['QURNUM'] = notifCause.get('urnum');
				retval['BAUTL'] = notifCause.get('bautl');
				
				retval['ERZEIT'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifCause.get('erzeit'));
				
				retval['MOBILEKEY'] = notifCause.get('mobileKey');
				retval['CHILDKEY'] = notifCause.get('childKey');
				retval['CHILDKEY2'] = notifCause.get('childKey2');
				retval['UPDFLAG'] = notifCause.get('updFlag');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForNotifCause of BaseNotifCauseManager', ex);
			}
			
			return retval;
		},
		
		getNextUrnum: function(notifCause) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifCause) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxUrnum = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
                            var urnumValue = cursor.value['URNUM'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(urnumValue, '%')) {
                                //cut of the percent sign
							    urnumValue = urnumValue.substr(1);
                                var curUrnumValue = parseInt(urnumValue);
							
							    if(!isNaN(curUrnumValue))
							    {
								    if(curUrnumValue > maxUrnum)
									    maxUrnum = curUrnumValue;
							    }
						    }

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxUrnum + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
						    eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextUrnum of BaseNotifCauseManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QMNUM', notifCause.get('qmnum'));
				keyMap.add('FENUM', notifCause.get('fenum'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTICAUSE', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTICAUSE', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextUrnum of BaseNotifCauseManager', ex);
			}
			
			return retval;			
		}
	}
});