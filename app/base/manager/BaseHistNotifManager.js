Ext.define('AssetManagement.base.manager.BaseHistNotifManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.HistNotifCache',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.NotifHistory',
        'Ext.data.Store',
        'AssetManagement.customer.helper.MobileIndexHelper'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.HistNotifCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseHistNotifManager', ex);
			}
			
			return retval;
		},
		
		getHistNotifs: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.NotifHistory',
			        autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildHistNotifsStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistNotifs of BaseHistNotifManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_ORDHEAD_HIST', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ORDHEAD_HIST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistNotifs of BaseHistNotifManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		getHistNotifsForFuncLoc: function(tplnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				//check if the cache can deliver func. loc.'s hist notifs
		        var cache = this.getCache();
		        var fromCache = cache.getFromCacheForFuncLoc(tplnr);
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			         model: 'AssetManagement.customer.model.bo.NotifHistory',
			         autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildHistNotifsStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add func. loc.'s hist notifs to cache
							cache.addToCacheForFuncLoc(tplnr, fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getFromCacheForFuncLoc(tplnr);
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistNotifsForFuncLoc of BaseHistNotifManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('TPLNR', tplnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_NOTIHEAD_HIST', 'TPLNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_NOTIHEAD_HIST', 'TPLNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistNotifsForFuncLoc of BaseHistNotifManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		getHistNotifsForEquipment: function(equnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				//check if the cache can deliver equipment's hist notifs
		        var cache = this.getCache();
		        var fromCache = cache.getFromCacheForEquipment(equnr);
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			         model: 'AssetManagement.customer.model.bo.NotifHistory',
			         autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildHistNotifsStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add equipment's hist notifs to cache
							cache.addToCacheForEquipment(equnr, fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getFromCacheForEquipment(equnr);
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistNotifsForFuncLoc of BaseHistNotifManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('EQUNR', equnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_NOTIHEAD_HIST', 'EQUNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_NOTIHEAD_HIST', 'EQUNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistNotifsForFuncLoc of BaseHistNotifManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		//private methods		
		buildHistNotifFromDataBaseQuery: function(eventArgs) {
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						var histNotif = this.buildHistNotifFromDbResultObject(eventArgs.target.result.value);			
						return histNotif;
					}
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistNotifFromDataBaseQuery of BaseHistNotifManager', ex);
			}
			return null;
		},
		
		buildHistNotifsStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var histNotif = this.buildHistNotifFromDbResultObject(cursor.value);
						store.add(histNotif);
	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistNotifsStoreFromDataBaseQuery of BaseHistNotifManager', ex);
			}
		},
		
		buildHistNotifFromDbResultObject: function(dbResult) {
			try {
			    //ordinary fields
				var retval = Ext.create('AssetManagement.customer.model.bo.NotifHistory', {
						
					qmnum: dbResult['QMNUM'],
					qmnam: dbResult['QMNAM'],
					qmart: dbResult['QMART'],
					tplnr: dbResult['TPLNR'],
					equnr: dbResult['EQUNR'],
					bautl: dbResult['BAUTL'],
					qmtxt: dbResult['QMTXT'],
					aufnr: dbResult['AUFNR'],
					artpr: dbResult['ARTPR'],
					priok: dbResult['PRIOK'],
					iwerk: dbResult['IWERK'],
					ingrp: dbResult['INGRP'],
					gewrk: dbResult['GEWRK'],
					swerk: dbResult['SWERK'],
					ktext: dbResult['KTEXT'],
					system_status: dbResult['SYSTEM_STATUS'],
					user_status: dbResult['USER_STATUS'],
					obj_part_p1: dbResult['OBJ_PART_P1'],
					obj_part_grp_p1: dbResult['OBJ_PART_GRP_P1'],
					damage_p1: dbResult['DAMAGE_P1'],
					damage_grp_p1: dbResult['DAMAGE_GRP_P1'],
					item_text_p1: dbResult['ITEM_TEXT_P1'],
					cause_p1: dbResult['CAUSE_P1'],
					cause_grp_p1: dbResult['CAUSE_GRP_P1'],
					cause_text_p1: dbResult['CAUSE_TEXT_P1'],
					fekat: dbResult['FEKAT'],
					urkat: dbResult['URKAT'],
					otkat: dbResult['OTKAT'],
					                
					mzeit: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['MZEIT']),
					qmdat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['QMDAT']),
					strmn: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['STRMN']),
					strur: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['STRUR']),
					ltrmn: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['LTRMN']),
					ltrur: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['LTRUR']),
					ausvn: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['AUSVN']),
					ausbs: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['AUSBS']),
					auztv: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['AUZTV']),
					auztb: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['AUZTB']),
									
					updFlag: dbResult['UPDFLAG']
	
				});
				
				retval.set('id', dbResult['QMNUM']);
				
				return retval;
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistNotifFromDbResultObject of BaseHistNotifManager', ex);
			}
		}
	}
});