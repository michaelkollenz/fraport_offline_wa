Ext.define('AssetManagement.base.manager.BaseObjectListManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
        requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.ObjectListItem',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.manager.QPlosManager'
    ],
	
    inheritableStatics: {
        _notifSaved: false,
        _objectListItemSaved: false,
        _notifSaveError: false,
        _objectListItemSaveError: false,
        _saveRunning: false,

		//public
        getObjectList: function (aufnr, useBatchProcessing) {
            var retval = -1;

            try {
                retval = this.getObjectListInternal(aufnr, true, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectList of BaseObjectListManager', ex);
            }

            return retval;
        },

        //public
        getObjectListWithoutDependentData: function (aufnr, useBatchProcessing) {
            var retval = -1;

            try {
                retval = this.getObjectListInternal(aufnr, false, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListLightVersion of BaseObjectListManager', ex);
            }

            return retval;
        },

        //private
		getObjectListInternal: function(aufnr, loadDependentData, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var ordersObjectList = this.getFromCache(aufnr);
					
				if(ordersObjectList) {
					eventController.requestEventFiring(retval, ordersObjectList);
					return retval;
				}
				
				ordersObjectList = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ObjectListItem',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildObjectListItemStoreFromDataBaseQuery.call(me, ordersObjectList, eventArgs);
						
						if (done) {
						    if (loadDependentData)
						        me.loadDependendDataForObjectList.call(me, retval, ordersObjectList, aufnr);
						    else 
						        AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, ordersObjectList);

						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListForOrder of BaseObjectListManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', aufnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_OBJECTLIST', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_OBJECTLIST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListForOrder of BaseObjectListManager', ex);
			}
	
			return retval;
		},
		
		getObjectListItem: function(aufnr, obknr, obzae, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(obknr)
							|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(obzae) ) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var fromCache = this.getSingleItemFromCache(aufnr, obknr, obzae);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);		
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
					    var objectList = me.buildObjectListItemFromDataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, objectList);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListItem of BaseObjectListManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', aufnr);
				keyMap.add('OBKNR', obknr);
				keyMap.add('OBZAE', obzae);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_OBJECTLIST', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_OBJECTLIST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListItem of BaseObjectListManager', ex);
			}
			
			return retval;
		},

		//private
		buildObjectListItemFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildObjectListItemFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectListItemFromDataBaseQuery of BaseObjectListManager', ex);
			}
			
			return retval;
		},
		
		buildObjectListItemStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var objectList = this.buildObjectListItemFromDbResultObject(cursor.value);
						store.add(objectList);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectListItemStoreFromDataBaseQuery of BaseObjectListManager', ex);
			}
		},
		
		buildObjectListItemFromDbResultObject: function(dbResult) {
	        var retval = null;
	        	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.ObjectListItem', {
					aufnr: dbResult['AUFNR'],
					obknr: dbResult['OBKNR'],
			     	obzae: dbResult['OBZAE'],
				    equnr: dbResult['EQUNR'],
				    eqtxt: dbResult['EQTXT'],
				    ihnum: dbResult['IHNUM'],
				    qmtxt: dbResult['QMTXT'],
			    	tplnr: dbResult['TPLNR'],
				    pltxt: dbResult['PLTXT'],
				    matnr: dbResult['MATNR'],
				    maktx: dbResult['MAKTX'],
				    bautl: dbResult['BAUTL'],
				    bautx: dbResult['BAUTX'],
				    sortf: dbResult['SORTF'],
				    bearb: dbResult['BEARB'],
				    sernr: dbResult['SERNR'],
                    mobilekey: dbResult['MOBILEKEY'],
                    mobilekey_notif: dbResult['MOBILEKEY_NOTIF'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['AUFNR'] + dbResult['OBKNR'] + dbResult['OBZAE']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectListItemFromDbResultObject of BaseObjectListManager', ex);
			}
						
			return retval;
		},

		loadDependendDataForObjectList: function (eventIdToFireWhenComplete, objList, aufnr) {
		    try {
		        if (!objList || objList.getCount == 0)
		        {
		            return false;
		        }

		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        var me = this;
		        var qplosStore = null;
		        var equipments = null;
		        var funcLocs = null;

		        var counter = 0;

		        var done = 0;

		        var errorOccurred = false;
		        var reported = false;

		        var completeFunction = function () {
		            if (errorOccurred === true && reported === false) {
		                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
		                reported = true;
		            } else if (counter === done && errorOccurred === false) {
		                me.assignDependendDataForForObjectList.call(me, eventIdToFireWhenComplete, objList, qplosStore, equipments, funcLocs, aufnr);
		            }
		        };

		        //get qplos
		        if (true) {
		            done++;

		            var qplosSuccessCallback = function (qplosRecieved) {
		                erroroccurred = qplosRecieved === undefined;

		                qplosStore = qplosRecieved;
		                counter++;

		                completeFunction();
		            };

		            var eventId = AssetManagement.customer.manager.QPlosManager.getAllQPlosForOrder(aufnr, true);
		            eventController.registerOnEventForOneTime(eventId, qplosSuccessCallback);
		        }

		        if (true) {
		            done++;

		            var equipmentSuccessCallback = function (equipReceived) {
		                erroroccurred = equipReceived === undefined;

		                equipments = equipReceived;
		                counter++;

		                completeFunction();
		            };
		            var eventId = AssetManagement.customer.manager.EquipmentManager.getEquipments(false, true);
		            eventController.registerOnEventForOneTime(eventId, equipmentSuccessCallback);
		        }

		        if (true) {
		            done++;

		            var funcLocSuccessCallback = function (funcLocReceived) {
		                erroroccurred = funcLocReceived === undefined;

		                funcLocs = funcLocReceived;

		                counter++;

		                completeFunction();
		            };
		            var eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(false, true);
		            eventController.registerOnEventForOneTime(eventId, funcLocSuccessCallback);
		        }

		        if (done > 0) {
		            AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
		        } else {
		            completeFunction();
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForObjectList of BaseObjectListManager', ex);
		        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
		    }
		},

		assignDependendDataForForObjectList: function (eventIdToFireWhenComplete, objList, qplosStore, equipments, funcLocs, aufnr) {
		    try {
		        if (objList && objList.getCount() > 0) {
		            objList.each(function (objListItem) {
		                var equnr = objListItem.get('equnr');
		                var tplnr = objListItem.get('tplnr');

		                if (equipments && equipments.getCount() > 0) {
		                    equipments.each(function (equipment) {
		                        if (equnr == equipment.get('equnr')) {
		                            objListItem.set('equipment', equipment);
		                            return false;
		                        }
		                    });
		                }

		                if (funcLocs && funcLocs.getCount() > 0) {
		                    funcLocs.each(function (funcLoc) {
		                        if (tplnr == funcLoc.get('tplnr')) {
		                            objListItem.set('funcLoc', funcLoc);
		                            return false;
		                        }
		                    });
		                }

		                if (qplosStore && qplosStore.getCount() > 0) {
		                    qplosStore.each(function (qplosObject) {
		                        var techObj = qplosObject.get('techObj');
		                        var techObjClassName = Ext.getClassName(techObj);
		                        if ((techObjClassName === 'AssetManagement.customer.model.bo.Equipment' && techObj.get('equnr') === equnr) ||
                                    (!equnr && techObjClassName === 'AssetManagement.customer.model.bo.FuncLoc' && techObj.get('tplnr') === tplnr)) {
		                            objListItem.set('qplos', qplosObject);
		                            return false;
		                        }
		                    });
		                }
		            });
		        }

		        this.addToCache(aufnr, objList);
		        AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, objList);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForForObjectList of BaseObjectListManager', ex);
		        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
		    }
		},

		//cache administration
		_objectListsCache: null,
	
		getCache: function() {
	        try {
	        	if(!this._objectListsCache) {
					this._objectListsCache = Ext.create('Ext.util.HashMap');
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseObjectListManager', ex);
			}
			
			return this._objectListsCache;
		},
	
		//public methods
		clearCache: function() {
			try {
			    this.getCache().clear();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseObjectListManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseObjectListManager', ex);
			} 
		},
		
		addToCache: function(aufnr, objectListStore) {	
	        try {
			    var temp = this.getCache().add(aufnr, objectListStore);
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseObjectListManager', ex);
		    }
		},
		
		getFromCache: function(aufnr) {
	        var retval = null;
	        
	        try {
			    retval = this.getCache().get(aufnr);
		    } catch(ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromToCache of BaseObjectListManager', ex);
	        }
		    
	        return retval;
		},
		
		getSingleItemFromCache: function(aufnr, obknr, obzae) {
			var retval = null;
		
			try {
				var id = aufnr + obknr + obzae;
				
				var currentCached = this.getFromCache(aufnr);
				
				if(currentCached) {
					currentCached.each(function(entry) {
						if(entry.get('id') === id) {
							retval = entry;
							return false;
						}
					}, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseObjectListManager', ex);
			}
			
			return retval;
		},

        /*
         * saves the changes to the object list to the notification and the object list
         * the object list item will be set to UPDFLAG = 'C' and should not be changed back. The changes will not be posted in SAP.
         */
        saveObjectListChanges: function(notif, objectListItem) {
			var retval = -1;
            try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
                if (!this._saveRunning) {
                    this._saveRunning = true;
                    this._notifSaved = this._objectListItemSaved = this._notifSaveError = this._objectListItemSaveError = false;
                    var me = this;

                    var callback = function () {
                        if(me._notifSaved && me._objectListItemSaved) {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            Ext.defer(function () {
                                me._saveRunning = false;
                            }, 3000, me);
                            
							eventController.requestEventFiring(retval, true);
                        }
                        if(me._notifSaveError || me._objectListItemSaveError) {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            Ext.defer(function () {
                                me._saveRunning = false;
                            }, 3000, me);

							eventController.requestEventFiring(retval, false);
                        }
                    }
                    //save the notification
                    this.saveNotificationChanges(notif, callback);
                    //save the objectListItem
                    this.saveObjectListItemChanges(objectListItem, callback);
                }
            }
            catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectListChanges of BaseObjectListManager', ex);
                this._saveRunning = false;
				AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
			return retval;
        },

        /*
         * saves the changes to the notification to the database
         * calls the provided callback function upon completion
         * sets _notifSaved or _notifSaveError flag depoending on save result
         */ 
        //@private
        saveNotificationChanges: function(notif, callback) {
            try {
                var me = this;
                var eventId = AssetManagement.customer.manager.NotifManager.saveNotification(notif);
                var saveNotifCallback = function(savedNotif) {
                    try {
                        if (savedNotif) {
                            me._notifSaved = true;
                            me._notifSaveError = false;
                            callback.call();
                        }
                        else {
                            me._notifSaved = false;
                            me._notifSaveError = true;
                            callback.call();
                        }
                    }
                    catch (ex) {
                        me._notifSaved = false;
                        me._notifSaveError = true;
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotificationChanges of BaseObjectListManager', ex);
                        callback.call();
                    }
                };

                if (eventId > 0) {
                    AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveNotifCallback);
                }
                else {
                    me._notifSaved = false;
                    me._notifSaveError = true;
                    
                    callback.call();
                }
            }
            catch (ex) {
                this._notifSaveError = true;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotificationChanges of BaseObjectListManager', ex);
            }
        },

        /*
         * saves the changes to the object list item to the database
         * calls the provided callback function upon completion
         * calls saveObjectListItem internally
         * sets _objectListItemSaved or _objectListItemSaveError flag depoending on save result
         */ 
        //@private
        saveObjectListItemChanges: function(objectListItem, callback) {
            try {
                var me = this;
                var saveObjectListItemCallback = function(savedObjectListItem) {
                    try {
                        if (savedObjectListItem) {
                            me._objectListItemSaved = true;
                            me._objectListItemSaveError = false;
                            
                            callback.call();
                        }
                        else {
                            me._objectListItemSaved = false;
                            me._objectListItemSaveError = true;
                            
                            callback.call();
                        }
                    }
                    catch (ex) {
                        me._objectListItemSaved = false;
                        me._objectListItemSaveError = true;
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectListItemChanges of BaseObjectListManager', ex);
                        callback.call();
                    }
                };
					
                var eventId = AssetManagement.customer.manager.ObjectListManager.saveObjectListItem(objectListItem);
                if (eventId > 0) {
                    AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveObjectListItemCallback);
                }
                else {
                    me._objectListItemSaved = false;
                    me._objectListItemSaveError = true;
                    callback.call();
                }
            }
            catch (ex) {
                this._objectListItemSaveError = true;
                callback.call();
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectListItemChanges of BaseObjectListManager', ex);
            }
        },


        /*
         * saves the object list item to the database
         */
        saveObjectListItem: function(objectlistItem, useBatchProcessing) {
			var retval = -1;
            try {
                var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
                
				if(!objectlistItem) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				var requiresNewObzae = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objectlistItem.get('obzae'));

				this.manageUpdateFlagForSaving(objectlistItem);

				var saveObjectListItemCallBack = function() {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, objectlistItem);
				};

                
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewObzae && nextCounterValue !== -1)
						{
							counter = 'MO'+AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 10);
							objectlistItem.set('obzae', counter);
						}						
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objectlistItem.get('obzae'))) {
							eventController.requestEventFiring(retval, false);
							return;
						}
						
						//set mobileKey CURRENTLY NOT IMPLEMENTED
						//if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.set('mobileKey')))
						//	notif.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([ notif.get('qmnum') ]));
						
						var callback = function(eventArgs) {
							try {
				                eventController.registerOnEventForOneTime(eventId, saveObjectListItemCallBack);
								eventController.requestEventFiring(retval, objectlistItem); 
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectListItem of BaseObjectListManager', ex);
								eventController.requestEventFiring(retval, false);
							}
						};
						var toSave = me.buildDataBaseObjectForObjectListItem(objectlistItem);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_OBJECTLIST', toSave, callback, callback);
					} catch(ex) {
						eventController.requestEventFiring(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectListItem of BaseObjectListManager', ex);
					}
				};                			

				if(!requiresNewObzae) {
					saveFunction();
				} else {
					eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_OBJECTLIST', false);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
            } catch (ex) {
				retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveObjectListItem of BaseObjectListManager', ex);
            }

			return retval;
        },
	
		/**
		 * create the databaseobject for saving the object list item to the database
		 */
		buildDataBaseObjectForObjectListItem: function(objectListItem){
			var retval = null;
			
			try {
			    var ac = AssetManagement.customer.core.Core.getAppConfig();
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['UPDFLAG'] =  objectListItem.get('updFlag'); 
				retval['AUFNR'] =  objectListItem.get('aufnr'); 
				retval['OBKNR'] =  objectListItem.get('obknr'); 
				retval['OBZAE'] =  objectListItem.get('obzae'); 
				retval['EQUNR'] =  objectListItem.get('equnr'); 
				retval['EQTXT'] =  objectListItem.get('eqtxt'); 
				retval['IHNUM'] =  objectListItem.get('ihnum'); 
				retval['QMTXT'] =  objectListItem.get('qmtxt'); 
				retval['TPLNR'] =  objectListItem.get('tplnr'); 
				retval['MATNR'] =  objectListItem.get('matnr');
				retval['MAKTX'] =  objectListItem.get('maktx');
				retval['BAUTL'] =  objectListItem.get('bautl');
				retval['BAUTX'] =  objectListItem.get('bautx');
				retval['SORTF'] =  objectListItem.get('sortf');
				retval['BEARB'] =  objectListItem.get('bearb');
				retval['SERNR'] =  objectListItem.get('sernr');
				retval['MOBILEKEY'] = objectListItem.get('mobilekey');
				retval['MOBILEKEY_NOTIF'] = objectListItem.get('mobilekey_notif');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForObjectListItem of BaseObjectListManager', ex);
			}
			
			return retval;
		},


            /*
             * returns the object list item linked to the operation
             */
		getObjectListItemForOperation: function (order, operation) {
		    var retVal = null;
		    try {
		        if (order !== null && operation !== null) {
		            var objectList = order.get('objectList');
		            if (objectList !== null) {
		                for (var i = 0; i < objectList.data.items.length; i++) {
		                    if (objectList.data.items[i].get('obknr') === operation.get('obknr') && objectList.data.items[i].get('obzae') === operation.get('obzae')) {
		                        return objectList.data.items[i];
		                    }
		                }
		            }
		        }
		    }
		    catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListItemForOperation of BaseObjectListManager', ex);
		    }
		    return retVal;
		}

    }
});