Ext.define('AssetManagement.base.manager.BaseCustIlartOrTypeManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.CustIlartOrTypeCache',
        'AssetManagement.customer.model.bo.CustIlartOrType',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.CustIlartOrTypeCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustIlartOrTypeManager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getCustIlartOrTypes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
					//it can, so return just this store
					eventController.requestEventFiring(retval, fromCache);
					return retval;
		        }
				
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.CustIlartOrType',
			        autoLoad: false
		        });
		         
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCustIlartOrTypesStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {											
							//add the whole store to the cache
					        cache.addStoreForAllToCache(fromDataBase);
					
					        //return a store from cache to eliminate duplicate objects
					        var toReturn = cache.getStoreForAll();
					        eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustIlartOrTypes of BaseCustIlartOrTypeManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_ILART_ORTYPE', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_ILART_ORTYPE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustIlartOrTypes of BaseCustIlartOrTypeManager', ex);
			}
	
			return retval;
		},
		
		getCustIlartOrType: function(auart, ilart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(auart)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ilart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
												
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(auart + ilart);
		
		        if(fromCache) {
		        	eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var custIlart = me.buildCustIlartOrTypeFromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(custIlart);
						eventController.requestEventFiring(retval, custIlart);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustIlartOrType of BaseCustIlartOrTypeManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUART', auart);
				keyMap.add('ILART', ilart);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_ILART_ORTYPE', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_ILART_ORTYPE', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustIlartOrType of BaseCustIlartOrTypeManager', ex);
			}
			
			return retval;
		},
		
		//private methods
		buildCustIlartOrTypeFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCustIlartOrTypeFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustIlartOrTypeFromDataBaseQuery of BaseCustIlartOrTypeManager', ex);
			}
			
			return retval;
		},
		
		buildCustIlartOrTypesStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var ilart = this.buildCustIlartOrTypeFromDbResultObject(cursor.value);
						store.add(ilart);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustIlartOrTypesStoreFromDataBaseQuery of BaseCustIlartOrTypeManager', ex);
			}
		},

		buildCustIlartOrTypeFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.CustIlartOrType', {
					auart: dbResult['AUART'],
					ilart: dbResult['ILART'],
					ilatx: dbResult['ILATX'],
					
				    updFlag: dbResult['UPDFLAG']
				});		
				retval.set('id', dbResult['AUART'] + dbResult['ILART']);
						
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustIlartOrTypeFromDbResultObject of BaseCustIlartOrTypeManager', ex);
			}
			return retval;
		}				
	}
});