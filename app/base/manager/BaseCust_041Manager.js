Ext.define('AssetManagement.base.manager.BaseCust_041Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Cust_041',
        'AssetManagement.customer.manager.Cust_040Manager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
        inheritableStatics: {
        /*
         * returns a list of cust_041 objects for the current user and scenario
         */
		//@public		
		getCust_041: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Cust_041',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_041StoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {                            
                            me.loadCust_040Data(retval, fromDataBase);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_041 of BaseCust_041Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_041', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_041', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_041 of BaseCust_041Manager', ex);
			}
			
			return retval;
		}, 

        loadCust_040Data: function(eventid, fromDataBase) {
            try {   
                var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var cust_040Callback = function(cust_040) {
					erroroccurred = cust_040 === undefined;
                    me.mapCust_040ToCust041(cust_040, fromDataBase);
                    //Loading finished, fire completed event
				    eventController.requestEventFiring(eventid, fromDataBase);
					//eventController.requestEventFiring(fromDataBase, eventid);			    
                    
				};                                                       
                var eventId = AssetManagement.customer.manager.Cust_040Manager.getCust_040(true);

				eventController.registerOnEventForOneTime(eventId, cust_040Callback);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadCust_040Data of BaseCust_041Manager', ex);
            }    
        },

        mapCust_040ToCust041: function(cust_040, cust_041) {
            try {
				cust_041.each(function(cust_041Entry, index, length) {
                    var cust_040Entry = cust_040.findRecord('actype', cust_041Entry.get('actype'));
                    if(cust_040Entry !== null && cust_040Entry !== 'undefined')
                        cust_041Entry.set('description', cust_040Entry.get('description'));
				});
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapCust_040ToCust041 of BaseCust_041Manager', ex);
            }
        },
		
		//private
		buildCust_041DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {							
			     if(eventArgs) {
				    if(eventArgs.target.result) {
					   retval = this.buildCust_041FromDbResultObject(eventArgs.target.result.value);					
				    }
			     }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_041DataBaseQuery of BaseCust_041Manager', ex);
			}
			return null;
		},
		
		buildCust_041StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_041 = this.buildCust_041FromDbResultObject(cursor.value);
						store.add(cust_041);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_041StoreFromDataBaseQuery of BaseCust_041Manager', ex);
			}
		},
		
		buildCust_041FromDbResultObject: function(dbResult) {
            var retval = null;
	
            try {
            	retval = Ext.create('AssetManagement.customer.model.bo.Cust_041', {
    				updFlag: dbResult['UPDFLAG'],
            	    scenario: dbResult['SCENARIO'],
    				actype: dbResult['ACTYPE'],
    				target: dbResult['TARGET'],
    				lstar: dbResult['LSTAR'],
    				kstar: dbResult['KSTAR'],
    				stagr: dbResult['STAGR'],
    				regtyp: dbResult['REGTYP'],
    				durationcalc: dbResult['DURATIONCALC'],
					timeinc: dbResult['TIMEINC'],
					leinh: dbResult['LEINH']
    			});
    			
    			retval.set('id', dbResult['SCENARIO'] + dbResult['ACTYPE'] + dbResult['TARGET'] );
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_041StoreFromDataBaseQuery of BaseCust_041Manager', ex);
		    }
			return retval;
		}
	}
});