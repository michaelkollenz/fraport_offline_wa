﻿Ext.define('AssetManagement.base.manager.BaseQPlanCharManager', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.QPlanChar',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.manager.CustAuswmManager',
        'AssetManagement.customer.manager.CustCodeManager',
        'AssetManagement.customer.manager.Cust_013Manager'
    ],
    
    inheritableStatics: {
        //public
        //returns chars for one specific qplan
        getQPlanChars: function (plnty, plnnr, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
                
                var charStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlanChar',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildQPlanCharStoreFromDataBaseQuery.call(me, charStore, eventArgs);

                        if (done) {
                            me.loadDependendDataForQPlanChars.call(me, retval, charStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosChars of BaseQPlanCharManager', ex);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PLNTY', plnty);
                keyMap.add('PLNNR', plnnr);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLANCHAR', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLANCHAR', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosChars of BaseQPlosCharManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private methods
        buildQPlanCharStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        var qplanChar = this.buildQPlanCharFromDbResultObject(cursor.value);
                        store.add(qplanChar);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanCharFromDbResultObject of BaseQPlanCharManager', ex);
            }
        },

        buildQPlanCharFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.QPlanChar', {
                    plnty: dbResult['PLNTY'],
                    plnnr: dbResult['PLNNR'],
                    plnkn: dbResult['PLNKN'],
                    kzeinstell: dbResult['KZEINSTELL'],
                    merknr: dbResult['MERKNR'],
                    zaehl: dbResult['ZAEHL'],
                    gueltigab: dbResult['GUELTIGAB'],
                    kurztext: dbResult['KURZTEXT'],
                    katab1: dbResult['KATAB1'],
                    katalgart1: dbResult['KATALGART1'],
                    auswmenge1: dbResult['AUSWMENGE1'],
                    auswmgwrk1: dbResult['AUSWMGWRK1'],
                    katab2: dbResult['KATAB2'],
                    katalgart2: dbResult['KATALGART2'],
                    auswmenge2: dbResult['AUSWMENGE2'],
                    auswmgwrk2: dbResult['AUSWMGWRK2'],
                    sollwert: dbResult['SOLLWERT'],
                    toleranzob: AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(dbResult['TOLERANZOB'], true),
                    toleranzun: AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(dbResult['TOLERANZUN'], true),
                    masseinhsw: dbResult['MASSEINHSW'],
                    char_type: dbResult['CHAR_TYPE'],
                    stellen: dbResult['STELLEN'],
                    formel1: dbResult['FORMEL1'],
                    dummy10: dbResult['DUMMY10'],
                    pzlfh: dbResult['PZLFH'],
                    dokukz: dbResult['DOKUKZ'],
                    verwerkm: dbResult ['VERMWERKM'],
                    dummy20: dbResult['DUMMY20'],
                    dummy04: dbResult['DUMMY40']
                });

                retval.set('id', dbResult['PLNTY'] + dbResult['PLNNR'] + dbResult['PLNKN'] + dbResult['KZEINSTELL'] + dbResult['MERKNR'] + dbResult['ZAEHL']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanCharFromDbResultObject of BaseQPlanCharManager', ex);
            }

            return retval;
        },

        loadDependendDataForQPlanChars: function (retval, charStore) {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //do not continue if there is no data
                if (charStore.getCount() === 0)
                {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                    return false;
                }

                var counter = 0;

                var done = 0;

                var custCodes = null;
                var custAuswms = null;
                var cust013Store = null;

                var counter = 0;

                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignListDependendDataForQPlanChar.call(me, retval, charStore, custCodes, custAuswms, cust013Store);
                    }
                };

                //get custCodes
                done++;

                var custCodesSuccessCallback = function (cCodes) {
                    errorOccured = cCodes === undefined;

                    custCodes = cCodes;
                    counter++;

                    completeFunction();
                };

                eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
                eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);

                //get custAuswm
                done++;

                var custAuswmSuccessCallback = function (custAuswmReceived) {
                    errorOccurred = custAuswmReceived === undefined;

                    custAuswms = custAuswmReceived;
                    counter++;

                    completeFunction();
                };

                var eventId = AssetManagement.customer.manager.CustAuswmManager.getCustAuswms(true);
                eventController.registerOnEventForOneTime(eventId, custAuswmSuccessCallback);

                done++;

                //get cust013 table
                var cust013SuccessCallback = function (ccust013) {
                    errorOccured = ccust013 === undefined;

                    cust013Store = ccust013;
                    counter++;

                    completeFunction();
                };

                eventId = AssetManagement.customer.manager.Cust_013Manager.getCust_013s(true);
                eventController.registerOnEventForOneTime(eventId, cust013SuccessCallback);

                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForQPlanChars of BaseQPlanCharManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
            }
        },

        assignListDependendDataForQPlanChar: function (retval, charStore, custCodes, custAuswms, cust013Store) {
            try {
                if (charStore && charStore.getCount() > 0) {
                    charStore.each(function (qplanChar) {
                        //codes
                        if (qplanChar.get('char_type') !== '01') { //char_type != 1 means a qualitative char, so the codes will be loaded (without mean value and boundries) 
                            var katalgart1 =  qplanChar.get('katalgart1');
                            var auswmgwrk1 = qplanChar.get('auswmgwrk1')
                            var auswmenge1 = qplanChar.get('auswmenge1')
                            if (auswmgwrk1 && auswmenge1 && katalgart1) {

                                var custCodesPerqQplanChar = Ext.create('Ext.data.Store', {
                                    model: 'AssetManagement.customer.model.bo.CustCode',
                                    autoLoad: false
                                });

                                var codesMeaningsMappingStore = Ext.create('Ext.data.Store', {
                                    model: 'AssetManagement.customer.model.bo.Cust_013',
                                    autoLoad: false
                                });

                                var codesForKatalogMap = custCodes.get(katalgart1); //out of all the codes, get only the ones from required catalog

                                if (codesForKatalogMap) {
                                    if (custAuswms && custAuswms.getCount() > 0) {
                                        custAuswms.each(function (custAuswm) { //first, looking for a match of of the char parameters in custAuswm
                                            if (custAuswm.get('auswahlmge') === auswmenge1 &&
                                                    custAuswm.get('werks') === auswmgwrk1 &&
                                                        custAuswm.get('katalogart') === katalgart1) {

                                                var codesInGroup = codesForKatalogMap.get(custAuswm.get('codegruppe')); //if a match was found, get the codegroup from the match in custAuswm, 
                                                //and based on that codegroup, get the codes for that group
                                                if (codesInGroup && codesInGroup.getCount() > 0) {
                                                    codesInGroup.each(function (codeEntry) {                    //out of all the codes in the group, get only those codes from the match in custAuswm
                                                        if (codeEntry.get('code') === custAuswm.get('code')) {
                                                            custCodesPerqQplanChar.add(codeEntry);
                                                            //now that the code entry has been found, a check if the codes exist in customizing table should be made
                                                            if (cust013Store && cust013Store.getCount() > 0) {
                                                                cust013Store.each(function (cust013entry) {
                                                                    if (cust013entry.get('auswahlmge') === qplanChar.get('auswmenge1') &&
                                                                        cust013entry.get('werks') === qplanChar.get('auswmgwrk1') &&
                                                                        cust013entry.get('katalogart') === qplanChar.get('katalgart1') &&
                                                                        cust013entry.get('code') === codeEntry.get('code') &&
                                                                        cust013entry.get('codegruppe') === codeEntry.get('codegruppe')) {

                                                                        codesMeaningsMappingStore.add(cust013entry);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }

                                qplanChar.set('codes', custCodesPerqQplanChar);
                                qplanChar.set('codeMeanings', codesMeaningsMappingStore);
                            }                                                                                        
                        }

                        this.determineCategory(qplanChar);
                    }, this);
                }

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, charStore);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForQPlanChar of BaseQPlanCharManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
            }
        },
        
        determineCategory: function (qplanChar) {
            try {
                var qplanCharModelInstance = AssetManagement.customer.model.bo.QPlanChar;

                if (qplanChar.get('char_type') !== '01') { //char_type != 1 means a qualitative char
                    var codeMeanings = qplanChar.get('codeMeanings');
                    var requiredCodesMappingMatched = 0;

                    if (codeMeanings && codeMeanings.getCount() > 0) {
                        codeMeanings.each(function (codeMeaningEntry) {
                            if (codeMeaningEntry.get('funktion') === 'OK' || codeMeaningEntry.get('funktion') === 'NOK')
                                requiredCodesMappingMatched++;                                                   
                        });
                    }

                    if (requiredCodesMappingMatched === 2) {                                         
                        qplanChar.set('category', qplanCharModelInstance.CATEGORIES.QUALITATIVE_CHECKBOX);
                    } else {                                                                         
                        qplanChar.set('category', qplanCharModelInstance.CATEGORIES.QUALITATIVE_STANDARD);
                    }                                                                                
                } else {
                    qplanChar.set('category', qplanCharModelInstance.CATEGORIES.QUANTITATIVE);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineCategory of BaseQPlanCharManager', ex);
            }
        }
    }
});
