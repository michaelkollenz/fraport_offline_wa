﻿Ext.define('AssetManagement.base.manager.BaseQPlosOperManager', {

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.QPlosOper',
        'AssetManagement.customer.model.bo.QPlosChar',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.manager.QPlosCharManager',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.helper.CursorHelper'
    ],

    inheritableStatics: {

        //public methods
        getOpersForQPlos: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
                
                var prueflos = prueflos;

                var operStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlosOper',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildQPlosOperStoreFromDataBaseQuery.call(me, operStore, eventArgs);

                        if (done) {
                            me.loadDependendDataForQPlosOpers.call(me, retval, operStore, prueflos);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOpersForQPlos of BaseQPlosOperManager', ex);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PRUEFLOS', prueflos);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS_OPER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLOS_OPER', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOpersForQPlos of BaseQPlosOperManager', ex);
                retval = -1;
            }

            return retval;
        },

        createQPlosOpers: function (qplan, qplos) {
            var retval = null;

            try {
                var prueflos = qplos.get('prueflos');
                var qplanOpers = qplan.get('qplanOpers');
                var retval = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlosOper'
                });

                if (qplanOpers && qplanOpers.getCount() > 0) {
                    qplanOpers.each(function (qplanOper) {
                        var vornr = qplanOper.get('vornr');
                        var childKey = AssetManagement.customer.manager.KeyManager.createKey([prueflos, vornr]);

                        var qplosOper = Ext.create('AssetManagement.customer.model.bo.QPlosOper', {
                            aufpl: '',
                            aplzl: '',
                            prueflos: qplos.get('prueflos'),
                            vornr: vornr,
                            ltxa1: qplanOper.get('ltxa1'),
                            mobileKey: qplos.get('mobileKey'),
                            childKey: childKey
                        });

                        var qplosChars = AssetManagement.customer.manager.QPlosCharManager.createQPlosChars(qplanOper, qplosOper);

                        if (!qplosChars) {
                            retval = null;
                            return false;
                        }

                        qplosOper.set('qplosChars', qplosChars);
                        retval.add(qplosOper);
                    });
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createQPlosOpers of BaseQPlosOperManager', ex);
                retrval = null;
            }

            return retval
        },

        saveQPlosOperStore: function (qplosOpers, completed) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qplosOpers || qplosOpers.getCount() === 0) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }

                var pendingSaves = 0;
                var errorOccurred = false;
                var errorReported = false;

                var successCallback = function (success) {
                    try {
                        if (errorOccured && !errorReported) {
                            eventController.requestEventFiring(retval, false);
                            errorReported = true;
                        } else if (!errorOccured) {

                            pendingSaves--;

                            if (pendingSaves === 0) {
                                eventController.requestEventFiring(retval, true);
                            }
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosOperStore of BaseQPlosOperManager', ex);
                    }
                };

                //begin saving the first item
                qplosOpers.each(function (qplosOper) {
                    var eventId = this.saveQPlosOper(qplosOper, completed);

                    if (eventId > -1) {
                        pendingSaves++;
                        eventController.registerOnEventForOneTime(eventId, successCallback);
                    } else {
                        errorOccurred = true;
                        return false;
                    }
                }, this);

                if (errorOccurred) {
                    eventController.requestEventFiring(retval, false);
                    errorReported = true;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosOperStore of BaseQPlosOperManager', ex);
            }

            return retval;
        },

        deleteQPlosOpers: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(prueflos)) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            var qplosCharsDeleteCallback = function (success) {
                                eventController.requestEventFiring(retval, success);
                            };

                            var deleteCharsEventId = AssetManagement.customer.manager.QPlosCharManager.deleteQPlosChars(prueflos);
                            eventController.registerOnEventForOneTime(deleteCharsEventId, qplosCharsDeleteCallback);
                        } else {
                            eventController.requestEventFiring(retval, success);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlosOpers of BaseQPlosOperManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PRUEFLOS', prueflos);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS_OPER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('C_QPLOS_OPER', keyRange, callback, callback, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlosOpers of BaseQPlosOperManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private methods
        buildQPlosOperStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        var prueflosOper = this.buildQPlosOperFromDbResultObject(cursor.value);
                        store.add(prueflosOper);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlosOperStoreFromDataBaseQuery of BaseQPlosOperManager', ex);
            }
        },

        buildQPlosOperFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.QPlosOper', {
                    prueflos: dbResult['PRUEFLOS'],
                    aufpl: dbResult['AUFPL'],
                    aplzl: dbResult['APLZL'],
                    vornr: dbResult['VORNR'],
                    ltxa1: dbResult['LTXA1'],
                    mobileKey: dbResult['MOBILEKEY'],
                    childKey: dbResult['CHILDKEY'],
                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['PRUEFLOS'] + dbResult['VORNR']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlosOperFromDbResultObject of BaseQPlosOperManager', ex);
                retval = null;
            }

            return retval;
        },

        loadDependendDataForQPlosOpers: function (retval, qplosOperStore, prueflos)
        {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //do not continue if there is no data
                if (qplosOperStore.getCount() === 0)
                {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, qplosOperStore);
                    return;
                }

                var qplosCharStore = null;
                var counter = 0;

                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignDependendDataForQPlosOpers.call(me, retval, qplosOperStore, qplosCharStore);
                    }
                };

                //get QPlosChar
                if (true) {
                    done++;

                    var qplosCharSuccessCallback = function (qplosCharReceived) {
                        erroroccurred = qplosCharReceived === undefined;

                        qplosCharStore = qplosCharReceived

                        counter++;

                        completeFunction();
                    };

                    var eventId = AssetManagement.customer.manager.QPlosCharManager.getQPlosChars(prueflos, true);
                    eventController.registerOnEventForOneTime(eventId, qplosCharSuccessCallback);
                }

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForQPlosOpers of BaseQPlosManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
            }
        },

        assignDependendDataForQPlosOpers: function (retval, qplosOperStore, qplosCharStore, qpResultStore) {
            try {
                if (qplosOperStore && qplosOperStore.getCount() > 0) {
                    qplosOperStore.each(function (qplosOper) {

                        var vornr = qplosOper.get('vornr');

                        qplosCharsOfOper = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.QPlosChar',
                            autoLoad: false
                        });
                        qplosOper.set('qplosChars', qplosCharsOfOper);

                        //filling qplosCharPerOper with additional data
                        if (qplosCharStore && qplosCharStore.getCount() > 0) {
                            qplosCharStore.each(function (qplosChar) {

                                if (qplosChar.get('vornr') === vornr) {
                                    qplosCharsOfOper.add(qplosChar);
                                }
                            });
                        }
                    });
                }

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, qplosOperStore);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForQPlosOpers of BaseQPlosOperManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
            }
        },

        saveQPlosOper: function (qplosOper, completed) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qplosOper) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }
                
                qplosOper.set('updFlag', completed ? 'I' : 'A');

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (!success) {
                            eventController.requestEventFiring(retval, false);
                        } else {
                            var qplosChars = qplosOper.get('qplosChars');

                            var qplosCharsSuccessCallback = function (success) {
                                eventController.requestEventFiring(retval, success);
                            };

                            var charsEventId = AssetManagement.customer.manager.QPlosCharManager.saveQPlosCharStore(qplosChars, completed);
                            eventController.registerOnEventForOneTime(charsEventId, qplosCharsSuccessCallback);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosOper of BaseQPlosOperManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var toSave = this.buildDataBaseObjectFromQPlosOper(qplosOper);
                AssetManagement.customer.core.Core.getDataBaseHelper().put('C_QPLOS_OPER', toSave, callback, callback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosOper of BaseQPlosOperManager', ex);
                retval = -1;
            }

            return retval;
        },

        buildDataBaseObjectFromQPlosOper: function (qplosOper) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();
                retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['UPDFLAG'] = qplosOper.get('updFlag');
                retval['AUFPL'] = qplosOper.get('aufpl');
                retval['APLZL'] = qplosOper.get('aplzl');
                retval['VORNR'] = qplosOper.get('vornr');
                retval['LTXA1'] = qplosOper.get('ltxa1');
                retval['PRUEFLOS'] = qplosOper.get('prueflos');
                retval['MOBILEKEY'] = qplosOper.get('mobileKey');
                retval['CHILDKEY'] = qplosOper.get('childKey');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectFromQPlosOper of BaseQPlosOperManager', ex);
                retval = null;
            }

            return retval;
        }
    }
});
