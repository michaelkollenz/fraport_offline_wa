Ext.define('AssetManagement.base.manager.BaseCust_001Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Cust_001',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public
		getCust_001s: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Cust_001',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_001StoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_001s of BaseCust_001Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_001', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_001', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_001s of BaseCust_001Manager', ex);
			}
	
			return retval;
		},
		
		getCust_001: function(mobileuser, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobileuser)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_001 = me.buildCust_001DataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, cust_001);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_001 of BaseCust_001Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('MOBILEUSER', mobileuser);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_001', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_001', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_001 of BaseCust_001Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_001DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCust_001FromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_001DataBaseQuery of BaseCust_001Manager', ex);
			}
			return retval;
		},
		
		buildCust_001StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_001 = this.buildCust_001FromDbResultObject(cursor.value);
						store.add(cust_001);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_001StoreFromDataBaseQuery of BaseCust_001Manager', ex);
			}
		},
		
		buildCust_001FromDbResultObject: function(dbResult) {
	        var retval = null;
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Cust_001', {
					mobileuser: dbResult['MOBILEUSER'],
					order_scenario: dbResult['ORDER_SCENARIO'],
					order_workcenter: dbResult['ORDER_WORKCENTER'],
					order_plant_wrkc: dbResult['ORDER_PLANT_WRKC'],
					order_pernr: dbResult['ORDER_PERNR'],
					order_plangroup: dbResult['ORDER_PLANGROUP'],
					order_plan_plant: dbResult['ORDER_PLAN_PLANT'],
					order_par_role: dbResult['ORDER_PAR_ROLE'],
					order_partner_no: dbResult['ORDER_PARTNER_NO'],
					notif_scenario: dbResult['NOTIF_SCENARIO'],
					notif_workcenter: dbResult['NOTIF_WORKCENTER'],
					notif_plant_wrkc: dbResult['NOTIF_PLANT_WRKC'],
					notif_plangroup: dbResult['NOTIF_PLANGROUP'],
					notif_plan_plant: dbResult['NOTIF_PLAN_PLANT'],
					notif_pernr: dbResult['NOTIF_PERNR'],
					notif_par_role: dbResult['NOTIF_PAR_ROLE'],
					notif_partner_no: dbResult['NOTIF_PARTNER_NO'],
					vari_tpl: dbResult['VARI_TPL'],
					change_tpl: dbResult['CHANGE_TPL'],
					vari_equi: dbResult['VARI_EQUI'],
					change_equi: dbResult['CHANGE_EQUI'],
					vari_class: dbResult['VARI_CLASS'],
					sprache: dbResult['SPRACHE'],
					lartDefault: dbResult['LART_DEFAULT'],
					intern: dbResult['INTERN'],
					ekorg: dbResult['EKORG'],
					ekgrp: dbResult['EKGRP'],
					keyfigure_kokrs: dbResult['KEYFIGURE_KOKRS'],
					keyfigure_grp: dbResult['KEYFIGURE_GRP'],
					vkorg: dbResult['VKORG'],
					vtweg: dbResult['VTWEG'],
					spart: dbResult['SPART'],
					kschl: dbResult['KSCHL'],
					kunnr_consi: dbResult['KUNNR_CONSI'],
					vkgrp: dbResult['VKGRP'],
					vkbur: dbResult['VKBUR'],
					kschl: dbResult['KSCHL'],
					vari_vendor: dbResult['VARI_VENDOR'],
					kunnr_consi: dbResult['KUNNR_CONSI'],
					kunnr_ag: dbResult['KUNNR_AG'],
					kunnr_we: dbResult['KUNNR_WE'],
					lifnr: dbResult['LIFNR'],
                    vari_mat: dbResult['VARI_MAT'],
                    tzone: dbResult['TZONE'],
                    waers: dbResult['WAERS'],
                    quot_allowed: dbResult['QUOT_ALLOWED']
				});
				
				retval.set('id', dbResult['MOBILEUSER']);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_001FromDbResultObject of BaseCust_001Manager', ex);
			}
			return retval;
		}
	}
});