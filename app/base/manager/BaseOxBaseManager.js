﻿Ext.define('AssetManagement.base.manager.BaseOxBaseManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap'
    ],

    inheritableStatics: {
        //protected
        getClassName: function () {
            var retval = 'BaseOxBaseManager';

            try {
                var className = Ext.getClassName(this);

                if (AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
                    var splitted = className.split('.');
                    retval = splitted[splitted.length - 1];
                } else {
                    retval = className;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, retval, ex);
            }

            return retval;
        },

        //protected
        //returns the primary managed database store of the manager
        //must be implemented when manual completion is used
        getDataBaseStore: function () {
            return '';
        },

        //begin region for manual completion management

        //protected
        //returns, if the managed business object requires manual completion
        //by default this is always false
        //override to switch on A/C/R logic
        requiresManualCompletion: function() {
            return false;
        },

        //public
        //returns, if the managed business object is completed
        //any completion scenario implemention must define a criteria to detect a completion
        //any object not using manual completion are considered as completed
        isCompleted: function (oxBusinessObject) {
            var retval = false;

            try {
                retval = !this.requiresManualCompletion();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }

            return retval;
        },

        //public
        //tries to mark a passed business object as completed
        //returns an event ID to register on
        //the triggered event returns a boolean, if the procedure was successful
        tryMarkAsCompleted: function (oxBusinessObject) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!this.isCompleted(oxBusinessObject)) {
                    this.beginCompletionSequence(oxBusinessObject, retval);
                } else {
                    //this object does not need a completion procedure (anymore)
                    //just trigger a dummy event with true
                    eventController.requestEventFiring(retval, true);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //start of the completion sequence
        beginCompletionSequence: function (oxBusinessObject, eventIdForCompletion) {
            try {
                var calleeName = arguments.callee.name;
                var me = this;

                var wrapperCallback = function (doContinue) {
                    try {
                        if (doContinue !== false) {
                            this.markAsCompleted(oxBusinessObject, eventIdForCompletion);
                        } else {
                            AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException(calleeName, me.getClassName(), ex);
                    }
                };

                this.beforeMarkAsCompleted(oxBusinessObject, wrapperCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }
        },

        //protected
        //injection method to override for actions to perform before the completion is executed
        //the callback MUST always be executed - else the process will get stuck!
        //returning false will prevent the completion and give a negative completion result
        beforeMarkAsCompleted: function (oxBusinessObject, callback) {
            callback.call(this);
        },

        //private
        //sets the passed business object completed and triggers the completion processing
        //continues with performCompletion
        markAsCompleted: function (oxBusinessObject, eventIdForCompletion) {
            try {
                this.changeUpdateFlagForCompletion(oxBusinessObject);

                //set a mark on the object, that it currenty pushed through a completion sequence
                //this is necessary to keep the update flag being not changed during re-save requests in implementations of performCompletion
                oxBusinessObject['COMPL_SEQ_RUNNING'] = true;

                var calleeName = arguments.callee.name;
                var me = this;

                var performCompletionCallback = function (success) {
                    try {
                        if (success === true) {
                            var wrapperCallback = function () {
                                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdForCompletion, true);
                            };

                            me.afterMarkAsCompleted(oxBusinessObject, wrapperCallback);
                        } else {
                            AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdForCompletion, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException(calleeName, me.getClassName(), ex);
                    }
                };

                this.performCompletion(oxBusinessObject, performCompletionCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }
        },

        //protected
        //saves the business object with completed data
        //this routine must be implemented by any manager class for manual completion
        //if there are dependent objects to be marked as completed as well this needs to be triggered within this method too
        //the callback must be called with an success indicator, if the the completion was successful
        performCompletion: function (oxBusinessObject, callback) {
            try {
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }
        },

        //protected
        //injection method to override for actions to perform after the completion is executed
        //anything done here is executed before the completion process returns
        //the callback MUST always be executed - else the process will get stuck!
        afterMarkAsCompleted: function (oxBusinessObject, callback) {
            callback.call(this);
        },

        //protected
        //manages the update flag for saving
        //returns true, if it was successful - false on errors
        manageUpdateFlagForSaving: function (oxBusinessObject) {
            var retval = false;

            try {
                //do not touch the update flag value, if the object is currently being pushed through a completion sequence
                //only this way an object with update flag value 'X' will not change on completion
                if (oxBusinessObject['COMPL_SEQ_RUNNING'] !== true) {
                    var formerUpdateFlag = oxBusinessObject.get('updFlag');
                    var newUpdateFlag = '';

                    switch (formerUpdateFlag) {
                        case '':
                            newUpdateFlag = this.requiresManualCompletion() ? 'A' : 'I';
                            break;

                        case 'X':
                            newUpdateFlag = this.requiresManualCompletion() ? 'C' : 'U';
                            break;

                        default:
							newUpdateFlag = formerUpdateFlag;
                            break;
                    }

                    oxBusinessObject.set('updFlag', newUpdateFlag);
                }

                retval = true;                
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, retval, ex);
            }

            return retval;
        },

        //protected
        //sets an object's update flag to the corresponding completed one
        //returns true, if it was successful - false on errors
        changeUpdateFlagForCompletion: function (oxBusinessObject) {
            var retval = false;

            try {
                var formerUpdateFlag = oxBusinessObject.get('updFlag');
                var newUpdateFlag = '';

                switch (formerUpdateFlag) {
                    case '':
                    case 'A':
                        newUpdateFlag = 'I';
                        break;

                    case 'C':
                        newUpdateFlag = 'U';
                        break;

                    default:
                        break;
                }

                if (newUpdateFlag) {
                    oxBusinessObject.set('updFlag', newUpdateFlag);
                    retval = true;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, retval, ex);
            }

            return retval;
        },

        //public
        //searches the database for pending deletion records
        //keyHashMap allows to specify and 
        //returns an event ID
        //the fired event returns via a boolean, if the procedure was successful
        markPendingDeletionsAsReady: function (keysHashMap) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId(); 

                var me = this;
                var calleeName = arguments.callee;

                //query the database using the index for the update flag
                var queryCallback = function (eventArgs) {
                    try {
                        var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                        if (cursor) {
                            var record = cursor.value;

                            //check, if the encountered record matches all key fields provided in the passed keyHashMap
                            var matches = true;

                            if (keysHashMap && keysHashMap.getCount() > 0) {
                                keysHashMap.each(function (key, value) {
                                    matches = value === record[key];
                                    return matches;
                                }, this);
                            }

                            if (matches) {
                                //reflag updateflag from R to D
                                record['UPDFLAG'] = 'D';

                                AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, record);
                            }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            //all done
                            eventController.requestEventFiring(retval, true);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException(calleeName + '-queryCallback', me.getClassName(), ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('UPDFLAG', 'R');

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(this.getDataBaseStore(), 'UPDFLAG', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().rawQueryUsingAnIndex(this.getDataBaseStore(), 'UPDFLAG', indexRange, queryCallback, queryCallback, false);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
                retval = -1;
            }

            return retval;
        }
        //end region for manual completion management
    }
});