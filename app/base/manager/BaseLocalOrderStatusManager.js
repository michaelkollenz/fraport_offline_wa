﻿Ext.define('AssetManagement.base.manager.BaseLocalOrderStatusManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.LocalOrderStatus',
        'AssetManagement.customer.manager.cache.LocalOrderStatusCache',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils'
    ],

    inheritableStatics: {
        //public
        LOCAL_STATUS_TYPES: {
            OPEN: 0,
            OVERDUE: 1,
            IN_PROCESS: 2,
            COMPLETED: 3
        },

        //private
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.LocalOrderStatusCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //public
        //evaluates passed order's current local status and sets it
        evaluateAndSetLocalStatus: function(order) {
            try {
                if(!order)
                    return;

                var statusTypeToSet = this.LOCAL_STATUS_TYPES.OPEN;

                if(this.matchesCompleteCriterias(order)) {
                    statusTypeToSet = this.LOCAL_STATUS_TYPES.COMPLETED;
                } else if(this.matchesInProcessCriterias(order)) {
                    statusTypeToSet = this.LOCAL_STATUS_TYPES.IN_PROCESS;
                } else if(this.matchesOverdueCriterias(order)) {
                    statusTypeToSet = this.LOCAL_STATUS_TYPES.OVERDUE;
                }

                order.set('localStatus', this.getStatusInstanceForStatusType(statusTypeToSet));
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside evaluateAndSetLocalStatus of BaseLocalOrderStatusManager', ex);
            }
        },

        resetStatusInstances: function() {
            try {
                this._openStatusInstance = null;
                this._overdueStatusInstance = null;
                this._inProcessStatusInstance = null;
                this._completeStatusInstance = null;
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetStatusInstances of BaseLocalOrderStatusManager', ex);
            }
        },

        //private
        //checks the order, if it matches criterias for status type complete
        matchesCompleteCriterias: function(order) {
            var retval = false;

            try {
                //order is complete, if every operation has a timeconf containing the final conf. flag

                if(order) {
					var operations = order.get('operations');

                    if(operations && operations.getCount() > 0) {
                        var anyOperationMissesFinalConfirmation = false;

                        operations.each(function(operation) {
                            var opersTimeConfs = operation.get('timeConfs');

                            if(opersTimeConfs && opersTimeConfs.getCount() > 0) {
                                var finalConfFound = false;

                                opersTimeConfs.each(function(timeConf) {
                                    if(timeConf.get('aueru') === 'X') {
                                        finalConfFound = true;
                                        return false;
                                    }
                                }, this);

                                if(!finalConfFound) {
                                    anyOperationMissesFinalConfirmation = true;
                                    return false;
                                }
                            } else {
                                anyOperationMissesFinalConfirmation = true;
                                return false;
                            }
                        }, this);

                        retval = !anyOperationMissesFinalConfirmation;
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matchesCompleteCriterias of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        //checks the order, if it matches criterias for status type in process
        matchesInProcessCriterias: function(order) {
            var retval = false;

            try {
                //order is in process, if it has been modiefied or locally created it self or has any local created timeconf, matconf or document
                if (order) {
                    var hasBeenModifiedItSelf = order.get('updFlag') !== 'X';

                    if (hasBeenModifiedItSelf) {
                        retval = true;
                    } else {
                        //next check for timeconfs and matconfs
                        var anyLocalCreatedTimeConfFound = false;
                        var anyLocalCreatedMatConfFound = false;

                        var operations = order.get('operations');

                        if (operations && operations.getCount() > 0) {
                            operations.each(function (operation) {
                                //timeconfs check
                                var opersTimeConfs = operation.get('timeConfs');

                                if (opersTimeConfs && opersTimeConfs.getCount() > 0) {
                                    opersTimeConfs.each(function (timeConf) {
                                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConf.get('childKey2'))) {
                                            anyLocalCreatedTimeConfFound = true;
                                            return false;
                                        }
                                    }, this);

                                    if (anyLocalCreatedTimeConfFound)
                                        return false;
                                }

                                //matconfs check
                                var opersMatConfs = operation.get('matConfs');

                                if (opersMatConfs && opersMatConfs.getCount() > 0) {
                                    opersMatConfs.each(function (matConf) {
                                        var updFlag = matConf.get('updFlag');

                                        if (updFlag === 'I') {
                                            anyLocalCreatedMatConfFound = true;
                                            return false;
                                        }
                                    }, this);

                                    if (anyLocalCreatedMatConfFound)
                                        return false;
                                }
                            }, this);
                        }

                        if (anyLocalCreatedTimeConfFound || anyLocalCreatedMatConfFound) {
                            retval = true;
                        } else {
                            //next check for any local appended document
                            var anyLocalDocumentFound = false;
                            var ordersDocuments = order.get('documents');

                            if (ordersDocuments && ordersDocuments.getCount() > 0) {
                                ordersDocuments.each(function (document) {
                                    if (document.get('fileOrigin') === 'local') {
                                        anyLocalDocumentFound = true;
                                        return false;
                                    }
                                }, this);
                            }

                            retval = anyLocalDocumentFound;
                        }
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matchesInProcessCriterias of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        //checks the order, if it matches criterias for status type overdue
        matchesOverdueCriterias: function(order) {
            var retval = false;

            try {
                //order is overdue, if it's end date is in the past

                if(order) {
                    var endTime = order.get('gltrp');

                    if(!AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(endTime)) {
                        retval = endTime < AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matchesOverdueCriterias of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        getStatusInstanceForStatusType: function(statusType) {
            var retval = null;

            try {
                if(statusType || statusType === 0) {
                    switch(statusType) {
                        case this.LOCAL_STATUS_TYPES.OPEN:
                            retval = this.getStatusInstanceForStatusOpen();
                            break;
                        case this.LOCAL_STATUS_TYPES.OVERDUE:
                            retval = this.getStatusInstanceForStatusOverdue();
                            break;
                        case this.LOCAL_STATUS_TYPES.IN_PROCESS:
                            retval = this.getStatusInstanceForStatusInProcess();
                            break;
                        case this.LOCAL_STATUS_TYPES.COMPLETED:
                            retval = this.getStatusInstanceForStatusComplete();
                            break;
                        default:
                            break;
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusInstanceForStatusType of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        getStatusInstanceForStatusOpen: function() {
            var retval = null;

            try {
                var cache = this.getCache();
                retval = cache.getFromCache(this.LOCAL_STATUS_TYPES.OPEN);

                if(!retval) {
                    retval = Ext.create('AssetManagement.customer.model.bo.LocalOrderStatus', {
                        statusCode: this.LOCAL_STATUS_TYPES.OPEN,
                        statusText: Locale.getMsg('open'),
                        statusIconPath: 'resources/icons/released.png',
                        id: this.LOCAL_STATUS_TYPES.OPEN
                    });

                    cache.addToCache(retval);
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusInstanceForStatusOpen of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        getStatusInstanceForStatusOverdue: function () {
            var retval = null;

            try {
                var cache = this.getCache();
                retval = cache.getFromCache(this.LOCAL_STATUS_TYPES.OVERDUE);

                if(!retval) {
                    retval = Ext.create('AssetManagement.customer.model.bo.LocalOrderStatus', {
                        statusCode: this.LOCAL_STATUS_TYPES.OVERDUE,
                        statusText: Locale.getMsg('overdue'),
                        statusIconPath: 'resources/icons/out_of_time.png',
                        id: this.LOCAL_STATUS_TYPES.OVERDUE
                    });

                    cache.addToCache(retval);
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusInstanceForStatusOverdue of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        getStatusInstanceForStatusInProcess: function() {
            var retval = null;

            try {
                var cache = this.getCache();
                retval = cache.getFromCache(this.LOCAL_STATUS_TYPES.IN_PROCESS);

                if(!retval) {
                    retval = Ext.create('AssetManagement.customer.model.bo.LocalOrderStatus', {
                        statusCode: this.LOCAL_STATUS_TYPES.IN_PROCESS,
                        statusText: Locale.getMsg('inProcess'),
                        statusIconPath: 'resources/icons/in_process.png',
                        id: this.LOCAL_STATUS_TYPES.IN_PROCESS
                    });

                    cache.addToCache(retval);
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusInstanceForStatusInProcess of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        },

        //private
        getStatusInstanceForStatusComplete: function() {
            var retval = null;

            try {
                var cache = this.getCache();
                retval = cache.getFromCache(this.LOCAL_STATUS_TYPES.COMPLETED);

                if(!retval) {
                    retval = Ext.create('AssetManagement.customer.model.bo.LocalOrderStatus', {
                        statusCode: this.LOCAL_STATUS_TYPES.COMPLETED,
                        statusText: Locale.getMsg('complete'),
                        statusIconPath: 'resources/icons/completed.png',
						id: this.LOCAL_STATUS_TYPES.COMPLETED
                    });

                    cache.addToCache(retval);
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStatusInstanceForStatusComplete of BaseLocalOrderStatusManager', ex);
            }

            return retval;
        }
    }
});