Ext.define('AssetManagement.base.manager.cache.BaseHistOrderCache', {
	extend: 'AssetManagement.customer.manager.cache.Cache',

	requires: [
	    'AssetManagement.customer.model.bo.OrderHistory'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.OrderHistory',
		onceRequestedFuncLocs: null,
		onceRequestedEquipments: null
	},
	
	//protected
	constructor: function(config) {
		this.callParent(arguments);
	
		try {
			this.setOnceRequestedFuncLocs(Ext.create('Ext.util.HashMap'));
			this.setOnceRequestedEquipments(Ext.create('Ext.util.HashMap'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseHistOrderCache', ex);
	    }
	},
	
	//protected
	//@override
	//also listen to the customizing data cache reset event
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseHistOrderCache', ex);
	    }
	},
	
	//protected
	//@override
	clearCache: function() {
		this.callParent(arguments);
		
        try {
        	this.getOnceRequestedFuncLocs().clear();
        	this.getOnceRequestedEquipments().clear();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseHistOrderCache', ex);
		}    
	},
	
	//adds a set of hist orders to cache for a specific func. loc.
	//when added this way, the cache may return them on future requests for this func. loc.
	addToCacheForFuncLoc: function(tplnr, histOrders) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr) && histOrders) {
				var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();
			
				if(!onceRequestedFuncLocs.containsKey(tplnr)) {
					onceRequestedFuncLocs.add(tplnr, tplnr);
						
					if(histOrders.getCount() > 0) {
						histOrders.each(function(histOrder) {
							this.addToCache(histOrder);
						}, this);
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForFuncLoc of BaseHistOrderCache', ex);
	    }
	},

	//adds a set of hist orders to cache for a specific equipment
	//when added this way, the cache may return them on future requests for this equipment
	addToCacheForEquipment: function(equnr, histOrders) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr) && histOrders) {
				var onceRequestedEquipments = this.getOnceRequestedEquipments();
			
				if(!onceRequestedEquipments.containsKey(equnr)) {
					onceRequestedEquipments.add(equnr, equnr);
					
					if(histOrders.getCount() > 0) {
						histOrders.each(function(histOrder) {
							this.addToCache(histOrder);
						}, this);
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForEquipment of BaseHistOrderCache', ex);
	    }
	},
	
	//returns func. loc.'s hist orders, if these have been added via addToCacheForFuncLoc once before
	getFromCacheForFuncLoc: function(tplnr) {
		var retval = null;
	
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
				var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();
				
				if(onceRequestedFuncLocs.containsKey(tplnr) || this.getCacheComplete()) {
					var funcLocsHistOrders = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.OrderHistory',
						autoLoad: false
					});
					
					if(this.getCache().getCount() > 0) {
						this.getCache().each(function(key, cacheEntry) {
							var histOrder = cacheEntry.get('value');
							
							if(histOrder.get('tplnr') === tplnr)
								funcLocsHistOrders.add(histOrder);
						}, this);
					}
				
					retval = funcLocsHistOrders;
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForFuncLoc of BaseHistOrderCache', ex);
	    }
		
	    return retval;
	},
	
	//returns equipment's hist orders, if these have been added via addToCacheForEquipment once before
	getFromCacheForEquipment: function(equnr) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
				var onceRequestedEquipments = this.getOnceRequestedEquipments();
				
				if(onceRequestedEquipments.containsKey(equnr) || this.getCacheComplete()) {
					var equipmentsHistOrders = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.OrderHistory',
						autoLoad: false
					});
					
					if(this.getCache().getCount() > 0) {
						this.getCache().each(function(key, cacheEntry) {
							var histOrder = cacheEntry.get('value');
							
							if(histOrder.get('equnr') === equnr)
								equipmentsHistOrders.add(histOrder);
						}, this);
					}
				
					retval = equipmentsHistOrders;
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForEquipment of BaseHistOrderCache', ex);
		}
		
		return retval;
	}
});