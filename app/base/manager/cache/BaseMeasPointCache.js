﻿Ext.define('AssetManagement.base.manager.cache.BaseMeasPointCache', {
    extend: 'AssetManagement.customer.manager.cache.Cache',

    requires: [
	    'AssetManagement.customer.model.bo.OrderHistory'
    ],

    config: {
        modelClass: 'AssetManagement.customer.model.bo.MeasurementPoint',
        onceRequestedFuncLocs: null,
        onceRequestedEquipments: null
    },

    //protected
    constructor: function (config) {
        this.callParent(arguments);

        try {
            this.setOnceRequestedFuncLocs(Ext.create('Ext.util.HashMap'));
            this.setOnceRequestedEquipments(Ext.create('Ext.util.HashMap'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseMeasPointCache', ex);
        }
    },

    //protected
    //@override
    //also listen to the customizing data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseMeasPointCache', ex);
        }
    },

    //protected
    //@override
    clearCache: function () {
        this.callParent(arguments);

        try {
            this.getOnceRequestedFuncLocs().clear();
            this.getOnceRequestedEquipments().clear();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseMeasPointCache', ex);
        }
    },

    //adds a set of meas points to cache for a specific func. loc.
    //when added this way, the cache may return them on future requests for this func. loc.
    addToCacheForFuncLoc: function (tplnr, measPoints) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr) && measPoints) {
                var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();

                if (!onceRequestedFuncLocs.containsKey(tplnr)) {
                    onceRequestedFuncLocs.add(tplnr, tplnr);

                    if (measPoints.getCount() > 0) {
                        measPoints.each(function (measPoint) {
                            this.addToCache(measPoint);
                        }, this);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForFuncLoc of BaseMeasPointCache', ex);
        }
    },

    //adds a set of meas points to cache for a specific equipment
    //when added this way, the cache may return them on future requests for this equipment
    addToCacheForEquipment: function (equnr, measPoints) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr) && measPoints) {
                var onceRequestedEquipments = this.getOnceRequestedEquipments();

                if (!onceRequestedEquipments.containsKey(equnr)) {
                    onceRequestedEquipments.add(equnr, equnr);

                    if (measPoints.getCount() > 0) {
                        measPoints.each(function (measPoint) {
                            this.addToCache(measPoint);
                        }, this);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForEquipment of BaseMeasPointCache', ex);
        }
    },

    //returns func. loc.'s meas points, if these have been added via addToCacheForFuncLoc once before
    getFromCacheForFuncLoc: function (tplnr) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();

                if (onceRequestedFuncLocs.containsKey(tplnr) || this.getCacheComplete()) {
                    var funcLocsMeasPoints = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                        autoLoad: false
                    });

                    if (this.getCache().getCount() > 0) {
                        this.getCache().each(function (key, cacheEntry) {
                            var measPoint = cacheEntry.get('value');

                            if (measPoint.get('mpobj') === tplnr)
                                funcLocsMeasPoints.add(measPoint);
                        }, this);
                    }

                    retval = funcLocsMeasPoints;
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForFuncLoc of BaseMeasPointCache', ex);
        }

        return retval;
    },

    //returns equipment's meas points, if these have been added via addToCacheForEquipment once before
    getFromCacheForEquipment: function (equnr) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var onceRequestedEquipments = this.getOnceRequestedEquipments();

                if (onceRequestedEquipments.containsKey(equnr) || this.getCacheComplete()) {
                    var equipmentsMeasPoints = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                        autoLoad: false
                    });

                    if (this.getCache().getCount() > 0) {
                        this.getCache().each(function (key, cacheEntry) {
                            var measPoint = cacheEntry.get('value');

                            if (measPoint.get('mpobj') === equnr)
                                equipmentsMeasPoints.add(measPoint);
                        }, this);
                    }

                    retval = equipmentsMeasPoints;
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForEquipment of BaseMeasPointCache', ex);
        }

        return retval;
    }
});