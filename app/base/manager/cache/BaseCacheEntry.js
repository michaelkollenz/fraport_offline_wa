Ext.define('AssetManagement.base.manager.cache.BaseCacheEntry', {
    extend: 'Ext.data.Model',
 
    fields: [
       { name: 'key', type: 'string' },
       { name: 'value', type: 'auto' }
    ]
});