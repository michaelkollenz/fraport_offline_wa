Ext.define('AssetManagement.base.manager.cache.BaseInventoryItemCache', {
    extend: 'AssetManagement.customer.manager.cache.Cache',

    requires: [
	    'AssetManagement.customer.model.bo.InventoryItem'
    ],

    config: {
        modelClass: 'AssetManagement.customer.model.bo.InventoryItem'
    },

    //protected
    //@override
    //also listen to the customizing data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseInventoryItemCache', ex);
        }
    },

    getFromCacheForInventory: function (physinventory, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(physinventory)) {
                var onceRequestedNotifs = this.getOnceRequestedNotifs();

                var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
                var coveredBySpecific = onceRequestedNotifs.containsKey(physinventory) && dataGrade <= onceRequestedNotifs.get(physinventory);

                if (coveredByAll || coveredBySpecific) {
                    retval = this.extractFromCacheForNotif(physinventory);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForInventory of BaseInventoryItemCache', ex);
        }

        return retval;
    },
    getAllUpToGrade: function (dataGrade) {
        var retval = null;

        try {
            if (!dataGrade)
                dataGrade = this.self.DATAGRADES.BASE;

            retval = Ext.create('Ext.data.Store', {
                model: this.getModelClass(),
                autoLoad: false
            });

            if (this.getCache().getCount() > 0) {
                this.getCache().each(function (physinventory, metaCacheEntry) {
                    var subHashMap = metaCacheEntry.get('value');

                    //check if the meta entry in not empty
                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            if (cacheEntry.get('dataGrade') <= dataGrade)
                                retval.add(cacheEntry.get('value'));
                        }, this);
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of BaseInventoryItemCache', ex);
            retval = null;
        }

        return retval;
    }
});