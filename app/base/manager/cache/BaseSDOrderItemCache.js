Ext.define('AssetManagement.base.manager.cache.BaseSDOrderItemCache', {
	extend: 'AssetManagement.customer.manager.cache.GradedCache',
	
	//THIS CACHE WORKS HIERARCHICALLY
	//THAT IS WHY IT HAS TO OVERRIDE NEARLY ALL APIS OF THE BASSE CLASS

	requires: [
	    'AssetManagement.customer.model.bo.SDOrderItem'
	],

	config: {
		//private
		modelClass: 'AssetManagement.customer.model.bo.SDOrderItem',
		onceRequestedSDOrders: null
	},
	
	//protected
	constructor: function(config) {
		this.callParent(arguments);
	
		try {
			this.setOnceRequestedSDOrders(Ext.create('Ext.util.HashMap'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseSDOrderItemCache', ex);
	    }
	},
	
	//protected
	//@override
	clearCache: function() {
		this.callParent(arguments);
		
        try {
        	this.getOnceRequestedSDOrders().clear();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseSDOrderItemCache', ex);
		}    
	},
	
	//protected
	//@override
	//also listen to the moving data cache reset event
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseSDOrderItemCache', ex);
	    }
	},
	
	//iterates over the cache to determine the lowest present datagrade
	getMinimumDataGrade: function() {
		var retval = this.self.DATAGRADES.BASE;
		
		try {
			var cacheRef = this.getCache();
			
			if(cacheRef.getCount() > 0) {
				var lowestEncountered = this.self.DATAGRADES.FULL;
			
				var breakLoop = false;

				cacheRef.each(function(metaKey, metaCacheEntry) {
					var subHashMap = metaCacheEntry.get('value');
					
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							var entriesDataGrade = cacheEntry.get('dataGrade');
							
							if(entriesDataGrade < lowestEncountered)
								lowestEncountered = entriesDataGrade;
							
							//break the loop, if the lowest possible value has been encountered
							breakLoop = lowestEncountered == retval;
							
							return !breakLoop;
						}, this);
					}
					
					return !breakLoop;
				}, this);

				retval = lowestEncountered;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of BaseSDOrderItemCache', ex);
		}
		
		return retval;
	},
	
	//public
	//@override
	//adds a single sdOrderItem to cache, considering the passed data grade
	//if the sdOrderItem is already cached, it will be checked if it's cache entrie's data grade has to be updated
	addToCache: function(sdOrderItem, dataGrade) {
		try {
			if(sdOrderItem) {
				var vbeln = sdOrderItem.get('vbeln');
				var key = this.extractKeyFromObject(sdOrderItem);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					if(!dataGrade)
						dataGrade = this.self.DATAGRADES.BASE;
						
					var cachedMetaEntry = this.getCache().get(vbeln);
					var subHashMap = null;

					if(!cachedMetaEntry) {
						subHashMap = Ext.create('Ext.util.HashMap');
					
						cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: vbeln,
							value: subHashMap,
							dataGrade: this.self.DATAGRADES.BASE
						});
						
						this.getCache().add(vbeln, cachedMetaEntry);
					} else {
						subHashMap = cachedMetaEntry.get('value');
					}
					
					var cachedEntry = subHashMap.get(key);

					if(!cachedEntry) {
						cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: key,
							value: sdOrderItem,
							dataGrade: dataGrade
						});
						
						subHashMap.add(key, cachedEntry);
					}
					
					if(dataGrade > cachedEntry.get('dataGrade')) {
						cachedEntry.set('dataGrade', dataGrade);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseSDOrderItemCache', ex);
	    }
	},
	
	//public
	//adds a store of sdOrderItems to the cache
	//when added this way, the cache may be able to return something on following requests for the same sd order 
	addToCacheForSDOrder: function(vbeln, sDOrderItems, dataGrade) {
		try {
			var onceRequestedSDOrders = this.getOnceRequestedSDOrders();
		
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln) && sDOrderItems) {
				if(!dataGrade)
					dataGrade = this.self.DATAGRADES.BASE;
				
				if(!onceRequestedSDOrders.containsKey(vbeln) || onceRequestedSDOrders.get(vbeln) < dataGrade)
					onceRequestedSDOrders.replace(vbeln, dataGrade);
					
				//next generate/update meta entry
				var cachedMetaEntry = this.getCache().get(vbeln);
				
				if(!cachedMetaEntry) {
					subHashMap = Ext.create('Ext.util.HashMap');
					
					cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
						key: vbeln,
						value: subHashMap,
						dataGrade: dataGrade
					});
					
					this.getCache().add(vbeln, cachedMetaEntry);
				} else if(cachedMetaEntry.get('dataGrade') < dataGrade) {
					cachedMetaEntry.set('dataGrade', dataGrade);
				}
				
				if(sDOrderItems.getCount() > 0) {
					sDOrderItems.each(function(sdOrderItem) {
						this.addToCache(sdOrderItem, dataGrade);
					}, this);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForSDOrder of BaseSDOrderItemCache', ex);
	    }
	},
	
	//public
	//@override
	//tries to fetch the corresponding cache entry for the passed key at atleast the requested data grade
	getFromCache: function(key, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key) && key.length > 10) {
				var vbeln = key.substring(0, 10);
			
				var cachedMetaEntry = this.getCache().get(vbeln);
				
				if(cachedMetaEntry) {
					subHashMap = cachedMetaEntry.get('value');
					
					var cachedEntry = subHashMap.get(key);
					
					if(cachedEntry) {
						if(!dataGrade)
							dataGrade = this.self.DATAGRADES.BASE;
					
						if(dataGrade <= cachedEntry.get('dataGrade')) {
							retval = cachedEntry.get('value');
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseSDOrderItemCache', ex);
	    }
		
		return retval;
	},

	//public
	//@override
	//will return a store of all cached sdOrderItems, if the cache has been once filled using addStoreForAllToCache
	//and the caches minimum data grade complies with the requested data grade
	getStoreForAll: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
		
			if(this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade()) {
				retval = Ext.create('Ext.data.Store', {
					model: this.getModelClass(),
					autoLoad: false
				});
			
				if(this.getCache().getCount() > 0) {
					this.getCache().each(function(vbeln, metaCacheEntry) {
						var subHashMap = metaCacheEntry.get('value');
						
						if(subHashMap.getCount() > 0) {
							subHashMap.each(function(key, cacheEntry) {
								retval.add(cacheEntry.get('value'));
							}, this);
						}
					}, this);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of BaseSDOrderItemCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	getAllUpToGrade: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
				
			retval = Ext.create('Ext.data.Store', {
				model: this.getModelClass(),
				autoLoad: false
			});
			
			if(this.getCache().getCount() > 0) {
				this.getCache().each(function(vbeln, metaCacheEntry) {
					var subHashMap = metaCacheEntry.get('value');
				
					//check if the meta entry in not empty
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							if(cacheEntry.get('dataGrade') <= dataGrade)
								retval.add(cacheEntry.get('value'));
						}, this);
					}
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of BaseSDOrderItemCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//tries to get sdOrderItems for a order from cache
	//will return order's sdOrderItems, only if they have been added via addToCacheForSDOrder once
	getFromCacheForSDOrder: function(vbeln, dataGrade) {
		var retval = null;
	
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln)) {
				var onceRequestedSDOrders = this.getOnceRequestedSDOrders();
				
				var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
				var coveredBySpecific = onceRequestedSDOrders.containsKey(vbeln) && dataGrade <= onceRequestedSDOrders.get(vbeln);
				
				if(coveredByAll || coveredBySpecific) {
					retval = this.extractFromCacheForSDOrder(vbeln);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForSDOrder of BaseSDOrderItemCache', ex);
		}
		
		return retval;
	},
	
	//public
	//combines logic from getFromCacheForSDOrder and getAllUpToGrade
	getAllUpToGradeForSDOrder: function(vbeln, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln)) {
				var onceRequestedSDOrders = this.getOnceRequestedSDOrders();
				
				var coveredByAll = this.getCacheComplete();
				var coveredBySpecific = onceRequestedSDOrders.containsKey(vbeln);
				
				if(coveredByAll || coveredBySpecific) {
					var cachedMetaEntry = this.getCache().get(vbeln);
					
					if(cachedMetaEntry) {
						var subHashMap = cachedMetaEntry.get('value');
						
						var sdOrderItems = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.SDOrderItem',
							autoLoad: false
						});
						
						//check if the meta entry in not empty
						if(subHashMap.getCount() > 0) {
							subHashMap.each(function(key, cacheEntry) {
								if(cacheEntry.get('dataGrade') <= dataGrade)
									sdOrderItems.add(cacheEntry.get('value'));
							}, this);
						}
					
						retval = sdOrderItems;
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForSDOrder of BaseSDOrderItemCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//removes a specific sdOrderItem from cache
	removeFromCache: function(sdOrderItem) {
		try {
			if(sdOrderItem) {
				var vbeln = sdOrderItem.get('vbeln');
				var key = this.extractKeyFromObject(sdOrderItem);
			
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					var cachedMetaEntry = this.getCache().get(vbeln);
					
					if(cachedMetaEntry) {
						var subHashMap = cachedMetaEntry.get('value');
						
						subHashMap.removeAtKey(key);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of BaseSDOrderItemCache', ex);
	    }
	},
	
	//private
	extractFromCacheForSDOrder: function(vbeln) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln)) {
				var cachedMetaEntry = this.getCache().get(vbeln);
			
				if(cachedMetaEntry) {
					var subHashMap = cachedMetaEntry.get('value');
					
					var sdOrderItems = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.SDOrderItem',
						autoLoad: false
					});
					
					//check if the meta entry in not empty
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							sdOrderItems.add(cacheEntry.get('value'));
						}, this);
					}
			
					retval = sdOrderItems;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForSDOrder of BaseSDOrderItemCache', ex);
		}
		
		return retval;
	}
});