﻿Ext.define('AssetManagement.base.manager.cache.BaseHistMeasDocCache', {
    extend: 'AssetManagement.customer.manager.cache.GradedCache',

    requires: [
	    'AssetManagement.customer.model.bo.MeasurementDocHistory'
    ],

    config: {
        modelClass: 'AssetManagement.customer.model.bo.MeasurementDocHistory'
    },

    //protected
    //@override
    //also listen to the moving data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseHistMeasDocCache', ex);
        }
    }
});