﻿Ext.define('AssetManagement.base.manager.cache.BaseClassificationCache', {
    extend: 'AssetManagement.customer.manager.cache.Cache',

    requires: [
	    'AssetManagement.customer.model.bo.OrderHistory'
    ],

    config: {
        modelClass: 'AssetManagement.customer.model.bo.MeasurementPoint',
        onceRequestedFuncLocs: null,
        onceRequestedEquipments: null
    },

    //protected
    constructor: function (config) {
        this.callParent(arguments);

        try {
            this.setOnceRequestedFuncLocs(Ext.create('Ext.util.HashMap'));
            this.setOnceRequestedEquipments(Ext.create('Ext.util.HashMap'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseClassificationCache', ex);
        }
    },

    //protected
    //@override
    //also listen to the customizing data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseClassificationCache', ex);
        }
    },

    //protected
    //@override
    clearCache: function () {
        this.callParent(arguments);

        try {
            this.getOnceRequestedFuncLocs().clear();
            this.getOnceRequestedEquipments().clear();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseClassificationCache', ex);
        }
    },

    //adds a set of meas points to cache for a specific func. loc.
    //when added this way, the cache may return them on future requests for this func. loc.
    addToCacheForFuncLoc: function (objnr, objClasses) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr) && objClasses) {
                var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();

                if (!onceRequestedFuncLocs.containsKey(objnr)) {
                    onceRequestedFuncLocs.add(objnr, objnr);

                    if (objClasses.getCount() > 0) {
                        objClasses.each(function (objClass) {
                            this.addToCache(objClass);
                        }, this);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForFuncLoc of BaseClassificationCache', ex);
        }
    },

    //adds a set of meas points to cache for a specific equipment
    //when added this way, the cache may return them on future requests for this equipment
    addToCacheForEquipment: function (equnr, measPoints) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr) && measPoints) {
                var onceRequestedEquipments = this.getOnceRequestedEquipments();

                if (!onceRequestedEquipments.containsKey(equnr)) {
                    onceRequestedEquipments.add(equnr, equnr);

                    if (measPoints.getCount() > 0) {
                        measPoints.each(function (measPoint) {
                            this.addToCache(measPoint);
                        }, this);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForEquipment of BaseClassificationCache', ex);
        }
    },

    //returns func. loc.'s meas points, if these have been added via addToCacheForFuncLoc once before
    getFromCacheForFuncLoc: function (objnr) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
                var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();

                if (onceRequestedFuncLocs.containsKey(objnr) || this.getCacheComplete()) {
                    var funcLocsObjClasses = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.ObjClass',
                        autoLoad: false
                    });

                    if (this.getCache().getCount() > 0) {
                        this.getCache().each(function (key, cacheEntry) {
                            var objClass = cacheEntry.get('value');

                            if (objClass.get('objnr') === objnr)
                                funcLocsObjClasses.add(objClass);
                        }, this);
                    }

                    retval = funcLocsObjClasses;
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForFuncLoc of BaseClassificationCache', ex);
        }

        return retval;
    },

    //returns equipment's meas points, if these have been added via addToCacheForEquipment once before
    getFromCacheForEquipment: function (equnr) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var onceRequestedEquipments = this.getOnceRequestedEquipments();

                if (onceRequestedEquipments.containsKey(equnr) || this.getCacheComplete()) {
                    var equipmentsMeasPoints = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                        autoLoad: false
                    });

                    if (this.getCache().getCount() > 0) {
                        this.getCache().each(function (key, cacheEntry) {
                            var measPoint = cacheEntry.get('value');

                            if (measPoint.get('mpobj') === equnr)
                                equipmentsMeasPoints.add(measPoint);
                        }, this);
                    }

                    retval = equipmentsMeasPoints;
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForEquipment of BaseClassificationCache', ex);
        }

        return retval;
    }
});