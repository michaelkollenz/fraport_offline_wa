Ext.define('AssetManagement.base.manager.cache.BaseNotifCache', {
	extend: 'AssetManagement.customer.manager.cache.GradedCache',

	requires: [
	    'AssetManagement.customer.model.bo.Notif',
	    'AssetManagement.customer.model.bo.NotifItem',
	    'AssetManagement.customer.model.bo.NotifTask',
	    'AssetManagement.customer.model.bo.NotifActivity',
	    'AssetManagement.customer.model.bo.SDOrder',
	    'Ext.data.Store'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.Notif'
	},
	
	//protected
	//@override
	//also listen to the moving data cache reset event
	//also listen to sub items data changed events
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifItemManager.EVENTS.ITEM_ADDED, this.onNotifItemAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifItemManager.EVENTS.ITEM_CHANGED, this.onNotifItemChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifItemManager.EVENTS.ITEM_DELETED, this.onNotifItemDeleted, this);

			eventController.registerOnEvent(AssetManagement.customer.manager.NotifTaskManager.EVENTS.TASK_ADDED, this.onNotifTaskAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifTaskManager.EVENTS.TASK_CHANGED, this.onNotifTaskChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifTaskManager.EVENTS.TASK_DELETED, this.onNotifTaskDeleted, this);

			eventController.registerOnEvent(AssetManagement.customer.manager.NotifActivityManager.EVENTS.ACTIVITY_ADDED, this.onNotifActivityAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifActivityManager.EVENTS.ACTIVITY_CHANGED, this.onNotifActivityChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifActivityManager.EVENTS.ACTIVITY_DELETED, this.onNotifActivityDeleted, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifCauseManager.EVENTS.CAUSE_ADDED, this.onNotifCauseAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifCauseManager.EVENTS.CAUSE_CHANGED, this.onNotifCauseChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.NotifCauseManager.EVENTS.CAUSE_DELETED, this.onNotifCauseDeleted, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderManager.EVENTS.SDORDER_ADDED, this.onSDOrderAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderManager.EVENTS.SDORDER_CHANGED, this.onSDOrderChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderManager.EVENTS.SDORDER_DELETED, this.onSDOrderDeleted, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseNotifCache', ex);
	    }
	},
	
	//handles case, a new notifItem has to be included into an affected cache entrie's dependend data
	onNotifItemAdded: function(notifItem) {
		try {
			if(notifItem) {
				var affectedNotif = this.getFromCache(notifItem.get('qmnum'));
				
				if(affectedNotif) {
					var notifsItems = affectedNotif.get('notifItems');
					
					if(!notifsItems) {
						notifsItems = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.NotifItem',
							autoLoad: false
						});
						
						affectedNotif.set('notifItems', notifsItems);
					}
					
					var indexOfAffectedNotifItem = notifsItems.indexOfId(notifItem.get('id'));
					
					if(indexOfAffectedNotifItem >= 0) {
						var affectedNotifItem = notifsItems.getAt(indexOfAffectedNotifItem);
						
						if(affectedNotifItem !== notifItem) {
							notifsItems.removeAt(indexOfAffectedNotifItem);
							notifsItems.insert(indexOfAffectedNotifItem, notifItem);
						}
					} else {
						notifsItems.add(notifItem);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifItemAdded of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifItem's data changed on a duplicate notifItem object
	onNotifItemChanged: function(notifItem) {
		try {
			//case is covered by logic of onNotifNotifItemAdded - the affected notifItem will be replaced by the duplicate
			this.onNotifItemAdded(notifItem);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifItemChanged of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifItem has not yet been deleted from an affected cache entrie's dependend data
	onNotifItemDeleted: function(notifItem) {
		try {
			if(notifItem) {
				var affectedNotif = this.getFromCache(notifItem.get('qmnum'));
				
				if(affectedNotif) {
					var notifsItems = affectedNotif.get('notifItems');
					
					if(notifsItems) {
						var indexOfAffectedNotifItem = notifsItems.indexOfId(notifItem.get('id'));
						
						if(indexOfAffectedNotifItem >= 0) {
							notifsItems.removeAt(indexOfAffectedNotifItem);
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifItemDeleted of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a new notifTask has to be included into an affected cache entrie's dependend data
	onNotifTaskAdded: function(notifTask) {
		try {
			if(notifTask) {
				if(notifTask.get('fenum') === '0000') {
					var affectedNotif = this.getFromCache(notifTask.get('qmnum'));
					
					if(affectedNotif) {
						var notifsTasks = affectedNotif.get('notifTasks');
						
						if(!notifsTasks) {
							notifsTasks = Ext.create('Ext.data.Store', {
								model: 'AssetManagement.customer.model.bo.NotifTask',
								autoLoad: false
							});
							
							affectedNotif.set('notifTasks', notifsTasks);
						}
						
						var indexOfAffectedNotifTask = notifsTasks.indexOfId(notifTask.get('id'));
						
						if(indexOfAffectedNotifTask >= 0) {
							var affectedNotifTask = notifsTasks.getAt(indexOfAffectedNotifTask);
							
							if(affectedNotifTask !== notifTask) {
								notifsTasks.removeAt(indexOfAffectedNotifTask);
								notifsTasks.insert(indexOfAffectedNotifTask, notifTask);
							}
						} else {
							notifsTasks.add(notifTask);
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTaskAdded of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifTask's data changed on a duplicate notifTask object
	onNotifTaskChanged: function(notifTask) {
		try {
			//case is covered by logic of onNotifNotifTaskAdded - the affected notifTask will be replaced by the duplicate
			this.onNotifTaskAdded(notifTask);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTaskChanged of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifTask has not yet been deleted from an affected cache entrie's dependend data
	onNotifTaskDeleted: function(notifTask) {
		try {
			if(notifTask) {
				if(notifTask.get('fenum') === '0000') {
					var affectedNotif = this.getFromCache(notifTask.get('qmnum'));
					
					if(affectedNotif) {
						var notifsTasks = affectedNotif.get('notifTasks');
						
						if(notifsTasks) {
							var indexOfAffectedNotifTask = notifsTasks.indexOfId(notifTask.get('id'));
							
							if(indexOfAffectedNotifTask >= 0) {
								notifsTasks.removeAt(indexOfAffectedNotifTask);
							}
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTaskDeleted of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a new notifActivity has to be included into an affected cache entrie's dependend data
	onNotifActivityAdded: function(notifActivity) {
		try {
			if(notifActivity) {
				if(notifActivity.get('fenum') === '0000') {
					var affectedNotif = this.getFromCache(notifActivity.get('qmnum'));
					
					if(affectedNotif) {
						var notifsActivities = affectedNotif.get('notifActivities');
						
						if(!notifsActivities) {
							notifsActivities = Ext.create('Ext.data.Store', {
								model: 'AssetManagement.customer.model.bo.NotifActivity',
								autoLoad: false
							});
							
							affectedNotif.set('notifActivities', notifsActivities);
						}
						
						var indexOfAffectedNotifActivity = notifsActivities.indexOfId(notifActivity.get('id'));
						
						if(indexOfAffectedNotifActivity >= 0) {
							var affectedNotifActivity = notifsActivities.getAt(indexOfAffectedNotifActivity);
							
							if(affectedNotifActivity !== notifActivity) {
								notifsActivities.removeAt(indexOfAffectedNotifActivity);
								notifsActivities.insert(indexOfAffectedNotifActivity, notifActivity);
							}
						} else {
							notifsActivities.add(notifActivity);
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifActivityAdded of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifActivity's data changed on a duplicate notifActivity object
	onNotifActivityChanged: function(notifActivity) {
		try {
			//case is covered by logic of onNotifNotifActivityAdded - the affected notifActivity will be replaced by the duplicate
			this.onNotifActivityAdded(notifActivity);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifActivityChanged of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifActivity has not yet been deleted from an affected cache entrie's dependend data
	onNotifActivityDeleted: function(notifActivity) {
		try {
			if(notifActivity) {
				if(notifActivity.get('fenum') === '0000') {
					var affectedNotif = this.getFromCache(notifActivity.get('qmnum'));
					
					if(affectedNotif) {
						var notifsActivities = affectedNotif.get('notifActivities');
						
						if(notifsActivities) {
							var indexOfAffectedNotifActivity = notifsActivities.indexOfId(notifActivity.get('id'));
							
							if(indexOfAffectedNotifActivity >= 0) {
								notifsActivities.removeAt(indexOfAffectedNotifActivity);
							}
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifActivityDeleted of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a new notifCause has been added - e.g. status conditions changed
	onNotifCauseAdded: function(notifCause) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifCauseAdded of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifCause has changed - e.g. status conditions changed
	onNotifCauseChanged: function(notifCause) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifCauseChanged of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a notifCause has been deleted - e.g. status conditions changed
	onNotifCauseDeleted: function(notifCause) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifCauseDeleted of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a new sd order has to be included into an affected cache entrie's dependend data
	onSDOrderAdded: function(sdOrder) {
		try {
			if(sdOrder) {
				var affectedNotif = this.getFromCache(sdOrder.get('qmnum'));
				
				if(affectedNotif) {
					var notifsSDOrders = affectedNotif.get('sdOrders');
					
					if(!notifsSDOrders) {
						notifsSDOrders = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.SDOrder',
							autoLoad: false
						});
						
						affectedNotif.set('sdOrders', notifsSDOrders);
					}
					
					var indexOfAffectedSDOrder = notifsSDOrders.indexOfId(sdOrder.get('id'));
					
					if(indexOfAffectedSDOrder >= 0) {
						var affectedSDOrder = notifsSDOrders.getAt(indexOfAffectedSDOrder);
						
						if(affectedSDOrder !== sdOrder) {
							notifsSDOrders.removeAt(indexOfAffectedSDOrder);
							notifsSDOrders.insert(indexOfAffectedSDOrder, sdOrder);
						}
					} else {
						notifsSDOrders.add(sdOrder);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderAdded of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a sdOrder's data changed on a duplicate sdOrder object
	onSDOrderChanged: function(sdOrder) {
		try {
			//case is covered by logic of onSDOrderAdded - the affected sdOrder will be replaced by the duplicate
			this.onSDOrderAdded(sdOrder);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderChanged of BaseNotifCache', ex);
		} 
	},
	
	//handles case, a sdOrder has not yet been deleted from an affected cache entrie's dependend data
	onSDOrderDeleted: function(sdOrder) {
		try {
			if(sdOrder) {
				var affectedNotif = this.getFromCache(sdOrder.get('qmnum'));
				
				if(affectedNotif) {
					var notifsSDOrders = affectedNotif.get('sdOrders');
					
					if(notifsSDOrders) {
						var indexOfAffectedSDOrder = notifsSDOrders.indexOfId(sdOrder.get('id'));
						
						if(indexOfAffectedSDOrder >= 0) {
							notifsSDOrders.removeAt(indexOfAffectedSDOrder);
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderDeleted of BaseNotifCache', ex);
		} 
	}
});