﻿Ext.define('AssetManagement.base.manager.cache.BaseQPlanCacheForMat', {
    extend: 'AssetManagement.customer.manager.cache.Cache',

    requires: [
	    'AssetManagement.customer.model.bo.QPlan'
    ],

    config: {
        modelClass: 'AssetManagement.customer.model.bo.QPlan'
    },

    //protected
    //@override
    //also listen to the customizing data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseQPlanCacheForMat', ex);
        }
    },

    //adds a qplan to cache for matnr
    addToCache: function (qplan, matnr) {
        try {
            if (qplan && matnr) {
                this.getCache().add(matnr, qplan);                
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseQPlanCacheForMat', ex);
        }
    },

    getFromCache: function (matnr) {
        var retval = null;

        try {
            if (matnr) {
                retval = this.getCache().get(matnr);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseQPlanCacheForMat', ex);
        }

        return retval;
    }
});