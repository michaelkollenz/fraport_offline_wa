Ext.define('AssetManagement.base.manager.cache.BaseHistNotifCache', {
	extend: 'AssetManagement.customer.manager.cache.Cache',

	requires: [
	    'AssetManagement.customer.model.bo.NotifHistory'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.NotifHistory',
		onceRequestedFuncLocs: null,
		onceRequestedEquipments: null
	},
	
	//protected
	constructor: function(config) {
		this.callParent(arguments);
	
		try {
			this.setOnceRequestedFuncLocs(Ext.create('Ext.util.HashMap'));
			this.setOnceRequestedEquipments(Ext.create('Ext.util.HashMap'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseHistNotifCache', ex);
	    }
	},
	
	//protected
	//@override
	//also listen to the customizing data cache reset event
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseHistNotifCache', ex);
	    }
	},
	
	//protected
	//@override
	clearCache: function() {
		this.callParent(arguments);
		
        try {
        	this.getOnceRequestedFuncLocs().clear();
        	this.getOnceRequestedEquipments().clear();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseHistNotifCache', ex);
		}    
	},
	
	//adds a set of hist notifs to cache for a specific func. loc.
	//when added this way, the cache may return them on future requests for this func. loc.
	addToCacheForFuncLoc: function(tplnr, histNotifs) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr) && histNotifs) {
				var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();
			
				if(!onceRequestedFuncLocs.containsKey(tplnr)) {
					onceRequestedFuncLocs.add(tplnr, tplnr);
						
					if(histNotifs.getCount() > 0) {
						histNotifs.each(function(histNotif) {
							this.addToCache(histNotif);
						}, this);
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForFuncLoc of BaseHistNotifCache', ex);
	    }
	},

	//adds a set of hist notifs to cache for a specific equipment
	//when added this way, the cache may return them on future requests for this equipment
	addToCacheForEquipment: function(equnr, histNotifs) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr) && histNotifs) {
				var onceRequestedEquipments = this.getOnceRequestedEquipments();
			
				if(!onceRequestedEquipments.containsKey(equnr)) {
					onceRequestedEquipments.add(equnr, equnr);
					
					if(histNotifs.getCount() > 0) {
						histNotifs.each(function(histNotif) {
							this.addToCache(histNotif);
						}, this);
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForEquipment of BaseHistNotifCache', ex);
	    }
	},
	
	//returns func. loc.'s hist notifs, if these have been added via addToCacheForFuncLoc once before
	getFromCacheForFuncLoc: function(tplnr) {
		var retval = null;
	
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
				var onceRequestedFuncLocs = this.getOnceRequestedFuncLocs();
				
				if(onceRequestedFuncLocs.containsKey(tplnr) || this.getCacheComplete()) {
					var funcLocsHistNotifs = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifHistory',
						autoLoad: false
					});
					
					if(this.getCache().getCount() > 0) {
						this.getCache().each(function(key, cacheEntry) {
							var histNotif = cacheEntry.get('value');
							
							if(histNotif.get('tplnr') === tplnr)
								funcLocsHistNotifs.add(histNotif);
						}, this);
					}
				
					retval = funcLocsHistNotifs;
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForFuncLoc of BaseHistNotifCache', ex);
	    }
		
	    return retval;
	},
	
	//returns equipment's hist notifs, if these have been added via addToCacheForEquipment once before
	getFromCacheForEquipment: function(equnr) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
				var onceRequestedEquipments = this.getOnceRequestedEquipments();
				
				if(onceRequestedEquipments.containsKey(equnr) || this.getCacheComplete()) {
					var equipmentsHistNotifs = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifHistory',
						autoLoad: false
					});
					
					if(this.getCache().getCount() > 0) {
						this.getCache().each(function(key, cacheEntry) {
							var histNotif = cacheEntry.get('value');
						
							if(histNotif.get('equnr') === equnr)
								equipmentsHistNotifs.add(histNotif);
						}, this);
					}
				
					retval = equipmentsHistNotifs;
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForEquipment of BaseHistNotifCache', ex);
		}
		
		return retval;
	}
});