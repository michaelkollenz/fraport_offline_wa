Ext.define('AssetManagement.base.manager.cache.BaseNotifActivityCache', {
	extend: 'AssetManagement.customer.manager.cache.GradedCache',

	//THIS CACHE WORKS HIERARCHICALLY
	//THAT IS WHY IT HAS TO OVERRIDE NEARLY ALL APIS OF THE BASSE CLASS

	requires: [
	    'AssetManagement.customer.model.bo.NotifActivity'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.NotifActivity',
		onceRequestedNotifs: null,
		onceRequestedNotifItems: null
	},
	
	//protected
	constructor: function(config) {
		this.callParent(arguments);
	
		try {
			this.setOnceRequestedNotifs(Ext.create('Ext.util.HashMap'));
			this.setOnceRequestedNotifItems(Ext.create('Ext.util.HashMap'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNotifActivityCache', ex);
	    }
	},
	
	//protected
	//@override
	clearCache: function() {
		this.callParent(arguments);
		
        try {
        	this.getOnceRequestedNotifs().clear();
        	this.getOnceRequestedNotifItems().clear();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseNotifActivityCache', ex);
		}    
	},
	
	//protected
	//@override
	//also listen to the moving data cache reset event
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseNotifActivityCache', ex);
	    }
	},
	
	//iterates over the cache to determine the lowest present datagrade
	getMinimumDataGrade: function() {
		var retval = this.self.DATAGRADES.BASE;
		
		try {
			var cacheRef = this.getCache();
			
			if(cacheRef.getCount() > 0) {
				var lowestEncountered = this.self.DATAGRADES.FULL;
			
				var breakLoop = false;

				cacheRef.each(function(metaKey, metaCacheEntry) {
					var subHashMap = metaCacheEntry.get('value');
					
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							var entriesDataGrade = cacheEntry.get('dataGrade');
							
							if(entriesDataGrade < lowestEncountered)
								lowestEncountered = entriesDataGrade;
							
							//break the loop, if the lowest possible value has been encountered
							breakLoop = lowestEncountered == retval;
							
							return !breakLoop;
						}, this);
					}
					
					return !breakLoop;
				}, this);

				retval = lowestEncountered;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of BaseNotifActivityCache', ex);
		}
		
		return retval;
	},
	
	//public
	//@override
	//adds a single notifActivity to cache, considering the passed data grade
	//if the notifItem is already cached, it will be checked if it's cache entrie's data grade has to be updated
	addToCache: function(notifActivity, dataGrade) {
		try {
			if(notifActivity) {
				var qmnum = notifActivity.get('qmnum');
				var fenum = notifActivity.get('fenum');
				var key = this.extractKeyFromObject(notifActivity);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)
						&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)
							&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					if(!dataGrade)
						dataGrade = this.self.DATAGRADES.BASE;
						
					var metaKey = qmnum + fenum;
						
					var cachedMetaEntry = this.getCache().get(metaKey);
					var subHashMap = null;

					if(!cachedMetaEntry) {
						subHashMap = Ext.create('Ext.util.HashMap');
					
						cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: metaKey,
							value: subHashMap,
							dataGrade: this.self.DATAGRADES.BASE
						});
						
						this.getCache().add(metaKey, cachedMetaEntry);
					} else {
						subHashMap = cachedMetaEntry.get('value');
					}
					
					var cachedEntry = subHashMap.get(key);

					if(!cachedEntry) {
						cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: key,
							value: notifActivity,
							dataGrade: dataGrade
						});
						
						subHashMap.add(key, cachedEntry);
					}
					
					if(dataGrade > cachedEntry.get('dataGrade')) {
						cachedEntry.set('dataGrade', dataGrade);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseNotifActivityCache', ex);
	    }
	},
	
	//public
	//adds a store of notifActivites to the cache
	//when added this way, the cache may be able to return something on following requests for the same notif
	addToCacheForNotif: function(qmnum, notifsActivities, dataGrade) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && notifsActivities) {
				var onceRequestedNotifs = this.getOnceRequestedNotifs();
			
				if(!dataGrade)
					dataGrade = this.self.DATAGRADES.BASE;
					
				if(!onceRequestedNotifs.containsKey(qmnum) || onceRequestedNotifs.get(qmnum) < dataGrade)
					onceRequestedNotifs.replace(qmnum, dataGrade);
					
				if(notifsActivities.getCount() > 0) {
					notifsActivities.each(function(notifActivity) {
						this.addToCache(notifActivity, dataGrade);
					}, this);
				}
				
				//next update all affected meta entries and the onceRequestedNotifItems map
				if(this.getCache().getCount() > 0) {
					var onceRequestedNotifItems = this.getOnceRequestedNotifItems();
				
					this.getCache().each(function(metaKey, cachedMetaEntry) {
						if(AssetManagement.customer.utils.StringUtils.startsWith(metaKey, qmnum)) {
							if(cachedMetaEntry.get('dataGrade') < dataGrade) {
								cachedMetaEntry.set('dataGrade', dataGrade);
							}
							
							if(onceRequestedNotifItems.get(metaKey) && onceRequestedNotifItems.get(metaKey) < dataGrade) {
								onceRequestedNotifItems.replace(metaKey, dataGrade);
							}
						}
					}, this);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForNotif of BaseNotifActivityCache', ex);
	    }
	},
	
	//public
	//adds a store of notifActivites to the cache
	//when added this way, the cache may be able to return something on following requests for the same notif head
	addToCacheForNotifHead: function(qmnum, notifHeadsActivities, dataGrade) {
		try {
			this.addToCacheForNotifItem(qmnum, '0000', notifHeadsActivities, dataGrade);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForNotifHead of BaseNotifActivityCache', ex);
	    }
	},

	//public
	//adds a store of notifActivites to the cache
	//when added this way, the cache may be able to return something on following requests for the same notifItem
	addToCacheForNotifItem: function(qmnum, fenum, notifItemsActivities, dataGrade) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)
					&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum) && notifItemsActivities) {
				var onceRequestedNotifItems = this.getOnceRequestedNotifItems();
					
				if(!dataGrade)
					dataGrade = this.self.DATAGRADES.BASE;
					
				var metaKey = qmnum + fenum;
				
				if(!onceRequestedNotifItems.containsKey(metaKey) || onceRequestedNotifItems.get(metaKey) < dataGrade)
					onceRequestedNotifItems.replace(metaKey, dataGrade);
					
				//next generate/update meta entry
				var cachedMetaEntry = this.getCache().get(metaKey);
				
				if(!cachedMetaEntry) {
					subHashMap = Ext.create('Ext.util.HashMap');
					
					cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
						key: metaKey,
						value: subHashMap,
						dataGrade: dataGrade
					});
					
					this.getCache().add(metaKey, cachedMetaEntry);
				} else if(cachedMetaEntry.get('dataGrade') < dataGrade) {
					cachedMetaEntry.set('dataGrade', dataGrade);
				}
				
				if(notifItemsActivities.getCount() > 0) {
					notifItemsActivities.each(function(notifActivity) {
						this.addToCache(notifActivity, dataGrade);
					}, this);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForNotifItem of BaseNotifActivityCache', ex);
	    }
	},
	
	//public
	//@override
	//tries to fetch the corresponding cache entry for the passed key at atleast the requested data grade
	getFromCache: function(key, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key) && key.length > 16) {
				var qmnum = key.substring(0, 12);
				var fenum = key.substring(12, 16);
				
				var cachedMetaEntry = this.getCache().get(qmnum + fenum);
				
				if(cachedMetaEntry) {
					subHashMap = cachedMetaEntry.get('value');
					
					var cachedEntry = subHashMap.get(key);
					
					if(cachedEntry) {
						if(!dataGrade)
							dataGrade = this.self.DATAGRADES.BASE;
					
						if(dataGrade <= cachedEntry.get('dataGrade')) {
							retval = cachedEntry.get('value');
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseNotifActivityCache', ex);
	    }
		
		return retval;
	},

	//public
	//@override
	//will return a store of all cached notifActivities, if the cache has been once filled using addStoreForAllToCache
	//and the caches minimum data grade complies with the requested data grade
	getStoreForAll: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
		
			if(this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade()) {
				retval = Ext.create('Ext.data.Store', {
					model: this.getModelClass(),
					autoLoad: false
				});
			
				if(this.getCache().getCount() > 0) {
					this.getCache().each(function(qmnum, metaCacheEntry) {
						var subHashMap = metaCacheEntry.get('value');
						
						if(subHashMap.getCount() > 0) {
							subHashMap.each(function(key, cacheEntry) {
								retval.add(cacheEntry.get('value'));
							}, this);
						}
					}, this);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of BaseNotifActivityCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	getAllUpToGrade: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
				
			retval = Ext.create('Ext.data.Store', {
				model: this.getModelClass(),
				autoLoad: false
			});
			
			if(this.getCache().getCount() > 0) {
				this.getCache().each(function(qmnum, metaCacheEntry) {
					var subHashMap = metaCacheEntry.get('value');
				
					//check if the meta entry in not empty
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							if(cacheEntry.get('dataGrade') <= dataGrade)
								retval.add(cacheEntry.get('value'));
						}, this);
					}
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of BaseNotifActivityCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//tries to get all notifActivities for a notif from cache
	//will return notif all notifActivities, only if they have been added via addToCacheForNotif once
	getFromCacheForNotif: function(qmnum, dataGrade) {
		var retval = null;
	
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var onceRequestedNotifs = this.getOnceRequestedNotifs();
				
				var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
				var coveredBySpecific = onceRequestedNotifs.containsKey(qmnum) && dataGrade <= onceRequestedNotifs.get(qmnum);
				
				if(coveredByAll || coveredBySpecific) {
					retval = this.extractFromCacheForNotif(qmnum);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForNotif of BaseNotifActivityCache', ex);
		}
		
		return retval;
	},
	
	//public
	//tries to get notifActivities for a notif head from cache
	//will return notif head's notifActivities, only if they have been added via addToCacheForNotifHead once
	getFromCacheForNotifHead: function(qmnum, dataGrade) {
		var retval = null;
	
		try {
			retval = getFromCacheForNotifItem.get(qmnum, '0000', dataGrade);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForNotifHead of BaseNotifActivityCache', ex);
		}
		
		return retval;
	},
	
	//public
	//tries to get notifActivities for a notifItem from cache
	//will return notifItem's notifActivities, only if they have been added via addToCacheForNotifItem once
	getFromCacheForNotifItem: function(qmnum, fenum, dataGrade) {
		var retval = null;
	
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
				var onceRequestedNotifItems = this.getOnceRequestedNotifItems();
				var metaKey = qmnum + fenum;
				
				var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
				var coveredBySpecific = onceRequestedNotifItems.containsKey(metaKey) && dataGrade <= onceRequestedNotifItems.get(metaKey);
				
				if(coveredByAll || coveredBySpecific) {
				    retval = this.extractFromCacheForNotifItem(qmnum, fenum);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForNotifItem of BaseNotifActivityCache', ex);
		}
		
		return retval;
	},
	
	//public
	//combines logic from getFromCacheForNotif and getAllUpToGrade
	getAllUpToGradeForNotif: function(qmnum, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var onceRequestedNotifs = this.getOnceRequestedNotifs();
				
				var coveredByAll = this.getCacheComplete();
				var coveredBySpecific = onceRequestedNotifs.containsKey(qmnum);
				
				if(coveredByAll || coveredBySpecific) {
					var notifsActivities = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
				
					this.getCache().each(function(key, cachedMetaEntry) {
						if(AssetManagement.customer.utils.StringUtils.startsWith(key, qmnum)) {
							var subHashMap = cachedMetaEntry.get('value');
							
							//check if the meta entry in not empty
							if(subHashMap.getCount() > 0) {
								subHashMap.each(function(key, cacheEntry) {
									if(cacheEntry.get('dataGrade') <= dataGrade)
										notifsActivities.add(cacheEntry.get('value'));
								}, this);
							}
						}
					}, this);
					
					retval = notifsActivities;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForNotif of BaseNotifActivityCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//combines logic from getFromCacheForNotifHead and getAllUpToGrade
	getAllUpToGradeForNotifHead: function(qmnum, dataGrade) {
		var retval = null;
		
		try {
			retval = this.getAllUpToGradeForNotifItem(qmnum, '0000', dataGrade);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForNotifHead of BaseNotifActivityCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//combines logic from getFromCacheForNotifItem and getAllUpToGrade
	getAllUpToGradeForNotifItem: function(qmnum, fenum, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
				var onceRequestedNotifsItems = this.getOnceRequestedNotifItems();
				var metaKey = qmnum + fenum;
				
				var coveredByAll = this.getCacheComplete();
				var coveredBySpecific = onceRequestedNotifsItems.containsKey(metaKey);
				
				if(coveredByAll || coveredBySpecific) {
					var cachedMetaEntry = this.getCache().get(metaKey);
					
					if(cachedMetaEntry) {
						var subHashMap = cachedMetaEntry.get('value');
						
						var notifsActivities = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.NotifActivity',
							autoLoad: false
						});
						
						//check if the meta entry in not empty
						if(subHashMap.getCount() > 0) {
							subHashMap.each(function(key, cacheEntry) {
								if(cacheEntry.get('dataGrade') <= dataGrade)
									notifsActivities.add(cacheEntry.get('value'));
							}, this);
						}
					
						retval = notifsActivities;
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForNotifItem of BaseNotifActivityCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//removes a specific notifActivity from cache
	removeFromCache: function(notifActivity) {
		try {
			if(notifActivity) {
				var qmnum = notifActivity.get('qmnum');
				var fenum = notifActivity.get('fenum');
				var key = this.extractKeyFromObject(notifActivity);
			
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)
						&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)
							&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					var cachedMetaEntry = this.getCache().get(qmnum + fenum);
					
					if(cachedMetaEntry) {
						var subHashMap = cachedMetaEntry.get('value');
						
						subHashMap.removeAtKey(key);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of BaseNotifActivityCache', ex);
	    }
	},
	
	//private
	extractFromCacheForNotif: function(qmnum) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var notifsActivities = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.NotifActivity',
					autoLoad: false
				});
			
				if(this.getCache().getCount() > 0) {
					this.getCache().each(function(key, cachedMetaEntry) {
						if(AssetManagement.customer.utils.StringUtils.startsWith(key, qmnum)) {
							var subHashMap = cachedMetaEntry.get('value');
							
							//check if the meta entry in not empty
							if(subHashMap.getCount() > 0) {
								subHashMap.each(function(key, cacheEntry) {
									notifsActivities.add(cacheEntry.get('value'));
								}, this);
							}
						}
					}, this);
				}
				
				retval = notifsActivities;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractFromCacheForNotif of BaseNotifActivityCache', ex);
		}
		
		return retval;
	},
	
	//private
	extractFromCacheForNotifHead: function(qmnum) {
		var retval = null;
		
		try {
			retval = this.extractFromCacheForNotifItem(qmnum, '0000');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractFromCacheForNotifHead of BaseNotifActivityCache', ex);
		}
		
		return retval;
	},
	
	//private
	extractFromCacheForNotifItem: function(qmnum, fenum) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
				var cachedMetaEntry = this.getCache().get(qmnum + fenum);
			
				if(cachedMetaEntry) {
					var subHashMap = cachedMetaEntry.get('value');
					
					var notifItemsActivities = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
					
					//check if the meta entry in not empty
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							notifItemsActivities.add(cacheEntry.get('value'));
						}, this);
					}
			
					retval = notifItemsActivities;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractFromCacheForNotifItem of BaseNotifActivityCache', ex);
		}
		
		return retval;
	}
});