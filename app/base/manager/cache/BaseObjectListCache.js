﻿Ext.define('AssetManagement.base.manager.cache.BaseObjectListCache', {
    extend: 'AssetManagement.customer.manager.cache.GradedCache',

    //THIS CACHE WORKS HIERARCHICALLY
    //THAT IS WHY IT HAS TO OVERRIDE NEARLY ALL APIS OF THE BASSE CLASS

    requires: [
	    'AssetManagement.customer.model.bo.ObjectListItem'
    ],

    config: {
        //private
        modelClass: 'AssetManagement.customer.model.bo.ObjectListItem',
        onceRequestedOrders: null
    },

    //protected
    constructor: function (config) {
        this.callParent(arguments);

        try {
            this.setOnceRequestedOrders(Ext.create('Ext.util.HashMap'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseObjectListCache', ex);
        }
    },

    //protected
    //@override
    clearCache: function () {
        this.callParent(arguments);

        try {
            this.getOnceRequestedOrders().clear();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseObjectListCache', ex);
        }
    },

    //protected
    //@override
    //also listen to the moving data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseObjectListCache', ex);
        }
    },

    //iterates over the cache to determine the lowest present datagrade
    getMinimumDataGrade: function () {
        var retval = this.self.DATAGRADES.BASE;

        try {
            var cacheRef = this.getCache();

            if (cacheRef.getCount() > 0) {
                var lowestEncountered = this.self.DATAGRADES.FULL;

                var breakLoop = false;

                cacheRef.each(function (metaKey, metaCacheEntry) {
                    var subHashMap = metaCacheEntry.get('value');

                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            var entriesDataGrade = cacheEntry.get('dataGrade');

                            if (entriesDataGrade < lowestEncountered)
                                lowestEncountered = entriesDataGrade;

                            //break the loop, if the lowest possible value has been encountered
                            breakLoop = lowestEncountered == retval;

                            return !breakLoop;
                        }, this);
                    }

                    return !breakLoop;
                }, this);

                retval = lowestEncountered;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of BaseObjectListCache', ex);
        }

        return retval;
    },

    //public
    //@override
    //adds a single objectList to cache, considering the passed data grade
    //if the objectList is already cached, it will be checked if it's cache entrie's data grade has to be updated
    addToCache: function (objectList, dataGrade) {
        try {
            if (objectList) {
                var aufnr = objectList.get('aufnr');
                var key = this.extractKeyFromObject(objectList);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    if (!dataGrade)
                        dataGrade = this.self.DATAGRADES.BASE;

                    var cachedMetaEntry = this.getCache().get(aufnr);
                    var subHashMap = null;

                    if (!cachedMetaEntry) {
                        subHashMap = Ext.create('Ext.util.HashMap');

                        cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                            key: aufnr,
                            value: subHashMap,
                            dataGrade: this.self.DATAGRADES.BASE
                        });

                        this.getCache().add(aufnr, cachedMetaEntry);
                    } else {
                        subHashMap = cachedMetaEntry.get('value');
                    }

                    var cachedEntry = subHashMap.get(key);

                    if (!cachedEntry) {
                        cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                            key: key,
                            value: objectList,
                            dataGrade: dataGrade
                        });

                        subHashMap.add(key, cachedEntry);
                    }

                    if (dataGrade > cachedEntry.get('dataGrade')) {
                        cachedEntry.set('dataGrade', dataGrade);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseObjectListCache', ex);
        }
    },

    //public
    //adds a store of objectLists to the cache
    //when added this way, the cache may be able to return something on following requests for the same order 
    addToCacheForOrder: function (aufnr, ordersObjects, dataGrade) {
        try {
            var onceRequestedOrders = this.getOnceRequestedOrders();

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && ordersObjects) {
                if (!dataGrade)
                    dataGrade = this.self.DATAGRADES.BASE;

                if (!onceRequestedOrders.containsKey(aufnr) || onceRequestedOrders.get(aufnr) < dataGrade)
                    onceRequestedOrders.replace(aufnr, dataGrade);

                //next generate/update meta entry
                var cachedMetaEntry = this.getCache().get(aufnr);

                if (!cachedMetaEntry) {
                    subHashMap = Ext.create('Ext.util.HashMap');

                    cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                        key: aufnr,
                        value: subHashMap,
                        dataGrade: dataGrade
                    });

                    this.getCache().add(aufnr, cachedMetaEntry);
                } else if (cachedMetaEntry.get('dataGrade') < dataGrade) {
                    cachedMetaEntry.set('dataGrade', dataGrade);
                }

                if (ordersObjects.getCount() > 0) {
                    ordersObjects.each(function (objectList) {
                        this.addToCache(objectList, dataGrade);
                    }, this);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForOrder of BaseObjectListCache', ex);
        }
    },

    //public
    //@override
    //tries to fetch the corresponding cache entry for the passed key at atleast the requested data grade
    getFromCache: function (key, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key) && key.length > 12) {
                var aufnr = key.substring(0, 12);

                var cachedMetaEntry = this.getCache().get(aufnr);

                if (cachedMetaEntry) {
                    subHashMap = cachedMetaEntry.get('value');

                    var cachedEntry = subHashMap.get(key);

                    if (cachedEntry) {
                        if (!dataGrade)
                            dataGrade = this.self.DATAGRADES.BASE;

                        if (dataGrade <= cachedEntry.get('dataGrade')) {
                            retval = cachedEntry.get('value');
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseObjectListCache', ex);
        }

        return retval;
    },

    //public
    //@override
    //will return a store of all cached objects, if the cache has been once filled using addStoreForAllToCache
    //and the caches minimum data grade complies with the requested data grade
    getStoreForAll: function (dataGrade) {
        var retval = null;

        try {
            if (!dataGrade)
                dataGrade = this.self.DATAGRADES.BASE;

            if (this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade()) {
                retval = Ext.create('Ext.data.Store', {
                    model: this.getModelClass(),
                    autoLoad: false
                });

                if (this.getCache().getCount() > 0) {
                    this.getCache().each(function (aufnr, metaCacheEntry) {
                        var subHashMap = metaCacheEntry.get('value');

                        if (subHashMap.getCount() > 0) {
                            subHashMap.each(function (key, cacheEntry) {
                                retval.add(cacheEntry.get('value'));
                            }, this);
                        }
                    }, this);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of BaseObjectListCache', ex);
            retval = null;
        }

        return retval;
    },

    getAllUpToGrade: function (dataGrade) {
        var retval = null;

        try {
            if (!dataGrade)
                dataGrade = this.self.DATAGRADES.BASE;

            retval = Ext.create('Ext.data.Store', {
                model: this.getModelClass(),
                autoLoad: false
            });

            if (this.getCache().getCount() > 0) {
                this.getCache().each(function (aufnr, metaCacheEntry) {
                    var subHashMap = metaCacheEntry.get('value');

                    //check if the meta entry in not empty
                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            if (cacheEntry.get('dataGrade') <= dataGrade)
                                retval.add(cacheEntry.get('value'));
                        }, this);
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of BaseObjectListCache', ex);
            retval = null;
        }

        return retval;
    },

    //public
    //tries to get objectLists for a order from cache
    //will return order's objects, only if they have been added via addToCacheForOrder once
    getFromCacheForOrder: function (aufnr, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var onceRequestedOrders = this.getOnceRequestedOrders();

                var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
                var coveredBySpecific = onceRequestedOrders.containsKey(aufnr) && dataGrade <= onceRequestedOrders.get(aufnr);

                if (coveredByAll || coveredBySpecific) {
                    retval = this.extractFromCacheForOrder(aufnr);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForOrder of BaseObjectListCache', ex);
        }

        return retval;
    },

    //public
    //combines logic from getFromCacheForOrder and getAllUpToGrade
    getAllUpToGradeForOrder: function (aufnr, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var onceRequestedOrders = this.getOnceRequestedOrders();

                var coveredByAll = this.getCacheComplete();
                var coveredBySpecific = onceRequestedOrders.containsKey(aufnr);

                if (coveredByAll || coveredBySpecific) {
                    var cachedMetaEntry = this.getCache().get(aufnr);

                    if (cachedMetaEntry) {
                        var subHashMap = cachedMetaEntry.get('value');

                        var ordersObjects = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.ObjectListItem',
                            autoLoad: false
                        });

                        //check if the meta entry in not empty
                        if (subHashMap.getCount() > 0) {
                            subHashMap.each(function (key, cacheEntry) {
                                if (cacheEntry.get('dataGrade') <= dataGrade)
                                    ordersObjects.add(cacheEntry.get('value'));
                            }, this);
                        }

                        retval = ordersObjects;
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForOrder of BaseObjectListCache', ex);
            retval = null;
        }

        return retval;
    },

    //public
    //removes a specific objectList from cache
    removeFromCache: function (objectList) {
        try {
            if (objectList) {
                var aufnr = objectList.get('aufnr');
                var key = this.extractKeyFromObject(objectList);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    var cachedMetaEntry = this.getCache().get(aufnr);

                    if (cachedMetaEntry) {
                        var subHashMap = cachedMetaEntry.get('value');

                        subHashMap.removeAtKey(key);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of BaseObjectListCache', ex);
        }
    },

    //private
    extractFromCacheForOrder: function (aufnr) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var cachedMetaEntry = this.getCache().get(aufnr);

                if (cachedMetaEntry) {
                    var subHashMap = cachedMetaEntry.get('value');

                    var ordersObjects = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.ObjectListItem',
                        autoLoad: false
                    });

                    //check if the meta entry in not empty
                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            ordersObjects.add(cacheEntry.get('value'));
                        }, this);
                    }

                    retval = ordersObjects;
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForOrder of BaseObjectListCache', ex);
        }

        return retval;
    }
});