Ext.define('AssetManagement.base.manager.cache.BaseCache', {
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.controller.EventController',
	    'AssetManagement.customer.manager.cache.CacheEntry',
	    'Ext.util.HashMap',
	    'Ext.data.Store',
	    'AssetManagement.customer.helper.OxLogger'
	],

	config: {
		//protected
		modelClass: '',						//has to be provided by derivates
		keyProperties: null,				//array of attributes to retrieve cache key from object
		
		//private
		useSpecialKeyPath: false,
		cache: null,
		cacheComplete: false
	},
	
	inheritableStatics: {
		_instance: null,
	
	    getInstance: function() {
	        try {
	        	if(!this._instance)
	        		this._instance = Ext.create(Ext.getClassName(this));
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of ' + this.getClassName(), ex);
		    }
				
	        return this._instance;
	    },

	    getClassName: function() {
	    	var retval = 'BaseCache';
	    
	    	try {
	    		var className = Ext.getClassName(this);
	    		
	    		if(AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
	    			var splitted = className.split('.');
	    			retval = splitted[splitted.length - 1];
	    		} else {
	    			retval = className;
	    		}
	    	} catch(ex) {
	    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseCache', ex);
	    	}
	    	
	    	return retval;
	    }
	},
	
	//protected
	constructor: function(config) {
		try {
			this.initConfig(config);
			
			if(config && config.keyProperties && config.keyProperties.length > 0)
				this.setUseSpecialKeyPath(true);
			
			this.setCache(Ext.create('Ext.util.HashMap'));
			this.setupDataEventListeners();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of ' + this.self.getClassName(), ex);
	    }
	},
	
	//protected
	//registers listener on global cache reset event - use this point to also listen to specific cache reset events in derivates
	//also this is the point where to register listeners on data events for cached data type's dependend data
	//this is only necessary if cached data type's dependend data may have copies - try to ensure that this is not the case
	setupDataEventListeners: function() {
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of ' + this.self.getClassName(), ex);
	    }
	},
	
	//protected
	//clears the cache and resets internal flags
	clearCache: function() {
        try {
		    this.getCache().clear();
		    this.setCacheComplete(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of ' + this.self.getClassName(), ex);
		}    
	},
	
	//protected
	//extracts the key from a object to store in the cache
	//if a keyPath has been provided, it will be used to combine the key, else the id property of the model will be used
	extractKeyFromObject: function(toGetKeyFrom) {
		var retval = '';
	
		try {
			if(toGetKeyFrom) {
				if(this.getUseSpecialKeyPath()) {
					retval = '';
				
					Ext.Array.each(this.getKeyProperties(), function(attribute) {
						retval += toGetKeyFrom.get(attribute);
					}, this);
				} else {
					retval = toGetKeyFrom.get('id');
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractKeyFromObject of ' + this.self.getClassName(), ex);
			retval = '';
		}
		
		return retval;
	},
	
	//public
	//adds an object to cache, if the object is not already stored in the cache
	addToCache: function(toAdd) {
		try {
			if(toAdd) {
				var key = this.extractKeyFromObject(toAdd);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					var cachedEntry = this.getCache().get(key);
					
					if(!cachedEntry) {
						cachedEntry = Ext.create('AssetManagement.customer.manager.cache.CacheEntry', {
							key: key,
							value: toAdd
						});
						
						this.getCache().add(key, cachedEntry);
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of ' + this.self.getClassName(), ex);
	    }
	},
	
	//public
	//fills the cache using the passed store
	//sets the caches complete flag to true
	addStoreForAllToCache: function(store) {
		try {
			if(store) {
				//only fill cache from the store, if the cache has not been filled completely yet
				if(!this.getCacheComplete()) {
					if(store.getCount() > 0) {
						store.each(function(item) {
						    this.addToCache(item);
						}, this);
					}
					
					this.setCacheComplete(true);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addStoreForAllToCache of ' + this.self.getClassName(), ex);
	    }
	},
	
	//public
	//tries to fetch the corresponding cache entry for the passed key
	getFromCache: function(key) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
				var cachedEntry = this.getCache().get(key);
				
				if(cachedEntry) {
					retval = cachedEntry.get('value');
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of ' + this.self.getClassName(), ex);
	    }
		
		return retval;
	},

	//public
	//will return a store of all cached objects, if the cache has been once filled using addStoreForAllToCache
	getStoreForAll: function() {
		var retval = null;
		
		try {
			if(this.getCacheComplete()) {
				retval = Ext.create('Ext.data.Store', {
					model: this.getModelClass(),
					autoLoad: false
				});
			
				if(this.getCache().getCount() > 0) {
					this.getCache().each(function(key, cacheEntry) {
						retval.add(cacheEntry.get('value'));
					}, this);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of ' + this.self.getClassName(), ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//removes a specific object from cache
	removeFromCache: function(objectToRemove) {
		try {
			if(objectToRemove) {
				var key = this.extractKeyFromObject(objectToRemove);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					this.getCache().removeAtKey(key);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of ' + this.self.getClassName(), ex);
	    }
	}
});