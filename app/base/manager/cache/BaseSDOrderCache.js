Ext.define('AssetManagement.base.manager.cache.BaseSDOrderCache', {
	extend: 'AssetManagement.customer.manager.cache.GradedCache',

	requires: [
	    'AssetManagement.customer.model.bo.SDOrder',
	    'AssetManagement.customer.model.bo.SDOrderItem',
	    'Ext.data.Store'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.SDOrder'
	},
	
	//protected
	//@override
	//also listen to the moving data cache reset event
	//also listen to sdOrderItem data changed events
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderItemManager.EVENTS.SDORDER_ITEM_ADDED, this.onSDOrderItemAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderItemManager.EVENTS.SDORDER_ITEM_CHANGED, this.onSDOrderItemChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderItemManager.EVENTS.SDORDER_ITEM_DELETED, this.onSDOrderItemDeleted, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseSDOrderCache', ex);
	    }
	},
	
	//handles case, a new sdOrderItem has to be included into an affected cache entrie's dependend data
	onSDOrderItemAdded: function(sdOrderItem) {
		try {
			if(sdOrderItem) {
				var affectedSDOrder = this.getFromCache(sdOrderItem.get('vbeln'));
				
				if(affectedSDOrder) {
					var sdItems = affectedSDOrder.get('items');
					
					if(!sdItems) {
						sdItems = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.SDOrderItem',
							autoLoad: false
						});
						
						affectedSDOrder.set('items', sdItems);
					} 

					var indexOfAffectedSDItem = sdItems.indexOfId(sdOrderItem.get('id'));
						
					if(indexOfAffectedSDItem >= 0) {
						var affectedSDItem = sdItems.getAt(indexOfAffectedSDItem);
						
						if(affectedSDItem !== sdOrderItem) {
							sdItems.removeAt(indexOfAffectedSDItem);
							sdItems.insert(indexOfAffectedSDItem, sdOrderItem);
						}
					} else {
						sdItems.add(sdOrderItem);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemAdded of BaseSDOrderCache', ex);
		} 
	},
	
	//handles case, an sdOrderItem's data changed on a duplicate sdOrderItem object
	onSDOrderItemChanged: function(sdOrderItem) {
		try {
			//case is covered by logic of onSDOrderItemAdded - the affected sdOrderItem will be replaced by the duplicate
			this.onSDOrderItemAdded(sdOrderItem);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemChanged of BaseSDOrderCache', ex);
		} 
	},
	
	//handles case, an sdOrderItem has not yet been deleted from an affected cache entrie's dependend data
	onSDOrderItemDeleted: function(sdOrderItem) {
		try {
			if(sdOrderItem) {
				var affectedSDOrder = this.getFromCache(sdOrderItem.get('vbeln'));
				
				if(affectedSDOrder) {
					var sdItems = affectedSDOrder.get('items');
					
					if(sdItems) {
						var indexOfAffectedSDItem = sdItems.indexOfId(sdOrderItem.get('id'));
						
						if(indexOfAffectedSDItem >= 0) {
							sdItems.removeAt(indexOfAffectedSDItem);
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemDeleted of BaseSDOrderCache', ex);
		} 
	}
});