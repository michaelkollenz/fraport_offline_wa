Ext.define('AssetManagement.base.manager.cache.BaseGradedCache', {
	extend: 'AssetManagement.customer.manager.cache.Cache',

	requires: [
	    'AssetManagement.customer.manager.cache.GradedCacheEntry'
	],

	config: {
		//private
		minimumDataGrade: 0			//the minimum data grade garantueed to all cached entries
	},
	
	inheritableStatics: {
		//default datagrades, may be restricted or extended by derivates
		DATAGRADES: {
			BASE: 0,
			LOW: 1,
			MEDIUM: 2,
			FULL: 3
		},
		
		getClassName: function() {
	    	var retval = 'BaseGradedCache';
	    
	    	try {
	    		var className = Ext.getClassName(this);
	    		
	    		if(AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
	    			var splitted = className.split('.');
	    			retval = splitted[splitted.length - 1];
	    		} else {
	    			retval = className;
	    		}
	    	} catch(ex) {
	    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseGradedCache', ex);
	    	}
	    	
	    	return retval;
	    }
	},
	
	//iterates over the cache to determine the lowest present datagrade
	getMinimumDataGrade: function() {
		var retval = this.self.DATAGRADES.BASE;
		
		try {
			var cacheRef = this.getCache();
			
			if(cacheRef.getCount() > 0) {
				var lowestEncountered = this.self.DATAGRADES.FULL;
			
				cacheRef.each(function(key, cacheEntry) {
					var entriesDataGrade = cacheEntry.get('dataGrade');
				
					if(entriesDataGrade < lowestEncountered)
						lowestEncountered = entriesDataGrade;
					
					//break the loop, if the lowest possible value has been encountered
					if(lowestEncountered == retval)
						return false;
				}, this);

				retval = lowestEncountered;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of ' + this.self.getClassName(), ex);
		}
		
		return retval;
	},
	
	//public
	//@override
	//adds an object to cache, considering the passed data grade
	//if the object is already cached, it will be checked if it's cache entrie's data grade has to be updated
	addToCache: function(toAdd, dataGrade) {
		try {
			if(toAdd) {
				var key = this.extractKeyFromObject(toAdd);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					var cachedEntry = this.getCache().get(key);
					
					if(!dataGrade)
						dataGrade = this.self.DATAGRADES.BASE;
					
					if(!cachedEntry) {
						cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: key,
							value: toAdd,
							dataGrade: dataGrade
						});
						
						this.getCache().add(key, cachedEntry);
					}
					
					if(dataGrade > cachedEntry.get('dataGrade')) {
						cachedEntry.set('dataGrade', dataGrade);
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of ' + this.self.getClassName(), ex);
	    }
	},
	
	//public
	//@override
	//fills the cache using the passed store
	//sets the caches complete flag to true and the minimum data grade to datagrade
	addStoreForAllToCache: function(store, dataGrade) {
		try {
			if(store) {
				if(!dataGrade)
					dataGrade = this.self.DATAGRADES.BASE;
				
				//only fill cache from the store, if the cache has neither been filled completely
				//or if the passed dataGrade is higher than the minimum data grade of the cache
				if(!this.getCacheComplete() || this.getMinimumDataGrade() < dataGrade) {
					if(store.getCount() > 0) {
						store.each(function(record) {
							this.addToCache(record, dataGrade);
						}, this);
					}
					
					this.setCacheComplete(true);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addStoreForAllToCache of ' + this.self.getClassName(), ex);
	    }
	},
	
	//public
	//@override
	//tries to fetch the corresponding cache entry for the passed key at at least the requested data grade
	getFromCache: function(key, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
				var cachedEntry = this.getCache().get(key);
				
				if(cachedEntry) {
					if(!dataGrade)
						dataGrade = this.self.DATAGRADES.BASE;
						
					if(dataGrade <= cachedEntry.get('dataGrade')) {
						retval = cachedEntry.get('value');
					}
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of ' + this.self.getClassName(), ex);
	    }
		
		return retval;
	},

	//public
	//@override
	//will return a store of all cached objects, if the cache has been once filled using addStoreForAllToCache
	//and the caches minimum data grade complies with the requested data grade
	getStoreForAll: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
		
			if(this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade()) {
				retval = Ext.create('Ext.data.Store', {
					model: this.getModelClass(),
					autoLoad: false
				});
				
				if(this.getCache().getCount() > 0) {
					this.getCache().each(function(key, cacheEntry) {
						retval.add(cacheEntry.get('value'));
					}, this);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of ' + this.self.getClassName(), ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	getAllUpToGrade: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
				
			retval = Ext.create('Ext.data.Store', {
				model: this.getModelClass(),
				autoLoad: false
			});
				
			if(this.getCache().getCount() > 0) {
				this.getCache().each(function(key, cacheEntry) {
					if(cacheEntry.get('dataGrade') <= dataGrade)
						retval.add(cacheEntry.get('value'));
				}, this);
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of ' + this.self.getClassName(), ex);
		    retval = null;
	    }
		
		return retval;
	}
});