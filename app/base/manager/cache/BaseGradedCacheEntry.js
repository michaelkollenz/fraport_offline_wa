Ext.define('AssetManagement.base.manager.cache.BaseGradedCacheEntry', {
    extend: 'AssetManagement.customer.manager.cache.CacheEntry',
 
    //additional field for data grade
    fields: [
       { name: 'dataGrade', type: 'int' }
    ]
});