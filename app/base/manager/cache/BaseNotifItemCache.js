Ext.define('AssetManagement.base.manager.cache.BaseNotifItemCache', {
	extend: 'AssetManagement.customer.manager.cache.GradedCache',
	
	//THIS CACHE WORKS HIERARCHICALLY
	//THAT IS WHY IT HAS TO OVERRIDE NEARLY ALL APIS OF THE BASSE CLASS

	requires: [
	    'AssetManagement.customer.model.bo.NotifItem'
	],

	config: {
		//private
		modelClass: 'AssetManagement.customer.model.bo.NotifItem',
		onceRequestedNotifs: null
	},
	
	//protected
	constructor: function(config) {
		this.callParent(arguments);
	
		try {
			this.setOnceRequestedNotifs(Ext.create('Ext.util.HashMap'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNotifItemCache', ex);
	    }
	},
	
	//protected
	//@override
	clearCache: function() {
		this.callParent(arguments);
		
        try {
        	this.getOnceRequestedNotifs().clear();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseNotifItemCache', ex);
		}    
	},
	
	//protected
	//@override
	//also listen to the moving data cache reset event
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseNotifItemCache', ex);
	    }
	},
	
	//iterates over the cache to determine the lowest present datagrade
	getMinimumDataGrade: function() {
		var retval = this.self.DATAGRADES.BASE;
		
		try {
			var cacheRef = this.getCache();
			
			if(cacheRef.getCount() > 0) {
				var lowestEncountered = this.self.DATAGRADES.FULL;
			
				var breakLoop = false;

				cacheRef.each(function(metaKey, metaCacheEntry) {
					var subHashMap = metaCacheEntry.get('value');
					
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							var entriesDataGrade = cacheEntry.get('dataGrade');
							
							if(entriesDataGrade < lowestEncountered)
								lowestEncountered = entriesDataGrade;
							
							//break the loop, if the lowest possible value has been encountered
							breakLoop = lowestEncountered == retval;
							
							return !breakLoop;
						}, this);
					}
					
					return !breakLoop;
				}, this);

				retval = lowestEncountered;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of BaseNotifItemCache', ex);
		}
		
		return retval;
	},
	
	//public
	//@override
	//adds a single notifItem to cache, considering the passed data grade
	//if the notifItem is already cached, it will be checked if it's cache entrie's data grade has to be updated
	addToCache: function(notifItem, dataGrade) {
		try {
			if(notifItem) {
				var qmnum = notifItem.get('qmnum');
				var key = this.extractKeyFromObject(notifItem);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					if(!dataGrade)
						dataGrade = this.self.DATAGRADES.BASE;
						
					var cachedMetaEntry = this.getCache().get(qmnum);
					var subHashMap = null;

					if(!cachedMetaEntry) {
						subHashMap = Ext.create('Ext.util.HashMap');
					
						cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: qmnum,
							value: subHashMap,
							dataGrade: this.self.DATAGRADES.BASE
						});
						
						this.getCache().add(qmnum, cachedMetaEntry);
					} else {
						subHashMap = cachedMetaEntry.get('value');
					}
					
					var cachedEntry = subHashMap.get(key);

					if(!cachedEntry) {
						cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
							key: key,
							value: notifItem,
							dataGrade: dataGrade
						});
						
						subHashMap.add(key, cachedEntry);
					}
					
					if(dataGrade > cachedEntry.get('dataGrade')) {
						cachedEntry.set('dataGrade', dataGrade);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseNotifItemCache', ex);
	    }
	},
	
	//public
	//adds a store of notifItems to the cache
	//when added this way, the cache may be able to return something on following requests for the same notif 
	addToCacheForNotif: function(qmnum, notifsNotifItems, dataGrade) {
		try {
			var onceRequestedNotifs = this.getOnceRequestedNotifs();
		
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && notifsNotifItems) {
				if(!dataGrade)
					dataGrade = this.self.DATAGRADES.BASE;
				
				if(!onceRequestedNotifs.containsKey(qmnum) || onceRequestedNotifs.get(qmnum) < dataGrade)
					onceRequestedNotifs.replace(qmnum, dataGrade);
					
				//next generate/update meta entry
				var cachedMetaEntry = this.getCache().get(qmnum);
				
				if(!cachedMetaEntry) {
					subHashMap = Ext.create('Ext.util.HashMap');
					
					cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
						key: qmnum,
						value: subHashMap,
						dataGrade: dataGrade
					});
					
					this.getCache().add(qmnum, cachedMetaEntry);
				} else if(cachedMetaEntry.get('dataGrade') < dataGrade) {
					cachedMetaEntry.set('dataGrade', dataGrade);
				}
				
				if(notifsNotifItems.getCount() > 0) {
					notifsNotifItems.each(function(notifItem) {
						this.addToCache(notifItem, dataGrade);
					}, this);
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForNotif of BaseNotifItemCache', ex);
	    }
	},
	
	//public
	//@override
	//tries to fetch the corresponding cache entry for the passed key at atleast the requested data grade
	getFromCache: function(key, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key) && key.length > 12) {
				var qmnum = key.substring(0, 12);
				
				var cachedMetaEntry = this.getCache().get(qmnum);
				
				if(cachedMetaEntry) {
					subHashMap = cachedMetaEntry.get('value');
					
					var cachedEntry = subHashMap.get(key);
					
					if(cachedEntry) {
						if(!dataGrade)
							dataGrade = this.self.DATAGRADES.BASE;
					
						if(dataGrade <= cachedEntry.get('dataGrade')) {
							retval = cachedEntry.get('value');
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseNotifItemCache', ex);
	    }
		
		return retval;
	},

	//public
	//@override
	//will return a store of all cached notifItems, if the cache has been once filled using addStoreForAllToCache
	//and the caches minimum data grade complies with the requested data grade
	getStoreForAll: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
		
			if(this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade()) {
				retval = Ext.create('Ext.data.Store', {
					model: this.getModelClass(),
					autoLoad: false
				});
			
				if(this.getCache().getCount() > 0) {
					this.getCache().each(function(qmnum, metaCacheEntry) {
						var subHashMap = metaCacheEntry.get('value');
						
						if(subHashMap.getCount() > 0) {
							subHashMap.each(function(key, cacheEntry) {
								retval.add(cacheEntry.get('value'));
							}, this);
						}
					}, this);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of BaseNotifItemCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	getAllUpToGrade: function(dataGrade) {
		var retval = null;
		
		try {
			if(!dataGrade)
				dataGrade = this.self.DATAGRADES.BASE;
				
			retval = Ext.create('Ext.data.Store', {
				model: this.getModelClass(),
				autoLoad: false
			});
			
			if(this.getCache().getCount() > 0) {
				this.getCache().each(function(qmnum, metaCacheEntry) {
					var subHashMap = metaCacheEntry.get('value');
				
					//check if the meta entry in not empty
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							if(cacheEntry.get('dataGrade') <= dataGrade)
								retval.add(cacheEntry.get('value'));
						}, this);
					}
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of BaseNotifItemCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//tries to get notifItems for a notif from cache
	//will return notif's notifItems, only if they have been added via addToCacheForNotif once
	getFromCacheForNotif: function(qmnum, dataGrade) {
		var retval = null;
	
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var onceRequestedNotifs = this.getOnceRequestedNotifs();
				
				var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
				var coveredBySpecific = onceRequestedNotifs.containsKey(qmnum) && dataGrade <= onceRequestedNotifs.get(qmnum);
				
				if(coveredByAll || coveredBySpecific) {
					retval = this.extractFromCacheForNotif(qmnum);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForNotif of BaseNotifItemCache', ex);
		}
		
		return retval;
	},
	
	//public
	//combines logic from getFromCacheForNotif and getAllUpToGrade
	getAllUpToGradeForNotif: function(qmnum, dataGrade) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var onceRequestedNotifs = this.getOnceRequestedNotifs();
				
				var coveredByAll = this.getCacheComplete();
				var coveredBySpecific = onceRequestedNotifs.containsKey(qmnum);
				
				if(coveredByAll || coveredBySpecific) {
					var cachedMetaEntry = this.getCache().get(qmnum);
					
					if(cachedMetaEntry) {
						var subHashMap = cachedMetaEntry.get('value');
						
						var notifItems = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.NotifItem',
							autoLoad: false
						});
						
						//check if the meta entry in not empty
						if(subHashMap.getCount() > 0) {
							subHashMap.each(function(key, cacheEntry) {
								if(cacheEntry.get('dataGrade') <= dataGrade)
                                    notifItems.add(cacheEntry.get('value'));
							}, this);
						}
					
						retval = notifItems;
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForNotif of BaseNotifItemCache', ex);
		    retval = null;
	    }
		
		return retval;
	},
	
	//public
	//removes a specific notifItem from cache
	removeFromCache: function(notifItem) {
		try {
			if(notifItem) {
				var qmnum = notifItem.get('qmnum');
				var key = this.extractKeyFromObject(notifItem);
			
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
					var cachedMetaEntry = this.getCache().get(qmnum);
					
					if(cachedMetaEntry) {
						var subHashMap = cachedMetaEntry.get('value');
						
						subHashMap.removeAtKey(key);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of BaseNotifItemCache', ex);
	    }
	},
	
	//private
	extractFromCacheForNotif: function(qmnum) {
		var retval = null;
		
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var cachedMetaEntry = this.getCache().get(qmnum);
			
				if(cachedMetaEntry) {
					var subHashMap = cachedMetaEntry.get('value');
					
					var notifsNotifItems = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifItem',
						autoLoad: false
					});
					
					//check if the meta entry in not empty
					if(subHashMap.getCount() > 0) {
						subHashMap.each(function(key, cacheEntry) {
							notifsNotifItems.add(cacheEntry.get('value'));
						}, this);
					}
			
					retval = notifsNotifItems;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForNotif of BaseNotifItemCache', ex);
		}
		
		return retval;
	}
});