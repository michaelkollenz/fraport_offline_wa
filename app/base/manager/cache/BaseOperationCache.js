Ext.define('AssetManagement.base.manager.cache.BaseOperationCache', {
    extend: 'AssetManagement.customer.manager.cache.GradedCache',

    //THIS CACHE WORKS HIERARCHICALLY
    //THAT IS WHY IT HAS TO OVERRIDE NEARLY ALL APIS OF THE BASE CLASS

    requires: [
	    'AssetManagement.customer.model.bo.Operation',
        'AssetManagement.customer.model.bo.TimeConf',
        'Ext.data.Store'
    ],

    config: {
        //private
        modelClass: 'AssetManagement.customer.model.bo.Operation',
        onceRequestedOrders: null
    },

    //protected
    constructor: function (config) {
        this.callParent(arguments);

        try {
            this.setOnceRequestedOrders(Ext.create('Ext.util.HashMap'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOperationCache', ex);
        }
    },

    //protected
    //@override
    clearCache: function () {
        this.callParent(arguments);

        try {
            this.getOnceRequestedOrders().clear();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseOperationCache', ex);
        }
    },

    //protected
    //@override
    //also listen to the moving data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);

            eventController.registerOnEvent(AssetManagement.customer.manager.TimeConfManager.EVENTS.TIMECONF_ADDED, this.onTimeConfAdded, this);
            eventController.registerOnEvent(AssetManagement.customer.manager.TimeConfManager.EVENTS.TIMECONF_CHANGED, this.onTimeConfChanged, this);
            eventController.registerOnEvent(AssetManagement.customer.manager.TimeConfManager.EVENTS.TIMECONF_DELETED, this.onTimeConfDeleted, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseOperationCache', ex);
        }
    },

    //iterates over the cache to determine the lowest present datagrade
    getMinimumDataGrade: function () {
        var retval = this.self.DATAGRADES.BASE;

        try {
            var cacheRef = this.getCache();

            if (cacheRef.getCount() > 0) {
                var lowestEncountered = this.self.DATAGRADES.FULL;

                var breakLoop = false;

                cacheRef.each(function (metaKey, metaCacheEntry) {
                    var subHashMap = metaCacheEntry.get('value');

                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            var entriesDataGrade = cacheEntry.get('dataGrade');

                            if (entriesDataGrade < lowestEncountered)
                                lowestEncountered = entriesDataGrade;

                            //break the loop, if the lowest possible value has been encountered
                            breakLoop = lowestEncountered == retval;

                            return !breakLoop;
                        }, this);
                    }

                    return !breakLoop;
                }, this);

                retval = lowestEncountered;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of BaseOperationCache', ex);
        }

        return retval;
    },

    //public
    //@override
    //adds a single operation to cache, considering the passed data grade
    //if the operation is already cached, it will be checked if it's cache entrie's data grade has to be updated
    addToCache: function (operation, dataGrade) {
        try {
            if (operation) {
                var aufnr = operation.get('aufnr');
                var key = this.extractKeyFromObject(operation);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    if (!dataGrade)
                        dataGrade = this.self.DATAGRADES.BASE;

                    var cachedMetaEntry = this.getCache().get(aufnr);
                    var subHashMap = null;

                    if (!cachedMetaEntry) {
                        subHashMap = Ext.create('Ext.util.HashMap');

                        cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                            key: aufnr,
                            value: subHashMap,
                            dataGrade: this.self.DATAGRADES.BASE
                        });

                        this.getCache().add(aufnr, cachedMetaEntry);
                    } else {
                        subHashMap = cachedMetaEntry.get('value');
                    }

                    var cachedEntry = subHashMap.get(key);

                    if (!cachedEntry) {
                        cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                            key: key,
                            value: operation,
                            dataGrade: dataGrade
                        });

                        subHashMap.add(key, cachedEntry);
                    }

                    if (dataGrade > cachedEntry.get('dataGrade')) {
                        cachedEntry.set('dataGrade', dataGrade);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseOperationCache', ex);
        }
    },

    //public
    //adds a store of operations to the cache
    //when added this way, the cache may be able to return something on following requests for the same order 
    addToCacheForOrder: function (aufnr, ordersOperations, dataGrade) {
        try {
            var onceRequestedOrders = this.getOnceRequestedOrders();

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && ordersOperations) {
                if (!dataGrade)
                    dataGrade = this.self.DATAGRADES.BASE;

                if (!onceRequestedOrders.containsKey(aufnr) || onceRequestedOrders.get(aufnr) < dataGrade)
                    onceRequestedOrders.replace(aufnr, dataGrade);

                //next generate/update meta entry
                var cachedMetaEntry = this.getCache().get(aufnr);

                if (!cachedMetaEntry) {
                    var subHashMap = Ext.create('Ext.util.HashMap');

                    cachedMetaEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                        key: aufnr,
                        value: subHashMap,
                        dataGrade: dataGrade
                    });

                    this.getCache().add(aufnr, cachedMetaEntry);
                } else if (cachedMetaEntry.get('dataGrade') < dataGrade) {
                    cachedMetaEntry.set('dataGrade', dataGrade);
                }

                if (ordersOperations.getCount() > 0) {
                    ordersOperations.each(function (operation) {
                        this.addToCache(operation, dataGrade);
                    }, this);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCacheForOrder of BaseOperationCache', ex);
        }
    },

    //public
    //@override
    //tries to fetch the corresponding cache entry for the passed key at atleast the requested data grade
    getFromCache: function (key, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key) && key.length > 12) {
                var aufnr = key.substring(0, 12);

                var cachedMetaEntry = this.getCache().get(aufnr);

                if (cachedMetaEntry) {
                    var subHashMap = cachedMetaEntry.get('value');

                    var cachedEntry = subHashMap.get(key);

                    if (cachedEntry) {
                        if (!dataGrade)
                            dataGrade = this.self.DATAGRADES.BASE;

                        if (dataGrade <= cachedEntry.get('dataGrade')) {
                            retval = cachedEntry.get('value');
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseOperationCache', ex);
        }

        return retval;
    },

    //public
    //@override
    //will return a store of all cached operations, if the cache has been once filled using addStoreForAllToCache
    //and the caches minimum data grade complies with the requested data grade
    getStoreForAll: function (dataGrade) {
        var retval = null;

        try {
            if (!dataGrade)
                dataGrade = this.self.DATAGRADES.BASE;

            if (this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade()) {
                retval = Ext.create('Ext.data.Store', {
                    model: this.getModelClass(),
                    autoLoad: false
                });

                if (this.getCache().getCount() > 0) {
                    this.getCache().each(function (aufnr, metaCacheEntry) {
                        var subHashMap = metaCacheEntry.get('value');

                        if (subHashMap.getCount() > 0) {
                            subHashMap.each(function (key, cacheEntry) {
                                retval.add(cacheEntry.get('value'));
                            }, this);
                        }
                    }, this);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStoreForAll of BaseOperationCache', ex);
            retval = null;
        }

        return retval;
    },

    getAllUpToGrade: function (dataGrade) {
        var retval = null;

        try {
            if (!dataGrade)
                dataGrade = this.self.DATAGRADES.BASE;

            retval = Ext.create('Ext.data.Store', {
                model: this.getModelClass(),
                autoLoad: false
            });

            if (this.getCache().getCount() > 0) {
                this.getCache().each(function (aufnr, metaCacheEntry) {
                    var subHashMap = metaCacheEntry.get('value');

                    //check if the meta entry in not empty
                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            if (cacheEntry.get('dataGrade') <= dataGrade)
                                retval.add(cacheEntry.get('value'));
                        }, this);
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGrade of BaseOperationCache', ex);
            retval = null;
        }

        return retval;
    },

    //public
    //tries to fetch the corresponding cache entry for the passed combination of aufnr and vornr at atleast the requested data grade
    getFromCacheByAufnrVornr: function (aufnr, vornr, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr)) {
                var cachedMetaEntry = this.getCache().get(aufnr);

                if (cachedMetaEntry) {
                    var subHashMap = cachedMetaEntry.get('value');

                    //check if the meta entry in not empty
                    if (subHashMap.getCount() > 0) {
                        //find the correct operation
                        subHashMap.each(function (key, cacheEntry) {
                            var operation = cacheEntry.get('value');

                            if (operation.get('vornr') === vornr) {
                                retval = operation;
                                return false;
                            }
                        }, this);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseOperationCache', ex);
        }

        return retval;
    },

    //public
    //tries to get operations for a order from cache
    //will return order's operations, only if they have been added via addToCacheForOrder once
    getFromCacheForOrder: function (aufnr, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var onceRequestedOrders = this.getOnceRequestedOrders();

                var coveredByAll = this.getCacheComplete() && dataGrade <= this.getMinimumDataGrade();
                var coveredBySpecific = onceRequestedOrders.containsKey(aufnr) && dataGrade <= onceRequestedOrders.get(aufnr);

                if (coveredByAll || coveredBySpecific) {
                    retval = this.extractFromCacheForOrder(aufnr);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForOrder of BaseOperationCache', ex);
        }

        return retval;
    },

    //public
    //combines logic from getFromCacheForOrder and getAllUpToGrade
    getAllUpToGradeForOrder: function (aufnr, dataGrade) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var onceRequestedOrders = this.getOnceRequestedOrders();

                var coveredByAll = this.getCacheComplete();
                var coveredBySpecific = onceRequestedOrders.containsKey(aufnr);

                if (coveredByAll || coveredBySpecific) {
                    var cachedMetaEntry = this.getCache().get(aufnr);

                    if (cachedMetaEntry) {
                        var subHashMap = cachedMetaEntry.get('value');

                        var ordersOperations = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.Operation',
                            autoLoad: false
                        });

                        //check if the meta entry in not empty
                        if (subHashMap.getCount() > 0) {
                            subHashMap.each(function (key, cacheEntry) {
                                if (cacheEntry.get('dataGrade') <= dataGrade)
                                    ordersOperations.add(cacheEntry.get('value'));
                            }, this);
                        }

                        retval = ordersOperations;
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllUpToGradeForOrder of BaseOperationCache', ex);
            retval = null;
        }

        return retval;
    },

    //public
    //removes a specific operation from cache
    removeFromCache: function (operation) {
        try {
            if (operation) {
                var aufnr = operation.get('aufnr');
                var key = this.extractKeyFromObject(operation);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    var cachedMetaEntry = this.getCache().get(aufnr);

                    if (cachedMetaEntry) {
                        var subHashMap = cachedMetaEntry.get('value');

                        subHashMap.removeAtKey(key);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of BaseOperationCache', ex);
        }
    },

    //private
    extractFromCacheForOrder: function (aufnr) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var cachedMetaEntry = this.getCache().get(aufnr);

                if (cachedMetaEntry) {
                    var subHashMap = cachedMetaEntry.get('value');

                    var ordersOperations = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.Operation',
                        autoLoad: false
                    });

                    //check if the meta entry in not empty
                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            ordersOperations.add(cacheEntry.get('value'));
                        }, this);
                    }

                    retval = ordersOperations;
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCacheForOrder of BaseOperationCache', ex);
        }

        return retval;
    },

    //handles case, a new time conf has to be included into an affected cache entrie's dependend data
    onTimeConfAdded: function (timeConf) {
        try {
            if (timeConf) {
                var affectedOperation = this.getFromCacheByAufnrVornr(timeConf.get('aufnr'), timeConf.get('vornr'));

                if (affectedOperation) {
                    var operationsTimeConfs = affectedOperation.get('timeConfs');

                    if (!operationsTimeConfs) {
                        operationsTimeConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.TimeConf',
                            autoLoad: false
                        });

                        affectedOperation.set('timeConfs', operationsTimeConfs);
                    }

                    var indexOfAffectedTimeConf = operationsTimeConfs.indexOfId(timeConf.get('id'));

                    if (indexOfAffectedTimeConf >= 0) {
                        var affectedTimeConf = operationsTimeConfs.getAt(indexOfAffectedTimeConf);

                        if (affectedTimeConf !== timeConf) {
                            operationsTimeConfs.removeAt(indexOfAffectedTimeConf);
                            operationsTimeConfs.insert(indexOfAffectedTimeConf, timeConf);
                        }
                    } else {
                        operationsTimeConfs.add(timeConf);
                    }

                    AssetManagement.customer.manager.TimeConfManager.updateTimeConfSumOperation(affectedOperation);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfAdded of BaseOperationCache', ex);
        }
    },

    //handles case, a time conf's data changed on a duplicate time conf object
    onTimeConfChanged: function (timeConf) {
        try {
            //case is covered by logic of onTimeConfAdded - the affected time conf will be replaced by the duplicate
            this.onTimeConfAdded(timeConf);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfChanged of BaseOperationCache', ex);
        }
    },

    //handles case, a time conf has not yet been deleted from an affected cache entrie's dependend data
    onTimeConfDeleted: function (timeConf) {
        try {
            if (timeConf) {
                var affectedOperation = this.getFromCacheByAufnrVornr(timeConf.get('aufnr'), timeConf.get('vornr'));

                if (affectedOperation) {
                    var operationsTimeConfs = affectedOperation.get('timeConfs');

                    if (operationsTimeConfs) {
                        var indexOfAffectedTimeConf = operationsTimeConfs.indexOfId(timeConf.get('id'));

                        if (indexOfAffectedTimeConf >= 0) {
                            operationsTimeConfs.removeAt(indexOfAffectedTimeConf);
                        }
                    }

                    AssetManagement.customer.manager.TimeConfManager.updateTimeConfSumOperation(affectedOperation);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfDeleted of BaseOperationCache', ex);
        }
    }
});