﻿Ext.define('AssetManagement.base.manager.cache.BaseQPResultCache', {
    extend: 'AssetManagement.customer.manager.cache.GradedCache',

    requires: [
	    'AssetManagement.customer.model.bo.QPResult'
    ],

    config: {
        modelClass: 'AssetManagement.customer.model.bo.QPResult',
        onceRequestedObjects: null
    },

    //protected
    constructor: function (config) {
        this.callParent(arguments);

        try {
            this.setOnceRequestedObjects(Ext.create('Ext.util.HashMap'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseQPResultCache', ex);
        }
    },

    //protected
    //@override
    //also listen to the customizing data cache reset event
    setupDataEventListeners: function () {
        this.callParent();

        try {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseQPResultCache', ex);
        }
    },

    //protected
    //@override
    clearCache: function() {
        this.callParent(arguments);

        try {
            this.getOnceRequestedObjects().clear();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseQPResultCache', ex);
        }
    },

    //public
    //@override
    //adds a single QPResult to cache, considering the passed data grade
    //if the notifItem is already cached, it will be checked if it's cache entrie's data grade has to be updated
    addToCache: function (QPResult, prueflos, dataGrade) {
        try {
            if (QPResult) {
                var key = prueflos;

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    if (!dataGrade)
                        dataGrade = this.self.DATAGRADES.BASE;
                    
                    var cache = this.getCache();
                    
                    var cachedEntry = cache.get(key);

                    if (!cachedEntry) {
                        cachedEntry = Ext.create('AssetManagement.customer.manager.cache.GradedCacheEntry', {
                            key: key,
                            value: QPResult,
                            dataGrade: dataGrade
                        });

                        cache.add(key, cachedEntry);
                    }

                    if (dataGrade > cachedEntry.get('dataGrade')) {
                        cachedEntry.set('dataGrade', dataGrade);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseQPResultCache', ex);
        }
    },

    //iterates over the cache to determine the lowest present datagrade
    getMinimumDataGrade: function () {
        var retval = this.self.DATAGRADES.BASE;

        try {
            var cacheRef = this.getCache();

            if (cacheRef.getCount() > 0) {
                var lowestEncountered = this.self.DATAGRADES.FULL;

                var breakLoop = false;

                cacheRef.each(function (metaKey, metaCacheEntry) {
                    var subHashMap = metaCacheEntry.get('value');

                    if (subHashMap.getCount() > 0) {
                        subHashMap.each(function (key, cacheEntry) {
                            var entriesDataGrade = cacheEntry.get('dataGrade');

                            if (entriesDataGrade < lowestEncountered)
                                lowestEncountered = entriesDataGrade;

                            //break the loop, if the lowest possible value has been encountered
                            breakLoop = lowestEncountered === retval;

                            return !breakLoop;
                        }, this);
                    }

                    return !breakLoop;
                }, this);

                retval = lowestEncountered;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMinimumDataGrade of BaseQPResultCache', ex);
        }

        return retval;
    },

    //public
    //@override
    //tries to fetch the corresponding cache entry for the passed key at atleast the requested data grade
    getFromCache: function (key,dataGrade){
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {


                    var cachedEntry = this.getCache().get(key);

                    if (cachedEntry) {
                        if (!dataGrade)
                            dataGrade = this.self.DATAGRADES.BASE;

                        if (dataGrade <= cachedEntry.get('dataGrade')) {
                            retval = cachedEntry.get('value');
                        }
                    }
                }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseQPResultCache', ex);
        }
        return retval;
    },
   

    //public
    //removes a specific prueflose from cache
    removeFromCache: function (prueflose) {
        try {
            if (prueflose) {
                var prueflos = prueflose.get('prueflos');
                var key = this.extractKeyFromObject(prueflose);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(prueflos)&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    var cachedMetaEntry = this.getCache().get(prueflos);

                    if (cachedMetaEntry) {
                        var subHashMap = cachedMetaEntry.get('value');

                        subHashMap.removeAtKey(key);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeFromCache of BaseQPResultCache', ex);
        }
    }

});