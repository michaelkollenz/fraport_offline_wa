Ext.define('AssetManagement.base.manager.cache.BaseCust_007Cache', {
	extend: 'AssetManagement.customer.manager.cache.Cache',

	requires: [
	    'AssetManagement.customer.model.bo.Cust_007'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.Cust_007'
	},
	
	//protected
	//@override
	//also listen to the customizing data cache reset event
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseCust_007Cache', ex);
	    }
	}
});