Ext.define('AssetManagement.base.manager.cache.BaseOrderCache', {
	extend: 'AssetManagement.customer.manager.cache.GradedCache',

	requires: [
	    'AssetManagement.customer.model.bo.Order',
	    'AssetManagement.customer.model.bo.Operation',
	    'AssetManagement.customer.model.bo.SDOrder',
        'AssetManagement.customer.manager.OperationManager',
        //'AssetManagement.customer.manager.SDOrderManager',     //CAUSES RING DEPENDENCY
        'AssetManagement.customer.manager.TimeConfManager',
        'AssetManagement.customer.manager.MatConfManager',
        'AssetManagement.customer.manager.QPlosManager',
        'AssetManagement.customer.manager.DocUploadManager',
	    'AssetManagement.customer.manager.LocalOrderStatusManager',
	    'Ext.data.Store'
	],

	config: {
		modelClass: 'AssetManagement.customer.model.bo.Order'
	},
	
	//protected
	//@override
	//also listen to the moving data cache reset event
	//also listen to operation data changed events
	setupDataEventListeners: function() {
		this.callParent();
	
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();
			eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.OperationManager.EVENTS.OPERATION_ADDED, this.onOrderOperationAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.OperationManager.EVENTS.OPERATION_CHANGED, this.onOrderOperationChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.OperationManager.EVENTS.OPERATION_DELETED, this.onOrderOperationDeleted, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderManager.EVENTS.SDORDER_ADDED, this.onSDOrderAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderManager.EVENTS.SDORDER_CHANGED, this.onSDOrderChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.SDOrderManager.EVENTS.SDORDER_DELETED, this.onSDOrderDeleted, this);
			
			eventController.registerOnEvent(AssetManagement.customer.manager.TimeConfManager.EVENTS.TIMECONF_ADDED, this.onTimeConfAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.TimeConfManager.EVENTS.TIMECONF_CHANGED, this.onTimeConfChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.TimeConfManager.EVENTS.TIMECONF_DELETED, this.onTimeConfDeleted, this);

			eventController.registerOnEvent(AssetManagement.customer.manager.MatConfManager.EVENTS.MATCONF_ADDED, this.onMatConfAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.MatConfManager.EVENTS.MATCONF_CHANGED, this.onMatConfChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.MatConfManager.EVENTS.MATCONF_DELETED, this.onMatConfDeleted, this);

			eventController.registerOnEvent(AssetManagement.customer.manager.DocUploadManager.EVENTS.DOCUPLOAD_ADDED, this.onDocUploadAdded, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.DocUploadManager.EVENTS.DOCUPLOAD_CHANGED, this.onDocUploadChanged, this);
			eventController.registerOnEvent(AssetManagement.customer.manager.DocUploadManager.EVENTS.DOCUPLOAD_DELETED, this.onDocUploadDeleted, this);

		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupDataEventListeners of BaseOrderCache', ex);
	    }
	},
	
	//handles case, a new operation has to be included into an affected cache entrie's dependend data
	onOrderOperationAdded: function(operation) {
		try {
			if(operation) {
				var affectedOrder = this.getFromCache(operation.get('aufnr'));
				
				if(affectedOrder) {
					var ordersOperations = affectedOrder.get('operations');
					
					if(!ordersOperations) {
						ordersOperations = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Operation',
							autoLoad: false
						});
						
						affectedOrder.set('operations', ordersOperations);
					} 

					var indexOfAffectedOperation = ordersOperations.indexOfId(operation.get('id'));
						
					if(indexOfAffectedOperation >= 0) {
						var affectedOperation = ordersOperations.getAt(indexOfAffectedOperation);
						
						if(affectedOperation !== operation) {
							ordersOperations.removeAt(indexOfAffectedOperation);
							ordersOperations.insert(indexOfAffectedOperation, operation);
						}
					} else {
						ordersOperations.add(operation);
					}

					AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(affectedOrder);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderOperationAdded of BaseOrderCache', ex);
		} 
	},
	
	//handles case, an operation's data changed on a duplicate operation object
	onOrderOperationChanged: function(operation) {
		try {
			//case is covered by logic of onOrderOperationAdded - the affected operation will be replaced by the duplicate
			this.onOrderOperationAdded(operation);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderOperationChanged of BaseOrderCache', ex);
		} 
	},
	
	//handles case, an operation has not yet been deleted from an affected cache entrie's dependend data
	onOrderOperationDeleted: function(operation) {
		try {
			if(operation) {
				var affectedOrder = this.getFromCache(operation.get('aufnr'));
				
				if(affectedOrder) {
					var ordersOperations = affectedOrder.get('operations');
					
					if(ordersOperations) {
						var indexOfAffectedOperation = ordersOperations.indexOfId(operation.get('id'));
						
						if(indexOfAffectedOperation >= 0) {
							ordersOperations.removeAt(indexOfAffectedOperation);
						}
					}
				}

				AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(affectedOrder);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderOperationDeleted of BaseOrderCache', ex);
		} 
	},
	
	//handles case, a new sd order has to be included into an affected cache entrie's dependend data
	onSDOrderAdded: function(sdOrder) {
		try {
			if(sdOrder) {
				var affectedOrder = this.getFromCache(sdOrder.get('aufnr'));
				
				if(affectedOrder) {
					var ordersSDOrders = affectedOrder.get('sdOrders');
					
					if(!ordersSDOrders) {
						ordersSDOrders = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.SDOrder',
							autoLoad: false
						});
						
						affectedOrder.set('sdOrders', ordersSDOrders);
					}
					
					var indexOfAffectedSDOrder = ordersSDOrders.indexOfId(sdOrder.get('id'));
					
					if(indexOfAffectedSDOrder >= 0) {
						var affectedSDOrder = ordersSDOrders.getAt(indexOfAffectedSDOrder);
						
						if(affectedSDOrder !== sdOrder) {
							ordersSDOrders.removeAt(indexOfAffectedSDOrder);
							ordersSDOrders.insert(indexOfAffectedSDOrder, sdOrder);
						}
					} else {
						ordersSDOrders.add(sdOrder);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderAdded of BaseOrderCache', ex);
		} 
	},
	
	//handles case, a sdOrder's data changed on a duplicate sdOrder object
	onSDOrderChanged: function(sdOrder) {
		try {
			//case is covered by logic of onSDOrderAdded - the affected sdOrder will be replaced by the duplicate
			this.onSDOrderAdded(sdOrder);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderChanged of BaseOrderCache', ex);
		} 
	},
	
	//handles case, a sdOrder has not yet been deleted from an affected cache entrie's dependend data
	onSDOrderDeleted: function(sdOrder) {
		try {
			if(sdOrder) {
				var affectedOrder = this.getFromCache(sdOrder.get('aufnr'));
				
				if(affectedOrder) {
					var ordersSDOrders = affectedOrder.get('sdOrders');
					
					if(ordersSDOrders) {
						var indexOfAffectedSDOrder = ordersSDOrders.indexOfId(sdOrder.get('id'));
						
						if(indexOfAffectedSDOrder >= 0) {
							ordersSDOrders.removeAt(indexOfAffectedSDOrder);
						}
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderDeleted of BaseOrderCache', ex);
		} 
	},
	
	//handles case, a new time conf has been added
	onTimeConfAdded: function(timeConf) {
	    try {
	        if (timeConf) {
	            //reevaluate the local status of the affected order
	            var affectedOrder = this.getFromCache(timeConf.get('aufnr'));

	            if (affectedOrder) {
	                //the time conf may not already be included into operation's time conf store yet - queue the status reevaluation, so this is happening first
	                Ext.defer(function() { AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(affectedOrder); }, 100, this);
	            }
	        }
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfAdded of BaseOrderCache', ex);
		} 
	},
	
	//handles case, a time conf's data changed
	onTimeConfChanged: function(timeConf) {
		try {
		    //case is covered by logic of onTimeConfAdded
		    this.onTimeConfAdded(timeConf);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfChanged of BaseOrderCache', ex);
		} 
	},
	
	//handles case, a time conf has been deleted from an affected cache entrie's dependend data
	onTimeConfDeleted: function (timeConf) {
		try {
		    this.onTimeConfAdded(timeConf);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTimeConfDeleted of BaseOrderCache', ex);
		} 
	},

    //handles case, a new mat conf has been added
	onMatConfAdded: function (matConf) {
	    try {
	        if (matConf) {
	            //reevaluate the local status of the affected order
	            var affectedOrder = this.getFromCache(matConf.get('aufnr'));

	            if (affectedOrder) {
	                //the mat conf may not already be included into operation's mat conf store yet - queue the status reevaluation, so this is happening first
	                Ext.defer(function () { AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(affectedOrder); }, 100, this);
	            }
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMatConfAdded of BaseOrderCache', ex);
	    }
	},

    //handles case, a mat conf's data changed
	onMatConfChanged: function (matConf) {
	    try {
	        //case is covered by logic of onMatConfAdded
	        this.onMatConfAdded(matConf);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMatConfChanged of BaseOrderCache', ex);
	    }
	},

    //handles case, a mat conf has been deleted from an affected cache entrie's dependend data
	onMatConfDeleted: function (matConf) {
	    try {
	        this.onMatConfAdded(matConf);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMatConfDeleted of BaseOrderCache', ex);
	    }
	},

    //handles case, a new time conf banf has added
	onDocUploadAdded: function (docUpload) {
	    try {
	        if (docUpload) {
	            //reevaluate the local status of the affected order
	            var affectedOrder = this.getFromCache(docUpload.get('refObject'));

	            if (affectedOrder) {
	                //the doc upload may not already be included into order's documents store yet - queue the status reevaluation, so this is happening first
	                Ext.defer(function () { AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(affectedOrder); }, 100, this);
	            }
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDocUploadAdded of BaseOrderCache', ex);
	    }
	},

    //handles case, a doc upload changed
	onDocUploadChanged: function (docUpload) {
	    try {
	        //case is covered by logic of onDocUploadAdded
	        this.onDocUploadAdded(docUpload);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDocUploadChanged of BaseOrderCache', ex);
	    }
	},

    //handles case, a doc upload has been deleted from an affected cache entrie's dependend data
	onDocUploadDeleted: function (docUpload) {
	    try {
	        //case is covered by logic of onDocUploadAdded
	        this.onDocUploadAdded(docUpload);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDocUploadDeleted of BaseOrderCache', ex);
	    }
	}

});