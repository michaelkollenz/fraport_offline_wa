Ext.define('AssetManagement.base.manager.BaseBanfManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.BanfItem',
        'Ext.data.Store',
        'AssetManagement.customer.helper.MobileIndexHelper',
        'AssetManagement.customer.model.bo.BanfHeader',
        'AssetManagement.customer.model.bo.BanfPosition'
    ],
	
	inheritableStatics: {
		EVENTS: {
			BANF_ADDED: 'banfAdded',
			BANF_CHANGED: 'banfChanged',
			BANF_DELETED: 'banfDeleted',
			
			EXT_BANF_ADDED: 'extBanfAdded',
			EXT_BANF_CHANGED: 'extBanfChanged',
			EXT_BANF_DELETED: 'extBanfDeleted'
		},
		
		/*
		 * get normal banfs for internal technicians
		 * returns banf store
		 */
		getBanfs: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._listCacheComplete === true) {
					eventController.requestEventFiring(retval, this._banfListCache);
					
					return retval;
				}
				
				var toFill = this.getListCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildBanfsStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me.loadDependendDataForExternalConfirmations.call(me, toFill, retval);
							
							me._listCacheComplete = true;
						
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBanfs of BaseBanfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PREQ_ITEM', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PREQ_ITEM', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBanfs of BaseBanfManager', ex);
			}
	
			return retval;
		},
		
		//private methods		
		buildBanfFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildBanfFromDbResultObject(eventArgs.target.result.value);			
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfFromDataBaseQuery of BaseBanfManager', ex);
			}
			return retval;
		},
		
		buildBanfsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var banf = this.buildBanfFromDbResultObject(cursor.value);
						store.add(banf);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
	        } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfsStoreFromDataBaseQuery of BaseBanfManager', ex);
			}	
		},
		
		buildBanfPositionsStoreFromDataBaseQuery: function(store, eventArgs){
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var banf = this.buildBanfPositionFromDbResultObject(cursor.value);
						store.add(banf);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
	        } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfPositionsStoreFromDataBaseQuery of BaseBanfManager', ex);
			}	
		},
		
		buildBanfHeadersStoreFromDataBaseQuery: function(store, eventArgs){
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var banfHeader = this.buildBanfHeaderDataBaseQuery(cursor.value);
						store.add(banfHeader);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
	        } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfHeadersStoreFromDataBaseQuery of BaseBanfManager', ex);
			}	
		},

		
		buildBanfFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
		    //ordinary fields
			    retval = Ext.create('AssetManagement.customer.model.bo.BanfItem', {
					banfn: dbResult['BANFN'],
					bnfpo: dbResult['BNFPO'],
					statu: dbResult['STATU'],
					frgst: dbResult['FRGST'],
					ekgrp: dbResult['EKGRP'],
					afnam: dbResult['AFNAM'],
					txz01: dbResult['TXZ01'],
					matnr: dbResult['MATNR'],
					werks: dbResult['WERKS'],
					lgort: dbResult['LGORT'],
					matkl: dbResult['MATKL'],
					menge: dbResult['MENGE'],
					meins: dbResult['MEINS'],
					reswk: dbResult['RESWK'],
					ktext: dbResult['KTEXT'],
					leinh: dbResult['LEINH'],
					wepos: dbResult['WEPOS'],
					ekorg: dbResult['EKORG'],
					ebeln: dbResult['EBELN'],
					ebelp: dbResult['EBELP'],
					bsmng: dbResult['BSMNG'],
					addrNo: dbResult['ADDR_NO'],
					formofaddr: dbResult['FORMOFADDR'],
					name1: dbResult['NAME'],
					name2: dbResult['NAME_2'],
					name3: dbResult['NAME_3'],
					name4: dbResult['NAME_4'],
					coname: dbResult['C_O_NAME'],
					city: dbResult['CITY'],
					district: dbResult['DISTRICT'],
					cityNo: dbResult['CITY_NO'],
					postlCod1: dbResult['POSTL_COD1'],
					postlCod2: dbResult['POSTL_COD2'],
					postlCod3: dbResult['POSTL_COD3'],
					poBox: dbResult['PO_BOX'],
					poBoxCit: dbResult['PO_BOX_CIT'],
					delivDis: dbResult['DELIV_DIS'],
					street: dbResult['STREET'],
					streetNo: dbResult['STREET_NO'],
					strAbbr: dbResult['STR_ABBR'],
					houseNo: dbResult['HOUSE_NO'],
					strSuppl1: dbResult['STR_SUPPL1'],
					strSuppl2: dbResult['STR_SUPPL2'],
					location: dbResult['LOCATION'],
					building: dbResult['BUILDING'],
					floor: dbResult['FLOOR'],
					roomNr: dbResult['ROOM_NO'],
					country: dbResult['COUNTRY'],
					langu: dbResult['LANGU'],
					region: dbResult['REGION'],
					pstyp: dbResult['PSTYP'],
                    
					sakto: dbResult['SAKTO'],
					aufnr: dbResult['AUFNR'],
					preis: dbResult['PREIS'],
					waers: dbResult['WAERS'],
					knttp: dbResult['KNTTP'],
					badat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BADAT']),
					lfdat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['LFDAT']),
					bedat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BEDAT']),
					belfdat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BE_LFDAT']),
	
				    updFlag: dbResult['UPDFLAG'],
				    childKey: dbResult['CHILDKEY'],
					mobileKey: dbResult['MOBILEKEY']
					                    
			    });			
			    
			    retval.set('id', dbResult['BANFN'] + dbResult['BNFPO'] );
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfFromDbResultObject of BaseBanfManager', ex);
			}			
			return retval;
		},
		
		buildBanfPositionFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
		    //ordinary fields
			    retval = Ext.create('AssetManagement.customer.model.bo.BanfPosition', {
					banfn: dbResult['BANFN'],
					bnfpo: dbResult['BNFPO'],
					statu: dbResult['STATU'],
					frgst: dbResult['FRGST'],
					ekgrp: dbResult['EKGRP'],
					afnam: dbResult['AFNAM'],
					txz01: dbResult['TXZ01'],
					matnr: dbResult['MATNR'],
					werks: dbResult['WERKS'],
					lgort: dbResult['LGORT'],
					matkl: dbResult['MATKL'],
					menge: dbResult['MENGE'],
					meins: dbResult['MEINS'],
					reswk: dbResult['RESWK'],
					ktext: dbResult['KTEXT'],
					leinh: dbResult['LEINH'],
					wepos: dbResult['WEPOS'],
					ekorg: dbResult['EKORG'],
					ebeln: dbResult['EBELN'],
					ebelp: dbResult['EBELP'],
					bsmng: dbResult['BSMNG'],
					addrNo: dbResult['ADDR_NO'],
					formofaddr: dbResult['FORMOFADDR'],
					name1: dbResult['NAME'],
					name2: dbResult['NAME_2'],
					name3: dbResult['NAME_3'],
					name4: dbResult['NAME_4'],
					coname: dbResult['C_O_NAME'],
					city: dbResult['CITY'],
					district: dbResult['DISTRICT'],
					cityNo: dbResult['CITY_NO'],
					postlCod1: dbResult['POSTL_COD1'],
					postlCod2: dbResult['POSTL_COD2'],
					postlCod3: dbResult['POSTL_COD3'],
					poBox: dbResult['PO_BOX'],
					poBoxCit: dbResult['PO_BOX_CIT'],
					delivDis: dbResult['DELIV_DIS'],
					street: dbResult['STREET'],
					streetNo: dbResult['STREET_NO'],
					strAbbr: dbResult['STR_ABBR'],
					houseNo: dbResult['HOUSE_NO'],
					strSuppl1: dbResult['STR_SUPPL1'],
					strSuppl2: dbResult['STR_SUPPL2'],
					location: dbResult['LOCATION'],
					building: dbResult['BUILDING'],
					floor: dbResult['FLOOR'],
					roomNr: dbResult['ROOM_NO'],
					country: dbResult['COUNTRY'],
					langu: dbResult['LANGU'],
					region: dbResult['REGION'],
					pstyp: dbResult['PSTYP'],
             		idnlf: dbResult['IDNLF'],
                    fixed: dbResult['FIXED'],
                    bednr: dbResult['BEDNR'],
                    aufnr: dbResult['AUFNR'],
                    vbeln: dbResult['VBELN'],
			        vbelp: dbResult['VBELP'],
			
					badat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BADAT']),
					lfdat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['LFDAT']),
					bedat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BEDAT']),
					belfdat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BE_LFDAT']),
	
				    updFlag: dbResult['UPDFLAG'],
				    childKey: dbResult['CHILDKEY'],
					mobileKey: dbResult['MOBILEKEY']
			    });
			
			    retval.set('id', dbResult['BANFN'] + dbResult['BNFPO'] );
			    
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfPositionFromDbResultObject of BaseBanfManager', ex);
		    }
			return retval;
		},
				
		//builds the raw object to store in the database out of a banf model
		buildDataBaseObjectForBanf: function(banf, isPosition) {
			var ac = null;
			var retval = null;
		
			try {
				ac = AssetManagement.customer.core.Core.getAppConfig();
				     
			    retval = { };
				retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['UPDFLAG'] =  banf.get('updFlag'); 
				retval['BANFN'] = banf.get('banfn');
				retval['BNFPO'] = banf.get('bnfpo');
				retval['STATU'] = banf.get('statu');
				retval['FRGST'] = banf.get('frgst');
				retval['EKGRP'] = banf.get('ekgrp');
				retval['AFNAM'] = banf.get('afnam');
				retval['TXZ01'] = banf.get('txz01');
				retval['MATNR'] = banf.get('matnr');
				retval['WERKS'] = banf.get('werks');
				retval['LGORT'] = banf.get('lgort');
				retval['MATKL'] = banf.get('matkl');
				retval['MENGE'] = banf.get('menge');
				retval['MEINS'] = banf.get('meins');
				retval['RESWK'] = banf.get('reswk');
				retval['KTEXT'] = banf.get('ktext');
				retval['LEINH'] = banf.get('leinh');
				retval['WEPOS'] = banf.get('wepos');
				retval['EKORG'] = banf.get('ekorg');
				retval['EBELN'] = banf.get('ebeln');
				retval['EBELP'] = banf.get('ebelp');
				retval['BSMNG'] = banf.get('bsmng');
				retval['ADDR_NO'] = banf.get('addrNo');
				retval['FORMOFADDR'] = banf.get('formofaddr');
				retval['NAME'] = banf.get('name1');
				retval['NAME_2'] = banf.get('name2');
				retval['NAME_3'] = banf.get('name3');
				retval['NAME_4'] = banf.get('name4');
				retval['C_O_NAME'] = banf.get('coname');
				retval['CITY'] = banf.get('city');
				retval['DISTRICT'] = banf.get('district');
				retval['CITY_NO'] = banf.get('cityNo');
				retval['POSTL_COD1'] = banf.get('postlCod1');
				retval['POSTL_COD2'] = banf.get('postlCod2');
				retval['POSTL_COD3'] = banf.get('postlCod3');
				retval['PO_BOX'] = banf.get('poBox');
				retval['PO_BOX_CIT'] = banf.get('poBoxCit');
				retval['DELIV_DIS'] = banf.get('delivDis');
				retval['STREET'] = banf.get('street');
				retval['STREET_NO'] = banf.get('streetNo');
				retval['STR_ABBR'] = banf.get('strAbbr');
				retval['HOUSE_NO'] = banf.get('houseNo');
				retval['STR_SUPPL1'] = banf.get('strSuppl1');
				retval['STR_SUPPL2'] = banf.get('strSuppl2');
				retval['LOCATION'] = banf.get('location');
				retval['BUILDING'] = banf.get('building');
				retval['FLOOR'] = banf.get('floor');
				retval['ROOM_NO'] = banf.get('roomNr');
				retval['COUNTRY'] = banf.get('country');
				retval['LANGU'] = banf.get('langu');
				retval['REGION'] = banf.get('region');
				retval['PSTYP'] = banf.get('pstyp');
                retval['IDNLF'] = banf.get('idnlf');
                retval['FIXED'] = banf.get('fixed');
                retval['BEDNR'] = banf.get('bednr');
                retval['AUFNR'] = banf.get('aufnr');
                retval['VBELN'] = banf.get('vbeln');
                retval['VBELP'] = banf.get('vbelp');
				
				retval['BADAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(banf.get('badat'));
				retval['LFDAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(banf.get('lfdat'));
				retval['BEDAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(banf.get('bedat'));
				retval['BE_LFDAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(banf.get('belfdat'));
				
				retval['MOBILEKEY'] = banf.get('mobileKey');
				retval['CHILDKEY'] = banf.get('childKey');
				
				if(isPosition) {
					retval['AUFNR'] = banf.get('aufnr');
					retval['SAKTO'] = banf.get('sakto');
					retval['PREIS'] = banf.get('preis');
					retval['WAERS'] = banf.get('waers');
					retval['KNTTP'] = banf.get('knttp');
				}			
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForBanf of BaseBanfManager', ex);
		    }			
			return retval;
		},
		
		buildBanfHeaderDataBaseQuery: function(dbResult){
	        var retval = null;
	        
	        try {	        
			    retval = Ext.create('AssetManagement.customer.model.bo.BanfHeader', {
			    banfn: dbResult['BANFN'],
			    bsart: dbResult['BSART'],
				lifnr: dbResult['LIFNR'],
				aufnr: dbResult['AUFNR'],
		
			    updFlag: dbResult['UPDFLAG'],
			    mobileKey_ref: dbResult['MOBILEKEY_REF'],
				mobileKey: dbResult['MOBILEKEY']
				                    
			    });
			
			    retval.set('id', dbResult['BANFN'] +  dbResult['AUFNR']);	
			    
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBanfHeaderDataBaseQuery of BaseBanfManager', ex);
		    }    
			return retval;
		},
		
		buildDataBaseObjectForBanfHeader: function(banf){
	        var retval = null;
			var ac = null;
			
			try {
			    ac = AssetManagement.customer.core.Core.getAppConfig();
			
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
			    retval['USERID'] =  ac.getUserId();
			    retval['UPDFLAG'] =  banf.get('updFlag'); 
			    retval['BANFN'] = banf.get('banfn');
			    retval['MOBILEKEY'] = banf.get('mobileKey');
			    retval['BSART'] = banf.get('bsart');
			    retval['LIFNR'] = banf.get('lifnr');
			    retval['AUFNR'] = banf.get('aufnr');
			    retval['MOBILEKEY_REF'] = banf.get('mobileKey_ref');
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForBanfHeader of BaseBanfManager', ex);
		    }
			return retval;
		}, 
		
		addToCache: function(banf) {
			try {
				var temp = this.getListCache().getById(banf.get('id'));
				
				if(temp !== null && temp !== undefined)
					this._banfListCache.remove(temp);
					
				this._banfListCache.add(banf);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseBanfManager', ex);
			}
		},
		
		getFromCache: function(banfn, bnfpo) {
			var retval = null;
			
			try {
				retval = this.getListCache().getById(banfn + bnfpo);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseBanfManager', ex);
			}
			
			return retval;
		}, 
		
		
		deleteBanf: function(banf, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!banf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(banf);
							eventController.fireEvent(me.EVENTS.BANF_DELETED, banf);
						}
						
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteBanf of BaseBanfManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the banf to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_PREQ_ITEM', banf);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_PREQ_ITEM', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteBanf of BaseBanfManager', ex);
			}
			
			return retval;
		},
		
			
		deleteExtBanf: function(banf, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!banf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(banf);
							eventController.fireEvent(me.EVENTS.EXT_BANF_DELETED, banf);
						}
					
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteExtBanf of BaseBanfManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the ext banf to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_BANF_ITEM', banf);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_BANF_ITEM', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteExtBanf of BaseBanfManager', ex);
			}
			
			return retval;
		},
		
		/*
		 * save normal banf from internal technician 
		 * returns success value
		 */
		saveBanf: function(banf) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!banf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
	
				this.manageUpdateFlagForSaving(banf);
			
				//check if the banf already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banf.get('banfn'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1) {
							counter = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 8);
							banf.set('banfn', counter);
						}
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banf.get('banfn'))) {
							eventController.fireEvent(retval, false);
							return;
						}
						
						banf.set('bnfpo', '0001');
						banf.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([banf.get('banfn'), banf.get('bnfpo') ]));
						
						//set the id
						banf.set('id', banf.get('banfn') + banf.get('bnfpo'));
						
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								
								if(success) {
									var eventType = requiresNewCounterValue ? me.EVENTS.BANF_ADDED : me.EVENTS.BANF_CHANGED;
									eventController.fireEvent(eventType, banf);
								}
								
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanf of BaseBanfManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForBanf(banf, false);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_PREQ_ITEM', toSave, callback, callback);
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanf of BaseBanfManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_PREQ_ITEM', false);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanf of BaseBanfManager', ex);
			}
			
			return retval;
		},
		
		/*
		 * save banf header of banf positions for external confirmations
		 * returns banfHeader object
		 */
		saveBanfHeader: function(banf) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!banf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
	
				this.manageUpdateFlagForSaving(banf);
				
				//check if the banf already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banf.get('banfn'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1)
						{
							counter = 'MO'+AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 8);
							banf.set('banfn', counter);
						}
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banf.get('banfn'))) {
							eventController.fireEvent(retval, false);
							return;
						}
						
						//set the id
						banf.set('id', banf.get('banfn'));
						//set mobileKey
						banf.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([banf.get('banfn'), banf.get('bnfpo') ]));
						
						var callback = function(eventArgs) {
							try {
								//eventController.fireEvent(retval, eventArgs.type === "success");
								eventController.fireEvent(retval, banf); 
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfHeader of BaseBanfManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForBanfHeader(banf);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_BANF_HEADER', toSave, callback, callback);
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfHeader of BaseBanfManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_BANF_HEADER', false);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfHeader of BaseBanfManager', ex);
			}
			
			return retval;
		}, 
		
		/*
		 * get all available banf positions of external confirmations of a specific order
		 * returns store of banf positions 
		 */
		getBanfPositions: function(aufnr, useBatchProcessing){
			var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.BanfPosition',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildBanfPositionsStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							me.loadDependendDataForExternalConfirmations.call(me, theStore, retval);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBanfPositions of BaseBanfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('AUFNR', aufnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_BANF_ITEM', 'AUFNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_BANF_ITEM', 'AUFNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBanfPositions of BaseBanfManager', ex);
			}
			
			return retval;
		},
		
		//private methods
		loadDependendDataForExternalConfirmations: function(extConfs, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(extConfs.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, extConfs);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var materials = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignDependendDataForExternalConfirmations.call(me, eventIdToFireWhenComplete, extConfs, materials);
					}
				};
				
				//get materials
				done++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccured = materialStore === undefined;
					
					materials = materialStore;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				if(done > 0)
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				else
					AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, extConfs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForExternalConfirmations of BaseBanfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForExternalConfirmations: function(eventIdToFireWhenComplete, extConfs, materials) {
			try {
				extConfs.each(function (extConf, index, length) {
					//assign materials
					if(materials) {
						var matnr = extConf.get('matnr');
						
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								extConf.set('material', material);
								return false;
							}
						});
					}
				});
				
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, extConfs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForExternalConfirmations of BaseBanfManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
			
		/*
		 * get banf header of a specific order
		 * returns banfHeader object 
		 */
		getBanfHeader: function(aufnr, useBatchProcessing){
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.BanfHeader',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						me.buildBanfHeadersStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						if(theStore.getCount() > 0)
							eventController.requestEventFiring(retval, theStore.first());
						else
							eventController.requestEventFiring(retval, null);
							
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBanfHeader of BaseBanfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('AUFNR', aufnr);
				indexMap.add('UPDFLAG', 'I');
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_BANF_HEADER', 'AUFNR-UPDFLAG', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_BANF_HEADER', 'AUFNR-UPDFLAG', indexRange, successCallback, null, useBatchProcessing);
				
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBanfHeader of BaseBanfManager', ex);
			}
			
			return retval;
		},

		/*
		 * save external confirmation positions 
		 * returns success value if data is saved to database or not
		 */
		saveBanfPosition: function(banf, banfHeader) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!banf) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				this.manageUpdateFlagForSaving(banf);
				
				//check if the banf already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banf.get('bnfpo'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1) {
							banf.set('bnfpo', nextCounterValue);
						}

						banf.set('banfn', banfHeader.get('banfn'));
						
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banf.get('banfn'))) {
							eventController.fireEvent(retval, false);
							return;
						}
						
						//set mobile key
						banf.set('mobileKey', banfHeader.get('mobileKey'));
						//set childKey
						banf.set('childKey', AssetManagement.customer.manager.KeyManager.createKey([ banf.get('banfn'), banf.get('bnfpo') ]));
						
						//set the id
						banf.set('id', banf.get('banfn') + banf.get('bnfpo'));
						
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
							
								if(success) {
									var eventType = requiresNewCounterValue ? me.EVENTS.EXT_BANF_ADDED : me.EVENTS.EXT_BANF_CHANGED;
									eventController.fireEvent(eventType, banf);
								}
								
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfPosition of BaseBanfManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForBanf(banf, true);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_BANF_ITEM', toSave, callback, callback);
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfPosition of BaseBanfManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextBanfPo(banfHeader);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfPosition of BaseBanfManager', ex);
			}
			
			return retval;
		},
		
		getNextBanfPo: function(banf) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!banf) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxBnfpo = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
							var bnfpoValue = cursor.value['BNFPO'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(bnfpoValue, '%')) {
                                //cut of the percent sign
							    bnfpoValue = bnfpoValue.substr(1);
                                var curBnfpoValue = parseInt(bnfpoValue);
							
							    if(!isNaN(curBnfpoValue))
							    {
								    if(curBnfpoValue > maxBnfpo)
									    maxBnfpo = curBnfpoValue;
							    } 
                            }

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxBnfpo + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
						    
							eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextBanfPo of BaseBanfManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('BANFN', banf.get('banfn'));
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_BANF_ITEM', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_BANF_ITEM', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextBanfPo of BaseBanfManager', ex);
			}
			
			return retval; 
		},
		
		//cache administration
		_banfListCache: null,
		_listCacheComplete: false,
		
		getListCache: function() {
	        try {
	        	if(!this._banfListCache) {
					this._banfListCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.BanfItem',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getListCache of BaseBanfManager', ex);
			}
			
			return this._banfListCache;
		},
		
		clearCache: function() {
	        try {
		    	this.getListCache().removeAll();
		    	this._listCacheComplete = false;
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseBanfManager', ex);
		    }
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseBanfManager', ex);
			} 
		}
	}
});