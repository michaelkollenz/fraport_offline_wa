Ext.define('AssetManagement.base.manager.BaseOrderManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.OrderCache',
        'AssetManagement.customer.model.bo.Order',
        'AssetManagement.customer.model.bo.TimeConf',
        'AssetManagement.customer.model.bo.MaterialConf',
        'AssetManagement.customer.manager.NotifManager',
        'AssetManagement.customer.manager.OperationManager',
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.AddressManager',
        'AssetManagement.customer.manager.ComponentManager',
        'AssetManagement.customer.manager.QPlosManager',
        'AssetManagement.customer.manager.PartnerManager',
        'AssetManagement.customer.manager.ObjectStatusManager',
        'AssetManagement.customer.manager.ObjectListManager',
        'AssetManagement.customer.manager.Cust_011Manager',
        'AssetManagement.customer.manager.CustIlartOrTypeManager',
        'AssetManagement.customer.manager.CustBemotManager',
        'AssetManagement.customer.manager.PriorityManager',
        'AssetManagement.customer.manager.DMSDocumentManager',
        'AssetManagement.customer.manager.DocUploadManager',
        'AssetManagement.customer.manager.LocalOrderStatusManager',
//      'AssetManagement.customer.manager.SDOrderManager',		INCLUSION CAUSES RING DEPENDENCY
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.MobileIndexHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

	inheritableStatics: {
		EVENTS: {
			ORDER_ADDED: 'orderAdded',
			ORDER_CHANGED: 'orderChanged',
			ORDER_DELETED: 'orderDeleted'
		},

		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.OrderCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseOrderManager', ex);
			}

            return retval;
		},

		//public methods
		getOrders: function(withDependendData, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}

				var baseStore = null;

				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if (withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForOrders(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Order',
						autoLoad: false
					});

					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

							me.buildOrdersStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);

								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForOrders.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrders of BaseOrderManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};

					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ORDHEAD', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ORDHEAD', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrders of BaseOrderManager', ex);
			}

			return retval;
		},

		getOrder: function(aufnr, useBatchProcessing) {
			var retval = -1;

			try {
				retval = this.getOrderInternal(aufnr, false, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrder of BaseOrderManager', ex);
			}

			return retval;
		},

		getOrderLightVersion: function(aufnr, useBatchProcessing) {
			var retval = -1;

			try {
				retval = this.getOrderInternal(aufnr, true, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderLightVersion of BaseOrderManager', ex);
			}

			return retval;
		},

		getOrderInternal: function(aufnr, lightVersion, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var cache = this.getCache();
				var requestedDataGrade = lightVersion ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				var fromCache = cache.getFromCache(aufnr, requestedDataGrade);
                
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
                
				//to avoid duplicate objects, check if there already exists an instance for the requested equipment and if so, use it
			    var order = cache.getFromCache(aufnr);
				//var order = null;

				if(order) {
					this.loadDependendDataForOrder(order, retval, lightVersion);
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var order = me.buildOrderFromDataBaseQuery.call(me, eventArgs);

							if(order) {
								me.loadDependendDataForOrder.call(me, order, retval, lightVersion);
							} else {
								eventController.requestEventFiring(retval, null);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderInternal of BaseOrderManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};

					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('AUFNR', aufnr);

					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_ORDHEAD', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ORDHEAD', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderInternal of BaseOrderManager', ex);
			}

			return retval;
		},

		//private methods
		buildOrderFromDataBaseQuery: function(eventArgs) {
			var retval = null;

			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildOrderFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOrderFromDataBaseQuery of BaseOrderManager', ex);
			}

			return retval;
		},

		buildOrdersStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;

				    if (cursor) {
				    	var order = this.buildOrderFromDbResultObject(cursor.value);
						store.add(order);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOrdersStoreFromDataBaseQuery of BaseOrderManager', ex);
		    }
		},

		buildOrderFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	var objnr = dbResult['OBJNR'];

	        	//sometimes objnr is empty due to a backend bug, if that is so, refill it
	        	if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
	        		objnr = 'OR' + dbResult['AUFNR'];
	        	}

	        	retval = Ext.create('AssetManagement.customer.model.bo.Order', {
					aufnr: dbResult['AUFNR'],
					priok: dbResult['PRIOK'],
					equnr: dbResult['EQUNR'],
					bautl: dbResult['BAUTL'],
					iwerk: dbResult['IWERK'],
					ingrp: dbResult['INGRP'],
					pmobjty: dbResult['PM_OBJTY'],
					gewrk: dbResult['GEWRK'],
					kunum: dbResult['KUNUM'],
					qmnum: dbResult['QMNUM'],
					serialnr: dbResult['SERIALNR'],
					sermat: dbResult['SERMAT'],
					auart: dbResult['AUART'],
					ktext: dbResult['KTEXT'],
					vaplz: dbResult['VAPLZ'],
					wawrk: dbResult['WAWRK'],
					tplnr: dbResult['TPLNR'],
					objnr: objnr,
					ilart: dbResult['ILART'],
					rsnum: dbResult['RSNUM'],
					fmat: dbResult['FMAT'],
					bemot: dbResult['BEMOT'],
					bemottxt: dbResult['BEMOT_TXT'],
					adrnra: dbResult['ADRNRA'],
					stsma: dbResult['STSMA'],
					gsber: dbResult['GSBER'],
					prctr: dbResult['PRCTR'],
					vkorg: dbResult['VKORG'],
					vtweg: dbResult['VTWEG'],
					spart: dbResult['SPART'],
					vkgrp: dbResult['VKGRP'],
					vkbur: dbResult['VKBUR'],
					bstkd: dbResult['BSTKD'],
					bstdk: dbResult['BSTDK'],
					matnr: dbResult['MATNR'],
					menge: dbResult['MENGE'],
					meins: dbResult['MEINS'],
					faktf: dbResult['FAKTF'],
					artpr: dbResult['ARTPR'],
					konty: dbResult['KONTY'],
					empge: dbResult['EMPGE'],
					anlzu: dbResult['ANLZU'],
					ltext: dbResult['LTEXT'],

					tidnr: dbResult['TIDNR'],
					strno: dbResult['STRNO'],

					fsav: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['FSAVD'], dbResult['FSAVZ']),
					fsed: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['FSEDD'], dbResult['FSEDZ']),
					gltrp: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GLTRP']),
					gstrp: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GSTRP']),

					usr01Flag: dbResult['USR01FLAG'],
					usr02Flag: dbResult['USR02FLAG'],

					usr03Code: dbResult['USR03CODE'],
					usr04Code: dbResult['USR04CODE'],

					usr05Txt: dbResult['USR05TXT'],
					usr06Txt: dbResult['USR06TXT'],
					usr07Txt: dbResult['USR07TXT'],

					usr08Date: dbResult['USR08DATE'],
					usr09Num: dbResult['USR09NUM'],

					gstrs: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GSTRS'], dbResult['GSUZS']),
					gltrs: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GLTRS'], dbResult['GLUZS']),

					gluzp : dbResult['GLUZP'],
					gsuzp : dbResult['GSUZP'],

				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY'],
				    mobileKeyNotif: dbResult['MOBILEKEY_QM']
			    });

			    retval.set('id', dbResult['AUFNR']);

            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOrderFromDbResultObject of BaseOrderManager', ex);
            }

			return retval;
		},

		loadListDependendDataForOrders: function(eventIdToFireWhenComplete, orders) {
			try {
				//do not continue if there is no data
				if(orders.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, orders);
					return;
				}

				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var orderTypes = null;
                var ordersIlarts = null;
				var opers = null;
				var equis = null;
				var flocs = null;
				var notifs = null;
				var partners = null;			//currenty disabled
				var addresses = null;
				var bemots = null;
				var statusList = null;			//currenty disabled
				var qplos = null;

				var counter = 0;
				var done = 0;
				var erroroccurred = false;
				var reported = false;

				var completeFunction = function() {
					if(erroroccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && erroroccurred === false) {
						me.assignListDependendDataForOrders.call(me, eventIdToFireWhenComplete, orders, orderTypes,
							bemots, opers, flocs, equis, notifs, partners, addresses, statusList, ordersIlarts);
					}
				};

				//TO-DO make dependend of customizing parameters --- this definitely will increase performance



				//get order types
				if(true) {
					done++;

					var orderTypesSuccessCallback = function(ordTypes) {
						erroroccurred = ordTypes === undefined;

						orderTypes = ordTypes;
						counter++;

						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.Cust_011Manager.getOrderTypes(true);
					eventController.registerOnEventForOneTime(eventId, orderTypesSuccessCallback);
				}

				//get bemots
//				if(true) {
//					done++;
//
//					var bemotsSuccessCallback = function(bemotss) {
//						erroroccurred = bemotss === undefined;
//
//						bemots = bemotss;
//						counter++;
//
//						completeFunction();
//					};
//
//					var eventId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
//					eventController.registerOnEventForOneTime(eventId, bemotsSuccessCallback);
//				}

				//get operation data
				if(true) {
					done++;

					var operSuccessCallback = function(operations) {
						erroroccurred = operations === undefined;

						opers = operations;
						counter++;

						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.OperationManager.getOperations(true, true);
					eventController.registerOnEventForOneTime(eventId, operSuccessCallback);
				}

				//get funcLoc data
				if(true) {
					done++;

					var flocSuccessCallback = function(funcLocs) {
						erroroccurred = funcLocs === undefined;

						flocs = funcLocs;
						counter++;

						completeFunction();
					};

					eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(false, true);
					eventController.registerOnEventForOneTime(eventId, flocSuccessCallback);
				}

				//get equipment data
				if(true) {
					done++;

					var equiSuccessCallback = function(equipments) {
						erroroccurred = equipments === undefined;

						equis = equipments;
						counter++;

						completeFunction();
					};

					eventId = AssetManagement.customer.manager.EquipmentManager.getEquipments(false, true);
					eventController.registerOnEventForOneTime(eventId, equiSuccessCallback);
				}

                if(true) {
                    done++;

                    var orderIlartSuccessCallback = function(orderIlarts) {
                        erroroccurred = orderIlarts === undefined;

                        ordersIlarts = orderIlarts;
                        counter++;

                        completeFunction();
                    };

                    var eventId = AssetManagement.customer.manager.CustIlartOrTypeManager.getCustIlartOrTypes(true);
                    eventController.registerOnEventForOneTime(eventId, orderIlartSuccessCallback);
                }

				//get notif data
				if(true) {
					done++;

					var notifSuccessCallback = function(notifications) {
						erroroccurred = notifications === undefined;

						notifs = notifications;
						counter++;

						completeFunction();
					};

					eventId = AssetManagement.customer.manager.NotifManager.getNotifs(false, true);
					eventController.registerOnEventForOneTime(eventId, notifSuccessCallback);
				}

//				//get partner data
//				if(true) {
//					done++;
//
//					var partnerSuccessCallback = function(partnerList) {
//						erroroccurred = partnerList === undefined;
//
//						partners = partnerList;
//						counter++;
//
//						completeFunction();
//					};
//
//					eventId = AssetManagement.customer.manager.PartnerManager.getPartnersForOrdersOnly(true);
//					eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
//				}


				//get address data
				if(true) {
					done++;

					var addressSuccessCallback = function(addressList) {
						erroroccurred = addressList === undefined;

						addresses = addressList;
						counter++;

						completeFunction();
					};

					eventId = AssetManagement.customer.manager.AddressManager.getAddresses(true);
					eventController.registerOnEventForOneTime(eventId, addressSuccessCallback);
				}

//				//get status data
//				if(true) {
//					done++;
//
//					var statusSuccessCallback = function(statuslist) {
//						erroroccurred = statuslist === undefined;
//
//						statusList = statuslist;
//						counter++;
//
//						completeFunction();
//					};
//
//					eventId = AssetManagement.customer.manager.StatusManager.getStatusStoreForOrders(false);
//					eventController.registerOnEventForOneTime(eventId, statusSuccessCallback);
//				}

				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForOrders of BaseOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		//call this function, when all neccessary data has been collected
		assignListDependendDataForOrders: function(eventIdToFireWhenComplete, orders, orderTypes,
				bemots, opers, flocs, equis, notifs, partners, addresses, statusList, ordersIlarts) {
			try {
				//to increase performance create a copy of operations' and notifs' store
				//remove all objects from this store, which have been assigned to it's order
			    //therefore following loops will have less iterations

				var opersCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Operation',
					autoLoad: false
				});

				if(opers) {
					opers.each(function(oper, index, length) {
						opersCopy.add(oper);
					});
				}

				var notifsCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Notif',
					autoLoad: false
				});

				if(notifs) {
					notifs.each(function(notif, index, length) {
						notifsCopy.add(notif);
					});
				}

				orders.each(function(order, index, length) {
					//begin orderType assignment
					var myAuart = order.get('auart');

					if(orderTypes) {
						orderTypes.each(function(orderType, index, length) {
							if(orderType.get('auart') === myAuart) {
								order.set('orderType', orderType);
								return false;
							}
						});
					}
					//end orderType assignment

//					//begin bemots assignment
//					var myBemot = order.get('bemot');
//
//					if(bemots) {
//						bemots.each(function(bemot, index, length) {
//							if(bemot.get('bemot') === myBemot) {
//								order.set('orderBemot', bemot);
//								return false;
//							}
//						});
//					}
//					//end bemots assignment

				    //begin operations assignment
					var myOpers = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Operation',
						autoLoad: false
					});

					//fetch all my operations
					var myAufnr = order.get('aufnr');

					opersCopy.each(function(oper, index, length) {
						if(oper.get('aufnr') === myAufnr) {
							myOpers.add(oper);
						}
					});

					//remove all my operations from the data pool
					myOpers.each(function(oper, index, length) {
						opersCopy.remove(oper);
					});

					//sorting is not required yet for order list
					order.set('operations', myOpers);
					//end operations assignment

					//begin funcLoc assignment
					var myTplnr = order.get('tplnr');

					if(flocs && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myTplnr)) {
						flocs.each(function(floc, index, length) {
							if(floc.get('tplnr') === myTplnr) {
								order.set('funcLoc', floc);
								return false;
							}
						});
					}
				    //end funcLoc assignment

					//begin equipment assignment
					var myEqunr = order.get('equnr');

					if(equis && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myEqunr)) {
						equis.each(function(equi, index, length) {
							if(equi.get('equnr') === myEqunr) {
								order.set('equipment', equi);
								return false;
							}
						});
					}
					//end equipment assignment

					//begin notif assignment
					var myQmnum = order.get('qmnum');
					var myNotif = null;

					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myQmnum)) {
						notifsCopy.each(function(notif, index, length) {
							if(notif.get('qmnum') === myQmnum) {
								order.set('notif', notif);
								myNotif = notif;
								return false;
							}
						});
					}

					if(myNotif)
						notifsCopy.remove(myNotif);
					//end notif assignment
	//
	//				//begin partners assignment
	//				var myPartners = Ext.create('Ext.data.Store', {
	//					model: 'AssetManagement.customer.model.bo.Partner',
	//					autoLoad: false
	//				});
	//
	//				partners.each(function(partner, index, length) {
	//					if(partner.get('objnr') === order.get('objnr'))
	//						myPartners.add(partner);
	//				});
	//
	//				//sorting is not required yet for order list
	//				order.set('partners', myPartners);
	//				//end partners assignment
				    //begin objlist assignment

					//begin address assignment
					if(addresses) {
						var addressNoToLookFor = '';
						if(order.get('equipment'))
							addressNoToLookFor = order.get('equipment').get('adrnr');

						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressNoToLookFor) && order.get('funcLoc'))
							addressNoToLookFor = order.get('funcLoc').get('adrnr');

						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressNoToLookFor))
                            addressNoToLookFor = order.get('adrnra');

						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressNoToLookFor)) {
							addresses.each(function(address, index, length) {
								if(address.get('adrnr') === addressNoToLookFor) {
									order.set('address', address);
									return false;
								}
							});
						} else if(order.get('partners') && order.get('partners').getCount() > 0) {
							order.set('address', AssetManagement.customer.manager.AddressManager.mapPartnerIntoAddress(order.get('partners').getAt(0)));
						}
					}
					//end address assignment

					//DISBALED
	//				//begin status assignment
	//				var myStatusList = Ext.create('Ext.data.Store', {
	//					model: 'AssetManagement.customer.model.bo.Status',
	//					autoLoad: false
	//				});
	//
	//				statusList.each(function(status, index, length) {
	//					if(status.get('objnr') === order.get('objnr'))
	//						myStatusList.add(status);
	//				});
	//
	//				order.set('statusList', myStatusList);
	//				//end status assignment

				    //set order's ilarts

                    //DISBALED

					var foundOrderIlart = null
					if(ordersIlarts && ordersIlarts.getCount() >0) {
                        ordersIlarts.each(function(orderIlart, index, length) {
                            if(orderIlart.get('ilart') === order.get('ilart'))
                                foundOrderIlart = orderIlart;
                            	return false;
                        });
					}

					order.set('orderIlart', foundOrderIlart);
					//end status assignment

                    //set order's local status

					AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(order);
				});

				var cache = this.getCache();
				cache.addStoreForAllToCache(orders, cache.self.DATAGRADES.LOW);

				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForOrders of BaseOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		loadDependendDataForOrder: function(order, eventIdToFireWhenComplete, lightVersion) {
			try {
				if(order) {
					var aufnr = order.get('aufnr');
					var objnr = order.get('objnr');

					//all of loadListDependendDataForOrders (but more specific and, party, with dependencies now)
					//additional longtext, matconfs, checklistStatus status, documents ...

					if(lightVersion === undefined)
						lightVersion = false;

					//lightversion currently contains: objectStatus

					var orderType = null;
					var orderIlart = null;
					var orderBemot = null;
					var opers = null;
					var notif = null;
					var equi = null;
					var floc = null;
					var address = null;
					var objectList = null;
					var objectStatusList = null;
					var components = null;
					var lumpSums = null;		//currently disabled
					var documents = null;
					var partners = null;
					var priority = null;
					var backendLongtext = null;
					var localLongtext = null;
					var checklistStatus = null;
					var sdOrders = null;
					var extConfirmations = null;
					var qplosStore = null;

					var counter = 0;
					var done = 0;
					var erroroccurred = false;
					var reported = false;

					var me = this;
					var eventController = AssetManagement.customer.controller.EventController.getInstance();

					var completeFunction = function() {
						if(erroroccurred === true && reported === false) {
							AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
							reported = true;
						} else if(counter === done && erroroccurred === false) {
							me.assignDependendDataForOrder.call(me, eventIdToFireWhenComplete, order, lightVersion, orderType,
									orderIlart, orderBemot, opers, notif, equi, floc, address, objectStatusList, components, lumpSums,
										documents, partners, priority, backendLongtext, localLongtext, checklistStatus, objectList, sdOrders, extConfirmations, qplosStore);
						}
					};

					//TO-DO make dependend of customizing parameters --- this definitely will increase performance

					//get orderType
					if(lightVersion === false) {
						var auart = order.get('auart');
						done++;

						var orderTypeSuccessCallback = function(ordType) {
							erroroccurred = ordType === undefined;

							orderType = ordType;
							counter++;

							completeFunction();
						};

						var eventId = AssetManagement.customer.manager.Cust_011Manager.getOrderType(auart, true);
						eventController.registerOnEventForOneTime(eventId, orderTypeSuccessCallback);
					}

				    //get qplos
					var qplosOnHeader = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_obj_list_active') === '';
					if (qplosOnHeader) {
					    var aufnr = order.get('aufnr');
					    done++;
                    
					    var qplosSuccessCallback = function (qplosRecieved) {
					        erroroccurred = qplosRecieved === undefined;
                    
					        qplosStore = qplosRecieved;
					        counter++;
                    
					        completeFunction();
					    };
                    
					    var eventId = AssetManagement.customer.manager.QPlosManager.getAllQPlosForOrder(aufnr, true);
					    eventController.registerOnEventForOneTime(eventId, qplosSuccessCallback);
					}
                    
					//get orderIlart
					if(lightVersion === false) {
						var auart = order.get('auart');
						var ilart = order.get('ilart');
						done++;

						var orderIlartSuccessCallback = function(ordIlart) {
							erroroccurred = ordIlart === undefined;

							orderIlart = ordIlart;
							counter++;

							completeFunction();
						};

						var eventId = AssetManagement.customer.manager.CustIlartOrTypeManager.getCustIlartOrType(auart, ilart, true);
						eventController.registerOnEventForOneTime(eventId, orderIlartSuccessCallback);
					}

					//get orderBemot
					if(lightVersion === false) {
						var bemot = order.get('bemot');
						done++;

						var orderBemotSuccessCallback = function(ordBemot) {
							erroroccurred = ordBemot === undefined;

							orderBemot = ordBemot;
							counter++;

							completeFunction();
						};

						var eventId = AssetManagement.customer.manager.CustBemotManager.getCustBemot(bemot, true);
						eventController.registerOnEventForOneTime(eventId, orderBemotSuccessCallback);
					}

					//get opers
					if(lightVersion === false) {
						done++;

						var operSuccessCallback = function(operations) {
							erroroccurred = operations === undefined;

							opers = operations;
							counter++;

							completeFunction();
						};

						var eventId = AssetManagement.customer.manager.OperationManager.getOperationsForOrder(aufnr, true);
						eventController.registerOnEventForOneTime(eventId, operSuccessCallback);
					}

					//get funLoc
					if(lightVersion === false) {
						var tplnr = order.get('tplnr');

						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
							done++;

							var flocSuccessCallback = function(funcLoc) {
								erroroccurred = funcLoc === undefined;

								floc = funcLoc;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
							eventController.registerOnEventForOneTime(eventId, flocSuccessCallback);
						}
					}

					//get equipment
					if(lightVersion === false) {
						var equnr = order.get('equnr');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
							done++;

							var equiSuccessCallback = function(equipment) {
								erroroccurred = equipment === undefined;

								equi = equipment;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
							eventController.registerOnEventForOneTime(eventId, equiSuccessCallback);
						}
					}

					//get notif
					if(lightVersion === false) {
						var qmnum = order.get('qmnum');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
							done++;

							var notifSuccessCallback = function(notification) {
								erroroccurred = notification === undefined;

								notif = notification;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnum, true);
							eventController.registerOnEventForOneTime(eventId, notifSuccessCallback);
						}
					}

					//get address
					if(lightVersion === false) {
						var adrnr = order.get('adrnra');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(adrnr)) {
							done++;

							var addressSuccessCallback = function(addresss) {
								erroroccurred = addresss === undefined;

								address = addresss;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.AddressManager.getAddress(adrnr, true);
							eventController.registerOnEventForOneTime(eventId, addressSuccessCallback);
						}
					}

					//get components
					if(lightVersion === false) {
						if(true) {
							done++;

							var componentSuccessCallback = function(comps) {
								erroroccurred = comps === undefined;

								components = comps;
								counter++;

								completeFunction();
							};

							var eventId = AssetManagement.customer.manager.ComponentManager.getComponentsForOrder(aufnr, true);
							eventController.registerOnEventForOneTime(eventId, componentSuccessCallback);
						}
					}

					//get local and backendlongtext
					if(lightVersion === false) {
						done++;

						var longtextSuccessCallback = function(localLT, backendLT) {
							erroroccurred = localLT === undefined || backendLT === undefined;
							localLongtext = localLT;
							backendLongtext = backendLT;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(order, true);
						eventController.registerOnEventForOneTime(eventId, longtextSuccessCallback);
					}

					//get partners
					if(lightVersion === false) {
						done++;

						var partnerSuccessCallback = function(partnerList) {
							erroroccurred = partnerList === undefined;

							partners = partnerList;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.PartnerManager.getAllPartnersForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
					}

					//get objectStatus
					if(true) {
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr)) {
							done++;

							var objectStatusSuccessCallback = function(objStatusList) {
								erroroccurred = objStatusList === undefined;

								objectStatusList = objStatusList;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject(objnr, true);
							eventController.registerOnEventForOneTime(eventId, objectStatusSuccessCallback);
						}
					}

					//get objectList
					if(lightVersion === false) {
						done++;

						var objectListSuccessCallback = function(objList) {
							erroroccurred = objList === undefined;

							objectList = objList;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.ObjectListManager.getObjectList(aufnr, true);
						eventController.registerOnEventForOneTime(eventId, objectListSuccessCallback);
					}


					//get priority
					if(lightVersion === false) {
						var artpr = order.get('artpr');
						var priok = order.get('priok');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(artpr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priok)) {
							done++;

							var prioritySuccessCallback = function(prio) {
								erroroccurred = prio === undefined;

								priority = prio;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.PriorityManager.getPriority(artpr, priok, true);
							eventController.registerOnEventForOneTime(eventId, prioritySuccessCallback);
						}
					}

					//get documents
					if(lightVersion === false) {
						documents = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.File',
							autoLoad: false
						});

						//get dms documents
						done++;

						var dmsDocsSuccessCallback = function(dmsDocs) {
							erroroccurred = dmsDocs === undefined;

							if(erroroccurred) {
								documents = null;
							} else if(documents && dmsDocs && dmsDocs.getCount() > 0) {
								dmsDocs.each(function(dmsDoc) {
									documents.add(dmsDoc);
								}, this);
							}

							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.DMSDocumentManager.getDmsDocumentsForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, dmsDocsSuccessCallback);

						//get doc upload records
						done++;

						var docUploadsSuccessCallback = function(docUploads) {
							erroroccurred = docUploads === undefined;

							if(erroroccurred) {
								documents = null;
							} else if(documents && docUploads && docUploads.getCount() > 0) {
								docUploads.each(function(docUpload) {
									documents.add(docUpload);
								}, this);
							}

							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.DocUploadManager.getDocUploadsForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, docUploadsSuccessCallback);
					}

					//get sd orders
					if(lightVersion === false) {
						done++;

						var sdOrdersSuccessCallback = function(sdOrds) {
							erroroccurred = sdOrds === undefined;

							sdOrders = sdOrds;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.SDOrderManager.getSDOrdersForOrder(aufnr, true);
						eventController.registerOnEventForOneTime(eventId, sdOrdersSuccessCallback);
					}

					//get ext confs
					if(lightVersion === false) {
						done++;

						var extConfOrdersSuccessCallback = function(extConfs) {
							erroroccurred = extConfs === undefined;

							extConfirmations = extConfs;
							counter++;

							completeFunction();
						};
						eventId = AssetManagement.customer.manager.BanfManager.getBanfPositions(aufnr, true);
						eventController.registerOnEventForOneTime(eventId, extConfOrdersSuccessCallback);
					}

					if(done > 0) {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					} else {
						completeFunction();
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForOrder of BaseOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		assignDependendDataForOrder: function(eventIdToFireWhenComplete, order, lightVersion, orderType, orderIlart, orderBemot,
								opers, notif, equi, floc, address, objectStatusList, components, lumpSums, documents,
									partners, priority, backendLongtext, localLongtext, checklistStatus, objectList, sdOrders, extConfirmations, qplosStore)
		{
			try
			{
				if(!order) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
					return;
				}
				//begin address assignment
				var addressToSet = null;

				if(equi)
					addressToSet = equi.get('address');

				if(!addressToSet && floc)
					addressToSet = floc.get('address');

				if(!addressToSet)
					addressToSet = address;

				if(!addressToSet && partners && partners.getCount() > 0)
					addressToSet = AssetManagement.customer.manager.AddressManager.mapPartnerIntoAddress(partners.getAt(0))

				order.set('address', addressToSet);
			    //end address assignment

			    //begin objectStatus assignment
				if (objectStatusList) {
				    var objectStatusStore = Ext.create('Ext.data.Store', {
				        model: 'AssetManagement.customer.model.bo.ObjectStatus',
				        autoLoad: false
				    });

                    //filter out all system status
				    objectStatusList.each(function (objectStatus) {
				        if (objectStatus.get('stat').indexOf('I') !== 0) {
				            objectStatusStore.add(objectStatus);
				        }
				    });

				    order.set('objectStatusList', objectStatusStore);
				}
			    //end objectStatus assignment

			    //begin qplos assignment
				var equnr = order.get('equnr');
				var tplnr = order.get('tplnr');

				if (qplosStore && qplosStore.getCount() > 0) {
				    qplosStore.each(function (qplosObject) {
				        var techObj = qplosObject.get('techObj');
				        var techObjClassName = Ext.getClassName(techObj);

				        if (techObjClassName === 'AssetManagement.customer.model.bo.Equipment' && techObj.get('equnr') === equnr) {
				            order.set('qplos', qplosObject);
				            return false;
				        } else if (techObjClassName === 'AssetManagement.customer.model.bo.FuncLoc' && techObj.get('tplnr') === tplnr) {
				            order.set('qplos', qplosObject);
				        }
				    });
				}
			    //end qplos assignment

				order.set('orderType', orderType);
				order.set('orderIlart', orderIlart);
				order.set('orderBemot', orderBemot);
				order.set('operations', opers);
				order.set('notif', notif);
				order.set('equipment', equi);
				order.set('funcLoc', floc);
				order.set('components', components);
				order.set('lumpSums', lumpSums);
				order.set('documents', documents);
				order.set('partners', partners);
				order.set('priority', priority);
				order.set('backendLongtext', backendLongtext);
				order.set('localLongtext', localLongtext);
				order.set('checklistStatus', checklistStatus);
				order.set('objectList', objectList);
				order.set('sdOrders', sdOrders);
				order.set('extConfirmations', extConfirmations);


                //set the order's local status
			    AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(order);

				var cache = this.getCache();
				var dataGrade = lightVersion === true ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				cache.addToCache(order, dataGrade);

				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, order);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForOrder of BaseOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		/**
		 * saves a new order with all it's dependant data to the database
		 */
		saveNewOrder: function(order) {
			var retval = -1;
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!order) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

                this.manageUpdateFlagForSaving(order);

				var saveLevel = 0;
				var maxLevel = 4;

				var operationsSaveCount = 0;
				var partnersSaveCount = 0;

				var me = this;

				var saveMethod = function() {
					//save the header
					if(saveLevel == 0) {
						var saveOrdHeadCallback = function(success) {
							saveLevel++;
							AssetManagement.customer.manager.OrderManager.updateOrderDependantDataAfterOrderSaving(order);
							saveMethod();
						};

						var eventId = AssetManagement.customer.manager.OrderManager.saveNewOrderHead(order, true);
						eventController.registerOnEventForOneTime(eventId, saveOrdHeadCallback);
					} else if(saveLevel == 1) {
						//save the operations
					    var operations = order.get('operations');
					    var operationsCount = operations ? operations.getCount() : 0;

					    if (operationsCount === 0) {
					        //no operations for saving => continue
					        saveLevel++;
					        saveMethod();
					    } else {
					        var saveOperationCallback = function (success) {
					            operationsSaveCount++;

					            if (operationsSaveCount == operationsCount)
					                saveLevel++;

					            saveMethod();
					        };
                            
					        if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operations.getAt(operationsSaveCount).get('aufnr'))){
					            operations.getAt(operationsSaveCount).set('aufnr', order.get('aufnr'));
					        }
					        var eventId = AssetManagement.customer.manager.OperationManager.saveOperation(operations.getAt(operationsSaveCount), true);
					        eventController.registerOnEventForOneTime(eventId, saveOperationCallback);
					    }
					} else if(saveLevel == 2) {
						//save the notification
						var saveOrderNotificationCallback = function(success) {
							saveLevel++;
							saveMethod();
						};

						var notification = order.get('notif');

						if (notification) {
                            //link the notification to the order, using the order's temporary order number and mobile key
						    notification.set('aufnr', order.get('aufnr'));
						    notification.set('mobileKeyOrder', order.get('mobileKey'));

						    var eventId = AssetManagement.customer.manager.NotifManager.saveNotification(notification);
						    eventController.registerOnEventForOneTime(eventId, saveOrderNotificationCallback, true);
						} else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('orderType').get('qmart'))) {
						    //notification not set, but required
						    var notification = AssetManagement.customer.manager.OrderManager.createNewOrderNotificationObject(order);
						    order.set('notif', notification);

						    var eventId = AssetManagement.customer.manager.NotifManager.saveNotification(notification, true);
						    eventController.registerOnEventForOneTime(eventId, saveOrderNotificationCallback);

						} else {
						    saveOrderNotificationCallback();
						}
					} else if(saveLevel == 3) {
						//save the local longtext
					    var saveOrderLongtextCallback = function (success) {
					        saveLevel++;
					        saveMethod();
					    };

					    var localLongtext = order.get('localLongtext');

					    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(localLongtext)) {
					        var eventId = AssetManagement.customer.manager.LongtextManager.saveLocalLongtext(localLongtext, order, true);
					        eventController.registerOnEventForOneTime(eventId, saveOrderLongtextCallback, true);
					    } else {
					        saveOrderLongtextCallback();
					    }
					} else if (saveLevel === 4) {
					    //save the partners
					    var partners = order.get('partners');
					    var partnersCount = partners ? partners.getCount() : 0;

					    if (partnersCount === 0) {
					        //no partners for saving => continue
					        saveLevel++;
					        saveMethod();
					    } else {
					        var savePartnerCallback = function (partner) {
					            partnersSaveCount++;

					            if (partnersSaveCount === partnersCount) {
					                saveLevel++;
					            }

					            saveMethod();
					        };

					        eventId = AssetManagement.customer.manager.PartnerManager.savePartnerForObject(partners.getAt(partnersSaveCount), true);
					        eventController.registerOnEventForOneTime(eventId, savePartnerCallback);
					    }
					}

					if(saveLevel > maxLevel) {
						//do not specify a datagrade, so all depenend data will loaded, when necessary
					    me.getCache().addToCache(order);

					    eventController.requestEventFiring(me.EVENTS.ORDER_ADDED, order);

					    eventController.requestEventFiring(retval, order);
					} else {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					}
				}

				saveMethod();
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrder of BaseOrderManager', ex);
			    retval = -1;
			}

			return retval;
		},

		/**
		 * saves the order header object to the database
		 */
		saveNewOrderHead: function(order, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!order) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

				var requiresNewAufnr = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('aufnr'));

				var me = this;

				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewAufnr && nextCounterValue !== -1) {
							var counter = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 10);
							order.set('aufnr', counter);
							order.set('objnr', "OR" + order.get('aufnr'));						
						}

						if (order.get('notif') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('qmnum'))) {
						    order.set('qmnum', order.get('notif').get('qmnum'));
						}

						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('aufnr'))) {
							eventController.requestEventFiring(retval, false);
							return;
						}

					    //set id
						order.set('id', order.get('aufnr'));

					    //set mobileKey - only get a new mobilekey if the mobilekey was not set before
                        if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('mobileKey')))
						    order.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([ order.get('aufnr') ]));

						//get a unique new rsnum for order if necessary
						var requiresNewRsnum = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('rsnum'));

						var setRsnumFunction = function(newRsnum) {
							try {
							    if (requiresNewRsnum && newRsnum !== -1) {
							        var counter = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(newRsnum, '0', 10);
									order.set('rsnum', counter);
								}

								var callback = function(eventArgs) {
									try {
										eventController.requestEventFiring(retval, eventArgs.type === "success");
									} catch(ex) {
										AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrderHead of BaseOrderManager', ex);
										eventController.requestEventFiring(retval, false);
									}
								};

								var toSave = me.buildDataBaseObjectForOrderHeader(order);
								AssetManagement.customer.core.Core.getDataBaseHelper().put('D_ORDHEAD', toSave, callback, callback);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrderHead of BaseOrderManager', ex);
								eventController.requestEventFiring(retval, false);
							}
						};

						if(!requiresNewRsnum) {
							setRsnumFunction();
						} else {
							var eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_ORDCOMP', false);

							if(eventId > 1)
								eventController.registerOnEventForOneTime(eventId, setRsnumFunction);
							else
								eventController.requestEventFiring(retval, false);
						}
					} catch(ex) {
						eventController.requestEventFiring(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrderHead of BaseOrderManager', ex);
					}
				};

				if(!requiresNewAufnr) {
					saveFunction();
				} else {
					eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_ORDHEAD', false);

					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrderHead of BaseOrderManager', ex);
			    retval = -1;
			}

			return retval;
		},

		buildDataBaseObjectForOrderHeader: function(order) {
            var retval = null;

	        try {
	        	var ac = AssetManagement.customer.core.Core.getAppConfig();
				retval = { };

				retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['UPDFLAG'] =  order.get('updFlag');
				retval['AUFNR'] = order.get('aufnr');
				retval['PRIOK'] = order.get('priok');
				retval['EQUNR'] = order.get('equnr');
				retval['BAUTL'] = order.get('bautl');
				retval['IWERK'] = order.get('iwerk');
				retval['INGRP'] = order.get('ingrp');
				retval['PM_OBJTY'] = order.get('pmobjty');
				retval['GEWRK'] = order.get('gewrk');
				retval['KUNUM'] = order.get('kunum');
				retval['QMNUM'] = order.get('qmnum');
				retval['SERIALNR'] = order.get('serialnr');
				retval['SERMAT'] = order.get('sermat');
				retval['AUART'] = order.get('auart');
				retval['KTEXT'] = order.get('ktext');
				retval['VAPLZ'] = order.get('vaplz');
				retval['WAWRK'] = order.get('wawrk');
				retval['TPLNR'] = order.get('tplnr');
				retval['OBJNR'] = order.get('objnr');
				retval['ILART'] = order.get('ilart');
				retval['RSNUM'] = order.get('rsnum');
				retval['FMAT'] = order.get('fmat');
				retval['BEMOT'] = order.get('bemot');
				retval['BEMOT_TXT'] = order.get('bemottxt');
				retval['ADRNRA'] = order.get('adrnra');
				retval['STSMA'] = order.get('stsma');
				retval['GSBER'] = order.get('gsber');
				retval['PRCTR'] = order.get('prctr');
				retval['VKORG'] = order.get('vkorg');
				retval['VTWEG'] = order.get('vtweg');
				retval['SPART'] = order.get('spart');
				retval['VKGRP'] = order.get('vkgrp');
				retval['VKBUR'] = order.get('vkbur');
				retval['BSTKD'] = order.get('bstkd');
				retval['BSTDK'] = order.get('bstdk');
				retval['MATNR'] = order.get('matnr');
				retval['MENGE'] = order.get('menge');
				retval['MEINS'] = order.get('meins');
				retval['FAKTF'] = order.get('faktf');
				retval['ARTPR'] = order.get('artpr');
				retval['KONTY'] = order.get('konty');
				retval['EMPGE'] = order.get('empge');
				retval['ANLZU'] = order.get('anlzu');
				retval['TIDNR'] = order.get('tidnr');
				retval['STRNO'] = order.get('strno');
				retval['GLUZP'] = order.get('gluzp');
				retval['GSUZP'] = order.get('gsuzp');

				//DateTimes
				retval['FSAVD'] = order.get('fsav');
				retval['FSAVZ'] = order.get('fsav');
				retval['FSEDD'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(order.get('fsed'));
				retval['FSEDZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(order.get('fsed'));
				retval['NTANF'] = order.get('ntan');
				retval['NTANZ'] = order.get('ntan');
				retval['NTEND'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(order.get('nten'));
				retval['NTENZ'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(order.get('nten'));
				retval['GLTRP'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(order.get('gltrp'));
				retval['GSTRP'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(order.get('gstrp'));

				retval['GSTRS'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(order.get('gstrs'));
				retval['GSUZS'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(order.get('gsuzs'));
				retval['GLTRS'] =  AssetManagement.customer.utils.DateTimeUtils.getDateString(order.get('gltrs'));
				retval['GLUZS'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(order.get('gluzs'));

				

				retval['KOSTL'] = order.get('kostl');

				retval['USR01FLAG'] = order.get('usr01Flag');
				retval['USR02FLAG'] = order.get('usr02Flag');
				retval['USR03CODE'] = order.get('usr03Code');
				retval['USR04CODE'] = order.get('usr04Code');
				retval['USR05TXT'] = order.get('usr05Txt');
				retval['USR06TXT'] = order.get('usr06Txt');
				retval['USR07TXT'] = order.get('usr07Txt');
				retval['USR08DATE'] = order.get('usr08Date');
				retval['USR09NUM'] = order.get('usr09Num');

				retval['MOBILEKEY'] = order.get('mobileKey');
				retval['MOBILEKEY_QM'] = order.get('mobileKeyNotif');
            } catch(ex) {
	            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForOrderHeader of BaseOrderManager', ex);
            }

			return retval;
		},

		/**
		 * updates the dependant objects of the order with the data set during the saving of the order head
		 * e.g. the aufnr or the mobilekey
		 */
		updateOrderDependantDataAfterOrderSaving: function(order) {
		    try {
		        //update notif
		        var notification = order.get('notif');

		        if (notification) {
		            //link the notification to the order, using the order's number and mobile key
		            notification.set('aufnr', order.get('aufnr'));
		            notification.set('mobileKeyOrder', order.get('mobileKey'));
		        }

			    //update operations
			    var operations = order.get('operations');

			    if (operations && operations.getCount() > 0) {
			        operations.each(function (operation) {
			            operation.set('qmnum', order.get('qmnum'));
			            operation.set('mobileKey', order.get('mobileKey'));
			        }, this);
			    }

			    //update partners
			    var partners = order.get('partners');

			    if (partners && partners.getCount() > 0) {
			        partners.each(function (partner) {
			            partner.set('objnr', 'OR' + order.get('aufnr'));
			            partner.set('mobileKey', order.get('mobileKey'));
			        }, this);
			    }
			} catch(ex){
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateOrderDependantDataAfterOrderSaving of BaseOrderManager', ex);
			}

			return order;
		},

		/**
		 * creates a notification object for the order to be saved
		 */
		createNewOrderNotificationObject: function(order) {
			var notif = null;

			try {
				notif = Ext.create('AssetManagement.customer.model.bo.Notif');
				notif.set('qmart', order.get('orderType').get('qmart'));
				notif.set('qmtxt', order.get('ktext'));
				notif.set('aufnr', order.get('aufnr'));
				notif.set('equnr', order.get('equnr'));
				notif.set('tplnr', order.get('tplnr'));
				notif.set('iwerk', order.get('iwerk'));
				notif.set('mobileKeyOrder', order.get('mobileKey'));

				notif.set('updFlag', order.get('updFlag'));

				//if partners have been set, move the partners store to the notification
				var orderPartners = order.get('partners');

				if (orderPartners && orderPartners.getCount() > 0) {
				    var notifsPartners = Ext.create('Ext.data.Store', {
				        model: 'AssetManagement.customer.model.bo.Partner',
                        autoLoad: false
				    });

					orderPartners.each(function (partner) {
					    notifsPartners.add(partner);
					});

					orderPartners.removeAll();

					notifsPartners.add(records);
					notif.set('partners', notifsPartners);
				}

				notif.set('order', order);
            } catch(ex) {
	            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createNewOrderNotificationObject of BaseOrderManager', ex);
            }

			return notif;
		},

		/**
		 * saves a changed order header to the database
		 */
		saveChangedOrderHeader: function(order) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!order || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('aufnr'))
							|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('updFlag'))) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

				var me = this;

				var callback = function(eventArgs) {
				    try {
				        var success = eventArgs.type === "success";

				        if (success) {
				            AssetManagement.customer.manager.LocalOrderStatusManager.evaluateAndSetLocalStatus(order);
				            eventController.requestEventFiring(me.EVENTS.ORDER_CHANGED, order);
				        }

				        eventController.requestEventFiring(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveChangedOrderHeader of BaseOrderManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				};

				//update the updFlag if necessary
				this.manageUpdateFlagForSaving(order);

				var toSave = me.buildDataBaseObjectForOrderHeader(order);

				if(order.get('updFlag') === 'U')
					AssetManagement.customer.core.Core.getDataBaseHelper().update('D_ORDHEAD', null, toSave, callback, callback);
				else
					AssetManagement.customer.core.Core.getDataBaseHelper().put('D_ORDHEAD', toSave, callback, callback);
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveChangedOrderHeader of BaseOrderManager', ex);
			    retval = -1;
			}

			return retval;
		},

		saveOrderBemotChange: function (order, newBemot) {
		    var retval = -1;

		    try {
		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        retval = eventController.getNextEventId();

		        var formerBemot = order.get('bemot');
		        order.set('bemot', newBemot);

		        var saveCallback = function (success) {
		            try {
		                success = success === true;

		                if (!success) {
		                    order.set('bemot', formerBemot);
		                    eventController.requestEventFiring(retval, success);
		                } else {
		                    //try to adjust order's account indication reference - on errors it will be set to null
		                    var newAccountIndi = null;

		                    var accIndiCallback = function (accountIndi) {
		                        try {
		                            if (!accountIndi)
		                                accountIndi = null;

		                            order.set('orderBemot', accountIndi);
		                        } catch (ex) {
		                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOrderBemotChange of BaseOrderManager', ex);
		                        } finally {
		                            eventController.requestEventFiring(retval, true);
		                        }
		                    };

		                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(newBemot)) {
		                        var accIndiEventId = AssetManagement.customer.manager.CustBemotManager.getCustBemot(newBemot);

		                        if (accIndiEventId > -1) {
		                            eventController.registerOnEventForOneTime(accIndiEventId, accIndiCallback);
		                        } else {
		                            accIndiCallback(undefined);
		                        }
		                    } else {
		                        accIndiCallback(null);
		                    }
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOrderBemotChange of BaseOrderManager', ex);
		                eventController.requestEventFiring(retval, false);
		            }
		        };

		        var saveEventId = this.saveChangedOrderHeader(order);

		        if (saveEventId > -1) {
		            eventController.registerOnEventForOneTime(saveEventId, saveCallback);
		        } else {
		            saveCallback(false);
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOrderBemotChange of BaseOrderManager', ex);
		    }

		    return retval;
		},

	    /*
         * Implementation of oxando completion paradigma
         */

        /*
         * IMPLEMENTATION
         * Marks all related objects as completed by changing the Updateflag from A to I or U
         */
        markAsCompleted: function () {
            try {
                if(this.manualCompletionRequired) {
                    var uncompletedObjects = this.getAllUncompletedObjects();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside markAsCompleted of BsaeOrderManager', ex);
            }
        },

        /*
         * returns the operation object form the orders operation store matching the vornr and splitnr
         */
        getOrderOperation: function (order, vornr, split) {
            var retval = null;

            try {
                if(order && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr)) {
                    var opers = order.get('operations');

                    if (opers) {
					    opers.each(function(oper, index, length) {
					        if (oper.get('vornr') === vornr) {
					            if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(split)) {
					                if(oper.get('split') === split) {
					                    retval = oper;
                                        return false;
					                }
					            }
					        }
					    });
				    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderOperation of BaseOrderManager', ex);
            }

            return retval;
        },

        deleteOrder: function () {
             /**
		     * deletes a order with all it's dependant data from the database
             * dependant data: operations, longtext, timeconfs, matconf, components, files, partners, notif
		     */ 
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if(!order) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var deleteLevel = 0;
                var maxLevel = 7;

                var deleteMethod = function() {
                    //save the header
                    if (deleteLevel === 0) {
                        //delete the operations
                        var operations = order.get('operations');
                        var operationsCount = operations ? operations.getCount() : 0;

                        if (operationsCount === 0) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteOperationsCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.OperationManager.deleteOperation(operations.getAt(operationsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteOperationsCallback);
                        }
                    }  else if (deleteLevel === 1) {
                        //delete the components
                        var components = order.get('components');
                        var componentsCount = components ? components.getCount() : 0;

                        if (componentsCount === 0) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteComponentsCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.ComponentManager.deleteComponent(components.getAt(componentsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteComponentsCallback);
                        }
                    } else if (deleteLevel === 2) {
                        //delete the orderPartners
                        var orderPartners = order.get('partners');
                        var partnersCount = orderPartners ? orderPartners.getCount() : 0;

                        if (partnersCount === 0) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deletePartnerCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.PartnerManager.deletePartnerForObject(orderPartners.getAt(partnersCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deletePartnerCallback);
                        }
                    } else if (deleteLevel === 3) {
                        //delete the documents
                        var documents = order.get('documents');
                        var documentsCount = documents ? documents.getCount() : 0;

                        if (documentsCount === 0) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteDocumentCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(documents.getAt(documentsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteDocumentCallback);
                        }
                    } else if (deleteLevel === 4) {
                        //delete the longtext
                        var longtext = order.get('localLongtext');

                        if (!longtext) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteLongtextCallback = function (success) {
                                deleteLevel++;
                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(order, true);
                            eventController.registerOnEventForOneTime(eventId, deleteLongtextCallback);
                        }
                    } else if (deleteLevel === 5) {
                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('orderType').get('qmart')) && order.get('notif') && order.get('notif').get('qmnum').indexOf('MO') !== -1
                        && order.get('orderType').get('qmart') === order.get('notif').get('qmart')) {
                            //delete notification
                            var notif = order.get('notif');

                            if (!notif) {
                                deleteLevel++;
                                deleteMethod();
                            } else {
                                var deleteNotifCallback = function (success) {
                                    deleteLevel++;
                                    deleteMethod();
                                };

                                eventId = AssetManagement.customer.manager.NotifManager.deleteNotification(notif, true);
                                eventController.registerOnEventForOneTime(eventId, deleteNotifCallback);
                            }
                        }
                    }else if (deleteLevel === 6) {
                        var deleteOrderHeadCallBack = function (success) {
                            deleteLevel++;
                            deleteMethod();
                        };

                        var eventId = AssetManagement.customer.manager.OrderManager.deleteOrdHead(order, true);
                        eventController.registerOnEventForOneTime(eventId, deleteOrderHeadCallBack);
                    } 

                    if (deleteLevel >= maxLevel) {
                        eventController.requestEventFiring(retval, order);
                    } else {
                        AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                    }
                }

                deleteMethod();
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOrder of BaseOrderManager', ex);
                retval = -1;
            }

            return retval;
            
        },

        deleteOrdHead: function (order, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!order) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(order);
                            me.getCache().removeFromCache(order);

                            eventController.requestEventFiring(me.EVENTS.ORDER_DELETED, order);
                        }

                        eventController.requestEventFiring(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOrdHead of BaseOrderManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                //get the key of the order to delete
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_ORDHEAD', order);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_ORDHEAD', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOrdHead of BaseOrderManager', ex);
                retval = -1;
            }

            return retval;
        }
	}
});
