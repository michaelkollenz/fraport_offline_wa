Ext.define('AssetManagement.base.manager.BaseCust_013Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Cust_013',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public
		getCust_013s: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Cust_013',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_013StoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_013s of BaseCust_013Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_013', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_013', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_013s of BaseCust_013Manager', ex);
			}
	
			return retval;
		}, 
	
	
        getCust_013: function(werks, katalogart, auswahlmge, funktion, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
			
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)
					  	|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(katalogart)
					  		|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(auswahlmge)
					  			|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(funktion)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
	
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_013 = me.buildCust_013DataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, cust_013);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_013 of BaseCust_013Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
		
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('WERKS', werk);
				keyMap.add('KATALOGART', katalogart);
				keyMap.add('AUSWAHLMGE', auswahlmge);
				keyMap.add('FUNKTION', funktion);
				
			    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_013', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_013', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_013 of BaseCust_013Manager', ex);
			}
	
	    	return retval;
		},
	
	     //private
	    buildCust_013DataBaseQuery: function(eventArgs) {
	    	var retval = null;
	    		    	
	    	try {
	    		if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCust_013FromDbResultObject(eventArgs.target.result.value);					
					}
				}
		    } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_013DataBaseQuery of BaseCust_013Manager', ex);
			}
			return retval;
	    },

	    buildCust_013StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_013 = this.buildCust_013FromDbResultObject(cursor.value);
						store.add(cust_013);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_013StoreFromDataBaseQuery of BaseCust_013Manager', ex);
			}
	    },

	    buildCust_013FromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Cust_013', {
					werks: dbResult['WERKS'],
					katalogart: dbResult['KATALOGART'],
				    auswahlmge: dbResult['AUSWAHLMGE'],
				    funktion: dbResult['FUNKTION'],
			        codegruppe: dbResult['CODEGRUPPE'],
			        code: dbResult['CODE'],
		            updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['WERKS'] + dbResult['KATALOGART'] + dbResult['AUSWAHLMGE'] + dbResult['FUNKTION']);
						
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_013FromDbResultObject of BaseCust_013Manager', ex);
			}
			return retval;
		}
	}
});