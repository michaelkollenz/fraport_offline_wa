Ext.define('AssetManagement.base.manager.BaseCustBersManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.CustBers',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public
		getCustBers: function(rbnr, katalogart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(rbnr)
				    || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(katalogart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var custBerStore = this.getFromCache(rbnr, katalogart);
					
				if(custBerStore) {
					eventController.requestEventFiring(retval, custBerStore);
					return retval;
				}
				
				custBerStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.CustBers',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						
						me.buildCustBersStoreFromDataBaseQuery.call(me, custBerStore, eventArgs);
						
						if(done) {
							me.addToCache(rbnr, katalogart, custBerStore)
							eventController.fireEvent(retval, custBerStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustBers of BaseCustBersManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('RBNR', rbnr);
				keyMap.add('QKATART', katalogart);
				
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('G_CUSTBERS', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('G_CUSTBERS', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustBers of BaseCustBersManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCustBersStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var custBers = this.buildCustBersFromDbResultObject(cursor.value);
						store.add(custBers);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustBersStoreFromDataBaseQuery of BaseCustBersManager', ex);
			}
		},
		
		buildCustBersFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.CustBers', {
					rbnr: dbResult['RBNR'],
					qkatart: dbResult['QKATART'],
				    qcodegrp: dbResult['QCODEGRP'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['RBNR'] + dbResult['QKATART'] + dbResult['QCODEGRP']);
				
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustBersFromDbResultObject of BaseCustBersManager', ex);
			}
			return retval;
		},
		
		//cache administration
		_custBersCache: null,
	
		getCache: function() {
	        try {
	        	if(!this._custBersCache) {
					this._custBersCache = Ext.create('Ext.util.HashMap');
					
					this.initializeCacheHandling();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustBersManager', ex);
			}
			
			return this._custBersCache;
		},
	
		clearCache: function() {
	        try {
			   this.getCache().clear();
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseCustBersManager', ex);
		    }
		},
				
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseCustBersManager', ex);
			} 
		},
		
		addToCache: function(rbnr, katalogart, custBers) {
			try {
				this.getCache().add(rbnr + katalogart, custBers);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseCustBersManager', ex);
			}
		},
		
		getFromCache: function(rbnr, katalogart) {
			var retval = null;
			
			try {
				retval = this.getCache().get(rbnr + katalogart);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseCustBersManager', ex);
			}
			
			return retval;
		}
	}
});