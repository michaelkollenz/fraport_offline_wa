﻿Ext.define('AssetManagement.base.manager.BaseMeasurementPointManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.MeasurementPoint',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.manager.cache.MeasPointCache',
        'AssetManagement.customer.manager.MeasurementDocManager',
        'AssetManagement.customer.model.bo.MeasurementDocHistory',
        'AssetManagement.customer.manager.HistMeasDocManager'
    ],

    inheritableStatics: {

        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.MeasPointCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseMeasurementPointManager', ex);
            }

            return retval;
        },

        getMeasurementPoints: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                //check if the cache can delivery the complete dataset
                var cache = this.getCache();
                var fromCache = cache.getStoreForAll();

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildMeasurementPointStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {

                            //add the whole store to the cache
                            cache.addStoreForAllToCache(fromDataBase);

                            var toReturn = cache.getStoreForAll();

                            me.loadListDependendDataForMeasPoints.call(me, retval, toReturn);
                            //return a store from cache to eliminate duplicate objects
                            eventController.requestEventFiring(retval, toReturn);
                        }

                        if (done) {

                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasurementPoints of BaseMeasurementPointManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MEASPOINT', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MEASPOINT', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasurementPoints of BaseMeasurementPointManager', ex);
                retval = -1;
            }

            return retval;
        },
        /*

        //public
        getMeasurementPoints: function (withDependendData, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
				
                var cache = this.getCache();
                var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
                var fromCache = cache.getStoreForAll(requestedDataGrade);
				
                if(fromCache) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }
				
                var baseStore = null;
				
                //if with dependend data is requested, check, if the cache may deliver the base store
                //if so, pull all instances from cache, with a too low data grade
                if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
                    baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
                    this.loadListDependendDataForMeasPoints(retval, baseStore);
                } else {
                    baseStore = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                        autoLoad: false
                    });
					
                    var me = this;
                    var successCallback = function(eventArgs) {
                        try {
                            var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						
                            me.buildMeasurementPointStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
                            if(done) {
                                cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
							
                                if(withDependendData) {
                                    //before proceeding pull all instances from cache, with a too low data grade
                                    //else the wrong instances would be filled with data
                                    baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
                                    me.loadListDependendDataForMeasPoints.call(me, retval, baseStore);
                                } else {
                                    //return a store from cache to eliminate duplicate objects
                                    var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
                                    eventController.requestEventFiring(retval, toReturn);
                                }
                            }
                        } catch(ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPoints of BaseMeasurementPointManager', ex);
                            eventController.requestEventFiring(retval, undefined);
                        }
                    };
					
                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MEASPOINT', null);
                    AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MEASPOINT', keyRange, successCallback, null, useBatchProcessing);
                }


           
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPoints of BaseMeasurementPointManager', ex);
            }

            return retval;
        },
        */
        getMeasPointsForFuncLoc: function (tplnr, withDependendData, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                //check if the cache can deliver func. loc.'s hist orders
                var cache = this.getCache();
                var fromCache = cache.getFromCacheForFuncLoc(tplnr);

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildMeasurementPointStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            //add func. loc.'s meas points to cache
                            cache.addToCacheForFuncLoc(tplnr, fromDataBase);

                            //return a store from cache to eliminate duplicate objects
                            var toReturn = cache.getFromCacheForFuncLoc(tplnr);
                            me.loadListDependendDataForMeasPoints.call(me, retval, toReturn);

                            eventController.requestEventFiring(retval, toReturn);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPointsForFuncLoc of BaseMeasPointManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };
                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('MPOBJ', tplnr);

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_MEASPOINT', 'MPOBJ', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_MEASPOINT', 'MPOBJ', indexRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPointsForFuncLoc of BaseMeasPointManager', ex);
            }

            return retval;
        },

        getMeasPointsForEquipment: function (equnr, withDependendData, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                //check if the cache can deliver equipment's meas points
                var cache = this.getCache();
                var fromCache = cache.getFromCacheForEquipment(equnr);

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildMeasurementPointStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            //add equipment's hist orders to cache
                            cache.addToCacheForEquipment(equnr, fromDataBase);
                            //return a store from cache to eliminate duplicate objects
                            var toReturn = cache.getFromCacheForEquipment(equnr);
                            me.loadListDependendDataForMeasPoints.call(me, retval, toReturn);

                            eventController.requestEventFiring(retval, toReturn);

                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPointsForEquipments of BaseMeasurementPointManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };
                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('MPOBJ', equnr);

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_MEASPOINT', 'MPOBJ', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_MEASPOINT', 'MPOBJ', indexRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPointsForEquipments of BaseMeasurementPointManager', ex);
            }

            return retval;
        },

        loadListDependendDataForMeasPoints: function (eventIdToFireWhenComplete, measPoints) {

            try {
                //do not continue if there is no data
                if (measPoints.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, measPoints);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //before the data can be assigned, the lists have to be loaded by all other managers
                //load these data flat!
                //register on the corresponding events
                var funcLocs = null;
                var equipments = null;
                var measurementDocs = null;
                var measDocHistory = null;


                //TO-DO make dependend of customizing parameters --- this definitely will increase performance

                var done = 0;
                var counter = 0;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignListDependendDataForMeasPoints.call(me, eventIdToFireWhenComplete, measPoints, equipments, funcLocs, measurementDocs, measDocHistory);
                    }
                };

                //get equis
                if (true) {
                    done++;

                    var equiSuccessCallback = function (equis) {
                        errorOccurred = equis === undefined;

                        equipments = equis;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.EquipmentManager.getEquipments(false, true);
                    eventController.registerOnEventForOneTime(eventId, equiSuccessCallback);
                }

                //get measDocs
                if (true) {
                    done++;

                    var measDocSuccessCallback = function (measDocs) {
                        errorOccurred = measDocs === undefined;
                        measurementDocs = measDocs;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.MeasurementDocManager.getMeasDocs(true);
                    eventController.registerOnEventForOneTime(eventId, measDocSuccessCallback);
                }

                //get funcLocs
                if (true) {
                    done++;

                    var funcLocSuccessCallback = function (flocs) {
                        errorOccurred = flocs === undefined;

                        funcLocs = flocs;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(false, true);
                    eventController.registerOnEventForOneTime(eventId, funcLocSuccessCallback);
                }

                //get measDocHistory
                if (true) {
                    done++;

                    var measDocHistorySuccessCallback = function (histMeasDocs) {
                        errorOccurred = histMeasDocs === undefined;

                        measDocHistory = histMeasDocs;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.HistMeasDocManager.getHistMeasDocs(false, true);
                    eventController.registerOnEventForOneTime(eventId, measDocHistorySuccessCallback);
                }

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForMeasPoints of BaseMeasPointManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }

        },

        //call this function, when all necessary data has been collected
        assignListDependendDataForMeasPoints: function (eventIdToFireWhenComplete, measPoints, equipments, funcLocs, measDocs, histMeasDocs) {
            try {
                measPoints.each(function (measPoint, index, length) {
                    var objnr = measPoint.get('mpobj');

                    //assign funcLocs
                    if (funcLocs) {

                        funcLocs.each(function (funcLoc) {
                            if (funcLoc.get('objnr') === objnr)
                                measPoint.set('funcLoc', funcLoc);
                        });
                    }

                    if (measDocs) {

                        var measPointMeasDocs = Ext.create('Ext.data.Store', {

                            model: 'AssetManagement.customer.model.bo.MeasurementDoc',
                            autoLoad: false
                        });

                        measPoint.set('measDocs', measPointMeasDocs);

                        measDocs.each(function (measDoc) {
                            if (measDoc.get('point') === measPoint.get('point'))
                                measPointMeasDocs.add(measDoc);
                        });

                        if (measPointMeasDocs.getCount() > 0) {
                            measPointMeasDocs.sort([
                                {
                                    property: 'itime',
                                    direction: 'DESC'
                                }
                            ]);

                            measPoint.set('lastRec', measPointMeasDocs.getAt(0).get('recdc'));
                        }

                        //remove assigned measDocs
                        measPointMeasDocs.each(function (measDoc) {
                            measDocs.remove(measDoc);
                        });
                    }

                    if (histMeasDocs) {
                        var measPointMeasDocsHist = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.MeasurementDocHistory',
                            autoLoad: false
                        });

                        measPoint.set('histMeasDocs', measPointMeasDocsHist);

                        histMeasDocs.each(function (histMeasDoc) {
                            if (histMeasDoc.get('point') === measPoint.get('point'))
                                measPointMeasDocsHist.add(histMeasDoc);
                        });

                        //remove assigned measDocs
                        measPointMeasDocsHist.each(function (measDoc) {
                            histMeasDocs.remove(measDoc);
                        });

                    }

                    //assign equipments
                    if (equipments) {

                        equipments.each(function (equipment) {
                            if (equipment.get('objnr') === objnr)
                                measPoint.set('equipment', equipment);
                        });
                    }
                });

                //var cache = this.getCache();
                //cache.addStoreForAllToCache(measPoints, cache.self.DATAGRADES.LOW);

                //return a store from cache to eliminate duplicate objects
                //var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, measPoints);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForMeasPoints of BaseMeasurementPointManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        getMeasPointsForTechObject: function (mpobj, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mpobj)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var fromCache = this.getFromCacheForMpobj(mpobj);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);

                    return retval;
                }

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementPoint',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildMeasurementPointStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            me.addToCache(point, theStore);

                            eventController.fireEvent(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPointsforObj of BaseMeasurementPointManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('MPOBJ', mpobj);

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_MEASPOINT', 'MPOBJ', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_MEASPOINT', 'MPOBJ', indexRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasPointsForTechObjWithIndex of BaseMeasurementPointManager', ex);
            }

            return retval;
        },

        getMeasurementPoint: function (point, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(point)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var fromCache = this.getFromCache(point);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);

                    return retval;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var measpoint = me.buildMeasurementPointDataBaseQuery.call(me, eventArgs);
                        eventController.requestEventFiring(retval, measpoint);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasurementPoint of BaseMeasurementPointManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('POINT', point);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MEASPOINT', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MEASPOINT', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMeasurementPoint of BaseMeasurementPointManager', ex);
            }

            return retval;
        },

        //private
        buildMeasurementPointDataBaseQuery: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildMeasurementPointFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMeasurementPointDataBaseQuery of BaseMeasurementPointManager', ex);
            }

            return retval;
        },

        buildMeasurementPointStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var measpoint = this.buildMeasurementPointFromDbResultObject(cursor.value);
                        store.add(measpoint);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMeasurementPointStoreFromDataBaseQuery of BaseMeasurementPointManager', ex);
            }
        },

        buildMeasurementPointFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.MeasurementPoint', {
                    point: dbResult['POINT'],
                    mpobj: dbResult['MPOBJ'],
                    psort: dbResult['PSORT'],
                    pttxt: dbResult['PTTXT'],
                    locas: dbResult['LOCAS'],
                    atinn: dbResult['ATINN'],
                    mrmin: dbResult['MRMIN'],
                    mrmax: dbResult['MRMAX'],
                    mrngu: dbResult['MRNGU'],
                    desir: dbResult['DESIR'],
                    indct: dbResult['INDCT'],
                    indrv: dbResult['INDRV'],
                    lastRec: dbResult['LAST_REC'],
                    lastUnit: dbResult['LAST_UNIT'],
                    mptyp: dbResult['MPTYP'],
                    codct: dbResult['CODCT'],
                    codgr: dbResult['CODGR'],
                    mseht: dbResult['MSEHT'],
                    mseh6: dbResult['MSEH6'],
                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['POINT']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMeasurementPointFromDbResultObject of BaseMeasurementPointManager', ex);
            }

            return retval;
        }
    }
});