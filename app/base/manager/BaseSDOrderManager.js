Ext.define('AssetManagement.base.manager.BaseSDOrderManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.SDOrderCache',
	    'AssetManagement.customer.manager.SDOrderTypeManager',
	    'AssetManagement.customer.manager.SDOrderItemManager',
	    'AssetManagement.customer.manager.OrderManager',
	    'AssetManagement.customer.manager.NotifManager',
	    'AssetManagement.customer.manager.ObjectStatusManager',
        'AssetManagement.customer.model.bo.SDOrder',
        'AssetManagement.customer.model.bo.SDOrderType',
        'AssetManagement.customer.model.bo.SDOrderItem',
        'AssetManagement.customer.model.bo.Order',
        'AssetManagement.customer.model.bo.Notif',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		EVENTS: {
			SDORDER_ADDED: 'SDOrderAdded',
			SDORDER_CHANGED: 'SDOrderChanged',
			SDORDER_DELETED: 'SDOrderDeleted'
		},
		
		//private
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.SDOrderCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseSDOrderManager', ex);
			}
			
			return retval;
		},
	
		//public methods
		getSDOrders: function(withDependendData, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForSDOrders(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.SDOrder',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildSDOrderStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
								
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForSDOrders.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrders of BaseSDOrderManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_SDHEADER', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_SDHEADER', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrders of BaseSDOrderManager', ex);
			}
	
			return retval;
		},
		
		getSDOrdersForNotif: function(qmnum, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				
				//first check, what are the sdOrders for the notif - when known, load them one by one as lightVersion
				var affectedVbeln = [];
				var loadFunction = function() {
					try {
						if(affectedVbeln.length > 0) {
							//there are affected sdOrders - load them one by one in light mode
							var toReturn = Ext.create('Ext.data.Store', {
								model: 'AssetManagement.customer.model.bo.SDOrder',
								autoLoad: false
							});
							
							var pendingRequests = 0;
							var errorOccurred = false;
							var reported = false;
							
							var completeFunction = function(sdOrder) {
								try {
									pendingRequests--;
								
									if(sdOrder) {
										toReturn.add(sdOrder);
									} else {
										errorOccurred = true;
									}
										
									if(errorOccurred === true && reported === false) {
										AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
										reported = true;
									} else if(pendingRequests == 0 && errorOccurred === false) {
										eventController.requestEventFiring(retval, toReturn);
									}
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForNotif of BaseSDOrderManager', ex);
									eventController.requestEventFiring(retval, undefined);
								}
							};
							
							Ext.Array.each(affectedVbeln, function(vbeln) {
								var toRegisterOn = me.getSDOrder(vbeln, true, true);
								
								if(toRegisterOn > 0) {
									pendingRequests++;
									eventController.registerOnEventForOneTime(toRegisterOn, completeFunction);
								} else {
									errorOccurred = true;
									return false;
								}
							}, this);
							
							if(!errorOccurred) {
								AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
							} else {
								completeFunction(null);
							}
						} else {
							eventController.requestEventFiring(retval, null);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForNotif of BaseSDOrderManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				//try to use the cache
				var cache = this.getCache();
				var fromCache = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
				
				if(fromCache) {
					if(fromCache.getCount() > 0) {
						fromCache.each(function(sdOrder) {
							if(sdOrder.get('qmnum') === qmnum)
								affectedVbeln.push(sdOrder.get('vbeln'));
						}, this);
					}
					
					loadFunction();
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						
							if(eventArgs.target.result) {
								var cursor = eventArgs.target.result;
							
								affectedVbeln.push(cursor.value['VBELN']);
								AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
							}
								
							if(done) {
								loadFunction();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForNotif of BaseSDOrderManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
	
					var indexMap = Ext.create('Ext.util.HashMap');
					indexMap.add('QMNUM', qmnum);
				
					var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_SDHEADER', 'QMNUM', indexMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_SDHEADER', 'QMNUM', indexRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForNotif of BaseSDOrderManager', ex);
			}
	
			return retval;
		},
		
		getSDOrdersForOrder: function(aufnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				
				//first check, what are the sdOrders for the order - when known, load them one by one as lightVersion
				var affectedVbeln = new Array();
				var loadFunction = function() {
					try {
						if(affectedVbeln.length > 0) {
							//there are affected sdOrders - load them one by one in light mode
							var toReturn = Ext.create('Ext.data.Store', {
								model: 'AssetManagement.customer.model.bo.SDOrder',
								autoLoad: false
							});
							
							var pendingRequests = 0;
							var errorOccurred = false;
							var reported = false;
							
							var completeFunction = function(sdOrder) {
								try {
									pendingRequests--;
								
									if(sdOrder) {
										toReturn.add(sdOrder);
									} else {
										errorOccurred = true;
									}
										
									if(errorOccurred === true && reported === false) {
										AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
										reported = true;
									} else if(pendingRequests == 0 && errorOccurred === false) {
										eventController.requestEventFiring(retval, toReturn);
									}
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForNotif of BaseSDOrderManager', ex);
									eventController.requestEventFiring(retval, undefined);
								}
							};
							
							Ext.Array.each(affectedVbeln, function(vbeln) {
								var toRegisterOn = me.getSDOrder(vbeln, true, true);
								
								if(toRegisterOn > 0) {
									pendingRequests++;
									eventController.registerOnEventForOneTime(toRegisterOn, completeFunction);
								} else {
									errorOccurred = true;
									return false;
								}
							}, this);
							
							if(!errorOccurred) {
								AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
							} else {
								completeFunction(null);
							}
						} else {
							eventController.requestEventFiring(retval, null);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForOrder of BaseSDOrderManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				//try to use the cache
				var cache = this.getCache();
				var fromCache = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
				
				if(fromCache) {
					if(fromCache.getCount() > 0) {
						fromCache.each(function(sdOrder) {
							if(sdOrder.get('aufnr') === aufnr)
								affectedVbeln.push(sdOrder.get('vbeln'));
						}, this);
					}
					
					loadFunction();
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						
							if(eventArgs.target.result) {
								var cursor = eventArgs.target.result;
							
								affectedVbeln.push(cursor.value['VBELN']);
								AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
							}
								
							if(done) {
								loadFunction();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForOrder of BaseSDOrderManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
	
					var indexMap = Ext.create('Ext.util.HashMap');
					indexMap.add('AUFNR', aufnr);
				
					var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_SDHEADER', 'AUFNR', indexMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_SDHEADER', 'AUFNR', indexRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrdersForOrder of BaseSDOrderManager', ex);
			}
	
			return retval;
		},
		
		getSDOrder: function(vbeln, lightVersion, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vbeln)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var requestedDataGrade = lightVersion ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				var fromCache = cache.getFromCache(vbeln, requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				//to avoid duplicate objects, check if there already exists an instance for the requested equipment and if so, use it
				var sdOrder = cache.getFromCache(vbeln);
				
				if(sdOrder) {
					this.loadDependendDataForSDOrder(sdOrder, retval);
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var sdOrder = me.buildSDPOrderFromDataBaseQuery.call(me, eventArgs);
							
							if(sdOrder)
								me.loadDependendDataForSDOrder.call(me, sdOrder, retval, lightVersion);
							else
								eventController.requestEventFiring(retval, null);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrder of BaseSDOrderManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('VBELN', vbeln);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_SDHEADER', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_SDHEADER', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrder of BaseSDOrderManager', ex);
			}
			
			return retval;
		},
		
		getNewSDOrder: function() {
			var retval = null;

			try {
				//draw the default values from Cust001 (UserInfo)
				var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
				
				retval = Ext.create('AssetManagement.customer.model.bo.SDOrder', {
					vbeln: '',
					auart: '',
					vkorg: userInfo.get('vkorg'),
					vtweg: userInfo.get('vtweg'),
					spart: userInfo.get('spart'),
					vkgrp: userInfo.get('vkgrp'),
					vkbur: userInfo.get('vkbur'),
					kunnrAG: userInfo.get('kunnrAG'),
					kunnrWE: userInfo.get('kunnrWE'),
					lfgsk: 'A',
					augru: '',
					bstnk: '',
					aufnr: '',
					qmnum: '',
					
					vdatu: AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(),
					bstdk: AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(),
	
				    updFlag: 'I',
				    mobileKey: '',
				    mobileKey_ref: ''
				});
				
				retval.set('items', Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.SDOrderItem',
					autoLoad: false
				}));
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewSDOrder of BaseSDOrderManager', ex);
			}
			
			return retval;
		},
		
		saveSDOrder: function(sdOrder) {
			var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrder) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = sdOrder.get('updFlag');
				
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;
				
				//update flag management
				this.manageUpdateFlagForSaving(sdOrder);
				
				//check if the sd order requires a new mobile number
				var requiresNewMobileNumber = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('vbeln'));
				
				var me = this;
				var saveFunction = function(nextMobileNumber) {
					try {
						if(requiresNewMobileNumber === true && nextMobileNumber !== -1) {
							var vbeln = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(nextMobileNumber, '0', 8);
							sdOrder.set('vbeln', vbeln);
						}
						
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('vbeln'))) {
							eventController.requestEventFiring(retval, false);
							return;
						}
						
						//mobilekey management
						if(isInsert === true && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('mobileKey')))
							sdOrder.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([ sdOrder.get('vbeln') ]));
						
						//set the id
						sdOrder.set('id', sdOrder.get('vbeln'));
			
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								var sdOrderItems = sdOrder.get('items');
								
								if(success && sdOrderItems && sdOrderItems.getCount() > 0) {
									//now save all sd order items
									var itemsSavedCallback = function(success) {
										try {
											var success = eventArgs.type === "success";
											if(success) {
												if(requiresNewMobileNumber === true) {
													//do not specify a datagrade, so all depenend data will loaded, when necessary
													me.getCache().addToCache(sdOrder);
												}
										
												var eventType = requiresNewMobileNumber === true ? me.EVENTS.SDORDER_ADDED : me.EVENTS.SDORDER_CHANGED;
												eventController.requestEventFiring(eventType, sdOrder);
											} else {
												if(requiresNewMobileNumber)
													me.deleteSDOrder(sdOrder);
											}
											
											eventController.requestEventFiring(retval, success);
										} catch(ex) {
											AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrder of BaseSDOrderManager', ex);
											eventController.requestEventFiring(retval, false);
										}
									};
									
									//prepare the items for saving
									var mobileKey = sdOrder.get('mobileKey');
									var vbeln = sdOrder.get('vbeln');
									
									sdOrderItems.each(function(sdOrderItem) {
										sdOrderItem.set('mobileKey', mobileKey);
										sdOrderItem.set('vbeln', vbeln);
									}, this);
									
									var itemsEventId = AssetManagement.customer.manager.SDOrderItemManager.saveSDOrderItems(sdOrderItems, false);
					
									if(itemsEventId > 1)
										eventController.registerOnEventForOneTime(itemsEventId, itemsSavedCallback);
									else
										eventController.requestEventFiring(retval, false);
								} else {
									eventController.requestEventFiring(retval, success);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrder of BaseSDOrderManager', ex);
								eventController.requestEventFiring(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForSDOrder(sdOrder);
						AssetManagement.customer.core.Core.getDataBaseHelper().put('D_SDHEADER', toSave, callback, callback);
					} catch(ex) {
						eventController.requestEventFiring(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrder of BaseSDOrderManager', ex);
					}
				};
				
				if(requiresNewMobileNumber === false) {
					saveFunction();
				} else {
					var eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_SDHEADER', false);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrder of BaseSDOrderManager', ex);
			}
			
			return retval;
		},
		
		deleteSDOrder: function(sdOrder, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!sdOrder || sdOrder.get('updFlag') !== 'I') {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;				

				var deleteItemsCallback = function(success) {
					try {
						if(success === true) {
							var callback = function(eventArgs) {
								try {
									var success = eventArgs.type === "success";
									
									if(success === true) {
										AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(sdOrder);
										me.getCache().removeFromCache(sdOrder);
										eventController.requestEventFiring(me.EVENTS.SDORDER_DELETED, sdOrder);
									}
									
									eventController.requestEventFiring(retval, success);
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrder of BaseSDOrderManager', ex);
									eventController.requestEventFiring(retval, false);
								}
							};
							
							//get the key of the sd order to delete
							var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_SDHEADER', sdOrder);
							AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_SDHEADER', keyRange, callback, callback, useBatchProcessing, me.requiresManualCompletion());
						} else {
							eventController.requestEventFiring(retval, false);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrder of BaseSDOrderManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				}
				
				//delete sd order items first
				var eventId = AssetManagement.customer.manager.SDOrderItemManager.deleteSDOrderItems(sdOrder.get('items'), false);
				
				if(eventId > 1)
					eventController.registerOnEventForOneTime(eventId, deleteItemsCallback);
				else
					eventController.requestEventFiring(retval, false);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrder of BaseSDOrderManager', ex);
			}
			
			return retval;
		},
		
		//private methods		
		buildSDPOrderFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildSDOrderFromDbResultObject(eventArgs.target.result.value);			
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDPOrderFromDataBaseQuery of BaseSDOrderManager', ex);
			}
			return retval;
		},
		
		buildSDOrderStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var sdOrder = this.buildSDOrderFromDbResultObject(cursor.value);
						store.add(sdOrder);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderStoreFromDataBaseQuery of BaseSDOrderManager', ex);
			}
		},
		
		buildSDOrderFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
			    retval = Ext.create('AssetManagement.customer.model.bo.SDOrder', {
					    vbeln: dbResult['VBELN'],
					    auart: dbResult['AUART'],
					    vkorg: dbResult['VKORG'],
					    vtweg: dbResult['VTWEG'],
					    spart: dbResult['SPART'],
					    vkgrp: dbResult['VKGRP'],
					    vkbur: dbResult['VKBUR'],
					    augru: dbResult['AUGRU'],
					    bstnk: dbResult['BSTNK'],
					    aufnr: dbResult['AUFNR'],
					    qmnum: dbResult['QMNUM'],
					    kunnrAG: dbResult['KUNNR_AG'],
					    kunnrWE: dbResult['KUNNR_WE'],
					    addrNo: dbResult['ADDR_NO'],
					    formOfAddress: dbResult['FORMOFADDR'],
					    name1: dbResult['NAME'],
					    name2: dbResult['NAME_2'],
					    name3: dbResult['NAME_3'],
					    name4: dbResult['NAME_4'],
					    coName: dbResult['C_O_NAME'],
					    city: dbResult['CITY'],
					    district: dbResult['DISTRICT'],
					    cityNo: dbResult['CITY_NO'],
					    postalCode1: dbResult['POSTL_COD1'],
					    postalCode2: dbResult['POSTL_COD2'],
					    postalCode3: dbResult['POSTL_COD3'],
					    poBox: dbResult['PO_BOX'],
					    poBoxCity: dbResult['PO_BOX_CIT'],
					    delivDis: dbResult['DELIV_DIS'],
					    street: dbResult['STREET'],
					    streetNo: dbResult['STREET_NO'],
					    strAbbr: dbResult['STR_ABBR'],
					    houseNo: dbResult['HOUSE_NO'],
					    strSuppl1: dbResult['STR_SUPPL1'],
					    strSuppl2: dbResult['STR_SUPPL2'],
					    location: dbResult['LOCATION'],
					    building: dbResult['BUILDING'],
					    floor: dbResult['FLOOR'],
					    roomNo: dbResult['ROOM_NO'],
					    country: dbResult['COUNTRY'],
					    langu: dbResult['LANGU'],
					    region: dbResult['REGION'],
					    lfgsk: dbResult['LFGSK'],
                        vtime: dbResult['VTIME'],
					
					    vdatu: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['VDATU']),
					    bstdk: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BSTDK']),
	
				        updFlag: dbResult['UPDFLAG'],
				        mobileKey: dbResult['MOBILEKEY'],
				        mobileKey_ref: dbResult['MOBILEKEY_REF']
			     });
			
			     retval.set('id', dbResult['VBELN']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSDOrderFromDbResultObject of BaseSDOrderManager', ex);
			}
			
			return retval;
		},
		
		loadListDependendDataForSDOrders: function(eventIdToFireWhenComplete, sdOrders) {
			try {
				//do not continue if there is no data
				if(sdOrders.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, sdOrders);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var sdOrderTypes = null;
				var sdOrderItems = null;	
				
				var pendingRequests = 0;			
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(pendingRequests === 0 && errorOccurred === false) {
						me.assignListDependendDataForSDOrders.call(me, eventIdToFireWhenComplete, sdOrders, sdOrderTypes,
						sdOrderItems);
					}
				};
				
				//TO-DO make dependend of customizing parameters --- this definitely will increase performance
				
				//get sd order types
				if(true) {
					pendingRequests++;
					
					var sdOrderTypesSuccessCallback = function(sdOrdTypes) {
						errorOccurred = sdOrdTypes === undefined;
						
						sdOrderTypes = sdOrdTypes;
						pendingRequests--;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.SDOrderTypeManager.getSDOrderTypes(true);
					eventController.registerOnEventForOneTime(eventId, sdOrderTypesSuccessCallback);
				}
				
				//get sd order items
				if(true) {
					pendingRequests++;
									
					var sdOrderItemsSuccessCallback = function(sdOrdItems) {
						errorOccurred = sdOrdItems === undefined;
						
						sdOrderItems = sdOrdItems;
						pendingRequests--;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.SDOrderItemManager.getSDOrderItems(false, true);
					eventController.registerOnEventForOneTime(eventId, sdOrderItemsSuccessCallback);
				}
				
				if(pendingRequests > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForSDOrders of BaseSDOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all neccessary data has been collected
		assignListDependendDataForSDOrders: function(eventIdToFireWhenComplete, sdOrders, sdOrderTypes, sdOrderItems) {
			try {
				//to increase performance create a copy of sdOrderItems' store
				//remove all objects from this store, which have been assigned to it's order
				//therefore following loops will have less iterations
				var sdItemsCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.SDOrderItem',
					autoLoad: false
				});
				
				if(sdOrderItems) {
					sdOrderItems.each(function(item, index, length) {
						sdItemsCopy.add(item);
					});
				}
				
				sdOrders.each(function(sdOrder, index, length) {
					//begin orderType assignment
					var myAuart = sdOrder.get('auart');
					
					if(sdOrderTypes) {
						sdOrderTypes.each(function(sdOrderType, index, length) {
							if(sdOrderType.get('auart') === myAuart) {
								sdOrder.set('orderType', sdOrderType);
								return false;
							}
						});
					}
					//end orderType assignment
					
				    //begin sd order items assignment
					var myItems = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.SDOrderItem',
						autoLoad: false
					});
					
					//fetch all my sd order items
					var myVbeln = sdOrder.get('vbeln');
					
					sdItemsCopy.each(function(item, index, length) {
						if(item.get('vbeln') === myVbeln) {
							myItems.add(item);
						}
					});
					
					//remove all my items from the data pool
					myItems.each(function(item, index, length) {
						sdItemsCopy.remove(item);
					});
					
					//sorting is not required yet for sd order list
					sdOrder.set('items', myItems);
					//end sd order items assignment
				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(sdOrders, cache.self.DATAGRADES.LOW);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForSDOrders of BaseSDOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForSDOrder: function(sdOrder, eventIdToFireWhenComplete, lightVersion) {
			try {
				if(sdOrder) {
					var vbeln = sdOrder.get('vbeln');
					
					//all of loadListDependendDataForSDOrders (but more specific and, partly, with dependencies now)
					//additional longtext

					if(lightVersion === undefined)
						lightVersion = false;
					
					//lightversion currently contains: items
					
					var sdOrderType = null;
					var sdOrderItems = null;
					var order = null;
					var notif = null;
					var backendLongtext = null;
					var localLongtext = null;
		
					var pendingRequests = 0;
					var errorOccurred = false;
					var reported = false;
					
					var me = this;
					var eventController = AssetManagement.customer.controller.EventController.getInstance();
					
					var completeFunction = function() {
						if(errorOccurred === true && reported === false) {
							AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
							reported = true;
						} else if(pendingRequests === 0 && errorOccurred === false) {
							me.assignDependendDataForSDOrder.call(me, eventIdToFireWhenComplete, sdOrder, lightVersion, sdOrderType,
									sdOrderItems, order, notif, backendLongtext, localLongtext);
						}
					};
					
					//TODO make dependend of customizing parameters --- this definitely will increase performance

					//get sd orderType
					if(lightVersion === false) {
						var auart = sdOrder.get('auart');
						pendingRequests++;
						
						var sdOrderTypeSuccessCallback = function(sdOrdType) {
							errorOccurred = sdOrdType === undefined;
							
							sdOrderType = sdOrdType;
							pendingRequests--;
							
							completeFunction();
						};
						
						var eventId = AssetManagement.customer.manager.SDOrderTypeManager.getSDOrderType(auart, true);
						eventController.registerOnEventForOneTime(eventId, sdOrderTypeSuccessCallback);
					}
					
					//get sd order items
					pendingRequests++;
					
					var sdOrderItemsSuccessCallback = function(sdOrdItems) {
						errorOccurred = sdOrdItems === undefined;
						
						sdOrderItems = sdOrdItems;
						pendingRequests--;
						
						completeFunction();
					};
					
					var eventId = AssetManagement.customer.manager.SDOrderItemManager.getSDOrderItemsForSDOrder(vbeln, true);
					eventController.registerOnEventForOneTime(eventId, sdOrderItemsSuccessCallback);
					
					//get order
					if(lightVersion === false) {
						var aufnr = sdOrder.get('aufnr');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
							pendingRequests++;
							
							var orderSuccessCallback = function(orderr) {
								errorOccurred = orderr === undefined;
								
								order = orderr;
								pendingRequests--;
								
								completeFunction();
							};
							
							eventId = AssetManagement.customer.manager.OrderManager.getOrderLightVersion(aufnr, true);
							eventController.registerOnEventForOneTime(eventId, orderSuccessCallback);
						}					
					}
					
					//get notif
					if(lightVersion === false) {
						var qmnum = sdOrder.get('qmnum');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
							pendingRequests++;
							
							var notifSuccessCallback = function(notification) {
								errorOccurred = notification === undefined;
								
								notif = notification;
								pendingRequests--;
								
								completeFunction();
							};
							
							eventId = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnum, true);
							eventController.registerOnEventForOneTime(eventId, notifSuccessCallback);
						}					
					}				
					
					//DISABLED
//					//get local and backendlongtext
//					if(lightVersion === false) {
//						done++;
//						
//						var localLongtextSuccessCallback = function(localLT, backendLT) {
//							errorOccurred = localLT === undefined || localLT === backendLT;
//							localLongtext = localLT;
//							backendLongtext = backendLT;
//							counter++;
//							
//							completeFunction();
//						};
//						eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(order, true);
//						eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
//					}
					
					if(pendingRequests > 0) {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					} else {
						completeFunction();
					}
				} else {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, null);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForSDOrder of BaseSDOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			} 
		}, 

		assignDependendDataForSDOrder: function(eventIdToFireWhenComplete, sdOrder, lightVersion, sdOrderType,
													sdOrderItems, order, notif, backendLongtext, localLongtext)
		{
			try
			{
				if(!sdOrder) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
					return;
				}
				
				sdOrder.set('orderType', sdOrderType);
				sdOrder.set('items', sdOrderItems);
				sdOrder.set('order', order);
				sdOrder.set('notif', notif);
				sdOrder.set('backendLongtext', backendLongtext);
				sdOrder.set('localLongtext', localLongtext);

				var cache = this.getCache();
				var dataGrade = lightVersion === true ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				cache.addToCache(sdOrder, dataGrade);
				
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, sdOrder);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForSDOrder of BaseSDOrderManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			} 
		},
		
		//builds the raw object to store in the database out of a sdOrder model
		buildDataBaseObjectForSDOrder: function(sdOrder) {
			var retval = null;
			var ac = null;
		
			try {
		        ac = AssetManagement.customer.core.Core.getAppConfig();
			
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['VBELN'] = sdOrder.get('vbeln');
				retval['AUART'] = sdOrder.get('auart');
				retval['VKORG'] = sdOrder.get('vkorg');
				retval['VTWEG'] = sdOrder.get('vtweg');
				retval['SPART'] = sdOrder.get('spart');
				retval['VKGRP'] = sdOrder.get('vkgrp');
				retval['VKBUR'] = sdOrder.get('vkbur');
				retval['AUGRU'] = sdOrder.get('augru');
				retval['BSTNK'] = sdOrder.get('bstnk');
				retval['AUFNR'] = sdOrder.get('aufnr');
				retval['QMNUM'] = sdOrder.get('qmnum');
				retval['MOBILEKEY_REF'] = sdOrder.get('mobileKey_ref');
				retval['KUNNR_AG'] = sdOrder.get('kunnrAG');
				retval['KUNNR_WE'] = sdOrder.get('kunnrWE');
				retval['ADDR_NO'] = sdOrder.get('addrNo');
				retval['FORMOFADDR'] = sdOrder.get('formOfAddress');
				retval['NAME'] = sdOrder.get('name1');
				retval['NAME_2'] = sdOrder.get('name2');
				retval['NAME_3'] = sdOrder.get('name3');
				retval['NAME_4'] = sdOrder.get('name4');
				retval['C_O_NAME'] = sdOrder.get('coName');
				retval['CITY'] = sdOrder.get('city');
				retval['DISTRICT'] = sdOrder.get('district');
				retval['CITY_NO'] = sdOrder.get('cityNo');
				retval['POSTL_COD1'] = sdOrder.get('postalCode1');
				retval['POSTL_COD2'] = sdOrder.get('postalCode2');
				retval['POSTL_COD3'] = sdOrder.get('postalCode3');
				retval['PO_BOX'] = sdOrder.get('poBox');
				retval['PO_BOX_CIT'] = sdOrder.get('poBoxCity');
				retval['DELIV_DIS'] = sdOrder.get('delivDis');
				retval['STREET'] = sdOrder.get('street');
				retval['STREET_NO'] = sdOrder.get('streetNo');
				retval['STR_ABBR'] = sdOrder.get('strAbbr');
				retval['HOUSE_NO'] = sdOrder.get('houseNo');
				retval['STR_SUPPL1'] = sdOrder.get('strSuppl1');
				retval['STR_SUPPL2'] = sdOrder.get('strSuppl2');
				retval['LOCATION'] = sdOrder.get('location');
				retval['BUILDING'] = sdOrder.get('building');
				retval['FLOOR'] = sdOrder.get('floor');
				retval['ROOM_NO'] = sdOrder.get('roomNo');
				retval['COUNTRY'] = sdOrder.get('country');
				retval['LANGU'] = sdOrder.get('langu');
				retval['REGION'] = sdOrder.get('region');
				retval['LFGSK'] = sdOrder.get('lfgsk');
				retval['VTIME'] = sdOrder.get('vtime');

				retval['VDATU'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(sdOrder.get('vdatu'));
				retval['BSTDK'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(sdOrder.get('bstdk'));
				
				retval['UPDFLAG'] = sdOrder.get('updFlag');
				retval['MOBILEKEY'] = sdOrder.get('mobileKey');
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForSDOrder of BaseSDOrderManager', ex);
			}
			
			return retval;
		}
	}
});