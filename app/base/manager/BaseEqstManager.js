Ext.define('AssetManagement.base.manager.BaseEqstManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Eqst',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public methods
		getEqsts: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._cacheComplete === true) {
					eventController.requestEventFiring(retval, this.getCache());
					
					return retval;
				}
				
				var toFill = this.getCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildEqstStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me._cacheComplete = true;
						
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEqsts of BaseEqstManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_EQST', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_EQST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEqsts of BaseEqstManager', ex);
			}
	
			return retval;
		}, 
		
		getEqst: function(equnr, werks, useBatchProcessing) {
			var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr) ||
						AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var eqst = this.getFromCache(equnr, werks);
					
				if(eqst) {
					eventController.requestEventFiring(retval, eqst);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var eqst = me.buildEqstFromDataBaseQuery.call(me, eventArgs);
						
						if(eqst)
							me.addToCache(eqst);
							
						eventController.requestEventFiring(retval, eqst);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEqst of BaseEqstManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('EQUNR', equnr);
				keyMap.add('WERKS', werks);
		
	            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_EQST', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_EQST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEqst of BaseEqstManager', ex);
			}
			
			return retval;
		},
	
	    //private methods
		buildEqstFromDataBaseQuery: function(eventArgs) {
	    	var retval = null;
	    	
	    	try {
	    		if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildEqstFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEqstFromDataBaseQuery of BaseEqstManager', ex);
			}
			
			return retval;
		},
		
		buildEqstStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var eqst = this.buildEqstFromDbResultObject(cursor.value);
						store.add(eqst);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEqstStoreFromDataBaseQuery of BaseEqstManager', ex);
			}
		}, 
		
		buildEqstFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Eqst', {
					equnr: dbResult['EQUNR'],
					werks: dbResult['WERKS'],
				    stlan: dbResult['STLAN'],
				    stlnr: dbResult['STLNR'],
				    stlal: dbResult['STLAL'],
				    
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['EQUNR'] + dbResult['WERKS']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEqstFromDbResultObject of BaseEqstManager', ex);
			}
			
			return retval;
		},
		
		//cache administration
		_eqstCache: null,
		_cacheComplete: false,
	
		getCache: function() {
	        try {
	        	if(!this._eqstCache) {
					this._eqstCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Eqst',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseEqstManager', ex);
			}
			
			return this._eqstCache;
		},
		
		clearCache: function() {
	        try {
			    this.getCache().removeAll();
			    this._cacheComplete = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseEqstManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseEqstManager', ex);
			} 
		},
		
		addToCache: function(eqst) {
			try {
				var temp = this.getCache().getById(eqst.get('id'));
				
				if(temp)
					this._eqstCache.remove(temp);
					
				this._eqstCache.add(eqst);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseEqstManager', ex);
			}
		},
		
		getFromCache: function(equnr, werks) {
			var retval = null;
			
			try {
				retval = this.getCache().getById(equnr + werks);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseEqstManager', ex);
			}
			
			return retval;
		}
	}
});