Ext.define('AssetManagement.base.manager.BaseCust_008Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.Cust_008Cache',
        'AssetManagement.customer.model.bo.Cust_008',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.Cust_008Cache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCust_008Manager', ex);
			}
			
			return retval;
		},
		
		//public
		getCust_008s: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			       //it can, so return just this store
			       eventController.requestEventFiring(retval, fromCache);
			       return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
		        	model: 'AssetManagement.customer.model.bo.Cust_008',
		        	autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_008StoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_008s of BaseCust_008Manager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_008', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_008', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_008s of BaseCust_008Manager', ex);
			}
	
			return retval;
		},
		
		getCust_008: function(qpart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qpart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(qpart);
		
		        if(fromCache) {
			         eventController.requestEventFiring(retval, fromCache);
			         return retval;
		        }
		
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_008 = me.buildCust_008DataBaseQuery.call(me, eventArgs);
						cache.addToCache(cust_008);
						eventController.requestEventFiring(retval, cust_008);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_008 of BaseCust_008Manager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QPART', qpart);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_008', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_008', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_008 of BaseCust_008Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_008DataBaseQuery: function(eventArgs) {
			var retval = null;
			try {			
			    if(eventArgs) {
				   if(eventArgs.target.result) {
					  retval = this.buildCust_008FromDbResultObject(eventArgs.target.result.value);
				   }
			    }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_008DataBaseQuery of BaseCust_008Manager', ex);
			}
			return retval;
		},
		
		buildCust_008StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_008 = this.buildCust_008FromDbResultObject(cursor.value);
						store.add(cust_008);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_008StoreFromDataBaseQuery of BaseCust_008Manager', ex);
			}
		},
		
		buildCust_008FromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {	        
		        retval = Ext.create('AssetManagement.customer.model.bo.Cust_008', {
				    qpart: dbResult['QPART'],
			        qkatartqual: dbResult['QKATART_QUAL'],
			        nove: dbResult['NO_VE'],
			        qkatartve: dbResult['QKATART_VE'],
			        qvmenge: dbResult['QVMENGE'],
			        codegrve:dbResult['CODEGR_VE'],
			        codeve: dbResult['CODE_VE'],
			        codegrvefehler:  dbResult['CODEGR_VE_FEHLER'],
			        codevefehler: dbResult['CODE_VE_FEHLER'],
			        plosref: dbResult['PLOS_REF'],
		            updFlag: dbResult['UPDFLAG']
			     });
			
			     retval.set('id', dbResult['QPART']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_008FromDbResultObject of BaseCust_008Manager', ex);
			}
			return retval;
		}
	}
});