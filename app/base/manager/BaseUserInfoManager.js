Ext.define('AssetManagement.base.manager.BaseUserInfoManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap'
    ],
	
	inheritableStatics: {
		//public
	    initializeUserInfoInstance: function (instance) {
	        var errorOccurred = false;

			try {
				//to initialize an instance of userinfo multiple database calls are neccessary
				//currently for stores: C_AM_CUST_001 & C_MC_CUST_002
			    //not calling the the setInitialized API to prevent an event firing
			    instance.set('isInitialized', false);

			    //cust001 presence is mandatory - if not present it has to be dealt as an error
			    //cust002 is optional - if not present the corresponding values of user info will just be left empty

			    var pendingRequests = 2;

			    var errorReported = false;

			    var completeFunction = function () {
			        if (errorOccurred && !errorReported) {
			            instance.setInitialized(false);
			        } else if (!errorOccurred && pendingRequests === 0) {
			            instance.setInitialized(true);
			        }
			    };
	
				var me = this;
				
				var successCallbackFor001 = function(eventArgs) {
				    try {
				        pendingRequests--;

				        errorOccurred = errorOccurred || !me.setUserInfo001Values.call(me, instance, eventArgs);
					} catch(ex) {
					    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeUserInfoInstance of BaseUserInfoManager', ex);
					    errorOccurred = true;
					} finally {
					    completeFunction();
					}
				};
				
				var successCallbackFor002 = function(eventArgs) {
				    try {
				        pendingRequests--;

						me.setUserInfo002Values.call(me, instance, eventArgs);
					} catch(ex) {
					    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeUserInfoInstance of BaseUserInfoManager', ex);
					    errorOccurred = true;
					} finally {
					    completeFunction();
					}
				};

				var mobileUser = AssetManagement.customer.core.Core.getAppConfig().getUserId().toUpperCase();
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add("MOBILEUSER", mobileUser);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_AM_CUST_001', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_001', keyRange, successCallbackFor001, null, true);				
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add("MOBILEUSER", mobileUser);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MC_CUST_002', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MC_CUST_002', keyRange, successCallbackFor002, null, true);
				
				AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeUserInfoInstance of BaseUserInfoManager', ex);
				instance.setInitialized(false);
			}
		},
		
		//private methods		
		setUserInfo001Values: function(instance, eventArgs) {
			try {
				if(eventArgs && eventArgs.target && eventArgs.target.result) {
					var dbResult = eventArgs.target.result.value;
				
					instance.set('mobileUser', dbResult['MOBILEUSER']);
					instance.set('orderScenario', dbResult['ORDER_SCENARIO']);
					instance.set('orderWorkcenter', dbResult['ORDER_WORKCENTER']);
					instance.set('orderPlantWrkc', dbResult['ORDER_PLANT_WRKC']);
					instance.set('orderPernr', dbResult['ORDER_PERNR']);
					instance.set('orderPlangroup', dbResult['ORDER_PLANGROUP']);
					instance.set('orderPlanPlant', dbResult['ORDER_PLAN_PLANT']);
					instance.set('orderParRole', dbResult['ORDER_PAR_ROLE']);
					instance.set('orderPartnerNo', dbResult['ORDER_PARTNER_NO']);
					instance.set('notifScenario', dbResult['NOTIF_SCENARIO']);
					instance.set('notifWorkcenter', dbResult['NOTIF_WORKCENTER']);
					instance.set('notifPlantWrkc', dbResult['NOTIF_PLANT_WRKC']);
					instance.set('notifPlanGroup', dbResult['NOTIF_PLANGROUP']);
					instance.set('notifPlanPlant', dbResult['NOTIF_PLAN_PLANT']);
					instance.set('notifPernr', dbResult['NOTIF_PERNR']);
					instance.set('notifParRole', dbResult['NOTIF_PAR_ROLE']);
					instance.set('notifPartnerNo', dbResult['NOTIF_PARTNER_NO']);
					instance.set('variTpl', dbResult['VARI_TPL']);
					instance.set('changeTpl', dbResult['CHANGE_TPL']);
					instance.set('variEqui', dbResult['VARI_EQUI']);
					instance.set('changeEqui', dbResult['CHANGE_EQUI']);
					instance.set('variClass', dbResult['VARI_CLASS']);
					instance.set('sprache', dbResult['SPRACHE']);
					instance.set('lartDefault', dbResult['LART_DEFAULT']);
					instance.set('intern', dbResult['INTERN']);
					instance.set('ekorg', dbResult['EKGRP']);
					instance.set('ekgrp', dbResult['EKORG']);
					instance.set('arbplKtext', dbResult['ARBPL_KTEXT']);
					instance.set('keyfigureKokrs', dbResult['KEYFIGURE_KOKRS']);
					instance.set('keyfigureGroup', dbResult['KEYFIGURE_GRP']);
					instance.set('vkorg', dbResult['VKORG']);
					instance.set('vtweg', dbResult['VTWEG']);
					instance.set('spart', dbResult['SPART']);
					instance.set('kschl', dbResult['KSCHL']);
					instance.set('kunnrConsi', dbResult['KUNNR_CONSI']);
					instance.set('vkgrp', dbResult['VKGRP']);
					instance.set('vkbur', dbResult['VKBUR']);
					instance.set('kunnrAG', dbResult['KUNNR_AG']);
					instance.set('kunnrWE', dbResult['KUNNR_WE']);
					instance.set('lifnr', dbResult['LIFNR']);
					instance.set('varMati', dbResult['VAR_MATI']);
					instance.set('timeZone', dbResult['TZONE']);
					instance.set('mobileScenario', dbResult['SCENARIO']);
					
					return true;
				} else {
					return false;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUserInfo001Values of BaseUserInfoManager', ex);
			}
		},
		
		setUserInfo002Values: function(instance, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target && eventArgs.target.result) {
					var dbResult = eventArgs.target.result.value;
				
					instance.set('mobileScenario', dbResult['SCENARIO']);
					instance.set('defaultScenario', dbResult['DEFAULT_SCENARIO']);
					instance.set('inactive', dbResult['INACTIVE']);
					
					return true;
				} else {
					return false;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUserInfo002Values of BaseUserInfoManager', ex);
			}
		}
	}
});