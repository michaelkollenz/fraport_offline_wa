﻿Ext.define('AssetManagement.base.manager.BaseCustAuswmManager', {
    requires: [
         'AssetManagement.customer.controller.EventController',
         'AssetManagement.customer.helper.OxLogger',
         'AssetManagement.customer.utils.StringUtils',
         'Ext.data.Store',
         'AssetManagement.customer.model.bo.CustAuswm',
         'AssetManagement.customer.manager.cache.CustAuswmCache'
    ],

    inheritableStatics: {
        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.CustAuswmCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCustAuswmManager', ex);
            }

            return retval;
        },

        getCustAuswms: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.CustAuswm',
                    autoLoad: false
                });

                //check if the cache can delivery the complete dataset
                var cache = this.getCache();
                var fromCache = cache.getStoreForAll();

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildCustAuswmStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            //add the whole store to the cache
                            cache.addStoreForAllToCache(fromDataBase);

                            //return a store from cache to eliminate duplicate objects
                            var toReturn = cache.getStoreForAll();
                            eventController.requestEventFiring(retval, toReturn);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustAuswms of BaseCustAuswmManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CUSTAUSWM', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTAUSWM', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustAuswms of BaseCustAuswmManager', ex);
                retval = -1;
            }

            return retval;
        },

        getCustAuswmForQPlosChar: function (auswahlmenge, useBatchProcessing) {
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(auswahlmenge)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                //check if the cache can deliver QplosChars checklists
                var cache = this.getCache();
                var fromCache = cache.getFromCacheForQPlosChar(auswahlmenge, cache.self.DATAGRADES.MEDIUM);

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }
                var baseStore = null;
                if (cache.getFromCacheForQPlosChar(auswahlmenge, cache.self.DATAGRADES.BASE)) {
                    baseStore = cache.getAllUpToGradeForQplosChar(auswahlmenge, cache.self.DATAGRADES.LOW);
                    this.loadDependendDataForQPLOSResult(baseStore, retval);
                } else {
                    var baseStore = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.CustAuswm',
                        autoLoad: false
                    });

                    var me = this;
                    var successCallback = function (eventArgs) {
                        try {
                            var done = !eventArgs || !eventArgs.target || !eventArgs.target.resul;

                            me.buildAuswmFromDbResultObject(auswahlmenge, baseStore, eventArgs);

                            if (done) {
                                // add the whole store to the cache
                                cache.addToCacheForQPlosChar(auswahlmenge, baseStore, cache.self.DATAGRADES.BASE);

                                //before proceeding pull all instances from cache, with a too low data grade
                                //ele the wrong instances would be filled with data
                                baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
                                me.loadListDependendDataForQPLOSResult.call(me, retval, baseStore);
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustAuswmForQPLOSChar of BaseChecklistResultManger', ex);
                            eventController.requestEventFiring(retval, undefined);
                        }
                    };

                    var keyMap = Ext.create('Ext.util.HashMap');
                    keyMap.add('auswahlmge', auswahlmenge);

                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CUSTAUSWM', keyMap);
                    AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CUSTAUSWM', keyRange, successCallback, null, useBatchProcessing);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustAuswmForQPLOSChar of BaseChecklistResultManager', ex);
                retval = -1;
            }
            return retval;
        },

        buildCustAuswmStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        var checklistInspl = this.builCusatuswmFromDbResultObject(cursor.value);
                        store.add(checklistInspl);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCustAuswmStoreFromDataBaseQuery of BaseCustAuswmManager', ex);
            }
        },

        builCusatuswmFromDbResultObject: function (dbResult) {
            try {
                //ordinary fields
                var retval = Ext.create('AssetManagement.customer.model.bo.CustAuswm', {
                    werks: dbResult['WERKS'],
                    katalogart: dbResult['KATALOGART'],
                    auswahlmge: dbResult['AUSWAHLMGE'],
                    codegruppe: dbResult['CODEGRUPPE'],
                    code: dbResult['CODE'],
                    versionam: dbResult['VERSIONAM'],
                    bewertung: dbResult['BEWERTUNG']
                });

                retval.set('id', dbResult['WERKS'] + dbResult['KATALOGART'] + dbResult['AUSWAHLMGE'] + dbResult['CODEGRUPPE'] + dbResult['CODE']);

                return retval;
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside builCusatuswmFromDbResultObject of BaseCustAuswmManager', ex);
            }
        }
    }
});
