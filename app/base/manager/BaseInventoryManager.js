Ext.define('AssetManagement.base.manager.BaseInventoryManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.InventoryHead',
        'AssetManagement.customer.manager.cache.InventoryCache',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.controller.EventController'
    ],

    inheritableStatics: {

        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.InventoryCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseInventoryManager', ex);
            }

            return retval;
        },


        //public methods
        getInventories: function (withDependendData, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var cache = this.getCache();
                var fromCache = cache.getStoreForAll(true);

                if (fromCache && !withDependendData) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                if (withDependendData === true && fromCache) {
                    this.loadListDependendDataForInventories(fromCache, retval);
                } else {
                    var theStore = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.InventoryHead',
                        autoLoad: false
                    });

                    var me = this;
                    var successCallback = function (eventArgs) {
                        try {
                            var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                            me.buildInventoryStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                            if (done) {
                                cache.addStoreForAllToCache(theStore, true);
                                if (withDependendData) {
                                    me.loadListDependendDataForInventories(theStore, retval);
                                    //eventController.requestEventFiring(retval, theStore);
                                } else {
                                    var toReturn = cache.getStoreForAll(true);
                                    eventController.requestEventFiring(retval, theStore);
                                }
                            }

                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventories of BaseInventoryManager', ex);
                            eventController.requestEventFiring(retval, undefined);
                        }
                    };

                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_MI_INV_HEAD', null);
                    AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_HEAD', keyRange, successCallback, null, useBatchProcessing);
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventories of BaseInventoryManager', ex);
            }

            return retval;
        },

        //no reference exists
        getInventory: function (physinventory, fiscalyear, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(physinventory) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fiscalyear)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var inventoryItem = me.buildInventoryFromDataBaseQuery.call(me, eventArgs);

                        if(inventoryItem) {
                            me.loadDependendDataForInventory.call(me, inventoryItem, retval);
                        } else {
                            eventController.requestEventFiring(retval, null);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventory of BaseInventoryManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PHYSINVENTORY', physinventory);
                keyMap.add('FISCALYEAR', fiscalyear);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_MI_INV_HEAD', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_MI_INV_HEAD', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInventory of BaseInventoryManager', ex);
            }

            return retval;
        },

        //no reference exists
        saveInventory: function (inventory) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!inventory) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                inventory.set('updFlag', 'I');

                //check if the inventory already has a counter value
                var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(inventory.get('physinventory'));

                var me = this;
                var saveFunction = function (nextCounterValue) {
                    try {
                        if (requiresNewCounterValue && nextCounterValue !== -1) {
                            counter = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 8);
                            inventory.set('physinventory', counter);
                        }

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(inventory.get('physinventory'))) {
                            eventController.fireEvent(retval, false);
                            return;
                        }

                        //set the id
                        inventory.set('id', inventory.get('physinventory'));
                        //set mobileKey
                        inventory.set('physinventory', AssetManagement.customer.manager.KeyManager.createKey([inventory.get('physinventory'), inventory.get('fiscalyear'), inventory.get('item')]));

                        var callback = function (eventArgs) {
                            try {
                                eventController.fireEvent(retval, eventArgs.type === "success");
                                eventController.fireEvent(retval, inventory);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveItemHeader of BaseInventoryItemsManager', ex);
                                eventController.fireEvent(retval, false);
                            }
                        };

                        var toSave = me.buildDataBaseObjectFromInventory(inventory);
                        AssetManagement.customer.core.Core.getDataBaseHelper().put('D_MI_INV_HEAD', toSave, callback, callback);
                    } catch (ex) {
                        eventController.fireEvent(retval, false);
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveItemHeader of BaseInventoryItemsManager', ex);
                    }
                };

                if (!requiresNewCounterValue) {
                    saveFunction();
                } else {
                    eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_MI_INV_HEAD', false);

                    if (eventId > 1)
                        eventController.registerOnEventForOneTime(eventId, saveFunction);
                    else
                        eventController.fireEvent(retval, false);
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveItemHeader of BaseInventoryItemsManager', ex);
            }

            return retval;
        },


        //private
        buildInventoryFromDataBaseQuery: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildInventoryFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryFromDataBaseQuery of BaseInventoryManager', ex);
            }

            return retval;
        },

        buildInventoryStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var inventoryitem = this.buildInventoryFromDbResultObject(cursor.value);
                        store.add(inventoryitem);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryStoreFromDataBaseQuery of BaseInventoryManager', ex);
            }
        },

        buildInventoryFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.InventoryHead', {
                    physinventory: dbResult['PHYSINVENTORY'],
                    fiscalyear: dbResult['FISCALYEAR'],
                    item: dbResult['ITEM'],
                    material: dbResult['MATERIAL'],
                    plant: dbResult['PLANT'],
                    stgeLoc: dbResult['STGE_LOC'],
                    planDate: dbResult['PLAN_DATE'],
                    batch: dbResult['BATCH'],
                    specstock: dbResult['SPEC_STOCK'],
                    stocktype: dbResult['STOCKTYPE'],
                    changeuser: dbResult['CHANGEUSER'],
                    countuser: dbResult['COUNTUSER'],
                    counted: dbResult['COUNTED'],
                    diffposted: dbResult['DIFFPOSTED'],
                    recount: dbResult['RECOUNT'],
                    altunit: dbResult['ALTUNIT'],
                    bookqty: dbResult['BOOKQTY'],
                    zerocount: dbResult['ZERO_COUNT'],
                    quantity: dbResult['QUANTITY'],
                    base_uom: dbResult['BASE_UOM'],
                    entryqty: dbResult['ENTRY_QTY'],
                    entryuom: dbResult['ENTRY_UOM'],
                    lgpbe: dbResult['LGPBE'],
                    updFlag: dbResult['UPDFLAG'],

                    plan: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['PLAN_DATE'], dbResult['DOC_DATE'])
                });

                retval.set('id', dbResult['PHYSINVENTORY'] + dbResult['FISCALYEAR']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildInventoryFromDbResultObject of BaseInventoryManager', ex);
            }

            return retval;
        },

        buildDataBaseObjectFromInventory: function (inventory) {
            var ac = null;
            var retval = null;

            try {
                ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['SCENARIO'] = ac.getScenario();
                retval['USERID'] = ac.getUserId();
                retval['UPDFLAG'] = inventory.get('updFlag');
                retval['PHYSINVENTORY'] = inventory.get('physinventory');
                retval['FISCALYEAR'] = inventory.get('fiscalyear');
                retval['MOBILEKEY'] = inventory.get('mobileKey');



            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForInventoryHead of BasInventoryItemManager', ex);
            }
            return retval;
        },


        loadListDependendDataForInventories: function (inventories, eventIdToFireWhenComplete) {
            try {
                //do not continue if there is no data
                if (inventories && inventories.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventories);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var inventoryItems = null;

                var counter = 0;

                var done = 0;
                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignListDependendDataForInventories.call(me, eventIdToFireWhenComplete, inventories, inventoryItems);
                    }
                };

                //get Inventory Items for Inventories
                if(true) {
                    done++;

                    var loadItemsCallback = function(invItems) {
                        errorOccurred = invItems === undefined;

                        inventoryItems = invItems;
                        counter++;

                        completeFunction();
                    };

                    var eventId = AssetManagement.customer.manager.InventoryItemsManager.getAllInventoryItems(true);
                    eventController.registerOnEventForOneTime(eventId, loadItemsCallback);
}

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForInventories of BaseInventoryManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        //call this function, when all necessary data has been collected
        assignListDependendDataForInventories: function (eventIdToFireWhenComplete, inventories, inventoryItems) {
            try {
                if(inventories && inventories.getCount() >0) {
                    inventories.each(function(inventory) {
                        var inventoryFiscalYear = inventory.get('fiscalyear');
                        var inventoryPhysInventory = inventory.get('physinventory');

                        var invItemsStore = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.InventoryItem',
                            autoLoad: false
                        });

                        if(inventoryItems && inventoryItems.getCount() >0) {
                            inventoryItems.each(function(inventoryItem) {
                                var inventoryItemFiscalYear = inventoryItem.get('fiscalyear');
                                var inventoryItemPhysInventory = inventoryItem.get('physinventory');

                                if(inventoryItemFiscalYear === inventoryFiscalYear && inventoryItemPhysInventory === inventoryPhysInventory) {
                                    invItemsStore.add(inventoryItem);
                                }
                            })
                        }
                        inventory.set('items', invItemsStore);

                    })
                }

                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventories);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForInventories of BaseInventoryManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        loadDependendDataForInventory: function (inventory, eventIdToFireWhenComplete) {
            try {
                if (!inventory) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, inventory);
                    return;
                }

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var inventoryItems = null;

                var counter = 0;
                var done = 0;
                var erroroccurred = false;
                var reported = false;

                var completeFunction = function() {
                    if(erroroccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if(counter === done && erroroccurred === false) {
                        me.assignDependendDataForInventory.call(me, eventIdToFireWhenComplete, inventory, inventoryItems);
                    }
                };

                //get inventoryItems
                if(true) {
                    done++;

                    var loadItemsCallback = function(invItems) {
                        erroroccurred = invItems === undefined;

                        inventoryItems = invItems;
                        counter++;

                        completeFunction();
                    };

                    var eventId = AssetManagement.customer.manager.InventoryItemsManager.getItemsForInventory(inventory, true);
                    eventController.registerOnEventForOneTime(eventId, loadItemsCallback);
                }

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForInventory of BaseInventoryManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        assignDependendDataForInventory: function(eventIdToFireWhenComplete, inventory, inventoryItems) {
            try {
                if(!inventory) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                    return;
                }

                inventory.set('items', inventoryItems);

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, inventory);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForOrder of BaseInventoryManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        }
    }

});
