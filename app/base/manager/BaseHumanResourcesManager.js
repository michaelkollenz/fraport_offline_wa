﻿Ext.define('AssetManagement.base.manager.BaseHumanResourcesManager', {
    requires: [
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.model.bo.HumanResource'
    ],

    inheritableStatics: {
        getAllHumanResourcesData: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();


                var baseStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.HumanResource',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildHrStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

                        if (done) {
                            //return a store from cache to eliminate duplicate objects
                            eventController.requestEventFiring(retval, baseStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllHumanResourcesData of BaseHumanResourcesManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PERS_P0001', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PERS_P0001', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllHumanResourcesData of BaseHumanResourcesManager', ex);
            }

            return retval;
        },

        buildHrStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var equi = this.buildHrFromDbResultObject(cursor.value);
                        store.add(equi);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHrStoreFromDataBaseQuery of BaseHumanResourcesManager', ex);
            }
        },

        buildHrFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.HumanResource', {
                    pernr: dbResult['PERNR'],
                    infty: dbResult['INFTY'],
                    subty: dbResult['SUBTY'],
                    objps: dbResult['OBJPS'],
                    sprps: dbResult['SPRPS'],
                    endda: dbResult['ENDDA'],
                    begda: dbResult['BEGDA'],
                    seqnr: dbResult['SEQNR'],
                    bukrs: dbResult['BUKRS'],
                    werks: dbResult['WERKS'],
                    persg: dbResult['PERSG'],
                    persk: dbResult['PERSK'],
                    vdsk1: dbResult['VDSK1'],
                    gsber: dbResult['GSBER'],
                    btrtl: dbResult['BTRTL'],
                    abkrs: dbResult['ABKRS'],
                    ansvh: dbResult['ANSVH'],
                    kostl: dbResult['KOSTL'],
                    orgeh: dbResult['ORGEH'],
                    plans: dbResult['PLANS'],
                    stell: dbResult['STELL'],
                    mstbr: dbResult['MSTBR'],
                    sacha: dbResult['SACHA'],
                    sachp: dbResult['SACHP'],
                    sachz: dbResult['SACHZ'],
                    sname: dbResult['SNAME'],
                    ename: dbResult['ENAME'],
                    otype: dbResult['OTYPE'],
                    sbmod: dbResult['SBMOD'],
                    kokrs: dbResult['KOKRS'],
                    fistl: dbResult['FISTL'],
                    geber: dbResult['GEBER'],
                    fkber: dbResult['FKBER'],
                    grantNbr: dbResult['GRANT_NBR'],
                    sgmnt: dbResult['SGMNT'],
                    budget_pd: dbResult['BUDGET_PD'],
                    juper: dbResult['JUPER'],
                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['PERNR']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHrFromDbResultObject of BaseHumanResourcesManager', ex);
            }

            return retval;
        }
    }
});