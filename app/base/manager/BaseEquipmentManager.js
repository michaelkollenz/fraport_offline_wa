Ext.define('AssetManagement.base.manager.BaseEquipmentManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.EquipmentCache',
        'AssetManagement.customer.model.bo.Equipment',
        'AssetManagement.customer.manager.AddressManager',
        //'AssetManagement.customer.manager.FuncLocManager',	INCLUSION CAUSES RING DEPDENCY
        'AssetManagement.customer.manager.PartnerManager',
        'AssetManagement.customer.manager.ObjectStatusManager',
        'AssetManagement.customer.manager.DMSDocumentManager',
        'AssetManagement.customer.manager.DocUploadManager',
        'AssetManagement.customer.manager.HistOrderManager',
        'AssetManagement.customer.manager.HistNotifManager',
        'AssetManagement.customer.manager.MeasurementPointManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.manager.ClassificationManager',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		EVENTS: {
			EQUI_ADDED: 'equiAdded',
			EQUI_CHANGED: 'equiChanged',
			EQUI_DELETED: 'equiDeleted'
		},
		//protected
		getCache: function() {
			var retval = null;
		
		    try {
				retval = AssetManagement.customer.manager.cache.EquipmentCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseEquipmentManager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getEquipments: function(withDependendData, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForEquipments(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Equipment',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						
							me.buildEquipmentsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
							
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForEquipments.call(me, retval, baseStore);
								} else {
									//return a store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipments of BaseEquipmentManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_EQUI', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_EQUI', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipments of BaseEquipmentManager', ex);
			}
	
			return retval;
		},
		
		//will use the list cache and return a new store of all elements matching the passed tplnr
		getEquipmentsForFuncLoc: function(tplnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var cache = this.getCache();
				var fromCache = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
				
				var filterAndReturn = function() {
					try {
						var funcLocsEquis = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Equipment',
							autoLoad: false
						});
						
						var base = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
						
						if(base && base.getCount() > 0) {
							base.each(function(equi) {
								if(equi.get('tplnr') === tplnr)
									funcLocsEquis.add(equi);
							}, this);
						}
					
						eventController.requestEventFiring(retval, funcLocsEquis);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipmentsForFuncLoc of BaseEquipmentManager', ex);
					}
				};
				
				if(fromCache) {
					filterAndReturn();
				} else {
					//else fill the cache first, load it without dependend data
					var toWaitFor = this.getEquipments(false);
					eventController.registerOnEventForOneTime(eventId, filterAndReturn);
				}

			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipmentsForFuncLoc of BaseEquipmentManager', ex);
			}
			
			return retval;
		},
		
		getEquipment: function(equnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				retval = this.getEquipmentInternal(equnr, false, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipment of BaseEquipmentManager', ex);
			}

			return retval;
		},
		
		getEquipmentLightVersion: function(equnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				retval = this.getEquipmentInternal(equnr, true, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipmentLightVersion of BaseEquipmentManager', ex);
			}

			return retval;
		},
		
		//protected
		getEquipmentInternal: function(equnr, lightVersion, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var requestedDataGrade = lightVersion ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				var fromCache = cache.getFromCache(equnr, requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				//to avoid duplicate objects, check if there already exists an instance for the requested equipment and if so, use it
				var equi = cache.getFromCache(equnr);
				
				if(equi) {
					this.loadDependendDataForEquipment(equi, retval, lightVersion);
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var equi = me.buildEquipmentFromDataBaseQuery.call(me, eventArgs);
							
							if(equi)
								me.loadDependendDataForEquipment.call(me, equi, retval, lightVersion);
							else
								eventController.requestEventFiring(retval, null);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipmentInternal of BaseEquipmentManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
				
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('EQUI', equnr);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_EQUI', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_EQUI', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipmentInternal of BaseEquipmentManager', ex);
			}

			return retval;
		},
		
//		getEquipmentByObjectNumber: function(objnr, useBatchProcessing) {
//			var retval = { eventId: 0, outReference: null };
//			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(objnr))
//				return retval;
//			
//			var equipment;
//
//			if(this._equipmentsRichCache === null || this._equipmentsRichCache === undefined) {
//				this._equipmentsRichCache= Ext.create('Ext.data.Store', {
//					model: 'AssetManagement.customer.model.bo.Equipment',
//					autoLoad: false
//				});
//			}
//			
//			var me = this;
//			var ac = AssetManagement.customer.core.Core.getAppConfig();
//			var successCallback = function(tx, results) { me.buildEquipmentFromDataBaseQuery.call(me, retval, tx, results); };
//			
//			retval.eventId = AssetManagement.customer.core.Core.getDataBaseHelper().rawQuery(this.SQL_SELECT_SPECIFIC_EQUIPMENT_BY_OBJNR(), [ ac.getMandt(), ac.getUserId(), objnr ], successCallback, null, useBatchProcessing);
//			
//			return retval;
//		},

	    //save equipment - if it is necessary it will get a mobile number
		saveEquipment: function (equipment) {
		    var retval = -1;

		    try {
		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        retval = eventController.getNextEventId();

		        if (!equipment) {
		            eventController.requestEventFiring(retval, false);
		            return retval;
		        }

		        var priorUpdateFlag = equipment.get('updFlag');

		        var isNewRecord = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equipment.get('equnr'));
		        var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag;

		        //update flag management
		        this.manageUpdateFlagForSaving(equipment);

		        var me = this;
		        var saveFunction = function (nextCounterValue) {
		            try {
		                if (isNewRecord === true && nextCounterValue !== -1) {
		                    var equnr = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 16);
		                    equipment.set('equnr', equnr);

		                    var objnr = 'IE' + equnr;
		                    equipment.set('objnr', objnr);
		                }

		                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equipment.get('equnr'))) {
		                    eventController.requestEventFiring(retval, false);
		                    return;
		                }

		                //mobilekey management
		                if (isInsert === true && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equipment.get('mobileKey')))
		                    equipment.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([equipment.get('equnr')]));

		                //set the id
		                equipment.set('id', equipment.get('equnr'));

		                var callback = function (eventArgs) {
		                    try {
		                        var success = eventArgs.type === "success";

		                        if (success) {
		                            if (isNewRecord === true) {
		                                //this is a new record, add it to the cache and exist saving
		                                //do not specify a data grade, so all depenend data will loaded, when necessary
		                                me.getCache().addToCache(equipment);
		                                eventController.requestEventFiring(me.EVENTS.EQUI_ADDED, equipment);
		                                eventController.requestEventFiring(retval, true);
		                            } else {
		                                //this is an already existing record
		                                //reload all dependend data to adjust references to new dependend data
		                                var reloadCallback = function (equipment) {
		                                    try {
		                                        eventController.requestEventFiring(me.EVENTS.EQUI_CHANGED, equipment);
		                                        eventController.requestEventFiring(retval, true);
		                                    } catch (ex) {
		                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveEquipment of BaseEquipmentManager', ex);
		                                        eventController.requestEventFiring(retval, false);
		                                    }
		                                };

		                                var reloadEventId = eventController.getNextEventId();
		                                eventController.registerOnEventForOneTime(reloadEventId, reloadCallback);

		                                me.loadDependendDataForEquipment(equipment, reloadEventId);
		                            }
		                        } else {
		                            eventController.requestEventFiring(retval, false);
		                        }
		                    } catch (ex) {
		                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveEquipment of BaseEquipmentManager', ex);
		                        eventController.requestEventFiring(retval, false);
		                    }
		                };

		                var toSave = me.buildDataBaseObjectForEqui(equipment);

		                if (!isInsert)
		                    AssetManagement.customer.core.Core.getDataBaseHelper().update('D_EQUI', null, toSave, callback, callback);
		                else
		                    AssetManagement.customer.core.Core.getDataBaseHelper().put('D_EQUI', toSave, callback, callback);
		            } catch (ex) {
		                eventController.requestEventFiring(retval, false);
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveEquipment of BaseEquipmentManager', ex);
		            }
		        };

		        if (isNewRecord === false) {
		            saveFunction();
		        } else {
		            eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_EQUI', false);

		            if (eventId > 1)
		                eventController.registerOnEventForOneTime(eventId, saveFunction);
		            else
		                eventController.requestEventFiring(retval, false);
		        }
		    } catch (ex) {
		        retval = -1;
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveEquipment of BaseEquipmentManager', ex);
		    }

		    return retval;
		},
		
	    //changes an equipment's position
	    //expects a dummy equipment instance (which holds the new position information for an equipment) and the change date
	    //on success the real equipment will be returned adjusted to the new position
	    //on error undefined will be returned
		performEquipmentPositionChange: function (equipment, changeDate) {
		    var retval = -1;

		    try {
		        if (!equipment || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equipment.get('equnr')) || AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(changeDate)) {
		            retval = -1;
		            return retval;
		        }

		        var eventController = AssetManagement.customer.controller.EventController.getInstance();
		        retval = eventController.getNextEventId();

		        //first the equi in record has to be generated and saved
		        var toSave = this.buildDataBaseObjectForEquiChange(equipment, changeDate);

		        if (toSave) {
		            //next drop the save request for D_EQIN
		            var me = this;
		            var callback = function (eventArgs) {
		                try {
		                    var success = eventArgs.type === "success";

		                    if (success) {
		                        //define action for errors cases, which will clear the saved equi in record
		                        var rollbackEquiInRecordAction = function () {
		                            try {
		                                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_EQUI', equipment);
		                                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_EQIN', keyRange);
		                            } catch (ex) {
		                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		                            }
		                        };

		                        //save successfull - continue with adjusting the equipments data
		                        //get the affected equipment next
		                        var affectedEquiCallback = function (affectedEquipment) {
		                            try {
		                                if (!affectedEquipment) {
		                                    if (affectedEquipment === undefined) {
		                                        //something went wrong, this is an error, clear the eqin record
		                                        rollbackEquiInRecordAction();
		                                    }

		                                    eventController.requestEventFiring(retval, affectedEquipment);
		                                } else {
		                                    //now adjust the equipment and pass it for saving again
		                                    var formerTplnr = equipment.get('tplnr');
		                                    var formerHequi = equipment.get('hequi');
		                                    var formerHeqnr = equipment.get('heqnr');

		                                    //define an extended action for errors cases, which will undo the changes on the equipment and clear the saved equi in record
		                                    var rollbackAllAction = function () {
		                                        try {
		                                            //undo equipment changes
		                                            affectedEquipment.set('tplnr', formerTplnr);
		                                            affectedEquipment.set('hequi', formerHequi);
		                                            affectedEquipment.set('heqnr', formerHeqnr);

		                                            rollbackEquiInRecordAction();
		                                        } catch (ex) {
		                                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		                                        }
		                                    };

		                                    try {
		                                        affectedEquipment.set('tplnr', equipment.get('tplnr'));
		                                        affectedEquipment.set('hequi', equipment.get('hequi'));
		                                        affectedEquipment.set('heqnr', equipment.get('heqnr'));

		                                        //drop the save request
		                                        var saveEventId = me.saveEquipment(affectedEquipment);

		                                        if (saveEventId > -1) {
		                                            var saveEquiChangesCallback = function (success) {
		                                                try {
		                                                    success = success === true;

		                                                    if (success) {
		                                                        eventController.requestEventFiring(retval, affectedEquipment);
		                                                    } else {
		                                                        rollbackAllAction();
		                                                        eventController.requestEventFiring(retval, undefined);
		                                                    }
		                                                } catch (ex) {
		                                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		                                                    rollbackAllAction();
		                                                    eventController.requestEventFiring(retval, undefined);
		                                                }
		                                            };

		                                            eventController.registerOnEventForOneTime(saveEventId, saveEquiChangesCallback);
		                                        } else {
		                                            //loading affected equipment failed, rollback the changes and abort procedure by returning undefined
		                                            rollbackAllAction();
		                                            eventController.requestEventFiring(retval, undefined);
		                                        }
		                                    } catch (ex) {
		                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		                                        rollbackAllAction();
		                                        eventController.requestEventFiring(retval, undefined);
		                                    }
		                                }
		                            } catch (ex) {
		                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		                                eventController.requestEventFiring(retval, undefined);
		                            }
		                        };

		                        var equiEventId = me.getEquipment(equipment.get('equnr'));

		                        if (equiEventId > -1) {
		                            eventController.registerOnEventForOneTime(equiEventId, affectedEquiCallback);
		                        } else {
		                            //loading affected equipment failed, abort procedure
		                            rollbackEquiInRecordAction();
		                            eventController.requestEventFiring(retval, undefined);
		                        }
		                    } else {
		                        //save failed, abort procedure
		                        eventController.requestEventFiring(retval, undefined);
		                    }
		                } catch (ex) {
		                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		                    eventController.requestEventFiring(retval, undefined);
		                }
		            };

		            AssetManagement.customer.core.Core.getDataBaseHelper().put('D_EQIN', toSave, callback, callback);
		        } else {
		            retval = -1;
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquipmentPositionChange of BaseEquipmentManager', ex);
		        retval = -1;
		    }

		    return retval;
		},
		
		deleteEquipment: function(equipment, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!equipment || equipment.get('updFlag') !== 'I') {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				//TODO delete all depended data like longtextes, classifications etc.
				
				var me = this;
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(equipment);
							me.getCache().removeFromCache(equipment);
							eventController.requestEventFiring(me.EVENTS.EQUI_DELETED, equipment);
						}
						
						eventController.requestEventFiring(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteEquipment of BaseEquipmentManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				};
				
				//get the key of the equipment to delete
				equipment.set('equi', equipment.get('equnr'));
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_EQUI', equipment);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_EQUI', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteEquipment of BaseEquipmentManager', ex);
			}
			
			return retval;
		},

	    //public
	    //cuts off leading zeros, if the passed serial number contains numeric characters only
		trimSerialNumber: function (sernr) {
		    var retval = '';

		    try {
		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sernr)) {
		            //cut off leading zeros, if the passed serial number contains numeric characters only
		            if (sernr.match(/^\d+$/))
		                retval = AssetManagement.customer.utils.StringUtils.trimStart(sernr, '0');
		            else
		                retval = sernr;
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trimSerialNumber of BaseEquipmentManager', ex);
		    }

		    return retval;
		},

	    //brings a passed serial number into format used by sap
	    //serial numbers containing number characters only will get padded left with zeros
		getFormattedSerialNumber: function (serialNumberToFormat) {
		    var retval = '';

		    try {
		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(serialNumberToFormat)) {
		            //check, if the passed string contains number characters only
		            var containsNumCharsOnly = serialNumberToFormat.search(/^\d+$/) > -1;

		            if (containsNumCharsOnly) {
		                retval = AssetManagement.customer.utils.StringUtils.padLeft(serialNumberToFormat, '0', 18);
		            } else {
		                //no adjustment necessary - even leading zeros are possible and may not filtered out
		                retval = serialNumberToFormat;
		            }
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFormattedSerialNumber of BaseEquipmentManager', ex);
		    }

		    return retval;
		},
		
		//private methods
		buildEquipmentFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildEquipmentFromDbResultObject(eventArgs.target.result.value);					
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEquipmentFromDataBaseQuery of BaseEquipmentManager', ex);
			}
			
			return retval;
		},
		
		buildEquipmentsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var equi = this.buildEquipmentFromDbResultObject(cursor.value);
						store.add(equi);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEquipmentsStoreFromDataBaseQuery of BaseEquipmentManager', ex);
			}
		},
		
		buildEquipmentFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Equipment', {
					equnr: dbResult['EQUNR'],
					eqktx: dbResult['EQKTX'],
					eqtyp: dbResult['EQTYP'],
					eqart: dbResult['EQART'],
					objnr: dbResult['OBJNR'],
					invnr: dbResult['INVNR'],
					herst: dbResult['HERST'],
					herld: dbResult['HERLD'],
					serge: dbResult['SERGE'],
					typbz: dbResult['TYPBZ'],
					baujj: dbResult['BAUJJ'],
					baumm: dbResult['BAUMM'],
					matnr: dbResult['MATNR'],
					sernr: dbResult['SERNR'],
					werk: dbResult['WERK'],
					lager: dbResult['LAGER'],
					iwerk: dbResult['IWERK'],
					submt: dbResult['SUBMT'],
					hequi: dbResult['HEQUI'],
					heqnr: dbResult['HEQNR'],
					ingrp: dbResult['INGRP'],
					kund1: dbResult['KUND1'],
					kund2: dbResult['KUND2'],
					kund3: dbResult['KUND3'],
					rbnr: dbResult['RBNR'],
					spras: dbResult['SPRAS'],
					tplnr: dbResult['TPLNR'],
					vkorg: dbResult['VKORG'],
					vtweg: dbResult['VTWEG'],
					spart: dbResult['SPART'],
					stort: dbResult['STORT'],
					tidnr: dbResult['TIDNR'],
					msgrp: dbResult['MSGRP'],
					eqfnr: dbResult['EQFNR'],
					groes: dbResult['GROES'],
					mapar: dbResult['MAPAR'],
					stsma: dbResult['STSMA'],
					gewrk: dbResult['GEWRK'],
					wergw: dbResult['WERGW'],
					txtGewrk: dbResult['TXT_GEWRK'],
					adrnr: dbResult['ADRNR'],
					vtextVkorg: dbResult['VTEXT_VKORG'],
					vtextVtweg: dbResult['VTEXT_VTWEG'],
					vtextSpart: dbResult['VTEXT_SPART'],               
                    abckz: dbResult['ABCKZ'],
                    b_werk: dbResult['B_WERK'],
                    b_lager: dbResult['B_LAGER'],
                    lbbsa: dbResult['LBBSA'],
                    b_charge: dbResult['B_CHARGE'],
                    sobkz: dbResult['SOBKZ'],
                    kunnr: dbResult['KUNNR'],
                    lifnr: dbResult['LIFNR'],
                    kdauf: dbResult['KDAUF'],
                    kdpos: dbResult['KDPOS'],
                    brgew: dbResult['BRGEW'],
                    gewei: dbResult['GEWEI'],
                    usr01flag: dbResult['USR01FLAG'],
                    usr02flag: dbResult['USR02FLAG'],
                    usr03code: dbResult['USR03CODE'],
                    usr04code: dbResult['USR04CODE'],
                    usr05txt: dbResult['USR05TXT'],
                    usr06txt: dbResult['USR06TXT'],
                    usr07txt: dbResult['USR07TXT'],
                    usr09num: dbResult['USR09NUM'],

					ansdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ANSDT']),
					inbdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['INBDT']),
					gwldt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GWLDT']),
					gwlen: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GWLEN']),
					gwldt_i: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GWLDT_I']),
					gwlen_i: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GWLEN_I']),
                    usr08date: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['USR08DATE']),
	
				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY']

			    });
			
			    retval.set('id', dbResult['EQUNR']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEquipmentFromDbResultObject of BaseEquipmentManager', ex);
			}
			
			return retval;
		},
		
		loadListDependendDataForEquipments: function(eventIdToFireWhenComplete, equipments) {
			try {
				//do not continue if there is no data
				if(equipments.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, equipments);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var flocs = null;
				var partners = null;		//currently disabled
				var addresses = null;
				var statusList = null;		//currently disabled
				
				var counter = 0;				
				var done = 0;
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccurred === false) {
						me.assignListDependendDataForEquipments.call(me, eventIdToFireWhenComplete, equipments, flocs, partners, addresses, statusList);
					}
				};
				                                                    
				//TO-DO make dependend of customizing parameters --- this definitely will increase performance
	
				//get funcLoc data
				done++;

				var flocSuccessCallback = function(funcLocs) {
					errorOccurred = funcLocs === undefined;
				
					flocs = funcLocs;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(false, true);
				eventController.registerOnEventForOneTime(eventId, flocSuccessCallback);
				
	//			//get partner data
	//			done++;
	//
	//			var partnerSuccessCallback = function(partnerList) {
	//				errorOccurred = partnerList === undefined;
	//		
	//				partners = partnerList;
	//				counter++;
	//				
	//				completeFunction();
	//			};
	//			
	//			eventId = AssetManagement.customer.manager.PartnerManager.getPartners(false);
	//			eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
				
				//get address data
				done++;
				
				var addressSuccessCallback = function(addressList) {
					errorOccurred = addressList === undefined;
				
					addresses = addressList;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.AddressManager.getAddresses(true);
				eventController.registerOnEventForOneTime(eventId, addressSuccessCallback);
							
	//			//get status data
	//			done++;
	//
	//			var statusSuccessCallback = function(statuslist) {
	//				errorOccurred = statuslist === undefined;
	//		
	//				statusList = statuslist;
	//				counter++;
	//				
	//				completeFunction();
	//			};
	//			
	//			eventId = AssetManagement.customer.manager.StatusManager.getStatusList(false);
	//			eventController.registerOnEventForOneTime(eventId, statusSuccessCallback);
	
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForEquipments of BaseEquipmentManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all neccessary data has been collected
		assignListDependendDataForEquipments: function(eventIdToFireWhenComplete, equipments, flocs, partners, addresses, statusList) {
			try {
				equipments.each(function(equi, index, length) {
					//begin funcLoc assignment
					var myTplnr = equi.get('tplnr');
					
					if(flocs && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myTplnr)) {
						flocs.each(function(floc, index, length) {
							if(floc.get('tplnr') === myTplnr) {
								equi.set('funcLoc', floc);
								return false;
							}
						});
					}
					//end funcLoc assignment
					
					//DISBALED
	//				
	//				//begin partners assignment
	//				var myPartners = Ext.create('Ext.data.Store', {
	//					model: 'AssetManagement.customer.model.bo.Partner',
	//					autoLoad: false
	//				});
	//				
	//				partners.each(function(partner, index, length) {
	//					if(partner.get('objnr') === order.get('objnr'))
	//						myPartners.add(partner);
	//				});
	//				
	//				//sorting is not required yet for equi list
	//				order.set('partners', myPartners);
	//				//end partners assignment
					
					//begin address assignment
					if(addresses) {
						var addressNumber = equi.get('adrnr');
						
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressNumber)) {
							addresses.each(function(address, index, length) {
								if(address.get('adrnr') === addressNumber) {
									equi.set('address', address);
									return false;
								}
							});
						}
					}
					//end address assignment
					
					//DISBALED				
	
	//				//begin status assignment
	//				var myStatusList = Ext.create('Ext.data.Store', {
	//					model: 'AssetManagement.customer.model.bo.Status',
	//					autoLoad: false
	//				});
	//				
	//				statusList.each(function(status, index, length) {
	//					if(status.get('objnr') === order.get('objnr'))
	//						myStatusList.add(status);
	//				});
	//				
	//				order.set('statusList', myStatusList);
	//				//end status assignment
				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(equipments, cache.self.DATAGRADES.LOW);
				
				//return a store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForEquipments of BaseEquipmentManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForEquipment: function(equi, eventIdToFireWhenComplete, lightVersion) {
			try {
				if(equi) {
					var equnr = equi.get('equnr');
					var objnr = equi.get('objnr');
				
					//all of assignListDependendDataForNotifs but with more details
					//additional longtext, checklist status, files ...
					
					if(lightVersion === undefined)
						lightVersion = false;

					//lightversion currently contains: address

					var funcLoc = null;
					var superiorEquipment = null;
					var partners = null;
					var address = null;		
					var objectStatusList = null;
					var classifications = null;		
					var documents = null;
					var measPoints = null;			
					var histOrders = null;
					var histNotifs = null;
					var backendLongtext = null;
					var localLongtext = null;
					var internNote = null;
					
					var counter = 0;														
					var done = 0;
					var errorOccurred = false;
					var reported = false;
					
					var me = this;
					var eventController = AssetManagement.customer.controller.EventController.getInstance();
					
					var completeFunction = function() {
						if(errorOccurred === true && reported === false) {
							eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
							reported = true;
						} else if(counter === done && errorOccurred === false) {
							me.assignDependendDataForEquipment.call(me, eventIdToFireWhenComplete, equi, lightVersion,
								funcLoc, superiorEquipment, partners, address, objectStatusList, classifications,
								 documents, measPoints, histOrders, histNotifs, backendLongtext, localLongtext, internNote);
						}
					};
					
					//TO-DO make dependend of customizing parameters --- this definitely will increase performance
				    //get local and backendlongtext

				    //get classifications
					if (lightVersion === false) {
					    done++;

					    var classificationSuccessCallback = function (classificationList) {
					        erroroccurred = classificationList === undefined;

					        classifications = classificationList;
					        counter++;

					        completeFunction();
					    };

					    eventId = AssetManagement.customer.manager.ClassificationManager.getObjClasses(objnr, true);
					    eventController.registerOnEventForOneTime(eventId, classificationSuccessCallback);
					}

					//get funcLoc
					if(lightVersion === false) {
						var tplnr = equi.get('tplnr');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
							done++;
						
							var funcLocSuccessCallback = function(floc) {
								errorOccurred = floc === undefined;
						
								funcLoc = floc;
								counter++;
								
								completeFunction();
							};
							
							eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
							eventController.registerOnEventForOneTime(eventId, funcLocSuccessCallback);
						}
					}
					
					//get superior equi
					if(lightVersion === false) {
						var hequi = equi.get('hequi');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(hequi)) {
							done++;
						
							var supEquiSuccessCallback = function(supEqui) {
								errorOccurred = supEqui === undefined;
						
								superiorEquipment = supEqui;
								counter++;
								
								completeFunction();
							};
							
							eventId = me.getEquipmentLightVersion(hequi, true);
							eventController.registerOnEventForOneTime(eventId, supEquiSuccessCallback);
						}
					}
		
                    //measPoints
					if (lightVersion === false) {
					  
					    done++;

					    var measPointsSuccessCallBack = function (measPointList) {
					        errorOccurred = measPointList === undefined;

					        measPoints = measPointList;
					        counter++;

					        completeFunction();
					    };

					    eventId = AssetManagement.customer.manager.MeasurementPointManager.getMeasPointsForEquipment(objnr, true);
					    eventController.registerOnEventForOneTime(eventId, measPointsSuccessCallBack);
					    
					}

					//get partners
					if(lightVersion === false) {
						done++;
					
						var partnerSuccessCallback = function(partnerList) {
							errorOccurred = partnerList === undefined;
					
							partners = partnerList;
							counter++;
							
							completeFunction();
						};
						
						eventId = AssetManagement.customer.manager.PartnerManager.getAllPartnersForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
					}
					
					//get address
					if(true) {
						var adrnr = equi.get('adrnr');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(adrnr)) {
							done++;
							
							var addressSuccessCallback = function(addresss) {
								errorOccurred = addresss === undefined;
								
								address = addresss;
								counter++;
								
								completeFunction();
							};
							
							eventId = AssetManagement.customer.manager.AddressManager.getAddress(adrnr, true);
							eventController.registerOnEventForOneTime(eventId, addressSuccessCallback);
						}
					}
					
					//get objectStatus
					if(lightVersion === false) {		
						done++;
						
						var objectStatusSuccessCallback = function(objStatusList) {
							errorOccurred = objStatusList === undefined;
						
							objectStatusList = objStatusList;
							counter++;
							
							completeFunction();
						};
						
						eventId = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, objectStatusSuccessCallback);
					}
				
					//get documents
					if(lightVersion === false) {
						documents = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.File',
							autoLoad: false
						});
						
						//get dms documents
						done++;
					
						var dmsDocsSuccessCallback = function(dmsDocs) {
							errorOccurred = dmsDocs === undefined;
						
							if(errorOccurred) {
								documents = null;
							} else if(documents && dmsDocs && dmsDocs.getCount() > 0) {
								dmsDocs.each(function(dmsDoc) {
									documents.add(dmsDoc);
								}, this);
							}
							
							counter++;
							
							completeFunction();
						};
						
						eventId = AssetManagement.customer.manager.DMSDocumentManager.getDmsDocumentsForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, dmsDocsSuccessCallback);
						
						//get doc upload records
						done++;
						
						var docUploadsSuccessCallback = function(docUploads) {
							errorOccurred = docUploads === undefined;
						
							if(errorOccurred) {
								documents = null;
							} else if(documents && docUploads && docUploads.getCount() > 0) {
								docUploads.each(function(docUpload) {
									documents.add(docUpload);
								}, this);
							}
							
							counter++;
							
							completeFunction();
						};
						
						eventId = AssetManagement.customer.manager.DocUploadManager.getDocUploadsForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, docUploadsSuccessCallback);
					}
					
					//get hist orders
					if(lightVersion === false) {		
						done++;
						
						var histOrdersSuccessCallback = function(histOrderss) {
							errorOccurred = histOrderss === undefined;
						
							histOrders = histOrderss;
							counter++;
							
							completeFunction();
						};
						
						eventId = AssetManagement.customer.manager.HistOrderManager.getHistOrdersForEquipment(equnr, true);
						eventController.registerOnEventForOneTime(eventId, histOrdersSuccessCallback);
					}
					
					//get hist notifs
					if(lightVersion === false) {		
						done++;
						
						var histNotifsSuccessCallback = function(histNotifss) {
							errorOccurred = histNotifss === undefined;
						
							histNotifs = histNotifss;
							counter++;
							
							completeFunction();
						};
						
						eventId = AssetManagement.customer.manager.HistNotifManager.getHistNotifsForEquipment(equnr, true);
						eventController.registerOnEventForOneTime(eventId, histNotifsSuccessCallback);
					}

                    //get longtext
					if (lightVersion === false) {
					    done++;

					    var longtextSuccessCallback = function (localLT, backendLT) {
					        erroroccurred = localLT === undefined || backendLT === undefined;
					        localLongtext = localLT;
					        backendLongtext = backendLT;
					        counter++;

					        completeFunction();
					    };

					    eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(equi, true);
					    eventController.registerOnEventForOneTime(eventId, longtextSuccessCallback);
					}

				    //get internNote
					if (lightVersion === false)
					{
					    done++;

					    var internNoteSuccessCallback = function (iNote) {
					        erroroccurred = iNote === undefined;
					        internNote = iNote;
					        counter++;

					        completeFunction();
					    }

					    eventId = AssetManagement.customer.manager.LongtextManager.getInternNote(equi, true);
					    eventController.registerOnEventForOneTime(eventId, internNoteSuccessCallback);
					}

					if(done > 0) {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					} else {
						completeFunction();
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForEquipment of BaseEquipmentManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForEquipment: function(eventIdToFireWhenComplete, equi, lightVersion, funcLoc, superiorEquipment,
													partners, address, objectStatusList, classifications, documents,
														measPoints, histOrders, histNotifs, backendLongtext, localLongtext, internNote)
		{
			try
			{
				if(!equi) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
					return;
				}
				
				equi.set('funcLoc', funcLoc);
				equi.set('superiorEquipment', superiorEquipment);
				equi.set('classifications', classifications);
				equi.set('address', address);
				equi.set('objectStatusList', objectStatusList);
				equi.set('measPoints', measPoints);
				equi.set('documents', documents);
				equi.set('partners', partners);
				equi.set('historicalOrders', histOrders);
				equi.set('historicalNotifs', histNotifs);
				equi.set('backendLongtext', backendLongtext);
				equi.set('localLongtext', localLongtext);
				equi.set('internNote', internNote);
				
				var cache = this.getCache();
				var dataGrade = lightVersion === true ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				cache.addToCache(equi, dataGrade);
				
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, equi);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForEquipment of BaseEquipmentManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			} 
		},
		
		//build databaseobject for equiChange
		buildDataBaseObjectForEquiChange: function(equipment, changeDate) {
			var retval = null;
			
			try {
				var ac = AssetManagement.customer.core.Core.getAppConfig();
			
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
			    retval['USERID'] = ac.getUserId();
			    retval['TPLNR'] = equipment.get('tplnr');
			    retval['EQUNR'] = equipment.get('equnr');
			    retval['HEQUI'] = equipment.get('hequi');
			    retval['HEQNR'] = equipment.get('heqnr');
			    retval['INST_DATE'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(changeDate);
			    retval['INST_TIME'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(changeDate);
			    retval['UPDFLAG'] = 'I';
			    retval['MOBILEKEY'] = AssetManagement.customer.manager.KeyManager.createKey([equipment.get('equnr'), changeDate.getTime()]);
			    retval['MATNR'] = equipment.get('matnr');
			    retval['SERNR'] = equipment.get('sernr');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForEquiChange of BaseEquipmentManager', ex);
			}     
			    
			return retval;
		},
		
		//builds the raw object to store in the database out of a equipment model
		buildDataBaseObjectForEqui: function(equipment) {
			var retval = null;
			var ac = null;
		
			try {
				ac = AssetManagement.customer.core.Core.getAppConfig();
			
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
			    retval['USERID'] =  ac.getUserId();
			    retval['EQUNR'] = equipment.get('equnr');
			    retval['EQKTX'] = equipment.get('eqktx');
			    retval['EQTYP'] = equipment.get('eqtyp');
			    retval['EQART'] = equipment.get('eqart');
			    retval['OBJNR'] = equipment.get('objnr');
			    retval['INVNR'] = equipment.get('invnr');
			    retval['HERST'] = equipment.get('herst');
			    retval['HERLD'] = equipment.get('herld');
			    retval['SERGE'] = equipment.get('serge');
			    retval['TYPBZ'] = equipment.get('typbz');
		     	retval['BAUJJ'] = equipment.get('baujj');
			    retval['BAUMM'] = equipment.get('baumm');
			    retval['MATNR'] = equipment.get('matnr');
		    	retval['SERNR'] = equipment.get('sernr');
		    	retval['WERK'] = equipment.get('werk');
		    	retval['LAGER'] = equipment.get('lager');
			    retval['IWERK'] = equipment.get('iwerk');
			    retval['SUBMT'] = equipment.get('submt');
			    retval['HEQUI'] = equipment.get('hequi');
			    retval['HEQNR'] = equipment.get('heqnr');
			    retval['INGRP'] = equipment.get('ingrp');
			    retval['KUND1'] = equipment.get('kund1');
			    retval['KUND2'] = equipment.get('kund2');
			    retval['KUND3'] = equipment.get('kund3');
			    retval['RBNR'] = equipment.get('rbnr');
			    retval['SPRAS'] = equipment.get('spras');
			    retval['TPLNR'] = equipment.get('tplnr');
			    retval['VKORG'] = equipment.get('vkorg');
			    retval['VTWEG'] = equipment.get('vtweg');
			    retval['SPART'] = equipment.get('spart');
			    retval['STORT'] = equipment.get('stort');
			    retval['TIDNR'] = equipment.get('tidnr');
			    retval['MSGRP'] = equipment.get('msgrp');
			    retval['EQFNR'] = equipment.get('eqfnr');
		    	retval['GROES'] = equipment.get('groes');
			    retval['MAPAR'] = equipment.get('mapar');
			    retval['STSMA'] = equipment.get('stsma');
			    retval['GEWRK'] = equipment.get('gewrk');
			    retval['WERGW'] = equipment.get('wergw');
			    retval['TXT_GEWRK'] = equipment.get('txtGewrk');
			    retval['ADRNR'] = equipment.get('adrnr');
			    retval['VTEXT_VKORG'] = equipment.get('vtextVkorg');
			    retval['VTEXT_VTWEG'] = equipment.get('vtextVtweg');
			    retval['VTEXT_SPART'] = equipment.get('vtextSpart');
                retval['ABCKZ'] = equipment.get('abckz');
                retval['B_WERK'] = equipment.get('b_werk');
                retval['B_LAGER'] = equipment.get('b_lager');
                retval['LBBSA'] = equipment.get('lbbsa');
                retval['B_CHARGE'] = equipment.get('b_charge');
                retval['SOBKZ'] = equipment.get('sobkz');
                retval['KUNNR'] = equipment.get('kunnr');
                retval['LIFNR'] = equipment.get('lifnr');
                retval['KDAUF'] = equipment.get('kdauf');
                retval['KDPOS'] = equipment.get('kdpos');
                retval['BRGEW'] = equipment.get('brgew');
                retval['GEWEI'] = equipment.get('gewei');
                retval['USR01FLAG'] = equipment.get('usr01flag');
                retval['USR02FLAG'] = equipment.get('usr02flag');
                retval['USR03CODE'] = equipment.get('usr03code');
                retval['USR04CODE'] = equipment.get('usr04code');
                retval['USR05TXT'] = equipment.get('usr05txt');
                retval['USR06TXT'] = equipment.get('usr06txt');
                retval['USR07TXT'] = equipment.get('usr07txt');
                retval['USR09NUM'] = equipment.get('usr09num');
			
			    retval['ANSDT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('ansdt'));
			    retval['INBDT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('inbdt'));
			    retval['GWLDT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('gwldt'));
			    retval['GWLEN'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('gwlen'));
			    retval['GWLDT_I'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('gwldt_i'));
			    retval['GWLEN_I'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('gwlen_i'));
                retval['USR08DATE'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(equipment.get('usr08date'));
			
			    retval['UPDFLAG'] = equipment.get('updFlag');
			    retval['MOBILEKEY'] = equipment.get('mobileKey');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForEqui of BaseEquipmentManager', ex);
			}
			
			return retval;
		}
	}
});