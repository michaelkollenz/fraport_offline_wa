﻿Ext.define('AssetManagement.base.manager.BaseViewLayoutManager', {

    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.model.bo.ViewLayout',
	    'AssetManagement.customer.helper.CursorHelper',
	    'AssetManagement.customer.utils.StringUtils',
	    'Ext.util.HashMap',
	    'Ext.data.Store'
    ],

    objArray: [],
    inheritableStatics: {

        initializeViewLayoutInstance: function (instance) {
            try {
                //not calling the the setInitialized API to prevent an event firing
                instance.set('isInitialized', false);

                var me = this;
                var viewLayoutFetched = false;
                var aborted = false;

                //setting a hasmap to get all values of form the db table
                var keyMap = Ext.create('Ext.util.HashMap');
                instance.set('hashMap', keyMap);
                var successCallbackForViewLayout = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        if (!done && aborted === false) {
                            viewLayoutFetched = me.buildViewLayoutStoreFromDataBaseQuery.call(me, instance, eventArgs, keyMap);

                            if (viewLayoutFetched === false) {
                                aborted = true;
                            }
                        }

                        if (done)
                            instance.setInitialized(viewLayoutFetched);
                    } catch (ex) {
                        aborted = true;
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeViewLayoutInstance of BaseCustomizingManager', ex);
                        instance.setInitialized(false);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_LAYOUT', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_LAYOUT', keyRange, successCallbackForViewLayout, null, true);

                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeViewLayoutInstance of BaseCustomizingManager', ex);
                instance.setInitialized(false);
            }
        },

        buildViewLayoutStoreFromDataBaseQuery: function (viewLayout, eventArgs, keyMap) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        viewLayout = this.fillViewLayout(cursor, viewLayout, keyMap);
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                    return true;
                }
                else
                    return false;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildViewLayoutStoreFromDataBaseQuery of BaseCust_ParaManager', ex);
            }
        },

        fillViewLayout: function (cursor, viewLayout, keyMap) {
            try {
                if (cursor.value) {
                    var viewName = cursor.value['VIEWNAME'];
                    var definition = JSON.stringify(cursor.value);//['VIEWCOLUMNS'];
                    keyMap.add(viewName, definition);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillViewLayout of BaseCustomizingManager', ex);
            }
        }
    }
});