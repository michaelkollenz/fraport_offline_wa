Ext.define('AssetManagement.base.manager.BaseUserManager', {
	requires: [
	    'AssetManagement.customer.manager.KeyManager',
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.utils.DateTimeUtils',
	    'AssetManagement.customer.sync.ConnectionManager',
	    'AssetManagement.customer.sync.UserLoginInfo',
	    'AssetManagement.customer.sync.UserChangePasswordResult',
	    'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {

	    MAXIMUM_OFFLINE_LOGIN_ATTEMPTS: 3,

		//public
		//callback will be called with a boolean, if the user is allowed to login
		//if no permission is given, a userInfo instance will be passed as second parameter for further information
		checkUserIsValid: function(mandt, userId, password, callback) {
			try {
				var me = this;

				var onlineCallback = function(success) {
					if(success) {
                        //if new user, create it in database
                        var innnerCallback = function(userInfo) {
                            try {
                                if (userInfo.isValid()) {
                                    me.userExists(mandt, userId, function(doesExist) {
                                        if(!doesExist) {
                                            me.createUser(mandt, userId, password);
                                        }

                                        callback(true, userInfo);
                                    });
                                } else {
                                    callback(false, userInfo);
                                }
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserIsValid of BaseUserManager', ex);

                                if(callback)
                                    callback(false, undefined);
                            }
                        };

                        var onSendFailed = function(failedType, responseContent) {
                            try {
                                var messageToPass = message;

                                //determine the kind of error, that occured and get the adequate message for it
                                switch(failedType) {
                                    case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                                    case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                                        //sap system could not be contacted - attempt offline login next
                                        //this attempt will result with a connection error message, if the user has never been logged in to the device at all
                                        me.checkUserOffline(mandt, userId, password, callback);
                                        return;
                                        break;
                                        break;
                                    case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                                    default:
                                        if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(responseContent))
                                            messageToPass = responseContent;
                                        else
                                            messageToPass = Locale.getMsg('sapConnectionCouldNotBeEstablished');
                                        break;
                                }

                                if(callback)
                                    callback(false, messageToPass);
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserIsValid of BaseUserManager', ex);

                                if(callback)
                                    callback(false, undefined);
                            }
                        };

                        var onError = function(message) {
                            try {
                                if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                                    message = Locale.getMsg('generalConnectionErrorOccured');

                                this._cancelNetworkActionRequested = false;

                                if(callback)
                                    callback(false, message);
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserIsValid of BaseUserManager', ex);

                                if(callback)
                                    callback(false, undefined);
                            }
                        };

                        var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                            linkedSyncManager: me,
                            listeners: {
                                networkActionCancelled: { fn: onError, scope: me },
                                sendFailed: { fn: onSendFailed, scope: me },
                                errorOccured: { fn: onError, scope: me }
                            }
                        });

                        connManager.getUserInfo(mandt, userId, password, null, innnerCallback);

					} else {
                        me.checkUserOffline(mandt, userId, password, callback);
					}
				}


                AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback)

				// if(AssetManagement.customer.helper.NetworkHelper.isClientOnline()) {
                //
				// } else {
                //
				// }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserIsValid of BaseUserManager', ex);
				
				if(callback)
					callback(false, undefined);
			}
		},
		
		//callback will be called with a boolean, if the password change has been successful
		//if it was not, a UserChangePasswordResult instance will be passed as second parameter for further information
		tryChangeUserPassword: function(mandt, userId, password, newPassword, callback) {
			try {
				var me = this;

				var onlineCallback = function(success) {
					if(!success) {
                        var result = Ext.create('AssetManagement.customer.sync.UserChangePasswordResult');
                        result.setSuccess(false);
                        result.setMessage(Locale.getMsg('noNetworkConnectionAvailable'));

                        if(callback)
                            callback(false, result);
					} else {
                        var remoteCallback = function(result) {
                            try {
                                if(result.getSuccess() === true) {
                                    me.userExists(mandt, userId, function(doesExist) {
                                        if(doesExist === true) {
                                            //change password in database
                                            me.changeUserPassword(mandt, userId, newPassword);
                                        }

                                        if(callback)
                                            callback(true);
                                    });
                                } else {
                                    if(callback)
                                        callback(false, result);
                                }
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryChangeUserPassword of BaseUserManager', ex);

                                if(callback)
                                    callback(false, undefined);
                            }
                        };

                        var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager');

                        connManager.changeUserPassword(mandt, userId, password, newPassword, remoteCallback);
					}
				}

                AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);

				/*if(AssetManagement.customer.helper.NetworkHelper.isClientOnline()) {

					var remoteCallback = function(result) {
						try {
							if(result.getSuccess() === true) {
								me.userExists(mandt, userId, function(doesExist) {
									if(doesExist === true) {
										//change password in database
										me.changeUserPassword(mandt, userId, newPassword);
									}
									
									if(callback)
										callback(true);
								});
							} else {
								if(callback)
									callback(false, result);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryChangeUserPassword of BaseUserManager', ex);
							
							if(callback)
								callback(false, undefined);
						}
					};
					
					var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager');
					
					connManager.changeUserPassword(mandt, userId, password, newPassword, remoteCallback);
				} else {
					var result = Ext.create('AssetManagement.customer.sync.UserChangePasswordResult');
					result.setSuccess(false);
					result.setMessage(Locale.getMsg('noNetworkConnectionAvailable'));
					
					if(callback)
						callback(false, result);
				}*/
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryChangeUserPassword of BaseUserManager', ex);
				
				if(callback)
					callback(false, undefined);
			}
		},
		
		updateUser: function(mandt, userId, username) {
			try {
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mandt) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userId))
					return;
					
				var successCallback = function(eventArgs) {
					if(eventArgs && eventArgs.target && eventArgs.target.result) {
						var now = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
						var userObject = eventArgs.target.result.value;
						
						userObject['NAME'] = username;
						userObject['LAST_LOGIN_DATE'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(now);
						userObject['LAST_LOGIN_TIME'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(now);
						userObject['FAILED_LOGINS_COUNTER'] = "0";
						
						AssetManagement.customer.helper.CursorHelper.doCursorUpdate(eventArgs.target.result, userObject);
					}			
				};
				
				var keyRange = IDBKeyRange.only([ mandt, userId ]);
				
				AssetManagement.customer.core.Core.getDataBaseHelper().query('S_USER', keyRange, successCallback, null, false);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateUser of BaseUserManager', ex);
			}
		},

		//private
		checkUserOffline: function(mandt, userId, password, callback) {
		    try {

		        var that = this;

				var hashedPassword = AssetManagement.customer.manager.KeyManager.hash(password);
			
				var successCallback = function(eventArgs) {
					try {
						var entry = (eventArgs && eventArgs.target && eventArgs.target.result) ? eventArgs.target.result.value : null;
						
						if (entry) {
						    //user is known, first check if user is blocked due to maximum count of failed login attemps					
						    var failedLoginCount = eventArgs.target.result.value['FAILED_LOGINS_COUNTER'];

						    var countAsInt = 0;

						    try {
						        countAsInt = parseInt(failedLoginCount);
						    } catch (ex) {
						        countAsInt = that.MAXIMUM_OFFLINE_LOGIN_ATTEMPTS;
						    }

						    if (countAsInt >= that.MAXIMUM_OFFLINE_LOGIN_ATTEMPTS) {
						        callback(false, Locale.getMsg('loginLimitReached'));
						    } else {
						        //not blocked by failed offline login attempts, now check password
						        var hashedPassword = AssetManagement.customer.manager.KeyManager.hash(password);
						        var passwordMatches = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(hashedPassword) && hashedPassword === entry['PASSHASH'];

						        if (!passwordMatches) {
						            var userObject = eventArgs.target.result.value;

						            var newCount = countAsInt + 1;

						            userObject['FAILED_LOGINS_COUNTER'] = newCount.toString();

						            AssetManagement.customer.helper.CursorHelper.doCursorUpdate(eventArgs.target.result, userObject);
						            callback(false, Locale.getMsg('wrongPassword'));
						        } else {
						            //user authentication successfull - generate a user info object containing the user's name
						            var userInfo = that.prepareUserInfoObject(eventArgs.target.result.value);
						            callback(true, userInfo);
						        }
						    }
						} else {
						    callback(false, Locale.getMsg('sapConnectionCouldNotBeEstablished'));
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserOffline of BaseUserManager', ex);
						
						if(callback)
							callback(false, undefined);
					}
				};
				
				var keyRange = IDBKeyRange.only([ mandt, userId ]);
				
				AssetManagement.customer.core.Core.getDataBaseHelper().query('S_USER', keyRange, successCallback, null, false);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserOffline of BaseUserManager', ex);
				
				if(callback)
					callback(false, undefined);
			}
		},


		prepareUserInfoObject: function (rawUserInfoObj) {
		    try {
		        var userInfoObject = Ext.create('AssetManagement.customer.sync.UserLoginInfo', {
		            fromOffline: true,
		            username: rawUserInfoObj['NAME']
		        });
		        return userInfoObject;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareUserInfoObject of BaseUserManager', ex);
		    }
		},
		
		createUser: function(mandt, userId, password) {
			try {
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mandt) ||
						AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userId) ||
							AssetManagement.customer.utils.StringUtils.isNullOrEmpty(password))
					return;
					
				var hashedPassword = AssetManagement.customer.manager.KeyManager.hash(password);
				
				var now = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
				var today = AssetManagement.customer.utils.DateTimeUtils.getDateString(now);
				var time = AssetManagement.customer.utils.DateTimeUtils.getTimeString(now);
				
				var userObject = {};

				var scenario = "";
				
				userObject['MANDT'] = mandt;
				userObject['SCENARIO'] = scenario;
				userObject['USERID'] = userId;
				userObject['PASSHASH'] = hashedPassword;
				userObject['NAME'] = '';
				userObject['LAST_LOGIN_DATE'] = today;
				userObject['LAST_LOGIN_TIME'] = time;
				userObject['CREATE_DATE'] = today;
				userObject['CREATE_TIME'] = time;
				userObject['USER_LOCKED'] = '';
				userObject['FAILED_LOGINS_COUNTER'] = "0";

				
				
				var dbHelper = AssetManagement.customer.core.Core.getDataBaseHelper();
				dbHelper.put('S_USER', userObject, null, null, false);
				
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateUser of BaseUserManager', ex);
			}
		},

		userExists: function(mandt, userId, callback) {
			try {
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							callback(true);
						} else {
							callback(false);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside userExists of BaseUserManager', ex);
						
						if(callback)
							callback(false);
					}
				};
				
				var keyRange = IDBKeyRange.only([ mandt, userId ]);
				
				AssetManagement.customer.core.Core.getDataBaseHelper().query('S_USER', keyRange, successCallback, null, false);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside userExists of BaseUserManager', ex);
				
				if(callback)
					callback(false);
			}
		},
		
		deleteUser: function() {
			try {
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteUser of BaseUserManager', ex);
			}
		},
		
		changeUserPassword: function(mandt, userId, newPassword) {
			try {
				try {
					if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mandt) ||
							AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userId) ||
								AssetManagement.customer.utils.StringUtils.isNullOrEmpty(newPassword))
						return;
						
					var hashedNewPassword = AssetManagement.customer.manager.KeyManager.hash(newPassword);
						
					var successCallback = function(eventArgs) {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var userObject = eventArgs.target.result.value;
							userObject['PASSHASH'] = hashedNewPassword;
							
							AssetManagement.customer.helper.CursorHelper.doCursorUpdate(eventArgs.target.result, userObject);
						}			
					};
					
					var keyRange = IDBKeyRange.only([ mandt, userId ]);
					
					AssetManagement.customer.core.Core.getDataBaseHelper().query('S_USER', keyRange, successCallback, null, false);
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside changeUserPassword of BaseUserManager', ex);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside changeUserPassword of BaseUserManager', ex);
			}
		}
	}
});