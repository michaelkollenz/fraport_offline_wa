Ext.define('AssetManagement.base.manager.BaseCust_017Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.ConfBanf',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public
		getCust_017s: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.ConfBanf',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_017StoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_017s of BaseCust_017Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CONF_BANF', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CONF_BANF', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_017s of BaseCust_017Manager', ex);
			}
	
			return retval;
		},
		
		getCust_017: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var confBanf = me.buildCust_017DataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, confBanf);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_017 of BaseCust_017Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_CONF_BANF', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_CONF_BANF', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_017 of BaseCust_017Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_017DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {							
			     if(eventArgs) {
				    if(eventArgs.target.result) {
					   retval = this.buildCust_017FromDbResultObject(eventArgs.target.result.value);					
				    }
			     }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_017DataBaseQuery of BaseCust_017Manager', ex);
			}
			return null;
		},
		
		buildCust_017StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var confBanf = this.buildCust_017FromDbResultObject(cursor.value);
						store.add(confBanf);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_017StoreFromDataBaseQuery of BaseCust_017Manager', ex);
			}
		},
		
		buildCust_017FromDbResultObject: function(dbResult) {
            var retval = null;
	
            try {
            	retval = Ext.create('AssetManagement.customer.model.bo.ConfBanf', {
    				scenario: dbResult['SCENARIO'],
    				bsart: dbResult['BSART'],
    				mm_scenario: dbResult['MM_SCENARIO'],
    				pstyp: dbResult['PSTYP'],
    				ekorg: dbResult['EKORG'],
    				ekgrp: dbResult['EKGRP'],
    				knttp: dbResult['KNTTP']
    			});
    			
    			retval.set('id', dbResult['SCENARIO'] + dbResult['MM_SCENARIO'] );
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_017StoreFromDataBaseQuery of BaseCust_017Manager', ex);
		    }
			return retval;
		}
	}
});