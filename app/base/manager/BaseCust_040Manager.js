﻿Ext.define('AssetManagement.base.manager.BaseCust_040Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Cust_040',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
        /*
         * returns a list of cust_040 objects for the current user and scenario
         */
		//@public		
		getCust_040: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Cust_040',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_040StoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							eventController.requestEventFiring(retval, fromDataBase);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_040 of BaseCust_040Manager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			        
		
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('SPRAS', AssetManagement.customer.core.Core.getAppConfig().getCurrentLanguageShort());
				//prepare indexRange                
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_AM_CUST_040', 'SPRAS', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_AM_CUST_040', 'SPRAS', indexRange, successCallback, null, true);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_040 of BaseCust_040Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_040DataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {							
			     if(eventArgs) {
				    if(eventArgs.target.result) {
					   retval = this.buildCust_040FromDbResultObject(eventArgs.target.result.value);					
				    }
			     }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_040DataBaseQuery of BaseCust_040Manager', ex);
			}
			return null;
		},
		
		buildCust_040StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_040 = this.buildCust_040FromDbResultObject(cursor.value);
						store.add(cust_040);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_040StoreFromDataBaseQuery of BaseCust_040Manager', ex);
			}
		},
		
		buildCust_040FromDbResultObject: function(dbResult) {
            var retval = null;
	
            try {
            	retval = Ext.create('AssetManagement.customer.model.bo.Cust_040', {
    				updflag: dbResult['UPDFLAG'],
    				actype: dbResult['ACTYPE'],
					spras: dbResult['SPRAS'],
    				description: dbResult['DESCRIPTION']
    			});
    			
    			retval.set('id', dbResult['ACTYPE'] );
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_040StoreFromDataBaseQuery of BaseCust_040Manager', ex);
		    }
			return retval;
		}
	}
});