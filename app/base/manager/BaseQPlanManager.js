﻿Ext.define('AssetManagement.base.manager.BaseQPlanManager', {
    requires: [
         'AssetManagement.customer.controller.EventController',
         'AssetManagement.customer.helper.OxLogger',
         'AssetManagement.customer.utils.StringUtils',
         'Ext.data.Store',
         'AssetManagement.customer.model.bo.QPlan',
         'AssetManagement.customer.manager.QPlanOperManager',
         'AssetManagement.customer.manager.cache.QPlanCache',
         'AssetManagement.customer.manager.cache.QPlanCacheForMat'
    ],


    inheritableStatics: {
        //protected
        getCache: function () {
            var retval = null;
        
            try {
                retval = AssetManagement.customer.manager.cache.QPlanCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseQPlanManager', ex);
            }
        
            return retval;
        },

        getCacheForMat: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.QPlanCacheForMat.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCacheForMat of BaseQPlanManager', ex);
            }

            return retval;
        },

        //public methods
        getQPlan: function (plnty, plnnr, plnal, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlan',
                    autoLoad: false
                });

                //check if the cache can delivery the complete dataset
                var cache = this.getCache();
                var id = plnty + plnnr + plnal;
                var fromCache = cache.getFromCache(id);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var qplan = me.buildQPlanFromDataBaseQuery.call(me, eventArgs);

                        if (!qplan) {
                            eventController.requestEventFiring(retval, null);
                            return;
                        }

                        me.loadDependendDataForQPlan.call(me, retval, qplan);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlan of BaseQPlanManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PLNTY', plnty);
                keyMap.add('PLNNR', plnnr);
                keyMap.add('PLNAL', plnal);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLANHEAD', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLANHEAD', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlan of BaseQPlanManager', ex);
                retval = -1;
            }

            return retval;
        },

        getNewestQPlanForMaterial: function (matnr, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                //check if the cache can delivery the complete dataset
                var cache = this.getCacheForMat();
                var fromCache = cache.getFromCache(matnr);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var newestQPlanMat = null;

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        if (!done) {
                            newestQPlanMat = me.checkDBReceivedQPlanMatIsNewest.call(me, newestQPlanMat, eventArgs);
                        } else if (newestQPlanMat) {
                            var qplanSuccessCallback = function (qplan) {
                                if (qplan)
                                    cache.addToCache(qplan, matnr);

                                eventController.requestEventFiring(retval, qplan);
                            };

                            var plnty = newestQPlanMat.get('plnty');
                            var plnnr = newestQPlanMat.get('plnnr');
                            var plnal = newestQPlanMat.get('plnal');

                            var eventId = me.getQPlan(plnty, plnnr, plnal);
                            eventController.registerOnEventForOneTime(eventId, qplanSuccessCallback);
                        } else {
                            eventController.requestEventFiring(retval, null);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewestQPlanForMaterial of BaseQPlanManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };
                
                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('MATNR', matnr);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLANMAT', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLANMAT', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewestQPlanForMaterial of BaseQPlanManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private methods
        loadDependendDataForQPlan: function (eventIdToFireWhenComplete, qplan) {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                if (!qplan) {
                    eventController.requestEventFiring(eventIdToFireWhenComplete, null);
                    return;
                }

                var plnty = qplan.get('plnty');
                var plnnr = qplan.get('plnnr');
                var plnal = qplan.get('plnal');

                var qplanOperStore = null;

                var counter = 0;

                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignDependendDataForQPlan.call(me, eventIdToFireWhenComplete, qplan, qplanOperStore);
                    }
                };

                //get qplosOper
                done++;

                var qplosOperSuccessCallback = function (qplanOperReceived) {
                    errorOccurred = qplanOperReceived === undefined;

                    qplanOperStore = qplanOperReceived;
                    counter++;

                    completeFunction();
                };

                var eventId = AssetManagement.customer.manager.QPlanOperManager.getQPlanOpers(plnty, plnnr, plnal, true);
                eventController.registerOnEventForOneTime(eventId, qplosOperSuccessCallback);

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForQPlos of BaseQPlanManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        assignDependendDataForQPlan: function (eventIdToFireWhenComplete, qplan, qplanOperStore) {
            try {
                qplan.set('qplanOpers', qplanOperStore);

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, qplan);

                var cache = this.getCache();
                cache.addToCache(qplan);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForQPlan of BaseQPlanManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
            }
        },

        buildQPlanFromDataBaseQuery: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildQPlanFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanFromDataBaseQuery of BaseQPlanManager', ex);
            }

            return retval;
        },

        buildQPlanFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.QPlan', {
                    plnty: dbResult['PLNTY'],
                    plnnr: dbResult['PLNNR'],
                    plnal: dbResult['PLNAL'],
                    zaehl: dbResult['ZAEHL'],
                    ktext: dbResult['KTEXT'],
                    delkz: dbResult['DELKZ']
                });

                retval.set('id', dbResult['PLNTY'] + dbResult['PLNNR'] + dbResult['PLNAL'] + dbResult['ZAEHL']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanFromDbResultObject of BaseQPlanManager', ex);
            }

            return retval;
        },
        
        checkDBReceivedQPlanMatIsNewest: function (newestQPlanMat, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    
                    if (cursor) {
                        var qplanMat = this.buildQPlanMatFromDbResultObject(cursor.value);
                        
                        if (!newestQPlanMat || newestQPlanMat.get('datuv') < qplanMat.get('datuv'))
                            newestQPlanMat = qplanMat;

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        return newestQPlanMat;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkDBReceivedQPlanMatIsNewest of BaseQPlanManager', ex);
            }
        },

        buildQPlanMatFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('Ext.data.Model', {
                    plnty: dbResult['PLNTY'],
                    plnnr: dbResult['PLNNR'],
                    plnal: dbResult['PLNAL'],
                    zaehl: dbResult['ZAEHL'],
                    werks: dbResult['WERKS'],
                    datuv: dbResult['DATUV'],
                    matnr: dbResult['MATNR'],
                    zkriz: dbResult['ZKRIZ'],
                    rcvtime: dbResult['RCVTIME']
                });

                retval.set('id', dbResult['PLNTY'] + dbResult['PLNNR'] + dbResult['PLNAL'] + dbResult['DATUV'] + dbResult['RCVTIME']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanMatFromDbResultObject of BaseQPlanManager', ex);
            }

            return retval;
        }
    }
});
