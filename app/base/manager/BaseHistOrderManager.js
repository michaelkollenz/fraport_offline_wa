Ext.define('AssetManagement.base.manager.BaseHistOrderManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.HistOrderCache',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.OrderHistory',
        'Ext.data.Store',
        'AssetManagement.customer.helper.MobileIndexHelper'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.HistOrderCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseHistOrderManager', ex);
			}
			
			return retval;
		},
		
		getHistOrders: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.OrderHistory',
			        autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildHistOrdersStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistOrders of BaseHistOrderManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_ORDHEAD_HIST', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ORDHEAD_HIST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistOrders of BaseHistOrderManager', ex);
			}
	
			return retval;
		},
		
		getHistOrdersForFuncLoc: function(tplnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				//check if the cache can deliver func. loc.'s hist orders
		        var cache = this.getCache();
		        var fromCache = cache.getFromCacheForFuncLoc(tplnr);
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			         model: 'AssetManagement.customer.model.bo.OrderHistory',
			         autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildHistOrdersStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add func. loc.'s hist orders to cache
							cache.addToCacheForFuncLoc(tplnr, fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getFromCacheForFuncLoc(tplnr);
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistOrdersForFuncLoc of BaseHistOrderManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('TPLNR', tplnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_ORDHEAD_HIST', 'TPLNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_ORDHEAD_HIST', 'TPLNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistOrdersForFuncLoc of BaseHistOrderManager', ex);
			}
	
			return retval;
		},
		
		getHistOrdersForEquipment: function(equnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				//check if the cache can deliver equipment's hist orders
		        var cache = this.getCache();
		        var fromCache = cache.getFromCacheForEquipment(equnr);
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			         model: 'AssetManagement.customer.model.bo.OrderHistory',
			         autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildHistOrdersStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add equipment's hist orders to cache
							cache.addToCacheForEquipment(equnr, fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getFromCacheForEquipment(equnr);
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistOrdersForFuncLoc of BaseHistOrderManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('EQUNR', equnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_ORDHEAD_HIST', 'EQUNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_ORDHEAD_HIST', 'EQUNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistOrdersForFuncLoc of BaseHistOrderManager', ex);
			}
	
			return retval;
		},
		
		//private methods		
		buildHistOrderFromDataBaseQuery: function(eventArgs) {
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						var histOrder = this.buildHistOrderFromDbResultObject(eventArgs.target.result.value);			
						return histOrder;
					}
				}
			
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistOrderFromDataBaseQuery of BaseHistOrderManager', ex);
			}
			
			return null;
		},
		
		buildHistOrdersStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
					
				    if (cursor) {
				    	var histOrder = this.buildHistOrderFromDbResultObject(cursor.value);
						store.add(histOrder);
	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistOrdersStoreFromDataBaseQuery of BaseHistOrderManager', ex);
			}
		},
		
		buildHistOrderFromDbResultObject: function(dbResult) {
			try {
			    //ordinary fields
				var retval = Ext.create('AssetManagement.customer.model.bo.OrderHistory', {
					aufnr: dbResult['AUFNR'],
					auart: dbResult['AUART'],
					tplnr: dbResult['TPLNR'],
					equnr: dbResult['EQUNR'],
					bautl: dbResult['BAUTL'],
					qmnum: dbResult['QMNUM'],
					addat: dbResult['ADDAT'],
					priok: dbResult['PRIOK'],
					iwerk: dbResult['IWERK'],
					ingpr: dbResult['INGPR'],
					kunum: dbResult['KUNUM'],
					ktext: dbResult['KTEXT'],
					vaplz: dbResult['VAPLZ'],
					wawrk: dbResult['WAWRK'],
					objnr: dbResult['OBJNR'],
					system_status: dbResult['SYSTEM_STATUS'],
					user_status: dbResult['USER_STATUS'],
					ilart: dbResult['ILART'],
					
					gltrp: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GLTRP']),
					gstrp: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['GSTRP']),
		
					updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['AUFNR']);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistOrderFromDbResultObject of BaseHistOrderManager', ex);
			}
			return retval;
		}
	}
});