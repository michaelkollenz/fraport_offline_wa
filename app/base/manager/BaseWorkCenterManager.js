Ext.define('AssetManagement.base.manager.BaseWorkCenterManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.WorkCenterCache',
        'AssetManagement.customer.model.bo.WorkCenter',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.WorkCenterCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseWorkCenterManager', ex);
			}
			
			return retval;
		},
		
		//public
		getWorkCenters: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			      //it can, so return just this store
			      eventController.requestEventFiring(retval, fromCache);
			      return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			       model: 'AssetManagement.customer.model.bo.WorkCenter',
			       autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildWorkCenterStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCenters of BaseWorkCenterManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_WORKCENTER', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_WORKCENTER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCenters of BaseWorkCenterManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		getWorkCentersForArbpl: function(arbpl, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(arbpl)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var cache = this.getCache();
				var baseStore = cache.getStoreForAll();
				
			    var workCentersOfArbpl = null;
			    
			    var extractFromBase = function() {
			    	try {
						workCentersOfArbpl = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.WorkCenter',
							autoLoad: false
						});
			    	
			    		if(baseStore && baseStore.getCount() > 0) {
			    			baseStore.each(function(workCenter) {
								if(workCenter.get('arbpl') === arbpl)
									workCentersOfArbpl.add(workCenter);
							}, this);
						}
			    		
						eventController.requestEventFiring(retval, workCentersOfArbpl);
						return retval; 
			    	} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCentersForArbpl of BaseWorkCenterManager', ex);
						eventController.requestEventFiring(retval, undefined);
			    	}
			    };
				
			    //check if base store could be drawn from cache
				if(baseStore) {
					extractFromBase();
				} else {
					//base store has not yet been cached, so directly draw them from database (this will cache it)
					var allWorkCentersCallback = function(workCenters) {
						try {
							if(workCenters === undefined) {
								eventController.requestEventFiring(retval, undefined);
							} else {
								baseStore = workCenters;
								extractFromBase();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCentersForArbpl of BaseWorkCenterManager', ex);
							eventController.requestEventFiring(retval, undefined);
				    	}
					};
					
					var requestForAll = this.getWorkCenters(useBatchProcessing);
					
					if(requestForAll > -1) {
						eventController.registerOnEventForOneTime(requestForAll, allWorkCentersCallback);
					} else {
						eventController.requestEventFiring(retval, undefined);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCentersForArbpl of BaseWorkCenterManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		getWorkCenter: function(arbpl, werks, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(arbpl)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(arbpl + werks);
		
		        if(fromCache) {
		        	eventController.requestEventFiring(retval, fromCache);
		        	return retval;
		        }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var workCenter = me.buildWorkCenterDataBaseQuery.call(me, eventArgs);
						cache.addToCache(workCenter);
						eventController.requestEventFiring(retval, workCenter);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCenter of BaseWorkCenterManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('ARBPL', arbpl);
				keyMap.add('WERKS', werks);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_WORKCENTER', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_WORKCENTER', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkCenter of BaseWorkCenterManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		//private
		buildWorkCenterDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildWorkCenterFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildWorkCenterDataBaseQuery of BaseWorkCenterManager', ex);
			}
			
			return retval;
		},
		
		buildWorkCenterStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var workcenter = this.buildWorkCenterFromDbResultObject(cursor.value);
						store.add(workcenter);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildWorkCenterStoreFromDataBaseQuery of BaseWorkCenterManager', ex);
			}
		},
		
		buildWorkCenterFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.WorkCenter', {
					arbpl: dbResult['ARBPL'],
			     	werks: dbResult['WERKS'],
				    ktext: dbResult['KTEXT'],
				    veran: dbResult['VERAN'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['ARBPL'] + dbResult['WERKS']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildWorkCenterFromDbResultObject of BaseWorkCenterManager', ex);
			}
			
			return retval;
		}
	}
});