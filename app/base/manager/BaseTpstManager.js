Ext.define('AssetManagement.base.manager.BaseTpstManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Tpst',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public methods
		getTpsts: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._cacheComplete === true) {
					eventController.requestEventFiring(retval, this.getCache());
					
					return retval;
				}
				
				var toFill = this.getCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildTpstStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me._cacheComplete = true;
						
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTpsts of BaseTpstManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_TPST', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_TPST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTpsts of BaseTpstManager', ex);
			}
	
			return retval;
		}, 
		
		getTpst: function(tplnr, werks, useBatchProcessing) {
			var retval = -1;
				
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr) ||
						AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var tpst = this.getFromCache(tplnr, werks);
					
				if(tpst) {
					eventController.requestEventFiring(retval, tpst);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var tpst = me.buildTpstFromDataBaseQuery.call(me, eventArgs);
						
						if(tpst)
							me.addToCache(tpst);
							
						eventController.requestEventFiring(retval, tpst);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTpst of BaseTpstManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('TPLNR', tplnr);
				keyMap.add('WERKS', werks);
		
	            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_STL_TPST', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_STL_TPST', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTpst of BaseTpstManager', ex);
			}
			
			return retval;
		},
	
	    //private methods
		buildTpstFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			if(eventArgs) {
				if(eventArgs.target.result) {
					retval = this.buildTpstFromDbResultObject(eventArgs.target.result.value);
				}
			}
			
			return retval;
		},
		
		buildTpstStoreFromDataBaseQuery: function(store, eventArgs) {
			if(eventArgs && eventArgs.target) {
				var cursor = eventArgs.target.result;
			    if (cursor) {
			    	var tpst = this.buildTpstFromDbResultObject(cursor.value);
					store.add(tpst);
			    	
			    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
			    }
			}
		}, 
		
		buildTpstFromDbResultObject: function(dbResult) {
			var retval = Ext.create('AssetManagement.customer.model.bo.Tpst', {
				tplnr: dbResult['TPLNR'],
				werks: dbResult['WERKS'],
			    stlan: dbResult['STLAN'],
			    stlnr: dbResult['STLNR'],
			    stlal: dbResult['STLAL'],
			    
			    updFlag: dbResult['UPDFLAG']
			});
			
			retval.set('id', dbResult['TPLNR'] + dbResult['WERKS']);
			
			return retval;
		},
		
		//cache administration
		_tpstCache: null,
		_cacheComplete: false,
	
		getCache: function() {
	        try {
	        	if(!this._tpstCache) {
					this._tpstCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Tpst',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseTpstManager', ex);
			}
			
			return this._tpstCache;
		},
	
		clearCache: function() {
			try {
				this.getCache().removeAll();
				this._cacheComplete = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseTpstManager', ex);
			} 
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.CUST_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseTpstManager', ex);
			} 
		},
		
		addToCache: function(tpst) {
			try {
				var temp = this.getCache().getById(tpst.get('id'));
				
				if(temp)
					this._tpstCache.remove(temp);
					
				this._tpstCache.add(tpst);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addToCache of BaseTpstManager', ex);
			}
		},
		
		getFromCache: function(tplnr, werks) {
			var retval = null;
			
			try {
				retval = this.getCache().getById(tplnr + werks);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFromCache of BaseTpstManager', ex);
			}
			
			return retval;
		}
	}
});