Ext.define('AssetManagement.base.manager.BaseKeyManager', {
    requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.utils.DateTimeUtils',
	    'AssetManagement.customer.utils.MD5Hasher',
	    'AssetManagement.customer.utils.SHA1Hasher',
	    'AssetManagement.customer.helper.CursorHelper',
	    'AssetManagement.customer.helper.OxLogger'
    ],
	
    inheritableStatics: {
        //private
        _mobileKeyStores: null,
        _childKeyStores: null,
        _childKey2Stores: null,
	
        //public methods
        hash: function (stringToHash, algorithm) {
            var retval = '';

            try {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(algorithm))
                    algorithm = algorithm.toUpperCase();

                switch (algorithm) {
                    case 'SHA1':
                        retval = this.hashUsingSHA1(stringToHash);
                        break;

                    case 'MD5':
                    default:
                        retval = this.hashUsingMD5(stringToHash);
                        break;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hash of BaseKeyManager', ex);
            }

            return retval;
        },

        hashUsingMD5: function (stringToHash) {
            var retval = '';

            try {
                retval = AssetManagement.customer.utils.MD5Hasher.hash(stringToHash);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hashUsingMD5 of BaseKeyManager', ex);
            }

            return retval;
        },

        hashUsingSHA1: function (stringToHash) {
            var retval = '';

            try {
                retval = AssetManagement.customer.utils.SHA1Hasher.hash(stringToHash);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hashUsingSHA1 of BaseKeyManager', ex);
            }

            return retval;
        },

        createKey: function (stringsArray) {
            var retval = '';

            try {
                var toHash = '';

                Ext.Array.each(stringsArray, function (string) {
                    toHash += string;
                });

                var appConfig = AssetManagement.customer.core.Core.getAppConfig();

                toHash += appConfig.getMandt();
                toHash += appConfig.getUserId();

                toHash += Ext.util.Format.date(AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(), AssetManagement.customer.utils.DateTimeUtils.RFC2445_FORMAT);

                retval = this.hash(toHash);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createKey of BaseKeyManager', ex);
            }

            return retval;
        },

        createUUID: function () {
            var retval = '';

            try {
                retval = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                });
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createUUID of BaseKeyManager', ex);
            }

            return retval;
        },

        createGUID: function (ommitDashes) {
            var retval = '';

            try {
                retval = this.createUUID();

                if (ommitDashes === true && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval))
                    retval = retval.replace(/-/g, '');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createUUID of BaseKeyManager', ex);
            }

            return retval;
        },

        deleteMobileKeyData: function (key, store, inTransaction) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                } else if (!this.isMobileKeyStore(store)) {
                    eventController.requestEventFiring(retval, 0);
                    return retval;
                }

                var doDeleteForAllStores = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(store);
                var deletedRecords = 0;
                var pendingRequests = 0;
                var errorOccured = false;
                var reported = false;

                var storeFinishedFunction = function () {
                    if (errorOccured === true && reported === false) {
                        eventController.fireEvent(retval, undefined);
                        reported = true;
                    } else if (--pendingRequests === 0 && reported === false) {
                        eventController.fireEvent(retval, deletedRecords);
                    }
                };

                var successCallback = function (eventArgs) {
                    try {
                        var cursor = null;

                        if (eventArgs && eventArgs.target) {
                            cursor = eventArgs.target.result;
                        }

                        if (cursor) {
                            deletedRecords++;
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            //deleting for this store completed
                            storeFinishedFunction();
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMobileKeyData of BaseKeyManager', ex);
                        errorOccured = true;
                        storeFinishedFunction();
                    }
                };

                var databaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();
                //prepare indexMap
                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('MOBILEKEY', key);

                if (doDeleteForAllStores === false) {
                    //prepare indexRange
                    var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'MOBILEKEY', indexMap);
                    pendingRequests++;
                    databaseHelper.queryUsingAnIndex(store, 'MOBILEKEY', indexRange, successCallback, null, inTransaction);
                } else {
                    Ext.Array.each(this.getMobileKeyStores(), function (store) {
                        if(store === 'S_DOCUPLOAD') {
                            //skip delete this store because the design needs this extra rule
                            return true;
                        }
					
                        var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'MOBILEKEY', indexMap);

                        pendingRequests++;
                        databaseHelper.queryUsingAnIndex(store, 'MOBILEKEY', childKeyIndexRange, successCallback, null, true);
                    }, this);

                    if (inTransaction === false)
                        databaseHelper.doExecuteCommandQueue();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMobileKeyData of BaseKeyManager', ex);
                retval = -1;
            }

            return retval;
        },

        deleteChildKeyData: function (key, store, inTransaction) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                } else if (!this.isChildKeyStore(store)) {
                    eventController.requestEventFiring(retval, 0);
                    return retval;
                }

                var doDeleteForAllStores = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(store);
                var deletedRecords = 0;
                var pendingRequests = 0;
                var errorOccured = false;
                var reported = false;

                var storeFinishedFunction = function () {
                    if (errorOccured === true && reported === false) {
                        eventController.fireEvent(retval, undefined);
                        reported = true;
                    } else if (--pendingRequests === 0 && reported === false) {
                        eventController.fireEvent(retval, deletedRecords);
                    }
                };

                var successCallback = function (eventArgs) {
                    try {
                        var cursor = null;

                        if (eventArgs && eventArgs.target) {
                            cursor = eventArgs.target.result;
                        }

                        if (cursor) {
                            deletedRecords++;
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            //deleting for this store completed
                            storeFinishedFunction();
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteChildKeyData of BaseKeyManager', ex);
                        errorOccured = true;
                        storeFinishedFunction();
                    }
                };

                var databaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();

                if (doDeleteForAllStores === false) {
                    //prepare indexRange for childkey and, if neccessary, childkey2
                    var useChildKey2 = this.isChildKey2Store(store);

                    var childKeyIndexMap = Ext.create('Ext.util.HashMap');
                    childKeyIndexMap.add('CHILDKEY', key);
                    var childKeyIndexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'CHILDKEY', childKeyIndexMap);

                    if (useChildKey2) {
                        var childKey2IndexMap = Ext.create('Ext.util.HashMap');
                        childKey2IndexMap.add('CHILDKEY2', key);
                        var childKey2IndexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'CHILDKEY2', childKey2IndexMap);
                    }

                    pendingRequests++;
                    databaseHelper.queryUsingAnIndex(store, 'CHILDKEY', indexRange, successCallback, null, inTransaction);

                    if (useChildKey2) {
                        pendingRequests++;
                        databaseHelper.queryUsingAnIndex(store, 'CHILDKEY2', indexRange, successCallback, null, inTransaction);
                    }
                } else {
                    //prepare indexMap for childkey and childkey2
                    var childKeyIndexMap = Ext.create('Ext.util.HashMap');
                    childKeyIndexMap.add('CHILDKEY', key);

                    var childKey2IndexMap = Ext.create('Ext.util.HashMap');
                    childKey2IndexMap.add('CHILDKEY2', key);

                    //all stores containing a childkey2 also have an usual childkey
                    Ext.Array.each(this.getChildKeyStores(), function (store) {
                        var childKeyIndexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'CHILDKEY', childKeyIndexMap);

                        pendingRequests++;
                        databaseHelper.queryUsingAnIndex(store, 'CHILDKEY', childKeyIndexRange, successCallback, null, true);

                        if (this.isChildKey2Store(store)) {
                            var childKey2IndexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'CHILDKEY2', childKey2IndexMap);

                            pendingRequests++;
                            databaseHelper.queryUsingAnIndex(store, 'CHILDKEY2', childKey2IndexRange, successCallback, null, true);
                        }
                    }, this);

                    if (inTransaction === false)
                        databaseHelper.doExecuteCommandQueue();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteChildKeyData of BaseKeyManager', ex);
                retval = -1;
            }

            return retval;
        },

        //sync only apis
        getDeleteMobileKeyDataCommands: function (key, successCallback, errorCallback, callbackScope) {
            //the passed callbacks will be called, when the work finishes or encounters an error
            var retval = null;

            try {
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    retval = undefined;
                    return retval;
                }

                var deletedRecords = 0;
                var pendingRequests = 0;
                var errorOccured = false;
                var reported = false;

                var storeFinishedFunction = function () {
                    if (errorOccured === true && reported === false) {
                        errorCallback.call(callbackScope);
                        reported = true;
                    } else if (--pendingRequests === 0 && reported === false && errorCallback) {
                        successCallback.call(callbackScope, deletedRecords);
                    }
                };

                //generate a index based query command for each affected store 
                //on it's successcall back the affected objects will be deleted
                var innerSuccessCallback = function (eventArgs) {
                    try {
                        var cursor = null;

                        if (eventArgs && eventArgs.target) {
                            cursor = eventArgs.target.result;
                        }

                        if (cursor) {
                            deletedRecords++;
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            //deleting for this store completed
                            storeFinishedFunction();
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteMobileKeyDataCommands of BaseKeyManager', ex);
                        errorOccured = true;
                        storeFinishedFunction();
                    }
                };

                var innerFailureCallback = function () {
                    try {
                        errorOccured = true;
                        storeFinishedFunction();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteMobileKeyDataCommands of BaseKeyManager', ex);
                    }
                };

                var databaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();
                retval = new Array();

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('MOBILEKEY', key);

                Ext.Array.each(this.getMobileKeyStores(), function (store) {
                    if(store === 'S_DOCUPLOAD') {
                        //skip delete this store because the design needs this extra rule
                        return true;
                    }
				
                    var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'MOBILEKEY', indexMap);

                    pendingRequests++;
                    retval.push(databaseHelper.getQueryWithIndexCommand(store, 'MOBILEKEY', indexRange, innerSuccessCallback, innerFailureCallback));
                }, this);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteMobileKeyDataCommands of BaseKeyManager', ex);
                retval = undefined;
            }

            return retval;
        },

        getDeleteChildKeyDataCommands: function (key, successCallback, errorCallback, callbackScope) {
            //the passed callbacks will be called, when the work finishes or encounters an error
            var retval = null;

            try {
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(key)) {
                    retval = undefined;
                    return retval;
                }

                var deletedRecords = 0;
                var pendingRequests = 0;
                var errorOccured = false;
                var reported = false;

                var storeFinishedFunction = function () {
                    if (errorOccured === true && reported === false) {
                        errorCallback.call(callbackScope);
                        reported = true;
                    } else if (--pendingRequests === 0 && reported === false && errorCallback) {
                        successCallback.call(callbackScope, deletedRecords);
                    }
                };

                //generate index based query commands for each affected store 
                //on it's successcall back the affected objects will be deleted
                var innerSuccessCallback = function (eventArgs) {
                    try {
                        var cursor = null;

                        if (eventArgs && eventArgs.target) {
                            cursor = eventArgs.target.result;
                        }

                        if (cursor) {
                            deletedRecords++;
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            //deleting for this store completed
                            storeFinishedFunction();
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteChildKeyDataCommands of BaseKeyManager', ex);
                        errorOccured = true;
                        storeFinishedFunction();
                    }
                };

                var innerFailureCallback = function () {
                    try {
                        errorOccured = true;
                        storeFinishedFunction();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteChildKeyDataCommands of BaseKeyManager', ex);
                    }
                };

                var databaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();
                retval = new Array();

                //prepare indexMap for childkey and childkey2
                var childKeyIndexMap = Ext.create('Ext.util.HashMap');
                childKeyIndexMap.add('CHILDKEY', key);

                var childKey2IndexMap = Ext.create('Ext.util.HashMap');
                childKey2IndexMap.add('CHILDKEY2', key);

                //all stores containing a childkey2 also have an usual childkey
                Ext.Array.each(this.getChildKeyStores(), function (store) {
                    var childKeyIndexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'CHILDKEY', childKeyIndexMap);

                    pendingRequests++;
                    retval.push(databaseHelper.getQueryWithIndexCommand(store, 'CHILDKEY', childKeyIndexRange, innerSuccessCallback, innerFailureCallback));

                    if (this.isChildKey2Store(store)) {
                        var childKey2IndexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(store, 'CHILDKEY2', childKey2IndexMap);

                        pendingRequests++;
                        retval.push(databaseHelper.getQueryWithIndexCommand(store, 'CHILDKEY2', childKey2IndexRange, innerSuccessCallback, innerFailureCallback));
                    }
                }, this);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteChildKeyDataCommands of BaseKeyManager', ex);
                retval = undefined;
            }

            return retval;
        },

        isMobileKeyStore: function (store) {
            var retval = false;

            try {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(store))
                    retval = Ext.Array.contains(this.getMobileKeyStores(), store);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isMobileKeyStore of BaseKeyManager', ex);
                retval = false;
            }

            return retval;
        },

        isChildKeyStore: function (store) {
            var retval = false;

            try {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(store))
                    retval = Ext.Array.contains(this.getChildKeyStores(), store);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isChildKeyStore of BaseKeyManager', ex);
                retval = false;
            }

            return retval;
        },

        isChildKey2Store: function (store) {
            var retval = false;
            try {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(store))
                    retval = Ext.Array.contains(this.getChildKey2Stores(), store);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isChildKey2Store of BaseKeyManager', ex);
                retval = false;
            }

            return retval;
        },
		
        //private methods
        getMobileKeyStores: function () {
            var retval;
		
            try {
                if(!this._mobileKeyStores) {
                    var temp = new Array();
				
                    var storeInfoMap = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformationMap();
					
                    if(storeInfoMap && storeInfoMap.getCount() > 0) {
                        storeInfoMap.each(function(storeInfo) {
                            if(storeInfo.hasAttribute('MOBILEKEY'))
                                temp.push(storeInfo.get('storeName'));
                        }, this);
                    }
					
                    this._mobileKeyStores = temp;
                }
				
                retval = this._mobileKeyStores;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMobileKeyStores of BaseKeyManager', ex);
            }
			
            return retval;
        },

        getChildKeyStores: function () {
            var retval;
		
            try {
                if(!this._childKeyStores) {
                    var temp = new Array();
				
                    var storeInfoMap = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformationMap();
					
                    if(storeInfoMap && storeInfoMap.getCount() > 0) {
                        storeInfoMap.each(function(storeInfo) {
                            if(storeInfo.hasAttribute('CHILDKEY'))
                                temp.push(storeInfo.get('storeName'));
                        }, this);
                    }
					
                    this._childKeyStores = temp;
                }
				
                retval = this._childKeyStores;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getChildKeyStores of BaseKeyManager', ex);
            }
			
            return retval;
        },
		
        getChildKey2Stores: function () {
            var retval;
		
            try {
                if(!this._childKey2Stores) {
                    var temp = new Array();
				
                    var storeInfoMap = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformationMap();
					
                    if(storeInfoMap && storeInfoMap.getCount() > 0) {
                        storeInfoMap.each(function(storeInfo) {
                            if(storeInfo.hasAttribute('CHILDKEY2'))
                                temp.push(storeInfo.get('storeName'));
                        }, this);
                    }
					
                    this._childKey2Stores = temp;
                }
				
                retval = this._childKey2Stores;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getChildKey2Stores of BaseKeyManager', ex);
            }
			
            return retval;
        }
    }
});