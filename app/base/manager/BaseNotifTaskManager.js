Ext.define('AssetManagement.base.manager.BaseNotifTaskManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.NotifTaskCache',
        'AssetManagement.customer.model.bo.NotifTask',
        'AssetManagement.customer.manager.CustCodeManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.StoreHelper',
        'Ext.data.Store'
    ],
    
    inheritableStatics: {
		EVENTS: {
			TASK_ADDED: 'notifTaskAdded',
			TASK_CHANGED: 'notifTaskChanged',
			TASK_DELETED: 'notifTaskDeleted'
		},
		
		//protected
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.NotifTaskCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},
	
		//public methods	
		getNotifTasks: function(withDependendData, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForNotifTasks(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
								
							me.buildNotifTasksStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
								
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForNotifTasks.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasks of BaseNotifTaskManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
						
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTITASK', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTITASK', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasks of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},
		
		getNotifTasksForNotif: function(qmnum, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();		
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
					this.loadDependendDataForNotifTasks(baseStore, retval, true);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});
				
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifTasksStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotif(qmnum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
								me.loadDependendDataForNotifTasks.call(me, baseStore, retval, true);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasksForNotif of BaseNotifTaskManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTITASK', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTITASK', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasksForNotif of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},
		
		getNotifTasksForNotifHeadOnly: function(qmnum, useBatchProcessing) {
	        var retval = null;

	        try {
			    retval = this.getNotifTasksForNotifItem(qmnum, '0', useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasksForNotifHeadOnly of BaseNotifTaskManager', ex);
			}

			return retval;
		},

		getNotifTasksForNotifItem: function(qmnum, fenum, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();		
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.MEDIUM);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotifItem(qmnum, fenum, cache.self.DATAGRADES.LOW);
					this.loadDependendDataForNotifTasks(baseStore, retval);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifTasksStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotifItem(qmnum, fenum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotifItem(qmnum, fenum, cache.self.DATAGRADES.LOW);
								me.loadDependendDataForNotifTasks.call(me, baseStore, retval);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasksForNotifItem of BaseNotifTaskManager', ex);
							eventController.fireEvent(retval, undefined);
						}
					};
					
					var indexMap = Ext.create('Ext.util.HashMap');
					indexMap.add('QMNUM', qmnum);
					indexMap.add('FENUM', fenum);
					
					var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_NOTITASK', 'QMNUM-FENUM', indexMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_NOTITASK', 'QMNUM-FENUM', indexRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifTasksForNotifItem of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},
		
		//save a notifTask - if it is neccessary it will get a new counter first
		saveNotifTask: function(notifTask) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifTask) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = notifTask.get('updFlag');
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;
				
				if(isInsert && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifTask.get('fenum')))
					notifTask.set('fenum', '0000');

				//update flag management
				this.manageUpdateFlagForSaving(notifTask);
				
				//check if the notifTask already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifTask.get('manum'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1)
							notifTask.set('manum', nextCounterValue);
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifTask.get('manum'))) {
							eventController.fireEvent(retval, false);
							return;
						}

						if(isInsert) {
							var key = AssetManagement.customer.manager.KeyManager.createKey([ notifTask.get('qmnum'), notifTask.get('fenum'), notifTask.get('manum') ]);
                            if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifTask.get('childKey'))) {
                                notifTask.set('childKey', key);
                            } else {
                                notifTask.set('childKey2', key);
                            }
						}

						//set the id
						notifTask.set('id', notifTask.get('qmnum') + notifTask.get('fenum') + notifTask.get('manum'));
	
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
							
								if(success) {
									if(isInsert === true) {
										//do not specify a datagrade, so all depenend data will loaded, when necessary
										me.getCache().addToCache(notifTask);
									}
									
									var eventType = isInsert ? me.EVENTS.TASK_ADDED : me.EVENTS.TASK_CHANGED;
									eventController.fireEvent(eventType, notifTask);
								}
							
								eventController.fireEvent(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifTask of BaseNotifTaskManager', ex);
								eventController.fireEvent(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForNotifTask(notifTask);						

						//if it is an insert perform an insert
						if(isInsert) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_NOTITASK', toSave, callback, callback);
						} else {
							//else it is an update
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_NOTITASK', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.fireEvent(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifTask of BaseNotifTaskManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextManum(notifTask);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.fireEvent(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifTask of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},
	
		deleteNotifTask: function(notifTask, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifTask) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				
				var callback = function(eventArgs) {
					try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(notifTask);
							me.getCache().removeFromCache(notifTask);

							me.deleteNotifTaskSubItems(notifTask, retval);
						}
					
						eventController.fireEvent(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifTask of BaseNotifTaskManager', ex);
						eventController.fireEvent(retval, false);
					}
				};
				
				//get the key of the notifTask to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_NOTITASK', notifTask);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_NOTITASK', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifTask of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},

		deleteNotifTaskSubItems : function(notifTask, eventIdToFireWhenComplete){
		    try {
                  //do not continue if there is no data
				if(!notifTask) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, false);
					return;
				}
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				var completeFunction = function () {
                    eventController.requestEventFiring(me.EVENTS.TASK_DELETED, notifTask);
				    eventController.requestEventFiring(eventIdToFireWhenComplete, true);
				};

                var localLongtext = notifTask.get('localLongtext');
				if (localLongtext)
				{
                     eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(notifTask, true);
                     eventController.registerOnEventForOneTime(eventId, completeFunction);
				}
                else
				{
                    eventController.requestEventFiring(me.EVENTS.TASK_DELETED, notifTask);
				    eventController.requestEventFiring(eventIdToFireWhenComplete, true);
				}
		        }
		        catch(ex)
		        {
		            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifTaskSubItems of BaseNotifTaskmanager', ex);
		        }
    
          
		},
	
		//private
		buildNotifTasksStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var notifTask = this.buildNotifTaskFromDbResultObject(cursor.value);
						store.add(notifTask);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifTasksStoreFromDataBaseQuery of BaseNotifTaskManager', ex);
			}
		},
		
		buildNotifTaskFromDbResultObject: function(dbResult) {
	        var retval = null;
	        	        
	        try {
			    retval = Ext.create('AssetManagement.customer.model.bo.NotifTask', {
			    	qmnum: dbResult['QMNUM'],
				    fenum: dbResult['FENUM'],
				    manum: dbResult['MANUM'],
				    matxt: dbResult['MATXT'],
				    mncod: dbResult['MNCOD'],
				    mngrp: dbResult['MNGRP'],
				    mnkat: dbResult['MNKAT'],
				    mnver: dbResult['MNVER'],
				    qmanum: dbResult['QMANUM'],
				    mngfa: dbResult['MNGFA'],
				    urnum: dbResult['URNUM'],
				    stsma: dbResult['STSMA'],
                    kzmla: dbResult['KZMLA'],
				
				    erzeit: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERZEIT']),
				    petdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['PSTER'], dbResult['PSTUR']),
				    pstdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['PETER'], dbResult['PETUR']),
		
			        updFlag: dbResult['UPDFLAG'],
				    mobileKey: dbResult['MOBILEKEY'],
				    childKey: dbResult['CHILDKEY'],
				    childKey2: dbResult['CHILDKEY2']
			    });
			
			    retval.set('id', dbResult['QMNUM'] + dbResult['FENUM'] + dbResult['MANUM']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifTaskFromDbResultObject of BaseNotifTaskManager', ex);
			}
						
			return retval;
		},
		
		loadListDependendDataForNotifTasks: function(eventIdToFireWhenComplete, notifTasks) {
			try {
				//do not continue if there is no data
				if(notifTasks.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifTasks);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				
				var counter = 0;
				
				var done = 0;
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccurred === false) {
						me.assignListDependendDataForNotifTasks.call(me, eventIdToFireWhenComplete, notifTasks);
					}
				};
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForNotifTasks of BaseNotifTaskManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all necessary data has been collected
		assignListDependendDataForNotifTasks: function(eventIdToFireWhenComplete, notifTasks) {
			try {
//				notifItems.each(function(notifItem, index, length) {
//					
//				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(notifTasks, cache.self.DATAGRADES.LOW);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForNotifTasks of BaseNotifTaskManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForNotifTasks: function(notifTasks, eventIdToFireWhenComplete, allOfOneNotif) {
			try {
				//do not continue if there is no data
				if(notifTasks.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifTasks);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//TODO Longtext - pass the task to LT Manager and let it set all textes. Fire an event on completition
				
				var custCodes = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done) {
						me.assignDependendDataForNotifTasks.call(me, eventIdToFireWhenComplete, notifTasks, custCodes, allOfOneNotif);
					}
				};
				
				//get custCodes
				done++;
				
				var custCodesSuccessCallback = function(cCodes) {
					errorOccured = cCodes === undefined;
					
					custCodes = cCodes;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
				eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);
				
				
				notifTasks.each(function (task){
					//getLocalLongtext
					if(true) {
						done++;
						
						var localLongtextSuccessCallback = function(localLT, backendLT) {
							errorOccured = localLT === undefined || backendLT === undefined;
							task.set('localLongtext', localLT);
							task.set('backendLongtext', backendLT);
							counter++;
							
							completeFunction();
						};
						eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(task, true);
						eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
					}
				}); 
					
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForNotifTasks of BaseNotifTaskManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForNotifTasks: function(eventIdToFireWhenComplete, notifTasks, custCodes, allOfOneNotif) {
			try {
				notifTasks.each(function (task, index, length) {
					//assign codes
					if(custCodes) {
						var katalogArt = task.get('mnkat');
						var codeGruppe = task.get('mngrp');
						var code = task.get('mncod');
						
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
							var groupHashMapForCatalogue = custCodes.get(katalogArt);
							
							if(groupHashMapForCatalogue) {
								var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
								
								if(codesGroupStore) {
									codesGroupStore.each(function(custCode) {
										if(custCode.get('code') === code) {
											task.set('taskCustCode', custCode);
											return false;
										}
									}, this);
								}
							}
						}
					}
				});
				
				notifTasks.sort('manum', 'ASC');
				
				var cache = this.getCache();
				var qmnum = notifTasks.getAt('0').get('qmnum');
				var toReturn = null;
				
				if(allOfOneNotif) {
					cache.addToCacheForNotif(qmnum, notifTasks, cache.self.DATAGRADES.MEDIUM);
					toReturn = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM); 
				} else {
					var fenum = notifTasks.getAt('0').get('fenum');
					cache.addToCacheForNotifItem(qmnum, fenum, notifTasks, cache.self.DATAGRADES.MEDIUM);
					toReturn = cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.MEDIUM); 
				}
				
				//return the store from cache to eliminate duplicate objects
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForNotifTasks of BaseNotifTaskManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//builds the raw object to store in the database out of a notifTask model
		buildDataBaseObjectForNotifTask: function(notifTask) {
			var retval = { };
			
			try {
				retval['QMNUM'] = notifTask.get('qmnum');
				retval['FENUM'] = notifTask.get('fenum');
				retval['MANUM'] = notifTask.get('manum');
				retval['QMANUM'] = notifTask.get('manum');
				retval['URNUM'] = notifTask.get('urnum');
				retval['MNKAT'] = notifTask.get('mnkat');
				retval['MNGRP'] = notifTask.get('mngrp');
				retval['MNCOD'] = notifTask.get('mncod');
				retval['MNVER'] = notifTask.get('mnver');
				retval['MNGFA'] = notifTask.get('mngfa');
				retval['MATXT'] = notifTask.get('matxt');
				
				retval['PSTER'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notifTask.get('pstdt'));
				retval['PETER'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notifTask.get('petdt'));
				retval['PSTUR'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifTask.get('pstdt'));
				retval['PETUR'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifTask.get('petdt'));
				retval['ERZEIT'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifTask.get('erzeit'));
				
				retval['MOBILEKEY'] = notifTask.get('mobileKey');
				retval['CHILDKEY'] = notifTask.get('childKey');
				retval['CHILDKEY2'] = notifTask.get('childKey2');
				retval['UPDFLAG'] = notifTask.get('updFlag');			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForNotifTask of BaseNotifTaskManager', ex);
			}
			
			return retval;
		},
		
		getNextManum: function(notifTask) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifTask) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxManum = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
                            var manumValue = cursor.value['MANUM'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(manumValue, '%')) {
                                //cut of the percent sign
							    manumValue = manumValue.substr(1);
                                var curManumValue = parseInt(manumValue);
							
							    if(!isNaN(curManumValue))
							    {
								    if(curManumValue > maxManum)
									    maxManum = curManumValue;
							    }
						    }
                            				
							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxManum + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
						    eventController.fireEvent(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextManum of BaseNotifTaskManager', ex);
						eventController.fireEvent(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QMNUM', notifTask.get('qmnum'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTITASK', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTITASK', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextManum of BaseNotifTaskManager', ex);
			}
			
			return retval;			
		}
	}
});