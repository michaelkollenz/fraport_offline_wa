Ext.define('AssetManagement.base.manager.BaseOptionsMenuManager', {
	inheritableStatics: {
		//public
		OPTION_NEWITEM: 0,
		OPTION_ADD: 1,
		OPTION_SAVE: 2,
		OPTION_DELETE: 3,
		OPTION_SEARCH: 4,
		OPTION_RFID: 5,
        OPTION_CLEAR_SEARCH: 6,
		
		//private
		_mainView: null,
		
		_newItemButton: null,
		_addButton: null,
		_saveButton: null,
		_deleteButton: null,
		_searchButton: null,
		_clearSearchButton: null,

		// argument is the optionsBar to build the menu in
		// display order is: last most right
		buildOptionsMenu: function(bar, creator) {
			try {
		
			this._mainView = creator;
			
			this._newItemButton = Ext.create('Ext.Button',{
				xtype: 'button',
			    itemId: 'newItemButton',
				cls: 'optionsMenuButton',
				pressedCls: 'buttonPressed',
			    html: '<img src="resources/icons/new_item.png"></img>',
			    align: 'right',
			    hidden: false,
				listeners: {
					scope: this,
					tap: function() {
						this._mainView.onOptionMenuItemSelected(this.OPTION_NEWITEM);
						console.log("New Item clicked");
					}
				}
			});

			this._addButton = Ext.create('Ext.Button',{
						xtype: 'button',
	                    itemId: 'addButton',
						cls: 'optionsMenuButton',
						pressedCls: 'buttonPressed',
	                    html: '<img src="resources/icons/add.png"></img>',
	                    align: 'right',
	                    hidden: false,
						listeners: {
							scope: this,
							tap: function() {
								this._mainView.onOptionMenuItemSelected(this.OPTION_ADD);
								console.log("Add Button clicked");
							}
						}
	        });
			
			this._rfidButton = Ext.create('Ext.Button',{
				xtype: 'button',
			    itemId: 'rfidButton',
				cls: 'optionsMenuButton',
				pressedCls: 'buttonPressed',
			    html: '<img src="resources/icons/rfid.png"></img>',
			    align: 'right',
			    hidden: false,
				listeners: {
					scope: this,
					tap: function() {
						this._mainView.onOptionMenuItemSelected(this.OPTION_RFID);
						console.log("RFID Button clicked");
					}
				}
			});

			this._searchButton = Ext.create('Ext.Button',{
				xtype: 'button',
			    itemId: 'searchButton',
				cls: 'optionsMenuButton',
				pressedCls: 'buttonPressed',
			    html: '<img src="resources/icons/search.png"></img>',
			    align: 'right',
			    hidden: false,
				listeners: {
					scope: this,
					tap: function() {
						this._mainView.onOptionMenuItemSelected(this.OPTION_SEARCH);
						console.log("Search Button clicked");
					}
				}
			});
			
			this._clearSearchButton = Ext.create('Ext.Button', {
			    xtype: 'button',
			    itemId: 'clearSearchButton',
			    cls: 'optionsMenuButton',
			    pressedCls: 'buttonPressed',
			    html: '<img src="resources/icons/clear_search.png"></img>',
			    align: 'right',
			    hidden: false,
			    listeners: {
			        scope: this,
			        tap: function () {
			            this._mainView.onOptionMenuItemSelected(this.OPTION_CLEAR_SEARCH);
			            console.log("ClearSearch Button clicked");
			        }
			    }
			});

	        this._saveButton = Ext.create('Ext.Button',{
	                    xtype: 'button',
	                    itemId: 'saveButton',
						cls: 'optionsMenuButton',
						pressedCls: 'buttonPressed',
	                    html: '<img src="resources/icons/save.png"></img>',
	                    align: 'right',
	                    hidden: false,
						listeners: {
							scope: this,
							tap: function() {
								this._mainView.onOptionMenuItemSelected(this.OPTION_SAVE);
								console.log("Save Button clicked");
							}
						}
	        });
	        
	        
	        bar.add(this._addButton);	
			bar.add(this._rfidButton);
			bar.add(this._searchButton);
			bar.add(this._newItemButton);
			bar.add(this._saveButton);
			bar.add(this._clearSearchButton);
            } catch(ex) {
               AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildOptionsMenu of BaseOptionsMenuManager', ex);
            } 
		},
		
		
		setOptionsMenuItems: function(optionsArray) {
	        try {
	        	// first hide all items
				this._addButton.hide();
				this._saveButton.hide();
				this._newItemButton.hide();
				this._searchButton.hide();
				this._rfidButton.hide();
				
				// show the elements, which's ids are passed in optionsArray
				if(!(typeof optionsArray === 'undefined')) {
					var length = optionsArray.length;
					var curOption = null;
					for (var i = 0; i < length; i++) {
					  curOption = optionsArray[i];
					  
					  if(curOption == this.OPTION_ADD) {
						this._addButton.show();
					  } else if (curOption == this.OPTION_SAVE) {
						this._saveButton.show();
					  } else if (curOption == this.OPTION_NEWITEM) {
						this._newItemButton.show();
					  } else if (curOption == this.OPTION_SEARCH) {
						this._searchButton.show();
					  } else if (curOption == this.OPTION_RFID) {
						this._rfidButton.show();
					  } else if (curOption === this.OPTION_CLEAR_SEARCH) {
					      this._clearSearchButton.show();
					  }
					}
				}
		    } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setOptionsMenuItems of BaseOptionsMenuManager', ex);
            }
		}	        
	}
});