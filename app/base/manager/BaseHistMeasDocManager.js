﻿Ext.define('AssetManagement.base.manager.BaseHistMeasDocManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.HistMeasDocCache',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.MeasurementDocHistory',
        'Ext.data.Store',
        'AssetManagement.customer.helper.MobileIndexHelper'
    ],

    inheritableStatics: {
        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.HistMeasDocCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseHistMeasDocManager', ex);
            }

            return retval;
        },

        getHistMeasDocs: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                //check if the cache can delivery the complete dataset
                var cache = this.getCache();
                var fromCache = cache.getStoreForAll();

                if (fromCache) {
                    //it can, so return just this store
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementDocHistory',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildHistMeasDocsStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            //add the whole store to the cache
                            cache.addStoreForAllToCache(fromDataBase);

                            //return a store from cache to eliminate duplicate objects
                            var toReturn = cache.getStoreForAll();
                            eventController.requestEventFiring(retval, toReturn);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistMeasDocs of BaseHistMeasDocManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MEASDOC_HIST', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MEASDOC_HIST', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHistMeasDocs of BaseHistMeasDocManager', ex);
                retval = -1;
            }

            return retval;
        },


        //private methods		
        buildHistMeasDocsFromDataBaseQuery: function (eventArgs) {
            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        var histMeasDocs = this.buildHistMeasDocsFromDbResultObject(eventArgs.target.result.value);
                        return histMeasDocs;
                    }
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistMeasDocsFromDataBaseQuery of BaseHistMeasDocManager', ex);
            }
            return null;
        },

        buildHistMeasDocsStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        var histMeasDocs = this.buildHistMeasDocsFromDbResultObject(cursor.value);
                        store.add(histMeasDocs);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistMeasDocsStoreFromDataBaseQuery of BaseHistMeasDocManager', ex);
            }
        },

        buildHistMeasDocsFromDbResultObject: function (dbResult) {
            try {
                //ordinary fields
                var retval = Ext.create('AssetManagement.customer.model.bo.MeasurementDocHistory', {

                    point: dbResult['POINT'],
                    mdocm: dbResult['MDOCM'],
                    mdtext: dbResult['MDTXT'],
                    readr: dbResult['READR'],
                    recdc: dbResult['RECDC'],
                    unitr: dbResult['UNITR'],
                    codct: dbResult['CODCT'],
                    codgr: dbResult['CODGR'],
                    vlcod: dbResult['VLCOD'],
                    idate: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['IDATE']),
                    itime: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ITIME']),

                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['POINT'] + dbResult['MDOCM']);

                return retval;
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHistMeasDocFromDbResultObject of BaseHistMeasDocManager', ex);
            }
        }
    }
});