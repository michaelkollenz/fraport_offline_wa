Ext.define('AssetManagement.base.manager.BaseAddressManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.AddressCache',
        'AssetManagement.customer.model.bo.Address',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//private
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.AddressCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseAddressManager', ex);
			}
			
			return retval;
		},
	
		//public methods
		getAddresses: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can deliver the complete dataset
				var cache = this.getCache();
				var fromCache = cache.getStoreForAll();
				
				if(fromCache) {
					//it can, so return just this store
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var fromDataBase = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Address',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildAdressesStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAddresses of BaseAddressManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_ADDRESS', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ADDRESS', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAddresses of BaseAddressManager', ex);
			}
	
			return retval;
		},
		
		getAddress: function(adressNumber, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(adressNumber)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCache(adressNumber);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var address = me.buildAddressFromDataBaseQuery.call(me, eventArgs);
						
						cache.addToCache(address);
						eventController.requestEventFiring(retval, address);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAddress of BaseAddressManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('ADDRNUMBER', adressNumber);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_ADDRESS', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ADDRESS', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAddress of BaseAddressManager', ex);
			}
			
			return retval;
		},
		
		buildGoogleMapsAddressQuery: function(address) {
			var retval = "";
		
			try {
				if (address) {
					retval = AssetManagement.customer.utils.StringUtils.concatenate([ address.get('country'),
						address.get('postCode1'), address.get('city1'), address.get('city2'), address.get('street'),
						address.get('houseNum1')], '+', true, true);
					retval = retval.trim();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildGoogleMapsAddressQuery of BaseAddressManager', ex);
			}

			return retval
		},
		
		getTechObjectAddressOfOrder: function(orderAddress, funcLocAddress, equipmentAddress) {
			var retval = null;
		
			try {
				if(equipmentAddress !== null && equipmentAddress !== undefined) {
					retval = equipmentAddress;
				}
				
				if(retval === null && funcLocAddress !== null && funcLocAddress !== undefined) {
					retval = funcLocAddress;
				}
				
				if(retval === null) {
					retval = orderAddress;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTechObjectAddressOfOrder of BaseAddressManager', ex);
			}
				
			return retval;
		},
		
		mapPartnerIntoAddress: function(partner) {
			var retval = null;
			
			try {
				if(partner) {
					retval = Ext.create('AssetManagement.customer.model.bo.Address', {
						title: partner.get('title'),
						name1: partner.get('name1'),
						name2: partner.get('name2'),
						name3: partner.get('name3'),
						name4: partner.get('name4'),
						city1: partner.get('city1'),
						city2: partner.get('city2'),
						postCode1: partner.get('postcode1'),
						street: partner.get('street'),
						houseNum1: partner.get('housenum1'),
						country: partner.get('country'),
						region: partner.get('region'),
						telNumber: partner.get('telNumber'),
						telExtens: partner.get('telExtens'),
						faxNumber: partner.get('faxNumber'),
						faxExtens: partner.get('faxExtens'),
						telnumbermob: partner.get('telnumbermob'),
						str_suppl1: partner.get('str_suppl1')
					});
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapPartnerIntoAddress of BaseAddressManager', ex);
			}
			
			return retval;
		},
		
		mapCustomerIntoAddress: function(customer) {
			var retval = null;
			
			try {
				if(customer) {
					retval = Ext.create('AssetManagement.customer.model.bo.Address', {
						title: customer.get('titlep'),
						name1: customer.get('firstname'),
						name2: customer.get('lastname'),
						city1: customer.get('city'),
						postCode1: customer.get('postlcod1'),
						street: customer.get('street'),
						country: customer.get('country'),
						region: customer.get('region'),
						telNumber: customer.get('tel1numbr'),
						telExtens: customer.get('telextens'),
						faxNumber: customer.get('faxnumber'),
						faxExtens: customer.get('faxextens'),
						telnumbermob: customer.get('telnumbermob'),
                        str_suppl1: customer.get('str_suppl1')
					});
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapCustomerIntoAddress of BaseAddressManager', ex);
			}
			
			return retval;
		},
		
		mapSDOrderIntoAddress: function(sdOrder) {
			var retval = null;
			
			try {
				if(sdOrder) {
					retval = Ext.create('AssetManagement.customer.model.bo.Address', {
						title: sdOrder.get('formOfAddress'),
						name1: sdOrder.get('name'),
						name2: sdOrder.get('name2'),
						city1: sdOrder.get('city'),
						postCode1: sdOrder.get('postalCode1'),
						street: sdOrder.get('street'),
						houseNum1: sdOrder.get('houseNo'),
						country: sdOrder.get('country'),
						region: sdOrder.get('region')
					});
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapSDOrderIntoAddress of BaseAddressManager', ex);
			}
			
			return retval;
		},
		
		//private methods
		buildAddressFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
					    retval = this.buildAddressFromDbResultObject(eventArgs.target.result.value);
						this.getCache().addToCache(retval);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildAddressFromDataBaseQuery of BaseAddressManager', ex);
			}
			return retval;
		},
		
		buildAdressesStoreFromDataBaseQuery: function(store, eventArgs) {
	       try {
	    	   if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var address = this.buildAddressFromDbResultObject(cursor.value);
						store.add(address);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildAdressesStoreFromDataBaseQuery of BaseAddressManager', ex);
			}
		},

		buildAddressFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Address', {
					adrnr: dbResult['ADDRNUMBER'],
					title: dbResult['TITLE'],
					name1: dbResult['NAME1'],
					name2: dbResult['NAME2'],
					name3: dbResult['NAME3'],
					name4: dbResult['NAME4'],
					city1: dbResult['CITY1'],
					city2: dbResult['CITY2'],
					postCode1: dbResult['POST_CODE1'],
					street: dbResult['STREET'],
					houseNum1: dbResult['HOUSE_NUM1'],
					country: dbResult['COUNTRY'],
					region: dbResult['REGION'],
					telNumber: dbResult['TEL_NUMBER'],
					telExtens: dbResult['TEL_EXTENS'],
					faxNumber: dbResult['FAX_NUMBER'],
					faxExtens: dbResult['FAX_EXTENS'],
					telnumbermob: dbResult['TEL_NUMBER_MOB'],
                    str_suppl1: dbResult['STR_SUPPL1'],
					
				    updFlag: dbResult['UPDFLAG']

				});		
	        	
				retval.set('id', dbResult['ADDRNUMBER']);			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildAddressFromDbResultObject of BaseAddressManager', ex);
			}			
			return retval;
		}
	}
});