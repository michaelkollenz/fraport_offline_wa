Ext.define('AssetManagement.base.manager.BaseNotifManager', {
    extend: 'AssetManagement.customer.manager.OxBaseManager',

	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.NotifCache',
        'AssetManagement.customer.model.bo.Notif',
        'AssetManagement.customer.model.bo.Order',
        'AssetManagement.customer.manager.CustNotifTypeManager',
        'AssetManagement.customer.model.bo.NotifItem',
        'AssetManagement.customer.model.bo.NotifTask',
        'AssetManagement.customer.model.bo.NotifActivity',
        'AssetManagement.customer.model.bo.NotifCause',
        'AssetManagement.customer.manager.NotifItemManager',
        'AssetManagement.customer.manager.NotifTaskManager',
        'AssetManagement.customer.manager.NotifActivityManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
//      'AssetManagement.customer.manager.OrderManager',		INCLUSION CAUSES RING DEPDENCY
//      'AssetManagement.customer.manager.SDOrderManager',	INCLUSION CAUSES RING DEPDENCY
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.AddressManager',
        'AssetManagement.customer.manager.PartnerManager',
        'AssetManagement.customer.manager.ObjectStatusManager',
        'AssetManagement.customer.manager.CustCodeManager',
        'AssetManagement.customer.manager.PriorityManager',
        'AssetManagement.customer.manager.DMSDocumentManager',
        'AssetManagement.customer.manager.DocUploadManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.manager.LongtextManager',
        'Ext.data.Store'
    ],

	inheritableStatics: {
		EVENTS: {
			NOTIF_ADDED: 'notifAdded',
			NOTIF_CHANGED: 'notifChanged',
			NOTIF_DELETED: 'notifDeleted'
		},

		//private
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.NotifCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseNotifManager', ex);
			}

			return retval;
		},

		//public methods
		getNotifs: function(withDependendData, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}

				var baseStore = null;

				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForNotifs(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.Notif',
						autoLoad: false
					});

					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

							me.buildNotifsStoreFromDataBaseQuery.call(me, baseStore, eventArgs);

							if(done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);

								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForNotifs.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifs of BaseNotifManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};

					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIHEAD', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIHEAD', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifs of BaseNotifManager', ex);
			}

			return retval;
		},

		getNotif: function(qmnum, useBatchProcessing) {
			var retval = -1;

			try {
				retval = this.getNotifInternal(qmnum, false, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotif of BaseNotifManager', ex);
			}

			return retval;
		},

		getNotifLightVersion: function(qmnum, useBatchProcessing) {
			var retval = -1;

			try {
				retval = this.getNotifInternal(qmnum, true, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifLightVersion of BaseNotifManager', ex);
			}

			return retval;
		},

		getNotifInternal: function(qmnum, lightVersion, useBatchProcessing) {
			var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var cache = this.getCache();
				//var requestedDataGrade = lightVersion ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				/*var fromCache = cache.getFromCache(qmnum, requestedDataGrade);

				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}*/

				//to avoid duplicate objects, check if there already exists an instance for the requested equipment and if so, use it
				var notif = cache.getFromCache(qmnum);

				if(notif) {
					this.loadDependendDataForNotif(notif, retval, lightVersion);
				} else {
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var notif = me.buildNotifFromDataBaseQuery.call(me, eventArgs);

							if(notif) {
								me.getCache().addToCache(notif);
								me.loadDependendDataForNotif.call(me, notif, retval, lightVersion);
							} else {
								eventController.requestEventFiring(retval, null);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifInternal of BaseNotifManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};

					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);

					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_NOTIHEAD', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIHEAD', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifInternal of BaseNotifManager', ex);
			}

			return retval;
		},

		getRbnrForNotif: function(notif, funcLoc, equi) {
			var rbnr = "";

			try {
				if(equi && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equi.get('rbnr'))) {
					rbnr = equi.get('rbnr');
				} else if(funcLoc && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(funcLoc.get('rbnr'))) {
					rbnr = funcLoc.get('rbnr');
				} else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('rbnr'))) {
					rbnr = notif.get('rbnr');
				} else if(notif.get('notifType')) {
					rbnr = notif.get('notifType').get('rbnr');
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifs of BaseNotifManager', ex);
			}

			return rbnr;
		},

		//private methods
		buildNotifFromDataBaseQuery: function(eventArgs) {
			var retval = null;

			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildNotifFromDbResultObject(eventArgs.target.result.value);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifFromDataBaseQuery of BaseNotifManager', ex);
			}

			return retval;
		},

		buildNotifsStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var notif = this.buildNotifFromDbResultObject(cursor.value);
						store.add(notif);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifsStoreFromDataBaseQuery of BaseNotifManager', ex);
			}
		},

		buildNotifFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Notif', {
					qmnum: dbResult['QMNUM'],
					qmart: dbResult['QMART'],
					qmtxt: dbResult['QMTXT'],
					qmartTxt: dbResult['QMARTX'],
					artpr: dbResult['ARTPR'],
					priok: dbResult['PRIOK'],
					aufnr: dbResult['AUFNR'],
					prueflos: dbResult['PRUEFLOS'],
					iwerk: dbResult['IWERK'],
					equnr: dbResult['EQUNR'],
					bautl: dbResult['BAUTL'],
					ingrp: dbResult['INGRP'],
					warpl: dbResult['WARPL'],
					wapos: dbResult['WAPOS'],
					wppos: dbResult['WPPOS'],
					tplnr: dbResult['TPLNR'],
					pltxt: dbResult['PLTXT'],
					eqktx: dbResult['EQKTX'],
					eqfnr: dbResult['EQFNR'],
					strno: dbResult['STRNO'],
					tidnr: dbResult['TIDNR'],
					qmnam: dbResult['QMNAM'],
					rbnr: dbResult['RBNR'],
					gewrk: dbResult['GEWRK'],
					swerk: dbResult['SWERK'],
					flagPlos: dbResult['FLAG_PLOS'],
					stsma: dbResult['STSMA'],
					qmkat: dbResult['QMKAT'],
					qmgrp: dbResult['QMGRP'],
					qmcod: dbResult['QMCOD'],
					matnr: dbResult['MATNR'],
					auswk: dbResult['AUSWK'],
					sernr: dbResult['SERNR'],
					msaus: dbResult['MSAUS'],
			        kzmla: dbResult['KZMLA'],
					ktext: dbResult['KTEXT'],

					usr01Flag: dbResult['USR01FLAG'],
					usr02Flag: dbResult['USR02FLAG'],

					usr03Code: dbResult['USR03CODE'],
					usr04Code: dbResult['USR04CODE'],

					usr05Txt: dbResult['USR05TXT'],
					usr06Txt: dbResult['USR06TXT'],
					usr07Txt: dbResult['USR07TXT'],

					usr08Date: dbResult['USR08DATE'],
					usr09Num: dbResult['USR09NUM'],

					qmdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['QMDAT'], dbResult['MZEIT']),
					strdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['STRMN'], dbResult['STRUR']),
					ltrdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['LTRMN'], dbResult['LTRUR']),
					vondt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['AUSVN'], dbResult['AUZTV']),
					bisdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['AUSBS'], dbResult['AUZTB']),

				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY'],
				    mobileKeyOrder: dbResult['MOBILEKEY_OR']
			    });

			    retval.set('id', dbResult['QMNUM']);

			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifFromDbResultObject of BaseNotifManager', ex);
			}

			return retval;
		},

		loadListDependendDataForNotifs: function(eventIdToFireWhenComplete, notifs) {
			try {
				//do not continue if there is no data
				if(notifs.getCount() === 0) {
					//return the store from cache to eliminate duplicate objects
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifs);
					return;
				}

				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var notifTypes = null;
				var notifItems = null;
				var notifActivities = null;
				var notifTasks = null;
				var equis = null;
				var flocs = null;
				var orders = null;
				var partners = null;		//currently disabled
				var statusList = null;		//currently disabled

				var counter = 0;
				var done = 0;
				var erroroccurred = false;
				var reported = false;

				var completeFunction = function() {
					if(erroroccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && erroroccurred === false) {
						me.assignListDependendDataForNotifs.call(me, eventIdToFireWhenComplete, notifs, notifTypes, notifItems, notifActivities, notifTasks, equis, flocs, orders, partners, statusList);
					}
				};

				//TO-DO make dependend of customizing parameters --- this definitely will increase performance

				//get notif types
				if(true) {
					done++;

					var notifTypesSuccessCallback = function(notiTypes) {
						erroroccurred = notiTypes === undefined;

						notifTypes = notiTypes;
						counter++;

						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.CustNotifTypeManager.getNotifTypes(true);
					eventController.registerOnEventForOneTime(eventId, notifTypesSuccessCallback);
				}

				//get notif item data
				if(true) {
					done++;

					var notifItemSuccessCallback = function(items) {
						erroroccurred = items === undefined;

						notifItems = items;
						counter++;

						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.NotifItemManager.getNotifItems(true, true);
					eventController.registerOnEventForOneTime(eventId, notifItemSuccessCallback);
				}

				//get notif activity data
				if(true) {
					done++;

					var notifActivitySuccessCallback = function(activities) {
						erroroccurred = activities === undefined;

						notifActivities = activities;
						counter++;

						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.NotifActivityManager.getNotifActivities(true, true);
					eventController.registerOnEventForOneTime(eventId, notifActivitySuccessCallback);
				}

				//get notif task data
				if(true) {
					done++;

					var notifTaskSuccessCallback = function(tasks) {
						erroroccurred = tasks === undefined;

						notifTasks = tasks;
						counter++;

						completeFunction();
					};

					var eventId = AssetManagement.customer.manager.NotifTaskManager.getNotifTasks(true, true);
					eventController.registerOnEventForOneTime(eventId, notifTaskSuccessCallback);
				}

				//get funcLoc data
				if(true) {
					done++;

					var flocSuccessCallback = function(funcLocs) {
						erroroccurred = funcLocs === undefined;

						flocs = funcLocs;
						counter++;

						completeFunction();
					};

					//request equis dependend data, because the funcloc's address will be needed
					eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(true, true);
					eventController.registerOnEventForOneTime(eventId, flocSuccessCallback);
				}

				//get equipment data
				if(true) {
					done++;

					var equiSuccessCallback = function(equipments) {
						erroroccurred = equipments === undefined;

						equis = equipments;
						counter++;

						completeFunction();
					};

					//request equis dependend data, because the equipment's address will be needed
					eventId = AssetManagement.customer.manager.EquipmentManager.getEquipments(true, true);
					eventController.registerOnEventForOneTime(eventId, equiSuccessCallback);
				}

				//get order data
				if(true) {
					done++;

					var orderSuccessCallback = function(ordersList) {
						erroroccurred = ordersList === undefined;

						orders = ordersList;
						counter++;

						completeFunction();
					};

					eventId = AssetManagement.customer.manager.OrderManager.getOrders(false, true);
					eventController.registerOnEventForOneTime(eventId, orderSuccessCallback);
				}

//				//get partner data
//				if(true) {
//					done++;
//
//					var partnerSuccessCallback = function(partnerList) {
//						erroroccurred = partnerList === undefined;
//
//						partners = partnerList;
//						counter++;
//
//						completeFunction();
//					};
//
//					eventId = AssetManagement.customer.manager.PartnerManager.getPartnersForNotifsOnly(true);
//					eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
//				}

//				//get status data
//				if(true) {
//					done++;
//
//					var statusSuccessCallback = function(statuslist) {
//						erroroccurred = statuslist === undefined;
//
//						statusList = statuslist;
//						counter++;
//
//						completeFunction();
//					};
//
//					eventId = AssetManagement.customer.manager.StatusManager.getStatusStoreForNotifs(false);
//					eventController.registerOnEventForOneTime(eventId, statusSuccessCallback);
//				}

				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForNotifs of BaseNotifManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		//call this function, when all neccessary data has been collected
		assignListDependendDataForNotifs: function(eventIdToFireWhenComplete, notifs, notifTypes, notifItems, notifActivites,
														notifTasks, equis, flocs, orders, partners, statusList) {
			try {
				//to increase performance create a copies of notif's subitems' stores and the order store
				//remove all objects from these stores, which have been assigned to it's notif
				//therefore following loops will have less iterations
				var notifItemsCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.NotifItem',
					autoLoad: false
				});

				if(notifItems) {
					notifItems.each(function(item, index, length) {
						notifItemsCopy.add(item);
					});
				}

				var notifActivitesCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.NotifActivity',
					autoLoad: false
				});

				if(notifActivites) {
					notifActivites.each(function(item, index, length) {
						notifActivitesCopy.add(item);
					});
				}

				var notifTasksCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.NotifTask',
					autoLoad: false
				});

				if(notifTasks) {
					notifTasks.each(function(task, index, length) {
						notifTasksCopy.add(task);
					});
				}

				var ordersCopy = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Order',
					autoLoad: false
				});

				if(orders) {
					orders.each(function(order, index, length) {
						ordersCopy.add(order);
					});
				}

				notifs.each(function (notif, index, length) {
					//begin notifType assignment
					var myQmart = notif.get('qmart');

					if(notifTypes) {
						notifTypes.each(function(notifType, index, length) {
							if(notifType.get('qmart') === myQmart) {
								notif.set('notifType', notifType);
								return false;
							}
						});
					}
					//end notifType assignment

				    //begin notif items assignment
					var myItems = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifItem',
						autoLoad: false
					});

					//fetch all my items
					notifItemsCopy.each(function(item, index, length) {
						if(item.get('qmnum') === notif.get('qmnum')) {
							myItems.add(item);
						}
					});

					//remove all my items from the data pool
					myItems.each(function(item, index, length) {
						notifItemsCopy.remove(item);
					});

					//sorting is not required yet for notif list
					notif.set('notifItems', myItems);
					//end notif items assignment

					//begin notif activities assignment
					var myActivities = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});

					//fetch all my activites
					var fenum;
					notifActivitesCopy.each(function(activity, index, length) {
						fenum = activity.get('fenum');

						if(fenum === '0000' || fenum === '0')
						{
							if(activity.get('qmnum') === notif.get('qmnum')) {
								myActivities.add(activity);
							}
						}
					});

					//remove all my activites from the data pool
					myActivities.each(function(activity, index, length) {
						notifActivitesCopy.remove(activity);
					});

					//sorting is not required yet for notif list
					notif.set('notifActivities', myActivities);
					//end notif activities assignment

					//begin notif tasks assignment
					var myTasks = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifTask',
						autoLoad: false
					});

					//fetch all my tasks
					notifTasksCopy.each(function(task, index, length) {
						fenum = task.get('fenum');

						if(fenum === '0000' || fenum === '0')
						{
							if(task.get('qmnum') === notif.get('qmnum')) {
								myTasks.add(task);
							}
						}
					});

					//remove all my tasks from the data pool
					myTasks.each(function(task, index, length) {
						notifTasksCopy.remove(task);
					});

					//sorting is not required yet for notif list
					notif.set('notifTasks', myTasks);
					//end notif tasks assignment

					//begin funcLoc assignment
					var myTplnr = notif.get('tplnr');

					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myTplnr)) {
						flocs.each(function(floc, index, length) {
							if(floc.get('tplnr') === myTplnr) {
								notif.set('funcLoc', floc);
								return false;
							}
						});
					}
					//end funcLoc assignment

					//begin equipment assignment
					var myEqunr = notif.get('equnr');

					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myEqunr)) {
						equis.each(function(equi, index, length) {
							if(equi.get('equnr') === myEqunr) {
								notif.set('equipment', equi);
								return false;
							}
						});
					}
					//end equipment assignment

					//begin notif assignment
					var myAufnr = notif.get('aufnr');
					var myOrder = null;

					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myAufnr)) {
						ordersCopy.each(function(order, index, length) {
							if(order.get('aufnr') === myAufnr) {
								notif.set('order', order);
								myOrder = order;
								return false;
							}
						});
					}

					if(myOrder)
						ordersCopy.remove(myOrder);
					//end notif assignment
			//
			//		//begin partners assignment
			//		var myPartners = Ext.create('Ext.data.Store', {
			//			model: 'AssetManagement.customer.model.bo.Partner',
			//			autoLoad: false
			//		});
			//
			//		partners.each(function(partner, index, length) {
			//			if(partner.get('objnr') === order.get('objnr'))
			//				myPartners.add(partner);
			//		});
			//
			//		//sorting is not required yet for order list
			//		order.set('partners', myPartners);
			//		//end partners assignment

					//DISBALED
			//		//begin status assignment
			//		var myStatusList = Ext.create('Ext.data.Store', {
			//			model: 'AssetManagement.customer.model.bo.Status',
			//			autoLoad: false
			//		});
			//
			//		statusList.each(function(status, index, length) {
			//			if(status.get('objnr') === order.get('objnr'))
			//				myStatusList.add(status);
			//		});
			//
			//		order.set('statusList', myStatusList);
			//		//end status assignment
				});

				var cache = this.getCache();
				cache.addStoreForAllToCache(notifs, cache.self.DATAGRADES.LOW);

				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForNotifs of BaseNotifManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		loadDependendDataForNotif: function(notif, eventIdToFireWhenComplete, lightVersion) {
			try {
				if(notif) {
					var qmnum = notif.get('qmnum');
					var aufnr = notif.get('aufnr');
					var objnr = "QM" + qmnum;

					//all of assignListDependendDataForNotifs but with more details
					//additional longtext, checklist status, files ...

					//when loading tasks and activities keep in mind to use getNotifXXXForNotifHeadOnly

					if(lightVersion === undefined)
						lightVersion = false;

					//lightversion currently contains: objectStatus

					var notifType = null;
					var codification = null;
					var notifItems = null;
					var notifActivities = null;
					var notifTasks = null;
					var floc = null;
					var equi = null;
					var order = null;
					var objectStatusList = null;
					var documents = null;
					var partners = null;
					var priority = null;
					var backendLongtext = null;
					var localLongtext = null;
					var checklistStatus = null; //currently disabled
					var sdOrders = null;
                    var eventId;

					var counter = 0;
					var done = 0;
					var erroroccurred = false;
					var reported = false;

					var me = this;
					var eventController = AssetManagement.customer.controller.EventController.getInstance();

					var completeFunction = function() {
						if(erroroccurred === true && reported === false) {
							AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
							reported = true;
						} else if(counter === done && erroroccurred === false) {
							me.assignDependendDataForNotif.call(me, eventIdToFireWhenComplete, notif, lightVersion, notifType,
									codification, notifItems, notifActivities, notifTasks, floc, equi, order,
										objectStatusList, documents, partners, priority, backendLongtext, localLongtext, checklistStatus, sdOrders);
						}
					};

					//TO-DO make dependend of customizing parameters --- this definitely will increase performance

					//get notifType
					if(lightVersion === false) {
						var qmart = notif.get('qmart');
						done++;

						var notifTypeSuccessCallback = function(notiType) {
							erroroccurred = notiType === undefined;

							notifType = notiType;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.CustNotifTypeManager.getNotifType(qmart, true);
						eventController.registerOnEventForOneTime(eventId, notifTypeSuccessCallback);
					}

					//get codification
					if(lightVersion === false) {
						var qmkat = notif.get('qmkat');
						var codeGrp = notif.get('qmgrp');
						var code = notif.get('qmcod');

						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmkat) &&
								!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(codeGrp) &&
									!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
							done++;

							var codificationSuccessCallback = function(custCode) {
								erroroccurred = custCode === undefined;

								codification = custCode;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.CustCodeManager.getCustCode(qmkat, codeGrp, code, true);
							eventController.registerOnEventForOneTime(eventId, codificationSuccessCallback);
						}
					}

					//get notifItems
					if(lightVersion === false) {
						done++;

						var notifItemsSuccessCallback = function(notiItems) {
							erroroccurred = notiItems === undefined;

							notifItems = notiItems;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.NotifItemManager.getNotifItemsForNotif(qmnum, true);
						eventController.registerOnEventForOneTime(eventId, notifItemsSuccessCallback);
					}

					//get notifActivities
					if(lightVersion === false) {
						done++;

						var notifActivitiesSuccessCallback = function(notiActivities) {
							erroroccurred = notiActivities === undefined;

							notifActivities = notiActivities;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.NotifActivityManager.getNotifActivitiesForNotifHeadOnly(qmnum, true);
						eventController.registerOnEventForOneTime(eventId, notifActivitiesSuccessCallback);
					}

					//get notifTasks
					if(lightVersion === false) {
						done++;

						var notifTasksSuccessCallback = function(notiTasks) {
							erroroccurred = notiTasks === undefined;

							notifTasks = notiTasks;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.NotifTaskManager.getNotifTasksForNotifHeadOnly(qmnum, true);
						eventController.registerOnEventForOneTime(eventId, notifTasksSuccessCallback);
					}

					//get funLoc
					if(lightVersion === false) {
						var tplnr = notif.get('tplnr');

						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
							done++;

							var flocSuccessCallback = function(funcLoc) {
								erroroccurred = funcLoc === undefined;

								floc = funcLoc;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
							eventController.registerOnEventForOneTime(eventId, flocSuccessCallback);
						}
					}

					//get equipment
					if(lightVersion === false) {
						var equnr = notif.get('equnr');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
							done++;

							var equiSuccessCallback = function(equipment) {
								erroroccurred = equipment === undefined;

								equi = equipment;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
							eventController.registerOnEventForOneTime(eventId, equiSuccessCallback);
						}
					}

					//get order
					if(lightVersion === false) {
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
							done++;

							var notifSuccessCallback = function(orderr) {
								erroroccurred = orderr === undefined;

								order = orderr;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.OrderManager.getOrderLightVersion(aufnr, true);
							eventController.registerOnEventForOneTime(eventId, notifSuccessCallback);
						}
					}

					//get partners
					if(lightVersion === false) {
						done++;

						var partnerSuccessCallback = function(partnerList) {
							erroroccurred = partnerList === undefined;

							partners = partnerList;
							counter++;

							completeFunction();
						};

						//extention:
						//if a notif was created mobile and it has an order, it's partners are conntected to this order
						//else the partners are connected to itself

						var objnrToUse = objnr;

						//check for the special condition descriped above
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('mobileKey')) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
							objnrToUse = 'OR' + aufnr;
						}

						eventId = AssetManagement.customer.manager.PartnerManager.getAllPartnersForObject(objnrToUse, true);
						eventController.registerOnEventForOneTime(eventId, partnerSuccessCallback);
					}

					//get objectStatus
					if(true) {
						done++;

						var objectStatusSuccessCallback = function(objStatusList) {
							erroroccurred = objStatusList === undefined;
							objectStatusList = objStatusList;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, objectStatusSuccessCallback);
					}

					//getLongtext
					if(true) {
						done++;

						var longtextSuccessCallback = function(localLT, backendLT) {
							erroroccurred = localLT === undefined || backendLT === undefined;
							localLongtext = localLT;
							backendLongtext = backendLT;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(notif, true);
						eventController.registerOnEventForOneTime(eventId, longtextSuccessCallback);
					}

					//get priority
					if(lightVersion === false) {
						var artpr = notif.get('artpr');
						var priok = notif.get('priok');
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(artpr) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priok)) {
							done++;

							var prioritySuccessCallback = function(prio) {
								erroroccurred = prio === undefined;

								priority = prio;
								counter++;

								completeFunction();
							};

							eventId = AssetManagement.customer.manager.PriorityManager.getPriority(artpr, priok, true);
							eventController.registerOnEventForOneTime(eventId, prioritySuccessCallback);
						}
					}

					//get documents
					if(lightVersion === false) {
						documents = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.File',
							autoLoad: false
						});

						//get dms documents
						done++;

						var dmsDocsSuccessCallback = function(dmsDocs) {
							erroroccurred = dmsDocs === undefined;

							if(erroroccurred) {
								documents = null;
							} else if(documents && dmsDocs && dmsDocs.getCount() > 0) {
								dmsDocs.each(function(dmsDoc) {
									documents.add(dmsDoc);
								}, this);
							}

							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.DMSDocumentManager.getDmsDocumentsForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, dmsDocsSuccessCallback);

						//get doc upload records
						done++;

						var docUploadsSuccessCallback = function(docUploads) {
							erroroccurred = docUploads === undefined;

							if(erroroccurred) {
								documents = null;
							} else if(documents && docUploads && docUploads.getCount() > 0) {
								docUploads.each(function(docUpload) {
									documents.add(docUpload);
								}, this);
							}

							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.DocUploadManager.getDocUploadsForObject(objnr, true);
						eventController.registerOnEventForOneTime(eventId, docUploadsSuccessCallback);
					}

					//get sd orders
					if(lightVersion === false) {
						done++;

						var sdOrdersSuccessCallback = function(sdOrds) {
							erroroccurred = sdOrds === undefined;

							sdOrders = sdOrds;
							counter++;

							completeFunction();
						};

						eventId = AssetManagement.customer.manager.SDOrderManager.getSDOrdersForNotif(qmnum, true);
						eventController.registerOnEventForOneTime(eventId, sdOrdersSuccessCallback);
					}

					if(done > 0) {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					} else {
						completeFunction();
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForNotif of BaseNotifManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		assignDependendDataForNotif: function(eventIdToFireWhenComplete, notif, lightVersion, notifType,
									codification, notifItems, notifActivities, notifTasks, floc, equi, order,
										objectStatusList, documents, partners, priority, backendLongtext, localLongtext, checklistStatus, sdOrders)
		{
			try
			{
				if(!notif) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
					return;
				}

				notif.set('notifType', notifType);
				notif.set('codification', codification);
				notif.set('notifItems', notifItems);
				notif.set('notifActivities', notifActivities);
				notif.set('notifTasks', notifTasks);
				notif.set('order', order);
				notif.set('equipment', equi);
				notif.set('funcLoc', floc);
				notif.set('objectStatusList', objectStatusList);
				notif.set('documents', documents);
				notif.set('partners', partners);
				notif.set('priority', priority);
				notif.set('backendLongtext', backendLongtext);
				notif.set('localLongtext', localLongtext);
				notif.set('checklistStatus', checklistStatus);
				notif.set('sdOrders', sdOrders);

				var cache = this.getCache();
				var dataGrade = lightVersion === true ? cache.self.DATAGRADES.MEDIUM : cache.self.DATAGRADES.FULL;
				cache.addToCache(notif, dataGrade);

				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, notif);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForNotif of BaseNotifManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},

		/**
		 * saves a new notification to the database
		 */
		saveNotification: function(notif, useBatchProcessing) {
			var retval = -1;
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();

				if(!notif) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

				var isNewNotification = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('qmnum'));

				this.manageUpdateFlagForSaving(notif);

				var saveLevel = 0;
				var maxLevel = 5;

				var itemsSaveCount = 0;
				var tasksSaveCount = 0;
				var activitiesSaveCount = 0;
				var partnersSaveCount = 0;

				var me = this;

				var saveMethod = function() {
                    var eventId;
					//save the header
					if(saveLevel == 0) {
						var saveNotifHeadCallBack = function(notif) {
							saveLevel++;
							AssetManagement.customer.manager.NotifManager.updateNotifDependantDataAfterNotifSaving(notif);
							saveMethod();
						};

						eventId = AssetManagement.customer.manager.NotifManager.saveNotifHead(notif);
						eventController.registerOnEventForOneTime(eventId, saveNotifHeadCallBack);
					}
					else if(saveLevel == 1) {
						//save the items
						var items = notif.get('notifItems');
					    var itemsCount = items ? items.getCount() : 0;

						if (itemsCount === 0) {
							//no items for saving => continue
							saveLevel++;
							saveMethod();
						} else {
							var saveItemsCallback = function () {
								itemsSaveCount++;

								if (itemsSaveCount == itemsCount)
									saveLevel++;

								saveMethod();
					        };

							eventId = AssetManagement.customer.manager.NotifItemManager.saveNotifItem(items.getAt(itemsSaveCount));
							eventController.registerOnEventForOneTime(eventId, saveItemsCallback);
						}
					} else if(saveLevel == 2) {
						//save the tasks
					    var tasks = notif.get('notifTasks');
					    var tasksCount = tasks ? tasks.getCount() : 0;

					    if(tasksCount === 0) {
							//no tasks for saving => continue
							saveLevel++;
							saveMethod();
						} else {
							var saveTasksCallback = function(notifTask) {
							    tasksSaveCount++;

								if(tasksSaveCount == tasksCount)
									saveLevel++;

								saveMethod();
							};

							eventId = AssetManagement.customer.manager.NotifTaskManager.saveNotifTask(tasks.getAt(tasksSaveCount));
							eventController.registerOnEventForOneTime(eventId, saveTasksCallback);
						}
					} else if(saveLevel == 3) {
						//save the activities
					    var acitivites = notif.get('notifActivities');
					    var activitiesCount = acitivites ? acitivites.getCount(): 0;

					    if (activitiesCount === 0) {
							//no activities for saving => continue
							saveLevel++;
							saveMethod();
						} else {
							var saveActivityCallback = function(notifActivity) {
							    activitiesSaveCount++;

								if (activitiesSaveCount == activitiesCount)
									saveLevel++;

								saveMethod();
							};

							eventId = AssetManagement.customer.manager.NotifActivityManager.saveNotifActivity(acitivites.getAt(activitiesSaveCount));
							eventController.registerOnEventForOneTime(eventId, saveActivityCallback);
						}
					} else if(saveLevel == 4) {
						//save the partners
					    var notifPartners = notif.get('partners');
					    var partnersCount = notifPartners ? notifPartners.getCount() : 0;

					    if (partnersCount === 0) {
							//no partners for saving => continue
							saveLevel++;
							saveMethod();
						} else {
							var savePartnerCallback = function(partner) {
							    partnersSaveCount++;

							    if (partnersSaveCount == partnersCount)
								    saveLevel++;

								saveMethod();
							};

							eventId = AssetManagement.customer.manager.PartnerManager.savePartnerForObject(notifPartners.getAt(partnersSaveCount));
							eventController.registerOnEventForOneTime(eventId, savePartnerCallback);
						}
					} else if(saveLevel == 5) {
					    //update the connected order header, if applicable
					    var performOrderUpdate = false;
					    var connectedOrder = notif.get('order');

					    if (connectedOrder) {
					        //the notification is linked to an order
					        //if the linked order is the notifications header order and it is a new notification the order also has to be updated
					        performOrderUpdate = isNewNotification && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(connectedOrder.get('qmnum'));
					    }

					    if (performOrderUpdate) {
					        var saveOrderCallback = function (success) {
					            saveLevel++;
					            saveMethod();
					        };

                            connectedOrder.set('qmnum', notif.get('qmnum'));
					        connectedOrder.set('mobileKeyNotif', notif.get('mobileKey'));

					        eventId = AssetManagement.customer.manager.OrderManager.saveChangedOrderHeader(connectedOrder);
					        eventController.registerOnEventForOneTime(eventId, saveOrderCallback);
					    } else {
					        saveLevel++;
					        saveMethod();
					    }
					}

					if (saveLevel >= maxLevel) {
					    //do not specify a datagrade, so all depenend data will loaded, when necessary
					    me.getCache().addToCache(notif);
					    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, notif);
					} else {
					    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					}
				}

				saveMethod();
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotification of BaseNotifManager', ex);
			    retval = -1;
			}

			return retval;
		},

		/**
		 * saves the notifhead to the database
		 */
		saveNotifHead: function(notif) {
		    var retval = -1;

			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();


				if(!notif) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}

				this.manageUpdateFlagForSaving(notif);

				var requiresNewQmnum = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('qmnum'));
				var me = this;

				var saveFunction = function(nextCounterValue1) {
					try {
						if(requiresNewQmnum && nextCounterValue1 !== -1) {

							counter = 'MO'+AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue1, '0', 10);
							notif.set('qmnum', counter);
							notif.set('objnr', "QM" + notif.get('qmnum'));
						}

						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('qmnum'))) {
							eventController.requestEventFiring(retval, false);
							return;
						}

					    //set notif id
						notif.set('id', notif.get('qmnum'));

						//set mobileKey
						if(requiresNewQmnum && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('mobileKey')))
							notif.set('mobileKey', AssetManagement.customer.manager.KeyManager.createKey([ notif.get('qmnum') ]));

						var callback = function() {
							try {
								eventController.requestEventFiring(retval, notif);
							} catch(ex) {
							    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifHead of BaseNotifManager', ex);
								eventController.requestEventFiring(retval, false);
							}
						};

						var toSave = me.buildDataBaseObjectForNotifHead(notif);
						//if it is an insert perform an insert
						if(requiresNewQmnum) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_NOTIHEAD', toSave, callback, callback);

						} else {
							//else it is an update
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_NOTIHEAD', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.requestEventFiring(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifHead of BaseNotifManager', ex);
					}
				};

				if(!requiresNewQmnum) {
					saveFunction();
				} else {
					eventId = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('D_NOTIHEAD', false);

					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifHead of BaseNotifManager', ex);
			    retval = -1;
			}

			return retval;
		},

		/**
		 * create the databaseobject for saving the notifhead to the database
		 */
		buildDataBaseObjectForNotifHead: function(notif) {
			var retval = null;

			try {
			    var ac = AssetManagement.customer.core.Core.getAppConfig();
			    retval = { };
			    retval['MANDT'] =  ac.getMandt();
				retval['USERID'] =  ac.getUserId();
				retval['UPDFLAG'] =  notif.get('updFlag');
				retval['QMNUM'] = notif.get('qmnum');
				retval['QMART'] = notif.get('qmart');
				retval['QMTXT'] = notif.get('qmtxt');
				retval['ARTPR'] = notif.get('artpr');
				retval['PRIOK'] = notif.get('priok');
				retval['AUFNR'] = notif.get('aufnr');
				retval['PRUEFLOS'] = notif.get('prueflos');
				retval['IWERK'] = notif.get('iwerk');
				retval['EQUNR'] = notif.get('equnr');
				retval['BAUTL'] = notif.get('bautl');
				retval['INGRP'] = notif.get('ingrp');
				retval['WARPL'] = notif.get('warpl');
				retval['WAPOS'] = notif.get('wapos');
				retval['TPLNR'] = notif.get('tplnr');
				retval['QMNAM'] = notif.get('qmnam');
				retval['RBNR'] = notif.get('rbnr');
				retval['GEWRK'] = notif.get('gewrk');
				retval['SWERK'] = notif.get('swerk');
				retval['KTEXT'] = notif.get('ktext');
				retval['QMNAM'] = notif.get('qmnam');
				retval['RBNR'] = notif.get('rbnr');
				retval['FLAG_PLOS'] = notif.get('flagPlos');
				retval['STSMA'] = notif.get('stsma');
				retval['QMKAT'] = notif.get('qmkat');
				retval['QMGRP'] = notif.get('qmgrp');
				retval['QMCOD'] = notif.get('qmcod');
				retval['MATNR'] = notif.get('matnr');
				retval['AUSWK'] = notif.get('auswk');
				retval['SERNR'] = notif.get('sernr');
				retval['MSAUS'] = notif.get('msaus');
				retval['KZMLA'] = notif.get('kzmla');

				
				retval['USR01FLAG'] = notif.get('usr01Flag');
				retval['USR02FLAG'] = notif.get('usr02Flag');
				retval['USR03CODE'] = notif.get('usr03Code');
				retval['USR04CODE'] = notif.get('usr04Code');
				retval['USR05TXT'] = notif.get('usr05Txt');
				retval['USR06TXT'] = notif.get('usr06Txt');
				retval['USR07TXT'] = notif.get('usr07Txt');
				retval['USR08DATE'] = notif.get('usr08Date');
				retval['USR09NUM'] = notif.get('usr09Num');

				//DateTimes
				retval['QMDAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notif.get('qmdt'));
				retval['MZEIT'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notif.get('qmdt'));
				retval['STRMN'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notif.get('strdt'));
				retval['STRUR'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notif.get('strdt'));
				retval['LTRMN'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notif.get('ltrdt'));
				retval['LTRUR'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notif.get('ltrdt'));
				retval['AUSVN'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notif.get('vondt'));
				retval['AUSBS'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notif.get('bisdt'));
				retval['AUZTV'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notif.get('vondt'));
				retval['AUZTB'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notif.get('bisdt'));

				retval['MOBILEKEY'] = notif.get('mobileKey');
				retval['MOBILEKEY_OR'] = notif.get('mobileKeyOrder');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForNotifHead of BaseNotifManager', ex);
			}

			return retval;
		},

		/**
		 * updates the dependant objects of the notif with the data set during the saving of the notif head
		 * e.g. the qmnum or the mobilekey
		 */
		updateNotifDependantDataAfterNotifSaving: function(notif) {
		    try {
		        //update order, if applicable
		        var linkNotifToOrderOnHeadLevel = false;
		        var connectedOrder = notif.get('order');

		        if (connectedOrder) {
		            //the notification is linked to an order
		            //if the linked order has no head notification yet, this one will be linked
		            linkNotifToOrderOnHeadLevel = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(connectedOrder.get('qmnum'));
		        }

		        if (linkNotifToOrderOnHeadLevel) {
		            connectedOrder.set('qmnum', notif.get('qmnum'));
		            connectedOrder.set('mobileKeyNotif', notif.get('mobileKey'));
		        }

			    //update items
			    var items = notif.get('notifItems');

			    if (items && items.getCount() > 0) {
			        items.each(function (item) {
			            item.set('qmnum', notif.get('qmnum'));
			            item.set('mobileKey', notif.get('mobileKey'));
			        }, this);
			    }

			    //update tasks
			    var tasks = notif.get('notifTasks');

			    if (tasks && tasks.getCount() > 0) {
			        tasks.each(function (task) {
			            task.set('qmnum', notif.get('qmnum'));
			            task.set('mobileKey', notif.get('mobileKey'));
			        }, this);
			    }

		        //update activities
			    var acitivities = notif.get('notifActivities');

			    if (acitivities && acitivities.getCount() > 0) {
			        acitivities.each(function (activity) {
			            activity.set('qmnum', notif.get('qmnum'));
			            activity.set('mobileKey', notif.get('mobileKey'));
			        }, this);
			    }

			    //update partners
			    var partners = notif.get('partners');

			    if (partners && partners.getCount() > 0) {
			        partners.each(function (partner) {
			            partner.set('objnr', 'QM' + notif.get('qmnum'));
			            partner.set('mobileKey', notif.get('mobileKey'));
			        }, this);
			    }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateNotifDependantDataAfterNotifSaving of BaseNotifManager', ex);
			}

			return notif;
		},

        saveNotifLongtext: function(longtext, notif) {
            var retVal = -1;

            try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retVal = eventController.getNextEventId();

                var deleteCallback = function() {
                    try {
                        var saveCallback = function () {
                            eventController.requestEventFiring(retVal, true);
                        };

                        //check if the longtext has been removed (empty text)
                        //if so, do not call the save method
                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(longtext.get('text'))) {
                            saveCallback.call(this, true);
                        } else {
                            //get busobjkey from specific busobject
                            //unused?
                            // var busobjkey = notif.get('qmnum');

                            //save new longtext
                            var eventId = AssetManagement.customer.manager.LongtextManager.saveLocalLongtext(longtext, notif, true);
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveLongtext of BaseNotifManager', ex);
                    }
                };

                //delete objects local longtexts
                var eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(notif); //.get('localLongtext'), false);
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, deleteCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifLongtext of BaseNotifManager', ex);
            }

            return retVal;
        },

        /**
        * saves a changed notif header to the database
        */
        saveChangedNotifHeader: function (notif) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!notif || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('qmnum'))
                            || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('updFlag'))) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            eventController.requestEventFiring(me.EVENTS.NOTIF_CHANGED, notif);
                        }

                        eventController.requestEventFiring(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveChangedNotifHeader of BaseNotifManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                //update the updFlag if necessary
                if (notif.get('updFlag') === 'X')
                    notif.set('updFlag', 'U');

                var toSave = me.buildDataBaseObjectForNotifHead(notif);

                if (notif.get('updFlag') === 'U')
                    AssetManagement.customer.core.Core.getDataBaseHelper().update('D_NOTIHEAD', null, toSave, callback, callback);
                else
                    AssetManagement.customer.core.Core.getDataBaseHelper().put('D_NOTIHEAD', toSave, callback, callback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveChangedNotifHeader of BaseNotifManager', ex);
                retval = -1;
            }

            return retval;
        },

	    /**
		 * deletes a notification with all its dependend data from the database
		 */
        deleteNotification: function (notif, useBatchProcessing) {
            var retval = -1;
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!notif) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var deleteLevel = 0;
                var maxLevel = 9;

                var deleteMethod = function () {
                    //delete the header
                    if (deleteLevel === 0) {
                        //delete the items
                        var items = notif.get('notifItems');
                        var itemsCount = items ? items.getCount() : 0;

                        if (itemsCount === 0) {
                            //no items for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteItemsCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.NotifItemManager.deleteNotifItem(items.getAt(itemsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteItemsCallback);
                        }
                    } else if (deleteLevel === 1) {
                        //delete the tasks
                        var tasks = notif.get('notifTasks');
                        var tasksCount = tasks ? tasks.getCount() : 0;

                        if (tasksCount === 0) {
                            //no tasks for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteTasksCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.NotifTaskManager.deleteNotifTask(tasks.getAt(tasksCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteTasksCallback);
                        }
                    } else if (deleteLevel === 2) {
                        //delete the activities
                        var acitivites = notif.get('notifActivities');
                        var activitiesCount = acitivites ? acitivites.getCount() : 0;

                        if (activitiesCount === 0) {
                            //no activities for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteActivityCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.NotifActivityManager.deleteNotifActivity(acitivites.getAt(activitiesCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteActivityCallback);
                        }
                    } else if (deleteLevel === 3) {
                        //delete the partners
                        var notifPartners = notif.get('partners');
                        var partnersCount = notifPartners ? notifPartners.getCount() : 0;

                        if (partnersCount === 0) {
                            //no partners for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deletePartnerCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.PartnerManager.deletePartnerForObject(notifPartners.getAt(partnersCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deletePartnerCallback);
                        }
                    } else if (deleteLevel === 4) {
                        //update the connected order header, if applicable
                        var performOrderUpdate = false;
                        var connectedOrder = notif.get('order');

                        if (connectedOrder) {
                            //the notification is linked to an order
                            //if the linked order is the notifications header order and it is a new notification the order also has to be updated
                            performOrderUpdate = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(connectedOrder.get('qmnum'));
                        }

                        if (performOrderUpdate) {
                            var saveOrderCallback = function (success) {
                                deleteLevel++;
                                deleteMethod();
                            };

                            connectedOrder.set('qmnum', '');

                            eventId = AssetManagement.customer.manager.OrderManager.saveChangedOrderHeader(connectedOrder);
                            eventController.registerOnEventForOneTime(eventId, saveOrderCallback);
                        } else {
                            deleteLevel++;
                            deleteMethod();
                        }
                    } else if (deleteLevel === 5) {
                        //delete the activities
                        var documents = notif.get('documents');
                        var documentsCount = documents ? documents.getCount() : 0;

                        if (documentsCount === 0) {
                            //no activities for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteDocumentCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(documents.getAt(documentsCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteDocumentCallback);
                        }
                    } else if (deleteLevel === 6) {
                        var longtext = notif.get('localLongtext');

                        if (!longtext) {
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteLongtextCallback = function (success) {
                                deleteLevel++;
                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(notif, true);
                            eventController.registerOnEventForOneTime(eventId, deleteLongtextCallback);
                        }
                    } else if (deleteLevel === 7) {
                        var sdOrders = notif.get('sdOrders');
                        var sdOrdersCount = sdOrders ? sdOrders.getCount() : 0;

                        if (sdOrdersCount === 0) {
                            //no partners for deleting => continue
                            deleteLevel++;
                            deleteMethod();
                        } else {
                            var deleteSdOrderCallback = function (success) {

                                deleteMethod();
                            };

                            eventId = AssetManagement.customer.manager.SDOrderManager.deleteSDOrder(sdOrders.getAt(sdOrdersCount - 1), true);
                            eventController.registerOnEventForOneTime(eventId, deleteSdOrderCallback);
                        }
                    } else if (deleteLevel === 8) {
                        var deleteNotifHeadCallBack = function (success) {
                            deleteLevel++;
                            deleteMethod();
                        };

                        var eventId = AssetManagement.customer.manager.NotifManager.deleteNotifHead(notif, true);
                        eventController.registerOnEventForOneTime(eventId, deleteNotifHeadCallBack);
                    }

                    if (deleteLevel >= maxLevel) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, true);
                    } else {
                        AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                    }
                }

                deleteMethod();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotification of BaseNotifManager', ex);
                retval = -1;
            }

            return retval;
        },


        deleteNotifHead: function (notif, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!notif) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(notif);
                            me.getCache().removeFromCache(notif);

                            eventController.requestEventFiring(me.EVENTS.NOTIF_DELETED, notif);
                        }

                        eventController.requestEventFiring(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifActivity of BaseNotifManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                //get the key of the notif to delete
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_NOTIHEAD', notif);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_NOTIHEAD', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifHead of BaseNotifManager', ex);
                retval = -1;
            }

            return retval;
        }
	}
});
