Ext.define('AssetManagement.base.manager.BaseCust_011Manager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.Cust_011Cache',
        'AssetManagement.customer.model.bo.Cust_011',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.Cust_011Cache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseCust_011Manager', ex);
			}
			
			return retval;
		},
		
		//public methods
		getOrderTypes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			        //it can, so return just this store
			        eventController.requestEventFiring(retval, fromCache);
			        return retval;
		         }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			         model: 'AssetManagement.customer.model.bo.Cust_011',
			         autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildCust_011StoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderTypes of BaseCust_011Manager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('SCENARIO', AssetManagement.customer.model.bo.UserInfo.getInstance().get('mobileScenario'));
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_011', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_011', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderTypes of BaseCust_011Manager', ex);
			}
	
			return retval;
		},
		
		getOrderTypesforNotifType: function(qmart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}                
				
				var me = this;
				var cache = this.getCache();
				var baseStore = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
				
			    var orderTypesforNotifType = null;
			    
			    var extractFromBase = function() {
			    	try {
						orderTypesforNotifType = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.Cust_011',
							autoLoad: false
						});
			    	
			    		if(baseStore && baseStore.getCount() > 0) {
			    			baseStore.each(function(orderType) {
								if(orderType.get('qmart') === qmart)
									orderTypesforNotifType.add(orderType);
							}, this);
						}
			    		
						eventController.requestEventFiring(retval, orderTypesforNotifType);
						return retval; 
			    	} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderTypesforNotiftype of BaseCust_011Manager', ex);
						eventController.requestEventFiring(retval, undefined);
			    	}
			    };
				
			    //check if base store could be drawn from cache
				if(baseStore) {
					extractFromBase();
				} else {
					//base store has not yet been cached, so directly draw them from database (this will cache it)
					var allOrderTypesCallback = function(orderTypes) {
						try {
							if(orderTypes === undefined) {
								eventController.requestEventFiring(retval, undefined);
							} else {
								baseStore = orderTypes;
								extractFromBase();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderTypesforNotiftype of BaseCust_011Manager', ex);
							eventController.requestEventFiring(retval, undefined);
				    	}
					};
					
					var requestForAll = this.getOrderTypes(useBatchProcessing);
					
					if(requestForAll > -1) {
						eventController.registerOnEventForOneTime(requestForAll, allOrderTypesCallback);
					} else {
						eventController.requestEventFiring(retval, undefined);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderTypesforNotiftype of BaseCust_011Manager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
        getOrderType: function(auart, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(auart)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(auart);
		
		        if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
		        }
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_011 = me.buildCust_011FromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(cust_011);
						eventController.requestEventFiring(retval, cust_011);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderType of BaseCust_011Manager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};

				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('SCENARIO', AssetManagement.customer.model.bo.UserInfo.getInstance().get('mobileScenario'));
				keyMap.add('AUART', auart);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_011', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_011', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrderType of BaseCust_011Manager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_011FromDataBaseQuery: function(eventArgs) {
			if(eventArgs) {
				if(eventArgs.target.result) {
					var cust_011 = this.buildCust_011FromDbResultObject(eventArgs.target.result.value);
					//this.addToCache(cust_011);
					
					return cust_011;
				}
			}
			
			return null;
		},
		
		buildCust_011StoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var cust_011 = this.buildCust_011FromDbResultObject(cursor.value);
						store.add(cust_011);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_011StoreFromDataBaseQuery of BaseCust_011Manager', ex);
			}
		},
		
		buildCust_011FromDbResultObject: function(dbResult) {
	        var retval = null; 

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Cust_011', {
					scenario: dbResult['SCENARIO'],
			     	auart: dbResult['AUART'],
				    txt: dbResult['TXT'],
				    createallowed: dbResult['CREATE_ALLOWED'],
				    steus: dbResult['STEUS'],
				    flagrelease:dbResult['FLAG_RELEASE'],
			    	withmeasdoc: dbResult['WITH_MEAS_DOC'],
			     	partnertransfer: dbResult['PARTNER_TRANSFER'],
			    	qmart: dbResult['QMART'],
			     	ilart: dbResult['ILART'],
				    service: dbResult['SERVICE'],
				    pdfrepactive: dbResult['PDF_REP_ACTIVE'],
			    	stsma: dbResult['STSMA'],
				    vrg_stsma: dbResult['VRG_STSMA'],
				    artpr: dbResult['ARTPR'],
				    qpart: dbResult['QPART'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['SCENARIO'] + dbResult['AUART']);
						
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_011FromDbResultObject of BaseCust_011Manager', ex);
			}
			return retval;
		}
	}
});