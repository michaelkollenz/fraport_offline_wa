Ext.define('AssetManagement.base.manager.BaseCustomizingManager', {
	
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.model.bo.Cust_para',
	    'AssetManagement.customer.helper.CursorHelper',
	    'AssetManagement.customer.utils.StringUtils',
	    'Ext.util.HashMap',
	    'Ext.data.Store',
	    'AssetManagement.customer.model.bo.FuncPara',
	    'AssetManagement.customer.model.bo.Cust_para'
    ],
    
	inheritableStatics: {

	    initializeCustParaInstance: function(instance) {
			try {
				//not calling the the setInitialized API to prevent an event firing
				instance.set('isInitialized', false);
	
				var me = this;
				var custParaFetched = false;
				var aborted = false;
				
				var successCallbackForCustPara = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
						
						if(!done && aborted === false) {
							custParaFetched = me.buildFunc_ParaFromDataBaseQuery.call(me, instance, eventArgs);

							if(custParaFetched === false) {
								aborted = true;
							}
						}
						
						if(done)
						    instance.setInitialized(custParaFetched);
					} catch(ex) {
						aborted = true;
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCustParaInstance of BaseCustomizingManager', ex);
						instance.setInitialized(false);
					}
				};

				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MC_CUST_PARA', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MC_CUST_PARA', keyRange, successCallbackForCustPara, null, true);
				
				AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCustParaInstance of BaseCustomizingManager', ex);
				instance.setInitialized(false);
			}
		},
	
		getCust_Para: function(paragroup, fieldname,  useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobileuser)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_para = me.buildCust_ParaDataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, cust_para);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_Para of BaseCust_ParaManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('PARAGROUP', paragroup);
				keyMap.add('FIELDNAME', fieldname);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MC_CUST_PARA', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MC_CUST_PARA', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_Para of BaseCust_ParaManager', ex);
			}
			
			return retval;
		},
		
		getCust_Para_Group: function(paragroup,  useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(paragroup)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var cust_para = me.buildCust_ParaDataBaseQuery.call(me, eventArgs);
						eventController.requestEventFiring(retval, cust_para);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_Para_Group of BaseCust_ParaManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('PARAGROUP', paragroup);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MC_CUST_PARA', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MC_CUST_PARA', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCust_Para_Group of BaseCust_ParaManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildCust_ParaDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildCust_ParaFromDbResultObject(eventArgs.target.result.value);
					}
				}		
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_ParaDataBaseQuery of BaseCust_ParaManager', ex);
			}
			
			return retval;
		},
		
		buildFunc_ParaFromDataBaseQuery: function(funcPara, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	funcPara = this.fillFuncPara(cursor, funcPara);
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				    return true; 
				}
				else 
					return false; 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFunc_ParaFromDataBaseQuery of BaseCust_ParaManager', ex);
			}	
		},
		
		buildFunc_ParaStoreFromDataBaseQuery: function(funcPara, eventArgs){
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	funcPara = this.fillFuncPara(cursor, funcPara);
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				    return true; 
				}
				else 
					return false; 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFunc_ParaStoreFromDataBaseQuery of BaseCust_ParaManager', ex);
			}	
		},
		
		fillFuncPara: function(cursor, funcPara){
	        try {
	        	if(cursor.value){
					var claimed = true;
					var paragroup = cursor.value['PARAGROUP']; 
					var name = cursor.value['FIELDNAME'];
					
					if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(name) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(paragroup))
					    AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);

					var parameter = this.buildCust_ParaFromDbResultObject(cursor.value);


					if (parameter.get('id')){
					    funcPara.set(name.toLowerCase(), parameter);
					}
                    else
					    claimed = false;
					
	        	    //exceptions 
					var exception_claimed = this.exeptionalParametersHandling(funcPara, name, paragroup, parameter, cursor)

					if (!(claimed || exception_claimed))
					{
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillFuncPara of BaseCustomizingManager', ex);
			}
		},
		exeptionalParametersHandling: function (funcPara, name, paragroup, parameter, cursor) {
		    try {
		        var claimed = true;
		        if ('Z_EXT_CONF' === paragroup) {
		            //OxTodo Todo 
		            //#this is not supposed to be in here!!
		            //needs to be changed! This is NOT at all a customizing table!!
		            //It's left for now as an exception
		            var isStore = funcPara.get('ext_conf_material') instanceof Ext.data.Store;
		            if (!isStore) {
		                funcPara.set('ext_conf_material', Ext.create('Ext.data.Store', {
		                    model: 'AssetManagement.customer.model.bo.Cust_para'
		                }));
		            }
		            var materialContainer = funcPara.get('ext_conf_material');
		            var record = this.buildCust_ParaFromDbResultObject(cursor.value);
		            materialContainer.add(record);
		            //**************************************************************************//
		        }
		        else {
		            claimed = false;
		        }
		        return claimed;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillFuncPara of BaseCustomizingManager', ex);
		    }
		},

		parseBooleanX: function (val1) {
		    try {
			    if(val1 !== null && 'X' === val1)
		    	    return true; 
		        else
			        return false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseBooleanX of BaseCustomizingManager', ex);
			}
		},

		parseInteger: function (valueAsString) {
		    var retval = 0;

		    try {
		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(valueAsString)) {
		            try {
		                retval = parseInt(valueAsString);
		            } catch (ex) {
		            }
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseInteger of BaseCustomizingManager', ex);
		    }

		    return retval;
		},
		
		buildCust_ParaFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.Cust_para', {
					scenario: dbResult['SCENARIO'],
					paragroup: dbResult['PARAGROUP'],
					fieldname: dbResult['FIELDNAME'],
					fieldval1: dbResult['FIELDVAL1'],
					fieldval2: dbResult['FIELDVAL2']
				});
	        	retval.fieldval1 = retval.fieldval1 ? retval.fieldval1 : '';
				retval.set('id', dbResult['SCENARIO'] + dbResult['PARAGROUP'] +  dbResult['FIELDNAME']);
			  
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCust_ParaFromDbResultObject of BaseCustomizingManager', ex);
			}
			
			return retval;
		}
	}
});