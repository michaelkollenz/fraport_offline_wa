Ext.define('AssetManagement.base.manager.BaseKeyFConfManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.KeyFConf',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
	    //public
        getKeyFConfsForOrder: function(aufnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
	
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.KeyFConf',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildKeyFConfStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFConfsForOrder of BaseKeyFConfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', aufnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_KEYFCONF', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_KEYFCONF', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFConfsForOrder of BaseKeyFConfManager', ex);
			}
	
			return retval;
		},
		
		
	    getKeyFConfs: function(aufnr, vornr, split, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)){
					eventController.requestEventFiring(retval, null);
					return retval;
				} else if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr)
								&& !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(split)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.KeyFConf',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildKeyFConfStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFConfs of BaseKeyFConfManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('AUFNR', aufnr);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr))
					keyMap.add('VORNR', vornr);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(split))
					keyMap.add('SPLIT', split);
	
	            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_KEYFCONF', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_KEYFCONF', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeyFConfs of BaseKeyFConfManager', ex);
			}
	
			return retval;
		},

        //private
		buildKeyFConfFromDataBaseQuery: function(eventArgs) {
        	var retval = null;
		
        	try {
        		if(eventArgs) {
    				if(eventArgs.target.result) {
    					retval = this.buildKeyFConfFromDbResultObject(eventArgs.target.result.value);
    				}
    			}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildKeyFConfFromDataBaseQuery of BaseKeyFConfManager', ex);
			}
			
			return retval;
		},
		
		buildKeyFConfStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var keyfconf = this.buildKeyFConfFromDbResultObject(cursor.value);
						store.add(keyfconf);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildKeyFConfStoreFromDataBaseQuery of BaseKeyFConfManager', ex);
			}
		},
		
		buildKeyFConfFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
			    retval = Ext.create('AssetManagement.customer.model.bo.KeyFConf', {
				    aufnr: dbResult['AUFNR'],
				    vornr: dbResult['VORNR'],
			        split: dbResult['SPLIT'],
			        rmzhl: dbResult['RMZHL'],
			        arbpl: dbResult['ARBPL'],
			        aueru: dbResult['AUERU'],   	
			        ausor: dbResult['AUSOR'],
			        bemot: dbResult['BEMOT'],
			        ernam: dbResult['ERNAM'],
			        idaue: dbResult['IDAUE'],
			        idaur: dbResult['IDAUR'],
			        ismne: dbResult['ISMNE'],
			        ismnw: dbResult['ISMNW'],
                    learr: dbResult['LEARR'],
			        ltxa1: dbResult['LTXA1'],   	
			        ofmne: dbResult['OFMNE'],
			        ofmnw: dbResult['OFMNW'],
			        pernr: dbResult['PERNR'],
			        txtsp: dbResult['TXTSP'],
                    werks: dbResult['WERKS'],
                    stagr: dbResult['STAGR'],
                    grund: dbResult['GRUND'],
                    bezei: dbResult['BEZEI'],
				
				    ersda: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERSDA']),
				    budat: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['BUDAT']),
				    isd: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ISDD'], dbResult['ISDZ']),
				    ied: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['IEDD'], dbResult['IEDZ']),
  
				    updFlag:   dbResult['UPDFLAG'],
				    mobilekey: dbResult['MOBILEKEY'], 
                    childkey:  dbResult['CHILDKEY'],
                    childkey2: dbResult['CHILDKEY2'],

					actype: dbResult['ACTYPE'],
					usr1: dbResult['USR1'],
					usr2: dbResult['USR2'],
					pausesz: dbResult['PAUSESZ'],
					pauseez: dbResult['PAUSEEZ'],
					kstar: dbResult['KSTAR'],
					betrag: dbResult['BETRAG'],
					tcurr: dbResult['TCURR'],
					lgart: dbResult['LGART']

			    });
			
			    retval.set('id', dbResult['AUFNR'] + dbResult['VORNR'] + dbResult['SPLIT'] + dbResult['RMZHL']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildKeyFConfFromDbResultObject of BaseKeyFConfManager', ex);
			}
			
			return retval;
		}
	}
});