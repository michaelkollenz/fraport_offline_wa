Ext.define('AssetManagement.base.manager.BaseInternalDeliveryManager', {
  extend: 'AssetManagement.customer.manager.OxBaseManager',
  requires: [
    'AssetManagement.customer.helper.OxLogger',
    // 'AssetManagement.customer.core.Core',
    // 'AssetManagement.customer.manager.SDDeliveryManager',		INCLUSION CAUSES RING DEPENDENCY
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.utils.DateTimeUtils',
    'AssetManagement.customer.helper.CursorHelper',
    'AssetManagement.customer.helper.MobileIndexHelper',
    'AssetManagement.customer.model.bo.InternalDelivery',
    'Ext.util.HashMap',
    'Ext.data.Store'
  ],

  inheritableStatics: {

    //protected
    getCache: function () {
      var retval = null;

      try {
        // retval = AssetManagement.customer.manager.cache.InternalDeliveryCache.getInstance();
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseInternalDeliveryManager', ex);
      }

      return retval;
    },

    //public methods
    getDeliverys: function (useBatchProcessing) {
      var retval = -1;

      try {
        var eventController = AssetManagement.customer.controller.EventController.getInstance();
        retval = eventController.getNextEventId();

        //check if the cache can delivery the complete dataset
        // var cache = this.getCache();
        // var fromCache = cache.getStoreForAll();
        //
        // if(fromCache) {
        //   //it can, so return just this store
        //   eventController.requestEventFiring(retval, fromCache);
        //   return retval;
        // }

        var fromDataBase = Ext.create('Ext.data.Store', {
          model: 'AssetManagement.customer.model.bo.InternalDelivery',
          autoLoad: false
        });

        var me = this;
        var successCallback = function (eventArgs) {
          try {
            var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

            me.buildDeliveriesStoreFromDataBaseQuery(fromDataBase, eventArgs);

            if (done) {
              //add the whole store to the cache
              // cache.addStoreForAllToCache(fromDataBase);

              //return a store from cache to eliminate duplicate objects
              var toReturn = fromDataBase
              eventController.requestEventFiring(retval, toReturn);
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeliverys of BaseInternalDeliveryManager', ex);
            eventController.requestEventFiring(retval, undefined);
          }
        };

        var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_ZIA_DEL', null);
        AssetManagement.customer.core.Core.getDataBaseHelper().query('D_ZIA_DEL', keyRange, successCallback, null, useBatchProcessing);
      } catch (ex) {
        retval = -1;
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterials of BaseInternalDeliveryManager', ex);
      }

      return retval;
    },


    //private methods
    buildDeliveryFromDataBaseQuery: function (eventArgs) {
      var retval = null;

      try {
        if (eventArgs) {
          if (eventArgs.target.result) {
            retval = this.buildDeliveryFromDbResultObject(eventArgs.target.result.value);
          }
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDeliveryFromDataBaseQuery of BaseInternalDeliveryManager', ex);
      }

      return retval;
    },

    buildDeliveriesStoreFromDataBaseQuery: function (store, eventArgs) {
      try {
        if (eventArgs && eventArgs.target) {
          var cursor = eventArgs.target.result;

          if (cursor) {
            var Delivery = this.buildDeliveryFromDbResultObject(cursor.value);
            store.add(Delivery);

            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
          }
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDeliveriesStoreFromDataBaseQuery of BaseInternalDeliveryManager', ex);
      }
    },

    buildDeliveryFromDbResultObject: function (dbResult) {
      var retval = null;

      try {

        retval = Ext.create('AssetManagement.customer.model.bo.InternalDelivery', {
          belnr: dbResult['BELNR'],
          erdat: dbResult['ERDAT'],
          segid: dbResult['SEGID'],
          ertim: dbResult['ERTIM'],
          mblnr: dbResult['MBLNR'],
          mjahr: dbResult['MJAHR'],
          mblpo: dbResult['MBLPO'],
          ebeln: dbResult['EBELN'],
          ebelp: dbResult['EBELP'],
          usnam: dbResult['USNAM'],
          txz01: dbResult['TXZ01'],
          menge: dbResult['MENGE'],
          meins: dbResult['MEINS'],
          empf: dbResult['EMPF'],
          gebnr: dbResult['GEBNR'],
          raum: dbResult['RAUM'],
          short: dbResult['SHORT'],
          kostl: dbResult['KOSTL'],
          aufnr: dbResult['AUFNR'],
          pspnr: dbResult['PSPNR'],
          wlief: dbResult['WLIEF'],
          wuzeitv: dbResult['WUZEITV'],
          wuzeitb: dbResult['WUZEITB'],
          sgtxt: dbResult['SGTXT'],
          bereich: dbResult['BEREICH'],
          ia_status: dbResult['IA_STATUS'],
          grund: dbResult['GRUND'],
          gtext: dbResult['GTEXT'],
          dienst: dbResult['DIENST'],
          nummer: dbResult['NUMMER'],
          lfname: dbResult['LFNAME'],
          telefon: dbResult['TELEFON'],
          guid: dbResult['GUID'],
          email: dbResult['EMAIL'],
          prio: dbResult['PRIO'],
          trans: dbResult['TRANS'],
          xblock: dbResult['XBLOCK'],
          vlgort: dbResult['VLGORT'],
          equnr: dbResult['EQUNR'],
          lidat: dbResult['LIDAT'],
          sequ: dbResult['SEQU'],
          matnr: dbResult['MATNR'],
          passcode: dbResult['PASSCODE'],
          mobilekey: dbResult['MOBILEKEY'],
          posid: dbResult['POSID'],
          updFlag: dbResult['updflag']
        });


        retval.set('id', dbResult['BELNR']);

      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDeliveryFromDbResultObject of BaseInternalDeliveryManager', ex);
      }

      return retval;
    },
    /**
     * saves a changed Delivery to the database
     */
    saveDeliveryItems: function (deliveryItems) {
      var retval = -1;

      try {
        var eventController = AssetManagement.customer.controller.EventController.getInstance();
        retval = eventController.getNextEventId();

        if (!deliveryItems) {
          eventController.requestEventFiring(retval, false);
          return retval;
        } else if (deliveryItems.getCount() === 0) {
          eventController.requestEventFiring(retval, true);
          return retval;
        }
        //save each delivery item
        var pendingSaveLoops = 0;
        var me = this;
        var errorOccurred = false;
        var reported = false;

        var completeFunction = function () {
          try {
            if (errorOccurred === true && reported === false) {
              eventController.fireEvent(retval, false);
              reported = true;
            } else if (pendingSaveLoops === 0 && errorOccurred === false) {

              eventController.fireEvent(retval, true);
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDeliveryItems of BaseInternalDeliveryManager', ex);
          }
        };

        var callback = function (eventArgs) {
          try {
            var success = eventArgs.type === "success";

            errorOccurred = errorOccurred || !success;

            pendingSaveLoops--;
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDeliveryItems of BaseInternalDeliveryManager', ex);
            errorOccurred = true;
          } finally {
            completeFunction();
          }
        };

        deliveryItems.each(function (deliveryItem) {
          //if (parseInt(item.get('entryQnt')) > 0 || item.get('zerocount') === true /*|| !isAllCompleted */ || item.get('updFlag') === 'I') {
          pendingSaveLoops++;

          deliveryItem.set('updFlag', 'U');


          // if (deliveryItem.get('ia_status') !== '' || deliveryItem.get('grund') !== '') {
          //   deliveryItem.set('localStatus', 'completed');
          // }

          var toSave = this.buildDataBaseObjectForDeliveryItem(deliveryItem);
          AssetManagement.customer.core.Core.getDataBaseHelper().put('D_ZIA_DEL', toSave, callback, callback);
          //AssetManagement.customer.core.Core.getDataBaseHelper().update('D_MI_INV_ITEM', null, toSave, callback, callback);
          //completeFunction();
          //}
        }, this);
      } catch (ex) {
        retval = -1;
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside savedeliveryItems of BaseInternalDeliveryManager', ex);
      }

      return retval;
    },

    buildDataBaseObjectForDeliveryItem: function (deliveryItem) {
      var retval = null;

      try {
        var ac = AssetManagement.customer.core.Core.getAppConfig();
        var iaStatus = deliveryItem.get('ia_status');

        if (iaStatus === 'XX') { // Clear the flag for not Delivered option since then we will have a reason instead
          iaStatus = '';
        }

        var retval = {};
        retval['MANDT'] = ac.getMandt();
        retval['USERID'] = ac.getUserId();
        retval['SCENARIO'] = ac.getScenario();
        retval['UPDFLAG'] = deliveryItem.get('updFlag');
        retval['BELNR'] = deliveryItem.get('belnr');
        retval['SEGID'] = deliveryItem.get('segid');
        retval['ERDAT'] = deliveryItem.get('erdat');
        retval['ERTIM'] = deliveryItem.get('ertim');
        retval['MBLNR'] = deliveryItem.get('mblnr');
        retval['MJAHR'] = deliveryItem.get('mjahr');
        retval['MBLPO'] = deliveryItem.get('mblpo');
        retval['EBELN'] = deliveryItem.get('ebeln');
        retval['EBELP'] = deliveryItem.get('ebelp');
        retval['USNAM'] = deliveryItem.get('usnam');
        retval['TXZ01'] = deliveryItem.get('txz01');
        retval['MENGE'] = deliveryItem.get('menge');
        retval['MEINS'] = deliveryItem.get('meins');
        retval['EMPF'] = deliveryItem.get('empf');
        retval['GEBNR'] = deliveryItem.get('gebnr');
        retval['RAUM'] = deliveryItem.get('raum');
        retval['SHORT'] = deliveryItem.get('short');
        retval['KOSTL'] = deliveryItem.get('kostl');
        retval['AUFNR'] = deliveryItem.get('aufnr');
        retval['PSPNR'] = deliveryItem.get('pspnr');
        retval['WLIEF'] = deliveryItem.get('wlief');
        retval['WUZEITV'] = deliveryItem.get('wuzeitv');
        retval['WUZEITB'] = deliveryItem.get('wuzeitb');
        retval['SGTXT'] = deliveryItem.get('sgtxt');
        retval['BEREICH'] = deliveryItem.get('bereich');
        retval['IA_STATUS'] = iaStatus;
        retval['GRUND'] = deliveryItem.get('grund');
        retval['GTEXT'] = deliveryItem.get('gtext');
        retval['DIENST'] = deliveryItem.get('dienst');
        retval['NUMMER'] = deliveryItem.get('nummer');
        retval['LFNAME'] = deliveryItem.get('lfname');
        retval['TELEFON'] = deliveryItem.get('telefon');
        retval['GUID'] = deliveryItem.get('guid');
        retval['EMAIL'] = deliveryItem.get('email');
        retval['PRIO'] = deliveryItem.get('prio');
        retval['TRANS'] = deliveryItem.get('trans');
        retval['XBLOCK'] = deliveryItem.get('xblock');
        retval['VLGORT'] = deliveryItem.get('vlgort');
        retval['EQUNR'] = deliveryItem.get('equnr');
        retval['LIDAT'] = deliveryItem.get('lidat');
        retval['SEQU'] = deliveryItem.get('sequ');
        retval['MATNR'] = deliveryItem.get('matnr');
        retval['PASSCODE'] = deliveryItem.get('passcode');
        retval['MOBILEKEY'] = deliveryItem.get('mobilekey');
        retval['POSID'] = deliveryItem.get('posid');
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForDeliveryItem of BaseInternalDeliveryManager', ex);
      }

      return retval;
    }
  }

});
