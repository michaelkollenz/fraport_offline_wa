Ext.define('AssetManagement.base.manager.BasePartnerAPManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.PartnerAPCache',
        'AssetManagement.customer.model.bo.PartnerAP',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.PartnerAPCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BasePartnerAPManager', ex);
			}
			
			return retval;
		},
		
		//public
		getPartnerAPs: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
		        //check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
			       //it can, so return just this store
			       eventController.requestEventFiring(retval, fromCache);
			       return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			       model: 'AssetManagement.customer.model.bo.PartnerAP',
			       autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildPartnerAPStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPartnerAPs of BasePartnerAPManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PARTNER_AP', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PARTNER_AP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPartnerAPs of BasePartnerAPManager', ex);
				retval = -1;
			}
	
			return retval;
		},
		
		getPartnerAP: function(parnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(parnr);
		
		        if(fromCache) {
			        eventController.requestEventFiring(retval, fromCache);
		        	return retval;
		        }
		
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var partnerAP = me.buildPartnerAPFromDataBaseQuery.call(me, eventArgs);
						cache.addToCache(partnerAP);
						eventController.requestEventFiring(retval, partnerAP);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPartnerAP of BasePartnerAPManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('PARNR', parnr);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('D_PARTNER_AP', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PARTNER_AP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPartnerAP of BasePartnerAPManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		//private
		buildPartnerAPFromDataBaseQuery: function(eventArgs) {
			var retval = null;
		
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildPartnerAPFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPartnerAPFromDataBaseQuery of BasePartnerAPManager', ex);
			}
			
			return retval;
		},
		
		buildPartnerAPStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var partAP = this.buildPartnerAPFromDbResultObject(cursor.value);
						store.add(partAP);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPartnerAPStoreFromDataBaseQuery of BasePartnerAPManager', ex);
			}
		},
		
		buildPartnerAPFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.PartnerAP', {
					parnr: dbResult['PARNR'],
					kunnr: dbResult['KUNNR'],
				    titlep: dbResult['TITLE_P'],
					firstname: dbResult['FIRSTNAME'],
					lastname: dbResult['LASTNAME'],
				    city: dbResult['CITY'],
			     	postlcod1: dbResult['POSTL_COD1'],
				    street: dbResult['STREET'],
			    	telnumbermob: dbResult['TEL_NUMBER_MOB'],
				    tel1numbr: dbResult['TEL1_NUMBR'],
				    faxnumber: dbResult['FAX_NUMBER'],
	                functionVal: dbResult['FUNCTION'],
	                email: dbResult['E_MAIL'],
	                telextens: dbResult['TEL_EXTENS'],
	                faxextens: dbResult['FAX_EXTENS'],
                    timezone: dbResult['TIME_ZONE'],
                    str_suppl1: dbResult['STR_SUPPL1'],
	                updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['PARNR']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPartnerAPFromDbResultObject of BasePartnerAPManager', ex);
			}
			
			return retval;
		}
	}
});