Ext.define('AssetManagement.base.manager.BaseMaterialStockageManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.MaterialStockageCache',
        'AssetManagement.customer.model.bo.MatStock',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.CursorHelper',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//protected
		getCache: function() {
			var retval = null;

			try {
				retval = AssetManagement.customer.manager.cache.MaterialStockageCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseMaterialStockageManager', ex);
			}
			
			return retval;
		},
	
		//public methods
		getMatStocks: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
		        	//it can, so return just this store
		        	eventController.requestEventFiring(retval, fromCache);
		        	return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
		        	model: 'AssetManagement.customer.model.bo.MatStock',
		        	autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildMatStocksStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							me.loadListDependendDataForMatStocks.call(me, fromDataBase, retval);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocks of BaseMaterialStockageManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
			
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MATSTOCK', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MATSTOCK', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocks of BaseMaterialStockageManager', ex);
			}
			
			return retval;
		},
		
		getMatStocksForMaterial: function(matnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var cache = this.getCache();
				var allMatStocks = cache.getStoreForAll();
				
			    var matStocksOfMaterial = null;
			    
			    var extractFromAll = function() {
			    	try {
			    		matStocksOfMaterial = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.MatStock',
							autoLoad: false
						});
			    	
			    		if(allMatStocks && allMatStocks.getCount() > 0) {
			    			allMatStocks.each(function(matStock) {
								if(matStock.get('matnr') === matnr)
									matStocksOfMaterial.add(matStock);
							}, this);
						}
			    		
						eventController.requestEventFiring(retval, matStocksOfMaterial);
						return retval; 
			    	} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
						eventController.requestEventFiring(retval, undefined);
			    	}
			    };
				
			    //check if store for all could be drawn from cache
				if(allMatStocks) {
					extractFromAll();
				} else {
					//the list store has not yet been cached, so directly draw them from database (this will cache it)
					var allMatStocksCallback = function(matStocks) {
						try {
							if(matStocks === undefined) {
								eventController.requestEventFiring(retval, undefined);
							} else {
								allMatStocks = matStocks;
								extractFromAll();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
							eventController.requestEventFiring(retval, undefined);
				    	}
					};
					
					var requestForAll = this.getMatStocks(useBatchProcessing);
					
					if(requestForAll > -1) {
					    eventController.registerOnEventForOneTime(requestForAll, allMatStocksCallback);
					} else {
						eventController.requestEventFiring(retval, undefined);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		getMatStocksForStorageLocation: function(werks, lgort, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var cache = this.getCache();
				var allMatStocks = cache.getStoreForAll();
				
			    var matStocksOfStorageLocation = null;
			    
			    var extractFromAll = function() {
			    	try {
			    		matStocksOfStorageLocation = Ext.create('Ext.data.Store', {
							model: 'AssetManagement.customer.model.bo.MatStock',
							autoLoad: false
						});
			    	
			    		if(allMatStocks && allMatStocks.getCount() > 0) {
			    			allMatStocks.each(function(matStock) {
								if(matStock.get('werks') === werks && matStock.get('lgort') === lgort)
									matStocksOfStorageLocation.add(matStock);
							}, this);
						}
			    		
						eventController.requestEventFiring(retval, matStocksOfStorageLocation);
						return retval; 
			    	} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
						eventController.requestEventFiring(retval, undefined);
			    	}
			    };
				
			    //check if store for all could be drawn from cache
				if(allMatStocks) {
					extractFromAll();
				} else {
					//the list store has not yet been cached, so directly draw them from database (this will cache it)
					var allMatStocksCallback = function(matStocks) {
						try {
							if(matStocks === undefined) {
								eventController.requestEventFiring(retval, undefined);
							} else {
								allMatStocks = matStocks;
								extractFromAll();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
							eventController.requestEventFiring(retval, undefined);
				    	}
					};
					
					var requestForAll = this.getMatStocks(useBatchProcessing);
					
					if(requestForAll > -1) {
					    eventController.registerOnEventForOneTime(requestForAll, allMatStocksCallback);
					} else {
						eventController.requestEventFiring(retval, undefined);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		getMatStock: function(matnr, werks, lgort, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)
							|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var me = this;
				var cache = this.getCache();
				var allMatStocks = cache.getStoreForAll();
				
			    var specificMatStock = null;
			    
			    var extractFromAll = function() {
			    	try {
			    		if(allMatStocks && allMatStocks.getCount() > 0) {
			    			allMatStocks.each(function(matStock) {
								if(matStock.get('matnr') === matnr && matStock.get('werks') === werks && matStock.get('lgort') === lgort) {
									specificMatStock = matStock;
									
									return false;
								}
							}, this);
						}
			    		
						eventController.requestEventFiring(retval, specificMatStock);
						return retval; 
			    	} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
						eventController.requestEventFiring(retval, undefined);
			    	}
			    };
				
			    //check if store for all could be drawn from cache
				if(allMatStocks) {
					extractFromAll();
				} else {
					//the list store has not yet been cached, so directly draw them from database (this will cache it)
					var allMatStocksCallback = function(matStocks) {
						try {
							if(matStocks === undefined) {
								eventController.requestEventFiring(retval, undefined);
							} else {
								allMatStocks = matStocks;
								extractFromAll();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
							eventController.requestEventFiring(retval, undefined);
				    	}
					};
					
					var requestForAll = this.getMatStocks(useBatchProcessing);
					
					if(requestForAll > -1) {
					    eventController.registerOnEventForOneTime(requestForAll, allMatStocksCallback);
					} else {
						eventController.requestEventFiring(retval, undefined);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatStocksForMaterial of BaseMaterialStockageManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		//checks if a material is available on a storage location in a specific quantity (passed as number)
		//returns an number indicating the result
		//0: available amount is less than requested
		//1: available
		//2: storage location is empty
		//NaN: on exception
		checkForAvailableQuantity: function(matnr, werks, lgort, quantity) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks)
							|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)
								|| quantity === undefined
									|| quantity < 0) {
					eventController.requestEventFiring(retval, Number.NaN);
					return retval;
				}
				
				var evaluationAction = function(matStock) {
					try {
						var result = -1;
					
						if(matStock === undefined) {
							result = Number.NaN;
						} else if(matStock === null) {
							result = 2;
						} else {
							var matStocksQuantity = AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(matStock.get('labst'));
							
							if(matStocksQuantity === Number.NaN || matStocksQuantity == 0) {
								result = 2;
							} else if(matStocksQuantity < quantity) {
								result = 0
							} else {
								result = 1;
							}
						}
						
						eventController.fireEvent(retval, result);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAvailableQuantity of BaseMatStockManager', ex);
						eventController.fireEvent(retval, Number.NaN);
					}
				};
				
				var matStockRequestId = this.getMatStock(matnr, werks, lgort);
				
				if (matStockRequestId > 0) {
					eventController.registerOnEventForOneTime(matStockRequestId, evaluationAction);
				} else {
					eventController.requestEventFiring(retval, Number.NaN);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAvailableQuantity of BaseMatStockManager', ex);
				retval = -1;
			}
			
			return retval;
		},

		//private methods
		loadListDependendDataForMatStocks: function(matStocks, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(!matStocks || matStocks.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, matStocks);

					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var materials = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						eventController.fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignListDependendDataForMatStocks.call(me, eventIdToFireWhenComplete, matStocks, materials);
					}
				};
				
				//get materials
				done++;
				
				var materialsSuccessCallback = function(materialStore) {
					errorOccured = materialStore === undefined;
					
					materials = materialStore;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
				eventController.registerOnEventForOneTime(eventId, materialsSuccessCallback);
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForMatStocks of BaseMaterialStockageManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignListDependendDataForMatStocks: function(eventIdToFireWhenComplete, matStocks, materials) {
			try {
				matStocks.each(function (matStock, index, length) {
					//assign materials
					if(materials) {
						var matnr = matStock.get('matnr');
						materials.each(function(material) {
							if(material.get('matnr') === matnr) {
								matStock.set('material', material);
								return false;
							}
						});
					}
				});
				
				var cache = this.getCache();
				cache.addStoreForAllToCache(matStocks);
				
				//return a store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForMatStocks of BaseMaterialStockageManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForMatStock: function(matStock, eventIdToFireWhenComplete) {
			try {
				//do not continue if there is no data
				if(!matStock) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, matStock);

					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var material = null;
	
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						eventController.fireEvent(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccured === false) {
						me.assignDependendDataForMatStock.call(me, eventIdToFireWhenComplete, matStock, material);
					}
				};
				
				//get material
				done++;
				
				var materialSuccessCallback = function(mat) {
					errorOccured = material === undefined;
					
					material = mat;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(matStock.get('matnr'), true);
				eventController.registerOnEventForOneTime(eventId, materialSuccessCallback);
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForMatStock of BaseMaterialStockageManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForMatStock: function(eventIdToFireWhenComplete, matStock, material) {
			try {
				if(material) {
					matStock.set('material', material);
				}
				
				var cache = this.getCache();
				cache.addToCache(matStock);
				
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, matStock);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForMatStock of BaseMaterialStockageManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().fireEvent(eventIdToFireWhenComplete, undefined);
			}
		},
		
		buildMatStockFromDataBaseQuery: function(eventArgs) {
            var retval = null;	
		
	        try {
	        	if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildMatStockFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMatStockFromDataBaseQuery of BaseMaterialStockageManager', ex);
			}
			
			return retval;
		},
		
		buildMatStocksStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var matstock = this.buildMatStockFromDbResultObject(cursor.value);
						store.add(matstock);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMatStocksStoreFromDataBaseQuery of BaseMaterialStockageManager', ex);
			}
		},
		
		buildMatStockFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.MatStock', {
					matnr: dbResult['MATNR'],
					werks: dbResult['WERKS'],
				    lgort: dbResult['LGORT'],
				    labst: dbResult['LABST'],
				    meins: dbResult['MEINS'],
				    sernp: dbResult['SERNP'],
			     	serpflicht: dbResult['SERPFLICHT'],
					speme: dbResult['SPEME'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['MATNR'] + dbResult['WERKS'] + dbResult['LGORT']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMatStockFromDbResultObject of BaseMaterialStockageManager', ex);
			}
			
			return retval;
		}
	}
});