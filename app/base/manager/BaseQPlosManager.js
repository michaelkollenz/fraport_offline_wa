﻿Ext.define('AssetManagement.base.manager.BaseQPlosManager', {

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.QPlos',
        'AssetManagement.customer.model.bo.QPlosOper',
        'AssetManagement.customer.manager.cache.QPlosCache',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.MobileIndexHelper',
        'AssetManagement.customer.manager.QPlosOperManager'
    ],

    inheritableStatics: {

        //protected
        getCache: function () {
            var retval = null;

            try {
                retval = AssetManagement.customer.manager.cache.QPlosCache.getInstance();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseQPlosManager', ex);
            }

            return retval;
        },

        //public methods
        getQPlos: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(prueflos)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                //check if the cache can delivery the complete dataset
                var cache = this.getCache();
                var fromCache = cache.getFromCache(prueflos);

                if (fromCache) {
                    eventController.requestEventFiring(retval, fromCache);
                    return retval;
                }

                var me = this;

                var successCallback = function (eventArgs) {
                    try {
                        var qplos = me.buildQplosFromDataBaseQuery.call(me, eventArgs);

                        if (!qplos) {
                            eventController.requestEventFiring(retval, null);
                            return;
                        }

                        me.loadDependendDataForQPlos.call(me, retval, qplos);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPrueflos of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PRUEFLOS', prueflos);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLOS', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPrueflos of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        //This method gets a specific qplos for an order and a specific equipment/funcLoc of that order
        //With the knowlage of the order and the equnr &&/|| tplnr we can identify 1:1 QPLOS
        getQPlosForOrderAndTechObj: function (aufnr, equnr, tplnr, qmnum, useBatchProcessing) {
            var retval = -1;
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var callback = function (keysTable) {
                    try {
                        var index = 0;
                        var errorOccurred = false;
                        if (keysTable.length > 0) { //pruflos numbers of the QPLOSes of the order
                            var prueflos = keysTable[index]['prueflos'];  //try the first one

                            var techObjSuccessCallback = function (techObj) {
                                try {
                                    errorOccurred = techObj === undefined;

                                    if (errorOccurred === true) {
                                        eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
                                        return false;
                                    } else {
                                        if (equnr === techObj.get('equnr') && tplnr === techObj.get('tplnr') && qmnum === keysTable[index]['qmnum']) { //check if the loaded equipment/funcloc matches the equnr or tplnr of the request
                                            var qplosSuccessCallback = function (qplos) { //if so, load the qplos for the currently selected prueflos number
                                                try {
                                                    errorOccurred = qplos === undefined;
                                                    if (errorOccurred === true) {
                                                        eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
                                                        return false;
                                                    } else {
                                                        eventController.requestEventFiring(retval, qplos);
                                                    }
                                                } catch (ex) {
                                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosForOrderAndTechObj of BaseQPlosManager', ex);
                                                    eventController.requestEventFiring(retval, undefined);
                                                }
                                            };

                                            var eventId = me.getQPlos(prueflos);
                                            eventController.registerOnEventForOneTime(eventId, qplosSuccessCallback);
                                        } else { //if the loaded techObj doesn't match the request,
                                            index++;
                                            var record = keysTable[index]; //take another prueflos number

                                            if (record) {
                                                prueflos = record['prueflos'];
                                                var eventId = me.loadTechObjectForQplos(prueflos); //load another techObj, for the new prueflos number
                                                if(eventId > -1) {
                                                    eventController.registerOnEventForOneTime(eventId, techObjSuccessCallback);
                                                } else {
                                                    eventController.requestEventFiring(retval, undefined);
                                                }
                                            } else { //if all prueflosesToLoad ware checked, it means there is no match, so there is no QPLOS for the request. Return null.
                                                eventController.requestEventFiring(retval, null);
                                            }
                                        }
                                    }
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosForOrderAndTechObj of BaseQPlosManager', ex);
                                    eventController.requestEventFiring(retval, undefined);
                                }
                            };

                            if (prueflos) {
                                var eventId = me.loadTechObjectForQplos(prueflos); //load the techObject for that prueflos number (so either an equipment or funcloc [in light mode] )
                                if(eventId > -1) {
                                    eventController.registerOnEventForOneTime(eventId, techObjSuccessCallback);
                                } else {
                                    eventController.requestEventFiring(retval, undefined);
                                }
                            } else {
                                eventController.requestEventFiring(retval, null);
                            }
                        }
                        else {
                            eventController.requestEventFiring(retval, null);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosForOrderAndTechObj of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var eventId = this.getKeysOfAllQplosForOrder(aufnr, useBatchProcessing); //getting PRUEFLOS (key field) for all the qploses of the order

                if (eventId > -1)
                    eventController.registerOnEventForOneTime(eventId, callback);
                else
                    eventController.requestEventFiring(retval, undefined);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosForOrderAndTechObj of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        getAllQPlosForOrder: function (aufnr, useBatchProcessing) {
            var retval = -1;

            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();

                var callback = function (keysTable) {
                    try {
                        var qplosStore = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.QPlos',
                            autoLoad: false
                        });
                        if (keysTable.length > 0) {
                            var counter = 0;
                            var errorOccurred = false;
                            var errorReported = false;

                            var qplosSuccessCallback = function (qplos) {
                                try {
                                    errorOccurred = qplos === undefined;

                                    if (errorOccurred === true && !errorReported) {
                                        errorReported = true;
                                        eventController.requestEventFiring(eventIdToFireWhenComplete, undefined);
                                    } else if (!errorOccurred) {
                                        qplosStore.add(qplos);
                                        counter++;
                                        if (counter === keysTable.length) {
                                            eventController.requestEventFiring(retval, qplosStore);
                                        }
                                    }
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllQPlosForOrder of BaseQPlosManager', ex);
                                    eventController.requestEventFiring(retval, undefined);
                                }
                            };

                            for (var index = 0; index < keysTable.length; index++) {
                                prueflos = keysTable[index]['prueflos'];
                                var eventId = me.getQPlos(prueflos);
                                eventController.registerOnEventForOneTime(eventId, qplosSuccessCallback);
                            }
                        }
                        else {
                            eventController.requestEventFiring(retval, qplosStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllQPlosForOrder of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var eventId = this.getKeysOfAllQplosForOrder(aufnr, useBatchProcessing);

                if (eventId > -1)
                    eventController.registerOnEventForOneTime(eventId, callback);
                else
                    eventController.requestEventFiring(retval, undefined);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllQPlosForOrder of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        //saveNewQPlos ARGUMENTS:
        //qplan is the object (with dependend data) based on which the qplos (with dependend data) will be created
        //techObj is the obj (equipment of funcLoc) for which the qplan was used to create a checklist. Needed to create QPLOS_POIN entry
        //matnr is the number of the material that qplos is associated with. It's a field of qplos
        //parent is either an order or a notification, for which the checklist must be created. Needed for some reference fields of qplos.
        //completed is a boolian value indicating if the checklist was completed
        //Expected RETURN VALUE is either a CREATED qplos or 'undefined' in case of an error
        saveNewQPlos: function (qplan, techObj, matnr, parent, qmnum, completed) {
            var retval = -1;
            
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var callback = function (nextCounterValue) {
                    try {
                        if (nextCounterValue === undefined) {
                            eventController.requestEventFiring(retval, undefined);
                            return;
                        }

                        var qplos = me.createQPlos(qplan, techObj, matnr, parent, qmnum, nextCounterValue);

                        if (!qplos) {
                            eventController.requestEventFiring(retval, undefined);
                            return
                        }

                        var saveCallback = function (success) {
                            try {
                                if (!success) {
                                    eventController.requestEventFiring(retval, undefined);
                                } else {
                                    var cache = me.getCache();
                                    cache.addToCache(qplos);

                                    eventController.requestEventFiring(retval, qplos);
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewQPlos of BaseQPlosOperManager', ex);
                                eventController.requestEventFiring(retval, undefined);
                            }
                        };

                        var eventId = me.saveQPlos(qplos, completed);
                        eventController.registerOnEventForOneTime(eventId, saveCallback);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewQPlos of BaseQPlosOperManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var qplosMobileIndex = AssetManagement.customer.helper.MobileIndexHelper.getNextMobileIndex('C_QPLOS', false);

                if (qplosMobileIndex > 1)
                    eventController.registerOnEventForOneTime(qplosMobileIndex, callback);
                else
                    retval = -1;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewQPlos of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        saveQPlos: function (qplos, completed) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qplos) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;
                qplos.set('updFlag', completed ? 'I' : 'A');
                
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (!success) {
                            eventController.requestEventFiring(retval, false);
                        } else {
                            var qplosPoinSuccessCallback = function (success) {
                                try {
                                    if (!success) {
                                        eventController.requestEventFiring(retval, false);
                                        return;
                                    }
                                    
                                    var qplosOpersSuccessCallback = function (success) {
                                        eventController.requestEventFiring(retval, success);
                                    };

                                    var qplosOpers = qplos.get('qplosOpers');
                                    var opersEventId = AssetManagement.customer.manager.QPlosOperManager.saveQPlosOperStore(qplosOpers, completed);
                                    eventController.registerOnEventForOneTime(opersEventId, qplosOpersSuccessCallback);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlos of BaseQPlosOperManager', ex);
                                    eventController.requestEventFiring(retval, false);
                                }
                            };

                            var poinEventId = me.createAndSaveQPlosPoin(qplos);
                            eventController.registerOnEventForOneTime(poinEventId, qplosPoinSuccessCallback);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlos of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var toSave = me.buildDataBaseObjectFromQPlos(qplos);
                AssetManagement.customer.core.Core.getDataBaseHelper().put('C_QPLOS', toSave, callback, callback);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQplos of BaseQPlosManager', ex);
            }

            return retval;
        },

        deleteQPlos: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(prueflos)) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;
                var requestsPending = 0;

                var successCallback = function () {
                    try {
                        var cache = me.getCache();
                        cache.removeFromCache(cache.getFromCache(prueflos)); 

                        var qplosOpersDeleteCallback = function (success) {
                            try {
                                var cache = me.getCache();
                                cache.removeFromCache(cache.getFromCache(prueflos));

                                eventController.requestEventFiring(retval, success);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlos of BaseQPlosManager', ex);
                                eventController.requestEventFiring(retval, false);
                            }
                        };

                        var deleteOpersEventId = AssetManagement.customer.manager.QPlosOperManager.deleteQPlosOpers(prueflos);
                        eventController.registerOnEventForOneTime(deleteOpersEventId, qplosOpersDeleteCallback);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlos of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        if (success) {
                            requestsPending--;
                            if (requestsPending === 0)
                                successCallback();
                        } else {
                            //can happen twice
                            eventController.requestEventFiring(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlos of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PRUEFLOS', prueflos);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('C_QPLOS', keyRange, callback, callback, useBatchProcessing);
                requestsPending++;

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS_POIN', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('C_QPLOS_POIN', keyRange, callback, callback, useBatchProcessing);
                requestsPending++;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlos of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private methods
        buildQplosFromDataBaseQuery: function (eventArgs) {
            var retval = null;
            try {
                if (eventArgs) {
                    if (eventArgs.target.result) {
                        retval = this.buildQplosFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifFromDataBaseQuery of BaseQPlosManager', ex);
            }

            return retval;
        },

        buildQplosFromDbResultObject: function (dbResult) {
            try {
                var retval = Ext.create('AssetManagement.customer.model.bo.QPlos', {
                    prueflos: dbResult['PRUEFLOS'],
                    kTextlos: dbResult['KTEXTLOS'],
                    aufnr: dbResult['AUFNR'],
                    qmnum: dbResult['QMNUM'],
                    aufpl: dbResult['AUFPL'],
                    stat35: dbResult['STAT35'],
                    qpart: dbResult['QPART'],
                    matnr: dbResult['MATNR'],
                    werks: dbResult['WERKS'],
                    plnty: dbResult['PLNTY'],
                    plnnr: dbResult['PLNNR'],
                    plnal: dbResult['PLNAL'],
                    zaehl: dbResult['ZAEHL'],
                    completed: dbResult['COMPLETED'],
                    mobileKey: dbResult['MOBILEKEY'],
                    mobileKey_ref: dbResult['MOBILEKEY_REF'],
                    equnr: dbResult['EQUNR'],
                    tplnr: dbResult['TPLNR'],
                    werk: dbResult['WERK'],

                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['PRUEFLOS']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQplosFromDbResultObject of BaseQPlosManager', ex);
            }

            return retval;
        },

        loadDependendDataForQPlos: function (eventIdToFireWhenComplete, qplos) {
            try {

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                if (!qplos) {
                    eventController.requestEventFiring(eventIdToFireWhenComplete, null);
                    return;
                }

                var prueflos = qplos.get('prueflos');

                var qplosOperStore = null;

                var techObj = null; //from qplosPoin

                var counter = 0;

                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignDependendDataForQPlos.call(me, eventIdToFireWhenComplete, qplos, qplosOperStore, techObj);
                    }
                };

                //get qplosOper
                if (true) {
                    done++;

                    var qplosOperSuccessCallback = function (qplosOperReceived) {
                        erroroccurred = qplosOperReceived === undefined;

                        qplosOperStore = qplosOperReceived;
                        counter++;

                        completeFunction();
                    };
                    var eventId = AssetManagement.customer.manager.QPlosOperManager.getOpersForQPlos(prueflos, true);
                    eventController.registerOnEventForOneTime(eventId, qplosOperSuccessCallback);
                }

                //get techObj from qplosPoin
                if (true) {
                    done++;

                    var techObjSuccessCallback = function (techObjReceived) {
                        erroroccurred = techObjReceived === undefined;

                        techObj = techObjReceived

                        counter++;

                        completeFunction();
                    };
                    var eventId = this.loadTechObjectForQplos(prueflos, true);
                    eventController.registerOnEventForOneTime(eventId, techObjSuccessCallback);
                }

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForQPlos of BaseQPlosManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        assignDependendDataForQPlos: function (eventIdToFireWhenComplete, qplos, qplosOperStore, techObj) {
            try {
                qplos.set('techObj', techObj);
                qplos.set('qplosOpers', qplosOperStore);

                var cache = this.getCache();
                cache.addToCache(qplos);

                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, qplos);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForQplos of BaseQPlosManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },

        getKeysOfAllQplosForOrder: function (aufnr, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }

                var keysTable = [];

                var counter = 0;
                var errorOccurred = false;

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        var keys = me.getQplosKeysForLoading.call(me, eventArgs);
                        if (keys !== null)
                            keysTable.push(keys);


                        if (done) {
                            AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, keysTable);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeysOfAllQplosForOrder of BaseQPlosManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('AUFNR', aufnr);

                var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('C_QPLOS', 'AUFNR', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('C_QPLOS', 'AUFNR', indexRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeysOfAllQplosForOrder of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        getQplosKeysForLoading: function (eventArgs) {
            var retval = null;

            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        retval = { prueflos: cursor.value['PRUEFLOS'], qmnum: cursor.value['QMNUM'] };
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQplosKeysForLoading of BaseQPlosManager', ex);
                var retval = null;
            }

            return retval;
        },

        loadTechObjectForQplos: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
                var me = this;

                var techObjNumbersCallback = function (eventArgs) {
                    try {
                        me.loadTechObj.call(me, eventArgs, retval);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTechObjectForQPlos of BaseQPlosManager', ex);
                    }
                };

                var indexMap = Ext.create('Ext.util.HashMap');
                indexMap.add('PRUEFLOS', prueflos);
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS_POIN', indexMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLOS_POIN', keyRange, techObjNumbersCallback, null, useBatchProcessing);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTechObjectForQPlos of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        loadTechObj: function (eventArgs, retval) {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var techObj = null;

                if (eventArgs) {
                    if (eventArgs.target.result) {
                        var dbResult = eventArgs.target.result.value
                    }
                }
                if (!dbResult) {
                    eventController.requestEventFiring(retval, null);
                    return retval;
                }
                var equnr = dbResult['EQUNR'];
                var tplnr = dbResult['TPLNR'];

                var counter = 0;
                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, techObj);
                    }
                };

                if (equnr) {
                    done++;

                    var equipmentSuccessCallback = function (equipReceived) {
                        erroroccurred = equipReceived === undefined;

                        techObj = equipReceived;
                        counter++;

                        completeFunction();
                    };
                    var eventId = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
                    eventController.registerOnEventForOneTime(eventId, equipmentSuccessCallback);
                }
                else if (tplnr) {
                    done++;

                    var funcLocSuccessCallback = function (funcLocReceived) {
                        erroroccurred = funcLocReceived === undefined;

                        techObj = funcLocReceived;

                        counter++;

                        completeFunction();
                    };
                    var eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
                    eventController.registerOnEventForOneTime(eventId, funcLocSuccessCallback);
                }

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadTechObj of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        },

        createQPlos: function (qplan, techObj, matnr, parent, qmnum, nextCounterValue) {
            var retval = null;

            try {
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var werks = userInfo.get('orderPlantWrkc');
                werks = werks ? werks : userInfo.get('orderPlanPlant');

                var parentClassName = Ext.getClassName(parent);

                var aufnr = '';
                var qmnum = qmnum;

                var qpart = '';

                if (parentClassName === 'AssetManagement.customer.model.bo.Order') {
                    aufnr = parent.get('aufnr');
                    qpart = parent.get('orderType').get('qpart');
                }

                var techObjClassName = Ext.getClassName(techObj);

                var equnr = '';
                var tplnr = '';

                if (techObjClassName === 'AssetManagement.customer.model.bo.Equipment') {
                    equnr = techObj.get('equnr');
                } else if (techObjClassName === 'AssetManagement.customer.model.bo.FuncLoc') {
                    tplnr = techObj.get('tplnr');
                }

                var prueflos = 'MO' + AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue, '0', 10);

                var newMobileKey = AssetManagement.customer.manager.KeyManager.createKey([prueflos]);

                var qplos = Ext.create('AssetManagement.customer.model.bo.QPlos', {
                    mobileKey: newMobileKey,
                    mobileKey_ref: parent.get('mobileKey'),
                    prueflos: prueflos,
                    aufnr: aufnr,
                    qmnum: qmnum,
                    aufpl: '',
                    qpart: qpart,
                    matnr: matnr,
                    plnty: qplan.get('plnty'),
                    plnnr: qplan.get('plnnr'),
                    plnal: qplan.get('plnal'),
                    zaehl: qplan.get('zaehl'),
                    werks: werks,
                    werk: werks,
                    kTextlos: qplan.get('ktext'),
                    techObj: techObj,
                    equnr: equnr,
                    tplnr: tplnr
                });

                var qplosOpers = AssetManagement.customer.manager.QPlosOperManager.createQPlosOpers(qplan, qplos);

                if (qplosOpers) {
                    qplos.set('qplosOpers', qplosOpers);
                    retval = qplos;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createQPlosObjectFromQPlanData of BaseQPlosManager', ex);
            }

            return retval;
        },

        buildDataBaseObjectFromQPlos: function (qplos) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['PRUEFLOS'] = qplos.get('prueflos');
                retval['QMNUM'] = qplos.get('qmnum');
                retval['AUFPL'] = qplos.get('aufpl');
                retval['AUFNR'] = qplos.get('aufnr');
                retval['WERK'] = qplos.get('werk');
                retval['STAT35'] = qplos.get('stat35');
                retval['QPART'] = qplos.get('qpart');
                retval['MATNR'] = qplos.get('matnr');
                retval['WERKS'] = qplos.get('werks');
                retval['PLNTY'] = qplos.get('plnty');
                retval['PLNNR'] = qplos.get('plnnr');
                retval['PLNAL'] = qplos.get('plnal');
                retval['KTEXTLOS'] = qplos.get('kTextlos');
                retval['ZAEHL'] = qplos.get('zaehl');
                retval['COMPLETED'] = qplos.get('completed');
                retval['MOBILEKEY'] = qplos.get('mobileKey');
                retval['MOBILEKEY_REF'] = qplos.get('mobileKey_ref');
                retval['UPDFLAG'] = qplos.get('updFlag');
                retval['EQUNR'] = qplos.get('equnr');
                retval['TPLNR'] = qplos.get('tplnr');
                retval['WERK'] = qplos.get('werk');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectFromQPlos of BaseQPlosManager', ex);
                retval = null;
            }

            return retval;
        },

        createAndSaveQPlosPoin: function (qplos) {
            var retval = -1;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var techObj = qplos.get('techObj');
                var techObjClassName = Ext.getClassName(techObj);
                var equnr = '';
                var tplnr = '';

                if (techObjClassName === 'AssetManagement.customer.model.bo.Equipment') {
                    equnr = techObj.get('equnr');
                } else {
                    tplnr = techObj.get('tplnr')
                }

                var toSave = {};
                toSave['MANDT'] = ac.getMandt();
                toSave['USERID'] = ac.getUserId();

                toSave['UPDFLAG'] = qplos.get('updFlag');
                toSave['PRUEFLOS'] = qplos.get('prueflos');
                toSave['VORGLFNR'] = '';
                toSave['PROBENR'] = '';
                toSave['MOBILEKEY'] = qplos.get('mobileKey')

                toSave['EQUNR'] = equnr;
                toSave['TPLNR'] = tplnr;
            
                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";
                        eventController.requestEventFiring(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createAndSaveQPlosPoin of BaseQPlosCharManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                AssetManagement.customer.core.Core.getDataBaseHelper().put('C_QPLOS_POIN', toSave, callback, callback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createAndSaveQPlosPoin of BaseQPlosManager', ex);
                retval = -1;
            }

            return retval;
        }
    }    
});
