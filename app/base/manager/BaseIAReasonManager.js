Ext.define('AssetManagement.base.manager.BaseIAReasonManager', {
    extend: 'AssetManagement.base.manager.BaseOxBaseManager',
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.IAReason',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store'
    ],

    inheritableStatics: {

        //public methods
        getAllIAReason: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.IAReason',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildIAReasonStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllIAReason of BaseIAReasonManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_ZIA_REASON', null);
                 AssetManagement.customer.core.Core.getDataBaseHelper().query('C_ZIA_REASON', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllIAReason of BaseIAReasonManager', ex);
                retval = -1;
            }

            return retval;
        },

        buildIAReasonStoreFromDataBaseQuery: function(store, eventArgs) {
            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var IAReason = this.buildIAReasonFromDbResultObject(cursor.value);
                        store.add(IAReason);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildIAReasonStoreFromDataBaseQuery of BaseIAReasonManager', ex);
            }
        },

        buildIAReasonFromDataBaseQuery: function( eventArgs) {
            var retval = null;
            try {
                if(eventArgs) {
                    if(eventArgs.target.result) {
                        retval = this.buildIAReasonFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildIAReasonStoreFromDataBaseQuery of BaseIAReasonManager', ex);
            }
            return retval;
        },

        //builds a single material conf. for a database record
        buildIAReasonFromDbResultObject: function(dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.MiGrund', {
                  grund:dbResult['GRUND'],
                  spras:dbResult['SPRAS'],
                  text: dbResult['TEXT']
                });
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildIAReasonFromDbResultObject of BaseIAReasonManager', ex);
            }
            return retval;
        }
    }
});