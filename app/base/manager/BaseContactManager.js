Ext.define('AssetManagement.base.manager.BaseContactManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.PartnerAP',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//public methods
		getContacts: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(this._cacheComplete === true) {
					eventController.requestEventFiring(retval, this.getCache());
					
					return retval;
				}
				
				var toFill = this.getCache();
				toFill.removeAll();
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildContactStoreFromDataBaseQuery.call(me, toFill, eventArgs);
						
						if(done) {
							me._cacheComplete = true;
							
							eventController.fireEvent(retval, toFill);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getContacts of BaseContactManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_PARTNER_AP', null);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_PARTNER_AP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getContacts of BaseContactManager', ex);
			}
	
			return retval;
		},
		
		getAllContactsForCustomer: function(kunnr, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(kunnr)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				if(this._cacheComplete === true) {
					var fromCache = this.extractFromCache(kunnr);
					eventController.requestEventFiring(retval, fromCache);
					
					return retval;
				}
				
				var theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.PartnerAP',
					autoLoad: false
				});
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildContactStoreFromDataBaseQuery.call(me, theStore, eventArgs);
						
						if(done) {
							eventController.fireEvent(retval, theStore);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllPartnersForObject of BaseContactManager', ex);
						eventController.fireEvent(retval, undefined);
					}
				};

				var indexMap = Ext.create('Ext.util.HashMap');
				indexMap.add('KUNNR', kunnr);
				
				var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_PARTNER_AP', 'KUNNR', indexMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_PARTNER_AP', 'KUNNR', indexRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllPartnersForObject of BaseContactManager', ex);
			}
	
			return retval;
		},
		
		//private
		buildContactFromDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildContactFromDbResultObject(eventArgs.target.result.value);						
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContactFromDataBaseQuery of BaseContactManager', ex);
			}
			return retval;
		},
		
		buildContactStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var contact = this.buildContactFromDbResultObject(cursor.value);
						store.add(contact);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContactStoreFromDataBaseQuery of BaseContactManager', ex);
			}
		},
		
		buildContactFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
		        retval = Ext.create('AssetManagement.customer.model.bo.PartnerAP', {
		        	parnr: dbResult['PARNR'],
				    kunnr: dbResult['KUNNR'],
				    lastname: dbResult['LASTNAME'],
				    firstname: dbResult['FIRSTNAME'],
				    titlep: dbResult['TITLE_P'],
				    city: dbResult['CITY'],
				    postlcod1: dbResult['POSTL_COD1'],
				    street: dbResult['STREET'],
				    telnumbermob: dbResult['TEL_NUMBER_MOB'],
				    tel1numbr: dbResult['TEL1_NUMBR'],
				    faxnumber: dbResult['FAX_NUMBER'],
				    functionVal: dbResult['FUNCTION'],
				    email: dbResult['E_MAIL'],
				    telextens: dbResult['TEL_EXTENS'],
				    faxextens: dbResult['FAX_EXTENS'],

			        updFlag: dbResult['UPDFLAG']
		        });
			
			    retval.set('id', dbResult['PARNR']);
						
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContactFromDbResultObject of BaseContactManager', ex);
			}
			return retval;
		},
		
		//cache administration
		_partnersCache: null,
		_cacheComplete: false,
	
		getCache: function() {	       
	        try {
	        	if(!this._contactCache) {
					this._contactCache = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.PartnerAP',
						autoLoad: false
					});
					
					this.initializeCacheHandling();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseContactManager', ex);
			}
			return this._contactCache;
		},
	
		clearCache: function() {
	        try {
			    this.getCache().removeAll();
			    this._cacheComplete = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCache of BaseContactManager', ex);
			}    
		},
		
		initializeCacheHandling: function() {
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.GLOBAL_CACHE_RESET, this.clearCache, this);
				eventController.registerOnEvent(AssetManagement.customer.manager.CacheManager.EVENTS.MOV_DATA_CACHE_RESET, this.clearCache, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeCacheHandling of BaseContactManager', ex);
			} 
		},
		
		extractFromCache: function(kunnr) {
			var retval = Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.PartnerAP',
				autoLoad: false
			});
			
			try {
				this.getCache().each(function(contact) {
					if(contact.get('kunnr') === kunnr) {
						retval.add(contact);
					}
				}, this);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractFromCache of BaseContactManager', ex);
			}
				
			return retval;
		}
	}
});