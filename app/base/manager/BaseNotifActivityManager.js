Ext.define('AssetManagement.base.manager.BaseNotifActivityManager', {
	extend: 'AssetManagement.customer.manager.OxBaseManager',
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.NotifActivityCache',
        'AssetManagement.customer.model.bo.NotifActivity',
        'AssetManagement.customer.manager.CustCodeManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.helper.StoreHelper',
        'Ext.data.Store',
        'AssetManagement.customer.manager.LongtextManager'
    ],
    
    inheritableStatics: {
		EVENTS: {
			ACTIVITY_ADDED: 'notifActivityAdded',
			ACTIVITY_CHANGED: 'notifActivityChanged',
			ACTIVITY_DELETED: 'notifActivityDeleted'
		},
		
		//protected
		getCache: function() {
			var retval = null;
		
			try {
				retval = AssetManagement.customer.manager.cache.NotifActivityCache.getInstance();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseNotifActivityManager', ex);
			}
			
			return retval;
		},
    
		//public methods		
		getNotifActivities: function(withDependendData, useBatchProcessing) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var cache = this.getCache();
				var requestedDataGrade = withDependendData ? cache.self.DATAGRADES.LOW : cache.self.DATAGRADES.BASE;
				var fromCache = cache.getStoreForAll(requestedDataGrade);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				//if with dependend data is requested, check, if the cache may deliver the base store
				//if so, pull all instances from cache, with a too low data grade
				if(withDependendData === true && cache.getStoreForAll(cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
					this.loadListDependendDataForNotifActivities(retval, baseStore);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
								
							me.buildNotifActivitiesStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if (done) {
								cache.addStoreForAllToCache(baseStore, cache.self.DATAGRADES.BASE);
								
								if(withDependendData) {
									//before proceeding pull all instances from cache, with a too low data grade
									//else the wrong instances would be filled with data
									baseStore = cache.getAllUpToGrade(cache.self.DATAGRADES.BASE);
									me.loadListDependendDataForNotifActivities.call(me, retval, baseStore);
								} else {
									//return the store from cache to eliminate duplicate objects
									var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.BASE);
									eventController.requestEventFiring(retval, toReturn);
								}
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivities of BaseNotifActivityManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
						
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIACTIVITY', null);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIACTIVITY', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivities of BaseNotifActivityManager', ex);
			}
			
			return retval;
		},
		
		getNotifActivitiesForNotif: function(qmnum, useBatchProcessing) {
			var retval = -1;
	
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
					this.loadDependendDataForNotifActivities(baseStore, retval, true);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
					
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifActivitiesStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotif(qmnum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotif(qmnum, cache.self.DATAGRADES.LOW);
								me.loadDependendDataForNotifActivities.call(me, baseStore, retval, true);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivitiesForNotif of BaseNotifActivityManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
		
					var keyMap = Ext.create('Ext.util.HashMap');
					keyMap.add('QMNUM', qmnum);
					
					var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIACTIVITY', keyMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIACTIVITY', keyRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivitiesForNotif of BaseNotifActivityManager', ex);
			}
				
			return retval;
		},
		
		getNotifActivitiesForNotifHeadOnly: function(qmnum, useBatchProcessing) {
	        var retval = null;

		    try {
			    retval = this.getNotifActivitiesForNotifItem(qmnum, '0000', useBatchProcessing);
		    } catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivitiesForNotif of BaseNotifActivityManager', ex);
		    }

		    return retval;
		},

		getNotifActivitiesForNotifItem: function(qmnum, fenum, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();		
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}

				var cache = this.getCache();
				var fromCache = cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.MEDIUM);
				
				if(fromCache) {
					eventController.requestEventFiring(retval, fromCache);
					return retval;
				}
				
				var baseStore = null;
				
				if(cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.BASE)) {
					baseStore = cache.getAllUpToGradeForNotifItem(qmnum, fenum, cache.self.DATAGRADES.LOW);
					this.loadDependendDataForNotifActivities(baseStore, retval);
				} else {
					baseStore = Ext.create('Ext.data.Store', {
						model: 'AssetManagement.customer.model.bo.NotifActivity',
						autoLoad: false
					});
				
					var me = this;
					var successCallback = function(eventArgs) {
						try {
							var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
							
							me.buildNotifActivitiesStoreFromDataBaseQuery.call(me, baseStore, eventArgs);
							
							if(done) {
								cache.addToCacheForNotifItem(qmnum, fenum, baseStore, cache.self.DATAGRADES.BASE);
								
								//before proceeding pull all instances from cache, with a too low data grade
								//else the wrong instances would be filled with data
								baseStore = cache.getAllUpToGradeForNotifItem(qmnum, fenum, cache.self.DATAGRADES.LOW);
								me.loadDependendDataForNotifActivities.call(me, baseStore, retval);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivitiesForNotifItem of BaseNotifActivityManager', ex);
							eventController.requestEventFiring(retval, undefined);
						}
					};
					
					var indexMap = Ext.create('Ext.util.HashMap');
					indexMap.add('QMNUM', qmnum);
					indexMap.add('FENUM', fenum);
					
					var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange('D_NOTIACTIVITY', 'QMNUM-FENUM', indexMap);
					AssetManagement.customer.core.Core.getDataBaseHelper().queryUsingAnIndex('D_NOTIACTIVITY', 'QMNUM-FENUM', indexRange, successCallback, null, useBatchProcessing);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotifActivitiesForNotifItem of BaseNotifActivityManager', ex);
			}
			
			return retval;
		},
    
		//save a notifActivity - if it is neccessary it will get a new counter first
		saveNotifActivity: function(notifActivity) {
			var retval = -1;
		
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifActivity) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var priorUpdateFlag = notifActivity.get('updFlag');
				var isInsert = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(priorUpdateFlag) || 'I' === priorUpdateFlag || 'A' === priorUpdateFlag;
				
				if(isInsert && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifActivity.get('fenum')))
					notifActivity.set('fenum', '0000');

				//update flag management
				this.manageUpdateFlagForSaving(notifActivity);
				
				//check if the notifActivity already has a counter value
				var requiresNewCounterValue = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifActivity.get('manum'));
				
				var me = this;
				var saveFunction = function(nextCounterValue) {
					try {
						if(requiresNewCounterValue && nextCounterValue !== -1)
							notifActivity.set('manum', nextCounterValue);
							
						if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifActivity.get('manum'))) {
							eventController.requestEventFiring(retval, false);
							return;
						}

						if(isInsert){
							var key = AssetManagement.customer.manager.KeyManager.createKey([ notifActivity.get('qmnum'), notifActivity.get('fenum'), notifActivity.get('manum') ]);
                            if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifActivity.get('childKey'))) {
                                notifActivity.set('childKey', key);
                            } else {
                                notifActivity.set('childKey2', key);
                            }
						}

						//set the id
						notifActivity.set('id', notifActivity.get('qmnum') + notifActivity.get('manum'));
	
						var callback = function(eventArgs) {
							try {
								var success = eventArgs.type === "success";
								
								if(success) {
									if(isInsert === true) {
										//do not specify a datagrade, so all depenend data will loaded, when necessary
										me.getCache().addToCache(notifActivity);
									}
								
									var eventType = isInsert ? me.EVENTS.ACTIVITY_ADDED : me.EVENTS.ACTIVITY_CHANGED;
									eventController.requestEventFiring(eventType, notifActivity);
								}
								
								eventController.requestEventFiring(retval, success);
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifActivity of BaseNotifActivityManager', ex);
								eventController.requestEventFiring(retval, false);
							}
						};
						
						var toSave = me.buildDataBaseObjectForNotifActivity(notifActivity);						

						//if it is an insert perform an insert
						if(isInsert) {
							AssetManagement.customer.core.Core.getDataBaseHelper().put('D_NOTIACTIVITY', toSave, callback, callback);
						} else {
							//else it is an update
							AssetManagement.customer.core.Core.getDataBaseHelper().update('D_NOTIACTIVITY', null, toSave, callback, callback);
						}
					} catch(ex) {
						eventController.requestEventFiring(retval, false);
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifActivity of BaseNotifActivityManager', ex);
					}
				};
				
				if(!requiresNewCounterValue) {
					saveFunction();
				} else {
					eventId = this.getNextManum(notifActivity);
					
					if(eventId > 1)
						eventController.registerOnEventForOneTime(eventId, saveFunction);
					else
						eventController.requestEventFiring(retval, false);
				}
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifActivity of BaseNotifActivityManager', ex);
			}
			
			return retval;
		},
	
		deleteNotifActivity: function(notifActivity, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifActivity) {
					eventController.requestEventFiring(retval, false);
					return retval;
				}
				
				var me = this;
				
				var callback = function(eventArgs) {
				    try {
						var success = eventArgs.type === "success";
						
						if(success) {
							AssetManagement.customer.helper.StoreHelper.removeRecordFromItsStores(notifActivity);
							me.getCache().removeFromCache(notifActivity);
							
							me.deleteNotifActivitySubItems(notifActivity, retval);
						}
					
						eventController.requestEventFiring(retval, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifActivity of BaseNotifActivityManager', ex);
						eventController.requestEventFiring(retval, false);
					}
				};
				
				//get the key of the notifActivity to delete
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject('D_NOTIACTIVITY', notifActivity);
				AssetManagement.customer.core.Core.getDataBaseHelper().del3te('D_NOTIACTIVITY', keyRange, callback, callback, useBatchProcessing, this.requiresManualCompletion());
			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifActivity of BaseNotifActivityManager', ex);
			    retval = -1;
			}
			
			return retval;	
		},

        
		deleteNotifActivitySubItems: function ( notifActivity, eventIdToFireWhenComplete) {
		    try {
                if(!notifActivity) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, false);
					return;
				}
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();

				var completeFunction = function () {
                    eventController.requestEventFiring(me.EVENTS.ACTIVITY_DELETED, notifActivity);
				    eventController.requestEventFiring(eventIdToFireWhenComplete, true);
				};

                var localLongtext = notifActivity.get('localLongtext');
				if (localLongtext)
				{
                     eventId = AssetManagement.customer.manager.LongtextManager.deleteLocalLongtext(notifActivity, true);
                     eventController.registerOnEventForOneTime(eventId, completeFunction);
				}
				else {
                    eventController.requestEventFiring(me.EVENTS.ACTIVITY_DELETED, notifActivity);
				    eventController.requestEventFiring(eventIdToFireWhenComplete, true);
				}

			  } catch(ex) {
			        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotifActivitySubItems of BaseNotifActivityManager', ex);
			  }
		},
		//private
		buildNotifActivitiesStoreFromDataBaseQuery: function(store, eventArgs) {
			try {
				if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var notifActivity = this.buildNotifActivityFromDbResultObject(cursor.value);
						store.add(notifActivity);

				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifActivitiesStoreFromDataBaseQuery of BaseNotifActivityManager', ex);
			}
		},
		
		buildNotifActivityFromDbResultObject: function(dbResult) {
	        var retval = null;
	        
	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.NotifActivity', {
					qmnum: dbResult['QMNUM'],
					fenum: dbResult['FENUM'],
					manum: dbResult['MANUM'],
					bautl: dbResult['BAUTL'],
					matxt: dbResult['MATXT'],
					mncod: dbResult['MNCOD'],
					mngrp: dbResult['MNGRP'],
					mnkat: dbResult['MNKAT'],
					mnver: dbResult['MNVER'],
					parnr: dbResult['PARNR'],
					parvw: dbResult['PARVW'],
					urnum: dbResult['URNUM'],
					erlnam: dbResult['ERLNAM'],
					stsma: dbResult['STSMA'],
					qsmnum: dbResult['QSMNUM'],
                    kzmla: dbResult['KZMLA'],
			
					erldt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERLDAT'], dbResult['ERZEIT']),
					petdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['PSTER'], dbResult['PSTUR']),
					pstdt: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['PETER'], dbResult['PETUR']),
					erzeit: AssetManagement.customer.utils.DateTimeUtils.parseTime(dbResult['ERZEIT']),
			
				    updFlag: dbResult['UPDFLAG'],
					mobileKey: dbResult['MOBILEKEY'],
					childKey: dbResult['CHILDKEY'],
					childKey2: dbResult['CHILDKEY2']
				});
				
	        	retval.set('id', dbResult['QMNUM'] + dbResult['MANUM']);
			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildNotifActivityFromDbResultObject of BaseNotifActivityManager', ex);
			}
			
			return retval;
		},
		
		loadListDependendDataForNotifActivities: function(eventIdToFireWhenComplete, notifActivities) {
			try {
				//do not continue if there is no data
				if(notifActivities.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifActivities);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				//before the data can be assigned, the lists have to be loaded by all other managers
				//load these data flat!
				//register on the corresponding events
				var activityStatus = null;		//not implemented
				
				var counter = 0;
				
				//TO-DO make dependend of customizing parameters --- this definitely will increase performance
				
				var done = 0;
				var errorOccurred = false;
				var reported = false;
				
				var completeFunction = function() {
					if(errorOccurred === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done && errorOccurred === false) {
						me.assignListDependendDataForNotifActivities.call(me, eventIdToFireWhenComplete, notifActivities, activityStatus);
					}
				};
				
				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListDependendDataForNotifActivities of BaseNotifActivityManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//call this function, when all necessary data has been collected
		assignListDependendDataForNotifActivities: function(eventIdToFireWhenComplete, notifActivities, activityStatus) {
			try {
//				notifActivities.each(function(notifActivity, index, length) {
//					
//				});

				
				var cache = this.getCache();
				cache.addStoreForAllToCache(notifActivities, cache.self.DATAGRADES.LOW);
				
				//return the store from cache to eliminate duplicate objects
				var toReturn = cache.getStoreForAll(cache.self.DATAGRADES.LOW); 
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForNotifActivities of BaseNotifActivityManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		loadDependendDataForNotifActivities: function(notifActivities, eventIdToFireWhenComplete, allOfOneNotif) {
			try {
				//do not continue if there is no data
				if(notifActivities.getCount() === 0) {
					AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, notifActivities);
					return;
				}
				
				var me = this;
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				
				var custCodes = null;
				var counter = 0;				
				var done = 0;
				var errorOccured = false;
				var reported = false;
			
				var completeFunction = function() {
					if(errorOccured === true && reported === false) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
						reported = true;
					} else if(counter === done) {
						me.assignDependendDataForNotifActivities.call(me, eventIdToFireWhenComplete, notifActivities, custCodes, allOfOneNotif);
					}
				};
				
				//get custCodes
				done++;
				
				var custCodesSuccessCallback = function(cCodes) {
					errorOccured = cCodes === undefined;
					
					custCodes = cCodes;
					counter++;
					
					completeFunction();
				};
				
				eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
				eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);
				
				
				notifActivities.each(function(activity){
					//getLocalLongtext
					if(true) {
						done++;
						
						var localLongtextSuccessCallback = function(localLT, backendLT) {
							errorOccured = localLT === undefined || backendLT === undefined;
							activity.set('localLongtext', localLT);
							activity.set('backendLongtext', backendLT);
							counter++;
							
							completeFunction();
						};
						eventId = AssetManagement.customer.manager.LongtextManager.getLongtext(activity, true);
						eventController.registerOnEventForOneTime(eventId, localLongtextSuccessCallback);
					}
				}); 

				if(done > 0) {
					AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
				} else {
					completeFunction();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForNotifActivities of BaseNotifActivityManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		assignDependendDataForNotifActivities: function(eventIdToFireWhenComplete, notifActivities, custCodes, allOfOneNotif) {
			try {
				notifActivities.each(function(activity, index, length) {
					//assign codes
					if(custCodes) {
						var katalogArt = activity.get('mnkat');
						var codeGruppe = activity.get('mngrp');
						var code = activity.get('mncod');
						
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(code)) {
							var groupHashMapForCatalogue = custCodes.get(katalogArt);
							
							if(groupHashMapForCatalogue) {
								var codesGroupStore = groupHashMapForCatalogue.get(codeGruppe);
								
								if(codesGroupStore) {
									codesGroupStore.each(function(custCode) {
										if(custCode.get('code') === code) {
											activity.set('activityCustCode', custCode);
											return false;
										}
									}, this);
								}
							}
						}
					}
				});
				
				notifActivities.sort('manum', 'ASC');
				
				var cache = this.getCache();
				var qmnum = notifActivities.getAt('0').get('qmnum');
				var toReturn = null;
				
				if(allOfOneNotif) {
					cache.addToCacheForNotif(qmnum, notifActivities, cache.self.DATAGRADES.MEDIUM);
					toReturn = cache.getFromCacheForNotif(qmnum, cache.self.DATAGRADES.MEDIUM); 
				} else {
					var fenum = notifActivities.getAt('0').get('fenum');
					cache.addToCacheForNotifItem(qmnum, fenum, notifActivities, cache.self.DATAGRADES.MEDIUM);
					toReturn = cache.getFromCacheForNotifItem(qmnum, fenum, cache.self.DATAGRADES.MEDIUM); 
				}
				
				//return the store from cache to eliminate duplicate objects
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, toReturn);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForNotifActivities of BaseNotifActivityManager', ex);
				AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
			}
		},
		
		//builds the raw object to store in the database out of a notifActivity model
		buildDataBaseObjectForNotifActivity: function(notifActivity) {			
			var retval = { };
			
			try {
				retval['QMNUM'] = notifActivity.get('qmnum');
				retval['FENUM'] = notifActivity.get('fenum');
				retval['MANUM'] = notifActivity.get('manum');
				retval['MNKAT'] = notifActivity.get('mnkat');
				retval['MNGRP'] = notifActivity.get('mngrp');
				retval['MNCOD'] = notifActivity.get('mncod');
				retval['MNVER'] = notifActivity.get('mnver');
				retval['URNUM'] = notifActivity.get('urnum');
				retval['MATXT'] = notifActivity.get('matxt');
				retval['PARVW'] = notifActivity.get('parvw');
				retval['PARNR'] = notifActivity.get('parnr');
				retval['BAUTL'] = notifActivity.get('bautl');
				retval['ERLNAM'] = notifActivity.get('erlnam');
				retval['STSMA'] = notifActivity.get('stsma');
				retval['QSMNUM'] = notifActivity.get('manum');
				
				retval['PSTER'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notifActivity.get('pstdt'));
				retval['PETER'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notifActivity.get('petdt'));
				retval['PSTUR'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifActivity.get('pstdt'));
				retval['PETUR'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifActivity.get('petdt'));
				retval['ERLDAT'] = AssetManagement.customer.utils.DateTimeUtils.getDateString(notifActivity.get('erldt'));
				retval['ERLZEIT'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifActivity.get('erldt'));
				retval['ERZEIT'] = AssetManagement.customer.utils.DateTimeUtils.getTimeString(notifActivity.get('erzeit'));
				
				retval['MOBILEKEY'] = notifActivity.get('mobileKey');
				retval['CHILDKEY'] = notifActivity.get('childKey');
				retval['CHILDKEY2'] = notifActivity.get('childKey2');
				retval['UPDFLAG'] = notifActivity.get('updFlag');			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForNotifActivity of BaseNotifActivityManager', ex);
			}
			
			return retval;
		},
		
		getNextManum: function(notifActivity) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				if(!notifActivity) {
					eventController.requestEventFiring(retval, -1);
					return retval;
				}
				
				var maxManum = 0;
								
				var successCallback = function(eventArgs) {
					try {
						if(eventArgs && eventArgs.target && eventArgs.target.result) {
							var cursor = eventArgs.target.result;
							
                            var manumValue = cursor.value['MANUM'];

						    //check if the counter value is a local one
						    if (AssetManagement.customer.utils.StringUtils.startsWith(manumValue, '%')) {
                                //cut of the percent sign
							    manumValue = manumValue.substr(1);
                                var curManumValue = parseInt(manumValue);
							
							    if(!isNaN(curManumValue))
							    {
								    if(curManumValue > maxManum)
									    maxManum = curManumValue;
							    }
                            }

							AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
						} else {
                            var nextLocalCounterNumberValue = maxManum + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
						    eventController.requestEventFiring(retval, nextLocalCounterValue);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextManum of BaseNotifActivityManager', ex);
						eventController.requestEventFiring(retval, -1);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('QMNUM', notifActivity.get('qmnum'));
				
			    //drop a raw query to include records flagged for deletion
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_NOTIACTIVITY', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('D_NOTIACTIVITY', keyRange, successCallback, null);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextManum of BaseNotifActivityManager', ex);
			}
			
			return retval;			
		}
	}
});