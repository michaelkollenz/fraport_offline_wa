﻿Ext.define('AssetManagement.base.manager.BaseQPlosCharManager', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.data.Store',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.QPlosChar',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.manager.QPResultManager',
        'AssetManagement.customer.manager.CustAuswmManager',
        'AssetManagement.customer.manager.CustCodeManager',
        'AssetManagement.customer.manager.Cust_013Manager'
    ],
    
    inheritableStatics: {
        //public methods
        //returns chars for one specific qplos
        getQPlosChars: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var charStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlosChar',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildQPlosCharStoreFromDataBaseQuery.call(me, charStore, eventArgs);

                        if (done) {
                            me.loadDependendDataForQPlosChars.call(me, retval, charStore, prueflos);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosChars of BaseQPlosCharManager', ex);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PRUEFLOS', prueflos);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS_CHAR', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLOS_CHAR', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosChars of BaseQPlosCharManager', ex);
                retval = -1;
            }

            return retval;
        },

        createQPlosChars: function (qplanOper, qplosOper) {
            var retval = null;

            try {
                var prueflos = qplosOper.get('prueflos');
                var vornr = qplosOper.get('vornr');
                var qplanChars = qplanOper.get('qplanChars');

                retval = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlosChar'
                });

                if (qplanChars && qplanChars.getCount() > 0) {
                    qplanChars.each(function (qplanChar) {
                        var merknr = qplanChar.get('merknr');
                        var childKey = AssetManagement.customer.manager.KeyManager.createKey([prueflos, vornr, merknr]);

                        var qplosChar = Ext.create('AssetManagement.customer.model.bo.QPlosChar', {
                            prueflos: prueflos,
                            vornr: vornr,
                            merknr: merknr,
                            childKey: childKey,
                            kzeinstell: qplanChar.get('kzeinstell'),
                            steuerkz: qplanChar.get('steuerkz'),
                            kurztext: qplanChar.get('kurztext'),
                            katab1: qplanChar.get('katab1'),
                            katalgart1: qplanChar.get('katalgart1'),
                            auswmenge1: qplanChar.get('auswmenge1'),
                            auswmgwrk1: qplanChar.get('auswmgwrk1'),
                            katab2: qplanChar.get('katab2'),
                            katalgart2: qplanChar.get('katalgart2'),
                            auswmenge2: qplanChar.get('auswmenge2'),
                            auswmgwrk2: qplanChar.get('auswmgwrk2'),
                            sollwert: qplanChar.get('sollwert'),
                            toleranzob: qplanChar.get('toleranzob'),
                            toleranzun: qplanChar.get('toleranzun'),
                            masseinhsw: qplanChar.get('masseinhsw'),
                            stellen: qplanChar.get('stellen'),
                            char_type: qplanChar.get('char_type'),
                            vorglfnr: qplanChar.get('vorglfnr'),
                            zaehl: qplanChar.get('zaehl'),
                            codes: qplanChar.get('codes'),
                            codeMeanings: qplanChar.get('codeMeanings'),
                            category: qplanChar.get('category'),
                            plnty: qplanChar.get('plnty'),
                            plnnr: qplanChar.get('plnnr'),
                            plnkn: qplanChar.get('plnkn'),
                            verwmerkm: '',
                            mobileKey: qplosOper.get('mobileKey'),
                            childKey_oper: qplosOper.get('childKey')
                        });

                        retval.add(qplosChar);
                    });
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createQPlosChars of BaseQPlosCharManager', ex);
                retval = null;
            }

            return retval;
        },

        saveQPlosCharStore: function (qplosChars, completed) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qplosChars || qplosChars.getCount() === 0) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }

                var pendingSaves = 0;
                var errorOccurred = false;
                var errorReported = false;

                var successCallback = function (success) {
                    try {
                        errorOccurred = success === false;

                        if (errorOccured && !errorReported) {
                            eventController.requestEventFiring(retval, false);
                            errorReported = true;
                        } else if (!errorOccured) {

                            pendingSaves--;

                            if (pendingSaves === 0) {
                                eventController.requestEventFiring(retval, true);
                            }
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosCharStore of BaseQPlosCharManager', ex);
                    }
                };
                
                //begin saving the first item
                qplosChars.each(function (qplosChar) {
                    var eventId = this.saveQPlosChar(qplosChar, completed);

                    if (eventId > -1) {
                        pendingSaves++;
                        eventController.registerOnEventForOneTime(eventId, successCallback);
                    } else {
                        errorOccurred = true;
                        errorReported = true;
                        return false;
                    }
                }, this);

                if (errorOccured) {
                    eventController.requestEventFiring(retval, false);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQplosChars of BaseQPlosCharManager', ex);
            }

            return retval;
        },

        deleteQPlosChars: function (prueflos, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(prueflos)) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";
                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlosChars of BaseQPlosCharManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PRUEFLOS', prueflos);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLOS_CHAR', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().del3te('C_QPLOS_CHAR', keyRange, callback, callback, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteQPlosChars of BaseQPlosCharManager', ex);
            }

            return retval;
        },

        //private methods
        buildQPlosCharStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        var qplosChar = this.buildQPlosCharFromDbResultObject(cursor.value);
                        store.add(qplosChar);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlosCharStoreFromDataBaseQuery of BaseQPlosCharManager', ex);
            }
        },

        buildQPlosCharFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.QPlosChar', {
                    prueflos: dbResult['PRUEFLOS'],
                    vornr: dbResult['VORNR'],
                    vorglfnr: dbResult['VORGLFNR'],
                    merknr: dbResult['MERKNR'],
                    zaehl: dbResult['ZAEHL'],
                    kzeinstell: dbResult['KZEINSTELL'],
                    steuerkz: dbResult['STEUERKZ'],
                    kurztext: dbResult['KURZTEXT'],
                    katab1: dbResult['KATAB1'],
                    katalgart1: dbResult['KATALGART1'],
                    auswmenge1: dbResult['AUSWMENGE1'],
                    auswmenge2: dbResult['AUSWMENGE2'],
                    auswmgwrk1: dbResult['AUSWMGWRK1'],
                    auswmgwrk2: dbResult['AUSWMGWRK2'],
                    katab2: dbResult['KATAB2'],
                    katalgart2: dbResult['KATALGART2'],
                    sollwert: dbResult['SOLLWERT'],
                    toleranzob: AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(dbResult['PLAUSIOBEN'], true), //PLAUSIOBEN is represented as toleranzob in the model
                    toleranzun: AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(dbResult['PLAUSIUNTE'], true), //PLAUSIUNTE is represented as toleranzun in the model
                    char_type: dbResult['CHAR_TYPE'],
                    masseinhsw: dbResult['MASSEINHSW'],
                    formel1: dbResult['FORMEL1'],
                    dummy10: dbResult['DUMMY10'],
                    plnty: dbResult['PLNTY'],
                    plnnr: dbResult['PLNNR'],
                    plnkn: dbResult['PLNKN'],
                    plausiobni: dbResult['PLAUSIOBNI'],
                    plausiunni: dbResult['PLAUSIUNNI'],
                    mobileKey: dbResult['MOBILEKEY'],
                    childKey: dbResult['CHILDKEY'],
                    childKey_oper: dbResult['CHILDKEY_OPER'],
                    stellen: dbResult['STELLEN'],
                    verwmerkm: dbResult['VERWMERKM'],
                    dummy20: dbResult['DUMMY20'],
                    dummy40: dbResult['DUMMY40'],

                    updFlag: dbResult['UPDFLAG']
                });

                retval.set('id', dbResult['PRUEFLOS'] + dbResult['VORNR'] + dbResult['MERKNR']);    
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlosCharFromDbResultObject of BaseQPlosCharManager', ex);
                retval = null;
            }

            return retval;
        },

        loadDependendDataForQPlosChars: function (retval, charStore, prueflos) {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //do not continue if there is no data
                if (charStore.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                    return false;
                }

                var counter = 0;

                var done = 0;
                var prueflos = prueflos;
                var custCodes = null;
                var custAuswms = null;
                var cust013Store = null;
                var qpResultStore = null;

                var counter = 0;

                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignListDependendDataForQPlosChar.call(me, retval, charStore, qpResultStore, custCodes, custAuswms, cust013Store);
                    }
                };

                if (true) {
                    done++;
                    //get qpresults
                    var qpresultSuccessCallback = function (qpresultReceived) {
                        errorOccured = qpresultReceived === undefined;

                        qpResultStore = qpresultReceived;
                        counter++;

                        completeFunction();
                    };

                    eventId = AssetManagement.customer.manager.QPResultManager.getQPResultsForQplos(prueflos, true);
                    eventController.registerOnEventForOneTime(eventId, qpresultSuccessCallback);
                }

                //get custCodes
                done++;

                var custCodesSuccessCallback = function (cCodes) {
                    errorOccured = cCodes === undefined;

                    custCodes = cCodes;
                    counter++;

                    completeFunction();
                };

                eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
                eventController.registerOnEventForOneTime(eventId, custCodesSuccessCallback);

                //get custAuswm
                done++;

                var custAuswmSuccessCallback = function (custAuswmReceived) {
                    errorOccurred = custAuswmReceived === undefined;

                    custAuswms = custAuswmReceived;
                    counter++;

                    completeFunction();
                };

                var eventId = AssetManagement.customer.manager.CustAuswmManager.getCustAuswms(true);
                eventController.registerOnEventForOneTime(eventId, custAuswmSuccessCallback);

                done++;

                //get cust013 table
                var cust013SuccessCallback = function (ccust013) {
                    errorOccured = ccust013 === undefined;

                    cust013Store = ccust013;
                    counter++;

                    completeFunction();
                };

                eventId = AssetManagement.customer.manager.Cust_013Manager.getCust_013s(true);
                eventController.registerOnEventForOneTime(eventId, cust013SuccessCallback);

                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForQPlosChars of BaseQPlosCharManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
            }
        },

        assignListDependendDataForQPlosChar: function (retval, charStore, qpResultStore, custCodes, custAuswms, cust013Store) {
            try {
                if (charStore && charStore.getCount() > 0) {
                    charStore.each(function (qplosChar) {
                        //results
                        if (qpResultStore && qpResultStore.getCount() > 0) {
                            qpResultStore.each(function (qpResult) {
                                if ((qpResult.get('inspchar') === qplosChar.get('merknr')) && (qpResult.get('inspoper') === qplosChar.get('vornr'))) {
                                    qplosChar.set('qpResult', qpResult);
                                    return false;
                                }
                            });
                        }
                        //codes
                        if (qplosChar.get('char_type') !== '01') { //char_type != 1 means a qualitative char, so the codes will be loaded
                            var katalgart1 = qplosChar.get('katalgart1');
                            var auswmgwrk1 = qplosChar.get('auswmgwrk1')
                            var auswmenge1 = qplosChar.get('auswmenge1')
                            if (auswmgwrk1 && auswmenge1 && katalgart1) {

                                var custCodesPerqqplosChar = Ext.create('Ext.data.Store', {
                                    model: 'AssetManagement.customer.model.bo.CustCode',
                                    autoLoad: false
                                });

                                var codesMeaningsMappingStore = Ext.create('Ext.data.Store', {
                                    model: 'AssetManagement.customer.model.bo.Cust_013',
                                    autoLoad: false
                                });

                                var codesForKatalogMap = custCodes.get(katalgart1); //out of all the codes, get only the ones from required catalog

                                if (codesForKatalogMap) {
                                    if (custAuswms && custAuswms.getCount() > 0) {
                                        custAuswms.each(function (custAuswm) { //first, looking for a match of of the char parameters in custAuswm
                                            if (custAuswm.get('auswahlmge') === auswmenge1 &&
                                                    custAuswm.get('werks') === auswmgwrk1 &&
                                                        custAuswm.get('katalogart') === katalgart1) {

                                                var codesInGroup = codesForKatalogMap.get(custAuswm.get('codegruppe')); //if a match was found, get the codegroup from the match in custAuswm, 
                                                //and based on that codegroup, get the codes for that group
                                                if (codesInGroup && codesInGroup.getCount() > 0) {
                                                    codesInGroup.each(function (codeEntry) {                    //out of all the codes in the group, get only those codes from the match in custAuswm
                                                        if (codeEntry.get('code') === custAuswm.get('code')) {
                                                            custCodesPerqqplosChar.add(codeEntry);
                                                            //now that the code entry has been found, a check if the codes exist in customizing table should be made
                                                            if (cust013Store && cust013Store.getCount() > 0) {
                                                                cust013Store.each(function (cust013entry) {
                                                                    if (cust013entry.get('auswahlmge') === qplosChar.get('auswmenge1') &&
                                                                        cust013entry.get('werks') === qplosChar.get('auswmgwrk1') &&
                                                                        cust013entry.get('katalogart') === qplosChar.get('katalgart1') &&
                                                                        cust013entry.get('code') === codeEntry.get('code') &&
                                                                        cust013entry.get('codegruppe') === codeEntry.get('codegruppe')) {

                                                                        codesMeaningsMappingStore.add(cust013entry);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }

                                qplosChar.set('codes', custCodesPerqqplosChar);
                                qplosChar.set('codeMeanings', codesMeaningsMappingStore);
                            }
                        }

                        this.determineCategory(qplosChar);
                    }, this);
                }

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, charStore);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignListDependendDataForQPlosChar of BaseQPlosCharManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
            }
        },

        determineCategory: function (qplosChar) {
            try {
                var qplosCharModelInstance = AssetManagement.customer.model.bo.QPlosChar;

                if (qplosChar.get('char_type') !== '01') { //char_type != 1 means a qualitative char, so the codes will be loaded
                    var codeMeanings = qplosChar.get('codeMeanings');
                    var requiredCodesMappingMatched = 0;
                    if (codeMeanings && codeMeanings.getCount() > 0) {
                        codeMeanings.each(function (codeMeaningEntry) {
                            if (codeMeaningEntry.get('funktion') === 'OK' || codeMeaningEntry.get('funktion') === 'NOK')
                                requiredCodesMappingMatched++;
                        });
                    }

                    if (requiredCodesMappingMatched === 2) {
                        qplosChar.set('category', qplosCharModelInstance.CATEGORIES.QUALITATIVE_CHECKBOX);
                    } else {
                        qplosChar.set('category', qplosCharModelInstance.CATEGORIES.QUALITATIVE_STANDARD);
                    }
                } else {
                    qplosChar.set('category', qplosCharModelInstance.CATEGORIES.QUANTITATIVE);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineCategory of BaseQPlosCharManager', ex);
            }
        },

        saveQPlosChar: function (qplosChar, completed) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!qplosChar) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                qplosChar.set('updFlag', completed ? 'I' : 'A');

                var callback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";

                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosChar of BaseQPlosCharManager', ex);
                        eventController.requestEventFiring(retval, false);
                    }
                };

                var toSave = this.buildDataBaseObjectFromQPlosChar(qplosChar);
                AssetManagement.customer.core.Core.getDataBaseHelper().put('C_QPLOS_CHAR', toSave, callback, callback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveQPlosChar of BaseQPlosCharManager', ex);
                retval = -1;
            }

            return retval;
        },        

        buildDataBaseObjectFromQPlosChar: function (qplosChar) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = {};
                retval['MANDT'] = ac.getMandt();
                retval['USERID'] = ac.getUserId();
                retval['UPDFLAG'] = qplosChar.get('updFlag');
                retval['PRUEFLOS'] = qplosChar.get('prueflos');
                retval['VORGLFNR'] = qplosChar.get('vorglfnr');
                retval['MERKNR'] = qplosChar.get('merknr');
                retval['ZAEHL'] = qplosChar.get('zaehl');
                retval['KZEINSTELL'] = qplosChar.get('kzeinstell');
                retval['STEUERKZ'] = qplosChar.get('steuerkz');
                retval['KURZTEXT'] = qplosChar.get('kurztext');
                retval['KATAB1'] = qplosChar.get('katab1');
                retval['KATALGART1'] = qplosChar.get('katalgart1');
                retval['AUSWMENGE1'] = qplosChar.get('auswmenge1');
                retval['AUSWMGWRK1'] = qplosChar.get('auswmgwrk1');
                retval['KATAB2'] = qplosChar.get('katab2');
                retval['KATALGART2'] = qplosChar.get('katalgart2');
                retval['AUSWMENGE2'] = qplosChar.get('auswmenge2');
                retval['AUSWMGWRK2'] = qplosChar.get('auswmgwrk2');
                retval['SOLLWERT'] = qplosChar.get('sollwert');
                retval['PLAUSIOBEN'] = AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(qplosChar.get('toleranzob'), false), //PLAUSIOBEN is represented as toleranzob in the model
                retval['PLAUSIUNTE'] = AssetManagement.customer.utils.NumberFormatUtils.convertNegativeNumber(qplosChar.get('toleranzun'), false), //PLAUSIUNTE is represented as toleranzun in the model
                retval['MASSEINHSW'] = qplosChar.get('masseinhsw');
                retval['CHAR_TYPE'] = qplosChar.get('char_type');
                retval['STELLEN'] = qplosChar.get('stellen');
                retval['FORMEL1'] = qplosChar.get('formel1');
                retval['DUMMY10'] = qplosChar.get('dummy10');
                retval['PLNTY'] = qplosChar.get('plnty');
                retval['PLNNR'] = qplosChar.get('plnnr');
                retval['PLNKN'] = qplosChar.get('plnkn');
                retval['VERWMERKM'] = qplosChar.get('verwmerkm');
                retval['PLAUSIOBNI'] = qplosChar.get('plausiobni');
                retval['PLAUSIUNNI'] = qplosChar.get('plausiunni');
                retval['VORNR'] = qplosChar.get('vornr');
                retval['MOBILEKEY'] = qplosChar.get('mobileKey');
                retval['CHILDKEY'] = qplosChar.get('childKey');
                retval['CHILDKEY_OPER'] = qplosChar.get('childKey_oper');
                retval['DUMMY20'] = qplosChar.get('dummy20');
                retval['DUMMY40'] = qplosChar.get('dummy40');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectFromQPlosChar of BaseQPlosCharManager', ex);
                retval = null;
            }

            return retval;
        }
    }
});