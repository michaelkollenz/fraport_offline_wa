Ext.define('AssetManagement.base.manager.BaseLongtextManager', {
    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.helper.DbKeyRangeHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        EVENTS: {
            LONGTEXT_ADDED: 'longtextAdded',
            LONGTEXT_CHANGED: 'longtextChanged',
            LONGTEXT_DELETED: 'longtextDeleted'
        },

        //public methods
        getLongtext: function (businessObject, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var localLongtext = '';
                var backendLongtext = '';

                var done = 2;

                var errorOccurred = false;
                var returned = false;

                var completeFunction = function () {
                    try {
                        if (errorOccurred === true && returned === false) {
                            eventController.requestEventFiring(retval, undefined, undefined);
                            returned = true;
                        } else if (done === 0 && errorOccurred === false) {
                            eventController.fireEvent(retval, localLongtext, backendLongtext);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtext of BaseLongtextManager', ex);
                        eventController.requestEventFiring(retval, undefined, undefined);
                    }
                };

                var backendLongtextCallback = function (backendLongtextString) {
                    try {
                        done--;
                        errorOccured = backendLongtextString === undefined;
                        backendLongtext = backendLongtextString;
                        completeFunction();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtext of BaseLongtextManager', ex);
                    }
                };

                var backendLongtextEventId = this.getBackendLongtext(businessObject, true);

                if (backendLongtextEventId > -1) {
                    eventController.registerOnEventForOneTime(backendLongtextEventId, backendLongtextCallback);
                } else {
                    errorOccurred = true;
                    completeFunction();
                }

                var localLongtextCallback = function (localLongtextString) {
                    try {
                        done--;
                        errorOccured = localLongtextString === undefined;
                        localLongtext = localLongtextString;
                        completeFunction();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtext of BaseLongtextManager', ex);
                    }
                };

                var localLongtextEventId = this.getLocalLongtext(businessObject, true);

                if (localLongtextEventId > -1) {
                    eventController.registerOnEventForOneTime(localLongtextEventId, localLongtextCallback);
                } else {
                    errorOccurred = true;
                    completeFunction();
                }

                if (!useBatchProcessing) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                }
            } catch(ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtext of BaseLongtextManager', ex);
            }

            return retval;
        },

        getInternNote: function (equipment, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!equipment) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var parameters = this.determineParameters(equipment);

                if (!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                //Change tdid for longtext to get InternNote
                parameters.tdId = 'INTV';

                var internNoteString = '';
                
                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        internNoteString = me.buildBackendLongtextFromDataBaseQuery.call(me, internNoteString, eventArgs);

                        if (done === true) {
                            eventController.fireEvent(retval, internNoteString);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInternNote of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', parameters.tdObject);
                keyMap.add('TDNAME', parameters.tdName);
                keyMap.add('TDID', parameters.tdId);
                keyMap.add('TDSPRAS', parameters.tdSpras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_LINE', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_LINE', keyRange, successCallback, null, useBatchProcessing);
                
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInternNote of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //public
        saveLocalLongtext: function (textToSave, objectToSaveFor, readyForPosting) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToSaveFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                //delete existing local longtext first - header and lines
                var deleteCallback = function (success) {
                    try {
                        if (success) {
                            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(textToSave)) {
                                eventController.fireEvent(retval, true);
                                return;
                            }

                            var saveCallback = function (saveSuccess) {
                                try {
                                    eventController.fireEvent(retval, saveSuccess === true);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtext of BaseLongtextManager', ex);
                                }
                            };

                            //save new longtext
                            var eventId = me.saveLocalLongtextInternal(textToSave, objectToSaveFor, readyForPosting);
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                        } else {
                            eventController.fireEvent(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtext of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var eventId = me.deleteLocalLongtext(objectToSaveFor);

                if (eventId > -1) {
                    eventController.registerOnEventForOneTime(eventId, deleteCallback);
                } else {
                    eventController.requestEventFiring(retval, false);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtext of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //public
        deleteLocalLongtext: function (objectToDeleteFor) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToDeleteFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                //first try to handle the longtext header
                var headerCallback = function (success) {
                    try {
                        success = success === true;

                        if (success) {
                            //header handled - next handle the lines
                            var linesCallback = function (linessuccess) {
                                try {
                                    linessuccess = linessuccess === true;

                                    if (linessuccess) {
                                        objectToDeleteFor.set('localLongtext', '');
                                    }

                                    eventController.fireEvent(retval, true);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteLocalLongtext of BaseLongtextManager', ex);
                                    eventController.fireEvent(retval, false);
                                }
                            };

                            var eventId = me.tryDeleteLongtextLines(objectToDeleteFor);
                            eventController.registerOnEventForOneTime(eventId, linesCallback);
                        } else {
                            eventController.fireEvent(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteLocalLongtext of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var eventId = me.tryDeleteLongtextHeader(objectToDeleteFor);

                if (eventId > -1) {
                    eventController.registerOnEventForOneTime(eventId, headerCallback);
                } else {
                    eventController.requestEventFiring(retval, false);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteLocalLongtext of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //public
        getStringFromLongtextStore: function (longtextStore) {
            var retval = '';

            try {
                var isFirst = true;

                if (longtextStore && longtextStore.getCount() > 0) {
                    longtextStore.each(function (longtextLine) {
                        var lineFormat = longtextLine.get('tdformat');

                        if (isFirst && (lineFormat === '*')) {
                            retval = longtextLine.get('tdline');
                        } else {
                            switch (lineFormat) {
                                case '*':
                                    retval += '\r\n' + longtextLine.get('tdline');
                                    break;
                                case '>X':
                                    retval += '\r\n' + longtextLine.get('tdline');
                                    break;
                                case '=':
                                    retval += longtextLine.get('tdline');
                                    break;
                                case '':
                                    retval += ' ' + longtextLine.get('tdline');
                                    break;
                                case '/':
                                    retval += '\r\n\t' + longtextLine.get('tdline');
                                    break;
                                case '/=':
                                    retval += '\t' + longtextLine.get('tdline');
                                    break;
                                default:
                                    retval += ' ' + longtextLine.get('tdline');
                                    break;
                            }
                        }

                        isFirst = false;
                    }, this);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStringFromLongtextStore of BaseLongtextManager', ex);
            }

            return retval;
        },

        //protected
        //@override
        //map fields of businessobjects
        determineParameters: function (businessObject) {
            var retval = null;

            try {
                var tdObject = '';
                var tdName = '';
                var tdId = '';
                var tdSpras = '';
                var mobileKey = '';
                var childKey = '';
                var childKey2 = '';
                var busObjectKey = '';

                var ac = AssetManagement.customer.core.Core.getAppConfig();
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var usersLanguage = userInfo.get('sprache').charAt(0);

                //ORDER
                if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.Order') {
                    tdObject = 'AUFK';
                    tdName = ac.getMandt() + businessObject.get('aufnr');
                    tdId = 'KOPF';
                    mobileKey = businessObject.get('mobileKey');
                    busObjectKey = businessObject.get('aufnr');
                    tdSpras = businessObject.get('ltext');
                }
                //ORDER OPERATION
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.Operation') {
                    tdObject = 'AUFK';
                    tdName = ac.getMandt() + businessObject.get('aufpl') + businessObject.get('aplzl');
                    tdId = 'AVOT';
                    mobileKey = businessObject.get('mobileKey');
                    childKey = businessObject.get('childKey');
                    busObjectKey = businessObject.get('aufnr');
                    tdSpras = businessObject.get('txtsp');
                }
                //NOTIF
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.Notif') {
                    tdObject = 'QMEL';
                    tdName = businessObject.get('qmnum');
                    tdId = 'LTXT';
                    mobileKey = businessObject.get('mobileKey');
                    busObjectKey = businessObject.get('qmnum');
                    tdSpras = businessObject.get('kzmla');
                }
                //NOTIF ITEM
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.NotifItem') {
                    tdObject = 'QMFE';
                    tdName = businessObject.get('qmnum') + businessObject.get('fenum');
                    tdId = 'LTXT';
                    mobileKey = businessObject.get('mobileKey');
                    childKey = businessObject.get('childKey');
                    busObjectKey = businessObject.get('qmnum');
                    tdSpras = businessObject.get('kzmla');
                }
                //NOTIF TASK
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.NotifTask') {
                    tdObject = "QMMA";
                    tdName = businessObject.get('qmnum') + businessObject.get('manum');
                    tdId = "LTXT";
                    mobileKey = businessObject.get('mobileKey');
                    childKey = businessObject.get('childKey');
                    childKey2 = businessObject.get('childKey2');
                    busObjectKey = businessObject.get('qmnum');
                    tdSpras = businessObject.get('kzmla');
                }
                //NOTIF ACTIVITY
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.NotifActivity') {
                    tdObject = "QMSM";
                    tdName = businessObject.get('qmnum') + businessObject.get('manum');
                    tdId = "LTXT";
                    mobileKey = businessObject.get('mobileKey');
                    childKey = businessObject.get('childKey');
                    childKey2 = businessObject.get('childKey2');
                    busObjectKey = businessObject.get('qmnum');
                    tdSpras = businessObject.get('kzmla');
                }
                //NOTIF CAUSE
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.NotifCause') {
                    tdObject = 'QMUR';
                    tdName = businessObject.get('qmnum') + businessObject.get('fenum') + businessObject.get('urnum');
                    tdId = 'LTXT';
                    mobileKey = businessObject.get('mobileKey');
                    childKey = businessObject.get('childKey');
                    childKey2 = businessObject.get('childKey2');
                    busObjectKey = businessObject.get('qmnum');
                    tdSpras = businessObject.get('kzmla');
                }
                //EQUI
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.Equipment') {
                    tdObject = 'EQUI';
                    tdName = businessObject.get('equnr');
                    tdId = 'LTXT';
                    mobileKey = businessObject.get('mobileKey');
                    busObjectKey = businessObject.get('equnr');
                    tdSpras = usersLanguage;
                }
                //FUNC LOC
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.FuncLoc') {
                    tdObject = 'IFLOT';
                    tdName = businessObject.get('tplnr');
                    tdId = 'LTXT';
                    mobileKey = businessObject.get('mobileKey');
                    busObjectKey = businessObject.get('tplnr');
                    tdSpras = usersLanguage;
                }
                //TIME CONF
                else if (Ext.getClassName(businessObject) === 'AssetManagement.customer.model.bo.TimeConf') {
                    tdObject = 'AUFK';
                    tdName = businessObject.get('aufnr') + businessObject.get('vornr') + businessObject.get('split') + businessObject.get('rmzhl');
                    tdId = 'RMEL';
                    mobileKey = businessObject.get('mobileKey');
                    tdSpras = usersLanguage;
                    childKey = businessObject.get('childKey');
                    childKey2 = businessObject.get('childKey2');
                    busObjectKey = businessObject.get('aufnr') + businessObject.get('vornr') + businessObject.get('split') + businessObject.get('rmzhl');
                }

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tdObject)) {
                    //the busobject key value (concatenated from different key fields) may contain any temporary ordering number
                    //backend can not handle any termporary numbers
                    //usually these numbers are converted during the synchroniazion - but this algorithm is only applied to key fields
                    //because the busobject key field is not a key field, this has to be done in advance here
                    //replace '%' character with a '0' character
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(busObjectKey) && busObjectKey.length > 0)
                        busObjectKey = busObjectKey.replace(/%/ig, '0');

                    retval = {
                        tdName: tdName,
                        tdObject: tdObject,
                        tdId: tdId,
                        tdSpras: tdSpras,
                        mobileKey: mobileKey,
                        childKey: childKey,
                        childKey2: childKey2,
                        busobjectkey: busObjectKey
                    };
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineParameters of BaseLongtextManager', ex);
            }

            return retval;
        },

        //private
        getBackendLongtext: function (businessObject, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!businessObject) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var parameters = this.determineParameters(businessObject);

                if (!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var sapPart = '';
                var convertedPart = '';

                var done = 2;

                var errorOccurred = false;
                var returned = false;

                var completeFunction = function () {
                    try {
                        if (errorOccurred === true && returned === false) {
                            eventController.requestEventFiring(retval, undefined);
                            returned = true;
                        } else if (done === 0 && errorOccurred === false) {
                            var concatenated = AssetManagement.customer.utils.StringUtils.concatenate([sapPart, convertedPart], '\n', true);
                            eventController.fireEvent(retval, concatenated);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtext of BaseLongtextManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var sapPartCallback = function (sapPartString) {
                    try {
                        done--;
                        errorOccured = sapPartString === undefined;
                        sapPart = sapPartString;
                        completeFunction();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtext of BaseLongtextManager', ex);
                    }
                };

                var sapPartEventId = this.getBackendLongtextSAPPart(parameters, true);

                if (sapPartEventId > -1) {
                    eventController.registerOnEventForOneTime(sapPartEventId, sapPartCallback);
                } else {
                    errorOccurred = true;
                    completeFunction();
                }

                var convertedPartCallback = function (convertedPartString) {
                    try {
                        done--;
                        errorOccured = convertedPartString === undefined;
                        convertedPart = convertedPartString;
                        completeFunction();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtext of BaseLongtextManager', ex);
                    }
                };

                var convertedPartEventId = this.getBackendLongtextConvertedPart(parameters, true);

                if (convertedPartEventId > -1) {
                    eventController.registerOnEventForOneTime(convertedPartEventId, convertedPartCallback);
                } else {
                    errorOccurred = true;
                    completeFunction();
                }

                if (!useBatchProcessing) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtext of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //reads the backend longtext part, which came from SAP
        getBackendLongtextSAPPart: function (parameters, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var sapPartString = '';

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        sapPartString = me.buildBackendLongtextFromDataBaseQuery.call(me, sapPartString, eventArgs);

                        if (done === true) {
                            eventController.fireEvent(retval, sapPartString);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtextSAPPart of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMapLower = Ext.create('Ext.util.HashMap');
                keyMapLower.add('TDOBJECT', parameters.tdObject);
                keyMapLower.add('TDNAME', parameters.tdName);
                keyMapLower.add('TDID', parameters.tdId);
                keyMapLower.add('TDSPRAS', parameters.tdSpras);
                keyMapLower.add('COUNTER', '0000');

                var keyMapUpper = Ext.create('Ext.util.HashMap');
                keyMapUpper.add('TDOBJECT', parameters.tdObject);
                keyMapUpper.add('TDNAME', parameters.tdName);
                keyMapUpper.add('TDID', parameters.tdId);
                keyMapUpper.add('TDSPRAS', parameters.tdSpras);
                keyMapUpper.add('COUNTER', '9999');

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_LINE', keyMapLower, keyMapUpper);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_LINE', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtextSAPPart of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //reads the backend longtext part, which was generated by sychronizing a local longtext
        getBackendLongtextConvertedPart: function (parameters, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var convertedPartString = '';

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        convertedPartString = me.buildBackendLongtextFromDataBaseQuery.call(me, convertedPartString, eventArgs);

                        if (done === true) {
                            eventController.fireEvent(retval, convertedPartString);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtextConvertedPart of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyMapLower = Ext.create('Ext.util.HashMap');
                keyMapLower.add('TDOBJECT', parameters.tdObject);
                keyMapLower.add('TDNAME', parameters.tdName);
                keyMapLower.add('TDID', parameters.tdId);
                keyMapLower.add('TDSPRAS', parameters.tdSpras);
                keyMapLower.add('COUNTER', '%000');

                var keyMapUpper = Ext.create('Ext.util.HashMap');
                keyMapUpper.add('TDOBJECT', parameters.tdObject);
                keyMapUpper.add('TDNAME', parameters.tdName);
                keyMapUpper.add('TDID', parameters.tdId);
                keyMapUpper.add('TDSPRAS', parameters.tdSpras);
                keyMapUpper.add('COUNTER', '%999');

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_LINE', keyMapLower, keyMapUpper);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_LINE', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLongtextConvertedPart of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        getLocalLongtext: function(businessObject, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!businessObject) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var parameters = this.determineParameters(businessObject);

                if(!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                var localLongtextString = '';

                var me = this;
                var successCallback = function(eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        localLongtextString = me.buildLocalLongtextFromDataBaseQuery.call(me, localLongtextString, eventArgs);

                        if (done === true) {
                            eventController.fireEvent(retval, localLongtextString);
                        }
                    } catch(ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLocalLongtext of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                //override language key to target local longtext lines
                //local longtext lines are always stored with user's language, even if the header comes from backend with a different language
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var tdSpras = userInfo.get('sprache').charAt(0);

                parameters.tdSpras = tdSpras;

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', parameters.tdObject);
                keyMap.add('TDNAME', parameters.tdName);
                keyMap.add('TDID', parameters.tdId);
                keyMap.add('TDSPRAS', parameters.tdSpras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_LINE', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_LINE', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLocalLongtext of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //will try to delete a longtext header for an object - this is only possible for local longtext headers
        //returns true, when the processing was successful
        tryDeleteLongtextHeader: function (objectToDeleteFor) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToDeleteFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var parameters = this.determineParameters(objectToDeleteFor);

                if(!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }

                //query database for the long text header
                var successCallback = function (eventArgs) {
                    try {
                        if (eventArgs && eventArgs.target && eventArgs.target.result && eventArgs.target.result.value) {
                            var cursor = eventArgs.target.result;
                            var headerObject = cursor.value;

                            //check update flag of the header
                            //a delete is only allowed, if it is local only
                            var updateFlag = headerObject['UPDFLAG'];

                            if (updateFlag === 'A' || updateFlag === 'I') {
                                AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            }

                            eventController.fireEvent(retval, true);
                        } else {
                            //no local header existing, so nothing to delete available
                            eventController.fireEvent(retval, true);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteLongtextHeader of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                //override language key to target local longtext header
                //any created (local) longtext header is always stored with user's language
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var tdSpras = userInfo.get('sprache').charAt(0);

                parameters.tdSpras = tdSpras;

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', parameters.tdObject);
                keyMap.add('TDNAME', parameters.tdName);
                keyMap.add('TDID', parameters.tdId);
                keyMap.add('TDSPRAS', parameters.tdSpras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_HEADER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_HEADER', keyRange, successCallback, null);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteLongtextHeader of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //will try to delete longtext lines for an object - this is only possible for local longtext lines
        //returns true, when the processing was successful
        tryDeleteLongtextLines: function (objectToDeleteFor) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToDeleteFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var parameters = this.determineParameters(objectToDeleteFor);

                if (!parameters) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                //query database for the long text lines
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        if (!done) {
                            var cursor = eventArgs.target.result;
                            var lineObject = cursor.value;

                            if (lineObject) {
                                //check update flag of the line
                                //a delete is only allowed, if it is local only
                                if (lineObject['UPDFLAG'] !== 'X') {
                                    AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                                }

                                AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                            } else {
                                //all lines handled - deletion done
                                eventController.fireEvent(retval, true);
                            }
                        } else {
                            //all lines handled - deletion done
                            eventController.fireEvent(retval, true);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteLongtextLines of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                //override language key to target local longtext lines
                //local longtext lines are always stored with user's language, even if the header comes from backend with a different language
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var tdSpras = userInfo.get('sprache').charAt(0);

                parameters.tdSpras = tdSpras;

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', parameters.tdObject);
                keyMap.add('TDNAME', parameters.tdName);
                keyMap.add('TDID', parameters.tdId);
                keyMap.add('TDSPRAS', parameters.tdSpras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_LINE', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_LINE', keyRange, successCallback, null);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteLongtextLines of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        saveLocalLongtextInternal: function (textToSave, objectToSaveFor, readyForPosting) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToSaveFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                //first try to handle the longtext header
                var headerCallback = function (success) {
                    try {
                        success = success === true;

                        if (success) {
                            //header saved - next save the lines
                            var linesCallback = function (linessuccess) {
                                try {
                                    linessuccess = linessuccess === true;

                                    if (linessuccess) {
                                        objectToSaveFor.set('localLongtext', textToSave);
                                    }

                                    eventController.fireEvent(retval, true);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextInternal of BaseLongtextManager', ex);
                                    eventController.fireEvent(retval, false);
                                }
                            };

                            var eventId = me.saveLocalLongtextLines(textToSave, objectToSaveFor, readyForPosting);

                            if (eventId > -1) {
                                eventController.registerOnEventForOneTime(eventId, linesCallback);
                            } else {
                                eventController.requestEventFiring(retval, false);
                            }
                        } else {
                            eventController.fireEvent(retval, false);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextInternal of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var eventId = me.saveLongtextHeader(objectToSaveFor, readyForPosting);

                if (eventId > -1) {
                    eventController.registerOnEventForOneTime(eventId, headerCallback);
                } else {
                    eventController.requestEventFiring(retval, false);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextInternal of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //will handle the long text header for saving a long text for an object
        saveLongtextHeader: function (objectToSaveFor, readyForPosting) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToSaveFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var parameters = this.determineParameters(objectToSaveFor);

                if (!parameters) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var me = this;

                //query database to check, if there already is a long text header
                var headerQueryCallback = function (eventArgs) {
                    try {
                        //check for header existence
                        var alreadyExisting = eventArgs && eventArgs.target && eventArgs.target.result && eventArgs.target.result.value;

                        //define the callback for the save/update request
                        var saveCallback = function (eventArgs) {
                            try {
                                var success = eventArgs && eventArgs.type === "success";
                                eventController.fireEvent(retval, success);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLongtextHeader of BaseLongtextManager', ex);
                                eventController.fireEvent(retval, false);
                            }
                        };

                        if (alreadyExisting) {
                            //header already existing - the update flag has to be adjusted
                            var record = eventArgs.target.result.value;
                            record['UPDFLAG'] = readyForPosting === true ? 'U' : 'C';

                            AssetManagement.customer.core.Core.getDataBaseHelper().update('D_LTXT_HEADER', null, record, saveCallback, saveCallback);
                        } else {
                            //header has to be created (local)

                            //use user's language for new longtext header
                            //any created (local) longtext header is always stored with user's language
                            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                            var tdSpras = userInfo.get('sprache').charAt(0);

                            parameters.tdSpras = tdSpras;

                            var toSave = me.buildDataBaseObjectForLongtextHeader(parameters.tdObject, parameters.tdName, parameters.tdId, parameters.tdSpras, parameters.mobileKey, parameters.childKey, parameters.childKey2, parameters.busobjectkey);
                            toSave['UPDFLAG'] = readyForPosting === true ? 'I' : 'A';

                            AssetManagement.customer.core.Core.getDataBaseHelper().put('D_LTXT_HEADER', toSave, saveCallback, saveCallback);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLongtextHeader of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', parameters.tdObject);
                keyMap.add('TDNAME', parameters.tdName);
                keyMap.add('TDID', parameters.tdId);
                keyMap.add('TDSPRAS', parameters.tdSpras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_HEADER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_HEADER', keyRange, headerQueryCallback, null);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLongtextHeader of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },
        //private
        //will save the provided text in long text lines
        //returns success
        saveLocalLongtextLines: function (textToSave, objectToSaveFor, readyForPosting) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!objectToSaveFor) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var parameters = this.determineParameters(objectToSaveFor);

                if (!parameters || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parameters.tdName)) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(textToSave)) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }

                var me = this;

                //generate the single longtext lines out of the text
                var longtextObjectsCallBack = function (longTextObjectsArray) {
                    try {
                        if (!longTextObjectsArray) {
                            //something went wrong
                            eventController.fireEvent(retval, false);
                        } else if (longTextObjectsArray.length === 0) {
                            //nothing to save found
                            eventController.fireEvent(retval, true);
                        } else {
                            //drop save requests for all longtext lines
                            var pendingSaves = longTextObjectsArray.length;
                            var errorOccurred = false;
                            var returned = false;

                            var completeFunction = function () {
                                try {
                                    if (errorOccurred === true && returned === false) {
                                        eventController.requestEventFiring(retval, false);
                                        returned = true;
                                    } else if (pendingSaves === 0 && errorOccurred === false) {
                                        eventController.fireEvent(me.EVENTS.LONGTEXT_ADDED, textToSave);
                                        eventController.fireEvent(retval, true);
                                    }
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextLines of BaseLongtextManager', ex);
                                    eventController.requestEventFiring(retval, false);
                                }
                            };

                            var saveCallback = function (success) {
                                try {
                                    pendingSaves--;

                                    errorOccured = !success;

                                    completeFunction();
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextLines of BaseLongtextManager', ex);
                                }
                            };

                            Ext.Array.each(longTextObjectsArray, function (longTextObject) {
                                var eventId = me.saveLongtextLine(longTextObject, readyForPosting);

                                if (eventId > -1) {
                                    eventController.registerOnEventForOneTime(eventId, saveCallback);
                                } else {
                                    errorOccured = true;
                                    completeFunction();
                                }
                            });
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextLines of BaseLongtextManager', ex);
                        callback.call(me, false);
                    }
                };

                //use user's language for local longtext lines
                //local longtext lines are always stored with user's language, even if the header comes from backend with a different language
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var tdSpras = userInfo.get('sprache').charAt(0);

                parameters.tdSpras = tdSpras;

                var eventId = this.getLongtextObjects(textToSave, parameters.tdObject, parameters.tdName, parameters.tdId, parameters.tdSpras, parameters.mobileKey, parameters.childKey2, parameters.busobjectkey);

                if (eventId > -1) {
                    eventController.registerOnEventForOneTime(eventId, longtextObjectsCallBack);
                } else {
                    eventController.requestEventFiring(retval, false);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLocalLongtextLines of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //requires the long text line already with a set counter value
        saveLongtextLine: function (longtextLineObject, readyForPosting) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!longtextLineObject) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(longtextLineObject['COUNTER'])) {
                    eventController.requestEventFiring(retval, false);
                    return;
                }

                longtextLineObject['UPDFLAG'] = readyForPosting === true ? 'I' : 'A';

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(longtextLineObject['CHILDKEY']))
                    longtextLineObject['CHILDKEY'] = AssetManagement.customer.manager.KeyManager.createKey([longtextLineObject['TDNAME'], longtextLineObject['COUNTER']]);

                var saveCallback = function (eventArgs) {
                    try {
                        var success = eventArgs.type === "success";
                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLongtextLine of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                AssetManagement.customer.core.Core.getDataBaseHelper().put('D_LTXT_LINE', longtextLineObject, saveCallback, saveCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveLongtextLine of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        //protected
        getLongtextObjects: function (longtext, tdObject, tdName, tdId, tdSpras, mobileKey, childKey, busObjKey) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var longTextObjectsArray = [];
                var linesArray = longtext.split('\n');

                if (linesArray && linesArray.length > 0) {
                    var me = this;
                    var getObjects = function (nextCounterValue) {
                        try {
                            if (nextCounterValue === -1) {
                                eventController.fireEvent(retval, undefined);
                            }

                            //trim the % from the nextCounter Value
                            nextCounterValue = parseInt(nextCounterValue.substr(1));

                            Ext.Array.each(linesArray, function (line) {
                                var tdformat = "*";
                                var pointer = 0;

                                while (pointer <= line.length) {
                                    //set the counter with % sign
                                    var counter  = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextCounterValue + '', '0', 3);

                                    if (pointer + 132 > line.length) {
                                        //last part of line
                                        longTextObjectsArray.push(me.buildDataBaseObjectForLongtextLine(tdObject, tdName, tdId, tdSpras, counter, tdformat, line.substring(pointer), mobileKey, childKey, busObjKey));
                                    } else {
                                        longTextObjectsArray.push(me.buildDataBaseObjectForLongtextLine(tdObject, tdName, tdId, tdSpras, counter, tdformat, line.substring(pointer, pointer + 132), mobileKey, childKey, busObjKey));
                                        //for next parts the format value has to be set to concatenation
                                        tdformat = "=";
                                    }

                                    nextCounterValue++;
                                    pointer += 132;
                                }
                            }, this);

                            eventController.fireEvent(retval, longTextObjectsArray);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtextLines of BaseLongtextManager', ex);
                            eventController.fireEvent(retval, undefined);
                        }
                    };

                    var eventId = this.getNextCounterValue(tdObject, tdName, tdId, tdSpras);

                    if (eventId > 1)
                        eventController.registerOnEventForOneTime(eventId, getObjects);
                    else
                        eventController.fireEvent(retval, undefined);
                } else {
                    eventController.fireEvent(retval, longTextObjectsArray);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtextLines of BaseLongtextManager', ex);
                retval = -1;
            }

            return retval;
        },

        buildDataBaseObjectForLongtextLine: function (tdObject, tdName, tdId, tdSpras, counter, tdformat, lineContent, mobileKey, childKey2, busObjKey) {
            var retval = null;

            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = { };
                retval['MANDT'] =  ac.getMandt();
                retval['USERID'] =  ac.getUserId();
                retval['TDID'] = tdId;
                retval['TDNAME'] = tdName;
                retval['TDOBJECT'] = tdObject;
                retval['TDSPRAS'] = tdSpras;
                retval['UPDFLAG'] = '';
                retval['TDLINE'] = lineContent;
                retval['COUNTER'] = counter;
                retval['TDFORMAT'] = tdformat;
                retval['TEXT'] = '';
                retval['AUFNR'] = '';
                retval['VORNR'] = '';

                retval['MOBILEKEY'] = mobileKey;
                retval['CHILDKEY2'] = childKey2;

                retval['BUSOBJKEY'] = busObjKey;
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForLongtextLine of BaseLongtextManager', ex);
            }

            return retval;
        },

        //private
        buildBackendLongtextFromDataBaseQuery: function (backendLongtextString, eventArgs) {
            var retval = '';

            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        if(cursor.value['UPDFLAG'] === 'X') {
                            backendLongtextString = this.extendLongtextStringFromDatabaseObject(backendLongtextString, cursor.value);
                        }

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }

                    retval = backendLongtextString;
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBackendLongtextFromDataBaseQuery of BaseLongtextManager', ex);
            }

            return retval;
        },

        //private
        buildLocalLongtextFromDataBaseQuery: function (localLongtextString, eventArgs) {
            var retval = '';

            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        if(cursor.value['UPDFLAG'] !== 'X') {
                            localLongtextString = this.extendLongtextStringFromDatabaseObject(localLongtextString, cursor.value);
                        }

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }

                    retval = localLongtextString;
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildLocalLongtextFromDataBaseQuery of BaseLongtextManager', ex);
            }

            return retval;
        },

        //private
        extendLongtextStringFromDatabaseObject: function (longtextString, object) {
            var retval = '';

            try {
                var lineFormat = object['TDFORMAT'];
                var lineContent = object['TDLINE'];

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(longtextString) && (lineFormat === '*')) {
                    retval = lineContent;
                } else {
                    switch (lineFormat) {
                        case '*':
                            retval = longtextString + '\n' + lineContent;
                            break;
                        case '>X':
                            retval = longtextString + '\n' + lineContent;
                            break;
                        case '=':
                            retval = longtextString + lineContent;
                            break;
                        case '':
                            retval = longtextString + ' ' + lineContent;
                            break;
                        case '/':
                            retval = longtextString + '\n\t' + lineContent;
                            break;
                        case '/=':
                            retval = longtextString + '\t' + lineContent;
                            break;
                        default:
                            retval = longtextString + ' ' + lineContent;
                            break;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extendLongtextStringFromDatabaseObject of BaseLongtextManager', ex);
            }

            return retval;
        },

        buildDataBaseObjectForLongtextHeader: function(tdObject, tdName, tdId, tdSpras, mobileKey, childKey, childKey2, busobjkey) {
            var retval = null;
            var ac = null;

            try {
                ac = AssetManagement.customer.core.Core.getAppConfig();

                retval = { };
                retval['MANDT'] =  ac.getMandt();
                retval['USERID'] =  ac.getUserId();
                retval['TDID'] = tdId;
                retval['TDNAME'] = tdName;
                retval['TDOBJECT'] = tdObject;
                retval['TDSPRAS'] = tdSpras;
                retval['UPDFLAG'] = '';
                retval['BUSOBJKEY'] = busobjkey;

                retval['MOBILEKEY'] = mobileKey;
                retval['CHILDKEY'] = childKey;
                retval['CHILDKEY2'] = childKey2;
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseObjectForLongtextHeader of BaseLongtextManager', ex);
            }

            return retval;
        },

        getNextCounterValue: function(tdObject, tdName, tdId, tdSpras) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if(!tdObject) {
                    eventController.requestEventFiring(retval, -1);
                    return retval;
                }

                var maxCounter = 0;

                var successCallback = function(eventArgs) {
                    try {
                        if(eventArgs && eventArgs.target && eventArgs.target.result) {
                            var cursor = eventArgs.target.result;

                            var counterValue = cursor.value['COUNTER'];

                            //check if the counter value is a local one
                            if (AssetManagement.customer.utils.StringUtils.startsWith(counterValue, '%')) {
                                //cut of the percent sign
                                counterValue = counterValue.substr(1);
                                var curCounter = parseInt(counterValue);

                                if(!isNaN(curCounter))
                                {
                                    if(curCounter > maxCounter)
                                        maxCounter = curCounter;
                                }
                            }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            var nextLocalCounterNumberValue = maxCounter + 1;
                            var nextLocalCounterValue = '%' + AssetManagement.customer.utils.StringUtils.padLeft(nextLocalCounterNumberValue + '', '0', 3);
                            eventController.fireEvent(retval, nextLocalCounterValue);
                        }
                    } catch(ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextCounterValue of BaseLongtextManager', ex);
                        eventController.fireEvent(retval, -1);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('TDOBJECT', tdObject);
                keyMap.add('TDNAME', tdName);
                keyMap.add('TDID', tdId);
                keyMap.add('TDSPRAS', tdSpras);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('D_LTXT_LINE', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('D_LTXT_LINE', keyRange, successCallback, null);
            } catch(ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextCounterValue of BaseLongtextManager', ex);
            }

            return retval;
        }
    }
});