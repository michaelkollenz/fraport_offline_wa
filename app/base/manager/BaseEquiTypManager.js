Ext.define('AssetManagement.base.manager.BaseEquiTypManager', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.cache.EquiTypCache',
        'AssetManagement.customer.model.bo.EquiTyp',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
        //protected
        getCache: function() {
	       var retval = null;

	       try {
		       retval = AssetManagement.customer.manager.cache.EquiTypCache.getInstance();
	       } catch(ex) {
		       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCache of BaseEquiTypManager', ex);
	       }
	
	       return retval;
        },

		//public
		getEquiTypes: function(useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				//check if the cache can delivery the complete dataset
		        var cache = this.getCache();
		        var fromCache = cache.getStoreForAll();
		
		        if(fromCache) {
				    //it can, so return just this store
				    eventController.requestEventFiring(retval, fromCache);
				    return retval;
		        }
		
		        var fromDataBase = Ext.create('Ext.data.Store', {
			        model: 'AssetManagement.customer.model.bo.EquiTyp',
			        autoLoad: false
		        });
				
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;	
					
						me.buildEquiTypStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
						
						if(done) {
							//add the whole store to the cache
							cache.addStoreForAllToCache(fromDataBase);
							
							//return a store from cache to eliminate duplicate objects
							var toReturn = cache.getStoreForAll();
							eventController.requestEventFiring(retval, toReturn);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquiTypes of BaseEquiTypManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
				
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('SCENARIO', AssetManagement.customer.model.bo.UserInfo.getInstance().get('mobileScenario'));
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_EQUITYP', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_EQUITYP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquiTypes of BaseEquiTypManager', ex);
			}
	
			return retval;
		},
		
		getEquiTyp: function(eqtyp, useBatchProcessing) {
			var retval = -1;
			
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				var retval = eventController.getNextEventId();
				
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(eqtyp)) {
					eventController.requestEventFiring(retval, null);
					return retval;
				}
				
				var cache = this.getCache();
		        var fromCache = cache.getFromCache(eqtyp);
		
		        if(fromCache) {
			       eventController.requestEventFiring(retval, fromCache);
			       return retval;
		        }
		
				var me = this;
				var successCallback = function(eventArgs) {
					try {
						var equiTyp = me.buildEquiTypDataBaseQuery.call(me, eventArgs);
						cache.addToCache(equiTyp);
						eventController.requestEventFiring(retval, equiTyp);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquiTyp of BaseEquiTypManager', ex);
						eventController.requestEventFiring(retval, undefined);
					}
				};
			
				var keyMap = Ext.create('Ext.util.HashMap');
				keyMap.add('SCENARIO', AssetManagement.customer.model.bo.UserInfo.getInstance().get('mobileScenario'));
				keyMap.add('EQTYP', eqtyp);
				
				var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_EQUITYP', keyMap);
				AssetManagement.customer.core.Core.getDataBaseHelper().query('C_EQUITYP', keyRange, successCallback, null, useBatchProcessing);
			} catch(ex) {
				retval = -1;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquiTyp of BaseEquiTypManager', ex);
			}
			
			return retval;
		},
		
		//private
		buildEquiTypDataBaseQuery: function(eventArgs) {
			var retval = null;
			
			try {
				if(eventArgs) {
					if(eventArgs.target.result) {
						retval = this.buildEquiTypFromDbResultObject(eventArgs.target.result.value);
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEquiTypDataBaseQuery of BaseEquiTypManager', ex);
			}
			
			return retval;
		},
		
		buildEquiTypStoreFromDataBaseQuery: function(store, eventArgs) {
	        try {
	        	if(eventArgs && eventArgs.target) {
					var cursor = eventArgs.target.result;
				    if (cursor) {
				    	var equiTyp = this.buildEquiTypFromDbResultObject(cursor.value);
						store.add(equiTyp);
				    	
				    	AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
				    }
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEquiTypStoreFromDataBaseQuery of BaseEquiTypManager', ex);
			}
		},
		
		buildEquiTypFromDbResultObject: function(dbResult) {
	        var retval = null;

	        try {
	        	retval = Ext.create('AssetManagement.customer.model.bo.EquiTyp', {
					scenario: dbResult['SCENARIO'],
					eqtyp: dbResult['EQTYP'],   	
				    txtbz: dbResult['TYPTX'],
				    updFlag: dbResult['UPDFLAG']
				});
				
				retval.set('id', dbResult['SCENARIO'] + dbResult['EQTYP']);
				
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildEquiTypFromDbResultObject of BaseEquiTypManager', ex);
			}
			
			return retval;
		}
	}
});