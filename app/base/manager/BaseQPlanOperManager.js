﻿Ext.define('AssetManagement.base.manager.BaseQPlanOperManager', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'Ext.util.HashMap',
        'AssetManagement.customer.model.bo.QPlanOper',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.manager.QPlanCharManager',
        'AssetManagement.customer.helper.StoreHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.model.bo.QPlanChar'
    ],

    inheritableStatics: {

        //public methods
        getQPlanOpers: function (plnty, plnnr, plnal, useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var operStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.QPlanOper',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildQPlanOperStoreFromDataBaseQuery.call(me, operStore, eventArgs);

                        if (done) {
                            me.loadDependendDataForQPlanOpers.call(me, retval, operStore, plnty, plnnr)
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlanOpers of BaseQPlanOperManager', ex);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('PLNTY', plnty);
                keyMap.add('PLNNR', plnnr);
                keyMap.add('PLNAL', plnal);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_QPLANOPER', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_QPLANOPER', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlanOpers of BaseQPlanOperManager', ex);
                retval = -1;
            }

            return retval;
        },

        //private methods
        buildQPlanOperStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;

                    if (cursor) {
                        var qplanOper = this.buildQPlanOperFromDbResultObject(cursor.value);
                        store.add(qplanOper);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanOperStoreFromDataBaseQuery of BaseQPlanOperManager', ex);
            }
        },

        buildQPlanOperFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.QPlanOper', {
                    plnty: dbResult['PLNTY'],
                    plnnr: dbResult['PLNNR'],
                    plnal: dbResult['PLNAL'],
                    plnkn: dbResult['PLNKN'],
                    zaehl: dbResult['ZAEHL'],
                    vornr: dbResult['VORNR'],
                    ltxa1: dbResult['LTXA1']
                });

                retval.set('id', dbResult['PLNTY'] + dbResult['PLNNR'] + dbResult['PLNAL'] + dbResult['PLNKN'] + dbResult['ZAEHL']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildQPlanOperFromDbResultObject of BaseQPlanOperManager', ex);
            }

            return retval;
        },

        loadDependendDataForQPlanOpers: function (retval, qplanOperStore, plnty, plnnr) {
            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                //do not continue if there is no data
                if (qplanOperStore.getCount() === 0) {
                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, qplanOperStore);
                    return;
                }

                var qplanCharStore = null;

                var counter = 0;

                var done = 0;

                var errorOccurred = false;
                var reported = false;

                var completeFunction = function () {
                    if (errorOccurred === true && reported === false) {
                        AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                        reported = true;
                    } else if (counter === done && errorOccurred === false) {
                        me.assignDependendDataForQPlanOpers.call(me, retval, qplanOperStore, qplanCharStore);
                    }
                };

                //get QPlanChar
                done++;

                var qplanCharSuccessCallback = function (qplanCharReceived) {
                    errorOccurred = qplanCharReceived === undefined;

                    qplanCharStore = qplanCharReceived

                    counter++;

                    completeFunction();
                };

                var eventId = AssetManagement.customer.manager.QPlanCharManager.getQPlanChars(plnty, plnnr, true);
                eventController.registerOnEventForOneTime(eventId, qplanCharSuccessCallback);

                if (done > 0) {
                    AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                } else {
                    completeFunction();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendDataForQPlanOpers of BaseQPlanOperManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
            }
        },

        assignDependendDataForQPlanOpers: function (retval, qplanOperStore, qplanCharStore) {
            try {
                if (qplanOperStore && qplanOperStore.getCount() > 0) {
                    qplanOperStore.each(function (qplanOper) {
                        var plnkn = qplanOper.get('plnkn');
               
                        var qplanCharsOfOper = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.QPlanChar',
                            autoLoad: false
                        });

                        qplanOper.set('qplanChars', qplanCharsOfOper);

                        //filling qplosCharPerOper with additional data
                        if (qplanCharStore && qplanCharStore.getCount() > 0) {
                            qplanCharStore.each(function (qplanChar) {
                                if (qplanChar.get('plnkn') === plnkn) {
                                    qplanCharsOfOper.add(qplanChar);
                                }
                            });
                        }
                    });
                }

                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, qplanOperStore);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendDataForQPlanOpers of BaseQPlanOperManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().fireEvent(retval, undefined);
            }
        }
    }
});
