Ext.define('AssetManagement.base.manager.BaseCalendarManager', {
	requires: [
	    'AssetManagement.customer.controller.EventController',
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.manager.OrderManager',
        'AssetManagement.customer.manager.OperationManager',
        'Ext.data.Store'
    ],
	
	inheritableStatics: {
		//will generate a store for all operations
		getOrdersForCalendar: function() {
			var retval = -1;
			try {
				var eventController = AssetManagement.customer.controller.EventController.getInstance();
				retval = eventController.getNextEventId();
				
				var loadLevel = 0;
				var maxLevel = 1;
				var me = this;
				var orderLoadCount = 0;
				var operationsLoadCount = 0;
				var ordersStore = null;
				var loadMethod = function (data) {


					//load the orders
					if(loadLevel == 0) {
						var loadOrdersCallback = function(orders) {
							loadLevel++;		
							ordersStore = orders;
							loadMethod(orders);
						};
						var eventId = AssetManagement.customer.manager.OrderManager.getOrders(true, true);
						eventController.registerOnEventForOneTime(eventId, loadOrdersCallback);





					} else if(loadLevel == 1) {
						var operations = me.getAllOrderOperations(ordersStore);
						var loadOperationsCallback = function(operations) {
							loadLevel++;
							loadMethod();
						};

						var eventId = eventController.getNextEventId();
						AssetManagement.customer.manager.OperationManager.loadDependendDataForCalendarOperations(operations, eventId);
						eventController.registerOnEventForOneTime(eventId, loadOperationsCallback);
					/*
						var orderCount = ordersStore.getCount();
						
						if(orderCount > 0) {
							//load the dependant data for the operations
							var loadOperationsCallback = function(operations) {
								orderLoadCount++;
								if(orderLoadCount == orderCount)
									loadLevel++;
								loadMethod();
							};
							
							eventController.registerOnEventForOneTime(eventId, loadOperationsCallback);
						} else {
							loadLevel++;
						}*/
					}
					
					if(loadLevel > maxLevel) {
						AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, ordersStore);
					} else {
						AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
					}
				}

				loadMethod();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScheduleItemsStoreForAllOperations of BaseScheduleManager', ex);
				retval = -1;
			}
			
			return retval;
		},
		
		getAllOrderOperations: function(orders) {
			var theStore = null;
			try {
				theStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.Operation',
					autoload: true
				});
				theStore.suspendEvents();
				for(var i =0;i<orders.data.length;i++) {
					for(var j=0;j<orders.data.items[i].get('operations').data.length;j++) {
						var operation = orders.data.items[i].get('operations').data.items[j];
						theStore.add(operation);						
					}
					
				}
				theStore.resumeEvents();
				
			}
			catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScheduleItemsStoreForAllOperations of BaseScheduleManager', ex);
			}
			return theStore;			
		}
	}
});