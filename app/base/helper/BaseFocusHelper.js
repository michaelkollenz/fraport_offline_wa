Ext.define('AssetManagement.base.helper.BaseFocusHelper', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		//private
		getFocusDummy: function() {
			var retval = null; 
		
			try {
				retval = Ext.getCmp('focusDummy');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFocusDummy of BaseFocusHelper', ex);
			}
			
			return retval;
		},
	
		setFocusToNone: function(delayed) {
			var retval = false;
		
		    try {
		    	var focusDummy = this.getFocusDummy();
		    	
		    	if(focusDummy) {
		    		var delayToUse = 0;
		    	
		    		if(delayed)
		    			delayToUse = 10;
		    	
		    		Ext.defer(focusDummy.focus, delayToUse, focusDummy);
		    		retval = true;
		    	}
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFocusToNone of BaseFocusHelper', ex);
			}
			
			return retval;
		}
	}
});