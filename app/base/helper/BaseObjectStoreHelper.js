Ext.define('AssetManagement.base.helper.BaseObjectStoreHelper', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
    ],

	inheritableStatics: {
		doObjectStoreDelete: function(objectStore, keyRange) {
			var deleteRequest = null;
	
		    try {			    	
			    if(objectStore) {
				   deleteRequest = objectStore['delete'](keyRange);
			    }
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doObjectStoreDelete of BaseObjectStoreHelper', ex);
			}
			
			return deleteRequest;
		}
	}
});