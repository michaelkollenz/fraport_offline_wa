Ext.define('AssetManagement.base.helper.BaseLibraryHelper', {
	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		loadLibrary: function(libraryToLoad, async, callback, callbackScope) {
			try {
				if(!async)
					async = false;
				
				var innerCallback = function(options, success, response) {
					try {
						//the passed success parameter is unreliable - evaluate, if there is something inside the response!
						var success = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(response.responseText);
					
						if(success)
							this.executeLibrary(response.responseText);
					
						if(callback) {
							callback.call(callbackScope, success);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLibrary of BaseLibraryHelper', ex);
					}
				};

				//chaching must be disabled, else the app cache will never be used
				Ext.Ajax.request({
					disableCaching: false,
		            async: async,
		            url: libraryToLoad,
		            callback: innerCallback,
		            scope: this
				});
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLibrary of BaseLibraryHelper', ex);
			}
		},
		
		executeLibrary: function(scriptString) {
			try {
		        //create a dom element to hold the code
		        var aScript = document.createElement('script');
		        aScript.type = 'text/javascript';

		        //set the script tag text, including the debugger id at the end!!
		        aScript.text = scriptString;

		        //append the code to the dom
		        document.getElementsByTagName('body')[0].appendChild(aScript);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside executeLibrary of BaseLibraryHelper', ex);
			}
		}
	}
});