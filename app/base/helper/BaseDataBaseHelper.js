Ext.define('AssetManagement.base.helper.BaseDataBaseHelper', {
  mixins: ['Ext.mixin.Observable'],

  requires: [
    'Ext.util.MixedCollection',
    'AssetManagement.customer.helper.model.StoreInfo',
    'AssetManagement.customer.helper.model.AttributeDetails',
    'AssetManagement.customer.helper.ObjectStoreHelper',
    'AssetManagement.customer.helper.DbKeyRangeHelper',
    'AssetManagement.customer.helper.CursorHelper',
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.utils.DateTimeUtils',
    'AssetManagement.customer.manager.KeyManager',
    'AssetManagement.customer.helper.OxLogger'
  ],

  inheritableStatics: {
    //public
    DEFAULT_DB_SIZE: 25,				//MB
    databaseRequestInterval: 10,		//ms

    usesNativeIndexedDbAPI: function () {
      var retval = true;

      try {
        retval = this._dataBaseAPI && this._usesNativeAPI;
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside usesNativeIndexedDbAPI of BaseDataBaseHelper', ex);
      }

      return retval;
    },

    //private
    _dataBaseAPI: null,
    _usesNativeAPI: true,

    COMMAND_TYPES: {
      RAW_QUERY: 0,
      QUERY: 1,
      PUT: 2,
      UPDATE: 3,
      RAW_DELETE: 4,
      DELETE: 5
    }
  },

  //private
  _dataBaseName: '',
  _dataBaseVersion: 1,
  _jsonResourceNameOfStrctInfo: '',
  _jsonResourceCustomerStrctInfo: '',
  _objectStoreInformationMap: null,

  _baseDB: null,
  _customerDB: null,
  _dataBase: null,
  _dataBaseIsOpen: false,
  _inTransaction: false,
  _commandCache: null,
  _runningDemandIdNumber: null,
  _lastDirtyHandleRequest: null,
  _databaseStructureJsonCache: '',

  //constructor
  //mandatory fields are: databaseName & databaseVersion or jsonResourceNameOfStrctInfo
  constructor: function (config) {
    try {
      this.mixins.observable.constructor.call(this, config);

      if (!config) {
        this.fireEvent('initialized', false);
        return;
      }

      this._dataBaseName = config.dataBaseName ? config.dataBaseName : '';
      this._dataBaseVersion = config.dataBaseVersion ? parseInt(config.dataBaseVersion) : 0;
      this._jsonResourceNameOfStrctInfo = config.jsonResourceNameOfStrctInfo ? config.jsonResourceNameOfStrctInfo : '';
      this._jsonResourceCustomerStrctInfo = config.jsonResourceCustomerStrctInfo ? config.jsonResourceCustomerStrctInfo : '';

      if (!this.hasDataForOpeningDatabase()) {
        //incomplete data for opening a database
        this.fireEvent('initialized', false);
        return;
      }

      this._commandCache = new Array();
      this._runningDemandIdNumber = 0;

      this.initializeDatabaseAPIAndOpenDatabase();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseDataBaseHelper', ex);
      this.fireEvent('initialized', false);
    }
  },

  //public methods
  isDataBaseOpen: function () {
    var retval = false;

    try {
      retval = this._dataBaseIsOpen;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isDataBaseOpen of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  getOpenedDataBaseName: function () {
    var retval = '';

    try {
      if (this.isDataBaseOpen()) {
        retval = this._dataBaseName;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOpenedDataBaseName of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  getOpenedDataBaseVersion: function () {
    var retval = 0;

    try {
      if (this.isDataBaseOpen()) {
        retval = this._dataBaseVersion;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOpenedDataBaseVersion of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  getObjectStoreInformationMap: function () {
    var retval = null;

    try {
      retval = this._objectStoreInformationMap;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectStoreInformationMap of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  getObjectStoreInformation: function (storeName) {
    var retval = null;

    try {
      var map = this.getObjectStoreInformationMap();

      if (map !== null && map !== undefined) {
        retval = map.get(storeName);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectStoreInformation of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  openDataBase: function () {
    //AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] DELETED DATABASE');
    //this.self._dataBaseAPI.deleteDatabase('AssetManagementDB');
    //AssetManagement.customer.controller.ClientStateController.setCurrentMainDataBaseData('', 0);
    //return;

    try {
      //return if the databaseAPI could not be initalized
      if (!this.self._dataBaseAPI) {
        this.fireEvent('initialized', false);
        return;
      }

      //return if the database is already open
      if (this.isDataBaseOpen()) {
        this.fireEvent('initialized', true);
        return;
      }

      //check if class has all neccessary data for opening an database
      if (!this.hasDataForOpeningDatabase()) {
        AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Database-Error: Missing data to open database!');

        this.fireEvent('initialized', false);
        return;
      }

      var me = this;

      //there are two cases to consider
      //case 1: first database opening, in this case the structure has to be initialized with information from databasestructure.json
      //case 2: database will be opened a second time. Version number can not differ, because its stored inside local storage.
      //		  Strucure information will be drawn out of the database itself. Has to be done, because structure in json file may differ
      //		  due to an application update with a different database structure.

      //Therefore it is neccessary to store the structure information on first opening inside the database itself

      //before the database is openend for the first time, it is neccessary to gather the databases' structure information
      //it will be needed in the following procedure, which does not allow any code flow interuption

      //where to draw this information from, depends on the fact, if the database has ever been opened
      var hasEverBeenOpened = this._dataBaseVersion > 0;

      var jsonCallback = function (success) {
        if (!success) {
          this.fireEvent('initialized', false);
          return;
        }

        if (!hasEverBeenOpened)
          AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Opening new database "' + me._dataBaseName + '" on version ' + me._dataBaseVersion);
        else
          AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Opening existing database "' + me._dataBaseName + '" on version ' + me._dataBaseVersion);

        var request = this.self._dataBaseAPI.open(this._dataBaseName, this._dataBaseVersion);

        //this is the only part where the database's structure may be changed
        //if anything is not as aspected or goes wrong, throw an exception to prevent invocation of the successcallback
        //do not call anything additional asynchronous from here. The successcallback will be called immediately as next step.
        request.onupgradeneeded = function (resultWrapper) {
          try {
            var db = resultWrapper.target.result;

            if (!db) {
              me.fireEvent('initialized', false);
            } else {
              me._dataBase = db;
              if (!me.buildDataBaseStructure()) {
                me.fireEvent('initialized', false);
              }
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openDataBase of BaseDataBaseHelper', ex);
            me.fireEvent('initialized', false);
          }
        };

        request.onsuccess = function (resultWrapper) {
          try {
            var db = resultWrapper.target.result;

            if (!db) {
              me.fireEvent('initialized', false);
            } else {
              //check if the database has been opened in buggy state (known on firefox)
              if (!db.objectStoreNames || db.objectStoreNames.length === 0) {
                var doReloadFunction = function () {
                  //BUG
                  //retry two times before informing the user
                  var doReload = false;
                  //try to check against a local storage value
                  if (AssetManagement.customer.helper.LocalStorageHelper.isLocalStorageAvailable()) {
                    var counter = AssetManagement.customer.helper.LocalStorageHelper.getNumber('buggyDatabaseOpeningCounter');
                    AssetManagement.customer.helper.LocalStorageHelper.putNumber('buggyDatabaseOpeningCounter', ++counter);
                    if ((counter % 3) != 0) {
                      doReload = true;
                    }
                  }
                  if (doReload) {
                    location.reload();
                  } else {
                    me.fireEvent('initialized', false, 'Opening database encountered a browser bug.');
                  }
                  return;
                }

                if (me.self._dataBaseAPI == window.shimIndexedDB) {
                  setTimeout(function () {
                    doReloadFunction();
                  }, 5000);
                } else {
                  doReloadFunction();
                }

                return;
              } else {
                if (AssetManagement.customer.helper.LocalStorageHelper.isLocalStorageAvailable()) {
                  //set back the buggy counter
                  AssetManagement.customer.helper.LocalStorageHelper.putNumber('buggyDatabaseOpeningCounter', 0);
                }
              }

              me._dataBase = db;

              db.onerror = function (eventArgs) {
                try {
                  var errorMessage = "";

                  if (eventArgs && eventArgs.target && eventArgs.target.error)
                    errorMessage = ": " + eventArgs.target.error.message;

                  me._inTransaction = false;

                  AssetManagement.customer.helper.OxLogger.logException('A general database error occured: ' + errorMessage);
                } catch (ex) {
                  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside general onerror callback for main database', ex);
                }
              };

              me._dataBaseIsOpen = true;
              if (hasEverBeenOpened === true) {
                //draw the structure information about the database out of itself
                var structureInformationCallback = function (success) {
                  try {
                    if (!success)
                      success = false;

                    me.fireEvent('initialized', success);
                  } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openDataBase of BaseDataBaseHelper', ex);
                    me.fireEvent('initialized', false);
                  }
                };

                me.initializeStructureInfoFromDatabase(structureInformationCallback, me);
              } else {
                //the structure information has already been loaded for the initialization of the database from json
                me.fireEvent('initialized', true);
              }
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openDataBase of BaseDataBaseHelper', ex);
            me.fireEvent('initialized', false);
          }
        };

        request.onerror = function () {
          me.fireEvent('initialized', false);
        };
      };

      if (!hasEverBeenOpened) {
        //draw all information from json file (including database name and version to open)
        this.loadDataBaseInformationFromJson(jsonCallback);
      } else {
        jsonCallback.call(this, true);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openDataBase of BaseDataBaseHelper', ex);
      this.fireEvent('initialized', false);
    }
  },

  waitForDataBase: function (callback, callbackScope) {
    try {
      if (!this._inTransaction && (!this._commandCache || this._commandCache.length == 0)) {
        callback.call(callbackScope);
      } else {
        Ext.defer(this.waitForDataBase, 250, this, [callback, callbackScope]);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside waitForDataBase of BaseDataBaseHelper', ex);
      callback.call(callbackScope);
    }
  },

  wipeDataBase: function (callback, callbackScope) {
    try {
      var me = this;
      if (this.self._dataBaseAPI) {
        //if the database is still stuck in an transaction, it is necessary to wait until it completed first
        var wipeAction = function () {
          try {
            var manualTableDeletionCallback =  function() {
              try {
                me.closeDataBase();

                me._dataBase = null;
                me._dataBaseIsOpen = false;

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(me._dataBaseName)) {
                  AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Deleting database "' + me._dataBaseName + '". Version ' + me._dataBaseVersion);

                  var request = me.self._dataBaseAPI.deleteDatabase(me._dataBaseName);

                  var successCallback = function (event) {
                    try {
                      if (callback)
                        callback.call(callbackScope, true);
                    } catch (ex) {
                      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wipeDataBase of BaseDataBaseHelper', ex);

                      if (callback)
                        callback.call(callbackScope, false);
                    }

                  };

                  var failureCallback = function (event) {
                    try {
                      if (callback)
                        callback.call(callbackScope, false);
                    } catch (ex) {
                      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wipeDataBase of BaseDataBaseHelper', ex);

                      if (callback)
                        callback.call(callbackScope, false);
                    }
                  };

                  request.onsuccess = successCallback;
                  request.onerror = failureCallback;
                }
              } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manualTableDeletionCallback of BaseDataBaseHelper', ex);
              }
            }

            if(!me.self.usesNativeIndexedDbAPI()) {
              this.performWebSQLDatabaseDeleteRequest(manualTableDeletionCallback);
            } else {
              manualTableDeletionCallback();
            }


          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wipeDataBase of BaseDataBaseHelper', ex);
          }
        };

        this.waitForDataBase(wipeAction, this);
      } else {
        if (callback)
          callback.call(callbackScope, false);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wipeDataBase of BaseDataBaseHelper', ex);

      if (callback)
        callback.call(callbackScope, false);
    }
  },

  performWebSQLDatabaseDeleteRequest: function(callback){
    try {
      var me = this;
      var db = me._dataBase;

      var counter = me._objectStoreInformationMap.getCount();
      var keys = me._objectStoreInformationMap.keys


      me._objectStoreInformationMap.each(function (store) {
        var curStoreName = store.get('storeName');

        var transaction = db.transaction(curStoreName, "readwrite");
        var objectStore = transaction.objectStore(curStoreName);

        if(objectStore) {
          var objectStoreRequest =  objectStore.clear();
          /*objectStoreRequest.onsuccess = function(evt) {
              counter--;
          };

          objectStoreRequest.onerror = function(evt) {
              counter--;
          }*/
        }

        transaction.oncomplete = function(event) {
          counter--;
          if(counter === 0){
            if (callback)
              callback.call(me, false);
          }
        };

      });

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performWebSQLDatabaseDeleteRequest of performWebSQLDatabaseDeleteRequest', ex);
    }
  },

  //deletes all data belonging to the current user from data stores
  //also sets back the sync delta information to inital state
  //use the storesToKeep parameter to exclude specific stores from deletion
  wipeUserData: function (callback, storesToKeep) {
    var me = this;

    try {
      var ac = AssetManagement.customer.core.Core.getAppConfig();
      AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Wiping user data on database: "' + me._dataBaseName + '". For user: "' + ac.getUserId() + '"');

      var basePool = new Array();
      var affectedStores = new Array();

      if (!this._objectStoreInformationMap) {
        callback.call(this, false);
        return;
      }

      this._objectStoreInformationMap.each(function (store) {
        var curStoreName = store.get('storeName');

        if (this.isDataStorage(curStoreName))
          basePool.push(curStoreName);
      }, this);

      basePool.push('S_DOCUPLOAD');
      basePool.push('S_SYNCDELTA');

      if (storesToKeep && storesToKeep.length > 0) {
        Ext.Array.each(basePool, function (storeName) {
          if (!Ext.Array.contains(storesToKeep, storeName))
            affectedStores.push(storeName);
        }, this);

        AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Deletion exluded stores: ' + storesToKeep.join(' '));
      } else {
        affectedStores = basePool;
      }

      var db = me._dataBase;
      var transaction = db.transaction(affectedStores, 'readwrite');

      //workaround, since db.objectStoreNames triggers exception in the following loop interation
      var copy = Ext.Array.clone(db.objectStoreNames);

      Ext.Array.each(affectedStores, function (storeName) {
        if (Ext.Array.contains(copy, storeName)) {
          var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRangeToWipeUserData(storeName);

          if (keyRange) {
            AssetManagement.customer.helper.ObjectStoreHelper.doObjectStoreDelete(transaction.objectStore(storeName), keyRange);
          }
        }
      });

      var appConfig = AssetManagement.customer.core.Core.getAppConfig();

      me.createUserDeltaStoreContent(appConfig.getMandt(), appConfig.getScenario(), appConfig.getUserId(), transaction);

      transaction.oncomplete = function () {
        me._inTransaction = false;

        if (callback != null && callback != undefined)
          callback.call(me, true);
      };

      transaction.onerror = function (err) {
        me._inTransaction = false;

        me.logDatabaseError(err);
      };

      me._inTransaction = true;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wipeUserData of BaseDataBaseHelper', ex);
      callback.call(null, false);
    }
  },

  createUserDeltaStoreContent: function (mandt, scenario, userId, transactionToUse) {
    var me = this;
    try {
      var affectedStores = new Array();
      var curStoreName;

      this._objectStoreInformationMap.each(function (store) {
          curStoreName = store.get('storeName');

          if (this.isDataStorage(curStoreName))
            affectedStores.push(curStoreName);
        }
        , this);

      affectedStores.push('S_DOCUPLOAD');
      if (!transactionToUse) {
        transactionToUse = this._dataBase.transaction('S_SYNCDELTA', 'readwrite');

        transactionToUse.onsuccess = function () {
          me._inTransaction = false;
        };

        transactionToUse.onerror = function () {
          me._inTransaction = false;
        };

        me._inTransaction = true;
      }

      var objectStore = transactionToUse.objectStore('S_SYNCDELTA');
      Ext.Array.each(affectedStores, function (storeName) {
        objectStore.put({ MANDT: mandt, SCENARIO: scenario, USERID: userId, STORENAME: storeName, UPLOAD: '', DOWNLOAD: '' });
      }, this);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createUserDeltaStoreContent of BaseDataBaseHelper', ex);
    }
  },

  isDataStorage: function (storageName) {
    var retval = false;

    try {
      retval = AssetManagement.customer.utils.StringUtils.startsWith(storageName, "D_") || AssetManagement.customer.utils.StringUtils.startsWith(storageName, "C_") || AssetManagement.customer.utils.StringUtils.startsWith(storageName, "O_");
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isDataStorage of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // basic database calls
  // use this methods, if there are no dependencies between database demands

  //drops a query on a store
  //all hits will be returned
  rawQuery: function (storeNames, keyRange, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var commandType = this.self.COMMAND_TYPES.RAW_QUERY;

      retval = this.prepareCommandExecution(commandType, storeNames, null, keyRange, null, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rawQuery of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a query on a store using an index
  rawQueryUsingAnIndex: function (storeNames, indexName, indexRange, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var commandType = this.self.COMMAND_TYPES.RAW_QUERY;

      retval = this.prepareCommandExecution(commandType, storeNames, indexName, indexRange, null, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rawQueryUsingAnIndex of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a query on a store
  //only valid hits will be returned - records with UPDFLAG value "D" will get exluded
  query: function (storeNames, keyRange, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var commandType = this.self.COMMAND_TYPES.QUERY;

      retval = this.prepareCommandExecution(commandType, storeNames, null, keyRange, null, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside query of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a query on a store using an index
  //only valid hits will be returned - records with UPDFLAG value 'D' or 'R' will get exluded
  queryUsingAnIndex: function (storeNames, indexName, indexRange, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var commandType = this.self.COMMAND_TYPES.QUERY;

      retval = this.prepareCommandExecution(commandType, storeNames, indexName, indexRange, null, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside queryUsingAnIndex of BaseDataBaseHelper', ex);
    }
    return retval;
  },

  manageTrackingGUID: function(storeName, object) {
    try {
      if(!storeName.startsWith('S')) {
        var guid = AssetManagement.customer.manager.KeyManager.createGUID(true);
        object["TRACKING_GUID"] = guid;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside put of BaseDataBaseHelper', ex);
    }

  },

  //puts a value to a store
  //does NOT handle any conflicts - existing records will be replaced
  put: function (storeName, object, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      var commandType = this.self.COMMAND_TYPES.PUT;

      this.manageTrackingGUID(storeName, object);

      retval = this.prepareCommandExecution(commandType, storeName, null, null, object, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside put of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //updates a subset of records on a store using an info object
  update: function (storeName, keyRange, updateInfoObject, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      var commandType = this.self.COMMAND_TYPES.UPDATE;

      this.manageTrackingGUID(storeName, updateInfoObject);

      retval = this.prepareCommandExecution(commandType, storeName, null, keyRange, updateInfoObject, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside update of BaseDataBaseHelper', ex);
    }
    return retval;
  },

  //deletes a subset of records on a store
  rawDel3te: function (storeName, keyRange, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var commandType = this.self.COMMAND_TYPES.RAW_DELETE;

      retval = this.prepareCommandExecution(commandType, storeName, null, keyRange, null, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rawDel3te of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a raw delete request on a store using an index
  //all affected records of the store will be deleted permanently
  rawDel3teUsingAnIndex: function (storeName, indexName, indexRange, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var commandType = this.self.COMMAND_TYPES.RAW_DELETE;

      retval = this.prepareCommandExecution(commandType, storeName, indexName, indexRange, null, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rawDel3teUsingAnIndex of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //deletes a subset of records on a store
  //stores with field UPDFLAG will get all affected records reflagged or physically deleted
  del3te: function (storeName, keyRange, successCallback, failureCallback, useBatchProcessing, useLocalUpdateFlag) {
    var retval = -1;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      var commandType = this.self.COMMAND_TYPES.DELETE;
      var useLocalUpdateFlag = useLocalUpdateFlag === true;

      retval = this.prepareCommandExecution(commandType, storeName, null, keyRange, { useLocalUpdateFlag: useLocalUpdateFlag }, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside del3te of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a delete request on a store using an index
  //stores with field UPDFLAG will get all affected records reflagged or physically deleted
  del3teUsingAnIndex: function (storeName, indexName, indexRange, successCallback, failureCallback, useBatchProcessing, useLocalUpdateFlag) {
    var retval = -1;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      var commandType = this.self.COMMAND_TYPES.DELETE;
      var useLocalUpdateFlag = useLocalUpdateFlag === true;

      retval = this.prepareCommandExecution(commandType, storeName, indexName, indexRange, { useLocalUpdateFlag: useLocalUpdateFlag }, successCallback, failureCallback, useBatchProcessing);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside del3teUsingAnIndex of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // executes the commands inside the command Queue
  doExecuteCommandQueue: function () {
    try {
      // if database is already in transaction, retry after the specified time interval
      if (this._inTransaction) {
        Ext.defer(this.doExecuteCommandQueue, this.self.databaseRequestInterval, this);
        return;
      }
      var commandSetToExecute = null;

      do {
        commandSetToExecute = this.getFirstCachedCommandSet();

        if (commandSetToExecute && commandSetToExecute.length === 0) {
          Ext.Array.remove(this._commandCache, commandSetToExecute);
        } else {
          break;
        }
      } while (true);

      if (commandSetToExecute) {
        var affectedStores = this.getAffectedStoresOfCommandSet(commandSetToExecute);

        this._inTransaction = true;

        var transaction = this._dataBase.transaction(affectedStores, 'readwrite');
        var me = this;

        var scb = null;
        var fcb = null;

        var temp = commandSetToExecute.pop();
        scb = temp.transactionSuccessCallback;
        fcb = temp.transactionFailureCallback;
        commandSetToExecute.push(temp);

        transaction.oncomplete = this.getCompleteTCB(scb, this);
        transaction.onerror = this.getErrorTCB(fcb, this);

        Ext.Array.forEach(commandSetToExecute, function (command) {
          this.executeCommandOnTransaction(command, transaction);
        }, me);
      }
    } catch (ex) {
      this.getErrorTCB(null, this).call(this);
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doExecuteCommandQueue of BaseDataBaseHelper', ex);
    }
  },

  // executes a command on transaction
  executeCommandOnTransaction: function (command, transaction) {
    try {
      var me = this;

      var request = this.generateRequestForCommand(command, transaction);
      if (request) {
        var successCB = function (eventArgs) {
          var retval = false;
          if (command.successCallback)
            retval = command.successCallback.call(me, eventArgs);

          //if a success callback decides to abort the transaction, it will return true. To abort the transaction an exception has to be thrown
          if (retval === true)
            throw new Exception();
        };

        var failureCB = function (errorEventArgs) {
          var logMessage = '[DATABASE] A database command execution failed. ';
          logMessage += me.getLogInfoStringForDbCommand(command);
          AssetManagement.customer.helper.OxLogger.logMessage(logMessage);

          var retval = true; //returning true or nothing means aborting transaction
          if (command.failureCallback)
            retval = command.failureCallback.call(me, errorEventArgs);

          return retval;
        };

        request.onsuccess = successCB;
        request.onerror = failureCB;
      } else {
        var logMessage = '[DATABASE] Could not generate the request for a database command. ';
        logMessage += this.getLogInfoStringForDbCommand(command);
        AssetManagement.customer.helper.OxLogger.logMessage(logMessage);

        if (command.failureCallback)
          command.failureCallback.call(me);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside executeCommandOnTransaction of BaseDataBaseHelper', ex);
    }
  },

  generateRequestForCommand: function (command, transaction) {
    var request = null;

    try {
      var me = this;
      var targetStore = command.targetStores;

      if ((typeof targetStore) === 'object' && targetStore.length > 0) {
        targetStore = targetStore[0];
      }

      var commandType = command.commandType;
      //PUT OR UPDATE
      if (this.self.COMMAND_TYPES.PUT === commandType || this.self.COMMAND_TYPES.UPDATE === commandType) {
        var ac = AssetManagement.customer.core.Core.getAppConfig();
        var oSI = this.getObjectStoreInformation(targetStore);

        var affectedObject = command.object;

        //add field MANDT, if missing
        if (oSI.hasAttribute('MANDT') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(affectedObject['MANDT']))
          affectedObject['MANDT'] = ac.getMandt();

        //add field SCENARIO, if missing
        if (oSI.hasAttribute('SCENARIO') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(affectedObject['SCENARIO']))
          affectedObject['SCENARIO'] = ac.getScenario();

        //add field USERID, if missing
        if (oSI.hasAttribute('USERID') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(affectedObject['USERID']))
          affectedObject['USERID'] = ac.getUserId();

        //add field DIRTY, if missing
        if (oSI.hasAttribute('DIRTY') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(affectedObject['DIRTY']))
          affectedObject['DIRTY'] = '0';
      }

      var objectStore = transaction.objectStore(targetStore);

      //evaluate, which kind of request may be generated
      if (this.self.COMMAND_TYPES.RAW_QUERY === commandType) {
        //RAW QUERY
        request = this.beginRawQuerySequence(objectStore, command);
      } else if (this.self.COMMAND_TYPES.QUERY === commandType) {
        //QUERY
        request = this.beginQuerySequence(objectStore, command);
      } else if (this.self.COMMAND_TYPES.PUT === commandType) {
        //PUT
        request = this.beginPutSequence(objectStore, command);
      } else if (this.self.COMMAND_TYPES.UPDATE === commandType) {
        //UPDATE
        request = this.beginUpdateSequence(objectStore, command);
      } else if (this.self.COMMAND_TYPES.RAW_DELETE === commandType) {
        //RAW DELETE
        request = this.beginRawDeleteSequence(objectStore, command);
      } else if (this.self.COMMAND_TYPES.DELETE === commandType) {
        //DELETE
        request = this.beginDeleteSequence(objectStore, command);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateRequestForCommand of BaseDataBaseHelper', ex);
    }

    return request;
  },

  getLogInfoStringForDbCommand: function (command) {
    var retval = '';

    try {
      if (command) {
        var targetStore = command.targetStores;

        if ((typeof targetStore) === 'object' && targetStore.length > 0) {
          targetStore = targetStore[0];
        }

        var temp = 'Command target store: ' + targetStore;
        temp += ' - Command type: ' + command.commandType;

        if (command.usesIndex === true) {
          temp += ' - Index: ';
          temp += command.indexName;
        }

        retval = temp;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLogInfoStringForRequest of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a raw query on a store
  beginRawQuerySequence: function (store, command) {
    var retval = null;

    try {
      var range = command.keyOrIndexRange;
      var storeName = store.name;
      var usesIndex = command.usesIndex === true;

      if (!range) {
        if (usesIndex) {
          range = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(storeName, null);
        } else if (store.keyPath) {
          range = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange(storeName, null);
        }
      }

      if (usesIndex) {
        request = store.index(command.indexName).openCursor(range);
      } else {
        request = store.openCursor(range);
      }

      retval = request;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginRawQuerySequence of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a query on a store
  //stores with field UPDFLAG will get all records excluded which have UPDFLAG value 'D' or 'R'
  beginQuerySequence: function (store, command) {
    var retval = null;

    try {
      request = this.beginRawQuerySequence(store, command);

      //check, if the store has the UPDFLAG field
      if (this.getObjectStoreInformation(store.name).hasAttribute('UPDFLAG')) {
        //extend the request actual successCallback to exclude record with updFlag value 'D' or 'R'
        var actualSuccessCallback = command.successCallback;

        var successCallback = function (eventArgs) {
          try {
            var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;
            var record = cursor ? cursor.value : null;
            //check, if the actual successCallback may not be called
            var skipThisRecord = record ? record['UPDFLAG'] === 'D' || record['UPDFLAG'] === 'R' : false;

            if (skipThisRecord) {
              AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
            } else {
              actualSuccessCallback(eventArgs);
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginQuerySequence of BaseDataBaseHelper', ex);

            if (command.failureCallback)
              command.failureCallback();

            return true;
          }
        };

        command.successCallback = successCallback;
      }

      retval = request;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginQuerySequence of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //drops a put request on a store for a specified object
  beginPutSequence: function (store, command) {
    var retval = null;

    try {
      retval = store.put(command.object);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginPutSequence of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //handles update logic according to presence of field "DIRTY" on a store
  //if present, drops another query on the store and updates all relevant records using the commands update info object
  //returns the query request for relevant records or a put command (store has no DIRTY attribute and no keyrange is specified)
  //calls the commands successCallback when all relevant records have been updated
  beginUpdateSequence: function (store, command) {
    var retval = null;

    try {
      var updateInfoObject = command.object;
      var storeName = store.name;
      var keyRange = command.keyOrIndexRange;

      //if the store supports delta sync (means it has the dirty row) and the update flag is marked as update some additional database actions have to be performed before the object can be updated
      //therefore the actual demand has to be replaced by another one. The actual update will be performed in it's success callback
      if (this.getObjectStoreInformation(storeName).hasAttribute('DIRTY') && updateInfoObject['UPDFLAG'] === 'U') {
        //extend command success call back by logic for dirty entry generation
        this.extendCommandByDirtyHandling(command);

        //if the keyRange is missing (single update) it is necessary to generate one
        if (!keyRange)
          keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.buildSingleUpdateKeyRangeOutOfObject(storeName, updateInfoObject);

        //drop a query command - this will exclude all records with UPDFLAG value 'D' or 'R'
        var queryCommand = {
          commandType: this.self.COMMAND_TYPES.QUERY,
          targetStores: command.targetStores,
          keyOrIndexRange: keyRange,
          indexName: command.indexName,
          usesIndex: command.usesIndex,
          successCallback: null,
          failureCallback: null,
          transactionSuccessCallback: null,
          transactionFailureCallback: null
        };

        retval = this.beginQuerySequence(store, queryCommand);
      } else if (keyRange) {
        var me = this;
        var actualSuccessCallback = command.successCallback;
        var lastUpdateRequest = null;

        var successCallback = function (eventArgs) {
          try {
            var cursor = eventArgs.target.result;

            if (cursor) {
              var affectedObject = cursor.value;

              //do the actual update
              //usually only one object will be affected, in this case it would be ok to perform an update with updateInfoObject
              //the following procedure is necessary to cover multi update cases
              var updateObject = me.generateUpdatedObject(cursor.source, affectedObject, updateInfoObject);
              var request = AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, updateInfoObject);

              //the actualSuccessCallback has to be executed when the last request returned with success
              lastUpdateRequest = request;
              AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
            } else {
              if (lastUpdateRequest && lastUpdateRequest.readyState === 'pending') {
                lastUpdateRequest.onsuccess = actualSuccessCallback;

                lastUpdateRequest = null;
              } else {
                actualSuccessCallback();
              }
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginUpdateSequence of BaseDataBaseHelper', ex);

            if (command.failureCallback)
              command.failureCallback();

            return true;
          }
        };

        command.successCallback = successCallback;

        //drop the query command - this will exclude all records with UPDFLAG value 'D' or 'R'
        var queryCommand = {
          commandType: this.self.COMMAND_TYPES.QUERY,
          targetStores: command.targetStores,
          keyOrIndexRange: keyRange,
          indexName: command.indexName,
          usesIndex: command.usesIndex,
          successCallback: null,
          failureCallback: null,
          transactionSuccessCallback: null,
          transactionFailureCallback: null
        };

        retval = this.beginQuerySequence(store, queryCommand);
      } else {
        retval = store.put(command.object);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginUpdateSequence of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //performs a raw delete on a store using the provided key or index range
  beginRawDeleteSequence: function (store, command) {
    var retval = null;

    try {
      retval = this.performRawDelete(store, command);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginRawDeleteSequence of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //handles delete logic according to presence of field "UPDFLAG" on a store
  //if present drops a query on store and handles each record accordingly to it's UPDFLAG value
  //returns the raw delete or query request
  //calls the commands successCallback when all affected records have been reflagged
  beginDeleteSequence: function (store, command) {
    var retval = null;

    try {
      var storeName = store.name;

      //perform a simple delete, if the affected store has no UPDFLAG field
      if (!this.getObjectStoreInformation(storeName).hasAttribute('UPDFLAG')) {
        retval = this.performRawDelete(store, command);
      } else {
        //it is a store with the UPDFLAG field - evaluate the stores record by update flag and ahndle these accordingly
        var me = this;
        var actualSuccessCallback = command.successCallback;
        var lastDeleteRequest = null;

        var successCallback = function (eventArgs) {
          if (!eventArgs || !eventArgs.target) {
            //something went wrong, log this and abort the current transaction
            AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] A data error occured while beginDeleteSequence on BaseDataBaseHelper');

            //this will abort the current transaction
            return true;
          }

          var cursor = eventArgs.target.result;

          if (cursor) {
            var me = this;
            var affectedObject = cursor.value;

            //check the update flag
            var recordsUpdFlag = affectedObject['UPDFLAG'];

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(recordsUpdFlag) || recordsUpdFlag === 'I' || recordsUpdFlag === 'A') {
              //delete record
              lastDeleteRequest = AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
            } else if (recordsUpdFlag === 'X') {
              //just resave record with update flag 'D' or 'R'
              affectedObject['UPDFLAG'] = command.object && command.object.useLocalUpdateFlag === true ? 'R' : 'D';
              this.manageTrackingGUID(storeName, affectedObject);

              lastDeleteRequest = AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, affectedObject);
            } else if (recordsUpdFlag === 'U') {
              //resave record with update flag 'D' or 'R' and delete connected dirty record
              affectedObject['UPDFLAG'] = command.object && command.object.useLocalUpdateFlag === true ? 'R' : 'D';
              this.manageTrackingGUID(storeName, affectedObject);

              lastDeleteRequest = AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, affectedObject);

              var keyRangeOfDirtyRecord = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject(storeName, affectedObject, true);
              AssetManagement.customer.helper.ObjectStoreHelper.doObjectStoreDelete(store, keyRangeOfDirtyRecord);
            }

            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
          } else {
            if (lastDeleteRequest && lastDeleteRequest.readyState === 'pending') {
              lastDeleteRequest.onsuccess = actualSuccessCallback;

              lastDeleteRequest = null;
            } else {
              actualSuccessCallback(eventArgs);
            }
          }
        };

        command.successCallback = successCallback;

        //drop the query command - this will exclude all records with UPDFLAG value 'D' or 'R'
        var queryCommand = {
          commandType: this.self.COMMAND_TYPES.QUERY,
          targetStores: command.targetStores,
          keyOrIndexRange: command.keyOrIndexRange,
          indexName: command.indexName,
          usesIndex: command.usesIndex,
          successCallback: null,
          failureCallback: null,
          transactionSuccessCallback: null,
          transactionFailureCallback: null
        };

        retval = this.beginQuerySequence(store, queryCommand);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginDeleteSequence of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //performs a raw delete on a store using the provided key or index range
  performRawDelete: function (store, command) {
    var retval = null;

    try {
      if (command.usesIndex !== true) {
        //a key range is used. in this case the native delete API can be used
        retval = AssetManagement.customer.helper.ObjectStoreHelper.doObjectStoreDelete(store, command.keyOrIndexRange);
      } else {
        //index case
        //the records have to read first and then be deleted on the cursor
        var me = this;
        var actualSuccessCallback = command.successCallback;
        var lastDeleteRequest = null;

        var successCallback = function (eventArgs) {
          if (!eventArgs || !eventArgs.target) {
            //something went wrong, log this and abort the current transaction
            AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] A data error occured while performRawDelete on BaseDataBaseHelper');

            //this will abort the current transaction
            return true;
          }

          var cursor = eventArgs.target.result;

          if (cursor) {
            //delete record
            lastDeleteRequest = AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);

            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
          } else {
            if (lastDeleteRequest && lastDeleteRequest.readyState === 'pending') {
              lastDeleteRequest.onsuccess = actualSuccessCallback;

              lastDeleteRequest = null;
            } else {
              actualSuccessCallback(eventArgs);
            }
          }
        };

        command.successCallback = successCallback;

        //drop a raw query command - this will include also records with UPDFLAG value 'D' or 'R'
        var rawQueryCommand = {
          commandType: this.self.COMMAND_TYPES.RAW_QUERY,
          targetStores: command.targetStores,
          keyOrIndexRange: command.keyOrIndexRange,
          indexName: command.indexName,
          usesIndex: command.usesIndex,
          successCallback: null,
          failureCallback: null,
          transactionSuccessCallback: null,
          transactionFailureCallback: null
        };

        retval = this.beginRawQuerySequence(store, rawQueryCommand);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performRawDelete of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // extended database calls
  // using the following three methods enables dependencies between database demands. These methods return a command,
  // which can be executed on a context given transaction of former demands

  // returns a raw query command
  getRawQueryCommand: function (storeNames, keyRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = {
        commandType: this.self.COMMAND_TYPES.RAW_QUERY,
        targetStores: storeNames,
        keyOrIndexRange: keyRange,
        indexName: "",
        usesIndex: false,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRawQueryCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns a query command using an index
  getRawQueryWithIndexCommand: function (storeName, indexName, indexRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = {
        commandType: this.self.COMMAND_TYPES.RAW_QUERY,
        targetStores: storeName,
        keyOrIndexRange: indexRange,
        indexName: indexName,
        usesIndex: true,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRawQueryWithIndexCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns a query command
  getQueryCommand: function (storeNames, keyRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = {
        commandType: this.self.COMMAND_TYPES.QUERY,
        targetStores: storeNames,
        keyOrIndexRange: keyRange,
        indexName: "",
        usesIndex: false,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQueryCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns a query command using an index
  getQueryWithIndexCommand: function (storeName, indexName, indexRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = {
        commandType: this.self.COMMAND_TYPES.QUERY,
        targetStores: storeName,
        keyOrIndexRange: indexRange,
        indexName: indexName,
        usesIndex: true,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQueryWithIndexCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns a put command
  getPutCommand: function (storeName, object, successCallback, failureCallback) {
    var retval = null;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      retval = {
        commandType: this.self.COMMAND_TYPES.PUT,
        targetStores: storeName,
        object: object,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPutCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns an update command
  getUpdateCommand: function (storeName, keyRange, updateInfoObject, successCallback, failureCallback) {
    var retval = null;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      retval = {
        commandType: this.self.COMMAND_TYPES.UPDATE,
        targetStores: storeName,
        keyOrIndexRange: keyRange,
        object: updateInfoObject,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUpdateCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns a raw delete command
  getRawDeleteCommand: function (storeName, keyRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = {
        commandType: this.self.COMMAND_TYPES.RAW_DELETE,
        targetStores: storeName,
        keyOrIndexRange: keyRange,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRawDeleteCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  // returns a delete command
  getDeleteCommand: function (storeName, keyRange, successCallback, failureCallback) {
    var retval = null;

    try {
      if (this.isDataStorage(storeName))
        successCallback = this.extendSuccessCallbackBySyncDeltaModification(successCallback, storeName);

      retval = {
        commandType: this.self.COMMAND_TYPES.DELETE,
        targetStores: storeName,
        keyOrIndexRange: keyRange,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeleteCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //sets an entry for a user in the S_DEVICE store
  //if there is no device id, identifing the hardware, a new one has to be created
  //this happens only one time after the software has been deployed
  createDeviceId: function (mandt, userId, successCallBack) {
    try {
      var me = this;

      me._inTransaction = true;

      var transaction = me._dataBase.transaction('S_DEVICE', 'readwrite');
      var callback = function () { me._inTransaction = false; };

      transaction.oncomplete = callback;
      transaction.onerror = callback;

      var store = transaction.objectStore('S_DEVICE');
      var request = store.openCursor();

      var successCallback = function (eventArgs) {
        try {
          var ac = AssetManagement.customer.core.Core.getAppConfig();
          var objectToStore = {};

          objectToStore['MANDT'] = ac.getMandt();
          objectToStore['SCENARIO'] = ac.getScenario();
          objectToStore['USERID'] = ac.getUserId();
          objectToStore['LASTLOGON'] = AssetManagement.customer.utils.DateTimeUtils.getDBTimestamp();

          var id;

          if (eventArgs && eventArgs.target && eventArgs.target.result) {
            var entry = AssetManagement.customer.helper.CursorHelper.getCurrentCursorValue(eventArgs.target.result);

            if (entry)
              id = entry['DEVICE'];
          } else {
            id = AssetManagement.customer.manager.KeyManager.createUUID();
          }

          ac.setDeviceId(id);
          objectToStore['DEVICE'] = id;
          if (successCallBack) {
            successCallBack.call()
          }
          store.put(objectToStore);
          me._inTransaction = false;
        } catch (ex) {
          me._inTransaction = false;
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createDeviceId of BaseDataBaseHelper', ex);
        }
      };

      request.onsuccess = successCallback;
    } catch (ex) {
      me._inTransaction = false;
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createDeviceId of BaseDataBaseHelper', ex);
    }
  },

  //private methods
  //checks if the database helper object has been provided with the neccessary data to open a database
  hasDataForOpeningDatabase: function () {
    var retval = false;

    try {
      if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._dataBaseName) && this._dataBaseVersion > 0) {
        retval = true;
      } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._jsonResourceNameOfStrctInfo)) {
        retval = true;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasDataForOpeningDatabase of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //will setup the API to use and then start the opening procedure
  initializeDatabaseAPIAndOpenDatabase: function () {
    try {
      var me = this;
      var afterAPIInitialized = function (success) {
        try {
          if (success === true) {
            me.openDataBase();
          } else {
            me.fireEvent('initialized', false);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeDatabaseAPIAndOpenDatabase of BaseDataBaseHelper', ex);
          me.fireEvent('initialized', false);
        }
      };

      this.initializeAPI(afterAPIInitialized);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeDatabaseAPIAndOpenDatabase of BaseDataBaseHelper', ex);
      this.fireEvent('initialized', false);
    }
  },

  //determines, which API to use and try to initialize it
  //callback will be called with a boolean, indicating if the initialization completed
  initializeAPI: function (callback) {
    try {
      if (!callback)
        return;

      if (this.self._dataBaseAPI) {
        callback.call(this, true);
        return;
      }

      var userAgentInfo = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();

      var hasNativeIndexDataBaseAPI = !!(window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB);
      var hasWebSQLDataBaseAPI = !!window.open;
      var hasAnyDataBaseAPI = hasNativeIndexDataBaseAPI || hasWebSQLDataBaseAPI;

      if (!hasAnyDataBaseAPI) {
        AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Webbrowser does not support any database API. Can\'t open database.');
        callback.call(this, false, 'Can\'t open database. Webbrowser does not support any database API.');
      } else {
        var isSafari = userAgentInfo.browser === 'Safari';
        var isAndroidStandardBrowser = AssetManagement.customer.utils.StringUtils.contains(userAgentInfo.OS, 'Android') && userAgentInfo.hasWebkit === true;
        var isInternetExplorer = userAgentInfo.browser === 'IE';

        //the safari indexeddb implemenation is seriously buggy, so rather use the polyfill api
        //internet explorer still misses indexeddb keyfeatures. Add these features loading the polyfill.
        if (isSafari || isInternetExplorer) {
          var libraryLoadedCallback = function (success) {
            try {
              if (success === true) {
                window.shimIndexedDB.__useShim();

                if (isSafari) {
                  this.self._dataBaseAPI = window.shimIndexedDB;
                  this.self._dataBaseAPI.setDefaultDbSize(this.self.DEFAULT_DB_SIZE * 1024 * 1024);
                  this.self._usesNativeAPI = false;
                  AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Will use a polyfill IndexedDb API, because native API is missing')
                } else {
                  this.self._dataBaseAPI = window.indexedDB || window.msIndexedDB;
                  AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Will use a polyfill IndexedDb API due to missing of some key features on native API')
                }

                callback.call(this, true);
              } else {
                callback.call(this, false, 'Can\'t open database. Failed to load polyfill library.');
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeAPI of BaseDataBaseHelper', ex);
              callback.call(this, false);
            }
          };

          AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/indexeddbshim.js', true, libraryLoadedCallback, this);
        } else {
          this.self._dataBaseAPI = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB;
          AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Will use native IndexedDb API')

          callback.call(this, true);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeAPI of BaseDataBaseHelper', ex);
      callback.call(this, false);
    }
  },

  //will load a json object and use this object to initialize the objectStoreInformationMap
  //the callback will be called after completition or failure
  loadDataBaseInformationFromJson: function (successCallback) {
    try {
      AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Loading database information from json file');
      Ext.Ajax.request({
        url: 'resources/json/' + this._jsonResourceNameOfStrctInfo,
        callback: function (opts, success, response) {
          try {
            if (success === true || response && response.status === 0) {
              this._baseDB = Ext.decode(response.responseText);
              Ext.Ajax.request({
                url: 'resources/json/' + this._jsonResourceCustomerStrctInfo,
                callback: function (opts, success, resp) {
                  var retval = false;

                  try {
                    if (success === true || resp && resp.status === 0) {
                      this._customerDB = Ext.decode(resp.responseText);

                      var dbStructure = this._baseDB;
                      dbStructure.databaseName = this._customerDB.databaseName;
                      dbStructure.databaseversion = dbStructure.databaseversion + this._customerDB.databaseversion;

                      for(var i = 0; i < this._customerDB.stores.length; i++) {
                        for(var j = 0; j< dbStructure.stores.length; j++) {
                          if(dbStructure.stores[j].storename === this._customerDB.stores[i].storename){
                            dbStructure.stores.splice(i, 1);
                          }
                        }
                      }

                      dbStructure.stores =  dbStructure.stores.concat(this._customerDB.stores)
                      retval = this.extractDataBaseInformation(dbStructure);
                      if (successCallback)
                        successCallback.call(this, retval);
                    } else {
                      console.log('failure reading \'customer.json\' with status code ' + response.status);
                    }
                  } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDataBaseInformationFromJson of BaseDataBaseHelper', ex);
                  }


                },
                scope: this,
                disableCaching: false
              });

              //retval = this.extractDataBaseInformation(Ext.decode(response.responseText));
            } else {
              console.log('failure reading \'databasestructure.json\' with status code ' + response.status);
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDataBaseInformationFromJson of BaseDataBaseHelper', ex);
          }

          /*if (successCallback)
              successCallback.call(this, retval);*/
        },
        scope: this,
        disableCaching: false
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDataBaseInformationFromJson of BaseDataBaseHelper', ex);

      if (successCallback)
        successCallback.call(this, false);
    }
  },

  extractDataBaseInformation: function (jsonResponseObject) {
    var retval = false;

    try {
      if (!jsonResponseObject)
        return;

      this._databaseStructureJsonCache = jsonResponseObject;

      this._dataBaseName = jsonResponseObject.databasename;
      this._dataBaseVersion = parseInt(jsonResponseObject.databaseversion);

      if (!this._objectStoreInformationMap)
        this._objectStoreInformationMap = new Ext.util.MixedCollection();

      var oSIM = this._objectStoreInformationMap;

      //extract information
      var storesArray = jsonResponseObject.stores;
      var newStoreInfo = null;
      var newAttribute = null;
      var attributesStore = null;

      Ext.Array.forEach(storesArray, function (item) {
        newStoreInfo = Ext.create('AssetManagement.customer.helper.model.StoreInfo', { storeName: item.storename });
        newStoreInfo.set('keyAttributes', item.keys);

        var indexes = item.indexes;

        if (indexes === undefined)
          indexes = null;

        newStoreInfo.set('indexes', indexes);

        var attributesMap = new Ext.util.MixedCollection();

        Ext.Array.forEach(item.attributes, function (item) {
          newAttribute = Ext.create('AssetManagement.customer.helper.model.AttributeDetails', {
            name: item.name,
            order: item.order,
            type: item.type,
            length: item.length
          });

          newAttribute.set('isKey', !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(item.isKey));
          newAttribute.set('suppressUpload', !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(item.suppressUpload));

          attributesMap.add(item.name, newAttribute);
        });

        newStoreInfo.set('attributes', attributesMap);
        oSIM.add(item.storename, newStoreInfo);
      });

      retval = true;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractDataBaseInformation of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //will initialize the objectstore information map by a json object stored inside the database itself
  //the provided callback will be called, if the initialization was successful
  initializeStructureInfoFromDatabase: function (callback, callbackScope) {
    try {
      var me = this;

      //read the json object from store and pass it to extractDataBaseInformation
      var informationCallback = function (eventArgs) {
        var retval = false;

        try {
          if (eventArgs && eventArgs.target && eventArgs.target.result && eventArgs.target.result.value) {
            var jsonObject = eventArgs.target.result.value;
            retval = me.extractDataBaseInformation(jsonObject);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeStructureInfoFromDatabase of BaseDataBaseHelper', ex);
        }

        if (callback)
          callback.call(callbackScope ? callbackScope : me, retval);
      };

      var transaction = this._dataBase.transaction(['S_DATABASESTRUCTURE'], 'readwrite');

      var request = transaction.objectStore('S_DATABASESTRUCTURE').openCursor();
      request.onsuccess = informationCallback;
      request.onerror = informationCallback;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeStructureInfoFromDatabase of BaseDataBaseHelper', ex);

      if (callback)
        callback.call(callbackScope ? callbackScope : this, false);
    }
  },

  buildDataBaseStructure: function () {
    var retval = false;

    var me = this;
    var db = me._dataBase;

    try {
      AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Initializing database structure for database "' + this._dataBaseName + '" of version ' + this._dataBaseVersion, true);
      //interate over the objectStoreInformationsStore and create every store listed inside
      var curStore = "";

      //workaround, since db.objectStoreNames triggers exception in the following loop interation
      var copy = Ext.Array.clone(db.objectStoreNames);

      me._objectStoreInformationMap.each(function () {
        curStore = this.get('storeName');
        storesIndexes = this.get('indexes');
        AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Begin creating DataBase ObjectStore: ' + curStore, true);

        if (Ext.Array.contains(copy, curStore)) {
          AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] DataBase already has this ObjectStore ' + curStore + '!', true);
          AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Deleting prior objectStore: ' + curStore, true);
          db.deleteObjectStore(curStore);
        }

        AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] Creating DataBase ObjectStore: ' + curStore, true);

        var createdStore = null;

        //if the store has explicit keys, create with keypath
        if (this.getKeysCount() > 0) {
          createdStore = db.createObjectStore(curStore, { keyPath: this.get('keyAttributes') });
        } else {
          //else create a store with autoIncrement = true;
          createdStore = db.createObjectStore(curStore, { autoIncrement: true });
        }

        //if the store has indexes create them
        if (createdStore && storesIndexes) {
          if (storesIndexes.length > 0) {
            Ext.Array.each(storesIndexes, function (index) {
              createdStore.createIndex(index.name, index.attributes);
            });
          }
        }
      });

      //done, now store the structure json inside the database itself
      var infoStore = db.createObjectStore('S_DATABASESTRUCTURE', { autoIncrement: true });
      infoStore.put(this._databaseStructureJsonCache);

      this._databaseStructureJsonCache = '';

      retval = true;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataBaseStructure of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  closeDataBase: function () {
    try {
      if (this._dataBase !== null && this._dataBase !== undefined)
        this._dataBase.close();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside closeDataBase of BaseDataBaseHelper', ex);
    }
  },

  prepareCommandExecution: function (commandType, storeNames, indexName, keyOrIndexRange, object, successCallback, failureCallback, useBatchProcessing) {
    var retval = -1;

    try {
      var demandId = this.getNextDemandId();

      useBatchProcessing = useBatchProcessing === true ? true : false;

      var theSetToAddCommandTo = null;

      //if batch processing is requested, be aware of the case, the database is still stuck in an transaction
      //if this is the case, there is still one command set left in the cache, which may not be used for pushing new demands to
      //(because it will be removed before the next execution cycle)
      //so if the cache has one entry-set while database is still in transaction, a new command set has to be generated
      if (useBatchProcessing && this._commandCache.length > 0 && !(this._inTransaction && this._commandCache.length === 1)) {
        theSetToAddCommandTo = this._commandCache.pop();
        this._commandCache.push(theSetToAddCommandTo);
      }

      if (!theSetToAddCommandTo) {
        theSetToAddCommandTo = new Array();
        this._commandCache.push(theSetToAddCommandTo);
      }

      var usesIndex = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(indexName);

      theSetToAddCommandTo.push({
        commandType: commandType,
        targetStores: storeNames,
        usesIndex: usesIndex,
        indexName: indexName,
        keyOrIndexRange: keyOrIndexRange,
        object: object, demandId: demandId,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      });

      if (!useBatchProcessing)
        this.doExecuteCommandQueue();

      retval = demandId;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareCommandExecution of BaseDataBaseHelper', ex);
    }
    return retval;
  },

  getNextDemandId: function () {
    try {
      return ((this._runningDemandIdNumber++ % 10000) + 1) + '';
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextDemandId of BaseDataBaseHelper', ex);
    }
  },

  //the eventArgs parameter should always contain a cursor as result
  handleDirtyObjectUpdateIssue: function (eventArgs, updateInfoObject, actualSuccessCallback) {
    try {
      if (!eventArgs || !eventArgs.target) {
        //something went wrong, log this and abort the current transaction
        AssetManagement.customer.helper.OxLogger.logMessage('[DATABASE] An data error occured while handleDirtyObjectUpdateIssue on BaseDataBaseHelper');

        //this will abort the current transaction
        return true;
      }

      //check if there still is any affected object
      if (eventArgs.target.result) {
        var me = this;
        var cursor = eventArgs.target.result;
        var affectedObject = cursor.value;

        //the actualSuccessCallback has to be executed when the last request returned with success
        var request;

        //examine if a dirty object already exists or has to be created
        if (affectedObject['UPDFLAG'] === 'X') {
          //perform a put with a modified key
          affectedObject['DIRTY'] = 'X';
          affectedObject['UPDFLAG'] = '0';
          cursor.source.put(affectedObject);
        }

        //do the actual update
        //usually only one object will be affected, in this case it would be ok to perform an update with updateInfoObject
        //the following procedure is necessary to cover multi update cases
        var updateObject = me.generateUpdatedObject(cursor.source, affectedObject, updateInfoObject);
        request = AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, updateInfoObject);

        this._lastDirtyHandleRequest = request;
        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
      } else {
        var request = this._lastDirtyHandleRequest;

        if (request && request.readyState === 'pending') {
          request.onsuccess = actualSuccessCallback;

          this._lastDirtyHandleRequest = null;
        } else {
          actualSuccessCallback(eventArgs);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside handleDirtyObjectUpdateIssue of BaseDataBaseHelper', ex);
    }
  },

  //generates a new object to store, merged out of from sourceObject and updateInfoObject
  //key attributes will be drawn from the source object except DIRTY
  //other attributes will be drawn from updateInfoObject if present except UPDFLAG
  //else these attributes will be drawn from the source object
  //UPDFLAG will only switch from X to U - all other values will stay as they are
  generateUpdatedObject: function (store, sourceObject, updateInfoObject) {
    var retval = {};

    try {
      var storeInfo = this.getObjectStoreInformation(store.name);

      var attributes = storeInfo.get('attributes');

      attributes.each(function (attributeDetails) {
        var attributeName = attributeDetails.get('name');
        var valueToSet;

        if (attributeDetails.get('isKey')) {
          if ('DIRTY' === attributeName)
            valueToSet = '0';
          else
            valueToSet = sourceObject[attributeName];
        } else if ('UPDFLAG' === attributeName) {
          if (sourceObject[attributeName] === 'X')
            valueToSet = 'U';
          else
            valueToSet = sourceObject[attributeName];
        } else {
          valueToSet = updateInfoObject[attributeName];

          if (valueToSet === undefined || valueToSet === null)
            valueToSet = sourceObject[attributeName];
        }

        if (valueToSet === undefined || valueToSet === null)
          valueToSet = '';

        retval[attributeName] = valueToSet;
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateUpdatedObject of BaseDataBaseHelper', ex);
    }
    return retval;
  },

  // nested transaction callbacks
  extendSuccessCallbackBySyncDeltaModification: function (originalCallback, store) {
    var modifiedCallback = null;

    try {
      var me = this;

      modifiedCallback = function (eventArgs) {
        try {
          var transaction = eventArgs.target.transaction;
          var originalResults = eventArgs;

          //check if command was successful
          //if it was begin sync delta update
          if (eventArgs.type === "success") {
            var errorCallback = function (eventArgs) {
              AssetManagement.customer.helper.OxLogger.logMessage("Failed to update sync delta for store: " + store);
              me.logDatabaseError(eventArgs);

              if (originalCallback)
                originalCallback.call(this, originalResults);
            };

            //next step, is to get the current object stored, which represents the sync state for the given store
            var ac = AssetManagement.customer.core.Core.getAppConfig();
            var request = transaction.objectStore('S_SYNCDELTA').get([ac.getMandt(), ac.getScenario(), ac.getUserId(), store]);
            request.onsuccess = function (eventArgs) {
              if (eventArgs.target.result) {
                //now take the object, analyse if the upload has to be set
                //only save again, if upload has not been set yet
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(eventArgs.target.result['UPLOAD'])) {
                  eventArgs.target.result['UPLOAD'] = 'X';

                  var innerSuccessCallback = function (eventArgs) {
                    if (!eventArgs.target.result) {
                      AssetManagement.customer.helper.OxLogger.logMessage("Failed to update sync delta for store: " + store);
                    }

                    if (originalCallback)
                      originalCallback.call(me, originalResults);
                  };

                  var request = eventArgs.target.transaction.objectStore('S_SYNCDELTA').put(eventArgs.target.result);
                  request.onsuccess = innerSuccessCallback;
                  request.onerror = errorCallback;

                } else {
                  if (originalCallback)
                    originalCallback.call(me, originalResults);
                }
              } else {
                AssetManagement.customer.helper.OxLogger.logMessage("Failed to update sync delta for store: " + store);

                if (originalCallback)
                  originalCallback.call(me, originalResults);
              }
            };

            request.onerror = errorCallback;
          } else {
            if (originalCallback)
              originalCallback.call(this, originalResults);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extendSuccessCallbackBySyncDeltaModification of BaseDataBaseHelper', ex);
        }
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extendSuccessCallbackBySyncDeltaModification of BaseDataBaseHelper', ex);
    }

    return modifiedCallback;
  },

  extendCommandByDirtyHandling: function (command) {
    try {
      var me = this;

      if (command) {
        var originalCallback = command.successCallback;

        command.successCallback = function (eventArgs) {
          try {
            //ensure the correct scope
            me.handleDirtyObjectUpdateIssue.call(me, eventArgs, command.object, originalCallback);
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extendCommandByDirtyHandling of BaseDataBaseHelper', ex);
          }
        };
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extendCommandByDirtyHandling of BaseDataBaseHelper', ex);
    }
  },

  getErrorTCB: function (callback, scope) {
    var retval = null;

    try {
      retval = function (err) {
        try {
          scope._inTransaction = false;

          scope.logDatabaseError(err);

          Ext.Array.remove(scope._commandCache, scope.getFirstCachedCommandSet());

          Ext.Array.each(scope.getFirstCachedCommandSet(), function (command) {
            if (command.demandId && command.demandId !== "") {
              scope.fireEvent(command.demandId, command.demandId, false);
            }
          });

          if (callback)
            callback();
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside Transaction Error Callback of BaseDataBaseHelper', ex);
        }
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getErrorTCB of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  getCompleteTCB: function (callback, scope) {
    var retval = null;

    try {
      retval = function () {
        try {
          scope._inTransaction = false;

          var transactionsCommandSet = scope.getFirstCachedCommandSet();
          Ext.Array.remove(scope._commandCache, transactionsCommandSet);

          Ext.Array.each(transactionsCommandSet, function (command) {
            if (command.demandId && command.demandId !== "") {
              scope.fireEvent(command.demandId, command.demandId, true);
            }
          });

          if (callback)
            callback.apply();
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside Transaction Complete Callback of BaseDataBaseHelper', ex);
        }
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCompleteTCB of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //use this method to log a database error
  logDatabaseError: function(errorObject) {
    try {
      //errorObject should be a DOM Exception, but if the shimDBs is working on WebSQL there are no DOM Exceptions but an event with a SQL error object
      var isDOMException = errorObject.name !== undefined;

      var errorDetails = isDOMException ? errorObject.name : errorObject.target.error.message;
      var message = "Error processing a database request: " + errorDetails;

      //check if QUOTA error, because logging in this case causes an infinite loop
      var isQuotaError = isDOMException ? errorObject.code === 22 : errorObject.target.error.code === 4;

      if (!isQuotaError) {
        AssetManagement.customer.helper.OxLogger.logException(message, errorObject);
      } else {
        console.log(message);
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('databaseIsFull'), false, 1);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
    } catch (ex) {
      //do not log the exception usually, because this could cause infintie loops - a logging itself will be another database call
      console.log("Error logging a database error!");
      console.log(errorObject);
    }
  },

  // command cache administration
  getFirstCachedCommandSet: function () {
    var commandSetToExecute = null;

    try {
      var theCache = this._commandCache;
      var curLenght = theCache.length;

      for (var i = 0; i < curLenght; i++) {
        if (theCache[i] !== null && theCache[i] !== undefined) {
          commandSetToExecute = theCache[i];
          break;
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFirstCachedCommandSet of BaseDataBaseHelper', ex);
    }
    return commandSetToExecute;
  },

  getLastCommandDemand: function () {
    var retval = null;

    try {
      var theCache = this._commandCache;

      if (theCache != null && theCache != undefined) {
        var lastCommandSet = theCache.pop();
        theCache.push(lastCommandSet);

        if (lastCommandSet != null && lastCommandSet != undefined) {
          retval = lastCommandSet.pop();

          if (retval != null && retval != undefined)
            lastCommandSet.push(retval);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLastCommandDemand of BaseDataBaseHelper', ex);
    }
    return retval;
  },

  getAffectedStoresOfCommandSet: function (commandSet) {
    var affectedStores = new Array();
    var me = this;
    var requiresSyncDeltaStore = false;

    try {
      Ext.Array.forEach(commandSet, function (command) {
        if ((typeof command.targetStores) === 'string') {
          if (!Ext.Array.contains(affectedStores, command.targetStores)) {
            if (me.isDataStorage(command.targetStores))
              requiresSyncDeltaStore = true;

            affectedStores.push(command.targetStores);
          }
        } else if ((typeof command.targetStores) === 'object') {
          Ext.Array.forEach(command.targetStores, function (storeName) {
            if (!Ext.Array.contains(affectedStores, storeName)) {
              if (me.isDataStorage(storeName))
                requiresSyncDeltaStore = true;

              affectedStores.push(storeName);
            }
          }, me);
        }
      }, me);

      if (requiresSyncDeltaStore === true && !Ext.Array.contains(affectedStores, 'S_SYNCDELTA'))
        affectedStores.push('S_SYNCDELTA');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAffectedStoresOfCommandSet of BaseDataBaseHelper', ex);
    }

    return affectedStores;
  },

  //sync only apis
  //returns a raw query command
  getSyncQueryCommand: function (storeNames, keyRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = this.getRawQueryCommand(storeNames, keyRange, successCallback, failureCallback);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncQueryCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //same as getPutCommand, but no delta extension - method does not call base version because of performance issues
  getSyncPutCommand: function (storeNames, object, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = {
        targetStores: storeNames,
        commandType: this.self.COMMAND_TYPES.PUT,
        object: object,
        successCallback: successCallback,
        failureCallback: failureCallback,
        transactionSuccessCallback: null,
        transactionFailureCallback: null
      };
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncPutCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //same as getSyncPutCommand, synced data objects never miss any data, just replace the object with the same key on the store
  getSyncUpdateCommand: function (storeNames, object, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = this.getSyncPutCommand(storeNames, object, successCallback, failureCallback);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncUpdateCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //same as getDeleteCommand, but no delta extension - method does not call base version because of performance issues
  getSyncDeleteCommand: function (storeNames, keyRange, successCallback, failureCallback) {
    var retval = null;

    try {
      retval = this.getRawDeleteCommand(storeNames, keyRange, successCallback, failureCallback);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncDeleteCommand of BaseDataBaseHelper', ex);
    }

    return retval;
  },

  //commits a set of commands to the database
  commitSyncCommandSet: function (arrayOfCommands, successCallback, errorCallback, callbackScope) {
    try {
      if (arrayOfCommands && arrayOfCommands.length > 0) {
        //to hook on the transaction complete event, use the properties of the last command of the set
        //this is currently used for setting transaction callbacks
        var setsLastCommand = arrayOfCommands[arrayOfCommands.length - 1];
        var lastCommandSTCB = setsLastCommand.transactionSuccessCallback;
        var lastCommandETCB = setsLastCommand.transactionFailureCallback;

        var setsTransactionSuccessCallback = function () {
          if (lastCommandSTCB)
            lastCommandSTCB.call(null);

          if (errorCallback)
            successCallback.call(callbackScope);
        };

        setsLastCommand.transactionSuccessCallback = setsTransactionSuccessCallback;

        var setsTransactionErrorCallback = function (err) {
          if (lastCommandETCB)
            lastCommandETCB.call(null, err);

          if (errorCallback)
            errorCallback.call(callbackScope, err);
        };

        setsLastCommand.transactionFailureCallback = setsTransactionErrorCallback;

        this._commandCache.push(arrayOfCommands);
        this.doExecuteCommandQueue();
      } else if (successCallback) {
        successCallback.call(callbackScope);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside commitSyncCommandSet of BaseDataBaseHelper', ex);
    }
  }
});