Ext.define('AssetManagement.base.helper.BaseCursorHelper', {
    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        //public
        getCurrentCursorValue: function (cursor) {
            var retval = null;

            try {
                if (cursor) {
                    retval = cursor.value;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentCursorValue of BaseCursorHelper', ex);
            }

            return retval;
        },

        //public
        doCursorContinue: function (cursor) {
            try {
                if (cursor) {
                    cursor['continue']();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doCursorContinue of BaseCursorHelper', ex);
            }
        },

        //public
        doCursorDelete: function (cursor, callback) {
            var deleteRequest = null;

            try {
                if (cursor) {
                    deleteRequest = cursor['delete']();
                }

                if (callback) {
                    if (deleteRequest) {
                        deleteRequest.onsuccess = function (eventArgs) {
                            callback(true, cursor);
                        };

                        deleteRequest.onfailure = function (eventArgs) {
                            callback(false, cursor);
                        };
                    } else {
                        callback(false);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doCursorDelete of BaseCursorHelper', ex);
            }

            return deleteRequest;
        },

        //public
        doCursorUpdate: function (cursor, updateObject, callback) {
            var updateRequest = null;

            try {
                //sometimes updating fails to unknown reasons
                //this is why the update procedure is applied until is has been successfull
                if (cursor && updateObject) {
                    var isIndex = cursor.source && cursor.source.objectStore ? true : false;
                    var store = isIndex ? cursor.source.objectStore : cursor.source;

                    if (store)
                        updateRequest = store.put(updateObject);
                    else
                        updateRequest = cursor['update'](updateObject);

                    if (updateRequest) {
                        var me = this;

                        var repeatCheck = function (eventArgs) {
                            try {
                                //first re-read the record from the database
                                var readCallback = function (eventArgs) {
                                    try {
                                        var changedSuccessfully = me.reflectsAinB(updateObject, eventArgs.target.result);

                                        if (changedSuccessfully) {
                                            if (callback)
                                                callback(true, cursor);
                                        } else {
                                            var keyString = AssetManagement.customer.utils.StringUtils.concatenate(cursor.primaryKey, ',', false);
                                            AssetManagement.customer.helper.OxLogger.logMessage('Database update failed! Store: ' + store.name + ' || Key: ' + keyString);

                                            me.doCursorUpdate(cursor, updateObject, callback);
                                        }
                                    } catch (ex) {
                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doCursorUpdate of BaseCursorHelper', ex);

                                        if (callback)
                                            callback(false, cursor);
                                    }
                                };

                                var readRequest = store.get(cursor.primaryKey);

                                if (readRequest) {
                                    readRequest.onsuccess = readCallback;

                                    readRequest.onfailure = function (eventArgs) {
                                        //read command could not even be executed
                                        //abort the update and return a negative result
                                        if (callback) {
                                            callback(false, cursor);
                                        }
                                    };
                                } else if (callback) {
                                    //read request could not even be generated, abort the update and return a negative result
                                    callback(false, cursor);
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doCursorUpdate of BaseCursorHelper', ex);

                                if (callback)
                                    callback(false, cursor);
                            }
                        };

                        updateRequest.onsuccess = repeatCheck;

                        updateRequest.onfailure = function (eventArgs) {
                            //update command could not even be executed
                            //abort the update and return a negative result
                            if (callback) {
                                callback(false, cursor);
                            }
                        };
                    } else if (callback) {
                        //update request could not even be generated, abort the update and return a negative result
                        callback(false, cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doCursorUpdate of BaseCursorHelper', ex);

                if (callback)
                    callback(false, cursor);
            }

            return updateRequest;
        },

        //private
        //determines if an object reflects in another by values
        reflectsAinB: function (a, b) {
            var retval = false;

            try {
                if (a && b) {
                    var differenceFound = false;

                    for (var property in a) {
                        if (a.hasOwnProperty(property)) {
                            if (a[property] !== b[property]) {
                                differenceFound = true;
                                break;
                            }
                        }
                    }

                    retval = !differenceFound;
                } else if (!a && !b) {
                    retval = true;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reflectsAinB of BaseCursorHelper', ex);
            }

            return retval;
        }
    }
});