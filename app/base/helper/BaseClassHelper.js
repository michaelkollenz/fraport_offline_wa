﻿Ext.define('AssetManagement.base.helper.BaseClassHelper', {
    requires: [
	    'AssetManagement.customer.utils.StringUtils'
    ],

    inheritableStatics: {
        //public
        getExtSuperClass: function (instance) {
            var retval = null;

            try {
                var clazz = instance;

                while (clazz && clazz.superclass) {
                    if (AssetManagement.customer.utils.StringUtils.startsWith(clazz.superclass.$className, 'Ext.')) {
                        retval = clazz.superclass;
                        break;
                    } else {
                        clazz = clazz.superclass;
                    }
                };
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, 'BaseClassHelper', ex);
            }

            return retval;
        }
    }
});