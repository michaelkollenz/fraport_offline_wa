Ext.define('AssetManagement.base.helper.BaseNetworkHelper', {
	requires: [
	    'Ext.Component',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.AddressManager',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		isClientOnline: function(callback) {
		    try {
		    	var ac = AssetManagement.customer.core.Core.getAppConfig();

                var infoAlias = ac.getGatewayAliasInfo();

                var service = null;

                if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infoAlias)) {
                    service = ac.getGatewayServiceInfo();
				} else {
                    service = infoAlias;
				}

                var resource = ac.getGatewayControllerPing();

                var url = null;

                if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infoAlias)) {
                    url = ac.getSyncProtocol() + "://" + ac.getSyncGateway() + ":" + ac.getSyncGatewayPort()
                        + "/sap" + ac.getSyncGatewayIdent() + service + resource + '?sap-client=' + ac.getMandt();
                } else {
                    url = ac.getSyncProtocol() + "://" + ac.getSyncGateway() + ":" + ac.getSyncGatewayPort()
                        + service + resource;
                }

				Ext.Ajax.request({
                    url: url,
                    scope: this,
                    timeout: 10000, //wait maximum 10sec for IP determination
                    withCredentials: false,
                    useDefaultXhrHeader: false,
                    method: "POST",
                    headers: {
                        'Accept': 'text/*'
                        //do not use content type for json, because this will disable the form field conversion from the params (string)
                        //this would result in all existing calls to be adjusted to work with a complete json object as params
                        //'Content-Type': 'application/json'
                    },
					success: function(response, options) {
                        try {
                        	callback.call(this, true);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isClientOnline of BaseNetworkHelper', ex);
                            callback.call(this, false);
                        }
                    },

                    failure: function(response, options) {
                        try {
                            AssetManagement.customer.helper.OxLogger.logException('Going offline');
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isClientOnline of BaseNetworkHelper', ex);
                        } finally {
                            callback.call(this, false);
                        }
                    }
                });
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isClientOnline of BaseNetworkHelper', ex);
			}
		},
	
		getCurrentIpAddress: function(callback) {
			try {
				var me = this;

				var onlineCallback = function(success) {
					if(!success) {
                        console.log('Failure determining current IP Address: ' + "Browser is offline");
                        callback.call(me, "");
                        return;
					} else {
                        Ext.Ajax.request({
                            url: 'https://api.ipify.org?format=json',
                            scope: me,
                            timeout: 10000, //wait maximum 10sec for IP determination
                            withCredentials: false,
                            useDefaultXhrHeader: false,
                            success: function(response, options) {
                                try {
                                    var result = Ext.decode(response.responseText);
                                    callback.call(me, result.ip);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentIpAddress of BaseNetworkHelper', ex);
                                    callback.call(me, "");
                                }
                            },

                            failure: function(response, options) {
                                try {
                                    console.log('Failure determining current IP Address: ' + response.status);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentIpAddress of BaseNetworkHelper', ex);
                                } finally {
                                    callback.call(me, "");
                                }
                            }
                        });
					}
				}

				this.isClientOnline(onlineCallback);

				/*if(!this.isClientOnline()) {
					console.log('Failure determining current IP Address: ' + "Browser is offline");
					callback.call(this, "");
					return;
				}
				
				Ext.Ajax.request({
					url: 'https://api.ipify.org?format=json',
					scope: this,
					timeout: 10000, //wait maximum 10sec for IP determination
					withCredentials: false,
					useDefaultXhrHeader: false,
					success: function(response, options) {
						try {
							var result = Ext.decode(response.responseText);
							callback.call(this, result.ip);
						} catch (ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentIpAddress of BaseNetworkHelper', ex);
							callback.call(this, "");
						}
					},
		
					failure: function(response, options) {
						try {
							if(!this.isClientOnline())
								console.log('Failure determining current IP Address: ' + "Browser is offline!");
							else	
								console.log('Failure determining current IP Address: ' + response.status);
						} catch (ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentIpAddress of BaseNetworkHelper', ex);
						} finally {
							callback.call(this, "");
						}
					}
				});*/
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentIpAddress of BaseNetworkHelper', ex);
				callback.call(this, "");
			}
		},
		
		openUrl: function (url) {
		    try {
		        var win = window.open(url, '_blank');

		        if (win)
		            win.focus();
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openUrl of BaseNetworkHelper', ex);
		    }
		},

		openAddress: function (address) {
		    try {
		        var googleMapsQueryString = AssetManagement.customer.manager.AddressManager.buildGoogleMapsAddressQuery(address);

		        var appConfig = AssetManagement.customer.core.Core.getAppConfig();
		        var clientLanguage = appConfig ? appConfig.getLocale() : '';
		        var languageToUse = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(clientLanguage) ? 'en' : clientLanguage;

		        var url = 'http://maps.google.com/maps?hl=' + languageToUse + '&q=' + googleMapsQueryString + '&um=1&ie=UTF-8&sa=N&tab=wl';

		        this.openUrl(url);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openAddress of BaseNetworkHelper', ex);
		    }
		},
		
	    //downloads a file from the passed URL using the passed parameters
	    //get parameters can be passed as url encoded extension or using requestParams
	    //requestParams expects an simple javascript object
        //always use the requestParams for necessary credentials
		downloadFile: function(url, requestParams) {
			try {
				var me = this;

				var onlineCallback = function(success) {
					if(!success) {
                        console.log('Failure downloading a file: ' + "Browser is offline");
                        return;
					} else {
                        //always use a new id to not reuse the existing window
                        //this is relevant, if the browser can render the received data inside itself
                        //the behaviour has to stay as former - always a new window for each downloaded file
                        var targetWindowID = (new Date()).getTime();
                        var params = requestParams;
                        var form = document.createElement("form");
                        form.setAttribute("method", "post");
                        form.setAttribute("action", url);
                        form.setAttribute("target", targetWindowID);

                        for (var para in requestParams) {
                            if (requestParams.hasOwnProperty(para)) {
                                var input = document.createElement('input');
                                input.type = 'hidden';
                                input.name = para;
                                input.value = requestParams[para];
                                form.appendChild(input);
                            }
                        }

                        window.open('', targetWindowID);

                        //Add the form to the DOM because opening the form without beeing part of the DOM is not supported in future Browser versions: https://codereview.chromium.org/2416033002/
                        document.body.appendChild(form);

                        form.submit();

                        //Remove the form to cleanup the DOM
                        document.body.removeChild(form);
					}
				}

				this.isClientOnline(onlineCallback)
				/*if(!this.isClientOnline()) {
					console.log('Failure downloading a file: ' + "Browser is offline");
					return;
				}

			    //always use a new id to not reuse the existing window
			    //this is relevant, if the browser can render the received data inside itself
                //the behaviour has to stay as former - always a new window for each downloaded file
				var targetWindowID = (new Date()).getTime();
				var params = requestParams;
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("action", url);
				form.setAttribute("target", targetWindowID);
				
				for (var para in requestParams) {
				    if (requestParams.hasOwnProperty(para)) {
				        var input = document.createElement('input');
				        input.type = 'hidden';
				        input.name = para;
				        input.value = requestParams[para];
				        form.appendChild(input);
				    }
				}
                
				window.open('', targetWindowID);
                
                //Add the form to the DOM because opening the form without beeing part of the DOM is not supported in future Browser versions: https://codereview.chromium.org/2416033002/
				document.body.appendChild(form);

				form.submit();

                //Remove the form to cleanup the DOM
                document.body.removeChild(form);*/
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside downloadFile of BaseNetworkHelper', ex);
			}
		},
		
		logNetworkFailure: function(response) {
			try {
				if(response) {
					var typeCode = response.status;
					
					if(typeCode == 0 && response.timedout) {
						typeCode = 'TIMEOUT';
					}
					
					var target = response.request.options.url;
					
					var isBinaryResponse = response.request.binary;
					
					if(isBinaryResponse)
						message = AssetManagement.customer.utils.StringUtils.getStringFromBytes(response.responseBytes);
					else
						message = response.responseText;
						
					if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
						message = response.statusText;
					
					AssetManagement.customer.helper.OxLogger.logMessage('[NETWORK] A request failed - Statuscode: ' + typeCode + ' - Target: ' + target + ' - ' + 'Message: ' + message, true);
				}
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logNetworkFailure of BaseNetworkHelper', ex);
			}
		}
	}
});