Ext.define('AssetManagement.base.helper.BaseDbKeyRangeHelper', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.utils.StringUtils'
	],
	
	inheritableStatics: {
		//public
		getDbKeyRange: function(storeName, keyAttributesMapLower, keyAttributesMapUpper) {
			var retval = null;
			
			try {
				var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
				
			    //a key range may only to be provided, if the store uses keys
				if (storeInfo.getKeysCount() > 0) {
				    var lowerBound = this.prepareLowerKeyRange(storeInfo, keyAttributesMapLower);
				    var upperBound = this.prepareUpperKeyRange(storeInfo, keyAttributesMapUpper ? keyAttributesMapUpper : keyAttributesMapLower);

				    retval = IDBKeyRange.bound(lowerBound, upperBound);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbKeyRange of BaseDbKeyRangeHelper', ex);
			}
					
			return retval;
		},
		
		getDbOnlyKeyRange: function(storeName, keyAttributesMap) {				
			var retval = null;	
			
			try {		
				var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
				
				var bound = this.prepareLowerKeyRange(storeInfo, keyAttributesMap);			
				
				retval = IDBKeyRange.only(bound);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbOnlyKeyRange of BaseDbKeyRangeHelper', ex);
			}
					
			return retval;
		},
		
		getDbIndexRange: function(storeName, indexName, indexAttributesMapLower, indexAttributesMapUpper) {				
			var retval = null;	
			
		    try {
		        var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
				
				var lowerBound = this.prepareLowerIndexRange(storeInfo, indexName, indexAttributesMapLower);
				var upperBound = this.prepareUpperIndexRange(storeInfo, indexName, indexAttributesMapUpper ? indexAttributesMapUpper : indexAttributesMapLower);			
				
				retval = IDBKeyRange.bound(lowerBound, upperBound);		
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbIndexRange of BaseDbKeyRangeHelper', ex);
			}
					
			return retval;
		},

		getDbOnlyIndexRange: function(storeName, indexName, indexAttributesMap) {				
			var retval = null;	
			
			try {		
				var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
					
				var bound = this.prepareLowerIndexRange(storeInfo, indexName, indexAttributesMapLower);	
				
				retval = IDBKeyRange.only(bound);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbOnlyIndexRange of BaseDbKeyRangeHelper', ex);
			}
					
			return retval;
		},
		
		//this method will extract the key used for a specific store from an object - return value will be a keyrange object
		//if the key is unique the returned keyrange object will be of type "only"
		//if the provided object misses values for some key fields values, a usual keyrange with diffrent bounds will be returned
	    //if some key attributes are not present on that object, null will be returned [empty string === present]
	    //CAUTION: will not work for models using different (case-insensetive) attribute names
		extractStoreKeyOfObject: function(storeName, object, requestAsDirty) {
			var retval = null;	
			
			try {
				var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
				var ac = AssetManagement.customer.core.Core.getAppConfig();
				
				var anyKeyAttributeIsMissing = false;
				var lowerBound = [];
				var upperBound = [];
				var i = 0;
				
				Ext.Array.each(storeInfo.get('keyAttributes'), function(keyAttribute) {
					var value = null;
					
					if('MANDT' === keyAttribute)
					    value = ac.getMandt();
					else if ('SCENARIO' === keyAttribute)
					    value = ac.getScenario();
					else if('USERID' === keyAttribute)
						value = ac.getUserId();
					else if('DIRTY' === keyAttribute)
						value = requestAsDirty ? 'X' : '0';
					else if(object.get)
						value = object.get(keyAttribute.toLowerCase());
					
					if(value === null || value === undefined) {
						value = object[keyAttribute];
					}
					
					if(value === null || value === undefined) {
						anyKeyAttributeIsMissing = true;
						return false;
					}
					
					lowerBound[i] = value;

					//if the key attribute is empty, the upper bound needs it's maximum value
					if(value === '') {
						upperBound[i] = storeInfo.getAttributeDetails(keyAttribute).getMaxValue();
					} else {
						upperBound[i] = lowerBound[i];
					}
					
					i++;
				});
				
				if(anyKeyAttributeIsMissing === false) {
					retval = IDBKeyRange.bound(lowerBound, upperBound);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractStoreKeyOfObject of BaseDbKeyRangeHelper', ex);
			}
			
			return retval;
		},
		
		buildSingleUpdateKeyRangeOutOfObject: function(storeName, object) {
			var retval = null;	
		
			try {
				var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
				var ac = AssetManagement.customer.core.Core.getAppConfig();
				
				var anyKeyAttributeIsMissing = false;
				var key = [];
				var i = 0;
				
				Ext.Array.each(storeInfo.get('keyAttributes'), function(keyAttribute) {
					if('MANDT' === keyAttribute)
					    object['MANDT'] = ac.getMandt();
					else if ('SCENARIO' === keyAttribute)
					    object['SCENARIO'] = ac.getScenario();
					else if('USERID' === keyAttribute)
						object['USERID'] = ac.getUserId();
					else if('DIRTY' === keyAttribute)
						object['DIRTY'] = '0';
						
					var value = object[keyAttribute];
					
					if(value === null || value === undefined) {
						anyKeyAttributeIsMissing = true;
						return false;
					}
					
					key[i] = value;
					
					i++;
				});
				
				if(anyKeyAttributeIsMissing === false) {
					retval = IDBKeyRange.only(key);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSingleUpdateKeyRangeOutOfObject of BaseDbKeyRangeHelper', ex);
			}
			
			return retval;
		},
		
		getDbKeyRangeToWipeUserData: function(storeName) {
			var retval = null;	
			
			try {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(storeName)) {
					var storeInfo = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformation(storeName);
						
					var lowerBound = this.prepareLowerKeyRange(storeInfo);
					var upperBound = this.prepareUpperKeyRange(storeInfo);
					
					//override the default value for "dirty" part of the key
					if(storeInfo.hasAttribute('DIRTY')) {
						var indexOfDirtyKey = 0;
						
						Ext.Array.each(storeInfo.get('keyAttributes'), function(keyAttribute, index) { 
							if('DIRTY' === keyAttribute) {
								indexOfDirtyKey = index
							
								return false;
							}
						});
						
						lowerBound[indexOfDirtyKey] = '';
						upperBound[indexOfDirtyKey] = '�' //ascii 255
					}
					
					retval = IDBKeyRange.bound(lowerBound, upperBound);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbKeyRangeToWipeUserData of BaseDbKeyRangeHelper', ex);
			}
					
			return retval;
		},
		
		//private
		prepareLowerKeyRange: function(storeInfo, keyAttributesMap) {		
			var keyRange = null;
		
			try {
				if(storeInfo) {
					var keyAttributesMapCopy;
					
					//do not use the original map
					if(keyAttributesMap) {
						keyAttributesMapCopy = keyAttributesMap.clone();
					} else {
						keyAttributesMapCopy = Ext.create('Ext.util.HashMap');
					}						
				
					//add missing keys to the key attributes map
					//for lower bound, missing values have to be set to their min value or may be omitted
					//except MANDT as from AppConfig, USERID as from AppConfig and DIRTY as 0
					var allKeyAttributes = storeInfo.get('keyAttributes');
					var value = null;
					
					Ext.Array.each(allKeyAttributes, function(keyName) { 
						if(!keyAttributesMapCopy.containsKey(keyName)) {
							if("MANDT" === keyName) {
								value = AssetManagement.customer.core.Core.getAppConfig().getMandt();
							} else if ("SCENARIO" === keyName) {
							    value = AssetManagement.customer.core.Core.getAppConfig().getScenario();
							} else if ("USERID" === keyName) {
								value = AssetManagement.customer.core.Core.getAppConfig().getUserId();
							} else if("DIRTY" === keyName) {
								value = '0';
							} else {
								value = '';
							}
							
							if(value !== undefined && value !== null) {
								keyAttributesMapCopy.add(keyName, value)
							}
							
							value = null;
						}						
					}, this);
					
					//build the lower bound for the key range
					//the keys order comes from the order of the store info
					//the keys values are drawn from the keyAttributesMap
					keyRange = [];
					var i = 0;
					Ext.Array.each(allKeyAttributes, function(keyName) { 
						keyRange[i] = keyAttributesMapCopy.get(keyName);
						i++;
					}, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareLowerKeyRange of BaseDbKeyRangeHelper', ex);
			}
			
			return keyRange;
		},

		prepareUpperKeyRange: function(storeInfo, keyAttributesMap) {
			var keyRange = null;
			
			try {
				if(storeInfo) {
					var keyAttributesMapCopy;
					
					//do not use the original map
					if(keyAttributesMap) {
						keyAttributesMapCopy = keyAttributesMap.clone();
					} else {
						keyAttributesMapCopy = Ext.create('Ext.util.HashMap');
					}
					
					//add all missing keys to the key attributes map
					//for upper bound missing values have to be set to their max value
					//except MANDT as from AppConfig, USERID as from AppConfig and DIRTY as 0
					var allKeyAttributes = storeInfo.get('keyAttributes');
					var value = null;
					
					Ext.Array.each(allKeyAttributes, function(keyName) { 
						if(!keyAttributesMapCopy.containsKey(keyName)) {
							if('MANDT' === keyName) {
								value = AssetManagement.customer.core.Core.getAppConfig().getMandt();
							} else if ("SCENARIO" === keyName) {
							    value = AssetManagement.customer.core.Core.getAppConfig().getScenario();
							} else if ('USERID' === keyName) {
								value = AssetManagement.customer.core.Core.getAppConfig().getUserId();
							} else if('DIRTY' === keyName) {
								value = '0';
							} else {
								value = storeInfo.getAttributeDetails(keyName).getMaxValue();
							}
							
							if(value !== undefined && value !== null) {
								keyAttributesMapCopy.add(keyName, value);
							}
							
							value = null;
						}						
					}, this);

					//build the upper bound for the key range
					//the keys order comes from the order of the store info
					//the keys values are drawn from the keyAttributesMap
					keyRange = [];
					var i = 0;
					Ext.Array.each(allKeyAttributes, function(keyName) { 
						keyRange[i] = keyAttributesMapCopy.get(keyName);
						i++;
					}, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareUpperKeyRange of BaseDbKeyRangeHelper', ex);
			}
			
			return keyRange;
		},
		
		prepareLowerIndexRange: function(storeInfo, indexName, indexAttributesMap) {		
			var indexRange = null;
		
			try {
				if(storeInfo) {
					var index = storeInfo.getIndex(indexName);
				
					if(index) {
						var indexAttributesMapCopy;
						
						//do not use the original map
						if(indexAttributesMap) {
							indexAttributesMapCopy = indexAttributesMap.clone();
						} else {
							indexAttributesMapCopy = Ext.create('Ext.util.HashMap');
						}						
					
						//add missing index parts to the index attributes map
						//for lower bound, missing values have to be set to their min value or may be omitted
						//except MANDT as from AppConfig, USERID as from AppConfig and DIRTY as 0
						var allIndexAttributes = index.attributes;
						var value = null;
						
						Ext.Array.each(allIndexAttributes, function(attributeName) { 
							if(!indexAttributesMapCopy.containsKey(attributeName)) {
								if("MANDT" === attributeName) {
									value = AssetManagement.customer.core.Core.getAppConfig().getMandt();
								} else if ("SCENARIO" === attributeName) {
								    value = AssetManagement.customer.core.Core.getAppConfig().getScenario();
								} else if ("USERID" === attributeName) {
									value = AssetManagement.customer.core.Core.getAppConfig().getUserId();
								} else if("DIRTY" === attributeName) {
									value = '0';
								} else {
									value = '';
								}
								
								if(value !== undefined && value !== null) {
									indexAttributesMapCopy.add(attributeName, value)
								}
								
								value = null;
							}						
						}, this);
						
						//build the lower bound for the index range
						//the index order comes from the order of the index info
						//the index values are drawn from the indexAttributesMapCopy
						indexRange = [];
						var i = 0;
						Ext.Array.each(allIndexAttributes, function(attributeName) { 
							indexRange[i] = indexAttributesMapCopy.get(attributeName);
							i++;
						}, this);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareLowerIndexRange of BaseDbKeyRangeHelper', ex);
			}
			
			return indexRange;
		},

		prepareUpperIndexRange: function(storeInfo, indexName, indexAttributesMap) {
			var indexRange = null;
			
			try {
				if(storeInfo) {
					var index = storeInfo.getIndex(indexName);
					
					if(index) {
						var indexAttributesMapCopy;
						
						//do not use the original map
						if(indexAttributesMap) {
							indexAttributesMapCopy = indexAttributesMap.clone();
						} else {
							indexAttributesMapCopy = Ext.create('Ext.util.HashMap');
						}
						
						//add all missing index parts to the index attributes map
						//for upper bound missing values have to be set to their max value
						//except MANDT as from AppConfig, USERID as from AppConfig and DIRTY as 0
						var allIndexAttributes = index.attributes;
						var value = null;
						
						Ext.Array.each(allIndexAttributes, function(attributeName) { 
							if(!indexAttributesMapCopy.containsKey(attributeName)) {
								if('MANDT' === attributeName) {
									value = AssetManagement.customer.core.Core.getAppConfig().getMandt();
								} else if ("SCENARIO" === attributeName) {
								    value = AssetManagement.customer.core.Core.getAppConfig().getScenario();
								} else if ('USERID' === attributeName) {
									value = AssetManagement.customer.core.Core.getAppConfig().getUserId();
								} else if('DIRTY' === attributeName) {
									value = '0';
								} else {
									value = storeInfo.getAttributeDetails(attributeName).getMaxValue();
								}
								
								if(value !== undefined && value !== null) {
									indexAttributesMapCopy.add(attributeName, value);
								}
								
								value = null;
							}						
						}, this);
	
						//build the lower bound for the index range
						//the index order comes from the order of the store info
						//the index values are drawn from the indexAttributesMapCopy
						indexRange = [];
						var i = 0;
						Ext.Array.each(allIndexAttributes, function(attributeName) { 
							indexRange[i] = indexAttributesMapCopy.get(attributeName);
							i++;
						}, this);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareUpperIndexRange of BaseDbKeyRangeHelper', ex);
			}
			
			return indexRange;
		}
	}
});