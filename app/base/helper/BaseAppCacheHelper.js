Ext.define('AssetManagement.base.helper.BaseAppCacheHelper', {
	requires: [
	    'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger'
	],

	inheritableStatics: {
		CACHE_STATE: {
			UNCACHED: 0,
			UPTODATE: 1,
			UPDATEREADY: 2,
			UNKNOWN: 3
		},

	    //public
	    //returns, if application caching is active
		isApplicationCachingActive: function () {
		    var retval = false;

		    try {
		        var appcache = window.applicationCache;

		        if (appcache)
		            retval = appcache.status !== appcache.UNCACHED;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isApplicationCachingActive of BaseAppCacheHelper', ex);
		    }

		    return retval;
		},
	
		//public
		//checks, if there is an application update
		//keep in mind, that application updates will be automatically downloaded on a check, so the update can not be rejected
		//callback will be called with the result (boolean) indicating, if there is an update available
		doUpdateReadyCheck: function(callback, callbackScope) {
			if(!callback)
				return;
		
			try {
				//ensure that an application update check will not be run if:
				// - client is offline
				// - application is in uncached state
				// - another check is already running
			
				//get the current application cache state
				var applicationStateCallback = function(cacheState) {
					try {
						if(cacheState === this.CACHE_STATE.UPDATEREADY) {
							//a former run check has already encountered, that an update is ready
							AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] An application update has been downloaded.', true);
							callback.call(callbackScope ? callbackScope : this, true);
						} else if(cacheState === this.CACHE_STATE.UPTODATE) {
							//cache is up-to-date, so the update request can be run

							var online = navigator.getGatewayServiceInfo

							var me = this;
							if(!online) {
								callback.call(callbackScope ? callbackScope : me, false);
								return;
							} else {
								var appCache = window.applicationCache;
								var noUpdateCallback;
								var updateReadyCallback;
								var errorCallback;

								noUpdateCallback = function(event) {
									try {
										//unregister event listener
										appCache.removeEventListener('noupdate', noUpdateCallback);
										appCache.removeEventListener('updateready', updateReadyCallback);
										appCache.removeEventListener('error', errorCallback);

										AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Manual application update check returned with: NO UPDATE', true);
										callback.call(callbackScope ? callbackScope : me, false);
									} catch(ex) {
										AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
										callback.call(callbackScope ? callbackScope : me, false);
									}
								};

								updateReadyCallback = function(event) {
									try {
										//unregister event listener
										appCache.removeEventListener('noupdate', noUpdateCallback);
										appCache.removeEventListener('updateready', updateReadyCallback);
										appCache.removeEventListener('error', errorCallback);

										AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Manual application update check returned with: UPDATE READY', true);
										callback.call(callbackScope ? callbackScope : me, true);
									} catch(ex) {
										AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
										callback.call(callbackScope ? callbackScope : me, false);
									}
								};

								errorCallback = function(event) {
									try {
										//unregister event listener
										appCache.removeEventListener('noupdate', noUpdateCallback);
										appCache.removeEventListener('updateready', updateReadyCallback);
										appCache.removeEventListener('error', errorCallback);

										AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Manual application update check returned with: ERROR', true);
										AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Error message: ' + event.message, true);
										callback.call(callbackScope ? callbackScope : me, false);
									} catch(ex) {
										AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
										callback.call(callbackScope ? callbackScope : me, false);
									}
								};

								//register event listener
								appCache.addEventListener('noupdate', noUpdateCallback);
								appCache.addEventListener('updateready', updateReadyCallback);
								appCache.addEventListener('error', errorCallback);

								//start the update
								appCache.update();
							}
							/*

							 //if the browser is offline, return false
							 if(!AssetManagement.customer.helper.NetworkHelper.isClientOnline()) {
							 	callback.call(callbackScope ? callbackScope : this, false);
							 	return;
							 }

							var appCache = window.applicationCache;
							var noUpdateCallback;
							var updateReadyCallback;
							var errorCallback;
							
							noUpdateCallback = function(event) {
								try {
									//unregister event listener
									appCache.removeEventListener('noupdate', noUpdateCallback);
									appCache.removeEventListener('updateready', updateReadyCallback);
									appCache.removeEventListener('error', errorCallback);
								
									AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Manual application update check returned with: NO UPDATE', true);
									callback.call(callbackScope ? callbackScope : me, false);
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
									callback.call(callbackScope ? callbackScope : me, false);
								}
							};
							
							updateReadyCallback = function(event) {
								try {
									//unregister event listener
									appCache.removeEventListener('noupdate', noUpdateCallback);
									appCache.removeEventListener('updateready', updateReadyCallback);
									appCache.removeEventListener('error', errorCallback);
								
									AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Manual application update check returned with: UPDATE READY', true);
									callback.call(callbackScope ? callbackScope : me, true);
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
									callback.call(callbackScope ? callbackScope : me, false);
								}
							};
							
							errorCallback = function(event) {
								try {
									//unregister event listener
									appCache.removeEventListener('noupdate', noUpdateCallback);
									appCache.removeEventListener('updateready', updateReadyCallback);
									appCache.removeEventListener('error', errorCallback);
								
									AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Manual application update check returned with: ERROR', true);
									AssetManagement.customer.helper.OxLogger.logMessage('[APPCACHE] Error message: ' + event.message, true);
									callback.call(callbackScope ? callbackScope : me, false);
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
									callback.call(callbackScope ? callbackScope : me, false);
								}
							};
							
							//register event listener
							appCache.addEventListener('noupdate', noUpdateCallback);
							appCache.addEventListener('updateready', updateReadyCallback);
							appCache.addEventListener('error', errorCallback);
			
							//start the update
							appCache.update();*/
						} else {
							//application is uncached or is in an unknown state - return false
							callback.call(callbackScope ? callbackScope : this, false);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
						callback.call(callbackScope ? callbackScope : this, false);
					}
				}
				
				this.getApplicationCacheState(applicationStateCallback, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doUpdateReadyCheck of BaseAppCacheHelper', ex);
				callback.call(callbackScope ? callbackScope : this, false);
			}
		},
		
		//will return the application's cache state
		//if the cache is currently in an valotile state, it will be waited for the next non valotile state
		//foundCacheDownloading parameter is for recursion only and part of a workaround
		getApplicationCacheState: function(callback, callbackScope, foundCacheDownloading) {
            try {
                if (!callback)  {
                    return;
                }
				
                var appCache = window.applicationCache;
                var curAPIState = appCache.status;
                var isChecking = curAPIState === appCache.CHECKING;
                var isDownloading = curAPIState === appCache.DOWNLOADING;

                var returnFunction = function() {
                        try {
							transformUpdateToDateToUpdateReady = foundCacheDownloading && AssetManagement.customer.helper.LocalStorageHelper.getSettingsBoolean('HaveIBeenHereBeFore', false);
							
                            var currentState = this.mapHTML5CacheStateToClassState(transformUpdateToDateToUpdateReady);
                            callback.call(callbackScope ? callbackScope : this, currentState);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getApplicationCacheState of BaseAppCacheHelper', ex);
                            callback.call(callbackScope ? callbackScope : this, this.CACHE_STATE.UNKNOWN);
                        }
                    };
                if (isChecking || isDownloading) {
                    var deferTimeForNextPolling = isChecking ? 10 : 500;
					var foundCacheDownloading = foundCacheDownloading || isDownloading;
                    Ext.defer(this.getApplicationCacheState, deferTimeForNextPolling, this, [
                        callback,
                        callbackScope,
                        foundCacheDownloading
                    ]);
                } else {
                    returnFunction.call(this);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getApplicationCacheState of BaseAppCacheHelper', ex);
                callback.call(callbackScope ? callbackScope : this, this.CACHE_STATE.UNKNOWN);
            }
        },
		
		//private
		mapHTML5CacheStateToClassState: function(treatIdleAsUpdateReady) {
			var retval = this.CACHE_STATE.UNKNOWN;
			
			try {
				var appCache = window.applicationCache;
				
				switch (appCache.status) {
					case appCache.UNCACHED: // UNCACHED == 0
						retval = this.CACHE_STATE.UNCACHED;
					    break;
					    
					case appCache.IDLE: // IDLE == 1
						retval = treatIdleAsUpdateReady ? this.CACHE_STATE.UPDATEREADY : this.CACHE_STATE.UPTODATE;
						break;
						
					case appCache.CHECKING: // CHECKING == 2
						retval = this.CACHE_STATE.UNKNOWN;
						break;
						
					case appCache.DOWNLOADING: // DOWNLOADING == 3
						retval = this.CACHE_STATE.UNKNOWN;
						break;
						
					case appCache.UPDATEREADY:  // UPDATEREADY == 4
						retval = this.CACHE_STATE.UPDATEREADY;
						break;
						
					case appCache.OBSOLETE: // OBSOLETE == 5
						retval = this.CACHE_STATE.UNKNOWN;
						break;
						
					default:
						retval = this.CACHE_STATE.UNKNOWN;
						break;
				};
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapHTML5CacheStateToClassState of BaseAppCacheHelper', ex);
			}
			
			return retval;
		}
	}
});