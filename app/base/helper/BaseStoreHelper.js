Ext.define('AssetManagement.base.helper.BaseStoreHelper', {
    requires: [
	    'Ext.data.Store',
	    'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        //will try to merge a array of BaseStores
        //if the stores are not of the same model type, undefined will be returned
        mergeStores: function (arrayOfStores) {
            var retval = null;

            try {
                if (arrayOfStores && arrayOfStores.length > 0) {
                    var arrayOfRelevantStores = new Array();
                    var anyStoreEncountered = false;
                    var firstEncounteredModelType = '';
                    var modelConflict = false;

                    Ext.Array.each(arrayOfStores, function (store) {
                        if (store) {
                            anyStoreEncountered = true;

                            var storesModelType = store.getModel();

                            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(firstEncounteredModelType)) {
                                firstEncounteredModelType = storesModelType;
                            } else if (firstEncounteredModelType !== storesModelType) {
                                modelConflict = true;
                                //break the loop
                                return false;
                            }

                            if (store.getCount() > 0) {
                                arrayOfRelevantStores.push(store);
                            }
                        }
                    }, this);

                    //if more than one stores has been found, merge them
                    //else return the only found store itself
                    if (modelConflict) {
                        retval = undefined;
                    } else if (anyStoreEncountered) {
                        var mergedStore = Ext.create('Ext.data.Store', {
                            model: firstEncounteredModelType,
                            autoLoad: false
                        });

                        if (arrayOfRelevantStores.length > 0) {
                            Ext.Array.each(arrayOfRelevantStores, function (store) {
                                store.each(function (record) {
                                    if (!mergedStore.getById(record.getId()))
                                        mergedStore.add(record);
                                }, this);
                            }, this);

                            retval = mergedStore;
                        }
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mergeStores of BaseStoreHelper', ex);
                retval = null;
            }

            return retval;
        },

        removeRecordFromItsStores: function (record) {
            try {
                var store;

                while (record.store) {
                    store = record.store;
                    toRemove = store.getById(record.get('id'));

                    store.remove(toRemove);
                    store.fireEvent('refresh');
                };
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeRecordFromItsStores of BaseStoreHelper', ex);
            }
        },

        notifyRecordsStoresAboutChangings: function (record) {
            try {
                if (record.store) {
                    //since the stores attribute of any model has been removed by sencha
                    //the only way left is to do a notification by removing the records step by step from it's current store-reference
                    //in the end put the record at the same index again for each affected store

                    //get the stores and put them into an array
                    //have another array with the index where the record once has been stored
                    var storeArray = new Array();
                    var indexArray = new Array();

                    var i = 0;
                    var index;

                    while (record.store) {
                        storeArray.push(record.store);

                        toRemove = record.store.getById(record.get('id'));
                        index = record.store.indexOf(toRemove);

                        indexArray.push(index);

                        record.store.remove(toRemove);
                    };

                    i = 0;
                    var store;

                    for (i; i < storeArray.length; i++) {
                        store = storeArray[i];
                        store.insert(indexArray[i], record);

                        store.fireEvent('refresh');
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifyRecordsStoresAboutChangings of BaseStoreHelper', ex);
            }
        }
    }
});
