﻿Ext.define('AssetManagement.base.helper.BaseAddressHelper', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap'
    ],

    inheritableStatics: {
        //public
        getCityInfoForDisplay: function (IAddress) {
            var retval = '';

            try {
                var city = IAddress.getCity();
                var region = IAddress.getRegion();
                var postalCode = IAddress.getPostalCode();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(region)) {
                    var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();

                    var usesRegion = false;

                    if (funcPara) {
                        var inclRegionPara = funcPara.get('incl_regi_addr_dis');
                        usesRegion = (inclRegionPara && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(inclRegionPara.get('fieldval1')));
                    }

                    if (usesRegion)
                        retval = region + ' ' + postalCode + ', ' + city;
                    else
                        retval = AssetManagement.customer.utils.StringUtils.concatenate([postalCode, city], ' ', true);
                } else {
                    retval = AssetManagement.customer.utils.StringUtils.concatenate([postalCode, city], ' ', true);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCityInfoForDisplay of BaseAddressHelper', ex);
            }

            return retval;
        }
    }
});