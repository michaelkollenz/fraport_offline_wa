Ext.define('AssetManagement.base.helper.BaseLocalStorageHelper', {
	requires: [
		'AssetManagement.customer.utils.StringUtils'
	],
	
	inheritableStatics: {
		isLocalStorageAvailable: function() {
			var retval = false;
		
			try {
				retval = 'localStorage' in window && window['localStorage'] !== null;
			} catch (ex) {
			}
			
			return retval;
		},
		
		getString: function(key) {
	        var retval = '';
	        
	        try {
			    retval = window.localStorage[key];
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getString of BaseLocalStorageHelper', ex);
			}   
			
			return retval;
		},
		
		putString: function(key, newValue) {
	        try {
			    window.localStorage[key] = newValue;
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside putString of BaseLocalStorageHelper', ex);
			}    
		},
		
		getBoolean: function(key, defaultValue) {
	         try {
			    var boolAsString = window.localStorage[key];
			
			    if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(boolAsString)){
				   boolAsString = boolAsString.toLowerCase();
			
				   if(boolAsString === 'true')
					  return true;
				
				   if(boolAsString === 'false')
					  return false;
			    }
			
			    if(defaultValue === undefined)
				   defaultValue = false;
		    } catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBoolean of BaseLocalStorageHelper', ex);
			}
			return defaultValue;
		},
		
		putBoolean: function(key, newValue) {
	       try {
	        	if(typeof newValue != 'boolean')
				return;
		
			    if(newValue)
				   window.localStorage[key] = 'true';
			    else
				   window.localStorage[key] = 'false';
		   } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside putBoolean of BaseLocalStorageHelper', ex);
		   }	   
		},
		
		getNumber: function(key) {
			var retval = -1;
			
			try {
			    retval = parseFloat(window.localStorage[key]);
			    if(isNaN(retval))
				   retval = 0;
		    } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNumber of BaseLocalStorageHelper', ex);
		    }
		    
			return retval;
		},

		putNumber: function(key, newValue) {
	        try {
			    window.localStorage[key] = newValue.toString();
		    } catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside putNumber of BaseLocalStorageHelper', ex);
		    }
		},
				
		getSettingsString: function(key) {
	        var retval = '';
	        
	        try {
			    retval = this.getString('AppPreferences-' + key);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSettingsString of BaseLocalStorageHelper', ex);
	        }
		    
	        return retval;
		},
		
		putSettingsString: function(key, newValue) {
	        try {
			    this.putString('AppPreferences-' + key, newValue);
			} catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside putSettingsString of BaseLocalStorageHelper', ex);
		    }    
		},
		
		getSettingsBoolean: function(key, defaultValue) {
	        var retval = false;
	        
	        try {
			    retval = this.getBoolean('AppPreferences-' + key, defaultValue);
			} catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSettingsBoolean of BaseLocalStorageHelper', ex);
		    } 
			
		    return retval;
		},
		
		putSettingsBoolean: function(key, newValue) {
	        try {
	        	if(typeof newValue == 'string') {
					var value = newValue.toLowerCase();
					
					if(value === 'x' || value === 'true')
						newValue = true;
					else
						newValue = false;
				}			
				this.putBoolean('AppPreferences-' + key, newValue);
			} catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside putSettingsBoolean of BaseLocalStorageHelper', ex);
		    } 	
		},
		
		getSettingsNumber: function(key) {
	        var retval = -1;
	        
	        try {
			    retval = this.getNumber('AppPreferences-' + key);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSettingsNumber of BaseLocalStorageHelper', ex);
	        }
		    
	        return retval;
		},
		
		putSettingsNumber: function(key, newValue) {
	        try {
			    this.putNumber('AppPreferences-' + key, newValue);
			} catch (ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside putSettingsNumber of BaseLocalStorageHelper', ex);
		    }    
		}	
	}
});
