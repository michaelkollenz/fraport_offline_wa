﻿Ext.define('AssetManagement.base.helper.BaseDatabaseContentExtractor', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.DataBaseHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.ObjectStoreHelper',
        'AssetManagement.customer.controller.EventController'
    ],
    inheritableStatics: {
        //crawling database for all stores and building json object
        //returns json string
        extractContentFromDatabase: function () {
            var retval = -1;
		
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                //request the map of database stores
                var map = AssetManagement.customer.core.Core.getDataBaseHelper().getObjectStoreInformationMap();
                var storesToExtract = new Array();

                //loop for stores to extract
                map.each(function (store) {
                    var curStoreName = store.get('storeName');

                    //only extract data stores
                    if (this.isDataStorage(curStoreName)) {
                        storesToExtract.push(store);
                    }
                }, this);
                
                var successCallback = function (databaseJsonRepresentation) {
                    try {
                        //return the result
                        eventController.requestEventFiring(retval, databaseJsonRepresentation);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractContentFromDatabase-successCallback of BaseDatabaseContentExtractor', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                //begin sequence of extracting database content
                this.buildJsonObject(storesToExtract, successCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractContentFromDatabase of BaseDatabaseContentExtractor', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //determines, if a store is a "data" store
        isDataStorage: function (storageName) {
            var retval = false;

            try {
                retval = AssetManagement.customer.utils.StringUtils.startsWith(storageName, "D_") || AssetManagement.customer.utils.StringUtils.startsWith(storageName, "C_") || AssetManagement.customer.utils.StringUtils.startsWith(storageName, "O_");
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isDataStorage of BaseDatabaseContentExtractor', ex);
            }

            return retval;
        },

        //private
        //query for each store in db, assigning object name and filling data
        //returns a json object with store names as attributes and arrays of objects per attribute (storename) for the content
        buildJsonObject: function (stores, callback) {
            if (!callback) {
                return;
            }

            try {
                //return with error, if stores is not supplied
                if (!stores) {
                    callback.call(this, undefined);
                }

                var toReturn = {};            
                var me = this;

                //return, if there is nothing to extract content for
                if (stores.length === 0) {
                    callback.call(this, toReturn);
                    return;
                }

                //declare index as pointer
                var index = 0;

                var extractForStore = function () {
                    try {
                        //extract relevant store name
                        var storeName = stores[index].get('storeName');
                        toReturn[storeName] = new Array();

                        var queryCallback = function (eventArgs) {
                            try {
                                var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                                if (cursor) {
                                    //if cursor is present extract the value and continue with next value
                                    toReturn[storeName].push(cursor.value);
                                    AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                                } else {
                                    //no (more) value for the store available, increment pointer
                                    index++;

                                    //check if done, else continue with next store
                                    if (index === stores.length) {
                                        callback.call(me, toReturn);
                                    } else {
                                        extractForStore.call(me);
                                    }
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildJsonObject-queryCallback of BaseDatabaseContentExtractor', ex);
                                callback.call(me, undefined);
                            }
                        };

                        //request all content for the store by passing no keyrange at all
                        AssetManagement.customer.core.Core.getDataBaseHelper().query(storeName, null, queryCallback);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildJsonObject-extractForStore of BaseDatabaseContentExtractor', ex);
                        callback.call(this, undefined);
                    }
                };

                //begin extracting content for first store
                extractForStore.call(this);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildJsonObject of BaseDatabaseContentExtractor', ex);
                callback.call(this, undefined);
            }
        }
    }
});