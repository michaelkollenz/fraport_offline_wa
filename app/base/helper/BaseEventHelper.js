Ext.define('AssetManagement.base.helper.BaseEventHelper', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		//examines the passed event by passing through it's event hierarchy
		//true will be returned, if any event is found with property button == 2 (w3c specification for right click)
		hasEventBeenCausedByRightClick: function(event) {
			var retval = false;
	
		    try {
		    	var curEvent = event;
		    	
		    	while(curEvent) {
		    		if(curEvent.button == 2) {
		    			retval = true;
		    			break;
		    		} else {
		    			curEvent = curEvent.parentEvent;
		    		}
		    	};
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasEventBeenCausedByRightClick of BaseEventHelper', ex);
			}
			
			return retval;
		},
	
		//examines the passed event by passing through it's event hierarchy
		//true will be returned, if any touch event is been encountered
		hasEventBeenCausedByTouch: function(event) {
			var retval = false;
	
		    try {
		    	var curEvent = event;
		    	
		    	while(curEvent) {
		    		if(curEvent.pointerType === 'touch' || curEvent.pointerType == 2) {
		    			retval = true;
		    			break;
		    		} else {
		    			curEvent = curEvent.parentEvent;
		    		}
		    	};
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasEventBeenCausedByTouch of BaseEventHelper', ex);
			}
			
			return retval;
		},

        //stops the passed event from propagation
        //this cancels later scheduled consumers from triggering
        stopEventPropagation: function (event) {
            var retval = false;

            try {
                if (event) {
                    //IE9 & Other Browsers
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else {
                        //IE8 and Lower
                        if (!event)
                            event = window.event;

                        event.cancelBubble = true;
                    }
                }

                retval = true;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stopEventPropagation of BaseEventHelper', ex);
            }

            return retval;
        }
	}
});