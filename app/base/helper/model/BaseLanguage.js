Ext.define('AssetManagement.base.helper.model.BaseLanguage', {
    extend: 'Ext.data.Model',
 
    fields: [
       { name: 'locale', type: 'string' },
       { name: 'acronym', type: 'string' },
       { name: 'description', type: 'string'}
    ]
});