Ext.define('AssetManagement.base.helper.model.BaseModeHistoryEntry', {
    extend: 'Ext.data.Model',
 
    fields: [
       { name: 'mode', type: 'int' },
       { name: 'argumentsObject', type: 'auto' }
    ]
});