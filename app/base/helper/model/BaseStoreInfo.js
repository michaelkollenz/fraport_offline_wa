Ext.define('AssetManagement.base.helper.model.BaseStoreInfo', {
    extend: 'Ext.data.Model',

    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.OxLogger'
    ],

    fields: [
         { name: 'storeName', type: 'string' },
         { name: 'keyAttributes', type: 'auto' },
         { name: 'attributes', type: 'auto' },
         { name: 'indexes', type: 'auto' }
    ],

    getIndex: function(name) {
		var retval = null;

		try {
		    var myIndexes = this.get('indexes');

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(name) && myIndexes) {
				Ext.Array.each(myIndexes, function(index) {
					if(index.name === name) {
						retval = index;
						return false;
					}
				});
			}
		} catch (ex) {
	       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getIndex of BaseStoreInfo', ex);
		}

		return retval;
	},

    getKeysCount: function() {
    	var retval = 0;

    	try {
    	    var keys = this.get('keyAttributes');

    	    if(keys)
    		   retval = this.get('keyAttributes').length
    	} catch (ex) {
    	    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getKeysCount of BaseStoreInfo', ex);
    	}

    	return retval;
	},

    hasAttribute: function(attribute) {
	   var retval = false;

	   try {
    	   var attributes = this.get('attributes');

    	   if(attributes)
    		  retval = attributes.containsKey(attribute);
       } catch (ex) {
	       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasAttribute of BaseStoreInfo', ex);
       }

       return retval;
	},

	getAttributeDetails: function(attribute) {
	    var retval = null;
	    try {
    	    var attributes = this.get('attributes');

    	    if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(attribute) && attributes)
    		   retval = attributes.get(attribute);
        } catch (ex) {
	       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAttributeDetails of BaseStoreInfo', ex);
        }
        return retval;
	},

    // private chaches
    _attributeNameListCache: null,
    _uploadAttributesCache: null,
    _parsingAttributesCache: null,
    _deleteAttributesCache: null,

    getAttributeNames: function () {
        var retval = null;

	    try {
	        if (!this._attributeNameListCache) {
	            this._attributeNameListCache = new Array();

	    		var myAttributes = this.get('attributes');

	    		if (myAttributes && myAttributes.getCount() > 0) {
	    			myAttributes.each(function(item) {
	    				this._attributeNameListCache.push(item.get('name'));
	    			}, this);
	    		}
	    	}

	    	retval = this._attributeNameListCache;
    	} catch (ex) {
	       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAttributeNames of BaseStoreInfo', ex);
    	}

    	return retval;
    },

    getUploadAttributes: function () {
        var retval = null;

	    try {
	        if (!this._uploadAttributesCache) {
	            this._uploadAttributesCache = new Array();

	    		var myAttributes = this.get('attributes');

	    		if (myAttributes && myAttributes.getCount() > 0) {
	    			myAttributes.each(function(item) {
	    				if(!item.get('suppressUpload'))
	    					this._uploadAttributesCache.push(item.get('name'));
	    			}, this);
	    		}
	    	}

	    	retval = this._uploadAttributesCache;
    	} catch (ex) {
	       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadAttributes of BaseStoreInfo', ex);
    	}

    	return retval;
	},

    getParsingAttributes: function () {
        var retval = null;

        try {
            if (!this._parsingAttributesCache) {
                var myAttributes = this.get('attributes');

                if (myAttributes) {
                    this._parsingAttributesCache = new Array();

                    myAttributes.each(function (item) {
                        if (!item.get('suppressUpload'))
                            this._parsingAttributesCache.push(item.get('name'));
                    }, this);
                }
            }

            retval = this._parsingAttributesCache;


	        /*if (!this._parsingAttributesCache) {
	            this._parsingAttributesCache = new Array();

	            var myAttributes = this.get('attributes');
	            if (myAttributes && myAttributes.getCount() > 0) {
		            myAttributes.each(function(item) {
			            //if(item.get('isKey') || !item.get('suppressUpload'))
				            this._parsingAttributesCache.push(item.get('name'));
		            }, this);
	            }
            }

            retval = this._parsingAttributesCache;*/
		} catch (ex) {
	       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getParsingAttributes of BaseStoreInfo', ex);
		}

	    return retval;
	},

	getSyncDeleteAttributes: function() {
	    var retval = null;

        try {
        	if(this.hasAttribute('UPDFLAG')) {
        	    if (!this._deleteAttributesCache) {
        	        //currently all fields will be uploaded (performance can be increased, when uploading keys and updflag only)
        	        if (true || this.storeRequiresAllAttributesForDelete()) {
        	            this._deleteAttributesCache = this.getUploadAttributes();
        	        } else {
        	            //build the delete values containing keys and updateflag only
        	            var deleteAttributes = new Array();
        	            var attributeCollection = this.get('attributes');
        	            var attributeCount = attributeCollection.getCount();

        	            if (attributeCollection) {
        	                var keyCount = this.get('keyAttributes').length;
        	                var attribute;
        	                var attributeName;
        	                var keysFound = 0;
        	                var i = 0;

        	                while (true) {
        	                    attribute = attributeCollection.getAt(i);
        	                    attributeName = attribute.get('name');

        	                    var isKey = attribute.get('isKey');

        	                    if ((isKey || attributeName === 'UPDFLAG') && !attribute.get('suppressUpload'))
        	                        deleteAttributes.push(attributeName);

        	                    if (isKey)
        	                        keysFound++;

        	                    i++;

        	                    if (keysFound === keyCount || i == attributeCount)
        	                        break;
        	                }
        	            }

        	            this._deleteAttributesCache = deleteAttributes;
        	        }
        	    }

    			retval = this._deleteAttributesCache;
    		}
    	} catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncDeleteAttributes of BaseStoreInfo', ex);
    	}

        return retval;
	},

	storeRequiresAllAttributesForDelete: function () {
	    var retval = false;

	    try {
	        var storeName = this.get('storeName');

	        retval = storeName === 'D_OBJCLASS';
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside storeRequiresAllAttributesForDelete of BaseStoreInfo', ex);
	    }

	    return retval;
	}
});