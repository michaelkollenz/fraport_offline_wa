Ext.define('AssetManagement.base.helper.model.BaseAttributeDetails', {
    extend: 'Ext.data.Model',

    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],

    fields: [
       { name: 'name', type: 'string' },
       { name: 'order', type: 'int' },
       { name: 'isKey', type: 'boolean' },
       { name: 'type', type: 'string' },
       { name: 'length', type: 'int' },
       { name: 'suppressUpload', type: 'boolean' },
       { name: 'filterType', type: 'string' }
    ],

    getMaxValue: function () {
        //build the key attributes max value
        var maxValue = '';

        try {
            var attributeType = this.get('type');
            var attributeLength = this.get('length');

            for (var i = 0; i < attributeLength; i++) {
                if ('n' === attributeType) {
                    maxValue += '9';
                } else if ('an' === attributeType) {
                    maxValue += '\uffff'; //255 of ascii
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaxValue of BaseAttributeDetails', ex);
        }

        return maxValue;
    }
});