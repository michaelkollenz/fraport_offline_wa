﻿Ext.define('AssetManagement.base.helper.process.BaseCompleteMarker', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.ActivityTypeManager',
        'AssetManagement.customer.manager.CustBemotManager',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'Ext.data.Store'
    ],
    
    inheritableStatics: {

    }
});