Ext.define('AssetManagement.base.helper.BaseOxLogger', {
    requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils'
    ],

    inheritableStatics: {
        //public
        MESSAGE_TYPES: {
            INFO: 0,
            ERROR: 1,
            SYNC: 2
        },

        //private
        DEFAULT_DB_LOG_ENTRIES_GUIDING_VALUE: 3000,		//may never be less than sum of kept messages - should be larger than the double of it
        MINIMUM_INFO_MSG_TO_KEEP: 500,
        MINIMUM_ERROR_MSG_TO_KEEP: 50,
        MINIMUM_SYNC_MSG_TO_KEEP: 100,					//~2 Syncs

        _logCache: [],
        _cachingForPersistentTraceEnabled: false,

        //public
        //logs a message to database and console (default)
        //console logging can be surpressed with parameter surpressConsoleLog
        logMessage: function (message, surpressConsoleLog) {
            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                if (!surpressConsoleLog || (ac && ac.isDebugMode()))
                    console.log(AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(new Date(), true, true) + ' --- ' + message);

                var utcNow = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');

                if (this._cachingForPersistentTraceEnabled === true) {
                    this._logCache.push({ message: message, type: this.MESSAGE_TYPES.INFO, time: utcNow })
                } else {
                    var isDataBaseOpen = AssetManagement.customer.core.Core.getDataBaseHelper() ? AssetManagement.customer.core.Core.getDataBaseHelper().isDataBaseOpen() : false;
                    if (isDataBaseOpen)
                        this.writeToDataBase(this.MESSAGE_TYPES.INFO, message, utcNow);
                }
            } catch (ex) {
                this.logException('Exception occurred inside logMessage of BaseOxLogger', ex);
            }
        },

        //public
        //logs a message to database
        //console logging can be forced too with parameter showInConsoleToo
        logSyncMessage: function (message, showInConsoleToo) {
            try {
                var ac = AssetManagement.customer.core.Core.getAppConfig();

                if (showInConsoleToo === true || (ac && ac.isDebugMode()))
                    console.log(AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(new Date(), true, true) + ' --- ' + message);

                var utcNow = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');

                if (this._cachingForPersistentTraceEnabled === true) {
                    this._logCache.push({ message: message, type: this.MESSAGE_TYPES.SYNC, time: utcNow })
                } else {
                    var isDataBaseOpen = AssetManagement.customer.core.Core.getDataBaseHelper() ? AssetManagement.customer.core.Core.getDataBaseHelper().isDataBaseOpen() : false;

                    if (isDataBaseOpen)
                        this.writeToDataBase(this.MESSAGE_TYPES.SYNC, message, utcNow);
                }
            } catch (ex) {
                this.logException('Exception occurred inside logSyncMessage of BaseOxLogger', ex);
            }
        },

        //public
        //logs an exception
        //valid calls are
        //arg1 = function name, arg2 = class name, arg3 = excpetion
        //arg1 = message, arg2 = exception
        //arg1 = exception
        logException: function (arg1, arg2, arg3) {
            try {
                //this method and all of it submethods MAY NOT use the logException in their try catch blocks
                //this may cause infinite loops

                //if the first argument is null or not set at all return
                if (arg1 === undefined || arg1 === null)
                    return;

                var message = '';
                var exception = null;

                var typeOfArg1 = typeof arg1;

                if (typeOfArg1 === 'string') {
                    var typeOfArg2 = typeof arg2;

                    if (typeOfArg2 === 'string') {
                        message = this.buildExceptionLogMessage(arg1, arg2);
                        exception = arg3;
                    } else if (typeOfArg2 === 'object') {
                        message = arg1;
                        exception = arg2;
                    }
                } else if (typeOfArg1 === 'object') {
                    exception = arg1;
                }

                this.logExceptionInternal(message, exception);
            } catch (ex) {
                console.log('Exception occurred inside logException of BaseOxLogger');
                console.log(ex);
            }
        },

        //public
        //clears the console
        clearConsole: function () {
            try {
                var api = null;

                if (console._commandLineAPI) {
                    api = console._commandLineAPI;
                } else if (console._inspectorCommandLineAPI) {
                    api = console._inspectorCommandLineAPI;
                } else {
                    api = console;
                }

                if (typeof api.clear !== 'undefined') {
                    api.clear();
                } else {
                    this.logMessage('Could not clear console. There is no clear method available.');
                }
            } catch (ex) {
                this.logException('Exception occurred inside logSyncMessage of BaseOxLogger', ex);
            }
        },

        //public
        //enables caching for all following log entries
        //use this if you wish to cache all log messages before the persistent trace is enabled
        //it's recommended to enable caching when a lot of trace is going to be written to keep database from pressure - do not to forget to disaable the caching afterwards
        enableCachingForPersistentTrace: function () {
            try {
                this._cachingForPersistentTraceEnabled = true;
            } catch (ex) {
                this.logException('Exception occurred inside enableCachingForPersistentTrace of BaseOxLogger', ex);
            }
        },

        //public
        //disables the caching and will try to write all cached log entries persistently
        //cache will be flushed with this call
        disableCachingForPersistentTrace: function () {
            try {
                this._cachingForPersistentTraceEnabled = false;
                this.flushLogCache();
            } catch (ex) {
                this.logException('Exception occurred inside disableCachingForPersistentTrace of BaseOxLogger', ex);
            }
        },

        //public
        //loads all trace objects from the database
        //return value is an eventId to listen to
        //first and only argument of the event will be an array of the trace objects
        getAllTraceEntries: function () {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var me = this;
                var traceArray = new Array();

                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        if (done) {
                            eventController.fireEvent(retval, traceArray);
                        } else {
                            var cursor = eventArgs.target.result;

                            traceArray.push(cursor.value);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractStackTrace of BaseOxLogger', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_TRACE', null, successCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractStackTrace of BaseOxLogger', ex);
                retval = -1;
            }

            return retval;
        },

        //public
        //will check the current number of log entries stored inside the database
        //if the number exceeded a defined guiding value, a necessary amount of log entries will be deleted, starting with the oldest one
        //finally returns a boolean, indicating, if action has been successful
        manageLogDatabaseSize: function () {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var me = this;
                var guidingValue = this.getDbLogEntriesGuidingValue();

                var countCallback = function (countInfoObject) {
                    try {
                        if (!countInfoObject) {
                            eventController.fireEvent(retval, false);
                        } else if (countInfoObject.allCount < guidingValue) {
                            //nothing to do, log size did not yet exceed the guiding value
                            eventController.fireEvent(retval, true);
                        } else {
                            //log entries exceeded the guiding value
                            //log entries will be reduced
                            var clearCallback = function (success) {
                                try {
                                    if (!success)
                                        success = false;

                                    eventController.fireEvent(retval, success);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLogDatabaseSize of BaseOxLogger', ex);
                                    eventController.fireEvent(retval, false);
                                }
                            };

                            var clearEventId = me.clearLogEntriesFromDatabase(countInfoObject);

                            if (clearEventId > 0) {
                                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(clearEventId, clearCallback);
                            } else {
                                eventController.fireEvent(retval, false);
                            }
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLogDatabaseSize of BaseOxLogger', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var sizeEventId = this.getDatabaseLogCount(true);

                if (sizeEventId > 0) {
                    AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(sizeEventId, countCallback);
                } else {
                    retval = -1;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLogDatabaseSize of BaseOxLogger', ex);
                retval = -1;
            }

            return retval;
        },

        //public
        //clears log entries from database
        //when using countInfoObject parameter, the function will decide what will be deleted and what kept
        //ommitting countInfoObject will clear all entries
        //finally returns a boolean, indicating, if action has been successful
        clearLogEntriesFromDatabase: function (countInfoObject) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (countInfoObject && countInfoObject.deleteCount == 0) {
                    eventController.requestEventFiring(retval, true);
                    return retval;
                }

                var deleteAll = !countInfoObject;

                var me = this;

                var deleteCallback;


                if (deleteAll) {
                    deleteCallback = function (eventArgs) {
                        try {
                            var cursor = eventArgs.target.result;

                            if (!cursor) {
                                eventController.fireEvent(retval, true);
                            } else {
                                AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                                AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearLogEntriesFromDatabase of BaseOxLogger', ex);
                            eventController.fireEvent(retval, false);
                        }
                    };
                } else {
                    var deleteInfoObject = this.determineEntryDeleteParameters(countInfoObject);

                    var infoDeleteCount = 0;
                    var errorDeleteCount = 0;
                    var syncDeleteCount = 0;
                    var deleteCounter = 0;

                    deleteCallback = function (eventArgs) {
                        try {
                            var cursor = eventArgs.target.result;

                            if (!cursor) {
                                eventController.fireEvent(retval, true);
                            } else {
                                //evaluate the deleteInfoObject, if the current value will be deleted or kept
                                var entry = eventArgs.target.result.value;

                                var keepThisEntry = false;

                                switch (entry.type) {
                                    case me.MESSAGE_TYPES.INFO:
                                        keepThisEntry = infoDeleteCount >= deleteInfoObject.maxInfoDeleteCount;
                                        if (!keepThisEntry)
                                            infoDeleteCount++;
                                        break;

                                    case me.MESSAGE_TYPES.ERROR:
                                        keepThisEntry = errorDeleteCount >= deleteInfoObject.maxErrorDeleteCount;
                                        if (!keepThisEntry)
                                            errorDeleteCount++;
                                        break;

                                    case me.MESSAGE_TYPES.SYNC:
                                        keepThisEntry = syncDeleteCount >= deleteInfoObject.maxSyncDeleteCount;
                                        if (!keepThisEntry)
                                            syncDeleteCount++;
                                        break;

                                    default:
                                        break;
                                }

                                if (!keepThisEntry) {
                                    AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                                    deleteCounter++;
                                }

                                if (deleteCounter < deleteInfoObject.deleteCount)
                                    AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                                else
                                    eventController.fireEvent(retval, true);
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearLogEntriesFromDatabase of BaseOxLogger', ex);
                            eventController.fireEvent(retval, false);
                        }
                    };
                }

                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_TRACE', null, deleteCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearLogEntriesFromDatabase of BaseOxLogger', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        //builds the usual default log message for an exception occurance
        //returns a string
        buildExceptionLogMessage: function (functionName, className) {
            var retval = '';

            try {
                //this method MAY NOT use the logException in their try catch block
                //this would cause infinite loops
                if (!functionName)
                    functionName = '[unknown]';

                if (!className)
                    className = '[unknown]';

                retval = 'Exception occurred inside ' + functionName + ' of ' + className;
            } catch (ex) {
                console.log('Exception occurred inside builtExceptionLogMessage of BaseOxLogger');
                console.log(ex);
            }

            return retval;
        },

        //private
        //logs an exception with a given message and exception object
        //either message or exception are mandatory
        logExceptionInternal: function (message, exception) {
            try {
                //this method and all of it submethods MAY NOT use the logException in their try catch blocks
                //this would cause infinite loops
                if (!exception && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message)) {
                    return;
                }

                var utcNow = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');

                //if queueing is currently enabled write it to the log cache
                if (this._queueEnabled === true) {
                    this._logCache.push({ message: message, exception: exception, time: utcNow })
                    return;
                }

                console.log(message);

                if (exception)
                    console.log(exception);

                //transform the exception in a more flat structure for logging
                exception = this.extractNeccessaryInformation(exception);

                //if the message is initial copy the exception message into the message
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                    message = exception.message;

                //if persistent trace is currently not enabled write it to the log cache
                if (this._cachingForPersistentTraceEnabled === true) {
                    this._logCache.push({ message: message, type: this.MESSAGE_TYPES.ERROR, exception: exception, time: utcNow })
                } else {
                    var isDataBaseOpen = AssetManagement.customer.core.Core.getDataBaseHelper() ? AssetManagement.customer.core.Core.getDataBaseHelper().isDataBaseOpen() : false;

                    if (isDataBaseOpen) {
                        this.writeToDataBase(this.MESSAGE_TYPES.ERROR, message, utcNow, exception);
                    }
                }
            } catch (ex) {
                console.log('Exception occurred inside logExceptionInternal of BaseOxLogger');
                console.log(ex);
            }
        },

        //private
        //will try to write each log request inside the queue to the database
        flushLogCache: function () {
            try {
                if (this._logCache && this._logCache.length > 0) {
                    var isDataBaseOpen = AssetManagement.customer.core.Core.getDataBaseHelper() ? AssetManagement.customer.core.Core.getDataBaseHelper().isDataBaseOpen() : false;

                    if (isDataBaseOpen) {
                        Ext.Array.each(this._logCache, function (queuedObject) {
                            if (queuedObject) {
                                this.writeToDataBase(queuedObject.type, queuedObject.message, queuedObject.time, queuedObject.exception, true);
                            }
                        }, this);

                        AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                    } else {
                        //database is not present, so write all messages to the console
                        Ext.Array.each(this._logCache, function (queuedObject) {
                            if (queuedObject) {
                                var message = queuedObject.message;

                                if (queuedObject.exception)
                                    message += 'Exception: ' + queuedObject.exception;

                                console.log(message);
                            }
                        }, this);
                    }
                }

                //clear the cache
                this._logCache = new Array();
            } catch (ex) {
                this.logException('Exception occurred inside flushLogCache of BaseOxLogger', ex);
            }
        },

        //private
        //writes a log entry to the database
        writeToDataBase: function (type, message, time, exception, useBatchProcessing) {
            try {
                var hasTime = !AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(time);

                if (!hasTime)
                    time = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');

                if (exception) {
                    type = this.MESSAGE_TYPES.ERROR;
                } else {
                    exception = null;
                }

                var objectToStore = {
                    type: type,
                    time: AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(time, true),
                    message: message,
                    exception: exception
                };

                AssetManagement.customer.core.Core.getDataBaseHelper().put('S_TRACE', objectToStore, null, null, useBatchProcessing);
            } catch (ex) {
                console.log('Exception occurred inside writeToDataBase of BaseOxLogger');
                console.log(ex);
            }
        },

        //private
        //extracts information from a exception object
        //returns an json object with properties: message, stack, name, time, code
        extractNeccessaryInformation: function (exceptionObject) {
            var retval = null;

            try {
                if (exceptionObject && ((typeof exceptionObject) === 'object')) {
                    retval = {};

                    retval.message = exceptionObject.message;

                    if (!retval.message)
                        retval.message = exceptionObject._message;

                    if (exceptionObject.stack) {
                        retval.stack = exceptionObject.stack;
                    }

                    if (exceptionObject.name) {
                        retval.type = exceptionObject.name;
                    }

                    if (exceptionObject.time) {
                        retval.time = exceptionObject.time;
                    }

                    if (exceptionObject.code) {
                        retval.code = exceptionObject.code;
                    }
                }
            } catch (ex) {
                console.log('Exception occurred inside extractNeccessaryInformation of BaseOxLogger');
                console.log(ex);
            }

            return retval;
        },

        //private
        getDbLogEntriesGuidingValue: function () {
            var retval = this.DEFAULT_DB_LOG_ENTRIES_GUIDING_VALUE;

            try {
                //avoid wrong configurations via constants
                var minimumByConstants = 2 * (this.MINIMUM_INFO_MSG_TO_KEEP + this.MINIMUM_ERROR_MSG_TO_KEEP + this.MINIMUM_SYNC_MSG_TO_KEEP);

                if (retval < minimumByConstants)
                    retval = minimumByConstants;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbLogEntriesGuidingValue of BaseOxLogger', ex);
            }

            return retval;
        },

        //private
        //check database for count of stored log entries
        //requesting detailedInformation will effect the type of return value
        //finally returns a number, -1 if an error occured, or
        //an object of structure { allCount, infoCount, errorCount, syncCount }, undefined if an error occured
        getDatabaseLogCount: function (getDetailedInfo) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                getDetailedInfo = getDetailedInfo === true;

                var me = this;
                var allCount = 0;
                var infoCount = 0;
                var errorCount = 0;
                var syncCount = 0;

                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        if (done) {
                            var toReturn;

                            if (getDetailedInfo) {
                                toReturn = {
                                    allCount: allCount,
                                    infoCount: infoCount,
                                    errorCount: errorCount,
                                    syncCount: syncCount
                                };
                            } else {
                                toReturn = allCount;
                            }

                            eventController.fireEvent(retval, toReturn);
                        } else {
                            allCount++;

                            if (getDetailedInfo) {
                                var entry = eventArgs.target.result.value;
                                switch (entry.type) {
                                    case me.MESSAGE_TYPES.INFO:
                                        infoCount++;
                                        break;
                                    case me.MESSAGE_TYPES.ERROR:
                                        errorCount++;
                                        break;
                                    case me.MESSAGE_TYPES.SYNC:
                                        syncCount++;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(eventArgs.target.result);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDatabaseLogCount of BaseOxLogger', ex);
                        eventController.fireEvent(retval, getDetailedInfo ? undefined : -1);
                    }
                };

                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_TRACE', null, successCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDatabaseLogCount of BaseOxLogger', ex);
                retval = -1;
            }

            return retval;
        },

        //private
        determineEntryDeleteParameters: function (countInfoObject) {
            var retval = null;

            try {
                var maxInfoDeleteCount = 0;
                var maxErrorDeleteCount = 0;
                var maxSyncDeleteCount = 0;
                var deleteCount = 0;

                var targetCount = Math.floor(this.getDbLogEntriesGuidingValue() * 0.8);
                deleteCount = countInfoObject.allCount - targetCount;

                maxInfoDeleteCount = countInfoObject.infoCount > this.MINIMUM_INFO_MSG_TO_KEEP ? (countInfoObject.infoCount - this.MINIMUM_INFO_MSG_TO_KEEP) : 0;
                maxErrorDeleteCount = countInfoObject.errorCount > this.MINIMUM_ERROR_MSG_TO_KEEP ? (countInfoObject.errorCount - this.MINIMUM_ERROR_MSG_TO_KEEP) : 0;
                maxSyncDeleteCount = countInfoObject.syncCount > this.MINIMUM_SYNC_MSG_TO_KEEP ? (countInfoObject.syncCount - this.MINIMUM_SYNC_MSG_TO_KEEP) : 0;

                retval = {
                    deleteCount: deleteCount,
                    maxInfoDeleteCount: maxInfoDeleteCount,
                    maxErrorDeleteCount: maxErrorDeleteCount,
                    maxSyncDeleteCount: maxSyncDeleteCount
                };
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineEntryCountToDelete of BaseOxLogger', ex);
            }

            return retval;
        }
    }
});