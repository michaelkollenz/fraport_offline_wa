Ext.define('AssetManagement.base.helper.BaseLanguageHelper', {

	requires: [
	    'AssetManagement.customer.helper.model.Language',
	    'Ext.data.Store',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		getAvailableLanguages: function() {
			var retval = null;
		
			try {
				retval = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.helper.model.Language',
					autoLoad: false
				});
				
				retval.add(Ext.create('AssetManagement.customer.helper.model.Language', {
					locale: 'de-DE',
					acronym: 'DE',
					description: Locale.getMsg('german')
				}));
				
				retval.add(Ext.create('AssetManagement.customer.helper.model.Language', {
					locale: 'en-US',
					acronym: 'EN',
					description: Locale.getMsg('english')
				}));
				
				return retval;
			} catch (ex) {
				retval = null;
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAvailableLanguages of BaseLanguageHelper', ex);
			}
			
			return retval;
		}
	}
});
	