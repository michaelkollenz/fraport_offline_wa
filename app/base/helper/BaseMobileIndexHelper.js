Ext.define('AssetManagement.base.helper.BaseMobileIndexHelper', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.helper.DbKeyRangeHelper',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap'
    ],

    //this class provides unique numbers for stores
    //it uses a sequence for this
    //most complex part of this class is a mechanism to avoid returning the first number multiple times (case of sequence creation for a store)
    //this issue is caused by database asynchronism

    //maybe some web browsers may cause returning a number multiple times (depending on it's indexedDB implementation)
    //to avoid it, the mutex logic has to be used before dropping the get request on a store's sequence
 
    inheritableStatics: {
        //public
        getNextMobileIndex: function(storeName, useBatchProcessing) {
            var retval = -1;
   
            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var retval = eventController.getNextEventId();
    
                if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(storeName)) {
                    eventController.requestEventFiring(retval, undefined);
                    return retval;
                }
    
                var me = this;
                var successCallback = function(eventArgs) {
                    try {
                        var nextMobileIndex = 0;
      
                        if(eventArgs && eventArgs.target && eventArgs.target.result && eventArgs.target.result.value) {
                            //store's sequence exists, look up it's value and increment it
                            var cursor = eventArgs.target.result;
                            var entry = cursor.value;
      
                            nextMobileIndex = parseInt(entry['INDEXVALUE']) + 1;
        
                            entry['INDEXVALUE'] = nextMobileIndex;
        
                            AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, entry);
       
                            eventController.fireEvent(retval, nextMobileIndex);
                        } else {
                            //could not get the store's sequence
                            //check, if the mutex for generating store's sequence is already set
                            var mutexForStore = me.getMutexForStore(storeName);

                            if (!mutexForStore) {
                                //did not get the mutex, so the sequence generation for this store is still pending
                                //request another get for the sequence
                                //a time shift is not necessary, because the request will be definitely processed after the pending sequence generation
                                var toWaitFor = me.getNextMobileIndex(storeName, false);

                                if (toWaitFor > -1) {
                                    var retrialCallback = function (nextNumber) {
                                        try {
                                            eventController.fireEvent(retval, eventArgs.type === "success" ? nextNumber : undefined);
                                        } catch (ex) {
                                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMobileIndex of BaseMobileIndexHelper', ex);
                                            eventController.fireEvent(retval, undefined);
                                        }
                                    };

                                    eventController.registerOnEventForOneTime(toWaitFor, retrialCallback);
                                } else {
                                    eventController.fireEvent(retval, undefined);
                                }
                            } else {
                                //mutex acquired, sequence can be generated - set mutex blocked to mark mutex as acquired 
                                mutexForStore.blocked = true;

                                var callback = function (eventArgs) {
                                    try {
                                                        //setting back the mutex is not necessary, because it may never be reacquired since the sequence will never be deleted on runtime
                                        //mutexForStore.blocked = false;

                                        eventController.fireEvent(retval, eventArgs.type === "success" ? 1 : undefined);
                                    } catch (ex) {
                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMobileIndex of BaseMobileIndexHelper', ex);
                                        eventController.fireEvent(retval, undefined);
                                    }
                                };

                                var toSave = {
                                    STORENAME: storeName,
                                    INDEXVALUE: '1'
                                };

                                AssetManagement.customer.core.Core.getDataBaseHelper().put('S_MOBILE_INDEXES', toSave, callback, callback);
                            }
                        }
                    } catch(ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMobileIndex of BaseMobileIndexHelper', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };
   
                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('STORENAME', storeName);
   
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('S_MOBILE_INDEXES', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_MOBILE_INDEXES', keyRange, successCallback, null, useBatchProcessing);
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextMobileIndex of BaseMobileIndexHelper', ex);
                retval = -1;
            }
   
            return retval;
        },

        //private
        _mutexMap: null,

        getMutexMap: function () {
            var retval = null;

            try {
                if (!this._mutexMap) {
                    this._mutexMap = Ext.create('Ext.util.HashMap');
                }

                //the mutex map has to be internally structured for mandt - scenario - userid to avoid conflicts
                var ac = AssetManagement.customer.core.Core.getAppConfig();
                var specifcKey = ac.getMandt() + ac.getScenario() + ac.getUserId();
                var specificMutexMap = this._mutexMap.get(specifcKey);

                if (!specificMutexMap) {
                    specificMutexMap = Ext.create('Ext.util.HashMap');
                    this._mutexMap.add(specifcKey, specificMutexMap);
                }

                retval = specificMutexMap;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMutexMap of BaseMobileIndexHelper', ex);
            }

            return retval;
        },

        getMutexForStore: function (storeName) {
            var retval = null;

            try {
                var mutexMap = this.getMutexMap();

                var mutex = null;

                if (mutexMap) {
                    mutex = mutexMap.get(storeName);
                }

                if(!mutex) {
                    mutex = { blocked: false };

                    if (mutexMap)
                        mutexMap.add(storeName, mutex);
                }

                if (!mutex.blocked) {
                    retval = mutex;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMutexForStore of BaseMobileIndexHelper', ex);
            }

            return retval;
        }
    }
});



