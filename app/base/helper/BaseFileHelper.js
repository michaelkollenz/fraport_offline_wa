Ext.define('AssetManagement.base.helper.BaseFileHelper', {
    requires: [
		'AssetManagement.customer.model.bo.File',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.UserAgentInfoHelper'
    ],

    inheritableStatics: {
        DEFAULT_MAX_FILE_SIZE: 2,
        DEFAULT_IMG_WIDTH: 1024,
        DEFAULT_IMG_HEIGHT: 768,

        //public
        isFileApiAvailable: function () {
            return window.File && window.FileReader && window.FileList;
        },

        //will try to generate a file record for a filehandle
        //return value will be of type AssetManagement.customer.model.bo.File
        getFileFromFileHandle: function (fileHandle, callback, callbackScope) {
            if (!callback)
                return;

            try {
                if (!fileHandle || !this.isFileApiAvailable()) {
                    callback.call(callbackScope, null);
                    return;
                }

                var maxFileSizeInMB = this.DEFAULT_MAX_FILE_SIZE;
                var maxFileSizeFuncPara = null;
                var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();

                if (funcPara) {
                    var valueAsString = funcPara.getValue1For('attach_max_file_size');

                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(valueAsString)) {
                        maxFileSizeInMB = parseInt(valueAsString);
                    }
                }

                var maxFileSizeInByte = maxFileSizeInMB * 1048576;

                var isImage = this.isImageFileHandle(fileHandle);

                //do not allow files larger than a specific limit
                //this is skipped for images, because images will get downscaled instead of running into the size check
                if (!isImage) {
                    if (fileHandle.size > maxFileSizeInByte) {
                        var message = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('sizeOfFileToBigLimitIs'), [maxFileSizeInMB]);

                        var alertDialogConfig = {
                            message: message
                        };

                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ALERT_DIALOG, alertDialogConfig);
                        callback.call(callbackScope, null);

                        return;
                    }
                }

                var me = this;

                //next read the files content
                //when this is done, build a record of type AssetManagement.customer.model.bo.File and return it via the passed callback
                var builderCallback = function (fileContent) {
                    try {
                        //put together the file record's information
                        var file = me.buildFileRecord(fileHandle, fileContent);

                        if (!file) {
                            //error case
                            callback.call(callbackScope, undefined);
                            return;
                        }

                        //in case of an image extra steps are required:
                        //1. rotate the image to the correct orientation
                        //2. scale down the size to avoid to heavy images
                        if (isImage) {
                            //first make sure the image is rotated, if necessarry
                            var rotationCallback = function (rotatedImage) {
                                try {
                                    //handle a not set return value as error case
                                    if (!rotatedImage) {
                                        callback.call(callbackScope, undefined);
                                        return;
                                    }

                                    var resizeCallback = function (resizedImage) {
                                        try {
                                            //handle a not set return value as error case
                                            if (!resizedImage)
                                                resizedImage = undefined;

                                            callback.call(callbackScope, resizedImage);
                                        } catch (ex) {
                                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFileFromFileHandle-rotationCallback of BaseFileHelper', ex);
                                            callback.call(callbackScope, undefined);
                                        }
                                    };

                                    me.resizeImageIfNecessary(rotatedImage, resizeCallback);
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFileFromFileHandle-rotationCallback of BaseFileHelper', ex);
                                    callback.call(callbackScope, undefined);
                                }
                            };

                            me.rotateImageIfNecessary(file, rotationCallback);
                        } else {
                            //no extra treatments necessary
                            callback.call(callbackScope, file);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFileFromFileHandle-builderCallback of BaseFileHelper', ex);
                        callback.call(callbackScope, undefined);
                    }
                };

                //call the file reading method
                me.readFileContent.call(me, fileHandle, builderCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFileFromFileHandle of BaseFileHelper', ex);
                callback.call(callbackScope, undefined);
            }
        },

        arrayBufferToBase64: function (buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);

            var len = bytes.byteLength;

            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }

            return window.btoa(binary);
        },

        //tries to open a file, so the browser will show it or display it
        openFile: function (file) {
            try {
                if (!file || !file.get('contentAsArrayBuffer')) {
                    return;
                }

                //get the target type
                var mimeType = this.getMimeTypeOfFile(file);

                var blob = new Blob([file.get('contentAsArrayBuffer')], { type: mimeType });
                var objectURL = URL.createObjectURL(blob);
                AssetManagement.customer.helper.NetworkHelper.openUrl(objectURL);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openFile of BaseFileHelper', ex);
            }
        },

        getMimeTypeOfFile: function (file) {
            if (!file)
                return '';

            var retval = 'application/octet-stream';

            try {
                var fileType = file.get('fileType');

                fileType = fileType.toUpperCase();

                if ('JPG' === fileType || 'JEPG' === fileType) {
                    retval = 'image/jpeg';
                } else if ('BMP' === fileType) {
                    retval = 'image/bmp';
                } else if ('GIF' === fileType) {
                    retval = 'image/gif';
                } else if ('PNG' === fileType) {
                    retval = 'image/png';
                } else if ('PDF' === fileType) {
                    retval = 'application/pdf';
                } else if ('TXT' === fileType) {
                    retval = 'text/plain;charset=utf-8';
                } else if ('ZIP' === fileType) {
                    retval = 'application/zip';
                } else if ('MP3' === fileType) {
                    retval = 'audio/mpeg3';
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMimeTypeOfFile of BaseFileHelper', ex);
                retval = 'application/octet-stream';
            }

            return retval;
        },

        //public
        //converts an array buffers content into a base64 string
        getBase64StringForFile: function (file) {
            var retval = '';

            try {
                if (file) {
                    var mimeType = this.getMimeTypeOfFile(file);

                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mimeType)) {
                        var prefix = 'data:' + mimeType + ';base64,';

                        var arrayBuffer = file.get('contentAsArrayBuffer');

                        var base64 = prefix;
                        var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

                        var bytes = new Uint8Array(arrayBuffer);
                        var byteLength = bytes.byteLength;
                        var byteRemainder = byteLength % 3;
                        var mainLength = byteLength - byteRemainder;

                        var a, b, c, d;
                        var chunk;

                        // Main loop deals with bytes in chunks of 3
                        for (var i = 0; i < mainLength; i = i + 3) {
                            // Combine the three bytes into a single integer
                            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

                            // Use bitmasks to extract 6-bit segments from the triplet
                            a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
                            b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
                            c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
                            d = chunk & 63;               // 63       = 2^6 - 1

                            // Convert the raw binary segments to the appropriate ASCII encoding
                            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
                        }

                        // Deal with the remaining bytes and padding
                        if (byteRemainder == 1) {
                            chunk = bytes[mainLength];

                            a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

                            // Set the 4 least significant bits to zero
                            b = (chunk & 3) << 4; // 3   = 2^2 - 1

                            base64 += encodings[a] + encodings[b] + '==';
                        } else if (byteRemainder == 2) {
                            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

                            a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
                            b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4

                            // Set the 2 least significant bits to zero
                            c = (chunk & 15) << 2; // 15    = 2^4 - 1

                            base64 += encodings[a] + encodings[b] + encodings[c] + '=';
                        }

                        retval = base64;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBase64StringForFile of BaseFileHelper', ex);
            }

            return retval;
        },

        //private
        //will use a file handle to read the file content asynchronously
        readFileContent: function (fileHandle, callback) {
            if (!callback)
                return;

            try {
                var reader = new FileReader();

                reader.onloadend = function (args) {
                    try {
                        var fileContent = null;

                        if (args && args.target && args.target.result)
                            fileContent = args.target.result;

                        callback(fileContent);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside readFileContent of BaseFileHelper', ex);
                        callback(undefined);
                    }
                };

                reader.readAsArrayBuffer(fileHandle);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside readFileContent of BaseFileHelper', ex);
                callback(undefined);
            }
        },

        //generates a file instance, using the provided information
        buildFileRecord: function (fileHandle, fileContent) {
            var retval = null;

            try {
                if (fileHandle) {
                    var id = fileHandle.name.substring(0, 4) + AssetManagement.customer.utils.DateTimeUtils.getCurrentTime().getTime();

                    //use the file name without suffix as description
                    var desc = fileHandle.name;

                    var parts = desc.split('.');

                    if (parts.length > 1) {
                        var lastpart = parts[parts.length - 1];
                        desc = desc.substring(0, fileHandle.name.length - (lastpart.length + 1));
                    }

                    var fileType = this.getFileTypeFromCompleteFileName(fileHandle.name);

                    retval = Ext.create('AssetManagement.customer.model.bo.File', {
                        fileName: fileHandle.name,
                        fileSize: fileHandle.size / 1024.0,
                        fileType: fileType,
                        fileOrigin: 'local',
                        fileDescription: desc,
                        createdAt: fileHandle.lastModifiedDate,
                        lastModified: fileHandle.lastModifiedDate
                    });

                    retval.set('contentAsArrayBuffer', fileContent);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFileRecord of BaseFileHelper', ex);
            }

            return retval;
        },

        getFileTypeFromCompleteFileName: function (completeFileName) {
            var retval = '';

            try {
                var fileExtention = this.getFileExtention(completeFileName);
                fileExtention = fileExtention.toUpperCase();

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fileExtention)) {
                    retval = 'MEB';
                } else if ('JPG' === fileExtention || 'JPEG' === fileExtention) {
                    retval = 'JPG';
                } else if ('BMP' === fileExtention) {
                    retval = fileExtention;
                } else if ('GIF' === fileExtention) {
                    retval = fileExtention;
                } else if ('PDF' === fileExtention) {
                    retval = fileExtention;
                } else if ('PNG' === fileExtention) {
                    retval = fileExtention;
                } else if ('TXT' === fileExtention) {
                    retval = fileExtention;
                } else if ('ZIP' === fileExtention) {
                    retval = fileExtention;
                } else if ('MP3' === fileExtention) {
                    retval = fileExtention;
                } else {
                    retval = 'MEB';
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFileTypeFromFullName of BaseFileHelper', ex);
                retval = '';
            }

            return retval;
        },

        getFileExtention: function (completeFileName) {
            var retval = '';

            try {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(completeFileName)) {
                    var parts = completeFileName.split('.');

                    if (parts.length > 1) {
                        retval = parts[parts.length - 1];
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFileExtention of BaseFileHelper', ex);
                retval = '';
            }

            return retval;
        },

        //transforms an array buffer into an extended ascii representation
        encodeArrayBufferAsAsciiString: function (arrayBuffer) {
            var retval = '';

            try {
                if (arrayBuffer && arrayBuffer.byteLength > 0) {
                    var bufView = new Uint8Array(arrayBuffer);

                    for (var i = 0; i < bufView.length; i++) {
                        var uint8 = bufView[i];

                        retval += String.fromCharCode(uint8);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside encodeArrayBufferAsAsciiString of BaseFileHelper', ex);
                retval = null;
            }

            return retval;
        },

        //return value will be an uint 8 array - works with ascii strings only
        //null will be returned, if any char with charcode larger than 255 is encountered
        decodeArrayBufferFromAsciiString: function (stringToDecode) {
            var retval = null;

            try {
                if (typeof (stringToDecode) === 'string' && stringToDecode.length > 0) {
                    var arrayBuffer = new ArrayBuffer(stringToDecode.length);
                    var view = new Uint8Array(arrayBuffer);

                    var cancelled = false;

                    for (var i = 0, strLen = stringToDecode.length; i < strLen; i++) {
                        var charCode = stringToDecode.charCodeAt(i);

                        if (charCode < 256) {
                            view[i] = charCode;
                        } else {
                            cancelled = false;
                            break;
                        }
                    }

                    if (!cancelled)
                        retval = arrayBuffer;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside decodeArrayBufferFromAsciiString of BaseFileHelper', ex);
                retval = null;
            }

            return retval;
        },

        arrayBufferToBase64: function (byteArray, mimeType) {
            var retval = '';
            try {
                var prefix = 'data:' + mimeType + ';base64,';

                var buffer = byteArray;
                var binary = '';
                var bytes = new Uint8Array(buffer);
                var len = bytes.byteLength;

                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode(bytes[i]);
                }

                retval = prefix + window.btoa(binary);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside arrayBufferToBase64 of BaseFileHelper', ex);
            }
            return retval;
        },

        //extremly needed method
        dataURLToBlob: function (dataURL) {
            try {
                var BASE64_MARKER = ';base64,';
                if (dataURL.indexOf(BASE64_MARKER) == -1) {
                    var parts = dataURL.split(',');
                    var contentType = parts[0].split(':')[1];
                    var raw = parts[1];

                    return new Blob([raw], { type: contentType });
                }

                var parts = dataURL.split(BASE64_MARKER);
                var contentType = parts[0].split(':')[1];
                var raw = window.atob(parts[1]);
                var rawLength = raw.length;

                var uInt8Array = new Uint8Array(rawLength);

                for (var i = 0; i < rawLength; ++i) {
                    uInt8Array[i] = raw.charCodeAt(i);
                }

                return new Blob([uInt8Array], { type: contentType });
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataURLToBlob of BaseFileHelper', ex);
            }

        },

        //public
        //resizes a passed image, if it is larger than the requested size
        //upscaling is not supported
        resizeIMG: function (boFile, width, height, returnNewObject, mimeType, callback) {
            if (!callback) {
                return;
            }

            try {
                var me = this;
                var newFile = null;

                if (returnNewObject) {
                    newFile = Ext.create('AssetManagement.customer.model.bo.File', {
                        'fileId': boFile.get('fileId'),
                        'fileName': boFile.get('fileName'),
                        'fileType': boFile.get('fileType'),
                        'fileOrigin': boFile.get('fileOrigin'),
                        'fileDescription': boFile.get('fileDescription'),
                        'createdAt': AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(),
                        'lastModified': AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(),
                        'contentAsArrayBuffer': boFile.get('contentAsArrayBuffer')
                    });
                } else {
                    newFile = boFile;
                }

                //check if file is already base64 encoded - if not encode it as base64
                var fileEncodedAsBase64 = this.arrayBufferToBase64(newFile.get('contentAsArrayBuffer'), this.getMimeTypeOfFile(newFile));

                if (!fileEncodedAsBase64) {
                    callback(undefined);
                    return;
                }

                var image = new Image();

                var imageLoadedCallback = function (imageEvent) {
                    try {
                        var oldWidth = image.width;
                        var oldHeight = image.height;

                        //determine the required resizing factor (will only scale down)
                        var resizingFactor = 1.0;

                        if (oldHeight > height)
                            resizingFactor = parseFloat(height) / oldHeight;

                        if (oldWidth > width) {
                            var resizingFactorWidth = parseFloat(width) / oldWidth;

                            if (resizingFactorWidth < resizingFactor) {
                                resizingFactor = resizingFactorWidth;
                            }
                        }

                        var newWidth = oldWidth * resizingFactor;
                        var newHeight = oldHeight * resizingFactor;

                        //check if resizing is not necessary
                        if (resizingFactor === 1.0) {
                            callback(newFile);
                            return;
                        }

                        // resize the image
                        var canvas = document.createElement('canvas');

                        canvas.width = newWidth;
                        canvas.height = newHeight;
                        var ctx = canvas.getContext('2d')

                        ctx.drawImage(image, 0, 0, newWidth, newHeight);

                        var dataUrl = canvas.toDataURL(mimeType);
                        var resizedImage = me.dataURLToBlob(dataUrl);

                        //convert blob to byte array
                        var fileReader = new FileReader();

                        var resizeOnLoadCallback = function () {
                            try {
                                newFile.set('fileSize', resizedImage.size / 1024.0);
                                newFile.set('contentAsArrayBuffer', this.result);
                                callback(newFile);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resizeIMG-resizeOnLoadCallback of BaseFileHelper', ex);
                                callback(undefined);
                            }
                        };

                        fileReader.onload = resizeOnLoadCallback;
                        fileReader.readAsArrayBuffer(resizedImage);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resizeIMG-imageLoadedCallback of BaseFileHelper', ex);
                        callback(undefined);
                    }
                };

                image.onload = imageLoadedCallback;
                image.src = fileEncodedAsBase64;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resizeIMG of BaseFileHelper', ex);
                callback(undefined);
            }
        },

        //public
        //rotates an image passed encapsulated as a business object file instance
        //factor is taken multiple of 90 degrees (no fractions allowed)
        //returnNewObject controls, if the passed object is transformed or a new file is created
        rotateImage: function (boFile, factor, returnNewObject, callback) {
            if (!callback)
                return;

            try {
                var me = this;
                var newFile = null;

                if (returnNewObject) {
                    newFile = Ext.create('AssetManagement.customer.model.bo.File', {
                        'fileId': boFile.get('fileId'),
                        'fileName': boFile.get('fileName'),
                        'fileType': boFile.get('fileType'),
                        'fileOrigin': boFile.get('fileOrigin'),
                        'fileDescription': boFile.get('fileDescription'),
                        'createdAt': AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(),
                        'lastModified': AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(),
                        'contentAsArrayBuffer': boFile.get('contentAsArrayBuffer')
                    });
                } else {
                    newFile = boFile;
                }

                if (factor === 0) {
                    callback(newFile);
                    return;
                }

                //get the array buffer and load it in an image instance
                var fileContent = newFile.get('contentAsArrayBuffer');

                //check if file is already base64 encoded - if not encode it as base64
                var fileEncodedAsBase64 = this.arrayBufferToBase64(fileContent, this.getMimeTypeOfFile(newFile));

                if (!fileEncodedAsBase64) {
                    callback.call(undefined);
                    return;
                }

                var image = new Image();

                var imageLoadedCallback = function (imageEvent) {
                    try {
                        var canvas = document.createElement('canvas');
                        var context = canvas.getContext('2d');

                        //set target width and heigt of canvas
                        var targetWidth, targetHeight;
                        var drawImageWidth, drawImageHeight;

                        if (factor === 90 || factor === -90) {
                            //rotation by 90 or 270 degree
                            targetWidth = image.height;
                            targetHeight = image.width;
                        } else {
                            //rotation by 180 degree
                            targetWidth = image.width;
                            targetHeight = image.height;
                        }

                        canvas.width = targetWidth;
                        canvas.height = targetHeight;

                        if (factor === 90 || factor === -90) {
                            drawImageWidth = targetHeight;
                            drawImageHeight = targetWidth;
                        } else {
                            drawImageWidth = targetWidth;
                            drawImageHeight = targetHeight;
                        }

                        context.clearRect(0, 0, targetWidth, targetHeight);
                        context.save();
                        context.translate(targetWidth / 2, targetHeight / 2);
                        context.rotate(factor * Math.PI / 180);
                        context.drawImage(image, -drawImageWidth / 2, -drawImageHeight / 2);
                        context.restore();

                        var mimeType = me.getMimeTypeOfFile(newFile);

                        //extract the rotated image
                        var dataUrl = canvas.toDataURL(mimeType);
                        var rotatedImage = me.dataURLToBlob(dataUrl);

                        //convert blob to array buffer
                        var fileReader = new FileReader();

                        var rotatedOnLoadCallback = function () {
                            try {
                                //set the array buffer as file content of the new return object
                                newFile.set('fileSize', rotatedImage.size / 1024.0);
                                newFile.set('contentAsArrayBuffer', this.result);

                                //return the rotated file
                                callback(newFile);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rotateImage-rotatedOnLoadCallback of BaseFileHelper', ex);
                                callback(undefined);
                            }
                        };

                        fileReader.onload = rotatedOnLoadCallback;
                        fileReader.readAsArrayBuffer(rotatedImage);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rotateImage-imageLoadedCallback of BaseFileHelper', ex);
                        callback(undefined);
                    }
                };

                image.onload = imageLoadedCallback;
                image.src = fileEncodedAsBase64;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rotateImage of BaseFileHelper', ex);
                callback(undefined);
            }
        },

        //public
        //returns, if this file (bo) is an image
        isImageFile: function (file) {
            var retval = false;

            try {
                var fileType = file.get('fileType') ? file.get('fileType').toLowerCase() : '';

                if (fileType !== '') {
                    retval = fileType === 'jpeg' || fileType === 'png' || fileType === 'jpg' || fileType === 'bmp';
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isImageFile of BaseFileHelper', ex);
            }

            return retval;
        },

        //private
        //returns, if a file handle contains an image
        isImageFileHandle: function (fileHandle) {
            var retval = false;

            try {
                var fileType = fileHandle['type'] ? fileHandle['type'].toLowerCase() : '';

                if (fileType !== '') {
                    retval = fileType === 'image/jpeg' || fileType === 'image/png' || fileType === 'image/jpg' || fileType === 'image/bmp';
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isImageFileHandle of BaseFileHelper', ex);
            }

            return retval;
        },

        //private
        //checks, if an image encapsluated as a business object file instance is in the correct orientation compared to the devices orientation
        //and if so, it gets reorientated
        rotateImageIfNecessary: function (boFile, callback) {
            if (!callback)
                return;

            try {
                var userAgentInfo = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();

                //currently rotation is only necessary for iOS devices
                if (userAgentInfo && userAgentInfo.OS === 'iOS') {
                    var fileContent = boFile.get('contentAsArrayBuffer');
                    var exif = EXIF.readFromBinaryFile(fileContent);
                    var rotationAngle = 0;

                    if (exif && exif.Orientation !== 1) {
                        switch (exif.Orientation) {
                            //portrait
                            case 6:
                                rotationAngle = 90;
                                break;
                                //reverse landscape
                            case 3:
                                rotationAngle = 180;
                                break;
                                //reverse portrait
                            case 8:
                                rotationAngle = -90;
                                break;
                        }

                        this.rotateImage(boFile, rotationAngle, false, callback);
                    } else {
                        callback.call(this, boFile);
                    }
                } else {
                    //no rotation required at all
                    callback.call(this, boFile);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rotateImageIfNecessary of BaseFileHelper', ex);
                callback(undefined);
            }
        },

        //private
        //checks, if an image encapsluated as a business object file instance is larger than the maximum allowed resolution
        //and if so, it gets down scaled to this maximum resolution
        resizeImageIfNecessary: function (boFile, callback) {
            if (!callback)
                return;

            try {
                //second make sure the image is downscaled, if necessary
                var desiredWidth = this.DEFAULT_IMG_WIDTH;
                var desiredHeight = this.DEFAULT_IMG_HEIGHT;

                var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();

                var widthAsString = funcPara.getValue1For('attach_max_img_res');
                var heightAsString = funcPara.getValue2For('attach_max_img_res');

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(widthAsString)) {
                    desiredWidth = parseInt(widthAsString);
                }

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(heightAsString)) {
                    desiredHeight = parseInt(heightAsString);
                }

                var formerFileType = 'image/' + boFile.get('fileType');

                this.resizeIMG(boFile, desiredWidth, desiredHeight, false, formerFileType, callback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resizeImageIfNecessary of BaseFileHelper', ex);
                callback(undefined);
            }
        }
    }
});