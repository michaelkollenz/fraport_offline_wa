Ext.define('AssetManagement.base.core.BaseCore', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.AppUpdateController',
        'AssetManagement.customer.controller.AppStartUpLoadingController',
        'AssetManagement.customer.controller.NavigationController',
        'AssetManagement.customer.view.MainView'
    ],

    inheritableStatics: {
        _version: '017.009.002',
        _baseDatabaseVersion: 31,
        _customerDatabaseVersion: 1,

        _appConfig: null,
        _mainView: null,
        _dataBaseHelper: null,
        _controller: null,

        getVersion: function () {
            var retval = null;

            try {
                retval = this._version;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVersion of BaseCore', ex);
            }

            return retval;
        },

        getRequiredDatabaseVersion: function () {
            this._customerDatabaseVersion = this._customerDatabaseVersion.toString();

            var requiredDatabaseVersion = this._baseDatabaseVersion + this._customerDatabaseVersion;
            return Number(requiredDatabaseVersion);
        },

        initializeViewPort: function () {
            try {
                this._mainView = Ext.create('AssetManagement.customer.view.MainView');
                AssetManagement.customer.controller.ClientStateController.initializeAutoLogout();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeViewPort of Main Application', ex);
            }
        },

        setMode: function (mode, argumentsObject, removeCurrentPageFromHistory) {
            try {
                this.getController().requestSetMode(mode, argumentsObject, removeCurrentPageFromHistory);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMode of Main Application', ex);
            }
        },

        navigateBack: function (ignoreResultingChangedEvent) {
            try {
                this.getController().requestGoBack(ignoreResultingChangedEvent);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateBack of Main Application', ex);
            }
        },

        showDialog: function (dialogType, argumentsObject) {
            try {
                this.getController().requestShowDialog(dialogType, argumentsObject);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showDialog of Main Application', ex);
            }
        },

        setMainToolbarOptions: function (arrayOfOptions) {
            try {
                this._mainView.getController().getMainToolbarController().setOptionsMenuItems(arrayOfOptions);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMainToolbarOptions of Main Application', ex);
            }
        },

        getAppConfig: function () {
            return this._appConfig;
        },

        setAppConfig: function (appConfig) {
            this._appConfig = appConfig;
        },

        getMainView: function () {
            return this._mainView;
        },

        setMainView: function (view) {
            this._mainView = view;
        },

        getDataBaseHelper: function () {
            return this._dataBaseHelper;
        },

        setDataBaseHelper: function (dataBaseHelper) {
            this._dataBaseHelper = dataBaseHelper;
        },

        getCurrentPage: function () {
            var retval = null;

            try {
                retval =this.getController().getCurrentPage();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentPage of BaseCore', ex);
            }

            return retval;
        },

        getController: function() {
            return this._controller;
        },

        setController: function(controller) {
            this._controller = controller;
        }
    }

});