Ext.define('AssetManagement.base.view.BaseToolbarView', {
    extend: 'Ext.toolbar.Toolbar',


    requires: [
        'AssetManagement.customer.model.viewmodel.ToolbarViewModel',
        'AssetManagement.customer.controller.ToolbarController',
        'Ext.util.HashMap',
        'Ext.container.Container',
        'AssetManagement.customer.modules.barcode.BarcodeScanButton',
        'Ext.button.Button',
        'Ext.form.Label'
    ],

    viewModel: {
        type: 'ToolbarViewModel'
    },

    controller: 'ToolbarController',
    height: 55,

	defaultListenerScope: true,
    id: 'toolbarView',
    cls: 'oxToolbar',
    layout: {
        type: 'hbox',
        align: 'center',
        pack: 'center'
    },

    items: [
        {
            xtype: 'container',
            flex: 1,
            margin: 0,
            minWidth: 150,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                 {
                    xtype: 'button',
                    html: '<img width="100%" src="resources/icons/left-arraow.png"></img>',
                    height: 50,
                    id: 'mainMenuButtonOM',
                    width: 38,
                    icon: '',
                    text: '',
	                listeners: {
	                	click: 'onOptionSelected'
	                }
                },
                {
                    xtype: 'button',
                    html: '<img width="100%" src="resources/icons/menu_home.png"></img>',
                    height: 50,
                    id: 'homeButtonOM',
                    width: 50,
                    icon: '',
                    text: '',
	                listeners: {
	                	click: 'onOptionSelected'
	                }
                },
                {
                    xtype: 'button',
                    html: '<img width="100%" src="resources/icons/goBack.png"></img>',
                    height: 50,
                    id: 'backButtonOM',
                    width: 50,
                    icon: '',
                    text: '',
	                listeners: {
	                	click: 'onOptionSelected'
	                }
                }
            ]
        },
        {
            xtype: 'label',
            id: 'titleLabel',
            margin: '0 8 0 8',
            minHeight: 20,
            cls: 'oxMainTitleLabel'
        },
        {
            xtype: 'container',
            id: 'optionsBar',
            minWidth: 60,
            margin: '0 0 0 0',
            padding: '0 10 0 0',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch',
                pack: 'end'
            },
            items: [
            ]
        }
    ],

    //private
    _optionsBar: null,
    _optionsMenuItems: null,
    _titleLabel: null,
    _homeButton: null,
    _backButton: null,
    _mainMenuButton: null,

    getOptionsBar: function() {
    	if(this._optionsBar === null)
    		this._optionsBar = this.queryById('optionsBar');

    	return this._optionsBar;
	},

    constructor: function(config) {
		this.callParent(arguments);
		this.addListener('resize', this.manageMaxWidthOfTitleLabel, this);
		this.addListener('resize', this.getController().reevaluateMenuReprensetation, this.getController());

		this.generateOptionsMenuItems();
	},

	//public
	getTitleLabel: function() {
		try {
			if(!this._titleLabel)
				this._titleLabel = this.queryById('titleLabel');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTitleLabel of BaseToolbarView', ex);
		}

		return this._titleLabel;
	},

	getMainMenuButton: function() {
		try {
			if(!this._mainMenuButton)
				this._mainMenuButton = this.queryById('mainMenuButtonOM');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMainMenuButton of BaseToolbarView', ex);
		}

		return this._mainMenuButton;
	},

	getHomeButton: function() {
		try {
			if(!this._homeButton)
				this._homeButton = this.queryById('homeButtonOM');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHomeButton of BaseToolbarView', ex);
		}

		return this._homeButton;
	},

	getBackButton: function() {
		try {
			if(!this._backButton)
				this._backButton = this.queryById('backButtonOM');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackButton of BaseToolbarView', ex);
		}

		return this._backButton;
	},

	getAlternateOptionsMenuButton: function() {
		var retval = null;

		try {
			retval = this._optionsMenuItems.get('alternateOptionsMenuButtonOM');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAlternateOptionsMenuButton of BaseToolbarView', ex);
		}

		return retval;
	},

	setOptionMenuItems: function(optionButtonIds) {
		try {
			var optionsBar = this.getOptionsBar();
			//do not destroy the buttons, because they will be used again
			optionsBar.removeAll(false);

			if(optionButtonIds && this._optionsMenuItems) {
				var optionButton;
				Ext.Array.each(optionButtonIds, function(id) {
					optionButton = this._optionsMenuItems.get(id);

					if(optionButton)
						optionsBar.add(optionButton);
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setOptionMenuItems of BaseToolbarView', ex);
		}
	},

    getAvailableOptionsMenuWidth: function() {
    	var retval = 0;

		try {
			retval = this.items.items[2].getWidth();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAvailableOptionsMenuWidth of BaseToolbarView', ex);
		}

		return retval;
	},

	//private
	//the max width of the label has to be set after each resize
	//without it would cover the options menu, when string get's to long
	manageMaxWidthOfTitleLabel: function(me, width, height, oldWidth, oldHeight, eOpts) {
		try {
			this._titleLabel.setMaxWidth(width - 210); //minWidth of left menu and optionsmenu
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageMaxWidthOfTitleLabel of BaseToolbarView', ex);
		}
	},

	onOptionSelected: function(item, e, eOpts) {
    	try {
    		this.getController().onOptionSelected(item);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionSelected of BaseToolbarView', ex);
    	}
    },

	generateOptionsMenuItems: function() {
		try {
			var optionMenuItems = Ext.create('Ext.util.HashMap');

			var button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/menu.png"></img>',
	            height: 50,
	            id: 'alternateOptionsMenuButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: this.getController().showAlternateOptionsMenu,
	            	scope: this.getController()
	            }
			});
			optionMenuItems.add('alternateOptionsMenuButtonOM', button);

			searchButton = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/search.png"></img>',
	            height: 50,
	            id: 'searchButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('searchButtonOM', searchButton);
		    //this.searchButtonVisibility(searchButton);

			clearSearchButton = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/clear_search.png"></img>',
			    height: 50,
			    id: 'clearSearchButtonOM',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			        click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('clearSearchButtonOM', clearSearchButton);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/edit.png"></img>',
	            height: 50,
	            id: 'editButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('editButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/demand_requirement.png"></img>',
	            height: 50,
	            id: 'newSDOrderButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('newSDOrderButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/duplicate.png"></img>',
	            height: 50,
	            id: 'duplicateButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('duplicateButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/class_charact.png"></img>',
	            height: 50,
	            id: 'classButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('classButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/add.png"></img>',
	            height: 50,
	            id: 'addButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('addButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/longtext.png"></img>',
	            height: 50,
	            id: 'longtextButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('longtextButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/longtext_exists.png"></img>',
	            height: 50,
	            id: 'longtextButtonExistOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('longtextButtonExistOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/equi_change_position.png"></img>',
	            height: 50,
	            id: 'equiChangeButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('equiChangeButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/save.png"></img>',
	            height: 50,
	            id: 'saveButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('saveButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/rfid.png"></img>',
	            height: 50,
	            id: 'rfidButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('rfidButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/report.png"></img>',
	            height: 50,
	            id: 'reportButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('reportButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/print.png"></img>',
	            height: 50,
	            id: 'printButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('printButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/trash.png"></img>',
	            height: 50,
	            id: 'deleteButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('deleteButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/new_item.png"></img>',
	            height: 50,
	            id: 'newItemButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('newItemButtonOM', button);

			button = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/order.png"></img>',
			    height: 50,
			    id: 'newOrderButtonOM',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			    	click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('newOrderButtonOM', button);

			button = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/notif.png"></img>',
			    height: 50,
			    id: 'newNotifButtonOM',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			    	click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('newNotifButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/measdoc.png"></img>',
	            height: 50,
	            id: 'measpointsButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('measpointsButtonOM', button);

			button = Ext.create('Ext.button.Button', {
	            html: '<img width="100%" src="resources/icons/mapsMarker.png"></img>',
	            height: 50,
	            id: 'showMyMapsPositionButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('showMyMapsPositionButtonOM', button);

			button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/object_structure.png"></img>',
	            height: 50,
	            id: 'objStructureButtonOM',
	            width: 50,
	            icon: '',
	            text: '',
	            listeners: {
	            	click: 'onOptionSelected'
	            }
			});
			optionMenuItems.add('objStructureButtonOM', button);

			button = Ext.create('AssetManagement.customer.modules.barcode.BarcodeScanButton', {
	            height: 50,
	            id: 'barcodeButtonOM',
	            width: 50,
	            padding: '6 8 10 8',
            	barcodeScanCallback: this.getController().onBarcodeScanned,
            	barcodeScanCallbackScope: this.getController()
			});
			optionMenuItems.add('barcodeButtonOM', button);

			button = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/printing_icon.png"></img>',
			    height: 50,
			    id: 'printMultipleReports',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			        click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('printMultipleReports', button);

			button = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/signature.png"></img>',
			    height: 50,
			    id: 'signingReportsButton',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			        click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('signingReportsButton', button);

			button = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/save.png"></img>',
			    height: 50,
			    id: 'saveReportsButton',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			        click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('saveReportsButton', button);

			button = Ext.create('Ext.button.Button', {
			    html: '<img width="100%" src="resources/icons/intern_note.png"></img>',
			    height: 50,
			    id: 'internNoteButtonOM',
			    width: 50,
			    icon: '',
			    text: '',
			    listeners: {
			        click: 'onOptionSelected'
			    }
			});
			optionMenuItems.add('internNoteButtonOM', button);

			this._optionsMenuItems = optionMenuItems;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateOptionsMenuItems of BaseToolbarView', ex);
		}
	},

	searchButtonVisibility: function(searchButton) {
		try {
	        searchButton.setVisible(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSearchButton of BaseToolbarView', ex);
		}
    }
});