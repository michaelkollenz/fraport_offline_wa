﻿Ext.define('AssetManagement.base.view.BaseCharactValueCheckboxItem', {
    extend: 'Ext.form.field.Checkbox',

    
    config: {
        xtype: 'checkboxfield',
        flex: 1,
        baseCls: 'oxCheckBox',
        checkedCls: 'checked',
        labelStyle: 'padding-top: 8px;',
        margin: '0 0 0 10',
        labelWidth: 420,
        width: 30,
        height: 35
  
    },
      
    _charact: null,
    _charactValue: null,
    _classValue: null,
    _einWert: false,
   
    constructor: function (config) {
        try {
            this.callParent(arguments);
            if (!config) {
                config = {};

                arguments = [config];
            }

            this._charact = config.charact;
            this._charactValue = config.charactValue;
            this._classValue = config.classValue;
            this._einWert = config.einWert;
          
            // Set Merkmal Label
            this.buildFieldLabel();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueCheckboxItem', ex);
        }
    },

    getClassValue: function () {
        var retval = null;
        try {
           
            if (this.getValue() === true) {
                retval = this._classValue;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassValue of BaseCharactValueCheckboxItem', ex);
        }
        return retval;
    },

    buildFieldLabel: function () {
        try {
            if (this._charactValue)
                this.fieldLabel = this._charactValue.get('atwrt') + ' ' + this._charactValue.get('atwtb') + ' ' + this._charactValue.get('atflv');
            else
                this.fieldLabel = this._classValue.get('atwrt') + ' ' + this._classValue.get('atflv');

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFieldLabel of BaseCharactValueCheckboxItem', ex);

        }
    }
});