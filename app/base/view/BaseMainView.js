Ext.define('AssetManagement.base.view.BaseMainView', {
  extend: 'Ext.container.Viewport',


  requires: [
    'AssetManagement.customer.model.viewmodel.MainViewViewModel',
    'AssetManagement.customer.controller.MainViewController',
    'AssetManagement.customer.helper.OxLogger',
    'Ext.menu.Menu',
    'AssetManagement.customer.view.menus.OxMainMenuItem',
    'Ext.menu.Separator',
    'AssetManagement.customer.view.ToolbarView',
    'Ext.window.Toast'
  ],

  controller: 'MainViewController',

  viewModel: {
    type: 'MainViewViewModel'
  },

  itemId: 'mainView',
  layout: 'card',
  defaultListenerScope: true,
  _menuBarOpen: true,
  _currentVisibleNotifications: null,

  constructor: function () {
    try {
      this.callParent(arguments);

      this.initialize();

      this._currentVisibleNotifications = Ext.create('Ext.util.MixedCollection');

      var myController = this.getController();
      this.addListener('resize', myController.onViewPortSizeChanged, myController);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseMainView', ex);
    }
  },

  initialize: function () {
    try {
      var mainMenuPanel = this.generateMainMenuPanel();

      var items = [
        {
          xtype: 'container',
          id: 'mainVboxContainer',
          layout: {
            type: 'vbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'toolbarview',
              id: 'optionsToolbar',
              hidden: true
            },
            {
              xtype: 'panel',
              flex: 1,
              id: 'navigationPanel',
              header: false,
              manageHeight: false,
              titleAlign: 'center',
              layout: {
                type: 'hbox',
                align: 'stretch'
              },
              items: [
                mainMenuPanel,
                {
                  xtype: 'panel',
                  flex: 1,
                  id: 'contentPanel',
                  layout: 'card',
                  titleAlign: 'center'
                }
              ]
            },
            {
              xtype: 'container',
              height: 30,
              id: 'statusBar',
              cls: 'oxToolbar',
              hidden: true,
              layout: {
                type: 'hbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'container',
                  margin: '0 30 0 10',
                  items: [
                    {
                      xtype: 'button',
                      id: 'statusBarButton',
                      padding: '0 0 0 0',
                      html: '<div><img width= 70% src="resources/icons/intern_note.png" /></div>',
                      listeners: {
                        click: {fn: this.getController().onInfoButtonClick, scope: this.getController()}
                      }
                    }
                  ]
                },
                {
                  xtype: 'container',
                  flex: 1,
                  layout: {
                    type: 'hbox',
                    align: 'bottom'
                  },
                  items: [
                    {
                      xtype: 'displayfield',
                      id: 'statusBarUserLabel',
                      fieldLabel: Locale.getMsg('user'),
                      labelStyle: 'color: black',
                      value: '',
                      labelWidth: 60
                    },
                    {
                      xtype: 'component',
                      minWidth: 30,
                      flex: 1
                    },
                    {
                      xtype: 'displayfield',
                      id: 'statusBarSystemLabel',
                      fieldLabel: Locale.getMsg('system'),
                      labelStyle: 'color: black',
                      value: '',
                      width: 90,
                      labelWidth: 50
                    },
                    {
                      xtype: 'component',
                      minWidth: 30,
                      flex: 1
                    },
                    {
                      xtype: 'displayfield',
                      id: 'statusBarLastSyncLabel',
                      fieldLabel: Locale.getMsg('lastSync'),
                      labelStyle: 'color: black',
                      value: '',
                      labelWidth: 140,
                      plugins: 'responsive',
                      responsiveConfig: {
                        'width <= 570': {
                          visible: false
                        },
                        'width > 570': {
                          visible: true
                        }
                      }
                    },
                    {
                      xtype: 'component',
                      width: 30
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          xtype: 'oxtextfield',
          id: 'focusDummy',
          style: 'display: none;'
        }
      ];

      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseMainView', ex);
    }
  },

  refresh: function () {
    try {
      this.manageSections();
      this.updateStatusBar();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refresh of BaseMainView', ex);
    }
  },

  showOptionsToolbar: function () {
    try {
      var optionsToolbar = this.queryById('optionsToolbar');

      optionsToolbar.setVisible(true);
      optionsToolbar.updateLayout();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showOptionsToolbar of BaseMainView', ex);
    }
  },

  hideOptionsToolbar: function () {
    try {
      this.queryById('optionsToolbar').setVisible(false);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideOptionsToolbar of BaseMainView', ex);
    }
  },

  showStatusBar: function () {
    try {
      var statusBar = this.queryById('statusBar');

      statusBar.setVisible(true);
      statusBar.updateLayout();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showStatusBar of BaseMainView', ex);
    }
  },

  hideStatusBar: function () {
    try {
      this.queryById('statusBar').setVisible(false);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideStatusBar of BaseMainView', ex);
    }
  },

  enableMainMenuPanel: function () {
    try {
      var panel = this.queryById('menuPanel');

      // panel.setVisible(false);
      panel.setWidth(260);
      // panel.setVisible(true);

      panel.updateLayout();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside enableMainMenuPanel of BaseMainView', ex);
    }
  },

  disableMainMenuPanel: function () {
    try {
      this.hideMainMenuPanel();
      this.queryById('menuPanel').setWidth(0);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside disableMainMenuPanel of BaseMainView', ex);
    }
  },

  showMainMenuPanel: function () {
    try {
      var menuPanel = this.queryById('menuPanel');

      if (menuPanel.width === 0) {
        this.enableMainMenuPanel();
      } else {
        this.queryById('menuPanel').setVisible(true);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showMainMenuPanel of BaseMainView', ex);
    }
  },

  hideMainMenuPanel: function () {
    try {
      this.queryById('menuPanel').setVisible(false);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideMainMenuPanel of BaseMainView', ex);
    }
  },

  toggleMainMenuVisibility: function () {
    try {
      var menuPanel = this.queryById('menuPanel');

      if (menuPanel.isVisible()) {
        menuPanel.setVisible(false);
      } else {
        menuPanel.setVisible(true);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside toggleMenuPanelVisibility of BaseMainView', ex);
    }
  },

  showNotification: function (msg) {
    try {
      var css = ['oxandoToast'];
      if (msg.type == 1) {
        //message is an information
      }
      else if (msg.type == 2) {
        //message is an error
        css.push('fg-red');
      }

      var notificationFrame = Ext.toast({
        html: '<div class="' + css.join(' ') + '"><p>' + msg.message + '</p></div>',
        autoCloseDelay: 3000,
        align: 't',
        stickOnClick: true,
        stickWhileHover: true,
        bodyStyle: {
          background: 'rgba(0, 0, 0, 0.69);',
          padding: '10px',
          borderRadius: '30px',
          color: 'white'
        },
        closeOnMouseDown: true,
        listeners: {
          destroy: this.onNotificationDestroyed,
          scope: this
        }
      });

      this._currentVisibleNotifications.add(notificationFrame.getId(), notificationFrame);
      notificationFrame.show();

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showNotification of BaseMainView', ex);
    }
  },

  getCurrentVisibleNotifications: function() {
    var retval = null;

    try {
      retval = new Array();

      if(this._currentVisibleNotifications.getCount() > 0) {
        this._currentVisibleNotifications.each(function(frame) {
          retval.push(frame);
        }, this);
      }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentVisibleNotifications of BaseMainView', ex);
    }

    return retval;
  },

  onNotificationDestroyed: function(notificationsFrame) {
    try {
      if(notificationsFrame)
        this._currentVisibleNotifications.removeAtKey(notificationsFrame.getId());
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotificationDestroyed of BaseMainView', ex);
    }
  },

  rebuildMainMenu: function () {
    try {
      var mainContainer = Ext.getCmp('navigationPanel');

      var currentPanel = Ext.getCmp('mainMenuPanel');

      mainContainer.remove(currentPanel);

      var newPanel = this.generateMainMenuPanel();
      mainContainer.insert(0, newPanel);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildMainMenu of BaseMainView', ex);
    }
  },

  generateMainMenuPanel: function () {
    var retval = null;

    try {
      retval = Ext.create('Ext.panel.Panel', {
        id: 'mainMenuPanel',
        cls: 'oxMainMenuPanel',
        itemId: 'menuPanel',
        overflowY: 'auto',
        width: 0,
        header: false,
        manageHeight: false,
        title: 'Menu',
        plugins: 'responsive',
        responsiveConfig: {
          'width <= 800': {
            visible: false
          },
          'width > 800': {
            visible: true
          }
        },

        items: [
          {
            xtype: 'menu',
            flex: 1,
            floating: false,
            itemId: 'menu',
            id: 'mainMenu',
            margin: '',
            padding: '',
            width: '',
            bodyBorder: true,
            manageHeight: false,
            items: [
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'orderlist',
                id: 'orderMenuItem',
                hidden: true,
                bind: {
                  html: '<div><img src="resources/icons/order.png" /><div class="font">' + Locale.getMsg('orders') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'orderMenuItemSep',
                hidden: true,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'notiflist',
                id: 'notifMenuItem',
                hidden: true,
                bind: {
                  html: '<div><img src="resources/icons/notif.png" />   <div class="font">' + Locale.getMsg('notifications') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'notifMenuItemSep',
                hidden: true,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                height: 55,
                frame: true,
                itemId: 'equipmentlist',
                id: 'equiMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/equi.png" /> <div class="font">' + Locale.getMsg('equipments') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'equiMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'funcloclist',
                id: 'funcLocMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/funcloc.png" />   <div class="font">' + Locale.getMsg('funcLocs') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'funcLocMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'measPointlist',
                id: 'measPointMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/measpoints.png" />   <div class="font">' + Locale.getMsg('measPoints') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'measPointsMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'materiallist',
                id: 'materialMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/matconf.png" />   <div class="font">' + Locale.getMsg('materials') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'materialMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'banf',
                id: 'banfMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/demand_requirement.png" />   <div class="font">' + Locale.getMsg('demandRequirement') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'banfMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'sdorderlist',
                id: 'sdOrderMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/demand_requirement.png" />   <div class="font">' + Locale.getMsg('materialOrders') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'sdOrderMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'timeconf',
                id: 'timeConfMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/timeconf.png" />   <div class="font">' + Locale.getMsg('timeConfirmations') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'timeConfMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'inventory',
                id: 'inventoryMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/inventory.png" />   <div class="font">' + Locale.getMsg('inventory') + '</div></div>'
                }

              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'inventoryMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'customerlist',
                id: 'customerMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/partner.png" />   <div class="font">' + Locale.getMsg('customers') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'customerMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'schedule',
                id: 'scheduleMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/car.png" />   <div class="font">' + Locale.getMsg('schedule') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'scheduleMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'calendar',
                id: 'calendarMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/timeconf.png" />   <div class="font">' + Locale.getMsg('calendar') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'calendarMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'settings',
                id: 'settingsMenuItem',
                visible: false,
                bind: {
                  html: '<div><img src="resources/icons/settings.png" />   <div class="font">' + Locale.getMsg('settings') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'settingsMenuItemSep',
                visible: false,
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                id: 'sync',
                itemId: 'sync',
                id: 'syncMenuItem',
                bind: {
                  html: '<div><img src="resources/icons/btsync.png" />   <div class="font">' + Locale.getMsg('sync') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'syncMenuItemSep',
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'stacktrace',
                id: 'stacktraceMenuItem',
                bind: {
                  html: '<div><img src="resources/icons/stack_trace.png" />   <div class="font">' + Locale.getMsg('stackTrace') + '</div></div>'
                }
              },
              {
                xtype: 'menuseparator',
                frame: true,
                id: 'stacktraceMenuItemSep',
                padding: 0
              },
              {
                xtype: 'oxmainmenuitem',
                border: 8,
                cls: 'oxMainMenuItem',
                frame: true,
                height: 55,
                itemId: 'logoff',
                id: 'logoffMenuItem',
                padding: '',
                bind: {
                  html: '<div><img src="resources/icons/logout.png" />   <div class="font">' + Locale.getMsg('logout') + '</div></div>'
                }
              }
            ],
            listeners: {
              click: {
                fn: this.getController().onMainMenuItemClick,
                scope: this.getController()
              }
            }
          }
        ]
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateMainMenuPanel of BaseMainView', ex);
    }

    return retval;
  },

  manageSections: function () {
    try {
      var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
      if (funcParaInstance.getValue1For('ord_active') !== 'X') {
        Ext.getCmp('orderMenuItem').setHidden(true);
        Ext.getCmp('orderMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('orderMenuItem').setHidden(false);
        Ext.getCmp('orderMenuItemSep').setHidden(false);
      }

      if (funcParaInstance.getValue1For('equi_active') !== 'X') {
        Ext.getCmp('equiMenuItem').setHidden(true);
        Ext.getCmp('equiMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('equiMenuItem').setHidden(false);
        Ext.getCmp('equiMenuItemSep').setHidden(false);
      }

      if (funcParaInstance.getValue1For('floc_active') !== 'X') {
        Ext.getCmp('funcLocMenuItem').setHidden(true);
        Ext.getCmp('funcLocMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('funcLocMenuItem').setHidden(false);
        Ext.getCmp('funcLocMenuItemSep').setHidden(false);
      }

      if (funcParaInstance.getValue1For('noti_active') !== 'X') {
        Ext.getCmp('notifMenuItem').setHidden(true);
        Ext.getCmp('notifMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('notifMenuItem').setHidden(false);
        Ext.getCmp('notifMenuItemSep').setHidden(false);
      }

      if (funcParaInstance.getValue1For('calendar_active') !== 'X') {
        Ext.getCmp('calendarMenuItem').setHidden(true);
        Ext.getCmp('calendarMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('calendarMenuItem').setHidden(false);
        Ext.getCmp('calendarMenuItemSep').setHidden(false);
      }

      if (funcParaInstance.getValue1For('partner_active') !== 'X') {
        Ext.getCmp('customerMenuItem').setHidden(true);
        Ext.getCmp('customerMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('customerMenuItem').setHidden(false);
        Ext.getCmp('customerMenuItemSep').setHidden(false);
      }


      if (funcParaInstance.getValue1For('inventory_active') !== 'X') {
        Ext.getCmp('inventoryMenuItem').setHidden(true);
        Ext.getCmp('inventoryMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('inventoryMenuItem').setHidden(false);
        Ext.getCmp('inventoryMenuItemSep').setHidden(false);
      }


      //settings are not yet implemented, so do not show
      //if(funcParaInstance.get('settings_active') != true) {
      Ext.getCmp('settingsMenuItem').setHidden(true);
      Ext.getCmp('settingsMenuItemSep').setHidden(true);
      /*} else {
       Ext.getCmp('settingsMenuItem').setHidden(false);
       Ext.getCmp('settingsMenuItemSep').setHidden(false);
       }*/

      if (funcParaInstance.getValue1For('equi_mp_active') === 'X' || funcParaInstance.getValue1For('floc_mp_active') === 'X') {
        Ext.getCmp('measPointMenuItem').setHidden(false);
        Ext.getCmp('measPointsMenuItemSep').setHidden(false);
      } else {
        Ext.getCmp('measPointMenuItem').setHidden(true);
        Ext.getCmp('measPointsMenuItemSep').setHidden(true);
      }


      if (funcParaInstance.getValue1For('material_active') !== 'X') {
        Ext.getCmp('materialMenuItem').setHidden(true);
        Ext.getCmp('materialMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('materialMenuItem').setHidden(false);
        Ext.getCmp('materialMenuItemSep').setHidden(false);
      }
      //ext_scen is customer specific!!
      //if (funcParaInstance.getValue1For('ext_scen_active') !== 'X') {
      Ext.getCmp('sdOrderMenuItem').setHidden(true);
      Ext.getCmp('sdOrderMenuItemSep').setHidden(true);

      if (funcParaInstance.getValue1For('banf_active') !== 'X') {
        Ext.getCmp('banfMenuItem').setHidden(true);
        Ext.getCmp('banfMenuItemSep').setHidden(true);
      } else {
        Ext.getCmp('banfMenuItem').setHidden(false);
        Ext.getCmp('banfMenuItemSep').setHidden(false);
      }
      // } else {
      //     Ext.getCmp('sdOrderMenuItem').setHidden(false);
      //     Ext.getCmp('sdOrderMenuItemSep').setHidden(false);
      //
      //     Ext.getCmp('banfMenuItem').setHidden(true);
      //     Ext.getCmp('banfMenuItemSep').setHidden(true);
      // }

      //			if(funcParaInstance.get('schedule_active') != true) {
      Ext.getCmp('scheduleMenuItem').setHidden(true);
      Ext.getCmp('scheduleMenuItemSep').setHidden(true);
      //			} else {
      //				Ext.getCmp('scheduleMenuItem').setHidden(false);
      //				Ext.getCmp('scheduleMenuItemSep').setHidden(false);
      //			}

      //time conf overview is not yet implemented, so do not show
      //	if(funcParaInstance.get('conf_rep_active') != true) {
      Ext.getCmp('timeConfMenuItem').setHidden(true);
      Ext.getCmp('timeConfMenuItemSep').setHidden(true);
      //	} else {
      //		Ext.getCmp('timeConfMenuItem').setHidden(false);
      //		Ext.getCmp('timeConfMenuItemSep').setHidden(false);
      //	}

      //'orderMenuItemSep','orderMenuItem',
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSections of BaseMainView', ex);
    }
  },

  getMainMenuPanel: function () {
    var mainMenuPanel = Ext.getCmp('mainMenuPanel');
    var retval = false;

    try {
      if (mainMenuPanel)
        retval = true;
      else
        retval = false;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMainMenuPanel of BaseMainView', ex);
    }

    return retval;
  },

  rebuildStatusBar: function () {
    try {
      var user = Ext.getCmp('statusBarUserLabel');
      var system = Ext.getCmp('statusBarSystemLabel');
      var lastSync = Ext.getCmp('statusBarLastSyncLabel');

      user.setFieldLabel(Locale.getMsg('user'));
      system.setFieldLabel(Locale.getMsg('system'));
      lastSync.setFieldLabel(Locale.getMsg('lastSync'));
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateStatusBar of BaseMainView', ex);
    }
  },

  updateStatusBar: function () {
    try {
      var user = Ext.getCmp('statusBarUserLabel');
      var system = Ext.getCmp('statusBarSystemLabel');
      var lastSync = Ext.getCmp('statusBarLastSyncLabel');

      user.setValue(AssetManagement.customer.core.Core.getAppConfig().getUserId());
      system.setValue(AssetManagement.customer.core.Core.getAppConfig().getSyncSystemId() + '/' + AssetManagement.customer.core.Core.getAppConfig().getMandt());
      lastSync.setValue(AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(AssetManagement.customer.core.Core.getAppConfig().getLastSync()));
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateStatusBar of BaseMainView', ex);
    }
  }
});
