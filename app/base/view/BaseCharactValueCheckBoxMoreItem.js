﻿Ext.define('AssetManagement.base.view.BaseCharactValueCheckboxMoreItem', {
    extend: 'Ext.form.field.Checkbox',




    config: {
        xtype: 'checkboxfield',
        flex: 1,
        baseCls: 'oxCheckBox',
        checkedCls: 'checked',
        labelStyle: 'padding-top: 8px;',
        margin: '0 0 0 10',
        labelWidth: 420,
        width: 30,
        height: 35
  
    },

  
    _charact: null,
    _charactValue: null,
    _atwrt: '',
    _atwtb: '',
    _atflv: '',



    constructor: function (config) {
        try {
            if (!config) {
                config = {};

                arguments = [config];
            }

            this._charact = config.charact;
            this._charactValue = config.charactValue;
            this._atwrt = config.atwrt;
            this._atwtb = config.atwtb;
          
            // Set Merkmal Label
            this.fieldLabel = config.atwrt + ' ' + config.atwtb;

            this.callParent(arguments);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueCheckboxMoreItem', ex);
        }
    }

 
});