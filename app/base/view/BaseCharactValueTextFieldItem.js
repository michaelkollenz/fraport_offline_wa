﻿Ext.define('AssetManagement.base.view.BaseCharactValueTextFieldItem', {
    extend: 'AssetManagement.customer.view.utils.OxTextField',


    requires: [
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.controller.dialogs.SingularValueLineDialogController',
        'AssetManagement.customer.model.dialogmodel.SingularValueLineDialogViewModel',
        'AssetManagement.customer.controller.dialogs.AppendCheckboxDialogController',
        'AssetManagement.customer.model.dialogmodel.AppendCheckboxDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.CharactValueCheckboxItem'
    ],

    _charact: null,
    _charactValue: null,
    _classValue: null,

    constructor: function (config) {

        try {
            this.callParent(arguments);
            if (!config) {
                config = {};

                arguments = [config];
            }

            this._charact = config.charact;
            this._charactValue = config.charactValue;
            if (this._charact && !config.classValue)
                this._classValue = this._charact.get('classValues').data.items[0];
            else
                this._classValue = config.classValues;
       
            this.fieldLabel = config.charact.get('mseht')||config.charact.get('msehi');
            
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueTextFieldItem', ex);
        }
    },
    getClassValue: function () {
        var retval = null;
        try {
            if (this.getValue()) {
                retval = this._classValue;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassValue of BaseCharactValueTextFieldItem', ex);
        }
        return retval;
    }

});