﻿Ext.define('AssetManagement.base.view.BaseCharactValueDateItem', {
    extend: 'Ext.form.field.Date',


    requires: [
        'AssetManagement.customer.controller.dialogs.SingularValueLineDialogController',
        'AssetManagement.customer.model.dialogmodel.SingularValueLineDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.Date',
        'Ext.button.Button',
        //'AssetManagement.customer.utils.StringUtils.isNullOrEmpty',
        'AssetManagement.customer.view.CharactValueCheckboxItem'
    ],

    _charact: null,
    _charactValue: null,
    _classValue: null,
   
    constructor: function (config) {

        try {
            this.callParent(arguments);

            if (!config) {
                config = {};

                arguments = [config];
            }

            this._charact = config.charact;
            this._charactValue = config.charactValue;
            this._classValue = config.classValues;
            //if (this._charact)
            //    this._classValue = this._charact.get('classValues');
           
            
            this.fieldLabel = '';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueDateItem', ex);
        }
    }
});