Ext.define('AssetManagement.base.view.dialogs.BaseNotifActivityDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.NotifActivityDialogController',
        'AssetManagement.customer.model.dialogmodel.NotifActivityDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance == null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.NotifActivityDialog');
                }           
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifActivityDialog', ex);
		    }
            
            return this._instance;
        }
    },
	
    viewModel: {
        type: 'NotifActivityDialogModel'
    },
    
    controller: 'NotifActivityDialogController',
    width: 500,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
        try {
            var items =  [
                {
                    xtype: 'component',
                    width: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            cls: 'oxHeaderLabel',
                            height: 25,
                            id: 'notifActivityDialogHeader'
                            //  text: Locale.getMsg('createNotifActivity')
                        },
                        {
                            xtype: 'component',
                            height: 15
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'oxcombobox',
                                    flex: 2,
                                    id: 'notifActivityDialogActGrpCb',
                                    maxWidth: 290,
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value',
                                    fieldLabel: Locale.getMsg('notifActivity'),
                                    listeners: {
                                        select: 'onActivityGrpSelected'
                                    }

                                },
                                {
                                    xtype: 'component',
                                    width: 5
                                },
                                {
                                    xtype: 'oxcombobox',
                                    flex: 1,
                                    id: 'notifActivityDialogActCodeCb',
                                    labelStyle: 'display: none;',
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value'
                                }
                            ]
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'oxtextfield',
                            id: 'notifActivityDialogDescrTextField',
                            fieldLabel: Locale.getMsg('specification'),
                            maxLength: 40
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'onSaveButtonClicked'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'onCancelButtonClicked'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    width: 10
                }
            ];

            this.add(items);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    //@override
    getDialogTitle: function() {
        var retval = '';

        try {
            var notifActivity = this.getViewModel().get('notifActivity');

            retval = notifActivity ? Locale.getMsg('editNotifActivity') : Locale.getMsg('createNotifActivity');

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    manageDialogTitle: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('notifActivityDialogHeader').setText(title);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

        }
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {

            this.manageDialogTitle();

            this.manageActivityCodeComboBoxes();

            this.manageShorttextInputField();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    manageActivityCodeComboBoxes: function () {
        try{
            this.fillActivityCodeGroupComboBox();

            this.setSelectionOfActivityCodeGroup();

            this.fillActivityCodeCombobox();

            this.setSelectionOfActivityCode();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //clears the input field value or fills it with the current notif item short text
    manageShorttextInputField: function () {
        try {
            var myModel = this.getViewModel();
            var notifActivity = myModel.get('notifActivity');
            var shorttext = notifActivity ? notifActivity.get('matxt') : '';

            Ext.getCmp('notifActivityDialogDescrTextField').setValue(shorttext);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    fillActivityCodeGroupComboBox:function(){
        try {
            var myModel = this.getViewModel();
            var activityCodeGroupsMap = myModel.get('activityCodeGroupsMap');

            // fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (activityCodeGroupsMap) {
                activityCodeGroupsMap.each(function (codeGroupKey, groupsCodes) {
                    if (groupsCodes.getCount() > 0) {
                        store.add({ "text": codeGroupKey + ' ' + groupsCodes.getAt(0).get('codegrkurztext'), "value": groupsCodes });
                    }
                }, this);
            }

            var activityGroupComboBox = Ext.getCmp('notifActivityDialogActGrpCb');
            activityGroupComboBox.clearValue();
            activityGroupComboBox.bindStore(store);
        }catch(ex){
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    setSelectionOfActivityCodeGroup: function(){
        try{
            var myModel = this.getViewModel();
            var activityCodeGroupsMap = myModel.get('activityCodeGroupsMap');

            if (activityCodeGroupsMap) {
                var notifActivity = myModel.get('notifActivity');
                var itemsGroupKey = notifActivity ? notifActivity.get('mngrp') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsGroupKey)) {
                    var groupToSelect = activityCodeGroupsMap.get(itemsGroupKey);

                    if (groupToSelect) {
                        var comboBox = Ext.getCmp('notifActivityDialogActGrpCb');

                        var recordToSelect = comboBox.findRecordByValue(groupToSelect);

                        if (recordToSelect)
                            comboBox.select(recordToSelect);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    fillActivityCodeCombobox: function(){
        try{
            // fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            var currentSelectedCodeGroup = Ext.getCmp('notifActivityDialogActGrpCb').getValue();

            if (currentSelectedCodeGroup && currentSelectedCodeGroup.getCount() > 0) {
                currentSelectedCodeGroup.each(function (code) {
                    store.add({ "text": code.get('code') + ' ' + code.get('kurztext'), "value": code });
                });
            }

            var activityGroupComboBox = Ext.getCmp('notifActivityDialogActCodeCb');
            activityGroupComboBox.clearValue();
            activityGroupComboBox.bindStore(store);
        }catch(ex){
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    setSelectionOfActivityCode: function () {
        try{
            var myModel = this.getViewModel();
            var activityCodeGroupsMap = myModel.get('activityCodeGroupsMap');

            if (activityCodeGroupsMap) {
                var notifActivity = myModel.get('notifActivity');
                var itemsCodeKey = notifActivity ? notifActivity.get('mncod') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsCodeKey)) {
                    var itemsGroupKey = notifActivity.get('mngrp');
                    var groupsCodes = activityCodeGroupsMap.get(itemsGroupKey);

                    if (groupsCodes && groupsCodes.getCount() > 0) {
                        var codeToSelect = null;

                        groupsCodes.each(function (code) {
                            if (code.get('code') === itemsCodeKey) {
                                codeToSelect = code;
                                return false;
                            }
                        });

                        if (codeToSelect) {
                            var comboBox = Ext.getCmp('notifActivityDialogActCodeCb');

                            var recordToSelect = comboBox.findRecordByValue(codeToSelect);

                            if (recordToSelect)
                                comboBox.select(recordToSelect);
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    getCurrentInputValues: function () {
        var retval = null;

        try {
            var shorttext = Ext.getCmp('notifActivityDialogDescrTextField').getValue();
            var activityCustCode = Ext.getCmp('notifActivityDialogActCodeCb').getValue();

            retval = {
                shorttext: shorttext,
                activityCustCode: activityCustCode
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    }
});