Ext.define('AssetManagement.base.view.dialogs.BaseOnlineGoodsMovementDialog', {
    extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',


    requires: [
        'AssetManagement.customer.model.dialogmodel.OnlineGoodsMovementDialogViewModel',
        'AssetManagement.customer.controller.dialogs.OnlineGoodsMovementDialogController',
        'AssetManagement.customer.view.utils.ProgressBar',
        'Ext.container.Container'
    ],

  inheritableStatics: {
        _instance: null,

        getInstance: function() {
            try {
                if(this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.OnlineGoodsMovementDialog');
                }
            } catch(ex) {
                AssetManagement.helper.OxLogger.logException('Exception occurred inside getInstance of BaseOnlineGoodsMovementDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'OnlineGoodsMovementDialogModel'
    },

    controller: 'OnlineGoodsMovementDialogController',
    width: 440,
    header: false,
    modal: true,

    layout: {
        type: 'vbox'
    },

    _progressBar: null,
    _titleBar: null,
    _cancelButton: null,
    _searchButton: null,
    _messageLine : null,

    buildUserInterface: function() {
        try {
            this.callParent();
        } catch(ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseOnlineGoodsMovementDialog', ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function() {
        try {
            this._titleBar.setHtml('Syncing SAP');
            // this._titleBar.setHtml(Locale.getMsg('creatingOrder'));
            this._messageLine.setHtml(Locale.getMsg('communicatingWithSAP'));
        } catch(ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseOnlineGoodsMovementDialog', ex);
        }
    }
});