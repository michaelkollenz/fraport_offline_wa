Ext.define('AssetManagement.base.view.dialogs.BaseLgortPickerDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',
    alias: 'widget.BaseLgortPickerDialog',

    requires: [
        'AssetManagement.customer.controller.dialogs.LgortPickerDialogController',
        'AssetManagement.customer.model.dialogmodel.LgortPickerDialogViewModel',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.utils.OxComboBox'
    ],

    viewModel: {
        type: 'LgortPickerDialogModel'
    },

    controller: 'LgortPickerDialogController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    header: false,
    modal: true,

    _titleBar: null,
    _progressBar: null,
    _messageLine: null,
    _cancelButton: null,

  inheritableStatics: {
        _instance: null,

        getInstance: function (arguments) {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.LgortPickerDialog', arguments);
                }

                // this._instance.setInitialWidth(arguments ? arguments.width : undefined);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseLgortPickerDialog', ex);
            }

            return this._instance;
        }
    },

    //private
    buildUserInterface: function() {
        try {
            // this.callParent(arguments);

            var items = [
                {
                    xtype: 'container',
                    id: 'lgortChangeMainContainer',
                    margin:'10 0 0 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items:[
                        {
                            xtype: 'label',
                            margin: '0 10 10 10',
                            text: Locale.getMsg('selectLgort')

                        },
                        {
                            xtype: 'oxcombobox',
                            id: 'lgortChangeSelectionCombobox',
                            margin: '0 10 10 10',
                            editable: false,
                            displayField: 'lgort',
                            valueField: 'value',
                            queryMode: 'local',
                            height: 30
                        }
                    ]
                },
                {
                    xtype: 'container',
                    // margin:'10 0 0 0',
                    flex: 1,
                    cls: 'oxDialogButtonBar',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype:'button',
                            flex: 1,
                            cls: 'oxDialogButton first',
                            text: Locale.getMsg('dialogOk'),
                            listeners: {
                                click: 'onConfirmSelection'
                            }
                        }
                    ]
                }
            ];

            this.add(items);

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseLgortPickerDialog', ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function() {
        try {
            //parent will update handle usual progress dialog contents
            this.callParent();

            this.manageVanCombobox();
            // this.manageProgressBar();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseLgortPickerDialog', ex);
        }
    },


    //private
    manageVanCombobox: function () {
        try {
            var myModel = this.getViewModel();
            // var isSelectionMode = myModel.get('isSelectionMode');
            var lgortStore = myModel.get('lgortStore');

            var vanCombobox = Ext.getCmp('lgortChangeSelectionCombobox');
            var comboboxStore = Ext.create('Ext.data.Store',{
                fields:['lgort', 'value']
            });

            var alreadySelectedVan = myModel.get('selectedLgort');
            var selection = null;

            if(lgortStore && lgortStore.getCount() > 0){
                lgortStore.each(function (lgort){
                    var comboRecord = Ext.create('Ext.data.Model', { lgort: lgort.get('werks') + ' ' +lgort.get('werksbez')+ ' / ' +lgort.get('lgort')+ ' '+lgort.get('lgobe'), value: lgort});
                    comboboxStore.add(comboRecord);
                    if (alreadySelectedVan && alreadySelectedVan.get('werks') === lgort.get('werks') && alreadySelectedVan.get('lgort') === lgort.get('lgort')) {
                        selection = comboRecord;
                    }
                });
            }
            vanCombobox.setStore(comboboxStore);

            // if (isSelectionMode) {
            //     vanCombobox.setDisabled(false);
            // } else {
            //     vanCombobox.setDisabled(true);
            // }
            vanCombobox.select(selection);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageVanCombobox of BaseLgortPickerDialog', ex);
        }
    },
    getInputValues: function () {
        var retval = null;
        try {
            retval = Ext.getCmp('lgortChangeSelectionCombobox').getValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInputValues of BaseLgortPickerDialog', ex);
        }
        return retval;

    }
});