Ext.define('AssetManagement.base.view.dialogs.BasePartnerPickerDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.PartnerPickerDialogController',
        'AssetManagement.customer.model.dialogmodel.PartnerPickerDialogViewModel',
        'Ext.Component',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.View',
        'Ext.button.Button',
        'AssetManagement.customer.view.OxDynamicGridPanel'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.PartnerPickerDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BasePartnerPickerDialog', ex);
            }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.PartnerPickerDialogRendererMixin'
    },

    viewModel: {
        type: 'PartnerPickerDialogModel'
    },

    controller: 'PartnerPickerDialogController',
    height: 500,
    width: 700,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var myController = this.getController();
            var me = this;
            var items = [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    id: 'partnerPickerSearch',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'textfield',
		                            flex: 0.7,
		                            id: 'partnerPickerSearchTextField',
		                            labelStyle: 'display: none;'
		                        },
		                        {
		                            xtype: 'component',
		                            width: 10
		                        },
		                        {
		                            xtype: 'oxcombobox',
		                            flex: 0.2,
		                            id: 'partnerPickerDropDownField',
		                            labelStyle: 'display: none;'
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'oxdynamicgridpanel',
		                    id: 'partnerPickerGrid',
		                    scollable: 'vertical',
		                    flex: 1,
		                    disableSelection: true,
		                    header: false,
		                    title: Locale.getMsg('partners'),
		                    forceFit: true,
		                    parentController: myController,
                            owner: me,
		                    /*columns: [
		                        {
		                            xtype: 'gridcolumn',
		                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('parnr'), '0');;
		                            },
		                            dataIndex: 'parnr',
		                            text: Locale.getMsg('partner')
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                //store -> item -> aufnr, ktext
		                                var firstname = record.get('firstname');
		                                var lastname = record.get('lastname');
		                                var retVal = firstname;
		                                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lastname))
		                                    retVal += ' ' + lastname;
		                                return retVal;
		                            },
		                            dataIndex: 'firstname',
		                            text: Locale.getMsg('name')
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                //store -> item -> aufnr, ktext
		                                var postlcode = record.get('postlcod1');
		                                var city = record.get('city');
		                                var retVal = postlcode + ' ' + city;
		                                return retVal;
		                            },
		                            dataIndex: 'postlcod1',
		                            text: Locale.getMsg('checklist_Loc')
		                        }
		                    ],*/
		                    listeners: {
		                        cellclick: myController.onPartnerCellSelected,
		                        scope: myController
		                    }
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'button',
		                    cls: 'oxDialogButton first',
		                    text: Locale.getMsg('cancel'),
		                    listeners: {
		                        click: 'cancel'
		                    }
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
            ];

            this.add(items);
            this.searchFieldVisibility();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BasePartnerPickerDialog', ex);
        }
    },

    //protected
    //@override
    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('partners');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BasePartnerPickerDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            this.resetViewState();

            //get data from modelview
            var partnerGrid = Ext.getCmp('partnerPickerGrid');
            partnerGrid.setStore(this.getViewModel().get('partners'));

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BasePartnerPickerDialog', ex);
        }
    },

    //public
    //@override
    resetViewState: function () {
        try {
            //since the store ref it self is not nullable, put an empty one inside
            Ext.getCmp('partnerPickerGrid').setStore(Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.Partner',
                autoLoad: false
            }));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BasePartnerPickerDialog', ex);
        }
    },

    searchFieldVisibility: function () {
        try {
            var field = Ext.getCmp('partnerPickerSearch');
            var para = false;

            if (para === false)
                field.setVisible(false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BasePartnerPickerDialog', ex);
        }
    }
});