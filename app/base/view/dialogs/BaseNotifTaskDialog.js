Ext.define('AssetManagement.base.view.dialogs.BaseNotifTaskDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.NotifTaskDialogController',
        'AssetManagement.customer.model.dialogmodel.NotifTaskDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance == null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.NotifTaskDialog');
                }           
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifTaskDialog', ex);
		    }
            
            return this._instance;
        }
    },
    
    viewModel: {
        type: 'NotifTaskDialogModel'
    },
    
    controller: 'NotifTaskDialogController',
    cls: 'oxDialogButtonBar',
    width: 500,
    header: false,
    modal: true,
   
    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
        try {
            var items = [
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            cls: 'oxHeaderLabel',
                            height: 25,
                            id: 'notifTaskDialogHeader'
                            // text: Locale.getMsg('createNotifTask')
                        },
                        {
                            xtype: 'component',
                            height: 15
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'oxcombobox',
                                    flex: 2,
                                    id: 'notifTaskDialogTaskGrpCb',
                                    maxWidth: 290,
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value',
                                    fieldLabel: Locale.getMsg('notifTask'),
                                    listeners: {
                                        select: 'onTaskGrpSelected'
                                    }
                                },
                                {
                                    xtype: 'component',
                                    width: 5
                                },
                                {
                                    xtype: 'oxcombobox',
                                    flex: 1,
                                    id: 'notifTaskDialogTaskCodeCb',
                                    labelStyle: 'display: none;',
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value'
                                }
                            ]
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'oxtextfield',
                            id: 'notifTaskDialogDescrTextField',
                            fieldLabel: Locale.getMsg('specification'),
                            maxLength: 40
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'onSaveButtonClicked'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'onCancelButtonClicked'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                }
            ];

            this.add(items);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    //@override
    getDialogTitle: function() {
        var retval = '';

        try {
            var notifTask = this.getViewModel().get('notifTask');

            retval = notifTask ? Locale.getMsg('editNotifTask') : Locale.getMsg('createNotifTask');

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    manageDialogTitle: function(){
        try{
            var title = this.getDialogTitle();
            Ext.getCmp('notifTaskDialogHeader').setText(title);
        }catch(ex){
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

        }
    },
    //protected
    //@override
    updateDialogContent: function() {
        try {

            this.manageDialogTitle();

            this.manageTaskCodeComboBoxes();

            this.manageShorttextInputField();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    manageTaskCodeComboBoxes: function () {
        try{
            this.fillTaskCodeGroupComboBox();

            this.setSelectionOfTaskCodeGroup();

            this.fillTaskCodeCombobox();

            this.setSelectionOfTaskCode();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //clears the input field value or fills it with the current notif item short text
    manageShorttextInputField: function () {
        try {
            var myModel = this.getViewModel();
            var notifTask = myModel.get('notifTask');
            var shorttext = notifTask ? notifTask.get('matxt') : '';

            Ext.getCmp('notifTaskDialogDescrTextField').setValue(shorttext);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //fill comboBoxes damageCodeGroup, ObjectCodeGroup
    fillTaskCodeGroupComboBox: function() {
        try {
            var myModel = this.getViewModel();
            var taskCodeGroupsMap = myModel.get('taskCodeGroupsMap');
            //fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (taskCodeGroupsMap) {
                taskCodeGroupsMap.each(function (custCodeGroup, groupsCodes) {
                    if (groupsCodes.getCount() > 0) {
                        store.add({ "text": custCodeGroup + ' ' + groupsCodes.getAt(0).get('codegrkurztext'), "value": groupsCodes });
                    }
                }, this);
            }

            var taskGrpComboBox = Ext.getCmp('notifTaskDialogTaskGrpCb');
            taskGrpComboBox.clearValue();
            taskGrpComboBox.bindStore(store);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    setSelectionOfTaskCodeGroup: function () {
        try {
            var myModel = this.getViewModel();
            var taskCodeGroupsMap = myModel.get('taskCodeGroupsMap');

            if (taskCodeGroupsMap) {
                var notifTask = myModel.get('notifTask');
                var itemsGroupKey = notifTask ? notifTask.get('mngrp') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsGroupKey)) {
                    var groupToSelect = taskCodeGroupsMap.get(itemsGroupKey);

                    if (groupToSelect) {
                        var comboBox = Ext.getCmp('notifTaskDialogTaskGrpCb');

                        var recordToSelect = comboBox.findRecordByValue(groupToSelect);

                        if (recordToSelect)
                            comboBox.select(recordToSelect);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    fillTaskCodeCombobox: function () {
        try {
            // fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            var currentSelectedCodeGroup = Ext.getCmp('notifTaskDialogTaskGrpCb').getValue();

            if (currentSelectedCodeGroup && currentSelectedCodeGroup.getCount() > 0) {
                currentSelectedCodeGroup.each(function (code) {
                    store.add({ "text": code.get('code') + ' ' + code.get('kurztext'), "value": code });
                });
            }

            var taskGroupComboBox = Ext.getCmp('notifTaskDialogTaskCodeCb');
            taskGroupComboBox.clearValue();
            taskGroupComboBox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    setSelectionOfTaskCode: function () {
        try {
            var myModel = this.getViewModel();
            var taskCodeGroupsMap = myModel.get('taskCodeGroupsMap');

            if (taskCodeGroupsMap) {
                var notifTask = myModel.get('notifTask');
                var itemsCodeKey = notifTask ? notifTask.get('mncod') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsCodeKey)) {
                    var itemsGroupKey = notifTask.get('mngrp');
                    var groupsCodes = taskCodeGroupsMap.get(itemsGroupKey);

                    if (groupsCodes && groupsCodes.getCount() > 0) {
                        var codeToSelect = null;

                        groupsCodes.each(function (code) {
                            if (code.get('code') === itemsCodeKey) {
                                codeToSelect = code;
                                return false;
                            }
                        });

                        if (codeToSelect) {
                            var comboBox = Ext.getCmp('notifTaskDialogTaskCodeCb');

                            var recordToSelect = comboBox.findRecordByValue(codeToSelect);

                            if (recordToSelect)
                                comboBox.select(recordToSelect);
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    getCurrentInputValues: function () {
        var retval = null;

        try {
            var shorttext = Ext.getCmp('notifTaskDialogDescrTextField').getValue();
            var taskCustCode = Ext.getCmp('notifTaskDialogTaskCodeCb').getValue();

            retval = {
                shorttext: shorttext,
                taskCustCode: taskCustCode
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    }
});