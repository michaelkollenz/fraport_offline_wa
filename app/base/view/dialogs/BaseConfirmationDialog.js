Ext.define('AssetManagement.base.view.dialogs.BaseConfirmationDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.ConfirmationDialogController',
        'AssetManagement.customer.model.dialogmodel.ConfirmationDialogViewModel',
        'Ext.container.Container',
        'Ext.Component',
        'Ext.button.Button',
        'AssetManagement.customer.utils.StringUtils'
    ],
	
    viewModel: {
        type: 'ConfirmationDialogModel'
    },
    
    controller: 'ConfirmationDialogController',
    cls: 'oxDialogButtonBar',
    width: 400,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    //members
    _titleLabel: null,
    _icon: null,
    _messageLabel: null,
    _confirmButton: null,
    _declineButton: null,
    
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
    		    if(!this._instance)
    		    	this._instance = Ext.create('AssetManagement.customer.view.dialogs.ConfirmationDialog');
    		} catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseConfirmationDialog', ex);
		    }
    			
            return this._instance;
        }
    },
   
    buildUserInterface: function() {
    	try {
    		this._titleLabel = Ext.create('Ext.form.Label', {
    			height: 25,
	    		cls: 'oxHeaderLabel',
	    		html: ''
	    	});
    	
    		this._icon = Ext.create('Ext.Img', {
    			width: 40,
    			height: 40,
    			cls: 'alertDialogIcon',
    		    src: 'resources/icons/question.png'
    		});
    	
        	this._messageLabel = Ext.create('Ext.form.Label', {
        		flex: 1,
        		cls: 'alertDialogMessageLabel',
        		html: ''
        	});
        	
        	this._confirmButton = Ext.create('Ext.button.Button', {
        		flex: 1,
	            cls: 'oxDialogButton first',
	            listeners: {
	        		click: 'confirm'
	        	}
	    	});
        	
	    	this._declineButton = Ext.create('Ext.button.Button', {
        		flex: 1,
	            cls: 'oxDialogButton',
	            listeners: {
	        		click: 'cancel'
	        	}
	    	});
    	
	    	var items =  [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 20
		                },
		                this._titleLabel,
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'center',
		                        pack: 'stretch'
		                    },
		                    items: [
		                        this._icon,
		                        {
		                        	type: 'component',
		                        	width: 15
		                        },
		                        this._messageLabel
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    cls: 'oxDialogButtonBar',
		                    layout: {
		                        type: 'hbox'
		                    },
		                    items: [
		                        this._confirmButton,
		                        this._declineButton
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
		    ];
		   
	    	this.add(items);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseConfirmationDialog', ex);
		}
	},
	
	//protected
	//@override
	updateDialogContent: function() {
		try {
			var myModel = this.getViewModel();
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('title'))) {
				this._titleLabel.setHtml(myModel.get('title'));
				this._titleLabel.show();
			} else {
				this._titleLabel.hide();
			}
		
			//set the icon
			if(myModel.get('icon')) {
				this._icon.setSrc(myModel.get('icon'));
			} else {
				this._icon.setSrc('resources/icons/question.png');
			}
			
			//set the message
			if(myModel.get('asHtml') === true) {
				this._messageLabel.setText('');
				this._messageLabel.setHtml(myModel.get('message'));
			} else {
				this._messageLabel.setText(myModel.get('message'));
			}
			
			//set confirm button's text
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('alternateConfirmText'))) {
				this._confirmButton.setText(myModel.get('alternateConfirmText'));
			} else {
				this._confirmButton.setText(Locale.getMsg('dialogYes'));
			}
			
			var hideDeclineButton = myModel.get('hideDeclineButton');
			
			if(hideDeclineButton === true) {
				this._declineButton.hide();
			} else {
				this._declineButton.show();
			}
			
			//set decline button's text
			if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('alternateDeclineText'))) {
				this._declineButton.setText(myModel.get('alternateDeclineText'));
			} else {
				this._declineButton.setText(Locale.getMsg('dialogNo'));
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseConfirmationDialog', ex);
		}
	}
});