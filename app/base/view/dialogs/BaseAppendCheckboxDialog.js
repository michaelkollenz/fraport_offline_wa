﻿Ext.define('AssetManagement.base.view.dialogs.BaseAppendCheckboxDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [

        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.dialogs.AppendCheckboxDialogController',
        'AssetManagement.customer.model.dialogmodel.AppendCheckboxDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.CharactValueCheckboxItem'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.AppendCheckboxDialog');
                }

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseAppendCheckboxDialog', ex);
            }

            return this._instance;
        }

    },

    viewModel: {
        type: 'AppendCheckboxDialogModel'
    },

    controller: 'AppendCheckboxDialogController',
    width: 500,
    floating: true,
    modal: true,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var items = [
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            id: 'classEditDialogTitle2',
                            cls: 'oxHeaderLabel',
                            height: 25
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            id: 'itemContainer2',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            xtype: 'component',
                            minHeight: 5
                        },

                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'save'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'cancel'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                }
            ];

            this.add(items);
            this.updateDialogContent();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseAppendCheckboxDialog', ex);
        }
    },

    getDialogTitle: function () {
        var retval = '';

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('charact')))
                retval = Locale.getMsg('classificationsFor') + " " + this.getViewModel().get('charact').get('atbez');
            else
                retval = Locale.getMsg('classifications');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseAppendCheckboxDialog', ex);
        }

        return retval;
    },

    updateDialogContent: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('classEditDialogTitle2').setText(title);
            this.resetFields();
            this.fillItemContainerWithCheckboxes();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseAppendCheckboxDialog', ex);
        }
    },

    resetFields: function () {
        try {
            Ext.getCmp('itemContainer2').removeAll();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseAppendCheckboxDialog', ex);
        }
    },

    createTextField: function () {
        var retval = null;
        try {
            var charact = this.getViewModel().get('charact');
            var objClass = this.getViewModel().get('objClass');
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues');
            var textInput = Ext.create('AssetManagement.customer.view.CharactValueTextFieldItem', {
                charact: charact,
                charactValue: charactList,
                classValues: classValues
            });
            retval = Ext.getCmp('itemContainer2').add(textInput);

        } catch (ex) {
            AssetManagemenet.helper.OxLogger.logException('Exception occurred inside createTextField of BaseAppendCheckboxDialog', ex);
        }
        return retval;
    },

    fillItemContainerWithCheckboxes: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var objClass = myModel.get('objClass');
            if (charact === null || charact === undefined)
                return;
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues');

            var newClassValue = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            var classValue = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });
            if (classValues) {
                classValues.each(function (item) {

                    if (item === undefined || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(item.get('atwrt'))) {
                        classValues.remove(item);
                    } else {
                        // prepare item data
                        var sAtwrt = item.get('atwrt');
                        var sAtflv = item.get('atflv');
                        var sAtzhl = item.get('atzhl');

                        sAtflv = sAtflv ? sAtflv : '';
                        if (sAtflv.length < 1)
                            sAtflv = '';
                        classValue.add(item);
                        // create checkbox              // Ext.form.field.Checkbox
                        var checkBox = Ext.create('AssetManagement.customer.view.CharactValueCheckboxItem', {
                            charact: charact,
                            charactValue: charactList,
                            classValue: item
                        });
                    }
                    //set checkbox status
                    if (classValue.getCount() > 0 && checkBox !== undefined)
                        checkBox.setValue(true);                // Set status true


                    Ext.getCmp('itemContainer2').add(checkBox);
                });

            } else {
                classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass);
                newClassValue.add(classValue);
                charact.set('classValues', newClassValue);
            }
            this.createTextField();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseAppendCheckboxDialog', ex);

        }
    },

    //get current values from screen
    getCurrentInputValues: function () {
        var retval = null;

        try {
            var value = Ext.getCmp('itemContainer2').items.items[0].getValue();
            var charact = this.getViewModel().get('charact');

            if (charact && charact.get('atfor') === 'CHAR' && charact.get('atein') === 'X')
                var valueAtwrt = Ext.getCmp('itemContainer2').items.items[0].getValue();
            else
                var valueAtflv = Ext.getCmp('itemContainer2').items.items[0].getValue();

            retval = {
                atflv: valueAtflv,
                atwrt: valueAtwrt
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseAppendCheckboxDialog', ex);
            retval = null;
        }

        return retval;
    }

});