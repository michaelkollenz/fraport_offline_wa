Ext.define('AssetManagement.base.view.dialogs.BaseMaterialPickerDialog', {
  extend: 'AssetManagement.customer.view.dialogs.OxDialog',


  requires: [
    'AssetManagement.customer.controller.dialogs.MaterialPickerDialogController',
    'AssetManagement.customer.model.dialogmodel.MaterialPickerDialogViewModel',
    'Ext.form.field.ComboBox',
    'Ext.tab.Panel',
    'AssetManagement.customer.view.OxDynamicGridPanel',
    'Ext.grid.Panel',
    'Ext.grid.column.Column',
    'Ext.grid.View',
    'Ext.tab.Tab'
  ],

  config: {
    minWidth: 300,
    maxWidth: 700
  },

  inheritableStatics: {
    _instance: null,


    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.dialogs.MaterialPickerDialog');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMaterialPickerDialog', ex);
      }
      return this._instance;
    }
  },

  mixins: {
    rendererMixin: 'AssetManagement.customer.view.mixins.MaterialPickerDialogRendererMixin'
  },

  viewModel: {
    type: 'MaterialPickerDialogModel'
  },

  controller: 'MaterialPickerDialogController',
  width: 700,
  header: false,
  modal: true,
  floating: true,
  resizable: false,

  layout: {
    type: 'hbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {
      var myController = this.getController();
      var me = this;
      var items = [
        {
          xtype: 'component',
          width: 10
        },
        {
          xtype: 'container',
          flex: 1,
          layout: {
            type: 'vbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'component',
              height: 10
            },
            {
              xtype: 'container',
              height: 30,
              id: 'searchMaterialPickerContainer',
              layout: {
                type: 'hbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'textfield',
                  margin: '0 0 0 5',
                  flex: 1,
                  id: 'searchFieldMaterialPickerList',
                  labelStyle: 'display: none;',
                  listeners: {
                    change: {
                      fn: 'onMaterialPickerDialogSearchFieldChange'
                    }
                  }
                },
                {
                  xtype: 'oxcombobox',
                  margin: '0 5 0 10',
                  width: 250,
                  id: 'materialPickerPlannedFilterCriteriaCombobox',
                  labelStyle: 'display: none;',
                  editable: false,
                  hidden: true,
                  store: [Locale.getMsg('materials'), Locale.getMsg('orderComponents_Qty'), Locale.getMsg('operation')],
                  value: Locale.getMsg('materials'),
                  listeners: {
                    select: 'onMaterialPickerDialogSearchFieldChange'
                  }
                },
                {
                  xtype: 'oxcombobox',
                  margin: '0 5 0 10',
                  width: 250,
                  id: 'materialPickerBomFilterCriteriaCombobox',
                  labelStyle: 'display: none;',
                  editable: false,
                  hidden: true,
                  store: [Locale.getMsg('materials'), Locale.getMsg('shorttext'), Locale.getMsg('positionNumberAndType'),
                    Locale.getMsg('requiredQuant')],
                  value: Locale.getMsg('materials'),
                  listeners: {
                    select: 'onMaterialPickerDialogSearchFieldChange'
                  }
                },
                {
                  xtype: 'oxcombobox',
                  margin: '0 5 0 10',
                  width: 250,
                  id: 'materialPickerUnplannedFilterCriteriaCombobox',
                  labelStyle: 'display: none;',
                  editable: false,
                  hidden: true,
                  store: [Locale.getMsg('materials'), Locale.getMsg('shorttext'), Locale.getMsg('quantity'),
                    Locale.getMsg('unit'), Locale.getMsg('matConf_Plant')],
                  value: Locale.getMsg('materials'),
                  listeners: {
                    select: 'onMaterialPickerDialogSearchFieldChange'
                  }
                },
                {
                  xtype: 'oxcombobox',
                  margin: '0 5 0 10',
                  width: 250,
                  id: 'materialPickerallMaterialsFilterCriteriaCombobox',
                  labelStyle: 'display: none;',
                  editable: false,
                  hidden: true,
                  store: [Locale.getMsg('materials'), Locale.getMsg('shorttext')],
                  value: Locale.getMsg('materials'),
                  listeners: {
                    select: 'onMaterialPickerDialogSearchFieldChange'
                  }
                }
              ]
            },
            {
              xtype: 'component',
              height: 10
            },
            {
              xtype: 'container',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'tabpanel',
                  id: 'materialDialogTabPanel',
                  activeTab: 0,
                  items: [
                    {
                      xtype: 'panel',
                      title: Locale.getMsg('plannedMaterials'),
                      itemId: 'plannedMaterialTab',
                      items: [
                        {
                          xtype: 'container',
                          height: 20
                        },
                        {
                          xtype: 'oxdynamicgridpanel',
                          id: 'plannedMaterialGridPanel',
                          scollable: 'vertical',
                          disableSelection: true,
                          hideContextMenuColumn: true,
                          header: false,
                          forceFit: true,
                          height: 330,
                          parentController: myController,
                          owner: me,
                          /*columns: [
                           {
                           xtype: 'actioncolumn',
                           maxWidth: 70,
                           minWidth: 70,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           return '<img src="resources/icons/settings.png"  />';
                           },
                           align: 'center'
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('material'),
                           flex: 1,
                           dataIndex: 'matnr',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var material = record.get('material');
                           var maktx = material ? material.get('maktx') : '';

                           return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('orderComponents_Qty'),
                           flex: 0.5,
                           dataIndex: 'bdmng',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bdmng'));
                           return amount + ' ' + record.get('meins');
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('operation'),
                           flex: 0.4,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           return record.get('vornr');
                           }
                           }
                           ],*/
                          listeners: {
                            cellclick: myController.plannedSelected,
                            scope: myController
                          }
                        }
                      ],
                      tabConfig: {
                        xtype: 'tab',
                        flex: 1
                      }
                    },
                    {
                      //BOM
                      xtype: 'panel',
                      title: Locale.getMsg('basicSchedule'),
                      itemId: 'bomTab',
                      items: [
                        {
                          xtype: 'container',
                          height: 20
                        },
                        {
                          xtype: 'oxdynamicgridpanel',
                          id: 'stockListGridPanel',
                          scollable: 'vertical',
                          disableSelection: true,
                          hideContextMenuColumn: true,
                          forceFit: true,
                          header: false,
                          height: 330,
                          parentController: myController,
                          owner: me,
                          /*columns: [
                           {
                           xtype: 'actioncolumn',
                           maxWidth: 70,
                           minWidth: 70,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           return '<img src="resources/icons/matconf.png"  />';
                           },
                           align: 'center'
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('material'),
                           flex: 1,
                           dataIndex: 'idnrk',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var material = record.get('material');
                           var maktx = material ? material.get('maktx') : '';

                           return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('positionNumberAndType'),
                           dataIndex: 'posnr',
                           flex: 0.5,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var posnr = record.get('posnr');
                           var postype = record.get('postp');

                           return AssetManagement.customer.utils.StringUtils.concatenate([ posnr, postype ], ' / ', false, true, '-');
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('requiredQuant'),
                           dataIndex: 'menge',
                           flex: 0.5,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
                           var unit = record.get('meins');

                           return AssetManagement.customer.utils.StringUtils.concatenate([ quantity, unit ], ' ', true);
                           }
                           }
                           ],*/
                          listeners: {
                            cellclick: myController.bomEntrySelected,
                            scope: myController
                          }
                        }
                      ],
                      tabConfig: {
                        xtype: 'tab',
                        flex: 1
                      }
                    },
                    {
                      //Matstock
                      xtype: 'panel',
                      itemId: 'matStockTab',
                      title: Locale.getMsg('unplanedMat'),
                      items: [
                        {
                          xtype: 'container',
                          height: 20
                        },
                        {
                          xtype: 'oxdynamicgridpanel',
                          id: 'unplannedMaterialGridPanel',
                          scollable: 'vertical',
                          disableSelection: true,
                          hideContextMenuColumn: true,
                          forceFit: true,
                          header: false,
                          height: 330,
                          parentController: myController,
                          owner: me,
                          /*columns: [
                           {
                           xtype: 'actioncolumn',
                           maxWidth: 70,
                           minWidth: 70,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           return '<img src="resources/icons/matconf.png"/>';
                           },
                           align: 'center'
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('material'),
                           flex: 1,
                           dataIndex: 'matnr',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var material = record.get('material');
                           var maktx = material ? material.get('maktx') : '';

                           return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('orderComponents_Qty'),
                           flex: 0.5,
                           dataIndex: 'labst',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
                           var unit = record.get('meins');

                           return quant + ' ' + unit;
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('plantStorLoc'),
                           flex: 0.5,
                           dataIndex: 'werks',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var plant = record.get('werks');
                           var storloc = record.get('lgort');

                           return AssetManagement.customer.utils.StringUtils.concatenate([ plant, storloc ], '/');
                           }
                           }
                           ],*/
                          listeners: {
                            cellclick: myController.unplannedSelected,
                            scope: myController
                          }
                        }
                      ],
                      tabConfig: {
                        xtype: 'tab',
                        flex: 1
                      }
                    },
                    {
                      //Matstock
                      xtype: 'panel',
                      itemId: 'onlineMatTab',
                      title: Locale.getMsg('onlineSearch'),
                      items: [
                        {
                          xtype: 'container',
                          height: 20
                        },
                        {
                          xtype: 'oxdynamicgridpanel',
                          id: 'onlineMaterialGridPanel',
                          scollable: 'vertical',
                          hideContextMenuColumn: true,
                          disableSelection: true,
                          forceFit: true,
                          header: false,
                          height: 330,
                          parentController: myController,
                          owner: me,
                          /*columns: [
                           {
                           xtype: 'actioncolumn',
                           maxWidth: 70,
                           minWidth: 70,
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           return '<img src="resources/icons/matconf.png"/>';
                           },
                           align: 'center'
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('material'),
                           flex: 1,
                           dataIndex: 'matnr',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var material = record.get('material');
                           var maktx = material ? material.get('maktx') : '';

                           return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('orderComponents_Qty'),
                           flex: 0.5,
                           dataIndex: 'labst',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
                           var unit = record.get('meins');

                           return quant + ' ' + unit;
                           }
                           },
                           {
                           xtype: 'gridcolumn',
                           text: Locale.getMsg('plantStorLoc'),
                           flex: 0.5,
                           dataIndex: 'werks',
                           renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                           var plant = record.get('werks');
                           var storloc = record.get('lgort');

                           return AssetManagement.customer.utils.StringUtils.concatenate([ plant, storloc ], '/');
                           }
                           }
                           ],*/
                          listeners: {
                            cellclick: myController.onlineMaterialSelected,
                            scope: myController
                          }
                        }
                      ],
                      tabConfig: {
                        xtype: 'tab',
                        flex: 1
                      }
                    },
                    {
                      xtype: 'panel',
                      title: Locale.getMsg('materials'),
                      itemId: 'allMaterialsTab',
                      items: [
                        {
                          xtype: 'container',
                          height: 20
                        },
                        {
                          xtype: 'oxdynamicgridpanel',
                          id: 'allMaterialsGridPanel',
                          scollable: 'vertical',
                          hideContextMenuColumn: true,
                          disableSelection: true,
                          forceFit: true,
                          header: false,
                          height: 330,
                          parentController: myController,
                          owner: me,
                          listeners: {
                            cellclick: myController.materialSelected,
                            scope: myController
                          }
                        }
                      ],
                      tabConfig: {
                        xtype: 'tab',
                        flex: 1
                      }
                    }
                  ],
                  listeners: {
                    tabchange: 'onTabChannelChanged'
                  }

                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  },
                  cls: 'oxDialogButtonBar',
                  items: [
                    {
                      xtype: 'button',
                      flex: 1,
                      cls: 'oxDialogButton first',
                      text: Locale.getMsg('cancel'),
                      listeners: {
                        click: 'cancel'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          xtype: 'component',
          width: 10
        }
      ];

      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMaterialPickerDialog', ex);
    }
  },

  //protected
  //@override
  getPageTitle: function () {
    var retval = '';

    try {
      retval = Locale.getMsg('chooseMaterial');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMaterialPickerDialog', ex);
    }

    return retval;
  },

  //protected
  //@override
  updateDialogContent: function () {
    try {
      var myModel = this.getViewModel();
      var tabPanel = Ext.getCmp('materialDialogTabPanel');
      var order = this.getViewModel().get('order');

      if (myModel.get('useComponents') === true && order) {
        tabPanel.child('#plannedMaterialTab').tab.show();
      } else {
        tabPanel.child('#plannedMaterialTab').tab.hide();
      }

      if (myModel.get('useBillOfMaterial') === true && order) {
        tabPanel.child('#bomTab').tab.show();
      } else {
        tabPanel.child('#bomTab').tab.hide();
      }

      if (myModel.get('useAllMaterials')) {
        tabPanel.child('#allMaterialsTab').tab.show();
      } else {
        tabPanel.child('#allMaterialsTab').tab.hide();
      }

      if (myModel.get('useMatStocks')) {
        tabPanel.child('#matStockTab').tab.show();
      } else {
        tabPanel.child('#matStockTab').tab.hide();
      }

      if (myModel.get('tabIndex') !== null) {
        tabPanel.setActiveTab(myModel.get('tabIndex'));
      } else {
        var startTabIndex = -1;

        if (order && myModel.get('useComponents'))
          startTabIndex = 0;
        else if (order && myModel.get('useBillOfMaterial'))
          startTabIndex = 1;
        else if (myModel.get('useMatStocks'))
          startTabIndex = 2;

        if (startTabIndex > -1)
          tabPanel.setActiveTab(startTabIndex);

        this.setCriteriaComboBox(tabPanel.getActiveTab());
      }

      myModel.set('tabIndex', tabPanel.getActiveTab());

      //get data from modelview
      var matStocks = myModel.get('matStocks');
      var components = myModel.get('components');
      var bom = myModel.get('billOfMaterial');
      var allMaterials = myModel.get('allMaterials');

      //UNKNOWN BEHAVIOR: reconfigure twice to avoid sometimes empty gridpanels
      //issue only appears when using tabs

      var unplannedGrid = Ext.getCmp('unplannedMaterialGridPanel');
      unplannedGrid.reconfigure(matStocks);
      unplannedGrid.reconfigure(matStocks);

      var plannedGrid = Ext.getCmp('plannedMaterialGridPanel');
      plannedGrid.reconfigure(components);
      plannedGrid.reconfigure(components);

      var bomGrid = Ext.getCmp('stockListGridPanel');
      bomGrid.reconfigure(bom);
      bomGrid.reconfigure(bom);

      var onlineMaterialGrid = Ext.getCmp('onlineMaterialGridPanel');
      onlineMaterialGrid.setStore(myModel.get('onlineMaterialStore'));

      var allMaterialsGrid = Ext.getCmp('allMaterialsGridPanel');
      allMaterialsGrid.reconfigure(allMaterials);
      allMaterialsGrid.reconfigure(allMaterials);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseMaterialPickerDialog', ex);
    }
  },

  resetViewState: function () {
    try {
      //since the store ref it self is not nullable, put an empty one inside
      Ext.getCmp('plannedMaterialGridPanel').reconfigure(Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.OrdComponent',
        autoLoad: false
      }));

      Ext.getCmp('stockListGridPanel').reconfigure(Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.Mast',
        autoLoad: false
      }));

      Ext.getCmp('unplannedMaterialGridPanel').reconfigure(Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.MatStock',
        autoLoad: false
      }));
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseMaterialPickerDialog', ex);
    }
  },

  getSearchValue: function () {
    try {
      var searchMaterial = Ext.create('AssetManagement.customer.model.bo.MatStock', {material: Ext.create('AssetManagement.customer.model.bo.Material', {})});
      var currentFilterCriterion = null;
      if (Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox').isVisible()) {
        currentFilterCriterion = (Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox')).getValue();
      } else       if (Ext.getCmp('materialPickerBomFilterCriteriaCombobox').isVisible()) {
        currentFilterCriterion = (Ext.getCmp('materialPickerBomFilterCriteriaCombobox')).getValue();
      } else       if (Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox').isVisible()) {
        currentFilterCriterion = (Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox')).getValue();
      } else       if (Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox').isVisible()) {
        currentFilterCriterion = (Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox')).getValue();
      }

      if (currentFilterCriterion) {
        // Materialien
        if (currentFilterCriterion === Locale.getMsg('materials'))
          searchMaterial.get('material').set('matnr', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        // Kurztext
        else if (currentFilterCriterion === Locale.getMsg('shorttext'))
          searchMaterial.get('material').set('maktx', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        // Menge
        else if (currentFilterCriterion === Locale.getMsg('quantity'))
          searchMaterial.set('labst', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        // Einheit
        else if (currentFilterCriterion === Locale.getMsg('unit'))
          searchMaterial.set('meins', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        // Wert / Lagerort
        else if (currentFilterCriterion === Locale.getMsg('matConf_Plant')) {
          searchMaterial.set('werks', Ext.getCmp('searchFieldMaterialPickerList').getValue());
          searchMaterial.set('lgort', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        }
        else if (currentFilterCriterion === Locale.getMsg('operation')) {
          searchMaterial.set('vornr', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        }
        else if (currentFilterCriterion === Locale.getMsg('orderComponents_Qty')) {
          searchMaterial.set('bdmng', Ext.getCmp('searchFieldMaterialPickerList').getValue());
          //searchMaterial.set('meins', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        }
        else if (currentFilterCriterion === Locale.getMsg('requiredQuant')) {
          searchMaterial.set('menge', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        }
        else if (currentFilterCriterion === Locale.getMsg('positionNumberAndType')) {
          searchMaterial.set('posnr', Ext.getCmp('searchFieldMaterialPickerList').getValue());
          searchMaterial.set('postp', Ext.getCmp('searchFieldMaterialPickerList').getValue());
        }

      }


      this.getViewModel().set('searchMaterial', searchMaterial);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseMaterialPickerDialog', ex);
    }
  },

  setCriteriaComboBox: function (tabIndex) {
    tabIndex = Ext.getCmp('materialDialogTabPanel').getActiveTab();
    this.getViewModel().set('tabIndex', tabIndex);

    if (tabIndex.itemId === 'plannedMaterialTab') {
      Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox').setHidden(false);
      Ext.getCmp('materialPickerBomFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox').hide();
    } else if (tabIndex.itemId === 'bomTab') {
      Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerBomFilterCriteriaCombobox').setHidden(false);
      Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox').hide();
    } else if (tabIndex.itemId === 'matStockTab') {
      Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerBomFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox').setHidden(false);
      Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox').hide();
    } else if (tabIndex.itemId === 'matStockTab') {
      Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerBomFilterCriteriaCombobox').setHidden(true);
      Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox').setHidden(false);
      Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox').hide();
    }else if (tabIndex.itemId === 'allMaterialsTab') {
      Ext.getCmp('materialPickerPlannedFilterCriteriaCombobox').hide();
      Ext.getCmp('materialPickerBomFilterCriteriaCombobox').hide();
      Ext.getCmp('materialPickerUnplannedFilterCriteriaCombobox').hide();
      Ext.getCmp('materialPickerallMaterialsFilterCriteriaCombobox').show();
    }
  }
});
