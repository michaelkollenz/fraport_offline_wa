Ext.define('AssetManagement.base.view.dialogs.BaseScenarioDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
       'AssetManagement.customer.controller.dialogs.ScenarioDialogController',
       'AssetManagement.customer.model.dialogmodel.ScenarioDialogViewModel',
       'AssetManagement.customer.model.bo.Scenario',
       'Ext.container.Container',
       'Ext.Component',
       'Ext.button.Button'
    ],

    viewModel: {
        type: 'ScenarioDialogModel'
    },

   inheritableStatics: {
    	_instance: null,
    	getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.ScenarioDialog');
                }            
            } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseScenarioDialog', ex);
		    }
            
            return this._instance;
        }
    },
    
    controller: 'ScenarioDialogController',
    width: 250,
    header: false,
    modal:true,
    gridstore: null,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    onEsc: Ext.emptyFn,
    
    buildUserInterface: function() {
        try {
            var myController = this.getController();
            var items = [
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                    {
                        xtype: 'label',
                        margin: 10,
                        cls: 'oxHeaderLabel',
                        id: 'scenarioTitle',
                        text: Locale.getMsg('chooseScenario')
                    },
		            {
		                xtype: 'oxgridpanel',
		                id: 'ScenGridPanel',
                        margin: 5,
		                flex: 1,
		                hideHeaders: true,
		                border: false,
		                hideContextMenuColumn: true,
		                columns: [{
		                    align: 'center',
		                    sortable: false,
		                    xtype: 'gridcolumn',
		                    dataIndex: 'description',
		                    flex: 1
		                }],
		                listeners: {
		                    cellclick: myController.onItemSelected,
		                    scope: myController
		                }
		            },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        cls: 'oxDialogButtonBar',
                        items: [
                            {
                                xtype: 'button',
                                flex: 1,
                                cls: 'oxDialogButton first',
                                text: Locale.getMsg('cancel'),
                                handler: function(){
                                    if (Ext.getCmp('loginPageMessageLabel')) {
                                        Ext.getCmp('loginPageMessageLabel').setText('');
                                    }
                                },
                                listeners: {
                                    click: 'cancel'
                                }

                            }
                        ]
                    }]
                }
                
		    ];
	        
	        this.add(items);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseScenarioDialog', ex);
		}
    },
   
   	
	//protected
	//@override
    updateDialogContent: function() {
		try {
		    var me = this;
		    
		    var scenarios = this.getViewModel().get('scenarios');

		    if (scenarios)
			    Ext.getCmp('ScenGridPanel').setStore(scenarios);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseScenarioDialog', ex);
		}
	}
});