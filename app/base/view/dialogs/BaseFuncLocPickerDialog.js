Ext.define('AssetManagement.base.view.dialogs.BaseFuncLocPickerDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.FuncLocPickerDialogController',
        'AssetManagement.customer.model.dialogmodel.FuncLocPickerDialogViewModel',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.View',
        'Ext.button.Button'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.FuncLocPickerDialog');
                }            
            } catch(ex) {
    		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseFuncLocPickerDialog', ex);
    	    }
            
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.FuncLocPickerDialogRendererMixin'
    },

    viewModel: {
        type: 'FuncLocPickerDialogModel'
    },
    
    controller: 'FuncLocPickerDialogController',
    height: 500,
    width: 570,
    header: false,
    modal: true,
    floating: true, 

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
		    var items = [
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'container',
		                    height: 20
		                },
		                {
	                        xtype: 'container',
	                        layout: {
	                        type: 'hbox',
	                        align: 'stretch'
		                },
	                	items: [
		                    {
		                        xtype: 'component',
		                        height: 5
		                    },
		                    {
		                        xtype: 'label',
		                        cls: 'oxHeaderLabel',
		                        height: 25,
		                        text: Locale.getMsg('funcLocs')
		                    }
			                ]
			            },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    id: 'funcLocPickerSearchContainer', 
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'container',
		                            flex: 0.7,
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'textfield',
		                                    id: 'funcLocPickerNumberShortTextField',
		                                    fieldLabel: Locale.getMsg('funcLoc_Short')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    id: 'funcLocPickerSortFieldTextField',
		                                    fieldLabel: Locale.getMsg('sortField')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 0.3,
		                            layout: {
		                                type: 'hbox',
		                                align: 'center',
		                                pack: 'center'
		                            },
		                            items: [
                                        {
                                            xtype: 'button',
                                            id: 'searchButtonFuncLocPickerDialog',
                                            padding: '5 0 0 0',
                                            html: '<div><img width="50%" src="resources/icons/search.png"></img></div><p class=\'schwarz\'>' + Locale.getMsg('startSearch') + '</p><style type="text/css"> <!-- .schwarz { color:rgb(60,60,60); font-family:Helvetica;  font-size:9pt;  }</style>',
                                            height: 80,
                                            width: 90,
                                            icon: '',
                                            listeners: {
                                                click: 'onFuncLocSearchButtonClick'
                                            }

                                        }
		                            ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'container',
		                    height: 5
		                },
		                {
		                    xtype: 'oxdynamicgridpanel',
		                    flex: 1,
		                    id: 'funcLocPickerGrid',
		                    scollable: 'vertical',
		                    disableSelection: true,
		                    header: false,
		                    title: Locale.getMsg('funcLocs'),
		                    forceFit: true,
		                    parentController: myController,
                            owner: me,
		                    /*columns: [
		                        {
		                            xtype: 'gridcolumn',
			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                            return '<img src="resources/icons/funcloc.png"/>';
			                        },
			                        maxWidth: 70,
			                        minWidth: 70,
			                        align: 'center'
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            text: Locale.getMsg('funcLoc'),
		                            flex: 18, 
			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                            var tplnr = record.get('tplnr');
			                            var pltxt = record.get('pltxt');
			                            return tplnr +'<p>'+pltxt+'</p>';
			                        },
			                        dataIndex: 'tplnr'
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            text: Locale.getMsg('sortField'),
		                            dataIndex: 'eqfnr',
		                            flex: 14
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            sortable: false,
		                            text: Locale.getMsg('locationAndRoom'),
			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                            var stort = record.get('stort');
			                            var msgrp = record.get('msgrp');
			                            return stort +'<p>'+msgrp+'</p>';
			                        },
		                            flex: 13
		                        }
		                    ],*/
			                listeners: {
		                		cellclick: myController.onFuncLocCellSelected,
		                		scope: myController
		                	}
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
				                    xtype: 'button',
				                    flex: 1,
				                    cls: 'oxDialogButton first',
				                    text: Locale.getMsg('cancel'),
					                listeners: {
				                		click: 'cancel'
				                	}
				                }
		                    ]
		                }
		            ]
		        },
		        {
			        xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseFuncLocPickerDialog', ex);
		}
	},
   
	//protected
	//@override
	getPageTitle: function() {
	    var retval = '';
	    
	    try {
	        retval = Locale.getMsg('funcLocs');
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of FuncLockPickerDialog', ex);
		}
	    
		return retval;
	},

	//protected
	//@override
	updateDialogContent: function() {
		try {
		
			//get data from modelview
			var funcLocs = this.getViewModel().get('funcLocs');
			var funcLocsGrid = Ext.getCmp('funcLocPickerGrid');

			funcLocsGrid.reconfigure(funcLocs);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseFuncLocPickerDialog', ex);
		}
	},

	//public
	//@override
	resetViewState: function() {
	    try {
	        this.clearSearchFields();

			//since the store ref it self is not nullable, put an empty one inside
			Ext.getCmp('funcLocPickerGrid').reconfigure(Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.FuncLoc',
				autoLoad: false
			}));
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseFuncLocPickerDialog', ex);
    	}
	},

	clearSearchFields: function() {
	    try {
	        Ext.getCmp('funcLocPickerNumberShortTextField').setValue(''); 
	        Ext.getCmp('funcLocPickerSortFieldTextField').setValue('');
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchFields of BaseFuncLocPickerDialog', ex);
	    }
	},
	
	getSearchValues: function() {
	    var retval = null;
		
	    try {
	        var funcLocToSearch = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
			
	        funcLocToSearch.set('tplnr', Ext.getCmp('funcLocPickerNumberShortTextField').getValue());
	        funcLocToSearch.set('eqfnr', Ext.getCmp('funcLocPickerSortFieldTextField').getValue());
	
	        this.getViewModel().set('searchFuncLoc', funcLocToSearch);
			
	        retval = funcLocToSearch;
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValues of BaseFuncLocPickerDialog', ex);
	    }
		
	    return retval;
	}
});