Ext.define('AssetManagement.base.view.dialogs.BaseSDOrderItemDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.model.dialogmodel.SDOrderItemDialogViewModel',
        'AssetManagement.customer.controller.dialogs.SDOrderItemDialogController',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.button.Button'
    ],

   inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance == null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.SDOrderItemDialog');
                }          
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSDOrderItemDialog', ex);
		    }
            
            return this._instance;
        }
    },
    
    viewModel: {
        type: 'SDOrderItemDialogViewModel'
    },
    
    controller: 'SDOrderItemDialogController',
    
    width: 550,
    cls: 'oxDialogButtonBar',
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
	        var items = [
		        {
		            xtype: 'component',
		            maxWidth: 10,
		            minWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    id: 'sdOrderItemDialogTitleLabel',
		                    text: Locale.getMsg('createSDOrderItem')
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'oxtextfield',
		                    id: 'sdOrderItemDialogMaterialTextField',
		                    fieldLabel: Locale.getMsg('materialNumberShort'),
		                    maxLength: 18
		                },
		                {
		                    xtype: 'component',
		                    height: 5
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                        	xtype: 'oxnumberfield',
                                    flex: 1,
                                    id: 'sdOrderItemDialogAmountTextField',
                                    fieldLabel: Locale.getMsg('quantityUnit'),
                                    maxLength: 13
                                },
                                {
                                    xtype: 'component',
                                    width: 10
                                },
                                {
                                    xtype: 'oxtextfield',
                                    labelStyle: 'display: none;',
                                    width: 60,
                                    id: 'sdOrderItemDialogUnitTextField',
                                    maxLength: 3
                                }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 5
		                },
		                {
		                    xtype: 'oxtextfield',
		                    id: 'sdOrderItemDialogRemarkTextField',
		                    fieldLabel: Locale.getMsg('posText'),
		                    maxLength: 40
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('save'),
		                            listeners: {
		                        		click: 'save'
		                        	}
		                        },
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton',
		                            text: Locale.getMsg('cancel'), 
		                            listeners: {
		                        		click: 'cancel'
		                        	}
		                        }
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
			        maxWidth: 10,
		            minWidth: 10
		        }
		    ];
	        
	        this.add(items);
        } catch(ex) {
        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSDOrderItemDialog', ex);
        }
    },
    
	resetInputFields: function() {
		try{
			Ext.getCmp('sdOrderItemDialogMaterialTextField').setValue('');
			Ext.getCmp('sdOrderItemDialogAmountTextField').setValue(1);
			Ext.getCmp('sdOrderItemDialogUnitTextField').setValue('ST');
			Ext.getCmp('sdOrderItemDialogRemarkTextField').setValue('');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseSDOrderItemDialog', ex)
		}
	},

	//protected
	//@override
    updateDialogContent: function() {
		try {
			this.setTitleLabel();
		
			this.transferModelStateIntoView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseSDOrderItemDialog', ex);
		}
	},
	
	setTitleLabel: function() {
		try {
			var isEditMode = this.getViewModel().get('isEditMode');
			
			Ext.getCmp('sdOrderItemDialogTitleLabel').setText(isEditMode === true ? Locale.getMsg('editSDOrderItem') : Locale.getMsg('createSDOrderItem'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setTitleLabel of BaseSDOrderItemDialog', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var sdOrderItem = this.getViewModel().get('sdOrderItem');
		
			Ext.getCmp('sdOrderItemDialogMaterialTextField').setValue(sdOrderItem.get('matnr'));
			Ext.getCmp('sdOrderItemDialogAmountTextField').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(sdOrderItem.get('zmeng')));
			Ext.getCmp('sdOrderItemDialogUnitTextField').setValue(sdOrderItem.get('zieme'));
			Ext.getCmp('sdOrderItemDialogRemarkTextField').setValue(sdOrderItem.get('arktx'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseSDOrderItemDialog', ex);
		}
	},
	
	//get current values from screen into model
	transferViewStateIntoModel: function() {
		try {
			var sdOrderItem = this.getViewModel().get('sdOrderItem');
			
			sdOrderItem.set('matnr', Ext.getCmp('sdOrderItemDialogMaterialTextField').getValue());
			sdOrderItem.set('zmeng', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp('sdOrderItemDialogAmountTextField').getValue()));
			sdOrderItem.set('zieme', Ext.getCmp('sdOrderItemDialogUnitTextField').getValue());
			sdOrderItem.set('arktx', Ext.getCmp('sdOrderItemDialogRemarkTextField').getValue());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseSDOrderItemDialog', ex);
		}
	},
	
	//get current values for the current on editing order sd item from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			retval = {
				matnr: Ext.getCmp('sdOrderItemDialogMaterialTextField').getValue(),
				amount: Ext.getCmp('sdOrderItemDialogAmountTextField').getValue(),
				unit: Ext.getCmp('sdOrderItemDialogUnitTextField').getValue(),
				remark: Ext.getCmp('sdOrderItemDialogRemarkTextField').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseSDOrderItemDialog', ex);
			retval = null;
		}
		
		return retval;
	}
});