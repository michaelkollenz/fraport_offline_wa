Ext.define('AssetManagement.base.view.dialogs.BaseSynchronizationDialog', {
	extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.SynchronizationDialogController',
        'AssetManagement.customer.model.dialogmodel.SynchronizationDialogViewModel',
        'AssetManagement.customer.helper.OxLogger'
    ],
    
    viewModel: {
        type: 'SynchronizationDialogModel'
    },
    
    controller: 'SynchronizationDialogController',

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function(arguments) {
	        try {
                if(this._instance === null)
                {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.SynchronizationDialog', arguments);
                }
            
                this._instance.setInitialWidth(arguments ? arguments.width : undefined);            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSynchronizationDialog', ex);
	    	}
            
            return this._instance;
        }
    }

});