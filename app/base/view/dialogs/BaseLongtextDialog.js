Ext.define('AssetManagement.base.view.dialogs.BaseLongtextDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.model.dialogmodel.LongtextDialogViewModel',
        'AssetManagement.customer.controller.dialogs.LongtextDialogController',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.TextArea',
        'Ext.button.Button',
        'AssetManagement.customer.manager.LongtextManager',
        'AssetManagement.customer.utils.StringUtils'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.LongtextDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseLongtextDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'LongtextDialogViewModel'
    },

    controller: 'LongtextDialogController',
    width: 400,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    //members
    _saveButton: null,
    _cancelButton: null,

    buildUserInterface: function () {
        try {

             this._saveButton = Ext.create('Ext.button.Button', {
                flex: 1,
                cls: 'oxDialogButton',
                text: Locale.getMsg('save'), 
                listeners: {
                    click: 'save'
                }
            });

            this._cancelButton = Ext.create('Ext.button.Button', {
                flex: 1,
                cls: 'oxDialogButton',
                text: Locale.getMsg('cancel'),
                listeners: {
                    click: 'cancel'
                }
            });

            var items = [
			    {
			        xtype: 'component',
			        maxWidth: 10,
			        minWidth: 10
			    },
		        {
		            xtype: 'container',
		            id: 'mainContainer',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'container',
		                    height: 15
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    id: 'longtextTitle',
                            margin: '0 0 10 0',
		                    text: Locale.getMsg('longtext')
		                },
		                {
		                    xtype: 'container',
                            margin: '0 0 20 0',
		                    id: 'backendLongtextContainer',
		                    layout: 'vbox',
		                    maxHeight: 150,
		                    scrollable: true,
		                    style: 'background-color:white',
		                    items: [
							   {
							       xtype: 'displayfield',
							       id: 'backendLongtextDialogTextArea',
							       labelStyle: 'display: none',
							       maxWidth: 376
							   }
		                    ]
		                },
		                {
		                    xtype: 'container',
		                    id: 'localLongtextContainer',
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
	                           this.getNewLocalLongtextTextArea()
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    height: '',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        this._saveButton,
                                this._cancelButton
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            maxWidth: 10,
		            minWidth: 10
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseLongtextDialog', ex);
        }
    },

    //private
    renewTextAreas: function () {
        try {
            var backEndLongtextContainer = Ext.getCmp('backendLongtextContainer');

            var mainContainer = Ext.getCmp('mainContainer');

            if (backEndLongtextContainer && mainContainer) {
                mainContainer.remove(backEndLongtextContainer);

                var newBackendTextArea = this.getNewBackEndLongtextTextArea();

                if (newBackendTextArea)
                    mainContainer.insert(2, newBackendTextArea);
            }

            var localLongtextContainer = Ext.getCmp('localLongtextContainer');

            if (localLongtextContainer)
                localLongtextContainer.removeAll(true);

            var newLocalTextArea = this.getNewLocalLongtextTextArea();

            if (localLongtextContainer && newLocalTextArea) {
                localLongtextContainer.add(newLocalTextArea);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewTextAreas of BaseLongtextDialog', ex);
        }
    },

    //private
    getNewBackEndLongtextTextArea: function () {
        var retval = null;

        try {
            var myController = this.getController();

            retval = {
                xtype: 'container',
                id: 'backendLongtextContainer',
                layout: 'vbox',
                maxHeight: 150,
                scrollable: true,
                style: 'background-color:white;',
                items: [
                   {
                       xtype: 'displayfield',
                       id: 'backendLongtextDialogTextArea',
                       labelStyle: 'display: none',
                       maxWidth: 376
                   }
                ]
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewBackEndLongtextTextArea of BaseLongtextDialog', ex);
        }

        return retval;
    },

    //private
    getNewLocalLongtextTextArea: function () {
        var retval = null;

        try {
            var myController = this.getController();
            var userAgentInfo = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();
            var isSafari = userAgentInfo.OS === 'iOS';

            retval = Ext.create('Ext.form.field.TextArea', {
                id: 'localLongtextDialogTextArea',
                growMin: 120,
                grow: true,
                growMax: isSafari ? 1000 : 200,
                labelStyle: 'display: none;',
                flex: 1
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewLocalLongtextTextArea of BaseLongtextDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('longtext');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseLongtextDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            this.renewTextAreas();

            var textFieldAreaBackend = Ext.getCmp('backendLongtextDialogTextArea');
            var textFieldAreaLocal = Ext.getCmp('localLongtextDialogTextArea');
            
            var myModel = this.getViewModel();
            var backendLongtext = this.formatBackendLongtext(myModel.get('backendLongtext'));
            var localLongText = myModel.get('localLongtext');

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(backendLongtext)) {
                Ext.getCmp('backendLongtextContainer').show();
                textFieldAreaBackend.setValue(backendLongtext);
            } else {
                Ext.getCmp('backendLongtextContainer').setHidden(true);
            }

            var isReadOnly = myModel.get('readOnly');

            if(isReadOnly){
                textFieldAreaLocal.hide();
                this.hideSaveButton();
            } else {
                this.showSaveButton();
                textFieldAreaLocal.setEditable(true);
                textFieldAreaLocal.setValue(localLongText);
                textFieldAreaLocal.show();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseLongtextDialog', ex);
        }
    },

    //private
    showSaveButton: function() {
        try {
            this._saveButton.show();
            this._saveButton.addCls('first');
            this._cancelButton.removeCls('first');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSaveButton of BaseLongtextDialog', ex);
        }
    },

    //private
    hideSaveButton: function () {
        try {
            this._saveButton.hide();
            this._saveButton.removeCls('first');
            this._cancelButton.addCls('first');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideSaveButton of BaseLongtextDialog', ex);
        }
    },

    //public
    getCurrentLocalLongtextInput: function () {
        var retval = '';

        try {
            retval = Ext.getCmp('localLongtextDialogTextArea').getValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentLocalLongtextInput of BaseLongtextDialog', ex);
        }

        return retval;
    },

    formatBackendLongtext : function(backendLongtext){
        try {
            var formattedBackendLongtext = '';

            if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(backendLongtext)){
                return formattedBackendLongtext;
            }

            formattedBackendLongtext = backendLongtext.replace(/\n/g, '<br>');
            return formattedBackendLongtext;
        } catch(ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside formatBackendLongtext of BaseLongtextDialog', ex);
        }
    }
});
