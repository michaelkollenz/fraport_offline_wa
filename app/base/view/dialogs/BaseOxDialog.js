Ext.define('AssetManagement.base.view.dialogs.BaseOxDialog', {
    extend: 'Ext.window.Window',
    
    config: {
		onEsc: function() { this.onEscape(); },
		resizable: false,
		supportsDynamicWidth: false,
		supportsDynamicHeight: false
    },

    requires: [
       'AssetManagement.customer.utils.OxException'
    ],

    inheritableStatics: {
        resetInstance: function() {
            try {

                if(this._instance) {
                    this._instance.destroy();
                }
                this._instance = null;
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetInstance of BaseOxDialog', ex);
            }

            return this._instance;
        }
    },

    //protected
    getClassName: function () {
        var retval = 'BaseOxDialog';

        try {
            var className = Ext.getClassName(this);

            if (AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
                var splitted = className.split('.');
                retval = splitted[splitted.length - 1];
            } else {
                retval = className;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseOxDialog', ex);
        }

        return retval;
    },

	constructor: function(config) {
        this.callParent(arguments);
        
        try {
            this.initialize();
        } catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxDialog', ex);
    	}    
	},
	
	initialize: function() {
	    try {
		    this.buildUserInterface();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseOxDialog', ex);
    	}    
	},
	
	//protected
	getDialogTitle: function() {
        return '';
    },
	
	//protected
	buildUserInterface: function() {
	},

    //public
    //triggers a refresh of dialog affected UI elements
    //refreshing the dialog content itself is a part of this method
    refreshView: function() {
    	try {
    		Ext.suspendLayouts();
    	
    		this.updateDialogContent();
    		
    		Ext.resumeLayouts(true);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshView of OxPage', ex);
    	}
    },
    
    //protected
    //may be implemented by derivates
    //using this method will ensure all updates will be done in one layout update cycle of the framework
    updateDialogContent: function() {
    },
	
	//protected
    resetViewState: function() {
    	try {
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseOxDialog', ex);
    	}
	},
	
	onEscape: function() {
	    try {
		    this.getController().requestCancellation();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEscape of BaseOxDialog', ex);
    	}    
	},
	
	rebuildDialog: function() {
		try {
			//remove all components and destroy them, to clear memory
			this.removeAll(true);
			
			//build the component again
			this.initialize();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildDialog of BaseOxDialog', ex);
    	}
	}
});