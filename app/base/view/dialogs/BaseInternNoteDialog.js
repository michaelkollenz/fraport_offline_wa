﻿Ext.define('AssetManagement.base.view.dialogs.BaseInternNoteDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.model.dialogmodel.InternNoteDialogViewModel',
        'AssetManagement.customer.controller.dialogs.InternNoteDialogController',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.button.Button',
        'AssetManagement.customer.manager.LongtextManager',
        'AssetManagement.customer.utils.StringUtils'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.InternNoteDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseInternNoteDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'InternNoteDialogViewModel'
    },

    controller: 'InternNoteDialogController',
    width: 400,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    //members
    _cancelButton: null,

    buildUserInterface: function () {
        try {
            this._cancelButton = Ext.create('Ext.button.Button', {
                flex: 1,
                cls: 'oxDialogButton first',
                text: Locale.getMsg('cancel'),
                listeners: {
                    click: 'cancel'
                }
            });

            var items = [
			    {
			        xtype: 'component',
                    width: 10
			    },
		        {
		            xtype: 'container',
		            id: 'mainInterNoteContainer',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'container',
		                    height: 15
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    id: 'internNoteTitle',
		                    margin: '0 0 10 0',
		                    text: Locale.getMsg('internNote')
		                },
		                this.getNewDisplayField(),
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    height: '',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
                                this._cancelButton
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseInternNoteDialog', ex);
        }
    },

    //private
    renewDisplayField: function () {
        try {
            var internNoteContainer = Ext.getCmp('internNoteContainer');

            var mainContainer = Ext.getCmp('mainInterNoteContainer');

            if (internNoteContainer && mainContainer) {
                mainContainer.remove(internNoteContainer);

                var newDisplayField = this.getNewDisplayField();

                if (newDisplayField)
                    mainContainer.insert(2, newDisplayField);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewDisplayField of BaseInternNoteDialog', ex);
        }
    },

    //private
    getNewDisplayField: function () {
        var retval = null;

        try {
            retval = {
                xtype: 'container',
                margin: '0 0 20 0',
                id: 'internNoteContainer',
                layout: 'vbox',
                maxHeight: 150,
                scrollable: true,
                style: 'background-color:white',
                items: [
                   {
                       xtype: 'displayfield',
                       id: 'internNoteDisplayField',
                       labelStyle: 'display: none',
                       maxWidth: 376
                   }
                ]
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewDisplayField of BaseInternNoteDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    getDialogTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('internNote');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseInternNoteDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            var myModel = this.getViewModel();

            this.renewDisplayField();

            var displayField = Ext.getCmp('internNoteDisplayField');
            
            var internNote = this.formatInternNote(myModel.get('internNote'));
            displayField.setValue(internNote);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseInternNoteDialog', ex);
        }
    },

    formatInternNote: function (InternNote) {
        try {
            var formattedInternNote = '';

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(InternNote)) {
                return formattedInternNote;
            }

            formattedInternNote = InternNote.replace(/\n/g, '<br>');
            return formattedInternNote;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside formatInternNote of BaseInternNoteDialog', ex);
        }
    }
});
