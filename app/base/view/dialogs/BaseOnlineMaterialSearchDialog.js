﻿Ext.define('AssetManagement.base.view.dialogs.BaseOnlineMaterialSearchDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OnlineSearchDialog',

    requires: [
        'AssetManagement.customer.model.dialogmodel.OnlineMaterialSearchDialogViewModel',
        'AssetManagement.customer.controller.dialogs.OnlineMaterialSearchDialogController'
    ],

    viewModel: {
        type: 'OnlineMaterialSearchDialogModel'
    },

    controller: 'OnlineMaterialSearchDialogController'
});