Ext.define('AssetManagement.base.view.dialogs.BaseCancelableProgressDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',

    
    requires: [
       'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',
       'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',
       'AssetManagement.customer.view.utils.ProgressBar',
       'Ext.container.Container',
       'Ext.form.Label',
       'Ext.button.Button',
       'AssetManagement.customer.helper.OxLogger'
    ],
    
    //public
    config: {
        supportsDynamicWidth: true,
        minWidth: 400,
        width: 400,
        maxWidth: 500,
        modal: true,
        header: false,
        centered: true,
        floating: true,
        
        layout: {
            type: 'vbox', 
            align: 'stretch'
        },
    
        cls: 'oxDialog'
    },
    
    viewModel: {
        type: 'CancelableProgressDialogModel'
    },
    
    controller: 'CancelableProgressDialogController',
    
    //private
    _titleBar: null,
    _progressBar: null,
    _messageLine: null,
    _cancelButton: null,
    
    constructor: function(config) {
        try {
            this.callParent(arguments);
        
            this.setInitialWidth(config ? config.width : undefined);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCancelableProgressDialog', ex);
        }    
    },
    
    //protected
    setInitialWidth: function(widthToSet) {
        try {
            if(!widthToSet)
                widthToSet = window.innerWidth * 0.4;
                
            var minWidth = this._minWidth;
            var maxWidth = this._maxWidth;
            
            if(widthToSet > maxWidth) {
                widthToSet = maxWidth;
            } else if(widthToSet < minWidth) {
                widthToSet = minWidth;
            }
            
            this.setWidth(widthToSet);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitialWidth of BaseCancelableProgressDialog', ex);
        }
    },
    
    buildUserInterface: function() {
        try {
            var myController = this.getController();

            var buttonBar = null;
    
            if(this.getController().getCancelable() === true) {
                this._cancelButton = Ext.create('Ext.button.Button', {
                    flex: 1,
                    height: '36px',  
                    cls: 'oxDialogButton first',
                    text: Locale.getMsg('cancel'),
                    handler: this.getController().cancelButtonClicked,
                    scope: this.getController()
                });
                
                buttonBar = Ext.create('Ext.container.Container', {
                    layout: {
                        type: 'hbox'
                    },
                    cls: 'oxDialogButtonBar',
                    style: 'margin-top: 10px'
                });
            
                buttonBar.add(this._cancelButton);
            }
                
            this._titleBar = Ext.create('Ext.form.Label', { cls: 'oxDialogTitle' });
            this._messageLine = Ext.create('Ext.form.Label', { cls: 'oxDialogMessage' });

            this._progressBar = Ext.create('AssetManagement.customer.view.utils.ProgressBar', { parent: this, marginLeftRight: 5, infinite: myController.getInfinite() });
    
            this.add(this._titleBar);
            this.add(this._messageLine);
            this.add(this._progressBar);
            
            if (buttonBar) {
                this.add(buttonBar);
            } else {
                this.add(Ext.create('Ext.Component', {
                    height: 20
                }));
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseCancelableProgressDialog', ex);
        }
    },
    
    //protected
    //@override
    updateDialogContent: function() {
        try {
            var myModel = this.getViewModel();
            
            this._titleBar.setHtml(myModel.get('title'));

            if (!myModel.get('infinite'))
                this._progressBar.setProgress(myModel.get('percentage'));

            this._messageLine.setHtml(myModel.get('message'));
            
            if(this._cancelButton)
                this._cancelButton.setDisabled(!myModel.get('currentlyCancelable'));
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseCancelableProgressDialog', ex);
        }
    },

    //public
    startProgressBar: function () {
        try {
            this._progressBar.startProgressBar();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startProgressBar of BaseCancelableProgressDialog', ex);
        }
    },

    //public
    stopProgressBar: function () {
        try {
            this._progressBar.stopProgressBar();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stopProgressBar of BaseCancelableProgressDialog', ex);
        }
    }
});