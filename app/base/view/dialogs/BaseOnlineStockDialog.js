Ext.define('AssetManagement.base.view.dialogs.BaseOnlineStockDialog', {
    extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',
    alias: 'widget.baseonlinestockdialog',

    requires: [
        'AssetManagement.customer.model.dialogmodel.OnlineStockDialogViewModel',
        'AssetManagement.customer.controller.dialogs.OnlineStockDialogController',
        'AssetManagement.customer.view.utils.ProgressBar',
        'Ext.container.Container'
    ],

  inheritableStatics: {
        _instance: null,

        getInstance: function() {
            try {
                if(this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.OnlineStockDialog');
                }
            } catch(ex) {
                AssetManagement.helper.OxLogger.logException('Exception occurred inside getInstance of BaseOnlineStockDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'OnlineStockDialogViewModel'
    },

    controller: 'OnlineStockDialogController',
    width: 440,
    header: false,
    modal: true,

    layout: {
        type: 'vbox'
    },

    _progressBar: null,
    _titleBar: null,
    _cancelButton: null,
    _searchButton: null,
    _messageLine : null,

    buildUserInterface: function() {
        try {
            this.callParent();

        } catch(ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseOnlineStockDialog', ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function() {
        try {
            this._titleBar.setHtml(Locale.getMsg('contactingSapSystem'));
            // this._titleBar.setHtml(Locale.getMsg('creatingOrder'));
            this._messageLine.setHtml(Locale.getMsg('communicatingWithSAP'));
        } catch(ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseOnlineStockDialog', ex);
        }
    }
});