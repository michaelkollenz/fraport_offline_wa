Ext.define('AssetManagement.base.view.dialogs.BaseNotifItemDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.NotifItemDialogController',
        'AssetManagement.customer.model.dialogmodel.NotifItemDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.model.bo.NotifItem'
    ],
    
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.NotifItemDialog');
                }            
            } catch(ex) {
    		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifItemDialog', ex);
    	    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'NotifItemDialogModel'
    },
    
    controller: 'NotifItemDialogController',
    width: 500,
    floating: true,
    modal: true,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
        try {
            var items = [
                {
                    xtype: 'component',
                    width: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            cls: 'oxHeaderLabel',
                            height: 25,
                            id: 'notifItemDialogHeader'
                            //text: Locale.getMsg('createNotifItem')
                        },
                        {
                            xtype: 'component',
                            height: 15
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'oxcombobox',
                                    flex: 2,
                                    id: 'notifItemDialogDamageGroupCb',
                                    maxWidth: 290,
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value',
                                    listeners: {
                                        select: 'onDamageGrpSelected'
                                    },
                                    fieldLabel: Locale.getMsg('checklist_Damage')
                                },
                                {
                                    xtype: 'component',
                                    width: 5
                                },
                                {
                                    xtype: 'oxcombobox',
                                    flex: 1,
                                    id: 'notifItemDialogDamageCodeCb',
                                    labelStyle: 'display: none;',
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value'
                                }
                            ]
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'oxcombobox',
                                    flex: 2,
                                    id: 'notifItemDialogObjGrpCb',
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value',
                                    maxWidth: 290,
                                    listeners: {
                                        select: 'onObjGrpSelected'
                                    },
                                    fieldLabel: Locale.getMsg('objectPart')
                                },
                                {
                                    xtype: 'component',
                                    width: 5
                                },
                                {
                                    xtype: 'oxcombobox',
                                    flex: 1,
                                    id: 'notifItemDialogObjCodeCb',
                                    labelStyle: 'display: none;',
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value'
                                }
                            ]
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'oxtextfield',
                            id: 'notifItemDialogDescrTextField',
                            fieldLabel: Locale.getMsg('specification'),
                            maxLength: 40
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'onSaveButtonClicked'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'onCancelButtonClicked'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    width: 10
                }
            ];

            this.add(items);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    //@override
    getDialogTitle: function() {
        var retval = '';

        try {
            var notifItem = this.getViewModel().get('notifItem');

            retval = notifItem ? Locale.getMsg('editNotifItem') : Locale.getMsg('createNotifItem');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    manageDialogTitle: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('notifItemDialogHeader').setText(title);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

        }
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {

            this.manageDialogTitle();

            this.manageDamageCodeComboBoxes();

            this.manageObjectCodeComboBoxes();

            this.manageShorttextInputField();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //fills damage combo boxes and preselects the value of the notif item
    manageDamageCodeComboBoxes: function () {
        try {
            this.fillDamageCodeGroupComboBox();

            this.setSelectionOfDamageCodeGroup();

            this.fillDamageCodeComboBox();

            this.setSelectionOfDamageCode();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //fills object part combo boxes and preselects the value of the notif item
    manageObjectCodeComboBoxes: function () {
        try {
            this.fillObjectCodeGroupComboBox();

            this.setSelectionOfObjectCodeGroup();

            this.fillObjectCodeComboBox();

            this.setSelectionOfObjectCode();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //clears the input field value or fills it with the current notif item short text
    manageShorttextInputField: function () {
        try {
            var myModel = this.getViewModel();
            var notifItem = myModel.get('notifItem');
            var shorttext = notifItem ? notifItem.get('fetxt') : '';

            Ext.getCmp('notifItemDialogDescrTextField').setValue(shorttext);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    fillDamageCodeGroupComboBox: function () {
        try {
            var myModel = this.getViewModel();
            var damageCodeGroupsMap = myModel.get('damageCodeGroupsMap');

            //fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (damageCodeGroupsMap) {
                damageCodeGroupsMap.each(function (codeGroupKey, groupsCodes) {
                    if (groupsCodes.getCount() > 0) {
                        store.add({ "text": codeGroupKey + ' ' + groupsCodes.getAt(0).get('codegrkurztext'), "value": groupsCodes });
                    }
                }, this);
            }

            var damageGroupCombobox = Ext.getCmp('notifItemDialogDamageGroupCb');
            damageGroupCombobox.clearValue();
            damageGroupCombobox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    setSelectionOfDamageCodeGroup: function () {
        try {
            var myModel = this.getViewModel();
            var damageCodeGroupsMap = myModel.get('damageCodeGroupsMap');

            if (damageCodeGroupsMap) {
                var notifItem = myModel.get('notifItem');
                var itemsGroupKey = notifItem ? notifItem.get('fegrp') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsGroupKey)) {
                    var groupToSelect = damageCodeGroupsMap.get(itemsGroupKey);

                    if (groupToSelect) {
                        var comboBox = Ext.getCmp('notifItemDialogDamageGroupCb');

                        var recordToSelect = comboBox.findRecordByValue(groupToSelect);

                        if (recordToSelect)
                            comboBox.select(recordToSelect);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //fill damage group dropdown using the current selected codes store from the group combo box
    fillDamageCodeComboBox: function () {
        try {
            //fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            var currentSelectedCodeGroup = Ext.getCmp('notifItemDialogDamageGroupCb').getValue();

            if (currentSelectedCodeGroup && currentSelectedCodeGroup.getCount() > 0) {
                currentSelectedCodeGroup.each(function (code) {
                    store.add({ "text": code.get('code') + ' ' + code.get('kurztext'), "value": code });
                });
            }

            var damageCodeCombobox = Ext.getCmp('notifItemDialogDamageCodeCb');
            damageCodeCombobox.clearValue();
            damageCodeCombobox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    setSelectionOfDamageCode: function () {
        try {
            var myModel = this.getViewModel();
            var damageCodeGroupsMap = myModel.get('damageCodeGroupsMap');

            if (damageCodeGroupsMap) {
                var notifItem = myModel.get('notifItem');
                var itemsCodeKey = notifItem ? notifItem.get('fecod') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsCodeKey)) {
                    var itemsGroupKey = notifItem.get('fegrp');
                    var groupsCodes = damageCodeGroupsMap.get(itemsGroupKey);

                    if (groupsCodes && groupsCodes.getCount() > 0) {
                        var codeToSelect = null;

                        groupsCodes.each(function (code) {
                            if (code.get('code') === itemsCodeKey) {
                                codeToSelect = code;
                                return false;
                            }
                        });

                        if (codeToSelect) {
                            var comboBox = Ext.getCmp('notifItemDialogDamageCodeCb');

                            var recordToSelect = comboBox.findRecordByValue(codeToSelect);

                            if (recordToSelect)
                                comboBox.select(recordToSelect);
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    fillObjectCodeGroupComboBox: function () {
        try {
            var myModel = this.getViewModel();
            var objectCodeGroupsMap = myModel.get('objectCodeGroupsMap');

            //fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (objectCodeGroupsMap) {
                objectCodeGroupsMap.each(function (codeGroupKey, groupsCodes) {
                    if (groupsCodes.getCount() > 0) {
                        store.add({ "text": codeGroupKey + ' ' + groupsCodes.getAt(0).get('codegrkurztext'), "value": groupsCodes });
                    }
                }, this);
            }

            var objectGroupCombobox = Ext.getCmp('notifItemDialogObjGrpCb');
            objectGroupCombobox.clearValue();
            objectGroupCombobox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    setSelectionOfObjectCodeGroup: function () {
        try {
            var myModel = this.getViewModel();
            var objectCodeGroupsMap = myModel.get('objectCodeGroupsMap');

            if (objectCodeGroupsMap) {
                var notifItem = myModel.get('notifItem');
                var itemsGroupKey = notifItem ? notifItem.get('otgrp') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsGroupKey)) {
                    var groupToSelect = objectCodeGroupsMap.get(itemsGroupKey);

                    if (groupToSelect) {
                        var comboBox = Ext.getCmp('notifItemDialogObjGrpCb');

                        var recordToSelect = comboBox.findRecordByValue(groupToSelect);

                        if (recordToSelect)
                            comboBox.select(recordToSelect);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //fill object group dropdown using the current selected codes store from the group combo box
    fillObjectCodeComboBox: function () {
        try {
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            var currentSelectedCodeGroup = Ext.getCmp('notifItemDialogObjGrpCb').getValue();

            if (currentSelectedCodeGroup && currentSelectedCodeGroup.getCount() > 0) {
                currentSelectedCodeGroup.each(function (code) {
                    store.add({ "text": code.get('code') + ' ' + code.get('kurztext'), "value": code });
                });
            }

            var objectCodeCombobox = Ext.getCmp('notifItemDialogObjCodeCb');
            objectCodeCombobox.clearValue();
            objectCodeCombobox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    setSelectionOfObjectCode: function () {
        try {
            var myModel = this.getViewModel();
            var objectCodeGroupsMap = myModel.get('objectCodeGroupsMap');

            if (objectCodeGroupsMap) {
                var notifItem = myModel.get('notifItem');
                var itemsCodeKey = notifItem ? notifItem.get('oteil') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsCodeKey)) {
                    var itemsGroupKey = notifItem.get('otgrp');
                    var groupsCodes = objectCodeGroupsMap.get(itemsGroupKey);

                    if (groupsCodes && groupsCodes.getCount() > 0) {
                        var codeToSelect = null;

                        groupsCodes.each(function (code) {
                            if (code.get('code') === itemsCodeKey) {
                                codeToSelect = code;
                                return false;
                            }
                        });

                        if (codeToSelect) {
                            var comboBox = Ext.getCmp('notifItemDialogObjCodeCb');

                            var recordToSelect = comboBox.findRecordByValue(codeToSelect);

                            if (recordToSelect)
                                comboBox.select(recordToSelect);
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    getCurrentInputValues: function() {
        var retval = null;

        try {
            var shorttext = Ext.getCmp('notifItemDialogDescrTextField').getValue();
            var damageCode = Ext.getCmp('notifItemDialogDamageCodeCb').getValue();
            var objectCode = Ext.getCmp('notifItemDialogObjCodeCb').getValue();

            retval = {
                shorttext: shorttext,
                damageCode: damageCode,
                objectCode: objectCode
            };
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name,this.getClassName(), ex);
        }

        return retval;
    }
});