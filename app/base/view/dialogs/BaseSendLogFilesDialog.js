Ext.define('AssetManagement.base.view.dialogs.BaseSendLogFilesDialog', {
	extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.SendLogFilesDialogController',
        'AssetManagement.customer.model.dialogmodel.SendLogFilesDialogViewModel',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.utils.DateTimePicker'
    ],
    
    viewModel: {
        type: 'SendLogFilesDialogModel'
    },
    
    controller: 'SendLogFilesDialogController',

    _titleBar: null,
    _progressBar: null,
    _messageLine: null,
    _selectionContainer: null,
    _progressContainer: null,
    _cancelButton: null,
    _sendButton: null,

    config: {
        supportsDynamicWidth: true,
        minWidth: 400,
        maxWidth: 500
    },

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function(arguments) {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.SendLogFilesDialog', arguments);
                }
            
                this._instance.setInitialWidth(arguments ? arguments.width : undefined);          
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSendLogFilesDialog', ex);
	    	}
            
            return this._instance;
        }
    },

    //@override
    //protected
    buildUserInterface: function () {
        try {
            var myController = this.getController();

            var mainContainer = Ext.create('Ext.container.Container', {
                xtype: 'container',
                id: 'sendLogFilesDialogContainer',
                flex: 1,                
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            });

            this.add(mainContainer);

            this._titleBar = Ext.create('Ext.form.Label', { cls: 'oxDialogTitle' });

            this._selectionContainer = Ext.create('Ext.container.Container', {
                xtype: 'container',
                margin: '0 10 0 10',
                id: 'sendLogFilesSelectionContainer',
                autoScroll: true,
                maxHeight: 300,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            });

            this.buildSelectionContainer();

            this._progressContainer = Ext.create('Ext.container.Container', {
                xtype: 'container',
                id: 'sendLogFilesProgressContent',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            });

            this.buildProgressContainer();

            var buttonBar = buttonBar = Ext.create('Ext.container.Container', {
                layout: {
                    type: 'hbox'
                },
                cls: 'oxDialogButtonBar',
                style: 'margin-top: 10px'
            });

            this._cancelButton = Ext.create('Ext.button.Button', {
                flex: 1,
                height: '32px',
                cls: 'oxDialogButton',
                text: Locale.getMsg('cancel'),
                handler: myController.cancelButtonClicked,
                scope: myController
            });

            this._sendButton = Ext.create('Ext.button.Button', {
                flex: 1,
                height: '32px',
                cls: 'oxDialogButton first',
                text: Locale.getMsg('send'),
                handler: myController.sendButtonClicked,
                scope: myController
            });            

            buttonBar.add(this._sendButton);
            buttonBar.add(this._cancelButton);

            mainContainer.add(this._titleBar);
            mainContainer.add(this._selectionContainer);
            mainContainer.add(this._progressContainer);

            mainContainer.add(buttonBar);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSendLogFilesDialog', ex);
        }
    },

    //private
    buildSelectionContainer: function () {
        try {
            var dateField = Ext.create('Ext.form.field.Date', {
		        format: AssetManagement.customer.utils.DateTimeUtils.getDateDisplayFormat(),
		        id: 'sendLogFilesErrorDatePicker',
			    editable: false,
		        height: 30,
                fieldLabel: Locale.getMsg('sendLogFilesErrorDate'),
                labelWidth: 120
            });

            var timeField = Ext.create('Ext.form.field.Time', {
                format: AssetManagement.customer.utils.DateTimeUtils.getTimeDisplayFormat(),
				id: 'sendLogFilesErrorTimePicker',
	            editable: false,
		        height: 30,
                fieldLabel: Locale.getMsg('sendLogFilesErrorTime'),
                labelWidth: 120
            });

            var userRemarks = Ext.create('Ext.form.field.TextArea', {
                id: 'sendLogFilesUserRemarksTextField',
                //labelStyle: 'padding-top: 7px;',
                padding: '9 0 0 0',
                growMin: 100,
                grow: true,
                name: 'message',
                fieldLabel: Locale.getMsg('sendLogFilesUserRemarks'),
                labelWidth: 120
            });

            var checkboxSection = Ext.create('Ext.container.Container', {
                margin: '15 0 10 0',
                id: 'sendLogFilesCheckBoxContainer',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'label',
                        flex: 5,
                        margin: '6 0 0 10',
                        text: Locale.getMsg('sendLogFilesIncDbLabel')
                    },
                    //spacer
                    {
                        xtype: 'container',
                        flex: 4
                    },
                    {
                        xtype: 'checkboxfield',
                        flex: 1,
                        id: 'sendLogFilesDialogIncludeDatabaseCheckBox',
                        baseCls: 'oxCheckBox',
                        checkedCls: 'checked',
                        checked: false,
                        height: 36,
                        listeners: {
                            'change': 'includeDatabaseCheckBoxChanged'
                        }
                    }                
                ]
            });

            this._selectionContainer.add(dateField);
            this._selectionContainer.add(timeField);
            this._selectionContainer.add(userRemarks);
            this._selectionContainer.add(checkboxSection);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSelectionContainer of BaseSendLogFilesDialog', ex);
        }
    },

    //private
    buildProgressContainer: function () {
        try {
            var myController = this.getController();

            this._messageLine = Ext.create('Ext.form.Label', { cls: 'oxDialogMessage' });
            this._progressBar = Ext.create('AssetManagement.customer.view.utils.ProgressBar', { parent: this, marginLeftRight: 5, infinite: myController.getInfinite() });
            this._progressContainer.add(this._messageLine);
            this._progressContainer.add(this._progressBar);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildProgressContainer of BaseSendLogFilesDialog', ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            //parent will update handle usual progress dialog contents
            this.callParent();
            var myController = this.getController();
            var myModel = this.getViewModel();

            var mode = myModel.get('mode');

            switch (mode) {
                case myController.DIALOG_MODES.SELECTION:
                    this._sendButton.setVisible(true);
                    this._cancelButton.setVisible(true);
                    this._cancelButton.removeCls('first');

                    Ext.getCmp('sendLogFilesCheckBoxContainer').setDisabled(false);
                    Ext.getCmp('sendLogFilesUserRemarksTextField').setDisabled(false);
                    Ext.getCmp('sendLogFilesErrorDatePicker').setDisabled(false);
                    Ext.getCmp('sendLogFilesErrorTimePicker').setDisabled(false);

                    this._progressContainer.setHidden(true);
                    this.populateFields();
                    break;

                case myController.DIALOG_MODES.SENDING:
                    this._cancelButton.addCls('first');
                    this._sendButton.setVisible(false);
                    this._cancelButton.setVisible(true);

                    Ext.getCmp('sendLogFilesCheckBoxContainer').setDisabled(true);
                    Ext.getCmp('sendLogFilesUserRemarksTextField').setDisabled(true);
                    Ext.getCmp('sendLogFilesErrorDatePicker').setDisabled(true);
                    Ext.getCmp('sendLogFilesErrorTimePicker').setDisabled(true);

                    this._progressContainer.setHidden(false);
                    break;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseSendLogFilesDialog', ex);
        }
    },

    //private
    //populates field with default values
    populateFields: function () {
        try {
            var myModel = this.getViewModel();
            var errorDateTime = myModel.get('errorDateTime');

            Ext.getCmp('sendLogFilesErrorDatePicker').setValue(errorDateTime);
            Ext.getCmp('sendLogFilesErrorTimePicker').setValue(errorDateTime);

            Ext.getCmp('sendLogFilesUserRemarksTextField').setValue(myModel.get('errorRemarks'));
            Ext.getCmp('sendLogFilesDialogIncludeDatabaseCheckBox').setValue(myModel.get('includeDatabase'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside populateFields of BaseSendLogFilesDialog', ex);
        }
    },

    //public
    transferViewValuesIntoModel: function () {
        try {
            var errorDate = Ext.getCmp('sendLogFilesErrorDatePicker').getValue();
            var errorTime = Ext.getCmp('sendLogFilesErrorTimePicker').getValue();

            var errorHours = errorTime.getHours();
            var errorMinutes = errorTime.getMinutes();
            //var errorTimeMs = errorTime ? errorTime.getTime() : new Date().getTime();

            var errorDateTime = AssetManagement.customer.utils.DateTimeUtils.getCurrentDate();

            errorDateTime.setDate(errorDate.getDate());
            errorDateTime.setMonth(errorDate.getMonth());
            errorDateTime.setFullYear(errorDate.getFullYear());
            errorDateTime.setHours(errorHours);
            errorDateTime.setMinutes(errorMinutes);

            var errorRemarks = Ext.getCmp('sendLogFilesUserRemarksTextField').getValue();
            var includeDatabase = Ext.getCmp('sendLogFilesDialogIncludeDatabaseCheckBox').getValue();

            var myModel = this.getViewModel();
            myModel.set('errorDateTime', errorDateTime);
            myModel.set('errorRemarks', errorRemarks);
            myModel.set('includeDatabase', includeDatabase);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewValuesIntoModel of BaseSendLogFilesDialog', ex);
        }
    }
});
