Ext.define('AssetManagement.base.view.dialogs.BaseOnlinePoGetDialog', {
    extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',

    requires: [
        'AssetManagement.customer.model.dialogmodel.OnlinePoGetDialogViewModel',
        'AssetManagement.customer.controller.dialogs.OnlinePoGetDialogController',
        'AssetManagement.customer.view.utils.ProgressBar',
        'Ext.container.Container'
    ],

  inheritableStatics: {
        _instance: null,

        getInstance: function() {
            try {
                if(this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.OnlinePoGetDialog');
                }
            } catch(ex) {
                AssetManagement.helper.OxLogger.logException('Exception occurred inside getInstance of BaseOnlinePoGetDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'OnlinePoGetDialogModel'
    },

    controller: 'OnlinePoGetDialogController',
    width: 440,
    header: false,
    modal: true,

    layout: {
        type: 'vbox'
    },

    _progressBar: null,
    _titleBar: null,
    _cancelButton: null,
    _searchButton: null,
    _messageLine : null,

    buildUserInterface: function() {
        try {
            this.callParent();

        } catch(ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseOnlinePoGetDialog', ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function() {
        try {
            this._titleBar.setHtml(Locale.getMsg('getPoSAP'));
            // this._titleBar.setHtml(Locale.getMsg('creatingOrder'));
            this._messageLine.setHtml(Locale.getMsg('communicatingWithSAP'));
        } catch(ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseOnlinePoGetDialog', ex);
        }
    }
});