Ext.define('AssetManagement.base.view.dialogs.BaseObjectListEditDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.ObjectListEditDialogController',
        'AssetManagement.customer.model.dialogmodel.ObjectListEditDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.ObjectListEditDialog');
                }            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseObjectListEditDialog', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'ObjectListEditDialogViewModel'
    },
    
    controller: 'ObjectListEditDialogController',
    width: 500,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
   buildUserInterface: function() {
    	try {
	    	var items = [
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    height: 25,
		                    text: Locale.getMsg('changeObject')
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
                        {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
                                {
		                            xtype: 'oxtextfield',
		                            id: 'objectListEditDialogEqunrTextField',
		                            fieldLabel: Locale.getMsg('equipment'),                      
		                            editable: false,
		                            maxLength: 18
		                        },
								{
									xtype: 'component',
									width: 10
								},
								{
									xtype: 'container',
									top: 20, 
									items: [
										{
											xtype: 'button',
											bind: {
											    html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'                                                               
											},
											padding: '3 0 0 10',
											listeners: {
											    click: 'onSelectEquiClick'
											}
											                
										}
									]
								}
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
                        {
		                    xtype: 'oxtextfield',
		                    id: 'objectListEditDialogSernrTextField',
		                    fieldLabel: Locale.getMsg('serialNumber'),                      
		                    editable: false,
		                    maxLength: 18
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'oxtextfield',
		                    id: 'objectListEditDialogMatnrTextField',
		                    fieldLabel: Locale.getMsg('materialNumber'),                            
		                    editable: false,
		                    maxLength: 18
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('save'),
		                            listeners: {
		                            	click: 'save'
		                            }
		                        
		                        },
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton',
		                            text: Locale.getMsg('cancel'),
		                            listeners: {
			                    		click: 'cancel'
			                    	}
		                        }
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
		    ];
    
	    	this.add(items);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseObjectListEditDialog', ex);
		}
	},
	
	//protected
	//@override
	getDialogTitle: function() {
	    var retval = '';	    
	    try {
	        retval = Locale.getMsg('changeObject');
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseObjectListEditDialog', ex);
		}
	    
		return retval;
	},
	
	//public
	//@override
	resetViewState: function() {
	    try {
            this.resetFields();
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseObjectListEditDialog', ex);
		}
	},
  
	//protected
	//@override
    updateDialogContent: function() {
		try {
			this.resetFields(); 
			this.fillTextfieldsWithData(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseObjectListEditDialog', ex);
		}
	},
	
	resetFields: function(){
		try {
            Ext.getCmp('objectListEditDialogEqunrTextField').setValue('');
            Ext.getCmp('objectListEditDialogSernrTextField').setValue('');
            Ext.getCmp('objectListEditDialogMatnrTextField').setValue('');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseObjectListEditDialog', ex);
		}
	},
	
	// load the data for the serialnumber and the materialnumber into the textfields
    fillTextfieldsWithData: function() {
        try {
            Ext.getCmp('objectListEditDialogEqunrTextField').setValue(this.getViewModel().get('objectListItem').get('equnr'));	 
            Ext.getCmp('objectListEditDialogSernrTextField').setValue(this.getViewModel().get('objectListItem').get('sernr'));	    	
            Ext.getCmp('objectListEditDialogMatnrTextField').setValue(this.getViewModel().get('objectListItem').get('matnr'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillTextfieldsWithData of BaseObjectListEditDialog', ex);
		}
    },

    //set the data entered on the dialog into the fields of the notification and the object list item
    getObjectListItemValues: function() {
        try {
            if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('notif'))) {
                this.getViewModel().get('notif').set('equnr', Ext.getCmp('objectListEditDialogEqunrTextField').getValue());
                this.getViewModel().get('notif').set('sernr', Ext.getCmp('objectListEditDialogSernrTextField').getValue());
                this.getViewModel().get('notif').set('matnr', Ext.getCmp('objectListEditDialogMatnrTextField').getValue());
            }
            this.getViewModel().get('objectListItem').set('equnr', Ext.getCmp('objectListEditDialogEqunrTextField').getValue());
            this.getViewModel().get('objectListItem').set('sernr', Ext.getCmp('objectListEditDialogSernrTextField').getValue());
            this.getViewModel().get('objectListItem').set('matnr', Ext.getCmp('objectListEditDialogMatnrTextField').getValue());
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectListItemValues of BaseObjectListEditDialog', ex);
        }
    }

});