﻿Ext.define('AssetManagement.base.view.dialogs.BaseObjectStatusDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.ObjectStatusDialogController',
        'AssetManagement.customer.model.dialogmodel.ObjectStatusDialogViewModel',
        'Ext.form.field.Text',
        'Ext.button.Button',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.View'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.ObjectStatusDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseObjectStatusDialog', ex);
            }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.ObjectStatusDialogRendererMixin'
    },

    viewModel: {
        type: 'ObjectStatusDialogModel'
    },

    controller: 'ObjectStatusDialogController',
    height: 500,
    width: 600,
    header: false,
    modal: true,
    resizable: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var myController = this.getController();
            var me = this;
            var items = [
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                 {
		                     xtype: 'component',
		                     height: 20
		                 },
		                {
		                    xtype: 'label',
		                    id: 'objectStatusHeaderLabel',
		                    cls: 'oxHeaderLabel',
		                    height: 25
		                    //text: Locale.getMsg('objectStatus')
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
                      {
                          xtype: 'container',
                          id: 'objectStatusWithOrdNoSection',
                          flex: 1,
                          layout: {
                              type: 'vbox',
                              align: 'stretch'
                          },
                          items: [
                              {
                                  xtype: 'label',
                                  cls: 'oxHeaderLabel',
                                  text: Locale.getMsg('objectStatusWithOrdNo')
                              },
                              {
                                  xtype: 'oxdynamicgridpanel',
                                  id: 'objectStatusWithOrdNoGridPanel',
                                  scrollable: 'vertical',
                                  flex: 1,
                                  //title: Locale.getMsg('equipments'),
                                  parentController: myController,
                                  hideContextMenuColumn: true,
                                  //flex: 1,
                                  //listeners: {
                                  //    cellclick: myController.onEquiCellSelected,
                                  //    scope: myController
                                  //},
                                  //needed in order to get the mixins to the grid, and additional items for dynamic generated columns.
                                  owner: me
                              }
                          ]
                      },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
                        {
                            xtype: 'container',
                            id: 'objectStatusWithoutOrdNoSection',
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    cls: 'oxHeaderLabel',
                                    text: Locale.getMsg('objectStatusWithoutOrdNo')
                                },
                                {
                                    xtype: 'oxdynamicgridpanel',
                                    id: 'objectStatusWithoutOrdNoGridPanel',
                                    scrollable: 'vertical',
                                    flex: 1,
                                    hideContextMenuColumn: true,
                                    parentController: myController,
                                    /*columns: [
                                       {
                                           xtype: 'checkcolumn',
                                           dataIndex: 'checked',
                                           tdCls: 'oxCheckBoxGridColumnItem'
                                       },
                                       {
                                           xtype: 'gridcolumn',
                                           renderer: me.mixins.rendererMixin.objectStatusDialogStatusColumnRenderer()

                                       }
                                    ],*/
                                    owner: me
                                }
                            ]
                        },
		                {
		                    xtype: 'container',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('save'),
		                            listeners: {
		                                click: 'save'
		                            }

		                        },
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton',
		                            text: Locale.getMsg('cancel'),
		                            listeners: {
		                                click: 'cancel'
		                            }

		                        }
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseObjectStatusDialog', ex);
        }
    },

    ////protected
    ////@override
    //getPageTitle: function() {
    //    var retval = '';

    //    try {
    //        retval = Locale.getMsg('equipments');
    //    } catch(ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseObjectStatusDialog', ex);
    //	}

    //	return retval;
    //},

    ////protected
    ////@override
    updateDialogContent: function () {
        try {
            //get data from modelview
            var myModel = this.getViewModel();

            var displayedlistWithOrdNr = myModel.get('displayedlistWithOrdNr');
            var displayedlistWithoutOrdNr = myModel.get('displayedlistWithoutOrdNr');

            var objectStatusWithOrdNoGridPanel = Ext.getCmp('objectStatusWithOrdNoGridPanel');
            var objectStatusWithoutOrdNoGridPanel = Ext.getCmp('objectStatusWithoutOrdNoGridPanel');

            var objectStatusHeaderLabel = Ext.getCmp('objectStatusHeaderLabel');
            var object = myModel.get('object');
            var objectClassName = Ext.getClassName(object);
            var objectName = objectClassName.substring(objectClassName.lastIndexOf('.') + 1);
            var message = Locale.getMsg('objectStatusFor') + " " + Locale.getMsg(objectName.toLowerCase());
            objectStatusHeaderLabel.setText(message);

            objectStatusWithOrdNoGridPanel.reconfigure(displayedlistWithOrdNr);
            objectStatusWithoutOrdNoGridPanel.reconfigure(displayedlistWithoutOrdNr);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseObjectStatusDialog', ex);
        }
    }
});