﻿Ext.define('AssetManagement.base.view.dialogs.BaseMultiValueCheckboxDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [

        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.dialogs.MultiValueCheckboxDialogController',
        'AssetManagement.customer.model.dialogmodel.MultiValueCheckboxDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.CharactValueCheckboxItem',
        'AssetManagement.customer.view.CharactValueSingleCheckbox',
        'AssetManagement.customer.manager.ClassificationManager'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.MultiValueCheckboxDialog');
                }

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMultiValueCheckboxDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'MultiValueCheckboxDialogModel'
    },

    controller: 'MultiValueCheckboxDialogController',
    width: 500,
    floating: true,
    modal: true,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var items = [
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            id: 'classEditDialogTitle',
                            cls: 'oxHeaderLabel',
                            height: 25
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            id: 'itemContainer',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            xtype: 'component',
                            minHeight: 5
                        },

                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'save'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'cancel'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                }
            ];

            this.add(items);
            this.updateDialogContent();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMultiValueCheckboxDialog', ex);
        }
    },

    getDialogTitle: function () {
        var retval = '';

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('charact')))
                retval = Locale.getMsg('classificationsFor') + " " + this.getViewModel().get('charact').get('atbez');
            else
                retval = Locale.getMsg('classifications');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseMultiValueCheckboxDialog', ex);
        }

        return retval;
    },

    updateDialogContent: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('classEditDialogTitle').setText(title);
            this.resetFields();
            this.fillItemContainerWithCheckboxes();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseMultiValueCheckboxDialog', ex);
        }
    },

    resetFields: function () {
        try {
            Ext.getCmp('itemContainer').removeAll();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseMultiValueCheckboxDialog', ex);
        }
    },

    fillItemContainerWithCheckboxes: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            if (charact === null || charact === undefined)
              return;
            var charactValues = charact.get('charactValues');
            var classValues = charact.get('classValues');

            if (charactValues) {
                charactValues.each(function (charactValue) {
                    // prepare item data
                    var sAtwrt = charactValue.get('atwrt');
                    var sAtflv = charactValue.get('atflv');

                    sAtflv = sAtflv ? sAtflv : '';

                    if (sAtflv.length < 1)
                        sAtflv = '';

                    var classValue = null;

                    // filter classValues 
                    if (classValues && classValues.getCount() > 0) {
                        classValues.each(function (clValue) {
                            if (clValue.get('atwrt') === sAtwrt && clValue.get('atflv') === sAtflv)
                                classValue = clValue;

                        });
                    }
                    // create checkbox              // Ext.form.field.Checkbox
                    var checkBox = Ext.create('AssetManagement.customer.view.CharactValueCheckboxItem', {
                        charact: charact,
                        charactValue: charactValue,
                        classValue: classValue
                    });
                    //set checkbox status
                    if (classValue) {
                        checkBox.setValue(true);                // Set status true
                    } else {
                        checkBox.setValue(false);               // Set status false
                    }

                    Ext.getCmp('itemContainer').add(checkBox);

                });
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillItemContainerWithCheckboxes of BaseMultiValueCheckboxDialog', ex);

        }
    },

    resetAllCheckBoxesExceptForOne: function (atwrt, atwtb, atflv) {
        try {
            // Diese Methode simuliert das verhalten der Radio Buttons bzw. Einwertige Checkboxen.
            // Alle Checkboxen bis auf eines werden zurück gesetzt.
            var checkBoxes = Ext.getCmp('itemContainer').items.items;

            Ext.Array.each(checkBoxes, function (checkBox) {
                if (checkBox.getAtwrt() !== atwrt || checkBox.getAtwtb() !== atwtb || checkBox.getAtflv() !== atflv) {
                    checkBox.setValue(false);
                }
            }, this);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetAllCheckBoxesExceptForOne of BaseMultiValueCheckboxDialog', ex);
        }
    },

    transferInputValuesIntoModel: function () {
        var retval = false;
        try {
            var myModel = this.getViewModel();
            if (myModel.get('charact').get('classValues') === undefined)
                return;
            else
                var classValue = myModel.get('charact').get('classValues').data.items[0];

            var charactValues = myModel.get('charact').get('charactValues');
            var charact = myModel.get('charact');
            var classValues = myModel.get('charact').get('classValues');
            var viewValue = Ext.getCmp('itemContainer').items;
            var objClass = myModel.get('objClass');

            var trueClassValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });
            viewValue.each(function (classV) {
                if (classV.getValue() === true && !classV._classValue) {
                    var newClassValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, classV.atwrt, classV.atflv);
                    newClassValue.set('clint', objClass.get('clint'));
                    newClassValue.set('atwrt', classV.charactValue.get('atwrt'));
                } else {
                    var newClassValue = classV.getClassValue();
                }
                if (newClassValue) {
                    trueClassValues.add(newClassValue);
                }

            }, this);
            myModel.set('newValues', trueClassValues);
            retval = true;


            classValues.each(function (clV) {
                clV.set('clint', objClass.get('clint'));
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferInputValuesIntoModel of BaseMultiValueCheckboxDialog', ex);
        }
        return retval;
    }

});