Ext.define('AssetManagement.base.view.dialogs.BaseSingleTextLineInputDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
       'AssetManagement.customer.controller.dialogs.SingleTextLineInputDialogController',
       'AssetManagement.customer.model.dialogmodel.SingleTextLineInputDialogViewModel',
       'Ext.container.Container',
       'Ext.Component',
       'Ext.button.Button',
       'AssetManagement.customer.utils.StringUtils'
    ],

    viewModel: {
        type: 'SingleTextLineInputDialogModel'
    },

    inheritableStatics: {
        _instance: null,
        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.SingleTextLineInputDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSingleTextLineInputDialog', ex);
            }

            return this._instance;
        }
    },

    controller: 'SingleTextLineInputDialogController',
    width: 350,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    listeners: {
        afterlayout: function () {
            try {
                var textLineInputField = Ext.getCmp('singleLineTextDialogMsgTextField');
                textLineInputField.focus();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterlayout of BaseSingleTextLineInputDialog', ex);
            }
        }
    },

    //protected
    //@override
    buildUserInterface: function () {
        try {
            var myController = this.getController();

            var items = [
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
		                {
		                    xtype: 'label',
		                    cls: 'oxDialogTitle',
		                    id: 'singleTextLineInputDialogTitleLabel'
		                },
                        {
                            xtype: 'container',
                            margin: '10 10 0 10',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    height: 35,
                                    margin: '0 0 10 0',
                                    id: 'singleLineTextDialogMsgLabel',
                                    layout: {
                                        align: 'center'
                                    },
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    height: 30,
                                    id: 'singleLineTextDialogMsgTextField',
                                    enforceMaxLength: true,
                                    listeners: {
                                        change: myController.onValueChange,
                                        specialkey: myController.onSpecialkey,
                                        scope: myController
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            cls: 'oxDialogButtonBar',
                            margin: '10 0 0 0',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('ok'),
                                    listeners: {
                                        click: 'confirm',
                                        scope: myController
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'cancel'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSingleTextLineInputDialog', ex);
        }
    },

    //public
    //@override
    resetViewState: function () {
        try {
            var textLineInputField = Ext.getCmp('singleLineTextDialogMsgTextField');
            textLineInputField.setValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseSingleTextLineInputDialog', ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            var myModel = this.getViewModel();
            var title = myModel.get('title');
            var message = myModel.get('message');
            var maxLength = myModel.get('maxLength');
            var readOnly = myModel.get('readOnly');
            var value = myModel.get('value');

            var headerLabel = Ext.getCmp('singleTextLineInputDialogTitleLabel');
            headerLabel.setText(title);

            var messageLabel = Ext.getCmp('singleLineTextDialogMsgLabel');
            messageLabel.setText(message);

            messageLabel.setHidden(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message));

            var textLineInputField = Ext.getCmp('singleLineTextDialogMsgTextField');
            textLineInputField.maxLength = maxLength;
            textLineInputField.setReadOnly(readOnly);
            textLineInputField.setValue(value);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseSingleTextLineInputDialog', ex);
        }
    },

    getCurrentTextLineValue: function () {
        var retval = '';

        try {
            var textField = Ext.getCmp('singleLineTextDialogMsgTextField');

            if (textField)
                retval = textField.getValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentTextLineValue of BaseSingleTextLineInputDialog', ex);
        }

        return retval;
    }
});