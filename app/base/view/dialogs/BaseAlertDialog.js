Ext.define('AssetManagement.base.view.dialogs.BaseAlertDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.AlertDialogController',
        'AssetManagement.customer.model.dialogmodel.AlertDialogViewModel',
        'Ext.container.Container',
        'Ext.Component',
        'Ext.button.Button'
    ],
	
    viewModel: {
        type: 'AlertDialogModel'
    },
    
    controller: 'AlertDialogController',
    cls: 'oxDialogButtonBar',
    width: 300,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    //members
    _icon: null,
    _messageLabel: null,
    
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
    		    if(!this._instance)
    			   this._instance = Ext.create('AssetManagement.customer.view.dialogs.AlertDialog');
            
    		} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseAlertDialog', ex);
		    }
    			
            return this._instance;
        }
    },
   
    buildUserInterface: function() {
    	try {
    		this._icon = Ext.create('Ext.Img', {
    			width: 40,
    			height: 40,
    			cls: 'alertDialogIcon',
    		    src: 'resources/icons/alert.png'
    		});
    	
        	this._messageLabel = Ext.create('Ext.form.Label', {
        		flex: 1,
        		cls: 'alertDialogMessageLabel',
        		html: ''
        	});
    	
	    	var items =  [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    height: 25,
		                    text: this.getDialogTitle()
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'center',
		                        pack: 'stretch'
		                    },
		                    items: [
		                        this._icon,
		                        {
		                        	type: 'component',
		                        	width: 15
		                        },
		                        this._messageLabel
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('dialogOk'),
			                        listeners: {
			                    		click: 'dismiss'
			                    	}
		                        }
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
		    ];
		   
	    	this.add(items);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseAlertDialog', ex);
		}
	},

	//protected
    //@override
	getDialogTitle: function() {
	    var retval = '';
	    
	    try {
    	     retval = Locale.getMsg('caution') + '!';
    	} catch(ex) {
 			 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseAlertDialog', ex);
 		}
    	
 		return retval;
    },
    
    //protected
    //@override
	updateDialogContent: function() {
		try {
			//set the icon and the message
			if(this.getViewModel().get('icon')) {
				this._icon.setSrc(this.getViewModel().get('icon'));
			}
			
			this._messageLabel.setText(this.getViewModel().get('message'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseAlertDialog', ex);
		}
	}
});