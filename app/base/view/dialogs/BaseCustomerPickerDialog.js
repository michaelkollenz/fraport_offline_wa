Ext.define('AssetManagement.base.view.dialogs.BaseCustomerPickerDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.CustomerPickerDialogController',
        'AssetManagement.customer.model.dialogmodel.CustomerPickerDialogViewModel',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.View',
        'Ext.button.Button',
        'AssetManagement.customer.view.OxDynamicGridPanel'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.CustomerPickerDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseCustomerPickerDialog', ex);
            }

            return this._instance;
        }
    },
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.CustomerPickerDialogRendererMixin'
    },

    viewModel: {
        type: 'CustomerPickerDialogModel'
    },
    controller: 'CustomerPickerDialogController',
    height: 500,
    width: 700,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var myController = this.getController();
            var me = this;
            var items = [
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 10
		                }, {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'container',
		                            flex: 0.7,
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'textfield',
		                                    id: 'customerPickerNameField',
		                                    fieldLabel: Locale.getMsg('name')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    disabledCls: 'oxDisabledTextField',
		                                    id: 'customerPickerPostalCodeTextField',
		                                    fieldLabel: Locale.getMsg('zipCode')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    disabledCls: 'oxDisabledTextField',
		                                    id: 'customerPickerCityTextField',
		                                    fieldLabel: Locale.getMsg('city')
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 0.3,
		                            layout: {
		                                type: 'hbox',
		                                align: 'center',
		                                pack: 'center'
		                            },
		                            items: [
                                        {
                                            xtype: 'button',
                                            id: 'searchButtonCustomerPickerDialog',
                                            padding: '5 0 0 0',
                                            html: '<div><img width="50%" src="resources/icons/search.png"></img></div><p class=\'schwarz\'>' + Locale.getMsg('startSearch') + '</p><style type="text/css"> <!-- .schwarz { color:rgb(60,60,60); font-family:Helvetica;  font-size:9pt;  }</style>',
                                            height: 80,
                                            width: 90,
                                            icon: '',
                                            listeners: {
                                                click: 'onCustomerSearchButtonClick'
                                            }

                                        }
		                            ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'oxdynamicgridpanel',
		                    flex: 1,
		                    id: 'customerPickerGrid',
		                    scollable: 'vertical',
		                    disableSelection: true,
		                    header: false,
		                    title: Locale.getMsg('partner'),
		                    forceFit: true,
		                    parentController: myController,
                            owner: me,
		                    /*columns: [
		                        {
		                            xtype: 'gridcolumn',
		                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('kunnr'), '0');;
		                            },
		                            dataIndex: 'kunnr',
		                            text: Locale.getMsg('partner'),
		                            flex: 0.12
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                //store -> item -> aufnr, ktext
		                                var firstname = record.get('firstname');
		                                var lastname = record.get('lastname');
		                                var retVal = lastname;
		                                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(firstname))
		                                    retVal += ' ' + firstname;
		                                return retVal;
		                            },
		                            dataIndex: 'lastname',
		                            text: Locale.getMsg('name'),
		                            flex: 0.44
		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                //store -> item -> aufnr, ktext
		                                var postlcode = record.get('postlcod1');
		                                var city = record.get('city');
		                                var retVal = postlcode + ' ' + city;
		                                return retVal;
		                            },
		                            dataIndex: 'postlcod1',
		                            text: Locale.getMsg('checklist_Loc'),
		                            flex: 0.44
		                        }
		                    ],*/
		                    listeners: {
		                        cellclick: myController.onCustomerCellSelected,
		                        scope: myController
		                    }
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('cancel'),
		                            listeners: {
		                                click: 'cancel'
		                            }
		                        }
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseCustomerPickerDialog', ex);
        }
    },

    //protected
    //@override
    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('customers');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseCustomerPickerDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            //get data from modelview
            var customers = this.getViewModel().get('customers');
            var customerGrid = Ext.getCmp('customerPickerGrid');
            customerGrid.setStore(customers);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseCustomerPickerDialog', ex);
        }
    },

    resetViewState: function () {
        try {
            this.clearSearchFields()
            //since the store ref it self is not nullable, put an empty one inside
            Ext.getCmp('customerPickerGrid').setStore(Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.Customer',
                autoLoad: false
            }));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseCustomerPickerDialog', ex);
        }
    },

    clearSearchFields: function () {
        try {
            Ext.getCmp('customerPickerNameField').setValue('');
            Ext.getCmp('customerPickerPostalCodeTextField').setValue('');
            Ext.getCmp('customerPickerCityTextField').setValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchFields of BaseCustomerPickerDialog', ex);
        }
    },

    getSearchValues: function () {
        var retval = null;

        try {
            var customerToSearch = Ext.create('AssetManagement.customer.model.bo.Customer', {});

            customerToSearch.set('lastname', Ext.getCmp('customerPickerNameField').getValue());
            customerToSearch.set('postlcod1', Ext.getCmp('customerPickerPostalCodeTextField').getValue());
            customerToSearch.set('city', Ext.getCmp('customerPickerCityTextField').getValue());

            this.getViewModel().set('searchCustomer', customerToSearch);

            retval = customerToSearch;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValues of BaseCustomerPickerDialog', ex);
        }

        return retval;
    }
});