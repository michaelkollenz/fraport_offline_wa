Ext.define('AssetManagement.base.view.dialogs.BaseEquipmentPickerDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.EquipmentPickerDialogController',
        'AssetManagement.customer.model.dialogmodel.EquipmentPickerDialogViewModel',
        'Ext.form.field.Text',
        'Ext.button.Button',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.View',
                'AssetManagement.customer.view.OxDynamicGridPanel'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.EquipmentPickerDialog');
                }            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseEquipmentPickerDialog', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'EquipmentPickerDialogModel'
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.EquipmentPickerDialogRendererMixin'
    },
    
    controller: 'EquipmentPickerDialogController',
    height: 500,
    width: 600,
    header: false,
    modal: true,
    resizable: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
		    var items = [
		        {
			        xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                 {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    height: 25,
		                    text: Locale.getMsg('equipments')
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'container',
		                            flex: 0.7,
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'textfield',
		                                    id: 'equiPickerShorttxtField',
		                                    fieldLabel: Locale.getMsg('shorttext')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    id: 'equiPickerSerialNrTextField',
		                                    fieldLabel: Locale.getMsg('serialNumber')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    id: 'equiPickerMatnrTextField',
		                                    fieldLabel: Locale.getMsg('material')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    disabledCls: 'oxDisabledTextField',
		                                    id: 'equiPickerTplnrTextField',
		                                    fieldLabel: Locale.getMsg('funcLoc_Short')
		                                }
		                               /* {
		                                    xtype: 'container',
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'textfield',
		                                    id: 'equiPickerPartnerTextField',
		                                    fieldLabel: Locale.getMsg('partner')
		                                }*/
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 0.3,
		                            layout: {
		                                type: 'hbox',
		                                align: 'center',
		                                pack: 'center'
		                            },
		                            items: [
                                        {
                                            xtype: 'button',
                                            id: 'searchButtonEquipmentPickerDialog',
                                            padding: '5 0 0 0',
                                            html: '<div><img width="50%" src="resources/icons/search.png"></img></div><p class=\'schwarz\'>'+Locale.getMsg('startSearch')+'</p><style type="text/css"> <!-- .schwarz { color:rgb(60,60,60); font-family:Helvetica;  font-size:9pt;  }</style>',
                                            height: 80,
                                            width: 90,
                                            icon: '', 
                                            listeners: {
                                        		click: 'onEquiSearchButtonClick'
                                        	}
                                        
                                        }
                                    ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'oxdynamicgridpanel',
		                    flex: 1,
		                    id: 'equiPickerGridPanel',
		                    scollable: 'vertical',
		                    disableSelection: true,
		                    hideContextMenuColumn: true,
		                    header: false,
		                    title: Locale.getMsg('equipments'),
		                    forceFit: true,
		                    parentController: myController,
                            owner: me,
		                    /*columns: [
		                        {
		                            xtype: 'gridcolumn',
		                            dataIndex: 'string',
			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                return '<img src="resources/icons/equi.png"/>';
		                            },
		                            enableColumnHide: false, 
		                            maxWidth: 70,
		                            minWidth: 70,
		                            align: 'center'
			                    },
		                        {
		                            xtype: 'gridcolumn',
		                            flex: 10,
			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                var equnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
		                                var eqktx = record.get('eqktx');
		                                return equnr + '<p>' + eqktx + '</p>';
		                            },
		                            enableColumnHide: false,
		                            dataIndex: 'equnr',
		                            text: Locale.getMsg('equipment')
		                        },
//		                        {
//		                            xtype: 'gridcolumn',
//		                            flex: 1,
//			                        enableColumnHide: false,
//		                            dataIndex: 'tidnr',
//		                            text: Locale.getMsg('technicalIdentNr')
//		                        },
//		                        {
//		                            xtype: 'gridcolumn',
//		                            flex: 1,
//			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
//		                                var eqfnr = record.get('eqfnr');
//		
//		                                return eqfnr;
//		                            },
//		                            enableColumnHide: false,
//		                            dataIndex: 'eqfnr',
//		                            text: Locale.getMsg('sortField')	
//		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            flex: 5,
			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
		                            },
		                            enableColumnHide: false,
		                            dataIndex: 'matnr',
		                            text: Locale.getMsg('material')	
		                        },
//		                        {
//		                            xtype: 'gridcolumn',
//		                            flex: 4,
//			                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
//		                                var stort = record.get('stort');
//		                                var room = record.get('msgrp');
//		                                
//			                            return stort +'<p>'+room+'</p>';
//		                            },
//		                            enableColumnHide: false,
//		                            dataIndex: 'stort',
//		                            text: Locale.getMsg('locationAndRoom')
//		                        },
		                        {
		                            xtype: 'gridcolumn',
		                            flex: 7,
		                            text: Locale.getMsg('serialNumber'),
		                            dataIndex: 'sernr'
		                        }
		                    ],*/
			                listeners: {
		                    	cellclick: myController.onEquiCellSelected,
		                    	scope: myController
		                    }
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
				                    xtype: 'button',
				                    flex: 1,
				                    cls: 'oxDialogButton first',
				                    text: Locale.getMsg('cancel'),
					                listeners: {
				                		click: 'cancel'
				                	}
				                }
		                    ]
		                }
		            ]
		        },
		        {
			        xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseEquipmentPickerDialog', ex);
		}
	},
    
	//protected
	//@override
	getPageTitle: function() {
	    var retval = '';
	    
	    try {
	        retval = Locale.getMsg('equipments');
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseEquipmentPickerDialog', ex);
		}
	    
		return retval;
	},
   
	//protected
	//@override
	updateDialogContent: function() {
		try {
			//get data from modelview
			var myModel = this.getViewModel();
			
			var superiorTplnr = myModel.get('superiorTplnr');
			var equipments = myModel.get('equipmentStore');
			
			var hasSupTplnr = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(superiorTplnr);
			var tplnrCriteriaTextField = Ext.getCmp('equiPickerTplnrTextField');
			
			if(tplnrCriteriaTextField) {
				if(hasSupTplnr) {
					tplnrCriteriaTextField.setValue(superiorTplnr);
					tplnrCriteriaTextField.setDisabled(true);
				} else {
					tplnrCriteriaTextField.setDisabled(false);
				}
			}

			var equiPickerGridPanel = Ext.getCmp('equiPickerGridPanel');
			equiPickerGridPanel.reconfigure(equipments);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseEquipmentPickerDialog', ex);
		}
	},

	//public
	//@override
	resetViewState: function() {
		try {
			this.clearSearchFields();
		
			//since the store ref it self is not nullable, put an empty one inside
			Ext.getCmp('equiPickerGridPanel').reconfigure(Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.Equipment',
				autoLoad: false
			}));
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseEquipmentPickerDialog', ex);
    	}
	},
	
	clearSearchFields: function() {
		try {
			Ext.getCmp('equiPickerShorttxtField').setValue(''); 
			Ext.getCmp('equiPickerSerialNrTextField').setValue('');
			Ext.getCmp('equiPickerMatnrTextField').setValue('');
			Ext.getCmp('equiPickerTplnrTextField').setValue('');
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchFields of BaseEquipmentPickerDialog', ex);
    	}
	},
	
	getSearchValues: function() {
		var retval = null;
		
		try {
			var equiToSearch = Ext.create('AssetManagement.customer.model.bo.Equipment', {}); 
			
			equiToSearch.set('eqktx', Ext.getCmp('equiPickerShorttxtField').getValue()); 
			equiToSearch.set('sernr',  Ext.getCmp('equiPickerSerialNrTextField').getValue()); 
			equiToSearch.set('matnr',  Ext.getCmp('equiPickerMatnrTextField').getValue()); 
			equiToSearch.set('tplnr',  Ext.getCmp('equiPickerTplnrTextField').getValue()); 
	
			this.getViewModel().set('searchEqui', equiToSearch);
			
			retval = equiToSearch;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValues of BaseEquipmentPickerDialog', ex);
		}
		
		return retval;
	}
});