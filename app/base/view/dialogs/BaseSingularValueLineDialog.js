﻿Ext.define('AssetManagement.base.view.dialogs.BaseSingularValueLineDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [

        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.dialogs.SingularValueLineDialogController',
        'AssetManagement.customer.model.dialogmodel.SingularValueLineDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.CharactValueTextFieldItem',
        'AssetManagement.customer.view.CharactValueNumberFieldItem',
        'AssetManagement.customer.view.CharactValueDateItem',
        'AssetManagement.customer.view.CharactValueTimeFieldItem',
        'AssetManagement.customer.manager.ClassificationManager'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.SingularValueLineDialog');
                }

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSingularValueLineDialog', ex);
            }

            return this._instance;
        }

    },

    viewModel: {
        type: 'SingularValueLineDialogModel'
    },

    controller: 'SingularValueLineDialogController',
    width: 500,
    floating: true,
    modal: true,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var items = [
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            id: 'classEditDialogTitleSingular',
                            cls: 'oxHeaderLabel',
                            height: 25
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            id: 'itemContainerSingular',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            xtype: 'component',
                            minHeight: 5
                        },

                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'save'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'cancel'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                }
            ];

            this.add(items);
            this.updateDialogContent();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSingularValueLineDialog', ex);
        }
    },

    getDialogTitle: function () {
        var retval = '';

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('charact')))
                retval = Locale.getMsg('classificationsFor') + " " + this.getViewModel().get('charact').get('atbez');
            else
                retval = Locale.getMsg('classifications');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseSingularValueLineDialog', ex);
        }

        return retval;
    },

    updateDialogContent: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('classEditDialogTitleSingular').setText(title);
            this.resetFields();
            this.fillItemContainerWithSingularValue();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseSingularValueLineDialog', ex);
        }
    },

    resetFields: function () {
        try {
            Ext.getCmp('itemContainerSingular').removeAll();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseSingularValueLineDialog', ex);
        }
    },

    createTextField: function () {
        var retval = null;
        try {
            var charact = this.getViewModel().get('charact');
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues').data.items[0];

            var textInput = Ext.create('AssetManagement.customer.view.CharactValueTextFieldItem', {
                charact: charact,
                charactValue: charactList,
                classValues: classValues
            });
            retval = Ext.getCmp('itemContainerSingular').add(textInput);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createTextField of BaseSingularValueLineDialog', ex);
        }
        return retval;
    },

    fillItemContainerWithSingularValue: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var objClass = myModel.get('objClass');

             if (charact === null || charact === undefined)
                 return;

            var newClassValue = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });
            var classValue = null;

            if (charact.get('classValues') === undefined) {
                classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass);
                newClassValue.add(classValue);
                charact.set('classValues', newClassValue);

            } else {
                var classValues = charact.get('classValues');
                classValue = charact.get('classValues').data.items[0];
            }

            if (classValue) {

                if (classValue.get('atinn') === charact.get('atinn')) {
                    newClassValue.add(classValue);
                } else {
                    //value does not exist
                    classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass);
                }
            } else {
                //value does not exist
                classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass);
                newClassValue.add(classValue);
            }

            this.filterField(charact);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillItemContainerWithSingularValue of BaseSingularValueLineDialog', ex);

        }
    },

    filterField: function (charact) {
        try {
            // Show a singular textfield on dialog
            if (charact.get('atfor') === "CHAR" && charact.get('atein') === 'X') {
                // create TextField Item
                this.createTextField();
            }
                //Show a singular datefield on dialog
            else if (charact.get('atfor') === "DATE") {
                //create DateField
                this.createDateField();

            }
                //Show a singular timefield on dialog
            else if (charact.get('atfor') === 'TIME') {
                //create TimeField
                this.createTimeField();
            }
                //Show a singular numberfield on dialog
            else if (charact.get('atfor') === 'NUM' || charact.get('atfor') === 'CURR') {
                //create NumberField
                this.createNumberField();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillItemContainerWithSingularValue of BaseSingularValueLineDialog', ex);
        }
    },
    createDateField: function () {
        var retval = null;
        try {
            var charact = this.getViewModel().get('charact');
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues').data.items[0];

            var dateInput = Ext.create('AssetManagement.customer.view.CharactValueDateItem', {
                charact: charact,
                charactValue: charactList,
                classValues: classValues
            });
            retval = Ext.getCmp('itemContainerSingular').add(dateInput);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createDateField of BaseSingularValueLineDialog', ex);
        }
        return retval;
    },

    createTimeField: function () {
        var retval = null;
        try {
            var charact = this.getViewModel().get('charact');
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues').data.items[0];

            var timeInput = Ext.create('AssetManagement.customer.view.CharactValueTimeFieldItem', {
                charact: charact,
                charactValue: charactList,
                classValues: classValues
            });
            retval = Ext.getCmp('itemContainerSingular').add(timeInput);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createTimeField of BaseSingularValueLineDialog', ex);
        }
        return retval;
    },

    createNumberField: function () {
        var retval = null;
        try {
            var charact = this.getViewModel().get('charact');
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues').data.items[0];

            var allowedDecimals = 0;

            //allow numberfield to be able to get the amount of decimals sent from sap
            if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(charact.get('anzdz'))){
                allowedDecimals = Number(charact.get('anzdz'));
            }

            var showDecimals = true;
            if(allowedDecimals === 0) {
                showDecimals = false;
            }

            var numberInput = Ext.create('AssetManagement.customer.view.CharactValueNumberFieldItem', {
                allowDecimals: showDecimals,
                decimalPrecision: allowedDecimals,
                charact: charact,
                charactValue: charactList,
                classValues: classValues
            });
            retval = Ext.getCmp('itemContainerSingular').add(numberInput);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createTimeField of BaseSingularValueLineDialog', ex);
        }
        return retval;
    },


    transferInputValuesIntoModel: function () {
        var retval = false;
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var objClass = myModel.get('objClass');
            var classValues2 = myModel.get('charact').get('classValues');
            var viewValue = Ext.getCmp('itemContainerSingular').items.items[0].getValue();
            var classValue = null;
            var classValues = myModel.get('charact').get('classValues');

            if (!classValues2) {
                classValues2 = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });

                if (charact.get('atfor') === 'CHAR')
                    classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, viewValue, null);
                else
                    classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, null, viewValue);

                classValue.set('clint', objClass.get('clint'));
                classValues2.add(classValue);
                charact.set('classValues', classValues2);

            } else {
                classValues2 = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });
                if (charact.get('atfor') === 'CHAR') {
                    classValue = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classValues.data.items[0]);
                    classValue.set('atwrt', viewValue);
                    classValue.set('clint', objClass.get('clint'));
                    classValues2.add(classValue);
                } else {
                    classValue = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classValues.data.items[0]);

                    classValue.set('atflv', viewValue);
                    classValue.set('clint', objClass.get('clint'));
                    classValues2.add(classValue);
                }
                charact.set('classValues', classValues2);

            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferInputValuesIntoModel of SingularValueLineDialogController', ex);
        }

    }

});