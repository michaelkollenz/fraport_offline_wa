﻿Ext.define('AssetManagement.base.view.dialogs.BaseOnlineSearchDialog', {
    extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',

    requires: [
       'AssetManagement.customer.controller.dialogs.OnlineSearchDialogController',
       'AssetManagement.customer.model.dialogmodel.OnlineSearchDialogViewModel'
    ],

    viewModel: {
        type: 'OnlineSearchDialogModel'
    },

    controller: 'OnlineSearchDialogController',

    requires: [
        'AssetManagement.customer.controller.dialogs.OnlineSearchDialogController',
        'AssetManagement.customer.model.dialogmodel.OnlineSearchDialogViewModel',
        'AssetManagement.customer.utils.StringUtils'
    ],

    viewModel: {
        type: 'BaseOnlineSearchDialogModel'
    },

    controller: 'BaseOnlineSearchDialogController',

    _searchCriteriaLinesContainer: null,
    _maxHitCountNF: null,
    _titleBar: null,
    _progressBar: null,
    _messageLine: null,
    _cancelButton: null,
    _searchButton: null,

    config: {
        supportsDynamicWidth: true,
        minWidth: 400,
        maxWidth: 500
    },

    inheritableStatics: {
        _instance: null,

        getInstance: function (arguments) {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create(Ext.getClassName(this), arguments);
                }

                this._instance.setInitialWidth(arguments ? arguments.width : undefined);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }

            return this._instance;
        }
    },

    //private
    buildUserInterface: function () {
        try {
            var myController = this.getController();
            var myModel = this.getViewModel();

            var mainContainer = Ext.create('Ext.container.Container', {
                xtype: 'container',
                flex: 1,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                }
            });

            this.add(mainContainer);

            var buttonBar = Ext.create('Ext.container.Container', {
                layout: {
                    type: 'hbox'
                },
                cls: 'oxDialogButtonBar',
                style: 'margin-top: 10px'
            });

            this._cancelButton = Ext.create('Ext.button.Button', {
                flex: 1,
                height: '36px',
                cls: 'oxDialogButton',
                text: Locale.getMsg('cancel'),
                handler: this.getController().cancelButtonClicked,
                scope: this.getController()
            });

            this._searchButton = Ext.create('Ext.button.Button', {
                flex: 1,
                height: '36px',
                cls: 'oxDialogButton first',
                text: Locale.getMsg('startSearch'),
                handler: this.getController().onSearchButtonClicked,
                scope: this.getController()
            });

            buttonBar.add(this._searchButton);
            buttonBar.add(this._cancelButton);

            this._titleBar = Ext.create('Ext.form.Label', { cls: 'oxDialogTitle' });
            this._messageLine = Ext.create('Ext.form.Label', { cls: 'oxDialogMessage' });
            this._progressBar = Ext.create('AssetManagement.customer.view.utils.ProgressBar', { parent: this, marginLeftRight: 0, infinite: myController.getInfinite() });

            mainContainer.add(this._titleBar);
            
            this._searchCriteriaLinesContainer = Ext.create('Ext.container.Container', {
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                margin: '10 0 0 0',
                cls: 'oxOnlineSearchCriteriaContainer'
            });

            mainContainer.add(this._searchCriteriaLinesContainer);

            this._maxHitCountNF = Ext.create('AssetManagement.customer.view.utils.OxNumberField', {
                fieldLabel: Locale.getMsg('maxHitCount'),
                labelWidth: 120,
                minValue: 0,
                maxValue: 500,
                allowDecimals: false,
                allowExponential: false,
                maxWidth: 230
            });

            mainContainer.add(this._maxHitCountNF);

            mainContainer.add(this._messageLine);
            mainContainer.add(this._progressBar);

            mainContainer.add(buttonBar);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            //parent will update handle usual progress dialog contents
            this.callParent();

            this.manageSearchCriteriaInput();

            this.manageMaximumHitCountInput();

            this.manageMessageLabelVisibility();

            this.manageProgressBar();

            this.manageButtonbar();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //fills the search criteria input control lines
    manageSearchCriteriaInput: function () {
        try {
            var myModel = this.getViewModel();
            var searchCriteria = myModel.get('searchCriteria');
            var requestRunning = myModel.get('mode') === this.getController().DIALOG_MODES.REQUEST;

            //first destroy the existing items (this avoids combobox issues on iOS)
            var container = this._searchCriteriaLinesContainer;
            container.removeAll(true, true);

            //loop all search criteria from the model and generate search criteria input lines
            if (searchCriteria && searchCriteria.getCount() > 0) {
                searchCriteria.each(function (attribute, searchCriterion) {
                    container.add(this.generateSearchCriterionInputContainer(searchCriterion));
                }, this);
            }

            //set the input disabled, if a request is currently running
            container.setDisabled(requestRunning);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //generates a search criterion input container
    generateSearchCriterionInputContainer: function (searchCriterion) {
        var retval = null;

        try {
            var lineContainer = Ext.create('Ext.container.Container', {
                itemId: searchCriterion['ATTR'],
                data: searchCriterion,
                layout: {
                    type: 'hbox'
                },
                cls: 'oxOnlineSearchCriteriaLineContainer'
            });

            var inputField = this.generateInputField(searchCriterion);
            lineContainer.add(inputField);

            var operatorDropDown = this.generateOperatorDropdown(searchCriterion);
            lineContainer.add(operatorDropDown);

            retval = lineContainer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    //generates the input field for the given search criterion
    generateInputField: function (searchCriterion) {
        var retval = null;

        try {
            retval = Ext.create('AssetManagement.customer.view.utils.OxTextField', {
                flex: 1,
                itemId: 'value',
                fieldLabel: Locale.getMsg(searchCriterion['LABEL']),
                labelWidth: 120,
                maxLength: searchCriterion['MAX_LENGTH'],
                value: searchCriterion['VALUE']
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    //generates the operator dropdown for the given search criterion
    generateOperatorDropdown: function (searchCriterion) {
        var retval = null;

        try {
            //fill damage group dropdown
            var operatorStore = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            operatorStore.add({ "text": '*', "value": 'LIKE' });
            operatorStore.add({ "text": '=', "value": 'EQ' });
            operatorStore.add({ "text": '!=', "value": 'NE' });
            operatorStore.add({ "text": '>', "value": 'GT' });
            operatorStore.add({ "text": '<', "value": 'LT' });

            var combobox = Ext.create('AssetManagement.customer.view.utils.OxComboBox', {
                width: '50px',
                itemId: 'oper',
                editable: false,
                displayField: 'text',
                valueField: 'value',
                queryMode: 'local',
                store: operatorStore
            });

            //set the current value
            combobox.setValue(searchCriterion['OPER']);

            retval = combobox;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    manageMaximumHitCountInput: function () {
        try {
            var myModel = this.getViewModel();
            var maxHitCount = myModel.get('maxHitCount');
            var requestRunning = myModel.get('mode') === this.getController().DIALOG_MODES.REQUEST;

            this._maxHitCountNF.setValue(maxHitCount);

            //set the input disabled, if a request is currently running
            this._maxHitCountNF.setDisabled(requestRunning);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    manageMessageLabelVisibility: function () {
        try {
            var myModel = this.getViewModel();
            var message = myModel.get('message');

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message)) {
                this._messageLine.setHidden(false);
            } else {
                this._messageLine.setHidden(true);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    manageProgressBar: function () {
        try {
            var myModel = this.getViewModel();
            var requestRunning = myModel.get('mode') === this.getController().DIALOG_MODES.REQUEST;

            //only show the progress bar if the search request is running
            this._progressBar.setHidden(!requestRunning);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    manageButtonbar: function () {
        try {
            var myModel = this.getViewModel();
            var isSelectionMode = myModel.get('mode') === this.getController().DIALOG_MODES.SELECTION;

            //show the search button, if the selection mode is active
            this._searchButton.setHidden(!isSelectionMode);

            //dynamically add the first css attribute the the cancel button, if it is the only one displayed
            if (!isSelectionMode) {
                this._cancelButton.addCls('first');
            } else {
                this._cancelButton.removeCls('first');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //provides the current input values
    getCurrentInputValues: function () {
        var retval = null;

        try {
            var returnObject = {};

            //loop the search criteria control lines and extract the input information
            var searchCriteriaMap = Ext.create('Ext.util.HashMap');
            var criteriaContainer = this._searchCriteriaLinesContainer;
            var criteriaLinesArray = criteriaContainer ? criteriaContainer.items.items : null; //TODO change access of items

            if (criteriaLinesArray && criteriaLinesArray.length > 0) {
                Ext.Array.each(criteriaLinesArray, function (criterionContainer) {
                    var criterion = {};

                    //extract the attribute
                    var data = criterionContainer.getData();
                    var attribute = data ? data['ATTR'] : criterionContainer.itemId;

                    //extract the search value
                    var valueInputField = criterionContainer.queryById('value');
                    var value = valueInputField ? valueInputField.getValue() : '';

                    //extract the search operator
                    var operatorDropDown = criterionContainer.queryById('oper');
                    var operator = operatorDropDown ? operatorDropDown.getValue() : '';

                    criterion['ATTR'] = attribute;
                    criterion['VALUE'] = value;
                    criterion['OPER'] = operator;

                    searchCriteriaMap.add(attribute, criterion);
                }, this);
            }

            returnObject['searchCriteria'] = searchCriteriaMap;

            var maxHitCount = this._maxHitCountNF.getValue();
            returnObject['maxHitCount'] = maxHitCount;

            retval = returnObject;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    }
});