﻿Ext.define('AssetManagement.base.view.dialogs.BaseSingularValueCheckboxDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [

        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.dialogs.SingularValueCheckboxDialogController',
        'AssetManagement.customer.model.dialogmodel.SingularValueCheckboxDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.CharactValueCheckboxItem'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.dialogs.SingularValueCheckboxDialog');
                }

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSingularValueCheckboxDialog', ex);
            }

            return this._instance;
        }

    },

    viewModel: {
        type: 'SingularValueCheckboxDialogModel'
    },

    controller: 'SingularValueCheckboxDialogController',
    width: 500,
    floating: true,
    modal: true,
    header: false,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function () {
        try {
            var items = [
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            id: 'classTitelForSingularCheckbox',
                            cls: 'oxHeaderLabel',
                            height: 25
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            id: 'itemContainerForSingularCheckbox',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            }
                        },
                        {
                            xtype: 'component',
                            minHeight: 5
                        },

                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'save'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'cancel'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                }
            ];

            this.add(items);
            this.updateDialogContent();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSingularValueCheckboxDialog', ex);
        }
    },

    getDialogTitle: function () {
        var retval = '';

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('charact')))
                retval = Locale.getMsg('classificationsFor') + " " + this.getViewModel().get('charact').get('atbez');
            else
                retval = Locale.getMsg('classifications');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseSingularValueCheckboxDialog', ex);
        }

        return retval;
    },

    updateDialogContent: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('classTitelForSingularCheckbox').setText(title);
            this.resetFields();
            this.fillItemContainerWithCheckboxes();
            
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseSingularValueCheckboxDialog', ex);
        }
    },

    resetFields: function () {
        try {
            Ext.getCmp('itemContainerForSingularCheckbox').removeAll();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseSingularValueCheckboxDialog', ex);
        }
    },

    createTextField: function () {
        var retval = null;
        try {
            var charact = this.getViewModel().get('charact');
            var charactList = charact.get('charactValues');
            var classValues = charact.get('classValues');
            var classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass)
            var textInput = Ext.create('AssetManagement.customer.view.CharactValueTextFieldItem', {
                charact: charact,
                charactValue: charactList,
                classValues: classValues
            });
            retval = Ext.getCmp('itemContainerForSingularCheckbox').add(textInput);

        } catch (ex) {
            AssetManagemenet.helper.OxLogger.logException('Exception occurred inside createTextField of BaseSingularValueCheckboxDialog', ex);
        }
        return retval;
    },

    fillItemContainerWithCheckboxes: function (charact) {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            if (charact === null || charact === undefined)
              return;

            var charactValues = charact.get('charactValues');
            var classValues = charact.get('classValues');

            if (charactValues) {
                charactValues.each(function (charactValue) {
                    // prepare item data
                    var sAtwrt = charactValue.get('atwrt');
                    var sAtwtb = charactValue.get('atwtb');
                    var sAtflv = charactValue.get('atflv');
                    var sAtzhl = charactValue.get('atzhl');
                    var id = charactValue.get('id');

                    sAtwtb = sAtwtb ? sAtwtb : '';
                    sAtflv = sAtflv ? sAtflv : '';

                    if (sAtwtb.length < 1)
                        sAtwtb = '';
                    if (sAtflv.length < 1)
                        sAtflv = '';
                    var einWert = false;
                    if (charact.get('atein') === ('X'))
                        einWert = true;

                    var classValue = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.ObjClassValue',
                        autoLoad: false
                    });

                    //// filter classValues 
                    if (classValues) {
                        classValues.each(function (clValues) {
                            if (clValues.get('atwrt') === sAtwrt && clValues.get('atflv') === sAtflv)
                                classValue.add(clValues);
                        });
                    }
                    // create checkbox              // Ext.form.field.Checkbox
                    var checkBox = Ext.create('AssetManagement.customer.view.CharactValueSingleCheckbox', {
                        charact: charact,
                        charactValue: charactValue,
                        classValue: classValue,
                        atwrt: sAtwrt,
                        atwtb: sAtwtb,
                        atflv: sAtflv,
                        atzhl: sAtzhl,
                        einWert: einWert,
                        sid: id,
                        value: classValue.getCount() > 0 ? true : false,
                        checked: classValue.getCount() > 0 ? true : false
                    });

                    Ext.getCmp('itemContainerForSingularCheckbox').add(checkBox);

                    ////set checkbox status
                    //if (classValue.getCount() > 0) {
                    //    checkBox.setValue(true);                // Set status true
                    //} else {
                    //    checkBox.setValue(false);               // Set status false
                    //}
                });

            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseSingularValueCheckboxDialog', ex);

        }
    },

    resetAllCheckBoxesExceptForOne: function (atwrt, atwtb, atflv) {
        try {
            // Diese Methode simuliert das verhalten der Radio Buttons bzw. Einwertige Checkboxen.
            // Alle Checkboxen bis auf eines werden zurück gesetzt.
            var checkBoxes = Ext.getCmp('itemContainerForSingularCheckbox').items.items;

            Ext.Array.each(checkBoxes, function (checkBox) {
                if (checkBox.getAtwrt() !== atwrt || checkBox.getAtwtb() !== atwtb || checkBox.getAtflv() !== atflv) {
                    checkBox.setValue(false);
                }
            }, this);


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetAllCheckBoxesExceptForOne of BaseSingularValueCheckboxDialog', ex);
        }
    },

    transferInputValuesIntoModel: function () {
        var retval = false;
        try {

            var myModel = this.getViewModel();
            var objClass = myModel.get('objClass');
            var classValues = myModel.get('charact').get('classValues').data.items[0];
            var charactValues = myModel.get('charact').get('charactValues');
            var charact = myModel.get('charact');
            var charactLists = myModel.get('charact').get('classValues');
            var viewValue = Ext.getCmp('itemContainerForSingularCheckbox').items;

            var trueClassValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            viewValue.each(function (classV) {
                if (classV.getValue() === true) {
                    var newClassValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, classV.atwrt, classV.atflv);
                    newClassValue.set('clint', objClass.get('clint'));
                    newClassValue.set('atwrt', classV.charactValue.get('atwrt'));
                } else {
                    var newClassValue = classV.getClassValue();
                }
                if (newClassValue) {
                    trueClassValues.add(newClassValue);
                }

            }, this);
            myModel.set('newValues', trueClassValues);
            retval = true;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferInputValuesIntoModel of BaseSingularValueCheckboxDialog', ex);
        }
        return retval;
    },

    //get current values from screen
    getCurrentInputValues: function () {
        var retval = null;

        try {
            var value = Ext.getCmp('itemContainerForSingularCheckbox').items.items[0].getValue();
            var charact = this.getViewModel().get('charact');

            if (charact && charact.get('atfor') === 'CHAR' && charact.get('atein') === 'X')
                var valueAtwrt = Ext.getCmp('itemContainerForSingularCheckbox').items.items[0].getValue();
            else
                var valueAtflv = Ext.getCmp('itemContainerForSingularCheckbox').items.items[0].getValue();

            retval = {
                atflv: valueAtflv,
                atwrt: valueAtwrt
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseSingularValueCheckboxDialog', ex);
            retval = null;
        }

        return retval;
    }

});