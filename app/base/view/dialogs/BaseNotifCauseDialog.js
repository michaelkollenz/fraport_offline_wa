Ext.define('AssetManagement.base.view.dialogs.BaseNotifCauseDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.controller.dialogs.NotifCauseDialogController',
        'AssetManagement.customer.model.dialogmodel.NotifCauseDialogViewModel',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.NotifCauseDialog');
                }            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifCauseDialog', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'NotifCauseDialogModel'
    },
    
    controller: 'NotifCauseDialogController',
    width: 500,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
        try {
            var items = [
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'label',
                            cls: 'oxHeaderLabel',
                            height: 25,
                            id: 'notifCauseDialogHeader'
                            //text: Locale.getMsg('createNotifCause')
                        },
                        {
                            xtype: 'component',
                            height: 15
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'oxcombobox',
                                    flex: 2,
                                    id: 'notifCauseDialogCauseGrpCb',
                                    maxWidth: 290,
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value',
                                    listeners: {
                                        select: 'onCauseGrpSelected'
                                    },
                                    fieldLabel: Locale.getMsg('cause')
                                },
                                {
                                    xtype: 'component',
                                    width: 5
                                },
                                {
                                    xtype: 'oxcombobox',
                                    flex: 1,
                                    id: 'notifCauseDialogCauseCodeCb',
                                    labelStyle: 'display: none;',
                                    editable: false,
                                    displayField: 'text',
                                    queryMode: 'local',
                                    valueField: 'value'
                                }
                            ]
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'oxtextfield',
                            id: 'notifCauseDialogDescrTextField',
                            fieldLabel: Locale.getMsg('specification'),
                            maxLength: 40
                        },
                        {
                            xtype: 'component',
                            height: 5
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            cls: 'oxDialogButtonBar',
                            items: [
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('save'),
                                    listeners: {
                                        click: 'onSaveButtonClicked'
                                    }

                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton',
                                    text: Locale.getMsg('cancel'),
                                    listeners: {
                                        click: 'onCancelButtonClicked'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    minWidth: 10,
                    maxWidth: 10
                }
            ];

            this.add(items);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    //@override
    getDialogTitle: function() {
        var retval = '';

        try {
            var notifCause = this.getViewModel().get('notifCause');

            retval = notifCause? Locale.getMsg('editNotifCause') : Locale.getMsg('createNotifCause');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    manageDialogTitle: function () {
        try {
            var title = this.getDialogTitle();
            Ext.getCmp('notifCauseDialogHeader').setText(title);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

        }
    },
    //protected
    //@override
    updateDialogContent: function () {
        try {

            this.manageDialogTitle();

            this.manageCauseCodeComboBoxes();

            this.manageShorttextInputField();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    manageCauseCodeComboBoxes: function () {
        try {
            this.fillCauseCodeGroupComboBox();

            this.setSelectionOfCauseCodeGroup();

            this.fillCauseCodeCombobox();

            this.setSelectionOfCauseCode();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //clears the input field value or fills it with the current notif item short text
    manageShorttextInputField: function () {
        try {
            var myModel = this.getViewModel();
            var notifCause = myModel.get('notifCause');
            var shorttext = notifCause ? notifCause.get('urtxt') : '';

            Ext.getCmp('notifCauseDialogDescrTextField').setValue(shorttext);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    fillCauseCodeGroupComboBox: function () {
        try {
            var myModel = this.getViewModel();
            var causeCodeGroupsMap = myModel.get('causeCodeGroupsMap');

            // fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (causeCodeGroupsMap) {
                causeCodeGroupsMap.each(function (codeGroupKey, groupsCodes) {
                    if (groupsCodes.getCount() > 0) {
                        store.add({ "text": codeGroupKey + ' ' + groupsCodes.getAt(0).get('codegrkurztext'), "value": groupsCodes });
                    }
                }, this);
            }

            var causeGroupComboBox = Ext.getCmp('notifCauseDialogCauseGrpCb');
            causeGroupComboBox.clearValue();
            causeGroupComboBox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    setSelectionOfCauseCodeGroup: function () {
        try {
            var myModel = this.getViewModel();
            var causeCodeGroupsMap = myModel.get('causeCodeGroupsMap');

            if (causeCodeGroupsMap) {
                var notifCause = myModel.get('notifCause');
                var itemsGroupKey = notifCause ? notifCause.get('urgrp') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsGroupKey)) {
                    var groupToSelect = causeCodeGroupsMap.get(itemsGroupKey);

                    if (groupToSelect) {
                        var comboBox = Ext.getCmp('notifCauseDialogCauseGrpCb');

                        var recordToSelect = comboBox.findRecordByValue(groupToSelect);

                        if (recordToSelect)
                            comboBox.select(recordToSelect);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    fillCauseCodeCombobox: function () {
        try {
            // fill damage group dropdown
            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            var currentSelectedCodeGroup = Ext.getCmp('notifCauseDialogCauseGrpCb').getValue();

            if (currentSelectedCodeGroup && currentSelectedCodeGroup.getCount() > 0) {
                currentSelectedCodeGroup.each(function (code) {
                    store.add({ "text": code.get('code') + ' ' + code.get('kurztext'), "value": code });
                });
            }

            var causeGroupComboBox = Ext.getCmp('notifCauseDialogCauseCodeCb');
            causeGroupComboBox.clearValue();
            causeGroupComboBox.bindStore(store);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    setSelectionOfCauseCode: function () {
        try {
            var myModel = this.getViewModel();
            var causeCodeGroupsMap = myModel.get('causeCodeGroupsMap');

            if (causeCodeGroupsMap) {
                var notifCause = myModel.get('notifCause');
                var itemsCodeKey = notifCause ? notifCause.get('urcod') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemsCodeKey)) {
                    var itemsGroupKey = notifCause.get('urgrp');
                    var groupsCodes = causeCodeGroupsMap.get(itemsGroupKey);

                    if (groupsCodes && groupsCodes.getCount() > 0) {
                        var codeToSelect = null;

                        groupsCodes.each(function (code) {
                            if (code.get('code') === itemsCodeKey) {
                                codeToSelect = code;
                                return false;
                            }
                        });

                        if (codeToSelect) {
                            var comboBox = Ext.getCmp('notifCauseDialogCauseCodeCb');

                            var recordToSelect = comboBox.findRecordByValue(codeToSelect);

                            if (recordToSelect)
                                comboBox.select(recordToSelect);
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    getCurrentInputValues: function () {
        var retval = null;

        try {
            var shorttext = Ext.getCmp('notifCauseDialogDescrTextField').getValue();
            var causeCustCode = Ext.getCmp('notifCauseDialogCauseCodeCb').getValue();

            retval = {
                shorttext: shorttext,
                causeCustCode: causeCustCode
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    }
});