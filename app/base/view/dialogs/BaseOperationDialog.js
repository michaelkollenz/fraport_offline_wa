Ext.define('AssetManagement.base.view.dialogs.BaseOperationDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.model.dialogmodel.OperationDialogViewModel',
        'AssetManagement.customer.controller.dialogs.OperationDialogController',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.Date',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

   inheritableStatics: {
    	_instance: null,
    	defaultTimeUnit: 'H', //TODO: get default timeunit from FuncPara
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.dialogs.OperationDialog');
                }            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseOperationDialog', ex);
		    }
            
            return this._instance;
        }
    },
    
    viewModel: {
        type: 'OperationDialogViewModel'
    },
    
    controller: 'OperationDialogController',
    width: 550,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
	        var items = [
		        {
		            xtype: 'component',
			        minWidth: 10,
			        maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    text: Locale.getMsg('createOperation')
		                },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'oxtextfield',
		                    id: 'orderNewOperDialogShortTextField',
		                    fieldLabel: Locale.getMsg('shorttext'),
		                    maxLength: 40
		                },
		                {
		                    xtype: 'component',
		                    height: 5
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'datefield',
		                            flex: 1,
		                            id: 'orderNewOperDialogStartDateField',
		                            format: 'd.m.Y',
		                            fieldLabel: Locale.getMsg('startDate')
		                        },
		                        {
		                            xtype: 'component',
		                            width: 10
		                        },
		                        {
		                            xtype: 'datefield',
		                            flex: 1,
		                            id: 'orderNewOperDialogEndDateField',
		                            format: 'd.m.Y',
		                            fieldLabel: Locale.getMsg('endDate')
		                        },
		                        {
		                            xtype: 'component',
		                            width: 5
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 5
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            //start time field
		                            xtype: 'timefield',
		                            fieldLabel: Locale.getMsg('worktime'),
		                            format: 'H:i',
		                            flex: 1, 
		                            id: 'orderNewOperDialogWorkTimeTextField'
		                        },
		                        {
		                            xtype: 'component',
		                            width: 10
		                        },
		                        {
		                            xtype: 'oxcombobox',
		                            flex: 1,
		                            id: 'orderNewOperDialogWorkPlaceComboField',
		                            editable: false,
		                            fieldLabel: Locale.getMsg('workcenter'),
			                        displayField: 'text',
			                       	queryMode: 'local',
			                       	valueField: 'value'
		                        },
		                        {
		                            xtype: 'component',
		                            width: 5
		                        }
		                    ]
		                },
		                {
		                    xtype: 'container',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('save'),
		                            listeners: {
		                        		click: 'save'
		                        	}
		                        
		                        },
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton',
		                            text: Locale.getMsg('cancel'), 
		                            listeners: {
		                        		click: 'cancel'
		                        	}
		                        
		                        }
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
			        minWidth: 10,
			        maxWidth: 10
		        }
		    ];
	        
	        this.add(items);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseOperationDialog', ex);
		}
    },
   
   	//protected
	//@override
    getDialogTitle: function() {
	    var retval = '';
	    
	    try {
	        retval = Locale.getMsg('createOperation');
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseOperationDialog', ex);
		}
	    
		return retval;
	},
    
	//protected
	//@override
    updateDialogContent: function() {
		try {
			this.resetFields(); 
		
			var order = this.getViewModel().get('order');
			var oper = this.getViewModel().get('operation'); 
			
			// if edit mode -> oper is filled
			if(oper)
				this.fillFieldsWithOperData(oper); 
			else
				this.fillWorkcenterComboBox(order.get('vaplz')); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseOperationDialog', ex);
		}
	},
	
	resetFields: function() {
		try {
			//reset all fields when calling dialog 
			Ext.getCmp('orderNewOperDialogShortTextField').setValue(''); 
			Ext.getCmp('orderNewOperDialogWorkTimeTextField').setValue('1'); 
			Ext.getCmp('orderNewOperDialogWorkPlaceComboField').setValue(''); 
			Ext.getCmp('orderNewOperDialogStartDateField').setValue(new Date()); 
			Ext.getCmp('orderNewOperDialogEndDateField').setValue(new Date()); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetFields of BaseOperationDialog', ex)
		}
	},

	fillFieldsWithOperData: function(operation) {
		try {
			Ext.getCmp('orderNewOperDialogShortTextField').setValue(operation.get('ltxa1')); 
			//transform time for picker
			var time = operation.get('arbei').split('.');
			var hours = AssetManagement.customer.utils.StringUtils.padLeft(time[0].trim(), '0', 2);
			var minutes = time.length === 2 ? AssetManagement.customer.utils.StringUtils.padLeft(((parseInt(time[1])/100)*60).toString(), '0', 2) : '00';
			Ext.getCmp('orderNewOperDialogWorkTimeTextField').setValue(hours + ':' + minutes); 
			
			this.fillWorkcenterComboBox(operation.get('arbpl'));
			//Ext.getCmp('orderNewOperDialogWorkPlaceComboField').setValue(operation.get('arbpl')); 
			
			Ext.getCmp('orderNewOperDialogStartDateField').setValue(operation.get('fsav')); 
			Ext.getCmp('orderNewOperDialogEndDateField').setValue(operation.get('fsed')); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillFieldsWithOperData of BaseOperationDialog', ex);
		}
	}, 
	
	//fills workcenter comboBox
	fillWorkcenterComboBox: function(arbpl) {
		try {
			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			var curWorkcenter = null;
			var seachForMatch = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(arbpl);
		
			var workCenters = this.getViewModel().get('workCenters');
			if(workCenters && workCenters.getCount() > 0) {
				workCenters.each(function(workCenter) {
					var curArbpl =  workCenter.get('arbpl');
				
					store.add({ 'text': curArbpl + ' ' + workCenter.get('ktext'), 'value': workCenter });
					
					if(seachForMatch && curArbpl === arbpl)
						curWorkcenter = workCenter;
				}, this);
				
				if(!curWorkcenter)
					curWorkcenter = workCenters.getAt(0);
			}
				
			
			var comboBox = Ext.getCmp('orderNewOperDialogWorkPlaceComboField');

			comboBox.clearValue();
			comboBox.bindStore(store);
			
			if(curWorkcenter) {
				comboBox.setValue(comboBox.findRecordByValue(curWorkcenter));
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillWorkcenterComboBox of BaseOperationDialog', ex)
		}
	},
	
	//get values from form
	getOperationValues: function() {
		try{
			var myViewModel = this.getViewModel();
			var opToSave = myViewModel.get('operation');
			var order = myViewModel.get('order');

			if(!opToSave) {
				opToSave = Ext.create('AssetManagement.customer.model.bo.Operation', { order: this.getViewModel().get('order') });
			}
	
			var hours = Ext.getCmp('orderNewOperDialogWorkTimeTextField').getValue().getHours(); 
			var minutes = (Ext.getCmp('orderNewOperDialogWorkTimeTextField').getValue().getMinutes()/60)*100;
			var time = hours + '.' + minutes; 
	
			opToSave.set('fsav', Ext.getCmp('orderNewOperDialogStartDateField').getValue()); 
			opToSave.set('fsed', Ext.getCmp('orderNewOperDialogEndDateField').getValue()); 
			opToSave.set('daune', this.self.defaultTimeUnit);
			opToSave.set('dauno', time);
			opToSave.set('arbeh', this.self.defaultTimeUnit);
			opToSave.set('arbei', time);
			
			if(Ext.getCmp('orderNewOperDialogWorkPlaceComboField').getValue()) {
				opToSave.set('werks', Ext.getCmp('orderNewOperDialogWorkPlaceComboField').getValue().get('werks')); 
				opToSave.set('arbpl', Ext.getCmp('orderNewOperDialogWorkPlaceComboField').getValue().get('arbpl')); 
			}

			opToSave.set('mobileKey', order ? order.get('mobileKey') : '')
			opToSave.set('ltxa1', Ext.getCmp('orderNewOperDialogShortTextField').getValue()); 
			
			this.getViewModel().set('operation', opToSave); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOperationValues of BaseOperationDialog', ex);
		}
	}
});