﻿Ext.define('AssetManagement.base.view.mixins.BaseOrderDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //operGridPanel Renderers
    operActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/operation.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside operActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    operOperationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
                var vornr = '';
                var ktext = '';

                if (isSplit) {
                    vornr = record.get('vornr') + " / " + record.get('split');
                    ktext = record.get('stext');
                } else {
                    vornr = record.get('vornr');
                    ktext = record.get('ltxa1');
                }
                return vornr + '<p>' + ktext + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside operOperationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },

    operWorkCenterColumnRenderer: function (owner) {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {

                var arbpl = record.get('arbpl');
                var werks = record.get('werks');


                return arbpl + ' / ' + werks;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside operMaterialColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },

    operSerialNumberColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var owner = this.owner;
                var order = owner.getViewModel().get('order');
                var objListItem = AssetManagement.customer.manager.ObjectListManager.getObjectListItemForOperation(order, record);
                var sernr = '';

                if (objListItem !== null && objListItem.get('sernr') !== null && objListItem.get('sernr') !== 'undefined') {
                    sernr = AssetManagement.customer.utils.StringUtils.trimStart(objListItem.get('sernr'), '0');
                }

                return sernr;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside operSerialNumberColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    operDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
                var startDate = '';
                var endDate = '';

                if (isSplit) {
                    startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fstadt'));
                    endDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fenddt'));
                } else {
                    startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fsav'));
                    endDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fsed'));
                }
                return startDate + record.getRelevantTimeZone() + '<p>' + endDate + record.getRelevantTimeZone() + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside operDateColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    operConfirmationsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var plannedDoneString = record.getPlannedDoneString()

                var splitString = plannedDoneString.split("/");

                return  splitString[0] +'<p>'+ splitString[1] + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside operConfirmationsColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },

    //end of operGridPanel Renderers

    //dispOperGridPanel Renderers
    dispOperActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/operation.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dispOperActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    dispOperOperationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
                var vornr = '';
                var ktext = '';

                if (isSplit) {
                    vornr = record.get('vornr') + " / " + record.get('split');
                    ktext = record.get('stext');
                } else {
                    vornr = record.get('vornr');
                    ktext = record.get('ltxa1');
                }
                return vornr + '<p>' + ktext + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dispOperOperationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    dispOperWorkcenterColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('werks') + ' / ' + record.get('arbpl');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dispOperWorkcenterColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    dispOperPlannedDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
                var startDate = '';
                var endDate = '';

                if (isSplit) {
                    startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fstadt'));
                    endDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fenddt'));
                } else {
                    startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fsav'));
                    endDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('fsed'));
                }
                return startDate + record.getRelevanTimeZone() + '<p>' + endDate + record.getRelevanTimeZone() + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dispOperPlannedDateColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    dispOperTechnicianColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('pernr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dispOperTechnicianColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end of dispOperGridPanel Renderers

    //objectGridPanel Renderers
    objectActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return '<img src="resources/icons/object_list.png"/>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },

    objectEquiColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var equnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
                var eqtxt = record.get('eqtxt');

                return equnr + '<p>' + eqtxt;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectEquiColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },

    objectFuncLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tplnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('tplnr'), '0');
                var tpltxt = record.get('pltxt');

                return tplnr + '<p>' + tpltxt;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectFuncLocColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    objectMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0') + '<p>' + record.get('maktx');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectMaterialColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    objectNotificationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('ihnum'), '0') + '<p>' + record.get('qmtxt');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectNotificationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    objectSerialNumberColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('sernr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectSerialNumberColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    objectChecklistColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';

                try {
                    var qplos = record.get('qplos');

                    if (qplos) {
                        var icon = (qplos.isCompleted()) ? 'documentOk.png' : 'document.png';
                        retval = '<img class="oxGridLineActionButton" src="resources/icons/' + icon + '"/>';
                    } else if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('use_qplan') === 'X') {
                        var matnr = record.get('matnr');
                        if (!matnr) {
                            var techObj = record.get('equipment');

                            if (techObj) {
                                matnr = techObj.get('matnr')
                                matnr = matnr ? matnr : techObj.get('submt') //if objListItem equipment doesn't have matnr, maybe the equipment has submt
                            } else {
                                techObj = record.get('funcLoc');
                                matnr = techObj ? techObj.get('submt') : '';
                            }
                        }

                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
                            var icon = 'documentAdd.png';
                            retval = '<img class="oxGridLineActionButton" src="resources/icons/' + icon + '"/>';
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checklistColumn', ex);
                }
                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside objectChecklistColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end of objectGridPanel Renderers

    //orderTimeConfPanel Renderers
    orderTimeConfActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/timeconf.png';
                return '<img src="' + imageSrc + '"  style="height:30px;width:30px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderTimeConfActivityTypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';

                try {
                    var learr = '';
                    var learrTxt = '';

                    if (record.get('activityType')) {
                        learr = record.get('activityType').get('lstar');
                        learrTxt = record.get('activityType').get('ktext');
                    } else {
                        learr = record.get('learr');
                    }

                    retval = AssetManagement.customer.utils.StringUtils.concatenate([learr, learrTxt], '-', true);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfActivityTypeColumnRenderer in BaseOrderDetailPageRendererMixin', ex);
                }

                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfActivityTypeColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderTimeConfAccountIndicationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';
                try {
                    var accountIndication = record.get('accountIndication');

                    retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfAccountIndicationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
                }

                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfAccountIndicationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderTimeConfDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('ied')) + record.getRelevantTimeZone();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfDateColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderTimeConfWorktimeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getWorkValueForDisplay();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfWorktimeColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderTimeConfFinalConfirmationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var aueru = record.get('aueru');

                if (aueru === 'X')
                    return '[END]';

                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderTimeConfFinalConfirmationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end of orderTimeConfPanel Renderers

    //orderDetailExtConfsItemsGridPanel Renderers
    orderDetailExtConfActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/timeconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDetailExtConfActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderDetailExtConfMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = record.get('matnr');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDetailExtConfMaterialColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderDetailExtConfDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bedat = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('bedat')) + record.getRelevantTimeZone();

                return bedat;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDetailExtConfDateColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderDetailExtConfQuantityUnitColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
                var unit = record.get('meins');

                return amount + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDetailExtConfQuantityUnitColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderDetailExtConfPriceCurrencyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var priceCurrency = record.get('preis') + ' ' + record.get('waers');

                return priceCurrency;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDetailExtConfPriceCurrencyColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end orderDetailExtConfsItemsGridPanel Renderers

    //componentGridPanelOrderDetail Renderers
    componentOrderDetailActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/settings.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentOrderDetailActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    componentOrderDetailMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = record.get('material');
                var maktx = material ? material.get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentOrderDetailMaterialColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    componentOrderDetailOrderComponentsQtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bdmng'));
                return amount + ' ' + record.get('meins');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentOrderDetailOrderComponentsQtyColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end componentGridPanelOrderDetail Renderers

    //orderMatConfPanel Renderers
    orderMatConfActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/matconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderMatConfActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderMatConfMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderMatConfMaterialColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderMatConfAccountIndicationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';
                try {
                    var accountIndication = record.get('accountIndication');
                    retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of matConfGrid/bemot-column in OrderDetailPage', ex);
                }
                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderMatConfAccountIndicationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderMatConfOrderComponentsQtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('quantity'));
                var unit = record.get('unit');

                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderMatConfOrderComponentsQtyColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderMatConfPlantStorLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var plant = record.get('plant');
                var storloc = record.get('storloc');

                return AssetManagement.customer.utils.StringUtils.concatenate([plant, storloc], '/');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderMatConfPlantStorLocColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end orderMatConfPanel Renderers

    //orderDetailPageSDOrderItemsGridPanel Renderers
    sdOrderActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    sdOrderPositionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bstnk = AssetManagement.customer.utils.StringUtils.trimStart(record.get('bstnk'), '0');
                var posnr = record.get('posnr');
                return bstnk + '<p>' + posnr + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderPositionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    sdOrderMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderMaterialColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    sdOrderQuantityUnitColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
                var unit = record.get('zieme');

                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderQuantityUnitColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    sdOrderDeliveryStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getDeliveryStatus();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDeliveryStatusColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    sdOrderRemarksColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('arktx');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderRemarksColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end orderDetailPageSDOrderItemsGridPanel Renderers

    //orderFileGridPanel Renderers
    orderFileActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/document.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderFileActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderFileNameDescColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = record.get('fileDescription');

                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderFileNameDescColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderFileTypeAndSizeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var type = record.get('fileType');

                var fileSize = record.get('fileSize') ? Number(record.get('fileSize')) : 0;
                var size = fileSize.toFixed(2) + ' kB';

                return type + '<p>' + size + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderFileTypeAndSizeColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    orderFileStorageLocationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stringToShow = "";
                var fileOrigin = record.get('fileOrigin');

                if (fileOrigin === "online") {
                    stringToShow = Locale.getMsg('sapSystem');
                } else if (fileOrigin === "local") {
                    stringToShow = Locale.getMsg('local');
                }

                return stringToShow;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderFileStorageLocationColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    //end orderFileGridPanel Renderers

    //partnerGridPanel Renderers
    partnerActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/partner.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerActionColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    partnerNameColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var nameLine1 = AssetManagement.customer.utils.StringUtils.concatenate([record.get('name1'), record.get('name2')], null, true);
                var nameLine2 = AssetManagement.customer.utils.StringUtils.concatenate([record.get('name3'), record.get('name4')], null, true);

                return nameLine1 + '<p>' + nameLine2 + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerNameColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    partnerContactColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tel = record.get('telnumber');
                var telExtens = record.get('telextens');
                var telMob = record.get('telnumbermob');

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tel)) {
                    tel = 'Tel.: ' + tel;

                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telExtens))
                        tel += ' ' + telExtens;
                }

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telMob)) {
                    telMob = 'Mobil: ' + telMob;
                }

                return tel + '<p>' + telMob + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerContactColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    partnerAddressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var street = '';
                var postalCode = '';

                street = AssetManagement.customer.utils.StringUtils.concatenate([record.get('street'), record.get('housenum1')], null, true);
                postalCode = AssetManagement.customer.utils.StringUtils.concatenate([record.get('postcode1'), record.get('city1')], null, true);


                return street + '<p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerAddressColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    },
    partnerMapsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('city1')) &&
					AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('postcode1'))) {
                    metaData.style = 'display: none;';
                } else {
                    metaData.style = '';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerAddressColumnRenderer of BaseOrderDetailPageRendererMixin', ex);
        }
    }


    //end partnerGridPanel Renderers
});