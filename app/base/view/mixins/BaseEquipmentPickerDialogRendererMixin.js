﻿Ext.define('AssetManagement.base.view.mixins.BaseEquipmentPickerDialogRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    equiPickerActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/equi.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiPickerActionColumnRenderer of BaseEquipmentPickerDialogRendererMixin', ex);
        }
    },
    equiPickerEquipmentColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var equnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
                var eqktx = record.get('eqktx');
                return equnr + '<p>' + eqktx + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiPickerEquipmentColumnRenderer of BaseEquipmentPickerDialogRendererMixin', ex);
        }
    },
    equiPickerSortFieldColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var eqfnr = record.get('eqfnr');
                return eqfnr;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiPickerSortFieldColumnRenderer of BaseEquipmentPickerDialogRendererMixin', ex);
        }
    },
    equiPickerMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('submt'), '0');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiPickerMaterialColumnRenderer of BaseEquipmentPickerDialogRendererMixin', ex);
        }
    },
    equiPickerLocationAndRoomColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stort = record.get('stort');
                var room = record.get('msgrp');
                return stort + '<p>' + room + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiPickerLocationAndRoomColumnRenderer of BaseEquipmentPickerDialogRendererMixin', ex);
        }
    }
})