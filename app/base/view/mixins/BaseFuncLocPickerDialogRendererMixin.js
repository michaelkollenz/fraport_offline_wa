﻿Ext.define('AssetManagement.base.view.mixins.BaseFuncLocPickerDialogRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    funcLocPickerActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/funcloc.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';

            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocPickerActionColumnRenderer of BaseFuncLocPickerDialogRendererMixin', ex);
        }
    },
    funcLocPickerFuncLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tplnr = record.getDisplayIdentification();
                var pltxt = record.get('pltxt');
                return tplnr + '<p>' + pltxt + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocPickerFuncLocColumnRenderer of BaseFuncLocPickerDialogRendererMixin', ex);
        }
    },
    funcLocPickerLocationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stort = record.get('stort');
                var msgrp = record.get('msgrp');
                return stort + '<p>' + msgrp + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocPickerLocationColumnRenderer of BaseFuncLocPickerDialogRendererMixin', ex);
        }
    }
})