﻿Ext.define('AssetManagement.base.view.mixins.BaseWeListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

    //weList Renderers
    wePositionColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var position = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('ebelp'));

            return position;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseWeListPageRendererMixin', ex);
        }
    },

    weMaterialColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var matnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('matnr'));
            var mattxt = record.get('txz01');

            return matnr + '<p>' + mattxt + '</p>';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseWeListPageRendererMixin', ex);
        }
    },

    weMengeColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var menge = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('menge')) * 1; //convert to nuber, to have a 0 if empty
            var meins = record.get('meins')
            var menge_del = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('menge_del')) * 1; //convert to nuber, to have a 0 if empty
            return menge.toLocaleString() + '<p>' + menge_del.toLocaleString() + ' ' + meins + '</p>';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseWeListPageRendererMixin', ex);
        }
    },
    weMengeWidgetAttached: function (column, widget, record) {
        try {
            var menge_ist = record.get('menge_ist');
            var toDeliver = record.get('menge') * 1; //*1 is an int conversion

            var alreadyDelivered = (record.get('menge_del') ? record.get('menge_del') : 0) * 1;

            widget.setMaxValue(toDeliver - alreadyDelivered);

            widget.setValue(menge_ist.toLocaleString()) //unless there already is a default value on that list (in back navigation)

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of weMengeWidgetAttached', ex);
        }
    }
});