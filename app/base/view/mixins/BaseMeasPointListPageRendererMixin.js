﻿Ext.define('AssetManagement.base.view.mixins.BaseMeasPointListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //measPointGridPanel Renderers
    measPointActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/measpoints.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside measPointActionColumnRenderer of BaseMeasPointListPageRendererMixin', ex);
        }
    },
    measPointMeasPointColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');
                var measpointDesc = record.get('pttxt');
                return measpoint + '<p>' + measpointDesc + '</p>' + '<br /><br />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside measPointMeasPointColumnRenderer of BaseMeasPointListPageRendererMixin', ex);
        }
    },
    measPointTechObjectColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var objNo = '';
                var objTxt = '';
                if (record.get('equipment')) {
                    objNo = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equipment').get('equnr'), '0');
                    objTxt = record.get('equipment').get('eqktx');
                }
                else if (record.get('funcLoc')) {
                    objNo = record.get('funcLoc').getDisplayIdentification();
                    objTxt = record.get('funcLoc').get('pltxt');
                }

                return objNo + '<p>' + objTxt + '</p>' + '<br /><br />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside measPointTechObjectColumnRenderer of BaseMeasPointListPageRendererMixin', ex);
        }
    },
    measPointMeasIndicatorColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('psort');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside measPointMeasIndicatorColumnRenderer of BaseMeasPointListPageRendererMixin', ex);
        }
    },
    measPointLastMeasValueColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('lastRec') + ' ' + record.get('lastUnit');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside measPointLastMeasValueColumnRenderer of BaseMeasPointListPageRendererMixin', ex);
        }
    }
    // End of the measPointGridPanel Renderers
})