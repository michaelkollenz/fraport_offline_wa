﻿Ext.define('AssetManagement.base.view.mixins.BaseNotifItemDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //notifItemCauseGrid Renderers
    notifItemCauseActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_cause.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemCauseActionColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    },
    notifItemCauseCauseColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('causeCustCode')) {
                    var causekGrp = record.get('causeCustCode').get('codegrkurztext');
                    var causeCode = record.get('causeCustCode').get('kurztext');

                    return causekGrp + '<p>' + causeCode + '</p>';
                }

                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemCauseCauseColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    },
    notifItemCauseDescriptionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var causeTxt = record.get('urtxt');
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(causeTxt))
                    causeTxt = '';

                return causeTxt;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemCauseDescriptionColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    },
    // End of the notifItemCauseGrid Renderers

    // Begin of the notifItemActivityGrid Renderers
    notifItemActivityActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_activity.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActivityColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    },
    notifItemActivityNotifActivityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('activityCustCode')) {
                    var activityGrp = record.get('activityCustCode').get('codegrkurztext');
                    var activityCode = record.get('activityCustCode').get('kurztext');

                    return activityGrp + '<p>' + activityCode + '</p>';
                }
                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActivityNotifActivityColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    },
    // End of the notifItemActivityGrid Renderers

    // Begin of the notifItemGrid Renderers
    notifItemActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_task.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActionColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    },
    notifItemNotifTaskColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('taskCustCode')) {
                    var taskGrp = record.get('taskCustCode').get('codegrkurztext');
                    var taskCode = record.get('taskCustCode').get('kurztext');

                    return taskGrp + '<p>' + taskCode + '</p>';
                }

                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemNotifTaskColumnRenderer of BaseNotifItemDetailPageRendererMixin', ex);
        }
    }
    // End of the notifItemGrid Renderers
})