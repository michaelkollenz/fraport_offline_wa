﻿Ext.define('AssetManagement.base.view.mixins.BaseMatListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

    //weList Renderers
    weMaterialColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var matnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('matnr'));
            var mattxt = record.get('maktx');

            return matnr + '<p>' + mattxt + '</p>';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseMatListPageRendererMixin', ex);
        }
    },

    weMengeColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var menge = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('verme')) * 1; //convert to nuber, to have a 0 if empty
            var meins = record.get('meins') ;
            return menge.toLocaleString() + ' ' + meins;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseMatListPageRendererMixin', ex);
        }
    },

  weStorLocColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
    try {
      var lgort = record.get('lgort') ;

      return lgort;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseMatListPageRendererMixin', ex);
    }
  },

  weStorPlaColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
    try {
      var lgpbe = record.get('lgpbe') ;
      return lgpbe;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseMatListPageRendererMixin', ex);
    }
  },

    weMengeWidgetAttached: function (column, widget, record) {
        try {
            var menge_ist = record.get('menge_ist');

            widget.setValue(menge_ist) //unless there already is a default value on that list (in back navigation)
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of weMengeWidgetAttached', ex);
        }
    }
});