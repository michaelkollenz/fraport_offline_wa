﻿Ext.define('AssetManagement.base.view.mixins.BaseSDOrderDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //sdOrderItemsSDOrderDetailPageGridPanel Renderers
    sdOrderItemsActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDetailPageItemsActionColumnRenderer of BaseSDOrderDetailPageRendererMixin', ex);
        }
    },

    sdOrderItemsPosTextColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('posnr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDetailPageItemsPositionColumnRenderer of BaseSDOrderDetailPageRendererMixin', ex);
        }
    },

    sdOrderItemsMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDetailPageItemsMaterialColumnRenderer of BaseSDOrderDetailPageRendererMixin', ex);
        }
    },

    sdOrderItemsQuantityUnitColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
                var unit = record.get('zieme');

                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDetailPageItemsQuantityUnitColumnRenderer of BaseSDOrderDetailPageRendererMixin', ex);
        }
    },

    sdOrderDetailPageItemsDeliveryStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getDeliveryStatus();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDetailPageItemsDeliveryStatusColumnRenderer of BaseSDOrderDetailPageRendererMixin', ex);
        }
    },

    sdOrderDetailPageItemsRemarksColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('arktx');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDetailPageItemsRemarksColumnRenderer of BaseSDOrderDetailPageRendererMixin', ex);
        }
    }
    // End of the sdOrderItemsSDOrderDetailPageGridPanel Renderers
})