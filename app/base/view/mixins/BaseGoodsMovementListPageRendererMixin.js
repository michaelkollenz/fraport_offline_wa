﻿Ext.define('AssetManagement.base.view.mixins.BaseGoodsMovementListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

    //GoodsMovementList Renderers
    goodsMovementPositionColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var position = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('ebelp'));

            return position;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseGoodsMovementListPageRendererMixin', ex);
        }
    },

    goodsMovementMaterialColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var matnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('matnr'));
            var mattxt = record.get('item_text');

            return matnr + '<p>' + mattxt + '</p>';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseGoodsMovementListPageRendererMixin', ex);
        }
    },

    storeLocationOnWidgetAttached: function (column, widget, record) {
        try {
            var view = widget.owner;
            var lgortStore = view.getViewModel().get('lgortStore');
            var comboboxStore = Ext.create('Ext.data.Store', {
                fields: ['lgort']
            });

            var defaultLgort = record.get('lgort');

            if (lgortStore && lgortStore.getCount() > 0) {
                lgortStore.each(function (lgortRecord) {
                    var comboRecord = Ext.create('Ext.data.Model', { lgort: lgortRecord.get('lgort') });
                    comboboxStore.add(comboRecord);
                });
            }
            widget.setStore(comboboxStore);

            if (defaultLgort) {
                widget.select(defaultLgort);
            }
            myController = view.getController();
            widget.addListener('select', myController.onLgortSelected,myController, {record: record});

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of storeLocationOnWidgetAttached', ex);
        }

    },
    mengeOnWidgetAttached: function (column, widget, record) {
        try {
            widget.setValue(record.get('menge'));
            widget.setDisabled(true); //it is an always disabled field, read-only
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of mengeOnWidgetAttached', ex);
        }
    },



  weMengeDelColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
    try {
      var menge = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('menge')) * 1; //convert to nuber, to have a 0 if empty
      var meins = record.get('meins') ;
      return menge.toLocaleString() + ' ' + meins;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseMatListPageRendererMixin', ex);
    }
  }


});