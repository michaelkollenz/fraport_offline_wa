﻿Ext.define('AssetManagement.base.view.mixins.BaseBanfEditPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //banfGridPanel renderers
    banfGridActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {

                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside banfGridActionColumnRenderer of BaseBanfEditPageRendererMixin', ex);
        }
    },
    banfGridMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = record.get('material');
                var maktx = material ? material.get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside banfGridMaterialColumnRenderer of BaseBanfEditPageRendererMixin', ex);
        }
    },
    banfGridComponentsQuantityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
                var unit = record.get('meins');

                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside banfGridComponentsQuantityColumnRenderer of BaseBanfEditPageRendererMixin', ex);
        }
    },
    banfGridConfirmedQuantityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bsmng'));
                return quantity;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside banfGridConfirmedQuantityColumnRenderer of BaseBanfEditPageRendererMixin', ex);
        }
    }
})