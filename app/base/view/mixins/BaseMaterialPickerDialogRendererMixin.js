﻿Ext.define('AssetManagement.base.view.mixins.BaseMaterialPickerDialogRendererMixin', {
  extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

  requires: [
    'AssetManagement.customer.manager.MaterialManager'
  ],

  // ###############################################################################################################
  // #################################### allMaterialsGridPanel Renderers ##########################################
  // ###############################################################################################################

  allMaterialsActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        return '<img src="resources/icons/matconf.png"  />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside allMaterialsActionColumnRenderer of MaterialPickerDialogRendererMixin', ex);
    }
  },

  allMaterialsMatColumnRenderer: function () {
    try {
      return function (value, metaData, record, rowIndex, colIndex, store, view) {
        return AssetManagement.customer.manager.MaterialManager.trimMaterialNumber(value, '0');
      };
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside allMaterialsMatColumnRenderer of MaterialPickerDialogRendererMixin', ex);
    }
  },

  allMaterialsShorttextColumnRenderer: function () {
    try {
      return function (value, metaData, record, rowIndex, colIndex, store, view) {
        return record.get('maktx') || '';
      };
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside allMaterialsOrderComponents_QtyColumnRenderer of MaterialPickerDialogRendererMixin', ex);
    }
  },

  // ###############################################################################################################
  // ################################################## END ########################################################
  // ###############################################################################################################

  //plannedMaterialGridPanel Renderers
  plannedMaterialActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/settings.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside plannedMaterialActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  plannedMaterialMatColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var material = record.get('material');
        var maktx = material ? material.get('maktx') : '';

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside plannedMaterialMatColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  plannedMaterialOrderComponents_QtyColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bdmng'));
        return amount + ' ' + record.get('meins');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside plannedMaterialOrderComponents_QtyColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  plannedMaterialOperationColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        return record.get('vornr');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside plannedMaterialMatColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  //plannedMaterialGridPanel Renderers
  //stockListGridPanel Renderers
  stockListActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/matconf.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stockListActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  stockListMaterialColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var material = record.get('material');
        var maktx = material ? material.get('maktx') : '';

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stockListMaterialColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  stockListPositionNumberAndTypeColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var posnr = record.get('posnr');
        var postype = record.get('postp');

        return AssetManagement.customer.utils.StringUtils.concatenate([posnr, postype], ' / ', false, true, '-');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stockListPositionNumberAndTypeColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  stockListRequiredQuantColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
        var unit = record.get('meins');

        return AssetManagement.customer.utils.StringUtils.concatenate([quantity, unit], ' ', true);
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stockListRequiredQuantColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  //stockListGridPanel Renderers
  //unplannedMaterialGridPanel Renderers
  unplannedMaterialActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/matconf.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedMaterialActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  unplannedMaterialMaterialColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var material = record.get('material');
        var maktx = material ? material.get('maktx') : '';

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedMaterialMaterialColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  unplannedMaterialOrderComponents_QtyColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
        var unit = record.get('meins');

        return quant + ' ' + unit;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedMaterialOrderComponents_QtyColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  unplannedMaterialPlantStorLocColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var plant = record.get('werks');
        var storloc = record.get('lgort');

        return AssetManagement.customer.utils.StringUtils.concatenate([plant, storloc], '/');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedMaterialActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  //unplannedMaterialGridPanel Renderers
  //matStockCustConsGridPanel Renderers
  matStockCustConsActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/settings.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockCustConsActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockCustConsMaterialColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var material = record.get('material');
        var maktx = material ? material.get('maktx') : '';

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockCustConsMaterialColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockCustConsOrderComponents_QtyColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('kulab'));
        return amount + ' ' + record.get('meins');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockCustConsOrderComponents_QtyColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  //matStockCustConsGridPanel Renderers
  //matStockOthersGridPanel Renderers
  matStockOthersActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/matconf.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOthersActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockOthersMaterialColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var material = record.get('material');
        var maktx = material ? material.get('maktx') : '';

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOthersActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockOthersOrderComponents_QtyColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
        var unit = record.get('meins');

        return quant + ' ' + unit;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOthersOrderComponents_QtyColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockOthersPlantStorLocColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var plant = record.get('werks');
        var storloc = record.get('lgort');

        return AssetManagement.customer.utils.StringUtils.concatenate([plant, storloc], '/');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOthersPlantStorLocColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  //matStockOthersGridPanel Renderers
  //matStockOwnGridPanel Renderers
  matStockOwnActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/matconf.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOwnActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockOwnMaterialColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var material = record.get('material');
        var maktx = material ? material.get('maktx') : '';

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOwnMaterialColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockOwnOrderComponents_QtyColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
        var unit = record.get('meins');

        return quant + ' ' + unit;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOwnOrderComponents_QtyColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  matStockOwnPlantStorLocColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var plant = record.get('werks');
        var storloc = record.get('lgort');

        return AssetManagement.customer.utils.StringUtils.concatenate([plant, storloc], '/');
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOwnPlantStorLocColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  //matStockOwnGridPanel Renderers

  //online material grid panel
  //unplannedMaterialGridPanel Renderers
  onlineMaterialActionColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/matconf.png';
        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlineMaterialActionColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  },
  onlineMaterialMaterialColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var maktx = record.get('maktx');

        return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlineMaterialMaterialColumnRenderer of BaseMaterialPickerDialogRendererMixin', ex);
    }
  }
  ///
})