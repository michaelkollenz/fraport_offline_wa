﻿Ext.define('AssetManagement.base.view.mixins.BaseCustomerListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

    requires: [
        'AssetManagement.customer.helper.AddressHelper'
    ],

    //customerList Renderers
    customerActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/partner.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerActionColumnRenderer of BaseCustomerListPageRendererMixin', ex);
        }
    },

    customerCustomerColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var customerGroup = record.get('kdgrp');
                var customerNo = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('kunnr'));

                return customerGroup + '<p>' + customerNo + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerCustomerColumnRenderer of BaseCustomerListPageRendererMixin', ex);
        }
    },

    customerNameAndCityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var name = record.getName();
                var city = AssetManagement.customer.utils.StringUtils.concatenate([record.get('postlcod1'), record.get('city')], ' ', true);
                return name + '<p>' + city + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerNameAndCityColumnRenderer of BaseCustomerListPageRendererMixin', ex);
        }
    },

    customerNameAndNumberColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var customerName = record.getName();
                var customerNo = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('kunnr'));
	                                                                                                                                       
                return customerName + '<p>' + customerNo + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerNameAndNumberColumnRenderer of BaseCustomerListPageRendererMixin', ex);
        }
    },

    customerNameColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var customerFirstName = record.get('firstname');
                var customerLastName = record.get('lastname');

                return customerFirstName + '<p>' + customerLastName + '</p>';
            };

            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerNameColumnRenderer of BaseCustomerListPageRendererMixin', ex);
        }
    },

    customerAdressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var steet = record.get('street');
                var city = record.get('city')

                return steet + '<p>' + city + '</p>';
            };

            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerAdressColumnRenderer of BaseCustomerListPageRendererMixin', ex);
        }
    }
})