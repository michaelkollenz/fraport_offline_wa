﻿Ext.define('AssetManagement.base.view.mixins.BaseNewSDOrderPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //sdOrderItemsPanelNewSDOrderPage Renderers
    sdOrderItemsActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderItemsActionColumnRenderer of BaseNewSDOrderPageRendererMixin', ex);
        }
    },
    sdOrderItemsMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('matnr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderItemsMaterialColumnRenderer of BaseNewSDOrderPageRendererMixin', ex);
        }
    },
    sdOrderItemsQuantityUnitColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
                return amount + ' ' + record.get('zieme');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderItemsQuantityUnitColumnRenderer of BaseNewSDOrderPageRendererMixin', ex);
        }
    },
    sdOrderItemsPosTextColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('arktx');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderItemsPosTextColumnRenderer of BaseNewSDOrderPageRendererMixin', ex);
        }
    }
    // End of the sdOrderItemsPanelNewSDOrderPage Renderers
})