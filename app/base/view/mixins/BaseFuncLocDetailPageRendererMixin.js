﻿Ext.define('AssetManagement.base.view.mixins.BaseFuncLocDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //funcLocClassGridPannel Renderers
    funcLocClassActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/class_charact.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocClassActionColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocClassCharacteristicColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = null;

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('atbez')))
                    description = record.get('atbez');
                else
                    description = record.get('atnam');

                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocClassCharacteristicColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocClassCharacteristicValueColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                // record = Characts from FuncLocDetailPageController (Function: beforeDataReady) 
                var valueItem = '';
                var filteredCharValues = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.CharactValue',
                    autoLoad: false
                });

                var valueList = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });

                if (record.get('classValues')) {

                    record.get('classValues').each(function (classValue) {
                        if ((classValue.get('atinn') == record.get('atinn')) && !(classValue.get('updflag') == 'D'))
                            valueList.add(classValue);
                    });

                    if (valueList.getCount() > 0)
                        valueList.each(function (value) {
                            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value.get('atwrt'))) {

                                if (record.get('charactValues') && record.get('charactValues').getCount() > 0) {
                                    record.get('charactValues').each(function (charValue) {
                                        if (charValue.get('atwrt') == value.get('atwrt'))
                                            filteredCharValues.add(charValue);
                                    });

                                    if ((filteredCharValues.getCount() > 0) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(filteredCharValues.getAt(0).get('atwtb')))
                                        valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(filteredCharValues.getAt(0).get('atwtb'), record);
                                    else
                                        valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atwrt'), record);
                                }
                                else
                                    valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atwrt'), record);
                            }
                            else
                                valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atflv'), record);

                            valueItem += "\n";
                        });
                }
                return valueItem;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocClassCharacteristicValueColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    // End of FuncLocDetailPage Renderers

    // Begin of the funcLocMeasPointGrid Renderers
    funcLocMeasPointActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/measpoints.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocMeasPointActionColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocMeasPointMeasPointColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');

                var measpointDesc = record.get('pttxt');
                return measpoint + '<p>' + measpointDesc + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocMeasPointMeasPointColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocMeasPointMeasIndicatorColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('psort');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocMeasPointMeasIndicatorColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocMeasPointLastMeasValueColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('lastRec') + ' ' + record.get('lastUnit');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocMeasPointLastMeasValueColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    // End of the funcLocMeasPointGrid Renderers

    // Begin of the funcLocFileGridPannel Renderers
    funcLocFileActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/document.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocFileActionColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocFileFileColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = record.get('fileDescription');

                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocFileFileColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocFileTypeAndSizeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var type = record.get('fileType');
                var fileSize = record.get('fileSize') ? Number(record.get('fileSize')) : 0;
                var size = fileSize.toFixed(2) + ' kB';

                return type + '<p>' + size + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocFileTypeAndSizeColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocFileFileStorageLocationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stringToShow = "";
                var fileOrigin = record.get('fileOrigin');

                if (fileOrigin === "online") {
                    stringToShow = Locale.getMsg('sapSystem');
                } else if (fileOrigin === "local") {
                    stringToShow = Locale.getMsg('local');
                }

                return stringToShow;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocFileFileStorageLocationColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    // End of the funcLocFileGridPannel Renderers

    // Begin of the funcLocHistNotifGrid Renderers
    funcLocHistNotifActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistNotifActionColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocHistNotifNotifColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var qmnum = AssetManagement.customer.utils.StringUtils.trimStart(record.get('qmnum'), '0');
                var qmtxt = record.get('qmtxt');

                return qmnum + '<p>' + qmtxt + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistNotifNotifColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocHistNotifStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var status = record.get('system_status');

                return status;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistNotifStatusColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocHistNotifNotifTypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var qmart = record.get('qmart');

                return qmart;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistNotifNotifTypeColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    // End of the funcLocHistNotifGrid Renderers

    // Begin of the funcLocHistOrderGrid Renderers
    funcLocHistOrderActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/order.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistOrderActionColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocHistOrderOrderColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('aufnr'), '0');
                var ktext = record.get('ktext');
                return aufnr + '<p>' + ktext + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistOrderOrderColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocHistOrderStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var status = record.get('system_status');

                return status;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistOrderStatusColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    },
    funcLocHistOrderOrderTypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var auart = record.get('auart');
                return auart;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocHistOrderOrderTypeColumnRenderer of BaseFuncLocDetailPageRendererMixin', ex);
        }
    }
    // End of the funcLocHistOrderGrid Renderers
})