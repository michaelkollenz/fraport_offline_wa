﻿Ext.define('AssetManagement.base.view.mixins.BaseExtConfPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //ExtConfPage Renderers
    confActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/timeconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confActionColumnRenderer of BaseExtConfPageRendererMixin', ex);
        }
    },
    confMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = record.get('matnr');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confMaterialColumnRenderer of BaseExtConfPageRendererMixin', ex);
        }
    },
    confMaterialDescriptionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = '';
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('txz01'))) {
                    description = record.get('txz01');
                } else if (record.get('material')) {
                    description = record.get('material').get('maktx');
                }

                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confMaterialDescriptionColumnRenderer of BaseExtConfPageRendererMixin', ex);
        }
    },
    confDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bedat = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(record.get('bedat'));

                return bedat;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confDateColumnRenderer of BaseExtConfPageRendererMixin', ex);
        }
    },
    confQuantityUnitColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var menge = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
                var meins = record.get('meins');

                return menge + ' ' + meins;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confQuantityUnitColumnRenderer of BaseExtConfPageRendererMixin', ex);
        }
    }
    
})