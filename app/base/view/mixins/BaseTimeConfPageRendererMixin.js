﻿Ext.define('AssetManagement.base.view.mixins.BaseTimeConfPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //timeConfGrid Renderers
    timeConfColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/timeconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside timeConfColumnRenderer of BaseTimeConfPageRendererMixin', ex);
        }
    },
    timeConfActivityTypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';

                try {
                    var learr = '';
                    var learrTxt = '';

                    var activityType = record.get('activityType');

                    if (activityType) {
                        learr = activityType.get('lstar');
                        learrTxt = activityType.get('ktext');
                    } else {
                        learr = record.get('learr');
                    }

                    retval = AssetManagement.customer.utils.StringUtils.concatenate([learr, learrTxt], '-', true);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfActivityTypeColumnRenderer/learr-column in TimeConfPage', ex);
                }

                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside timeConfActivityTypeColumnRenderer of BaseTimeConfPageRendererMixin', ex);
        }
    },
    timeConfAccountIndicationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bemot = '';
                var bemottxt = '';

                var accountIndication = record.get('accountIndication');
                if(accountIndication) {
                    bemot = accountIndication.get('bemot') ? accountIndication.get('bemot') : '';

                    bemottxt = accountIndication.get('bemottxt') ? accountIndication.get('bemottxt') : '';
                }


                return bemot + ' ' + bemottxt;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocClassCharacteristicValueColumnRenderer of BaseTimeConfPageRendererMixin', ex);
        }
    },
    timeConfWorktimeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getWorkValueForDisplay();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside timeConfWorktimeColumnRenderer of BaseTimeConfPageRendererMixin', ex);
        }
    },
    timeConfSpecificationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('ltxa1');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside timeConfSpecificationColumnRenderer of BaseTimeConfPageRendererMixin', ex);
        }
    },
    timeConfFinalConfirmationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var aueru = record.get('aueru');

                if (aueru === 'X')
                    return '[END]';

                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside timeConfFinalConfirmationColumnRenderer of BaseTimeConfPageRendererMixin', ex);
        }
    }
    // End of the timeConfGrid Renderers
})