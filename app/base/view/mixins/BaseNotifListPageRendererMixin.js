﻿Ext.define('AssetManagement.base.view.mixins.BaseNotifListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //NotifListRenderers
    notifActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifActionColumnRenderer of BaseNotifListPageRendererMixin', ex);
        }
    },
    notifNotifColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var qmart = record.get('qmart');
                var qmnum = AssetManagement.customer.utils.StringUtils.trimStart(record.get('qmnum'), '0');
                var qmtxt = record.get('qmtxt');

                return qmart + ' ' + qmnum + '<p>' + qmtxt + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifNotifColumnRenderer of BaseNotifListPageRendererMixin', ex);
        }
    },
    notifDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {

                var time  = record.get('strdt');
                var resTime = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(time).split(" ");

                return  resTime[0] +'<p>'+ resTime[1] + " "+ record.getRelevantTimeZone() + '</p>';
                //return AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDateColumnRenderer of BaseNotifListPageRendererMixin', ex);
        }
    },
    techObjectColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var row1 = '';
                var row2 = '';
                var row3 = '';

                var useEqui = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('equnr'));

                if (useEqui === true) {
                    var equi = record.get('equipment');
                    if (equi) {
                        row1 = equi.get('eqktx');
                        row2 = equi.get('submt');
                        /*
                        if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(row2)) {
                            row2 = AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'));
                        }*/

                        row3 = AssetManagement.customer.utils.StringUtils.concatenate([equi.get('baujj'), equi.get('baumm')], '/', false, false, '-');
                    } else {
                        row1 = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
                    }
                } else {
                    var funcLoc = record.get('funcLoc');

                    if (funcLoc) {
                        row1 = funcLoc.get('pltxt');
                        row2 = funcLoc.getDisplayIdentification();
                        row3 = AssetManagement.customer.utils.StringUtils.concatenate([funcLoc.get('baujj'), funcLoc.get('baumm')], '/', false, false, '-');
                    } else {
                        row1 = record.get('tplnr');
                    }
                }

                return row1 + '<p>' + row2 + '</p><p>' + row3 + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside techObjectColumnRenderer of BaseNotifListPageRendererMixin', ex);
        }
    },
    notifAddressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var name = '';
                var street = '';
                var postalCode = '';

                var address = record.getObjectAddress();
                if (address !== null && address !== undefined) {
                    name = address.getName();
                    street = AssetManagement.customer.utils.StringUtils.concatenate([address.getStreet(), address.getHouseNo()], null, true);
                    postalCode = AssetManagement.customer.utils.StringUtils.concatenate([address.getPostalCode(), address.getCity()], null, true);
                } else {
                    name = '';
                    street = '';
                    postalCode = '';
                }

                return name + '<p>' + street + '</p><p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifAddressColumnRenderer of BaseNotifListPageRendererMixin', ex);
        }
    },
    notifMapsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (!record.getObjectAddress()) {
                    metaData.style = 'display: none;';
                } else {
                    metaData.style = '';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifMapsColumnRenderer of BaseNotifListPageRendererMixin', ex);
        }
    }
})