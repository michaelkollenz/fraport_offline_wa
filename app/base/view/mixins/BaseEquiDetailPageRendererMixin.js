﻿Ext.define('AssetManagement.base.view.mixins.BaseEquiDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //equiMeasPointGrid Renderers
    equiMeasPointActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/measpoints.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiDetailActionColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
    equiMeasPointMeasPointColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');
                var measpointDesc = record.get('pttxt');
                return measpoint + '<p>' + measpointDesc + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiMeasPointMeasPointColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
    equiMeasPointMeasIndicatorColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('psort');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiMeasPointMeasIndicatorColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
    equiMeasPointLastMeasValueColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('lastRec') + ' ' + record.get('lastUnit');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiMeasPointLastMeasValueColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
    //End of equiMeasPointGrid Renderers

    //Begin of equiFileGridPanel Renderers

     equiFileActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/document.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiFileActionColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
     equiFileFileColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = record.get('fileDescription');

                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiFileFileColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
     equiFileTypeAndSizeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var type = record.get('fileType');
                var fileSize = record.get('fileSize') ? Number(record.get('fileSize')) : 0;
                var size = fileSize.toFixed(2) + ' kB';

                return type + '<p>' + size + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiFileTypeAndSizeColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },
     equiFileFileOriginColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stringToShow = "";
                var fileOrigin = record.get('fileOrigin');

                if (fileOrigin === "online") {
                    stringToShow = Locale.getMsg('sapSystem');
                } else if (fileOrigin === "local") {
                    stringToShow = Locale.getMsg('local');
                }

                return stringToShow;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiFileFileOriginColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
     },
    //End of equiFileGridPanel Renderers

    //Begin of equiHistNotifGrid Renderers

     equiHistNotifActionColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var imageSrc = 'resources/icons/notif.png';
                 return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistNotifActionColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistNotifNotifColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var qmnum = AssetManagement.customer.utils.StringUtils.trimStart(record.get('qmnum'), '0');
                 var qmtxt = record.get('qmtxt');

                 return qmnum + '<p>' + qmtxt + '</p>';
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistNotifNotifColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistNotifStatusColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var status = record.get('system_status');

                 return status;
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistNotifStatusColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistNotifTypeColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var qmart = record.get('qmart');

                 return qmart;
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistNotifTypeColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
    //End of equiHistNotifGrid Renderers

    // Begin of equiHistOrderGrid Renderers
     equiHistOrderActionColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var imageSrc = 'resources/icons/order.png';
                 return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiDetailActionColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistOrderColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');
                 var measpointDesc = record.get('pttxt');
                 return measpoint + '<p>' + measpointDesc + '</p>';
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistOrderColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistOrderOrderColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 //store -> item -> aufnr, ktext
                 var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('aufnr'), '0');
                 var ktext = record.get('ktext');
                 return aufnr + '<p>' + ktext + '</p>';
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistOrderOrderColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistOrderStatusColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 var status = record.get('system_status');

                 return status;
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistOrderStatusColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
     equiHistOrderOrderTypeColumnRenderer: function () {
         try {
             var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                 //store -> item -> aufnr, ktext
                 var auart = record.get('auart');
                 return auart;
             }
             return renderer;
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiHistOrderOrderTypeColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
         }
     },
    // End of equiHistOrderGrid Renderers
         
    equiClassActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/class_charact.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiClassActionColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },

    equiClassCharacteristicColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = null;

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('atbez')))
                    description = record.get('atbez');
                else
                    description = record.get('atnam');

                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiClassCharacteristicColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    },

    equiClassCharacteristicValueColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                // record = Characts from FuncLocDetailPageController (Function: beforeDataReady) 
                var valueItem = '';
                var filteredCharValues = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.CharactValue',
                    autoLoad: false
                });

                var valueList = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });

                var classValues = record.get('classValues');
                if (classValues && classValues.getCount() > 0) {

                    classValues.each(function (classValue) {
                        if ((classValue.get('atinn') === record.get('atinn')) && !(classValue.get('updflag') === 'D'))
                            valueList.add(classValue);
                    });

                    if (valueList.getCount() > 0)
                        valueList.each(function (value) {
                            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value.get('atwrt'))) {

                                if (record.get('charactValues') && record.get('charactValues').getCount() > 0) {
                                    record.get('charactValues').each(function (charValue) {
                                        if (charValue.get('atwrt') === value.get('atwrt'))
                                            filteredCharValues.add(charValue);
                                    });

                                    if ((filteredCharValues.getCount() > 0) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(filteredCharValues.getAt(0).get('atwtb')))
                                        valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(filteredCharValues.getAt(0).get('atwtb'), record);
                                    else
                                        valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atwrt'), record);
                                }
                                else
                                    valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atwrt'), record);
                            }
                            else
                                valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atflv'), record);

                            valueItem += "\n";
                        });
                }
                return valueItem;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiClassCharacteristicValueColumnRenderer of BaseEquiDetailPageRendererMixin', ex);
        }
    }


    
})