﻿Ext.define('AssetManagement.base.view.mixins.BaseChecklistPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //inspectionGridPanel Renderers

    //TODO add try catches EVERYWHERE

    checklistRemarksColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        var icon = 'document.png'; //basic icon for the, if it's not required, nor given.
        
        if (record.get('remark')) //if remark has been given, a different icon should be used
            icon = 'documentOk.png';
        return '<img src="resources/icons/' + icon + '"/>';
    },

    checklistInspectionColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        //a field displaying the shorttext of the characteristic, and then the tolerance boundries
        var id = Ext.id();
        var toleranzText = "";
        //tolerance boundries used for toleranzText
        var toleranzun = record.get('toleranzun');
        var toleranzob = record.get('toleranzob');
        var sollwert = record.get('sollwert');
        //**//
        //concatenating boundries variables into the toleranzText
        if (toleranzun)
            toleranzText += Locale.getMsg('toleranzun') + ": " + toleranzun + "   ;";

        if (sollwert)
            toleranzText += Locale.getMsg('sollwert') + ": " + sollwert + "   ;";

        if (toleranzob)
            toleranzText += Locale.getMsg('toleranzob') + ": " + toleranzob + "   ;";
        //**//
        toleranzText = toleranzText.substr(0, toleranzText.length - 1);

        //a field displaying the shorttext of the characteristic, and then the tolerance boundries
        Ext.defer(function () {
            if (Ext.get(id)) {
                Ext.widget(
                    'container', {
                        padding: '8 0 0 0',
                        //flex: 1,
                        renderTo: id,
                        items: [
                            {
                                xtype: 'label',
                                text: record.get('kurztext'),
                                width: 150
                            },
                            {
                                xtype: 'component',
                                height: 3
                            },
                            {
                                xtype: 'label',
                                text: toleranzText,
                                width: 3
                            }

                        ]
                    });
            }
        }, 50);

        return Ext.String.format('<div id="{0}"></div>', id);
    },

    checklistMeanValueOrDropdownRenederer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        //a field conteining either an number input field, for meanvalue (if char_type === 1)
        //or a combobox to select the result of the inspection (code and codegruppe)
        var myController = this.getController();

        var qplos = this.getViewModel().get('qplos');
        var disabled = qplos ? qplos.isCompleted() : false;

        if (record.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUANTITATIVE) { //meanValue numberField

            var numberField = null;


            var id = Ext.id();

            Ext.defer(function () {
                if (Ext.get(id)) {
                    Ext.create('Ext.container.Container', {
                        xtype: 'container',
                        layout: 'column',
                        padding: '12 0 0 0',
                        disabled: disabled,
                        disabledCls: 'oxMeanvalueDisabled',
                        renderTo: id,
                        items:
                            [
                                {
                                    xtype: 'oxnumberfield',
                                    fieldLabel: '',
                                    width: 90,
                                    maxLength: 19,
                                    decimalPrecision: 19,
                                    maxValue: 100000000000000000000,
                                    minValue: -100000000000000000000,
                                    autofocus: false,
                                    hideTrigger: true,          //Specifies whether the up and down arrows for changing the value should be hidden
                                    keyNavEnabled: false,       //Specifies whether the up and down arrow keys should trigger spinning up and down. 
                                    mouseWheelEnabled: false,   //Specifies if the number valuse can be changed using mousewheel
                                    allowNegativeValues: true,
                                    enforceMaxLength: true,
                                    value: record.get('meanValue'),//record.get('checkBoxNREL') ? "" : record.get('meanValue'),
                                    listeners: {
                                        change: 'onMeanValueChanged',
                                        //focusleave: 'onMeanValueFieldLooseFocus',
                                        scope: myController,
                                        record: record
                                    }
                                },
                                {
                                    xtype: 'label',
                                    text: ' ' + record.get('masseinhsw'), //displaying measurement units

                                    padding: '3 0 0 10'
                                }
                            ]
                    });
                }
            }, 50);
            return Ext.String.format('<div id="{0}"></div>', id);
        }
            //combobox
            //only if it's a qualitative value AND not set in customizing to use checkboxes insetad of combobox   showCheckboxQualitive
        else if (record.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_STANDARD) {

            var id = Ext.id();
            //creating a display store for the combobox
            var storeToPass = Ext.create('Ext.data.Store', {
                fields: ['text', 'record'],
                autoLoad: false     //setting autoload to false prevent the store to load on changing the record and improves the performance a lot
            });

            var codes = record.get('codes'); //avaliable codes store

            if (codes && codes.getCount() > 0) {
                codes.each(function (code) {
                    var record = {
                        text: code.get('kurztext'),
                        entry: code
                    }
                    storeToPass.add(record);
                });
            }
            Ext.defer(function () {
                if (Ext.get(id)) {
                    var comboBox = Ext.create('AssetManagement.customer.view.utils.OxComboBox', {
                        queryMode: 'local',
                        displayField: 'text',
                        layout: 'fit',
                        padding: '12 0 0 0',
                        width: 120,
                        store: storeToPass,
                        disabled: disabled,
                        editable: false,
                        renderTo: id,
                        listeners: {
                            select: 'onDropdownItemSelected',
                            scope: myController,
                            entry: record
                        },
                        listConfig: {
                            cls: 'min-row-height'
                        }
                    });
                    //code and codegruppe of the record
                    var auwmenge2 = record.get('auswmenge2');
                    var auswmgwrk2 = record.get('auswmgwrk2');

                    //if the record already has a chosen code and codegroup, it's selected in the combobox
                    if (auwmenge2 && auswmgwrk2) {
                        storeToPass.each(function (code) {
                            if (code.get('entry').get('code') === auswmgwrk2 &&
                                code.get('entry').get('codegruppe') === auwmenge2) {
                                comboBox.select(code);
                                return false;
                            }
                        });
                    }
                }
            }, 50);


            return Ext.String.format('<div id="{0}"></div>', id)
        }
        else return ''
    },


    //renderer of the checkboxes, loads the require img to display it
    checklistCheckboxRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        var qplos = this.getViewModel().get('qplos');
        var disabled = qplos ? qplos.isCompleted() : false;

        var qplosCharModel = AssetManagement.customer.model.bo.QPlosChar;

        if (record.get('category') === qplosCharModel.CATEGORIES.QUANTITATIVE ||              //only displayed for qpuantitative characteristics (char_type == 01) 
            record.get('category') === qplosCharModel.CATEGORIES.QUALITATIVE_CHECKBOX) {      //or qualitative characteristics customized to used checkbox 

            var cssPrefix = Ext.baseCSSPrefix,
            cls = cssPrefix + 'grid-checkcolumn';

            if (value) {
                cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';
            }

            if (disabled) {
                cls += ' ' + cssPrefix + 'grid-checkcolumn-disabled';
            }

            return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
        }
    }
    // End of the inspectionGridPanel Renderers
})