﻿Ext.define('AssetManagement.base.view.mixins.BasePickupListPageRendererMixin', {
  extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
  //pickupGridPanel Column renderers
  deliveryIconColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var imageSrc = 'resources/icons/delivery.png';

        if (record && record.get('localStatus')) {
          imageSrc = record.get('localStatus').get('statusIconPath');
        }

        return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deliveryActionColumnRenderer of BasePickupListPageRendererMixin', ex);
    }
  },
  deliveryDepartmentColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var abt = record.get('short');
        return abt;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deliveryDeliveryColumnRenderer of BasePickupListPageRendererMixin', ex);
    }
  },
  deliveryStatusColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        //   var count = record.get('items').getCount();
        //   if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
        //   return count;
        return '0';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deliveryStatusColumnRenderer of BasePickupListPageRendererMixin', ex);
    }
  },
  iaReasonOnWidgetAttached: function (column, widget, record) {
    try {
      var view = widget.owner;
      var myModel = view.getViewModel();
      var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
      var spras = userInfo.get('sprache');
      // var miProcess = myModel.get('miProcess');
      // var bwart = miProcess.get('bwart');
      var reasonStore = myModel.get('reasonStore');
      reasonStore.each(function (reason) {
        if (!(reason.get('spras') === spras)) {
          reasonStore.remove(reason);
        }
      });
      var containerItems = widget.query();
      //
      containerItems[1].setStore(reasonStore);
      // containerItems[0].setConfig('allowBlank', false);
      // containerItems[0].setConfig('validateBlank', true);
      var moveReason = record.get('grund');
      if (moveReason === '000') {
        containerItems[1].setValue('');
      }else{
        containerItems[1].setValue(moveReason);
      }

      var status = record.get('ia_status');
      if(status === '' || status === 'Z' || status === 'A'){
        containerItems[1].setReadOnly(true);
      }

      containerItems[0].setText(record.get('empf'));

      // widget.addListener('select', myController.onLgortSelected,myController, {record: record});

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside miReasonOnWidgetAttached of BaseWaCreatePageRendererMixin', ex);
    }

  },
  iaRoomOnWidgetAttached: function (column, widget, record) {
    try {
      var view = widget.owner;
      // var myModel = view.getViewModel();
      // var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
      // var spras = userInfo.get('sprache');
      // // var miProcess = myModel.get('miProcess');
      // // var bwart = miProcess.get('bwart');
      // var reasonStore = myModel.get('reasonStore');
      // reasonStore.each(function (reason) {
      //   if (!(reason.get('spras') === spras)) {
      //     reasonStore.remove(reason);
      //   }
      // });
      var containerItems = widget.query();
      //
      // containerItems[1].setStore(reasonStore);
      // // containerItems[0].setConfig('allowBlank', false);
      // // containerItems[0].setConfig('validateBlank', true);
      // var moveReason = record.get('grund');
      // if (moveReason) {
      //   containerItems[1].setValue(moveReason);
      // }
      containerItems[0].setText(record.get('short'));
      // containerItems[1].items.items[0].setName(record.get('belnr'))
      // containerItems[1].items.items[1].setName(record.get('belnr'))
      var status = record.get('ia_status');
      if (status == '' && record.get('grund') !== '000'){
        status = 'XX';
      }
      containerItems[1].setValue(status);

      // widget.addListener('select', myController.onLgortSelected,myController, {record: record});

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside miReasonOnWidgetAttached of BaseWaCreatePageRendererMixin', ex);
    }

  },
  deliveryActivityTypeColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var ilartText = '';
        if (record.get('deliveryIlart') !== undefined) {
          var ilart = record.get('deliveryIlart');
          if (ilart !== null && ilart !== undefined) {
            ilartText = ilart.get('ilart') + '<div>' + ilart.get('ilatx') + '</div>';
          }
        }
        return ilartText;
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deliveryDeliveryColumnRenderer of BasePickupListPageRendererMixin', ex);
    }
  },
  deliveryAddressColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var name = '';
        var street = '';
        var postalCode = '';
        var address = record.getObjectAddress();
        if (address !== null && address !== undefined) {
          name = address.getName();
          street = AssetManagement.customer.utils.StringUtils.concatenate([address.getStreet(), address.getHouseNo()], null, true);
          postalCode = AssetManagement.customer.utils.StringUtils.concatenate([address.getPostalCode(), address.getCity()], null, true);
        } else {
          name = '';
          street = '';
          postalCode = '';
        }

        return name + '<p>' + street + '</p><p>' + postalCode + '</p>';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deliveryDeliveryColumnRenderer of BasePickupListPageRendererMixin', ex);
    }
  },

  deliveryMapsColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var partnerList = record.get('partners');
        if (partnerList !== null && partnerList !== undefined) {
          var partner = AssetManagement.customer.manager.PartnerManager.getSingularPartnerFromPartnerList(partnerList, 'WE');
          if (partner !== null && partner !== undefined) {
            metaData.style = '';
          }
          else {
            metaData.style = 'display: none;';
          }
        }
        else {
          metaData.style = 'display: none;';
        }
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifMapsColumnRenderer of BasePickupListPageRendererMixin', ex);
    }
  },

  deliveryDispatchingOperationsRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var operations = record.get('operations');
        var dispatchingOpKey = AssetManagement.customer.model.bo.FuncPara.getInstance().get('accind_disp_oper');
        var counter = 0;
        if (operations) {
          operations.each(function (operation) {
            if (operation.get('steus') === dispatchingOpKey) {
              counter += 1;
            }
          });
          return counter;
        }
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deliveryDispatchingOperationsRenderer of BasePickupListPageRendererMixin', ex);

    }
  }
})