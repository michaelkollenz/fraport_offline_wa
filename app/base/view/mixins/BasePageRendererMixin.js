﻿Ext.define('AssetManagement.base.view.mixins.BasePageRendererMixin', {
    defaultRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return value;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside defaultRenderer of BasePageRendererMixin', ex);
        }
    },
    defaultMultipleLineRenderer: function (primaryAttribute, secondaryAttribute, tertiaryAttribute) {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retString = '';
                if (primaryAttribute && primaryAttribute !== '') {
                    var primAttr = record.get(primaryAttribute)
                    retString = retString.concat(primAttr)
                }
                if (secondaryAttribute && secondaryAttribute !== '') {
                    var secAttr = record.get(secondaryAttribute)
                    if (retString === '') {
                        retString = retString.concat(secAttr)
                    } else {
                        retString = retString.concat('<p>' + secAttr + '</p>')
                    }
                    
                }
                if (tertiaryAttribute && tertiaryAttribute !== '') {
                    var terAttr = record.get(tertiaryAttribute)
                    if (retString === '') {
                        retString = retString.concat(terAttr)
                    } else {
                        retString = retString.concat('<p>' + terAttr + '</p>')
                    }
                }
                return retString;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside defaultRenderer of BasePageRendererMixin', ex);
        }
    },
    defaultCheckBoxRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var cssPrefix = Ext.baseCSSPrefix,
	                            cls = cssPrefix + 'grid-checkcolumn';

                if (value) {
                    cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';
                }
                
                return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside defaultRenderer of BasePageRendererMixin', ex);
        }
    }
   
})