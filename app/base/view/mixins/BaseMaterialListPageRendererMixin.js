﻿Ext.define('AssetManagement.base.view.mixins.BaseMaterialListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //matStockGridPanel Renderers
    matStockActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/matconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockActionColumnRenderer of BaseMaterialListPageRendererMixin', ex);
        }
    },
    matStockMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = record.get('material');
                var maktx = material ? material.get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockMaterialColumnRenderer of BaseMaterialListPageRendererMixin', ex);
        }
    },
    matStockOrderComponents_QtysColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
                var unit = record.get('meins');

                return quant + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockOrderComponents_QtysColumnRenderer of BaseMaterialListPageRendererMixin', ex);
        }
    },
    matStockPlantStorLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var plant = record.get('werks');
                var lgort = record.get('lgort');

                return AssetManagement.customer.utils.StringUtils.concatenate([plant, lgort], '/', false, false, '-');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matStockPlantStorLocColumnRenderer of BaseMaterialListPageRendererMixin', ex);
        }
    }
    // End of the matStockGridPanel Renderers
})