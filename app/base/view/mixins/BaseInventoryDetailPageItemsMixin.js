﻿Ext.define('AssetManagement.base.view.mixins.BaseInventoryDetailPageItemsMixin', {
    setPositionColumnItems: function () {
        try {
            retval = [
            {
                xtype: 'oxtextfield',
                id: 'positionField',
                flex: 0.2,
                labelWidth: 5,
                listeners: {
                    change: {
                        fn: 'onInventoryDetailPageSearchFieldChange'
                    }
                }
            }];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setPositionColumnItems of BaseInventoryDetailPageItemsMixin', ex);
        }

        return retval;
    },
    setMaterialColumnItems: function () {
        try {
            retval = [
            {
                xtype: 'oxtextfield',
                id: 'materialField',
                flex: 1,
                labelWidth: 5,
                listeners: {
                    change: {
                        fn: 'onInventoryDetailPageSearchFieldChange'
                    }
                }
            }];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMaterialColumnItems of BaseInventoryDetailPageItemsMixin', ex);
        }

        return retval;
    },
    setQuantityColumnItems: function () {
        try {
            retval = [
            {
                xtype: 'numberfield',
                flex: 1,
                labelWidth: 5,
                minValue: 0,
                id: 'quantityField',
                listeners: {
                    change: {
                        fn: 'onInventoryDetailPageSearchFieldChange'
                    }
                }
            }];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setQuantityColumnItems of BaseInventoryDetailPageItemsMixin', ex);
        }

        return retval;
    },
    setUnitColumnItems: function () {
        try {
            retval = [
            {
                xtype: 'oxtextfield',
                flex: 1,
                id: 'unitField',
                labelWidth: 5,
                listeners: {
                    change: {
                        fn: 'onInventoryDetailPageSearchFieldChange'
                    }
                }
            }];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUnitColumnItems of BaseInventoryDetailPageItemsMixin', ex);
        }

        return retval;
    },
    setZeroCountColumnItems: function () {
        try {
            retval = [
            {
                xtype: 'checkbox',
                flex: 1,
                labelWidth: 30,
                id: 'zerocountField',
                listeners: {
                    change: {
                        fn: 'onInventoryDetailPageSearchFieldChange'
                    }
                }
            }];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUnitColumnItems of BaseInventoryDetailPageItemsMixin', ex);
        }

        return retval;
    }
})