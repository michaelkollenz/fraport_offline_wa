﻿Ext.define('AssetManagement.base.view.mixins.BaseWaCreatePageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

    //GoodsMovementList Renderers
    waCreatePositionColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var position = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('ebelp'));

            return position;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseWaCreatePageRendererMixin', ex);
        }
    },

    waCreateMaterialColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
        try {
            var matnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('matnr'));
            var mattxt = record.get('matkt');

            return matnr + '<p>' + mattxt + '</p>';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseWaCreatePageRendererMixin', ex);
        }
    },


  waCreateMengeColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
    try {
      var menge = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('menge')) * 1; //convert to nuber, to have a 0 if empty
      var meins = record.get('meins') ;
      return menge.toLocaleString() + ' ' + meins;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseMatListPageRendererMixin', ex);
    }
  },

  miReasonOnWidgetAttached: function (column, widget, record) {
        try {
            var view = widget.owner;
            var myModel = view.getViewModel();
            var miProcess = myModel.get('miProcess');
            var bwart = miProcess.get('bwart');
            var reasonStore = myModel.get('reasonStore');
            reasonStore.each(function (reason) {
              if(!(reason.get('bwart') === bwart)){
                reasonStore.remove(reason);
              }

            });
            var containerItems = widget.query();

            containerItems[0].setStore(reasonStore);
            containerItems[0].setConfig('allowBlank', false);
            containerItems[0].setConfig('validateBlank', true);
              var moveReason = record.get('move_reas');
              if(moveReason){
                containerItems[0].setValue(moveReason);
              }
            containerItems[1].setValue(record.get('item_text'));
            containerItems[1].setConfig('allowBlank', false);
            containerItems[1].setConfig('validateBlank', true);

            // widget.addListener('select', myController.onLgortSelected,myController, {record: record});

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside miReasonOnWidgetAttached of BaseWaCreatePageRendererMixin', ex);
        }

    },


    mengeOnWidgetAttached: function (column, widget, record) {
        try {
            widget.setValue(record.get('menge'));
            widget.setDisabled(true); //it is an always disabled field, read-only
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside wePositionColumnRenderer of BaseWaCreatePageRendererMixin', ex);
        }
    },

  checkDeleteColumnRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
    try {
      var prefilled = record.get('prefilled');

      var cssPrefix = Ext.baseCSSPrefix,
        cls = cssPrefix + 'oxCheckBoxGridColumnItem';

      if (!prefilled) {
        // cls += ' ' + cssPrefix + 'deleteButton';
        this.tdCls += ' deleteButton'

      }
      // return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';

    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside deliveredCheckColumnRenderer of GoodsReceiptPageRendererMixin', ex);
    }
  }

});