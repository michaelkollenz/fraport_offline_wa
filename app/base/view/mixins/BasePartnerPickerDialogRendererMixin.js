﻿Ext.define('AssetManagement.base.view.mixins.BasePartnerPickerDialogRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    partnerPickerPartnerColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('parnr'), '0');;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerPickerPartnerColumnRenderer of BasePartnerPickerDialogRendererMixin', ex);
        }
    },
    partnerPickerNameColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                //store -> item -> aufnr, ktext
                var firstname = record.get('firstname');
                var lastname = record.get('lastname');
                var retVal = firstname;
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lastname))
                    retVal += ' ' + lastname;
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerPickerNameColumnRenderer of BasePartnerPickerDialogRendererMixin', ex);
        }
    },
    partnerPickerLocationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                //store -> item -> aufnr, ktext
                var postlcode = record.get('postlcod1');
                var city = record.get('city');
                var retVal = postlcode + ' ' + city;
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside partnerPickerLocationColumnRenderer of BasePartnerPickerDialogRendererMixin', ex);
        }
    }
})