﻿Ext.define('AssetManagement.base.view.mixins.BaseCalendarPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //unplannedOperationGridPanel Renderers
    unplannedOperationOrderSlashOperationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('Operation').get('aufnr'), '0');
                var vornr = record.get('Operation').get('vornr');
                var ktext = record.get('Title');
                return aufnr + "/" + vornr + " " + ktext;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedOperationOrderSlashOperationColumnRenderer of BaseCalendarPageRendererMixin', ex);
        }
    },
    unplannedOperationDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var date = Ext.Date.format(record.get('Fsav'), 'd.m.Y H:i');
                //var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('Operation').get('aufnr'), '0');
                //var vornr = record.get('Operation').get('vornr');
                //var ktext = record.get('Title');
                return date;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedOperationDateColumnRenderer of BaseCalendarPageRendererMixin', ex);
        }
    },
    unplannedOperationFuncLoc_ShortColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tpl = record.data.Order.data.funcLoc;
                var retVal = '';
                if (tpl != null) {
                    retVal = tpl.data.pltxt;
                }
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedOperationFuncLoc_ShortColumnRenderer of BaseCalendarPageRendererMixin', ex);
        }
    },
    unplannedOperationZipCodeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal = '';
                try {
                    var address = record.data.Order.data.address;
                    if (address != null) {
                        retVal = address.get('postCode1');
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPageRendererMixin', ex);
                }
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedOperationZipCodeColumnRenderer of BaseCalendarPageRendererMixin', ex);
        }
    },
    unplannedOperationCityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal = '';
                try {
                    var address = record.data.Order.data.address;
                    if (address != null) {
                        retVal = address.get('city1');
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPageRendererMixin', ex);
                }
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedOperationZipCodeColumnRenderer of BaseCalendarPageRendererMixin', ex);
        }
    },
    unplannedOperationAddressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal = '';
                try {
                    var address = record.data.Order.data.address;
                    if (address != null) {
                        retVal = address.get('name1') + ' ' + address.get('name2') + ' ' + address.get('street') + ' ' +
                                address.get('houseNum1');
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPageRendererMixin', ex);
                }
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedOperationAddressColumnRenderer of BaseCalendarPageRendererMixin', ex);
        }
    }
    // End of the unplannedOperationGridPanel Renderers
})