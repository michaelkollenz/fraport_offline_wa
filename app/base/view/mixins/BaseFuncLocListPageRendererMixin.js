﻿Ext.define('AssetManagement.base.view.mixins.BaseFuncLocListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //funcLocGridPanel Renderers
    funcLocActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/funcloc.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocActionColumnRenderer of BaseFuncLocListPageRendererMixin', ex);
        }
    },
    funcLocFuncLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tplnr = record.getDisplayIdentification();
                var pltxt = record.get('pltxt');

                return tplnr + '<p>' + pltxt + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocFuncLocColumnRenderer of BaseFuncLocListPageRendererMixin', ex);
        }
    },
    funcLocAddressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var name = '';
                var street = '';
                var postalCode = '';

                var address = record.get('address');
                if (address !== null && address !== undefined) {
                    name = address.getName();
                    street = AssetManagement.customer.utils.StringUtils.concatenate([address.getStreet(), address.getHouseNo()], null, true);
                    postalCode = AssetManagement.customer.utils.StringUtils.concatenate([address.getPostalCode(), address.getCity()], null, true);
                } else {
                    name = '';
                    street = '';
                    postalCode = '';
                }

                return name + '<p>' + street + '</p><p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocClassCharacteristicValueColumnRenderer of BaseFuncLocListPageRendererMixin', ex);
        }
    },
    funcLocMapsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (!record.get('address')) {
                    metaData.style = 'display: none;';
                } else {
                    metaData.style = '';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside funcLocMapsColumnRenderer of BaseFuncLocListPageRendererMixin', ex);
        }
    }
    // End of the funcLocGridPanel Renderers
})