﻿Ext.define('AssetManagement.base.view.mixins.BaseSDOrderListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //sdOrderGridPanel Renderers
    sdOrderActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderActionColumnRenderer of BaseSDOrderListPageRendererMixin', ex);
        }
    },
    sdOrderOrderingNumberColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bstnk = record.get('bstnk');
                var vbeln = AssetManagement.customer.utils.StringUtils.trimStart(record.get('vbeln'), '0');
                return bstnk + '<p>' + vbeln + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderOrderingNumberColumnRenderer of BaseSDOrderListPageRendererMixin', ex);
        }
    },
    sdOrderDeliveryStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getDeliveryStatus();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sdOrderDeliveryStatusColumnRenderer of BaseSDOrderListPageRendererMixin', ex);
        }
    }
    // End of the sdOrderGridPanel Renderers
})