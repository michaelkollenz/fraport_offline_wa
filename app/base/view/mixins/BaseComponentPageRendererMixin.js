﻿Ext.define('AssetManagement.base.view.mixins.BaseComponentPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //ComponentPage List Renderers
    componentActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/settings.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentActionColumnRenderer of BaseComponentPageRendererMixin', ex);
        }
    },
    componentMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = record.get('material');
                var maktx = material ? material.get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentMaterialColumnRenderer of BaseComponentPageRendererMixin', ex);
        }
    },
    componentOrderQtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bdmng'));
                return amount + ' ' + record.get('meins');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentOrderQtyColumnRenderer of BaseComponentPageRendererMixin', ex);
        }
    },
    componentOperationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';

                try {
                    retval = record.get('vornr');
                    var myModel = this.getViewModel();

                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval) && myModel) {
                        var order = myModel.get('order');
                        var operations = order ? order.get('operations') : null;

                        if (operations && operations.getCount() > 0) {
                            operations.each(function (operation) {
                                if (operation.get('vornr') === retval) {
                                    var ltxa1 = operation.get('ltxa1');

                                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ltxa1))
                                        retval += ' - ' + ltxa1;

                                    return false;
                                }
                            }, this);
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception while rendering Components of ComponentPage', ex);
                }

                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentOperationColumnRenderer of BaseComponentPageRendererMixin', ex);
        }
    }
})