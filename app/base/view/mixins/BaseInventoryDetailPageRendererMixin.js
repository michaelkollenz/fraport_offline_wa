﻿Ext.define('AssetManagement.base.view.mixins.BaseInventoryDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    inventoryPositionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var position = record.get('item');
                return position;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryPositionColumnRenderer of BaseInventoryDetailPageRendererMixin', ex);
        }
    },
    inventoryMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = AssetManagement.customer.utils.StringUtils.trimStart(record.get('material'), '0');
                return material;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryMaterialColumnRenderer of BaseInventoryDetailPageRendererMixin', ex);
        }
    },
    inventoryQuantityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = record.get('entryQnt');
                return quantity;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryQuantityColumnRenderer of BaseInventoryDetailPageRendererMixin', ex);
        }
    },
    inventoryUnitColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var unit = AssetManagement.customer.utils.StringUtils.trimStart(record.get('entryuom'), '0');
                return unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryUnitColumnRenderer of BaseInventoryDetailPageRendererMixin', ex);
        }
    }
})