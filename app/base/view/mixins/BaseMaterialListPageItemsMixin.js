﻿Ext.define('AssetManagement.base.view.mixins.BaseMaterialListPageItemsMixin', {
    setbanfIconGridCOlumnItems: function () {
        try {
            retval = [{
                icon: 'resources/icons/demand_requirement.png',
                tooltip: Locale.getMsg('newPreq'),
                iconCls: 'oxGridLineActionButton',
                /*handler: function (grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
                    event.stopEvent();
                    this.getController().onDemandRequirementClick.call(this.getController(), grid, rowIndex, colIndex, clickedItem, event, record, tableRow);
                },*/
                scope: this
            }]
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMaterialListPageItemsMixin', ex);
        }

        return retval;
    }
})