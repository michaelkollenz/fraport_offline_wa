﻿Ext.define('AssetManagement.base.view.mixins.BaseNotifDetailPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //NotifItemsGrid Renderers
    notifItemActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_item.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActionColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifItemChecklistDamageColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('damageCustCode')) {
                    var damageGrp = record.get('damageCustCode').get('codegrkurztext');//record.get('fegrp'); 
                    var damageCode = record.get('damageCustCode').get('kurztext');//record.get('fecod');

                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(damageGrp)) damageGrp = '';
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(damageCode)) damageCode = '';

                    return damageGrp + '<p>' + damageCode + '</p>';
                }
                else
                    return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemChecklistDamageColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },

    notifItemObjColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('objectPartCustCode')) {
                    var otgrp = record.get('objectPartCustCode').get('codegrkurztext'); //record.get('otgrp'); 
                    var oteil = record.get('objectPartCustCode').get('kurztext');//record.get('oteil');
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(otgrp)) otgrp = '';
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(oteil)) oteil = '';
                    return otgrp + '<p>' + oteil + '</p>';
                }
                else
                    return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemObjColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },

    notifDamageCauseColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('notifItemCauses').getCount().toString();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDamageCauseColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },

    notifItemActivityColumnrenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('notifItemActivities').getCount().toString();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActivityColumnrenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },

    notifItemTaskColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('notifItemTasks').getCount().toString();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside NotifItemTaskColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    //End ofNotifItemsGrid Renderers

    //Begin of notifActivitiesGrid Renderers
    notifActionsColumnRenderer: function() {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_activity.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifActionsColumnrenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifActivityActColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('activityCustCode')) {
                    var activityGrp = record.get('activityCustCode').get('codegrkurztext');
                    var activityCode = record.get('activityCustCode').get('kurztext');

                    return activityGrp + '<p>' + activityCode + '</p>';
                }
                else
                    return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifActivityActColumn of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    //End of notifActivitiesGrid Renderers

    //Begin of notifTasksGrid Renderers
    notifTaskActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_task.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifActionsColumnrenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },

    notifTaskMassColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('taskCustCode')) {
                    var taskGrp = record.get('taskCustCode').get('codegrkurztext');
                    var taskCode = record.get('taskCustCode').get('kurztext');

                    return taskGrp + '<p>' + taskCode + '</p>';
                }

                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifTaskMassColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    //End of notifTasksGrid Renderers

    //Begin of notifDetailPageSDOrderItemsGridPanel Renderers
    notifDetailPageSDOrderItemsActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsActionColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },

    notifDetailPageSDOrderItemsPositionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bstnk = AssetManagement.customer.utils.StringUtils.trimStart(record.get('bstnk'), '0');
                var posnr = record.get('posnr');
                return bstnk + '<p>' + posnr + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsPositionColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';
                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsMaterialColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsQuantityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
                var unit = record.get('zieme');
                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsQuantityColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsDeliveryStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getDeliveryStatus();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsDeliveryStatusColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsRemarksColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('arktx');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsRemarksColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    //End of notifDetailPageSDOrderItemsGridPanel Renderers

    //Begin of notifFileGridPanel Renderers
    notifFileIconColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/document.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileIconColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifFileNameDescColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = record.get('fileDescription');
                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileNameDescColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifFileTypeSizeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var type = record.get('fileType');
                var fileSize = record.get('fileSize') ? Number(record.get('fileSize')) : 0;
                var size = fileSize.toFixed(2) + ' kB';
                return type + '<p>' + size + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileTypeSizeColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifFileOriginColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stringToShow = "";
                var fileOrigin = record.get('fileOrigin');
                if (fileOrigin === "online") {
                    stringToShow = Locale.getMsg('sapSystem');
                } else if (fileOrigin === "local") {
                    stringToShow = Locale.getMsg('local');
                }
                return stringToShow;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileOriginColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    //End of notifFileGridPanel Renderers

    //Begin of notifPartnerGrid Renderers
    notifPartnerImgColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/partner.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerImgColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifPartnerNameColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var nameLine1 = AssetManagement.customer.utils.StringUtils.concatenate([record.get('name1'), record.get('name2')], null, true);
                var nameLine2 = AssetManagement.customer.utils.StringUtils.concatenate([record.get('name3'), record.get('name4')], null, true);
                return nameLine1 + '<p>' + nameLine2 + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerNameColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifPartnerContactColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tel = record.get('telnumber');
                var telExtens = record.get('telextens');
                var telMob = record.get('telnumbermob');
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tel)) {
                    tel = 'Tel.: ' + tel;
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telExtens))
                        tel += ' ' + telExtens;
                }
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telMob)) {
                    telMob = 'Mobil: ' + telMob;
                }
                return tel + '<p>' + telMob + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerContactColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifPartnerAdressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var street = '';
                var postalCode = '';
                street = AssetManagement.customer.utils.StringUtils.concatenate([record.get('street'), record.get('housenum1')], null, true);
                postalCode = AssetManagement.customer.utils.StringUtils.concatenate([record.get('postcode1'), record.get('city1')], null, true);
                return street + '<p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerAdressColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    },
    notifPartnerMapsColumn: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('city1')) &&
					  AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('postcode1'))) {
                    metaData.style = 'display: none;';
                } else {
                    metaData.style = '';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerAdressColumnRenderer of BaseNotifDetailPageRendererMixin', ex);
        }
    }
    //End of notifPartnerGrid Renderers
});