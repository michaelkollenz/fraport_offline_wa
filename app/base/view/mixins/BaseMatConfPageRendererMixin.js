﻿Ext.define('AssetManagement.base.view.mixins.BaseMatConfPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //matConfGrid Renderers
    matConfActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/matconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfActionColumnRenderer of BaseMatConfPageRendererMixin', ex);
        }
    },
    matConfMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = record.get('material');
                var maktx = material ? material.get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfMaterialColumnRenderer of BaseMatConfPageRendererMixin', ex);
        }
    },
    matConfAccountIndicationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';

                try {
                    var accountIndication = record.get('accountIndication');

                    retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of matConfGrid/bemot-column in BaseMatConfPageRendererMixin', ex);
                }

                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfAccountIndicationColumnRenderer of BaseMatConfPageRendererMixin', ex);
        }
    },
    matConfOrderComponents_QtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('quantity'));
                var unit = record.get('unit');

                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfOrderComponents_QtyColumnRenderer of BaseMatConfPageRendererMixin', ex);
        }
    },
    matConfPlantStorLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var plant = record.get('plant');
                var storloc = record.get('storloc');

                return AssetManagement.customer.utils.StringUtils.concatenate([plant, storloc], '/');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfPlantStorLocColumnRenderer of BaseMatConfPageRendererMixin', ex);
        }
    }
    // End of the matConfGrid Renderers
})