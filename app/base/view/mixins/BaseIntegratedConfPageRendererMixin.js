﻿Ext.define('AssetManagement.base.view.mixins.BaseIntegratedConfPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    matConfListActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/matconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfListActionColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    matConfListMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var material = record.get('material');
                var maktx = material ? material.get('maktx') : '';

                return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfListMaterialColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    matConfListKunnrColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('customer');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfListKunnrColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },

    matConfListAccIndColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval = '';

                try {
                    var accountIndication = record.get('accountIndication');

                    retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                }
                catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of matConfListAccIndColumnRenderer in BaseIntegratedConfPageRendererMixin', ex);
                }

                return retval;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfListAccIndColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    matConfListComponents_QtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('quantity'));
                var unit = record.get('unit');

                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfListComponents_QtyColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    matConfListPlantStorLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var plant = record.get('plant');
                var storloc = record.get('storloc');

                return AssetManagement.customer.utils.StringUtils.concatenate([plant, storloc], '/');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matConfListPlantStorLocColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/timeconf.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListActionColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListActTypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval
                var learr = '';
                var learrTxt = '';
                try {
                    var activityType = record.get('actype');
                    var cust_041Store = view.up('integratedconfpage').getViewModel().get('cust_041');
                    var cust041 = AssetManagement.customer.manager.Cust_041Manager.getCust041ByActypeFromList(cust_041Store, activityType);
                    if (cust041 !== null && cust041 !== undefined) {
                        learr = cust041.get('actype');
                        learrTxt = cust041.get('ktext');
                    }
                    else {
                        learr = record.get('learr');
                    }

                    retval = AssetManagement.customer.utils.StringUtils.concatenate([learr, learrTxt], '-', true);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListActTypeColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
                }
                return retval
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListActTypeColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListAccountIndicationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retval
                var accountIndication = record.get('accountIndication');
                try {
                    retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListAccountIndicationColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
                }
                return retval
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListAccountIndicationColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListWorktypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal;
                try {
                    var worktype = record.get('usr1');
                    if (worktype === 'N')
                        retVal = 'N - ' + (AssetManagement.customer.model.bo.FuncPara.getInstance().get('maq_act_type_n')? AssetManagement.customer.model.bo.FuncPara.getInstance().get('maq_act_type_n'): 'N');
                    else if (worktype === 'O')
                        retVal = 'O - ' + (AssetManagement.customer.model.bo.FuncPara.getInstance().get('maq_act_type_o')? AssetManagement.customer.model.bo.FuncPara.getInstance().get('maq_act_type_o'): 'O');
                    else if (worktype === 'D')
                        retVal = 'D - ' + (AssetManagement.customer.model.bo.FuncPara.getInstance().get('maq_act_type_d')? AssetManagement.customer.model.bo.FuncPara.getInstance().get('maq_act_type_d'): 'D');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorktypeColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
                }
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorktypeColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListWorktimeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal
                try {
                    var actype = record.get('actype');
                    var cust_041Store = view.up('integratedconfpage').getViewModel().get('cust_041');
                    var cust041 = AssetManagement.customer.manager.Cust_041Manager.getCust041ByActypeFromList(cust_041Store, actype);
                    if (cust041 !== null && cust041 !== undefined) {
                        if (cust041.get('regtyp') === '02' || cust041.get('regtyp') === '03' || cust041.get('regtyp') === '04') {
                            var workStart = AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(record.get('isd'), false, false);
                            var WorkEnd = AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(record.get('ied'), false, false);
                            retVal = workStart + ' - ' + WorkEnd;
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorktimeColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
                }
                return retVal;

            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorktimeColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListWorkColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal;
                try {
                    var actype = record.get('actype');
                    var cust_041Store = view.up('integratedconfpage').getViewModel().get('cust_041');
                    var cust041 = AssetManagement.customer.manager.Cust_041Manager.getCust041ByActypeFromList(cust_041Store, actype);
                    if (cust041 !== null && cust041 !== undefined) {
                        if (cust041.get('regtyp') === '02' || cust041.get('regtyp') === '03' || cust041.get('regtyp') === '04') {
                            retVal = record.getWorkValueForDisplay();
                        }
                        else if (cust041.get('regtyp') === '01') {
                            retVal = record.get('idaur') + ' ' + record.get('idaue');
                        }
                        else if (cust041.get('regtyp') === '05') {
                            retVal = record.get('betrag') + ' ' + record.get('tcurr');
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorkColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
                }
                return retVal

            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorkColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListBreakColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var retVal;
                try {
                    var actype = record.get('actype');
                    var cust_041Store = view.up('integratedconfpage').getViewModel().get('cust_041');
                    var cust041 = AssetManagement.customer.manager.Cust_041Manager.getCust041ByActypeFromList(cust_041Store, actype);
                    if (cust041 !== null && cust041 !== undefined) {
                        if (cust041.get('regtyp') === '03') {
                            var breakStart = AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(record.get('pausesz'), false, false);
                            var breakEnd = AssetManagement.customer.utils.DateTimeUtils.getTimeStringForDisplay(record.get('pauseez'), false, false);
                            retVal = breakStart + ' - ' + breakEnd;
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorkColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
                }
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorkColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    confListSpecificationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('ltxa1');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confListWorkColumnRenderer of BaseIntegratedConfPageRendererMixin', ex);
        }
    },
    //NotifItemsGrid Renderers
    notifItemActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_item.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActionColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifItemChecklistDamageColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('damageCustCode')) {
                    var damageGrp = record.get('damageCustCode').get('codegrkurztext');//record.get('fegrp'); 
                    var damageCode = record.get('damageCustCode').get('kurztext');//record.get('fecod');

                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(damageGrp)) damageGrp = '';
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(damageCode)) damageCode = '';

                    return damageGrp + '<p>' + damageCode + '</p>';
                }
                else
                    return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemChecklistDamageColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },

    notifItemObjColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('objectPartCustCode')) {
                    var otgrp = record.get('objectPartCustCode').get('codegrkurztext'); //record.get('otgrp'); 
                    var oteil = record.get('objectPartCustCode').get('kurztext');//record.get('oteil');
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(otgrp)) otgrp = '';
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(oteil)) oteil = '';
                    return otgrp + '<p>' + oteil + '</p>';
                }
                else
                    return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemObjColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },

    notifDamageCauseColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('notifItemCauses').getCount().toString();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDamageCauseColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },

    notifItemActivityColumnrenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('notifItemActivities').getCount().toString();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifItemActivityColumnrenderer of NotifItemsGridColumnMixin', ex);
        }
    },

    notifItemTaskColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('notifItemTasks').getCount().toString();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside NotifItemTaskColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    //End ofNotifItemsGrid Renderers

    //Begin of notifActivitiesGrid Renderers
    notifActionsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_activity.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifActionsColumnrenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifActivityActColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('activityCustCode')) {
                    var activityGrp = record.get('activityCustCode').get('codegrkurztext');
                    var activityCode = record.get('activityCustCode').get('kurztext');

                    return activityGrp + '<p>' + activityCode + '</p>';
                }
                else
                    return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifActivityActColumn of NotifItemsGridColumnMixin', ex);
        }
    },
    //End of notifActivitiesGrid Renderers

    //Begin of notifTasksGrid Renderers
    notifTaskActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/notif_task.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifTaskActionColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },

    notifTaskMassColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('taskCustCode')) {
                    var taskGrp = record.get('taskCustCode').get('codegrkurztext');
                    var taskCode = record.get('taskCustCode').get('kurztext');

                    return taskGrp + '<p>' + taskCode + '</p>';
                }

                return '';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifTaskMassColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    //End of notifTasksGrid Renderers

    //Begin of notifDetailPageSDOrderItemsGridPanel Renderers
    notifDetailPageSDOrderItemsActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/demand_requirement.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsActionColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },

    notifDetailPageSDOrderItemsPositionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var bstnk = AssetManagement.customer.utils.StringUtils.trimStart(record.get('bstnk'), '0');
                var posnr = record.get('posnr');
                return bstnk + '<p>' + posnr + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsPositionColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
                var maktx = record.get('material') ? record.get('material').get('maktx') : '';
                return AssetManagement.customer.utils.StringUtils.concatenate([matnr, maktx], null, true);
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsMaterialColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsQuantityColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
                var unit = record.get('zieme');
                return quantity + ' ' + unit;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsQuantityColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsDeliveryStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.getDeliveryStatus();
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsDeliveryStatusColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifDetailPageSDOrderItemsRemarksColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('arktx');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifDetailPageSDOrderItemsRemarksColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    //End of notifDetailPageSDOrderItemsGridPanel Renderers

    //Begin of notifFileGridPanel Renderers
    notifFileIconColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/document.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileIconColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifFileNameDescColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var description = record.get('fileDescription');
                return description;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileNameDescColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifFileTypeSizeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var type = record.get('fileType');
                var fileSize = record.get('fileSize') ? Number(record.get('fileSize')) : 0;
                var size = fileSize.toFixed(2) + ' kB';
                return type + '<p>' + size + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileTypeSizeColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifFileOriginColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stringToShow = "";
                var fileOrigin = record.get('fileOrigin');
                if (fileOrigin === "online") {
                    stringToShow = Locale.getMsg('sapSystem');
                } else if (fileOrigin === "local") {
                    stringToShow = Locale.getMsg('local');
                }
                return stringToShow;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifFileOriginColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    //End of notifFileGridPanel Renderers

    //Begin of notifPartnerGrid Renderers
    notifPartnerImgColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/partner.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerImgColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifPartnerNameColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var nameLine1 = AssetManagement.customer.utils.StringUtils.concatenate([record.get('name1'), record.get('name2')], null, true);
                var nameLine2 = AssetManagement.customer.utils.StringUtils.concatenate([record.get('name3'), record.get('name4')], null, true);
                return nameLine1 + '<p>' + nameLine2 + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerNameColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifPartnerContactColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var tel = record.get('telnumber');
                var telExtens = record.get('telextens');
                var telMob = record.get('telnumbermob');
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tel)) {
                    tel = 'Tel.: ' + tel;
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telExtens))
                        tel += ' ' + telExtens;
                }
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telMob)) {
                    telMob = 'Mobil: ' + telMob;
                }
                return tel + '<p>' + telMob + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerContactColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifPartnerAdressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var street = '';
                var postalCode = '';
                street = AssetManagement.customer.utils.StringUtils.concatenate([record.get('street'), record.get('housenum1')], null, true);
                postalCode = AssetManagement.customer.utils.StringUtils.concatenate([record.get('postcode1'), record.get('city1')], null, true);
                return street + '<p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerAdressColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    },
    notifPartnerMapsColumn: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('city1')) &&
					  AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('postcode1'))) {
                    metaData.style = 'display: none;';
                } else {
                    metaData.style = '';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifPartnerAdressColumnRenderer of NotifItemsGridColumnMixin', ex);
        }
    }
    //End of notifPartnerGrid Renderers

})