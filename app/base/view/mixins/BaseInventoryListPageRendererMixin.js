﻿Ext.define('AssetManagement.base.view.mixins.BaseInventoryListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //inventoryGridPanel Renderers
    inventoryActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/inventory.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryActionColumnRenderer of BaseInventoryListPageRendererMixin', ex);
        }
    },
    inventoryInventoryDocumentColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var physinventory = AssetManagement.customer.utils.StringUtils.trimStart(record.get('physinventory'), '0');

                return physinventory;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryInventoryDocumentColumnRenderer of BaseInventoryListPageRendererMixin', ex);
        }
    },
    inventoryPlantColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var plant = record.get('plant');

                return plant;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryPlantColumnRenderer of BaseInventoryListPageRendererMixin', ex);
        }
    },
    inventoryStorLocColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var stgeloc = record.get('stgeLoc');
                return stgeloc;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryStorLogColumnRenderer of FuncLocListPageRendererMixin', ex);
        }
    },

  inventoryPlantStorLocSmallColumnRenderer: function () {
    try {
      var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
        var stgeloc = record.get('stgeLoc');
        var plant = record.get('plant');

        return plant + '<p>' + stgeloc + '</p>';
      }
      return renderer;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryPlantStorLocSmallColumnRenderer of FuncLocListPageRendererMixin', ex);
    }
  },
    inventoryScheduledDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(record.get('plan'));
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryScheduledDateColumnRenderer of FuncLocListPageRendererMixin', ex);
        }
    },
    inventoryStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var count = record.get('items').getCount();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count)) count = '';
                return count;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside inventoryStatusColumnRenderer of FuncLocListPageRendererMixin', ex);
        }
    }
    // End of the funcLocGridPanel Renderers
})