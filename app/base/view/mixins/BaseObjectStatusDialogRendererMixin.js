﻿Ext.define('AssetManagement.base.view.mixins.BaseObjectStatusDialogRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',

    objectStatusDialogStatusColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var txt04 = record.get('txt04');
                var txt30 = record.get('txt30');
                return txt04 + ' ' + txt30;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equiPickerLocationAndRoomColumnRenderer of BaseObjectStatusDialogRendererMixin', ex);
        }
    }
})