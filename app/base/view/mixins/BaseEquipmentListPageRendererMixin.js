﻿Ext.define('AssetManagement.base.view.mixins.BaseEquipmentListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //equipmentGridPanel Renderers
    equipmentActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/equi.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentActionColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    },
    equipmentEquipmentColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                //store -> item -> aufnr, ktext
                var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
                var ktext = record.get('eqktx');
                return aufnr + '<p>' + ktext + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentEquipmentColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    },
    equipmentSortFieldColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var eqfnr = record.get('eqfnr');

                return eqfnr;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentSortFieldColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    },
    equipmentMaterialColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var matnr = record.get('matnr');

                return matnr;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentMaterialColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    },
    equipmentSerialNumberColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var sernr = record.get('sernr');	
                return sernr;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentSerialNumberColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    },
    equipmentAddressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var name = '';
                var street = '';
                var postalCode = '';

                var address = record.get('address');
                if (address !== null && address !== undefined) {
                    name = address.getName();
                    street = AssetManagement.customer.utils.StringUtils.concatenate([address.getStreet(), address.getHouseNo()], null, true);
                    postalCode = AssetManagement.customer.utils.StringUtils.concatenate([address.getPostalCode(), address.getCity()], null, true);
                } else {
                    name = '';
                    street = '';
                    postalCode = '';
                }

                return name + '<p>' + street + '</p><p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentAddressColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    },
     equipmentMapsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                if (!record.get('address')) {
                    metaData.style = 'display: none;';
                } else {
                    metaData.style = '';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside equipmentMapsColumnRenderer of BaseEquipmentListPageRendererMixin', ex);
        }
    }




    
})