﻿Ext.define('AssetManagement.base.view.mixins.BaseOrderListPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //orderGridPanel Column renderers
    orderActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/order.png';

                if (record && record.get('localStatus')) {
                    imageSrc = record.get('localStatus').get('statusIconPath');
                }

                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderActionColumnRenderer of BaseOrderListPageRendererMixin', ex);
        }
    },
    orderOrderColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('aufnr'), '0');
                var ktext = record.get('ktext');
                return aufnr + '<p>' + ktext + '</p>' + '<br /><br />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderOrderColumnRenderer of BaseOrderListPageRendererMixin', ex);
        }
    },
    orderDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var operation = AssetManagement.customer.manager.OperationManager.getEarliestOperation(record);
                var time = null;

                if (operation) {
                    if (operation.get('ntan') != null)
                        time = operation.get('ntan');
                    else
                        time = operation.get('fsav');
                }

                var resTime = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(time).split(" ");

                return  resTime[0] +'<p>'+ resTime[1] + " "+ record.getRelevantTimeZone() + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDateColumnRenderer of BaseOrderListPageRendererMixin', ex);
        }
    },
    orderActivityTypeColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var ilartText = '';
                if (record.get('orderIlart') !== undefined) {
                    var ilart = record.get('orderIlart');
                    if (ilart !== null && ilart !== undefined) {
                        ilartText = ilart.get('ilart') + '<div>' + ilart.get('ilatx') + '</div>';
                    }
                }
                return ilartText;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderOrderColumnRenderer of BaseOrderListPageRendererMixin', ex);
        }
    },
    orderAddressColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var name = '';
                var street = '';
                var postalCode = '';
                var address = record.getObjectAddress();
                if (address !== null && address !== undefined) {
                    name = address.getName();
                    street = AssetManagement.customer.utils.StringUtils.concatenate([address.getStreet(), address.getHouseNo()], null, true);
                    postalCode = AssetManagement.customer.utils.StringUtils.concatenate([address.getPostalCode(), address.getCity()], null, true);
                } else {
                    name = '';
                    street = '';
                    postalCode = '';
                }

                return name + '<p>' + street + '</p><p>' + postalCode + '</p>';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderOrderColumnRenderer of BaseOrderListPageRendererMixin', ex);
        }
    },

    orderMapsColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var partnerList = record.get('partners');
                if (partnerList !== null && partnerList !== undefined) {
                    var partner = AssetManagement.customer.manager.PartnerManager.getSingularPartnerFromPartnerList(partnerList, 'WE');
                    if (partner !== null && partner !== undefined) {
                        metaData.style = '';
                    }
                    else {
                        metaData.style = 'display: none;';
                    }
                }
                else {
                    metaData.style = 'display: none;';
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside notifMapsColumnRenderer of BaseOrderListPageRendererMixin', ex);
        }
    },

    orderDispatchingOperationsRenderer: function () {
        try{
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var operations = record.get('operations');
                var dispatchingOpKey = AssetManagement.customer.model.bo.FuncPara.getInstance().get('accind_disp_oper');
                var counter = 0;
                if (operations) {
                    operations.each(function (operation) {
                        if (operation.get('steus') === dispatchingOpKey) {
                            counter += 1;
                        }
                    });
                    return counter;
                }
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside orderDispatchingOperationsRenderer of BaseOrderListPageRendererMixin', ex);

        }
    }
})