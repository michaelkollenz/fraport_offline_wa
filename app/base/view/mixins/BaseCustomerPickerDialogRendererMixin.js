﻿Ext.define('AssetManagement.base.view.mixins.BaseCustomerPickerDialogRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    customerPickerPartnerColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.StringUtils.trimStart(record.get('kunnr'), '0');;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerPickerPartnerColumnRenderer of BaseCustomerPickerDialogRendererMixin', ex);
        }
    },
    customerPickerNameColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                //store -> item -> aufnr, ktext
                var firstname = record.get('firstname');
                var lastname = record.get('lastname');
                var retVal = firstname;
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lastname))
                    retVal += ' ' + lastname;
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerPickerNameColumnRenderer of BaseCustomerPickerDialogRendererMixin', ex);
        }
    },
    customerPickerLocationColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                //store -> item -> aufnr, ktext
                var postlcode = record.get('postlcod1');
                var city = record.get('city');
                var retVal = postlcode + ' ' + city;
                return retVal;
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside customerPickerLocationColumnRenderer of BaseCustomerPickerDialogRendererMixin', ex);
        }
    }
})