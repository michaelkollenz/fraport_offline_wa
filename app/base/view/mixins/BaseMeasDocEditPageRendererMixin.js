﻿Ext.define('AssetManagement.base.view.mixins.BaseMeasDocEditPageRendererMixin', {
    extend: 'AssetManagement.customer.view.mixins.PageRendererMixin',
    //newMeasDocsGridPanel Renderers
    newMeasDocsActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/measdoc.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newMeasDocsActionColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    newMeasDocsReaderColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('readr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newMeasDocsReaderColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    newMeasDocsOrderComponents_QtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('recdc') + ' ' + record.get('unitr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newMeasDocsOrderComponents_QtyColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    newMeasDocsRemarkColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('mdtext');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newMeasDocsRemarkColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    // End of the newMeasDocsGridPanel Renderers

    //Begin of the histMeasDocGrid Renderers
    histMeasDocActionColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                var imageSrc = 'resources/icons/measdoc.png';
                return '<img src="' + imageSrc + '"  style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newMeasDocsActionColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    histMeasDocDateColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(record.get('idate'));
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside newMeasDocsReaderColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    histMeasDocReaderColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('readr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histMeasDocReaderColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    histMeasDocOrderComponents_QtyColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('recdc') + ' ' + record.get('unitr');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histMeasDocOrderComponents_QtyColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    },
    histMeasDocRemarkColumnRenderer: function () {
        try {
            var renderer = function (value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('mdtext');
            }
            return renderer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histMeasDocRemarkColumnRenderer of BaseMeasDocEditPageRendererMixin', ex);
        }
    }
    //End of the histMeasDocGrid Renderers
})