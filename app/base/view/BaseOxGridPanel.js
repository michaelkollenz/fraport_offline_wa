Ext.define('AssetManagement.base.view.BaseOxGridPanel', {
    extend: 'Ext.grid.Panel',

    
	requires: [
	   'AssetManagement.customer.utils.StringUtils',
	   'AssetManagement.customer.helper.EventHelper',
	   'AssetManagement.customer.helper.OxLogger'
	],

	config: {
		cls: 'oxGridPanel',
		emptyTextCache: '',
		useLoadingIndicator: false,
		loadingIndicator: '<div><div class="spinner loadingIndicatorForList"><div class="spinner-container container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div></div></div>',
		loadingText: '',
		scrollable: 'vertical',
		header: false,
		overlapHeader: true,
		forceFit: true,
		enableColumnHide: false,
		enableColumnMove: false,
	    viewConfig: {
	  		deferEmptyText: false,
	  		markDirty: false
	  	},
	  	keepSelection: false,
	  	hideContextMenuColumn: false,
	    variableRowHeight: true
	},
	
	_disableSelection: false,
	_loadingContent: '',
	_contextMenuListenerCallback: null,
	_contextMenuListenerCallbackScope: null,
	_cellClickListenerCallback: null,
	_cellClickListenerCallbackScope: null,
	
	constructor: function(config) {
		//manipulate config
		try {
			var cellClickCallback;
		
			if(config) {
				var columnsConfig = config.columns;
			
				if(columnsConfig && config.hideContextMenuColumn !== true) {
					var contextMenuColumnConfig = {
                        xtype: 'actioncolumn',
	                	text: Locale.getMsg('menu'),
	                	maxWidth: 80,
	                    minWidth: 80,
	                    items: [{
	                        icon: 'resources/icons/menu.png',
	                        tooltip: Locale.getMsg('contextMenu'),
	                        iconCls: 'oxGridLineActionButton',
	                        handler: this.onContextMenuButtonClick,
	                        scope: this
	                    }],
	                    enableColumnHide: false,
	                    align: 'center'
	                }
				
					columnsConfig.push(contextMenuColumnConfig);
				}
				
				if(config.listeners) {
					//extract and remove cellClickEventCallback
					cellClickCallback = config.listeners['cellclick'];
					
					var newListenersConfig = {};
					
					for(var property in config.listeners) {
					    if(config.listeners.hasOwnProperty(property)) {
					    
					    	if(property !== 'cellclick')
					    		newListenersConfig[property] = config.listeners[property];
					    }
					}
					
					config.listeners = newListenersConfig;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxGridPanel', ex);
		}
		
		//call constructor
		this.callParent(arguments);
		
		//do own constructor logic
		try {
			if(config) {
				this._disableSelection = config.disableSelection === true;
				
				if(config.listeners) {
					//extract contextMenuEventCallback
					var contextMenuCallback = config.listeners['cellcontextmenu'];
					var controller = this.getController();
					
					if(typeof(contextMenuCallback) === 'string') {
						//dissolve the callback
						controller = this.getNextSupportingController(contextMenuCallback);
						
						contextMenuCallback = controller ? controller[contextMenuCallback] : null;
					}
					
					this._contextMenuListenerCallback = contextMenuCallback;
					
					var contextMenuCallbackScope = config.listeners['scope'];
					this._contextMenuListenerCallbackScope = contextMenuCallbackScope ? contextMenuCallbackScope : controller;
					
					//manage former extracted cell click callback
					if(typeof(cellClickCallback) === 'string') {
						//dissolve the callback
						controller = this.getNextSupportingController(cellClickCallback);
						
						cellClickCallback = controller ? controller[cellClickCallback] : null;
					}
					
					this._cellClickListenerCallback = cellClickCallback;
					
					var cellClickCallbackScope = config.listeners['scope'];
					this._cellClickListenerCallbackScope = cellClickCallbackScope ? cellClickCallbackScope : controller;
				}
			}
			
			this.initialize();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxGridPanel', ex);
		}
	},
	
	//public
	//will "remove" the current store
	//if the grid panel uses a loading indicator, it will be set as the empty text again  
	reset: function() {
		try {
			if(this.getUseLoadingIndicator() === true) {
				this.getView().emptyText = this._loadingContent;
			}
			
			this.setStore(null);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reset of BaseOxGridPanel', ex);
		}
	},

	//protected
	//@override
	getNextSupportingController: function(nameOfSupportedCallback) {
		var retval = null;
	
		try {
			//check, if the grid panels controller supports the callback
			var myOwnController = this.getController();
			
			if(myOwnController)
				retval = myOwnController[nameOfSupportedCallback] ? myOwnController : null;
			
			//if there is no match, try to find the next controller 
			if(!retval) {
				var curParent = this.up();
				var curParentsController = curParent ? curParent.getController() : null;
				
				if(curParentsController)
					retval = curParentsController[nameOfSupportedCallback] ? curParentsController : null;
			
				while(curParent && !retval) {
					var curParent = curParent.up();
					var curParentsController = curParent ? curParent.getController() : null;
					
					if(curParentsController)
						retval = curParentsController[nameOfSupportedCallback] ? curParentsController : null;
				};
			}
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getController of BaseOxGridPanel', ex);
		}
		
		return retval;
	},

	//protected
	//@override
	setStore: function(toSet) {
		try {
			if(!this._keepSelection)
				this.getSelectionModel().deselectAll();
		
			if(!toSet) {
				//the base class does not accept null or undefined, so if there already is a store it has to be replaced with an empty store of the same model
				var myCurrentStore = this.getStore();
				
				if(myCurrentStore && myCurrentStore.getModel()) {
					toSet = Ext.create('Ext.data.Store', { model: myCurrentStore.getModel(), autoLoad: false });
				}
			} else if(this.getUseLoadingIndicator() === true) {
				//if this store uses an loading indicator, it's empty text now has to be set back to it's actual content
				this.getView().emptyText = this.getEmptyTextCache();
			}
			
			this.callParent([toSet]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setStore of BaseOxGridPanel', ex);
		}
	},
	
	//private
	//will trigger a context menu call using the current passed arguments
	onContextMenuButtonClick: function(grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
		try {
	        if(this._contextMenuListenerCallback) {
	        	this._contextMenuListenerCallback.call(this._contextMenuListenerCallbackScope, this, null, rowIndex, record, tableRow, rowIndex, event, null);
	        }
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuButtonClick of BaseOxGridPanel', ex);
		}
    },
	
	initialize: function() {
		try {
			this.addListener('beforecellcontextmenu', this.onBeforeCellContextMenu, this);
			this.addListener('beforecellmouseup', this.onBeforeCellMouseUp, this);
			this.addListener('beforecellclick', this.onBeforeCellClick, this);
			this.addListener('cellclick', this.onCellClick, this);
//			this.addListener('resize', this.onResize, this);
			
			this.initializeLoadingFeedbackFeature();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseOxGridPanel', ex);
		}
	},
	
	_ignoreNextCellClickIfTouch: false,
	//this handler will be called after the handlers registering on this event via config
	//manages the ignoreNextCellClickIfTouch for cases of noninitiation of a cellclick event
	onBeforeCellMouseUp: function(me, td, cellIndex, record, tr, rowIndex, event, eOpts) {
		var retval = true;
	
		try {
			if(this._ignoreNextCellClickIfTouch === true) {
				//it's not sure, if a cellclickw ill also be triggered
				//if so, a immediately reset of the flag would cause the cellclick to get not blocked
				//so the reset is queued
				Ext.defer(function() { this._ignoreNextCellClickIfTouch = false; }, 50, this);
				
				retval = !AssetManagement.customer.helper.EventHelper.hasEventBeenCausedByTouch(event);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBeforeCellMouseUp of BaseOxGridPanel', ex);
		}
		
		return retval;
	},
	
	//this handler will be called after the handlers registering on this event via config
	//stops default behavior for right mouse button clicks - do not stop event propagation, this will break context menu triggering
	//checks, if the next cell click has to be ignored, which is a touch only case
	onBeforeCellClick: function(me, td, cellIndex, record, tr, rowIndex, event, eOpts) {
		var retval = true;
	
		try {
			if(AssetManagement.customer.helper.EventHelper.hasEventBeenCausedByRightClick(event)) {
				retval = false;
			} else if(this._ignoreNextCellClickIfTouch === true) {
				retval = !AssetManagement.customer.helper.EventHelper.hasEventBeenCausedByTouch(event);
			}
			
			this._ignoreNextCellClickIfTouch = false;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBeforeCellClick of BaseOxGridPanel', ex);
		}
		
		return retval;
	},
	
	//this handler will be called BEFORE the handler which has registered on this event via config
	//performs checks on the event, if the registered event may be called
	onCellClick: function(me, td, cellIndex, record, tr, rowIndex, event, eOpts) {
		var retval = true;
	
		try {
			if(!event.isStopped && this._cellClickListenerCallback) {
				this._cellClickListenerCallback.call(this._cellClickListenerCallbackScope, me, td, cellIndex, record, tr, rowIndex, event, eOpts);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBeforeCellClick of BaseOxGridPanel', ex);
		}
		
		return retval;
	},
	
	//this handler will be called after the handlers registering on this event via config
	//undoes a potential triggered selection
	//marks the next cellclick event to be ignored, but this is only for touch events
	onBeforeCellContextMenu: function(me, td, cellIndex, record, tr, rowIndex, event, eOpts) {
		try {
			this._ignoreNextCellClickIfTouch = true;
			
			if(!this._disableSelection && !this.getKeepSelection())
				this.getSelectionModel().deselectAll();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBeforeCellContextMenu of BaseOxGridPanel', ex);
		}
	},
	
//	onResize: function(newWidth, newHeight, oldWidth, oldHeight) {
//		try {
//			if(this.columns) {
//				Ext.defer(this.setFixColumnWidths, 1, this, [ newWidth ])
//			}
//		} catch(ex) {
//			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onResize of BaseOxGridPanel', ex);
//		}
//	},
	
	initializeLoadingFeedbackFeature: function() {
		try {
			var hasLoadingIndicator = this.getUseLoadingIndicator() === true;
			var hasLoadingText = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getLoadingText());
			
			if(hasLoadingIndicator || hasLoadingText) {
				this.setEmptyTextCache(this.getView().emptyText);
				
				var toSet = '';
				
				if(hasLoadingIndicator === true) {
					toSet = this.getLoadingIndicator();
				}
				
				if(hasLoadingText === true) {
					var classToUse = hasLoadingIndicator === true ? 'loadingMessageForListWithIndicator' : 'loadingMessageForListWithoutIndicator';
					var loadingDiv = '<div class="' + classToUse + '">' + this.getLoadingText() + '</div>';
					
					if(hasLoadingIndicator === true) {
						//insert the loading div before the last closing div tag of the loading indicator hmtl
						//alternative would be behind the first opening div tag
						toSet = AssetManagement.customer.utils.StringUtils.insert(toSet, toSet.lastIndexOf('</div>'), loadingDiv);
					} else {
						toSet = loadingDiv;
					}
				}
				
				this.getView().emptyText = toSet;
			}
			
			this._loadingContent = toSet;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildLoadingContent of BaseOxGridPanel', ex);
		}
	},
	
	setFixColumnWidths: function(newWidth) {
		try {
			var widthToShare = newWidth - this.getColumnsFixedWidth(); 
			var divisor = this.getColumnsFlexSum();
			
			if(divisor === 0)
				return;
				
			Ext.suspendLayouts();				

			if(this.getColumnCount() > 0) {
				Ext.Array.each(this.columns, function(column) {
					if(column.flex) {
						column.setMaxWidth(widthToShare * column.flex / divisor);
					}
				}, this);
			}
			
			Ext.resumeLayouts(true);
			AssetManagement.customer.helper.OxLogger.logMessage('SET COLUMN WIDTHS ON GRID PANEL DONE.');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFixColumnWidths of BaseOxGridPanel', ex);
		}
	},
	
	getColumnCount: function() {
		var retval = 0;
		
		try {
			if(this.columns)
				retval = this.columns.length;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getColumnCount of BaseOxGridPanel', ex);
			retval = 0;
		}
		
		return retval;
	},
	
	getColumnsFlexSum: function() {
		var retval = 0;
	
		try {
			var myColumns = this.columns;
			
			if(myColumns && myColumns.length > 0) {
				Ext.Array.each(myColumns, function(column) {
					if(column.flex)
						retval += column.flex;
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getColumnsFlexSum of BaseOxGridPanel', ex);
			retval = 0;
		}
		
		return retval;
	},
	
	getColumnsFixedWidth: function() {
		var retval = 0;
			
		try {
			var myColumns = this.columns;
			
			if(myColumns && myColumns.length > 0) {
				Ext.Array.each(myColumns, function(column) {
					if(!column.flex)
						retval += column.maxWidth;
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getColumnsFixedWidth of BaseOxGridPanel', ex);
			retval = 0;
		}
		
		return retval;
	}
});