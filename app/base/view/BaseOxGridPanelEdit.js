Ext.define('AssetManagement.base.view.BaseOxGridPanelEdit', {
    extend: 'Ext.grid.Panel',

    
	//protected
	//@override
	setStore: function(toSet) {
		try {
			
			this.callParent([toSet]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setStore of BaseOxGridPanelEdit', ex);
		}
	}
});