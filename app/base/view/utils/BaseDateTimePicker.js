﻿Ext.define('AssetManagement.base.view.utils.BaseDateTimePicker', {
    extend: 'Ext.form.field.Date',

    requires: [
        'AssetManagement.customer.view.utils.DatePicker',
        'AssetManagement.customer.utils.DateTimeUtils'
    ],

    format: "d/m/Y H:i",

    altFormats: "d/m/Y H:i",

    okText: 'Ok',
    nowText: 'Now',
    todayText: 'Today',

    createPicker: function () {
        var me = this, format = Ext.String.format;

        return new AssetManagement.customer.view.utils.DatePicker({
            okText: me.okText,
            nowText: me.nowText,
            todayText: me.todayText,
            pickerField: me,
            floating: true,
            preventRefocus: true,
            hidden: true,
            minDate: me.minValue,
            maxDate: me.maxValue,
            disabledDatesRE: me.disabledDatesRE,
            disabledDatesText: me.disabledDatesText,
            ariaDisabledDatesText: me.ariaDisabledDatesText,
            disabledDays: me.disabledDays,
            disabledDaysText: me.disabledDaysText,
            ariaDisabledDaysText: me.ariaDisabledDaysText,
            format: me.format,
            showToday: me.showToday,
            startDay: me.startDay,
            minText: format(me.minText, me.formatDate(me.minValue)),
            ariaMinText: format(me.ariaMinText, me.formatDate(me.minValue, me.ariaFormat)),
            maxText: format(me.maxText, me.formatDate(me.maxValue)),
            ariaMaxText: format(me.ariaMaxText, me.formatDate(me.maxValue, me.ariaFormat)),
            editable: true,

            listeners: {
                scope: me,
                select: me.onSelect
            },

            keyNavConfig: {
                esc: function () {
                    me.inputEl.focus();
                    me.collapse();
                }
            }
        });
    },

    onExpand: function () {
        var value = this.getValue();
        this.picker.setValue(Ext.isDate(value) ? value : AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(), true);
    }
});