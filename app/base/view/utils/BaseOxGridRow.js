Ext.define('AssetManagement.base.view.utils.BaseOxGridRow', {
    extend: 'Ext.container.Container',


    requires: [
        'AssetManagement.customer.view.utils.OxGridRowViewModel',
        'AssetManagement.customer.helper.OxLogger'
    ],

    config: {
        rowId: null,
        templateType: 'custom',
        label: null,
        template: null,
        owningGrid: null
    },

    viewModel: {
        type: 'utilsoxgridrow'
    },

    constructor: function(config) {
        try {
        	this.callParent(arguments);
        
	        if(this.templateType === 'custom' || this.templateType === 'multilinelabel') {
	        	var isMultiLabel = this.templateType === 'multilinelabel';
	            this.template.addListener('resize', this.centerLabelString, this, [isMultiLabel]);
	        }
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxGridRow', ex);
		}
    },

    hide: function() {
    	try {
	        if(!this.getLabel().isHidden()) {
	        	this.getLabel().setHidden(true);
		    	this.getTemplate().setHidden(true);
	
		    	this.getOwningGrid().onRowIsHidden(this.getRowId());
	        }
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hide of BaseOxGridRow', ex);
		}
    },

    show: function() {
    	try {
	        if(this.getLabel().isHidden()) {
	        	this.getLabel().setHidden(false);
				this.getTemplate().setHidden(false);
	
				this.getOwningGrid().onRowIsShown(this.getRowId());
			}
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside show of BaseOxGridRow', ex);
		}
    },

    setContentString: function(string) {
    	try {
	        if(this.templateType === 'label' || this.templateType === 'multilinelabel') {
	        	this.template.setHtml(string);
	        } else if(this.templateType === 'link') {
	        	this.template.setText(string);
	        }
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setContentString of BaseOxGridRow', ex);
		}
    },

    setDataForCustomTemplate: function(data) {
    	try {
    		this.template.items.items[0].updateData(data, this.label.text);
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDataForCustomTemplate of BaseOxGridRow', ex);
		}
    },

    centerLabelString: function(isMultiLabel) {
    	try {
	        //determine the templates current height
			var templatesHeight = this.template.getHeight();
	
			//set the labels size equivalent
			this.label.setHeight(templatesHeight);
	
			var scalingFactor = (templatesHeight + 82) / 164.0;
			
			//set the innerHtmls line height to x/2 + textHeight + label padding
			var textHeight = AssetManagement.customer.utils.StringUtils.getTextHeight(this.label.text, 'normal 12pt helvetica');
			var targetLineHeight = (templatesHeight/2 + textHeight + 7.5) * scalingFactor;

			if(isMultiLabel){
				targetLineHeight -= 10;
			} else {
				targetLineHeight += 5;
			}

			this.label.getEl().dom.style.lineHeight = targetLineHeight + 'px';
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside centerLabelString of BaseOxGridRow', ex);
		}
    }
});