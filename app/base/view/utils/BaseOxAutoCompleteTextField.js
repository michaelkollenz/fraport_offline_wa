﻿Ext.define('AssetManagement.base.view.utils.BaseOxAutoCompleteTextField', {
    extend: 'AssetManagement.customer.view.utils.OxComboBox',


    requires: [
        'AssetManagement.customer.utils.StringUtils'
    ],

    config: {
        typeAhead: true,
        hideTrigger: true,
        enableKeyEvents: true,
        queryMode: 'local',
        includeSubMatches: false
    },

    constructor: function (config) {
        try {
            this.callParent(arguments);

            this.initializeEventHandling();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxAutoCompleteTextField', ex);
        }
    },

    //private
    //register beforequery event to also include suggestions containing the current input (startsWith ---> contains)
    initializeEventHandling: function () {
        try {
            if (this.getIncludeSubMatches()) {
                this.addListener('beforequery', function (queryPlan) {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(queryPlan.query)) {
                        //needed in order to have a valid regex expression if there is only one bracket available
                        if (queryPlan.query.indexOf('(') > -1 && queryPlan.query.indexOf(')')  ===  -1) {
                            queryPlan.query = queryPlan.query.replace('(', '\\(');
                        } else if (queryPlan.query.indexOf('{') > -1 && queryPlan.query.indexOf('}') === -1) {
                            queryPlan.query = queryPlan.query.replace('{', '\\{');
                        } else if (queryPlan.query.indexOf('[') > -1 && queryPlan.query.indexOf(']') === -1) {
                            queryPlan.query = queryPlan.query.replace('(', '\\[');
                        }
						queryPlan.query = new RegExp(queryPlan.query, 'i');
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeEventHandling of BaseOxAutoCompleteTextField', ex);
        }
    },

    //protected
    //@override
    //necessary override for a bug fix (ExtJS 6.0.0) on occasionally losing correct store reference
    onValueCollectionEndUpdate: function () {
        try {

            var me = this,
                store = me.store,
                selectedRecords = me.valueCollection.getRange(),
                selectedRecord = selectedRecords[0],
                selectionCount = selectedRecords.length;
            me.updateBindSelection(me.pickerSelectionModel, selectedRecords);
            if (me.isSelectionUpdating()) {
                return;
            }
            Ext.suspendLayouts();
            me.lastSelection = selectedRecords;
            if (selectionCount) {
                // Track the last selection with a value (non blank) for use in
                // assertValue
                me.lastSelectedRecords = selectedRecords;
            }
            me.updateValue();
            // If we have selected a value, and it's not possible to select any more values
            // or, we are configured to hide the picker each time, then collapse the picker.

            //BUG FIX
            store = me.store;

            if (selectionCount && ((!me.multiSelect && me.store.contains(selectedRecord)) || me.collapseOnSelect || !store.getCount())) {
                me.updatingValue = true;
                me.collapse();
                me.updatingValue = false;
            }
            Ext.resumeLayouts(true);
            if (selectionCount && !me.suspendCheckChange) {
                if (!me.multiSelect) {
                    selectedRecords = selectedRecord;
                }
                me.fireEvent('select', me, selectedRecords);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onValueCollectionEndUpdate of BaseOxAutoCompleteTextField', ex);
        }
    }
});