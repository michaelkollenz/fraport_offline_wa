Ext.define('AssetManagement.base.view.utils.BaseChecklistTemplate', {
    extend: 'Ext.Container',
    
	requires: [
	   'Ext.form.Label',
	   'Ext.button.Button',
	   'AssetManagement.customer.utils.StringUtils',
	   'AssetManagement.customer.manager.AddressManager',
	   'AssetManagement.customer.helper.NetworkHelper',
	   'AssetManagement.customer.helper.OxLogger',
	   'AssetManagement.customer.model.helper.ReturnMessage'
	],
    
    config: {
        cls: 'oxGridChecklistRowContainer',
		layout: { type: 'hbox', align: 'center' }
	},
	
	//private
	_data: null,
	_labelContainer: null,
	_label: null,
	_checklistButton: null,
    _reportButton: null,
	_object: null,
	
	//public
	constructor: function(config) {
		this.callParent(arguments);
		
		try {
		    this.initializeComponent();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseChecklistTemplate', ex);
		}    
	},
	
	updateData: function(data, object) {
		try {
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(Ext.getClassName(data))) {
				this._data = null;
			} else {
			    this._data = data;
			    this._object = object;
			}
				
			this.refreshContent();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateData of BaseChecklistTemplate', ex);
		}
	},
	
	//private
	initializeComponent: function() {
	    try {
	        this._label = Ext.create('Ext.button.Button', {
	            flex: 1,
	            cls: 'oxGridChecklistContentLink',
	            handler: this.navigateToObject,
	            scope: this
	        });

	        this._reportButton = Ext.create('Ext.button.Button', {
	            docked: 'right',
	            //cls: 'oxChecklistReport',
	            html: '<img width="100%" src="resources/icons/report.png"></img>',
	            handler: this.printChecklistReport,
	            scope: this,
	            height: 60,
	            width: 60,
	            icon: '',
	            text: ''
	        });

	        this._checklistAddButton = Ext.create('Ext.button.Button', {
	            docked: 'right',
	            html: '<img width="100%" src="resources/icons/documentAdd.png"></img>',
	            //cls: 'oxChecklistIcon',
	            handler: this.navigateToChecklist,
	            scope: this,
	            height: 60,
	            width: 60,
	            icon: '',
	            text: ''
	        });

	        this._checklistButton = Ext.create('Ext.button.Button', {
	            docked: 'right',
	            html: '<img width="100%" src="resources/icons/document.png"></img>',
	            //cls: 'oxChecklistIcon',
	            handler: this.navigateToChecklist,
	            scope: this,
	            height: 60,
	            width: 60,
	            icon: '',
	            text: ''
	        });

	        this._checklistCompleteButton = Ext.create('Ext.button.Button', {
	            docked: 'right',
	            html: '<img width="100%" src="resources/icons/documentOk.png"></img>',
	            //cls: 'oxChecklistIcon',
	            handler: this.navigateToChecklist,
	            scope: this,
	            height: 60,
	            width: 60,
	            icon: '',
	            text: ''
	        });

			this.add(this._label);
			this.add(this._reportButton);
			this.add(this._checklistButton);
			this.add(this._checklistAddButton);
			this.add(this._checklistCompleteButton);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeComponent of BaseChecklistTemplate', ex);
		}
	},
	
	refreshContent: function() {
		try {
			var label = this._data ?  this._data.get('text') : '';
            
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(label)) {
			    this._label.setText(label);
			    this._label.show();
			} else {
			    this._label.hide();
			}

			var qplos = this._data ? this._data.get('qplos') : null;
			var qplosCompleted = qplos ? qplos.isCompleted() : false;

			if (qplos) {
			    if (qplosCompleted) {
			        this._reportButton.show();
			        this._checklistButton.hide();
			        this._checklistAddButton.hide();
			        this._checklistCompleteButton.show();
			    }
			    else {
			        this._checklistButton.show();
			        this._checklistAddButton.hide();
			        this._checklistCompleteButton.hide();
			        this._reportButton.hide();
			    }
			} else if (this._data.get('matnr')) {
			        this._checklistButton.hide();
			        this._checklistAddButton.show();
			        this._checklistCompleteButton.hide();
			        this._reportButton.hide();
			} else {
			    this._checklistButton.hide();
			    this._checklistAddButton.hide();
			    this._checklistCompleteButton.hide();
			    this._reportButton.hide();
			}
        } catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshContent of BaseChecklistTemplate', ex);
		}
	},

	navigateToObject: function()
	{
	    try {
	        var controller = this._data.get('controller');

	        if (this._object == Locale.getMsg('equipment'))
	            controller.navigateToEquipment();
	        else
	            controller.navigateToFuncLoc();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToObject of BaseChecklistTemplate', ex);
	    }
	},

	navigateToChecklist: function ()
	{
	    try {
	        var controller = this._data.get('controller');
	        controller.navigateToChecklist();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToHeaderChecklist of BaseChecklistTemplate', ex);
	    }
	},

	printChecklistReport: function () {
	    try {
	        var controller = this._data.get('controller');
	        controller.printChecklistReport();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printChecklistReport of BaseChecklistTemplate', ex);
	    }
	}
});