Ext.define('AssetManagement.base.view.utils.BaseOxTextField', {
    extend: 'Ext.form.field.Text',

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.ClassHelper'
    ],

    config: {
        disabledCls: 'oxDisabledTextField',
        enforceMaxLength: true
    },

    //buffer for unmasked value of read only (read only value of base class may be masked by a workaround for disabling the control)
    _unmaskedReadOnly: false,

    constructor: function (config) {
        try {
            var disabled = false;

            if (config) {
                if (config.readOnly === true) {
                    this._unmaskedReadOnly = true;
                }

                if (config.disabled === true) {
                    disabled = true;
                    config.disabled = false;
                }
            }

            this.callParent(arguments);

            if (disabled) {
                this.setDisabled(true);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxTextField', ex);
        }
    },

    //@override
    //transfers the passed value into the unmasked buffer
    setReadOnly: function (readOnly) {
        try {
            readOnly = readOnly === true;

            this._unmaskedReadOnly = readOnly;

            this.callParent([readOnly]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setReadOnly of BaseOxTextField', ex);
        }
    },

    //@override
    //some browsers have non overrideable effects on disabled controls
    //to avoid this effect, do not disable the control, but add the disabled css class and set the input to read only
    setDisabled: function (disabled) {
        try {
            var superClass = AssetManagement.customer.helper.ClassHelper.getExtSuperClass(this);

            if (disabled === true) {
                //call super method of set read only, to keep the unmasked read only flag untouched
                superClass.setReadOnly.call(this, true);

                this.addCls(this.config.disabledCls);
            } else {
                //apply the unmasked read only state by calling super method of set read only
                superClass.setReadOnly.call(this, this._unmaskedReadOnly);

                this.removeCls(this.config.disabledCls);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDisabled of BaseOxTextField', ex);
        }
    }
});