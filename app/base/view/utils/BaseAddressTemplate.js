Ext.define('AssetManagement.base.view.utils.BaseAddressTemplate', {
    extend: 'Ext.Container',
    
	requires: [
	   'Ext.form.Label',
	   'Ext.button.Button',
	   'AssetManagement.customer.utils.StringUtils',
	   'AssetManagement.customer.manager.AddressManager',
	   'AssetManagement.customer.helper.NetworkHelper',
	   'AssetManagement.customer.helper.OxLogger',
	   'AssetManagement.customer.model.helper.ReturnMessage'
	],
    
    config: {
		cls: 'oxAddressTemplate', //default appearance
		layout: { type: 'hbox', align: 'center' }
	},
	
	//private
	_data: null,
	_mapsHint: '',
	_addressLabelContainer: null,
	_nameLabel: null,
	_streetLabel: null,
	_cityLabel: null,
	_telNumberLabel: null,
	_mobNumberLabel: null,
	_mapsButton: null,
	
	//public
	constructor: function(config) {
		this.callParent(arguments);
		
		try {
		    this.initializeComponent();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseAddressTemplate', ex);
		}    
	},
	
	updateData: function(address, mapsHint) {
		try {
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(Ext.getClassName(address))) {
				this._data = null;
			} else {
				this._data = address;
				this._mapsHint = mapsHint;
			}
				
			this.refreshContent();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateData of BaseAddressTemplate', ex);
		}
	},
	
	//private
	initializeComponent: function() {
		try {
			this._addressLabelContainer = Ext.create('Ext.Container', { layout: { type: 'vbox', pack: 'center', align: 'stretch' }, flex: 1 });
			this._nameLabel = Ext.create('Ext.form.Label', { flex: 1, cls: 'oxAddressLabel' });
			this._streetLabel = Ext.create('Ext.form.Label', { flex: 1, cls: 'oxAddressLabel' });
			this._cityLabel = Ext.create('Ext.form.Label', { flex: 1, cls: 'oxAddressLabel' });
			this._telNumberLabel = Ext.create('Ext.form.Label', { flex: 1, cls: 'oxAddressLabel' });
			this._mobNumberLabel = Ext.create('Ext.form.Label', { flex: 1, cls: 'oxAddressLabel' });
	
			this._addressLabelContainer.add(this._nameLabel);
			this._addressLabelContainer.add(this._streetLabel);
			this._addressLabelContainer.add(this._cityLabel);
			this._addressLabelContainer.add(this._telNumberLabel);
			this._addressLabelContainer.add(this._mobNumberLabel);
			
			this._mapsButton = Ext.create('Ext.button.Button', {
				cls: 'oxMapsButton',
				docked: 'right',
				handler: this.openAddress,
				scope: this,
				pressedCls: 'buttonPressed',
				html: '<img src="resources/icons/maps_small.png"></img>'
			});
			
			this.add(this._addressLabelContainer);
			this.add(this._mapsButton);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeComponent of BaseAddressTemplate', ex);
		}
	},
	
	refreshContent: function() {
		try {
			var name = '';
			var street = '';
			var city = '';
			var telNumber = '';
			var mobNumber = '';

			if(this._data) {
				name = this._data.getName();
				street = AssetManagement.customer.utils.StringUtils.concatenate([ this._data.getStreet(), this._data.getHouseNo() ], null, true);
				city = AssetManagement.customer.utils.StringUtils.concatenate([ this._data.getPostalCode(), this._data.getCity() ], null, true);
				telNumber = this._data.get('telnumber');
				mobNumber = this._data.get('telnumbermob');
			}
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(name)) {
				this._nameLabel.setHtml(name);
				this._nameLabel.show();
			} else {
				this._nameLabel.hide();
			}
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(street)) {
				this._streetLabel.setHtml(street);
				this._streetLabel.show();
			} else {
				this._streetLabel.hide();
			}
					
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(city)) {
				this._cityLabel.setHtml(city);
				this._cityLabel.show();
			} else {
				this._cityLabel.hide();
			}
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telNumber)) {
				this._telNumberLabel.setHtml(telNumber);
				this._telNumberLabel.show();
			} else {
				this._telNumberLabel.hide();
			}
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobNumber)) {
				this._mobNumberLabel.setHtml(mobNumber);
				this._mobNumberLabel.show();
			} else {
				this._mobNumberLabel.hide();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshContent of BaseAddressTemplate', ex);
		}
	},
	
	openAddress: function() {
		try {
			var address = this._data;
			
			if(address && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(address.getCity()) && 
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(address.getPostalCode())) {
						var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
							type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
						});
						
						//generate the hint					
						hint = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._mapsHint) ? this._mapsHint : '';

						request.addAddressWithHint(address, hint);

						AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openAddress of BaseAddressTemplate', ex);
		}
	}
});