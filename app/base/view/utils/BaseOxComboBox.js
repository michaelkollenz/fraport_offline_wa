Ext.define('AssetManagement.base.view.utils.BaseOxComboBox', {
    extend: 'Ext.form.field.ComboBox',

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.ClassHelper'
    ],

    config: {
        disabledCls: 'oxDisabledComboBox'
    },

    //private
    _unmaskedReadOnly: false,
    _unmaskedHideTrigger: false,

    constructor: function (config) {
        try {
            var disabled = false;

            if (config) {
                if (config.readOnly === true) {
                    this._unmaskedReadOnly = true;
                }

                if (config.hideTrigger === true) {
                    this._unmaskedHideTrigger = true;
                }

                if (config.disabled === true) {
                    disabled = true;
                    config.disabled = false;
                }
            }

            this.callParent(arguments);

            if (disabled) {
                this.setDisabled(true);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxComboBox', ex);
        }
    },

    //@override
    //transfers the passed value into the unmasked buffer
    setReadOnly: function (readOnly) {
        try {
            readOnly = readOnly === true;

            this._unmaskedReadOnly = readOnly;

            this.callParent([readOnly]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setReadOnly of BaseOxComboBox', ex);
        }
    },

    //@override
    //transfers the passed value into the unmasked buffer
    setHideTrigger: function (hideTrigger) {
        try {
            hideTrigger = hideTrigger === true;

            this._unmaskedHideTrigger = hideTrigger;

            this.callParent([hideTrigger]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setHideTrigger of BaseOxComboBox', ex);
        }
    },

    //@override
    //some browsers have non overrideable effects on disabled controls
    //to avoid this effect, do not disable the control, but add the disabled css class and set the input to read only
    setDisabled: function (disabled) {
        try {
            var superClass = AssetManagement.customer.helper.ClassHelper.getExtSuperClass(this);

            if (disabled === true) {
                //call super method of set read only, to keep the unmasked read only flag untouched
                superClass.setReadOnly.call(this, true);
                //call super method of set hide trigger, to keep the unmasked hide trigger flag untouched
                superClass.setHideTrigger.call(this, true);

                this.addCls(this.config.disabledCls);
            } else {
                //apply the unmasked read only state by calling super method of set read only
                superClass.setReadOnly.call(this, this._unmaskedReadOnly);
                //apply the unmasked hide trigger state by calling super method of hide trigger
                superClass.setHideTrigger.call(this, this._unmaskedHideTrigger);

                this.removeCls(this.config.disabledCls);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDisabled of BaseOxComboBox', ex);
        }
    },

    //@override
    //will try to find a record by evaluating the value property
    findRecordByValue: function (valueToSearchFor) {
        var retval = null;

        try {
            retval = this.findRecord(this.valueField, valueToSearchFor);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside findRecordByValue of BaseOxComboBox', ex);
        }

        return retval;
    }
});