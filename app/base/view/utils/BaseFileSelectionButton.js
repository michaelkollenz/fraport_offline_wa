Ext.define('AssetManagement.base.view.utils.BaseFileSelectionButton', {
    extend: 'Ext.Component',

    
	requires: [
	   'AssetManagement.customer.helper.FileHelper',
	   'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
		cls: 'oxFileSelectButton',
	    labelText: '',
	    textAlignment: 'left',
	    selectionCallback: false,
	    selectionCallbackScope: null,
	    height: 38,
	    padding: 0,
	    imageSrc: 'resources/icons/add.png',
	    useEdgedCorners: false
	},
	
	_buttonPadding: '0 5 0 5',
	
    constructor: function(config) {
		try {
			var paddingBuffer = 0;
		
			if(config) {
				if(config.padding)
					this._buttonPadding = config.padding + '';
			}
			
			config.padding = 0;
			
			this.callParent(arguments);
			
			this.buildUiContent();
			
			this.addListener('boxready', this.setUpFileListener, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseFileSelectionButton', ex);
		}
	},
	
	buildUiContent: function() {
		try {
			var divCSS = 'position: relative; overflow: hidden; padding:' + this.getButtonPadding() + '; height: ' + this.height + 'px;' + 'text-align: ' + this.getTextAlignment() + ';';
			
			if(this.getUseEdgedCorners())
				divCSS += ' border-radius: 0px;';
			
			var inputCSS = 'position: absolute;	top: 0; right: 0; margin: 0; padding: 0; font-size: 25px; cursor: pointer; opacity: 0; filter: alpha(opacity=0);';
			inputCSS += 'height: ' + this.height + 'px;';
			
			var htmlContent = '<div class="oxFileUploadDiv" style="' + divCSS +'">'
					
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getImageSrc()))
				htmlContent +='<img src="' + this.getImageSrc()  +'" style="vertical-align: middle;" height="' + (this.height - this.getPaddingHeightRequirements()) +'"/>';
					
			htmlContent += '<input type="file" id="oxFileUploadInput-' + this.getId() + '" class="oxFileUploadInput" style="' + inputCSS + '"/></div>';
		
			this.setHtml(htmlContent);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUiContent of BaseFileSelectionButton', ex);
		}
	},
	
	getButtonPadding: function() {
		var retval = '0px';
		
		try {
			var values = this._buttonPadding.split(' ');
			
			if(values && values.length > 0) {
				retval = '';
			
				Ext.Array.each(values, function(value) {
					retval += value + 'px ';
				}, this);
				
				retval.trim();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getButtonPadding of BaseFileSelectionButton', ex);
			retval = '0px';
		}

		return retval;
	},
	
	getPaddingHeightRequirements: function() {
		var retval = 0;
		
		try {
			var values = this._buttonPadding.split(' ');
			
			if(values && values.length > 0) {
				switch(values.length) {
					case 1:
						retval = parseFloat(values[0]) * 2;
						break;
					case 2:
						retval = parseFloat(values[0]) * 2;
						break;
					case 4:
						retval = parseFloat(values[0]) + parseFloat(values[2]);
						break;
					default:
						break;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPaddingHeightRequirements of BaseFileSelectionButton', ex);
			retval = 0;
		}

		return retval;
	},
	
	getPaddingWidthRequirements: function() {
		var retval = 0;
		
		try {
			var values = this._buttonPadding.split(' ');
			
			if(values && values.length > 0) {
				switch(values.length) {
					case 1:
						retval = parseFloat(values[0]) * 2;
						break;
					case 2:
						retval = parseFloat(values[1]) * 2;
						break;
					case 4:
						retval = parseFloat(values[1]) + parseFloat(values[3]);
						break;
					default:
						break;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPaddingWidthRequirements of BaseFileSelectionButton', ex);
			retval = 0;
		}

		return retval;
	},
	
	setUpFileListener: function() {
		try {
			var queryResult = this.getEl().query('#oxFileUploadInput-' + this.getId());
			var htmlInputElement = (queryResult && queryResult.length > 0) ? queryResult[0] : null;
			
			if(htmlInputElement) {
				var me = this;
				
				htmlInputElement.addEventListener('change', function(args) { me.onFileSelected.call(me, args, this); }, false);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUpFileListener of BaseFileSelectionButton', ex);
		}
	},
	
	onFileSelected: function(args, inputElement) {
		try {
			var fileHandle = null;
			if (!AssetManagement.customer.controller.ClientStateController.isClientInLoadingState()) {
                AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
		    }
			if(args && args.target && args.target.files && args.target.files.length > 0)
				fileHandle = args.target.files[0];
				
			if(!fileHandle) {
				this.callSelectionCallback(null);
			} else {
				AssetManagement.customer.helper.FileHelper.getFileFromFileHandle(fileHandle, this.callSelectionCallback, this);
				inputElement.value = null;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileSelected of BaseFileSelectionButton', ex);
		}
	},
	
	callSelectionCallback: function(file) {
		try {
			var scope = this.getSelectionCallbackScope();
			if (AssetManagement.customer.controller.ClientStateController.isClientInLoadingState()) {
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		    }
			this.getSelectionCallback().call(scope ? scope : this, file);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callSelectionCallback of BaseFileSelectionButton', ex);
		}
	}
});