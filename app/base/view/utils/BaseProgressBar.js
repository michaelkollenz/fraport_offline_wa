Ext.define('AssetManagement.base.view.utils.BaseProgressBar', {
    extend: 'Ext.container.Container',

    requires: [
       'AssetManagement.customer.helper.OxLogger',
	   'Ext.Component',
       'Ext.ProgressBar'
    ],

    config: {
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        cls: 'oxProgressBar',
        parent: null,
        border: 1,
        style: 'border-color: #b0b0b0; border-style: solid; border-radius: 3px;'
    },

    //private
    _infinite: false,
    _interval: 50,
    _increment: 1,
    _marginLeftRight: 0,

    constructor: function (config) {
        try {
            this.callParent(config);

            this._parent = config.parent;
            this._infinite = config.infinite === true;

            if (config.interval)
                this._interval = config.interval

            if (config.increment)
                this._increment = config.increment;

            this._marginLeftRight = config.marginLeftRight;

            this.initializeComponent();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseProgressBar', ex);
        }
    },

    //public
    setProgress: function (percentage) {
        try {
            if (!this._infinite) {
                this._progressBar.updateProgress(percentage / 100.0);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setProgress of BaseProgressBar', ex);
        }
    },

    //public
    startProgressBar: function () {
        try {
            if (this._infinite) {
                this._progressBar.wait({
                    interval: this._interval,
                    increment: 100 / this._increment,
                    text: '',
                    animate: false
                });
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startProgressBar of BaseProgressBar', ex);
        }
    },

    //public
    stopProgressBar: function () {
        try {
            if (this._infinite) {
                this._progressBar.reset();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stopProgressBar of BaseProgressBar', ex);
        }
    },

    initializeComponent: function () {
        try {
            var marginRequirements = 0;
            var marginToSet = "0px";

            if (this._marginLeftRight) {
                marginRequirements += 2 * this._marginLeftRight;
                marginToSet += " " + this._marginLeftRight + "px";
            }

            this.setMargin(marginToSet);
            this.setWidth(this._parent.width - marginRequirements - 22);
            this.setHeight(32);

            this._progressBar = Ext.create('Ext.ProgressBar', {
                flex: 1
            });

            //using the text attribute @config currently does not work (ExtJS 6.0.0)
            this._progressBar.updateText('');

            this.add(this._progressBar);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeComponent of BaseProgressBar', ex);
        }
    }
});