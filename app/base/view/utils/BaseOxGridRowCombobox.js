Ext.define('AssetManagement.base.view.utils.BaseOxGridRowCombobox', {
    extend: 'Ext.Container',
    
	requires: [
	   'Ext.form.Label',
	   'Ext.button.Button',
	   'Ext.form.field.ComboBox',
	   'AssetManagement.customer.utils.StringUtils',
	   'AssetManagement.customer.helper.NetworkHelper',
	   'AssetManagement.customer.helper.OxLogger',
	   'AssetManagement.customer.model.helper.ReturnMessage'
	],
    
    config: {
		cls: 'oxGridContentCombobox', //default appearance
		layout: { type: 'hbox' },
		height: 30
	},
	
	//private
	_data: null,
	_dataContainer: null,
	_combobox: null,
	
	//public
	constructor: function(config) {
		this.callParent(arguments);
		
		try {
		    this.initializeComponent();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxGridRowCombobox', ex);
		}    
	},
	
	//private
	initializeComponent: function() {
		try {
			this._dataContainer = Ext.create('Ext.Container', { layout: { type: 'vbox', pack: 'center', align: 'stretch' }, flex: 1, cls: 'oxGridContentCombobox' });
			this._combobox = Ext.create('Ext.form.field.ComboBox', { labelStyle: 'display: none;', flex: 1 });
			this._combobox.editable = false;
			this._combobox.queryMode = 'local';
			this._combobox.displayField = 'text';
			this._combobox.valueField = 'value';			
	
			this._dataContainer.add(this._combobox);
			
			this.add(this._dataContainer);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeComponent of BaseOxGridRowCombobox', ex);
		}
	},
	
	getComboBox: function() {
	    var retval = null;
	    
	    try {
		    retval = this._combobox;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getComboBox of BaseOxGridRowCombobox', ex);
		}
		
		return retval;
	}	
});