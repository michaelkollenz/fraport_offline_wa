Ext.define('AssetManagement.base.view.utils.BaseOxGrid', {
    extend: 'Ext.container.Container',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.utils.OxGridViewModel', 
        'AssetManagement.customer.view.utils.OxGridRowCombobox'
    ],

    config: {
        _contentPanel: null,
        _labelCls: 'oxGridContentLabel',
        _contentLabelCls: 'oxGridContentLabel',
        _contentMultiLineLabelCls: 'oxGridContentMultiLineLabel',
        _contentLinkCls: 'oxGridContentLink',
        _contentComboboxCls: 'oxGridContentCombobox',
        _contentCustomTemplateContainerCls: 'oxGridContentContainer',
        _labelPanel: null
    },

    viewModel: {
        type: 'utilsoxgrid'
    },
    
    cls: 'oxGrid',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    constructor: function(config) {
        this.callParent(arguments);
        
        try {
	        this.initializeComponent();
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxGrid', ex);
		}
    },

    onRowIsShown: function(rowId) {
    	try {
    		this.keepVisualIntegrety();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onRowIsShown of BaseOxGrid', ex);
		}
    },
    
    onRowIsHidden: function(rowId) {
    	try {
    		this.keepVisualIntegrety();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onRowIsHidden of BaseOxGrid', ex);
		}
    },

    addRow: function(labelString, template, moreAddsFollowing) {
    	try {
    		if(!labelString || !template)
        		return null;

        	var label = Ext.create('Ext.form.Label', {
        		cls: this._labelCls,
        		text: labelString
        	});

        	var templateType = 'notSupported';
        	var temp = typeof template;
        	if(temp == 'string') {
        		templateType = template;
        		if(template == 'label') {
        			template = Ext.create('Ext.form.Label', { cls: this._contentLabelCls}); //this._contentLabelWrapCls]});
        		} else if(template == 'link') {
        			template = Ext.create('Ext.button.Button', { cls: this._contentLinkCls });
        		} else if(template == 'combobox') {
        			template = Ext.create('AssetManagement.customer.view.utils.OxGridRowCombobox');
        		} else if (template == 'multilinelabel') {
        		    label.cls = 'oxGridContentMultiLineLabel';
        		    template = Ext.create('Ext.form.Label', { cls: this._contentMultiLineLabelCls });
        		}
        	} else if(temp == 'object') {
        		var templateContainer = Ext.create('Ext.container.Container', { cls: this._contentCustomTemplateContainerCls, layout: { type: 'vbox', align: 'stretch' }});
        		templateContainer.add(template);
        		template = templateContainer;
        		templateType = 'custom';
        	}

        	if(template === undefined || template === null)
        		return null;

        	this._labelPanel.add(label);
        	this._contentPanel.add(template);

        	if(!moreAddsFollowing)
        		this.keepVisualIntegrety();

        	var nextRowId = 0;
        	var items = this._labelPanel.items;

        	if(items && items.length)
        		nextRowId = items.length;

        	return Ext.create('AssetManagement.customer.view.utils.OxGridRow', { rowId: nextRowId, label: label, templateType: templateType, template: template, owningGrid: this});
        } catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addRow of BaseOxGrid', ex);
    	}
    },

    initializeComponent: function() {
    	try {
	        this._labelPanel = Ext.create('Ext.container.Container', { layout: { type: 'vbox', align: 'stretch' }, docked: 'left', cls: 'oxStaticLabelGridSubContainer' });
	        this._contentPanel = Ext.create('Ext.container.Container', { layout: { type: 'vbox', align: 'stretch' }, flex: 1 });
	
	        this.add(this._labelPanel);
	        this.add(this._contentPanel);
        } catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addRow of BaseOxGrid', ex);
    	}
    },

    keepVisualIntegrety: function() {
    	try {
        	//the rule to obey is
        	//the last shown row has to conform it's border, all others use the usual styles

        	//first the last visual row has to be determined
        	var lastVisibleRowId = 0;
       		var labels = this._labelPanel.items;

       		if(labels && labels.length > 1) {
       			var counter = labels.length - 1;
       			var label;
       			
       			do {
       				label = labels.getAt(counter);

       				if(!label.isHidden()) {
       					lastVisibleRowId = counter;
       					break;
       				}
        			
       				counter--;
        		} while(counter >= 0);
        	}

        	var contentArray = this._contentPanel.items;

        	//conform the affected items' borders
        	var affectedLabel = labels.getAt(lastVisibleRowId);
        	affectedLabel.setStyle('border-width', '0px');
        	var affectedTemplate = contentArray.getAt(lastVisibleRowId);
        	affectedTemplate.setStyle('border-width', '0px');
        	
        	//rebuild border of all items before the affected one
        	for(var i = 0; i < lastVisibleRowId; i++) {
        		labels.getAt(i).setStyle('border-width', '0px 0px 1px 0px');
        		contentArray.getAt(i).setStyle('border-width', '0px 0px 1px 0px');
        	}
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside keepVisualIntegrety of BaseOxGrid', ex);
		}
    }
});