Ext.define('AssetManagement.base.view.utils.BaseOxSpinner', {
    extend: 'Ext.form.field.Spinner',


    // override onSpinUp (using step isn't neccessary)
    onSpinUp: function() {
    	try {
    		var me = this;
            if (!me.readOnly) {
            	var val = 0;
            	if(me.allowDecimals) 
            		val = parseFloat(me.getValue().split(' '))||0;
            	else
            		val = parseInt(me.getValue().split(' '), 10)||0;
            		
    	    	if (val >= me.maxValue) {
    	    		me.setValue(me.maxValue);
    	    	} else {
    	    		me.setValue((val + me.incrementValue).toFixed(me.decimalPrecision));
    	    	}
            }
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSpinUp of BaseOxSpinner', ex);
		}
    },

    // override onSpinDown
    onSpinDown: function() {
    	try {
    		var me = this;
            if (!me.readOnly) {
            	if(me.allowDecimals) 
    	    		val = parseFloat(me.getValue().split(' '))||0;
    	    	else
    	    		val = parseInt(me.getValue().split(' '), 10)||0;
    	    	if (val <= me.incrementValue) {
    	    		me.setValue(0.0);
    	    	} else {
    	    		me.setValue((val - me.incrementValue).toFixed(me.decimalPrecision));
    	    	}
            }
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSpinDown of BaseOxSpinner', ex);
		}
    }
});