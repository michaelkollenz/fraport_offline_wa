﻿Ext.define('AssetManagement.base.view.BaseCharactValueNumberFieldItem', {
    extend: 'AssetManagement.customer.view.utils.OxNumberField',


    requires: [
       'AssetManagement.customer.controller.dialogs.SingularValueLineDialogController',
       'AssetManagement.customer.model.dialogmodel.SingularValueLineDialogViewModel',
       'Ext.container.Container',
       'Ext.form.Label',
       'Ext.form.field.Number',
       'Ext.button.Button',
       'AssetManagement.customer.utils.StringUtils',
       'AssetManagement.customer.view.CharactValueCheckboxItem'
    ],

    _charact: null,
    _charactValue: null,
    _classValue: null,

    constructor: function (config) {

        try {
            if (!config) {
                config = {};

                arguments = [config];
            }

            this._charact = config.charact;
            this._charactValue = config.charactValue;

            if (this._charact)
                this._classValue = this._charact.get('classValues');
        
            this.fieldLabel = config.charact.get('mseht') || config.charact.get('msehi');
            this.callParent(arguments);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueNumberFieldItem', ex);
        }
    }
});