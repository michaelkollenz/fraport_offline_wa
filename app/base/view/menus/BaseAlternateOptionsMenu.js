Ext.define('AssetManagement.base.view.menus.BaseAlternateOptionsMenu', {
	extend: 'Ext.menu.Menu',


	requires: [
	    'AssetManagement.customer.controller.menus.AlternateOptionsMenuController',
	    'Ext.util.HashMap'
	],

	config: {
		floating: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		id: 'AlternateOptionsMenu',
		cls: 'oxAlternateOptionsMenu'
	},

	controller: 'AlternateOptionsMenuController',

	constructor: function() {
		//Framework Bug:
		//for menu registration it is neccessary, to pass a copy of the classes config
		//the defined attributes inside the classes config will get extracted later
		try {
			if(arguments.length === 0) {
				arguments = new Array();
				arguments.push(this.config);
			}

			this.callParent(arguments);

			this.generateMenuItems();
	    } catch(ex) {
	    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseAlternateOptionsMenu', ex);
	    }
	},

	//private
	_myMenuItems: null,

	//public
	setMenuItems: function(menuItemIds) {
		//do not destroy the items again, because they will be used again
		this.removeAll(false);

		try {
			if(menuItemIds && this._myMenuItems) {
				var menuItem;
				Ext.Array.each(menuItemIds, function(id) {
					menuItem = this._myMenuItems.get(id);

					if(menuItem)
						this.add(menuItem);
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMenuItems of BaseAlternateOptionsMenu', ex);
		}
	},

	//private
	generateMenuItems: function() {
		try {
			var myItems = Ext.create('Ext.util.HashMap');

			var menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/search.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('search'),
				id: 'searchMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('searchMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/search_clear_small.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('clearSearch'),
			    id: 'clearSearchButtonAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('clearSearchButtonAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/edit.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('edit'),
				id: 'editMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('editMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/demand_requirement.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('addSDOrder'),
				id: 'newSDOrderMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('newSDOrderMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/duplicate.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('duplicate'),
				id: 'duplicateMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('duplicateMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/class_charact.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('classifications'),
				id: 'classMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('classMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/add.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('add'),
				id: 'addMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('addMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/longtext.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('longtext'),
				id: 'longtextMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('longtextMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/longtext_exists.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('longtext'),
				id: 'longtextExtistsMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('longtextExtistsMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/equi_change_position.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('equipmentInOut'),
				id: 'equiChangeMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('equiChangeMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/save.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('save'),
				id: 'saveMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('saveMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/report.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('generateReport'),
				id: 'reportMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('reportMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/print.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('print'),
				id: 'printMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('printMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/trash.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('delete'),
				id: 'deleteMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('deleteMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/new_item.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('newItem'),
				id: 'newItemMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('newItemMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/order.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('createNewOrder'),
				id: 'newOrderMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('newOrderMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/notif.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('createNewNotif'),
				id: 'newNotifMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('newNotifMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/measdoc.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('measPoints'),
				id: 'measpointsMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('measpointsMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/rfid.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('rfid'),
				id: 'rfidMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('rfidMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/mapsMarker.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('showMyPosition'),
				id: 'showMyMapsPositionMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('showMyMapsPositionMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/object_structure.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('objectStructure'),
				id: 'objStructureMenuItemAOM',
				listeners: {
					click: 'onMenuItemSelected'
				}
			});
			myItems.add('objStructureMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
				icon: 'resources/icons/rfid.png',
				iconCls: 'oxMenuItemIcon',
				cls: 'oxMenuItemWithIcon',
				text: Locale.getMsg('scanForBarcode'),
				id: 'barcodeMenuItemAOM'
//				,
//				listeners: {
//					boxReady: buildBarcodeEntry,
//					scope: this
//				}
			});
			myItems.add('barcodeMenuItemAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/printing_icon.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('print'),
			    id: 'printMultipleReportsAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('printMultipleReportsAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/signing_icon.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('signature'),
			    id: 'signingReportsButtonAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('signingReportsButtonAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/save_icon.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('save'),
			    id: 'saveReportsButtonAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('saveReportsButtonAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/download.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('download'),
			    id: 'downloadButtonAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('downloadButtonAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/demand_requirement.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('createBanf'),
			    id: 'banfButtonAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('banfButtonAOM', menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
			    icon: 'resources/icons/intern_note.png',
			    iconCls: 'oxMenuItemIcon',
			    cls: 'oxMenuItemWithIcon',
			    text: Locale.getMsg('internNote'),
			    id: 'internNoteButtonAOM',
			    listeners: {
			        click: 'onMenuItemSelected'
			    }
			});
			myItems.add('internNoteButtonAOM', menuItem);

			this._myMenuItems = myItems;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateMenuItems of BaseAlternateOptionsMenu', ex);
		}
	}
});