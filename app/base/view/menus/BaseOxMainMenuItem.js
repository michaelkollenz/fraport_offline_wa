Ext.define('AssetManagement.base.view.menus.BaseOxMainMenuItem', {
	extend: 'Ext.menu.Item',

	
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
	    //Fix for error getAttribute and setAttribute of null
	    focusable: false,
		cls: 'oxMainMenuItem'
	},
    
	
	//private
	//@override
	//will do a fallback to ext js 5 logic without aria element, if it's dom element is null
	//else function will fail on ext js 6 (framework bug)
	onShow: function() {
		try {
			if(this.ariaEl.dom) {
				this.callParent(arguments);
			} else {
				var me = this;
				
				me.el.show();
				
				me.updateLayout({ isRoot: false });
				
				// Constraining/containing element may have changed size while this Component was hidden
				if (me.floating) {
				    if (me.maximized) {
				        me.fitContainer();
				    }
				    else if (me.constrain) {
				        me.doConstrain();
				    }
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onShow of BaseOxMainMenuItem', ex);
		}
	},

	//private
	//@override
	//will do a fallback to ext js 5 logic without aria element, if it's dom element is null
	//else function will fail on ext js 6 (framework bug)
	onHide: function(animateTarget, cb, scope) {
		try {
			if(this.ariaEl.dom) {
				this.callParent(arguments);
			} else {
				var me = this,
	            ghostPanel,
	            fromSize,
	            toBox,
	            activeEl = Ext.Element.getActiveElement();

		        // If hiding a Component which is focused, or contains focus: blur the focused el.
		        if (activeEl === me.el || me.el.contains(activeEl)) {
		            Ext.fly(activeEl).blur();
		        }
	
		        // Default to configured animate target if none passed
		        animateTarget = me.getAnimateTarget(animateTarget);
	
		        // Need to be able to ghost the Component
		        if (!me.ghost) {
		            animateTarget = null;
		        }
		        // If we're animating, kick off an animation of the ghost down to the target
		        if (animateTarget) {
		            toBox = {
		                x: animateTarget.getX(),
		                y: animateTarget.getY(),
		                width: animateTarget.dom.offsetWidth,
		                height: animateTarget.dom.offsetHeight
		            };
		            ghostPanel = me.ghost();
		            ghostPanel.el.stopAnimation();
		            fromSize = me.getSize();
		            ghostPanel.el.animate({
		                to: toBox,
		                listeners: {
		                    afteranimate: function() {
		                        delete ghostPanel.componentLayout.lastComponentSize;
		                        ghostPanel.el.hide();
		                        ghostPanel.setHiddenState(true);
		                        ghostPanel.el.setSize(fromSize);
		                        me.afterHide(cb, scope);
		                    }
		                }
		            });
		        }
		        me.el.hide();
		        if (!animateTarget) {
		            me.afterHide(cb, scope);
		        }
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onHide of BaseOxMainMenuItem', ex);
		}
	}
});