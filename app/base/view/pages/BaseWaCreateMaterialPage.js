Ext.define('AssetManagement.base.view.pages.BaseWaCreateMaterialPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',
  alias: 'widget.BaseWaCreateMaterialPage',

  requires: [
    'AssetManagement.customer.model.pagemodel.WaCreateMaterialPageViewModel',
    'AssetManagement.customer.controller.pages.WaCreateMaterialPageController'

  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.WaCreateMaterialPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseWaCreateMaterialPage', ex);
      }

      return this._instance;
    }
  },

  config: {
    flex: 1,
    bodyCls: ''
  },

  viewModel: {
    type: 'WaCreateMaterialPageModel'
  },

  mixins: {
    rendererMixin: 'AssetManagement.customer.view.mixins.WaCreatePageRendererMixin'
  },

  controller: 'WaCreateMaterialPageController',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {
      var items = [
        {
          xtype: 'container',
          layout: {
            type: 'vbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'component',
              width: 30
            },
            {
              xtype: 'container',
              flex: 1,
              margin: '20 0 0 0',
              scrollable: false,
              layout: {
                type: 'hbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'label',
                  margin: '8 0 0 0',
                  text: Locale.getMsg('printKZ'),
                  width: 107
                },
                {
                  xtype: 'checkbox',
                  baseCls: 'oxCheckBox',
                  checkedCls: 'checked',
                  height: 52,
                  width: 54,
                  // html: '<div><img  src="resources/icons/save.png"></img></div>',
                  id: 'materialCheckPrintKZ',
                  listeners: {
                    change: 'onPrintChangeClick'
                  }
                }]
            },
            {
              xtype: 'container',
              id: 'waCreateAddSection',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'oxtextfield',
                  id: 'waCreateMaterialNo',
                  fieldLabel: Locale.getMsg('material'),
                  maxLength: 18,
                  height: 40,
                  padding: '10 10 10 10',
                  listeners: {
                    keypress: 'onMatNoTextKeypress'
                  },
                  triggers: {
                    clear: {
                      hidden: false,
                      cls: 'x-form-clear-trigger',
                      weight: 10,
                      extraCls: 'ox-custom-clear-trigger-textfield',
                      handler: function () {
                        this.setValue('');
                      }
                    }
                  }
                },
                {
                  xtype: 'oxtextfield',
                  id: 'waCreateQuantityIs',
                  height: 40,
                  // readOnly: true,
                  padding: '10 10 10 10',
                  fieldLabel: Locale.getMsg('quantity'),
                  maxLength: 14,
                  listeners: {
                    keypress: 'onQuantTextKeypress'
                  },
                  triggers: {
                    clear: {
                      hidden: false,
                      cls: 'x-form-clear-trigger',
                      weight: 10,
                      extraCls: 'ox-custom-clear-trigger-textfield',
                      handler: function () {
                        this.setValue('');
                      }
                    }
                  }
                },
                {
                  xtype: 'container',
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  },
                  items: [
                    {
                      xtype: 'button',
                      height: 52,
                      width: 54,
                      html: '<div><img src="resources/icons/MM/remove.png"></img></div>',
                      id: 'removeQuantWaCreateMaterialPage',
                      listeners: {
                        click: 'onRemoveButtonClick'
                      }
                    },
                    {
                      xtype: 'button',
                      margin: '0 0 0 20',
                      height: 52,
                      width: 54,
                      html: '<div><img src="resources/icons/MM/add.png"></img></div>',
                      id: 'addQuantWaCreateMaterialPage',
                      listeners: {
                        click: 'onAddButtonClick'
                      }
                    }
                  ]
                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'button',
                  height: 52,
                  width: 54,
                  html: '<div><img  src="resources/icons/save.png"></img></div>',
                  id: 'saveWaItem',
                  listeners: {
                    click: 'onSaveClick'
                  }
                }
              ]
            },
            {
              xtype: 'container',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  id: 'waGoodsMovementListGridContainer',
                  flex: 1,
                  // layout: {
                  //     type: 'hbox',
                  //     align: 'stretch'
                  // },
                  margin: '20 0 0 0',
                  items: [
                    this.getNewGoodsMovementListGridPanel()
                  ]
                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'button',
                  height: 52,
                  width: 54,
                  html: '<div><img  src="resources/icons/btsync.png"></img></div>',
                  listeners: {
                    click: 'onSubmitClick'
                  }
                }
              ]
            }
          ]
        }
      ];

      this.add(items);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseWaCreateMaterialPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';
    try {
      retval = this.getViewModel().get('title');
      // retval = Locale.getMsg('WA');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseWaCreateMaterialPage', ex);
    }

    return retval;
  },

  checkPrintKZ: function () {
    try {
      var myModel = this.getViewModel();
      var printKZLastUserValue = myModel.get('printKZLastUserValue');
      var goodsMovementsStore = myModel.get('goodsMovementItemsStore');
      if (goodsMovementsStore && goodsMovementsStore.getCount() > 0 && goodsMovementsStore.findRecord('kzumw', 'X')) {
        myModel.set('printKZ', true);
        Ext.getCmp('materialCheckPrintKZ').setRawValue(true);
        Ext.getCmp('materialCheckPrintKZ').setDisabled(true);
      }
      // else if (goodsMovementsStore && goodsMovementsStore.getCount() > 0) {
      else {
        myModel.set('printKZ', printKZLastUserValue);
        Ext.getCmp('materialCheckPrintKZ').setDisabled(false);
      }


    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkPrintKZ of BaseWaCreateMaterialPage', ex);
    }
  },
  updatePageContent: function () {
    try {
      var myModel = this.getViewModel();
      this.checkPrintKZ();
      this.transferModelStateIntoView();
      var goodsMovementsStore = myModel.get('goodsMovementItemsStore');

      //workaround for chrome43+ & ExtJS 6.0.0 Bug
      if (AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
        this.renewGoodsMovementListGridPanel();

      var goodsMovementListGridPanel = Ext.getCmp('waGoodsMovementListGridPanel');
      goodsMovementListGridPanel.setStore(goodsMovementsStore);

      var miProcess = myModel.get('miProcess');

      var waCreateAddSection = Ext.getCmp('waCreateAddSection');
      if (miProcess.get('man_entries')) {
        waCreateAddSection.show();
      } else {
        waCreateAddSection.hide();
      }

      var miReasonColumn = Ext.getCmp('miReasonColumn');
      if (miProcess.get('kzgru') === '-')
        miReasonColumn.hide();
      else
        miReasonColumn.show();

      var waCheckColumn = Ext.getCmp('waCheckColumn');
      var waDeleteColumn = Ext.getCmp('waDeleteColumn');
      if (miProcess.get('planned_items') === 'X') {
        waCheckColumn.setHidden(false);
        waDeleteColumn.setHidden(true);
      }
      else {
        waCheckColumn.setHidden(true);
        waDeleteColumn.setHidden(false);
      }

      var materialNoField = Ext.getCmp('waCreateMaterialNo');
      if (goodsMovementsStore && goodsMovementsStore.getCount() == 0){
        materialNoField.focus();
      }
      else{
        document.activeElement.blur();
        goodsMovementListGridPanel.focus();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseWaCreateMaterialPage', ex);
    }
  },
  //protected
  onActivation: function() {
    try {
      var goodsMovementsStore = this.getViewModel().get('goodsMovementItemsStore');
      var goodsMovementListGridPanel = Ext.getCmp('waGoodsMovementListGridPanel');
      var materialNoField = Ext.getCmp('waCreateMaterialNo');
      if (goodsMovementsStore && goodsMovementsStore.getCount() == 0){
        materialNoField.focus();
      }
      else{
        goodsMovementListGridPanel.focus();
      }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onActivation of BaseWaCreateMaterialPage', ex);
    }
  },

  afterRefreshView: function () {
    try {

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRefreshView of BaseWaCreateMaterialPage', ex);
    }
  },

  //private
  getNewGoodsMovementListGridPanel: function () {
    var retval = null;

    try {
      var myController = this.getController();
      var me = this;
      var lgortStore = this.getViewModel().get('lgortStore');

      var rendererMixins = this.mixins.rendererMixin;

      retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
        id: 'waGoodsMovementListGridPanel',
        flex: 1,
        useLoadingIndicator: true,
        hideContextMenuColumn: true,
        emptyText: Locale.getMsg('noItems'),
        scrollable: 'vertical',
        parentController: myController,
        owner: me,
        columns: [
          // {
          //   xtype: 'gridcolumn',
          //   text: Locale.getMsg('position'),
          //   dataIndex: 'ebelp',
          //   flex: 4,
          //   renderer: rendererMixins.waCreatePositionColumnRenderer
          // },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('material'),
            dataIndex: 'matnr',
            flex: 3,
            renderer: rendererMixins.waCreateMaterialColumnRenderer
          },
          {
            xtype: 'widgetcolumn',
            id: 'miReasonColumn',
            // flex: 3,
            Width: 100,
            resizeable: false,
            // hidden: true,
            text: Locale.getMsg('reason'),
            layout: {
              type: 'vbox',
              align: 'stretch'
            },
            widget: {
              xtype: 'container',
              owner: me,
              flex: 3,
              items: [
                {
                  xtype: 'oxcombobox',
                  padding: '15 0 0 0',
                  editable: false,
                  width: 90,
                  displayField: 'grtxt',
                  valueField: 'grund',
                  queryMode: 'local',
                  height: 30,
                  listeners: {
                    change: myController.onReasonChanged
                  }
                },
                {
                  xtype: 'oxtextfield',
                  fieldLabel: '',
                  margin: '3 0 0 0 ',
                  padding: '15 0 0 0',
                  width: 90,
                  maxLength: 50,
                  autofocus: false,
                  hideTrigger: true,
                  mouseWheelEnabled: false,
                  listeners: {
                    blur: myController.onReasonTextChanged
                  }
                }
              ]
            },
            onWidgetAttach: rendererMixins.miReasonOnWidgetAttached
          },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('confirmed_Qty'),
            dataIndex: 'menge',
            resizable: false,
            // width: 40,
            flex: 1,
            renderer: rendererMixins.waCreateMengeColumnRenderer
          },
          {
            xtype: 'checkcolumn',
            id: 'waCheckColumn',
            width: 60,
            resizable: false,
            dataIndex: 'elikz',
            tdCls: 'oxCheckBoxGridColumnItem',
            text: Locale.getMsg('OK')
            // listeners: {
            //   checkchange: 'onDeliveredCheckChange',
            //   scope: myController
            // }
          },
          {
            xtype: 'checkcolumn',
            width: 60,
            resizable: false,
            tdCls: 'oxCheckBoxGridColumnItem editButton',
            text: Locale.getMsg('edit'),
            listeners: {
              checkchange: 'onEditClicked',
              scope: myController
            }
          },
          {
            xtype: 'checkcolumn',
            id: 'waDeleteColumn',
            width: 60,
            resizable: false,
            tdCls: 'oxCheckBoxGridColumnItem deleteButton',
            text: Locale.getMsg('delete'),
            // renderer: rendererMixins.checkDeleteColumnRenderer,
            listeners: {
              checkchange: 'onDeleteClicked',
              scope: myController
            }
          }

        ]
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewGoodsMovementListGridPanel of BaseGoodsMovementListPage', ex);
    }
    return retval;
  },

  //private
  renewGoodsMovementListGridPanel: function () {
    var retval = null;

    try {
      var gridContainer = Ext.getCmp('waGoodsMovementListGridContainer');

      if (gridContainer)
        gridContainer.removeAll(true);

      retval = this.getNewGoodsMovementListGridPanel();

      if (gridContainer && retval) {
        gridContainer.add(retval);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewWeListGridPanel of BaseWaCreateMaterialPage', ex);
      retval = null;
    }
    return retval;
  },

  transferModelStateIntoView: function () {
    try {
      var myModel = this.getViewModel();
      var material = myModel.get('material');
      var menge = myModel.get('menge');
      var printKZ = myModel.get('printKZ');

      Ext.getCmp('waCreateMaterialNo').setValue(material);

      Ext.getCmp('waCreateQuantityIs').setValue(menge);
      var materialCheckPrintKZ = Ext.getCmp('materialCheckPrintKZ');
      if (!materialCheckPrintKZ.getValue() === printKZ) {
        materialCheckPrintKZ.setValue(printKZ);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of FbUmbPage', ex);
    }
  },

  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();
      myModel.set('material', Ext.getCmp('waCreateMaterialNo').getValue());
      myModel.set('menge', Ext.getCmp('waCreateQuantityIs').getValue());
      myModel.set('printKZ', Ext.getCmp('materialCheckPrintKZ').getValue());

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseWaCreateMaterialPage', ex);
    }
  }

});