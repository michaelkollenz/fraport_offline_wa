Ext.define('AssetManagement.base.view.pages.BaseSelfDispositionPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',


  requires: [
    'AssetManagement.customer.model.pagemodel.SelfDispositionPageViewModel',
    'AssetManagement.customer.controller.pages.SelfDispositionPageController',
    'AssetManagement.customer.view.OxGridPanel',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.view.utils.OxTextField',
    'AssetManagement.customer.view.utils.OxNumberField',
    'AssetManagement.customer.view.utils.OxComboBox',
    'Ext.grid.plugin.CellEditing',
    'Ext.grid.column.Number',
    'Ext.grid.column.Check',
    'Ext.grid.column.Action'
  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.SelfDispositionPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSelfDispositionPage', ex);
      }

      return this._instance;
    }
  },

  mixins: {
    // rendererMixin: 'AssetManagement.customer.view.mixins.SelfDispositionPageRendererMixin'
    // itemsMixin: 'AssetManagement.customer.view.mixins.SelfDispositionPageItemsMixin'
  },
  _DispositionDocumentRow: null,
  _plantRow: null,
  _storLocRow: null,

  viewModel: {
    type: 'SelfDispositionPageModel'
  },

  controller: 'SelfDispositionPageController',

  buildUserInterface: function () {
    try {
      var myController = this.getController();
      var me = this;

      this.editing = Ext.create('Ext.grid.plugin.CellEditing');
      var items = [
        {
          xtype: 'container',
          flex: 1,
          layout: {
            type: 'hbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'container',
              margin: '0 10 0 10',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [{
                xtype: 'container',
                flex: 1,
                layout: {
                  type: 'hbox',
                  align: 'stretch'
                },
                items: [
                  {
                    xtype: 'oxnumberfield',
                    id: 'iaDispositionNumber',
                    fieldLabel: Locale.getMsg('record'),
                    maxLength: 10,
                    height: 40,
                    // readOnly: true,
                    padding: '10 10 10 10',
                    margin: '0 0 10 0',
                    triggers: {
                      clear: {
                        hidden: false,
                        cls: 'x-form-clear-trigger',
                        weight: 10,
                        extraCls: 'ox-custom-clear-trigger-textfield',
                        handler: function () {
                          this.setValue('');
                        }
                      }
                    }
                  },
                  {
                    xtype: 'button',
                    margin: '0 0 0 20',
                    height: 52,
                    width: 54,
                    html: '<div><img src="resources/icons/MM/add.png"></img></div>',
                    // id: 'addQuantWaCreateMaterialPage',
                    listeners: {
                      click: 'onAddButtonClick'
                    }
                  }
                ]
              },
                {
                  xtype: 'container',
                  id: 'DispositionGridContainer',
                  margin: '10 0 10 0',
                  flex: 1,
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  }/*,
                               items:
                                   [
                                       this.getDispositionItemsGridPanel()
                                   ]*/
                }
              ]
            }
          ]
        }
      ];


      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSelfDispositionPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';

    try {
      var title = 'Self Disposition';//TODO

      // var Disposition = this.getViewModel().get('DispositionStore') ? this.getViewModel().get('Disposition').get('items') : null;

      // if (Disposition) {
      //   title += "(" + Disposition.getCount() + ")";
      // }
      retval = title;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseSelfDispositionPage', ex);
    }

    return retval;
  },

  //protected
  //@Override
  updatePageContent: function () {
    try {
      //do not remove - causes issue if we are not generating each and every time the grid

      this.refreshGridPanel();

      // this.clearAllFields();
      // this.fillMainDataSection();
      this.refreshDispositionItemPanel();
      this.transferModelStateIntoView();
      // this.searchFieldVisibility();


    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseSelfDispositionPage', ex);
    }
  },

  refreshGridPanel: function () {
    try {
      var gridContainer = Ext.getCmp('DispositionGridContainer');

      // var invItemGrid;

      gridContainer.removeAll();
      gridContainer.add(this.getDispositionItemsGridPanel())
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseSelfDispositionPage', ex);
    }
  },


  refreshDispositionItemPanel: function () {
    try {
      var myModel = this.getViewModel();
      var DispositionItemGridPanel = Ext.getCmp('DispositionGridPanel');

      var dispositionStore = myModel.get('dispositionStore');

      if(!dispositionStore){
        var dispositionStore = Ext.create('Ext.data.Store', {
          extend: 'Ext.data.Model',
          fields: [
            { name: 'value',            type: 'string' }

          ]
        });
        myModel.set('dispositionStore', dispositionStore);
      }

      DispositionItemGridPanel.setStore(dispositionStore);

      // invItemsList.sort('item', 'ASC');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshDispositionItemPanel of BaseSelfDispositionPage', ex);
    }

  },

  getDispositionItemsGridPanel: function () {
    var retval = null;
    try {
      var me = this;
      var myController = this.getController();

      retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
        id: 'DispositionGridPanel',
        hideContextMenuColumn: true,
        useLoadingIndicator: true,
        loadingText: Locale.getMsg('loadingDisposition'),
        emptyText: Locale.getMsg('noDisposition'),
        flex: 1,
        scrollable: 'vertical',
        columns:
          [
            {
              xtype: 'gridcolumn',
              dataIndex: 'value',
              text: Locale.getMsg('record'),
              flex: 4,
              // width: 200,
              resizable: false,
              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                var value = record.get('value');
                return value;

              }
            },
            {
              xtype: 'checkcolumn',
              // width: 60,
              flex: 1,
              resizable: false,
              tdCls: 'oxCheckBoxGridColumnItem deleteButton',
              text: Locale.getMsg('delete'),
              listeners: {
                checkchange: 'onDeleteClicked',
                scope: myController
              }
            }
          ]
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDispositionItemsGridPanel of BaseSelfDispositionPage', ex);
    }

    return retval;
  },

  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();

      myModel.set('persNo', Ext.getCmp('iaDispositionFieldPerNr').getValue());
      myModel.set('kostl', Ext.getCmp('iaDispositionFieldPerKostSt').getValue());

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseSelfDispositionPage', ex);
    }
  },

  transferModelStateIntoView: function () {
    try {
      var myModel = this.getViewModel();
      var DispositionStore = myModel.get('DispositionStore');

      if (DispositionStore) {
        var DispositionItem = DispositionStore.getAt(0);
        //set general data
        if (DispositionItem.get('kostl')) {
          Ext.getCmp('iaDispositionFieldPerKostSt').setValue(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(DispositionItem.get('kostl')));
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of FbUmbPage', ex);
    }
  },

  getCurrentInputValues: function () {
    var retval = null;

    try {
      retval = Ext.getCmp('DispositionGridPanel').getStore();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseSelfDispositionPage', ex);
      retval = null;
    }
    return retval;
  }
});
