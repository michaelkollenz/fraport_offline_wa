Ext.define('AssetManagement.base.view.pages.BaseMatConfPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.MatConfPageViewModel',
        'AssetManagement.customer.controller.pages.MatConfPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.Img',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'AssetManagement.customer.model.bo.MaterialConf',        
        'AssetManagement.customer.helper.OxLogger'
    ],
    
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null)
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.MatConfPage');
            
            } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMatConfPage', ex);
		    }
            	
            return this._instance;
        }
    },
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.MatConfPageRendererMixin'
    },
    controller: 'MatConfPageController',
    
    viewModel: {
        type: 'MatConfPageModel'
    },
    
    buildUserInterface: function() {
         try {
        	 var myController = this.getController();
        	 var me = this;
        	 var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            items: [
		                                {
		                                    xtype: 'component'
		                                },
		                                {
		                                    xtype: 'label',
		                                    cls: 'oxHeaderLabel',
		                                    id: 'matConfPageModeLabel',
		                                    text: Locale.getMsg('createNewMatConf')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'container',
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'container',
		                                            flex: 100,
		                                            maxWidth: 800,
		                                            layout: {
		                                                type: 'vbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
				                                        {
				                                            xtype: 'oxcombobox',
				                                            id: 'matConfOperDropDownBox',
					                                        displayField: 'text',
					                                        queryMode: 'local',
					                                        valueField: 'value',
				        		                            editable: false,
				                                            fieldLabel: Locale.getMsg('operation'),
					                                        listeners: {
					                                    		select: 'onOperationSelected'
					                                    	}
				                                        },
				                                        {
				                                            xtype: 'component',
				                                            height: 5
				                                        },
				                                        {
				                                            xtype: 'oxtextfield',
				                                            id: 'matConfEquiTextField',
				                                            fieldLabel: Locale.getMsg('equipment'),
					                                        disabledCls: 'oxDisabledTextField',
						                                    disabled: true
				                                        },
				                                        {
				                                            xtype: 'component',
				                                            height: 5
				                                        },
			                                            {
                                                            xtype: 'datefield',
                                                            id: 'matConfDate',
                                                            format: 'd.m.Y',
                                                            maxLength: 10,
                        		                            editable: false,
                                                            fieldLabel: Locale.getMsg('date')
	                                                    },
                                                        {
                                                            xtype: 'component',
                                                            height: 5
                                                        },
                                                        {
                                                            xtype: 'oxtextfield',
                                                            id:'matConfMaterialNo',
                                                            fieldLabel: Locale.getMsg('material'),
                                                            maxLength: 18
                                                        },
                                                        {
                                                            xtype: 'component',
                                                            height: 5
                                                        },
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'oxnumberfield',
                                                                    flex: 0.7,
                                                                    id: 'matConfAmountNumberField',
                                                                    fieldLabel: Locale.getMsg('quantityUnit'),
                                                                    maxLength: 13
                                                                },
                                                                {
                                                                    xtype: 'component',
                                                                   	width: 10
                                                                },
                                                                {
                                                                    xtype: 'oxtextfield',
                                                                    id: 'matConfUnitTextField', 
                                                                    flex: 0.3,
                                                                    labelStyle: 'display: none;',
                                                                    maxLength: 3
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'component',
                                                            height: 10
                                                        },
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'oxtextfield',
                                                                    flex: 0.2,
                                                                    id: 'matConfPlantTextField', 
                                                                    fieldLabel: Locale.getMsg('plantStorLoc'),
                                                                    maxLength: 4,
	    		                                                    disabled: true,
                                                                    enableKeyEvents: true, 
                                                                    listeners: {
                                                                		keyup: 'onPlantTextChanged'
                                                                	}
                                                                
                                                                },
                                                                {
                                                                    xtype: 'component',
                                                                    width: 10
                                                                },
                                                                {
                                                                    xtype: 'oxcombobox',
                                                                    flex: 0.3,
                                                                    id: 'matConfStorLocComboBox',
                                                                    labelStyle: 'display: none;',
                                                                    displayField: 'text',
							                                        queryMode: 'local',
							                                        valueField: 'value',
							                                        editable: false
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'component',
                                                            height: 10
                                                        },
                                                        {
                                                            xtype: 'oxcombobox',
                                                            flex: 1,
                                                            id: 'matConfAccIndicationDropDownBox',
                        		                            editable: false,
                                                            fieldLabel: Locale.getMsg('accountIndiShort'),
                                                            displayField: 'text',
                                                           	queryMode: 'local',
                                                           	valueField: 'value'
                                                        }
				                                        
				                                    ]
				                                },
				                                {
		                                            xtype: 'component',
		                                            flex: 3,
		                                            mindWidth: 10,
		                                            maxWidth: 40
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'vbox',
		                                                align: 'left',
		                                                pack: 'center'
		                                            },
		                                            items: [
		                                                {
		                                                	xtype: 'container',
			                                                items: [
			                                                    {
			                                                        xtype: 'button',
			                                                        html: '<div><img width="80%" src="resources/icons/search.png"></img></div>',
			                                                        id: 'searchButtonMatConfPage',
				                                                    listeners: {
	                                                                	click: 'onSelectMaterialClick'
	                                                                }
			                                                    }
			                                                ]
		                                                }
		                                            ]
		                                        }
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 45
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'label',
		                                            margin: '0 0 0 5',
		                                            cls: 'oxHeaderLabel',
		                                            text: Locale.getMsg('matConf_RecordedMatConfs')
		                                        }
		                                    ]
				                        },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'matConfGrid',
		                                    disableSelection: true,
		                                    parentController: myController,
                                            owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/matconf.png"  />';
		                                            },
		                                            id: 'MatConfGridActionColumn',
		                                            maxWidth: 80,
		                                            minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'matConfGridMaterialColumn',
		                                            text: Locale.getMsg('material'),
		                                            dataIndex: 'matnr',
		                                            flex: 2,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                	                		var material = record.get('material');
			                	                		var maktx = material ? material.get('maktx') : '';
			                	                    
			                	                		return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
			                                        }
		                                        },
		                                        {
		                                        	//column account Indication 
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'bemot',
		                                            flex: 1,
		                                            text: Locale.getMsg('accountIndication'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	var retval = '';
			                                        
		                                        		try {
		                                        			var accountIndication = record.get('accountIndication');
		                                        		
				                                        	retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
		                                        		} catch(ex) {
		                                        			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of matConfGrid/bemot-column in BaseMatConfPage', ex);
		                                        		}
		                                        		
		                                        		return retval;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'datecolumn',
		                                            id: 'matConfGridDateColumn',
		                                            text: Locale.getMsg('date'),
			                                        maxWidth: 125,
			                                        minWidth: 125,
		                                            format: 'd.m.Y',
		                                            dataIndex: 'isdd'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'matConfGridAmountColumn',
		                                            text: Locale.getMsg('orderComponents_Qty'),
		                                            dataIndex: 'quantity',
			                                        maxWidth: 160,
			                                        minWidth: 160,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    		var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('quantity')); 
			                                    		var unit = record.get('unit'); 
			                                    		
			                                    		return quantity + ' ' + unit; 
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'matConfGridWerkColumn',
		                                            dataIndex: 'plant',
			                                        maxWidth: 160,
			                                        minWidth: 160,
		                                            text: Locale.getMsg('plantStorLoc'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    		var plant = record.get('plant'); 
			                                    		var storloc = record.get('storloc'); 
			                                    		
			                                    		return AssetManagement.customer.utils.StringUtils.concatenate([ plant, storloc ], '/');
			                                        }
		                                        }
		                                    ],*/
			                                listeners: {
			                            		cellcontextmenu: myController.onCellContextMenu,
			                            		scope: myController
			                            	}
		                                }
		                            ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 30
		                }
		            ]
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMatConfPage', ex);
		}
    },
    
    getPageTitle: function() {
    	var retval = '';
    
    	try {
	    	var title = Locale.getMsg('matconfsForOrder');
	    	
	    	var order = this.getViewModel().get('order');
	    	
	    	if(order) {
	    		title += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(order.get('aufnr'));
	    	}
	
	    	retval = title;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMatConfPage', ex);
		}
		
		return retval;
	},
    
	//protected
	//@override
    updatePageContent: function() {
    	try {
    		this.fillDropDownBoxes(); 
    		
    		this.transferModelStateIntoView(); 
			
			this.refreshMatConfPanel(); 
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseMatConfPage', ex);
    	}
	},
	
	fillDropDownBoxes: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operations = order ? order.get('operations') : null;
			var accountIndications = myModel.get('accountIndications');
    	
			//filling operations dropdownbox
			var operationComboBoxSource = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			if(operations && operations.getCount() > 0) {
				operations.each(function(oper) {
					var vornr = oper.get('vornr');
					
					operationComboBoxSource.add({ text: vornr + " - " + oper.get('ltxa1'), value: oper })
				}, this);
			}
			
			var operComboBox = Ext.getCmp('matConfOperDropDownBox');
			operComboBox.clearValue();
			operComboBox.setStore(operationComboBoxSource);
		
			//filling accountIndication dropdownbox
			bemotComboBoxSource = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
	
			if(accountIndications && accountIndications.getCount() > 0) {
				accountIndications.each(function(accountIndication) {
					bemotComboBoxSource.add({ text: (accountIndication.get('bemot') + ' - ' + accountIndication.get('bemottxt')), value: accountIndication });
				}, this);
			}
			
			var bemotComboBox = Ext.getCmp('matConfAccIndicationDropDownBox');
			bemotComboBox.clearValue();
			bemotComboBox.setStore(bemotComboBoxSource);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseMatConfPage', ex);
		}
	},

	fillStorageLocationComboBox: function(plant) {
		try {		
			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			var myModel = this.getViewModel();

			var defaultStorLoc = null; 
			var matConfsStorLoc = null;
			var matConfsLgort = myModel.get('matConf').get('storloc');
			var storLocs = myModel.get('storLocs');
			
			if(storLocs && storLocs.getCount() > 0) {
				storLocs.each(function(storLoc) {
					if(storLoc.get('werks') === plant) {
						store.add({ text: storLoc.get('lgort') + " - " + storLoc.get('lgobe'), value: storLoc});
						
						if(storLoc.get('default_lgort') === 'X')
							defaultStorLoc = storLoc;
							
						if(storLoc.get('lgort') === matConfsLgort)
							matConfsStorLoc = storLoc;
					}
				}, this);
				
				if(!defaultStorLoc)
					defaultStorLoc = storLocs.getAt(0);
			}
			
			var comboBox = Ext.getCmp('matConfStorLocComboBox');
			comboBox.clearValue();
			comboBox.setStore(store);
			comboBox.setStore(store);
			
			var storLocToSet = matConfsStorLoc;

			if(!storLocToSet)
				storLocToSet = defaultStorLoc;
				
			if(storLocToSet) {
				var storLocRecord = storLocToSet ? comboBox.findRecordByValue(storLocToSet) : null;	
				
				if(storLocRecord)
					comboBox.setValue(storLocRecord);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillStorageLocationComboBox of BaseMatConfPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var matConf = myModel.get('matConf');
			var editMode = myModel.get('isEditMode');

			Ext.getCmp('matConfPageModeLabel').setText(editMode === true ? Locale.getMsg('editMatConf') : Locale.getMsg('createNewMatConf'));
			
			 //fill equi field
	   		if(order.get('equipment') !== null && order.get('equipment') !== undefined)
				Ext.getCmp('matConfEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0') + " " + order.get('equipment').get('eqktx'));
			else
				Ext.getCmp('matConfEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0')); 
		
	   		//set general data
			Ext.getCmp('matConfDate').setValue(matConf.get('isdd'));
			Ext.getCmp('matConfMaterialNo').setValue(matConf.get('matnr'));
			Ext.getCmp('matConfAmountNumberField').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(matConf.get('quantity'))); 
			Ext.getCmp('matConfUnitTextField').setValue(matConf.get('unit')); 
			Ext.getCmp('matConfPlantTextField').setValue(matConf.get('plant'));
			
			//set drop down box values
	    	this.setDropDownBoxesConvenientToCurrentModel();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseMatConfPage', ex);
		}
	},
	
	setDropDownBoxesConvenientToCurrentModel: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operations = order ? order.get('operations') : null;
			var accountIndications = myModel.get('accountIndications');
			var matConf = myModel.get('matConf');
		
			//operation
			var matConfsOperation = null;
			
			if(operations && operations.getCount() > 0) {
				operations.each(function(operation) {
					if(operation.get('vornr') === matConf.get('vornr')) {
						matConfsOperation = operation;
						return false;
					}
				}, this);
			}
			
			var matConfsOperationRecord = matConfsOperation ? Ext.getCmp('matConfOperDropDownBox').findRecordByValue(matConfsOperation) : null;	
			
			if(matConfsOperationRecord)
				Ext.getCmp('matConfOperDropDownBox').setValue(matConfsOperationRecord);
			
			//account indication
			var matConfsAccountIndi = null;
			
			if(accountIndications && accountIndications.getCount() > 0) {
				accountIndications.each(function(accountIndication) {
					if(accountIndication.get('bemot') === matConf.get('bemot')) {
						matConfsAccountIndi = accountIndication;
						return false;
					}
				}, this);
			}
			
			var matConfsAccountIndiRecord = matConfsAccountIndi ? Ext.getCmp('matConfAccIndicationDropDownBox').findRecordByValue(matConfsAccountIndi) : null;	
			
			if(matConfsAccountIndiRecord)
				Ext.getCmp('matConfAccIndicationDropDownBox').setValue(matConfsAccountIndiRecord);
			
			//storage location
			this.fillStorageLocationComboBox(matConf.get('plant'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDropDownBoxesConvenientToCurrentModel of BaseMatConfPage', ex);
		}
	},
	
	refreshMatConfPanel: function() {
		try {
			var matConfGridPanel = Ext.getCmp('matConfGrid');
			matConfGridPanel.setStore(this.getViewModel().get('operation').get('matConfs'));
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshMatConfPanel of BaseMatConfPage', ex);
		}
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			var operation = Ext.getCmp('matConfOperDropDownBox').getValue();
			var storLocObject = Ext.getCmp('matConfStorLocComboBox').getValue();
			var storLoc = '';
			
			if(storLocObject)
				storLoc = storLocObject.get('lgort');
		
			retval = {
				vornr: operation ? operation.get('vornr') : '',
				isdd: Ext.getCmp('matConfDate').getValue(),
				matnr: Ext.getCmp('matConfMaterialNo').getValue(),
				quantity: Ext.getCmp('matConfAmountNumberField').getValue(),
				unit: Ext.getCmp('matConfUnitTextField').getValue(),
				plant: Ext.getCmp('matConfPlantTextField').getValue(),
				storLoc: storLoc,
				accountIndication: Ext.getCmp('matConfAccIndicationDropDownBox').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseMatConfPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	//transfer current values from screen into model
	transferViewStateIntoModel: function() {
		try {
			var matConf = this.getViewModel().get('matConf');
			
			var operation = Ext.getCmp('matConfOperDropDownBox').getValue();
			var accountIndication = Ext.getCmp('matConfAccIndicationDropDownBox').getValue();
			
			matConf.set('vornr', operation ? operation.get('vornr') : '');
			
			//set bemot
			if(accountIndication) {
				matConf.set('accountIndication', accountIndication);
				matConf.set('bemot', accountIndication.get('bemot'));
			} else {
				matConf.set('accountIndication', null);
				matConf.set('bemot', '');
			}
			
            matConf.set('isdd', Ext.getCmp('matConfDate').getValue());
            matConf.set('matnr', Ext.getCmp('matConfMaterialNo').getValue());
            matConf.set('quantity', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp('matConfAmountNumberField').getValue()));
            matConf.set('unit', Ext.getCmp('matConfUnitTextField').getValue());
            matConf.set('plant', Ext.getCmp('matConfPlantTextField').getValue());
            matConf.set('storloc', Ext.getCmp('matConfStorLocComboBox').getValue().get('lgort'));
            
           accountIndication = Ext.getCmp('matConfAccIndicationDropDownBox').getValue();
            matConf.set('bemot', accountIndication ? accountIndication.get('bemot') : '');
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseMatConfPage', ex);
		}
	},
	
	fillMaterialSpecFieldsFromComponent: function(component) {
		try {
			if(component) {
				this.fillStorageLocationComboBox(component.get('werks'));	
			
				Ext.getCmp('matConfMaterialNo').setValue(component.get('matnr'));
				Ext.getCmp('matConfAmountNumberField').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(component.get('bdmng'))); 
				Ext.getCmp('matConfUnitTextField').setValue(component.get('meins'));
				Ext.getCmp('matConfPlantTextField').setValue(component.get('werks'));
				
				this.trySetStorageLocationComboBoxByKeyField(component.get('werks'), component.get('lgort'));
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromComponent of BaseMatConfPage', ex);
		}
	},
	
	fillMaterialSpecFieldsFromStpo: function(stpo) {
		try {
			if(stpo) {
				Ext.getCmp('matConfMaterialNo').setValue(stpo.get('idnrk'));
				Ext.getCmp('matConfAmountNumberField').setValue(1); 
				Ext.getCmp('matConfUnitTextField').setValue(stpo.get('meins'));
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromStpo of BaseMatConfPage', ex);
		}
	},
	
	fillMaterialSpecFieldsFromMatStock: function(matStock) {
		try {
			if(matStock) {
				this.fillStorageLocationComboBox(matStock.get('werks'));	
			
				Ext.getCmp('matConfMaterialNo').setValue(matStock.get('matnr'));
				Ext.getCmp('matConfAmountNumberField').setValue(1); 
				Ext.getCmp('matConfUnitTextField').setValue(matStock.get('meins'));
				Ext.getCmp('matConfPlantTextField').setValue(matStock.get('werks'));
				
				this.trySetStorageLocationComboBoxByKeyField(matStock.get('werks'), matStock.get('lgort'));
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromMatStock of BaseMatConfPage', ex);
		}
	},

	fillMaterialSpecFieldsFromMaterial: function (material) {
	    try {
	        if (material) {
	            Ext.getCmp('matConfMaterialNo').setValue(material.get('matnr'));
	            Ext.getCmp('matConfAmountNumberField').setValue(1);
	            Ext.getCmp('matConfUnitTextField').setValue(material.get('meins'));
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromMaterial of BaseMatConfPage', ex);
	    }
	},
	
	//will try to set stor loc identified by the key values - if not found a message will be triggered and the value will be reset
	//passing incomplete values will just clear the current value
	trySetStorageLocationComboBoxByKeyField: function(werks, lgort) {
		try {
			var storLocComboBox = Ext.getCmp('matConfStorLocComboBox');
		
			storLocComboBox.clearValue();

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
				var targetValue = null;

				var targetStorLoc = null;
				var storLocs = this.getViewModel().get('storLocs');
				
				if(storLocs && storLocs.getCount() > 0) {
					storLocs.each(function(storLoc) {
						if(storLoc.get('werks') === werks && storLoc.get('lgort') === lgort) {
							targetStorLoc = storLoc;
							return true;
						}
					}, this);
				}
				
				targetValue = targetStorLoc ? storLocComboBox.findRecordByValue(targetStorLoc) : null;	
				
				if(targetValue) {
					//value found, set it
					storLocComboBox.setValue(targetValue);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('storageLocationNotFound'), true, 1);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySetStorageLocationComboBoxByKeyField of BaseMatConfPage', ex);
		}
	}
});