Ext.define('AssetManagement.base.view.pages.BaseNotifListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.model.pagemodel.NotifListPageViewModel',
        'AssetManagement.customer.controller.pages.NotifListPageController',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date'        
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.NotifListPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifListPage', ex);
		    }
            
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.NotifListPageRendererMixin'
    },
    
    controller: 'NotifListPageController',
    
    viewModel: {
        type: 'NotifListPageModel'
    },
    
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    //protected
	//@override
    buildUserInterface: function() {
         try {
        	 var myController = this.getController();
        	 
        	 var items = [
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
		                            xtype: 'container',
		                            height: 30,
		                            id: 'searchNotifContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldNotif',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,
		                                    listeners: {
			                                	change: {
			                                        fn: 'onNotifSearchFieldChange'
			                                    }
		                                	}
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
			                                id: 'filterCriteriaNotif',
		                                    editable: false,
		                                    store: [Locale.getMsg('notifNumber'), Locale.getMsg('shorttext'), Locale.getMsg('orderNum'),  
		                                            Locale.getMsg('equipment'), Locale.getMsg('checklist_FuncLoc'), Locale.getMsg('mainWorkctr'),
		                                            Locale.getMsg('sortField'), Locale.getMsg('date'), Locale.getMsg('zipCode'), Locale.getMsg('city')],
		                                    value: Locale.getMsg('notifNumber'),
			                                listeners: {
	                                    		select: 'onNotifSearchFieldChange'
	                                    	}
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },                       
	                            {
			                        xtype: 'container',
			                        id: 'notifGridContainer',
			                        flex: 1,
			                        layout: {
	                            		type: 'hbox',
	                            		align: 'stretch'
	                            	},
			                        items: [
			                           this.getNewNotifGridPanel()
			                        ]
			                    },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];

		    this.add(items);
		    this.searchFieldVisibility();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNotifListPage', ex);
    	}
    },
    
    //private
    renewNotifGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('notifGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewNotifGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewNotifGridPanel of BaseNotifListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewNotifGridPanel: function() {
    	var retval = null;
    	
    	try {
    		var myController = this.getController();
    		var me = this;
    		retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
                id: 'notifGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            loadingText: Locale.getMsg('loadingNotifs'),
	            emptyText: Locale.getMsg('noNotifs'),
	            flex: 1,
	            parentController: myController,
	            owner: me,
	            scrollable: 'vertical',
	            /*columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        return '<img src="resources/icons/notif.png"/>';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,
	                    align: 'center'
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var qmart = record.get('qmart');
	                        var qmnum =  AssetManagement.customer.utils.StringUtils.trimStart(record.get('qmnum'), '0');
	                        var qmtxt = record.get('qmtxt');
	
	                        return qmart + ' ' + qmnum +'<p>'+qmtxt+'</p>';
	                    },
	                    itemId: 'notifColumn',
	                    dataIndex: 'qmnum',
	                    text: Locale.getMsg('notif')
	                },
	                {
	                    xtype: 'datecolumn',
	                    dataIndex: 'strdt',
	                    maxWidth: 125,
	                    minWidth: 125,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        return AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('strdt'));
	                    },
	                    text: Locale.getMsg('date')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                    	var row1 = '';
	                        var row2 = '';
	                        var row3 = '';
	                		
	                		var useEqui = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('equnr'));
	                		
	                		if(useEqui === true) {
	                			var equi = record.get('equipment');
	                			if(equi) {
	                				row1 = equi.get('eqktx');
	                				row2 = equi.get('submt');
	                				/*
	                				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(row2)) {
	                					row2 = AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'));
	                				}
	                				
	                				row3 = AssetManagement.customer.utils.StringUtils.concatenate([ equi.get('baujj'), equi.get('baumm') ], '/', false, false, '-');
	                			} else {
	                				row1 = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
	                			}
	                		} else {
	                			var funcLoc = record.get('funcLoc');
	                			
	                			if(funcLoc) {
	                				row1 = funcLoc.get('pltxt');
	                				row2 = funcLoc.get('tplnr');
	                				
									if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(row2)) {
										row2 = funcLoc.get('tplnr');
									}
									
	                				row3 = AssetManagement.customer.utils.StringUtils.concatenate([ funcLoc.get('baujj'), funcLoc.get('baumm') ], '/', false, false, '-');
	                			} else {
	                				row1 = record.get('tplnr');
	                			}
	                		}
	
	                        return row1 +'<p>'+row2+'</p><p>'+row3+'</p>';
	                    },
	                    text: Locale.getMsg('techObject')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		var name = '';
	                        var street = '';
	                        var postalCode = '';
	                        
	                        var address = record.getObjectAddress();
							if(address !== null && address !== undefined) {
								name = address.getName();
								street = AssetManagement.customer.utils.StringUtils.concatenate([ address.getStreet(), address.getHouseNo() ], null, true);
								postalCode = AssetManagement.customer.utils.StringUtils.concatenate([ address.getPostalCode(), address.getCity() ], null, true);
							} else {
							  	name = '';
	                            street = '';
	                            postalCode = '';
							}
							    
	                        return name +'<p>'+street+'</p><p>'+postalCode+'</p>';
	                 	},
	                    enableColumnHide: false,
	                    dataIndex: 'postalCode',
	                    text: Locale.getMsg('address')
	                },
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		if(!record.getObjectAddress()) {
	                			metaData.style = 'display: none;';
	                		} else {
	                			metaData.style = '';
	                		}
	                	},
	                	text: Locale.getMsg('maps'),
	                	maxWidth: 80,
	                    width: 80,
	                    items: [{
	                        icon: 'resources/icons/maps_small.png',
	                        tooltip: 'Google Maps',
	                        iconCls: 'oxGridLineActionButton',
	                        handler: function(grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
	                        	event.stopEvent();
	                            this.getController().navigateToAddress(record);
	                        },
	                        scope: this
	                    }],
	                    enableColumnHide: false,
	                    align: 'center'
	                }
	            ],*/
	            listeners: {
	            	cellclick: myController.onNotifSelected,
	            	cellcontextmenu: myController.onCellContextMenu,
	            	scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewNotifGridPanel of BaseNotifListPage', ex);
    	}
    	
    	return retval;
	},
    
	//protected
	//@override    
    getPageTitle: function() {
    	var retval = '';
    
    	try {
	    	var title = Locale.getMsg('notifications');
	    	
	    	var notifs = this.getViewModel().get('notifStore');
	    	
	    	if(notifs) {
	    		title += " (" + notifs.getCount() + ")";
	    	}
	    	
	    	retval = title;
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNotifListPage', ex);
    	}
    	

    	return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
    	try {
    		var notifStore = this.getViewModel().get('notifStore');
    		
    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    		    this.renewNotifGridPanel();

    		if (this.getViewModel().get('constrainedNotifs')) {
    		    notifStore = this.getViewModel().get('constrainedNotifs');
    		}
    		
    		var notifGridPanel = Ext.getCmp('notifGridPanel');
    		
    		notifGridPanel.setStore(notifStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNotifListPage', ex);
    	}
	},
	
	//public
	//@override
	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('notifGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseNotifListPage', ex);
    	}
	},
	
	//private
    searchFieldVisibility: function(){
		try { 	
	       var field = Ext.getCmp('searchNotifContainer');	
	       var para = true;
	
	       if (para === false)
	    	   field.setVisible(false);

		} catch(ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseNotifListPage', ex);
		}
	},
	
	//private
	getSearchValue: function() {
		try {
			var notifToSearch = Ext.create('AssetManagement.customer.model.bo.Notif', {});
			var currentFilterCriterion = Ext.getCmp('filterCriteriaNotif').getValue();	

			// Meldungsnummer
			if(currentFilterCriterion === Locale.getMsg('notifNumber'))
				notifToSearch.set('qmnum', Ext.getCmp('searchFieldNotif').getValue());
			// Kurztext
			else if(currentFilterCriterion === Locale.getMsg('shorttext'))
				notifToSearch.set('qmtxt', Ext.getCmp('searchFieldNotif').getValue());
			// Auftragsnummer	
			else if(currentFilterCriterion === Locale.getMsg('orderNum'))
				notifToSearch.set('aufnr', Ext.getCmp('searchFieldNotif').getValue());
			// Equipment
			else if(currentFilterCriterion === Locale.getMsg('equipment')) {
				var equipment = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
				equipment.set('eqktx', Ext.getCmp('searchFieldNotif').getValue());
				notifToSearch.set('equipment', equipment);
				notifToSearch.set('equnr', Ext.getCmp('searchFieldNotif').getValue())
			// Techn. Platz
			} else if(currentFilterCriterion === Locale.getMsg('checklist_FuncLoc')) {
				var funcLoc = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
				funcLoc.set('pltxt', Ext.getCmp('searchFieldNotif').getValue());
				notifToSearch.set('funcLoc', funcLoc);
				notifToSearch.set('tplnr', Ext.getCmp('searchFieldNotif').getValue())
			//Arbeitsplatz
			} else if(currentFilterCriterion === Locale.getMsg('mainWorkctr'))
				notifToSearch.set('gewrk', Ext.getCmp('searchFieldNotif').getValue());
			// Soertierfeld
			else if(currentFilterCriterion === Locale.getMsg('sortField')) {
				var equi = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
				equi.set('eqfnr', Ext.getCmp('searchFieldNotif').getValue());
				notifToSearch.set('equipment', equi);
			}
			// Datum
			else if(currentFilterCriterion === Locale.getMsg('date')) {
				notifToSearch.set('strdt', Ext.getCmp('searchFieldNotif').getValue());
			}
			// PLZ
			else if(currentFilterCriterion === Locale.getMsg('zipCode')) {				
				notifToSearch.set('address', Ext.create('AssetManagement.customer.model.bo.Address', {postCode1: Ext.getCmp('searchFieldNotif').getValue()}))
			}
			// Ort
			else if(currentFilterCriterion === Locale.getMsg('city')) {				
				notifToSearch.set('address', Ext.create('AssetManagement.customer.model.bo.Address', {city1: Ext.getCmp('searchFieldNotif').getValue()}))
			}
			
			this.getViewModel().set('searchNotif', notifToSearch);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseNotifListPage', ex);
		}
	},


    //public
	getFilterValue: function () {
	    var retval = '';

	    try {
	        retval = Ext.getCmp('searchFieldNotif').getValue();

	        if (!retval)
	            retval = '';
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseNotifListPage', ex);
	    }

	    return retval;
	},
    //public
	setFilterValue: function (value) {
	    try {
	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
	            value = '';

	        retval = Ext.getCmp('searchFieldNotif').setValue(value);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseNotifListPage', ex);
	    }
	}
});