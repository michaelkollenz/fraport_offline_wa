Ext.define('AssetManagement.base.view.pages.BaseGoogleMapsPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.GoogleMapsPageViewModel',
        'AssetManagement.customer.controller.pages.GoogleMapsPageController',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.container.Container',
        'Ext.Component',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.GoogleMapsPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseGoogleMapsPage', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'GoogleMapsPageModel'
    },
    
    controller: 'GoogleMapsPageController',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    _mapsDiv: null,
    
    buildUserInterface: function() {
    	try {
    		this._mapsDiv = document.createElement('div');
    		this._mapsDiv.id = "googleMapsDiv";
    		this._mapsDiv.style.border = "1px solid";
    		this._mapsDiv.style.width = "100%";
    		this._mapsDiv.style.height = "100%";
    		
		    var items = [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
                        {
                            xtype: 'component',
                            height: 30
                        },
                        {
                            xtype: 'label',
                            cls: 'oxHeaderLabel',
                            text: Locale.getMsg('mapsView')
                        },
                        {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                	xtype: 'container',
		                	id: 'mapsDivContainer',
			    			flex: 1,
				            listeners: {
				             	boxready: this.appendMapsDivIntoView,
				             	resize: this.mapsContainersSizeChanged,
				             	scope: this
				            }
		                }
	                ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseGoogleMapsPage', ex);
    	}
    },
    
    appendMapsDivIntoView: function(mainverticalcontainer, width, height) {
    	try {
    		var hacked = document.getElementById(mainverticalcontainer.getId() + "-innerCt");
    		
    		if(!document.getElementById('googleMapsDiv'))
    			hacked.appendChild(this._mapsDiv);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside appendMapsDivIntoView of BaseGoogleMapsPage', ex);
    	}
	},
	
	mapsContainersSizeChanged: function(mapsDivContainer, width, height, oldWidth, oldHeight, eOpts) {
    	try {
    		this.getController().adjustMapToNewSize(width, height);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapsContainersSizeChanged of BaseGoogleMapsPage', ex);
    	}
    },
   
    getPageTitle: function() {
		var retval = '';
   
		try {
			var retval = Locale.getMsg('googleMaps');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseGoogleMapsPage', ex);
		}

		return retval;
    },

    getMapsDiv: function() {
	   var retval = null;
	   
	   try {
	        retval = this._mapsDiv;
	   } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMapsDiv of BaseGoogleMapsPage', ex);
	   }
	   
	   return retval;
    }
});