Ext.define('AssetManagement.base.view.pages.BaseExtConfPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.ExtConfPageViewModel',
        'AssetManagement.customer.controller.pages.ExtConfPageController',
        'Ext.container.Container',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
			    if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.ExtConfPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseExtConfPage', ex);
		    }
            
            return this._instance;
        }
    },
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.ExtConfPageRendererMixin'
    },
    viewModel: {
        type: 'ExtConfPageModel'
    },
    
    controller: 'ExtConfPageController',
    
    buildUserInterface: function() {
        try {
            var myController = this.getController();
            var me = this;
    		var items = [
			        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [            
			        {
			            xtype: 'component',
			            maxWidth: 10,
			            minWidth: 10
			        },	        
			        {
			            xtype: 'container',
			            flex: 1,
			            layout: {
			                type: 'vbox',
			                align: 'stretch'
			            },
			            items: [
			                {
			                    xtype: 'component',
			                    height: 10
			                },
			                {
			                    xtype: 'label',
			                    cls: 'oxHeaderLabel',
			                    id: 'extConfPageModeLabel',
			                    text: Locale.getMsg('createActivityConf')
			                },
			                {
			                    xtype: 'component',
			                    height: 20
			                },
			                {
			                    xtype: 'container',
			                    layout: {
			                        type: 'hbox',
			                        align: 'stretch'
			                    },
			                    items: [
			                        {
			                            xtype: 'container',
			                            flex: 1,
			                            layout: {
			                                type: 'vbox',
			                                align: 'stretch'
			                            },
			                            items: [
			                                {
			                                    xtype: 'oxtextfield',
			                                    fieldLabel: Locale.getMsg('order'),
			                                    labelWidth: 130,
			                                    id: 'extConfOrderTextField',
			                                    disabled: true
			                                },
			                                {
			                                    xtype: 'component',
			                                    height: 10
			                                },
			                                {
			                                    xtype: 'oxcombobox',
			                                    fieldLabel: Locale.getMsg('materialNumberShort'),
			                                    id: 'extConfMaterialNoComboBox',
				                                displayField: 'text',
			                                    queryMode: 'local',
			                                    valueField: 'value',
			                                    editable: false,
			                                    labelWidth: 130,
			                                    listeners: {
			                                    	select: 'onMaterialSelected'
			                                	}
			                                },
			                                {
			                                    xtype: 'component',
			                                    height: 10
			                                },
			                                {
			                                    xtype: 'container',
			                                    layout: {
			                                        type: 'hbox',
			                                        align: 'stretch'
			                                    },
			                                    items: [
			                                        {
			                                        	xtype: 'oxnumberfield',
			                                            fieldLabel: Locale.getMsg('quantityUnit'),
			                                            id: 'extConfAmountNF',
			                                            maxLength: 13,
			                                            labelWidth: 130
			                                        },
			                                        {
			                                            xtype: 'component',
			                                            width: 10
			                                        },
			                                        {
			                                            xtype: 'oxtextfield',
			                                            id: 'extConfUnitTF',
			                                            labelStyle: 'display: none;',
			                                            flex: 0.2,
			                                            maxLength: 3,
					                                    disabled: true
			                                        },
			                                        {
				                                        xtype: 'container',
						                                id: 'extConfPriceCurrencyContainer',
						                                margin: '0 0 0 6',
						                                hidden: true,
						                                flex: 1,
					                                    layout: {
					                                        type: 'hbox',
					                                        align: 'stretch'
					                                    },
				                                    	items: 
				                                    	[
					                                      
					                                        {
					                                            xtype: 'oxnumberfield',
					                                            flex: 1,
					                                            fieldLabel: Locale.getMsg('priceCurrency'),
					                                            id: 'extConfPriceNF',
					                                            maxLength: 13,
					                                            labelWidth: 100
					                                        },
					                                        {
					                                            xtype: 'component',
					                                            width: 10
					                                        },
					                                        {
					                                            xtype: 'oxtextfield',
					                                            id: 'extConfCurrencyTF', 
					                                            labelStyle: 'display: none;',
					                                            maxLength: 3,
					                                            width: 60
					                                        }
					                                    ]
					                                }
			                                    ]
			                                    
			                                },
			                                {
			                                    xtype: 'component',
			                                    height: 10
			                                },
			                                {
			                                    xtype: 'oxtextfield',
			                                    fieldLabel: Locale.getMsg('description'),
			                                    maxLength: 40,
			                                    labelWidth: 130,
			                                    hidden: true,
			     	                            id: 'extConfDescriptionTextField'
			                                },
                                            {
                                            	xtype: 'checkbox',
                                                baseCls: 'oxCheckBox',
                                                id: 'finalConfCheckBox',
                                                checkedCls: 'checked',
                                                fieldLabel: Locale.getMsg('finalConfirmationDoubleDot'),
                                                labelStyle: 'padding-top: 8px;',
                                                labelWidth: 130,
                                                width: 30,
                                                height: 35
                                            }
			                            ]
			                        }
			                    ]
			                },
			                {
			                    xtype: 'component',
			                    height: 30
			                },
			                {
		                        xtype: 'container',
		                        layout: {
		                            type: 'hbox',
		                            align: 'stretch'
		                        },
		                        items: [
		                            {
		                                xtype: 'component',
		                                minWidth: 5,
		                                maxWidth: 5
		                            },
		                            {
		                                xtype: 'label',
		                                flex: 1,
		                                cls: 'oxHeaderLabel',
		                                text: Locale.getMsg('activityConfs_RecActivityConfs')
		                            }
		                        ]
		                    },
			                {
			                    xtype: 'component',
			                    height: 20
			                },
			                {
			                    xtype: 'oxdynamicgridpanel',
			                    id: 'confGridPanel',
			                    disableSelection: true,
				                listeners: {
			                    	cellcontextmenu: 'onCellContextMenu'
				                },
				                parentController: myController,
                                owner: me
			                    /*columns: [
			                        {
			                            xtype: 'gridcolumn',
			                            maxWidth: 80,
			                            minWidth: 80, 
			                            align: 'center',
				                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                return '<img src="resources/icons/timeconf.png"  />';
			                            }
			                        },
			                        {
			                            xtype: 'gridcolumn',
			                            text: Locale.getMsg('material'),
			                            flex: 1,
				                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                var matnr = record.get('matnr');
			                                var maktx = record.get('material') ? record.get('material').get('maktx') : '';
			
			                                return AssetManagement.customer.utils.StringUtils.concatenate([ matnr, maktx ], null, true);
			                            }
			                        },
			                        {
			                            xtype: 'gridcolumn',
			                            text: Locale.getMsg('materialDescription'),
			                            flex: 1,
				                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				                        	var description = ''; 
				                        	if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('txz01'))) {
				                        		description = record.get('txz01');
				                        	} else if(record.get('material')) {
				                        		description = record.get('material').get('maktx');
				                        	}
			                        			
			                                return description;
			                            }
			                        },
			                        {
			                            xtype: 'datecolumn',
			                            maxWidth: 125,
			                            minWidth: 125,
			                            dataIndex: 'bedat',
			                            text: Locale.getMsg('date'), 
				                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                var bedat = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(record.get('bedat'));
			
			                                return bedat;
			                            }
			                        },
			                        {
			                            xtype: 'gridcolumn',
			                            text: Locale.getMsg('quantityUnit'),
			                            flex: 1,
				                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                        		var menge = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
			                                var meins = record.get('meins');
			
			                                return menge  + ' ' + meins;
			                            }
			                        }
			                    ]*/
			                }
			            ]
			        },
		            {
		                xtype: 'component',
		                width: 30
		            }
			    ]
		      }
		    ];
			    
			this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseExtConfPage', ex);
		}
	},

    getPageTitle: function() {
		var retval = '';
    
		try {
			var title = Locale.getMsg('extConfsForOrder');
	    	
	    	var order = this.getViewModel().get('order');
	    	
	    	if(order) {
	    		title += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(order.get('aufnr'));
	    	}
	
	    	retval = title;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseExtConfPage', ex);
		}
		
		return retval;
	},
	
	//protected
	//@override	
	updatePageContent: function() {
		try {
			this.fillMaterialsDropDownBox();
		
			this.transferModelStateIntoView();
			
			this.manageSmallPartsInputFieldsVisibility();
			
			this.refreshExtConfPanel();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseExtConfPage', ex);
		}
	},

	/**
	 * fills dropdown with the specific materials from customizing
	 */
	fillMaterialsDropDownBox: function() {
		try {
			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			var extConfMaterials = this.getViewModel().get('extConfMaterials'); 
			
			if(extConfMaterials && extConfMaterials.getCount() > 0) {
				extConfMaterials.each(function(material) {
					store.add({ text: material.get('matnr') + " - " + material.get('maktx'), value: material });
				}, this);
			}
			
			Ext.getCmp('extConfMaterialNoComboBox').clearValue();
			Ext.getCmp('extConfMaterialNoComboBox').setStore(store);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialsDropDownBox of BaseExtConfPage', ex);
		}
	},
	
	refreshExtConfPanel: function() {
		try {
			var banfPositions = this.getViewModel().get('banfPositions')
			Ext.getCmp('confGridPanel').setStore(banfPositions);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshExtConfPanel of BaseExtConfPage', ex);
		}
	},
		 
    transferModelStateIntoView: function() {
    	try {
    		var myModel = this.getViewModel();
    		var order = myModel.get('order');
    		var extConf = myModel.get('extConf');
    		var editMode = myModel.get('isEditMode');
    		var hasFinalConf = myModel.get('hasFinalConf');
    		
    		//set mode label
    		Ext.getCmp('extConfPageModeLabel').setText(editMode === true ? Locale.getMsg('editActivityConf') : Locale.getMsg('createActivityConf'));
		
    		//set order information
    		Ext.getCmp('extConfOrderTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0') + " " + order.get('ktext'));
    	
			Ext.getCmp('extConfUnitTF').setValue(extConf.get('meins'));
			Ext.getCmp('extConfAmountNF').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(extConf.get('menge')));
			
			Ext.getCmp('extConfDescriptionTextField').setValue(extConf.get('txz01'));
			Ext.getCmp('extConfPriceNF').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(extConf.get('preis')));
			Ext.getCmp('extConfCurrencyTF').setValue(extConf.get('waers'));
			
			//disable the final checkbox, if a final flagged time conf already exists for the context
			Ext.getCmp('finalConfCheckBox').setDisabled(hasFinalConf);
			Ext.getCmp('finalConfCheckBox').setValue(hasFinalConf);
			
			//set drop down box values
	    	this.setDropDownBoxesConvenientToCurrentModel();
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDataWithBanf of BaseExtConfPage', ex);
		}
    },
    
    setDropDownBoxesConvenientToCurrentModel: function() {
		try {
			var myModel = this.getViewModel();
			var materials = myModel.get('materials');
			var extConf = myModel.get('extConf');
		
			//materials
			var extConfsMaterial = null;
			
			if(materials && materials.getCount() > 0) {
				var matnrToSearch = extConf.get('matnr');
			
				materials.each(function(material) {
					if(material.get('matnr') === matnrToSearch) {
						extConfsMaterial = material;
						return false;
					}
				}, this);
			}
			
			var extConfsMaterialRecord = extConfsMaterial ? Ext.getCmp('extConfMaterialNoComboBox').findRecordByValue(extConfsMaterial) : null;
			
			if(extConfsMaterialRecord)
				Ext.getCmp('extConfMaterialNoComboBox').setValue(extConfsMaterialRecord);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDropDownBoxesConvenientToCurrentModel of BaseExtConfPage', ex);
		}
	},

    /**
	 * fill material specific textfields if material has been choosen from materialpicker
	 */
    fillMaterialSpecFields: function(material) {
	    try {
		    if(material) {
	    		Ext.getCmp('extConfUnitTF').setValue(material.get('meins'));
	    		Ext.getCmp('extConfAmountNF').setValue(1);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFields of BaseExtConfPage', ex);
		}
    }, 
    
    transferViewStateIntoModel: function() {
    	try {
	    	var extConf = this.getViewModel().get('extConf');
	    	
	    	var material = Ext.getCmp('extConfMaterialNoComboBox').getValue(); 
	    	
	    	//general information
	    	extConf.set('matnr', material ? material.get('matnr') : ''); 				//Materialnummer
	    	extConf.set('menge', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp('extConfAmountNF').getValue()));	//Bestellanforderungsmenge
	    	extConf.set('meins', Ext.getCmp('extConfUnitTF').getValue());				//Einheit
	    	
	    	//small part information
	    	extConf.set('txz01', Ext.getCmp('extConfDescriptionTextField').getValue()); //Kleinteilinformation
	    	extConf.set('preis', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp('extConfPriceNF').getValue())); 	//Preis
	   		extConf.set('waers', Ext.getCmp('extConfCurrencyTF').getValue()); 			//Währung
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseExtConfPage', ex);
		}
    },
    
    //get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			var aueru = ''; 
			if(Ext.getCmp('finalConfCheckBox').checked)
				aueru = 'X';
				
			var material = Ext.getCmp('extConfMaterialNoComboBox').getValue(); 
	    		
			retval = {
				matnr: material ? material.get('matnr') : '',
				menge: Ext.getCmp('extConfAmountNF').getValue(),
				meins: Ext.getCmp('extConfUnitTF').getValue(),
				txz01: Ext.getCmp('extConfDescriptionTextField').getValue(),
				preis: Ext.getCmp('extConfPriceNF').getValue(),
				waers: Ext.getCmp('extConfCurrencyTF').getValue(), 
				finalConf: aueru
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseExtConfPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	//will hide or show the input fields for small pieces, depending on the currently selected material
	manageSmallPartsInputFieldsVisibility: function() {
		try {
			var isKtp = this.getController().isCurrentlySelectedMaterialSmallPartsExpense();
		
			Ext.getCmp('extConfPriceCurrencyContainer').setHidden(!isKtp);
			Ext.getCmp('extConfDescriptionTextField').setHidden(!isKtp);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSmallPartsInputFieldsVisibility of BaseExtConfPage', ex);
		}
	},
	
	//will clear small parts input fields and set a default currency
	resetSmallPartsInputFields: function(defaultCurrency) {
		try {
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(defaultCurrency))
				defaultCurrency = '';
		
			Ext.getCmp('extConfDescriptionTextField').setValue('');
			Ext.getCmp('extConfPriceNF').setValue(null);
			Ext.getCmp('extConfCurrencyTF').setValue(defaultCurrency);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetSmallPartsInputFields of BaseExtConfPage', ex);
		}
	}
});