Ext.define('AssetManagement.base.view.pages.BaseNotifDetailPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.NotifDetailPageViewModel',
        'AssetManagement.customer.controller.pages.NotifDetailPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.FileSelectionButton',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action'
    ],

    inheritableStatics: {
		_instance: null,

        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.NotifDetailPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifDetailPage', ex);
		    }

            return this._instance;
        }
    },

    config: {
		notif: null
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.NotifDetailPageRendererMixin'
    },

	_notifRow: null,
	_notifTypeRow: null,
	_funcLocRow: null,
	_equipmentRow: null,
	_orderRow: null,
	_notifDateRowRight: null,
	_reporterRowRight: null,
	_disruptionFromRowRight: null,
	_disruptionToRowRight: null,
	_notifDateRowBottom: null,
	_reporterRowBottom: null,
	_disruptionFromRowBottom: null,
	_disruptionToRowBottom: null,
    _codeRow: null,

    viewModel: {
        type: 'NotifDetailPageModel'
    },

    controller: 'NotifDetailPageController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
    		var items =  [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    width: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'notifUpperContent',
			                        layout: {
			                            type: 'vbox',
			                            align: 'stretch'
			                        },
			                        items: [
			                            {
			                                xtype: 'label',
			                                margin: '0 0 0 5',
			                                cls: 'oxHeaderLabel',
			                                text: Locale.getMsg('generalData')
					                    },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'container',
		                                    layout: {
		                                        type: 'hbox'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'container',
		                                            flex: 1,
		                                            id: 'notifLeftGrid'

		                                        },
		                                        {
		                                            xtype: 'container',
		                                            padding: '0 0 0 20',
		                                            flex: 1,
		                                            id: 'notifRightGrid',
		                                            plugins: 'responsive',
		                                            responsiveConfig: {
		                                        	'width <= 800': {
		                                        			visible: false
		                                    			},
		                                    		'width > 800': {
		                                    				visible: true
		                                    			}
		                                    		}
		                                        }
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                        	xtype: 'container',
		                            margin: '40 0 0 0',
		                            id: 'notifBottomGrid',
		                            plugins: 'responsive',
		                            responsiveConfig: {
		                        	'width <= 800': {
		                        		visible: true
		                    		},
		                    		'width > 800': {
		                    			visible: false
		                    		}
		                    	  }
		                        },
                                {
	                                xtype: 'container',
	                                id: 'notifItemsGridSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('notifItems')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifItemsGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                parentController: myController,
                                            owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        	 	return '<img src="resources/icons/notif_item.png"/>';
		                                            },
		                                            id: 'notifItemActionColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifItemColumn',
		                                            flex: 1,
		                                            dataIndex: 'codegrkurztext',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	if(record.get('damageCustCode'))
			                                        	{
			                                        		var damageGrp = record.get('damageCustCode').get('codegrkurztext');//record.get('fegrp');
			                                        		var damageCode = record.get('damageCustCode').get('kurztext');//record.get('fecod');

			                                        		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(damageGrp))damageGrp = '';
				                                    		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(damageCode))damageCode = '';

			                                        		return damageGrp + '<p>' + damageCode + '</p>';
			                                        	}
			                                        	else
			                                        		return '';
			                                        },
		                                            text: Locale.getMsg('checklist_Damage')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifItemObjColumn',
		                                            flex: 1,
		                                            dataIndex: 'codegrkurztext',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		if(record.get('objectPartCustCode'))
		                                        		{
		                                        			var otgrp = record.get('objectPartCustCode').get('codegrkurztext'); //record.get('otgrp');
		    	                                    		var oteil =  record.get('objectPartCustCode').get('kurztext');//record.get('oteil');
		    	                                    		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(otgrp))otgrp = '';
		    	                                    		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(oteil))oteil = '';
		    	                                    		return otgrp + '<p>' + oteil + '</p>';
		                                        		}
		                                        		else
		                                        			return '';

			                                        },
		                                            text: Locale.getMsg('objectPart')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifDamageCauseColumn',
		                                            flex: 0.5,
		                                            text: Locale.getMsg('cause'),
		                                            dataIndex: 'notifItemCauses',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	var count = record.get('notifItemCauses').getCount().toString();
			                                        	if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count))count = '';
			                                    		return count;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifItemActivityColumn',
		                                            flex: 0.5,
			                                        text: Locale.getMsg('edit_NotifActivities'),
		                                            dataIndex: 'notifItemActivities',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	var count = record.get('notifItemActivities').getCount().toString();
			                                        	if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count))count = '';
			                                    		return count;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'NotifItemTaskColumn',
		                                            flex: 0.5,
		                                            text: Locale.getMsg('notifTasks'),
		                                            dataIndex: 'notifItemTasks',
		                                            maxWidth: 80,
		                                            minWidth: 80,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	var count = record.get('notifItemTasks').getCount().toString();
			                                        	if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(count))count = '';
			                                    		return count;
			                                        }
		                                        }
		                                    ],*/
			                                //clicklistener to detailPage
			                               	listeners: {
		                            			cellclick: myController.onItemSelected,
		                            			cellcontextmenu: myController.onCellContextMenuSubOjects,
		                            			scope: myController
			                            	},
		                                	//grid footer
											dockedItems: [
												{
											    xtype: 'toolbar',
											    dock: 'bottom',
											    height: 50,
											    id: 'notifItemsFooterPanel',
											    layout: {
											        type: 'hbox',
											        align: 'middle'
											    },
											    items:
											    	[
											    	 	{
												            xtype: 'container',
												            flex: 1,
												            layout: {
												                type: 'hbox',
												                align: 'middle'
												            },
												            items:
												            	[
													                {
													                    xtype: 'component',
													                    maxWidth: 6,
							                                            minWidth: 6
													                },
													                {
													                	xtype: 'button',
													                    html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
													                    height: 40,
													                    width: 50,
													                    padding: 0,
													                    listeners: {
		                                                                	click: 'addItemClick'
		                                                                }
													                },
													                {
													                    xtype: 'label',
													                    flex: 1,
													                    text: Locale.getMsg('addNotifItem')
													                }
												            ]
											          	}
											        ]
												}
											]
                                        }
                                    ]

		                        },
                                {
	                                xtype: 'container',
	                                id: 'notifActivityGridSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('notifActivities')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifActivitiesGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
                                            owner: me,
		                                    dockedItems:
		                                    	[
		                                    	 	{
					                                    xtype: 'toolbar',
					                                    dock: 'bottom',
					                                    height: 50,
					                                    id: 'notifHeadActivityFooterPanel',
					                                    layout: {
					                                        type: 'hbox',
					                                        align: 'middle'
					                                	},
					                                	items:
					                                		[
						                                	 	{
								                                    xtype: 'container',
								                                    flex: 1,
								                                    layout: {
								                                        type: 'hbox',
								                                        align: 'middle'
								                                    },
								                                    items:
								                                    [
							                                    	 	{
							                                    	 		xtype: 'component',
							                                    	 		maxWidth: 6,
							                                    	 		minWidth: 6
							                                    	 	},
							                                    	 	{
							                                        	   xtype: 'button',
							                                                html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
							                                                height: 40,
							                                                width: 50,
							                                                padding: 0,
							                                                listeners: {
							                                        			click: 'addActivityClick'
							                                               }
							                                            },
							                                            {
							                                                xtype: 'label',
							                                                flex: 1,
							                                                text: Locale.getMsg('addNotifActivity')
							                                            }
							                                        ]
						                                	 	}
						                                	]
		                                    	 	}
		                                    	 ],
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/notif_activity.png"/>';
		                                            },
		                                            id: 'notifActionsColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifActivityActColumn',
		                                            flex: 1,
		                                            dataIndex: 'codegrkurztext',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        	if(record.get('activityCustCode')) {
	                                        			var activityGrp = record.get('activityCustCode').get('codegrkurztext');
	    												var activityCode = record.get('activityCustCode').get('kurztext');

	    	                                    		return activityGrp + '<p>' + activityCode + '</p>';
		                                        	}
		                                        	else
		                                        		return '';
			                                        },
		                                            text: Locale.getMsg('notifActivity')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifActivityDescrColumn',
		                                            flex: 1,
		                                            dataIndex: 'matxt',
		                                            text: Locale.getMsg('specification')
		                                        }
		                                    ],*/
			                                listeners: {
			                            		cellcontextmenu: myController.onCellContextMenuSubOjects,
			                            		scope: myController
			                            	}
                                        }
                                    ]
		                        },
                                {
	                                xtype: 'container',
	                                id: 'notifHeadTaskGridSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('notifTasks')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifTasksGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
                                            owner: me,
		                                    dockedItems: [
                                                {
				                                    xtype: 'toolbar',
				                                    dock: 'bottom',
				                                    height: 50,
				                                    id: 'notifHeadTaskFooterPanel',
				                                    layout: {
				                                        type: 'hbox',
				                                        align: 'middle'
				                                    },

				                                    items:
				                                    [
			                                            {
					                                        xtype: 'container',
					                                        flex: 1,

					                                        layout: {
					                                            type: 'hbox',
					                                            align: 'middle'
			                                            	},
					                                        items: [
					                                            {
					                                                xtype: 'component',
					                                                maxWidth: 6,
						                                            minWidth: 6
					                                            },
					                                            {
				                                            	   xtype: 'button',
				                                                    html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
				                                                    height: 40,
				                                                    width: 50,
				                                                    padding: 0,
				                                                    listeners: {
				                                            			click: 'addTaskClick'
				                                                    }
				                                                },
				                                                {
				                                                    xtype: 'label',
				                                                    flex: 1,
				                                                    text: Locale.getMsg('addNotifTask')
				                                                }
					                                        ]
	                                            		}
				                                    ]
		                                		}
		                                	],
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/notif_task.png"/>';
		                                            },
		                                            id: 'notifTaskActionColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifTaskMassColumn',
		                                            flex: 1,
		                                            dataIndex: 'codegrkurztext',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	if(record.get('taskCustCode')){
			                                        		var taskGrp = record.get('taskCustCode').get('codegrkurztext');
				                                    		var taskCode =  record.get('taskCustCode').get('kurztext');

				                                    		return taskGrp + '<p>' + taskCode + '</p>';
			                                        	}

			                                        	return '';
			                                        },
		                                            text: Locale.getMsg('notifTask')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifTaskDescrColumn',
		                                            flex: 1,
		                                            dataIndex: 'matxt',
		                                            text: Locale.getMsg('specification')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifTaskUserColumn',
		                                            dataIndex: '',
		                                            maxWidth: 120,
		                                            minWidth: 120,
		                                            text: Locale.getMsg('userStatus')
		                                        }
		                                    ],*/
			                                listeners: {
			                            		cellcontextmenu: myController.onCellContextMenuSubOjects,
			                            		scope: myController
			                            	}
                                        }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'notifDetailPageSDOrderItemsSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('materialOrders')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifDetailPageSDOrderItemsGridPanel',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
			                                owner: me,
		                                    dockedItems: [
		                                         {
			                                        xtype: 'toolbar',
			                                        dock: 'bottom',
			                                        height: 50,
			                                        layout: {
			                                            type: 'hbox',
			                                            align: 'middle'
			                                        },
			                                        items: [
	                                                {
	                                                    xtype: 'container',
	                                                    flex: 1,
	                                                    layout: {
	                                                        type: 'hbox',
	                                                        align: 'middle'
	                                                    },
	                                                    items: [
			                                                    {
			                                                        xtype: 'button',
			                                                        margin: '0 0 0 6',
			                                                        html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
			                                                        height: 40,
			                                                        width: 50,
				                                                    padding: 0,
			                                                        listeners: {
				                    	                                click: 'onAddSDOrderItemClick'
				                    	                            }
			                                                    },
			                                                    {
			                                                        xtype: 'label',
			                                                        flex: 1,
			                                                        text: Locale.getMsg('addSDOrder')
			                                                    }
			                                                ]
			                                         	}
			                                        ]
			                                    }
			                                ],
		                                    /*columns: [
		                                        {
				                                	xtype: 'actioncolumn',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    return '<img src="resources/icons/demand_requirement.png"  />';
					                                },
					                                enableColumnHide: false,
					                                maxWidth: 80,
				                                    minWidth: 80,
				                                    align: 'center'
				                                },
				                                {
				                                    xtype: 'gridcolumn',
					                                maxWidth: 140,
					                                minWidth: 140,
				                                    text: Locale.getMsg('sdOrderPosition'),
				                                    dataIndex: 'bstnk',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                	var bstnk = AssetManagement.customer.utils.StringUtils.trimStart(record.get('bstnk'), '0');
					                                    var posnr = record.get('posnr');
					                                    return bstnk +'<p>'+posnr+'</p>';
					                                },
					                                enableColumnHide: false
				                                },
				                                {
				                                    xtype: 'gridcolumn',
				                                    flex: 1,
				                                    text: Locale.getMsg('material'),
				                                    dataIndex: 'matnr',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                	var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
					                            		var maktx = record.get('material') ? record.get('material').get('maktx') : '';

					                            		return AssetManagement.customer.utils.StringUtils.concatenate([ matnr, maktx ], null, true);
					                                },
					                                enableColumnHide: false
				                                },
				                                {
				                                    xtype: 'gridcolumn',
					                                maxWidth: 120,
					                                minWidth: 120,
				                                    text: Locale.getMsg('quantityUnit'),
				                                    dataIndex: 'zmeng',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
				                                		var unit = record.get('zieme');

				                                		return quantity + ' ' + unit;
					                                },
				                                    enableColumnHide: false
				                                },
				                                {
					                                xtype: 'gridcolumn',
					                                maxWidth: 130,
					                                minWidth: 130,
					                                dataIndex: 'lfsga',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		return record.getDeliveryStatus();
					                             	},
					                                enableColumnHide: false,
					                                text: Locale.getMsg('deliveryStatus')
					                            },
				                                {
				                                    xtype: 'gridcolumn',
				                                    flex: 2,
				                                    dataIndex: 'arktx',
				                                    text: Locale.getMsg('remarks'),
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				                                		return record.get('arktx');
					                                },
				                                    enableColumnHide: false
				                                }
		                                    ],*/
			                                listeners: {
			                                	cellclick: myController.onSDOrderItemSelected,
			                                	cellcontextmenu: myController.onCellContextMenuSDOrderList,
			                                	scope: myController
		    	                        	}
		                                }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'notifFileSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('filesAndDocuments')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifFileGridPanel',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
                                            owner: me,
		                                    dockedItems: [
		                                	{
												xtype: 'toolbar',
												dock: 'bottom',
												id: 'notifDocumentsFooterPanel',
												height: 50,
												layout: {
													type: 'hbox',
													align: 'middle'
												},
												items:
		                                    	[
				                                    {
				                                        xtype: 'container',
				                                        flex: 1,
				                                        layout: {
				                                            type: 'hbox',
				                                            align: 'middle'
				                                        },
				                                        items: [
				                                            {
                                                                xtype: 'oxfileselectbutton',
                                                                margin: '0 7 0 7',
		                                                        height: 38,
		                                                        selectionCallback: this.getController().onFileForDocUploadSelected,
		                                                        selectionCallbackScope: this.getController()
				                                            },
															{
                                                                xtype: 'label',
                                                                text: Locale.getMsg('addFilesAndDocuments')
                                                            }
				                                        ]
													}
												]
											}],
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/document.png"  />';
		                                            },
		                                            id: 'notifFileIconColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifFileNameDescColumn',
		                                            dataIndex: 'fileDescription',
		                                            text: Locale.getMsg('file'),
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var description = record.get('fileDescription');

			                                            return description;
			                                    	},
			                                    	flex: 2
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifFileTypeSizeColumn',
		                                            text: Locale.getMsg('typeAndSize'),
		                                            dataIndex: 'fileType',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var type = record.get('fileType');
		                                        		var size = record.get('fileSize').toFixed(2) + ' kB';

			                                            return type +'<p>' + size + '</p>';
			                                    	},
			                                    	flex: 1,
			                                    	minWidth: 100
		                                        },
		                                       {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifFileOriginColumn',
		                                            text:  Locale.getMsg('fileStorageLocation'),
		                                            dataIndex: 'fileOrigin',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var stringToShow = "";
		                                        		var fileOrigin = record.get('fileOrigin');

		                                        		if(fileOrigin === "online") {
		                                        			stringToShow = Locale.getMsg('sapSystem');
		                                        		} else if(fileOrigin === "local") {
		                                        			stringToShow = Locale.getMsg('local');
		                                        		}

			                                            return stringToShow;
			                                    	},
		                                        	flex: 1,
		                                        	minWidth: 100,
		                                        	maxWidth: 200
		                                        }
		                                    ],*/
			                                listeners: {
				                        		cellclick: myController.onFileSelected,
				                        		cellcontextmenu: myController.onCellContextMenuFileList,
				                        		scope: myController
				                        	}
		                                }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'notifPartnerSection',
	                                margin: '40 5 0 5',
	                                layout: {
	                                    type: 'vbox',
	                                    align: 'stretch'
	                                },
	                                items: [
	                                    {
	                                        xtype: 'label',
	                                        cls: 'oxHeaderLabel',
	                                        text: Locale.getMsg('partners')
	                                    },
	                                    {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifPartnerGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
                                            owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/partner.png"/>';
		                                            },
		                                            id: 'notifPartnerImgColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            height: 30,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifPartnerRoleColumn',
			                                        maxWidth: 110,
			                                        minWidth: 110,
		                                            dataIndex: 'parvw',
		                                            text: Locale.getMsg('partnerFunction_Short')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifPartnerNameColumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('name'),
		                                            dataIndex: 'name1',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var nameLine1 = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('name1'), record.get('name2') ], null, true);
		                                        		var nameLine2 = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('name3'), record.get('name4') ], null, true);

		                                        		return nameLine1 + '<p>' + nameLine2 + '</p>';
			                                    	}
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('contact'),
		                                            dataIndex: 'telnumber',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	var tel = record.get('telnumber');
			                                        	var telExtens = record.get('telextens');
			                                        	var telMob = record.get('telnumbermob');

			                                        	if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tel)) {
			                                        		tel = 'Tel.: ' + tel;

			                                        		if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telExtens))
			                                        			tel += ' ' + telExtens;
			                                        	}

			                                        	if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telMob)) {
			                                        		telMob = 'Mobil: ' + telMob;
			                                        	}

			                                        	return tel + '<p>' + telMob + '</p>';
			                                    	}
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'notifPartnerAdressColumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('address'),
		                                            dataIndex: 'city1',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var street = '';
			                                            var postalCode = '';

			    										street = AssetManagement.customer.utils.StringUtils.concatenate([record.get('street'), record.get('housenum1') ], null, true);
			    										postalCode = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('postcode1'), record.get('city1') ], null, true);


			                                            return street + '<p>' + postalCode + '</p>';
		                                        	},
		                                        	enableColumnHide: false
		                                        },
		                                        {
		                                            xtype: 'actioncolumn',
		                                            id: 'notifPartnerMapsColumn',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('city1')) &&
					                            				AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('postcode1'))) {
					                            			metaData.style = 'display: none;';
					                            		} else {
					                            			metaData.style = '';
					                            		}
					                            	},
			                                        text: Locale.getMsg('maps'),
			                                        maxWidth: 80,
			                                        width: 80,
			                                        items: [{
			                                            icon: 'resources/icons/maps_small.png',
			                                            tooltip: 'Google Maps',
			                                            iconCls: 'oxGridLineActionButton',
			                                            handler: function(grid, rowIndex, colIndex) {
			                                                var partner = grid.getStore().getAt(rowIndex);
			                                                this.getController().navigateToPartnerAddress(partner);
			                                            },
			                                            scope: this
			                                        }],
			                                        enableColumnHide: false,
			                                        align: 'center'
		                                        }
		                                    ], */
		                                    listeners:{
			                                	cellcontextmenu: myController.onCellContextMenuPartner,
			                                	scope: myController
			                                }

		                                }
		                            ]
	                            },
		                        {
		                            xtype: 'component',
		                            height: 30
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];

            this.add(items);

            var leftGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._notifRow = leftGrid.addRow(Locale.getMsg('notification'), 'multilinelabel', true);
			this._notifTypeRow = leftGrid.addRow(Locale.getMsg('notifType'), 'label', true);
			this._funcLocRow = leftGrid.addRow(Locale.getMsg('checklist_FuncLoc'), 'link', true);
			this._funcLocRow.getTemplate().addListener('click', this.getController().navigateToFuncLoc, this);
			this._equipmentRow = leftGrid.addRow(Locale.getMsg('equipment'), 'link', true);
			this._equipmentRow.getTemplate().addListener('click', this.getController().navigateToEquipment, this);
			this._orderRow = leftGrid.addRow(Locale.getMsg('order'), 'link', false);
			this._orderRow.getTemplate().addListener('click', this.getController().navigateToOrder, this);
            this._codeRow = leftGrid.addRow(Locale.getMsg('codification'), 'multilinelabel', false);


			// The rightGrid will be shown if the page width is over 800.
			var rightGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._notifDateRowRight = rightGrid.addRow(Locale.getMsg('notifsCreationDate'), 'label', true);
			this._reporterRowRight = rightGrid.addRow(Locale.getMsg('reporter'), 'multilinelabel', true);
			this._disruptionFromRowRight = rightGrid.addRow(Locale.getMsg('disruptionFrom'), 'label', true);
			this._disruptionToRowRight = rightGrid.addRow(Locale.getMsg('disruptionTo'), 'label', false);

			// The bottonGrid will be hidden if the page width is under 800.
			var bottomGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._notifDateRowBottom = bottomGrid.addRow(Locale.getMsg('notifsCreationDate'), 'label', true);
			this._reporterRowBottom = bottomGrid.addRow(Locale.getMsg('reporter'), 'label', true);
			this._disruptionFromRowBottom = bottomGrid.addRow(Locale.getMsg('disruptionFrom'), 'label', true);
			this._disruptionToRowBottom = bottomGrid.addRow(Locale.getMsg('disruptionTo'), 'label', false);

	        this.queryById('notifLeftGrid').add(leftGrid);
	        this.queryById('notifRightGrid').add(rightGrid);
	        this.queryById('notifBottomGrid').add(bottomGrid);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNotifDetailPage', ex);
		}
   },

   getPageTitle: function() {
	   var retval = '';

	   try {
		   var title = Locale.getMsg('notification');

		   var notif = this.getViewModel().get('notif');

		   if(notif) {
			   title += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(notif.get('qmnum'));
		   }

		   retval = title;
	   } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNotifDetailPage', ex);
	   }

	   return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
		try {
			this.fillMainDataSection();
			this.manageSections();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNotifDetailPage', ex);
		}
    },

    fillMainDataSection: function() {
	   try {
		   var notif = this.getViewModel().get('notif');
		   var notifType = notif.get('notifType');
		   var funcLoc = notif.get('funcLoc');
		   var equi = notif.get('equipment');
		   var order = notif.get('order');
		   var notifCustCode = notif.get('notifCustCode');

		   /* LEFT GRID */
		   this._notifRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(notif.get('qmnum'), '0') + " " + notif.get('qmtxt'));

		   var notifTypeString = '';
		   if(notifType) {
			   notifTypeString = notifType.get('qmart') + " " + notifType.get('qmartx')
		   } else {
			   notifTypeString = notif.get('qmart');
		   }

		   this._notifTypeRow.setContentString(notifTypeString);

		   if(funcLoc !== null && funcLoc !== undefined) {
			   this._funcLocRow.show();
			   this._funcLocRow.setContentString(funcLoc.getDisplayIdentification() + " " + funcLoc.get('pltxt'));
		   } else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('tplnr'))) {
			   this._funcLocRow.show();
			   this._funcLocRow.setContentString(notif.get('tplnr'));
		   } else {
			   this._funcLocRow.hide();
			   this._funcLocRow.setContentString('');
		   }

		   if(equi !== null && equi !== undefined) {
			   this._equipmentRow.show();
			   this._equipmentRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + " " + equi.get('eqktx'));
		   } else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('equnr'))) {
			   this._equipmentRow.show();
			   this._equipmentRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(notif.get('equnr'), '0'));
		   } else {
			   this._equipmentRow.hide();
			   this._equipmentRow.setContentString('');
		   }

		   if(order !== null && order !== undefined) {
			   this._orderRow.show();
			   this._orderRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0') + " " + order.get('ktext'));
		   } else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('aufnr'))) {
			   this._orderRow.show();
			   this._orderRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(notif.get('aufnr'), '0'));
		   } else if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_create') === 'X') {
		       this._orderRow.show();
		       this._orderRow.setContentString(Locale.getMsg('createNewOrder'));
		   } else {
			   this._orderRow.hide();
			   this._orderRow.setContentString('');
		   }

		   /* RIGHT AND BOTTON GRID */

		   // Meldungsdatum
		   if(notif.get('qmdt')) {
			   this._notifDateRowRight.show();
			   this._notifDateRowRight.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(notif.get('qmdt')));

			   this._notifDateRowBottom.show();
			   this._notifDateRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(notif.get('qmdt')));
		   } else {
			   this._notifDateRowRight.hide();
			   this._notifDateRowBottom.hide();
		   }

		   // Meldender
		   if(notif.get('qmnam')) {
			   this._reporterRowRight.show();
			   this._reporterRowRight.setContentString(notif.get('qmnam'));

			   this._reporterRowBottom.show();
			   this._reporterRowBottom.setContentString(notif.get('qmnam'));
		   } else {
			   this._reporterRowRight.hide();
			   this._reporterRowBottom.hide();
		   }

		   // Störung von
		   if(notif.get('vondt')) {
			   this._disruptionFromRowRight.show();
			   this._disruptionFromRowRight.setContentString(AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(notif.get('vondt')));

			   this._disruptionFromRowBottom.show();
			   this._disruptionFromRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(notif.get('vondt')));
		   } else {
			   this._disruptionFromRowRight.hide();
			   this._disruptionFromRowBottom.hide();
		   }

		   // Störung bis
		   if(notif.get('bisdt')) {
			   this._disruptionToRowRight.show();
			   this._disruptionToRowRight.setContentString(AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(notif.get('bisdt')));

			   this._disruptionToRowBottom.show();
			   this._disruptionToRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(notif.get('bisdt')));
		   } else {
			   this._disruptionToRowRight.hide();
			   this._disruptionToRowBottom.hide();
		   }

           var codification = notif.get('codification');
           if(codification){
               this._codeRow.show();
               this._codeRow.setContentString(codification.get('kurztext'));
           } else {
               this._codeRow.hide();
           }

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseNotifDetailPage', ex);
		}
	},

	manageSections: function() {
		try {
			var notif = this.getViewModel().get('notif');

			//set stores
			var itemStore = notif.get('notifItems');
			var notifItemsGridPanel = Ext.getCmp('notifItemsGrid');
			notifItemsGridPanel.setStore(itemStore);

			var activityStore = notif.get('notifActivities');
			var activityGridPanel = Ext.getCmp('notifActivitiesGrid');
			activityGridPanel.setStore(activityStore);

			var taskStore = notif.get('notifTasks');
			var notifTasksGridPanel = Ext.getCmp('notifTasksGrid') ;
			notifTasksGridPanel.setStore(taskStore);

			var partnerStore = notif.get('partners');
			var partnerGridPanel = Ext.getCmp('notifPartnerGrid');
			partnerGridPanel.setStore(partnerStore);

			var fileStore = notif.get('documents');
			var fileGridPanel = Ext.getCmp('notifFileGridPanel');
			fileGridPanel.setStore(fileStore);

			var sdItemsStore = notif.getAllSDItems();
			var sdOrderItemsGridPanel = Ext.getCmp('notifDetailPageSDOrderItemsGridPanel');
			sdOrderItemsGridPanel.setStore(sdItemsStore);

			//manage visibility
			var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();

			//notif items section
			var showItems = funcPara.getValue1For('head_item_active') === 'X';
			Ext.getCmp('notifItemsGridSection').setHidden(!showItems);

			var showItemsAddOption = funcPara.getValue1For('head_item_create') === 'X';
			Ext.getCmp('notifItemsFooterPanel').setHidden(!showItemsAddOption);

			//notif activities section
			var showActivities = funcPara.getValue1For('head_activity_active') === 'X';
			Ext.getCmp('notifActivityGridSection').setHidden(!showActivities);

			var showActivitiesAddOption = funcPara.getValue1For('head_activity_create') === 'X';
			Ext.getCmp('notifHeadActivityFooterPanel').setHidden(!showActivitiesAddOption);

			//notif tasks section
			var showTasks = funcPara.getValue1For('head_task_active') === 'X';
			Ext.getCmp('notifHeadTaskGridSection').setHidden(!showTasks);

			var showTasksAddOption = funcPara.getValue1For('head_task_create') === 'X';
			Ext.getCmp('notifHeadTaskFooterPanel').setHidden(!showTasksAddOption);

		    //partner section
			var showPartners = funcPara.getValue1For('notif_partner_active') === 'X';
			Ext.getCmp('notifPartnerSection').setHidden(!showPartners);

		    //docs and files section
			var showDocsAndFiles = funcPara.getValue1For('notif_documents_active') === 'X';
			Ext.getCmp('notifFileSection').setHidden(!showDocsAndFiles);

		    //sd orders
		    //ext_scen is customer specific!!
			//var showSDOrders = funcPara.getValue1For('ext_scen_active') === 'X';

			//This is some implementation from Franke
			Ext.getCmp('notifDetailPageSDOrderItemsSection').hide();

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSections of BaseNotifDetailPage', ex);
		}
	}
});