Ext.define('AssetManagement.base.view.pages.BaseSDOrderListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.SDOrderListPageViewModel',
        'AssetManagement.customer.controller.pages.SDOrderListPageController',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date'
    ],

    inheritableStatics: {
		_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.SDOrderListPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSDOrderListPage', ex);
		    }
            
            return this._instance;
        }
    },
    
    controller: 'SDOrderListPageController',
    
    viewModel: {
        type: 'SDOrderListPageModel'
    },

    layout:{
        type: 'hbox',
	    align: 'stretch'
	},
    
	//protected
	//@override
    buildUserInterface: function() {
		try {
			var myController = this.getController();
			
			var items = [
			     {
			    	 xtype: 'container',
			         flex: 1,
			         frame: true,
			         layout: {
			            type: 'hbox',
			            align: 'stretch'
			         },
			         items: [
			            {
			                xtype: 'component',
			                maxWidth: 10,
	                        minWidth: 10
			            },
			            {
			                xtype: 'container',
			                flex: 5,
			                layout: {
			                    type: 'vbox',
			                    align: 'stretch'
			                },
			                items: [
			                    {
			                        xtype: 'component',
			                        height: 10
			                    },
			                    {
		                            xtype: 'container',
		                            height: 30,
			                        id: 'searchSDOrderContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                               
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldSDOrder',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,
			                                listeners: {
			                                	change: {
			                                        fn: 'onSearchFieldChange'
			                                    }
		                                	}
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
		                                    id: 'sdOrderListPageFilterCriteriaCombobox',
		                                    editable: false,
		                                    store: [Locale.getMsg('orderingNumber'), Locale.getMsg('orderNumber'), Locale.getMsg('notifNumber')],
		                                    value: Locale.getMsg('orderingNumber')
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
	                            {
			                        xtype: 'container',
			                        id: 'sdOrderGridContainer',
			                        flex: 1,
			                        layout: {
	                            		type: 'hbox',
	                            		align: 'stretch'
	                            	},
			                        items: [
			                           this.getNewSDOrderGridPanel()
			                        ]
			                    },
			                    {
		                            xtype: 'component',
		                            height: 10
		                        }
			                ]
			            },
			            {
			                xtype: 'component',
			                width: 10
			            }
			        ]
			    }
			];
			
			this.add(items);
			this.searchFieldVisibility();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSDOrderListPage', ex);
    	}
    },
    
    //private
    renewSDOrderGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('sdOrderGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewSDOrderGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewSDOrderGridPanel of BaseSDOrderListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewSDOrderGridPanel: function() {
    	var retval = null;
    	
    	try {
    		var myController = this.getController();
    	
    		retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
                id: 'sdOrderGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            loadingText: Locale.getMsg('loadingSDOrders'),
	            emptyText: Locale.getMsg('noSDOrders'),
	            flex: 1,
	            scrollable: 'vertical',
	            columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        return '<img src="resources/icons/demand_requirement.png"  />';
	                    },
	                    maxWidth: 70,
	                    minWidth: 70,
	                    enableColumnHide: false,
	                    align: 'center'		                                
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 2,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                    	var bstnk = record.get('bstnk');
	                     	var vbeln = AssetManagement.customer.utils.StringUtils.trimStart(record.get('vbeln'), '0');
	                        return bstnk +'<p>'+vbeln+'</p>';
	                    },
	                    enableColumnHide: false,
	                    dataIndex: 'bstnk',
	                    text: Locale.getMsg('orderingNumber')		                                
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    dataIndex: 'lfgsk',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		return record.getDeliveryStatus();
	                 	},
	                    enableColumnHide: false,
	                    text: Locale.getMsg('deliveryStatus')
	                },
	                {
	                    xtype: 'datecolumn',
	                    enableColumnHide: false,
	                    format: 'd.m.Y',
	                    dataIndex: 'bstdk',
	                    maxWidth: 120,
	                    minWidth: 120,
	                    text: Locale.getMsg('orderingDate')
	                },
	                {
	                    xtype: 'datecolumn',
	                    enableColumnHide: false,
	                    format: 'd.m.Y',
	                    dataIndex: 'vdatu',
	                    maxWidth: 140,
	                    minWidth: 140,
	                    text: Locale.getMsg('desiredDelivery')
	                }
	            ],
	            
	            listeners: {
	             	cellclick: myController.onSDOrderSelected,
	             	scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewSDOrderGridPanel of BaseSDOrderListPage', ex);
    	}
    	
    	return retval;
	},
    
	//protected
	//@override
    getPageTitle: function() {
    	var retval = "";
    
    	try {
	    	retval = Locale.getMsg('materialOrders');
	    	
	    	var sdOrders = this.getViewModel().get('sdOrderStore');
	    	
	    	if(sdOrders) {
	    		retval += " (" + sdOrders.getCount() + ")";
	    	}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseSDOrderListPage', ex);
    	}

    	return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
    	try {
    		var sdOrderStore = this.getViewModel().get('sdOrderStore');
    		
    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    			this.renewSDOrderGridPanel();
    		
    		var sdOrderGridPanel = Ext.getCmp('sdOrderGridPanel');
    		
    		sdOrderGridPanel.setStore(sdOrderStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseSDOrderListPage', ex);
    	}
	},
	
	//public
	//@override
	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('sdOrderGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseSDOrderListPage', ex);
    	}
	},
	
	//private
	searchFieldVisibility: function() { 
       try {
	       var field = Ext.getCmp('searchSDOrderContainer');	
	       var para = true;
	      
	       if (para === false)
	    	   field.setVisible(false);
       } catch(ex) {
    	  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseSDOrderListPage', ex);
       }
	},
	
	//public
	getSearchValue: function() {
		try {
			var orderToSearch = Ext.create('AssetManagement.customer.model.bo.SDOrder', {});
			var currentFilterCriterion = Ext.getCmp('sdOrderListPageFilterCriteriaCombobox').getValue();	

			if(currentFilterCriterion == Locale.getMsg('orderingNumber'))
				orderToSearch.set('bstnk', Ext.getCmp('searchFieldSDOrder').getValue());
			else if(currentFilterCriterion == Locale.getMsg('orderNumber'))
				orderToSearch.set('aufnr', Ext.getCmp('searchFieldSDOrder').getValue());
			else if(currentFilterCriterion == Locale.getMsg('notifNumber'))
				orderToSearch.set('qmnum', Ext.getCmp('searchFieldSDOrder').getValue());
				
			this.getViewModel().set('searchSDOrder', orderToSearch);
		
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseSDOrderListPage', ex);
		}
	}
});