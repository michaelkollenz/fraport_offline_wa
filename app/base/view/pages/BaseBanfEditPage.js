Ext.define('AssetManagement.base.view.pages.BaseBanfEditPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.BanfEditPageViewModel',
        'AssetManagement.customer.controller.pages.BanfEditPageController',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.Date',
        'Ext.button.Button',
        'Ext.form.field.ComboBox',
        'AssetManagement.customer.view.OxGridPanel',
        'Ext.grid.column.Date',
        'Ext.grid.View',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.OxDynamicGridPanel'
    ],

    inheritableStatics: {
    	_instance: null,
    	 
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.BanfEditPage');
                }           
            } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseBanfEditPage', ex);
		    }
            
            return this._instance;
        }
    },


    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.BanfEditPageRendererMixin'
    },
    
    viewModel: {
        type: 'BanfEditPageModel'
    },
    
    controller:'BanfEditPageController',
      
    buildUserInterface: function() {
        try {
            var me = this;
            var myController = this.getController();
		    var items = [
		        {
		            xtype: 'container',           
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                //left spacer container
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                //main vertical container
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
                                    xtype: 'label',
                                    cls: 'oxHeaderLabel',
                                    id: 'banfEditPageModeLabel',
                                    text: Locale.getMsg('createBanf')
                                },
                                {
                                    xtype: 'component',
                                    height: 20
                                },
		                        //upper container with form fields
		                        {
		                            xtype: 'container',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    flex: 100,
		                                    maxWidth: 800,
		                                    layout: {
		                                        type: 'vbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'datefield',
		                                            id: 'banfEditDeliveryDateField',
		                                            fieldLabel: Locale.getMsg('deliveryDate'),
		                                            format: 'd.m.Y',
		                                            maxLength: 10,
		                                            labelWidth: 120
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 5
		                                        },
		                                        /*{
		                                            xtype: 'oxtextfield',
		                                            id: 'banfEditOrderNumberTextField',
			                                        maxLength: 10, 
			        	                            enforceMaxLength: true,
		                                            fieldLabel: Locale.getMsg('purchOrderNo'),
		                                            labelWidth: 120
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 5
		                                        },*/
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'hbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                	xtype: 'oxtextfield',
			                                                flex: 0.2,
			                                                id: 'banfEditMaterialTextField',
			                                                fieldLabel: Locale.getMsg('material'),
			                                                labelWidth: 120,
			                                                maxLength: 18
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    width: 10
		                                                },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 0.3,
		                                                    id: 'banfEditMaterialDescriptionTf',
		                                                    labelStyle: 'display: none;',
		                                                    disabled: true,
		                                                    disabledCls: 'oxDisabledTextField'
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                   heigth: 10
		                                                }
		                                            ]
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 10
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'hbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                    xtype: 'oxnumberfield',
		                                                    id: 'banfEditAmountTextField',
		                                                    flex: 1.5,
		                                                    fieldLabel: Locale.getMsg('quantityUnit'),
		                                                    labelWidth: 120,
		                                                    maxLength: 13
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    width: 10
		                                                },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    id: 'banfEditUnitTextField',
		                                                    flex: 0.3,
		                                                    labelStyle: 'display: none;',
		                                                    maxLength: 3
		                                                }
		                                            ]
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 10
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'hbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 0.2,
		                                                    id: 'banfEditFactoryTextField',
		                                                    fieldLabel: Locale.getMsg('plantStorLoc'),
		                                                    labelWidth: 120,
		                                                    minWidth: 200,
		                                                    maxLength: 4,
		                                                    disabled: true,
		                                                    enableKeyEvents: true,
		                                                    listeners: {
		                                            			keyup: 'onPlantTextChanged'
		                                            		}
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    width: 10
		                                                },
		                                                {
		                                                    xtype: 'oxcombobox',
		                                                    flex: 0.3,
		                                                    id: 'banfEditStorLocComboField',
		                                                    labelStyle: 'display: none;',
		                                                    displayField: 'text',
			                                                queryMode: 'local',
					                                        valueField: 'value',
		                                                    editable: false
		                                                }
		                                            ]
		                                        }
		                                    ]
		                                },
                                        {
                                            xtype: 'component',
                                            flex: 3,
                                            mindWidth: 10,
                                            maxWidth: 40
                                        },
		                                {
		                                    xtype: 'container',
	                                        layout: {
	                                            type: 'vbox',
	                                            align: 'left',
	                                            pack: 'center'
	                                        },
		                                    items: [
		                                        {
		                                            xtype: 'container',
		                                            layout: 'vbox',
		                                            items: [
		                                                {
		                                                    xtype: 'button',
		                                                    html: '<img width="120%" src="resources/icons/search.png"></img>',
		                                                    width: 45,
		                                                    height: 38,
		                                                    id: 'searchButtonBanf',
		                                                    textAlign: 'left',
		                                                    padding: '1 5 3 1',
			                                                listeners: {
			                                            		click: 'onMaterialSearchButtonClick'
			                                            	}
		                                                }
		                                            ]
		                                        }                                 
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 30
		                        },
		                        //containers address data
		                        {
		                            xtype: 'label',
		                            flex: 1,
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('deliveryAddress')
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 20
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'banfEditAdressContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    layout: {
		                                        type: 'vbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'oxtextfield',
		                                            id: 'banfEditTitleTextField',
		                                            fieldLabel: Locale.getMsg('termOfAddress'),
		                                            maxLength: 20,
		                                            labelWidth: 120,
		                                            hidden: true
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 5,
		                                            hidden: true
		                                        },
		                                        {
		                                            xtype: 'oxtextfield',
		                                            id: 'banfEditNameTextField',
		                                            fieldLabel: Locale.getMsg('name'),
		                                            maxLength: 40,
		                                            labelWidth: 120
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 5
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'hbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                    xtype: 'component',
		                                                    width: 125
		                                                },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 1,
		                                                    id: 'banfEditName2TextField',
		                                                    maxLength: 40,
		                                                    labelStyle: 'display: none;'
		                                                }
		                                            ]
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 10
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'hbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 1.5,
		                                                    id: 'banfEditStreetTextField',
		                                                    fieldLabel: Locale.getMsg('streetHouseNr'),
		                                                    maxLength: 60,
		                                                    labelWidth: 120
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    width: 10
		                                                },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 0.2,
		                                                    id: 'banfEditHouseNumTextField',
		                                                    labelStyle: 'display: none;',
		                                                    maxLength: 10
		                                                }
		                                            ]
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 10
		                                        }
		                                    ]
		                                },
		                                {
		                                    xtype: 'component',
		                                    width: 40
		                                },
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    layout: {
		                                        type: 'vbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'oxtextfield',
		                                            id: 'banfEditPlzTextField',
		                                            fieldLabel: Locale.getMsg('zipCode'),
		                                            maxLength: 10
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 5
		                                        },
		                                        {
		                                            xtype: 'oxtextfield',
		                                            id: 'banfEditCityTextField',
		                                            fieldLabel: Locale.getMsg('city'),
		                                            maxLength: 40
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 5
		                                        },
		                                        {
		                                            xtype: 'oxtextfield',
		                                            id: 'banfEditLandTextField',
		                                            fieldLabel: Locale.getMsg('countryHintCode'),
		                                            maxLength: 3,
		                                            maxWidth: 150
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 30
		                                        }
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 40
		                        },
		                        //container header label
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'component',
		                                    minWidth: 5,
		                                    maxWidth: 5
		                                },
		                                {
		                                    xtype: 'label',
		                                    flex: 1,
		                                    cls: 'oxHeaderLabel',
		                                    text: Locale.getMsg('alreadyActivDemandRequirements')
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                        	//gridpanel with banf items - cellcontextmenu: edit, delete
		                        	xtype: 'oxdynamicgridpanel',                        
			                        id: 'banfGridPanel',
			                        disableSelection: true,
			                        parentController: myController,
			                        owner: me,
			                        listeners: {
			                            cellcontextmenu: 'onCellContextMenu'
			                        }
		                           /* columns: [
		                                {
		                                	xtype: 'actioncolumn',
			                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    return '<img src="resources/icons/demand_requirement.png"  />';
			                                },
			                                minWidth:  80,
			                                maxtWidth: 80,
			                                enableColumnHide: false,
			                                align: 'center'
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    text: Locale.getMsg('banfNumer'),
		                                    dataIndex: 'banfn',
		                                    flex: 1,
			                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    return record.get('banfn');
			                                },
			                                enableColumnHide: false
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    text: Locale.getMsg('material'),
		                                    dataIndex: 'matnr',
		                                    flex: 1,
			                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                	                		var material = record.get('material');
	                	                		var maktx = material ? material.get('maktx') : '';
	                	                    
	                	                		return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
			                                },
			                                enableColumnHide: false
		                                },
		                                {
		                                    xtype: 'datecolumn',
		                                    enableColumnHide: false,
			                                format: 'd.m.Y',
			                                dataIndex: 'lfdat',
			                                maxWidth: 125,
		                                    minWidth: 125,
		                                    text: Locale.getMsg('deliveryDate')
		
		                                },
		                                {
		                                    xtype: 'datecolumn',
		                                    enableColumnHide: false,
			                                format: 'd.m.Y',
			                                dataIndex: 'bedat',
			                                maxWidth: 125,
	                                        minWidth: 125,
		                                    text: Locale.getMsg('confirmedDate')
		
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    text: Locale.getMsg('orderComponents_Qty'),
		                                    dataIndex: 'menge',
			                                maxWidth: 160,
	                                        minWidth: 160,
			                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge')); 
		                                		var unit = record.get('meins'); 
		                                		
		                                		return quantity + ' ' + unit;
			                                },
		                                    enableColumnHide: false
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    text: Locale.getMsg('confirmed_Qty'),
		                                    dataIndex: 'bsmng',
			                                maxWidth: 160,
		                                    minWidth: 160,
			                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bsmng')); 
		                                		return quantity;
			                                },
		                                    enableColumnHide: false
		                                }
		                            ],
		                            listeners: {
		                        		cellcontextmenu: 'onCellContextMenu'
		                        	}*/
		                        
		                        }
		                    ]
		                },
		                //right spacer container
		                {
		                    xtype: 'component',
		                    width: 30
		                }
		            ]
		        }
		    ];
		    
		    this.add(items);
         } catch(ex) {
        	 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseBanfEditPage', ex);
         }    
    },

    getPageTitle: function() {
    	var retval = '';
    
    	try {
    		retval = Locale.getMsg('newPreq');
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseBanfEditPage', ex);
    	}
    	
    	return retval;
	},
	
	//protected
	//@override
    updatePageContent: function() {
    	try {
			this.transferModelStateIntoView(); 
			
			this.refreshBanfPanel(); 
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseBanfEditPage', ex);
    	}
	},
	
	refreshBanfPanel: function() {
		try {
			var banfGridPanel = Ext.getCmp('banfGridPanel'); 
			banfGridPanel.setStore(this.getViewModel().get('banfStore'));
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshBanfPanel of BaseBanfEditPage', ex);
		}
	},

	transferModelStateIntoView: function() {
		try {
			var myModel = this.getViewModel();
		
			var banf = myModel.get('banf');
			var editMode = myModel.get('isEditMode');

			Ext.getCmp('banfEditPageModeLabel').setText(editMode === true ? Locale.getMsg('editBanf') : Locale.getMsg('createBanf'));

            //general data
			Ext.getCmp('banfEditDeliveryDateField').setValue(banf.get('lfdat'));
			Ext.getCmp('banfEditAmountTextField').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(banf.get('menge')));
			Ext.getCmp('banfEditUnitTextField').setValue(banf.get('meins'));
			Ext.getCmp('banfEditFactoryTextField').setValue(banf.get('werks'));
			Ext.getCmp('banfEditStorLocComboField').setValue(banf.get('lgort'));
			Ext.getCmp('banfEditMaterialTextField').setValue(banf.get('matnr'));
			
			//material related data
			var banfsMaterial = banf.get('material');
			Ext.getCmp('banfEditMaterialDescriptionTf').setValue(banfsMaterial ? banfsMaterial.get('maktx') : '');
			
			//address data
			Ext.getCmp('banfEditTitleTextField').setValue(banf.get('formofaddr'));
			Ext.getCmp('banfEditNameTextField').setValue(banf.get('name1'));
			Ext.getCmp('banfEditName2TextField').setValue(banf.get('name2'));
			Ext.getCmp('banfEditStreetTextField').setValue(banf.get('street'));
			Ext.getCmp('banfEditHouseNumTextField').setValue(banf.get('houseNo'));
			Ext.getCmp('banfEditPlzTextField').setValue(banf.get('postlCod1'));
			Ext.getCmp('banfEditCityTextField').setValue(banf.get('city'));
			Ext.getCmp('banfEditLandTextField').setValue(banf.get('country'));
			
			//storage location
			this.fillStorageLocationComboBox(banf.get('werks'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseBanfEditPage', ex);
		}
	},
	
	fillStorageLocationComboBox: function(plant) {
		try {
			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			var myModel = this.getViewModel();

			var defaultStorLoc = null; 
			var banfsStorLoc = null;
			var banfsLgort = myModel.get('banf').get('lgort');
			var storLocs = myModel.get('storLocs');
			
			if(storLocs && storLocs.getCount() > 0) {
				storLocs.each(function(storLoc) {
					if(storLoc.get('werks') === plant) {
						store.add({ text: storLoc.get('lgort') + " - " + storLoc.get('lgobe'), value: storLoc });
						
						if(storLoc.get('default_lgort') === 'X')
							defaultStorLoc = storLoc;
							
						if(storLoc.get('lgort') === banfsLgort)
							banfsStorLoc = storLoc;
					}
				}, this);
				
				if(!defaultStorLoc)
					defaultStorLoc = storLocs.getAt(0);
			}
			
			var comboBox = Ext.getCmp('banfEditStorLocComboField');
			comboBox.clearValue();
			comboBox.setStore(store);
			comboBox.setStore(store);
			
			var storLocToSet = banfsStorLoc;

			if(!storLocToSet)
				storLocToSet = defaultStorLoc;
				
			if(storLocToSet) {
				var storLocRecord = storLocToSet ? comboBox.findRecordByValue(storLocToSet) : null;	
				
				if(storLocRecord)
					comboBox.setValue(storLocRecord);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillStorageLocationComboBox of BaseBanfEditPage', ex);
		}
	},
	
	//transfer current values from screen into model
	transferViewStateIntoModel: function() {
	    try {
			var myModel = this.getViewModel();
			var banf = myModel.get('banf');
			
			//set stor loc information
			banf.set('lgort', Ext.getCmp('banfEditStorLocComboField').getValue().get('lgort'));
			
			//set general data
            banf.set('lfdat', Ext.getCmp('banfEditDeliveryDateField').getValue()); 
            banf.set('badat', new Date()); 
            banf.set('menge', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp('banfEditAmountTextField').getValue()));
            banf.set('meins', Ext.getCmp('banfEditUnitTextField').getValue());
            banf.set('werks', Ext.getCmp('banfEditFactoryTextField').getValue());
            banf.set('matnr', Ext.getCmp('banfEditMaterialTextField').getValue());
            //banf.set('ebeln', Ext.getCmp('banfEditOrderNumberTextField').getValue());
            
            //set address data
            banf.set('formofaddr', Ext.getCmp('banfEditTitleTextField').getValue());
            banf.set('name1', Ext.getCmp('banfEditNameTextField').getValue()); 
            banf.set('name2', Ext.getCmp('banfEditName2TextField').getValue());
            banf.set('street', Ext.getCmp('banfEditStreetTextField').getValue());
            banf.set('houseNo', Ext.getCmp('banfEditHouseNumTextField').getValue());
            banf.set('postlCod1', Ext.getCmp('banfEditPlzTextField').getValue());
            banf.set('city', Ext.getCmp('banfEditCityTextField').getValue());
            banf.set('country', Ext.getCmp('banfEditLandTextField').getValue());
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseBanfEditPage', ex);
		}
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			var storLoc = Ext.getCmp('banfEditStorLocComboField').getValue();
			var lgort = '';

			if(storLoc)
				lgort = storLoc.get('lgort');
		
			retval = {
			    //general data
				lfdat: Ext.getCmp('banfEditDeliveryDateField').getValue(),
				menge: Ext.getCmp('banfEditAmountTextField').getValue(),
			    meins: Ext.getCmp('banfEditUnitTextField').getValue(),
			    matnr: Ext.getCmp('banfEditMaterialTextField').getValue(),
				werks: Ext.getCmp('banfEditFactoryTextField').getValue(),
			    lgort: lgort,
				
			    //address data
			    formofaddr: Ext.getCmp('banfEditTitleTextField').getValue(),
			    name: Ext.getCmp('banfEditNameTextField').getValue(),
			    name2: Ext.getCmp('banfEditName2TextField').getValue(),
			    street: Ext.getCmp('banfEditStreetTextField').getValue(),
			    houseNo: Ext.getCmp('banfEditHouseNumTextField').getValue(),
			    postalCode: Ext.getCmp('banfEditPlzTextField').getValue(),
			    city: Ext.getCmp('banfEditCityTextField').getValue(),
			    country: Ext.getCmp('banfEditLandTextField').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseBanfEditPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	fillMaterialSpecFields: function(materialInfoObject) {
		try {
		    if (materialInfoObject) {
		        var materialMaster = materialInfoObject.get('material') ? materialInfoObject.get('material') : materialInfoObject;
		        var matnr = materialInfoObject.get('matnr');
		        var maktx = materialMaster ? materialMaster.get('maktx') : '';
		        var unit = materialInfoObject.get('meins');
		        var werks = materialInfoObject.get('werks');
		        var lgort = materialInfoObject.get('lgort');		        
	
		        Ext.getCmp('banfEditMaterialTextField').setValue(matnr);
		        Ext.getCmp('banfEditMaterialDescriptionTf').setValue(maktx);
		        Ext.getCmp('banfEditAmountTextField').setValue(1);
		        Ext.getCmp('banfEditUnitTextField').setValue(unit);

		        if (werks) {
		            Ext.getCmp('banfEditFactoryTextField').setValue(materialInfoObject.get('werks'));
		        }
				
                this.trySetStorageLocationComboBoxByKeyField(werks, lgort);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFields of BaseBanfEditPage', ex);
		}
	},
	
	//will try to set stor loc identified by the key values - if not found a message will be triggered and the value will be reset
	//passing incomplete values will just clear the current value
	trySetStorageLocationComboBoxByKeyField: function(werks, lgort) {
		try {
			var storLocComboBox = Ext.getCmp('banfEditStorLocComboField');
		
			storLocComboBox.clearValue();

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
				var targetValue = null;

				var targetStorLoc = null;
				var storLocs = this.getViewModel().get('storLocs');
				
				if(storLocs && storLocs.getCount() > 0) {
					storLocs.each(function(storLoc) {
						if(storLoc.get('werks') === werks && storLoc.get('lgort') === lgort) {
							targetStorLoc = storLoc;
							return true;
						}
					}, this);
				}
				
				var targetValue = targetStorLoc ? storLocComboBox.findRecordByValue(targetStorLoc) : null;	
				
				if(targetValue) {
					//value found, set it
					storLocComboBox.setValue(targetValue);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('storageLocationNotFound'), true, 1);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySetStorageLocationComboBoxByKeyField of BaseBanfEditPage', ex);
		}
	}
});