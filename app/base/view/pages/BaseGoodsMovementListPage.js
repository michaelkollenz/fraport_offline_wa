Ext.define('AssetManagement.base.view.pages.BaseGoodsMovementListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',
    alias: 'widget.BaseGoodsMovementListPage',

    requires: [
      'AssetManagement.customer.controller.pages.GoodsMovementListPageController',
      'AssetManagement.customer.model.pagemodel.GoodsMovementListPageViewModel'
    ],

	inheritableStatics: {
		_instance: null,

	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	    	       this._instance = Ext.create('AssetManagement.customer.view.pages.GoodsMovementListPage');
	            }
	        } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseGoodsMovementListPage', ex);
		    }

	        return this._instance;
	    }
	},

    config: {
        flex: 1,
        bodyCls: ''
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.GoodsMovementListPageRendererMixin'
    },

    viewModel: {
        type: 'GoodsMovementListPageModel'
    },

    controller: 'GoodsMovementListPageController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
        try {
            var items = [
                {
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'container',
                                    id: 'goodsMovementListGridContainer',
                                    flex: 1,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    margin: '20 0 0 0',
                                    items: [
                                        this.getNewGoodsMovementListGridPanel()
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'component',
                    height: 10
                },
                {
                    xtype: 'button',
                    height: 52,
                    width: 54,
                    html: '<div><img  src="resources/icons/btsync.png"></img></div>',
                    id: 'submitGM',
                    listeners: {
                        click: 'onSubmitClick'
                    }
                }
            ];

            this.add(items);

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseGoodsMovementListPage', ex);
		}
  	},

	getPageTitle: function() {
		var retval = '';
		try {
			retval = Locale.getMsg('goodsMovements');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseGoodsMovementListPage', ex);
		}

		return retval;
	},

    //private
    getNewGoodsMovementListGridPanel: function() {
        var retval = null;

        try {
            var myController = this.getController();
            var me = this;
            var lgortStore = this.getViewModel().get('lgortStore');

            var rendererMixins = this.mixins.rendererMixin;

            retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
                id: 'goodsMovementListGridPanel',
                flex: 1,
                useLoadingIndicator: true,
                hideContextMenuColumn: true,
                scrollable: 'vertical',
                parentController: myController,
                owner: me,
                columns: [
                            {
                                xtype: 'gridcolumn',
                                text: Locale.getMsg('position'),
                                dataIndex: 'ebelp',
                                flex: 4,
                                renderer: rendererMixins.goodsMovementPositionColumnRenderer
                            },
                            {
                                xtype: 'gridcolumn',
                                text: Locale.getMsg('material'),
                                dataIndex: 'matnr',
                                flex: 4,
                                renderer: rendererMixins.goodsMovementMaterialColumnRenderer
                            },
                            {
                                xtype: 'widgetcolumn',
                                id: 'storageLocColumn',
                                width: 90,
                                hidden: true,
                                text: Locale.getMsg('storLoc'),
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },

                                widget: {
                                    xtype: 'oxcombobox',
                                    padding: '15 0 0 0',
                                    editable: false,
                                    displayField: 'lgort',
                                    valueField: 'lgort',
                                    queryMode: 'local',
                                    height: 30,
                                    owner: me

                                },
                                onWidgetAttach: rendererMixins.storeLocationOnWidgetAttached
                            },
                            {
                              xtype: 'gridcolumn',
                              text: Locale.getMsg('confirmed_Qty'),
                              dataIndex: 'menge',
                              // width: 40,
                              flex: 1,
                              renderer: rendererMixins.weMengeDelColumnRenderer
                            },
                            {
                              xtype: 'checkcolumn',
                              width: 60,
                              tdCls: 'oxCheckBoxGridColumnItem editButton',
                              text: Locale.getMsg('edit'),
                              listeners: {
                                checkchange: 'onEditClicked',
                                scope: myController
                              }
                            },
                            {
                                xtype: 'checkcolumn',
                                width: 60,
                                dataIndex: 'elikz',
                                tdCls: 'oxCheckBoxGridColumnItem deleteButton',
                                text: Locale.getMsg('delete'),
                                listeners: {
                                    checkchange: 'onDeleteClicked',
                                    scope: myController
                                }
                            }

                ]
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewGoodsMovementListGridPanel of BaseGoodsMovementListPage', ex);
        }

        return retval;
    },

    updatePageContent: function() {
        try {
            var myModel = this.getViewModel();
            var goodsMovementsStore = myModel.get('goodsMovementsStore');
            var goodsMovementListGridPanel = Ext.getCmp('goodsMovementListGridPanel');
            var storageLocColumn = Ext.getCmp('storageLocColumn');
            storageLocColumn.setHidden(!myModel.get('showStorageLocation'));
            goodsMovementListGridPanel.setStore(goodsMovementsStore);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseGoodsMovementListPage', ex);
        }
    }
});