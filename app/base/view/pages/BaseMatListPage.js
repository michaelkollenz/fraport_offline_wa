Ext.define('AssetManagement.base.view.pages.BaseMatListPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',
  alias: 'widget.BaseMatListPage',

  requires: [
    'AssetManagement.customer.model.pagemodel.MatListPageViewModel',
    'AssetManagement.customer.controller.pages.MatListPageController'

  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.MatListPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMatListPage', ex);
      }

      return this._instance;
    }
  },

  config: {
    flex: 1,
    bodyCls: ''
  },

  mixins: {
    rendererMixin: 'AssetManagement.customer.view.mixins.MatListPageRendererMixin'
  },

  viewModel: {
    type: 'MatListPageViewModel'
  },

  controller: 'MatListPageController',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {

      var items = [
        {
          xtype: 'container',
          layout: {
            type: 'hbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'container',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  flex: 1,
                  layout: {
                    type: 'vbox',
                    align: 'stretch'
                  },
                  items: [
                    {
                      xtype: 'component',
                      height: 10
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'matListMatNumber',
                      fieldLabel: Locale.getMsg('material'),
                      // maxLength: 18,
                      height: 40,
                      padding: '10 10 10 10',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onMatNumberTextKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'button',
                      height: 52,
                      width: 54,
                      html: '<div><img  src="resources/icons/search.png"></img></div>',
                      id: 'searchMaterial',
                      listeners: {
                        click: 'onSearchMaterialClick'
                      }
                    }
                  ]
                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  id: 'matListGridContainer',
                  flex: 1,
                  minHeight: 200,
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  },
                  margin: '20 0 0 0',
                  items: [
                    this.getNewMatListGridPanel()
                  ]
                }
              ]
            }
          ]
        }
      ];

      this.add(items);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMatListPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';
    try {
      retval = Locale.getMsg('Stock');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMatListPage', ex);
    }

    return retval;
  },


  updatePageContent: function () {
    try {
      var materialStore = this.getViewModel().get('materialStore');
      var matListGridPanel = Ext.getCmp('matListGridPanel');
      matListGridPanel.setStore(materialStore);
      if (!(materialStore && materialStore.getCount() > 0)) {
        Ext.getCmp('matListMatNumber').focus();
        Ext.getCmp('matListMatNumber').setValue('');
      }
      else {
        matListGridPanel.focus();
        Ext.getCmp('matListMatNumber').setValue('');
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseMatListPage', ex);
    }
  },

  //private
  getNewMatListGridPanel: function () {
    var retval = null;

    try {
      var myController = this.getController();
      var me = this;
      var rendererMixins = this.mixins.rendererMixin;

      retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
        id: 'matListGridPanel',
        flex: 1,
        useLoadingIndicator: true,
        hideContextMenuColumn: true,
        emptyText: Locale.getMsg('noItems'),
        scrollable: 'vertical',
        parentController: myController,
        owner: me,
        columns: [
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('material'),
            dataIndex: 'matnr',
            flex: 2,
            renderer: rendererMixins.weMaterialColumnRenderer
          },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('storLoc'),
            dataIndex: 'menge',
            flex: 2,
            renderer: rendererMixins.weStorLocColumnRenderer
          },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('storPla'),
            dataIndex: 'menge',
            flex: 2,
            renderer: rendererMixins.weStorPlaColumnRenderer
          },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('quantSAP'),
            dataIndex: 'menge',
            flex: 2,
            renderer: rendererMixins.weMengeColumnRenderer
          },
          {
            xtype: 'widgetcolumn',
            text: Locale.getMsg('quantIs'),
            flex: 2,
            // width: 150,
            layout: {
              type: 'vbox',
              align: 'stretch'
            },

            widget: {
              xtype: 'oxnumberfield',
              fieldLabel: '',
              padding: '10 0 0 0',
              // width: 200,
              flex: 2,
              hideTrigger: true, //hides arrows
              keyNavEnabled: false, //can't imput values with arrow keys
              mouseWheelEnabled: false, //nor the mouseweel
              allowDecimals: false,
              allowNegativeValues: false,
              validateOnChange: true,
              dataIndex: 'menge_ist',
              listeners: {
                change: 'onActualAmmountChange',
                //focusleave: 'onMeanValueFieldLooseFocus',
                scope: myController
              }
            },
            onWidgetAttach: rendererMixins.weMengeWidgetAttached
          }
        ]
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewMatListGridPanel of BaseMatListPage', ex);
    }

    return retval;
  },

  setSearchFieldValues: function (materialInput) {
    try {
      var myModel = this.getViewModel();
      Ext.getCmp('matListMatNumber').setValue(materialInput);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSearchFieldValues of BaseMatListPage', ex);
    }
  },

  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();
      var materialInput = Ext.getCmp('matListMatNumber').getValue();
      var myController = this.getController();
      myController._argumentsObject['materialInput'] = materialInput;
      myModel.set('materialInput', materialInput);


    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseMatListPage', ex);
    }
  },

  transferModelToViewState: function () {
    try {
      var myModel = this.getViewModel();
      var materialInput = myModel.get('materialInput');
      this.setSearchFieldValues(materialInput);

      var materialStore = myModel.get('materialStore');

      if (!materialStore || materialStore.getCount() === 0) {
        materialStore = Ext.create('Ext.data.Store', {
          model: 'AssetManagement.customer.model.bo.MatQuant'
        });

        myModel.set('materialStore', materialStore);
        if (materialInput) {
          this.onSearchMaterialClick();
        } else {
          this.refreshView(); //refresh the page, because it was just recently filled with data
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelToViewState of BaseMatListPage', ex);
    }
  }
});