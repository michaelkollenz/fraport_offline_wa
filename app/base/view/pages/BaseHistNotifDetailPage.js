Ext.define('AssetManagement.base.view.pages.BaseHistNotifDetailPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.HistNotifDetailPageViewModel',
        'AssetManagement.customer.controller.pages.HistNotifDetailPageController',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.utils.DateTimeUtils'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance == null)
                {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.HistNotifDetailPage');
                }           
            } catch (ex) {
		    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseHistNotifDetailPage', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'HistNotifDetailPageModel'
    },
    
    controller: 'HistNotifDetailPageController',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   
    initialize: function() {    
       try { 
    	   
        var items = [
                         
        {
            xtype: 'component',
            maxWidth: 10,
            minWidth: 10
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    height: 20
                },
                {
                    xtype: 'container',
                    id: 'upper',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                                xtype: 'container',
                                flex: 1,
                                layout: {
                                type: 'hbox',
                                align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'component',
                                minWidth: 6,
                                maxWidth: 6
                            },
                            {
                                xtype: 'label',
                                flex: 1,
                                cls: 'oxHeaderLabel',
                                text: Locale.getMsg('generalData')
                            }
                        ]
                    },
                        {
                            xtype: 'component',
                            height: 20
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'                               
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    id: 'leftHistNotifData'
                                },                        
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    padding: '0 0 0 20',
                                    id: 'rightHistNotifData',
                                    plugins: 'responsive',
                                    responsiveConfig: {
                                	'width <= 800': {
                                		visible: false
                                	},
                                	'width > 800': {
                                		visible: true
                                	}
                            	}
                                }
                            ]
                        }
                    ]
                },
                {
                	xtype: 'container',
                    margin: '40 0 0 0',
                    id: 'histNotifBottomGrid',                                                      
                    plugins: 'responsive',
                    responsiveConfig: {
                    	'width <= 800': {
                    		visible: true
                        },
                       'width > 800': {
                        	visible: false
                        }
                   	}
                }
            ]
        },
        {
            xtype: 'component',
            maxWidth: 10,
            minWidth: 10
        }
    ];
    
          this.add(items);
    
          var leftGrid= Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			     this._notifRow = leftGrid.addRow(Locale.getMsg('notif'), 'label', true);			
			     this._shorttextRow = leftGrid.addRow(Locale.getMsg('shorttext'), 'label', true);
			     this._funcLocRow = leftGrid.addRow(Locale.getMsg('funcLoc'), 'label', true);
			     this._userRow = leftGrid.addRow(Locale.getMsg('userStatus'), 'label', true);
			     this._sysRow = leftGrid.addRow(Locale.getMsg('systemStatus'), 'label', true);
			     this._orderRow = leftGrid.addRow(Locale.getMsg('order'), 'label', true);
			     this._equiRow = leftGrid.addRow(Locale.getMsg('equipment'), 'label', false);
			
	     // The rightGrid will be shown if the page width is over 800.
	     var rightGrid= Ext.create('AssetManagement.customer.view.utils.OxGrid',{});	  
	             this._strBeginRow = rightGrid.addRow(Locale.getMsg('dateDisrubtionStart'), 'label', true);
	             this._strEndRow = rightGrid.addRow(Locale.getMsg('timeDisrubtionEnd'), 'label', true);
	             this._objGrRow = rightGrid.addRow(Locale.getMsg('objectGroupCode'), 'label', true);
	             this._posRow = rightGrid.addRow(Locale.getMsg('position'), 'label', true);
	             this._posGrRow = rightGrid.addRow(Locale.getMsg('positionGroupCode'), 'label', true);
	             this._causeRow = rightGrid.addRow(Locale.getMsg('cause'), 'label', true);
	             this._causeGrRow = rightGrid.addRow(Locale.getMsg('causeGroupCode'), 'label', false);
	        
         // The bottonGrid will be hidden if the page width is under 800.        
	     var bottomGrid= Ext.create('AssetManagement.customer.view.utils.OxGrid',{});	       
	             this._strBeginRowBottom = bottomGrid.addRow(Locale.getMsg('dateDisrubtionStart'), 'label', true);
	             this._strEndRowBottom = bottomGrid.addRow(Locale.getMsg('timeDisrubtionEnd'), 'label', true);
	             this._objGrRowBottom = bottomGrid.addRow(Locale.getMsg('objectGroupCode'), 'label', true);
	             this._posRowBottom = bottomGrid.addRow(Locale.getMsg('position'), 'label', true);
	             this._posGrRowBottom = bottomGrid.addRow(Locale.getMsg('positionGroupCode'), 'label', true);
	             this._causeRowBottom = bottomGrid.addRow(Locale.getMsg('cause'), 'label', true);
	             this._causeGrRowBottom = bottomGrid.addRow(Locale.getMsg('causeGroupCode'), 'label', false);
	        
	             this.queryById('leftHistNotifData').add(leftGrid);	        
		         this.queryById('rightHistNotifData').add(rightGrid);
		         this.queryById('histNotifBottomGrid').add(bottomGrid);  
		    
	   } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseHistNotifDetailPage', ex);
	   }	    
    },
    
	getPageTitle: function() {
    	var title = '';
    	
    	try {
	        title = Locale.getMsg('notification');
    	
    	    var histNotif = this.getViewModel().get('histNotif');
    	
    	    if(histNotif) {
    		   title += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(histNotif.get('qmnum'));
    	    }
        } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseHistNotifDetailPage', ex);
	   }
    	
    	return title;
	},
	
	//protected
	//@override
	updatePageContent: function() {	
		try {		
			this.fillMainDataSection();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseHistNotifDetailPage', ex);
		}
	},

	fillMainDataSection: function() {
		try {	   	
	        var histNotif = this.getViewModel().get('histNotif');
			var tpl = this.getViewModel().get('tpl');
			var equi = this.getViewModel().get('equi');
			var ord = this.getViewModel().get('ord');
			
			/* Left Grid */
	       if(histNotif.get('qmnum')) {
	    	   this._notifRow.show();
	    	   this._notifRow.setContentString(histNotif.get('qmnum'));			   			  
		   } else {
			   this._notifRow.hide();
		   }
	       
		   if(histNotif.get('qmtxt')) {
			   this._shorttextRow.show();
			   this._shorttextRow.setContentString(histNotif.get('qmtxt'));			   			  
		   } else {
			   this._shorttextRow.hide();
		   }

		   if(tpl)
		   {
			   if(tpl.get('tplnr') || tpl.get('pltxt')) {
				   this._funcLocRow.show();
				   this._funcLocRow.setContentString(tpl.getDisplayIdentification() + ' ' + tpl.get('pltxt'));			   			  
			   } else {
				   this._funcLocRow.hide();
			   }
		   }
		   else
			   this._funcLocRow.hide();
		   		  
		   if(equi)
		   {
				if(equi.get('equnr') || equi.get('eqktx')) {
					this._equiRow.show();
				   this._equiRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + ' ' + equi.get('eqktx') );					   			  
				} else {
				   this._equiRow.hide();
				}
		   }
		   else
			   this._equiRow.hide();
			
			
		   if(histNotif.get('system_status')) {
			   this._sysRow.show();
			   this._sysRow.setContentString(histNotif.get('system_status'));					   			  
		   } else {
			   this._sysRow.hide();
		   }

		   if(histNotif.get('user_status')) {
			   this._userRow.show();
			   this._userRow.setContentString(histNotif.get('user_status'));					   			  
		   } else {
			   this._userRow.hide();
		   }

			if(ord){
			     this._orderRow.show();	
			     this._orderRow.setContentString(ord.get('aufnr') + ' ' + ord.get('ktext'));  
			}
			else if(histNotif.get('aufnr').length !== 0){
				this._orderRow.show();
				this._orderRow.setContentString(histNotif.get('aufnr')); 
			}
			else
				this._orderRow.hide();
			
			/* Right Grid */
	       if(histNotif.get('strmn')) {
	    	   this._strBeginRow.show();
			   this._strBeginRow.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histNotif.get('strmn')));					   			  
		   } else {
			   this._strBeginRow.hide();
		   }
			
		   if(histNotif.get('strur')) {
			   this._strEndRow.show();
			   this._strEndRow.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histNotif.get('strur')));					   			  
		   } else {
			   this._strEndRow.hide();
		   }
		   
		   if(histNotif.get('obj_part_grp_p1') || histNotif.get('obj_part_p1')) {
			   this._objGrRow.show();
			   this._objGrRow.setContentString(histNotif.get('obj_part_grp_p1') + ' ' + histNotif.get('obj_part_p1'));
		   } else {
			   this._objGrRow.hide();
		   }
		   
		   if(histNotif.get('cause_text_p1')) {
			   this._causeRow.show();
			   this._causeRow.setContentString(histNotif.get('cause_text_p1'));
		   } else {
			   this._causeRow.hide();
		   }
		   
		   if(histNotif.get('cause_grp_p1') || histNotif.get('cause_p1')) {
			   this._causeGrRow.show();
			   this._causeGrRow.setContentString(histNotif.get('cause_grp_p1') + ' ' + histNotif.get('cause_p1'));
		   } else {
			   this._causeGrRow.hide();
		   }
		   
		   if(histNotif.get('item_text_p1')) {
			   this._posRow.show();
			   this._posRow.setContentString(histNotif.get('item_text_p1'));
		   } else {
			   this._posRow.hide();
		   }
		   
		   if(histNotif.get('damage_grp_p1') || histNotif.get('damage_p1')) {
			   this._posGrRow.show();
			   this._posGrRow.setContentString(histNotif.get('damage_grp_p1')+ ' ' + histNotif.get('damage_p1'));
		   } else {
			   this._posGrRow.hide();
		   }

			
			/* Bottom Grid */
			if(histNotif.get('strmn')) {
	    	   this._strBeginRowBottom.show();
			   this._strBeginRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histNotif.get('strmn')));					   			  
		   } else {
			   this._strBeginRowBottom.hide();
		   }
			
		   if(histNotif.get('strur')) {
			   this._strEndRowBottom.show();
			   this._strEndRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histNotif.get('strur')));					   			  
		   } else {
			   this._strEndRowBottom.hide();
		   }
		   
		   if(histNotif.get('obj_part_grp_p1') || histNotif.get('obj_part_p1')) {
			   this._objGrRowBottom.show();
			   this._objGrRowBottom.setContentString(histNotif.get('obj_part_grp_p1') + ' ' + histNotif.get('obj_part_p1'));
		   } else {
			   this._objGrRowBottom.hide();
		   }
		   
		   if(histNotif.get('cause_text_p1')) {
			   this._causeRowBottom.show();
			   this._causeRowBottom.setContentString(histNotif.get('cause_text_p1'));
		   } else {
			   this._causeRowBottom.hide();
		   }
		   
		   if(histNotif.get('cause_grp_p1') || histNotif.get('cause_p1')) {
			   this._causeGrRowBottom.show();
			   this._causeGrRowBottom.setContentString(histNotif.get('cause_grp_p1') + ' ' + histNotif.get('cause_p1'));
		   } else {
			   this._causeGrRowBottom.hide();
		   }
		   
		   if(histNotif.get('item_text_p1')) {
			   this._posRowBottom.show();
			   this._posRowBottom.setContentString(histNotif.get('item_text_p1'));
		   } else {
			   this._posRowBottom.hide();
		   }
		   
		   if(histNotif.get('damage_grp_p1') || histNotif.get('damage_p1')) {
			   this._posGrRowBottom.show();
			   this._posGrRowBottom.setContentString(histNotif.get('damage_grp_p1')+ ' ' + histNotif.get('damage_p1'));
		   } else {
			   this._posGrRowBottom.hide();
		   }
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseHistNotifDetailPage', ex);
		}
	}
});