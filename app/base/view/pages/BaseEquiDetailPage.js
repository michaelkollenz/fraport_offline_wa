Ext.define('AssetManagement.base.view.pages.BaseEquiDetailPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.EquiDetailPageViewModel',
        'AssetManagement.customer.controller.pages.EquipmentDetailPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.View',
        'AssetManagement.customer.model.bo.CharactValue',
        'AssetManagement.customer.model.bo.ObjClassValue',
        'AssetManagement.customer.manager.ClassificationManager'
    ],

    inheritableStatics: {
    	_instance: null,
    	 
        getInstance: function() {
	        try {
                 if(this._instance === null) {
            	    this._instance = Ext.create('AssetManagement.customer.view.pages.EquiDetailPage');
                 }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseEquiDetailPage', ex);
		    }
            
            return this._instance;
        }
    },
    
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.EquiDetailPageRendererMixin'
    },

    config: {
        equipment: null
    },

    _equiRow: null,
	_shorttextRow: null,
	_sortFieldRow: null,
	_workcenterPlantRow: null,
	_producerAssetRowRight: null,
	_typeIdentifierRowRight: null,
	_constrMthAndYrRowRight: null,
	_guaranteeRowRight: null,
	_producerItemNumberRowRight: null,
	_producerAssetRowBottom: null,
	_typeIdentifierRowBottom: null,
	_constrMthAndYrRowBottom: null,
	_guaranteeRowBottom: null,
	_producerItemNumberRowBottom: null,

    viewModel: {
        type: 'EquiDetailModel'
    },
    
    controller: 'EquiDetailController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
    		var items =  [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    width: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 5,
		                    width: 1125,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'headerContainer',
		                            flex: 1,		                            
                            		layout: {
		                            	type: 'vbox',
	                                    align: 'stretch'
                                	},
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    id: 'generalDataContainer',
		                                    layout: {
		                                        type: 'vbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'label',
		                                            margin: '0 0 0 5',
		                                            cls: 'oxHeaderLabel',
		                                            text: Locale.getMsg('generalData')
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 20
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            id: 'equiGeneralData'
		                                        }
		                                    ]
		                                },
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    id: 'manufacturerDataContainer',
		                                    margin: '40 0 0 0',
		                                    layout: {
		                                        type: 'vbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'label',
		                                            margin: '0 0 0 5',
		                                            cls: 'oxHeaderLabel',
		                                            text: Locale.getMsg('producerData')
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 20
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            id: 'equiManufacturerData'
		                                        }
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            id: 'equiAddressContainer',
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    height: 50,
		                                    margin: '25 0 0 0',
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'bottom'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'label',
		                                            flex: 1,
		                                            cls: 'oxHeaderLabel',
		                                            margin: '0 0 0 5',
		                                            padding: '0 0 20 0',
		                                            text: Locale.getMsg('address')
		                                        },
		                                        {
		                                            xtype: 'button',
													padding: '0',
													margin: '0 5 0 0',
								                    html: '<img width="100%" src="resources/icons/maps_small.png"></img>',
									                height: 50,
									                width: 60,
									                id: 'equiAdressMapsButton',
									                listeners: {
									                	click: 'openEquipmentAddress'
									                }
		                                        }
		                                    ]
		                                },
		                                {
		                                    xtype: 'container',
		                                    id: 'equiAddressGrid'
		                                }
		                            ]
		                        },
                                 {
                                     xtype: 'container',
                                     margin: '40 5 0 5',
                                     id: 'equiClassContainer',
                                     layout: {
                                         type: 'vbox',
                                         align: 'stretch'
                                     },
                                     items: [
                                         {
                                             xtype: 'label',
                                             cls: 'oxHeaderLabel',
                                             text: Locale.getMsg('classifications')
                                         },
                                         {
                                             xtype: 'oxdynamicgridpanel',
                                             id: 'equiClassGridPanel',
                                             margin: '20 0 0 0',
                                             hideContextMenuColumn: true,
                                             disableSelection: true,
                                             parentController: myController,
                                             owner:me,
                                             /*columns: [
                                                 {
                                                     xtype: 'actioncolumn',
                                                     renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                         return '<img src="resources/icons/class_charact.png"  />';
                                                     },
                                                     id: 'equiClassIconColumn',
                                                     maxWidth: 80,
                                                     minWidth: 80,
                                                     align: 'center'
                                                 },
                                                 {
                                                     xtype: 'gridcolumn',
                                                     id: 'equiClassCharacteristicColumn',
                                                     dataIndex: 'classCharacteristic',
                                                     text: Locale.getMsg('characteristic'),
                                                     renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                         var description = null;

                                                         if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('atbez')))
                                                             description = record.get('atbez');
                                                         else
                                                             description = record.get('atnam');

                                                         return description;
                                                     },
                                                     flex: 2
                                                 },
                                                 {
                                                     xtype: 'gridcolumn',
                                                     id: 'equiClassCharacteristicValueColumn',
                                                     dataIndex: 'classCharacteristicValue',
                                                     text: Locale.getMsg('characteristicValue'),
                                                     renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        
                                                         var valueItem = '';
                                                         var filteredCharValues = Ext.create('Ext.data.Store', {
                                                             model: 'AssetManagement.customer.model.bo.CharactValue',
                                                             autoLoad: false
                                                         });

                                                         var valueList = Ext.create('Ext.data.Store', {
                                                             model: 'AssetManagement.customer.model.bo.ObjClassValue',
                                                             autoLoad: false
                                                         });
                                                         if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('classValues'))) {
                                                             
                                                             record.get('classValues').each(function (classValue) {
                                                                 if ((classValue.get('atinn') === record.get('atinn')) && !(classValue.get('updFlag') === 'D'))
                                                                     valueList.add(classValue);
                                                             });

                                                             if (valueList.getCount() > 0) {
                                                                 valueList.each(function (value) {
                                                                     if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value.get('atwrt'))) {
                                                                         if (record.get('charactValues') && record.get('charactValues').getCount() > 0) {
                                                                             record.get('charactValues').each(function (charValue) {
                                                                                 if (charValue.get('atwrt') === value.get('atwrt'))
                                                                                     filteredCharValues.add(charValue);
                                                                             });
                                                                         }
                                                                         else
                                                                             valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atwrt'), record);
                                                                     }
                                                                 });
                                                             }

                                                             if (filteredCharValues.getCount() !== 0) {
                                                                 if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(filteredCharValues.getAt(0).get('atwrt'))) {

                                                                     if ((filteredCharValues.getCount() > 0) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(filteredCharValues.getAt(0).get('atwtb')))
                                                                         filteredCharValues.each(function (itemVl) {
                                                                             valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(itemVl.get('atwtb'), record);
                                                                         });
                                                                     else
                                                                         valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(filteredCharValues.getAt(0).get('atwrt'), record);
                                                                 }
                                                                 else
                                                                     valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(filteredCharValues.getAt(0).get('atflv'), record);
                                                             }
                                                             else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(valueList.getAt(0).get('atflv')) && filteredCharValues.getCount() === 0)
                                                                 valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(valueList.getAt(0).get('atflv'), record);

                                                             valueItem += "\n";

                                                         }
                                                             
                                                         
                                                         return valueItem;
                                                     },
                                                     flex: 1,
                                                     minWidth: 100
                                                 }
                                             ],*/
                                             listeners: {
                                                 cellclick: myController.onClassificationSelected,
                                                 scope: myController
                                             }
                                         }
                                     ]
                                 },
                                {
                                    xtype: 'container',
                                    id: 'equiMeasPointListContainer',
                                    margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('measPoints')
                                        },
                                        {
                                            xtype: 'oxdynamicgridpanel',
                                            id: 'equiMeasPointGrid',
                                            hideContextMenuColumn: true,
                                            viewModel: {},
                                            margin: '20 0 0 0',
                                            parentController: myController,
                                            owner: me,
                                           /* columns: [
		                                          {
		                                              xtype: 'actioncolumn',
		                                              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                  var imageSrc = 'resources/icons/measpoints.png';

		                                                  return '<img src="' + imageSrc + '"/>';
		                                              },
		                                              maxWidth: 80,
		                                              minWidth: 80,
		                                              enableColumnHide: false,
		                                              align: 'center'
		                                          },
		                                         {
		                                             xtype: 'gridcolumn',
		                                             flex: 1,
		                                             renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                 var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');
		                                                 var measpointDesc = record.get('pttxt');
		                                                 return measpoint + '<p>' + measpointDesc + '</p>';
		                                             },
		                                             enableColumnHide: false,
		                                             dataIndex: '',
		                                             text: Locale.getMsg('measPoint')
		                                         },
		                                         {
		                                             xtype: 'gridcolumn',
		                                             flex: 1,
		                                             renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                 return record.get('psort');
		                                             },
		                                             enableColumnHide: false,
		                                             dataIndex: '',
		                                             text: Locale.getMsg('measIndicator')
		                                         },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                return record.get('lastRec') + ' ' + record.get('lastUnit');
		                                            },
		                                            enableColumnHide: false,
		                                            dataIndex: '',
		                                            text: Locale.getMsg('lastMeasValue')
		                                        }
                                            ],*/
                                            listeners: {
                                                cellclick: myController.measPointSelected,
                                                cellcontextmenu: myController.onCellContextMenu,
                                                scope: myController
                                            }
                                        }
                                    ]
                                },
		                        {
		                            xtype: 'container',
		                            margin: '40 5 0 5',
		                            id:'equiFilesAndDocumentsContainer',
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
                                	    {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('filesAndDocuments')
		                                },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'equiFileGridPanel',
		                                    margin: '20 0 0 0',
		                                    hideContextMenuColumn: true,
		                                    disableSelection: true,
		                                    parentController: myController,
		                                    owner: me,
		                                   /* columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/document.png"  />';
		                                            },
		                                            id: 'equiFileIconColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiFileNameDescColumn',
		                                            dataIndex: 'fileDescription',
		                                            text: Locale.getMsg('file'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var description = record.get('fileDescription');
			                                        
			                                            return description;
			                                    	},
			                                    	flex: 2
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiFileTypeSizeColumn',
		                                            text: Locale.getMsg('typeAndSize'),
		                                            dataIndex: 'fileType',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var type = record.get('fileType');
		                                        		var size = record.get('fileSize').toFixed(2) + ' kB';
			                                        
			                                            return type +'<p>' + size + '</p>';
			                                    	},
			                                    	flex: 1,
			                                    	minWidth: 100
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiFileOriginColumn',
		                                            text:  Locale.getMsg('fileStorageLocation'),
		                                            dataIndex: 'fileOrigin',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var stringToShow = "";
		                                        		var fileOrigin = record.get('fileOrigin');
		                                        		
		                                        		if(fileOrigin === "online") {
		                                        			stringToShow = Locale.getMsg('sapSystem');
		                                        		} else if(fileOrigin === "local") {
		                                        			stringToShow = Locale.getMsg('local');
		                                        		}
		                                            
			                                            return stringToShow;
			                                    	},
		                                        	flex: 1,
		                                        	minWidth: 100,
		                                        	maxWidth: 200
		                                        }
		                                    ],*/
			                                listeners: {
		    	                        		cellclick: myController.onFileSelected,
		    	                        		cellcontextmenu: myController.onCellContextMenuFileList,
		    	                        		scope: myController
		    	                        	}
		                                }
		                            ]
			                    },
                                {
	                                xtype: 'container',
	                                id: 'equiHistNotifContainer',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('historicalNotifs')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'equiHistNotifGrid',
		                                    hideContextMenuColumn: true,
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                parentController: myController,
			                                owner: me,
		                                   /* columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        	return '<img src="resources/icons/notif.png"  />';
		                                            },
		                                            id: 'equiHistNotifActionColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiHistNotifNotifColumn',
		                                            dataIndex: 'qmnum',
		                                            text: Locale.getMsg('notif'),
		                                            flex: 1, 
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var qmnum =  AssetManagement.customer.utils.StringUtils.trimStart(record.get('qmnum'), '0');
			                                            var qmtxt = record.get('qmtxt');
	
			                                            return qmnum +'<p>'+qmtxt+'</p>';
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'system_status',
		                                            id: 'equiHistNotifStatusColumn',
		                                            text: Locale.getMsg('status'),
		                                            flex: 1, 
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var status = record.get('system_status');
	
			                                            return status;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiHistNotifArtColumn',
		                                            dataIndex: 'qmart',
		                                            text: Locale.getMsg('notifType'),
		                                            flex: 1, 
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var qmart = record.get('qmart');
	
			                                            return qmart;
			                                        }
		                                        }
		                                    ],*/
		                                    listeners: {
	                            		     	cellclick: myController.histNotifSelected,
	                            		     	cellcontextmenu: myController.onCellContextMenu,
	                            		     	scope: myController
	                            	        }
		                                }
		                            ]
			                    },
                                {
	                                xtype: 'container',
	                                id: 'equiHistOrderContainer',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('historicalOrders')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'equiHistOrderGrid',
		                                    hideContextMenuColumn: true,
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                parentController: myController,
			                                owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        	           return '<img src="resources/icons/order.png"  />';
		                                            },
		                                            id: 'equiHistOrderActionColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'right'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiHistOrderOrderColumn',
		                                            dataIndex: 'aufnr',
		                                            flex: 1,
		                                            text: Locale.getMsg('order'),
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		//store -> item -> aufnr, ktext
					                                 	var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('aufnr'), '0');
					                                    var ktext = record.get('ktext');
					                                    return aufnr +'<p>'+ktext+'</p>';
					                                }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiHistOrderStatusColumn',
		                                            text: Locale.getMsg('status'),
		                                            dataIndex: 'system_status',
		                                            flex: 1,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var status = record.get('system_status');
	
			                                            return status;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'equiHistOrderArtColumn',
		                                            text: Locale.getMsg('orderType'),
		                                            dataIndex: 'auart',
		                                            flex: 1, 
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		//store -> item -> aufnr, ktext
					                                    var auart = record.get('auart');
					                                    return auart;
					                                }
		                                        }
		                                    ],*/
		                                    listeners: {
		                                		cellclick: myController.histOrderSelected, 
		                                		cellcontextmenu: myController.onCellContextMenu,
		                                		scope: myController
		                            	    }
                                        }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 30
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]		            
		        }
		    ]; 
    		
		    this.add(items); 
    		
		    var leftGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._equiRow = leftGrid.addRow(Locale.getMsg('equipment'), 'multilinelabel', true);
			this._shorttextRow = leftGrid.addRow(Locale.getMsg('shorttext'), 'multilinelabel', true);
			this._superiorFuncLocRow = leftGrid.addRow(Locale.getMsg('superiorFuncLoc'), 'link', true);
			this._superiorFuncLocRow.getTemplate().addListener('click', this.getController().navigateToFuncLoc, this);
			this._sortFieldRow = leftGrid.addRow(Locale.getMsg('sortField'), 'label', true);
			//this._storLocRow = leftGrid.addRow(Locale.getMsg('locationAndRoom'), 'label', true);
			this._sernrRow = leftGrid.addRow(Locale.getMsg('manifSerialnumber'), 'multilinelabel', true);
			this._matnrRow = leftGrid.addRow(Locale.getMsg('material'), 'multilinelabel', true);
			this._supEquiRow = leftGrid.addRow(Locale.getMsg('superordEqui'), 'link', true);
			this._supEquiRow.getTemplate().addListener('click', this.getController().navigateToSuperiorEquipment, this);
			this._workcenterPlantRow = leftGrid.addRow(Locale.getMsg('workcenterPlant'), 'multilinelabel', false);
		
			// The rightGrig will be shown if the page width is over 800.
		    var rightGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._producerAssetRowRight = rightGrid.addRow(Locale.getMsg('producerAsset'), 'multilinelabel', true);
			this._typeIdentifierRowRight = rightGrid.addRow(Locale.getMsg('typeIdentifier'), 'multilinelabel', true);
			this._constrMthAndYrRowRight = rightGrid.addRow(Locale.getMsg('constrMth') + ' / ' + Locale.getMsg('constrYr'), 'multilinelabel', true);
			this._guaranteeRowRight = rightGrid.addRow(Locale.getMsg('guarantee'), 'multilinelabel', true);
			this._producerItemNumberRowRight = rightGrid.addRow(Locale.getMsg('producerItemNumber'), 'multilinelabel', false);
			
			var addressGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._nameRow = addressGrid.addRow(Locale.getMsg('name'), 'multilinelabel', true);
			this._cityRow = addressGrid.addRow(Locale.getMsg('city'), 'multilinelabel', true);
			this._streetRow = addressGrid.addRow(Locale.getMsg('street'), 'multilinelabel', true);
			this._phonRow = addressGrid.addRow(Locale.getMsg('phone'), 'multilinelabel', false);
		
		    this.queryById('equiGeneralData').add(leftGrid);
		    this.queryById('equiManufacturerData').add(rightGrid);
		    this.queryById('equiAddressGrid').add(addressGrid);

    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseEquiDetailPage', ex);
		}
    },
    
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
    		retval = Locale.getMsg('equipment');
        	
			var equi = this.getViewModel().get('equipment');
			
			if(equi) {
				retval += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(equi.get('equnr'));
			}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseEquiDetailPage', ex);
		}

    	return retval;
	},
	
	//protected
	//@override    
	updatePageContent: function() {
		try {
			equipment = this.getViewModel().get('equipment');

			this.fillMainDataSection();
			this.manageSections(); 
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseEquiDetailPage', ex);
		}
	}, 
	
	fillMainDataSection: function() {
		try {
			/* LEFT GRID */
			this._equiRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(equipment.get('equnr'), '0'));
			
			// Kurztext
			if(equipment.get('eqktx')) {
				this._shorttextRow.show();
				this._shorttextRow.setContentString(equipment.get('eqktx'));
			} else {
				this._shorttextRow.hide();
				this._shorttextRow.setContentString('');
			}
			
			// Übergeordneter Technischer Platz
			var superiorFuncLoc = equipment.get('funcLoc');

			if(superiorFuncLoc) {
				this._superiorFuncLocRow.show();
				this._superiorFuncLocRow.setContentString(superiorFuncLoc.getDisplayIdentification() + ' ' + superiorFuncLoc.get('pltxt'));
			} else {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equipment.get('tplnr'))) {
					this._superiorFuncLocRow.show();
					this._superiorFuncLocRow.setContentString(equipment.get('tplnr'));
				} else {
					this._superiorFuncLocRow.hide();
					this._superiorFuncLocRow.setContentString('');
				}	
			}
			
			// Sortierfeld
			if(equipment.get('eqfnr')) {
				this._sortFieldRow.show();
				this._sortFieldRow.setContentString(equipment.get('eqfnr'));
			} else {
				this._sortFieldRow.hide();
				this._sortFieldRow.setContentString('');
			}
			/*
			// Standord / Raum
			if(equipment.get('stort') || equipment.get('msgrp')) {
				this._storLocRow.show();
				this._storLocRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ equipment.get('stort'), equipment.get('msgrp') ], '/', false, false, '-'));
			} else {
				this._storLocRow.hide();
				this._storLocRow.setContentString('');
			}*/
			
			// Arbeitspl. / Werk
			if(equipment.get('gewrk') || equipment.get('wergw')) {
				this._workcenterPlantRow.show();
				this._workcenterPlantRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ equipment.get('gewrk'), equipment.get('wergw') ], '/', false, false, '-'));
			} else {
				this._workcenterPlantRow.hide();
				this._workcenterPlantRow.setContentString('');
			}

			// Serialnummer
			if(equipment.get('sernr')) {
				this._sernrRow.show();
				this._sernrRow.setContentString(equipment.get('sernr'));
			} else {
				this._sernrRow.hide();
				this._sernrRow.setContentString('');
			}
			
			// Materialnummer
			if(equipment.get('matnr')) {
				this._matnrRow.show();
				this._matnrRow.setContentString(equipment.get('matnr'));
			} else {
				this._matnrRow.hide();
				this._matnrRow.setContentString('');
			}
			
			// Superior equipment
			var superiorEquipment = equipment.get('superiorEquipment');
			
			if(superiorEquipment) {
				this._supEquiRow.show();
				this._supEquiRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(superiorEquipment.get('equnr'), '0') + ' ' + superiorEquipment.get('eqktx'));
			} else {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equipment.get('hequi'))) {
					this._supEquiRow.show();
					this._supEquiRow.setContentString(equipment.get('hequi'));
				} else {
					this._supEquiRow.hide();
					this._supEquiRow.setContentString('');
				}	
			}
			
			/* RIGHT AND BOTTOM GRID */
	
			if(equipment.get('herst') || equipment.get('typbz') || equipment.get('baumm') || equipment.get('baujj') || 
					(equipment.get('gwldt') && equipment.get('gwlen')) || equipment.get('mapar')) {
				this.queryById('manufacturerDataContainer').show();
//				this.queryById('manufacturerDataContainerBottom').show();
			} else {
				this.queryById('manufacturerDataContainer').hide();
//				this.queryById('manufacturerDataContainerBottom').hide();
			}
			
			// Hersteller der Anlage
			if(equipment.get('herst')) {
				this._producerAssetRowRight.show();
				this._producerAssetRowRight.setContentString(equipment.get('herst'));
				
//				this._producerAssetRowBottom.show();
//				this._producerAssetRowBottom.setContentString(equipment.get('herst'));
			} else {
				this._producerAssetRowRight.hide();
//				this._producerAssetRowBottom.hide();
			}
			
			// Typbezeichnung
			if(equipment.get('typbz')) {
				this._typeIdentifierRowRight.show();
				this._typeIdentifierRowRight.setContentString(equipment.get('typbz'));
				
//				this._typeIdentifierRowBottom.show();
//				this._typeIdentifierRowBottom.setContentString(equipment.get('typbz'));
			} else {
				this._typeIdentifierRowRight.hide();
//				this._typeIdentifierRowBottom.hide();
			}			

			// Baumonat / Baujahr
			if(equipment.get('baumm') || equipment.get('baujj')) {
				this._constrMthAndYrRowRight.show();					
				this._constrMthAndYrRowRight.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ equipment.get('baumm'), equipment.get('baujj') ], '/', false, false, '-'));
				
//				this._constrMthAndYrRowBottom.show();					
//				this._constrMthAndYrRowbottom.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ equipment.get('baumm'), equipment.get('baujj') ], '/', false, false, '-'));
			} else {
				this._constrMthAndYrRowRight.hide();
//				this._constrMthAndYrRowBottom.hide();
			}	

			// Gewährleistung
			if(equipment.get('gwldt') && equipment.get('gwlen')) {
				this._guaranteeRowRight.show();
				this._guaranteeRowRight.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(equipment.get('gwldt')) + ' - ' + AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(equipment.get('gwlen')));
				
//				this._guaranteeRowBottom.show();
//				this._guaranteeRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(equipment.get('gwldt')) + ' - ' + AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(equipment.get('gwlen_i')));
			} else if (equipment.get('gwldt')){
				this._guaranteeRowRight.show();
				this._guaranteeRowRight.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(equipment.get('gwldt'))); 
			} else {
				this._guaranteeRowRight.hide();
//				this._guaranteeRowBottom.hide();
			}
			
			// Teilenummer (Herst.)
			if(equipment.get('mapar')) {
				this._producerItemNumberRowRight.show();
				this._producerItemNumberRowRight.setContentString(equipment.get('mapar'));
				
//				this._producerItemNumberRowBottom.show();
//				this._producerItemNumberRowBottom.setContentString(equipment.get('mapar'));
			} else {
				this._producerItemNumberRowRight.hide();
//				this._producerItemNumberRowBottom.hide();
			}
			
			/* ADDRESS DATA */
			var address = equipment.get('address');
			
			// show / hide addressLabe/Gride/Spaceholder
			if(address && (address.get('name1') || address.get('name2') || address.get('name3') || address.get('name4') ||
			   address.get('postCode1') || address.get('city1') || address.get('city2') || address.get('street') ||
			   address.get('houseNum1') || address.get('telNumber') || address.get('telExtens'))) {
				Ext.getCmp('equiAddressContainer').setHidden(false);

				// Name
				if(address.get('name1') || address.get('name2') || address.get('name3') || address.get('name4')) {
					this._nameRow.show();
					this._nameRow.setContentString(address.get('name1') + ' ' + address.get('name2') + ' ' + 
						address.get('name3') + ' ' + address.get('name4'));
				} else {
					this._nameRow.hide();
				}
				
				// Ort
				if(address.get('postCode1') || address.get('city1') || address.get('city2')) {
					this._cityRow.show();
					this._cityRow.setContentString(address.get('postCode1') + ' ' + address.get('city1') + ' ' + 
						address.get('city2'));
				} else {
					this._cityRow.hide();
				}
				
				// Straße
				if(address.get('street') || address.get('houseNum1')) {
					this._streetRow.show();
					this._streetRow.setContentString(address.get('street') + ' ' + address.get('houseNum1'));
				} else {
					this._streetRow.hide();
				}
				
				// Telefon
				if(address.get('telNumber') || address.get('telExtens')) {
					this._phonRow.show();
					this._phonRow.setContentString(address.get('telNumber') + ' ' + address.get('telExtens'));
				} else {
					this._phonRow.hide();
				}
				
			} else {
				Ext.getCmp('equiAddressContainer').setHidden(true); 
			}
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseEquiDetailPage', ex);
		}
	},
	
	manageSections: function() {
		try {
			var funcParas = AssetManagement.customer.model.bo.FuncPara.getInstance();
		
			var equipment = this.getViewModel().get('equipment');
			
			//files and documents
			if (funcParas.getValue1For('equi_docs_active') !== 'X') {
				Ext.getCmp('equiFilesAndDocumentsContainer').setHidden(true); 
			} else {
				Ext.getCmp('equiFilesAndDocumentsContainer').setHidden(false); 
					
				var fileStore = equipment.get('documents');
				Ext.getCmp('equiFileGridPanel').setStore(fileStore);
			}
			//histnotifs
			if (funcParas.getValue1For('equi_hfhist_active') !== 'X') {
				Ext.getCmp('equiHistNotifContainer').setHidden(true); 
			} else {
				Ext.getCmp('equiHistNotifContainer').setHidden(false); 
					
				var histnotifs = equipment.get('historicalNotifs'); 
				Ext.getCmp('equiHistNotifGrid').setStore(histnotifs);	
			}
			
			//historders
			if (funcParas.getValue1For('equi_orhist_active') !== 'X') {
				Ext.getCmp('equiHistOrderContainer').setHidden(true); 
			} else {
				Ext.getCmp('equiHistOrderContainer').setHidden(false); 
					
				var historders = equipment.get('historicalOrders');  
				Ext.getCmp('equiHistOrderGrid').setStore(historders); 
			}
			
		    //measPoints
			if (funcParas.getValue1For('equi_mp_active')!== 'X') {
			    Ext.getCmp('equiMeasPointListContainer').setHidden(true);
			} else {
			    Ext.getCmp('equiMeasPointListContainer').setHidden(false);

			    var measPoints = equipment.get('measPoints');
			    Ext.getCmp('equiMeasPointGrid').setStore(measPoints);
			}

		    //classifications
			if (funcParas.getValue1For('equi_cl_active') !== 'X') {
			    Ext.getCmp('equiClassContainer').setHidden(true);
			} else {
			    Ext.getCmp('equiClassContainer').setHidden(false);

			    var classifications = this.getViewModel().get('preparedClassifications');
			    Ext.getCmp('equiClassGridPanel').setStore(classifications);
			}

			//maps zur equi adresse  
			if (funcParas.getValue1For('equi_maps_active')!== 'X') {
				Ext.getCmp('equiAdressMapsButton').setHidden(true); 
			} else {
				Ext.getCmp('equiAdressMapsButton').setHidden(false); 
			}
			
			var address = equipment.get('address');
			
			// show / hide addressLabel/Gride/Spaceholder
			if (funcParas.getValue1For('equi_adr_active')=== true && address && (address.get('name1') || address.get('name2') || address.get('name3') || address.get('name4') ||
			   address.get('postCode1') || address.get('city1') || address.get('city2') || address.get('street') ||
			   address.get('houseNum1') || address.get('telNumber') || address.get('telExtens'))) {
				Ext.getCmp('equiAddressContainer').setHidden(false);
			} else {
				Ext.getCmp('equiAddressContainer').setHidden(true);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSections of BaseEquiDetailPage', ex);
		}
	}
});