Ext.define('AssetManagement.base.view.pages.BaseWaCreatePage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',
  alias: 'widget.BaseWaCreatePage',

  requires: [
    'AssetManagement.customer.model.pagemodel.WaCreatePageViewModel',
    'AssetManagement.customer.controller.pages.WaCreatePageController'

  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.WaCreatePage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseWaCreatePage', ex);
      }
      //
      return this._instance;
    }
  },

  config: {
    flex: 1,
    bodyCls: ''
  },

  viewModel: {
    type: 'WaCreatePageModel'
  },

  controller: 'WaCreatePageController',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {
      var items = [
        {
          xtype: 'container',
          flex: 1,
          layout: {
            type: 'hbox'
            // align: 'stretch'
          },
          items: [
            {
              xtype: 'container',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'oxcombobox',
                  height: 40,
                  // maxLength: 14,
                  // padding: '10 0 10 10',
                  id: 'waCreateBwartDropdown',
                  editable: false,
                  fieldLabel: Locale.getMsg('WaMiProcess'),
                  displayField: 'mi_descript',
                  queryMode: 'local',
                  listeners: {
                    select: 'onProcessDropdownSelect'
                  }
                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  flex: 1,
                  id: 'waCreateDynamicContainer',
                  layout: {
                    type: 'vbox',
                    align: 'stretch'
                  },
                  items: [
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldAUFNR',
                      fieldLabel: null,
                      maxLength: 12,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldKOSTL',
                      fieldLabel: null,
                      maxLength: 10,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldPS_POSID',
                      fieldLabel: null,
                      maxLength: 24,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldRSNUM',
                      fieldLabel: null,
                      maxLength: 10,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldLGORT_D',
                      fieldLabel: null,
                      maxLength: 4,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'component',
                      height: 10
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldWEMPF',
                      fieldLabel: null,
                      maxLength: 12,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'component',
                      height: 10
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'waCreateDynFieldXBLNR_LONG', //Materialscheinnummer REF_DOC_NO
                      fieldLabel: null,
                      maxLength: 16,
                      height: 40,
                      // readOnly: true,
                      padding: '10 10 10 10',
                      margin: '0 0 10 0',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onDynfieldKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    }
                  ]
                },
                {
                  xtype: 'container',
                  flex: 1,
                  margin: '20 0 0 0',
                  scrollable: false,
                  layout: {
                    type: 'hbox',
                    align: 'left',
                    pack: 'left'
                  },
                  items: [
                    {
                      xtype: 'label',
                      margin: '8 0 0 0',
                      text: Locale.getMsg('printKZ'),
                      width: 107
                    },
                    {
                      xtype: 'checkbox',
                      baseCls: 'oxCheckBox',
                      checkedCls: 'checked',
                      height: 52,
                      width: 54,
                      id: 'checkPrintKZ'
                    }]
                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  flex: 1,
                  layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                  },
                  items: [
                    {
                      xtype: 'button',
                      height: 52,
                      width: 54,
                      html: '<div><img  src="resources/icons/search.png"></img></div>',
                      id: 'saveWa',
                      listeners: {
                        click: 'onSaveClick'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        }
      ];

      this.add(items);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of waCreatePage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';
    try {
      retval = Locale.getMsg('WA');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of waCreatePage', ex);
    }

    return retval;
  },

  updatePageContent: function () {
    try {
      this.fillDropDownBoxes();
      // this.rebuildView();
      this.transferModelStateIntoView();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of waCreatePage', ex);
    }
  },
  transferModelStateIntoView: function () {
    try {
      var myModel = this.getViewModel();
      var poItem = myModel.get('poItem');
      var printKZ = myModel.get('printKZ');

      if (poItem) {
        //set general data
        if (poItem.get('ebelp')) {
          Ext.getCmp('waCreatePoPos').setValue(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('ebelp')));
        }
        if (poItem.get('matnr')) {
          Ext.getCmp('waCreateMaterialNo').setValue(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('matnr')));
        }
        if (poItem.get('txz01')) {
          Ext.getCmp('waCreateMaterialText').setValue(poItem.get('txz01'));
        }
        if (poItem.get('menge')) {
          Ext.getCmp('waCreateQuant').setValue(poItem.get('menge'));
        }
        if (poItem.get('menge_del')) {
          Ext.getCmp('waCreateQuantDel').setValue(poItem.get('menge_del'));
        }
        if (poItem.get('menge_ist')) {
          Ext.getCmp('waCreateQuantityIs').setValue(poItem.get('menge_ist'));
        }
      }
      Ext.getCmp('checkPrintKZ').setValue(printKZ);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of FbUmbPage', ex);
    }
  },

  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();
      var miProcess = myModel.get('miProcess');
      var relevantFields = myModel.get('relevantFields');
      var goodsMovementDefaults = myModel.get('goodsMovementDefaults');
      var container = Ext.getCmp('waCreateDynamicContainer');
      var printKZCheck = Ext.getCmp('checkPrintKZ');
      var refDoc = Ext.getCmp('waCreateDynFieldXBLNR_LONG').getValue();
      var checkItem = null;
      var rel_field = null;

      myModel.set('checkValue', '');

      if (!this.validateInputs()) {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorReqFields'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        return false;
      }


      if (!goodsMovementDefaults)
        goodsMovementDefaults = Ext.create('AssetManagement.customer.model.bo.GoodsMovementItem');

      goodsMovementDefaults.set('aufnr', Ext.getCmp('waCreateDynFieldAUFNR').getValue());
      goodsMovementDefaults.set('kostl', Ext.getCmp('waCreateDynFieldKOSTL').getValue());
      goodsMovementDefaults.set('rsnum', Ext.getCmp('waCreateDynFieldRSNUM').getValue());
      goodsMovementDefaults.set('umglo', Ext.getCmp('waCreateDynFieldLGORT_D').getValue());
      goodsMovementDefaults.set('wempf', Ext.getCmp('waCreateDynFieldWEMPF').getValue());
      goodsMovementDefaults.set('ps_posid', Ext.getCmp('waCreateDynFieldPS_POSID').getValue());

      myModel.set('goodsMovementDefaults', goodsMovementDefaults);


      relevantFields.each(function (relevantField) {
        if (relevantField.get('check_value') && relevantField.get('mi_process') == miProcess.get('mi_process')) {
          rel_field = relevantField;
        }

      });

      if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(rel_field.get('rel_field'))) {
        var containerItems = container.query();
        if (containerItems && containerItems.length > 0) {
          Ext.Array.each(containerItems, function (item) {
            var itemID = item.getId();
            if (AssetManagement.customer.utils.StringUtils.contains(itemID, rel_field.get('rel_field'), true)) {
              checkItem = item;
              return false;
            }
          });
        }
      }
      if (checkItem) {
        myModel.set('checkValue', checkItem.getValue());
        var title = rel_field.get('fieldname') + ' ' + checkItem.getValue();
        if (Ext.getCmp('waCreateDynFieldWEMPF').getValue()) {
          title = title + '<br>' + Locale.getMsg('wempf') + ' ' + Ext.getCmp('waCreateDynFieldWEMPF').getValue();
        }

        myModel.set('title', title)
      }

      if (printKZCheck) {
        myModel.set('printKZ', printKZCheck.getValue());
      }
      if (refDoc) {
        myModel.set('refDoc', refDoc);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of waCreatePage', ex);
    }
  },

  validateInputs: function () {

    try {
      var container = Ext.getCmp('waCreateDynamicContainer');
      var containerItems = container.query();
      var checkOK = true;

      Ext.Array.each(containerItems, function (item) {
        itemID = item.getId();
        if (AssetManagement.customer.utils.StringUtils.contains(itemID, 'DynField', true)) {
          if (!item.validate()) {
            checkOK = false;
          }
        }
      });
      return checkOK;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateInputs of waCreatePage', ex);
    }
  },

  fillDropDownBoxes: function () {
    try {
      var myModel = this.getViewModel();
      var availableItemsArray = [];
      var processStore = myModel.get('processStore');
      var waCreateBwartDropdown = Ext.getCmp('waCreateBwartDropdown');
      var container = Ext.getCmp('waCreateDynamicContainer');
      var miProcessSelected = myModel.get('miProcess');
      var relevantFields = myModel.get('relevantFields');
      var containerItems = container.query();
      var itemID = '';
      if (containerItems && containerItems.length > 0 && relevantFields && relevantFields.getCount() > 0) {
        if (miProcessSelected) {

          Ext.Array.each(containerItems, function (item) {
            itemID = item.getId();
            if (AssetManagement.customer.utils.StringUtils.contains(itemID, 'DynField', true)) {
              item.setValue(null);
              item.hide();
              item.setConfig('allowBlank', true);
              item.setConfig('validateBlank', false);
            }

            relevantFields.each(function (relevantField) {
              if (relevantField.get('mi_process') == miProcessSelected.get('mi_process') && AssetManagement.customer.utils.StringUtils.contains(itemID, relevantField.get('rel_field'), true)) {

                item.setFieldLabel(relevantField.get('fieldname'));
                if (relevantField.get('relevance') == '-') { // don´t show
                  item.hide();
                  item.setConfig('allowBlank', true);

                } else if (relevantField.get('relevance') == '+') { // show and mandatory
                  item.show();
                  item.setConfig('allowBlank', false);
                  item.setConfig('validateBlank', true);
                  availableItemsArray.push(itemID);

                } else if (relevantField.get('relevance') == '.' || relevantField.get('relevance') == '') { // show and optional
                  item.show();
                  item.setConfig('allowBlank', true);
                  availableItemsArray.push(itemID);
                }
              }
            });
          });
        } else {
          Ext.Array.each(containerItems, function (item) {
            itemID = item.getId();
            if (AssetManagement.customer.utils.StringUtils.contains(itemID, 'DynField', true)) {
              item.hide();
            }
          });
          waCreateBwartDropdown.setValue('')
        }
      }
      waCreateBwartDropdown.setStore(processStore);
      myModel.set('availableItemsArray', availableItemsArray);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of waCreatePage', ex);
    }
  }
});