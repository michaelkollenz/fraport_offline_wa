Ext.define('AssetManagement.base.view.pages.BaseComponentPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'AssetManagement.customer.model.pagemodel.ComponentPageViewModel',
        'AssetManagement.customer.controller.pages.ComponentPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.ComboBox',
        'Ext.Img',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.View',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.ComponentPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseComponentPage', ex);
		    }	

            return this._instance;
        }
    },
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.ComponentPageRendererMixin'
    },
    viewModel: {
        type: 'ComponentPageModel'
    },
    
    controller: 'ComponentPageController',    

    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
		    var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            items: [
		                                {
		                                    xtype: 'label',
		                                    cls: 'oxHeaderLabel',
		                                    id: 'componentPageModeLabel',
		                                    text: Locale.getMsg('planningComp')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'container',
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'container',
		                                            flex: 1,
		                                            maxWidth: 800,
		                                            layout: {
		                                                type: 'vbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                    xtype: 'oxcombobox',
			                                                id: 'componentOperDropDownBox',
			    	                                        displayField: 'text',
			    	                                        queryMode: 'local',
			    	                                        valueField: 'value',
			    	                                        editable: false,
			                                                fieldLabel: Locale.getMsg('operation')
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    height: 5
		                                                },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 1,
		                                                    id:'componentMaterialNo', 
		                                                    fieldLabel: Locale.getMsg('material'),
		                                                    maxLength: 18
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    height: 5
		                                                },
		                                                {
		                                                    xtype: 'container',
		                                                    flex: 1,
		                                                    layout: {
		                                                        type: 'hbox',
		                                                        align: 'stretch'
		                                                    },
		                                                    items: [
		                                                        {
		                                                            xtype: 'oxnumberfield',
		                                                            flex: 0.8,
		                                                            id: 'componentAmountTextField',
		                                                            fieldLabel: Locale.getMsg('quantityUnit')
		                                                        },
		                                                        {
		                                                            xtype: 'component',
		                                                            width: 10
		                                                        },
		                                                        {
		                                                            xtype: 'oxtextfield',
		                                                            flex: 0.2,
		                                                            id: 'componentUnitTextField',
		                                                            labelStyle: 'display: none;',
		                                                            maxLength: 3
		                                                        }
		                                                    ]
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    height: 10
		                                                },
		                                                {
		        		                                    xtype: 'container',
		        		                                    layout: {
		        		                                        type: 'hbox',
		        		                                        align: 'stretch'
		        		                                    },
		        		                                    items: [
		        		                                        {
		        		                                            xtype: 'oxtextfield',
		        		                                            id: 'componentPlantTextField', 
		        		                                            flex: 0.2,
		        		                                            minWidth: 180,
		        		                                            fieldLabel: Locale.getMsg('plantStorLoc'),
		        			                                        disabled: true,
		        			                                        maxLength: 4,
		        		                                            enableKeyEvents: true, 
		        			                                        listeners: {
		        			                                    		keyup: 'onPlantTextChanged'
		        			                                    	}
		        		                                        },
		        		                                        {
		        		                                            xtype: 'component',
		        		                                            width: 10
		        		                                        },
		        		                                        {
		        		                                            xtype: 'oxcombobox',
		        		                                            id: 'componentStorLocComboBox', 
		        		                                            flex: 0.3,
		        		                                            labelStyle: 'display: none;',
		        		                                            displayField: 'text',
		        			                                        queryMode: 'local',
		        			                                        valueField: 'value',
		        			                                        editable: false
		        		                                        }
		        		                                    ]
		        		                                }
		                                            ]
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            flex: 3,
		                                            mindWidth: 10,
		                                            maxWidth: 40
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            layout: {
		                                                type: 'vbox',
		                                                align: 'left',
		                                                pack: 'center'
		                                            },
		                                            items: [
		                                                {
		                                                	xtype: 'container',
			                                                items: [
			                                                    {
			                                                        xtype: 'button',
			                                                        html: '<div><img width="80%" src="resources/icons/search.png"></img></div>',
			                                                        id: 'searchButtonComponentPage',
				                                                    listeners: {
	                                                                	click: 'onSelectMaterialClick'
	                                                                }
			                                                    }
			                                                ]
		                                                }
		                                            ]
		                                        }
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 45
		                        },
		                        {
		                            xtype: 'container',
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'label',
		                                    margin: '0 0 0 5',
		                                    cls: 'oxHeaderLabel',
		                                    text: Locale.getMsg('plannedComponents')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'componentGridPanelComponentPage',
		                                    viewModel: { },
		                                    disableSelection: true,
		                                    parentController: myController,
                                            hideContextMenuColumn: true,
                                            owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/settings.png"  />';
		                                            },
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'matnr',
		                                            text: Locale.getMsg('material'),
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                	                		var material = record.get('material');
			                	                		var maktx = material ? material.get('maktx') : '';
			                	                    
			                	                		return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            text: Locale.getMsg('orderComponents_Qty'),
		                                            dataIndex: 'bdmng',
			                                        maxWidth: 160,
			                                        minWidth: 160,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bdmng'));
		                                        		return amount + ' ' + record.get('meins');
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            text: Locale.getMsg('operation'),
		                                            dataIndex: 'vornr',
		                                            flex: 0.6,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var retval = '';
		                                        
		                                        		try {
		                                        			retval = record.get('vornr');
		                                        			var myModel = this.getViewModel();
		                                        			
		                                        			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval) && myModel) {
		                                        				var order = myModel.get('order');
			                                        			var operations = order ? order.get('operations') : null;
			                                        			
			                                        			if(operations && operations.getCount() > 0) {
			                                        				operations.each(function(operation) {
			                                        					if(operation.get('vornr') === retval) {
			                                        						var ltxa1 = operation.get('ltxa1');
			                                        					
			                                        						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ltxa1))
			                                        							retval +=  ' - ' + ltxa1;
			                                        					
			                                        						return false;
			                                        					}
			                                        				}, this);
			                                        			}
			                                        		}
		                                        		} catch(ex) {
		                                        			AssetManagement.customer.helper.OxLogger.logException('Exception while rendering Components of BaseComponentPage', ex);
		                                        		}
		                                        		
		                                        		return retval;
			                                        }
		                                        }
		                                    ],*/
		                                    listeners: {
		                                		cellcontextmenu: myController.onCellContextMenu,
		                                		scope: myController
		                                	}
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 30
		                                }
		                            ]
		                        }
		                    ]
		                },
		                {
		                	xtype: 'component',
		                	width: 10
		                }
		            ]
		        }
		    ];
		    
		    this.add(items);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseComponentPage', ex);
    	}
	},
	
    getPageTitle: function() {
		var retval = '';
    
		try {
			var retval = Locale.getMsg('planningComp');
			
			var order = this.getViewModel().get('order');
			
			if(order) {
				retval += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(order.get('aufnr'));
			}
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseComponentPage', ex);
    	}

    	return retval;
	},

	//protected
	//@override
	updatePageContent: function() {
		try {
			this.fillDropDownBoxes();
			
			this.transferModelStateIntoView(); 
			
			this.refreshComponentPanel(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseComponentPage', ex);
		}
	},
	
	fillDropDownBoxes: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operations = order ? order.get('operations') : null;
    	
			//filling operations dropdownbox
			var operationComboBoxSource = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			if(operations && operations.getCount() > 0) {
				operations.each(function(oper) {
					var vornr = oper.get('vornr');
					
					operationComboBoxSource.add({ text: vornr + " - " + oper.get('ltxa1'), value: oper })
				}, this);
			}
			
			var operComboBox = Ext.getCmp('componentOperDropDownBox');
			operComboBox.clearValue();
			operComboBox.setStore(operationComboBoxSource);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseComponentPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var component = this.getViewModel().get('component');
			var editMode = this.getViewModel().get('isEditMode');

			Ext.getCmp('componentPageModeLabel').setText(editMode === true ? Locale.getMsg('editComponent') : Locale.getMsg('planNewComponent'));
		
			Ext.getCmp('componentMaterialNo').setValue(component.get('matnr'));
			Ext.getCmp('componentAmountTextField').setValue(AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(component.get('bdmng'))); 
			Ext.getCmp('componentUnitTextField').setValue(component.get('meins')); 
			Ext.getCmp('componentPlantTextField').setValue(component.get('werks'));
			
			//set drop down box values
	    	this.setDropDownBoxesConvenientToCurrentModel();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseComponentPage', ex);
		}
	},
	
	setDropDownBoxesConvenientToCurrentModel: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operations = order ? order.get('operations') : null;
			var component = myModel.get('component');
		
			//operation
			var componentsOperation = null;
			
			if(operations && operations.getCount() > 0) {
				operations.each(function(operation) {
					if(operation.get('vornr') === component.get('vornr')) {
						componentsOperation = operation;
						return false;
					}
				}, this);
			}
			
			var componentsOperationRecord = componentsOperation ? Ext.getCmp('componentOperDropDownBox').findRecordByValue(componentsOperation) : null;	
			
			if(componentsOperationRecord)
				Ext.getCmp('componentOperDropDownBox').setValue(componentsOperationRecord);
			
			//storage location
			this.fillStorageLocationComboBox(component.get('werks'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDropDownBoxesConvenientToCurrentModel of BaseComponentPage', ex);
		}
	},
	
	fillStorageLocationComboBox: function(plant) {
		try {
			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			var myModel = this.getViewModel();

			var defaultStorLoc = null; 
			var componentsStorLoc = null;
			var componentLgort = myModel.get('component').get('lgort');
			var storLocs = myModel.get('storLocs');
			
			if(storLocs && storLocs.getCount() > 0) {
				storLocs.each(function(storLoc) {
					if(storLoc.get('werks') === plant) {
						store.add({ text: storLoc.get('lgort') + " - " + storLoc.get('lgobe'), value: storLoc });
						
						if(storLoc.get('default_lgort') === 'X')
							defaultStorLoc = storLoc;
							
						if(storLoc.get('lgort') === componentLgort)
							componentsStorLoc = storLoc;
					}
				}, this);
				
				if(!defaultStorLoc)
					defaultStorLoc = storLocs.getAt(0);
			}
			
			var comboBox = Ext.getCmp('componentStorLocComboBox');
			comboBox.clearValue();
			comboBox.setStore(store);
			comboBox.setStore(store);
			
			var storLocToSet = componentsStorLoc;

			if(!storLocToSet)
				storLocToSet = defaultStorLoc;
				
			if(storLocToSet) {
				var storLocRecord = storLocToSet ? comboBox.findRecordByValue(storLocToSet) : null;	
				
				if(storLocRecord)
					comboBox.setValue(storLocRecord);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillStorageLocationComboBox of BanfEditPage', ex);
		}
	},
	
	//transfer current values from screen into model
	transferViewStateIntoModel: function() {
		try {
			var component = this.getViewModel().get('component');
			
			var operation = Ext.getCmp('componentOperDropDownBox').getValue();

			var vornr = operation ? operation.get('vornr') : '';
									
			component.set('vornr', vornr);
	        component.set('matnr', Ext.getCmp('componentMaterialNo').getValue());	  
			component.set('bdmng', AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(Ext.getCmp('componentAmountTextField').getValue()));		
	        component.set('meins', Ext.getCmp('componentUnitTextField').getValue());
	        component.set('werks', Ext.getCmp('componentPlantTextField').getValue());
	        
	        //set stor loc information
			component.set('lgort', Ext.getCmp('componentStorLocComboBox').getValue().get('lgort'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseComponentPage', ex);
		}
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			var operation = Ext.getCmp('componentOperDropDownBox').getValue();
			var vornr = operation ? operation.get('vornr') : '';
			
			var storLoc = Ext.getCmp('componentStorLocComboBox').getValue();
			var lgort = storLoc ? storLoc.get('lgort') : '';
		
			retval = {
				vornr: vornr,
				matnr: Ext.getCmp('componentMaterialNo').getValue(),
				bdmng: Ext.getCmp('componentAmountTextField').getValue(),
				meins: Ext.getCmp('componentUnitTextField').getValue(),
				werks: Ext.getCmp('componentPlantTextField').getValue(),
				lgort: lgort
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseComponentPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	refreshComponentPanel: function() {
		try {
			var componentGridPanel = Ext.getCmp('componentGridPanelComponentPage');
			componentGridPanel.setStore(this.getViewModel().get('order').get('components'));
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshComponentPanel of BaseComponentPage', ex);
		}
	},
	
	fillMaterialSpecFieldsFromStpo: function(stpo) {
		try {
			if(stpo) {
				Ext.getCmp('componentMaterialNo').setValue(stpo.get('idnrk'));
				Ext.getCmp('componentAmountTextField').setValue(1); 
				Ext.getCmp('componentUnitTextField').setValue(stpo.get('meins'));
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromStpo of BaseComponentPage', ex);
		}
	},
	
	fillMaterialSpecFieldsFromMatStock: function(matStock) {
		try {
			if(matStock) {
				this.fillStorageLocationComboBox(matStock.get('werks'));	
			
				Ext.getCmp('componentMaterialNo').setValue(matStock.get('matnr'));
				Ext.getCmp('componentAmountTextField').setValue(1); 
				Ext.getCmp('componentUnitTextField').setValue(matStock.get('meins'));
				Ext.getCmp('componentPlantTextField').setValue(matStock.get('werks'));
				
				this.trySetStorageLocationComboBoxByKeyField(matStock.get('werks'), matStock.get('lgort'));
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromMatStock of BaseComponentPage', ex);
		}
	},

	fillMaterialSpecFieldsFromMaterial: function (material) {
	    try {
	        if (material) {
	            Ext.getCmp('componentMaterialNo').setValue(material.get('matnr'));
	            Ext.getCmp('componentAmountTextField').setValue(1);
	            Ext.getCmp('componentUnitTextField').setValue(material.get('meins'));
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromMaterial of BaseComponentPage', ex);
	    }
	},
	
	//will try to set stor loc identified by the key values - if not found a message will be triggered and the value will be reset
	//passing incomplete values will just clear the current value
	trySetStorageLocationComboBoxByKeyField: function(werks, lgort) {
		try {
			var storLocComboBox = Ext.getCmp('componentStorLocComboBox');
		
			storLocComboBox.clearValue();

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(werks) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lgort)) {
				var targetValue = null;

				var targetStorLoc = null;
				var storLocs = this.getViewModel().get('storLocs');
				
				if(storLocs && storLocs.getCount() > 0) {
					storLocs.each(function(storLoc) {
						if(storLoc.get('werks') === werks && storLoc.get('lgort') === lgort) {
							targetStorLoc = storLoc;
							return true;
						}
					}, this);
				}
				
				var targetValue = targetStorLoc ? storLocComboBox.findRecordByValue(targetStorLoc) : null;	
				
				if(targetValue) {
					//value found, set it
					storLocComboBox.setValue(targetValue);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('storageLocationNotFound'), true, 1);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySetStorageLocationComboBoxByKeyField of BanfEditPage', ex);
		}
	}
});