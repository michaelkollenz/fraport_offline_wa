Ext.define('AssetManagement.base.view.pages.BaseCustomerDetailPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.CustomerDetailPageViewModel',
        'AssetManagement.customer.controller.pages.CustomerDetailPageController',
        'Ext.form.Label'
    ],

    inheritableStatics: {
    	_instance: null,
    	 
        getInstance: function() {
	        try {
                if(this._instance == null)
                {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.CustomerDetailPage');
                }
            } catch(ex) {
		    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseCustomerDetailPage', ex);
		    }
            
            return this._instance;
        }
    },

    _customerNoRow: null,
	_titleRow: null,
	_nameRow: null,
	_zipCodeRow: null,
	_cityRow: null,
	_streetRow: null,
	_mobilePhoneRow: null,
	_phoneRow: null,
	_faxRow: null,
	_functionRow: null,
	_emailRow: null,

    viewModel: {
        type: 'CustomerDetailPageModel'
    },
    
    controller: 'CustomerDetailController',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	this.callParent();
    	
    	try {
    		var items =  [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
	                        minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    width: 1125,
		                    minWidth: 750,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'container',
				                    layout: {
				                        type: 'vbox',
				                        align: 'stretch'
				                    },
		                            itemId: 'upperContent',
		                            items: [
		                                {
	                                        xtype: 'label',
	                                        margin: '0 0 0 7',
	                                        cls: 'oxHeaderLabel',
	                                        text: Locale.getMsg('generalData')
			                            },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		    	                        {
				                            xtype: 'container',
						                    layout: {
						                        type: 'hbox'
						                    },
				                            items: [
				                                {
				                                    xtype: 'container',
				                                    maxWidth: 600,
				                                    flex: 1,
				                                    layout: {
				                                        type: 'vbox',
				                                        align: 'stretch'
				                                    },
				                                    items: [
				                                        {
				                                            xtype: 'container',
				                                            id: 'customerGeneralData'
				                                        }
				                                    ]
				                                },
				                                {
			                                    	xtype: 'button',
			                                    	id: 'customerDetailsMapsButton',
			                                    	margin: '0',
													padding: '0',
								                    html: '<img width="100%" src="resources/icons/maps_small.png"></img>',
									                height: 60,
									                width: 60,
									                listeners: {
									                	click: 'openCustomerAddress'
									                }
				                                }
				                            ]
				                        }
		                            ]
		                           
		                        },
		                        {
		                            xtype: 'component',
		                            height: 30
		                        }
		                    ]
		                },
		                {
		                    xtype: 'container',
		                    width:30,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    }
		                }
		            ]
		        }
		    ]; 
		
    		this.add(items);
    	
		    var mainDataGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid', { flex: 1 });
			this._customerNoRow = mainDataGrid.addRow(Locale.getMsg('customerNumberShort'), 'label', true);
			this._titleRow = mainDataGrid.addRow(Locale.getMsg('formOfAddress'), 'label', true);
			this._nameRow = mainDataGrid.addRow(Locale.getMsg('name'), 'label', true);
			this._zipCodeRow = mainDataGrid.addRow(Locale.getMsg('zipCode'), 'label',  true);
			this._cityRow = mainDataGrid.addRow(Locale.getMsg('city'), 'label', true);
			this._streetRow = mainDataGrid.addRow(Locale.getMsg('street'), 'label', true);
			this._mobilePhoneRow = mainDataGrid.addRow(Locale.getMsg('mobilePhone'), 'label', true);
			this._phoneRow = mainDataGrid.addRow(Locale.getMsg('phone'), 'label', true);
			this._faxRow = mainDataGrid.addRow(Locale.getMsg('faxNumber'), 'label', true);
			this._functionRow = mainDataGrid.addRow(Locale.getMsg('functionOfPerson'), 'label', true);
			this._emailRow = mainDataGrid.addRow(Locale.getMsg('email'), 'label', false);
			
			this.queryById('customerGeneralData').add(mainDataGrid);
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseCustomerDetailPage', ex);
		}
    },
    		
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
    		retval = Locale.getMsg('customer');
        	
			var customer = this.getViewModel().get('customer');
			
			if(customer) {
				retval += " " + customer.get('kunnr');
			}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseCustomerDetailPage', ex);
		}

    	return retval;
	},
    
	//protected
	//@override
	updatePageContent: function() {
		try {
			this.fillMainDataSection();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseCustomerDetailPage', ex);
		}
	}, 
	
	fillMainDataSection: function() {
		try {
			
			var customer = this.getViewModel().get('customer');
			
			//customer no.
			this._customerNoRow.setContentString(customer.get('kunnr'));
			
			// title
			if(customer.get('titlep')) {
				this._titleRow.show();
				this._titleRow.setContentString(customer.get('titlep'));
			} else {
				this._titleRow.hide();
				this._titleRow.setContentString('');
			}
			
			// name
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('firstname')) ||
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('lastname'))) {
				var content = AssetManagement.customer.utils.StringUtils.concatenate([ customer.get('firstname'), customer.get('lastname') ], ' ', true);
					
				this._nameRow.show();
				this._nameRow.setContentString(content);
			} else {
				this._nameRow.hide();
				this._nameRow.setContentString('');
			}
			
			// zip code
			var hasZipCode = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('postlcod1'));
			
			if(hasZipCode) {
				this._zipCodeRow.show();
				this._zipCodeRow.setContentString(customer.get('postlcod1'));
			} else {
				this._zipCodeRow.hide();
				this._zipCodeRow.setContentString('');
			}
			
			// city
			var hasCity = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('city'));
			
			if(hasCity) {
				this._cityRow.show();
				this._cityRow.setContentString(customer.get('city'));
			} else {
				this._cityRow.hide();
				this._cityRow.setContentString('');
			}
			
			// street
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('street'))) {
				this._streetRow.show();
				this._streetRow.setContentString(customer.get('street'));
			} else {
				this._streetRow.hide();
				this._streetRow.setContentString('');
			}

			// mobile phone
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('telnumbermob'))) {
				this._mobilePhoneRow.show();
				this._mobilePhoneRow.setContentString(customer.get('telnumbermob'));
			} else {
				this._mobilePhoneRow.hide();
				this._mobilePhoneRow.setContentString('');
			}
			
			// phone
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('tel1numbr'))) {
				this._phoneRow.show();
				this._phoneRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ customer.get('tel1numbr'), customer.get('telextens') ], '-', true));
			} else {
				this._phoneRow.hide();
				this._phoneRow.setContentString('');
			}			

			// fax
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('faxnumber'))) {
				this._faxRow.show();					
				this._faxRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ customer.get('faxnumber'), customer.get('faxextens') ], '-', true));
			} else {
				this._faxRow.hide();
				this._faxRow.setContentString('');
			}	

			// function
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('funktion'))) {
				this._functionRow.show();
				this._functionRow.setContentString(customer.get('funktion'));
			} else {
				this._functionRow.hide();
				this._functionRow.setContentString('');
			}
			
			// email
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('email'))) {
				this._emailRow.show();
				this._emailRow.setContentString(customer.get('email'));
			} else {
				this._emailRow.hide();
				this._emailRow.setContentString('');
			}
			
			//maps button
			if(hasZipCode && hasCity) {
				this.queryById('customerDetailsMapsButton').show();
			} else {
				this.queryById('customerDetailsMapsButton').hide();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseCustomerDetailPage', ex);
		}
	}
});