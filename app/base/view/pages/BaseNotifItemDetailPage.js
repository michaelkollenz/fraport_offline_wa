Ext.define('AssetManagement.base.view.pages.BaseNotifItemDetailPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.NotifItemDetailPageViewModel',
        'AssetManagement.customer.controller.pages.NotifItemDetailPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],
    
    inheritableStatics: {
    	_instance: null,
    	 
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.NotifItemDetailPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNotifItemDetailPage', ex);
		    }
            
            return this._instance;
        }
    },
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.NotifItemDetailPageRendererMixin'
    },
    viewModel: {
        type: 'NotifItemDetailPageModel'
    },
    
    controller: 'NotifItemDetailPageController',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
   
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
    		var items =  [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    width: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
	                                xtype: 'label',
	                                margin: '0 0 0 5',
	                                cls: 'oxHeaderLabel',
	                                text: Locale.getMsg('notifItemDetails')
			                    },
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'notifItemGeneralDataContainer'
		                        },
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
	                                xtype: 'container',
	                                id: 'notifItemCausesSection',
	                                margin: '40 5 0 5',
	                                layout: {
	                                    type: 'vbox',
	                                    align: 'stretch'
	                                },
	                                items: [
	                                    {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('causes')
		                                },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifItemCauseGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
                                            owner: me,
		                                	dockedItems: [
				                                {
				                                    xtype: 'toolbar',
				                                    id: 'notifItemCausesFooterPanel', 
				                                    dock: 'bottom',
				                                    height: 50,
				                                    layout: {
				                                        type: 'hbox',
				                                        align: 'middle'
				                                    },
				                                    items: [
				                                    {
				                                        xtype: 'container',
				                                        flex: 1,
				                                        layout: {
				                                            type: 'hbox',
				                                            align: 'middle'
				                                        },
				                                        items: [
				                                            {
				                                                xtype: 'component',
					                                            maxWidth: 6,
					                                            minWidth: 6
				                                            },
				                                            {      
				                                            	   xtype: 'button',
				                                                    html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
				                                                    height: 40,
				                                                    width: 50,
				                                                    padding: 0,
				                                                    listeners: {
		                                                               	click: 'addCauseClick'
		                                                            }
				                                                },
				                                                {
				                                                    xtype: 'label',
				                                                    flex: 1,
				                                                    text: Locale.getMsg('addNotifCause')
				                                                }
				                                            ]
				                                     	}
				                                    ]
				                                }
				                            ],
		                                   /* columns: [
		                                        {
		                                            xtype: 'actioncolumn',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return '<img src="resources/icons/notif_cause.png"/>';
			                                        },
			                                        maxWidth: 80,
			                                        minWidth: 80,
			                                        align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'codegrkurztext',
		                                            flex: 1,
		                                            text: Locale.getMsg('cause'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	if(record.get('causeCustCode')) {
			                                        		var causekGrp = record.get('causeCustCode').get('codegrkurztext');
				                                    		var causeCode = record.get('causeCustCode').get('kurztext');
				                                    		
				                                    		return causekGrp + '<p>' + causeCode + '</p>'; 	
			                                        	}
			                                        	
			                                        	return ''; 
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            text: Locale.getMsg('description'),
		                                            flex: 1,
		                                            dataIndex: 'urtxt',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    		var causeTxt = record.get('urtxt');
			                                        	if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(causeTxt))
			                                        		causeTxt = '';
			                                        		
			                                    		return causeTxt; 
			                                        }
		                                        }
		                                    ],*/
			                                listeners: {
			                                	cellcontextmenu: myController.onCellContextMenuSubOjects,
			                                	scope: myController
			                                }
			                            }
		                            ]
		                        },
                                {
	                                xtype: 'container',
	                                id: 'notifItemActivitiesSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('notifActivities')
                                        },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifItemActivityGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
		                                    header: false,                                    
		                                    forceFit: true,
		                                    enableColumnHide: false,
		                                    disableSelection: true,
		                                    parentController: myController,
		                                    owner: me,
		                                    dockedItems: [
		                                         {
				                                    xtype: 'toolbar',
				                                    id: 'notifItemActivitiesFooterPanel', 
				                                    dock: 'bottom',
				                                    height: 50,
				                                    layout: {
				                                        type: 'hbox',
				                                        align: 'middle'
				                                    },
				                                    items: [
				                                    {
				                                        xtype: 'container',
				                                        flex: 1,
				                                        layout: {
				                                            type: 'hbox',
				                                            align: 'middle'
				                                        },
				                                        items: [
				                                            {
				                                                xtype: 'component',
					                                            maxWidth: 6,
					                                            minWidth: 6
				                                            },
				                                            {      
				                                            	   xtype: 'button',
				                                                   // flex: 0.035,
				                                                    html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
				                                                    height: 40,
				                                                    width: 50,
				                                                    padding: 0,
				                                                   listeners: {
				                                                	   click: 'addActivityClick'
				                                                   	}
				                                                },
				                                                {
				                                                    xtype: 'label',
				                                                    flex: 1,
				                                                    text: Locale.getMsg('addNotifActivity')
				                                                }
				                                            ]
				                                     	}
				                                    ]
				                                }
			                                ],
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/notif_activity.png"/>';
			                                        },
			                                        maxWidth: 80,
			                                        minWidth: 80,
			                                        align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'codegrkurztext',
		                                            flex: 1,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {			                                        	
			                                        	if(record.get('activityCustCode')) {
		                                        			var activityGrp = record.get('activityCustCode').get('codegrkurztext');
		    												var activityCode = record.get('activityCustCode').get('kurztext');
		    												
		    	                                    		return activityGrp + '<p>' + activityCode + '</p>'; 
		                                        		}
		                                        		return ''; 
		                                        		
														
			                                        },
			                                        text: Locale.getMsg('notifActivity')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            dataIndex: 'matxt',
			                                        text: Locale.getMsg('specification')
		                                        }
		                                    ],*/
			                                listeners: {
			                            		cellcontextmenu: myController.onCellContextMenuSubOjects,
			                            		scope: myController
		                            		}
		                                }
		                            ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'notifItemTasksSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('notifTasks')
                                        },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'notifItemGrid',
			                                viewModel: { },
			                                margin: '20 0 0 0',
		                                    header: false,                                    
		                                    forceFit: true,
		                                    enableColumnHide: false,
		                                    disableSelection: true,
		                                    parentController: myController,
		                                    owner: me,
		                                    dockedItems: [
		                                         {
				                                    xtype: 'toolbar',
				                                    id: 'notifItemTasksFooterPanel', 
				                                    dock: 'bottom',
				                                    height: 50,
				                                    layout: {
				                                        type: 'hbox',
				                                        align: 'middle'
				                                    },
				                                    items: [
				                                    {
				                                        xtype: 'container',
				                                        flex: 1,
				                                        layout: {
				                                            type: 'hbox',
				                                            align: 'middle'
				                                        },
				                                        items: [
				                                            {
				                                                xtype: 'component',
					                                            maxWidth: 6,
					                                            minWidth: 6
				                                            },
				                                            {      
				                                            	   xtype: 'button',
				                                                   // flex: 0.035,
				                                                    html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
				                                                    height: 40,
				                                                    width: 50,
				                                                    padding: 0, 
				                                                    listeners: {
				                                                	   click: 'addTaskClick'
				                                                   	}
				                                                   
				                                                },
				                                                {
				                                                	xtype: 'label',
				                                                	flex: 1,
				                                                	text: Locale.getMsg('addNotifTask')
				                                                }
				                                            ]
				                                     	}
				                                    ]
				                                }
			                                ],
		                                    /*columns: [
		                                         {
		                                            xtype: 'actioncolumn',
			                                         renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                             return '<img src="resources/icons/notif_task.png"/>';
			                                         },
			                                            maxWidth: 80,
				                                        minWidth: 80,
				                                        align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            dataIndex: 'codegrkurztext',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	if(record.get('taskCustCode')){
			                                        		var taskGrp = record.get('taskCustCode').get('codegrkurztext');
				                                    		var taskCode =  record.get('taskCustCode').get('kurztext');
				                                    		
				                                    		return taskGrp + '<p>' + taskCode + '</p>';
			                                        	}
			                                        	
			                                        	return ''; 
			                                        },
			                                        text: Locale.getMsg('notifTask')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('description'),
		                                            dataIndex: 'matxt',
			                                        text: Locale.getMsg('specification')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            dataIndex: '',
			                                        maxWidth: 120,
			                                        minWidth: 120,
			                                        text: Locale.getMsg('userStatus')
		                                        }
		                                    ],*/
		                                    listeners: {
			                            		cellcontextmenu: myController.onCellContextMenuSubOjects,
			                            		scope: myController
			                            	}
		                                }
		                            ]
		                        },
				                {
				                    xtype: 'component',
				                    height: 30
				                }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];
    		    
			this.add(items);
			
			var leftGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._notifItemDamage = leftGrid.addRow(Locale.getMsg('notifItem'), 'label', true);
			this._notifItemObject = leftGrid.addRow(Locale.getMsg('objectPart'), 'label', true);
			this._notifItemDescription = leftGrid.addRow(Locale.getMsg('description'), 'label', false);
			
			this.queryById('notifItemGeneralDataContainer').add(leftGrid);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNotifItemDetailPage', ex);
		}
    },

    getPageTitle: function() {
	    var retval = '';
    
	    try {
    	    retval = Locale.getMsg('notifItemDetailsForNotif');
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNotifItemDetailPage', ex);
		}
    	
		return retval;
	}, 

	//protected
	//@override
	updatePageContent: function() {
		try {
			item = this.getViewModel().get('item'); 
			
			this.fillMainDataSection();
			this.manageGridPanels();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNotifItemDetailPage', ex);
		}
	},
	
	fillMainDataSection: function() {
	   try {
			var otgrp = ''; 
   			var oteil = ''; 
		   	var damageGrp = '';
   			var damageCode = '';
   			
   			if(item.get('damageCustCode')) {
   				damageGrp = item.get('damageCustCode').get('codegrkurztext');
   				damageCode = item.get('damageCustCode').get('kurztext'); 
   				
   				this._notifItemDamage.setContentString(damageGrp + ' / ' + damageCode);
   			}
   			
   			if(item.get('objectPartCustCode')) {
   				otgrp = item.get('objectPartCustCode').get('codegrkurztext');
   		        oteil =  item.get('objectPartCustCode').get('kurztext');
   		        
   		        this._notifItemObject.setContentString(otgrp + ' / ' + oteil);
   			}
   		
	        this._notifItemDescription.setContentString(item.get('fetxt'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseNotifItemDetailPage', ex);
		}
	},
	
	manageGridPanels: function() {
		try {
			//set activities to activity Panel
			var itemStore = item.get('notifItemTasks');
			var notifItemActivityGridPanel =  Ext.getCmp('notifItemGrid');
			notifItemActivityGridPanel.setStore(itemStore);
			
			//set tasks to task panel
			var itemActivityStore = item.get('notifItemActivities');
			var notifItemGridPanel = Ext.getCmp('notifItemActivityGrid'); 
			notifItemGridPanel.setStore(itemActivityStore);
			
			//set causes to cause panel
			var itemCauseStore = item.get('notifItemCauses');
			var notifItemCauseGridPanel = Ext.getCmp('notifItemCauseGrid');
			notifItemCauseGridPanel.setStore(itemCauseStore);
			
			//manage visibility
			var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
			
			//notif item causes section
			var showCauses = funcPara.getValue1For('item_cause_active') === 'X';
			Ext.getCmp('notifItemCausesSection').setHidden(!showCauses);
			
			var showCausesAddOption = funcPara.getValue1For('item_cause_create') === 'X';
			Ext.getCmp('notifItemCausesFooterPanel').setHidden(!showCausesAddOption);
			
			//notif activities section
			var showActivities = funcPara.getValue1For('item_activity_active') === 'X';
			Ext.getCmp('notifItemActivitiesSection').setHidden(!showActivities);
			
			var showActivitiesAddOption = funcPara.getValue1For('item_activity_create') === 'X';
			Ext.getCmp('notifItemActivitiesFooterPanel').setHidden(!showActivitiesAddOption);
			
			//notif tasks section
			var showTasks = funcPara.getValue1For('item_task_active') === 'X';
			Ext.getCmp('notifItemTasksSection').setHidden(!showTasks);
			
			var showTasksAddOption = funcPara.getValue1For('item_task_create') === 'X';
			Ext.getCmp('notifItemTasksFooterPanel').setHidden(!showTasksAddOption);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageGridPanels of BaseNotifItemDetailPage', ex);
		}
	}
});