Ext.define('AssetManagement.base.view.pages.BaseNewOrderPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.NewOrderPageViewModel',
        'AssetManagement.customer.controller.pages.NewOrderPageController',
        'AssetManagement.customer.view.utils.OxSpinner',
        'Ext.container.Container',
        'AssetManagement.customer.view.utils.OxTextField',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.Label'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.NewOrderPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNewOrderPage', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'NewOrderPageModel'
    },

    controller: 'NewOrderPageController',

    buildUserInterface: function () {
        try {
            var items = [
               {
                   xtype: 'container',
                   layout: {
                       type: 'hbox',
                       align: 'stretch'
                   },
                   items: [
                       {
                           xtype: 'component',
                           width: 10
                       },
                       {
                           xtype: 'container',
                           flex: 1,
                           layout: {
                               type: 'vbox',
                               align: 'stretch'
                           },
                           items: [
                               {
                                   xtype: 'component',
                                   height: 20
                               },
                               {
                                   xtype: 'container',
                                   layout: {
                                       type: 'vbox',
                                       align: 'stretch'
                                   },
                                   items: [
                                       {
                                           xtype: 'oxcombobox',
                                           id: 'auartComboBox',
                                           fieldLabel: Locale.getMsg('orderDetail_AuartDesc'),
                                           labelWidth: 125,
                                           queryMode: 'local',
                                           displayField: 'text',
                                           valueField: 'value',
                                           //allowBlank: false,
                                           editable: false,
                                           listeners: {
                                               change: this.onOrdTypeSelected,
                                               scope: this
                                           }
                                       }
                                   ]
                               },
                               {
                                   xtype: 'component',
                                   height: 5
                               },
                               {
                                   xtype: 'container',
                                   id: 'containerNewOrderPmActivityType',
                                   minWidth: 500,
                                   layout: {
                                       type: 'vbox',
                                       align: 'stretch'
                                   },
                                   items: [
                                       {
                                           xtype: 'oxcombobox',
                                           id: 'ilartComboBox',
                                           fieldLabel: Locale.getMsg('pmActivType'),
                                           labelWidth: 125,
                                           queryMode: 'local',
                                           displayField: 'text',
                                           valueField: 'value',
                                           //allowBlank: false,
                                           editable: false,
                                           listeners: {
                                               change: this.onActivityTypeSelected,
                                               //blur: this.onActivityTypeBlur,
                                               scope: this
                                           }
                                       }
                                   ]
                               },
                               {
                                   xtype: 'component',
                                   height: 5
                               },
                               {
                                   xtype: 'container',
                                   layout: {
                                       type: 'vbox',
                                       align: 'stretch'
                                   },
                                   id: 'containerNewOrderHeaderFields',
                                   items: [
                                       {
                                           xtype: 'oxtextfield',
                                           id: 'newOrderShortTextField',
                                           labelWidth: 125,
                                           fieldLabel: Locale.getMsg('shorttext'),
                                           maxLength: 40,
                                           listeners: {
                                               blur: 'onOrderTextChanged'
                                           }
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       {
                                           xtype: 'container',
                                           layout: {
                                               type: 'hbox',
                                               align: 'stretch'
                                           },
                                           items: [
                                               {
                                                   xtype: 'datefield',
                                                   id: 'newOrderStartDateField',
                                                   format: 'd.m.Y',
                                                   maxLength: 10,
                                                   labelWidth: 125,
                                                   fieldLabel: Locale.getMsg('bscStart')
                                               },
                                               {
                                                   xtype: 'label',
                                                   text: '     ',
                                                   cls: 'ox-separator-label'
                                               },
                                               {
                                                   xtype: 'datefield',
                                                   id: 'newOrderEndDateField',
                                                   format: 'd.m.Y',
                                                   maxLength: 10,
                                                   labelWidth: 125,
                                                   fieldLabel: Locale.getMsg('basicFin')
                                               }
                                           ]
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       {
                                           xtype: 'oxcombobox',
                                           id: 'newOrderBemotComboBox',
                                           fieldLabel: Locale.getMsg('accountIndiShort'),
                                           queryMode: 'local',
                                           displayField: 'text',
                                           valueField: 'value',
                                           allowBlank: true,
                                           labelWidth: 125,
                                           editable: false,
                                           listeners: {
                                               change: this.onBemotSelected,
                                               scope: this
                                           }
                                       },
                                       {
                                            xtype: 'oxtextfield',
                                            id: 'newOrderSalesOrgTextField',
                                            maxLength: 4,
                                            labelWidth: 125,
                                            fieldLabel: Locale.getMsg('salesOrganisation')
                                        },
                                        {
                                            xtype: 'label',
                                            text: '     ',
                                            cls: 'ox-separator-label'
                                        },
                                        {
                                            xtype: 'oxtextfield',
                                            id: 'newOrderSalesChannelTextField',
                                            maxLength: 2,
                                            labelWidth: 125,
                                            fieldLabel: Locale.getMsg('salesChannel')
                                        },
                                        {
                                            xtype: 'label',
                                            text: '     ',
                                            cls: 'ox-separator-label'
                                        },
                                        {
                                            xtype: 'oxtextfield',
                                            id: 'newOrderDivisonTextField',
                                            maxLength: 3,
                                            labelWidth: 125,
                                            fieldLabel: Locale.getMsg('division')
                                        },
                                       
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       // =========== FuncLoc ==============
                                       {
                                           xtype: 'container',
                                           flex: 1,
                                           layout: {
                                               type: 'hbox',
                                               align: 'middle'
                                           },
                                           items: [
                                               {
                                                   xtype: 'oxtextfield',
                                                   flex: 1,
                                                   id: 'newOrderFuncLocTextField',
                                                   readOnly: true,
                                                   fieldLabel: Locale.getMsg('funcLoc'),
                                                   labelWidth: 125
                                               },
                                               {
                                                   xtype: 'component',
                                                   width: 10
                                               },
                                               {
                                                   xtype: 'container',
                                                   width: 62,
                                                   height: 40,
                                                   top: 20,
                                                   items: [
                                                       {
                                                           xtype: 'button',
                                                           bind: {
                                                               html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
                                                           },
                                                           padding: '3 0 0 10',
                                                           listeners: {
                                                               click: 'onSelectFuncLocClick'
                                                           }

                                                       }
                                                   ]
                                               }
                                           ]
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       // =========== Equi ==============			                        
                                       {
                                           xtype: 'container',
                                           flex: 1,
                                           layout: {
                                               type: 'hbox',
                                               align: 'middle'
                                           },
                                           items: [
                                               {
                                                   xtype: 'oxtextfield',
                                                   flex: 1,
                                                   id: 'newOrderEquiTextField',
                                                   readOnly: true,
                                                   fieldLabel: Locale.getMsg('equipment'),
                                                   labelWidth: 125
                                               },
                                               {
                                                   xtype: 'component',
                                                   width: 10
                                               },
                                               {
                                                   xtype: 'container',
                                                   width: 62,
                                                   height: 40,
                                                   top: 20,
                                                   items: [
                                                       {
                                                           xtype: 'button',
                                                           id: 'searchButtonEquiChange',
                                                           padding: '3 0 0 10',
                                                           bind: {
                                                               html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
                                                           },
                                                           listeners: {
                                                               click: 'onSelectEquiClick'
                                                           }

                                                       }
                                                   ]
                                               }
                                           ]
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       // =========== Partner ==============	
                                       {
                                           xtype: 'container',
                                           id: 'containerNewOrderPartners',
                                           layout: {
                                               type: 'vbox',
                                               align: 'stretch'
                                           },
                                           items: [
                                               {
                                                   xtype: 'container',
                                                   id: 'containerNewOrderCustomer',
                                                   flex: 1,
                                                   layout: {
                                                       type: 'hbox',
                                                       align: 'middle'
                                                   },
                                                   items: [
                                                       {
                                                           xtype: 'oxtextfield',
                                                           flex: 1,
                                                           id: 'newOrderCustomerTextField',
                                                           readOnly: true,
                                                           fieldLabel: Locale.getMsg('employer'),
                                                           labelWidth: 125
                                                       },
                                                       {
                                                           xtype: 'component',
                                                           width: 10
                                                       },
                                                       {
                                                           xtype: 'container',
                                                           width: 62,
                                                           height: 40,
                                                           top: 20,
                                                           items: [
                                                               {
                                                                   xtype: 'button',
                                                                   id: 'searchButtonCustomerChange',
                                                                   bind: {
                                                                       html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
                                                                   },
                                                                   padding: '3 0 0 10',
                                                                   listeners: {
                                                                       click: 'onSelectCustomerClick'
                                                                   }

                                                               }
                                                           ]
                                                       }
                                                   ]
                                               },
                                               {
                                                   xtype: 'container',
                                                   id: 'containerNewOrderAp',
                                                   flex: 1,
                                                   layout: {
                                                       type: 'hbox',
                                                       align: 'middle'
                                                   },
                                                   items: [
                                                       {
                                                           xtype: 'oxtextfield',
                                                           flex: 1,
                                                           id: 'newOrderApTextField',
                                                           readOnly: true,
                                                           fieldLabel: Locale.getMsg('partnerAp'),
                                                           labelWidth: 125
                                                       },
                                                       {
                                                           xtype: 'component',
                                                           width: 10
                                                       },
                                                       {
                                                           xtype: 'container',
                                                           width: 62,
                                                           height: 40,
                                                           top: 20,
                                                           items: [
                                                               {
                                                                   xtype: 'button',
                                                                   id: 'searchButtonApChange',
                                                                   bind: {
                                                                       html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
                                                                   },
                                                                   padding: '3 0 0 10',
                                                                   listeners: {
                                                                       click: 'onSelectePartnerApClick'
                                                                   }

                                                               }
                                                           ]
                                                       }
                                                   ]
                                               },
                                               {
                                                   xtype: 'container',
                                                   id: 'containerNewOrderZr',
                                                   flex: 1,
                                                   layout: {
                                                       type: 'hbox',
                                                       align: 'middle'
                                                   },
                                                   items: [
                                                       {
                                                           xtype: 'oxtextfield',
                                                           flex: 1,
                                                           id: 'newOrderZrTextField',
                                                           readOnly: true,
                                                           fieldLabel: Locale.getMsg('partnerZr'),
                                                           labelWidth: 125
                                                       },
                                                       {
                                                           xtype: 'component',
                                                           width: 10
                                                       },
                                                       {
                                                           xtype: 'container',
                                                           width: 62,
                                                           height: 40,
                                                           top: 20,
                                                           items: [
                                                               {
                                                                   xtype: 'button',
                                                                   id: 'searchButtonZrChange',
                                                                   bind: {
                                                                       html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
                                                                   },
                                                                   padding: '3 0 0 10',
                                                                   listeners: {
                                                                       click: 'onSelectePartnerZrClick'
                                                                   }

                                                               }
                                                           ]
                                                       }
                                                   ]
                                               }
                                           ]
                                       }
                                   ]
                               },
                               /*
                                * ==== Frist Operation ====
                                */
                               {
                                   xtype: 'container',
                                   id: 'containerNewOrderFirstOperation',
                                   layout: {
                                       type: 'vbox',
                                       align: 'stretch'
                                   },
                                   items: [
                                       {
                                           xtype: 'component',
                                           height: 30
                                       },
                                       {
                                           xtype: 'label',
                                           cls: 'oxHeaderLabel',
                                           text: Locale.getMsg('firstOperation')
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       {
                                           xtype: 'oxtextfield',
                                           id: 'newOrdOperShortTextField',
                                           maxLength: 40,
                                           fieldLabel: Locale.getMsg('shorttext'),
                                           labelWidth: 125
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       {
                                           xtype: 'container',
                                           flex: 1,
                                           layout: {
                                               type: 'hbox',
                                               align: 'stretch'
                                           },
                                           items: [
                                               {
                                                   xtype: 'datefield',
                                                   id: 'newOrderOperStartDateField',
                                                   fieldLabel: Locale.getMsg('startDate'),
                                                   format: 'd.m.Y',
                                                   maxLength: 10,
                                                   labelWidth: 125
                                               },
                                               {
                                                   xtype: 'label',
                                                   text: '     ',
                                                   cls: 'ox-separator-label'
                                               },
                                               {
                                                   xtype: 'datefield',
                                                   id: 'newOrderOperEndDateField',
                                                   fieldLabel: Locale.getMsg('endDate'),
                                                   format: 'd.m.Y',
                                                   maxLength: 10
                                               }
                                           ]
                                       },
                                       {
                                           xtype: 'component',
                                           height: 5
                                       },
                                       {
                                           xtype: 'container',
                                           flex: 1,
                                           layout: {
                                               type: 'hbox',
                                               align: 'stretch'
                                           },
                                           items: [
                                               {
                                                   xtype: 'utilsoxspinner',
                                                   id: 'newOrderWorkTimeHourTextField',
                                                   cls: 'ox-textbox-align-right',
                                                   labelWidth: 125,
                                                   fieldLabel: Locale.getMsg('worktime'),
                                                   minValue: 0,
                                                   maxValue: 24,
                                                   allowDecimals: false,
                                                   decimalPrecision: 0,
                                                   incrementValue: 1,
                                                   maxLength: 2,
                                                   width: 175,
                                                   editable: false,
                                                   enforceMaxLength: true,
                                                   accelerate: false
                                               },
                                               {
                                                   xtype: 'label',
                                                   width: 20,
                                                   text: ':',
                                                   cls: 'ox-separator-label'
                                               },
                                               {
                                                   xtype: 'utilsoxspinner',
                                                   id: 'newOrderWorkTimeMinuteTextField',
                                                   cls: 'ox-textbox-align-right',
                                                   labelStyle: 'display: none;',
                                                   minValue: 0,
                                                   maxValue: 59,
                                                   allowDecimals: false,
                                                   decimalPrecision: 0,
                                                   incrementValue: 1,
                                                   maxLength: 2,
                                                   width: 50,
                                                   editable: false,
                                                   enforceMaxLength: true,
                                                   accelerate: false
                                               }
                                           ]
                                       }
                                   ]
                               }
                           ]
                       },
                       {
                           xtype: 'component',
                           width: 10
                       }
                   ]
               }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNewOrderPage', ex);
        }
    },

    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('createNewOrder');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNewOrderPage', ex);
        }

        return retval;
    },

    //protected
    //@override
    updatePageContent: function () {
        try {
            Ext.getCmp('auartComboBox').setDisabled(false);
            Ext.getCmp('ilartComboBox').setDisabled(false);
            Ext.getCmp('containerNewOrderPmActivityType').setVisible(false);
            Ext.getCmp('containerNewOrderHeaderFields').setVisible(false);
            Ext.getCmp('containerNewOrderFirstOperation').setVisible(false);

            this.initFields();
            this.fillOrdTypeDropdown();
            this.fillBemotDropdown();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNewOrderPage', ex);
        }
    },

    /**
     * initializes the form fields with values from the order object
     */
    initFields: function () {
        try {
            this.clearAllFields()
            Ext.getCmp('newOrderShortTextField').setValue(this.getViewModel().get('order').get('ktext'));
            Ext.getCmp('newOrderStartDateField').setValue(this.getViewModel().get('order').get('gstrp'));
            Ext.getCmp('newOrderEndDateField').setValue(this.getViewModel().get('order').get('gltrp'));
            Ext.getCmp('newOrderSalesOrgTextField').setValue(this.getViewModel().get('order').get('vkorg'));
            Ext.getCmp('newOrderSalesChannelTextField').setValue(this.getViewModel().get('order').get('vtweg'));
            Ext.getCmp('newOrderDivisonTextField').setValue(this.getViewModel().get('order').get('vkgrp'));
            Ext.getCmp('newOrderFuncLocTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(this.getViewModel().get('order').get('tplnr'), '0'));
            Ext.getCmp('newOrderEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(this.getViewModel().get('order').get('equnr'), '0'));
            this.initWorktimeFields();
            Ext.getCmp('newOrdOperShortTextField').setValue(this.getViewModel().get('operation').get('ltxa1'));
            Ext.getCmp('newOrderOperStartDateField').setValue(this.getViewModel().get('operation').get('fsav'));
            Ext.getCmp('newOrderOperEndDateField').setValue(this.getViewModel().get('operation').get('fsed'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initFields of BaseNewOrderPage', ex);
        }
    },

    clearAllFields: function () {
        try {
            Ext.getCmp('newOrderShortTextField').setValue('');
            Ext.getCmp('newOrderStartDateField').setValue('');
            Ext.getCmp('newOrderEndDateField').setValue('');
            Ext.getCmp('newOrderSalesOrgTextField').setValue('');
            Ext.getCmp('newOrderSalesChannelTextField').setValue('');
            Ext.getCmp('newOrderDivisonTextField').setValue('');
            Ext.getCmp('newOrderFuncLocTextField').setValue('');
            Ext.getCmp('newOrderEquiTextField').setValue('');
            Ext.getCmp('newOrdOperShortTextField').setValue('');
            Ext.getCmp('newOrderOperStartDateField').setValue('');
            Ext.getCmp('newOrderOperEndDateField').setValue('');
            Ext.getCmp('newOrderCustomerTextField').setValue('');
            Ext.getCmp('newOrderApTextField').setValue('');
            Ext.getCmp('newOrderZrTextField').setValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearAllFields of BaseNewOrderPage', ex);
        }
    },

    /*
	 * =================================================================================
	 * ===================== Operations when initialising the page =====================
	 * =================================================================================
	 */
    /**
     * Loads the ordertypes from the database and fills the newOrderTypeComboField 
     */
    fillOrdTypeDropdown: function () {
        try {
            var auartComboBox = Ext.getCmp('auartComboBox');
            var orderTypes = this.getViewModel().get('orderTypes');
            var setValueIndex = -1;

            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            orderTypes.each(function (orderType) {
                if (orderType.get('createallowed') === 'X') {
                    store.add({ "text": orderType.get('auart') + " " + orderType.get('txt'), "value": orderType.get('auart') });
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('order').get('auart')) && orderType.get('auart') === this.getViewModel().get('order').get('auart'))
                        setValueIndex = store.length;
                }
            }, this);

            auartComboBox.clearValue();
            auartComboBox.setStore(store);

            //set the value of the order auart
            if (setValueIndex > 0) {
                auartComboBox.select(store.data.items[setValueIndex - 1].data.value);
            } else if (store.data.length == 1) {
                //set default value if only one auart was found
                auartComboBox.select(store.data.items[0].data.value);
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillOrdTypeDropdown of BaseNewOrderPage', ex);
        }
    },

    /**
     * Loads the pm activity types from the database and fills the ilartComboBox 
     */
    fillPmActivityTypeDropdown: function (auart) {
        try {
            var ilartComboBox = Ext.getCmp('ilartComboBox');
            var ilartTypes = this.getViewModel().get('ilartTypes');

            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            ilartTypes.each(function (ilartType) {
                if (ilartType.get('auart') == this.getViewModel().get('order').get('auart'))
                    store.add({ "text": ilartType.get('ilart') + " " + ilartType.get('ilatx'), "value": ilartType.get('ilart') });
            }, this);

            ilartComboBox.clearValue();
            ilartComboBox.bindStore(store);

            //set default value if only one ilart was found
            if (store.data.length == 1) {
                ilartComboBox.select(store.data.items[0].data.value);
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillOrdTypeDropdown of BaseNewOrderPage', ex);
        }
    },

    /**
     * fills the newOrderBemotComboBox
     */
    fillBemotDropdown: function () {
        try {
            var myModel = this.getViewModel();
            var bemotStore = myModel.get('bemotTypes');

            var store = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (bemotStore && bemotStore.getCount() > 0) {
                bemotStore.each(function (bemot) {
                    var bemotKey = bemot.get('bemot');

                    store.add({ text: (bemotKey + ' - ' + bemot.get('bemottxt')), value: bemotKey });
                }, this);
            }

            var bemotComboBox = Ext.getCmp('newOrderBemotComboBox');

            bemotComboBox.clearValue();
            bemotComboBox.bindStore(store);

            var bemotToPreselect = '';

            //set default value if only one bemot has been found
            if (store.getCount() == 1) {
                bemotToPreselect = store.getAt(0).get('value');
            } else {
                //set default value if order has already a bemot set
                var order = myModel.get('order');
                bemotToPreselect = order ? order.get('bemot') : '';
            }

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(bemotToPreselect)) {
                var defaultRecord = bemotComboBox.findRecordByValue(bemotToPreselect);

                if (defaultRecord) {
                    bemotComboBox.setValue(defaultRecord);
                }
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillBemotDropdown of BaseNewOrderPage', ex);
        }
    },

    /**
     * initializes the worktime fields from the value of operation.dauno
     */
    initWorktimeFields: function () {
        try {
            var date = AssetManagement.customer.utils.DateTimeUtils.getTimeFromFloat(this.getViewModel().get('operation').get('dauno'), 'H');
            var hours = 0;
            var minutes = 0;
            if (hours === 0 && minutes === 0)
                hours = 1;
            Ext.getCmp('newOrderWorkTimeHourTextField').setValue(hours);
            Ext.getCmp('newOrderWorkTimeMinuteTextField').setValue(minutes);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initWorktimeFields of BaseNewOrderPage', ex);
        }
    },

    /**
     * checks if and what partner lines should be displayed and changes their visibility
     */
    displayPartners: function () {
        try {
            Ext.getCmp('containerNewOrderPartners').setVisible(false);
            var ordType = this.getViewModel().get('orderTypes').findRecord('auart', this.getViewModel().get('order').get('auart'));
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ordType.get('partnertransfer'))) {
                Ext.getCmp('containerNewOrderPartners').setVisible(true);
                Ext.getCmp('containerNewOrderCustomer').setVisible(true);
                if (this.getViewModel().get('customer')) {
                    Ext.getCmp('containerNewOrderAp').setVisible(true);
                    Ext.getCmp('containerNewOrderZr').setVisible(true);
                }
                else {
                    Ext.getCmp('containerNewOrderAp').setVisible(false);
                    Ext.getCmp('containerNewOrderZr').setVisible(false);
                }
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside displayPartners of BaseNewOrderPage', ex);
        }
    },

    /*
	 * =================================================================================
	 * ================= operations to be performed on saving the order ================
	 * =================================================================================
	 */

    /**
	 *  update the date in the models from the data set in the view fields
	 */
    updateModelData: function () {
        try {
            this.getViewModel().get('order').set('ktext', Ext.getCmp('newOrderShortTextField').getValue());
            this.getViewModel().get('order').set('vkorg', Ext.getCmp('newOrderSalesOrgTextField').getValue());
            this.getViewModel().get('order').set('vtweg', Ext.getCmp('newOrderSalesChannelTextField').getValue());
            this.getViewModel().get('order').set('vkgrp', Ext.getCmp('newOrderDivisonTextField').getValue());
            this.getViewModel().get('order').set('gstrp', Ext.getCmp('newOrderStartDateField').getValue());
            this.getViewModel().get('order').set('gltrp', Ext.getCmp('newOrderEndDateField').getValue());
            this.getViewModel().get('operation').set('ltxa1', Ext.getCmp('newOrdOperShortTextField').getValue());
            this.getViewModel().get('operation').set('fsav', Ext.getCmp('newOrderOperStartDateField').getValue());
            this.getViewModel().get('operation').set('fsed', Ext.getCmp('newOrderOperEndDateField').getValue());
            this.updateOperationWorktime();
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateModelData of BaseNewOrderPage', ex);
        }
    },

    /**
	 * updates te operation store data for the worktime from the values set in the view fields
	 */
    updateOperationWorktime: function () {
        try {
            var hours = Ext.getCmp('newOrderWorkTimeHourTextField').getValue();
            var minutes = Ext.getCmp('newOrderWorkTimeMinuteTextField').getValue();
            var date = new Date(0, 0, 0, hours, minutes, 0, 0);
            var floatValue = AssetManagement.customer.utils.DateTimeUtils.getWorkAsFloat(date, 'h');
            this.getViewModel().get('operation').set('dauno', floatValue);
            this.getViewModel().get('operation').set('daune', 'H');
            this.getViewModel().get('operation').set('arbei', floatValue);
            this.getViewModel().get('operation').set('arbeh', 'H');
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateModelData of BaseNewOrderPage', ex);
        }
    },

    /*
	 * =================================================================================
     * ===================================== Events ====================================
	 * =================================================================================
     */
    /**
     * event called after an object in the auartComboBox has been selected
     */
    onOrdTypeSelected: function (combo, record) {
        try {
            if (record != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record)) {
                this.getViewModel().get('order').set('auart', record);
                var auart = this.getViewModel().get('orderTypes').findRecord('auart', this.getViewModel().get('order').get('auart'));
                this.getViewModel().get('order').set('orderType', auart);
                this.fillPmActivityTypeDropdown(this.getViewModel().get('order').get('auart'));
                Ext.getCmp('containerNewOrderPmActivityType').setVisible(true);
                Ext.getCmp('ilartComboBox').focus('', 10);
                this.showDataBasedOnOrderType();
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrdTypeSelected of BaseNewOrderPage', ex);
        }
    },

    /**
     * event called after an object in the ilartComboBox has been selected
     * disables the auartComboBox, sets the containerNewOrderHeaderFields and the containerNewOrderFirstOperation to visible
     */
    onActivityTypeSelected: function (combo, record) {
        try {
            if (record != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record)) {
                this.getViewModel().get('order').set('ilart', record);
                var ilart = this.getViewModel().get('ilartTypes').findRecord('ilart', this.getViewModel().get('order').get('ilart'));
                this.getViewModel().get('order').set('orderIlart', ilart);

                //the auartComboBox will be disabled to avoid changes which could result in data inconsistencies
                //Ext.getCmp('auartComboBox').setDisabled(true);
                Ext.getCmp('containerNewOrderHeaderFields').setVisible(true);
                this.displayPartners();
                Ext.getCmp('containerNewOrderFirstOperation').setVisible(true);

            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrdTypeSelected of BaseNewOrderPage', ex);
        }
    },

    /**
	 * event fired after the ilartComboBox loses focus
	 * disables ilartComboBox if a value has been selected
	 */
    onActivityTypeBlur: function (combo, event, options) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('order').get('ilart')))
                combo.setDisabled(true);
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onActivityTypeBlur of BaseNewOrderPage', ex);
        }
    },

    /**
	 * event fired after an object in the bemotComboBox has been selected
	 */
    onBemotSelected: function (combo, record) {
        try {
            if (record != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record)) {
                this.getViewModel().get('order').set('bemot', record);
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrdTypeSelected of BaseNewOrderPage', ex);
        }
    },


    /**
	 * Callback method for the Save order function in the controller
	 */
    saveOrderCallback: function (message) {
        try {
            if (message.success == false)
                AssetManagement.customer.core.Core.getMainView().showNotification(message);
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOrderCallback of BaseNewOrderPage', ex);
        }
    },

    /**
	 * called after equi or func loc has been selected through picker, updates the textfields with the selected data
	 */
    fillEquiFuncLocTextField: function () {
        try {
            var equi = this.getViewModel().get('equi');
            if (equi)
                Ext.getCmp('newOrderEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + ' ' + equi.get('eqktx'));
            else
                Ext.getCmp('newOrderEquiTextField').setValue('');
            var funcLoc = this.getViewModel().get('funcLoc');
            if (funcLoc)
                Ext.getCmp('newOrderFuncLocTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(funcLoc.getDisplayIdentification(), '0') + ' ' + funcLoc.get('pltxt'));
            else
                Ext.getCmp('newOrderFuncLocTextField').setValue('');
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquiFuncLocTextField of BaseNewOrderPage', ex);
        }
    },

    /**
	 * called after a partner has been selected through picker, updates the textfields with the selected data
	 */
    fillCustomerTextField: function () {
        try {
            var customer = this.getViewModel().get('customer');
            if (customer) {
                var custString = customer.get('name1');
                custString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('name2')) ? ' ' + customer.get('name2') : '';
                custString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('postcode1')) ? '    ' + customer.get('postcode1') + ' ' + customer.get('city1') : '';

                Ext.getCmp('newOrderCustomerTextField').setValue(custString.trim());
            }
            var partnerAp = this.getViewModel().get('partnerAp');
            if (partnerAp) {
                var apString = partnerAp.get('name1');
                apString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerAp.get('name2')) ? ' ' + partnerAp.get('name2') : '';
                apString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerAp.get('postcode1')) ? '    ' + partnerAp.get('postcode1') + ' ' + partnerAp.get('city1') : '';

                Ext.getCmp('newOrderApTextField').setValue(apString.trim());
            }
            var partnerZr = this.getViewModel().get('partnerZr');
            if (partnerZr) {
                var zrString = partnerZr.get('name1');
                zrString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerZr.get('name2')) ? ' ' + partnerZr.get('name2') : '';
                zrString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerZr.get('postcode1')) ? '    ' + partnerZr.get('postcode1') + ' ' + partnerZr.get('city1') : '';

                Ext.getCmp('newOrderZrTextField').setValue(zrString.trim());
            }
            this.displayPartners();

        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquiFuncLocTextField of BaseNewOrderPage', ex);
        }
    },

    /**
	 * callback after the order shorttext has been set
	 */
    setOperShortText: function () {
        try {
            var shortText = this.getViewModel().get('order').get('ktext');
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(shortText) &&
					AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('operation').get('ltxa1'))) {
                this.getViewModel().get('operation').set('ltxa1', shortText);
                Ext.getCmp('newOrdOperShortTextField').setValue(this.getViewModel().get('operation').get('ltxa1'));
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquiFuncLocTextField of BaseNewOrderPage', ex);
        }
    },

    showDataBasedOnOrderType: function() {
        try {
            var myModel = this.getViewModel();
            var orderTypes = myModel.get('orderTypes');
            var order = myModel.get('order');
            var selectedAuart = orderTypes.findRecord('auart', order.get('auart'));

            var bemotComboBox = Ext.getCmp('newOrderBemotComboBox');
            var orgTextField = Ext.getCmp('newOrderSalesOrgTextField');
            var vtwegTextField = Ext.getCmp('newOrderSalesChannelTextField');
            var spartTextField = Ext.getCmp('newOrderDivisonTextField');
            var newOrderCustomerTextField = Ext.getCmp('newOrderCustomerTextField');
            var searchButtonCustomerChange = Ext.getCmp('searchButtonCustomerChange');
            var isService = selectedAuart.get('service') === 'X' ? true : false;

            if(isService) {
                bemotComboBox.show();
                orgTextField.show();
                vtwegTextField.show();
                spartTextField.show();
                newOrderCustomerTextField.show();
                searchButtonCustomerChange.show()
            } else {
                bemotComboBox.setValue('');
                bemotComboBox.hide();
                orgTextField.setValue('');
                orgTextField.hide();
                vtwegTextField.setValue('');
                vtwegTextField.hide();
                spartTextField.setValue('');
                spartTextField.hide();
                newOrderCustomerTextField.setValue('');
                newOrderCustomerTextField.hide();
                searchButtonCustomerChange.hide()
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquiFuncLocTextField of BaseNewOrderPage', ex);
        }
    }
});