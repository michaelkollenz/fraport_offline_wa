Ext.define('AssetManagement.base.view.pages.BaseFuncLocEditPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.FuncLocEditPageViewModel',
        'AssetManagement.customer.controller.pages.FuncLocEditPageController',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.Date'
    ],

    inheritableStatics: {
    	_instance: null,
    	 
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.FuncLocEditPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseFuncLocEditPage', ex);
		    }
            
            return this._instance;
        }
    },
    
    viewModel: {
        type: 'FuncLocEditPageModel'
    },
    
    controller: 'FuncLocEditPageController',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
   
    buildUserInterface: function() {
    	try {
		    var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                            xtype: 'label',
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('generalData')
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditFuncLocTextField',
			                        disabled: true,
		                            fieldLabel: Locale.getMsg('funcLoc'),
		                            maxLength: 30,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditObjNumberTextField',
			                        disabled: true,
		                            fieldLabel: Locale.getMsg('objectnumber'),
		                            maxLength: 22,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            disabled: true, 
		                            id: 'funcLocEditShortTextField',
		                            fieldLabel: Locale.getMsg('shorttext'),
		                            maxLength: 40,
		                            labelWidth: 150
		                        },
		                       
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditInventoryNrTextField',
		                            fieldLabel: Locale.getMsg('inventoryNumber'),
		                            maxLength: 25,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
			                        xtype: 'datefield',
		                            id: 'funcLocEditDateField',
		                            format: 'd.m.Y',
		                            maxLength: 10,
		                            maxWidth: 300,
		                            fieldLabel: Locale.getMsg('dateOfAcquisition'),
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditSortTextField',
		                            fieldLabel: Locale.getMsg('sortField'),
		                            maxLength: 30,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditLocationTextField',
		                            fieldLabel: Locale.getMsg('location'),
		                            maxLength: 10,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            flex: 1,
		                            id: 'funcLocEditRoomTextField',
		                            fieldLabel: Locale.getMsg('room'),
		                            maxLength: 8,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'label',
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('producerData')
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditManufacturerTextField',
		                            fieldLabel: Locale.getMsg('producerAsset'),
		                            maxLength: 30,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditManufactCountryTextField',
		                            fieldLabel: Locale.getMsg('producingCountry'),
		                            maxLength: 3,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditTypeTextField',
		                            fieldLabel: Locale.getMsg('typeIdentifier'),
		                            maxLength: 20,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxnumberfield',
		                            id: 'funcLocEditBuildYearNumberField',
		                            fieldLabel: Locale.getMsg('constrYr'),
		                            maxLength: 4,
		                            labelWidth: 150,
		                            allowDecimals: false
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'funcLocEditBuildMonthTextField',
		                            fieldLabel: Locale.getMsg('constrMth'),
		                            maxLength: 2,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];
		    
		    this.add(items);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseFuncLocEditPage', ex);
		}
	},

	 getPageTitle: function() {
		var retval = '';
	 
		try {
		    retval = Locale.getMsg('masterDataInProcess');
	    } catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseFuncLocEditPage', ex);
		}
    	return retval; 
    },
    
   	//protected
	//@override
	updatePageContent: function() {
		try {
			this.transferModelStateIntoView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseFuncLocEditPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var funcLoc = this.getViewModel().get('funcLoc');
		
			Ext.getCmp('funcLocEditFuncLocTextField').setValue(funcLoc.getDisplayIdentification());
			Ext.getCmp('funcLocEditShortTextField').setValue(funcLoc.get('pltxt'));
			Ext.getCmp('funcLocEditObjNumberTextField').setValue(funcLoc.get('objnr'));
			Ext.getCmp('funcLocEditBuildMonthTextField').setValue(funcLoc.get('baumm'));
			Ext.getCmp('funcLocEditBuildYearNumberField').setValue(funcLoc.get('baujj'));
			Ext.getCmp('funcLocEditTypeTextField').setValue(funcLoc.get('typbz'));
			Ext.getCmp('funcLocEditManufactCountryTextField').setValue(funcLoc.get('herld'));
			Ext.getCmp('funcLocEditManufacturerTextField').setValue(funcLoc.get('herst'));
			Ext.getCmp('funcLocEditRoomTextField').setValue(funcLoc.get('msgrp'));
			Ext.getCmp('funcLocEditLocationTextField').setValue(funcLoc.get('stort'));
			Ext.getCmp('funcLocEditDateField').setValue(funcLoc.get('ansdt'));
			Ext.getCmp('funcLocEditSortTextField').setValue(funcLoc.get('eqfnr'));
			Ext.getCmp('funcLocEditInventoryNrTextField').setValue(funcLoc.get('invnr'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseFuncLocEditPage', ex);
		}
	},
	
	transferViewStateIntoModel: function() {
		try {
			var funcLoc = this.getViewModel().get('funcLoc');
			
			var baujj = Ext.getCmp('funcLocEditBuildYearNumberField').getValue();
			
			if(baujj)
				baujj += '';
			else
				baujj = '';
			
			funcLoc.set('baumm', Ext.getCmp('funcLocEditBuildMonthTextField').getValue());
			funcLoc.set('baujj', baujj);
			funcLoc.set('typbz', Ext.getCmp('funcLocEditTypeTextField').getValue());
			funcLoc.set('herld', Ext.getCmp('funcLocEditManufactCountryTextField').getValue());
			funcLoc.set('herst', Ext.getCmp('funcLocEditManufacturerTextField').getValue());
			funcLoc.set('msgrp', Ext.getCmp('funcLocEditRoomTextField').getValue());
			funcLoc.set('stort', Ext.getCmp('funcLocEditLocationTextField').getValue());
			funcLoc.set('ansdt', Ext.getCmp('funcLocEditDateField').getValue());
			funcLoc.set('eqfnr', Ext.getCmp('funcLocEditSortTextField').getValue());
			funcLoc.set('invnr', Ext.getCmp('funcLocEditInventoryNrTextField').getValue());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseFuncLocEditPage', ex);
		}
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			var baujj = Ext.getCmp('funcLocEditBuildYearNumberField').getValue();
			
			if(baujj)
				baujj += '';
			else
				baujj = '';
		
			retval = {
			    baumm: Ext.getCmp('funcLocEditBuildMonthTextField').getValue(),
			    baujj: baujj,
			    typbz: Ext.getCmp('funcLocEditTypeTextField').getValue(),
			    herld: Ext.getCmp('funcLocEditManufactCountryTextField').getValue(),
			    herst: Ext.getCmp('funcLocEditManufacturerTextField').getValue(),
			    msgrp: Ext.getCmp('funcLocEditRoomTextField').getValue(),
			    stort: Ext.getCmp('funcLocEditLocationTextField').getValue(),
			    ansdt: Ext.getCmp('funcLocEditDateField').getValue(),
			    eqfnr: Ext.getCmp('funcLocEditSortTextField').getValue(),
			    invnr: Ext.getCmp('funcLocEditInventoryNrTextField').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseFuncLocEditPage', ex);
			retval = null;
		}
		
		return retval;
	}
});