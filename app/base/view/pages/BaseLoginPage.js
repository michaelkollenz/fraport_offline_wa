Ext.define('AssetManagement.base.view.pages.BaseLoginPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',


  requires: [
    'AssetManagement.customer.model.pagemodel.LoginPageViewModel',
    'AssetManagement.customer.controller.pages.LoginPageController',
    'Ext.Component',
    'Ext.Img',
    'Ext.plugin.Responsive',
    'AssetManagement.customer.view.utils.OxTextField',
    'AssetManagement.customer.view.utils.OxNumberField',
    'Ext.button.Button',
    'Ext.form.field.ComboBox',
    'AssetManagement.customer.helper.OxLogger'
  ],

  plugins: [
    {
      ptype: 'responsive'
    }
  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.LoginPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseLoginPage', ex);
      }

      return this._instance;
    }
  },

  config: {
    flex: 1,
    bodyCls: ''
  },

  viewModel: {
    type: 'LoginPageModel'
  },

  controller: 'LoginPageController',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {
      var items = [
        {
          xtype: 'container',
          flex: 1,
          overflowY: 'auto',
          layout: {
            type: 'vbox',
            align: 'center',
            pack: 'center'
          },
          items: [
            {
              xtype: 'image',
              height: 84,
              width: 302,
              responsiveConfig: {
                'width < 350': {
                  height: 66,
                  width: 237
                },
                'width > 350': {
                  height: 84,
                  width: 302
                }
              },
              src: 'resources/icons/home_screen_logo.png',
              plugins: [
                {
                  ptype: 'responsive'
                }
              ]
            },
            //CUSTOMER LOGO
            //{
            //    xtype: 'component',
            //    height: 25
            //},
            //{
            //    xtype: 'image',
            //    height: 83,
            //    width: 299,
            //    src: 'resources/icons/customer_logo.jpg',
            //    responsiveConfig: {
            //    'width < 350': {
            //        height: 66,
            //        width: 237
            //    },
            //    'width > 350': {
            //        height: 83,
            //        width: 299
            //    }
            //},
            //    plugins: [
            //        {
            //            ptype: 'responsive'
            //        }
            //    ]
            //},
            {
              xtype: 'container',
              margin: '15 0 0 0',
              minHeight: 40,
              layout: {
                type: 'vbox',
                align: 'center',
                pack: 'center'
              },
              items: [
                {
                  xtype: 'label',
                  cls: 'loginPageInfoLabel',
                  id: 'loginPageMessageLabel',
                  maxWidth: 292,
                  responsiveConfig: {
                    'width < 350': {
                      maxWidth: 230
                    },
                    'width > 350': {
                      maxWidth: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                }
              ]
            },
            {
              xtype: 'component',
              height: 25
            },
            {
              xtype: 'container',
              id: 'loginPageLoginInputFieldsContainer',
              layout: {
                type: 'vbox'
              },
              items: [
                {
                  xtype: 'oxtextfield',
                  id: 'loginPageMandtTextField',
                  width: 292,
                  maxLength: 3,
                  fieldLabel: Locale.getMsg('sapClient'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onLoginButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },
                {
                  xtype: 'oxtextfield',
                  id: 'loginPageUserTextField',
                  width: 292,
                  maxLength: 12,
                  fieldLabel: Locale.getMsg('sapUser'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onLoginButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },
                {
                  xtype: 'oxtextfield',
                  inputType: 'password',
                  maxLength: 40,
                  id: 'loginPagePasswordTextField',
                  width: 292,
                  fieldLabel: Locale.getMsg('password'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onLoginButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },

                {
                  xtype: 'oxcombobox',
                  id: 'loginPageLanguageCombobox',
                  displayField: 'acronym',
                  valueField: 'locale',
                  queryMode: 'local',
                  maxWidth: 180,
                  editable: false,
                  fieldLabel: Locale.getMsg('language'),
                  listConfig: {
                    minWidth: 150,
                    itemTpl: Ext.create('Ext.XTemplate', '{acronym}', ' - ', '{description}')
                  },
                  listeners: {
                    change: 'onLocaleSelected'
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                }
              ]
            },
            {
              xtype: 'container',
              id: 'loginPageChangePasswordInputFieldsContainer',
              layout: {
                type: 'vbox'
              },
              items: [
                {
                  xtype: 'oxtextfield',
                  id: 'loginPageChangePasswordMandtTextField',
                  width: 292,
                  labelWidth: 135,
                  maxLength: 3,
                  fieldLabel: Locale.getMsg('sapClient'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onChangePasswordButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },
                {
                  xtype: 'oxtextfield',
                  id: 'loginPageChangePasswordUserTextField',
                  width: 292,
                  labelWidth: 135,
                  maxLength: 12,
                  fieldLabel: Locale.getMsg('sapUser'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onChangePasswordButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },
                {
                  xtype: 'oxtextfield',
                  inputType: 'password',
                  id: 'loginPagePasswordOldTextField',
                  width: 292,
                  labelWidth: 135,
                  maxLength: 40,
                  fieldLabel: Locale.getMsg('oldPassword'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onChangePasswordButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },
                {
                  xtype: 'oxtextfield',
                  inputType: 'password',
                  id: 'loginPageNewPasswordTextField',
                  width: 292,
                  labelWidth: 135,
                  maxLength: 40,
                  fieldLabel: Locale.getMsg('password'),
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onChangePasswordButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                },
                {
                  xtype: 'oxtextfield',
                  inputType: 'password',
                  id: 'loginPageNewPasswordRepeatTextField',
                  width: 292,
                  fieldLabel: Locale.getMsg('repeatPassword'),
                  labelWidth: 135,
                  maxLength: 40,
                  listeners: {
                    specialkey: {
                      fn: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                          this.getController().onChangePasswordButtonClick();
                        }
                      },
                      scope: this
                    }
                  },
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                }
              ]
            },
            {
              xtype: 'component',
              height: 30
            },
            {
              xtype: 'button',
              id: 'loginPageLoginButton',
              height: 31,
              width: 293,
              cls: 'oxLoginButton',
              responsiveConfig: {
                'width < 350': {
                  width: 230
                },
                'width > 350': {
                  width: 292
                }
              },
              text: Locale.getMsg('login'),
              listeners: {
                click: 'onLoginButtonClick'
              },
              plugins: [
                {
                  ptype: 'responsive'
                }
              ]
            },
            {
              xtype: 'button',
              id: 'loginPageChangePasswordButton',
              height: 31,
              width: 293,
              margin: '10 0 0 0',
              cls: 'oxLoginButton',
              responsiveConfig: {
                'width < 350': {
                  width: 230
                },
                'width > 350': {
                  width: 292
                }
              },
              text: Locale.getMsg('changePassword'),
              listeners: {
                click: 'onChangePasswordButtonClick'
              },
              plugins: [
                {
                  ptype: 'responsive'
                }
              ]
            },
            {
              xtype: 'button',
              id: 'loginPageCancelButton',
              height: 31,
              margin: '10 0 0 0',
              hidden: true,
              width: 293,
              cls: 'oxLoginButton',
              responsiveConfig: {
                'width < 350': {
                  width: 230
                },
                'width > 350': {
                  width: 292
                }
              },
              text: Locale.getMsg('cancel'),
              listeners: {
                click: 'onCancelChangePasswordButtonClick'
              },
              plugins: [
                {
                  ptype: 'responsive'
                }
              ]
            },
            {
              xtype: 'container',
              height: 50
            }
          ]
        },
        {
          xtype: 'container',
          layout: {
            type: 'hbox',
            align: 'left'
          },
          items: [
            {
              xtype: 'button',
              id: 'loginPageSettingsButton',
              html: '<div><img width="100%" src="resources/icons/settings.png"></img></div>',
              height: 40,
              width: 50,
              padding: 0,
              right: 0,
              listeners: {
                click: function(){
                  AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SETTINGS);
                }
              }
            },
            {
              xtype: 'displayfield',
              margin: '10 0 0 20',
              fieldLabel: 'Version',
              labelStyle: 'color: black',
              value: AssetManagement.customer.core.Core.getVersion(),
              labelWidth: 50
            }
          ]
        }
      ];

      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseLoginPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';
    try {
      retval = Locale.getMsg('login');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseLoginPage', ex);
    }

    return retval;
  },

  //protected
  //@override
  updatePageContent: function () {
    try {
      this.adoptToCurrentMode();

      this.transferModelStateIntoView();

      this.manageLanguageComboBox();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseLoginPage', ex);
    }
  },

  //will show the neccessary controls for the current mode and hide all others
  adoptToCurrentMode: function () {
    try {
      var myModel = this.getViewModel();

      var mode = this.getViewModel().get('mode');
      var controllerConstants = this.getController().self;

      switch (mode) {
        case controllerConstants.MODE_LOGIN:
          //hide
          Ext.getCmp('loginPageChangePasswordInputFieldsContainer').setVisible(false);
          Ext.getCmp('loginPageCancelButton').setVisible(false);

          //show
          Ext.getCmp('loginPageLoginInputFieldsContainer').setVisible(true);
          Ext.getCmp('loginPageLoginButton').setVisible(true);
          Ext.getCmp('loginPageChangePasswordButton').setVisible(true);
          break;
        case controllerConstants.MODE_CHANGE_PASSWORD:
          //hide
          Ext.getCmp('loginPageLoginInputFieldsContainer').setVisible(false);
          Ext.getCmp('loginPageLoginButton').setVisible(false);

          //show
          Ext.getCmp('loginPageChangePasswordInputFieldsContainer').setVisible(true);
          Ext.getCmp('loginPageChangePasswordButton').setVisible(true);
          Ext.getCmp('loginPageCancelButton').setVisible(true);
          break;
        default:
          break;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adoptToCurrentMode of BaseLoginPage', ex);
    }
  },

  transferModelStateIntoView: function () {
    try {
      var myModel = this.getViewModel();
      var isLogin = myModel.get('mode') === this.getController().self.MODE_LOGIN;

      var mandtTarget = isLogin ? Ext.getCmp('loginPageMandtTextField') : Ext.getCmp('loginPageChangePasswordMandtTextField');
      var userTagret = isLogin ? Ext.getCmp('loginPageUserTextField') : Ext.getCmp('loginPageChangePasswordUserTextField');

      mandtTarget.setValue(myModel.get('mandt'));
      userTagret.setValue(myModel.get('user'));
      Ext.getCmp('loginPagePasswordTextField').setValue(myModel.get('password'));
      Ext.getCmp('loginPagePasswordOldTextField').setValue(myModel.get('oldPassword'));
      Ext.getCmp('loginPageNewPasswordTextField').setValue(myModel.get('newPassword'));
      Ext.getCmp('loginPageNewPasswordRepeatTextField').setValue(myModel.get('newPasswordRepeat'));

      //set the message
      var message = myModel.get('statusMessage');
      var isHtml = AssetManagement.customer.utils.StringUtils.startsWith(message, '<');
      var messageLabel = Ext.getCmp('loginPageMessageLabel');

      if (isHtml) {
        messageLabel.setText('');
        messageLabel.setHtml(message);
      } else {
        messageLabel.setText(message);
      }

      Ext.getCmp('loginPageLanguageCombobox').setStore(myModel.get('languages'));
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseLoginPage', ex);
    }
  },

  manageLanguageComboBox: function (plant) {
    try {
      var valueToSet = AssetManagement.customer.core.Core.getAppConfig().getLocale()

      var comboBox = Ext.getCmp('loginPageLanguageCombobox');
      comboBox.setValue(valueToSet);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLanguageComboBox of BaseLoginPage', ex);
    }
  },

  //get current values from screen into model
  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();

      var isLogin = myModel.get('mode') === this.getController().self.MODE_LOGIN;

      var mandtSource = isLogin ? Ext.getCmp('loginPageMandtTextField') : Ext.getCmp('loginPageChangePasswordMandtTextField');
      var userSource = isLogin ? Ext.getCmp('loginPageUserTextField') : Ext.getCmp('loginPageChangePasswordUserTextField');

      myModel.set('mandt', mandtSource.getValue());
      myModel.set('user', userSource.getValue());
      myModel.set('password', Ext.getCmp('loginPagePasswordTextField').getValue());
      myModel.set('oldPassword', Ext.getCmp('loginPagePasswordOldTextField').getValue());
      myModel.set('newPassword', Ext.getCmp('loginPageNewPasswordTextField').getValue());
      myModel.set('newPasswordRepeat', Ext.getCmp('loginPageNewPasswordRepeatTextField').getValue());
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseLoginPage', ex);
    }
  }
});