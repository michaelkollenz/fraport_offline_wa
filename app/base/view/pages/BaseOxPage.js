Ext.define('AssetManagement.base.view.pages.BaseOxPage', {
    extend: 'Ext.panel.Panel',
    
	requires: [
	   'AssetManagement.customer.helper.OxLogger',
	   'AssetManagement.customer.helper.FocusHelper',
       'AssetManagement.customer.utils.OxException'
	],
	
	config: {
		cls: 'oxPage',
		bodyCls: 'pageStandardBody',
		header: false,
		scrollable: 'vertical'
	},

    inheritableStatics: {
        resetInstance: function() {
            try {

            	if(this._instance) {
            		this._instance.destroy();
				}
				this._instance = null;
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetInstance of BaseOxPage', ex);
            }

            return this._instance;
        }
    },

    //protected
    getClassName: function () {
        var retval = 'BaseOxPage';

        try {
            var className = Ext.getClassName(this);

            if (AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
                var splitted = className.split('.');
                retval = splitted[splitted.length - 1];
            } else {
                retval = className;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseOxPage', ex);
        }

        return retval;
    },

	constructor: function() {
		this.callParent(arguments);
		
		try {
		    this.initialize();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxPage', ex);
    	}    
	},
	
	//private
	initialize: function() {
		try {
		    this.buildUserInterface();
		    
		    this.addListener('activated', this.onActivation, this);
		    this.addListener('deactivated', this.onDeactivation, this);
		    this.addListener('hide', this.onHidden, this);
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseOxPage', ex);
    	}    
	},
	
	//private
	onHidden: function() {
		try {
		    this.fireEvent('deactivated');
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onHidden of BaseOxPage', ex);
    	}    
	},
	
	//protected
	onActivation: function() {
		try {
		    AssetManagement.customer.helper.FocusHelper.setFocusToNone();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onActivation of BaseOxPage', ex);
    	}    
	},
	
	//protected
	onDeactivation: function() {
		try {
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeactivation of BaseOxPage', ex);
    	}    
	},

	//protected
    getPageTitle: function() {
        return '';
    },

    //public
    //triggers a refresh of page affected UI elements
    //refreshing the page content itself is a part of this method
    refreshView: function() {
    	try {
    		Ext.suspendLayouts();
    	
    		//extenstion for asynchronous pages
    		if(this.getController().getIsAsynchronous() === true) {
    			//only set the page title, if the page itself is the current active one
    			if(this === AssetManagement.customer.core.Core.getCurrentPage()) {
    				AssetManagement.customer.core.Core.getMainView().getController().getMainToolbarController().setTitle(this.getPageTitle());
    			}
    		} else {
    			AssetManagement.customer.core.Core.getMainView().getController().getMainToolbarController().setTitle(this.getPageTitle());
    		}
    		
    		this.updatePageContent();
    		
    		Ext.resumeLayouts(true);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshView of BaseOxPage', ex);
    	}
    },
    
    //protected
    //may be implemented by derivates
    //using this method will ensure all updates will be done in one layout update cycle of the framework
    updatePageContent: function() {
    },
    
    //protected
    resetViewState: function() {
    	try {
    		AssetManagement.customer.core.Core.getMainView().getController().getMainToolbarController().setTitle(this.getPageTitle());
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseOxPage', ex);
    	}
	},
	
	//protected
	buildUserInterface: function() {
	},
	
	rebuildPage: function() {
		try {
			//remove all components and destroy them, to clear memory
			this.removeAll(true);
			
			//build the component again
			this.initialize();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildPage of BaseOxPage', ex);
    	}
	}
});