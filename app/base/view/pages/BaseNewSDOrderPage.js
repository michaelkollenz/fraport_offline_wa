Ext.define('AssetManagement.base.view.pages.BaseNewSDOrderPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.NewSDOrderPageViewModel',
        'AssetManagement.customer.controller.pages.NewSDOrderPageController',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.button.Button',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
		_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.NewSDOrderPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNewSDOrderPage', ex);
		    }
            
            return this._instance;
        }
    },

    mixins:{
        rendererMixin: 'AssetManagement.customer.view.mixins.NewSDOrderPageRendererMixin'
    },
    
    viewModel: {
        type: 'NewSDOrderPageModel'
    },
    
    controller: 'NewSDOrderPageController',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
   
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;

			var items = [
			    {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
				        {
				            xtype: 'component',
				            maxWidth: 15,
				            minWidth: 15
				        },
				        {
				            xtype: 'container',
				            flex: 1,
				            layout: {
				                type: 'vbox',
				                align: 'stretch'
				            },
				            items: [
				                {
				                    xtype: 'component',
				                    height: 20
				                },
				                {
				                    xtype: 'label',
				                    cls: 'oxHeaderLabel',
				                    text: Locale.getMsg('generalSDOrderData')
				                },
				                {
				                    xtype: 'component',
				                    height: 20
				                },
				                {
				                    xtype: 'oxtextfield',
				                    id: 'NewSDOrderPageSDOrderNumberTextField',
				                    fieldLabel: Locale.getMsg('orderingNumber'),
				                    maxLength: 20,
				                    labelWidth: 150,
				                    maxWidth: 400
				                },
				                {
				                    xtype: 'component',
				                    height: 10
				                },
				                {
		                            xtype: 'datefield',
		                            id: 'NewSDOrderPageRequestedDeliveryDateField',
		                            fieldLabel: Locale.getMsg('requestedDeliveryDate'),
		                            format: 'd.m.Y',
		                            maxLength: 10,
		                            labelWidth: 150,
		                            maxWidth: 400
				                },
				                {
				                    xtype: 'component',
				                    height: 30
				                },
				                {
                                	xtype: 'checkbox',
                                    baseCls: 'oxCheckBox',
                                    id: 'differentDeliveryAddressCheckBox',
                                    checked: true,
                                    checkedCls: 'checked',
                                    fieldLabel: Locale.getMsg('differentDeliveryAddress'),
                                    labelStyle: 'padding-top: 8px;',
                                    labelWidth: 170,
                                    width: 30,
                                    height: 35,
					                listeners: {
	                            		change: 'onAddressCheckBoxClicked'
	                            	}
	                            },
				                {
				                    xtype: 'container',
				                    margin: '15 0 0 0',
				                    id: 'sdOrderDeliveryAdressContainer',
				                    layout: {
				                        type: 'hbox',
				                        align: 'stretch'
				                    },
				                    items: [
				                        {
				                            xtype: 'container',
				                            flex: 1,
				                            maxWidth: 500,
				                            layout: {
				                                type: 'vbox',
				                                align: 'stretch'
				                            },
				                            items: [
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPageTitleTextField',
				                                    fieldLabel: Locale.getMsg('termOfAddress'),
				                                    maxLength: 20,
				                                    labelWidth: 150,
				                                    hidden: true
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10,
				                                    hidden: true
				                                },
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPageNameTextField',
				                                    fieldLabel: Locale.getMsg('name'),
				                                    maxLength: 40,
				                                    labelWidth: 150
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10
				                                },
		                                        {
		                                            xtype: 'oxtextfield',
		                                            id: 'NewSDOrderPageName2TextField',
		                                            labelWidth: 150,
		                                            maxLength: 40
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10
				                                },
				                                {
				                                    xtype: 'container',
				                                    layout: {
				                                        type: 'hbox',
				                                        align: 'stretch'
				                                    },
				                                    items: [
				                                        {
				                                            xtype: 'oxtextfield',
				                                            id: 'NewSDOrderPageStreetTextField',
				                                            flex: 1.5,
				                                            maxLength: 60,
				                                            fieldLabel: Locale.getMsg('streetHouseNr'),
				                                            labelWidth: 150
				                                        },
				                                        {
				                                            xtype: 'component',
					                                        minWidth: 10,
					    		                            maxWidth: 10
				                                        },
				                                        {
				                                            xtype: 'oxtextfield',
				                                            id: 'NewSDOrderPageHouseNumTextField',
				                                            flex: 0.2,
				                                            labelStyle: 'display: none;',
				                                            maxLength: 10
				                                        }
				                                    ]
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10
				                                }
				                            ]
				                        },
				                        {
				                            xtype: 'component',
				                            minWidth: 10,
				                            maxWidth: 10
				                        },
				                        {
				                            xtype: 'container',
				                            margin: '0 0 0 30',
				                            flex: 1,
				                            maxWidth: 350,
				                            layout: {
				                                type: 'vbox',
				                                align: 'stretch'
				                            },
				                            items: [
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPageZipCodeTextField',
				                                    fieldLabel: Locale.getMsg('zipCode'),
				                                    maxLength: 10
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10
				                                },
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPageCityTextField',
				                                    fieldLabel: Locale.getMsg('city'),
				                                    maxLength: 40
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10
				                                },
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPageCountryTextField',
				                                    maxLength: 3,
				                                    maxWidth: 150,
				                                    fieldLabel: Locale.getMsg('countryHintCode')
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 30
				                                }
				                            ]
				                        }
				                    ]
				                },
				                {
				                    xtype: 'component',
				                    height: 40
				                },
				                {
				                    xtype: 'label',
				                    cls: 'oxHeaderLabel',
				                    text: Locale.getMsg('captureSDOrderPosition')
				                },
				                {
				                    xtype: 'component',
				                    height: 20
				                },
				                {
				                    xtype: 'container',
				                    layout: {
				                        type: 'hbox',
				                        align: 'center',
				                        pack: 'left'
				                    },
				                    items: [
				                        {
				                            xtype: 'container',
				                            flex: 1,
				                            maxWidth: 500,
					                        layout: {
				                                type: 'vbox',
				                                align: 'stretch'
				                            },
				                            items: [
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPageMaterialNrTextField',
				                                    fieldLabel: Locale.getMsg('materialNumberShort'),
				                                    maxLength: 18,
				                                    labelWidth: 150,
				                                    maxWidth: 400
				                                },
				                                {
				                                    xtype: 'component',
				                                    height: 10
				                                },
				                                {
		                                            xtype: 'container',
		                                            maxWidth: 400,
		                                            layout: {
		                                                type: 'hbox'
		                                            },
		                                            items: [
		                                                {
		                                                    xtype: 'oxnumberfield',
		                                                    id: 'NewSDOrderPagePositionAmountTextField',
		                                                    flex: 1,
		                                                    fieldLabel: Locale.getMsg('quantityUnit'),
		                                                    maxLength: 13,
		                                                    labelWidth: 150
		                                                },
		                                                {
		                                                    xtype: 'component',
		                                                    width: 10
		                                                },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    id: 'NewSDOrderPagePositionUnitTextField',
		                                                    width: 60,
		                                                    labelStyle: 'display: none;',
		                                                    maxLength: 3
		                                                }
		                                            ]
		                                        },
		                                        {
				                                    xtype: 'component',
				                                    height: 15
				                                },
				                                {
				                                    xtype: 'oxtextfield',
				                                    id: 'NewSDOrderPagePositionRemarkTextField',
				                                    flex: 1,
				                                    maxLength: 40,
				                                    fieldLabel: Locale.getMsg('posText'),
				                                    labelWidth: 150
				                                }
				                            ]
				                        },
				                        {
                                            xtype: 'button',
                                            margin: '0 0 0 30',
                                            width: 50,
                                            height: 50,
                                            html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
                                            id: 'addSDOrderItemButtonNewSDOrderPage',
                                            listeners: {
                                            	click: 'onAddSDOrderItemClick'
                                            }
                                        }
				                    ]
				                },
				                {
				                    xtype: 'component',
				                    height: 40
				                },
				                {
		                            xtype: 'container',
		                            flex: 1,
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'label',
		                                    flex: 1,
		                                    cls: 'oxHeaderLabel',
		                                    text: Locale.getMsg('sdOrderItems')
		                                },
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    margin: '0',
		                                    id: 'sdOrderItemsPanelNewSDOrderPage',
		                                    disableSelection: true,
		                                    parentController: myController,
                                            owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/demand_requirement.png"  />';
		                                            },
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'matnr',
		                                            text: Locale.getMsg('material'),
		                                            flex: 0.5,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                            	return record.get('matnr');
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            text: Locale.getMsg('quantityUnit'),
		                                            dataIndex: 'zmeng',
		                                            flex: 0.5,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
		                                        		return amount + ' ' + record.get('zieme');
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            text: Locale.getMsg('posText'),
		                                            dataIndex: 'arktx',
		                                            flex: 1,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return record.get('arktx');
			                                        }
		                                        }
		                                    ],*/
			                                listeners: {
			                            		cellcontextmenu: myController.onCellContextMenu,
			                            		scope: myController
			                            	}
		                                },
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    height: 30
		                                }
		                            ]
		                        }
				            ]
				        },
				        {
				            xtype: 'component',
				            maxWidth: 15,
				            minWidth: 15
				        }
				    ]    
			    }
		    ];
	    
			this.add(items);
	    } catch (ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNewSDOrderPage', ex);
		}
	},

	getPageTitle: function() {
		var retval = '';
	
    	try {
    		retval = Locale.getMsg('newMaterialOrder');
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNewSDOrderPage', ex);
    	}
    	
    	return retval;
	},

	//protected
	//@override
	updatePageContent: function() {
		try {
			this.transferModelStateIntoView();
			this.resetSDOrderItemInputFields();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNewSDOrderPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var sdOrder = this.getViewModel().get('sdOrder');
		
			//ordering number and date
			Ext.getCmp('NewSDOrderPageSDOrderNumberTextField').setValue(sdOrder.get('bstnk'));
			Ext.getCmp('NewSDOrderPageRequestedDeliveryDateField').setValue(sdOrder.get('vdatu'));
			
			//delivery address data
			Ext.getCmp('NewSDOrderPageTitleTextField').setValue(sdOrder.get('formOfAddress'));
			Ext.getCmp('NewSDOrderPageNameTextField').setValue(sdOrder.get('name'));
			Ext.getCmp('NewSDOrderPageName2TextField').setValue(sdOrder.get('name2'));
			Ext.getCmp('NewSDOrderPageStreetTextField').setValue(sdOrder.get('street'));
			Ext.getCmp('NewSDOrderPageHouseNumTextField').setValue(sdOrder.get('houseNo'));
			Ext.getCmp('NewSDOrderPageZipCodeTextField').setValue(sdOrder.get('postalCode1'));
			Ext.getCmp('NewSDOrderPageCityTextField').setValue(sdOrder.get('city'));
			Ext.getCmp('NewSDOrderPageCountryTextField').setValue(sdOrder.get('country'));
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('postalCode1'))) {
				Ext.getCmp('differentDeliveryAddressCheckBox').setValue(true);
			} else {
				Ext.getCmp('differentDeliveryAddressCheckBox').setValue(false);
			}
			
			var sdOrderItemsGridPanel = Ext.getCmp('sdOrderItemsPanelNewSDOrderPage');
			sdOrderItemsGridPanel.setStore(sdOrder.get('items'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseNewSDOrderPage', ex);
		}
	},
	
	showDifferentDeliveryAddressSection: function() {
		try {
			Ext.getCmp('sdOrderDeliveryAdressContainer').show();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showDifferentDeliveryAddressSection of BaseNewSDOrderPage', ex);
		}
	},
	
	hideDifferentDeliveryAddressSection: function() {
		try {
			Ext.getCmp('sdOrderDeliveryAdressContainer').hide();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideDifferentDeliveryAddressSection of BaseNewSDOrderPage', ex);
		}
	},
	
	resetDifferentDeliveryAddressInputFields: function() {
		try {
			Ext.getCmp('NewSDOrderPageTitleTextField').setValue('');
			Ext.getCmp('NewSDOrderPageNameTextField').setValue('');
			Ext.getCmp('NewSDOrderPageName2TextField').setValue('');
			Ext.getCmp('NewSDOrderPageStreetTextField').setValue('');
			Ext.getCmp('NewSDOrderPageHouseNumTextField').setValue('');
			Ext.getCmp('NewSDOrderPageZipCodeTextField').setValue('');
			Ext.getCmp('NewSDOrderPageCityTextField').setValue('');
			Ext.getCmp('NewSDOrderPageCountryTextField').setValue('');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetDifferentDeliveryAddressInputFields of BaseNewSDOrderPage', ex);
		}
	},
	
	//get current values from screen into model
	transferViewStateIntoModel: function() {
		try {
			var sdOrder = this.getViewModel().get('sdOrder');
			
			//ordering number and date
			sdOrder.set('bstnk', Ext.getCmp('NewSDOrderPageSDOrderNumberTextField').getValue());
			sdOrder.set('vdatu', Ext.getCmp('NewSDOrderPageRequestedDeliveryDateField').getValue());
			
			//delivery address data
			sdOrder.set('formOfAddress', Ext.getCmp('NewSDOrderPageTitleTextField').getValue());
			sdOrder.set('name', Ext.getCmp('NewSDOrderPageNameTextField').getValue());
			sdOrder.set('name2', Ext.getCmp('NewSDOrderPageName2TextField').getValue());
			sdOrder.set('street', Ext.getCmp('NewSDOrderPageStreetTextField').getValue());
			sdOrder.set('houseNo', Ext.getCmp('NewSDOrderPageHouseNumTextField').getValue());
			sdOrder.set('postalCode1', Ext.getCmp('NewSDOrderPageZipCodeTextField').getValue());
			sdOrder.set('city', Ext.getCmp('NewSDOrderPageCityTextField').getValue());
			sdOrder.set('country', Ext.getCmp('NewSDOrderPageCountryTextField').getValue());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseNewSDOrderPage', ex);
		}
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			retval = this.getCurrentInputValuesForOrderItem();
			
			//ordering number and date
			retval.bstnk = Ext.getCmp('NewSDOrderPageSDOrderNumberTextField').getValue();
			retval.vdatu = Ext.getCmp('NewSDOrderPageRequestedDeliveryDateField').getValue();
			
			//delivery address data
			retval.useDifferentDeliveryAddress = Ext.getCmp('differentDeliveryAddressCheckBox').getValue();
			
			retval.title = Ext.getCmp('NewSDOrderPageTitleTextField').getValue();
			retval.name = Ext.getCmp('NewSDOrderPageNameTextField').getValue();
			retval.name2 = Ext.getCmp('NewSDOrderPageName2TextField').getValue();
			retval.street = Ext.getCmp('NewSDOrderPageStreetTextField').getValue();
			retval.houseNo = Ext.getCmp('NewSDOrderPageHouseNumTextField').getValue();
			retval.postalCode = Ext.getCmp('NewSDOrderPageZipCodeTextField').getValue();
			retval.city = Ext.getCmp('NewSDOrderPageCityTextField').getValue();
			retval.country = Ext.getCmp('NewSDOrderPageCountryTextField').getValue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseNewSDOrderPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	//get current values for the current on editing order sd item from screen
	getCurrentInputValuesForOrderItem: function() {
		var retval = null;
	
		try {
			retval = {
				matnr: Ext.getCmp('NewSDOrderPageMaterialNrTextField').getValue(),
				amount: Ext.getCmp('NewSDOrderPagePositionAmountTextField').getValue(),
				unit: Ext.getCmp('NewSDOrderPagePositionUnitTextField').getValue(),
				remark: Ext.getCmp('NewSDOrderPagePositionRemarkTextField').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseNewSDOrderPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	//resets all inputs fields for sd order item capturing
	resetSDOrderItemInputFields: function() {
		var retval = null;
	
		try {
			Ext.getCmp('NewSDOrderPageMaterialNrTextField').setValue('');
			Ext.getCmp('NewSDOrderPagePositionAmountTextField').setValue(1);
			Ext.getCmp('NewSDOrderPagePositionUnitTextField').setValue('ST');
			Ext.getCmp('NewSDOrderPagePositionRemarkTextField').setValue('');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetSDOrderItemInputFields of BaseNewSDOrderPage', ex);
			retval = null;
		}
		
		return retval;
	}
});