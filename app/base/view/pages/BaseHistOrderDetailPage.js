Ext.define('AssetManagement.base.view.pages.BaseHistOrderDetailPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.HistOrderDetailPageViewModel',
        'AssetManagement.customer.controller.pages.HistOrderDetailPageController',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.utils.DateTimeUtils'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null)
                {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.HistOrderDetailPage');
                }
            } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseHistOrderDetailPage', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'HistOrderDetailPageModel'
    },
    
    controller: 'HistOrderDetailPageController',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   
    initialize: function() {
    	try {
    		   	
	       var items = [
	        {
	            xtype: 'component',
	            maxWidth: 10,
	            minWidth: 10
	        },
	        {
	            xtype: 'container',
	            flex: 1,
	            layout: {
	                type: 'vbox',
	                align: 'stretch'
	            },
	            items: [
	                {
	                    xtype: 'component',
	                    height: 20
	                },
	                {
	                    xtype: 'container',
	                    id: 'upperCont',
	                    layout: {
	                        type: 'vbox',
	                        align: 'stretch'
	                    },
	                    items: [
	                        {
	                                xtype: 'container',
	                                flex: 1,
	                                layout: {
	                                type: 'hbox',
	                                align: 'stretch'
	                        },
	                        items: [
	                            {
	                                xtype: 'component',
	                                minWidth: 6,
	                                maxWidth: 6
	                            },
	                            {
	                                xtype: 'label',
	                                flex: 1,
	                                cls: 'oxHeaderLabel',
	                                text: Locale.getMsg('generalData')
	                            }
	                        ]
	                    },
	                        {
	                            xtype: 'component',
	                            height: 20
	                        },
	                        {
	                            xtype: 'container',
	                            layout: {
	                                type: 'hbox'                               
	                            },
	                            items: [
	                                {
	                                    xtype: 'container',
	                                    flex: 1,
	                                    id: 'leftGridHistOrder'
	                                },	                                
	                                {
	                                    xtype: 'container',
	                                    flex: 1,
	                                    padding: '0 0 0 20',
	                                    id: 'rightGridHistOrder',
	                                    plugins: 'responsive',
                                        responsiveConfig: {
                                    	'width <= 800': {
                                    		visible: false
                                    	},
                                    	'width > 800': {
                                    		visible: true
                                    	}
                                	}
	                                }
	                            ]
	                        }
	                    ]
	                },
	                {
	                	xtype: 'container',
	                    margin: '40 0 0 0',
	                    id: 'histOrderBottomGrid',                                                      
	                    plugins: 'responsive',
	                    responsiveConfig: {
	                    	'width <= 800': {
	                    		visible: true
	                        },
	                       'width > 800': {
	                        	visible: false
	                        }
	                   	}
	                }
	            ]
	        },
	        {
	            xtype: 'component',
	            maxWidth: 10,
	            minWidth: 10
	        }	        
	     ];
	    
	        this.add(items);
	    
	        var leftGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
				   this._orderRow = leftGrid.addRow(Locale.getMsg('order'), 'label', true);
				   this._orderTypeRow = leftGrid.addRow(Locale.getMsg('orderType'), 'label', true);
				   this._activityTypeRow = leftGrid.addRow(Locale.getMsg('timeConfs_Ilart'), 'label', true);
				   this._shorttextRow = leftGrid.addRow(Locale.getMsg('shorttext'), 'label', true);
				   this._funcLocRow = leftGrid.addRow(Locale.getMsg('funcLoc'), 'label', false);
				
		   // The rightGrid will be shown if the page width is over 800.		
	       var rightGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
	               this._notifRow = rightGrid.addRow(Locale.getMsg('notif'), 'label', true);
				   this._equiRow = rightGrid.addRow(Locale.getMsg('equipment'), 'label', true);
			       this._startRow = rightGrid.addRow(Locale.getMsg('bscStart'), 'label', true);
				   this._endRow = rightGrid.addRow(Locale.getMsg('basicFin'), 'label', false);
				
		   // The bottonGrid will be hidden if the page width is under 800.		
		   var bottomGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
	              this._notifRowBottom = bottomGrid.addRow(Locale.getMsg('notif'), 'label', true);
				  this._equiRowBottom = bottomGrid.addRow(Locale.getMsg('equipment'), 'label', true);
				  this._startRowBottom = bottomGrid.addRow(Locale.getMsg('bscStart'), 'label', true);
				  this._endRowBottom = bottomGrid.addRow(Locale.getMsg('basicFin'), 'label', false);
						
			      this.queryById('leftGridHistOrder').add(leftGrid);	        
			      this.queryById('rightGridHistOrder').add(rightGrid);
			      this.queryById('histOrderBottomGrid').add(bottomGrid);
			    
	    } catch (ex) {
			 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseHistOrderDetailPage', ex);
		}		    		    
    },
    
    getPageTitle: function() {
	    var title = '';
	    
	    try {
	        title = Locale.getMsg('order');
    	
    	    var histOrder = this.getViewModel().get('histOrder');
    	
    	    if(histOrder) {
    		   title += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(histOrder.get('aufnr'));
    	    }
        } catch (ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseHistOrderDetailPage', ex);
		}
        
    	return title;
	},
    
	//protected
	//@override
	updatePageContent: function() {
		try {
			this.fillMainDataSection();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseHistOrderDetailPage', ex);
		}
	},

	fillMainDataSection: function() {
		try {  	
			var histOrder = this.getViewModel().get('histOrder');	 
			var tpl = this.getViewModel().get('tpl');
			var equi = this.getViewModel().get('equi');
			
			/* Left Grid */
			if(histOrder.get('aufnr')) {
				this._orderRow.show();
				this._orderRow.setContentString(histOrder.get('aufnr'));			   			  
		   } else {
			   this._orderRow.hide();
		   }		
		
			if(histOrder.get('ktext')) {
				this._shorttextRow.show();
				this._shorttextRow.setContentString(histOrder.get('ktext'));			   			  
		   } else {
			   this._shorttextRow.hide();
		   }		
			if(tpl)
				this._funcLocRow.setContentString(tpl.getDisplayIdentification() + ' ' + tpl.get('pltxt'));
			
			if(histOrder.get('auart')) {
				this._orderTypeRow.show();
				this._orderTypeRow.setContentString(histOrder.get('auart'));			   			  
		   } else {
			   this._orderTypeRow.hide();
		   }
			
		   if(histOrder.get('ilart')) {
			   this._activityTypeRow.show();
			   this._activityTypeRow.setContentString(histOrder.get('ilart'));			   			  
		   } else {
			   this._activityTypeRow.hide();
		   }
	
			
			/* Right Grid */
	       if(histOrder.get('qmnum')) {
	    	   this._notifRow.show();
	    	   this._notifRow.setContentString(histOrder.get('ilart'));			   			  
		   } else {
			   this._notifRow.hide();			   
		   }
		
		   if(equi)
		   {
			   if(equi.get('equnr') || equi.get('eqktx')) {
				   this._equiRow.show();
		    	   this._equiRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + ' ' + equi.get('eqktx') );				   			  
			   } else {
				   this._equiRow.hide();			   
			   }   
		   }else {
			   this._equiRow.hide();			   
		   }   
		   
		   if(histOrder.get('gltrp')) {
			   this._startRow.show();   
	    	   this._startRow.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histOrder.get('gltrp')));					   			  
		   } else {
			   this._startRow.hide();			   
		   }
			
		   if(histOrder.get('gstrp')) {
			   this._endRow.show();
	    	   this._endRow.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histOrder.get('gstrp')));					   			  
		   } else {
			   this._endRow.hide();			   
		   }		
			
			/* Bottom Grid */
			if(histOrder.get('qmnum')) {
	    	   this._notifRowBottom.show();
	    	   this._notifRowBottom.setContentString(histOrder.get('ilart'));			   			  
		   } else {
			   this._notifRowBottom.hide();			   
		   }
		
		    if(equi)
		   {
			   if(equi.get('equnr') || equi.get('eqktx')) {
				   this._equiRow.show();
		    	   this._equiRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + ' ' + equi.get('eqktx') );				   			  
			   } else {
				   this._equiRow.hide();			   
			   }   
		   }else {
			   this._equiRow.hide();			   
		   }   
			
		   if(histOrder.get('gltrp')) {
			   this._startRowBottom.show();
	    	   this._startRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histOrder.get('gltrp')));					   			  
		   } else {
			   this._startRowBottom.hide();			   
		   }
			
		   if(histOrder.get('gstrp')) {
			   this._endRowBottom.show();
	    	   this._endRowBottom.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(histOrder.get('gstrp')));					   			  
		   } else {
			   this._endRowBottom.hide();			   
		   }		
		} catch (ex) {
			 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseHistOrderDetailPage', ex);
		}
	}
});