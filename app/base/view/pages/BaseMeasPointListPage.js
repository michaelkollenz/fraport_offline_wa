// JavaScript source code
Ext.define('AssetManagement.base.view.pages.BaseMeasPointListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.MeasPointListPageViewModel',
        'AssetManagement.customer.controller.pages.MeasPointListPageController',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.MeasPointListPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMeasPointListPage', ex);
            }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.MeasPointListPageRendererMixin'
    },

    controller: 'MeasPointListPageController',

    viewModel: {
        type: 'MeasPointListPageModel'
    },

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    //protected
    //@override
    buildUserInterface: function () {
        try {
            //var myController = this.getController();
            //var me = this;
            var items = [
			     {
			         xtype: 'container',
			         flex: 1,
			         frame: true,
			         layout: {
			             type: 'hbox',
			             align: 'stretch'
			         },
			         items: [
			            {
			                xtype: 'component',
			                maxWidth: 10,
			                minWidth: 10
			            },
			            {
			                xtype: 'container',
			                flex: 5,
			                layout: {
			                    type: 'vbox',
			                    align: 'stretch'
			                },
			                items: [
			                    {
			                        xtype: 'component',
			                        height: 10
			                    },
			                    {
			                        xtype: 'container',
			                        height: 30,
			                        id: 'searchMeasPointContainer',
			                        layout: {
			                            type: 'hbox',
			                            align: 'stretch'

			                        },
			                        items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldMeasPoint',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,
		                                    listeners: {
		                                        change: {
		                                            fn: 'onMeasPointListPageSearchFieldChange'
		                                        }
		                                    }
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
		                                    id: 'measPointListPageFilterCriteriaCombobox',
		                                    editable: false,
		                                    store: [Locale.getMsg('measPoint'), Locale.getMsg('shorttext'), Locale.getMsg('equipment'),
		                                            Locale.getMsg('funcLoc'), Locale.getMsg('sortField')],
		                                    value: Locale.getMsg('measPoint'),
		                                    listeners: {
		                                        select: 'onMeasPointListPageSearchFieldChange'
		                                    }
		                                }
			                        ]
			                    },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
	                            {
	                                xtype: 'container',
	                                id: 'measPointGridContainer',
	                                flex: 1,
	                                layout: {
	                                    type: 'hbox',
	                                    align: 'stretch'
	                                },
	                                items: [
			                           this.getNewMeasPointGridPanel()
	                                ]
	                            },
			                    {
			                        xtype: 'component',
			                        height: 10
			                    }
			                ]
			            },
			            {
			                xtype: 'component',
			                width: 10
			            }
			         ]
			     }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMeasPointListPage', ex);
        }
    },

    //private
    renewMeasPointGridPanel: function () {
        var retval = null;

        try {
            var gridContainer = Ext.getCmp('measPointGridContainer');

            if (gridContainer)
                gridContainer.removeAll(true);

            retval = this.getNewMeasPointGridPanel();

            if (gridContainer && retval) {
                gridContainer.add(retval);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewMeasPointGridPanel of BaseMeasPointListPage', ex);
            retval = null;
        }

        return retval;
    },

    //private
    getNewMeasPointGridPanel: function () {
        var retval = null;

        try {
            var me = this;
            var myController = this.getController();

            retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
                id: 'measPointGridPanel',
                hideContextMenuColumn: true,
                useLoadingIndicator: true,
                loadingText: Locale.getMsg('loadingMeasPoints'),
                emptyText: Locale.getMsg('noMeasPoints'),
                flex: 1,
                scrollable: 'vertical',
                parentController: myController,
                owner: me,
               /* columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
	                        var imageSrc = 'resources/icons/measpoints.png';

	                        return '<img src="' + imageSrc + '"/>';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,
	                    enableColumnHide: false,
	                    align: 'center'
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
	                        var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');
	                        var measpointDesc = record.get('pttxt');
	                        return measpoint + '<p>' + measpointDesc + '</p>' + '<br /><br />';
	                    },
	                    enableColumnHide: false,
	                    dataIndex: '',
	                    text: Locale.getMsg('measPoint')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
	                        var objNo = '';
	                        var objTxt = ''; 
	                        if(record.get('equipment'))
	                        {
	                            objNo = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equipment').get('equnr'), '0');
	                            objTxt = record.get('equipment').get('eqktx'); 
	                        }
	                        else if(record.get('funcLoc'))
	                        {
	                            objNo = record.get('funcLoc').get('tplnr');
	                            objTxt = record.get('funcLoc').get('pltxt');
	                        }

	                        return objNo + '<p>' + objTxt + '</p>' + '<br /><br />';
	                    },
	                    enableColumnHide: false,
	                    dataIndex: '',
	                    text: Locale.getMsg('techObject')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
	                        return record.get('psort'); 
	                    },
	                    enableColumnHide: false,
	                    dataIndex: '',
	                    text: Locale.getMsg('measIndicator')
	                },
                    {
                         xtype: 'gridcolumn',
                         flex: 1,
                         renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                             return record.get('lastRec') + ' ' + record.get('lastUnit'); 
                         },
                         enableColumnHide: false,
                         dataIndex: '',
                         text: Locale.getMsg('lastMeasValue')
                    }
                ],*/
                listeners: {
                    cellclick: myController.onMeasPointSelected,
                    cellcontextmenu: myController.onCellContextMenu,
                    scope: myController
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewMeasPointGridPanel of BaseMeasPointListPage', ex);
        }

        return retval;
    },

    //protected
    //@override 
    getPageTitle: function () {
        var retval = '';

        try {
            var title = Locale.getMsg('measPoints');

            var measPoints = this.getViewModel().get('measPointStore');

            if (measPoints) {
                title += " (" + measPoints.getCount() + ")";
            }

            retval = title;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMeasPointListPage', ex);
        }

        return retval;
    },

    //protected
    //@override    
    updatePageContent: function () {
        try {
            var measPointStore = this.getViewModel().get('measPointStore');

            //workaround for chrome43+ & ExtJS 6.0.0 Bug
            if (AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
                this.renewMeasPointGridPanel();

            if (this.getViewModel().get('constrainedMeaspoints')) {
                measPointStore = this.getViewModel().get('constrainedMeaspoints');
            }

            var measPointGridPanel = Ext.getCmp('measPointGridPanel');

            measPointGridPanel.setStore(measPointStore);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseMeasPointListPage', ex);
        }
    },

    //public
    //@override  
    resetViewState: function () {
        this.callParent();

        try {
            Ext.getCmp('measPointGridPanel').reset();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseMeasPointListPage', ex);
        }
    },

   

    //public
    getSearchValue: function () {
        try {
            var measPointToSearch = Ext.create('AssetManagement.customer.model.bo.MeasurementPoint', {});
            var currentFilterCriterion = Ext.getCmp('measPointListPageFilterCriteriaCombobox').getValue();

            if (currentFilterCriterion === Locale.getMsg('measPoint')){
                measPointToSearch.set('point', Ext.getCmp('searchFieldMeasPoint').getValue())
            }
            else if (currentFilterCriterion === Locale.getMsg('shorttext'))
                measPointToSearch.set('pttxt', Ext.getCmp('searchFieldMeasPoint').getValue());
            else if (currentFilterCriterion === Locale.getMsg('equipment')){
                var equipment = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
                equipment.set('equnr', Ext.getCmp('searchFieldMeasPoint').getValue());
                equipment.set('eqktx', Ext.getCmp('searchFieldMeasPoint').getValue());
                measPointToSearch.set('equipment', equipment);
            }
            else if (currentFilterCriterion === Locale.getMsg('funcLoc')) {
                var funcLoc = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
                funcLoc.set('pltxt', Ext.getCmp('searchFieldMeasPoint').getValue());
                funcLoc.set('tplnr', Ext.getCmp('searchFieldMeasPoint').getValue());
                measPointToSearch.set('funcLoc', funcLoc);
            } else if (currentFilterCriterion === Locale.getMsg('sortField')) {
                var equipment = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
                equipment.set('eqfnr', Ext.getCmp('searchFieldMeasPoint').getValue());
                measPointToSearch.set('equipment', equipment);
            }
            this.getViewModel().set('searchMeasPoint', measPointToSearch);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseMeasPointListPage', ex);
        }
    },

    //public
    getFilterValue: function () {
        var retval = '';

        try {
            retval = Ext.getCmp('searchFieldMeasPoint').getValue();

            if (!retval)
                retval = '';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseMeasPointListPage', ex);
        }

        return retval;
    },
    //public
    setFilterValue: function (value) {
        try {
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
                value = '';

            retval = Ext.getCmp('searchFieldMeasPoint').setValue(value);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseMeasPointListPage', ex);
        }
    }
});