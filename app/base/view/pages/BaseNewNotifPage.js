Ext.define('AssetManagement.base.view.pages.BaseNewNotifPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.NewNotifPageViewModel',
        'AssetManagement.customer.controller.pages.NewNotifPageController',
        'Ext.container.Container',
        'AssetManagement.customer.view.utils.OxTextField',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date'
    ],

    inheritableStatics: {
    	_instance: null,

        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.NewNotifPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNewNotifPage', ex);
		    }

            return this._instance;
        }
    },

    viewModel: {
        type: 'NewNotifPageModel'
    },

    controller: 'NewNotifPageController',

    buildUserInterface: function() {
        try {
        	var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                            xtype: 'oxcombobox',
		                            flex: 1,
		                            id: 'newNotifTypeComboField',
							        labelWidth: 125,
		                            fieldLabel: Locale.getMsg('notifType'),
		                            queryMode: 'local',
		                            displayField: 'text',
		                            valueField: 'value',
		                            allowBlank: false,
		                            editable: false,
		                            listeners: {
		                            	change: 'onNotifTypeSelected',
		                            	blur: 'onNotifTypeBlur'
		                        	}
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'newNotifText_TextField',
		                            fieldLabel: Locale.getMsg('shorttext'),
							        labelWidth: 125,
							        maxLength: 40
		                        },
                                {
                                    xtype: 'container',
                                    id: 'codificationSection',
                                    flex: 1,
                                    margin: '5 0 0 0',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'oxcombobox',
                                            id: 'newNotifCodificationGroupComboBox',
                                            fieldLabel: Locale.getMsg('codification'),
                                            margin: '0 10 0 0',
                                            labelWidth: 125,
                                            queryMode: 'local',
                                            displayField: 'text',
                                            valueField: 'value',
                                            editable: false,
                                            flex: 1,
                                            listeners: {
                                                change: 'onNotifCodificationGroupChange'
                                            }
                                        },
                                        {
                                            xtype: 'oxcombobox',
                                            id: 'newNotifCodificationCodeComboBox',
                                            margin: '0 0 0 0',
                                            queryMode: 'local',
                                            displayField: 'text',
                                            valueField: 'value',
                                            editable: false,
                                            flex: 1,
                                            listeners: {
                                                change: 'onNotifCodificationChange'
                                            }
                                        }
                                    ]
                                },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        // =========== FuncLoc ==============
		                        {
									xtype: 'container',
									flex: 1,
									layout: {
									    type: 'hbox',
									    align: 'middle'
									},
									items: [
									    {
									    	xtype: 'oxtextfield',
									    	flex: 1,
				                            id: 'newNotifFuncLocTextField',
				                            readOnly: true,
									        fieldLabel: Locale.getMsg('funcLoc'),
									        labelWidth: 125
									    },
									    {
									        xtype: 'component',
									        width: 10
									    },
									    {
									    	xtype: 'container',
									        width: 62,
									        height: 40,
									        top: 20,
									        items: [
									            {
									                xtype: 'button',
									                id: 'newNotifSearchButtonFunclocChange',
									                bind: {
									                    html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
									            	},
									            	padding: '3 0 0 10',
									                listeners: {
									                	click: 'onSelectFuncLocClick'
									                }

									            }
									        ]
									    }
									]
								},
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        // =========== Equi ==============
		                        {
									xtype: 'container',
									flex: 1,
									layout: {
									    type: 'hbox',
									    align: 'middle'
									},
									items: [
									    {
									    	xtype: 'oxtextfield',
									    	flex: 1,
				                            id: 'newNotifEquiTextField',
				                            readOnly: true,
									        fieldLabel: Locale.getMsg('equipment'),
									        labelWidth: 125
									    },
									    {
									        xtype: 'component',
									        width: 10
									    },
									    {
									    	xtype: 'container',
									        width: 62,
									        height: 40,
									        top: 20,
									        items: [
									            {
									                xtype: 'button',
									                id: 'newNotifSearchButtonEquiChange',
									                bind: {
									                    html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
									            	},
									            	padding: '3 0 0 10',
									                listeners: {
									                	click: 'onSelectEquiClick'
									                }

									            }
									        ]
									    }
									]
								},
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        // =========== Partner ==============
		                        {
		                            xtype: 'container',
		                            id: 'containerNewNotifPartners',
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
				                        {
											xtype: 'container',
				                            id: 'containerNewNotifCustomer',
											flex: 1,
											layout: {
											    type: 'hbox',
											    align: 'middle'
											},
											items: [
											    {
											    	xtype: 'oxtextfield',
											    	flex: 1,
											        id: 'newNotifCustomerTextField',
											        readOnly: true,
						                            fieldLabel: Locale.getMsg('employer'),
											        labelWidth: 125
											    },
											    {
											        xtype: 'component',
											        width: 10
											    },
											    {
											    	xtype: 'container',
											        width: 62,
											        height: 40,
											        top: 20,
											        items: [
											            {
											                xtype: 'button',
											                id: 'newNotifSearchButtonCustomerChange',
											                bind: {
											                    html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
											            	},
											            	padding: '3 0 0 10',
											                listeners: {
											                	click: 'onSelectCustomerClick'
											                }

											            }
											        ]
											    }
											]
				                        },
				                        {
											xtype: 'container',
				                            id: 'containerNewNotifAp',
											flex: 1,
											layout: {
											    type: 'hbox',
											    align: 'middle'
											},
											items: [
											    {
											    	xtype: 'oxtextfield',
											    	flex: 1,
											        id: 'newNotifApTextField',
											        readOnly: true,
						                            fieldLabel: Locale.getMsg('partnerAp'),
											        labelWidth: 125
											    },
											    {
											        xtype: 'component',
											        width: 10
											    },
											    {
											    	xtype: 'container',
											        width: 62,
											        height: 40,
											        top: 20,
											        items: [
											            {
											                xtype: 'button',
											                id: 'newNotifSearchButtonApChange',
											                bind: {
											                    html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
											            	},
											            	padding: '3 0 0 10',
											                listeners: {
											                	click: 'onSelectePartnerApClick'
											                }

											            }
											        ]
											    }
											]
				                        },
				                        {
											xtype: 'container',
				                            id: 'containerNewNotifZr',
											flex: 1,
											layout: {
											    type: 'hbox',
											    align: 'middle'
											},
											items: [
											    {
											    	xtype: 'oxtextfield',
											    	flex: 1,
											        id: 'newNotifZrTextField',
											        readOnly: true,
						                            fieldLabel: Locale.getMsg('partnerZr'),
											        labelWidth: 125
											    },
											    {
											        xtype: 'component',
											        width: 10
											    },
											    {
											    	xtype: 'container',
											        width: 62,
											        height: 40,
											        top: 20,
											        items: [
											            {
											                xtype: 'button',
											                id: 'newNotifSearchButtonZrChange',
											                bind: {
											                    html: '<div><img width="75%" src="resources/icons/search.png"></img></div>'
											            	},
											            	padding: '3 0 0 10',
											                listeners: {
											                	click: 'onSelectePartnerZrClick'
											                }
											            }
											        ]
											    }
											]
				                        }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'container',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'datefield',
		                                    flex: 0.5,
		                                    format: 'd.m.Y',
		                                    id: 'newNotifStartDateField',
		                                    maxLength: 10,
									        labelWidth: 125,
		                                    fieldLabel: Locale.getMsg('disruptionFrom')
		                                },
		                                {
		                                    xtype: 'component',
		                                    width: 10
		                                },
		                                {
		                                    xtype: 'timefield',
		                                    labelStyle: 'display: none;',
		                                    flex: 0.2,
		                                    format:'H:i',
		                                    id: 'newNotifStartTimeField',
		                                    maxLength: 5
		                                },
		                                {
		                                    xtype: 'component',
		                                    flex: 0.1
		                                },
		                                {
		                                    xtype: 'datefield',
		                                    flex: 0.5,
		                                    id: 'newNotifEndDateField',
		                                    format: 'd.m.Y',
		                                    fieldLabel: Locale.getMsg('disruptionTo'),
		                                    maxLength: 10
		                                },
		                                {
		                                    xtype: 'component',
		                                    width: 10
		                                },
		                                {
		                                    xtype: 'timefield',
		                                    labelStyle: 'display: none;',
		                                    flex: 0.2,
		                                    format:'H:i',
		                                    id: 'newNotifEndTimeField',
		                                    maxLength: 5
		                                }
		                            ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    minWidth: 10,
		                    maxWidth: 10
		                }
		            ]
		        }
	        ];

		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNewNotifPage', ex);
		}
	},

	getPageTitle: function() {
	    var retval = '';

	    try {
		    retval = Locale.getMsg('createNewNotif');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNewNotifPage', ex);
		}

		return retval;
	},

	//protected
	//@override
	updatePageContent: function() {
		try {
			this.initFields();
			this.fillNotifTypeDropdown();
			var isCodingActive = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('coding_active');
			Ext.getCmp('codificationSection').setVisible(isCodingActive);
			//this.fillBemotDropdown();
			//Ext.getCmp('auartComboBox').setDisabled(false);
			//Ext.getCmp('ilartComboBox').setDisabled(false);
			//Ext.getCmp('containerNewOrderPmActivityType').setVisible(false);
			//Ext.getCmp('containerNewOrderHeaderFields').setVisible(false);
			//Ext.getCmp('containerNewOrderFirstOperation').setVisible(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNewNotifPage', ex);
		}
	},

	/*
	 * =================================================================================
	 * ===================== Operations when initializing the page =====================
	 * =================================================================================
	 */
	/**
	 * initializes the form fields with values from the order object
	 */
	initFields: function(){
		try {
			this.clearAllFields();
			Ext.getCmp('newNotifText_TextField').setValue(this.getViewModel().get('notif').get('qmtxt'));
		    this.displayPartners();
		    if(this.getViewModel().get('notif').get('vondt') == null || this.getViewModel().get('notif').get('vondt') === undefined){
		    	Ext.getCmp('newNotifStartDateField').setValue(new Date());
		    	Ext.getCmp('newNotifStartTimeField').setValue(new Date());
		    } else {
		    	Ext.getCmp('newNotifStartDateField').setValue(this.getViewModel().get('notif').get('vondt'));
		    	Ext.getCmp('newNotifStartTimeField').setValue(this.getViewModel().get('notif').get('vondt'));
		    }
		    if(this.getViewModel().get('notif').get('bisdt') == null || this.getViewModel().get('notif').get('bisdt') === undefined){
		    	Ext.getCmp('newNotifEndDateField').setValue(new Date());
		    	Ext.getCmp('newNotifEndTimeField').setValue(new Date());
		    } else {
		    	Ext.getCmp('newNotifEndDateField').setValue(this.getViewModel().get('notif').get('bisdt'));
		    	Ext.getCmp('newNotifEndTimeField').setValue(this.getViewModel().get('notif').get('bisdt'));
		    }
			Ext.getCmp('newNotifFuncLocTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(this.getViewModel().get('notif').get('tplnr'), '0'));
			Ext.getCmp('newNotifEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(this.getViewModel().get('notif').get('equnr'), '0'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initFields of BaseNewNotifPage', ex);
		}
	},

	clearAllFields: function() {
		try {
    		Ext.getCmp('containerNewNotifPartners').setVisible(false);
			Ext.getCmp('containerNewNotifAp').setVisible(false);
			Ext.getCmp('containerNewNotifZr').setVisible(false);
			Ext.getCmp('newNotifTypeComboField').setDisabled(false);
			Ext.getCmp('newNotifText_TextField').setValue('');
			Ext.getCmp('newNotifFuncLocTextField').setValue('');
			Ext.getCmp('newNotifEquiTextField').setValue('');
			Ext.getCmp('newNotifCustomerTextField').setValue('');
			Ext.getCmp('newNotifApTextField').setValue('');
			Ext.getCmp('newNotifZrTextField').setValue('');
            Ext.getCmp('newNotifCodificationGroupComboBox').bindStore(
                Ext.create('Ext.data.Store', {
                    fields: ['text', 'value']
            }));
            Ext.getCmp('newNotifCodificationCodeComboBox').bindStore(
                Ext.create('Ext.data.Store', {
                    fields: ['text', 'value']
            }));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearAllFields of BaseNewNotifPage', ex);
		}
	},

    /**
     * Loads the notiftypes from the database and fills the newNotifTypeComboField
     */
    fillNotifTypeDropdown: function() {
    	try {
    		var notifTypeComboBox = Ext.getCmp('newNotifTypeComboField');
    		var notifTypes = this.getViewModel().get('notifTypes');
    		var setValueIndex = -1;

			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			notifTypes.each(function(notifType) {
				if(notifType.get('createAllowed')){
					store.add({ "text": notifType.get('qmart') + " " + notifType.get('qmartx'), "value": notifType.get('qmart')});
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('notif').Qmart) && notifType.get('qmart') == this.getViewModel().get('notif').Qmart)
						setValueIndex = store.length;
				}
			}, this);

			notifTypeComboBox.clearValue();
			notifTypeComboBox.bindStore(store);

			//set the value of the order auart
			if(setValueIndex > 0)
			{
				notifTypeComboBox.select(store.data.items[setValueIndex-1].data.value);
				notifTypeComboBox.fireEvent('select', notifTypeComboBox, store.data.items[setValueIndex-1].data);
			}
			//set default value if only one auart was found
			else if(store.data.length == 1){
				notifTypeComboBox.select(store.data.items[0].data.value);
				notifTypeComboBox.fireEvent('select', notifTypeComboBox, store.data.items[0].data);
			}
    	}
    	catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillNotifTypeDropdown of BaseNewNotifPage', ex);
    	}
    },

    /**
     * fills the custcodeGroup dropdown with data from viewmodel
     */
    fillCodeGroupDropdown: function () {
        try {
            var codeGroups = this.getViewModel().get('custCodeGroupsMap');

            var codeGroupsStore = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (codeGroups && codeGroups.getCount() > 0) {
                codeGroups.each(function (key, value, length) {
                    codeGroupsStore.add({ "text": key + ' ' + codeGroups.get(key).getAt(0).get('codegrkurztext'), "value": value});
                }, this);
            }

            var codeGroupsComboBox = Ext.getCmp('newNotifCodificationGroupComboBox');
            codeGroupsComboBox.value && codeGroupsComboBox.clearValue();
            codeGroupsComboBox.bindStore(codeGroupsStore);

        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside fillCodeGroupDropdown of BaseNewNotifPage', ex);
        }
    },

    /**
     * fills the custcode dropdown with given codes
     * @param {Ext.data.Store} codeStore - store of AssetManagement.customer.model.bo.CustCode
     */
    fillCodeDropdown: function (codeStore) {
        try {
            var dropDownCodeStore = Ext.create('Ext.data.Store', {
                fields: ['text', 'value']
            });

            if (codeStore && codeStore.getCount() > 0) {
                codeStore.each(function (code) {
                    dropDownCodeStore.add({ "text": code.get('code') + ' ' + code.get('kurztext'), "value": code});
                }, this);
            }

            var codeComboBox = Ext.getCmp('newNotifCodificationCodeComboBox');
            codeComboBox.value && codeComboBox.clearValue();
            codeComboBox.bindStore(dropDownCodeStore);

        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside fillCodeDropdown of BaseNewNotifPage', ex);
        }
    },

    /**
     * checks if and what partner lines should be displayed and changes their visibility
     */
    displayPartners: function() {
    	try {
    		Ext.getCmp('containerNewNotifPartners').setVisible(false);
    		var notifType = this.getViewModel().get('notifTypes').findRecord('qmart', this.getViewModel().get('notif').get('qmart'));
    		if(notifType != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifType.get('partnerTransfer'))) {
        		Ext.getCmp('containerNewNotifPartners').setVisible(true);
				Ext.getCmp('containerNewNotifCustomer').setVisible(true);
				if(this.getViewModel().get('customer')) {
					Ext.getCmp('containerNewNotifAp').setVisible(true);
					Ext.getCmp('containerNewNotifZr').setVisible(true);
				}
				else {
					Ext.getCmp('containerNewNotifAp').setVisible(false);
					Ext.getCmp('containerNewNotifZr').setVisible(false);
				}
    		}
    	}
    	catch(ex){
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside displayPartners of BaseNewNotifPage', ex);
    	}
     },

	/**
	 * called after equi or func loc has been selected through picker, updates the textfields with the selected data
	 */
	fillEquiFuncLocTextField: function(){
		try {
			var equi = this.getViewModel().get('equi');
			if(equi)
				Ext.getCmp('newNotifEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + ' ' + equi.get('eqktx'));
			else
				Ext.getCmp('newNotifEquiTextField').setValue('');
			var funcLoc = this.getViewModel().get('funcLoc');
			if(funcLoc)
				Ext.getCmp('newNotifFuncLocTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(funcLoc.getDisplayIdentification(), '0') + ' ' + funcLoc.get('pltxt'));
			else
				Ext.getCmp('newNotifFuncLocTextField').setValue('');
		}
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquiFuncLocTextField of BaseNewNotifPage', ex);
		}
	},

	/**
	 * called after a partner has been selected through picker, updates the textfields with the selected data
	 */
	fillCustomerTextField: function(){
		try {
			var customer = this.getViewModel().get('customer');
			if(customer) {
			    var custString = customer.get('name1');
				custString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('name2'))? ' ' +customer.get('name2'): '';
				custString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('postcode1'))? '    ' + customer.get('postcode1') + ' ' + customer.get('city1'): '';

				Ext.getCmp('newNotifCustomerTextField').setValue(custString.trim());
			}
			var partnerAp = this.getViewModel().get('partnerAp');
			if(partnerAp) {
			    var apString = partnerAp.get('name1');
			    apString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerAp.get('name2'))? ' ' +partnerAp.get('name2'): '';
			    apString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerAp.get('postcode1'))? '    ' + partnerAp.get('postcode1') + ' ' + partnerAp.get('city1'): '';

				Ext.getCmp('newNotifApTextField').setValue(apString.trim());
			}
			else
				Ext.getCmp('newNotifApTextField').setValue('');
			var partnerZr = this.getViewModel().get('partnerZr');
			if(partnerZr){
			    var zrString = partnerZr.get('name1');
			    zrString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerZr.get('name2'))? ' ' +partnerZr.get('name2'): '';
			    zrString += !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(partnerZr.get('postcode1'))? '    ' + partnerZr.get('postcode1') + ' ' + partnerZr.get('city1'): '';

				Ext.getCmp('newNotifZrTextField').setValue(zrString.trim());
			}
			else
				Ext.getCmp('newNotifZrTextField').setValue('');
			this.displayPartners();

		}
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillCustomerTextField of BaseNewNotifPage', ex);
		}
	},

	/**
	 * disables the notiftypecombobox if a notiftype is selected. usualy called after the notiftypecombobox looses focus
	 */
	disableNotifTypeComboBox: function() {
		try {
			if(this.getViewModel().get('notifType') != null || !(this.getViewModel().get('notifType') === undefined)) {
				Ext.getCmp('newNotifTypeComboField').setDisabled(true);
			}
		}
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillCustomerTextField of BaseNewNotifPage', ex);
		}
	},

	/*
	 * =================================================================================
	 * ================= operations to be performed on saving the notif ================
	 * =================================================================================
	 */

	/**
	 *  update the data in the model from the data set in the view fields
	 */
	updateModelData: function() {
		try {
			this.getViewModel().get('notif').set('qmtxt', Ext.getCmp('newNotifText_TextField').getValue());
			if(this.getViewModel().get('equi') != null && this.getViewModel().get('equi') != undefined) {
				this.getViewModel().get('notif').set('equipment', this.getViewModel().get('equi'));
				this.getViewModel().get('notif').set('equnr', this.getViewModel().get('equi').get('equnr'));
			}
			if(this.getViewModel().get('funcLoc') != null && this.getViewModel().get('funcLoc') != undefined){
				this.getViewModel().get('notif').set('funcLoc', this.getViewModel().get('funcLoc'));
				this.getViewModel().get('notif').set('tplnr', this.getViewModel().get('funcLoc').getDisplayIdentification());
			}
			var vondt = Ext.getCmp('newNotifStartDateField').getValue();
			vondt.setHours(Ext.getCmp('newNotifStartTimeField').getValue().getHours());
			vondt.setMinutes(Ext.getCmp('newNotifStartTimeField').getValue().getMinutes());
			var bisdt = Ext.getCmp('newNotifEndDateField').getValue();
			bisdt.setHours(Ext.getCmp('newNotifEndTimeField').getValue().getHours());
			bisdt.setMinutes(Ext.getCmp('newNotifEndTimeField').getValue().getMinutes());
			this.getViewModel().get('notif').set('vondt', vondt);
			this.getViewModel().get('notif').set('bisdt', bisdt);
		}
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateModelData of NewONotifPage', ex);
		}
	},

	/**
	 * Callback method for the save notif function in the controller
	 */
	saveNotifCallback: function(message){
		try {
			if(message.success == false)
				AssetManagement.customer.core.Core.getMainView().showNotification(message);
		}
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifCallback of BaseNewNotifPage', ex);
		}
	}
});