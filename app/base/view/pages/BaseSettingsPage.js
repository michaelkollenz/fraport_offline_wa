Ext.define('AssetManagement.base.view.pages.BaseSettingsPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',
  requires: [
    'AssetManagement.customer.controller.pages.SettingsPageController',
    'AssetManagement.customer.model.pagemodel.SettingsPageViewModel',
    'AssetManagement.customer.view.utils.OxComboBox',
    'AssetManagement.customer.view.utils.OxTextField',
    'Ext.button.Button',
    'Ext.container.Container',
    'Ext.layout.container.VBox'
  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.SettingsPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSettingsPage', ex);
      }
      return this._instance;
    }
  },

  viewModel:'settingspageviewmodel',

  controller: 'settingspagecontroller',

  config: {
    flex: 1,
    bodyCls: ''
  },

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {
      var labelWidth = 150;

      var items = [
        {
          xtype: 'container',
          flex: 1,
          overflowY: 'auto',
          layout: {
            type: 'vbox',
            align: 'center',
            pack: 'center'
          },
          items: [
            {
              xtype: 'container',
              layout: {
                type: 'vbox',
                align: 'center',
                pack: 'center'
              },
              itemId: 'generalSettings',
              margin: '0 0 10 0',
              items: [
                // {
                //   xtype: 'button',
                //   height: 31,
                //   margin: '10 0 0 0',
                //   width: 293,
                //   cls: 'oxLoginButton',
                //   responsiveConfig: {
                //     'width < 350': {
                //       width: 230
                //     },
                //     'width > 350': {
                //       width: 292
                //     }
                //   },
                //   text: Locale.getMsg('sendLogFiles'),
                //   handler: 'onSendLogFilesButtonClicked',
                //   plugins: [
                //     {
                //       ptype: 'responsive'
                //     }
                //   ]
                // },
                {
                  xtype: 'button',
                  height: 31,
                  margin: '10 0 0 0',
                  width: 293,
                  cls: 'oxLoginButton',
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  text: Locale.getMsg('doClientReset'),
                  handler: 'ondoClientResetButtonClicked',
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                }
              ]
            },
            {
              xtype: 'container',
              itemId: 'advancedSettings',
              items: [
                {
                  xtype: 'oxtextfield',
                  id: 'settingsPagesapClientIdTextfield',
                  maxLength: 3,
                  fieldLabel: Locale.getMsg('sapClient'),
                  labelWidth: labelWidth
                },
                {
                  xtype: 'oxtextfield',
                  id: 'settingsPageSapSystemIdTextField',
                  maxLength: 40,
                  fieldLabel: Locale.getMsg('sapSystem'),
                  labelWidth: labelWidth
                },
                {
                  xtype: 'oxcombobox',
                  fieldLabel: Locale.getMsg('protocol'),
                  id: 'settingsPageProtokollCombobox',
                  labelWidth: labelWidth,
                  displayField: 'text',
                  queryMode: 'local',
                  valueField: 'value',
                  store: {
                    data: [{
                      "text": "http",
                      "value": "http"
                    }, {
                      "text": "https",
                      "value": "https"
                    }]
                  }
                },
                {
                  xtype: 'oxtextfield',
                  id: 'settingsPageGatewayTextField',
                  maxLength: 40,
                  fieldLabel: Locale.getMsg('gateway'),
                  labelWidth: labelWidth
                },
                {
                  xtype: 'oxtextfield',
                  id: 'settingsPageGatewayPortTextfield',
                  maxLength: 40,
                  fieldLabel: Locale.getMsg('gatewayPort'),
                  labelWidth: labelWidth
                },
                {
                  xtype: 'oxtextfield',
                  id: 'settingsPageGatewayIdentTextfield',
                  maxLength: 40,
                  fieldLabel: Locale.getMsg('gatewayIdent'),
                  labelWidth: labelWidth
                },
                {
                  xtype: 'displayfield',
                  id: 'settingsPageGatewayInfoServiceCodePageTextField',
                  fieldLabel: Locale.getMsg('version'),
                  labelStyle: 'color: black',
                  value: AssetManagement.customer.core.Core.getVersion(),
                  labelWidth: labelWidth
                }
              ]
            },
            {
              xtype: 'container',
              layout: {
                type: 'vbox',
                pack: 'center'
              },
              items: [
                {
                  xtype: 'button',
                  height: 31,
                  margin: '10 10 0 0',
                  width: 293,
                  cls: 'oxLoginButton',
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  text: Locale.getMsg('save'),
                  handler: 'trySaveSettings',
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                }, {
                  xtype: 'button',
                  height: 31,
                  margin: '10 0 0 0',
                  width: 293,
                  cls: 'oxLoginButton',
                  responsiveConfig: {
                    'width < 350': {
                      width: 230
                    },
                    'width > 350': {
                      width: 292
                    }
                  },
                  text: Locale.getMsg('back'),
                  handler: 'onCancelClicked',
                  plugins: [
                    {
                      ptype: 'responsive'
                    }
                  ]
                }
              ]
            }
          ]
        }
      ];
      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSettingsPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';

    try {
      retval = Locale.getMsg('settings');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseSettingsPage', ex);
    }
    return retval;
  },

  //protected
  //@override
  updatePageContent: function () {
    try {
      this.getController().updateView();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseLoginPage', ex);
    }
  }
});