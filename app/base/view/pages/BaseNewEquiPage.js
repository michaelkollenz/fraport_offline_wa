Ext.define('AssetManagement.base.view.pages.BaseNewEquiPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.NewEquiPageViewModel',
        'AssetManagement.customer.controller.pages.NewEquiPageController',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.container.Container',
        'Ext.Component',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.NewEquiPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseNewEquiPage', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'NewEquiPageModel'
    },
    
    controller: 'NewEquiPageController',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
		    var items = [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
	                        {
	                            xtype: 'component',
	                            height: 30
	                        },
	                        {
	                            xtype: 'label',
	                            cls: 'oxHeaderLabel',
	                            text: Locale.getMsg('newEquiRegister')
	                        },
	                        {
			                    xtype: 'component',
			                    height: 20
			                },
	                        {
	                            xtype: 'oxtextfield',
	                            id: 'newEquiPageEquiDescTextField',
	                            maxLength: 40,
	                            maxWidth: 650,
	                            fieldLabel: Locale.getMsg('shorttext'),
	                            labelWidth: 150
	                        },
	                        {
	                            xtype: 'component',
	                            height: 10
	                        },
	                        {
	                            xtype: 'oxcombobox',
	                            maxWidth: 650,
	                            id: 'newEquiPageEquiTypeComboBox',
	                            fieldLabel: Locale.getMsg('equipmentType'),
	                            labelWidth: 150,
		                        displayField: 'text',
	                            queryMode: 'local',
	                            valueField: 'value'
	                        },
	                        {
	                        	
	                            xtype: 'container',
	                            maxWidth: 650,
	                            layout: {
	                                type: 'hbox',
	                                align: 'center'
	                            },
	                            items: [
	                                {
	                                	xtype: 'oxtextfield',
	                                	height: 20, 
	                                	flex: 1, 
	    	                            id: 'newEquiFuncLocTextField',
	    	                            maxLength: 40,
	    	                            fieldLabel: Locale.getMsg('funcLoc'),
	    	                            labelWidth: 150
	                                },
	                                {
	    	                            xtype: 'component',
	    	                            width: 10
	    	                        },
	                                {
	                                	xtype: 'container',
	                                    width: 40,
	                                    height: 40,
	                                    items: [
	                                        {
	                                            xtype: 'button',
	                                            bind: {
	                                                html: '<div><img width="100%" src="resources/icons/search.png"></img></div>'                                                               
	                                        	},
	                                            listeners: {
	                                            	click: 'onSelectSuperiorFuncLocClick'
	                                            }
	                                        }
	                                    ]
	                                }
	                           ]
	                        },
	                        {
	                            xtype: 'component',
	                            height: 5
	                        },
	                        {
	                        	
								xtype: 'container',
								maxWidth: 650,
								id: 'HeadEquiContainer',
								layout: {
								    type: 'hbox',
								    align: 'center'
								},
								items: [
								    {
								    	xtype: 'oxtextfield',
			                            flex: 1,
			                            id: 'newEquiHeadEquipmentTextField',
			                            maxLength: 18,
			                            fieldLabel: Locale.getMsg('superordEqui'),
			                            labelWidth: 150 
								    },
								    {
								        xtype: 'component',
								        width: 10
								    },
								    {
								    	xtype: 'container',
								        width: 40,
								        height: 40,
								        items: [
								            {
								                xtype: 'button',
								                bind: {
								                    html: '<div><img width="100%" src="resources/icons/search.png"></img></div>'                                                               
								            	},
								                listeners: {
								                	click: 'onSelectHeadEquiClick'
								                }								                
								            }								            
								        ]								        
								    }							    
								]
							},
	                        {
	                            xtype: 'component',
	                            height: 5
	                        },
							{
	                            xtype: 'oxtextfield',
	                            id: 'newEquiPageConstrTypeTextField',
	                            maxWidth: 650,
	                            maxLength: 18,
	                            fieldLabel: Locale.getMsg('constructionType'),
	                            labelWidth: 150
	                        },
	                        {
	                            xtype: 'component',
	                            height: 10
	                        },
	                        {
	                            xtype: 'oxnumberfield',
	                            id: 'newEquiPageConstrYrTextField',
	                            maxLength: 4,
	                            allowDecimals: false,
	                            maxWidth: 222,
	                            fieldLabel: Locale.getMsg('constrYr'),
	                            labelWidth: 150
	                        },
	                        {
	                            xtype: 'component',
	                            height: 10
	                        },
	                        {
	                            xtype: 'oxtextfield',
	                            id: 'newEquiPageConstrMthTextField',
		                        maxLength: 2,
	                            maxWidth: 185,
	                            fieldLabel: Locale.getMsg('constrMth'),
	                            labelWidth: 150
	                        },
	                        {
	                            xtype: 'component',
	                            height: 10
	                        },
	                        {
	                            xtype: 'oxtextfield',
	                            id: 'newEquiPageSerialNrTextField',
	                            maxWidth: 650,
	                            maxLength: 18,
	                            fieldLabel: Locale.getMsg('serialNumber'),
	                            labelWidth: 150
	                        },
	                        {
	                            xtype: 'component',
	                            height: 10
	                        }
	                        
	                    ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseNewEquiPage', ex);
    	}
   },
   
   getPageTitle: function() {
		var retval = '';
   
		try {
			var retval = Locale.getMsg('newEqui');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseNewEquiPage', ex);
		}

		return retval;
	},
	
	//protected
	//@override
	updatePageContent: function() {
		try {
			this.manageHeadEquiInputVisibility();
		
			this.fillEquiTypeDropDownBox();
			
			this.transferModelStateIntoView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseNewEquiPage', ex);
		}
	},
	
	fillEquiTypeDropDownBox: function() {
		try {
			//filling equi types combobox
			var store = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
		
			this.getViewModel().get('equipmentTypes').each(function(eqTyp) {
				store.add({ 'text': eqTyp.get('txtbz'), 'value': eqTyp.get('eqtyp')});
			}, this);
			
			var comboBox = Ext.getCmp('newEquiPageEquiTypeComboBox');

			comboBox.clearValue();
			comboBox.setStore(store);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquiTypeDropDownBox of BaseNewEquiPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var equipment = this.getViewModel().get('equipment');
			
			Ext.getCmp('newEquiPageEquiDescTextField').setValue(equipment.get('eqktx'));
			Ext.getCmp('newEquiPageEquiTypeComboBox').setValue(equipment.get('eqtyp'));
			
			Ext.getCmp('newEquiFuncLocTextField').setValue(equipment.get('tplnr')); 
			Ext.getCmp('newEquiHeadEquipmentTextField').setValue(equipment.get('hequi')); 
			
			Ext.getCmp('newEquiPageConstrMthTextField').setValue(equipment.get('baumm')); 
			Ext.getCmp('newEquiPageConstrYrTextField').setValue(equipment.get('baujj')); 
			Ext.getCmp('newEquiPageSerialNrTextField').setValue(equipment.get('sernr'));
			Ext.getCmp('newEquiPageConstrTypeTextField').setValue(equipment.get('submt')); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseNewEquiPage', ex);
		}
	},
	
	//get current values from screen to save component
	transferViewStateIntoModel: function() {
		try {
			var equipment = this.getViewModel().get('equipment');
			
			var equiType = this.getCurrentEquiTypeFromSpinner();
			
			equipment.set('eqktx', Ext.getCmp('newEquiPageEquiDescTextField').getValue());
			equipment.set('eqtyp', equiType);
			equipment.set('tplnr', Ext.getCmp('newEquiFuncLocTextField').getValue());
			equipment.set('hequi', Ext.getCmp('newEquiHeadEquipmentTextField').getValue());
			
			equipment.set('submt', Ext.getCmp('newEquiPageConstrTypeTextField').getValue());
			equipment.set('baujj', Ext.getCmp('newEquiPageConstrYrTextField').getValue());
			
			var constrMonth = AssetManagement.customer.utils.StringUtils.padLeft(Ext.getCmp('newEquiPageConstrMthTextField').getValue(), '0', 2);
			equipment.set('baumm', constrMonth);
			equipment.set('sernr', Ext.getCmp('newEquiPageSerialNrTextField').getValue());
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseNewEquiPage', ex);
		}
	},
	
	getCurrentEquiTypeFromSpinner: function() {
		var retval = '';
			
		try {
			var retval = Ext.getCmp('newEquiPageEquiTypeComboBox').getValue();

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval) && retval.length > 1) {
				//restore the equiType value from the base equitype store
				var equiTypes = this.getViewModel().get('equipmentTypes');
				
				if(equiTypes && equiTypes.getCount() > 0) {
					equiTypes.each(function(eqType) {
						if(eqType.get('txtbz') === retval) {
							retval = eqType.get('eqtyp');
							return false;
						}
					}, this);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentEquiTypeFromSpinner of BaseNewEquiPage', ex);
			retval = '';
		}
		
		return retval;
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			retval = {
				eqktx: Ext.getCmp('newEquiPageEquiDescTextField').getValue(),
				eqtyp: this.getCurrentEquiTypeFromSpinner(),
				tplnr: Ext.getCmp('newEquiFuncLocTextField').getValue(),
				hequi: Ext.getCmp('newEquiHeadEquipmentTextField').getValue(),
			    baumm: Ext.getCmp('newEquiPageConstrMthTextField').getValue(),
			    baujj: Ext.getCmp('newEquiPageConstrYrTextField').getValue(),
			    sernr: Ext.getCmp('newEquiPageSerialNrTextField').getValue(),
			    typbz: Ext.getCmp('newEquiPageConstrTypeTextField').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseNewEquiPage', ex);
			retval = null;
		}
		
		return retval;
	},
	
	fillFuncLocTextFields: function(funcLoc) {
		try {
			Ext.getCmp('newEquiFuncLocTextField').setValue(funcLoc ? funcLoc.getDisplayIdentification() : '');
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMaterialSpecFieldsFromStpo of BaseNewEquiPage', ex);
		}
	},
	
	fillHeadEquipmentTextFields: function(equi) {
		try {
			var toPut = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(equi ? equi.get('equnr') : '');
		
			Ext.getCmp('newEquiHeadEquipmentTextField').setValue(toPut);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillHeadEquipmentTextFields of BaseNewEquiPage', ex);
		}
	},
	
	manageHeadEquiInputVisibility: function() { 
		try {
		    var field = Ext.getCmp('HeadEquiContainer');	
		    var para = true;
		   
		    if(para === false)
		 	   field.setVisible(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageHeadEquiInputVisibility of BaseNewEquiPage', ex);
		}
	}
});