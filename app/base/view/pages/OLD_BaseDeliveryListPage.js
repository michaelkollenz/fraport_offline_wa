 Ext.define('AssetManagement.base.view.pages.OLD_BaseDeliveryListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.DeliveryListPageViewModel',
        'AssetManagement.customer.controller.pages.DeliveryListPageController',
        'AssetManagement.customer.model.bo.Address',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date'
    ],

    inheritableStatics: {
		_instance: null,

        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.DeliveryListPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseDeliveryListPage', ex);
		    }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.DeliveryListPageRendererMixin'
    },

    controller: 'DeliveryListPageController',

    viewModel: {
        type: 'DeliveryListPageModel'
    },

    layout:{
        type: 'hbox',
	    align: 'stretch'
	},

	//protected
	//@override
    buildUserInterface: function() {
		try {
			var myController = this.getController();

			var items = [
			     {
			    	 xtype: 'container',
			         flex: 1,
			         frame: true,
			         layout: {
			            type: 'hbox',
			            align: 'stretch'
			         },
			         items: [
			            {
			                xtype: 'component',
			                maxWidth: 10,
	                        minWidth: 10
			            },
			            {
			                xtype: 'container',
			                // flex: 5,
			                layout: {
			                    type: 'vbox',
			                    align: 'stretch'
			                },
			                items: [
                        {
                          xtype: 'container',
                          id: 'DeliveryGridContainer',
                          flex: 1,
                          minHeight: 200,
                          layout: {
                            type: 'hbox',
                            align: 'stretch'
                          },
                          margin: '20 0 0 0',
                          items: [
                            this.getNewDeliveryGridPanel()
                          ]
                        }

			                ]
			            },
			            {
			                xtype: 'component',
			                width: 10
			            }
			        ]
			    }
			];

			this.add(items);
			// this.searchFieldVisibility();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseDeliveryListPage', ex);
    	}
    },

    //private
    renewDeliveryGridPanel: function() {
    	var retval = null;

    	try {
    		var gridContainer = Ext.getCmp('DeliveryGridContainer');

    		if(gridContainer)
    			gridContainer.removeAll(true);

    		retval = this.getNewDeliveryGridPanel();

    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewDeliveryGridPanel of BaseDeliveryListPage', ex);
    		retval = null;
    	}

    	return retval;
    },

    //private
    getNewDeliveryGridPanel: function() {
    	var retval = null;
        try {
            var me = this;
    		var myController = this.getController();
          var rendererMixin = this.mixins.rendererMixin;

    		retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
    			id: 'deliveryGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            parentController: myController,
                owner: me,
	            loadingText: Locale.getMsg('loadingDeliverys'),
	            emptyText: Locale.getMsg('noDeliverys'),
	            flex: 1,
	            scrollable: 'vertical',
              columns: [
                {
                  xtype: 'gridcolumn',
                  text: Locale.getMsg('building'),
                  dataIndex: 'gebnr',
                  width: 40,
                  // flex: 2,
                  renderer: rendererMixin.deliveryBuildingColumnRenderer
                },
                {
                  xtype: 'gridcolumn',
                  text: Locale.getMsg('status'),
                  dataIndex: 'items',
                  flex: 2,
                  renderer: rendererMixin.deliveryStatusColumnRenderer
                }
              //   {
              //     xtype: 'gridcolumn',
              //     text: Locale.getMsg('quantity'),
              //     dataIndex: 'menge_ist',
              //     flex: 2,
              //     renderer: rendererMixins.weMengeColumnRenderer
              //   },
              //   {
              //     xtype: 'widgetcolumn',
              //     text: Locale.getMsg('deliveryQuant'),
              //     width: 70,
              //     layout: {
              //       type: 'vbox',
              //       align: 'stretch'
              //     },
              //
              //     widget: {
              //       xtype: 'oxnumberfield',
              //       fieldLabel: '',
              //       padding: '10 0 0 0',
              //       width: 40,
              //       hideTrigger: true, //hides arrows
              //       keyNavEnabled: false, //can't imput values with arrow keys
              //       mouseWheelEnabled: false, //nor the mouseweel
              //       allowDecimals: false,
              //       allowNegativeValues: false,
              //       validateOnChange: true,
              //       dataIndex: 'menge_ist',
              //       listeners: {
              //         change: 'onDeliveredAmmountChange',
              //         //focusleave: 'onMeanValueFieldLooseFocus',
              //         scope: myController
              //       }
              //     },
              //     onWidgetAttach: rendererMixins.weMengeWidgetAttached
              //   },
              //   {
              //     xtype: 'checkcolumn',
              //     width: 60,
              //     dataIndex: 'elikz',
              //     tdCls: 'oxCheckBoxGridColumnItem',
              //     text: Locale.getMsg('ok'),
              //     listeners: {
              //       checkchange: 'onDeliveredCheckChange',
              //       scope: myController
              //     }
              //   }
              ],
	            listeners: {
	             	cellclick: myController.onDeliverySelected,
	        		cellcontextmenu: myController.onCellContextMenu,
	        		scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewDeliveryGridPanel of BaseDeliveryListPage', ex);
    	}

    	return retval;
	},

	//protected
	//@override
    getPageTitle: function() {
    	var retval = '';

    	try {
	    	var title = Locale.getMsg('deliverys');


	    	var deliverys = this.getViewModel().get('deliveryStore');

	    	if(deliverys) {
	    		title += " (" + deliverys.getCount() + ")";
	    	}

	    	retval = title;
	    } catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseDeliveryListPage', ex);
    	}

    	return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
    	try {
    		var deliveryStore = this.getViewModel().get('deliveryStore');

    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    		    this.renewDeliveryGridPanel();

    		// if (this.getViewModel().get('constrainedDeliverys')) {
    		//     deliveryStore = this.getViewModel().get('constrainedDeliverys');
    		// }

    		var deliveryGridPanel = Ext.getCmp('deliveryGridPanel');

    		deliveryGridPanel.setStore(deliveryStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseDeliveryListPage', ex);
    	}
	},

	//public
	//@override
	resetViewState: function() {
    this.callParent();

    try {
      Ext.getCmp('deliveryGridPanel').reset();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseDeliveryListPage', ex);
    }

	},

	//private
	searchFieldVisibility: function() {
       try {
	       var field = Ext.getCmp('searchDeliveryContainer');
	       var para = true;

	       if (para === false)
	    	   field.setVisible(false);

       } catch(ex) {
    	  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseDeliveryListPage', ex);
       }
	},

	//public
	getSearchValue: function() {
		try {
			// var deliveryToSearch = Ext.create('AssetManagement.customer.model.bo.Delivery', {});
			// var currentFilterCriterion = Ext.getCmp('deliveryListPageFilterCriteriaCombobox').getValue();
            //
			// if(currentFilterCriterion === Locale.getMsg('deliveryNum'))
			// 	deliveryToSearch.set('aufnr', Ext.getCmp('searchFieldDelivery').getValue());
			// else if(currentFilterCriterion === Locale.getMsg('shorttext'))
			// 	deliveryToSearch.set('ktext', Ext.getCmp('searchFieldDelivery').getValue());
			// else if(currentFilterCriterion === Locale.getMsg('notifNumber'))
			// 	deliveryToSearch.set('qmnum', Ext.getCmp('searchFieldDelivery').getValue());
			// else if(currentFilterCriterion === Locale.getMsg('equipment')) {
			// 	var equipment = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
			// 	equipment.set('eqktx', Ext.getCmp('searchFieldDelivery').getValue());
			// 	deliveryToSearch.set('equipment', equipment);
			// 	deliveryToSearch.set('equnr', Ext.getCmp('searchFieldDelivery').getValue())
			// } else if(currentFilterCriterion === Locale.getMsg('funcLoc')) {
			// 	var funcLoc = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
			// 	funcLoc.set('pltxt', Ext.getCmp('searchFieldDelivery').getValue());
			// 	deliveryToSearch.set('funcLoc', funcLoc);
			// 	deliveryToSearch.set('tplnr', Ext.getCmp('searchFieldDelivery').getValue())
			// } else if(currentFilterCriterion === Locale.getMsg('mainWorkctr'))
			// 	deliveryToSearch.set('vaplz', Ext.getCmp('searchFieldDelivery').getValue());
			// else if(currentFilterCriterion === Locale.getMsg('sortField')) {
			// 	var equi = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
			// 	equi.set('eqfnr', Ext.getCmp('searchFieldDelivery').getValue());
			// 	deliveryToSearch.set('equipment', equi);
			// }
            //
            //
			// // TODO: Kunde (customer)
            //
			// // Franke Client
			// // Datum
			// else if(currentFilterCriterion === Locale.getMsg('date')) {
             //    var operationStore = Ext.create('Ext.data.Store', {
             //        model: 'AssetManagement.customer.model.bo.Operation',
             //        autoLoad: false
             //    });
            //
             //    var operation = Ext.create('AssetManagement.customer.model.bo.Operation', {});
             //    operation.set('ntanSearch', Ext.getCmp('searchFieldDelivery').getValue());
            //
             //    operationStore.add(operation);
			// 	deliveryToSearch.set('operations', operationStore);
			// }
			// // PLZ
			// else if(currentFilterCriterion === Locale.getMsg('zipCode')) {
			// 	deliveryToSearch.set('address', Ext.create('AssetManagement.customer.model.bo.Address', {postCode1: Ext.getCmp('searchFieldDelivery').getValue()}))
			// }
			// // Ort
			// else if(currentFilterCriterion === Locale.getMsg('city')) {
			// 	deliveryToSearch.set('address', Ext.create('AssetManagement.customer.model.bo.Address', {city1: Ext.getCmp('searchFieldDelivery').getValue()}))
			// }
            //
			// this.getViewModel().set('searchDelivery', deliveryToSearch);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseDeliveryListPage', ex);
		}
	},


     //public
	getFilterValue: function () {
	    var retval = '';

	    // try {
	    //     retval = Ext.getCmp('searchFieldDelivery').getValue();
        //
	    //     if (!retval)
	    //         retval = '';
	    // } catch (ex) {
	    //     AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseDeliveryListPage', ex);
	    // }

	    return retval;
	},
     //public
	setFilterValue: function (value) {
	    try {
	        // if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
	        //     value = '';
            //
	        // retval = Ext.getCmp('searchFieldDelivery').setValue(value);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseDeliveryListPage', ex);
	    }
	}
});