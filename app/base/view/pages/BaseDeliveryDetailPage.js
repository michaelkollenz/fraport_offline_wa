Ext.define('AssetManagement.base.view.pages.BaseDeliveryDetailPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',


  requires: [
    'AssetManagement.customer.model.pagemodel.DeliveryDetailPageViewModel',
    'AssetManagement.customer.controller.pages.DeliveryDetailPageController',
    'AssetManagement.customer.view.OxGridPanel',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.view.utils.OxTextField',
    'AssetManagement.customer.view.utils.OxNumberField',
    'AssetManagement.customer.view.utils.OxComboBox',
    'Ext.grid.plugin.CellEditing',
    'Ext.grid.column.Number',
    'Ext.grid.column.Check',
    'Ext.grid.column.Action'
  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.DeliveryDetailPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseDeliveryDetailPage', ex);
      }

      return this._instance;
    }
  },

  mixins: {
    // rendererMixin: 'AssetManagement.customer.view.mixins.DeliveryDetailPageRendererMixin'
    // itemsMixin: 'AssetManagement.customer.view.mixins.DeliveryDetailPageItemsMixin'
  },
  _DeliveryDocumentRow: null,
  _plantRow: null,
  _storLocRow: null,

  viewModel: {
    type: 'DeliveryDetailPageModel'
  },

  controller: 'DeliveryDetailPageController',

  buildUserInterface: function () {
    try {
      var myController = this.getController();
      var me = this;

      this.editing = Ext.create('Ext.grid.plugin.CellEditing');
      var items = [
        {
          xtype: 'container',
          flex: 1,
          layout: {
            type: 'hbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'container',
              margin: '0 10 0 10',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [{
                xtype: 'container',
                flex: 1,
                layout: {
                  type: 'vbox',
                  align: 'stretch'
                },
                items: [
                  {
                    xtype: 'oxtextfield',
                    id: 'iaDeliveryFieldPerNr',
                    fieldLabel: Locale.getMsg('personalNumber'),
                    maxLength: 12,
                    height: 40,
                    // readOnly: true,
                    padding: '10 10 10 10',
                    margin: '0 0 10 0',
                    triggers: {
                      clear: {
                        hidden: false,
                        cls: 'x-form-clear-trigger',
                        weight: 10,
                        extraCls: 'ox-custom-clear-trigger-textfield',
                        handler: function () {
                          this.setValue('');
                        }
                      }
                    }
                  },
                  {
                    xtype: 'oxtextfield',
                    id: 'iaDeliveryFieldPerKostSt',
                    fieldLabel: Locale.getMsg('costCenter'),
                    maxLength: 10,
                    height: 40,
                    // readOnly: true,
                    padding: '10 10 10 10',
                    margin: '0 0 10 0',
                    triggers: {
                      clear: {
                        hidden: false,
                        cls: 'x-form-clear-trigger',
                        weight: 10,
                        extraCls: 'ox-custom-clear-trigger-textfield',
                        handler: function () {
                          this.setValue('');
                        }
                      }
                    }
                  }
                ]
              },
                {
                  xtype: 'container',
                  id: 'DeliveryReportGridContainer',
                  margin: '10 0 10 0',
                  flex: 1,
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  }/*,
                               items:
                                   [
                                       this.getDeliveryItemsGridPanel()
                                   ]*/
                },
                {
                  xtype: 'image',
                  id: 'barcode',
                  hidden: true
                }
              ]
            }
          ]
        }
      ];


      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseDeliveryDetailPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';

    try {
      var title = 'ReportPage';//TODO

      // var Delivery = this.getViewModel().get('deliveryStore') ? this.getViewModel().get('Delivery').get('items') : null;

      // if (Delivery) {
      //   title += "(" + Delivery.getCount() + ")";
      // }
      retval = title;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseDeliveryDetailPage', ex);
    }

    return retval;
  },

  //protected
  //@Override
  updatePageContent: function () {
    try {
      //do not remove - causes issue if we are not generating each and every time the grid

      this.refreshGridPanel();

      // this.clearAllFields();
      // this.fillMainDataSection();
      this.refreshDeliveryItemPanel();
      this.transferModelStateIntoView();
      // this.searchFieldVisibility();


    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseDeliveryDetailPage', ex);
    }
  },

  refreshGridPanel: function () {
    try {
      var gridContainer = Ext.getCmp('DeliveryReportGridContainer');

      // var invItemGrid;

      gridContainer.removeAll();
      gridContainer.add(this.getDeliveryItemsGridPanel())
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseDeliveryDetailPage', ex);
    }
  },


  refreshDeliveryItemPanel: function () {
    try {
      var DeliveryItemGridPanel = Ext.getCmp('DeliveryReportGridPanel');

      var deliveryStore = this.getViewModel().get('deliveryStore');

      DeliveryItemGridPanel.setStore(deliveryStore);

      // invItemsList.sort('item', 'ASC');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshDeliveryItemPanel of BaseDeliveryDetailPage', ex);
    }

  },

  getDeliveryItemsGridPanel: function () {
    var retval = null;
    try {
      var me = this;
      var myController = this.getController();
      var reasonStore = this.getViewModel().get('reasonStore');
      // var rendererMixins = this.mixins.rendererMixin;

      retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
        id: 'DeliveryReportGridPanel',
        hideContextMenuColumn: true,
        useLoadingIndicator: true,
        loadingText: Locale.getMsg('loadingDelivery'),
        emptyText: Locale.getMsg('noDelivery'),
        flex: 1,
        scrollable: 'vertical',
        columns:
          [
            {
              xtype: 'gridcolumn',
              dataIndex: 'raum',
              text: Locale.getMsg('room'),
              flex: 1,
              // width: 80,
              resizable: false,
              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                var raum = record.get('raum');
                var ia_status = record.get('ia_status');
                var status = '';
                if (ia_status === 'Z') {
                  status = Locale.getMsg('delivered');
                } else if (ia_status === 'A') {
                  status = Locale.getMsg('pickedUp');
                } else if (ia_status === 'XX') {
                  status = Locale.getMsg('failed');
                }
                return raum + '<p>' + status + '</p>';

              }
            },
            {
              xtype: 'gridcolumn',
              dataIndex: 'empf',
              text: Locale.getMsg('empf'),
              flex: 1,
              // width: 80,
              resizable: false,
              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                var empf = record.get('empf');
                var grund = record.get('grund');
                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
                var spras = userInfo.get('sprache');
                reasonStore.each(function (reason) {
                  if (reason.get('grund') === record.get('grund') && reason.get('spras') === spras) {
                    grund = reason.get('text');
                  }
                });
                if(grund === '000'){
                  grund = '';
                }
                return empf + '<p>' + grund + '</p>';

              }
            },
            {
              xtype: 'gridcolumn',
              dataIndex: 'menge',
              text: Locale.getMsg('quantity'),
              flex: 1,
              // width: 80,
              resizable: false,
              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                var menge = record.get('menge');
                var meins = record.get('meins');
                return menge + '<p>' + meins + '</p>';

              }
            }
          ]
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeliveryItemsGridPanel of BaseDeliveryDetailPage', ex);
    }

    return retval;
  },

  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();

      myModel.set('persNo', Ext.getCmp('iaDeliveryFieldPerNr').getValue());
      myModel.set('kostl', Ext.getCmp('iaDeliveryFieldPerKostSt').getValue());

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseDeliveryDetailPage', ex);
    }
  },

  transferModelStateIntoView: function () {
    try {
      var myModel = this.getViewModel();
      var deliveryStore = myModel.get('deliveryStore');

      if (deliveryStore) {
        var deliveryItem = deliveryStore.getAt(0);
        //set general data
        if (deliveryItem.get('kostl')) {
          Ext.getCmp('iaDeliveryFieldPerKostSt').setValue(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(deliveryItem.get('kostl')));
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of FbUmbPage', ex);
    }
  },

  getCurrentInputValues: function () {
    var retval = null;

    try {
      retval = Ext.getCmp('DeliveryReportGridPanel').getStore();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseDeliveryDetailPage', ex);
      retval = null;
    }
    return retval;
  }
});
