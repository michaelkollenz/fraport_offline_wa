Ext.define('AssetManagement.base.view.pages.BaseEquipmentListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.EquipmentListPageViewModel',
        'AssetManagement.customer.controller.pages.EquipmentListPageController',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'AssetManagement.customer.view.utils.OxTextField',
        'Ext.grid.column.Action'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null)
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.EquipmentListPage');
            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of EquiListPage', ex);
		    }
            	
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.EquipmentListPageRendererMixin'
    },

    controller: 'EquipmentListPageController',
    
    viewModel: {
        type: 'EquipmentListPageModel'
    },

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   
    //protected
	//@override
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    	
				var items = [
		        {
		            xtype: 'container',
		            flex: 1,
		            frame: true,
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 5,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                       
		                         {
		                            xtype: 'container',
		                            height: 30,
		                            id: 'searchEquiContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                               
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldEqui',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,
		                                    listeners: {
		                                		change: {
		                                        	fn: 'onEquiListPageSearchFieldChange'
		                                    	}
		                            		}
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
			                                id: 'equiListPageFilterCriteriaCombobox',
		                                    editable: false,
		                                    store: [Locale.getMsg('equipmentNumber'), Locale.getMsg('shorttext'), Locale.getMsg('technicalIdentNr'),
		                                            Locale.getMsg('funcLoc'), Locale.getMsg('sortField'), Locale.getMsg('material'), Locale.getMsg('location'),
		                                            Locale.getMsg('room')],
		                                            
		                                    value: Locale.getMsg('equipmentNumber'),
											listeners: {
												select: 'onEquiListPageSearchFieldChange'
											}
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
	                            {
			                        xtype: 'container',
			                        id: 'equipmentGridContainer',
			                        flex: 1,
			                        layout: {
	                            		type: 'hbox',
	                            		align: 'stretch'
	                            	},
			                        items: [
			                           this.getNewEquipmentGridPanel()
			                        ]
			                    },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];
		
			this.add(items);
			this.searchFieldVisibility();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseEquipmentListPage', ex);
		}
    },
    
    //private
    renewEquipmentGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('equipmentGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewEquipmentGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewEquipmentGridPanel of BaseEquipmentListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewEquipmentGridPanel: function() {
    	var retval = null;
    	
        try {
            var me = this;
    		var myController = this.getController();
    	
    		retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
    			id: 'equipmentGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            loadingText: Locale.getMsg('loadingEquipments'),
	            emptyText: Locale.getMsg('noEquipments'),
	            flex: 1,
	            parentController: myController,
                owner: me,
	            scrollable: 'vertical', 
	            /*columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        return '<img src="resources/icons/equi.png"  />';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,                                   
	                    enableColumnHide: false,
	                    align: 'right'
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        //store -> item -> aufnr, ktext
	                        var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
	                        var ktext = record.get('eqktx');
	                        return aufnr +'<p>'+ktext+'</p>';
	                    },
	                    enableColumnHide: false,
	                    dataIndex: 'equnr',                                 
	                    text: Locale.getMsg('equipment')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    enableColumnHide: false,
	                    dataIndex: 'tidnr',
	                    flex: 0.5,
	                    text: Locale.getMsg('technicalIdentNr')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var eqfnr = record.get('eqfnr');
	
	                        return eqfnr;
	                    },
	                    enableColumnHide: false,
	                    dataIndex: 'eqfnr',
	                    text: Locale.getMsg('sortField')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var matnr = record.get('matnr');
	
	                        return matnr;
	                    },
	                    enableColumnHide: false,
	                    dataIndex: 'matnr',
	                    text: Locale.getMsg('material')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		var name = '';
	                        var street = '';
	                        var postalCode = '';
	                        
	                        var address = record.get('address');
							if(address !== null && address !== undefined) {
								name = address.getName();
								street = AssetManagement.customer.utils.StringUtils.concatenate([ address.getStreet(), address.getHouseNo() ], null, true);
								postalCode = AssetManagement.customer.utils.StringUtils.concatenate([ address.getPostalCode(), address.getCity() ], null, true);
							} else {
							  	name = '';
	                            street = '';
	                            postalCode = '';
							}
							    
	                        return name +'<p>'+street+'</p><p>'+postalCode+'</p>';
	                 	},
	                    enableColumnHide: false,
	                    dataIndex: 'postalCode',
	                    text: Locale.getMsg('address')
					},
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		if(!record.get('address')) {
	                			metaData.style = 'display: none;';
	                		} else {
	                			metaData.style = '';
	                		}
	                	},
	                	text: Locale.getMsg('maps'),
	                	maxWidth: 80,
	                    width: 80,
	                    items: [{
	                        icon: 'resources/icons/maps_small.png',
	                        tooltip: 'Google Maps',
	                        iconCls: 'oxGridLineActionButton',
	                        handler: function(grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
	                            event.stopEvent();
	                            this.getController().navigateToAddress(record);
	                        },
	                        scope: this
	                    }],
	                    enableColumnHide: false,
	                    align: 'center'
	                }
	            ],*/
	            listeners: {
	            	cellclick: myController.onEquipmentSelected,
	            	cellcontextmenu: myController.onCellContextMenu,
	            	scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewEquipmentGridPanel of BaseEquipmentListPage', ex);
    	}
    	
    	return retval;
	},
   
    //protected
	//@override
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
	    	var title = Locale.getMsg('equipments');
	    	
	    	var equis = this.getViewModel().get('equipmentStore');
	    	
	    	if(equis) {
	    		title += " (" + equis.getCount() + ")";
	    	}
	    	
	    	retval = title;
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseEquipmentListPage', ex);
		}

    	return retval;
	},
    
	//protected
	//@override
    updatePageContent: function() {
    	try {
    		var equipmentStore = this.getViewModel().get('equipmentStore');
    		
    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    			this.renewEquipmentGridPanel();
    		
    		if (this.getViewModel().get('constrainedEquipments')) {
    		    equipmentStore = this.getViewModel().get('constrainedEquipments');
    		}
    		var equiGridPanel = Ext.getCmp('equipmentGridPanel');
    		
    		equiGridPanel.setStore(equipmentStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseEquipmentListPage', ex);
    	}
	},

	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('equipmentGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseEquipmentListPage', ex);
    	}
	},
	
	searchFieldVisibility: function() {      	   
		try {      
			var field = Ext.getCmp('searchEquiContainer');	
			var para = true;

			if(para === false)
    		   field.setVisible(false);
    		   
		} catch(ex) {
	     	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of EquiListPage', ex);
		}
	},

	getSearchValue: function() {
		try {
			var equiToSearch = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
			var currentFilterCriterion = Ext.getCmp('equiListPageFilterCriteriaCombobox').getValue();	

			// Equipmentnummer
			if (currentFilterCriterion == Locale.getMsg('equipmentNumber')) {
			    equiToSearch.set('equnr', Ext.getCmp('searchFieldEqui').getValue());
			    //Kurztext
			} else if (currentFilterCriterion == Locale.getMsg('shorttext')) {
			    equiToSearch.set('eqktx', Ext.getCmp('searchFieldEqui').getValue());
			    // Tech. Indentnummer
			} else if (currentFilterCriterion == Locale.getMsg('technicalIdentNr')) {
			    equiToSearch.set('tidnr', Ext.getCmp('searchFieldEqui').getValue());
			    // Techn. Platz
			} else if (currentFilterCriterion == Locale.getMsg('funcLoc')) {
			    var funcLoc = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
			    funcLoc.set('pltxt', Ext.getCmp('searchFieldEqui').getValue());
			    equiToSearch.set('funcLoc', funcLoc);
			    equiToSearch.set('tplnr', Ext.getCmp('searchFieldEqui').getValue())
			    // Sortierfeld
			} else if (currentFilterCriterion == Locale.getMsg('material')) {
			    equiToSearch.set('matnr', Ext.getCmp('searchFieldEqui').getValue());
			    // Standort
			} else if (currentFilterCriterion == Locale.getMsg('sortField')) {
			    equiToSearch.set('eqfnr', Ext.getCmp('searchFieldEqui').getValue());
			    // Standort
			} else if (currentFilterCriterion == Locale.getMsg('location')) {
			    equiToSearch.set('stort', Ext.getCmp('searchFieldEqui').getValue());
			    // Raum
			} else if (currentFilterCriterion == Locale.getMsg('room')) {
			    equiToSearch.set('msgrp', Ext.getCmp('searchFieldEqui').getValue());
			}
		
			this.getViewModel().set('searchEqui', equiToSearch);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of EquiListPage', ex);
		}
	},

    //public
	getFilterValue: function () {
	    var retval = '';

	    try {
	        retval = Ext.getCmp('searchFieldEqui').getValue();

	        if (!retval)
	            retval = '';
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseEquipmentListPage', ex);
	    }

	    return retval;
	},
    //public
	setFilterValue: function (value) {
	    try {
	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
	            value = '';

	        retval = Ext.getCmp('searchFieldEqui').setValue(value);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseEquipmentListPage', ex);
	    }
	}
});