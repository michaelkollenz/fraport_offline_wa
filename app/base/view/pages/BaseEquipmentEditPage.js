Ext.define('AssetManagement.base.view.pages.BaseEquipmentEditPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.EquipmentEditPageViewModel',
        'AssetManagement.customer.controller.pages.EquipmentEditPageController',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.Date'
    ],

    inheritableStatics: {
		_instance: null,
	
	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	        	   this._instance = Ext.create('AssetManagement.customer.view.pages.EquipmentEditPage');
	            }
	        } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseEquipmentEditPage', ex);
		    }	
	        
	        return this._instance;
	    }
	},
	
    viewModel: {
        type: 'EquipmentEditPageModel'
    },
    
    controller: 'EquipmentEditPageController',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
   
    buildUserInterface: function() {
         try {
		     var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'label',
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('generalData')
		                        },
		                        {
		                            xtype: 'component',
		                            height: 15
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditEquipmentTextField',
		                            disabled: true,
		                            fieldLabel: Locale.getMsg('equipment'),
		                            maxLength: 18,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditObj_NrTextField',
		                            disabled: true,
		                            fieldLabel: Locale.getMsg('objectnumber'),
		                            maxLength: 22,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditBuildTypeTextField',
		                            fieldLabel: Locale.getMsg('constructionType'),
		                            maxLength: 18,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            flex: 1,
		                            id: 'equiEditShortTextField',
		                            fieldLabel: Locale.getMsg('shorttext'),
		                            maxLength: 40,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditInventTextField',
		                            fieldLabel: Locale.getMsg('inventoryNumber'),
		                            maxLength: 25,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
			                        xtype: 'datefield',
		                            id: 'equiEditAcquiDateTextField',
		                            format: 'd.m.Y',
		                            maxLength: 10,
		                            maxWidth: 300,
		                            fieldLabel: Locale.getMsg('dateOfAcquisition'),
		                            labelWidth: 150
		                        },
		                        {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                    	{
         			                        xtype: 'datefield',
         		                            flex: 1,
         		                            id: 'equiEditGwldtDateTextField',
         		                            format: 'd.m.Y',
         		                            maxWidth: 300,
         		                            maxLength: 10,
         		                            fieldLabel: Locale.getMsg('guaranteeDate'),
         		                            labelWidth: 150
         		                        },
                                        {
                                            xtype: 'component',
                                           	width: 10
                                        },
                                        {
        			                        xtype: 'datefield',
        		                            flex: 1,
        		                            id: 'equiEditGwldt_eDateTextField',
        		                            format: 'd.m.Y',
        		                            maxWidth: 300,
        		                            maxLength: 10,
        		                            fieldLabel: Locale.getMsg('guaranteeDateEnd'),
        		                            labelWidth: 150
        		                        }
                                    ]
                                },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            flex: 1,
		                            id: 'equiEditTech_IDTextField',
		                            fieldLabel: Locale.getMsg('technicalIdentNr'),
		                            maxLength: 25,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditSortTextField',
		                            fieldLabel: Locale.getMsg('sortField'),
		                            maxLength: 30,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditLocationTextField',
		                            fieldLabel: Locale.getMsg('location'),
		                            maxLength: 10,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditRoomTextField',
		                            fieldLabel: Locale.getMsg('room'),
		                            maxLength: 8,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 40
		                        },
		                        {
		                            xtype: 'label',
		                            flex: 1,
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('producerData')
		                        },
		                        {
		                            xtype: 'component',
		                            height: 15
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditManufacturerTextField',
		                            fieldLabel: Locale.getMsg('manufacturer'),
		                            maxLength: 30,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            flex: 1,
		                            id: 'equiEditManufactCountryTextField',
		                            fieldLabel: Locale.getMsg('producingCountry'),
		                            maxLength: 3,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditTypeTextField',
		                            fieldLabel: Locale.getMsg('typeIdentifier'),
		                            maxLength: 20,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditSerial_NrTextField',
		                            fieldLabel: Locale.getMsg('manifSerialnumber'),
		                            maxLength: 18,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxnumberfield',
		                            id: 'equiEditBuildYearNumberField',
		                            fieldLabel: Locale.getMsg('constrYr'),
		                            maxLength: 4,
		                            labelWidth: 150,
		                            allowDecimals: false
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiEditBuildMonthTextField',
		                            fieldLabel: Locale.getMsg('constrMth'),
		                            maxLength: 2,
		                            labelWidth: 150
		                        },
		                        {
		                            xtype: 'component',
		                            height: 5
		                        }
		                    ]
		                },
		                {
		                	xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                }
		            ]
		        }
		    ];
		     
		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseEquipmentEditPage', ex);
		}
    },
    
    getPageTitle: function() {
    	var retval = '';
    
    	try {
    		retval = Locale.getMsg('masterDataInProcess');
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseEquipmentEditPage', ex);
		}
    	
		return retval;
    },
    
	//protected
	//@override
	updatePageContent: function() {
		try {
			this.transferModelStateIntoView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseEquipmentEditPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var equi = this.getViewModel().get('equipment'); 
		
			Ext.getCmp('equiEditEquipmentTextField').setValue(equi.get('equnr'));
			Ext.getCmp('equiEditBuildTypeTextField').setValue(equi.get('submt'));
			Ext.getCmp('equiEditBuildMonthTextField').setValue(equi.get('baumm'));
			Ext.getCmp('equiEditBuildYearNumberField').setValue(equi.get('baujj'));
			Ext.getCmp('equiEditSerial_NrTextField').setValue(equi.get('sernr'));
			Ext.getCmp('equiEditTypeTextField').setValue(equi.get('typbz'));
			Ext.getCmp('equiEditManufactCountryTextField').setValue(equi.get('herld'));
			Ext.getCmp('equiEditManufacturerTextField').setValue(equi.get('herst'));
			Ext.getCmp('equiEditRoomTextField').setValue(equi.get('msgrp'));
			Ext.getCmp('equiEditLocationTextField').setValue(equi.get('stort'));
			Ext.getCmp('equiEditSortTextField').setValue(equi.get('eqfnr'));
			Ext.getCmp('equiEditTech_IDTextField').setValue(equi.get('tidnr'));
			Ext.getCmp('equiEditAcquiDateTextField').setValue(equi.get('ansdt'));
			Ext.getCmp('equiEditInventTextField').setValue(equi.get('invnr'));
			Ext.getCmp('equiEditShortTextField').setValue(equi.get('eqktx'));
			Ext.getCmp('equiEditObj_NrTextField').setValue(equi.get('objnr'));
			Ext.getCmp('equiEditGwldtDateTextField').setValue(equi.get('gwldt'));
			Ext.getCmp('equiEditGwldt_eDateTextField').setValue(equi.get('gwlen'));
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseEquipmentEditPage', ex);
		}
	},
	
	transferViewStateIntoModel: function() {
		try {
			var equipment = this.getViewModel().get('equipment');
			
			var baujj = Ext.getCmp('equiEditBuildYearNumberField').getValue();
			
			if(baujj)
				baujj += '';
			else
				baujj = '';
			
			equipment.set('submt', Ext.getCmp('equiEditBuildTypeTextField').getValue());
			equipment.set('baumm', Ext.getCmp('equiEditBuildMonthTextField').getValue());
			equipment.set('baujj', baujj);
			equipment.set('sernr', Ext.getCmp('equiEditSerial_NrTextField').getValue());
			equipment.set('typbz', Ext.getCmp('equiEditTypeTextField').getValue());
			equipment.set('herld', Ext.getCmp('equiEditManufactCountryTextField').getValue());
			equipment.set('herst', Ext.getCmp('equiEditManufacturerTextField').getValue());
			equipment.set('msgrp', Ext.getCmp('equiEditRoomTextField').getValue());
			equipment.set('stort', Ext.getCmp('equiEditLocationTextField').getValue());
			equipment.set('eqfnr', Ext.getCmp('equiEditSortTextField').getValue());
			equipment.set('tidnr', Ext.getCmp('equiEditTech_IDTextField').getValue());
			equipment.set('ansdt', Ext.getCmp('equiEditAcquiDateTextField').getValue());
			equipment.set('invnr', Ext.getCmp('equiEditInventTextField').getValue());
			equipment.set('eqktx', Ext.getCmp('equiEditShortTextField').getValue());
			equipment.set('gwldt', Ext.getCmp('equiEditGwldtDateTextField').getValue());
			equipment.set('gwlen', Ext.getCmp('equiEditGwldt_eDateTextField').getValue());
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseEquipmentEditPage', ex);
		}
	},
	
	//get current values from screen
	getCurrentInputValues: function() {
		var retval = null;
	
		try {
			var baujj = Ext.getCmp('equiEditBuildYearNumberField').getValue();
			
			if(baujj)
				baujj += '';
			else
				baujj = '';
		
			retval = {
				submt: Ext.getCmp('equiEditBuildTypeTextField').getValue(),
			    baumm: Ext.getCmp('equiEditBuildMonthTextField').getValue(),
			    baujj: baujj,
			    sernr: Ext.getCmp('equiEditSerial_NrTextField').getValue(),
			    typbz: Ext.getCmp('equiEditTypeTextField').getValue(),
			    herld: Ext.getCmp('equiEditManufactCountryTextField').getValue(),
			    herst: Ext.getCmp('equiEditManufacturerTextField').getValue(),
			    msgrp: Ext.getCmp('equiEditRoomTextField').getValue(),
			    stort: Ext.getCmp('equiEditLocationTextField').getValue(),
			    eqfnr: Ext.getCmp('equiEditSortTextField').getValue(),
			    tidnr: Ext.getCmp('equiEditTech_IDTextField').getValue(),
			    ansdt: Ext.getCmp('equiEditAcquiDateTextField').getValue(),
			    invnr: Ext.getCmp('equiEditInventTextField').getValue(),
			    eqktx: Ext.getCmp('equiEditShortTextField').getValue(),
			    gwldt: Ext.getCmp('equiEditGwldtDateTextField').getValue(),
			    gwlen: Ext.getCmp('equiEditGwldt_eDateTextField').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseEquipmentEditPage', ex);
			retval = null;
		}
		
		return retval;
	}
});