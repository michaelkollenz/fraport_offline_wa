Ext.define('AssetManagement.base.view.pages.BaseOrderDetailPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'AssetManagement.customer.model.pagemodel.OrderDetailPageViewModel',
        'Ext.Component',
        'Ext.form.Label',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'AssetManagement.customer.view.utils.FileSelectionButton',
        'Ext.grid.View',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date',
        'AssetManagement.customer.controller.pages.OrderDetailPageController',
        'AssetManagement.customer.view.utils.AddressTemplate',
        'AssetManagement.customer.view.utils.ChecklistTemplate',
        'Ext.util.Format'
    ],

    inheritableStatics: {
    	_instance: null,

        getInstance: function() {
	        try {
                if(this._instance === null) {
        	       this._instance = Ext.create('AssetManagement.customer.view.pages.OrderDetailPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseOrderDetailPage', ex);
		    }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.OrderDetailPageRendererMixin'
    },

    config: {
        order: null
    },

    _orderRow: null,
	_orderTypeRow: null,
	_ilartRow: null,
	_berRow: null,
	_funcLocRowRight: null,
	_equipmentRowRight: null,
	_notifRow: null,
	_startDateRow: null,
	_endDateRow: null,
	_systemStatus: null,
	_statusRow: null,
	_storLocRow: null,
	_factoryRow: null,
	_objectAddressRowRight: null,
	_customerAddressRowRight: null,
	_equipmentRowBottom: null,
	_funcLocRowBottom: null,
	_customerAddressRowBottom: null,
	_objectAddressRowBottom: null,

    viewModel: {
        type: 'OrderDetailPageModel'
    },

    controller: 'OrderDetailPageController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;

    		var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    width: 10
	                    },
		                {
		                    xtype: 'container',
                            flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'upperContent',
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
	                                        xtype: 'label',
	                                        margin: '0 0 0 5',
	                                        cls: 'oxHeaderLabel',
	                                        text: Locale.getMsg('generalData')
			                            },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'container',
		                                    layout: {
		                                        type: 'hbox'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'container',
		                                            id:'leftGrid',
		                                            flex: 1
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            id: 'rightGrid',
		                                            padding: '0 0 0 20',
		                                            flex: 1,
		                                            plugins: 'responsive',
	                                                responsiveConfig: {
	                                                	'width <= 800': {
	                                                		visible: false
	                                            		},
	                                            		'width > 800': {
	                                            			visible: true
	                                            		}
		                                        	}
		                                        }
		                                    ]
		                                }
		                        	]
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'bottomGrid',
		                            margin: '40 0 0 0',
		                            plugins: 'responsive',
		                            responsiveConfig: {
		                            	'width <= 800': {
		                            		visible: true
				                        },
				                        'width > 800': {
				                            visible: false
				                        }
		                        	}
		                        },
	                            {
	                                xtype: 'container',
	                                id: 'orderDetailOperationsSection',
	                                margin: '40 5 0 5',
	                                layout: {
	                                    type: 'vbox',
	                                    align: 'stretch'
	                                },
	                                items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('operations')
                                        },
		                                {
		                                	xtype: 'oxdynamicgridpanel',
		                                	id: 'operGridPanel',
		                                	margin: '20 0 0 0',
                                            parentController: myController,
		                                	owner: me,
			                                dockedItems: [
			                                    {
			                                        xtype: 'toolbar',
			                                        dock: 'bottom',
			                                        id: 'operationListToolbar',
			                                        height: 50,
			                                        layout: {
			                                            type: 'hbox',
			                                            align: 'middle'
			                                        },
			                                        items: [
	                                                {
	                                                    xtype: 'container',
	                                                    flex: 1,
	                                                    layout: {
	                                                        type: 'hbox',
	                                                        align: 'middle'
	                                                    },
	                                                    items: [
	                                                        {
	                                                            xtype: 'component',
	                                                            maxWidth: 6,
	    			                                            minWidth: 6
	                                                        },
	                                                        {
	                                                        	   xtype: 'button',
			                                                        html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
			                                                        height: 40,
			                                                        width: 50,
			                                                        padding: 0,
				                                                    listeners: {
				                                                   		click: 'onNewOperClick'
			                                                    	}
			                                                    },
			                                                    {
			                                                        xtype: 'label',
			                                                        flex: 1,
			                                                        text: Locale.getMsg('addOperation')
			                                                    }
			                                                ]
			                                         	}
			                                        ]
			                                    }
			                                ],
			                                /*columns: [
			                                    {
			                                        xtype: 'actioncolumn',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return '<img src="resources/icons/operation.png"/>';
			                                        },
			                                        maxWidth: 80,
			                                        minWidth: 80,
			                                        align: 'right'
			                                    },

			                                    {
			                                        xtype: 'gridcolumn',
			                                        flex: 1,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
			                                            var vornr = '';
			                                            var ktext = '';

			                                            if(isSplit) {
			                                            	vornr = record.get('vornr') + " / " + record.get('split');
			                                            	ktext = record.get('stext');
			                                            } else {
			                                            	vornr = record.get('vornr');
			                                            	ktext = record.get('ltxa1');
			                                        	}

			                                            var vornr = record.get('vornr');
			                                            var ktext = record.get('ltxa1');
			                                            return vornr +'<p>'+ktext+'</p>';
			                                        },
			                                        enableColumnHide: false,
			                                        dataIndex: 'vornr',
			                                        text: Locale.getMsg('operation')
			                                    },
			                                    {
			                                        xtype: 'gridcolumn',
			                                        maxWidth: 160,
			                                        minWidth: 160,
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return record.get('arbpl') + ' / '+ record.get('werks');
			                                        },
			                                        enableColumnHide: false,
			                                        dataIndex: 'arbpl',
			                                        text: Locale.getMsg('workcenterPlant')
			                                    },
			                                    {
			                                        xtype: 'datecolumn',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
			                                            var startDate = '';
			                                            var endDate = '';

												        if(isSplit) {
													        startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('fstadt'), record.getRelevantTimeZone());
													        endDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('fenddt'), record.getRelevantTimeZone());
												        } else {
												            startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('fsav'), record.getRelevantTimeZone());
												            endDate= AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('fsed'), record.getRelevantTimeZone());
												        }
			                                            return startDate +'<p>'+endDate+'</p>';
			                                        },
			                                        enableColumnHide: false,
			                                        dataIndex: 'fstadt',
			                                        maxWidth: 125,
			                                        minWidth: 125,
			                                        text: Locale.getMsg('date')
			                                    },
			                                    {
			                                        xtype: 'gridcolumn',
			                                        maxWidth: 200,
			                                        minWidth: 200,
			                                        hidden: false, //forExternals,
			                                        dataIndex: 'string',
			                                        text: Locale.getMsg('planActualData'),
				                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return record.getPlannedDoneString();
			                                        }
			                                    }/*,
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'ordOperStatusColumn',
		                                            dataIndex: 'system_status',
		                                            maxWidth: 120,
		                                            minWidth: 120,
		                                            text: Locale.getMsg('status')
		                                        }
		                                    ],*/
		                                    listeners: {
		    	                        		cellclick: myController.onOperationSelected,
		    	                        		cellcontextmenu: myController.onCellContextMenuOperationList,
		    	                        		scope: myController
		    	                        	}
		                                }
	                                ]
	                            },

                                {
	                                xtype: 'container',
	                                id: 'orderDetailObjectGridItemsSection',
	                                margin: '40 5 0 5',
	                                layout: {
	                                    type: 'vbox',
	                                    align: 'stretch'
	                                },
	                                items: [
	                                    {
	                                        xtype: 'label',
	                                        cls: 'oxHeaderLabel',
	                                        text: Locale.getMsg('objects')
	                                    },
                                        {
                                            xtype: 'oxdynamicgridpanel',
                                            id: 'objectGridPanel',
                                            //hideContextMenuColumn: true,
                                            margin: '20 0 0 0',
                                            parentController: myController,
                                            owner: me,
											/*columns: [
											{
											 	xtype: 'actioncolumn',
											 	renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
											 		return '<img src="resources/icons/object_list.png"/>';
											 	},
											 	id: 'ordObjectImgColumn',
											 	maxWidth: 80,
											 	minWidth: 80,
											 	align: 'right'
											},
											{
											 	xtype: 'gridcolumn',
											 	id: 'ordObjectEquiColumn',
											 	flex: 1,
											 	dataIndex: 'equnr',
											 	renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
											 		var equnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
											 		var eqtxt = record.get('eqtxt');
											 		return equnr + '<p>' + eqtxt;
												},
											 	text: Locale.getMsg('equipment'),
											 	listeners: {
											 		click: 'onEquiSelected'
											 	}
									 		},
											{
											 	xtype: 'gridcolumn',
											 	id: 'ordObjectFuncLocColumn',
											 	flex: 1,
											 	dataIndex: 'tplnr',
											 	renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
											 		var tplnr =  AssetManagement.customer.utils.StringUtils.trimStart(record.get('tplnr'), '0');
											 		var tpltxt = record.get('pltxt');
											 		return tplnr + '<p>' + tpltxt;
									 			},
											 	text: Locale.getMsg('funcLoc'),
											 	listeners: {
											 		click:  'onFuncLocSelected'
											 	}
											},
											{
											 	xtype: 'gridcolumn',
											 	flex: 1,
											 	id: 'ordObjectNotifColumn',
											 	dataIndex: 'ihnum',
											 	renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
											 		return AssetManagement.customer.utils.StringUtils.trimStart(record.get('ihnum'), '0') + '<p>' + record.get('qmtxt');
											 	},
											 	text: Locale.getMsg('notification'),
											 	listeners: {
											 		click: 'onNotifSelected'
											 	}
											},
											{
											 	xtype: 'gridcolumn',
											 	flex: 1,
											 	dataIndex: 'matnr',
											 	renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
											 		return AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0') + '<p>' + record.get('maktx');
											 	},
											 	text: Locale.getMsg('material')
											},
											{
												xtype: 'gridcolumn',
											 	flex: 1,
											 	dataIndex: 'sernr',
											 	renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
											 		return record.get('sernr');
											 	},
												text: Locale.getMsg('serialNumber')
											},
											{
											 	xtype: 'actioncolumn',
											 	text: Locale.getMsg('checklist'),
											 	maxWidth: 80,
											 	width: 80,
											 	renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
											 		var retval = '';
											 		try {
											 			var qplos = record.get('qplos');
											 			if (qplos) {
											 				var icon = (qplos.isCompleted()) ? 'documentOk.png' : 'document.png';
											 				retval = '<img class="oxGridLineActionButton" src="resources/icons/' + icon + '"/>';
											 			} else if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('use_qplan') === 'X'){
											 				var matnr = ''
											 				var techObj = record.get('equipment');

											 				if (techObj) {
											 					matnr = techObj.get('matnr')
											 				} else {
											 					techObj = record.get('funcLoc');
											 					matnr = techObj ? techObj.get('submt') : '';
											 				}

											 				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
											 					var icon = 'documentAdd.png';
											 					retval = '<img class="oxGridLineActionButton" src="resources/icons/' + icon + '"/>';
											 				}
											 			}
											 		} catch (ex) {
											 			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checklistColumn', ex);
											 		}
											 		return retval;
											 	},
											 	listeners: {
											 		click: 'navigateToChecklist'
											 	},
											 	enableColumnHide: false,
											 	align: 'center'
										 }
										 ],*/
                                            listeners:{
                                                cellcontextmenu: myController.onCellContextMenuObjectList,
                                                scope: myController
                                            }
                                        }
	                               ]
	                            },
                                {
	                                xtype: 'container',
	                                id: 'orderDetailTimeConfSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('timeConfirmations')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'orderTimeConfPanel',
		                                    margin: '20 0 0 0',
		                                    hideContextMenuColumn: false,
			                                disableSelection: true,
			                                parentController: myController,
                                            owner:me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return '<img src="resources/icons/timeconf.png"/>';
			                                        },
		                                            id: 'ordTimeConfImgColumn',
			                                        maxWidth: 80,
			                                        minWidth: 80,
			                                        align: 'right'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'ordTimeConfOperationColumn',
		                                            dataIndex: 'vornr',
			                                        maxWidth: 100,
			                                        minWidth: 100,
		                                            text: Locale.getMsg('operation')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'ordTimeConfIlartColumn',
		                                            flex: 1,
		                                            dataIndex: 'learr',
		                                            text: Locale.getMsg('activityType'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var retval = '';

		                                        		try {
				                                            var learr = '';
				                                            var learrTxt = '';

				                                            if(record.get('activityType')) {
				                                            	learr = record.get('activityType').get('lstar');
				                                            	learrTxt = record.get('activityType').get('ktext');
				                                            } else {
				                                            	learr = record.get('learr');
				                                            }

				                                            retval = AssetManagement.customer.utils.StringUtils.concatenate([ learr, learrTxt ], '-', true);
				           				                } catch(ex) {
		                                        			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfGrid/learr-column in BaseOrderDetailPage', ex);
		                                        		}

		                                        		return retval;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'ordTimeConfBemotColumn',
		                                            flex: 1,
		                                            dataIndex: 'bemot',
		                                            text: Locale.getMsg('accountIndication'),
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        	var retval = '';
														try {
															var accountIndication = record.get('accountIndication');

															retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
														} catch(ex) {
															AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfGrid/bemot-column in BaseOrderDetailPage', ex);
														}

														return retval;
													}
		                                        },
		                                        {
		                                            xtype: 'datecolumn',
		                                            id: 'ordTimeConfDateColumn',
		                                            dataIndex: 'ied',
		                                            maxWidth: 125,
		                                            minWidth: 125,
		                                            text: Locale.getMsg('date'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	return AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('ied'), record.getRelevantTimeZone());
			                                        }

		                                        },
		                                        {
		                                            xtype: 'numbercolumn',
		                                            id: 'ordTimeConfTimeColumn',
			                                        maxWidth: 125,
		                                            minWidth: 125,
		                                            text: Locale.getMsg('worktime'),
		                                            dataIndex: 'idaur',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return record.getWorkValueForDisplay();
													}
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'ordTimeConfFinalConfColumn',
			                                        maxWidth: 160,
		                                            minWidth: 160,
		                                            text: Locale.getMsg('finalConfirmation'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	var aueru = record.get('aueru');

			                                           	if(aueru === 'X')
			                                           		return '[END]';

			                                           	return '';
			                                        }
		                                        }
			                                ],*/
	                                        listeners: {
		    	                        		cellcontextmenu: myController.onCellContextMenuTimeConfList,
		    	                        		scope: myController
		    	                        	}
                                        }
	                               ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'orderDetailExtConfsItemsSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('activityConfs_RecActivityConfs')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'orderDetailExtConfsItemsGridPanel',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
                                            owner:me,
		            	                    /*columns: [
		            	                        {
		            	                            xtype: 'gridcolumn',
		            	                            maxWidth: 80,
		            	                            minWidth: 80,
		            	                            align: 'center',
		            		                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		            	                                return '<img src="resources/icons/timeconf.png"/>';
		            	                            }
		            	                        },
		            	                        {
		            	                            xtype: 'gridcolumn',
		            	                            text: Locale.getMsg('material'),
		            	                            flex: 1,
		            		                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			            	                        	var matnr = record.get('matnr');
						                                var maktx = record.get('material') ? record.get('material').get('maktx') : '';

						                                return AssetManagement.customer.utils.StringUtils.concatenate([ matnr, maktx ], null, true);
		            	                            }
		            	                        },
		            	                        {
		            	                            xtype: 'datecolumn',
		            	                            maxWidth: 125,
		            	                            minWidth: 125,
		            	                            dataIndex: 'bedat',
		            	                            text: Locale.getMsg('date'),
		            		                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		            	                                var bedat = AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('bedat'), record.getRelevantTimeZone());

		            	                                return bedat;
		            	                            }
		            	                        },
		            	                        {
		            	                            xtype: 'gridcolumn',
		            	                            text: Locale.getMsg('quantityUnit'),
			            	                        maxWidth: 160,
		            	                            minWidth: 160,
		            		                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		            	                        		var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('menge'));
		            	                                var unit = record.get('meins');

		            	                                return amount + ' ' + unit;
		            	                            }
		            	                        },
		            	                        {
		            	                            xtype: 'gridcolumn',
		            	                            text: Locale.getMsg('priceCurrency'),
		            	                            flex: 1,
		            		                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		            	                                var priceCurrency = record.get('preis') + ' ' +record.get('waers');

		            	                                return priceCurrency;
		            	                            }
		            	                        }
		            	                    ],*/
			                                listeners: {
		    	                        		cellcontextmenu: myController.onCellContextMenuExtConfList,
		    	                        		scope: myController
		    	                        	}
		            		           }
		            		       ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'orderDetailComponentGridPanelItemsSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('plannedComponents')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'componentGridPanelOrderDetail',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
			                                owner: me,
		                                    dockedItems: [
		                                         {
			                                        xtype: 'toolbar',
			                                        dock: 'bottom',
			                                        id: 'componentGridToolbar',
			                                        height: 50,
			                                        layout: {
			                                            type: 'hbox',
			                                            align: 'middle'
			                                        },
			                                        items: [
	                                                {
	                                                    xtype: 'container',
	                                                    flex: 1,
	                                                    layout: {
	                                                        type: 'hbox',
	                                                        align: 'middle'
	                                                    },
	                                                    items: [
	                                                        {
	                                                            xtype: 'container',
	                                                            maxWidth: 6,
	    			                                            minWidth: 6
	                                                        },
			                                                    {
			                                                        xtype: 'button',
			                                                        html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
			                                                        height: 40,
			                                                        width: 50,
				                                                    padding: 0,
			                                                        listeners: {
				                    	                                click: 'onNewComponentClick'
				                    	                            }
			                                                    },
			                                                    {
			                                                        xtype: 'label',
			                                                        flex: 1,
			                                                        text: Locale.getMsg('addComponent')
			                                                    }
			                                                ]
			                                         	}
			                                        ]
			                                    }
			                                ],
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                                  return '<img src="resources/icons/settings.png"/>';
		                                            },
		                                            id: 'componentImgColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'componentOperColumn',
		                                            dataIndex: 'vornr',
			                                        maxWidth: 100,
			                                        minWidth: 100,
		                                            text: Locale.getMsg('operation')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'componentMaterialColumn',
		                                            flex: 1,
		                                            dataIndex: 'matnr',
		                                            text: Locale.getMsg('material'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                	                		var material = record.get('material');
			                	                		var maktx = material ? material.get('maktx') : '';

			                	                		return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'componentUnitColumn',
		                                            dataIndex: 'bdmng',
		                                            text: Locale.getMsg('orderComponents_Qty'),
			                                        maxWidth: 160,
			                                        minWidth: 160,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                                        			var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('bdmng'));
														return amount + ' ' + record.get('meins');
			                                        }
		                                        }
		                                    ],*/
			                                listeners: {
		    	                        		cellcontextmenu: myController.onCellContextMenuComponentList,
		    	                        		scope: myController
		    	                        	}
                                        }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'materialConfirmationsItemsSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('materialConfirmations_Short')
                                        },
                                        {
			                                margin: '20 0 0 0',
			                                xtype: 'oxdynamicgridpanel',
		                                    id: 'orderMatConfPanel',
		                                    disableSelection: true,
		                                    parentController: myController,
		                                    owner: me,
			                                dockedItems: [
			                                    {
			                                        xtype: 'toolbar',
			                                        dock: 'bottom',
			                                        height: 50,
			                                        layout: {
			                                            type: 'hbox',
			                                            align: 'middle'
			                                        },
			                                        items: [
	                                                {
	                                                    xtype: 'container',
	                                                    flex: 1,
	                                                    layout: {
	                                                        type: 'hbox',
	                                                        align: 'middle'
	                                                    },
	                                                    items: [
	                                                        {
	                                                            xtype: 'container',
	                                                            maxWidth: 6,
	    			                                            minWidth: 6
	                                                        },
			                                                    {
			                                                        xtype: 'button',
			                                                        html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
			                                                        height: 40,
			                                                        width: 50,
				                                                    padding: 0,
			                                                        listeners: {
				                    	                                click: 'onNewMatConfClick'
				                    	                            }
			                                                    },
			                                                    {
			                                                        xtype: 'label',
			                                                        flex: 1,
			                                                        text: Locale.getMsg('addMatConf')
			                                                    }
			                                                ]
			                                         	}
			                                        ]
			                                    }
		                                    ],
		                                   /* columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/matconf.png"/>';
		                                            },
		                                            id: 'orderMatConfImgColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                        	id: 'orderMatConfOperColumn',
		                                            dataIndex: 'vornr',
			                                        maxWidth: 100,
			                                        minWidth: 100,
		                                            text: Locale.getMsg('operation')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderMatConfColumn',
		                                            flex: 3,
		                                            dataIndex: 'matnr',
		                                            text: Locale.getMsg('material'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
		                                        		var maktx = record.get('material') ? record.get('material').get('maktx') : '';

		                                        		return AssetManagement.customer.utils.StringUtils.concatenate([ matnr, maktx ], null, true);
		                                            }
		                                        },
		                                        {
		                                        	//column account Indication
		                                            xtype: 'gridcolumn',
		                                        	id: 'orderMatConfBemotColumn',
		                                            dataIndex: 'bemot',
		                                            flex: 2,
		                                            text: Locale.getMsg('accountIndication'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	var retval = '';

		                                        		try {
		                                        			var accountIndication = record.get('accountIndication');

				                                        	retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
		                                        		} catch(ex) {
		                                        			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of matConfGrid/bemot-column in BaseOrderDetailPage', ex);
		                                        		}

		                                        		return retval;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderMatConfUnitColumn',
			                                        maxWidth: 160,
			                                        minWidth: 160,
		                                            text: Locale.getMsg('orderComponents_Qty'),
		                                            dataIndex: 'quantity',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    		var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('quantity'));
			                                    		var unit = record.get('unit');

			                                    		return quantity + ' ' + unit;
		                                            }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderMatConfFactoryColumn',
			                                        maxWidth: 160,
			                                        minWidth: 160,
		                                            text: Locale.getMsg('plantStorLoc'),
		                                            dataIndex: 'plant',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                    		var plant = record.get('plant');
			                                    		var storloc = record.get('storloc');

			                                    		return AssetManagement.customer.utils.StringUtils.concatenate([ plant, storloc ], '/');
		                                            }
		                                        }
		                                    ],*/
		                                    listeners: {
												cellcontextmenu: myController.onCellContextMenuMatConfList,
			                                	scope: myController
											}
			                             }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'orderDetailPageSDOrderItemsSection',
	                                margin: '40 5 0 5',
	                                hideContextMenuColumn: true,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('materialOrders')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'orderDetailPageSDOrderItemsGridPanel',
		                                    hideContextMenuColumn: true,
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                parentController: myController,
			                                owner: me,
		                                    dockedItems: [
		                                         {
			                                        xtype: 'toolbar',
			                                        dock: 'bottom',
			                                        height: 50,
			                                        layout: {
			                                            type: 'hbox',
			                                            align: 'middle'
			                                        },
			                                        items: [
	                                                {
	                                                    xtype: 'container',
	                                                    flex: 1,
	                                                    layout: {
	                                                        type: 'hbox',
	                                                        align: 'middle'
	                                                    },
	                                                    items: [
			                                                    {
			                                                        xtype: 'button',
			                                                        margin: '0 0 0 6',
			                                                        html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
			                                                        height: 40,
			                                                        width: 50,
				                                                    padding: 0,
			                                                        listeners: {
				                    	                                click: 'onAddSDOrderItemClick'
				                    	                            }
			                                                    },
			                                                    {
			                                                        xtype: 'label',
			                                                        flex: 1,
			                                                        text: Locale.getMsg('addSDOrder')
			                                                    }
			                                                ]
			                                         	}
			                                        ]
			                                    }
			                                ],
		                                    /*columns: [
		                                        {
				                                	xtype: 'actioncolumn',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    return '<img src="resources/icons/demand_requirement.png"/>';
					                                },
					                                enableColumnHide: false,
					                                maxWidth: 80,
				                                    minWidth: 80,
				                                    align: 'center'
				                                },
				                                {
				                                    xtype: 'gridcolumn',
					                                maxWidth: 140,
					                                minWidth: 140,
				                                    text: Locale.getMsg('sdOrderPosition'),
				                                    dataIndex: 'bstnk',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                	var bstnk = AssetManagement.customer.utils.StringUtils.trimStart(record.get('bstnk'), '0');
					                                    var posnr = record.get('posnr');
					                                    return bstnk +'<p>'+posnr+'</p>';
					                                },
					                                enableColumnHide: false
				                                },
				                                {
				                                    xtype: 'gridcolumn',
				                                    flex: 1,
				                                    text: Locale.getMsg('material'),
				                                    dataIndex: 'matnr',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                	var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
					                            		var maktx = record.get('material') ? record.get('material').get('maktx') : '';

					                            		return AssetManagement.customer.utils.StringUtils.concatenate([ matnr, maktx ], null, true);
					                                },
					                                enableColumnHide: false
				                                },
				                                {
				                                    xtype: 'gridcolumn',
					                                maxWidth: 120,
					                                minWidth: 120,
				                                    text: Locale.getMsg('quantityUnit'),
				                                    dataIndex: 'zmeng',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng'));
				                                		var unit = record.get('zieme');

				                                		return quantity + ' ' + unit;
					                                },
				                                    enableColumnHide: false
				                                },
				                                {
					                                xtype: 'gridcolumn',
					                                maxWidth: 130,
					                                minWidth: 130,
					                                dataIndex: 'lfsga',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		return record.getDeliveryStatus();
					                             	},
					                                enableColumnHide: false,
					                                text: Locale.getMsg('deliveryStatus')
					                            },
				                                {
				                                    xtype: 'gridcolumn',
				                                    flex: 2,
				                                    dataIndex: 'arktx',
				                                    text: Locale.getMsg('remarks'),
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				                                		return record.get('arktx');
					                                },
				                                    enableColumnHide: false
				                                }
		                                    ],*/
			                                listeners: {
			                                	cellclick: myController.onSDOrderItemSelected,
			                                	cellcontextmenu: myController.onCellContextMenuSDOrderList,
			                                	scope: myController
		    	                        	}
		                                }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'filesAndDocumentsSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('filesAndDocuments')
                                        },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'orderFileGridPanel',
		                                    margin: '20 0 0 0',
		                                    disableSelection: true,
		                                    parentController: myController,
		                                    owner: me,
			                                dockedItems: [
	                                        {
	                                            xtype: 'toolbar',
	                                            dock: 'bottom',
	                                            height: 50,
	                                            layout: {
	                                                type: 'hbox',
	                                                align: 'top'
	                                            },
	                                            items: [
	                                                {
	                                                    xtype: 'container',
	                                                    flex: 1,
	                                                    id: 'ordDetailAddFilesToolbar',
	                                                    layout: {
	                                                        type: 'hbox',
	                                                        align: 'stretch'
	                                                    },
	                                                    items: [
	                                                        {
	                                                            xtype: 'container',
	                                                            flex: 1,
	                                                            id: 'ordDetailAddFilesToolbarItem',
	                                                            layout: {
	                                                                type: 'hbox',
	                                                                align: 'middle'
	                                                            },
	                                                            items: [
	                                                                {
	                                                                    xtype: 'oxfileselectbutton',
				                                                        margin: '0 7 0 7',
				                                                        height: 38,
				                                                        selectionCallback: this.getController().onFileForDocUploadSelected,
				                                                        selectionCallbackScope: this.getController()
	                                                                }, {
	                                                                    xtype: 'label',
	                                                                    text: Locale.getMsg('addFilesAndDocuments')
	                                                                }
	                                                            ]
	                                                        },
	                                                        {
	                                                            xtype: 'container',
	                                                            flex: 1,
	                                                            id: 'ordDetailAddOrderReportToolbarItem',
	                                                            layout: {
	                                                                type: 'hbox',
	                                                                align: 'middle'
	                                                            },
	                                                            items: [
	                                                                {
	                                                                    xtype: 'button',
	                                                                    html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
	                                                                    height: 40,
	                                                                    width: 50,
	                                                                    padding: 0,
	                                                                    listeners: {
					                    	                                click: 'onAddOrderReportClick'
					                    	                            }
	                                                                },
	                                                                {
	                                                                    xtype: 'label',
	                                                                    text: Locale.getMsg('addOrderReport')
	                                                                }
	                                                            ]
	                                                        }
	                                                    ]
	                                                }
	                                            ]
	                                        }
	                                    ],

		                                /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                                 return '<img src="resources/icons/document.png"/>';
		                                            },
		                                            id: 'orderFileIconColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'

		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderFileNameDescColumn',
		                                            dataIndex: 'fileDescription',
		                                            text: Locale.getMsg('file'),
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var description = record.get('fileDescription');

			                                            return description;
		                                    		},
		                                    		flex: 2
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderFileTypeSizeColumn',
		                                            text: Locale.getMsg('typeAndSize'),
		                                            dataIndex: 'fileType',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                                        		var type = record.get('fileType');
	                                        		var size = record.get('fileSize').toFixed(2) + ' kB';

		                                            return type +'<p>' + size + '</p>';
			                                    	},
			                                    	flex: 1
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderFileOriginColumn',
		                                            text:  Locale.getMsg('fileStorageLocation'),
			                                        dataIndex: 'fileOrigin',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var stringToShow = "";
		                                        		var fileOrigin = record.get('fileOrigin');

		                                        		if(fileOrigin === "online") {
		                                        			stringToShow = Locale.getMsg('sapSystem');
		                                        		} else if(fileOrigin === "local") {
		                                        			stringToShow = Locale.getMsg('local');
		                                        		}

			                                            return stringToShow;
				                                    },
			                                        flex: 1
		                                        }
		                                    ],*/
											listeners: {
												cellclick: myController.onFileSelected,
												cellcontextmenu: myController.onCellContextMenuFileList,
			                                	scope: myController
											}
		                                }
		                            ]
		                        },
                                {
	                                xtype: 'container',
	                                id: 'orderDetailpartnerGridItemsSection',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('partner')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'partnerGridPanel',
			                                margin: '20 0 0 0',
			                                disableSelection: true,
			                                hideContextMenuColumn: true,
			                                parentController: myController,
			                                owner: me,
		                                   /* columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/partner.png"/>';
		                                            },
		                                            id: 'orderPartnerImgColumn',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            height: 30,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderPartnerRoleColumn',
			                                        maxWidth: 110,
			                                        minWidth: 110,
		                                            dataIndex: 'parvw',
		                                            text: Locale.getMsg('partnerFunction_Short')
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderPartnerNameColumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('name'),
		                                            dataIndex: 'name1',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var nameLine1 = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('name1'), record.get('name2') ], null, true);
		                                        		var nameLine2 = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('name3'), record.get('name4') ], null, true);

		                                        		return nameLine1 + '<p>' + nameLine2 + '</p>';
			                                    	}
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('contact'),
		                                            dataIndex: 'telnumber',
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                        	var tel = record.get('telnumber');
			                                        	var telExtens = record.get('telextens');
			                                        	var telMob = record.get('telnumbermob');

			                                        	if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tel)) {
			                                        		tel = 'Tel.: ' + tel;

			                                        		if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telExtens))
			                                        			tel += ' ' + telExtens;
			                                        	}

			                                        	if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(telMob)) {
			                                        		telMob = 'Mobil: ' + telMob;
			                                        	}

			                                        	return tel + '<p>' + telMob + '</p>';
			                                    	}
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'orderPartnerAdressColumn',
		                                            flex: 1,
		                                            text: Locale.getMsg('address'),
		                                            dataIndex: 'city1',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var street = '';
			                                            var postalCode = '';

			    										street = AssetManagement.customer.utils.StringUtils.concatenate([record.get('street'), record.get('housenum1') ], null, true);
			    										postalCode = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('postcode1'), record.get('city1') ], null, true);


			                                            return street + '<p>' + postalCode + '</p>';
		                                        	},
		                                        	enableColumnHide: false
		                                        },
		                                        {
		                                            xtype: 'actioncolumn',
		                                            id: 'orderPartnerMapsColumn',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('city1')) &&
					                            				AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('postcode1'))) {
					                            			metaData.style = 'display: none;';
					                            		} else {
					                            			metaData.style = '';
					                            		}
					                            	},
			                                        text: Locale.getMsg('maps'),
			                                        maxWidth: 80,
			                                        width: 80,
			                                        items: [{
			                                            icon: 'resources/icons/maps_small.png',
			                                            tooltip: 'Google Maps',
			                                            iconCls: 'oxGridLineActionButton',
			                                            handler: function(grid, rowIndex, colIndex) {
			                                                var partner = grid.getStore().getAt(rowIndex);
			                                                this.getController().navigateToPartnerAddress(partner);
			                                            },
			                                            scope: this
			                                        }],
			                                        enableColumnHide: false,
			                                        align: 'center'
		                                        }
		                                    ], */
		                                    listeners: {
			                                	cellcontextmenu: myController.onCellContextMenuPartnerList,
			                                	scope: myController
			                                }
			                            }
                                    ]
                                },
		                        {
		                            xtype: 'component',
		                            height: 30
		                        }
                            ]
                        },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];
			this.add(items);

			var leftGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._orderRow = leftGrid.addRow(Locale.getMsg('order'), 'multilinelabel', true);
			this._orderTypeRow = leftGrid.addRow(Locale.getMsg('orderType'), 'label', true);
			this._ilartRow = leftGrid.addRow(Locale.getMsg('activityType'), 'label', true);
			this._checklistComp = leftGrid.addRow(Locale.getMsg('ChecklistsCompl'), 'label', true);
			this._notifRow = leftGrid.addRow(Locale.getMsg('notification'), 'link', false);
			this._notifRow.getTemplate().addListener('click', this.getController().navigateToNotif, this);
			this._berRow = leftGrid.addRow(Locale.getMsg('accountingIndicator_Short'), 'combobox', true);
			this._berRow.setHeight('30px');
			//this._notifRow.addCls('ordDetailNotifNrLinkCSS');

			// The rightGrig will be shown if the page width is over 800.
			var rightGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid', {});

    	    //will be used when there is no qplos
			this._equipmentRowRight = rightGrid.addRow(Locale.getMsg('equipment'), 'link', true);
			this._equipmentRowRight.getTemplate().addListener('click', this.getController().navigateToEquipment, this);
			this._funcLocRowRight = rightGrid.addRow(Locale.getMsg('funcLoc_Short'), 'link', true);
			this._funcLocRowRight.getTemplate().addListener('click', this.getController().navigateToFuncLoc, this);
    	    //

			//this._systemStatusRow = rightGrid.addRow(Locale.getMsg('systemStatus'), 'label', true);

    	    //will be used when there is qplos nor matnr
			var checklistTemplate = Ext.create('AssetManagement.customer.view.utils.ChecklistTemplate');
			this._equipmentRowRightChecklist = rightGrid.addRow(Locale.getMsg('equipment'), checklistTemplate, true);

			var checklistTemplate2 = Ext.create('AssetManagement.customer.view.utils.ChecklistTemplate');
			this._funcLocRowRightChecklist = rightGrid.addRow(Locale.getMsg('funcLoc_Short'), checklistTemplate2, true);
    	    //


			var objectAddressTemplate = Ext.create('AssetManagement.customer.view.utils.AddressTemplate');
			this._objectAddressRowRight = rightGrid.addRow(Locale.getMsg('objectAddress'), objectAddressTemplate, true);

			var customerAddressTemplate = Ext.create('AssetManagement.customer.view.utils.AddressTemplate');
			this._customerAddressRowRight = rightGrid.addRow(Locale.getMsg('customerAddress'), customerAddressTemplate, false);

			this._userStatus = rightGrid.addRow(Locale.getMsg('userStatus'), 'link', true);
			this._userStatus.getTemplate().addListener('click', this.getController().navigateToUserStatus, this);


			// The bottomGrid will be hidden if the page width is over 800.
			var bottomGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._equipmentRowBottom = bottomGrid.addRow(Locale.getMsg('equipment'), 'link', true);
			this._equipmentRowBottom.getTemplate().addListener('click', this.getController().navigateToEquipment, this);
			this._funcLocRowBottom = bottomGrid.addRow(Locale.getMsg('funcLoc_Short'), 'link', true);
			this._funcLocRowBottom.getTemplate().addListener('click', this.getController().navigateToFuncLoc, this);

			this._userStatusBottom = bottomGrid.addRow(Locale.getMsg('userStatus'), 'link', true);
			this._userStatusBottom.getTemplate().addListener('click', this.getController().navigateToUserStatus, this);

    	    //will be used when there is qplos or matnr
			var checklistTemplateBot = Ext.create('AssetManagement.customer.view.utils.ChecklistTemplate');
			this._equipmentRowRightChecklistBottom = bottomGrid.addRow(Locale.getMsg('equipment'), checklistTemplateBot, true);

			var checklistTemplateBot2 = Ext.create('AssetManagement.customer.view.utils.ChecklistTemplate');
			this._funcLocRowRightChecklistBottom = bottomGrid.addRow(Locale.getMsg('funcLoc_Short'), checklistTemplateBot2, true);
    	    //
    	    
			objectAddressTemplate = Ext.create('AssetManagement.customer.view.utils.AddressTemplate');
			this._objectAddressRowBottom = bottomGrid.addRow(Locale.getMsg('objectAddress'), objectAddressTemplate, true);

			customerAddressTemplate = Ext.create('AssetManagement.customer.view.utils.AddressTemplate');
			this._customerAddressRowBottom = bottomGrid.addRow(Locale.getMsg('customerAddress'), customerAddressTemplate, false);

	        this.queryById('leftGrid').add(leftGrid);
	        this.queryById('rightGrid').add(rightGrid);
	        this.queryById('bottomGrid').add(bottomGrid);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseOrderDetailPage', ex);
		}
    },

    getPageTitle: function() {
	    var title = '';

	    try {
    	    title = Locale.getMsg('order');

    	    var order = this.getViewModel().get('order');

    	    if(order) {
    		    title += " " + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(order.get('aufnr'));
    	    }
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseOrderDetailPage', ex);
		}

    	return title;
	},

	//protected
	//@override
    updatePageContent: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');

			if(order) {
				if(this.getViewModel().get('bemot') !== null && this.getViewModel().get('bemot').data.items.length == 1) {
					if(this.getViewModel().get('bemot').data.items[0].get('order') == order.get('aufnr'))
						order.set('bemot', this.getViewModel().get('bemot').data.items[0].get('bemot'));
				}

				this.fillMainDataSection();

				this.manageSections();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseOrderDetailPage', ex);
		}
    },

    fillMainDataSection: function () {
        try {
        	var myModel = this.getViewModel();
        	
            var order = myModel.get('order');
            this._orderRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0') + " " + order.get('ktext'));

            var orderType = order.get('orderType');
            var orderTypeString = '';
            if (orderType) {
                orderTypeString = orderType.get('auart') + " " + orderType.get('txt')
            } else {
                orderTypeString = order.get('auart');
            }

            this._orderTypeRow.setContentString(orderTypeString);

            var orderIlart = order.get('orderIlart');
            var orderIlartString = '';
            if (orderIlart) {
                orderIlartString = orderIlart.get('ilart') + " " + orderIlart.get('ilatx')
            } else {
                orderIlartString = order.get('ilart');
            }

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(orderIlartString)) {
                this._ilartRow.show();
                this._ilartRow.setContentString(orderIlartString);
            } else {
                this._ilartRow.hide();
                this._ilartRow.setContentString('');
            }

            var notif = order.get('notif');

            var funcLoc = order.get('funcLoc');
            var equi = order.get('equipment');

            var matnr = '';

            var qplos = order.get('qplos');
            var techObj = null;
            if (qplos) {
                techObj = qplos.get('techObj');
            } else {
                techObj = equi ? equi : funcLoc;
            }

            var checklistOnHeader = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_obj_list_active') === '';
            var qplanActive = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('use_qplan') === 'X';

            var techObjClassName = Ext.getClassName(techObj);
            var funcLocQplos = techObjClassName === 'AssetManagement.customer.model.bo.FuncLoc';

            if (funcLoc) {
                matnr = funcLoc.get('submt');
                var funcLocString = funcLoc.getDisplayIdentification() + " " + funcLoc.get('pltxt');
                if (checklistOnHeader && funcLocQplos && (qplos || (matnr && qplanActive))) {
                    var rowData = Ext.create('Ext.data.Model', {
                        controller: this.getController(),
                        qplos: qplos,
                        matnr: matnr,
                        text: funcLocString
                    });

                    this._funcLocRowRightChecklist.show();
                    this._funcLocRowRightChecklist.setDataForCustomTemplate(rowData);

                    this._funcLocRowRightChecklistBottom.show();
                    this._funcLocRowRightChecklistBottom.setDataForCustomTemplate(rowData);

                    this._funcLocRowRight.hide();
                    this._funcLocRowBottom.hide();
                }
                else {
                    this._funcLocRowRight.show();
                    this._funcLocRowRight.setContentString(funcLocString);
                    this._funcLocRowRightChecklist.hide();
                    this._funcLocRowRightChecklistBottom.hide();
                    this._funcLocRowBottom.show();
                    this._funcLocRowBottom.setContentString(funcLocString);
                }

            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('tplnr'))) {
                this._funcLocRowRight.show();
                this._funcLocRowRight.setContentString(order.get('tplnr'));
                this._funcLocRowRightChecklist.hide();
                this._funcLocRowRightChecklistBottom.hide();

                this._funcLocRowBottom.show();
                this._funcLocRowBottom.setContentString(order.get('tplnr'));
            } else {
                this._funcLocRowRight.hide();
                this._funcLocRowRightChecklist.hide();
                this._funcLocRowBottom.hide();
                this._funcLocRowRightChecklistBottom.hide();
            }


            var equipmentQplos = techObjClassName === 'AssetManagement.customer.model.bo.Equipment';

            if (equi) {
                matnr = equi.get('matnr');
                var equipmentSting = AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0') + " " + equi.get('eqktx');
                if (checklistOnHeader && equipmentQplos && (qplos || (matnr && qplanActive))) {
                    var rowData = Ext.create('Ext.data.Model', {
                        controller: this.getController(),
                        qplos: qplos,
                        matnr: matnr,
                        text: equipmentSting
                    });

                    this._equipmentRowRightChecklist.show();
                    this._equipmentRowRightChecklist.setDataForCustomTemplate(rowData);

                    this._equipmentRowRightChecklistBottom.show();
                    this._equipmentRowRightChecklistBottom.setDataForCustomTemplate(rowData);

                    this._equipmentRowRight.hide();
                    this._equipmentRowBottom.hide();
                }
                else {
                    this._equipmentRowRight.show();
                    this._equipmentRowRight.setContentString(equipmentSting);
                    this._equipmentRowRightChecklist.hide();
                    this._equipmentRowRightChecklistBottom.hide();
                    this._equipmentRowBottom.show();
                    this._equipmentRowBottom.setContentString(equipmentSting);
                }

            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('equnr'))) {
                this._equipmentRowRight.show();
                this._equipmentRowRight.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0'));
                this._equipmentRowRightChecklist.hide();
                this._equipmentRowRightChecklistBottom.hide();

                this._equipmentRowBottom.show();
                this._equipmentRowBottom.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0'));
            } else {
                this._equipmentRowRight.hide();
                this._equipmentRowRightChecklist.hide();
                this._equipmentRowBottom.hide();
                this._equipmentRowRightChecklistBottom.hide();
            }

            if (notif && notif.get('qmnum') !== '') {
                this._notifRow.show();
                this._notifRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(notif.get('qmnum'), '0') + " " + notif.get('qmtxt'));
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('qmnum'))) {
                this._notifRow.show();
                this._notifRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(order.get('qmnum'), '0'));
            } else if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('noti_create') === 'X') {
                this._notifRow.show();
                this._notifRow.setContentString(Locale.getMsg('createNewNotif'));
            } else {
                this._notifRow.hide();
                this._notifRow.setContentString('');
            }

			var orderTypes = myModel.get('orderTypes');

            var selectedAuart = orderTypes.findRecord('auart', order.get('auart'));
            var isService = selectedAuart.get('service') === 'X' ? true : false;
           	if(isService) {
                this.fillBemotDropdown(this._berRow.template.getComboBox());
                this._berRow.show();
			} else {
				this._berRow.hide()
			}

            //this row is not displayed if there is no oblject list, because we show the checklist on order header level
            var objList = this.getViewModel().get('order').get('objectList');
            var checklistsCount = 0;
            var checklistsCompletedCount = 0;
            if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_obj_list_active') === 'X') {
                if (objList && objList.getCount() > 0) {
                    objList.each(function (objItem) {
                        var qplos = objItem.get('qplos');
                        if (qplos) {
                            checklistsCount++;
                            if (qplos.isCompleted())
                                checklistsCompletedCount++;
                        }
                    });
                }
            }

            if (checklistsCount > 0) {
                this._checklistComp.setContentString(checklistsCompletedCount + ' / ' + checklistsCount);
                this._checklistComp.show();
            } else {
                this._checklistComp.hide();
            }

            var partners = order.get('partners');
            var customerAddress = null;

            if (partners) {
                // get customer address
                if (partners.getData().length === 1) {
                    customerAddress = partners.getAt(0);
                } else {
                    if (partners && partners.getCount() > 0) {
                        partners.each(function (partner) {
                            var parvw = partner.get('parvw');

                            if (parvw === 'AG')
                                customerAddress = partner;
                        });
                    }
                }
            }

            // show/hide customer address
            if (customerAddress) {
                this._customerAddressRowRight.show();
                this._customerAddressRowRight.setDataForCustomTemplate(customerAddress);

                this._customerAddressRowBottom.show();
                this._customerAddressRowBottom.setDataForCustomTemplate(customerAddress);
            } else {
                this._customerAddressRowRight.hide();
                this._customerAddressRowBottom.hide();
            }

            var objectAddress = null;

            // get object address
            if (equi !== null && equi !== undefined && equi.get('address')) {
                objectAddress = equi.get('address');
            } else if (funcLoc !== null && funcLoc !== undefined && funcLoc.get('address')) {
                objectAddress = funcLoc.get('address');
            }

            if (!objectAddress) {
                if (partners && partners.getCount() > 0) {

                    partners.each(function (partner) {
                        objectAddress = partner.get('address');

                        if (objectAddress)
                            return false;
                    });
                }
            }

            // show/hide object address
            if (objectAddress !== null && objectAddress !== undefined) {
                this._objectAddressRowRight.show();
                this._objectAddressRowRight.setDataForCustomTemplate(objectAddress);

                this._objectAddressRowBottom.show();
                this._objectAddressRowBottom.setDataForCustomTemplate(objectAddress);
            } else {
                this._objectAddressRowRight.hide();
                this._objectAddressRowBottom.hide();
            }

            var orderStatus = order.get('objectStatusList');

            //set checked Userstatus to the row
            var orderStatusString = '';

            if (orderStatus) {
                orderStatus.each(function (oStatus) {
                    orderStatusString += oStatus.get('txt04') + ',';
                })
            } else {
                orderStatusString = '';
            }

            this._userStatus.setContentString(orderStatusString.slice(0, -1));
            this._userStatusBottom.setContentString(orderStatusString.slice(0, -1));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseOrderDetailPage', ex);
        }
    },


    /**
     * Loads the bemot data from the database and fills the bemot-combobox
     */
	fillBemotDropdown: function(bemotCb) {
		try {
			if(bemotCb != null) {
				var bemotList = this.getViewModel().get('bemotTypes');

				var store = Ext.create('Ext.data.Store', {
				    fields: ['text', 'value']
				});
				var currentBemot = null;
				bemotList.each(function(bemot) {
					store.add({ text: (bemot.get('bemot') + ' - ' + bemot.get('bemottxt')), value: bemot.get('bemot') });

					if(bemot.get('bemot') === this.getViewModel().get('order').get('bemot'))
						currentBemot = bemot.get('bemot');
				}, this);

				bemotCb.clearValue();
				bemotCb.setStore(store);
				bemotCb.addListener('select', 'onOrderBemotSelected');
				var foundBemot = bemotCb.findRecordByValue(currentBemot);

				//set value if order.bemot is already set
				if(foundBemot) {
					bemotCb.setValue(foundBemot);
				}
			}
		}
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillBemotDropdown of NewOrderPage', ex);
		}
	},

	manageSections: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');

			var operStore = order.get('operations');
			var operGridPanel = Ext.getCmp('operGridPanel');
			operGridPanel.setStore(operStore);

			var timeConfStore = myModel.get('timeConfs');
			var timeConfGridPanel = Ext.getCmp('orderTimeConfPanel');
			timeConfGridPanel.setStore(timeConfStore);

			var matConfStore = myModel.get('matConfs');
			var matConfGridPanel = Ext.getCmp('orderMatConfPanel');
			matConfGridPanel.setStore(matConfStore);

			var extConfsStore = order.get('extConfirmations');
			var extConfGridPanel = Ext.getCmp('orderDetailExtConfsItemsGridPanel');
			extConfGridPanel.setStore(extConfsStore);

			var componentsStore = order.get('components');
			var componentGridPanel = Ext.getCmp('componentGridPanelOrderDetail');
			componentGridPanel.setStore(componentsStore);

			var fileStore = order.get('documents');
			var fileGridPanel = Ext.getCmp('orderFileGridPanel');
			fileGridPanel.setStore(fileStore);

			var objectListStore = order.get('objectList');
			var objectListPanel = Ext.getCmp('objectGridPanel');
			objectListPanel.setStore(objectListStore);

			var partnerStore = order.get('partners');
			var partnerGridPanel = Ext.getCmp('partnerGridPanel');
			partnerGridPanel.setStore(partnerStore);

			var extConfGridPanel = Ext.getCmp('orderDetailExtConfsItemsGridPanel');
			var extConfsSection = Ext.getCmp('orderDetailExtConfsItemsSection');

			var timeConfsSection = Ext.getCmp('orderDetailTimeConfSection');

			var sdSection = Ext.getCmp('orderDetailPageSDOrderItemsSection');
			var sdOrderItemsGridPanel = Ext.getCmp('orderDetailPageSDOrderItemsGridPanel');

			var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
			var funcParas = AssetManagement.customer.model.bo.FuncPara.getInstance();
		    //ext_scen is customer specific!!
			//if (funcParas.getValue1For('ext_scen_active') !== 'X') {
				//SD Orders
				//sdSection.hide();
				//sdOrderItemsGridPanel.reset();
				sdSection.hide();
				//external confirmations
				extConfsSection.hide();
				timeConfsSection.show();
			//} else {
			//	//SD Orders
				sdOrderItemsGridPanel.setStore(order.getAllSDItems());
			//	sdSection.show();
			//
			//	//external confirmations
			//	extConfsSection.show();
			//	timeConfsSection.hide();
			//}

			//check customizing parameters
			if (funcParas.getValue1For('oper_create_active') !== 'X')
				Ext.getCmp('operationListToolbar').setHidden(true);

			//component active
			if (funcParas.getValue1For('ord_comp_active') === 'X') {
				Ext.getCmp('orderDetailComponentGridPanelItemsSection').setHidden(false);
			} else {
				Ext.getCmp('orderDetailComponentGridPanelItemsSection').setHidden(true);
			}

			//matConf active
			if (funcParas.getValue1For('matconf_active') !== 'X') {
				Ext.getCmp('materialConfirmationsItemsSection').setHidden(true);
			} else {
				Ext.getCmp('materialConfirmationsItemsSection').setHidden(false);
			}

			//component create active
			if (funcParas.getValue1For('ord_comp_create') !== 'X')
				Ext.getCmp('componentGridToolbar').setHidden(true);
			else
				Ext.getCmp('componentGridToolbar').setHidden(false);

			//files and documents
			if (funcParas.getValue1For('ord_documents_active') !== 'X') {
				Ext.getCmp('filesAndDocumentsSection').setHidden(true);
			} else {
				Ext.getCmp('filesAndDocumentsSection').setHidden(false);
			}

			//documents create
			if (funcParas.getValue1For('ord_documents_create') !== 'X')
				Ext.getCmp('ordDetailAddFilesToolbar').setHidden(true);
			else
				Ext.getCmp('ordDetailAddFilesToolbar').setHidden(false);

			//object list
			if (funcParas.getValue1For('ord_obj_list_active') !== 'X')
			{
				Ext.getCmp('orderDetailObjectGridItemsSection').setHidden(true);
			} else {
				Ext.getCmp('orderDetailObjectGridItemsSection').setHidden(false);
			}

			//partner active
			if (funcParas.getValue1For('ord_partner_active') !== 'X') {
				Ext.getCmp('orderDetailpartnerGridItemsSection').setHidden(true);
			} else {
				Ext.getCmp('orderDetailpartnerGridItemsSection').setHidden(false);
			}

			//order report active
			if (funcParas.getValue1For('ord_pdf_rep_active') !== 'X')
				Ext.getCmp('ordDetailAddOrderReportToolbarItem').setHidden(true);
			else
				Ext.getCmp('ordDetailAddOrderReportToolbarItem').setHidden(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSections of BaseOrderDetailPage', ex);
		}
	}

});
