Ext.define('AssetManagement.base.view.pages.BaseIntegratedConfPage', {
        extend: 'AssetManagement.customer.view.pages.OxPage',


        requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.IntegratedConfPageViewModel',
        'AssetManagement.customer.controller.pages.IntegratedConfPageController',
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConf',     
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConf',    
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_LongtextNotif',
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_Header',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.view.OxGridPanel',
        'Ext.grid.column.Date',
        'Ext.grid.View',
        'AssetManagement.customer.AppConfig'
    ],
    
        inheritableStatics: {
        _instance: null,
        _generalTimeConfDefinition: null,
        _generalTimeConfUid: null,
        _headerDefinition: null,
        _headerUid: null,
        _matConfDefinition: null,
        _matConfUid: null,
        _longtextNotifDefinition: null,
        _longtextNotifUid: null,

		
        getInstance: function() {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.IntegratedConfPage');
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseIntegratedConfPage', ex);
            }
	        
            return this._instance;
        }
    },

        viewModel: {
        type: 'IntegratedConfPageModel'
    },
    
        controller: 'IntegratedConfPageController',

    
        buildUserInterface: function() {
            try {
                var myController = this.getController();
                this._headerDefinition = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_Header.getUiDefinition(myController);
                this._headerUid = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_Header.getUid();
                this._generalTimeConfDefinition = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConf.getUiDefinition(myController);
                this._generalTimeConfUid = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConf.getUid();     
                this._matConfDefinition = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConf.getUiDefinition(myController);
                this._matConfUid = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConf.getUid();        
                this._longtextNotifDefinition = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_LongtextNotif.getUiDefinition(myController);
                this._longtextNotifUid = AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_LongtextNotif.getUid();
    		
                var items = [{
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            //leftContainer 
                            {
                                        xtype: 'component',
                                        maxWidth: 10,
                                        minWidth: 10
                                    },
                            //middle Container
                            {
                                        xtype: 'container',
                                        flex: 1,
                                        layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                        items: [
                                                this._headerDefinition,
                                                this._generalTimeConfDefinition,           
                                                this._matConfDefinition,
                                                this._longtextNotifDefinition
                                            ]
                                    },
                            //right container
                            {
                                        xtype: 'component',
                                        width: 30
                                    }
                        ]
                    }
                ];
		     
                this.add(items);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseIntegratedConfPage', ex);
            }
        },
        
        getMatConfView: function() {
            try {
                return AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConf;        
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatConfView of BaseIntegratedConfPage', ex);
            }
        },
    
        getTimeConfView: function() {
            try {
                return AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConf;        
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeConfView of BaseIntegratedConfPage', ex);
            }
        }, 
    
        getLongtextNotifView: function() {
            try {
                return AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_LongtextNotif;        
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLongtextNotifView of BaseIntegratedConfPage', ex);
            }
        },    
    
        getPageTitle: function() {
            var retval = '';
    
            try {
                var title = Locale.getMsg('confirmations');
    	
                var operation = this.getViewModel().get('operation');
    	
                if (operation) {
                    title += " " + operation.get('vornr');
                }
    		
                retval = title;
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseIntegratedConfPage', ex);
            }

            return retval;
        },

        //protected
        //@override
        updatePageContent: function() {
            try {
                this.fillDropDownBoxes(); 
			
                this.transferModelStateIntoView();
			
                //refresh timeConfs
                this.refreshTimeConfPanel();    
                //check if operation is relevant for general costs or work costs
                var work = this.controller.isWorkCostOperation();
                if(work) {
                    this._matConfDefinition.setHidden('false');
                    this._longtextNotifDefinition.setHidden('false');
                    this._matConfDefinition.show();
                    this._longtextNotifDefinition.show();
                     //refresh timeConfs
                    this.refreshMatConfPanel(); 
                    //refresh Longtext
                    this.refreshLongtextPanel();                        
                }
                else {                          
                    this._matConfDefinition.setHidden('true');
                    this._longtextNotifDefinition.setHidden('true');
                }
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseIntegratedConfPage', ex);
            }
        },

        //fill drop down boxes operations, activity types, bemots
        fillDropDownBoxes: function() {
            try {
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_Header.fillDropDownBoxes();
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConf.fillDropDownBoxes(); 
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConf.fillDropDownBoxes();
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseIntegratedConfPage', ex);
            }
        },
	
        transferModelStateIntoView: function() {
            try {                                                      
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_Header.transferModelStateIntoView();    
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConf.transferModelStateIntoView(); 
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConf.transferModelStateIntoView(); 
                AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_LongtextNotif.transferModelStateIntoView();
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseIntegratedConfPage', ex);
            }
        },
	
        setWorkFieldConvienentToCurrentModel: function() {
            try {
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setWorkFieldConvienentToCurrentModel of BaseIntegratedConfPage', ex);
            }
        },
	
        setDropDownBoxesConvenientToCurrentModel: function() {
            try {
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDropDownBoxesConvenientToCurrentModel of BaseIntegratedConfPage', ex);
            }
        },

        initializeNewObjects: function() {
            try {
                if (!this.getViewModel().get('timeConf'))
                    this.getController().mixins.mixinUIICP_GeneralTimeConfController.initializeNewTimeConf.call(this, true);    
                if (!this.getViewModel().get('matConf'))
                    this.getController().mixins.mixinUIICP_MatConfController.initializeNewMatConf.call(this, true);  
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initilizeNewObjects of BaseIntegratedConfPage', ex);
            }
        },
	
        refreshTimeConfPanel: function() {
            try {
                var timeConfGridPanel = Ext.getCmp(this._generalTimeConfUid+'timeConfGrid');
			    timeConfGridPanel.setStore(this.getViewModel().get('operation').get('timeConfs'));
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshTimeConfPanel of BaseIntegratedConfPage', ex);
            }
        },  
	
        refreshMatConfPanel: function() {
            try {
                var matConfGridPanel = Ext.getCmp(this._matConfUid+'matConfGrid');
			    matConfGridPanel.setStore(this.getViewModel().get('operation').get('matConfs'));
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshMatConfPanel of BaseIntegratedConfPage', ex);
            }
        },
	
        refreshLongtextPanel: function() {
            try {
                this.getController().mixins.mixinUIICP_LongtextNotifController.initializeNotifLongtext.call(this);
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshMatConfPanel of BaseIntegratedConfPage', ex);
            }
        },
	
        //TODO implement teamUser! Check FuncPara
        transferViewStateIntoModel: function() {
            try {
            }
            catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseIntegratedConfPage', ex);
            }
        }
});