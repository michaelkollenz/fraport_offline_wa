Ext.define('AssetManagement.base.view.pages.BaseDeliveryListPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',


  requires: [
    'AssetManagement.customer.model.pagemodel.DeliveryListPageViewModel',
    'AssetManagement.customer.controller.pages.DeliveryListPageController',
    'AssetManagement.customer.view.OxGridPanel',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.view.utils.OxTextField',
    'AssetManagement.customer.view.utils.OxNumberField',
    'AssetManagement.customer.view.utils.OxComboBox',
    'Ext.grid.plugin.CellEditing',
    'Ext.grid.column.Number',
    'Ext.grid.column.Check',
    'Ext.grid.feature.Grouping'
  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.DeliveryListPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseDeliveryDetailPage', ex);
      }

      return this._instance;
    }
  },

  mixins: {
    rendererMixin: 'AssetManagement.customer.view.mixins.DeliveryListPageRendererMixin'
    // itemsMixin: 'AssetManagement.customer.view.mixins.DeliveryDetailPageItemsMixin'
  },
  _DeliveryDocumentRow: null,
  _plantRow: null,
  _storLocRow: null,
  _groupingFeature: null,

  viewModel: {
    type: 'DeliveryListPageModel'
  },

  controller: 'DeliveryListPageController',

  buildUserInterface: function () {
    try {
      var myController = this.getController();
      var me = this;

      this.editing = Ext.create('Ext.grid.plugin.CellEditing');
      var items = [
        {
          xtype: 'container',
          flex: 1,
          layout: {
            type: 'hbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'container',
              margin: '0 10 0 10',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'container',
                  id: 'DeliveryItemGridContainer',
                  margin: '10 0 10 0',
                  flex: 1,
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  }
                }
              ]
            }
          ]
        }
      ];


      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseDeliveryDetailPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';

    try {
      var title = Locale.getMsg('delivery');

      var Delivery = this.getViewModel().get('Delivery') ? this.getViewModel().get('Delivery').get('items') : null;

      if (Delivery) {
        title += "(" + Delivery.getCount() + ")";
      }
      retval = title;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseDeliveryDetailPage', ex);
    }

    return retval;
  },

  //protected
  //@Override
  updatePageContent: function () {
    try {
      //do not remove - causes issue if we are not generating each and every time the grid

      this.refreshGridPanel();

      // this.clearAllFields();
      // this.fillMainDataSection();
      this.refreshDeliveryItemPanel();
      // this.searchFieldVisibility();


    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseDeliveryDetailPage', ex);
    }
  },

  refreshGridPanel: function () {
    try {
      var gridContainer = Ext.getCmp('DeliveryItemGridContainer');

      // var invItemGrid;

      gridContainer.removeAll();
      gridContainer.add(this.getDeliveryItemsGridPanel())
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseDeliveryDetailPage', ex);
    }
  },


  refreshDeliveryItemPanel: function () {
    try {
      var me = this;
      var DeliveryItemGridPanel = Ext.getCmp('DeliveryItemGridPanel');

      var invItemsList = this.getViewModel().get('listItems');
      invItemsList.setGroupField('gebnr');

      DeliveryItemGridPanel.setStore(invItemsList);
      Ext.defer(function () {
        DeliveryItemGridPanel.features[0].collapseAll();
      }, 500, me);

      // invItemsList.sort('item', 'ASC');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshDeliveryItemPanel of BaseDeliveryDetailPage', ex);
    }

  },

  getDeliveryItemsGridPanel: function () {
    var retval = null;
    try {
      var me = this;
      var myController = this.getController();
      var rendererMixins = this.mixins.rendererMixin;
      var groupingFeature = Ext.create('Ext.grid.feature.GroupingSummary',{
        groupHeaderTpl: Locale.getMsg('building') + ': {groupValue}',
      }) ;

      retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
        id: 'DeliveryItemGridPanel',
        hideContextMenuColumn: true,
        useLoadingIndicator: true,
        loadingText: Locale.getMsg('loadingDelivery'),
        emptyText: Locale.getMsg('noDelivery'),
        flex: 1,
        scrollable: 'vertical',
        columns:
          [
            {
              xtype: 'widgetcolumn',
              id: 'IARoomColumn',
              flex: 1,
              // Width: 100,
              resizeable: false,
              // hidden: true,
              text: Locale.getMsg('room') + '<br>' + Locale.getMsg('delivered') + ' ' + Locale.getMsg('failed') ,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              summaryType: 'count',
              summaryRenderer: function (value, summaryData, dataIndex) {
                return ((value === 0 || value > 1) ? '(' + value + ' Belege)' : '(1 Beleg)'); //TODO
              },
              widget: {
                xtype: 'container',
                owner:
                me,
                flex:
                  1,
                items:
                  [
                    {
                      xtype: 'label',
                      fieldLabel: '',
                      margin: '3 0 0 0 ',
                      padding: '15 0 0 0',
                      width: 50,
                      // maxLength: 50,
                      autofocus: false,
                      hideTrigger: true,
                      mouseWheelEnabled: false
                    },
                    {
                      xtype: 'radiogroup',
                      // fieldLabel: 'Zug.',
                      fieldLabel: '',
                      labelWidth: 40,
                      labelAlign: 'top',
                      simpleValue: true,  // set simpleValue to true to enable value binding
                      // bind: '{record.ia_status}',
                      defaults: {
                        flex: 1
                      },
                      listeners: {
                        scope: myController,
                        change: myController.onZugChanged
                      },
                      layout: 'hbox',
                      items: [
                        {
                          boxLabel: '',
                          // name      : 'zug',
                          inputValue: 'Z'
                          // id        : 'radio1'
                        }, {
                          boxLabel: '',
                          padding: '0 0 0 20',
                          // name      : 'zug',
                          inputValue: 'XX' // Value needs to be changes before sending to SAP (Done in the Manager)
                          // id        : 'radio2'
                        },
                        {
                          boxLabel: 'offen', // dummy to have an empty value when saving
                          // name      : 'zug',
                          hidden: true,
                          inputValue: ''
                          // id        : 'radio2'
                        }
                      ]
                    }
                  ]
              }
              ,
              onWidgetAttach: rendererMixins.iaRoomOnWidgetAttached
            },
            // {
            //   xtype: 'gridcolumn',
            //   dataIndex: 'raum',
            //   text: Locale.getMsg('room'),
            //   width: 80,
            //   resizable: false,
            //   renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            //     var position = record.get('raum');
            //     return position;
            //   },
            // },
            {
              xtype: 'widgetcolumn',
              id: 'IAReasonColumn',
              flex: 2,
              // Width: 100,
              resizeable: false,
              // hidden: true,
              text: Locale.getMsg('empf'),
              // dataIndex: 'ia_status',
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              widget: {
                xtype: 'container',
                owner: me,
                flex: 1,
                items: [
                  {
                    xtype: 'label',
                    fieldLabel: '',
                    margin: '3 0 0 0 ',
                    padding: '15 0 0 0',
                    width: 90,
                    // maxLength: 50,
                    autofocus: false,
                    hideTrigger: true,
                    mouseWheelEnabled: false,
                  },
                  {
                    xtype: 'oxcombobox',
                    padding: '15 0 0 0',
                    editable: false,
                    width: 90,
                    displayField: 'text',
                    valueField: 'grund',
                    queryMode: 'local',
                    height: 30,
                    listeners: {
                      scope: myController,
                      change: myController.onReasonChanged
                    }
                  }
                ]
              },
              onWidgetAttach: rendererMixins.iaReasonOnWidgetAttached
            },
            {
              xtype: 'gridcolumn',
              dataIndex: 'menge',
              text: Locale.getMsg('quantity'),
              // flex: 1,
              width: 110,
              resizable: false,
              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                var menge = record.get('menge');
                var meins = record.get('meins');
                return menge + '<p>' + meins + '</p>';

              }
            }
          ],
        features: [groupingFeature]
      });
      // this._groupingFeature = groupingFeature;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewOrderGridPanel of BaseOrderListPage', ex);
    }

    return retval;
  },
  getCurrentInputValues: function () {
    var retval = null;

    try {
      retval = Ext.getCmp('DeliveryItemGridPanel').getStore();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseDeliveryDetailPage', ex);
      retval = null;
    }
    return retval;
  }
});
