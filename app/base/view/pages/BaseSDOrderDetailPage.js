Ext.define('AssetManagement.base.view.pages.BaseSDOrderDetailPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.SDOrderDetailPageViewModel',
        'AssetManagement.customer.controller.pages.SDOrderDetailPageController',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'AssetManagement.customer.helper.OxLogger'
    ],
    
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.SDOrderDetailPage');
                }
            } catch (ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSDOrderDetailPage', ex);
		    }
            
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.SDOrderDetailPageRendererMixin'
    },
    
    viewModel: {
        type: 'SDOrderDetailPageModel'
    },
    
    controller: 'SDOrderDetailPageController',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    _orderingNumberRow: null,
	_orderDateRow: null,
	_desDelivDateRow: null,
	_deliveryStatusRow: null,
	_notifRow: null,
	_orderRow: null,
	_nameRow: null,
	_cityRow: null,
	_streetRow: null,
	_countryRow: null,
    
    initialize: function() {
        try {
        	var myController = this.getController();
        	var me = this;
		    var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
				        {
				            xtype: 'component',
				            maxWidth: 10,
				            minWidth: 10
				        },
				        {
				            xtype: 'container',
				            flex: 1,
				            layout: {
				                type: 'vbox',
				                align: 'stretch'
				            },
				            items: [
				                {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            margin: '0 0 0 5',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('generalData')
                                        },
                                        {
                                            xtype: 'component',
                                            height: 20
                                        },
                                        {
                                            xtype: 'container',
                                            id: 'sdOrderGeneralDataContainer'
                                        },
                                        {
                                            xtype: 'component',
                                            height: 20
                                        }
                                    ]
                                },
                                {
		                            xtype: 'container',
		                            flex: 1,
		                            id: 'SDOrderDetailPageDeliveryAddressContainer',
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    height: 60,
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'bottom'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'label',
		                                            flex: 1,
		                                            cls: 'oxHeaderLabel',
		                                            margin: '0 0 0 5',
		                                            padding: '0 0 20 0',
		                                            text: Locale.getMsg('differentDeliveryAddress')
		                                        },
		                                        {
		                                        	xtype: 'button',
													padding: '0',
													margin: '0 5 0 0',
								                    html: '<img width="100%" src="resources/icons/maps_small.png"></img>',
									                height: 50,
									                width: 60,
									                listeners: {
									                	click: 'openDeliveryAddress'
									                }
		                                        }
		                                    ]
		                                },
		                                {
		                                    xtype: 'container',
		                                    id: 'SDOrderDetailPageDeliveryAddressDataContainer'
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                }
		                            ]
		                        },
		                        {
                                    xtype: 'component',
                                    height: 20
                                },
		                        {
		                            xtype: 'container',
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
                                	    {
                                            xtype: 'label',
                                            margin: '0 0 0 5',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('sdOrderItems')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
						                {
						                    xtype: 'oxdynamicgridpanel',
						                    id: 'sdOrderItemsSDOrderDetailPageGridPanel',
						                    viewModel: {},
						                    margin: '20 0 0 0',
						                    disableSelection: true,
						                    parentController: myController,
						                    owner: me,
							                dockedItems: [
						                        {
						                        	xtype: 'toolbar',
						                        	id: 'SDOrderDetailPageItemsGridPanelFooter',
						                        	dock: 'bottom',
						                        	height: 50,
						                        	layout: {
						                               type: 'hbox',
						                               align: 'middle'
						                        	},
						                        	items: [
						                        	   {
							                               xtype: 'container',
							                               flex: 1,
							                               layout: {
							                                   type: 'hbox',
							                                   align: 'middle'
							                               },
							                               items: [
							                                   {
							                                       xtype: 'container',
							                                       maxWidth: 6,
							                                       minWidth: 6
							                                   },
							                                   {
						                                           xtype: 'button',
						                                           html: '<div><img width="100%" src="resources/icons/add.png"></img></div>',
						                                           height: 40,
						                                           width: 50,
						                                           padding: 0,
						                                           listeners: {
						           	                               		click: 'onAddSDOrderItemClick'
							           	                       	   }
							                                   },
						                                       {
						                                           xtype: 'label',
						                                           flex: 1,
						                                           text: Locale.getMsg('addAnotherSDItem')
						                                       }
							                                ]
						                            	}
						                           ]
						                       }
						                   ],
						                   /*columns: [
						                        {
				                                	xtype: 'actioncolumn',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    return '<img src="resources/icons/demand_requirement.png"  />';
					                                },
					                                enableColumnHide: false,
					                                maxWidth: 80,
				                                    minWidth: 80,
				                                    align: 'center'
				                                },
				                                {
				                                    xtype: 'gridcolumn',
					                                maxWidth: 70,
					                                minWidth: 70,
				                                    text: Locale.getMsg('position'),
				                                    dataIndex: 'posnr',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    return record.get('posnr');
					                                },
					                                enableColumnHide: false
				                                },
				                                {
				                                    xtype: 'gridcolumn',
				                                    flex: 1,
				                                    text: Locale.getMsg('material'),
				                                    dataIndex: 'matnr',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                	var matnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('matnr'), '0');
					                            		var maktx = record.get('material') ? record.get('material').get('maktx') : '';
					
					                            		return AssetManagement.customer.utils.StringUtils.concatenate([ matnr, maktx ], null, true);
					                                },
					                                enableColumnHide: false
				                                },
				                                {
				                                    xtype: 'gridcolumn',
					                                maxWidth: 120,
					                                minWidth: 120,
				                                    text: Locale.getMsg('quantityUnit'),
				                                    dataIndex: 'zmeng',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    var quantity = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('zmeng')); 
				                                		var unit = record.get('zieme'); 
				                                		
				                                		return quantity + ' ' + unit;
					                                },
				                                    enableColumnHide: false
				                                },
				                                {
					                                xtype: 'gridcolumn',
					                                maxWidth: 130,
					                                minWidth: 130,
					                                dataIndex: 'lfsga',
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                            		return record.getDeliveryStatus();
					                             	},
					                                enableColumnHide: false,
					                                text: Locale.getMsg('deliveryStatus')
					                            },
				                                {
				                                    xtype: 'gridcolumn',
				                                    flex: 2,
				                                    dataIndex: 'arktx',
				                                    text: Locale.getMsg('remarks'),
					                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				                                		return record.get('arktx');
					                                },
				                                    enableColumnHide: false
				                                }
						                    ],*/
							                listeners: {
					                    		cellcontextmenu: myController.onCellContextMenuSDOrderItemList,
					                    		scope: myController
					                    	}
						                }
							        ]
						        }
				            ]
				        },
				        {
				            xtype: 'component',
					        maxWidth: 10,
				            minWidth: 10
				        }
				    ]
		        }
		    ];
		    
		    this.add(items);
		    
		    var generalDataGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._orderingNumberRow = generalDataGrid.addRow(Locale.getMsg('orderingNumber'), 'label', true);
			this._orderDateRow = generalDataGrid.addRow(Locale.getMsg('orderDate'), 'label', true);
			this._desDelivDateRow = generalDataGrid.addRow(Locale.getMsg('requestedDeliveryDate'), 'label', true);
			this._deliveryStatusRow = generalDataGrid.addRow(Locale.getMsg('deliveryStatus'), 'label', true);
			this._notifRow = generalDataGrid.addRow(Locale.getMsg('notif'), 'link', true);
			this._notifRow.getTemplate().addListener('click', this.getController().navigateToNotif, this);
			this._orderRow = generalDataGrid.addRow(Locale.getMsg('order'), 'link', false);
			this._orderRow.getTemplate().addListener('click', this.getController().navigateToOrder, this);
			
			var addressGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._nameRow = addressGrid.addRow(Locale.getMsg('name'), 'label', true);
			this._cityRow = addressGrid.addRow(Locale.getMsg('city'), 'label', true);
			this._streetRow = addressGrid.addRow(Locale.getMsg('street'), 'label', true);
			this._countryRow = addressGrid.addRow(Locale.getMsg('country'), 'label', false);
		    
		    this.queryById('sdOrderGeneralDataContainer').add(generalDataGrid);
		    this.queryById('SDOrderDetailPageDeliveryAddressDataContainer').add(addressGrid);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseSDOrderDetailPage', ex);
		}
    },
    
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
	    	retval = Locale.getMsg('materialOrder');
	    	
			var sdOrder = this.getViewModel().get('sdOrder');
			
			if(sdOrder) {
				retval += ' ' + sdOrder.get('bstnk') + ' (' + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(sdOrder.get('vbeln') + ')');
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseSDOrderDetailPage', ex);
		}
		
		return retval;
    },
    
   	//protected
	//@override
	updatePageContent: function() {
		try {
			this.fillMainDataSection();
			this.fillAddressDataSection();
			this.refreshSDOrderItemsPanel();

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseSDOrderDetailPage', ex);
		}
	},
	
	fillMainDataSection: function() {
		try {
			var sdOrder = this.getViewModel().get('sdOrder');
		
			this._orderingNumberRow.setContentString(sdOrder.get('bstnk') + ' (' + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(sdOrder.get('vbeln')) + ')');
			
			this._orderDateRow.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(sdOrder.get('bstdk')));
			this._desDelivDateRow.setContentString(AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(sdOrder.get('vdatu')));
			
			this._deliveryStatusRow.setContentString(sdOrder.getDeliveryStatus());

			// notif
			var notif = sdOrder.get('notif');
			
			if(notif !== null && notif !== undefined) {
				this._notifRow.show();
				this._notifRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(notif.get('qmnum'), '0') + " " + notif.get('qmtxt'));
			} else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('qmnum'))) {
				this._notifRow.show();
				this._notifRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(sdOrder.get('qmnum'), '0') );
			} else {
				this._notifRow.hide();
				this._notifRow.setContentString('');
			}
			
			// order
			var order = sdOrder.get('order');
			
			if(order !== null && order !== undefined) {
				this._orderRow.show();
				this._orderRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0') + " " + order.get('ktext'));
			} else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('aufnr'))) {
				this._orderRow.show();
				this._orderRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(sdOrder.get('aufnr'), '0'));
			} else {
				this._orderRow.hide();
				this._orderRow.setContentString('');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseSDOrderDetailPage', ex);
		}
	},
	
	fillAddressDataSection: function() {
		try {
			var sdOrder = this.getViewModel().get('sdOrder');
		
			var hasAddressData = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('postalCode1'));
			
			var addressContainer = this.queryById('SDOrderDetailPageDeliveryAddressContainer');
			if(hasAddressData === true) {
				addressContainer.show();
				
				//name
				this._nameRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ sdOrder.get('formOfAddress'), sdOrder.get('name1'), sdOrder.get('name2') ], ' ', true, true));
				
				//city
				this._cityRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ sdOrder.get('postalCode1'), sdOrder.get('city') ], ' ', true, true));
				
				//street
				this._streetRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ sdOrder.get('street'), sdOrder.get('houseNo') ], ' ', true, true));
				
				//country
				this._countryRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ sdOrder.get('region'), sdOrder.get('country') ], '/', true));
			} else {
				addressContainer.hide();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillAddressDataSection of BaseSDOrderDetailPage', ex);
		}
	},
	
	refreshSDOrderItemsPanel: function() {
		try {
			var sdOrder = this.getViewModel().get('sdOrder');
		
			//hide or show the footer to add new items
			var doShow = sdOrder.get('updFlag') === 'I';
			var footer = Ext.getCmp('SDOrderDetailPageItemsGridPanelFooter');
			
			if(doShow === true) {
				footer.show();
			} else {
				footer.hide();
			}
		
			var sdOrderItemsGridPanel = Ext.getCmp('sdOrderItemsSDOrderDetailPageGridPanel');
			sdOrderItemsGridPanel.setStore(sdOrder.get('items'));
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshSDOrderItemsPanel of BaseSDOrderDetailPage', ex);
		}
	}
});