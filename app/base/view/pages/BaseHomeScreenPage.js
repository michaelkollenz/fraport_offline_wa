Ext.define('AssetManagement.base.view.pages.BaseHomeScreenPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.controller.pages.HomeScreenPageController',
        'AssetManagement.customer.model.pagemodel.HomeScreenPageModel',
        'Ext.container.Container',
        'Ext.Img',
        'Ext.button.Button',
        'Ext.mixin.Responsive'       
    ],
  
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null)
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.HomeScreenPage');

            } catch(ex) {
    		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseHomeScreenPage', ex);
    	    }
            	
            return this._instance;
        }
    },
    
    config: {
        flex: 1,
        bodyCls: ''
    },
    
    viewModel: {
        type: 'HomeScreenPageModel'
    },    
    
    controller: 'HomeScreenPageController',
    itemId: 'homeScreenPage',
    overflowY: 'auto',
    header: false,
    titleAlign: 'center',   
    layout: {
        type: 'vbox',
        align: 'stretch'
    }, 
    listeners: {
    	resize: 'buttonSize'	   
    },
    	
    buildUserInterface: function() {
    	try {
		   	var items = [
		        {
		           xtype: 'container',
		           margin: '60 0 80 0',
		           layout: {
		               type: 'vbox',
		               align: 'center'
		           },
		           items: [
		                {
		                    xtype: 'image',
		                    height: 84,
		                    width: 302,
		                    src: 'resources/icons/home_screen_logo.png',
		                    id: 'home_logo',
		                    responsiveConfig: {
		                        'width < 600': {
		                            height: 66,
		                            width: 237
		                        },
		                        'width > 1100': {
		                            height: 84,
		                            width: 302
		                        },
		                        'width > 600 && width < 1100': {
		                            height: 73,
		                            width: 262
		                        }	                        
	                    	},
		                    plugins: [
		                        {
		                            ptype: 'responsive'
		                        }
                            ]
		                }
		                //CUSTOMER LOGO
//		                ,
//		                {    	
//			            xtype: 'component',
//			            height: 15
//			            },
//		                {
//		                    xtype: 'image',
//		                    height: 83,
//		                    width: 299,
//		                    src: 'resources/icons/customer_logo.jpg',
//		                    id: 'customer_logo',
//	                        responsiveConfig: {
//                            	'width < 600': {
//	                                height: 66,
//	                                width: 237
//		                        },
//		                        'width > 1100': {
//	                                height: 83,
//	                                width: 299
//		                        },
//		                        'width > 600 && width < 1100': {
//		                            height: 72,
//		                            width: 259
//		                        }
//			            	},
//			            	plugins: [
//			                    {
//			                        ptype: 'responsive'
//			                    }
//			                ]
//		                }
		            ],
                    responsiveConfig: {
                       'height < 600': {
                           margin: '30 0 40 0'
                       },
                       'height >= 600': {
                    	   margin: '60 0 80 0'
                       }	                        
               	    },
                    plugins: [
                       {
                           ptype: 'responsive'
                       }
                    ]
		        },
		        {
		            xtype: 'container',	     
		            id: 'firstButtonRow',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
			            {
		                    xtype: 'container',
	                        flex: 1,                        
	                        layout: {
		                        type: 'vbox',
		                        align: 'center'
		                    },
		                    items: [
		                        {
			                        xtype: 'button',
			                        html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/button_order_home.png"></img></div><p class="schwarz">'+Locale.getMsg('orders')+'</p>',		                           
			                        height: 200,		                            
			                        id: 'homeScreenOrderButton',
			                        maxHeight: '200',
			                        maxWidth: '176',
			                        minHeight: '135',
			                        minWidth: '119',	
			                        width: 176,			
			                        text: '',
			                        listeners: {
			                            click: 'onOrderButtonClick'		                               
			                        }		                            
				                }
		                    ]
		                },		                
		                {
		                    xtype: 'container',
		                    flex: 1,	
		                    layout: {
		                        type: 'vbox',
		                        align: 'center'
		                    },
		                    items: [
		                        {
		                            xtype: 'button',
		                            html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/button_notif_home.png"></img></div><p class="schwarz">'+Locale.getMsg('notifications')+'</p>',
		                            height: 200,		                        
		                            id: 'homeScreenNotifButton',
		                            maxHeight: '200',
                                    maxWidth: '176',
		                            minHeight: '135',
                                    minWidth: '130',	
		                            width: 176,
		                            text: '',
		                            listeners: {
		                                click: 'onNotifButtonClick'
		                            }
		                        }
		                    ]
		                }
		            ]
		        },
                {
	        	    xtype: 'container',
	        	    id: 'syncButton',
	        	    flex: 1,
	                layout: {
	            		type: 'vbox',
	            		align: 'center'
	        		},
	        		items: [
	        		    {
		        		    xtype: 'component',
		        		    height: 50
	        		    },
	        		    {
	        		    	xtype: 'button',
	        		    	html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/Button_Syncx2.png"></img></div><p class="schwarz">'+Locale.getMsg('sync')+'</p><style type="text/css">\n<!--\n</style>',		                           
	        		    	id: 'homeScreenSyncButton',
	        		    	height: 200,
	        		    	width: 176,
	        		    	maxHeight: '200',
	        		    	maxWidth: '176',
	        		    	minHeight: '135',
	        		    	minWidth: '130',	
	        		    	itemId: 'sync',		                          
	        		    	listeners: {
	                        	click: 'onSyncButtonClick'
	        		      	}
	        		    }
	        		]
              },
              {
                  xtype: 'component',
                  height: 20
              },
              {
		            xtype: 'container',	     
		            id: 'secondButtonRow',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
			            {
		                    xtype: 'container',
	                        flex: 1,                        
	                        layout: {
		                        type: 'vbox',
		                        align: 'center'
		                    },
		                    items: [
		                        {
			                        xtype: 'button',
			                        html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/button_equi_home.png"></img></div><p class="schwarz">'+Locale.getMsg('equipments')+'</p>',		                           
			                        height: 200,		                            
			                        id: 'homeScreenEquiButton',
			                        maxHeight: '200',
			                        maxWidth: '176',
			                        minHeight: '135',
			                        minWidth: '130',	
			                        width: 176,			
			                        text: '',
			                        listeners: {
			                            click: 'onEquiButtonClick'		                               
			                        }		                            
				                }
		                    ]
		                },		                
		                {
		                    xtype: 'container',
		                    flex: 1,	
		                    layout: {
		                        type: 'vbox',
		                        align: 'center'
		                    },
		                    items: [
		                        {
		                            xtype: 'button',
		                            html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/button_funcloc_home.png"></img></div><p class="schwarz">'+Locale.getMsg('funcLocs')+'</p>',
		                            height: 200,		                        
		                            id: 'homeScreenFuncLocButton',
		                            maxHeight: '200',
                                    maxWidth: '176',
		                            minHeight: '135',
                                    minWidth: '130',	
		                            width: 176,
		                            text: '',
		                            listeners: {
		                                click: 'onFuncLocButtonClick'
		                            }
		                        }
		                    ]
		                }
		            ]
		        }
		    ];
	  
			this.add(items);
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseHomeScreenPage', ex);
    	}
    },
    
    getPageTitle: function() {
    	var retval = '';
    
    	try {
    		retval = Locale.getMsg('home');
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseHomeScreenPage', ex);
    	}
    	
    	return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
    	try {
    		//TODO
    		//read customizing parameter and remove unneccessary buttons
    		//or show sync button, if there is no data for this user yet
    		var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
    		
    		var buttonCont  = Ext.getCmp('firstButtonRow');
    		var buttonCont2 = Ext.getCmp('secondButtonRow');
    		var syncButton = Ext.getCmp('syncButton');
    	  
    		if(!userInfo.get('isInitialized')) {
    			buttonCont.setHidden(true);
    			buttonCont2.setHidden(true);
    			syncButton.setVisible(true);                                                                
    		} else {    			
    			buttonCont.setHidden(false);
    			buttonCont2.setHidden(false);
    			syncButton.setVisible(false);
    			var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();

    			if (funcParaInstance.getValue1For('equi_active') !== 'X') {
    			    Ext.getCmp('homeScreenEquiButton').setHidden(true);
    			} else {
    			    Ext.getCmp('homeScreenEquiButton').setHidden(false);
    			}

    			if (funcParaInstance.getValue1For('noti_active') !== 'X') {
    			    Ext.getCmp('homeScreenNotifButton').setHidden(true);
    			} else {
    			    Ext.getCmp('homeScreenNotifButton').setHidden(false);
    			}

    			if (funcParaInstance.getValue1For('ord_active') !== 'X') {
    			    Ext.getCmp('homeScreenOrderButton').setHidden(true);
    			} else {
    			    Ext.getCmp('homeScreenOrderButton').setHidden(false);
    			}

    			if (funcParaInstance.getValue1For('floc_active') !== 'X') {
    			    Ext.getCmp('homeScreenFuncLocButton').setHidden(true);
    			} else {
    			    Ext.getCmp('homeScreenFuncLocButton').setHidden(false);
    			}
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseHomeScreenPage', ex);
    	}   	
    }

});