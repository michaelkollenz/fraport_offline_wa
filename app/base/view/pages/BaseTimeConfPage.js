Ext.define('AssetManagement.base.view.pages.BaseTimeConfPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.TimeConfPageViewModel',
        'AssetManagement.customer.controller.pages.TimeConfPageController',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxComboBox',
        'Ext.button.Button',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Date',
        'Ext.grid.View',
        'AssetManagement.customer.AppConfig',
        'Ext.form.field.TextArea'
    ],
    
	inheritableStatics: {
		_instance: null,
		
	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	    	       this._instance = Ext.create('AssetManagement.customer.view.pages.TimeConfPage');
	            }
	        } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseTimeConfPage', ex);
		    }
	        
	        return this._instance;
	    }
	},

	mixins: {
	    rendererMixin: 'AssetManagement.customer.view.mixins.TimeConfPageRendererMixin'
	},

    viewModel: {
        type: 'TimeConfPageModel'
    },
    
    controller: 'TimeConfPageController',
    
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		var me = this;
    		var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                 //leftContainer 
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                //middle Container
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        //upper container
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                        	//upper section - form 
		                            xtype: 'container',
		                            flex: 1,
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    items: [
		                                        {
		                                        	//header label timeConf form
		                                            xtype: 'label',
		                                            cls: 'oxHeaderLabel',
		                                            id: 'timeConfPageModeLabel',
		                                            width: 110,
		                                            text: Locale.getMsg('createNewTimeConf')
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            height: 20
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            id: 'createContainer',
		                                            layout: {
		                                                type: 'vbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
		                                                {
		                                                	//order textfield
		                                                    xtype: 'oxtextfield',
		                                                    flex: 1,
		                                                    id: 'timeConfOrderTF', 
		                                                    fieldLabel: Locale.getMsg('order'),
		                                                	labelWidth: 110,
		    			                                    disabled: true
		                                                },                                                                                             
		                                                {
		                                                	//operation combobox
		                                                    xtype: 'oxcombobox',
		                                                    flex: 1,
		                                                    border: 5,
		                                                    height: 10,
		                                                    id: 'operationDropDownBox',
		                                                    fieldLabel: Locale.getMsg('operation'),
		                                                	labelWidth: 110,
		                                                    displayField: 'text',
		                                                    queryMode: 'local',
		                                                    valueField: 'value',
		                		                            editable: false,
		                                                    listeners: {
		                                                		select: 'onOperationSelected'
		                                                	}
		                                                
		                                                },
		                                                {
		                                                	//spacer
		                                                    xtype: 'component',
		                                                    height: 15
		                                                },
		                                                {
		                                                	//equipment textfield
															hidden: true,
		                                                    xtype: 'oxtextfield',
		                                                    id: 'timeConfEquiTF',
		                                                    fieldLabel: Locale.getMsg('equipment'),
		                                                	labelWidth: 110,
		    			                                    disabled: true
		                                                },
		                                                {
		                                                	//hbox with 2 columns
		                                                    xtype: 'container',
		                                                    layout: {
		                                                        type: 'hbox',
		                                                        align: 'stretch'
		                                                    },
		                                                    items: [
		                                                        {
		                                                        	//first column 
		                                                            xtype: 'container',
		                                                            flex: 1,
		                                                            layout: {
		                                                                type: 'vbox',
		                                                                align: 'stretch'
		                                                            },
		                                                            items: [
		                                                                {
		                                                                	//activity type dropdown
		                                                                    xtype: 'oxcombobox',
		                                                                    border: 5,
			                                                                displayField: 'text',
			                                                                queryMode: 'local',
			                                                                valueField: 'value',
		                                                                    height: 10,
		                                                                    id: 'activityTypeDropDownBox',
		                                		                            editable: false,
		                                                                    fieldLabel: Locale.getMsg('timeConfs_Ilart'),
		                                                                	labelWidth: 110,
		                    		                                        listeners: {
		                                                                		select: 'onLearrSelected'
		                                                                	}
		                                                                },
		                                                                {
		                                                                	//spacer
		                                                                    xtype: 'component',
		                                                                    height: 15
		                                                                },
		                                                                {
		                                                                	//accounting indication drop down
		                                                                    xtype: 'oxcombobox',
		                                                                    border: 5,
		                                                                    height: 10,
			                                                                displayField: 'text',
			                                                                queryMode: 'local',
			                                                                valueField: 'value',
			                            		                            editable: false,
			                                                                fieldLabel: Locale.getMsg('accountIndiShortDoubleDot'),
		                                                                	labelWidth: 110,
		                                                                    id: 'accountIndicationDropDownBox',
                                                                            listeners: {
                                                                                select: 'onBemotSelected'
                                                                            }
		                                                                    
		                                                                },
		                                                                {
		                                                                	//spacer
		                                                                    xtype: 'component',
		                                                                    height: 15
		                                                                },
                                                                        {
                                                                        	xtype: 'checkbox',
	                                                                        baseCls: 'oxCheckBox',
	                                                                        id: 'finalConf',
	                                                                        checkedCls: 'checked',
	                                                                        fieldLabel: Locale.getMsg('finalConfirmationDoubleDot'),
	                                                                        labelStyle: 'padding-top: 8px;',
	                                                                        labelWidth: 110,
	                                                                        width: 30,
	                                                                        height: 35
                                                                        }
		                                                            ]
		                                                        },
		                                                        {
		                                                        	//spacer
		                                                            xtype: 'component',
		                                                            width: 50
		                                                        },
		                                                        {
		                                                            xtype: 'container',
			                                                        layout: {
			                                                            type: 'vbox',
			                                                            align: 'stretch'
			                                                        },
		                                                            flex: 1,
		                                                            items: [
		                                                            {
		                                                                xtype: 'container',
		                                                                layout: {
		                                                                    type: 'hbox',
		                                                                    align: 'stretch'
		                                                                },
		                                                                 
		                                                                items: [
		                                                                    {
		                                                                    	//start date field
		                                                                        xtype: 'datefield',
		                                                                        labelWidth: 100,
		                                                                        flex: 1,
		                                                                        format: 'd.m.Y',
		                                                                        id: 'startDateTimeField',
			                                		                            editable: false,
		                                                                        fieldLabel: Locale.getMsg('timeConfs_Isdd')
		                                                                    }/*,
		                                                                    {
		                                                                    	//spacer
		                                                                        xtype: 'component',
		                                                                        width:20
		                                                                    },
		                                                                    {
		                                                                    	//start time field
		                                                                        xtype: 'timefield',
		                                                                        format: 'H:i',
			                                		                            editable: false,
		                                                                        flex: 0.5,
		                                                                        id: 'startTimeTextField'
		                                                                    }*/
		                                                                ]
		                                                            },
		                                                            {
		                                                            	//spacer
		                                                                xtype: 'component',
		                                                                height: 5
		                                                            },
		                                                            {
		                                                                xtype: 'container',
		                                                                layout: {
		                                                                    type: 'hbox',
		                                                                    align: 'stretch'
		                                                                },
		                                                                
		                                                                items: [
		                                                                    {
		                                                                    	//end date field
		                                                                        xtype: 'datefield',
		                                                                        id: 'endDateTimeField',
		                                                                        labelWidth: 100,
		                                                                        flex: 1,
		                                                                        format: 'd.m.Y',             
			                                		                            editable: false,                                                        
		                                                                        fieldLabel: Locale.getMsg('timeConfs_Iedd')
		                                                                    }/*,
		                                                                    {
		                                                                    	//spacer
		                                                                        xtype: 'component',
		                                                                        width: 20
		                                                                    },
		                                                                    {
		                                                                    	//end time field
		                                                                        xtype: 'timefield',                                                                   
		                                                                        format: 'H:i', 
		                                                                        flex: 0.5,
			                                		                            editable: false,
		                                                                        id: 'endTimeTextField'
		                                                                    }*/
		                                                                ]
		                                                            },
		                                                            {
		                                                            	//spacer
		                                                                xtype: 'component',
		                                                                height: 5
		                                                            },
		                                                            {
		                                                            	//worktime field
					                                                    xtype: 'timefield',                                                                   
		                                                                format: 'H:i', 
					                                                    id: 'timeConfWorkTimeField',
	                                		                            editable: false,
					                                                    fieldLabel: Locale.getMsg('worktime'),
		                                                            	hidden: true
		                                                            },
		                                                            {
		                                                            	//worktime textfield
					                                                    xtype: 'oxtextfield',
					                                                    id: 'timeConfWorkTimeTextField',
					                                                    fieldLabel: Locale.getMsg('worktime'),
		                                                            	hidden: true
		                                                            }
		                                                        ]
		                                                        }
		                                                    ]
		                                                },
		                                                {
		                                                	//spacer
		                                                    xtype: 'component',
		                                                    flex: 1,
		                                                    height: 20
		                                                },{
							                                xtype: 'container',
							                                margin: '20 0 0 0',
                                                            maxWidth: 500,
							                                layout: {
							                                    type: 'hbox',
							                                    align: 'stretch'
							                                },
							                                items: [
							                                    {
						                                            xtype: 'label',
						                                            text: Locale.getMsg('specification'),
						                                            width: 110,
						                                            style: 'color:#666666',
						                                            padding: '0 5 0 0'
						                                        },
				                                                {
				                                                	//description textfield
				                                                    xtype: 'oxtextfield',
				                                                	id: 'descriptionTextField',
				                                                    flex: 1,
				                                                    maxLength: 40
				                                                }
							                                ]
							                            }
		                                            ]
		                                        }
		                                    ]
		                                }
		                            ]
		                        },{
                                    xtype: 'container',
                                    margin: '20 0 0 0',
                                    maxWidth: 500,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            text: Locale.getMsg('longtext'),
                                            width: 110,
                                            style: 'color:#666666',
                                            padding: '0 5 0 0'
                                        },
                                        {
                                            xtype: 'textarea',
                                            id: 'timeConfLocalLongtextTextArea',
                                            grow: true,
                                            labelStyle: 'display: none;',
                                            flex: 1
                                        }
                                    ]
                                },
		                        {
		                        	//spacer
		                            xtype: 'component',
		                            flex: 1,
		                            height: 30
		                        },
		                        {
		                        	//header label timeconfs container
		                            xtype: 'label',
		                            cls: 'oxHeaderLabel',
		                            width: 100,
		                            text: Locale.getMsg('timeConfs_RecTimeConfs')
		                        },
		                        {
		                        	//spacer
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                        	//container with created timeconfs
		                            xtype: 'container',
		                            flex: 1,
		                            id: 'timeConfsContainer',
		                            items: [
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'timeConfGrid',
		                                    disableSelection: true,
		                                    parentController: myController,
                                            owner: me,
		                                   /* columns: [
		                                        {
		                                        	//column with timconf image
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'string',
		                                            text: '',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            return '<img src="resources/icons/timeconf.png"  />';
			                                        },
			                                        maxWidth: 80,
			                                        minWidth: 80,
			                                        align: 'center'
		                                        },
		                                        {
		                                        	//column activity type and description
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'learr',
		                                            flex: 1,
		                                            text: Locale.getMsg('activityType'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var retval = '';
			                                        
		                                        		try {
		                                        			var learr = ''; 
				                                            var learrTxt = '';
				                                            
				                                            var activityType = record.get('activityType');
				                                            
				                                            if(activityType) {
				                                            	learr = activityType.get('lstar');
				                                            	learrTxt = activityType.get('ktext');
				                                            } else {
				                                            	learr = record.get('learr');
				                                            }
				                                            
				                                            retval = AssetManagement.customer.utils.StringUtils.concatenate([ learr, learrTxt ], '-', true);
		                                        		} catch(ex) {
		                                        			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfGrid/learr-column in BaseTimeConfPage', ex);
		                                        		}
		                                        		
		                                        		return retval;
			                                        }
		                                        },
		                                        {
		                                        	//column account Indication 
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'bemot',
		                                            flex: 1,
		                                            text: Locale.getMsg('accountIndication'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	var retval = '';
			                                        
		                                        		try {
		                                        			var accountIndication = record.get('accountIndication');
		                                        		
				                                        	retval = accountIndication ? accountIndication.get('bemottxt') : record.get('bemot');
		                                        		} catch(ex) {
		                                        			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renderer of timeConfGrid/bemot-column in BaseTimeConfPage', ex);
		                                        		}
		                                        		
		                                        		return retval;
			                                        }
		                                        },
                                                {
                                                    xtype: 'datecolumn',
                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        var isSplit = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('split')) && record.get('split') !== '000';
                                                        var startDate = '';
                                                        if (isSplit) {
                                                            startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('fstadt'));
                                                        } else {
                                                            startDate = AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(record.get('ied'));
                                                        }
                                                        return startDate;
                                                    },
                                                    enableColumnHide: false,
                                                    dataIndex: 'fstadt',
                                                    maxWidth: 125,
                                                    minWidth: 125,
                                                    text: Locale.getMsg('date')
                                                },
		                                        {
		                                        	//column work time
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'idaur',
		                                        	maxWidth: 125,
		                                            minWidth: 125,
		                                            text: Locale.getMsg('worktime'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return record.getWorkValueForDisplay();
			                                        }
		                                        },
		                                        {
		                                        	//column personalno. 
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'ltxa1',
		                                            flex: 1.5,
		                                            text: Locale.getMsg('specification'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	return record.get('ltxa1');
			                                        }
		                                        },
		                                        {
		                                        	//column final conf
		                                            xtype: 'gridcolumn',
		                                            dataIndex: 'aueru',
		                                            flex: 0.5,
		                                            text: Locale.getMsg('finalConfirmation'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                           	var aueru = record.get('aueru');
			                                           	
			                                           	if(aueru === 'X')
			                                           		return '[END]'; 
			                                          
			                                           	return ''; 
			                                        }
		                                        }
		                                    ],*/
		                                    listeners: {
		                                		cellcontextmenu: myController.onCellContextMenu,
		                                		scope: myController
		                                	}
		                                }
		                            ]
		                        },
		                        //lower container
		                        {
		                            xtype: 'component',
		                            height: 30
		                        }
		                    ]
		                },
		                //right container
		                {
		                    xtype: 'component',
		                    width: 30
		                }
		            ]
		        }
		    ];
		     
	     	this.add(items);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseTimeConfPage', ex);
    	}
    },
    
    getPageTitle: function() {
    	var retval = '';
    
    	try {
    		var title = Locale.getMsg('timeconfsForOperation');
    	
    		var operation = this.getViewModel().get('operation');
    	
    		if(operation) {
    			title += " " + operation.get('vornr');
    		}
    		
    		retval = title;
	    } catch(ex) {
        	 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseTimeConfPage', ex);
        }

    	return retval;
	},

	//protected
	//@override
	updatePageContent: function() {
		try {
			this.fillDropDownBoxes(); 
			
			this.transferModelStateIntoView();
			
			//refresh timeConfs
			this.refreshTimeConfPanel(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseTimeConfPage', ex);
		}
    },

    //fill drop down boxes operations, activity types, bemots
    fillDropDownBoxes: function() {
    	try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var orderTypes = myModel.get('orderTypes');
			var operations = order ? order.get('operations') : null;
			var activityTypes = myModel.get('activityTypes');
			var accountIndications = myModel.get('accountIndications');
    	
			//filling operations dropdownbox
			var operationComboBoxSource = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			if(operations && operations.getCount() > 0) {
				order.get('operations').each(function(oper) {
					var id = oper.get('vornr');
					var split = oper.get('split');
					
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(split) && split !== '000')
						id += " / " + oper.get('split');
						
					operationComboBoxSource.add({ text: id + " - " + oper.get('ltxa1'), value: oper });
				}, this);
			}
			
			var operComboBox = Ext.getCmp('operationDropDownBox');
			operComboBox.clearValue();
			operComboBox.setStore(operationComboBoxSource);
			
			//filling activitytype dropdownbox
			var atComboBoxSource = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
			
			if(activityTypes && activityTypes.getCount() > 0) {
				activityTypes.each(function(activityType) {
					atComboBoxSource.add({ text: activityType.get('lstar') + " - " + activityType.get('ktext'), value: activityType });
				}, this);
			}
			var atComboBox = Ext.getCmp('activityTypeDropDownBox');
			var emptyValue = {text:'&nbsp;', value:''};
            atComboBoxSource.insert(0,emptyValue);
			atComboBox.clearValue();
			atComboBox.setStore(atComboBoxSource);
			
			//filling accountIndication dropdownbox
			var bemotComboBoxSource = Ext.create('Ext.data.Store', {
			    fields: ['text', 'value']
			});
	
			if(accountIndications && accountIndications.getCount() > 0) {
				accountIndications.each(function(accountIndication) {
					bemotComboBoxSource.add({ text: (accountIndication.get('bemot') + ' - ' + accountIndication.get('bemottxt')), value: accountIndication });
				}, this);
			}
            bemotComboBoxSource.insert(0,emptyValue);
			var bemotComboBox = Ext.getCmp('accountIndicationDropDownBox');

            var selectedAuart = orderTypes.findRecord('auart', order.get('auart'));
            var isService = selectedAuart.get('service') === 'X' ? true : false;
            if(isService) {
                bemotComboBox.clearValue();
                bemotComboBox.setStore(bemotComboBoxSource);
                bemotComboBox.show();
            } else {
                bemotComboBox.clearValue();
                bemotComboBox.hide()
            }

		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillDropDownBoxes of BaseTimeConfPage', ex);
		}
	},
	
	transferModelStateIntoView: function() {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var timeConf = myModel.get('timeConf');
			var editMode = myModel.get('isEditMode');
			
			//set mode label
			Ext.getCmp('timeConfPageModeLabel').setText(editMode === true ? Locale.getMsg('timeConfEdit') : Locale.getMsg('createNewTimeConf'));			

			//set general data
			Ext.getCmp('timeConfOrderTF').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0') + " " + order.get('ktext'));
			
			if(order.get('equipment') !== null && order.get('equipment') !== undefined)
				Ext.getCmp('timeConfEquiTF').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0') + " " + order.get('equipment').get('eqktx'));
			else
				Ext.getCmp('timeConfEquiTF').setValue(AssetManagement.customer.utils.StringUtils.trimStart(order.get('equnr'), '0'));

	    	//set start/end 
	    	Ext.getCmp('startDateTimeField').setValue(timeConf.get('isd'));
	    	Ext.getCmp('endDateTimeField').setValue(timeConf.get('ied'));
	    	
	    	//Ext.getCmp('startTimeTextField').setValue(timeConf.get('isd'));
	    	//Ext.getCmp('endTimeTextField').setValue(timeConf.get('ied'));
	    	
	    	//set description
	    	Ext.getCmp('descriptionTextField').setValue(timeConf.get('ltxa1'));
	    	
	    	//set final flag
	    	Ext.getCmp('finalConf').setValue(timeConf.get('aueru') === 'X');	    

	    	//set drop down box values
	    	this.setDropDownBoxesConvenientToCurrentModel();

	    	//set local longtext from model
	    	Ext.getCmp('timeConfLocalLongtextTextArea').setValue(timeConf.get('localLongtext'));

		    //manage work time field
	    	this.setWorkFieldConvienentToCurrentModel();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseTimeConfPage', ex);
		}
	},
	
	setDropDownBoxesConvenientToCurrentModel: function () {
	    try {
	        var myModel = this.getViewModel();
	        var order = myModel.get('order');
	        var operations = order ? order.get('operations') : null;
	        var activityTypes = myModel.get('activityTypes');
	        var accountIndications = myModel.get('accountIndications');
	        var timeConf = myModel.get('timeConf');

	        //operation
	        var timeConfsOperation = null;

	        if (operations && operations.getCount() > 0) {
	            operations.each(function (operation) {
	                if (operation.get('vornr') === timeConf.get('vornr') && operation.get('split') === timeConf.get('split')) {
	                    timeConfsOperation = operation;
	                    return false;
	                }
	            }, this);
	        }

	        var timeConfsOperationRecord = timeConfsOperation ? Ext.getCmp('operationDropDownBox').findRecordByValue(timeConfsOperation) : null;

	        if (timeConfsOperationRecord)
	            Ext.getCmp('operationDropDownBox').setValue(timeConfsOperationRecord);

	        //activity type
	        var timeConfsActivityType = null;

	        if (activityTypes && activityTypes.getCount() > 0) {
	            activityTypes.each(function (activityType) {
	                if (activityType.get('lstar') === timeConf.get('learr')) {
	                    timeConfsActivityType = activityType;
	                    return false;
	                }
	            }, this);
	        }

	        var timeConfsActivityTypeRecord = timeConfsActivityType ? Ext.getCmp('activityTypeDropDownBox').findRecordByValue(timeConfsActivityType) : null;

	        if (timeConfsActivityTypeRecord)
	            Ext.getCmp('activityTypeDropDownBox').setValue(timeConfsActivityTypeRecord);

	        //account indication
	        var timeConfsAccountIndi = null;

	        if (accountIndications && accountIndications.getCount() > 0) {
	            accountIndications.each(function (accountIndication) {
	                if (accountIndication.get('bemot') === timeConf.get('bemot')) {
	                    timeConfsAccountIndi = accountIndication;
	                    return false;
	                }
	            }, this);
	        }

	        var timeConfsAccountIndiRecord = timeConfsAccountIndi ? Ext.getCmp('accountIndicationDropDownBox').findRecordByValue(timeConfsAccountIndi) : null;

	        if (timeConfsAccountIndiRecord)
	            Ext.getCmp('accountIndicationDropDownBox').setValue(timeConfsAccountIndiRecord);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDropDownBoxesConvenientToCurrentModel of BaseTimeConfPage', ex);
	    }
	},

	setWorkFieldConvienentToCurrentModel: function () {
	    try {
	        var timeConf = this.getViewModel().get('timeConf');
	        var valueAsString = timeConf.get('ismnw');

	        //parse the value first
	        var valueAsNumber = 0.0;

	        parsedValue = AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(valueAsString);

	        if (!isNaN(parsedValue)) {
	            valueAsNumber = parsedValue;
	        }

	        //check the unit next
	        var unit = timeConf.get('ismne').toUpperCase();
	        var caseHours = unit === 'H' || unit === 'STD';

	        //get hours and minutes convinent to unit
	        if (caseHours) {
	            var hours = Math.floor(parsedValue);
	            var minutes = Math.round((parsedValue % 1.0) * 60);

	            hours = AssetManagement.customer.utils.StringUtils.padLeft(hours, '0', 2);
	            minutes = AssetManagement.customer.utils.StringUtils.padLeft(minutes, '0', 2);

	            var workTimeValue = hours + ':' + minutes;

	            Ext.getCmp('timeConfWorkTimeField').setValue(workTimeValue);

	            this.showTimeField();
	        } else {
	            Ext.getCmp('timeConfWorkTimeTextField').setValue(valueAsNumber + '');
	            this.showTimeTextField();
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setWorkFieldConvienentToCurrentModel of BaseTimeConfPage', ex);
	    }
	},
	
	refreshTimeConfPanel: function() {
		try {
			var myModel = this.getViewModel();
			var operation = myModel.get('operation');
		
			var timeConfGridPanel = Ext.getCmp('timeConfGrid');
			timeConfGridPanel.setStore(operation.get('timeConfs'));
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshTimeConfPanel of BaseTimeConfPage', ex);
		}
	},
	
	//TODO implement teamUser! Check FuncPara
	transferViewStateIntoModel: function() {
		try {
			var myModel = this.getViewModel();
			var timeConf = myModel.get('timeConf');
			var operation = myModel.get('operation');
            var order = myModel.get('order');
            var orderTypes = myModel.get('orderTypes');
			
			var activityType = Ext.getCmp('activityTypeDropDownBox').getValue(); 
			var accountIndication = Ext.getCmp('accountIndicationDropDownBox').getValue();
			
			//set operation data
			if(operation) {
				timeConf.set('vornr', operation.get('vornr'));
				timeConf.set('split', operation.get('split'));
			} else {
				timeConf.set('vornr', '');
				timeConf.set('split', '');
			}
			
			//set activity type
			if(activityType) {
				timeConf.set('activityType', activityType);
				timeConf.set('learr', activityType.get('lstar'));
			} else {
				timeConf.set('activityType', null);
				timeConf.set('learr', '');
			}


            var selectedAuart = orderTypes.findRecord('auart', order.get('auart'));
            var isService = selectedAuart.get('service') === 'X' ? true : false;

			//set bemot
			if(accountIndication && isService) {
				timeConf.set('accountIndication', accountIndication);
				timeConf.set('bemot', accountIndication.get('bemot'));
			} else {
				timeConf.set('accountIndication', null);
				timeConf.set('bemot', '');
			}
			
			//set unit
			var unit = '';	
			
			if(activityType)
				unit = activityType.get('leinh');
			
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit) && operation)
				unit = operation.get('arbeh');
				
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit))
			    unit = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('default_timeunit');
				
			timeConf.set('ismne', unit);
			timeConf.set('idaue', unit);
			
			//set start/end date
			var endDate = Ext.getCmp('endDateTimeField').getValue()/*Ext.getCmp('endTimeTextField').getValue()*/;
			var startDate = Ext.getCmp('startDateTimeField').getValue()/*Ext.getCmp('startTimeTextField').getValue()*/;
			
			timeConf.set('isd', startDate);
			timeConf.set('ied', endDate);
			
		    //set work value
			var workValue = '';

			if (unit.toUpperCase() === 'H' || unit.toUpperCase() === 'STD') {
			    var minutes = Ext.getCmp('timeConfWorkTimeField').getValue().getMinutes();
			    var hours = Ext.getCmp('timeConfWorkTimeField').getValue().getHours();

			    workValue = AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(hours + minutes / 60);
			} else {
			    workValue = AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(parseInt(Ext.getCmp('timeConfWorkTimeTextField').getValue()));
			}

			timeConf.set('ismnw', workValue);
			timeConf.set('idaur', workValue);

			//set description
			timeConf.set('ltxa1', Ext.getCmp('descriptionTextField').getValue());

			//set localLongtext
			timeConf.set('localLongtext', Ext.getCmp('timeConfLocalLongtextTextArea').getValue());
			
			//set final flag
			if(Ext.getCmp('finalConf').checked)
				timeConf.set('aueru', 'X');
			else
				timeConf.set('aueru', '');
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseTimeConfPage', ex);
		}
	},
	
	getCurrentInputValues: function() {
		var retval = null;
			
	    try {
	        var work = "";

	        if (Ext.getCmp('timeConfWorkTimeField').isVisible())
	            work = Ext.getCmp('timeConfWorkTimeField').getValue();
	        else if (Ext.getCmp('timeConfWorkTimeTextField').isVisible())
	            work = Ext.getCmp('timeConfWorkTimeTextField').getValue();

			retval = {
				activityTpye: Ext.getCmp('activityTypeDropDownBox').getValue(),
				accountIndication: Ext.getCmp('accountIndicationDropDownBox').getValue(),
				work: work,
				startDate: Ext.getCmp('startDateTimeField').getValue(),
				endDate: Ext.getCmp('endDateTimeField').getValue(),
				localLongtext: Ext.getCmp('timeConfLocalLongtextTextArea').getValue()
			};
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseTimeConfPage', ex);
			retval = null;
		}
		
		return retval;
	},

	showTimeField: function () {
	    try {
	        Ext.getCmp('timeConfWorkTimeTextField').setHidden(true);
	        Ext.getCmp('timeConfWorkTimeField').setHidden(false);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showTimeField of BaseTimeConfPage', ex);
	    }
	},

	hideTimeField: function () {
	    try {
	        Ext.getCmp('timeConfWorkTimeField').setHidden(true);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideTimeField of BaseTimeConfPage', ex);
	    }
	},

	showTimeTextField: function () {
	    try {
	        Ext.getCmp('timeConfWorkTimeTextField').setHidden(false);
	        Ext.getCmp('timeConfWorkTimeField').setHidden(true);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showTimeTextField of BaseTimeConfPage', ex);
	    }
	},

	hideTimeTextField: function () {
	    try {
	        Ext.getCmp('timeConfWorkTimeTextField').setHidden(true);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideTimeTextField of BaseTimeConfPage', ex);
	    }
	}
});