Ext.define('AssetManagement.base.view.pages.BaseFuncLocListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.FuncLocListPageViewModel',
        'AssetManagement.customer.controller.pages.FuncLocListPageController',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'AssetManagement.customer.view.utils.OxTextField',
        'Ext.grid.column.Action'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null)
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.FuncLocListPage');
            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of OrderListPage', ex);
		    }
            	
            return this._instance;
        }
    },
    
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.FuncLocListPageRendererMixin'
    },

    controller:'FuncLocListPageController',
    
    viewModel: {
        type: 'FuncLocListPageModel'
    },
    
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
   	//protected
	//@override
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		
		    var  items= [
		        {
		            xtype: 'container',
		            flex: 0.5,
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 5,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
		                            xtype: 'container',
		                            height: 30,
		                            id: 'searchFuncLocContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                               
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldFuncLoc',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,	                                    
		                                    listeners: {
			                                	change: {
			                                        fn: 'onFuncLocListPageSearchFieldChange'
			                                    }
		                                	}
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
			                                id: 'funcLocListPageFilterCriteriaCombobox',
		                                    editable: false,
		                                    store: [Locale.getMsg('funcLocNumberShort'), Locale.getMsg('shorttext'), Locale.getMsg('sortField'),
		                                            Locale.getMsg('location'), Locale.getMsg('room')],
		                                    value: Locale.getMsg('funcLocNumberShort'),
			                                listeners: {
			                                	select: 'onFuncLocListPageSearchFieldChange'
			                                }
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
			                        xtype: 'container',
			                        id: 'funcLocGridContainer',
			                        flex: 1,
			                        layout: {
	                            		type: 'hbox',
	                            		align: 'stretch'
	                            	},
			                        items: [
			                           this.getNewFuncLocGridPanel()
			                        ]
			                    },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];
		    
		    this.add(items);
		    this.searchFieldVisibility();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseFuncLocListPage', ex);
		}
    },
    
    //private
    renewFuncLocGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('funcLocGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewFuncLocGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewFuncLocGridPanel of BaseFuncLocListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewFuncLocGridPanel: function() {
    	var retval = null;
    	
        try {
            var me = this;
    		var myController = this.getController();
    	
    		retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
    			id: 'funcLocGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            loadingText: Locale.getMsg('loadingFuncLocs'),
	            emptyText: Locale.getMsg('noFuncLocs'),
	            flex: 1,
                owner:me,
	            parentController: myController,
	            scrollable: 'vertical',
	            /*columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        return '<img src="resources/icons/funcloc.png"  />';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,
	                    align: 'right'
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var tplnr = record.get('tplnr');
	                        var pltxt = record.get('pltxt');
	                                                                                                                                       
	                        return tplnr +'<p>'+pltxt+'</p>';                              
	                    },
	                    dataIndex: 'tplnr',
	                    text: Locale.getMsg('funcLoc')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    dataIndex: 'eqfnr',
	                    text: Locale.getMsg('sortField')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		var name = '';
	                        var street = '';
	                        var postalCode = '';
	                        
	                        var address = record.get('address');
							if(address !== null && address !== undefined) {
								name = address.getName();
								street = AssetManagement.customer.utils.StringUtils.concatenate([ address.getStreet(), address.getHouseNo() ], null, true);
								postalCode = AssetManagement.customer.utils.StringUtils.concatenate([ address.getPostalCode(), address.getCity() ], null, true);
							} else {
							  	name = '';
	                            street = '';
	                            postalCode = '';
							}
							    
	                        return name +'<p>'+street+'</p><p>'+postalCode+'</p>';
	                 	},
	                    enableColumnHide: false,
	                    dataIndex: 'postalCode',
	                    text: Locale.getMsg('address')
					},
					{
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		if(!record.get('address')) {
	                			metaData.style = 'display: none;';
	                		} else {
	                			metaData.style = '';
	                		}
	                	},
	                	text: Locale.getMsg('maps'),
	                	maxWidth: 80,
	                    width: 80,
	                    items: [{
	                        icon: 'resources/icons/maps_small.png',
	                        tooltip: 'Google Maps',
	                        iconCls: 'oxGridLineActionButton',
	                        handler: function(grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
	                            event.stopEvent();
	                            this.getController().navigateToAddress(record);
	                        },
	                        scope: this
	                    }],
	                    enableColumnHide: false,
	                    align: 'center'
	                }
	            ],*/
	            listeners: {
	            	cellclick: myController.onFuncLocSelected,
	            	cellcontextmenu: myController.onCellContextMenu,
	            	scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewFuncLocGridPanel of BaseFuncLocListPage', ex);
    	}
    	
    	return retval;
	},

	//protected
	//@override	
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
	    	retval = Locale.getMsg('funcLocs');
	    	
	    	var funcLocs = this.getViewModel().get('funcLocStore');
	    	
	    	if(funcLocs) {
	    		retval += " (" + funcLocs.getCount() + ")";
	    	}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseFuncLocListPage', ex);
    	}

    	return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
    	try {
    		var funcLocStore = this.getViewModel().get('funcLocStore');
    		
    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    			this.renewFuncLocGridPanel();
    		
    		if (this.getViewModel().get('constrainedFuncLocs')) {
    		    funcLocStore = this.getViewModel().get('constrainedFuncLocs');
    		}

    		var funcLocGridPanel = Ext.getCmp('funcLocGridPanel');
    		
    		funcLocGridPanel.setStore(funcLocStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseFuncLocListPage', ex);
    	}
	},

	//public
	//@override
	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('funcLocGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseFuncLocListPage', ex);
    	}
	},
	
	//private
	searchFieldVisibility: function()	{
		try {
	       var field = Ext.getCmp('searchFuncLocContainer');	
	       var para = true;
	
	       if(para === false)
	    	   field.setVisible(false);
		} catch(ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseFuncLocListPage', ex);
		}
	},
   
	//public
	getSearchValue: function() {
		try {
			var funcLocToSearch = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
			var currentFilterCriterion = Ext.getCmp('funcLocListPageFilterCriteriaCombobox').getValue();

			// Techn. Platz Nr.
			if(currentFilterCriterion == Locale.getMsg('funcLocNumberShort'))
				funcLocToSearch.set('tplnr', Ext.getCmp('searchFieldFuncLoc').getValue());
			// Kurztext
			else if(currentFilterCriterion == Locale.getMsg('shorttext'))
				funcLocToSearch.set('pltxt', Ext.getCmp('searchFieldFuncLoc').getValue());
			// Sortierfeld
			else if(currentFilterCriterion == Locale.getMsg('sortField'))
				funcLocToSearch.set('eqfnr', Ext.getCmp('searchFieldFuncLoc').getValue());
			// Standort
			else if(currentFilterCriterion == Locale.getMsg('location'))
				funcLocToSearch.set('stort', Ext.getCmp('searchFieldFuncLoc').getValue());
			// Raum
			else if(currentFilterCriterion == Locale.getMsg('room'))
				funcLocToSearch.set('msgrp', Ext.getCmp('searchFieldFuncLoc').getValue());
				
			this.getViewModel().set('searchFuncLoc', funcLocToSearch);
		
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseFuncLocListPage', ex);
		}
	},

    //public
	getFilterValue: function () {
	    var retval = '';

	    try {
	        retval = Ext.getCmp('searchFieldFuncLoc').getValue();

	        if (!retval)
	            retval = '';
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseFuncLocListPage', ex);
	    }

	    return retval;
	},
    //public
	setFilterValue: function (value) {
	    try {
	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
	            value = '';

	        retval = Ext.getCmp('searchFieldFuncLoc').setValue(value);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseFuncLocListPage', ex);
	    }
	}
});