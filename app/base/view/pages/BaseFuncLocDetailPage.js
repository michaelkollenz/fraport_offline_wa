Ext.define('AssetManagement.base.view.pages.BaseFuncLocDetailPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.FuncLocDetailPageViewModel',
        'AssetManagement.customer.controller.pages.FuncLocDetailPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.View',
        'Ext.Img',
        'Ext.grid.column.Action',
        'AssetManagement.customer.model.bo.CharactValue',
        'AssetManagement.customer.model.bo.ObjClassValue',
        'AssetManagement.customer.manager.ClassificationManager'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {	
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.FuncLocDetailPage');
                }
            } catch(ex) {
		    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseFuncLocDetailPage', ex);
		    }
            
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.FuncLocDetailPageRendererMixin'
    },

    config: {
        funcLoc: null
    },
    
	_funcLocRow: null,
	_shorttextRow: null,
	_storLocRow: null,
	_workcenterPlantRow: null,
	_producerAssetRow: null,
	_typeIdentifierRow: null,
	_constrMthAndYrRow: null,
	_nameRow: null,
	_cityRow: null,
	_streetRow: null,
	_Row: null,

    viewModel: {
        type: 'FuncLocDetailModel'
    },
    
    controller: 'FuncLocDetailController',

    buildUserInterface: function() {
    	this.callParent();
    	
    	try {
    		var myController = this.getController();
    		var me = this;
    		var items =  [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    	width: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 5,
		                    width: 1125,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            id: 'funcLocGeneralDataContainer',
		                            items: [
		                                {
		                                    xtype: 'label',
		                                    cls: 'oxHeaderLabel',
		                                    margin: '0 0 0 5',
		                                    text: Locale.getMsg('generalData')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'container',
		                                    id: 'funcLocMainGrid'
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            id: 'funcLocProducerContainer',
		                            margin: '40 0 0 0',
		                            items: [
		                                {
		                                    xtype: 'label',
		                                    cls: 'oxHeaderLabel',
		                                    margin: '0 0 0 5',
		                                    text: Locale.getMsg('producerData')
		                                },
		                                {
		                                    xtype: 'component',
		                                    height: 20
		                                },
		                                {
		                                    xtype: 'container',
		                                    id: 'producerGrid'
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            id: 'funcLocAddressContainer',
		                            items: [
		                                {
		                                    xtype: 'container',
		                                    height: 50,
		                                    margin: '25 0 0 0',
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'bottom'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'label',
		                                            flex: 1,
		                                            cls: 'oxHeaderLabel',
		                                            margin: '0 0 0 5',
		                                            padding: '0 0 20 0',
		                                            text: Locale.getMsg('address')
		                                        },
		                                        {
		                                            xtype: 'button',
		                                            id: 'funcLocAdressMapsButton',
													padding: '0',
													margin: '0 5 0 0',
								                    html: '<img width="100%" src="resources/icons/maps_small.png"></img>',
									                height: 50,
									                width: 60,
									                listeners: {
									                	click: 'openFuncLocAddress'
									                }
		                                        }
		                                    ]
		                                },
		                                {
		                                    xtype: 'container',
		                                    id: 'funcLocAddressGrid'
		                                }
		                            ]
		                        },
                                {
                                    xtype: 'container',
                                    margin: '40 5 0 5',
                                    id: 'funcLocClassContainer',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('classifications')
                                        },
                                        {
                                            xtype: 'oxdynamicgridpanel',
                                            id: 'funcLocClassGridPannel',
                                            margin: '20 0 0 0',
                                            hideContextMenuColumn: true,
                                            disableSelection: true,
                                            parentController: myController,
                                            owner:me,
                                            /*columns: [
                                                {
                                                    xtype: 'actioncolumn',
                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                      return '<img src="resources/icons/class_charact.png"  />';
                                                    },
                                                    id: 'funcLocClassIconColumn',
                                                    maxWidth: 80,
                                                    minWidth: 80,
                                                    align: 'center'
                                                },
                                                {
                                                    xtype: 'gridcolumn',
                                                    id: 'funcLocClassCharacteristicColumn',
                                                    dataIndex: 'classCharacteristic',
                                                    text: Locale.getMsg('characteristic'),
                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        var description = null;

                                                        if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('atbez')))
                                                            description = record.get('atbez');
                                                        else
                                                            description = record.get('atnam');

                                                        return description;
                                                    },
                                                    flex: 2
                                                },
                                                {
                                                    xtype: 'gridcolumn',
                                                    id: 'funcLocCharacteristicContainer',
                                                    dataIndex: 'classCharacteristicValueColumn',
                                                    text: Locale.getMsg('characteristicValue'),
                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {

                                                        var valueItem = '';
                                                        var filteredCharValues = Ext.create('Ext.data.Store', {
                                                            model: 'AssetManagement.customer.model.bo.CharactValue',
                                                            autoLoad: false
                                                        });

                                                        var valueList = Ext.create('Ext.data.Store', {
                                                            model: 'AssetManagement.customer.model.bo.ObjClassValue',
                                                            autoLoad: false
                                                        });

                                                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('classValues'))) {
                                                            record.get('classValues').each(function (classValue) {
                                                                if ((classValue.get('atinn') === record.get('atinn')) && !(classValue.get('updFlag') === 'D'))
                                                                    valueList.add(classValue);
                                                            });

                                                            if (valueList.getCount() > 0) {
                                                                valueList.each(function (value) {
                                                                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value.get('atwrt'))) {
                                                                        if (record.get('charactValues') && record.get('charactValues').getCount() > 0) {
                                                                            record.get('charactValues').each(function (charValue) {
                                                                                if (charValue.get('atwrt') === value.get('atwrt'))
                                                                                    filteredCharValues.add(charValue);
                                                                            });
                                                                        }
                                                                        else
                                                                            valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(value.get('atwrt'), record);
                                                                    }
                                                                });
                                                            }

                                                            if (filteredCharValues.getCount() !== 0) {
                                                                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(filteredCharValues.getAt(0).get('atwrt'))) {

                                                                    if ((filteredCharValues.getCount() > 0) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(filteredCharValues.getAt(0).get('atwtb')))
                                                                        filteredCharValues.each(function (itemVl) {
                                                                            valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(itemVl.get('atwtb'), record);
                                                                        });
                                                                    else
                                                                        valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(filteredCharValues.getAt(0).get('atwrt'), record);
                                                                }
                                                                else
                                                                    valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(filteredCharValues.getAt(0).get('atflv'), record);
                                                            }
                                                            else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(valueList.getAt(0).get('atflv')) && filteredCharValues.getCount() === 0)
                                                                valueItem += AssetManagement.customer.manager.ClassificationManager.CreateValueString(valueList.getAt(0).get('atflv'), record);

                                                            valueItem += "\n";

                                                        }


                                                        return valueItem;
                                                    },
                                                    flex: 1,
                                                    minWidth: 100
                                                }
                                            ],*/
                                            listeners: {
                                                cellclick: myController.onClassificationSelected,
                                                scope: myController
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    id: 'funcLocMeasPointListContainer',
                                    margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('measPoints')
                                        },
                                        {
                                            xtype: 'oxdynamicgridpanel',
                                            id: 'funcLocMeasPointGrid',
                                            hideContextMenuColumn: true,
                                            viewModel: {},
                                            margin: '20 0 0 0',
                                            parentController: myController,
                                            owner: me,
                                            /*columns: [
		                                          {
		                                              xtype: 'actioncolumn',
		                                              renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                  var imageSrc = 'resources/icons/measpoints.png';

		                                                  return '<img src="' + imageSrc + '"/>';
		                                              },
		                                              maxWidth: 80,
		                                              minWidth: 80,
		                                              enableColumnHide: false,
		                                              align: 'center'
		                                          },
		                                         {
		                                             xtype: 'gridcolumn',
		                                             flex: 1,
		                                             renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                 var measpoint = AssetManagement.customer.utils.StringUtils.trimStart(record.get('point'), '0');
		                                                 
		                                                 var measpointDesc = record.get('pttxt');
		                                                 return measpoint + '<p>' + measpointDesc + '</p>';
		                                             },
		                                             enableColumnHide: false,
		                                             dataIndex: '',
		                                             text: Locale.getMsg('measPoint')
		                                         },
		                                         {
		                                             xtype: 'gridcolumn',
		                                             flex: 1,
		                                             renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                 return record.get('psort');
		                                             },
		                                             enableColumnHide: false,
		                                             dataIndex: '',
		                                             text: Locale.getMsg('measIndicator')
		                                         },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            flex: 1,
		                                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                                return record.get('lastRec') + ' ' + record.get('lastUnit');
		                                            },
		                                            enableColumnHide: false,
		                                            dataIndex: '',
		                                            text: Locale.getMsg('lastMeasValue')
		                                        }
                                            ],*/
                                            listeners: {
                                                cellclick: myController.measPointSelected,
                                                cellcontextmenu: myController.onCellContextMenu,
                                                scope: myController
                                            }
                                        }
                                    ]
                                },
		                        {
		                            xtype: 'container',
		                            margin: '40 5 0 5',
		                            id: 'funcLocFilesAndDocumentsContainer',
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('filesAndDocuments')
                                        },
		                                {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'funcLocFileGridPanel',
		                                    margin: '20 0 0 0',
		                                    hideContextMenuColumn: true,
		                                    disableSelection: true,
		                                    parentController: myController,
		                                    owner: me,
		                                    /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		return '<img src="resources/icons/document.png"  />';
		                                            },
		                                            id: 'funcLocFileIconColumn',
		                                            maxWidth: 80,
		                                            minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocFileNameDescColumn',
		                                            dataIndex: 'fileDescription',
		                                            text: Locale.getMsg('file'),
			                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var description = record.get('fileDescription');
			                                        
			                                            return description;
			                                    	},
			                                    	flex: 2
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocFileTypeSizeColumn',
		                                            text: Locale.getMsg('typeAndSize'),
		                                            dataIndex: 'fileType',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var type = record.get('fileType');
		                                        		var size = record.get('fileSize').toFixed(2) + ' kB';
			                                        
			                                            return type +'<p>' + size + '</p>';
			                                    	},
			                                    	flex: 1,
			                                    	minWidth: 100
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocFileOriginColumn',
		                                            text:  Locale.getMsg('fileStorageLocation'),
		                                            dataIndex: 'fileOrigin',
		                                            renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        		var stringToShow = "";
		                                        		var fileOrigin = record.get('fileOrigin');
		                                        		
		                                        		if(fileOrigin === "online") {
		                                        			stringToShow = Locale.getMsg('sapSystem');
		                                        		} else if(fileOrigin === "local") {
		                                        			stringToShow = Locale.getMsg('local');
		                                        		}
		                                            
			                                            return stringToShow;
			                                    	},
		                                        	flex: 1,
		                                        	minWidth: 100,
		                                        	maxWidth: 200
		                                        }
		                                    ],*/
			                                listeners: {
		    	                        		cellclick: myController.onFileSelected,
		    	                        		cellcontextmenu: myController.onCellContextMenuFileList,
		    	                        		scope: myController
		    	                        	}
		                                }
			                        ]
		                        },
                                {
	                                xtype: 'container',
	                                id: 'funcLocHistNotifContainer',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('historicalNotifs')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'funcLocHistNotifGrid',
		                                    hideContextMenuColumn: true,
		                                    scrollable: 'vertical',
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                parentController: myController,
			                                owner: me,
			                                /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                                return '<img src="resources/icons/notif.png"  />';
		                                            },
		                                            id: 'funcLocHistNotifActionColumn1',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocHistNotifMeldungColumn1',
		                                            dataIndex: 'qmnum',
		                                            text: Locale.getMsg('notif'),
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var qmnum =  AssetManagement.customer.utils.StringUtils.trimStart(record.get('qmnum'), '0');
			                                            var qmtxt = record.get('qmtxt');
	
			                                            return qmnum +'<p>'+qmtxt+'</p>';
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocHistNotifStatusColumn1',
		                                            text: Locale.getMsg('status'),
		                                            dataIndex: 'system_status',
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var status = record.get('system_status');
	
			                                            return status;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocHistNotifArtColumn1',
		                                            text: Locale.getMsg('notifType'),
		                                            dataIndex: 'qmart',
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var qmart = record.get('qmart');
	
			                                            return qmart;
			                                        }
		                                        }
		                                    ],		 */                               
			                                listeners: {
		    	                        		cellclick: myController.histNotifSelected,
		    	                        		cellcontextmenu: myController.onCellContextMenu,
		    	                        		scope: myController
		    	                        	}
                                        }
                                    ]
                                },
                                {
	                                xtype: 'container',
	                                id: 'funcLocHistOrdersContainer',
	                                margin: '40 5 0 5',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            cls: 'oxHeaderLabel',
                                            text: Locale.getMsg('historicalOrders')
                                        },
                                        {
		                                    xtype: 'oxdynamicgridpanel',
		                                    id: 'funcLocHistOrderGrid',
		                                    hideContextMenuColumn: true,
			                                viewModel: { },
			                                margin: '20 0 0 0',
			                                parentController: myController,
			                                owner: me,
			                                /*columns: [
		                                        {
		                                            xtype: 'actioncolumn',
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                                                 return '<img src="resources/icons/order.png"  />';
		                                            },
		                                            id: 'funcLocHistOrderActionColumn1',
		                                            maxWidth: 80,
			                                        minWidth: 80,
		                                            align: 'center'
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocHistOrderColumn1',
		                                            dataIndex: 'aufnr',
		                                            text: Locale.getMsg('order'),
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                 	var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('aufnr'), '0');
					                                    var ktext = record.get('ktext');
					                                    return aufnr +'<p>'+ktext+'</p>';
					                                }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocHistOrderStatusColumn1',
		                                            text: Locale.getMsg('status'),
		                                            dataIndex: 'system_status',
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			                                            var status = record.get('system_status');
	
			                                            return status;
			                                        }
		                                        },
		                                        {
		                                            xtype: 'gridcolumn',
		                                            id: 'funcLocHistOrderArtColumn1',
		                                            text: Locale.getMsg('orderType'),
		                                            dataIndex: 'auart',
		                                            flex: 1,
		                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					                                    var auart = record.get('auart');
					                                    return auart;
					                                }
		                                        }
		                                    ],*/
		                                    listeners: {
		    	                        		cellclick: myController.histOrderSelected,
		    	                        		cellcontextmenu: myController.onCellContextMenu,
		    	                        		scope: myController
		    	                        	}
                                        }
                                    ]
                                },
		                        {
		                            xtype: 'component',
		                            height: 30
		                        }
		                    ]
		                },
		                {
		                    xtype: 'container',
		                    width: 30,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    }
		                }
		            ]
		        }
		    ];
    		
    		this.add(items); 
    		
	    	var leftGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
	    	this._funcLocRow = leftGrid.addRow(Locale.getMsg('funcLoc'), 'multilinelabel', true);
			this._shorttextRow = leftGrid.addRow(Locale.getMsg('shorttext'), 'multilinelabel', true);
			this._sortFieldRow = leftGrid.addRow(Locale.getMsg('sortField'), 'multilinelabel', true);
			this._storLocRow = leftGrid.addRow(Locale.getMsg('locationAndRoom'), 'multilinelabel', true);
			this._workcenterPlantRow = leftGrid.addRow(Locale.getMsg('workcenterPlant'), 'multilinelabel', false);
		
			var producerGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._producerAssetRow = producerGrid.addRow(Locale.getMsg('producerAsset'), 'multilinelabel', true);
			this._typeIdentifierRow = producerGrid.addRow(Locale.getMsg('typeIdentifier'), 'multilinelabel', true);
			this._constrMthAndYrRow = producerGrid.addRow(Locale.getMsg('constrMth') + ' / ' + Locale.getMsg('constrYr'), 'multilinelabel', false);
			
			var addressGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid',{});
			this._nameRow = addressGrid.addRow(Locale.getMsg('name'), 'multilinelabel', true);
			this._cityRow = addressGrid.addRow(Locale.getMsg('city'), 'multilinelabel', true);
			this._streetRow = addressGrid.addRow(Locale.getMsg('street'), 'multilinelabel', true);
			this._phonRow = addressGrid.addRow(Locale.getMsg('phone'), 'multilinelabel', false);

		    this.queryById('funcLocMainGrid').add(leftGrid);
		    this.queryById('producerGrid').add(producerGrid);
		    this.queryById('funcLocAddressGrid').add(addressGrid);
		} 
		catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseFuncLocDetailPage', ex);
		}
	},
    
    
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
    		var title = Locale.getMsg('funcLoc');

	    	var funcLoc = this.getViewModel().get('funcLoc');
	    	
	    	if(funcLoc) {
	    		title += " " + funcLoc.getDisplayIdentification();
	    	}
	    	
	    	retval = title;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseFuncLocDetailPage', ex);
		}
    	
    	return retval;
	},
	
	//protected
	//@override
	updatePageContent: function() {
		try {
			funcLoc = this.getViewModel().get('funcLoc');
			
			this.fillMainDataSection();
			
			this.manageSections(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseFuncLocDetailPage', ex);
		}
	}, 

	fillMainDataSection: function() {
		try {
			/* GENERAL DATA */
			this._funcLocRow.setContentString(funcLoc.getDisplayIdentification());
			
			// Kurztext
			if(funcLoc.get('pltxt')) {
				this._shorttextRow.show();
				this._shorttextRow.setContentString(funcLoc.get('pltxt'));
			} else {
				this._shorttextRow.hide();
			}
			
			// Sortierfeld
			if(funcLoc.get('eqfnr')) {
				this._sortFieldRow.show();
				this._sortFieldRow.setContentString(funcLoc.get('eqfnr'));
			} else {
				this._sortFieldRow.hide();
				this._sortFieldRow.setContentString('');
			}
			
			// Standort / Raum
			if(funcLoc.get('stort') || funcLoc.get('msgrp')) {
				this._storLocRow.show();
				this._storLocRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ funcLoc.get('stort'), funcLoc.get('msgrp') ], '/', false, false, '-'));
			} else {
				this._storLocRow.hide();
			}
			
			// Arbeitspl. / Werk
			if(funcLoc.get('gewrk') || funcLoc.get('wergw')) {
				this._workcenterPlantRow.show();
				this._workcenterPlantRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ funcLoc.get('gewrk'), funcLoc.get('wergw') ], '/', false, false, '-'));
			} else {
				this._workcenterPlantRow.hide();
			}
			
			/* PRODUCER DATA */
			// show / hide producerDataLabel/Gride/Spaceholder
			if(funcLoc.get('herst') || funcLoc.get('typbz') || funcLoc.get('baumm') || funcLoc.get('baujj')) {
				this.queryById('funcLocProducerContainer').show();
				
				// Hersteller der Anlage
				if(funcLoc.get('herst')) {
					this._producerAssetRow.show();
					this._producerAssetRow.setContentString(funcLoc.get('herst'));
				} else {
					this._producerAssetRow.hide();
				}
				
				// Typbezeichnung
				if(funcLoc.get('typbz')) {
					this._typeIdentifierRow.show();
					this._typeIdentifierRow.setContentString(funcLoc.get('typbz'));
				} else {
					this._typeIdentifierRow.hide();
				}			
		
				// Baumonat / Baujahr
				if(funcLoc.get('baumm') || funcLoc.get('baujj')) {
					this._constrMthAndYrRow.show();					
					this._constrMthAndYrRow.setContentString(AssetManagement.customer.utils.StringUtils.concatenate([ funcLoc.get('baumm'), funcLoc.get('baujj') ], '/', false, false, '-'));
				} else {
					this._constrMthAndYrRow.hide();
				}
				
			} else {
				this.queryById('funcLocProducerContainer').hide();
			}
			
			/* ADDRESS DATA */
			var address = funcLoc.get('address');
			
			// show / hide addressLabel/Gride/Spaceholder
			if(address && (address.get('name1') || address.get('name2') || address.get('name3') || address.get('name4') ||
			   address.get('postCode1') || address.get('city1') || address.get('city2') || address.get('street') ||
			   address.get('houseNum1') || address.get('telNumber') || address.get('telExtens'))) {
				this.queryById('funcLocAddressContainer').show();
				
				// Name
				if(address.get('name1') || address.get('name2') || address.get('name3') || address.get('name4')) {
					this._nameRow.show();
					this._nameRow.setContentString(address.get('name1') + ' ' + address.get('name2') + ' ' + 
						address.get('name3') + ' ' + address.get('name4'));
				} else {
					this._nameRow.hide();
				}
				
				// Ort
				if(address.get('postCode1') || address.get('city1') || address.get('city2')) {
					this._cityRow.show();
					this._cityRow.setContentString(address.get('postCode1') + ' ' + address.get('city1') + ' ' + 
						address.get('city2'));
				} else {
					this._cityRow.hide();
				}
				
				// Straße
				if(address.get('street') || address.get('houseNum1')) {
					this._streetRow.show();
					this._streetRow.setContentString(address.get('street') + ' ' + address.get('houseNum1'));
				} else {
					this._streetRow.hide();
				}
				
				// Telefon
				if(address.get('telNumber') || address.get('telExtens')) {
					this._phonRow.show();
					this._phonRow.setContentString(address.get('telNumber') + ' ' + address.get('telExtens'));
				} else {
					this._phonRow.hide();
				}
			} else {
				this.queryById('funcLocAddressContainer').hide();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseFuncLocDetailPage', ex);
		}
	},
	
	manageSections: function() {
		try {
		    var funcParas = AssetManagement.customer.model.bo.FuncPara.getInstance();
			var funcLoc = this.getViewModel().get('funcLoc');
			
			//files and documents
			if (funcParas.getValue1For('floc_docs_active') !== 'X') {
				Ext.getCmp('funcLocFilesAndDocumentsContainer').setHidden(true); 
			} else {
				Ext.getCmp('funcLocFilesAndDocumentsContainer').setHidden(false); 
					
				var fileStore = funcLoc.get('documents');
				Ext.getCmp('funcLocFileGridPanel').setStore(fileStore);
			}
			
			//histnotifs
			if (funcParas.getValue1For('floc_nfhist_active') !== 'X') {
				Ext.getCmp('funcLocHistNotifContainer').setHidden(true); 
			} else {
				Ext.getCmp('funcLocHistNotifContainer').setHidden(false); 
					
				var histnotifs = funcLoc.get('historicalNotifs');
				Ext.getCmp('funcLocHistNotifGrid').setStore(histnotifs); 	
			}
			
			//historders
			if (funcParas.getValue1For('floc_orhist_active') !== 'X') {
				Ext.getCmp('funcLocHistOrdersContainer').setHidden(true); 
			} else {
				Ext.getCmp('funcLocHistOrdersContainer').setHidden(false); 
					
				var historders = funcLoc.get('historicalOrders'); 
				Ext.getCmp('funcLocHistOrderGrid').setStore(historders); 
			}
			
			var address = funcLoc.get('address');
			
		    //measPoints
			if (funcParas.getValue1For('floc_mp_active') !== 'X') {
			    Ext.getCmp('funcLocMeasPointListContainer').setHidden(true);
			} else {
			    Ext.getCmp('funcLocMeasPointListContainer').setHidden(false);

			    var measPoints = funcLoc.get('measPoints');
			    Ext.getCmp('funcLocMeasPointGrid').setStore(measPoints);
			}

		    //classifications
			if (funcParas.getValue1For('floc_cl_active') !== 'X') {
			    Ext.getCmp('funcLocClassContainer').setHidden(true);
			} else {
			    Ext.getCmp('funcLocClassContainer').setHidden(false);

			    var classifications = this.getViewModel().get('preparedClassifications');
			    Ext.getCmp('funcLocClassGridPannel').setStore(classifications);
			}

			//adresse zum tp
			if (funcParas.getValue1For('floc_adr_active') === 'X' && address && (address.get('name1') || address.get('name2') || address.get('name3') || address.get('name4') ||
			   address.get('postCode1') || address.get('city1') || address.get('city2') || address.get('street') ||
			   address.get('houseNum1') || address.get('telNumber') || address.get('telExtens'))) {
				Ext.getCmp('funcLocAddressContainer').setHidden(false); 
			} else {
				Ext.getCmp('funcLocAddressContainer').setHidden(true); 
			}
			
			//maps zur tp adresse  
			if (funcParas.getValue1For('floc_maps_active') !== 'X') {
				Ext.getCmp('funcLocAdressMapsButton').setHidden(true); 
			} else {
				Ext.getCmp('funcLocAdressMapsButton').setHidden(false); 
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSections of BaseFuncLocDetailPage', ex);
		}
	}

});