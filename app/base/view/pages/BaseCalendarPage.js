Ext.define('AssetManagement.base.view.pages.BaseCalendarPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',



	requires: [
        'Ext.Viewport',
        'Ext.layout.container.Border',
        'Ext.picker.Date',
        'AssetManagement.customer.modules.calendar.util.Date',
        'AssetManagement.customer.modules.calendar.CalendarPanel',
        'AssetManagement.customer.modules.calendar.dd.UnplannedDragZone',
        'AssetManagement.customer.modules.calendar.data.Events',
        'AssetManagement.customer.modules.calendar.form.EventWindow',
		'AssetManagement.customer.model.pagemodel.CalendarPageViewModel',
		'AssetManagement.customer.controller.pages.CalendarPageController',
		'AssetManagement.customer.manager.StatusManager',
        'AssetManagement.customer.view.OxDynamicGridPanel'
	],

	inheritableStatics: {
		_instance: null,
	
	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	        	   this._instance = Ext.create('AssetManagement.customer.view.pages.CalendarPage');
	            }
	        } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseCalendarPage', ex)
			}
	        
	        return this._instance;
	    },

	    initNewInstance: function(){
	        try {
	    	    this._instance = Ext.create('AssetManagement.customer.view.pages.CalendarPage');
	    	} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initNewInstance of BaseCalendarPage', ex)
			}    
	    }

	},

	mixins:{
        rendererMixin: 'AssetManagement.customer.view.mixins.CalendarPageRendererMixin'
	},
    
	controller: 'CalendarPageController',
	
	viewModel: {
	    type: 'CalendarPageModel'
	},
	
    itemId: 'calendarPanel',
    layout:{
        type: 'hbox',
	    align: 'stretch'
	},
	
	isBuild: false,
	
    initialize: function() {
        // Minor workaround for OSX Lion scrollbars
        try {
            this.checkScrollOffset();
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseCalendarPage', ex)
		}    
    },
    
    setupReadyStore: function(){
	    try {
	    	AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
			var orders = this.getViewModel().get('orderStore');
			var store = Ext.data.StoreManager.lookup('calendarStore');
			if(this.getViewModel().get('cust001') != undefined && this.getViewModel().get('cust001') != null && this.getViewModel().get('cust001').get('intern') == 'X') {
				var tmpStore = Ext.create('Ext.data.Store', {
					fields: ['text', 'value']
				});
				this.getViewModel().set('wrkcStore', tmpStore);
			}	
			if(this.getViewModel().get('wrkcStore') != null)
				this.getViewModel().get('wrkcStore').loadData([],false);
			
			if(this.getViewModel().plannedEventStore != null){
				//event store is already initialized => reload the data from the database
				this.getViewModel().plannedEventStore.suspendEvents();
				this.getViewModel().plannedEventStore.removeAll();
				var data = AssetManagement.customer.modules.calendar.data.Events.getPlannedOperations(orders).evts;
				for(var i=0;i<data.length;i++){
					this.getViewModel().plannedEventStore.add(data[i]);
					if(this.getViewModel().get('wrkcStore') != null && !this.getViewModel().get('wrkcStore').findRecord('value', data[i].Arbpl+'|'+data[i].Werks))
						this.getViewModel().get('wrkcStore').add({ "text": data[i].Arbpl, "value": data[i].Arbpl+'|'+data[i].Werks});
				}			
				this.getViewModel().plannedEventStore.resumeEvents();
				//this.getViewModel().plannedEventStore.sync();
				
				this.getViewModel().unplannedEventStore.suspendEvents();
				this.getViewModel().unplannedEventStore.removeAll();
				var data = AssetManagement.customer.modules.calendar.data.Events.getUnplannedOperations(orders).evts;
				for(var i=0;i<data.length;i++) {
					this.getViewModel().unplannedEventStore.add(data[i]);
					if(this.getViewModel().get('wrkcStore') != null && !this.getViewModel().get('wrkcStore').findRecord('value', data[i].Arbpl+'|'+data[i].Werks))
						this.getViewModel().get('wrkcStore').add({ "text": data[i].Arbpl, "value": data[i].Arbpl+'|'+data[i].Werks});
				}
				this.getViewModel().unplannedEventStore.resumeEvents();
				//this.getViewModel().unplannedEventStore.sync();
			}
			else{			
				//initialize the data stores
				var plannedData = AssetManagement.customer.modules.calendar.data.Events.getPlannedOperations(orders);
				for(var i=0;i<plannedData.evts.length;i++){
					if(this.getViewModel().get('wrkcStore') != null && !this.getViewModel().get('wrkcStore').findRecord('value', plannedData.evts[i].Arbpl+'|'+plannedData.evts[i].Werks))
						this.getViewModel().get('wrkcStore').add({ "text": plannedData.evts[i].Arbpl, "value": plannedData.evts[i].Arbpl+'|'+plannedData.evts[i].Werks});
				}
				this.getViewModel().plannedEventStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.modules.calendar.data.EventModel',
				    data: plannedData.evts,
				    id: 'calendarStore'
				});

				var unplannedData = AssetManagement.customer.modules.calendar.data.Events.getUnplannedOperations(orders);
				for(var i=0;i<unplannedData.evts.length;i++){
					if(this.getViewModel().get('wrkcStore') != null && !this.getViewModel().get('wrkcStore').findRecord('value', unplannedData.evts[i].Arbpl+'|'+unplannedData.evts[i].Werks))
						this.getViewModel().get('wrkcStore').add({ "text": unplannedData.evts[i].Arbpl, "value": unplannedData.evts[i].Arbpl+'|'+unplannedData.evts[i].Werks});
				}
				this.getViewModel().unplannedEventStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.modules.calendar.data.EventModel',
				    data: unplannedData.evts,
				    id: 'unplannedCalendarStore'/*,
				    fields: 
				    [
				     	{	name: 'Operation', 	type: 'auto'	},
				     	//mapping fields
					    { 	name: 'aufnr',
					    	convert: function (notUsed, data) {
					        	return data.getOrderNo.call(data);
					        },
					        depends: ['Operation']
					    }
				    ],
				    
					getOrderNo: function() {
						var retval = '';
						
						try {
							var address = this.get('Operation');
							
							if(address) {
								retval = address.get('aufnr');
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured inside getOrderNo of BaseCalendarPage', ex);
						}
						
						return retval;
					}*/
						
				});
				this.setDefaultSortForUnplanned();
			}		
			
			if(this.getViewModel().get('currentArbpl') != undefined && this.getViewModel().get('currentArbpl') != null){
				//filter events
				this.filterEventsByArbpl(this.getViewModel().get('currentArbpl'));
			}
			else {
				//set current arbpl to the arbpl from the customizing and filter the events
				if(this.getViewModel().get('cust001') != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('cust001').get('order_workcenter'))) {
					this.getViewModel().set('currentArbpl', [this.getViewModel().get('cust001').get('order_workcenter'),this.getViewModel().get('cust001').get('order_plant_wrkc')]);
				}	
			}
			
			var calendarPanel = Ext.get('oxCalendar');
			if(calendarPanel != null){
				calendarPanel.component.setStore(this.getViewModel().unplannedEventStore, true);
			}
			
			this.getController().setPlannedEventStore(this.getViewModel().plannedEventStore);
			this.getController().setUnplannedEventStore(this.getViewModel().unplannedEventStore);
			this.updateUnplanned();
	    } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupStore of BaseCalendarPage', ex)
	    }
    },
    
    addItems: function() {
	    try {
	        var myController = this.getController();
	        var me = this;
	    	var items = [
		    {
		    	id: 'calender-outer',
		        xtype: 'container',
		        /*flex: 0,
		        layout: 'fit',
		        autoHeight: true,
	            align : 'stretch',*/ 
	            width: '100%',
	            flex: 2,
		        frame: true,
		        layout: {
		           type: 'vbox',
		           align: 'stretch'
		        },
		        items: [        
					{
					    id: 'app-center',
					    title: '...', // will be updated to the current view's date range
					    width: '100%',
					    flex: 0,
					    listeners: {
					        'afterrender': function(){
					            Ext.getCmp('app-center').header.addCls('app-center-header');
					    	}
				        }
					},
		            { 
		            	xtype: 'container',
		            	id: 'app-cal-ox-header',
		            	flex: 2,
		            	border: 1,
				        layout: {
					           type: 'hbox',
					           align: 'stretch'
				        },
	            		        
	    		        items: [{	
	    		        	
	                        xtype: 'container',
	                        id:'app-west',
	                        flex: 0,
	                         margins: '0 2 0 2', 
							layout: {
							    type: 'vbox',
							    align: 'stretch'
							 },
		                     items: [{
			                     xtype: 'container',
			                     //flex: 5,
			                     items: [                             
		                         {
		                        	 xtype: 'oxcombobox',
		                        	 id: 'cal_workcenter_picker',
		                        	 labelStyle: 'display: none;',
								     flex: 5,
	                                 displayField: 'text',
	                                 valueField: 'value',
	                                 queryMode: 'local',
	                                 allowBlank: false,
	                                 editable: false,
		   	                         listeners: {
		   	                         	change: this.onArbplChanged,
		   	                            scope: this
	   	                             }
								 },		                             
			                     {
			                            xtype: 'datepicker',
			                            flex: 5,
			                            id: 'app-nav-picker',
			                            cls: 'ext-cal-toolbar',
			                            startDay: 1,
			                            todayText: Locale.getMsg('calendarToday'),
			                            listeners: {
			                                'select': {
			                                    fn: function(dp, dt){
			                                        Ext.getCmp('oxCalendar').setStartDate(dt);
			                                    },
			                                    scope: this
			                                }
		                            	}	                            
			                      }]
		                     }]
	                    },
	                    {
		                    xtype: 'calendarpanel',
	                        margins: '0 2 0 2', 
		                    id: 'oxCalendar',
		                    region: 'center', 	
		                    eventStore: this.getViewModel().plannedEventStore,
		                    calendarStore: this.getViewModel().calendarStore,
		                    border: false,
		                    activeItem: 3, // month view
		                    align: 'stretch',
		    	            flex: 1,
	                        //height: 500,
	                        scrollable: 'vertical',
	                        //width: '100%', 
		                    
		                    monthViewCfg: {
		                        showHeader: true,
		                        showWeekLinks: true,
		                        showWeekNumbers: true
		                    },

		                    listeners: {
		                        'eventclick': {
		                            fn: function(vw, rec, el){
		                                this.showEditWindow(rec, el);
		                                this.clearMsg();
		                            },
		                            scope: this
		                        },
		                        'eventover': function(vw, rec, el){
		                            //console.log('Entered evt rec='+rec.data.Title+', view='+ vw.id +', el='+el.id);
		                        },
		                        'eventout': function(vw, rec, el){
		                            //console.log('Leaving evt rec='+rec.data.Title+', view='+ vw.id +', el='+el.id);
		                        },
		                        'eventupdate': {
		                            fn: function(cp, rec){
		                                this.showMsg('Event '+ rec.data.Title +' was updated');
		                            },
		                            scope: this
		                        },
		                        'eventcancel': {
		                            fn: function(cp, rec){
		                                // edit canceled
		                            },
		                            scope: this
		                        },
		                        'viewchange': {
		                            fn: function(p, vw, dateInfo){
		                                if(this.editWin){
		                                    this.editWin.hide();
		                                }
		                                if(dateInfo){
		                                    // will be null when switching to the event edit form so ignore
		                                    //Ext.getCmp('app-nav-picker').setValue(dateInfo.activeDate);
		                                    this.updateTitle(dateInfo.viewStart, dateInfo.viewEnd);
		                                }
		                            },
		                            scope: this
		                        },
		                        /*
		                        'dayclick': {
		                            fn: function(vw, dt, ad, el){
		                                this.showEditWindow({
		                                    StartDate: dt,
		                                    IsAllDay: ad
		                                }, el);
		                                this.clearMsg();
		                            },
		                            scope: this
		                        },*/
		                        'rangeselect': {
		                            fn: function(win, dates, onComplete){
		                                this.showEditWindow(dates);
		                                this.editWin.on('hide', onComplete, this, {single:true});
		                                this.clearMsg();
		                            },
		                            scope: this
		                        },
		                        'eventmove': {
		                            fn: function(vw, rec){
		                        		if(rec.StartDate){
			                                var time = rec.IsAllDay ? '' : ' \\a\\t G:i';
			                                this.getViewModel().plannedEventStore.add(rec);
			                    			this.getController().setPlannedEventStore(this.getViewModel().plannedEventStore);
			                                this.removeUnplannedRecord(rec);
			                                this.updateUnplanned();
			                                this.showMsg('Event '+ rec.Title +' was moved to '+
			                                    Ext.Date.format(rec.StartDate, ('F jS'+time)));
		                        		}
		                        		else {
			                                var mappings = AssetManagement.customer.modules.calendar.data.EventMappings,
			                                    time = rec.data[mappings.IsAllDay.name] ? '' : ' \\a\\t G:i';
			                                
			                                rec.commit();
			                                
			                                this.showMsg('Event '+ rec.data[mappings.Title.name] +' was moved to '+
			                                    Ext.Date.format(rec.data[mappings.StartDate.name], ('F jS'+time)));
		                                }
		                            },
		                            scope: this
		                        },
		                        'eventresize': {
		                            fn: function(vw, rec){
		                                rec.commit();
		                                this.showMsg('Event '+ rec.data.Title +' was updated');
		                            },
		                            scope: this
		                        },
		                        'eventdelete': {
		                            fn: function(win, rec){
		                            	this.addUnplannedRecord(rec);
		                                this.showMsg('Event '+ rec.data.Title +' was deleted');
		                            },
		                            scope: this
		                        },
		                        'initdrag': {
		                            fn: function(vw){
		                                if(this.editWin && this.editWin.isVisible()){
		                                    this.editWin.hide();
		                                }
		                            },
		                            scope: this
		                        },
		                        'objectChanged': {
		                            fn: function(vw, rec){
		                                rec.commit();
		                                this.showMsg('Event '+ rec.data.Title +' was updated');
		                            },
		                            scope: this
		                        }
		                        
		                    }
	                    }] 	                
	                },
	                //========== Unplanned items panel ==========
	                {
		   		    	 xtype: 'container',
		   		    	 id: 'unplannedOperationSection',
		   		         flex: 1,
		   		         frame: true,
		   		         layout: {
		   		            type: 'hbox',
		   		            align: 'stretch'
		   		         },
		   		         items: [
		                 {
			                xtype: 'container',
			                flex: 2,
			                height: 648,
			                width: 783,
			                layout: {
			                    type: 'vbox',
			                    align: 'stretch'
			                },
			                items: [	                 
		                    {
		                    	xtype: 'oxdynamicgridpanel',
		                    	parentController: myController,
                                owner:me,
                                id: 'unplannedOperationGridPanel',
                                hideContextMenuColumn: true,
		                        region: 'center',
		                        split: false,
		                        flex: 1,
		                        //cls: 'oxGridPanel',
		                        reference: 'list',
		                        header: false,
		                        overlapHeader: true,
		                        titleAlign: 'center',
		                        enableColumnHide: false,
		                        scrollable: 'vertical',
		                        /*columns: [
		                            {
		                                xtype: 'gridcolumn',
		                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                             		var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('Operation').get('aufnr'), '0');
		                            		var vornr = record.get('Operation').get('vornr');
		                                    var ktext = record.get('Title');
		                                    return aufnr + "/" + vornr + " " + ktext;
		                                },
		                                enableColumnHide: false,
		                                dataIndex: 'Aufnr',
		                        		flex: 1,
		                                text: Locale.getMsg('orderSlashOperation')
		                            },
		                            {
		                                xtype: 'datecolumn',
		                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                            		var date = Ext.Date.format(record.get('Fsav'), 'd.m.Y H:i');
		                             		//var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('Operation').get('aufnr'), '0');
		                            		//var vornr = record.get('Operation').get('vornr');
		                                    //var ktext = record.get('Title');
		                                    return date;
		                                },
		                                enableColumnHide: false,
		                                format: 'd.m.Y',
		                                dataIndex: 'Fsav',
		                                width: 150,
		                                autoSizeColumn: false,
		                                text: Locale.getMsg('date')
		                            },
		                            {
		                                xtype: 'gridcolumn',
		                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                            		var tpl = record.data.Order.data.funcLoc;
		                            		var retVal = '';
		                            		if(tpl != null) {
		                            			retVal = tpl.data.pltxt;
		                            		}	                            		
		                                    return retVal;
		                                },
		                                flex: 1,
		                                enableColumnHide: false,
		                                dataIndex: 'FuncLocDesc',
		                                width: 150,
		                                autoSizeColumn: true,
		                                text: Locale.getMsg('funcLoc_Short')
		                            },
		                            {
		                                xtype: 'gridcolumn',
		                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {	                            		
		                            		var retVal = '';
		                                	try{	                                
			                                	var address = record.data.Order.data.address;
			                            		if(address != null) {
			                            			retVal = address.get('postCode1');
			                            		}	                            		
			                                } catch(ex) {
			                        			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPage', ex);
			                        		}
		                                    return retVal;
		                                },
		                                enableColumnHide: false,
		                                dataIndex: 'PostalCode',
		                                width: 100,
		                                autoSizeColumn: true,
		                                text: Locale.getMsg('zipCode')
		                            },
		                            {
		                                xtype: 'gridcolumn',
		                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                            		var retVal = '';
		                            		try{	                            
			                            		var address = record.data.Order.data.address;
			                            		if(address != null) {
			                            			retVal = address.get('city1');
			                            		}
		                            		} catch(ex) {
		                            			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPage', ex);
		                            		}
		                                    return retVal;
		                                },
		                                flex: 0.6,
		                                enableColumnHide: false,
		                                dataIndex: 'City',
		                                autoSizeColumn: true,
		                                text: Locale.getMsg('city')
		                            },
		                            {
		                                xtype: 'gridcolumn',
		                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
		                            		var retVal = '';
		                            		try{	                            
			                            		var address = record.data.Order.data.address;
			                            		if(address != null) {
			                            			retVal = address.get('name1') + ' ' + address.get('name2') + ' ' + address.get('street') + ' ' + 
			                            					address.get('houseNum1');
			                            		}
		                            		} catch(ex) {
		                            			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPage', ex);
		                            		}	                            		
		                                    return retVal;
		                                },
		                                flex: 1,
		                                enableColumnHide: false,
		                                dataIndex: 'adrnr',
		                                width: 120,
		                                autoSizeColumn: true,
		                                text: Locale.getMsg('address')
		                            }
		                        ], 	   */                     
			                    listeners: {	                    
			                        afterrender: this.afterRenderUnplannedPanel		                    
			                    }		                   
		                    }]
			            }
		   		        ]
	                }
	            ]}
			];
			this.add(items);
			this.isBuild = true;		
			
	    } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addItems of BaseCalendarPage', ex)
	    }
    },

	getPageTitle: function() {
	    var retval = '';
	    
	    try {
		    retval = Locale.getMsg('calendar');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseCalendarPage', ex);
		}    
		
		return retval;
	},
	
	afterRenderUnplannedPanel: function() {
		try{
			var unplannedOperationPanel = Ext.get('unplannedOperationGridPanel');
			var cfg = {
	            view: unplannedOperationPanel.el,
	            grid: unplannedOperationPanel,
	            ddGroup: 'MonthViewDD'	            	
	        };

	        unplannedOperationPanel.dragZone = new AssetManagement.customer.modules.calendar.dd.UnplannedDragZone(unplannedOperationPanel.el, cfg);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPage', ex);
		}
	},

	//protected
	//@override
	updatePageContent: function() {
		try {
			AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
		
			var calendarOuterElement = Ext.get('calender-outer');
			this.setupReadyStore();
			if(calendarOuterElement == null) {
				this.addItems();
			}
			this.updateUnplanned();
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			
			//set visibility of workcenter combobox
			if(this.getViewModel().get('cust001').get('intern') == 'X'){ 
				var wrkcPicker = Ext.getCmp('cal_workcenter_picker');
				wrkcPicker.setVisible(true);
				wrkcPicker.clearValue();
				wrkcPicker.bindStore(this.getViewModel().get('wrkcStore'));	
			}			
			else				
				Ext.getCmp('cal_workcenter_picker').setVisible(false);				
				
			this.setInitArbpl();
			this.filterEventsByArbpl(this.getViewModel().get('currentArbpl'));
			//var orderStore = this.getViewModel().get('orderStore');
			//var orderGridPanel = Ext.getCmp('orderGridPanel');
			
			//orderGridPanel.setStore(orderStore);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseCalendarPage', ex);
		}
		finally{
		}
	},
	
	resetViewState: function() {
		this.callParent();
	
		try {
			//since the store ref it self is not nullable, put an empty one inside
			/*
			Ext.getCmp('orderGridPanel').setStore(Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.Order',
				autoLoad: false
			}));*/
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseCalendarPage', ex);
		}
	},
        
    // The edit popup window is not part of the CalendarPanel itself -- it is a separate component.
    // This makes it very easy to swap it out with a different type of window or custom view, or omit
    // it altogether. Because of this, it's up to the application code to tie the pieces together.
    // Note that this function is called from various event handlers in the CalendarPanel above.
    showEditWindow : function(rec, animateTarget){
    	try {
            if(!this.editWin){
              this.editWin = Ext.create('AssetManagement.customer.modules.calendar.form.EventWindow', {
                  calendarStore: this.calendarStore,
                constrain: true,
                listeners: {
                    'eventadd': {
                        fn: function(win, rec){
                    	    try {
                                win.hide();
                                rec.data.IsNew = false;
                                this.getViewModel().plannedEventStore.add(rec);
                                //this.getViewModel().plannedEventStore.sync();
                                this.showMsg('Event '+ rec.data.Title +' was added');
                                win.close();
                            } catch(ex) {
                			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fn of BaseCalendarPage', ex);
                		    }
                        },
                        scope: this
                    },
                    'eventupdate': {
                        fn: function(win, rec){
                    	    try {
                                win.hide();
                                rec.commit();
                                //this.getViewModel().plannedEventStore.sync();
                                this.showMsg('Event '+ rec.data.Title +' was updated');
                                win.close();
                            } catch(ex) {
                			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fn of BaseCalendarPage', ex);
                		    }    
                        },
                        scope: this
                    },
                    'eventdelete': {
                        fn: function(win, rec){
                    	    try {
                    		    this.addUnplannedRecord(rec);
                                win.hide();
                                win.close();
                                this.showMsg('Event '+ rec.data.Title +' was deleted');
                            } catch(ex) {
                    			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fn of BaseCalendarPage', ex);
                    		}    
                        },
                        scope: this
                    },
                    'eventcancel': {
                        fn: function(win, rec){
                    	    try {
                                win.hide();
                                win.close();
                            } catch(ex) {
                    			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fn of BaseCalendarPage', ex);
                    		}     
                        }
                    },
                    'editdetails': {
                        fn: function(win, rec){
                    	    try {
                                win.hide();
                                Ext.getCmp('app-calendar').showEditWindow(rec);
                            } catch(ex) {
                    			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fn of BaseCalendarPage', ex);
                    		}    
                        }
                    },
                    'eventShowOrder': {
                    	fn: function(win, rec) {
                    		try {
                    			win.hide();
                    			win.close();
                    			var aufnr = this._rec.data.Operation.get('aufnr');
                    			if(aufnr != null) {
                					AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: aufnr });
                    			}
                    		} catch(ex) {
                    			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fn of BaseCalendarPage', ex);
                    		}
                    	}                    
                    }
                    
                 }
              });
            }
            this.editWin.show(rec, animateTarget, this.getViewModel().get('stsma'));
        } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showEditWindow of BaseCalendarPage', ex);
	    }
    },
        
    // The CalendarPanel itself supports the standard Panel title config, but that title
    // only spans the calendar views.  For a title that spans the entire width of the app
    // we added a title to the layout's outer center region that is app-specific. This code
    // updates that outer title based on the currently-selected view range anytime the view changes.
    updateTitle: function(startDt, endDt){
    	try {
    		var p = Ext.getCmp('app-center'),
            fmt = Ext.Date.format;
        
            if(Ext.Date.clearTime(startDt).getTime() === Ext.Date.clearTime(endDt).getTime()){
               p.setTitle(fmt(startDt, 'F j, Y'));
            }
            else if(startDt.getFullYear() === endDt.getFullYear()){
                if(startDt.getMonth() === endDt.getMonth()){
                   p.setTitle(fmt(startDt, 'F j') + ' - ' + fmt(endDt, 'j, Y'));
                }
                else{
                    p.setTitle(fmt(startDt, 'F j') + ' - ' + fmt(endDt, 'F j, Y'));
                }
            }
            else{
                p.setTitle(fmt(startDt, 'F j, Y') + ' - ' + fmt(endDt, 'F j, Y'));
           }
        } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateTitle of BaseCalendarPage', ex);
	    }
    },
    
    // This is an application-specific way to communicate CalendarPanel event messages back to the user.
    // This could be replaced with a function to do "toast" style messages, growl messages, etc. This will
    // vary based on application requirements, which is why it's not baked into the CalendarPanel.
    showMsg: function(msg){
        //Ext.fly('app-msg').update(msg).removeCls('x-hidden');
    },
    clearMsg: function(){
        //Ext.fly('app-msg').update('').addCls('x-hidden');
    },
    
    // OSX Lion introduced dynamic scrollbars that do not take up space in the
    // body. Since certain aspects of the layout are calculated and rely on
    // scrollbar width, we add a special class if needed so that we can apply
    // static style rules rather than recalculate sizes on each resize.
    checkScrollOffset: function() {
    	try {
    		var scrollbarWidth = Ext.getScrollbarSize ? Ext.getScrollbarSize().width : Ext.getScrollBarWidth();
            
            // We check for less than 3 because the Ext scrollbar measurement gets
            // slightly padded (not sure the reason), so it's never returned as 0.
            if (scrollbarWidth < 3) {
                Ext.getBody().addCls('x-no-scrollbar');
            }
            if (Ext.isWindows) {
                Ext.getBody().addCls('x-win');
            }
        } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkScrollOffset of BaseCalendarPage', ex);
	    }
    },
    
    /*
     * Updates the unplanned items list with data from the event store
     */
    updateUnplanned: function() {
        try {
        	var operationGridPanel = Ext.getCmp('unplannedOperationGridPanel');
    		
            if(operationGridPanel) {
	           //only add events that have not been planned yet => Date is 31.12.9999
	           operationGridPanel.setStore(this.getViewModel().unplannedEventStore);
	           //operationGridPanel.getView().refresh();
               this.getController().setUnplannedEventStore(this.getViewModel().unplannedEventStore);
            }
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateUnplanned of BaseCalendarPage', ex);
	    }
    },
     
    /*
     * identifies a record in unplannedEventStore and removes it
     */
    removeUnplannedRecord: function(rec) {
		try{
			var event = null;
			for(var i=0;i<this.getViewModel().unplannedEventStore.data.length;i++){
				if(this.getViewModel().unplannedEventStore.data.items[i].data.Operation != null && 
					this.getViewModel().unplannedEventStore.data.items[i].data.Operation == rec.Operation) { 
					
					event = this.getViewModel().unplannedEventStore.data.items[i];
					break;
				}				
			}
			if(event != null){
				this.getViewModel().unplannedEventStore.remove(event);
				this.getController().setUnplannedEventStore(this.getViewModel().unplannedEventStore);				
			}			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeUnplannedRecord of BaseCalendarPage', ex);
		}
    },
     
    /*
     * identifies a record in plannedEventStore and removes it
     * sets ntan and nten to 9999/12/31  
     */
    removePlannedRecord: function(rec) {
		try{
			var event = null;
			for(var i=0;i<this.getViewModel().plannedEventStore.data.length;i++){
				if(this.getViewModel().plannedEventStore.data.items[i].data.Operation != null && 
					this.getViewModel().plannedEventStore.data.items[i].data.Operation.get('aufnr') == rec.data.Operation.get('aufnr') &&
					this.getViewModel().plannedEventStore.data.items[i].data.Operation.get('vornr') == rec.data.Operation.get('vornr') && 
					this.getViewModel().plannedEventStore.data.items[i].data.Operation.get('split') == rec.data.Operation.get('split')) { 
					
					event = this.getViewModel().plannedEventStore.data.items[i];
					break;
				}				
			}
			if(event != null){
				rec.data.IsModified =  true;
				rec.data.StartDate = new Date(9999,11,31,0,0,0);
				rec.data.EndDate = new Date(9999,11,31,0,0,0);
				this.getViewModel().plannedEventStore.remove(rec);
				//this.getViewModel().plannedEventStore.sync();
				this.getController().setPlannedEventStore(this.getViewModel().plannedEventStore);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPage', ex);
		}
    },
    
    /**
     * adds an existing record to the unplannedEventStore
     * calls removePlannedRecord(rec) internally to ensure consistency 
     */
    addUnplannedRecord: function(rec) {
    	try {
    		this.removePlannedRecord(rec);
    		this.getViewModel().unplannedEventStore.add(rec);
			this.setDefaultSortForUnplanned();
    		this.updateUnplanned();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRenderUnplannedPanel of BaseCalendarPage', ex);
		}
    },

    /**
     * sets the sorting of the unplannedEventStore to the default value
     */
    setDefaultSortForUnplanned: function() {
    	try {
    	    this.getViewModel().unplannedEventStore.sort('Fsav', 'ASC');
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDefaultSortForUnplanned of BaseCalendarPage', ex);
		}    
    },
    
    /**
     * fired after the workcenter-value has been changed
     */
	onArbplChanged: function(combo, record) {
		try {
			if(record != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record)) {
				var arr = record.split("|");
				this.getViewModel().set('currentArbpl', arr);
				var wrkcPicker = Ext.getCmp('cal_workcenter_picker');	
				this.filterEventsByArbpl(this.getViewModel().get('currentArbpl') );
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onArbplChanged of BaseCalendarPage', ex)
		}
    },
     
    /**
     * Filters the Events store by the provided arbpl
     */
	filterEventsByArbpl: function(arbplArray) {
		try {		
			var arbpl = arbplArray[0];
			var werk = arbplArray[1];
			this.getViewModel().plannedEventStore.filter('Arbpl', arbpl);
			this.getViewModel().plannedEventStore.filter('Werks', werk);
			this.getController().setPlannedEventStore(this.getViewModel().plannedEventStore);
			//this.getViewModel().plannedEventStore.sync();
			
			this.getViewModel().unplannedEventStore.filter('Arbpl', arbpl);
			this.getViewModel().unplannedEventStore.filter('Werks', werk);
			this.setDefaultSortForUnplanned();
    		this.updateUnplanned();			
    		Ext.getCmp('oxCalendar').updateStores(this.getViewModel().plannedEventStore);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onArbplChanged of BaseCalendarPage', ex)
		} 
	},
	
	setInitArbpl: function() {
		try{
			var arbpl = this.getViewModel().get('currentArbpl')[0];
			var werk = this.getViewModel().get('currentArbpl')[1];
			Ext.getCmp('cal_workcenter_picker').select(arbpl+'|'+werk);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitArbpl of BaseCalendarPage', ex)
		}
	}
},
function() {
    /*
     * A few Ext overrides needed to work around issues in the calendar
     */
    
    Ext.form.Basic.override({
        reset: function() {
            var me = this;
            // This causes field events to be ignored. This is a problem for the
            // DateTimeField since it relies on handling the all-day checkbox state
            // changes to refresh its layout. In general, this batching is really not
            // needed -- it was an artifact of pre-4.0 performance issues and can be removed.
            //me.batchLayouts(function() {
                me.getFields().each(function(f) {
                    f.reset();
                });
            //});
            return me;
        }
    });
    /*
    // Currently MemoryProxy really only functions for read-only data. Since we want
    // to simulate CRUD transactions we have to at the very least allow them to be
    // marked as completed and successful, otherwise they will never filter back to the
    // UI components correctly.
    Ext.data.MemoryProxy.override({
        updateOperation: function(operation, callback, scope) {
            operation.setCompleted();
            operation.setSuccessful();
            Ext.callback(callback, scope || this, [operation]);
        },
        create: function() {
            this.updateOperation.apply(this, arguments);
        },
        update: function() {
            this.updateOperation.apply(this, arguments);
        },
        destroy: function() {
            this.updateOperation.apply(this, arguments);
        }
    });*/


});