Ext.define('AssetManagement.base.view.pages.BaseWeListPage', {
  extend: 'AssetManagement.customer.view.pages.OxPage',
  alias: 'widget.BaseWeListPage',

  requires: [
    'AssetManagement.customer.model.pagemodel.WeListPageViewModel',
    'AssetManagement.customer.controller.pages.WeListPageController'

  ],

  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance === null) {
          this._instance = Ext.create('AssetManagement.customer.view.pages.WeListPage');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseWeListPage', ex);
      }

      return this._instance;
    }
  },

  config: {
    flex: 1,
    bodyCls: ''
  },

  mixins: {
    rendererMixin: 'AssetManagement.customer.view.mixins.WeListPageRendererMixin'
  },

  viewModel: {
    type: 'WeListPageModel'
  },

  controller: 'WeListPageController',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  buildUserInterface: function () {
    try {

      var items = [
        {
          xtype: 'container',
          layout: {
            type: 'hbox',
            align: 'stretch'
          },
          items: [
            {
              xtype: 'container',
              flex: 1,
              layout: {
                type: 'vbox',
                align: 'stretch'
              },
              items: [
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  flex: 1,
                  layout: {
                    type: 'vbox',
                    align: 'stretch'
                  },
                  items: [
                    {
                      xtype: 'component',
                      height: 10
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'poScanPoNumber',
                      fieldLabel: Locale.getMsg('purchOrderNo'),
                      // maxLength: 18,
                      height: 40,
                      padding: '10 10 10 10',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onPoNoTextKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'oxtextfield',
                      id: 'poScanReferenceNumber',
                      fieldLabel: Locale.getMsg('referenceNumber'),
                      maxLength: 16,
                      height: 40,
                      padding: '10 10 10 10',
                      enableKeyEvents: true,
                      listeners: {
                        keypress: 'onReferenceNoTextKeypress'
                      },
                      triggers: {
                        clear: {
                          hidden: false,
                          cls: 'x-form-clear-trigger',
                          weight: 10,
                          extraCls: 'ox-custom-clear-trigger-textfield',
                          handler: function () {
                            this.setValue('');
                          }
                        }
                      }
                    },
                    {
                      xtype: 'button',
                      height: 52,
                      maxWidth: 54,
                      html: '<div><img  src="resources/icons/search.png"></img></div>',
                      id: 'searchPo',
                      listeners: {
                        click: 'onSearchPoClick'
                      }
                    }
                  ]
                },
                {
                  xtype: 'component',
                  height: 10
                },
                {
                  xtype: 'container',
                  id: 'weListGridContainer',
                  flex: 1,
                  minHeight: 200,
                  layout: {
                    type: 'hbox',
                    align: 'stretch'
                  },
                  margin: '20 0 0 0',
                  items: [
                    this.getNewWeListGridPanel()
                  ]
                },
                {
                  xtype: 'component',
                  height: 400
                }
              ]
            }
          ]
        }
      ];

      this.add(items);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseWeListPage', ex);
    }
  },

  getPageTitle: function () {
    var retval = '';
    try {
      retval = Locale.getMsg('WE');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseWeListPage', ex);
    }

    return retval;
  },


  updatePageContent: function () {
    try {
      var poItemsStore = this.getViewModel().get('poItemsStore');
      var weListGridPanel = Ext.getCmp('weListGridPanel');
      weListGridPanel.setStore(poItemsStore);
      if (poItemsStore && poItemsStore.getCount() == 0)
        Ext.getCmp('poScanPoNumber').focus();
      else
        weListGridPanel.focus();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseWeListPage', ex);
    }
  },

  afterRefreshView: function () {
    try {

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRefreshView of BaseWeListPage', ex);
    }
  },

  //private
  getNewWeListGridPanel: function () {
    var retval = null;

    try {
      var myController = this.getController();
      var me = this;
      var rendererMixins = this.mixins.rendererMixin;

      retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
        id: 'weListGridPanel',
        flex: 1,
        useLoadingIndicator: true,
        hideContextMenuColumn: true,
        emptyText: Locale.getMsg('noItems'),
        scrollable: 'vertical',
        parentController: myController,
        owner: me,
        columns: [
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('position'),
            dataIndex: 'ebelp',
                                width: 40,
                                // flex: 2,
            renderer: rendererMixins.wePositionColumnRenderer
          },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('material'),
            dataIndex: 'matnr',
            flex: 2,
            renderer: rendererMixins.weMaterialColumnRenderer
          },
          {
            xtype: 'gridcolumn',
            text: Locale.getMsg('quantity'),
            dataIndex: 'menge_ist',
            flex: 2,
            renderer: rendererMixins.weMengeColumnRenderer
          },
          {
            xtype: 'widgetcolumn',
            text: Locale.getMsg('deliveryQuant'),
            width: 70,
            layout: {
              type: 'vbox',
              align: 'stretch'
            },

            widget: {
              xtype: 'oxnumberfield',
              fieldLabel: '',
              padding: '10 0 0 0',
              width: 40,
              hideTrigger: true, //hides arrows
              keyNavEnabled: false, //can't imput values with arrow keys
              mouseWheelEnabled: false, //nor the mouseweel
              allowDecimals: false,
              allowNegativeValues: false,
              validateOnChange: true,
              dataIndex: 'menge_ist',
              listeners: {
                change: 'onDeliveredAmmountChange',
                //focusleave: 'onMeanValueFieldLooseFocus',
                scope: myController
              }
            },
            onWidgetAttach: rendererMixins.weMengeWidgetAttached
          },
          {
            xtype: 'checkcolumn',
            width: 60,
            dataIndex: 'elikz',
            tdCls: 'oxCheckBoxGridColumnItem',
            text: Locale.getMsg('ok'),
            listeners: {
              checkchange: 'onDeliveredCheckChange',
              scope: myController
            }
          }
        ]
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewWeListGridPanel of BaseWeListPage', ex);
    }

    return retval;
  },

  setSearchFieldValues: function (poInput, reference) {
    try {
      var myModel = this.getViewModel();
      Ext.getCmp('poScanPoNumber').setValue(poInput);
      Ext.getCmp('poScanReferenceNumber').setValue(reference);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSearchFieldValues of BaseWeListPage', ex);
    }
  },

  transferViewStateIntoModel: function () {
    try {
      var myModel = this.getViewModel();
      var poInput = Ext.getCmp('poScanPoNumber').getValue();
      var myController = this.getController();
      myController._argumentsObject['poInput'] = poInput;

      var hasSplit = AssetManagement.customer.utils.StringUtils.countOccurences(poInput, '/');
      if (hasSplit > 0) {
        var split = AssetManagement.customer.utils.StringUtils.splitStringHalves(poInput, '/');
        myModel.set('poNumber', split[0]);
        myModel.set('poPos', split[1]);
      } else {
        myModel.set('poNumber', poInput);
      }

      var reference = Ext.getCmp('poScanReferenceNumber').getValue();
      myController._argumentsObject['reference'] = reference;
      myModel.set('reference', reference);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseWeListPage', ex);
    }
  },


  //protected
  onActivation: function() {
    try {
      var poItemsStore = this.getViewModel().get('poItemsStore');
      var weListGridPanel = Ext.getCmp('weListGridPanel');
      if (poItemsStore && poItemsStore.getCount() == 0)
        Ext.getCmp('poScanPoNumber').focus();
      else
        weListGridPanel.focus();
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onActivation of BaseOxPage', ex);
    }
  },

  transferModelToViewState: function () {
    try {
      var myModel = this.getViewModel();
      var poInput = myModel.get('poInput');
      var reference = myModel.get('reference');
      this.setSearchFieldValues(poInput, reference);

      var poItemStore = myModel.get('poItemsStore');

      if (!poItemStore || poItemStore.getCount() === 0) {
        poItemStore = Ext.create('Ext.data.Store', {
          model: 'AssetManagement.customer.model.bo.PoItem'
        });

        myModel.set('poItemsStore', poItemStore);
        if (poInput) {
          this.onSearchPoClick();
        } else {
          this.refreshView(); //refresh the page, because it was just recently filled with data
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelToViewState of BaseWeListPage', ex);
    }
  }
});