Ext.define('AssetManagement.base.view.pages.BasePoDetailPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',
    alias: 'widget.BasePoDetailPage',

    requires: [
        'AssetManagement.customer.model.pagemodel.PoDetailPageViewModel',
        'AssetManagement.customer.controller.pages.PoDetailPageController'

    ],

	inheritableStatics: {
		_instance: null,

	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	    	       this._instance = Ext.create('AssetManagement.customer.view.pages.PoDetailPage');
	            }
	        } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BasePoDetailPage', ex);
		    }

	        return this._instance;
	    }
	},

    config: {
        flex: 1,
        bodyCls: ''
    },

    viewModel: {
        type: 'PoDetailPageModel'
    },

    controller: 'PoDetailPageController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    buildUserInterface: function() {
		try {
            var items = [
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'label',
                                            id: 'poDetailMaterialNo',
                                            text: '',
                                            maxLength: 18,
                                            height: 40,
                                            readOnly: true,
                                            padding: '10 10 10 10'
                                        },
                                        {
                                            xtype: 'component',
                                            width: 10
                                        },
                                        {
                                            xtype: 'label',
                                            id: 'poDetailMaterialText',
                                            fieldLabel: ' ',
                                            maxLength: 18,
                                            height: 40,
                                            readOnly: true,
                                            padding: '10 10 10 10'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'label',
                                    id: 'poDetailQuantLabel',
                                    text: 'Bestellt / geliefert:',
                                    height: 40,
                                    readOnly: true,
                                    padding: '10 10 10 10'
                                },
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'label',
                                    id: 'poDetailQuant',
                                    text: '',
                                    height: 40,
                                    readOnly: true,
                                    padding: '10 10 10 10'
                                },
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'label',
                                    id: 'poDetailQuantIsLabel',
                                    text: Locale.getMsg('quantity'),
                                    height: 40,
                                    readOnly: true,
                                    padding: '10 10 10 10'
                                },
                                {
                                    xtype: 'oxtextfield',
                                    id: 'poDetailQuantityIs',
                                    height: 40,
                                    readOnly: true,
                                    padding: '10 10 10 10',
                                    maxLength: 14
                                },
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            height: 52,
                                            width: 54,
                                            html: '<div><img src="resources/icons/MM/remove.png"></img></div>',
                                            id: 'removeQuantPoDetailPage',
                                            listeners: {
                                                click: 'onRemoveButtonClick'
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            margin: '0 0 0 20',
                                            height: 52,
                                            width: 54,
                                            html: '<div><img src="resources/icons/MM/add.png"></img></div>',
                                            id: 'addQuantPoDetailPage',
                                            listeners: {
                                                click: 'onAddButtonClick'
                                            }
                                        }

                                    ]
                                },
                                {
                                    xtype: 'component',
                                    height: 10
                                },
                                {
                                    xtype: 'button',
                                    height: 52,
                                    width: 54,
                                    html: '<div><img  src="resources/icons/save.png"></img></div>',
                                    id: 'savePo',
                                    listeners: {
                                        click: 'onSaveClick'
                                    }
                                }
                            ]
                        }

                    ]
                }
            ];

            this.add(items);

		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of poDetailPage', ex);
		}
  	},

	getPageTitle: function() {
		var retval = '';
		try {
			retval = Locale.getMsg('WE');
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of poDetailPage', ex);
		}

		return retval;
	},

    updatePageContent: function() {
        try {
            this.transferModelStateIntoView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of poDetailPage', ex);
        }
    },
    transferModelStateIntoView: function() {
        try {
            var myModel = this.getViewModel();
            var poItem = myModel.get('poItem');

            if (poItem) {
                if (poItem.get('matnr')) {
                    Ext.getCmp('poDetailMaterialNo').setText(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('matnr')));
                }
                if (poItem.get('txz01')) {
                    Ext.getCmp('poDetailMaterialText').setText(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('txz01')));
                }
                if(poItem.get('menge')){
                    var menge = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('menge'));
                    var menge_del = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('menge_del'));
                    Ext.getCmp('poDetailQuant').setText(menge + ' / ' + menge_del);
                }
                if (poItem.get('menge_ist')) {
                    Ext.getCmp('poDetailQuantityIs').setValue(AssetManagement.customer.utils.StringUtils.filterLeadingZeros(poItem.get('menge_ist')));
                } else{
                    Ext.getCmp('poDetailQuantityIs').setValue(null);
                }
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of FbUmbPage', ex);
        }
    }
});