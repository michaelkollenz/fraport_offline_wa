Ext.define('AssetManagement.base.view.pages.BaseEquiChangePositionPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.EquiChangePositionPageViewModel',
        'AssetManagement.customer.controller.pages.EquiChangePositionPageController',
        'Ext.container.Container',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'Ext.form.field.Date'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.EquiChangePositionPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseEquiChangePositionPage', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'EquiChangePositionPageModel'
    },

    controller: 'EquiChangePositionPageController',

    buildUserInterface: function () {
        try {
            var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 20
		                        },
		                        {
		                            xtype: 'label',
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('generalData')
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            margin: '15 0 0 0',
		                            id: 'equiChangePositionEquiTextField',
		                            fieldLabel: Locale.getMsg('equipment'),
		                            labelWidth: 150,
		                            maxLength: 18,
		                            disabled: true
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            margin: '10 0 0 0',
		                            id: 'equiChangePositionShortTextField',
		                            fieldLabel: Locale.getMsg('shorttext'),
		                            labelWidth: 150,
		                            maxLength: 40,
		                            disabled: true
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiChangePositionFuncLocTextField',
		                            margin: '10 0 0 0',
		                            fieldLabel: Locale.getMsg('funcLoc'),
		                            labelWidth: 150,
		                            disabled: true
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiChangePositionSuperiorEquiTextField',
		                            margin: '10 0 0 0',
		                            fieldLabel: Locale.getMsg('superordEqui'),
		                            labelWidth: 150,
		                            disabled: true
		                        },
		                        {
		                            xtype: 'component',
		                            height: 40
		                        },
		                        {
		                            xtype: 'label',
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('newEquiLocation')
		                        },
		                        {

		                            xtype: 'container',
		                            margin: '10 0 0 0',
		                            layout: {
		                                type: 'hbox',
		                                align: 'middle'
		                            },
		                            items: [
	                                    {
	                                        xtype: 'oxtextfield',
	                                        id: 'equiChangePositionNewFuncLocTextField',
	                                        height: 20,
	                                        flex: 1,
	                                        fieldLabel: Locale.getMsg('funcLoc'),
	                                        maxLength: 30,
	                                        maxWidth: 440,
	                                        labelWidth: 150
	                                    },
	                                    {
	                                        xtype: 'component',
	                                        width: 10
	                                    },
	                                    {
	                                        xtype: 'container',
	                                        width: 40,
	                                        height: 40,
	                                        top: 20,
	                                        items: [
	                                            {
	                                                xtype: 'button',
	                                                bind: {
	                                                    html: '<div><img width="100%" src="resources/icons/search.png"></img></div>'
	                                                },
	                                                listeners: {
	                                                    click: 'onSelectSuperiorFuncLocClick'
	                                                }
	                                            }
	                                        ]
	                                    }
		                            ]
		                        },
		                        {

		                            xtype: 'container',
		                            margin: '10 0 0 0',
		                            layout: {
		                                type: 'hbox',
		                                align: 'middle'
		                            },
		                            items: [
									    {
									        xtype: 'oxtextfield',
									        id: 'equiChangePositionNewEquiTextField',
									        height: 20,
									        flex: 1,
									        fieldLabel: Locale.getMsg('superordEqui'),
									        maxLength: 18,
									        maxWidth: 440,
									        labelWidth: 150
									    },
									    {
									        xtype: 'component',
									        width: 10
									    },
                                        {
                                            xtype: 'container',
                                            width: 40,
                                            height: 40,
                                            top: 20,
                                            items: [
	                                            {
	                                                xtype: 'button',
	                                                bind: {
	                                                    html: '<div><img width="100%" src="resources/icons/search.png"></img></div>'
	                                                },
	                                                listeners: {
	                                                    click: 'onSelectSuperiorEquipmentClick'
	                                                }
	                                            }
                                            ]
                                        }
		                            ]
		                        },
		                        {
		                            xtype: 'oxtextfield',
		                            id: 'equiChangePositionNewPosTextField',
		                            margin: '5 0 0 0',
		                            maxLength: 4,
		                            fieldLabel: Locale.getMsg('position'),
		                            labelWidth: 150,
		                            maxWidth: 210,
                                    hidden: true
		                        },
		                        {
		                            xtype: 'container',
		                            margin: '10 0 0 0',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'datefield',
		                                    id: 'equiChangePositionDateField',
		                                    width: 280,
		                                    format: 'd.m.Y',
		                                    fieldLabel: Locale.getMsg('instDismTime'),
		                                    labelWidth: 150,
		                                    enforceMaxLength: true,
		                                    maxLength: 10
		                                },
		                                {
		                                    xtype: 'component',
		                                    width: 10
		                                },
		                                {
		                                    xtype: 'timefield',
		                                    id: 'equiChangePositionTimeTextField',
		                                    width: 200,
		                                    format: 'H:i',
		                                    labelStyle: 'display: none;',
		                                    enforceMaxLength: true,
		                                    maxLength: 5
		                                }
		                            ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                }
		            ]
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseEquiChangePositionPage', ex);
        }
    },

    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('equipmentInOut');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseEquiChangePositionPage', ex);
        }

        return retval;
    },

    //protected
    //@override
    updatePageContent: function () {
        try {
            this.transferModelStateIntoView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseEquiChangePositionPage', ex);
        }
    },

    transferModelStateIntoView: function () {
        try {
            this.fillEquipmentFields();
            this.manageInputFields();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseEquiChangePositionPage', ex);
        }
    },

    fillEquipmentFields: function () {
        try {
            var equi = this.getViewModel().get('equipment');
            var funcLoc = equi.get('funcLoc');
            var superiorEqui = equi.get('superiorEquipment');

            var flocString = funcLoc ? AssetManagement.customer.utils.StringUtils.concatenate([funcLoc.getDisplayIdentification(), funcLoc.get('pltxt')], '-', true) : '';
            var superiorEquiString = '';

            if (superiorEqui) {
                var equnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(superiorEqui.get('equnr'));
                superiorEquiString = AssetManagement.customer.utils.StringUtils.concatenate([equnr, superiorEqui.get('eqktx')], '-', true);
            }

            Ext.getCmp('equiChangePositionEquiTextField').setValue(equi.get('equnr'));
            Ext.getCmp('equiChangePositionShortTextField').setValue(equi.get('eqktx'));
            Ext.getCmp('equiChangePositionFuncLocTextField').setValue(flocString);
            Ext.getCmp('equiChangePositionSuperiorEquiTextField').setValue(superiorEquiString);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillEquipmentFields of BaseEquiChangePositionPage', ex);
        }
    },

    manageInputFields: function () {
        try {
            this.refreshNewFuncLocTextField();
            this.refreshNewSuperiorEquiTextField();

            var myModel = this.getViewModel();

            var newPosition = myModel.get('newPosition');
            var changeDate = myModel.get('changeDate');

            Ext.getCmp('equiChangePositionNewPosTextField').setValue(newPosition);
            Ext.getCmp('equiChangePositionDateField').setValue(changeDate);
            Ext.getCmp('equiChangePositionTimeTextField').setValue(changeDate);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageInputFields of BaseEquiChangePositionPage', ex);
        }
    },

    refreshNewFuncLocTextField: function () {
        try {
            var funcLoc = this.getViewModel().get('funcLoc');
            var flocString = funcLoc ? funcLoc.getDisplayIdentification() : '';

            Ext.getCmp('equiChangePositionNewFuncLocTextField').setValue(flocString);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshNewFuncLocTextField of BaseEquiChangePositionPage', ex);
        }
    },

    refreshNewSuperiorEquiTextField: function () {
        try {
            var superiorEqui = this.getViewModel().get('superiorEqui');
            var equiString = superiorEqui ? AssetManagement.customer.utils.StringUtils.filterLeadingZeros(superiorEqui.get('equnr')) : '';

            Ext.getCmp('equiChangePositionNewEquiTextField').setValue(equiString);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshNewSuperiorEquiTextField of BaseEquiChangePositionPage', ex);
        }
    },

    getCurrentInputValues: function () {
        var retval = null;

        try {
            retval = {};

            retval.tplnr = Ext.getCmp('equiChangePositionNewFuncLocTextField').getValue();
            retval.hequi = Ext.getCmp('equiChangePositionNewEquiTextField').getValue();
            retval.heqnr = Ext.getCmp('equiChangePositionNewPosTextField').getValue();

            retval.changeDate = Ext.getCmp('equiChangePositionDateField').getValue();
            retval.changeTime = Ext.getCmp('equiChangePositionTimeTextField').getValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentValues of BaseEquiChangePositionPage', ex);
            retval = null;
        }

        return retval;
    },

    //transfers input fields values into the model
    transferViewStateIntoModel: function () {
        try {
            //func loc and head equi do not have to be set
            var newPosition = Ext.getCmp('equiChangePositionNewPosTextField').getValue();
            var changeDate = null;

            //merge change date and time objects into one
            var date = Ext.getCmp('equiChangePositionDateField').getValue();
            var time = Ext.getCmp('equiChangePositionTimeTextField').getValue();

            var changeDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes());

            var myModel = this.getViewModel();

            myModel.set('newPosition', newPosition);
            myModel.set('changeDate', changeDate);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseEquiChangePositionPage', ex);
        }
    }
});