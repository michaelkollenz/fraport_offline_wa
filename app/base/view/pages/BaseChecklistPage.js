﻿Ext.define('AssetManagement.base.view.pages.BaseChecklistPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',

    requires: [
        'AssetManagement.customer.model.pagemodel.ChecklistPageViewModel',
        'AssetManagement.customer.controller.pages.ChecklistPageController',
        'AssetManagement.customer.view.utils.ComponentColumn',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.utils.OxTextField',
        'Ext.grid.plugin.CellEditing',
        'AssetManagement.customer.utils.StringUtils'
    ],

    inheritableStatics: {
        _instance: null,
        getInstance: function () {
            try {
                //CR comment
                //this page uses DEFERRED RENDERING for some elements to improve performence. 
                //it first creates an empty dom to render an into and after a fixed time, creates an object that will render to that dom
                //once an object is created it remains in the instance of the page after we leave the page, but the dom element is cleared
                //if that happens, the objects tries to render to an undefined dom element and would couse a crash
                //because of that, the instance of the page has to be cleared each time - the performence is still better then before using this rendering method.
                if (this._instance !== null) {
                    this._instance.destroy();
                    this._instance = null;
                }

                this._instance = Ext.create('AssetManagement.customer.view.pages.ChecklistPage');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseChecklistPage', ex);
            }

            return this._instance;
        }
    },

    _checklistRow: null,
    _funcLocRow: null,
    _equiRow: null,
    _mapOfCodeGrpStores: null,
    viewModel: {
        type: 'ChecklistPageViewModel'
    },

    controller: 'ChecklistPageController',
    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.ChecklistPageRendererMixin'
    },

    buildUserInterface: function () {
        try {
            var me = this;
            var myController = this.getController();
            var rendererMixins = this.mixins.rendererMixin;

            var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
                groupHeaderTpl:
                    '{[values.rows[0].data.qpOpertxt]}' //this strange string allows us to access the specific field of the grouped store to display in the header, in this case a text of the oper.
                    + '({rows.length})', 
                collapsible: false
            });

            var items = [

                 {
                     xtype: 'container',
                     flex: 1,

                     layout: {
                         type: 'hbox',
                         align: 'stretch'
                     },

                     items: [

                         {
                             xtype: 'component',
                             width: 10
                         },

                         {
                             xtype: 'container',
                             flex: 1,

                             layout: {
                                 type: 'vbox',
                                 align: 'stretch'
                             },

                             items: [

                                 {
                                     xtype: 'container',
                                     id: 'checklistgeneralDataContainer',
                                     items:
                                         [

                                              {
                                                  xtype: 'label',
                                                  margin: '0 0 0 5',
                                                  cls: 'oxHeaderLabel'
                                              },

                                               {
                                                   xtype: 'component',
                                                   height: 10
                                               },

                                              {
                                                  xtype: 'container',

                                                  layout: {
                                                      type: 'hbox',
                                                      align: 'stretch'
                                                  },

                                                  flex: 1,
                                                  items: [

                                                      {
                                                          xtype: 'container',
                                                          flex: 1,
                                                          id: 'checklistMainGrid'
                                                      }

                                                  ]
                                              }

                                         ]
                                 },

                         {
                             xtype: 'component',
                             height: 10
                         },

                         {
                             xtype: 'container',
                             id: 'checklistCriteria',
                             flex: 1,

                             layout: {
                                 type: 'hbox',
                                 align: 'stretch'
                             },

                             items: [

                                 {
                                     xtype: 'oxgridpanel',
                                     id: 'inGridPanel',
                                     text: Locale.getMsg('checklists'),
                                     hideContextMenuColumn: true,
                                     flex: 1,
                                     disableSelection: true,
                                     sortableColumns: false,
                                     columns: [

                                        {
                                            xtype: 'actioncolumn',
                                            id: 'checklistRemark',
                                            renderer: rendererMixins.checklistRemarksColumnRenderer,
                                            maxWidth: 80,
                                            minWidth: 80,
                                            align: 'center',

                                            listeners: {
                                                click: 'onSelectChecklistRemark'
                                            }
                                        },

                                        {
                                            xtype: 'gridcolumn',
                                            text: Locale.getMsg('inspection'),
                                            id: 'inspectionColumn',
                                            flex: 2,
                                            renderer: rendererMixins.checklistInspectionColumnRenderer
                                        },

                                        {
                                            xtype: 'gridcolumn',
                                            id: 'checklistsM',
                                            text: Locale.getMsg('checklist_Result'),
                                            enableColumnHide: true,
                                            sortableColumns: false,
                                            columns: [

                                                {
                                                    xtype: 'gridcolumn',
                                                    dataIndex: 'meanValue',
                                                    width: 150,

                                                    layout: {
                                                        type: 'vbox',
                                                        align: 'stretch'
                                                    },

                                                    id: 'mValueColumn',

                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        return rendererMixins.checklistMeanValueOrDropdownRenederer.call(me, value, metaData, record, rowIndex, colIndex, store, view);
                                                    }
                                                },

                                                {
                                                    xtype: 'checkcolumn',
                                                    width: 60,
                                                    id: 'checkOK',
                                                    dataIndex: 'checkBoxOK',
                                                    tdCls: 'oxCheckBoxGridColumnItem',
                                                    text: Locale.getMsg('inOrder'),

                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        return rendererMixins.checklistCheckboxRenderer.call(me, value, metaData, record, rowIndex, colIndex, store, view);
                                                    },

                                                    listeners: {
                                                        beforecheckchange: 'beforeCheckboxCheckChange',
                                                        checkChange: 'checkboxCheckChange'
                                                    }
                                                },

                                                {
                                                    xtype: 'checkcolumn',
                                                    tdCls: 'oxCheckBoxNokColumnItem',
                                                    width: 60,
                                                    id: 'checkNOK',
                                                    dataIndex: 'checkBoxNOK',
                                                    text: Locale.getMsg('notInOrder'),

                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        return rendererMixins.checklistCheckboxRenderer.call(me, value, metaData, record, rowIndex, colIndex, store, view);
                                                    },

                                                    listeners: {
                                                        beforecheckchange: 'beforeCheckboxCheckChange',
                                                        checkChange: 'checkboxCheckChange'
                                                    }
                                                },

                                                {
                                                    xtype: 'checkcolumn',
                                                    tdCls: 'oxCheckBoxNotRelColumnItem',
                                                    id: 'checkNREL',
                                                    width: 60,
                                                    dataIndex: 'checkBoxNREL',
                                                    text: Locale.getMsg('notRelevant'),

                                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                                                        return rendererMixins.checklistCheckboxRenderer.call(me, value, metaData, record, rowIndex, colIndex, store, view);
                                                    },

                                                    listeners: {
                                                        beforecheckchange: 'beforeCheckboxCheckChange',
                                                        checkChange: 'checkboxCheckChange'
                                                    }
                                                }

                                            ]
                                        }

                                     ],
                                     features: [groupingFeature]
                                 }

                             ]
                         },

                         {
                             xtype: 'container',
                             id: 'checklistCompletedSection',
                             margin: '20 5 0 5',
                             baseCls: 'oxGridContentContainer',

                             layout: {
                                 type: 'hbox',
                                 align: 'middle',
                                 pack: 'center'
                             },

                             items: [

                             {
                                 xtype: 'label',
                                 text: Locale.getMsg('checklistComplete') + ': ',
                                 width: 200,

                                 style: {
                                     'text-align': 'end'
                                 },

                                 padding: '0 5 0 8'
                             },

                             {
                                 xtype: 'checkbox',
                                 baseCls: 'oxCheckBox',
                                 id: 'checklistCompletedCb',
                                 checkedCls: 'checked',
                                 height: 35,

                                 listeners: {
                                     change: 'completeChecklist',
                                     scope: myController
                                 }
                             }],

                             padding: '0 0 15 0'
                         }

                             ]
                         },

                        {
                            xtype: 'component',
                            width: 10
                        }

                     ]
                 }

            ];
            this.add(items);
            var generalDataGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid', {});
            this._checklistRow = generalDataGrid.addRow(Locale.getMsg('inspectionLot'), 'label', true);
            this._funcLocRow = generalDataGrid.addRow(Locale.getMsg('checklist_FuncLoc'), 'label', true);
            this._equiRow = generalDataGrid.addRow(Locale.getMsg('equipment'), 'label', true);
            var inGridPanel = this.queryById('inGridPanel');
            var info = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();
            this.queryById('checklistMainGrid').add(generalDataGrid);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseChecklistPage', ex);
        }
    },

    //protected
    //@override
    getPageTitle: function () {
        var retval = '';
        try {
            var title = Locale.getMsg('inspectionLot');
            var prueflos = '';
            var qplos = this.getViewModel().get('qplos');

            if (qplos)
                prueflos = " " + qplos.get('prueflos');
            else
                title = Locale.getMsg('newInspLot');

            retval = title + prueflos;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseChecklistPage', ex);
        }

        return retval;
    },

    updatePageContent: function () {
        try {
            this.fillMainDataSection();
            this.refreshChecklistGridPanel();
            this.manageCompletionCheckbox();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseChecklistPage', ex);
        }
    },

    manageCompletionCheckbox: function () {
        try {
            var qplos = this.getViewModel().get('qplos');
            var completed = qplos ? qplos.isCompleted() : false;

            var checkbox = Ext.getCmp('checklistCompletedCb');
            checkbox.setRawValue(completed);
            checkbox.setDisabled(completed);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageCompletionCheckbox of BaseChecklistPage', ex);
        }
    },

    refreshChecklistGridPanel: function () {
        try {
            var viewChars = this.getViewModel().get('viewChars');
            var checklistGridPanel = Ext.getCmp('inGridPanel');
            checklistGridPanel.setStore(viewChars);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshChecklistGridPanel of BaseChecklistPage', ex);
        }
    },

    fillMainDataSection: function () {
        try {
            var myModel = this.getViewModel();
            var qplos = myModel.get('qplos');
            var prueflos = '(' + Locale.getMsg('new') + ')';

            if (qplos)
                prueflos = qplos.get('prueflos');

            this._checklistRow.setContentString(prueflos);
            this._checklistRow.show();

            var techObj = myModel.get('techObj');
            var techObjClassName = Ext.getClassName(techObj);

            if (techObjClassName === 'AssetManagement.customer.model.bo.Equipment') {
                this._funcLocRow.hide();
                this._equiRow.show();
                var equiText = techObj.get('eqktx');
                var content = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(techObj.get('equnr')) + ' ' + equiText;
                this._equiRow.setContentString(content);
            } else if (techObjClassName === 'AssetManagement.customer.model.bo.FuncLoc') {
                this._funcLocRow.show();
                this._equiRow.hide();
                var techObjText = techObj.get('pltxt');
                var techObjId = techObj.getDisplayIdentification();
                var content = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(techObjId) + ' ' + techObjText;

                this._funcLocRow.setContentString(content);
            } else {
                this._funcLocRow.hide();
                this._equiRow.hide();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseChecklistPage', ex);
        }
    }
});