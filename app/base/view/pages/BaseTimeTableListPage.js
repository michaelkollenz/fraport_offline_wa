Ext.define('AssetManagement.base.view.pages.BaseTimeTableListPage', {
    extend: 'Ext.panel.Panel',


    requires: [
        'AssetManagement.customer.model.pagemodel.TimeTableListPageViewModel',
        'AssetManagement.customer.controller.pages.TimeTableListPageController',
        'Ext.form.Label',
        'Ext.form.field.Date',
        'Ext.Img',
        'AssetManagement.customer.view.OxGridPanel',
        'Ext.grid.column.Column',
        'Ext.grid.View'
    ],

    inheritableStatics: {
	   	_instance: null,

	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	               this._instance = Ext.create('AssetManagement.customer.view.pages.TimeTableListPage');
	            }
	        } catch(ex) {
        	    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseTimeTableListPage', ex);
            }
	        
	        return this._instance;
	   	}
    },
    
    viewModel: {
        type: 'TimeTableListPageModel'
    },
    
    controller: 'TimeTableListPageController',
   
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
		    var items = [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    text: Locale.getMsg('timeTable')
		                },
		                {
		                    xtype: 'component',
		                    height: 20
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'label',
		                            flex: 0.2,
		                            text: Locale.getMsg('startDate')
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 0.1
		                        },
		                        {
		                            xtype: 'label',
		                            flex: 0.2,
		                            text: Locale.getMsg('endDate')
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 5
		                },
		                {
		                    xtype: 'container',
		                    height: '',
		                    layout: {
		                        type: 'hbox',
		                        align: 'bottom'
		                    },
		                    items: [
		                        {
		                            xtype: 'datefield',
		                            flex: 0.2,
		                            id: 'timeTableStartDateField',
		                            maxHeight: 30
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 0.1
		                        },
		                        {
		                            xtype: 'datefield',
		                            flex: 0.2,
		                            id: 'timeTableEndDateField',
		                            maxHeight: 30
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            height: 30,
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'component',
		                                    width: 20
		                                },
		                                {
		                                    xtype: 'button',
		                                    html: '<div><img width="73%" src="resources/icons/search.png"></img></div>',
		                                    padding: 0,
		                                    id: 'searchButtonTimeTableList',
		                                    width: 50
		                                },
		                                {
		                                    xtype: 'component',
		                                    flex: 1
		                                }
		                            ]
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    height: 30
		                },
		                {
		                    xtype: 'oxgridpanel',
		                    flex: 1,
		                    id: 'timeTableGridPanel',
		                    columns: [
		                        {
		                            xtype: 'gridcolumn',
		                            dataIndex: 'string',
		                            text: '',
		                            flex: 1
		                        }
		                    ] 
		                   
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
		    ];
		    
		    
		    this.add(items);
		    this.searchButtonVisibility();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseTimeTableListPage', ex);
        }
	},
	
	getPageTitle: function() {
		var retval = '';
		
		try {
			retval = Locale.getMsg('timeTable');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseTimeTableListPage', ex);
		}
		
		return retval;
	},
	
    searchButtonVisibility: function(){       
         try {
        	 var button = Ext.getCmp('searchButtonTimeTableList');	
         
        	 button.setVisible(false);
         } catch(ex) {
        	 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchButtonVisibility of BaseTimeTableListPage', ex);
         }
	}
});