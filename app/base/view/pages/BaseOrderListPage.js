 Ext.define('AssetManagement.base.view.pages.BaseOrderListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.OrderListPageViewModel',
        'AssetManagement.customer.controller.pages.OrderListPageController',
        'AssetManagement.customer.model.bo.Address',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date'
    ],

    inheritableStatics: {
		_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
            	   this._instance = Ext.create('AssetManagement.customer.view.pages.OrderListPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseOrderListPage', ex);
		    }
            
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.OrderListPageRendererMixin'
    },

    controller: 'OrderListPageController',
    
    viewModel: {
        type: 'OrderListPageModel'
    },

    layout:{
        type: 'hbox',
	    align: 'stretch'
	},
	
	//protected
	//@override
    buildUserInterface: function() {
		try {
			var myController = this.getController();
			
			var items = [
			     {
			    	 xtype: 'container',
			         flex: 1,
			         frame: true,
			         layout: {
			            type: 'hbox',
			            align: 'stretch'
			         },
			         items: [
			            {
			                xtype: 'component',
			                maxWidth: 10,
	                        minWidth: 10
			            },
			            {
			                xtype: 'container',
			                flex: 5,
			                layout: {
			                    type: 'vbox',
			                    align: 'stretch'
			                },
			                items: [
			                    {
			                        xtype: 'component',
			                        height: 10
			                    },
			                    {
		                            xtype: 'container',
		                            height: 30,
			                        id: 'searchOrderContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                               
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldOrder',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,
			                                listeners: {
			                                	change: {
			                                        fn: 'onOrderListPageSearchFieldChange'
			                                    }
		                                	}
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
		                                    id: 'orderListPageFilterCriteriaCombobox',
		                                    editable: false,
		                                    store: [Locale.getMsg('orderNum'), Locale.getMsg('shorttext'), Locale.getMsg('notifNumber'),
		                                            Locale.getMsg('equipment'), Locale.getMsg('funcLoc'), Locale.getMsg('mainWorkctr'),
		                                            Locale.getMsg('sortField'), Locale.getMsg('date'), Locale.getMsg('zipCode'), Locale.getMsg('city')],
		                                    value: Locale.getMsg('orderNum'),
			                                listeners: {
	                                    		select: 'onOrderListPageSearchFieldChange'
	                                    	}
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
	                            {
			                        xtype: 'container',
			                        id: 'orderGridContainer',
			                        flex: 1,
			                        layout: {

	                            		type: 'hbox',
	                            		align: 'stretch'
	                            	},
			                        items: [
			                           this.getNewOrderGridPanel()
			                        ]
			                    },
			                    {
		                            xtype: 'component',
		                            height: 10
		                        }
			                ]
			            },
			            {
			                xtype: 'component',
			                width: 10
			            }
			        ]
			    }
			];
			
			this.add(items);
			this.searchFieldVisibility();
		} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseOrderListPage', ex);
    	}
    },
    
    //private
    renewOrderGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('orderGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewOrderGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewOrderGridPanel of BaseOrderListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewOrderGridPanel: function() {
    	var retval = null;
        try {
            var me = this;
    		var myController = this.getController();
    	
    		retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
    			id: 'orderGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            parentController: myController,
                owner: me,
	            loadingText: Locale.getMsg('loadingOrders'),
	            emptyText: Locale.getMsg('noOrders'),
	            flex: 1,
	            scrollable: 'vertical',
	            /*columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var imageSrc = 'resources/icons/order.png';

	                        if(record && record.get('localStatus')) {
	                            imageSrc = record.get('localStatus').get('statusIconPath');
	                        }

	                        return '<img src="' + imageSrc + '"/>';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,
	                    enableColumnHide: false,
	                    align: 'center'		                                
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                     	var aufnr = AssetManagement.customer.utils.StringUtils.trimStart(record.get('aufnr'), '0');
	                        var ktext = record.get('ktext');
	                        return aufnr +'<p>'+ktext+'</p>' +'<br /><br />';
	                    },
	                    enableColumnHide: false,
	                    dataIndex: 'aufnr',
	                    text: Locale.getMsg('order')		                                
	                },
	                {
	                    xtype: 'datecolumn',
	                    enableColumnHide: false,
	                    format: 'd.m.Y H:M',
	                    dataIndex: 'gstrp',
	                    maxWidth: 125,
	                    minWidth: 125,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                    	var operation = AssetManagement.customer.manager.OperationManager.getEarliestOperation(record);
	                    	var time = null;
	                        var row1 = '';
	                        
	                        if(operation) {
								if(operation.get('ntan') !== null)
									time = operation.get('ntan');
								else 
									time = operation.get('fsav');
							}
								
	                        return AssetManagement.customer.utils.DateTimeUtils.getFullTimeWithTimeZoneForDisplay(time, record.getRelevantTimeZone());
	                 	},
	                    text: Locale.getMsg('date')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                    	var row1 = ' ';
	                        var row2 = ' ';
	                        var row3 = ' ';
	                		
	                		var useEqui = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('equnr'));
	                		
	                		if(useEqui === true) {
	                			var equi = record.get('equipment');
	                			
	                			if(equi) {
	                				row1 = equi.get('eqktx');
	                				row2 = equi.get('submt');
	                				
	                				/*
	                				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(row2)) {
	                					row2 = AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0');
	                				}
	                				
	                				row3 = AssetManagement.customer.utils.StringUtils.concatenate([ equi.get('baujj'), equi.get('baumm') ], '/', false, false, '-');
	                			} else {
	                				row1 = AssetManagement.customer.utils.StringUtils.trimStart(record.get('equnr'), '0');
	                			}
	                		} else {
	                			var funcLoc = record.get('funcLoc');
	                			
	                			if(funcLoc) {
	                				row1 = funcLoc.get('pltxt');
	                				row2 = funcLoc.get('tplnr');
	                				
									if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(row2)) {
										row2 = funcLoc.get('tplnr');
									}
									
	                				row3 = AssetManagement.customer.utils.StringUtils.concatenate([ funcLoc.get('baujj'), funcLoc.get('baumm') ], '/', false, false, '-');
	                			} else {
	                				row1 = record.get('tplnr');
	                			}
	                		}
	
	                        return row1 +'<p>'+row2+'</p><p>'+row3+'</p>';
	                 	},
	                    enableColumnHide: false,
	                    dataIndex: 'tplnr',
	                    text: Locale.getMsg('techObject')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		var name = '';
	                        var street = '';
	                        var postalCode = '';
	                           
	                        var address = record.get('address');
							if(address !== null && address !== undefined) {
								name = address.getName();
								street = AssetManagement.customer.utils.StringUtils.concatenate([ address.getStreet(), address.getHouseNo() ], null, true);
								postalCode = AssetManagement.customer.utils.StringUtils.concatenate([ address.getPostalCode(), address.getCity() ], null, true);
							} else {
							  	name = '';
	                            street = '';
	                            postalCode = '';
							}
							    
	                        return name +'<p>'+street+'</p><p>'+postalCode+'</p>';
	                 	},
	                    enableColumnHide: false,
	                    dataIndex: 'postalCode',
	                    text: Locale.getMsg('address')
	                },
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
	                		if(!record.get('address')) {
	                			metaData.style = 'display: none;';
	                		} else {
	                			metaData.style = '';
	                		}
	                	},
	                	text: Locale.getMsg('maps'),
	                	maxWidth: 80,
	                    width: 80,
	                    items: [{
	                        icon: 'resources/icons/maps_small.png',
	                        tooltip: 'Google Maps',
	                        iconCls: 'oxGridLineActionButton',
	                        handler: function (grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
	                            event.stopEvent();
	                            this.getController().navigateToAddress(record);
	                        },
	                        scope: this
	                    }],
	                    enableColumnHide: false,
	                    align: 'center'
	                }
	            ], */
	           
	            listeners: {
	             	cellclick: myController.onOrderSelected,
	        		cellcontextmenu: myController.onCellContextMenu,
	        		scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewOrderGridPanel of BaseOrderListPage', ex);
    	}
    	
    	return retval;
	},
	
	//protected
	//@override 
    getPageTitle: function() {
    	var retval = '';
    
    	try {
	    	var title = Locale.getMsg('orders');
	    	

	    	var orders = this.getViewModel().get('orderStore');
	    	
	    	if(orders) {
	    		title += " (" + orders.getCount() + ")";
	    	}
	    	
	    	retval = title;
	    } catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseOrderListPage', ex);
    	}

    	return retval;
	},
	
	//protected
	//@override    
    updatePageContent: function() {
    	try {
    		var orderStore = this.getViewModel().get('orderStore');
    		
    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    		    this.renewOrderGridPanel();

    		if (this.getViewModel().get('constrainedOrders')) {
    		    orderStore = this.getViewModel().get('constrainedOrders');
    		}
    		
    		var orderGridPanel = Ext.getCmp('orderGridPanel');
    		
    		orderGridPanel.setStore(orderStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseOrderListPage', ex);
    	}
	},

	//public
	//@override  
	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('orderGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseOrderListPage', ex);
    	}
	},
	
	//private
	searchFieldVisibility: function() { 
       try {
	       var field = Ext.getCmp('searchOrderContainer');	
	       var para = true;
	      
	       if (para === false)
	    	   field.setVisible(false);
           
       } catch(ex) {
    	  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseOrderListPage', ex);
       }
	},
	
	//public
	getSearchValue: function() {
		try {
			var orderToSearch = Ext.create('AssetManagement.customer.model.bo.Order', {});
			var currentFilterCriterion = Ext.getCmp('orderListPageFilterCriteriaCombobox').getValue();	

			if(currentFilterCriterion === Locale.getMsg('orderNum'))
				orderToSearch.set('aufnr', Ext.getCmp('searchFieldOrder').getValue());
			else if(currentFilterCriterion === Locale.getMsg('shorttext'))
				orderToSearch.set('ktext', Ext.getCmp('searchFieldOrder').getValue());
			else if(currentFilterCriterion === Locale.getMsg('notifNumber'))
				orderToSearch.set('qmnum', Ext.getCmp('searchFieldOrder').getValue());
			else if(currentFilterCriterion === Locale.getMsg('equipment')) {
				var equipment = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
				equipment.set('eqktx', Ext.getCmp('searchFieldOrder').getValue());
				orderToSearch.set('equipment', equipment);
				orderToSearch.set('equnr', Ext.getCmp('searchFieldOrder').getValue())
			} else if(currentFilterCriterion === Locale.getMsg('funcLoc')) {
				var funcLoc = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {});
				funcLoc.set('pltxt', Ext.getCmp('searchFieldOrder').getValue());
				orderToSearch.set('funcLoc', funcLoc);
				orderToSearch.set('tplnr', Ext.getCmp('searchFieldOrder').getValue())
			} else if(currentFilterCriterion === Locale.getMsg('mainWorkctr'))
				orderToSearch.set('vaplz', Ext.getCmp('searchFieldOrder').getValue());
			else if(currentFilterCriterion === Locale.getMsg('sortField')) {
				var equi = Ext.create('AssetManagement.customer.model.bo.Equipment', {});
				equi.set('eqfnr', Ext.getCmp('searchFieldOrder').getValue());
				orderToSearch.set('equipment', equi);
			}
				
				
			// TODO: Kunde (customer)
				
			// Franke Client
			// Datum
			else if(currentFilterCriterion === Locale.getMsg('date')) {
                var operationStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.Operation',
                    autoLoad: false
                });

                var operation = Ext.create('AssetManagement.customer.model.bo.Operation', {});
                operation.set('ntanSearch', Ext.getCmp('searchFieldOrder').getValue());

                operationStore.add(operation);
				orderToSearch.set('operations', operationStore);
			}
			// PLZ
			else if(currentFilterCriterion === Locale.getMsg('zipCode')) {				
				orderToSearch.set('address', Ext.create('AssetManagement.customer.model.bo.Address', {postCode1: Ext.getCmp('searchFieldOrder').getValue()}))
			}
			// Ort
			else if(currentFilterCriterion === Locale.getMsg('city')) {				
				orderToSearch.set('address', Ext.create('AssetManagement.customer.model.bo.Address', {city1: Ext.getCmp('searchFieldOrder').getValue()}))
			}
				
			this.getViewModel().set('searchOrder', orderToSearch);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseOrderListPage', ex);
		}
	},


     //public
	getFilterValue: function () {
	    var retval = '';

	    try {
	        retval = Ext.getCmp('searchFieldOrder').getValue();

	        if (!retval)
	            retval = '';
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseOrderListPage', ex);
	    }

	    return retval;
	},
     //public
	setFilterValue: function (value) {
	    try {
	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
	            value = '';

	        retval = Ext.getCmp('searchFieldOrder').setValue(value);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseOrderListPage', ex);
	    }
	}
});