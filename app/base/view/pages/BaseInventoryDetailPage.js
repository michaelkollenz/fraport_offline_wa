Ext.define('AssetManagement.base.view.pages.BaseInventoryDetailPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.InventoryDetailPageViewModel',
        'AssetManagement.customer.controller.pages.InventoryDetailPageController',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.grid.plugin.CellEditing',
        'Ext.grid.column.Number',
        'Ext.grid.column.Check',
        'Ext.grid.column.Action'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.InventoryDetailPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseInventoryDetailPage', ex);
            }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.InventoryDetailPageRendererMixin',
        itemsMixin: 'AssetManagement.customer.view.mixins.InventoryDetailPageItemsMixin'
    },

    _inventoryDocumentRow: null,
    _plantRow: null,
    _storLocRow: null,

    viewModel: {
        type: 'InventoryDetailPageModel'
    },

    controller: 'InventoryDetailPageController',

    buildUserInterface: function () {
        try {
            var myController = this.getController();
            var me = this;

            this.editing = Ext.create('Ext.grid.plugin.CellEditing');
            var items = [
               {
                   xtype: 'container',
                   flex: 1,
                   layout: {
                       type: 'hbox',
                       align: 'stretch'
                   },
                   items: [
                       {
                           xtype: 'container',
                           margin: '0 10 0 10',
                           flex: 1,
                           layout: {
                               type: 'vbox',
                               align: 'stretch'
                           },
                           items: [
                               {
                                   xtype: 'container',
                                   id: 'inventoryUpperContent',
                                   margin: '0 0 0 10',
                                   layout: {
                                       type: 'vbox',
                                       align: 'stretch'
                                   },
                                   items: [
                                        {
                                            xtype: 'label',
                                            margin: '0 0 0 5',
                                            cls: 'oxHeaderLabel'
                                        },
                                        {
                                            xtype: 'container',
                                            margin: '10 0 0 0',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            flex: 1,
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    flex: 1,
                                                    id: 'inventoryDataGrid'
                                                }
                                            ]
                                        }
                                   ]
                               },
                           {
                               xtype: 'container',
                               id: 'inventoryItemGridContainer',
                               margin: '10 0 10 0',
                               flex: 1,
                               layout: {
                                   type: 'hbox',
                                   align: 'stretch'
                               }/*,
                               items:
                                   [
                                       this.getInventoryItemsGridPanel()
                                   ]*/
                           }]
                       }
                   ]
               }
            ];


            this.add(items);

            var generalDataGrid = Ext.create('AssetManagement.customer.view.utils.OxGrid', {});
            this._inventoryDocumentRow = generalDataGrid.addRow(Locale.getMsg('inventoryDocument'), 'label', true);
            this._plantRow = generalDataGrid.addRow(Locale.getMsg('plant'), 'label', true);
            this._storLocRow = generalDataGrid.addRow(Locale.getMsg('storLoc'), 'label', false);


            Ext.getCmp('inventoryDataGrid').add(generalDataGrid);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseInventoryDetailPage', ex);
        }
    },

    getPageTitle: function () {
        var retval = '';

        try {
            var title = Locale.getMsg('inventory');

            var inventory = this.getViewModel().get('inventory') ? this.getViewModel().get('inventory').get('items') : null;

            if (inventory) {
                title += "(" + inventory.getCount() + ")";
            }
            retval = title;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseInventoryDetailPage', ex);
        }

        return retval;
    },

    //protected
    //@Override
    updatePageContent: function () {
        try {
            //do not remove - causes issue if we are not generating each and every time the grid

            this.refreshGridPanel();

            this.clearAllFields();
            this.fillMainDataSection();
            this.refreshInventoryItemPanel();
            this.searchFieldVisibility();


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseInventoryDetailPage', ex);
        }
    },

    refreshGridPanel: function() {
        try {
            var gridContainer = Ext.getCmp('inventoryItemGridContainer');

            // var invItemGrid

            gridContainer.removeAll();
            gridContainer.add(this.getInventoryItemsGridPanel())
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseInventoryDetailPage', ex);
        }
    },


    getInventoryItemsGridPanel: function() {
        var retval = null;
        try {
            var me = this;
            var myController = this.getController();

            retval = Ext.create('AssetManagement.customer.view.OxGridPanel', {
                id: 'inventoryItemGridPanel',
                hideContextMenuColumn: true,
                useLoadingIndicator: true,
                loadingText: Locale.getMsg('loadingInventory'),
                emptyText: Locale.getMsg('noInventory'),
                flex: 1,
                scrollable: 'vertical',
                columns:
                [{
                    xtype: 'actioncolumn',
                    dataIndex: 'localStatus',
                    text: Locale.getMsg('status'),
                    width: 60,
                    resizable: false,
                    listeners: {
                        click: 'onSelectInventoryItem'
                    },
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        var retval = '';
                        var imageSrc = '';

                        imageSrc = 'resources/icons/out_of_time.png';
                        retval = '<img src="' + imageSrc + '"   style="height:30px;width:30px;margin-left:auto;margin-right:auto;display:block;" />';

                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('serpr'))) {
                            if (record.get('seriallist').getCount() === parseInt(record.get('entryQnt')) && parseInt(record.get('entryQnt')) > 0) {
                                imageSrc = 'resources/icons/completed.png';
                                retval = '<img src="' + imageSrc + '"   style="height:30px;width:30px;margin-left:auto;margin-right:auto;display:block;" />';
                            } else if (record.get('zerocount') === true) {
                                imageSrc = 'resources/icons/completed.png';
                                retval = '<img src="' + imageSrc + '"   style="height:30px;width:30px;margin-left:auto;margin-right:auto;display:block;" />';
                            }
                            else {
                                imageSrc = 'resources/icons/barcode_small.png';
                                retval = '<img src="' + imageSrc + '"   style="height:30px;width:30px;margin-left:auto;margin-right:auto;display:block;" />';
                            }
                        } else {
                            if (record.get('entryQnt') > 0 || record.get('zerocount') === true) {
                                imageSrc = 'resources/icons/completed.png';
                                retval = '<img src="' + imageSrc + '"   style="height:30px;width:30px;margin-left:auto;margin-right:auto;display:block;" />';
                            }
                        }
                        return retval;
                    }
                },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'item',
                        text: Locale.getMsg('position'),
                        width: 80,
                        resizable: false,
                        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                            var position = record.get('item');
                            return position;
                        },
                        items: [
                            {
                                xtype: 'oxtextfield',
                                id: 'positionField',
                                margin: '0 5 0 5',
                                width: 50,
                                listeners: {
                                    change: 'onInventoryDetailPageSearchFieldChange'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'material',
                        text: Locale.getMsg('material'),
                        flex: 1,
                        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                            var retval = '';
                            var material = AssetManagement.customer.manager.MaterialManager.trimMaterialNumber(record.get('material'));
                            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('maktx'))) {
                                retval = material + " " + record.get('maktx');
                            } else {
                                retval = material;
                            }
                            return retval;
                        },
                        items: [
                            {
                                xtype: 'oxtextfield',
                                id: 'materialField',
                                margin: '0 5 0 5',
                                listeners: {
                                    change: 'onInventoryDetailPageSearchFieldChange'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'widgetcolumn',
                        dataIndex: 'entryQnt',
                        text: Locale.getMsg('quantity'),
                        sortable: true,
                        width: 100,
                        resizable: false,
                        maxWidth: 120,
                        minWidth: 120,
                        items: [
                            {
                                xtype: 'numberfield',
                                minValue: 0,
                                margin: '0 5 0 5',
                                width: 70,
                                resizable: false,
                                id: 'quantityField',
                                listeners: {
                                    change: 'onInventoryDetailPageSearchFieldChange'
                                }
                            }
                        ],
                        onWidgetAttach: function(column, widget, record) {
                            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('serpr'))) {
                                widget.setEditable(false);
                                //widget.disable();
                            } else {
                                widget.setEditable(true);
                            }
                        },
                        widget: {
                            xtype: 'oxnumberfield',
                            //width: '95%',
                            width: 80,
                            fieldLabel: '',
                            autofocus: false,
                            hideTrigger: true,
                            keyNavEnabled: false,
                            mouseWheelEnabled: false,
                            enableKeyEvents: true,
                            //value: parseInt(record.get('entryQnt')) === 0 ? '' : record.get('entryQnt'),
                            editable: true,
                            listeners: {
                                keyup: myController.onTextFieldValueChange,
                                paste: myController.onTextFieldValueChange,
                                scope: myController
                            }

                        }
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'entryUom',
                        text: Locale.getMsg('unit'),
                        width: 80,
                        resizable: false,
                        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                            var unit = AssetManagement.customer.utils.StringUtils.trimStart(record.get('entryUom'), '0');
                            return unit;

                        },
                        items: [
                            {
                                xtype: 'oxtextfield',
                                id: 'unitField',
                                margin: '0 5 0 5',
                                width: 40,
                                resizable: false,
                                listeners: {
                                    change: 'onInventoryDetailPageSearchFieldChange'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'batch',
                        text: Locale.getMsg('batch'),
                        width: 120,
                        resizable: false,
                        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                            var batch = AssetManagement.customer.utils.StringUtils.trimStart(record.get('batch'), '0');
                            return batch;

                        },
                        items: [
                            {
                                xtype: 'oxtextfield',
                                id: 'batchField',
                                listeners: {
                                    change: {
                                        fn: myController.onInventoryDetailPageSearchFieldChange
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'checkcolumn',
                        text: Locale.getMsg('zeroCount'),
                        dataIndex: 'zerocount',
                        tdCls: 'oxCheckBoxGridColumnItem',
                        width: 80,
                        resizable: false,
                        listeners: {
                            checkchange: {
                                fn: myController.onZeroCountFieldChange,
                                scope: myController
                            },
                            beforecheckchange: function (checkbox, rowIndex, checked, eOpts) {
                                var record = Ext.getCmp('inventoryItemGridPanel').getStore().getAt(rowIndex);
                                if (record) {
                                    if (record.get('entryQnt') > 0) {
                                        record.set('entryQnt', '0.000');
                                    }
                                    var serialList = record.get('seriallist');
                                    if (serialList && serialList.getCount() > 0) {
                                        serialList.removeAll();
                                    }
                                }
                            }
                        }
                    }]
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewOrderGridPanel of BaseOrderListPage', ex);
        }

        return retval;
    },

    refreshInventoryItemPanel: function () {
        try {
            var inventoryItemGridPanel = Ext.getCmp('inventoryItemGridPanel');

            var invItemsList = this.getViewModel().get('listItems');

            inventoryItemGridPanel.setStore(invItemsList);

            invItemsList.sort('item', 'ASC');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshInventoryItemPanel of BaseInventoryDetailPage', ex);
        }

    },

    clearAllFields: function () {
        try{
            Ext.getCmp('positionField').setValue('');
            Ext.getCmp('materialField').setValue('');
            Ext.getCmp('quantityField').setValue();
            Ext.getCmp('unitField').setValue('');
        }catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearAllFields of BaseInventoryDetailPage', ex);
        }
    },

    fillMainDataSection: function () {
        try {

            var inventory = this.getViewModel().get('inventory');
            if(inventory){
                var physinventory = inventory.get('physinventory');
                var plant = inventory.get('plant');
                var storLoc = inventory.get('stgeLoc');

                physinventory ? this._inventoryDocumentRow.setContentString(AssetManagement.customer.utils.StringUtils.trimStart(physinventory, '0')) : null;

                if (physinventory) {
                    this._inventoryDocumentRow.show();
                    this._inventoryDocumentRow.setContentString(physinventory);
                }

                if (plant) {
                    this._plantRow.show();
                    this._plantRow.setContentString(plant);
                } else {
                    this._plantRow.hide();
                }

                if (storLoc) {
                    this._storLocRow.show();
                    this._storLocRow.setContentString(storLoc);
                } else {
                    this._storLocRow.hide();
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillMainDataSection of BaseInventoryDetailPage', ex);
        }
    },

    //public
    getSearchValue: function () {
        try {
            var itemToSearch = Ext.create('AssetManagement.customer.model.bo.InventoryItem', {});
            var currentFilterCriterionPosition = Ext.getCmp('positionField').getValue();
            var currentFilterCriterionMaterial = Ext.getCmp('materialField').getValue();
            var currentFilterCriterionQuantity = Ext.getCmp('quantityField').getValue();
            var currentFilterCriterionUnit = Ext.getCmp('unitField').getValue();

            if (currentFilterCriterionPosition) {
                itemToSearch.set('item', currentFilterCriterionPosition);
            } else if (currentFilterCriterionMaterial) {
                itemToSearch.set('material', currentFilterCriterionMaterial);
            } else if (currentFilterCriterionQuantity) {
                itemToSearch.set('entryQnt', currentFilterCriterionQuantity);
            } else if (currentFilterCriterionUnit) {
                itemToSearch.set('entryUom', currentFilterCriterionUnit);
            }

            this.getViewModel().set('searchItem', itemToSearch);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseInventoryDetailPage', ex);
        }
    },
    //private
    searchFieldVisibility: function () {
        try {
            var field = Ext.getCmp('inventoryItemGridContainer');
            var para = true;

            if (para === false)
                field.setVisible(false);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseInventoryDetailPage', ex);
        }
    },

    resetViewState: function () {
        this.callParent();

        try {
            Ext.getCmp('inventoryItemGridPanel').reset();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseInventoryDetailPage', ex);
        }
    },

    getCurrentInputValues: function () {
        var retval = null;

        try {
            retval = Ext.getCmp('inventoryItemGridPanel').getStore();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseInventoryDetailPage', ex);
            retval = null;
        }
        return retval;
    }
});
