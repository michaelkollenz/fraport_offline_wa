Ext.define('AssetManagement.base.view.pages.BaseObjectStructurePage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',

    layout:'fit',
    requires: [
        'AssetManagement.customer.model.pagemodel.ObjectStructurePageViewModel',
        'AssetManagement.customer.controller.pages.ObjectStructurePageController',
        'Ext.tree.View',
        'Ext.data.TreeModel'
    ],
    
    treePanel: null,
    bodyCls: 'oxTreePanelAndroid',
    inheritableStatics: {
		_instance: null,
		_instanceCounter: 0, 
		
	    getInstance: function() {
	        if(this._instance == null)
	        {
	            this._instance = Ext.create('AssetManagement.customer.view.pages.ObjectStructurePage');
	        }
	        return this._instance;
	   }
   },
   
   viewModel: {
        type: 'ObjectStructurePageViewModel'
   },
   
   controller: 'ObjectStructurePageController',
   
   buildUserInterface: function() {
       try {
            var myController = this.getController();
            var items = [
                {
                    xtype: 'container',
                    layout: 'border',
                    items: [
                        {
                            xtype: 'treepanel',
                            layout: 'fit',
                            region: 'center',
                            id: 'objStructureTreePanel',
                            hideHeaders: true,
                            rootVisible: true,
                            scrollable: true,
                            disableSelection: true,
                            columns: [
                                {
                                    xtype: 'treecolumn',
                                    dataIndex: 'text',
                                    text: 'Nodes',
                                    flex: 2
                                },
                                {
                                    dataIndex: 'matnr',
                                    text: 'Nodes',
                                    flex: 1
                                },
                                {
                                    dataIndex: 'sernr',
                                    text: 'Nodes',
                                    flex: 1
                                }
                            ],
                            listeners: {
                                cellclick: myController.onRecordClicked,
                                scope: myController
                            }
                        }
                    ]
                }
            ];
            this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseObjectStructurePage', ex);
		}
    },
    
   	//protected
	//@override
    updatePageContent: function() {
 		try {
 			var myModel = this.getViewModel();
 			var objectStructure = myModel.get('objStructureStore');
 			var treePanel = Ext.getCmp('objStructureTreePanel');
 			treePanel.getRootNode().removeAll();
 			treePanel.setStore(objectStructure);

 			if(!objectStructure)
 				AssetManagement.customer.helper.OxLogger.logMessage('Store to fill treePanel is empty');
 		} catch(ex) {
 			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseObjectStructurePage', ex);
 		}
    },	
     
	getPageTitle: function() {
    	var retval = '';
	
		try {
			retval = Locale.getMsg('objectStructure');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseObjectStructurePage', ex);
		}
		
		return retval;
	}
});