Ext.define('AssetManagement.base.view.pages.BaseMeasDocEditPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.MeasDocEditPageViewModel',
        'AssetManagement.customer.controller.pages.MeasDocEditPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.OxNumberField',
        'Ext.form.field.Date',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.grid.column.Date'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.MeasDocEditPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMeasDocEditPage', ex);
            }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.MeasDocEditPageRendererMixin'
    },

    viewModel: {
        type: 'MeasDocEditPageModel'
    },

    controller: 'MeasDocEditPageController',

    buildUserInterface: function () {
        try {
            var myController = this.getController();
            var me = this;
            var items = [
		        {
		            xtype: 'container',
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 30
		                        },
		                        {
		                            xtype: 'label',
		                            flex: 1,
		                            cls: 'oxHeaderLabel',
		                            id: 'createMeasDocLabel',
		                            text: Locale.getMsg('createNewMeasDoc')
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 20
		                        },
		                        {
		                            xtype: 'container',
		                            flex: 1,
		                            layout: {
		                                type: 'vbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    flex: 1,
		                                    id: 'measDocEditMeasPointTextField',
		                                    fieldLabel: Locale.getMsg('measPoint'),
		                                    labelWidth: 150,
		                                    readOnly: true
		                                },
		                                {
		                                    xtype: 'component',
		                                    flex: 1,
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'oxtextfield',
		                                    flex: 1,
		                                    id: 'measDocEditEquiTextField',
		                                    fieldLabel: Locale.getMsg('equipment'),
		                                    labelWidth: 150,
		                                    hidden: true,
		                                    readOnly: true
		                                },
		                                {
		                                    xtype: 'oxtextfield',
		                                    flex: 1,
		                                    id: 'measDocEditFuncLocTextField',
		                                    fieldLabel: Locale.getMsg('funcLoc'),
		                                    labelWidth: 150,
		                                    readOnly: true
		                                },
		                                {
		                                    xtype: 'component',
		                                    flex: 1,
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'oxtextfield',
		                                    flex: 1,
		                                    id: 'measDocEditPositionTextField',
		                                    fieldLabel: Locale.getMsg('position'),
		                                    labelWidth: 150,
		                                    readOnly: true
		                                },
		                                {
		                                    xtype: 'component',
		                                    flex: 1,
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'oxtextfield',
		                                    flex: 1,
		                                    id: 'measDocEditLastValueTextField',
		                                    fieldLabel: Locale.getMsg('lastMeasValue'),
		                                    labelWidth: 150,
		                                    readOnly: true
		                                },
		                                {
		                                    xtype: 'component',
		                                    flex: 1,
		                                    height: 5
		                                },
		                                {
		                                    xtype: 'container',
		                                    flex: 1,
		                                    layout: {
		                                        type: 'hbox',
		                                        align: 'stretch'
		                                    },
		                                    items: [
		                                        {
		                                            xtype: 'container',
		                                            flex: 1,
		                                            layout: {
		                                                type: 'vbox',
		                                                align: 'stretch'
		                                            },
		                                            items: [
                                                        {
                                                            xtype: 'container',
                                                            flex: 1,
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'stretch'
                                                            },

                                                            items: [
                                                                {
                                                                    xtype: 'datefield',
                                                                    flex: 0.5,
                                                                    format: 'd.m.Y',
                                                                    id: 'measDocEditDateField',
                                                                    maxLength: 10,
                                                                    labelWidth: 150,
                                                                    fieldLabel: Locale.getMsg('date')
                                                                },
                                                                {
                                                                    xtype: 'component',
                                                                    width: 10
                                                                },
                                                                {
                                                                    xtype: 'timefield',
                                                                    labelStyle: 'display: none;',
                                                                    flex: 0.2,
                                                                    format: 'H:i',
                                                                    id: 'measDocEditTimeField',
                                                                    fieldLabel: Locale.getMsg('time'),
                                                                    maxLength: 5
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'component',
                                                            height: 10
                                                        },
		                                                {
		                                                    xtype: 'oxtextfield',
		                                                    flex: 1,
		                                                    id: 'measDocEditReadOffTextField',
		                                                    fieldLabel: Locale.getMsg('reader'),
		                                                    labelWidth: 150
		                                                }
		                                            ]
		                                        },
		                                        {
		                                            xtype: 'component',
		                                            flex: 0.1
		                                        },
		                                        {
		                                            xtype: 'container',
		                                            flex: 1,
		                                            items: [
                                                        {
                                                            xtype: 'container',
                                                            //width: 500,
                                                            flex: 1,
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                                {
		                                                            xtype: 'oxtextfield',
		                                                            id: 'measDocEditNoticeTextField',
                                                                    flex: 1,
		                                                            fieldLabel: Locale.getMsg('remark'),
		                                                            labelWidth: 120
		                                                        }
                                                            ]
                                                        },
		                                                
		                                                {
		                                                    xtype: 'component',
		                                                    height: 5
		                                                },
		                                                {
		                                                    xtype: 'container',
                                                            flex: 1,
		                                                    layout: {
		                                                        type: 'hbox',
		                                                        align: 'stretch'
		                                                    },
		                                                    items: [
		                                                        {
		                                                            xtype: 'oxnumberfield',
		                                                            flex: 1,
		                                                            id: 'measDocEditReadOffValueTextField',
		                                                            fieldLabel: Locale.getMsg('readValue'),
		                                                            labelWidth: 120
		                                                        },
		                                                        {
		                                                            xtype: 'component',
		                                                            width: 20
		                                                        },
		                                                        {
		                                                            xtype: 'oxtextfield',
		                                                            flex: 0.15,
		                                                            id: 'measDocEditReadOffUnit'
		                                                        }
		                                                    ]
		                                                }
		                                            ]
		                                        }
		                                    ]
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 40
		                        },
		                        {
		                            xtype: 'label',
		                            flex: 1,
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('list_MeasDocs')
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 10
		                        },
		                        {
		                            xtype: 'oxdynamicgridpanel',
		                            id: 'newMeasDocsGridPanel',
		                            disableSelection: true,
		                            parentController: myController,
                                    owner: me,
		                            /*columns: [
		                                {
		                                    xtype: 'actioncolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return '<img src="resources/icons/measdoc.png"/>';
		                                    },
		                                    minWidth: 80,
		                                    maxWidth: 80
		                                },
		                                {
		                                    xtype: 'datecolumn',
		                                    format: 'd.m.Y',
		                                    maxWidth: 125,
		                                    minWidth: 125,
		                                    dataIndex: 'idate',
		                                    text: Locale.getMsg('date'),
		                                    flex: 1
		                                },
                                        {
                                            xtype: 'datecolumn',
                                            format: 'H:i',
                                            maxWidth: 125,
                                            minWidth: 125,
                                            dataIndex: 'itime',
                                            text: Locale.getMsg('time'),
                                            flex: 1
                                        },
		                                {
		                                    xtype: 'gridcolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return record.get('readr');
		                                    },
		                                    dataIndex: 'readr',
		                                    text: Locale.getMsg('reader'),
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return record.get('recdc') + ' ' + record.get('unitr');
		                                    },
		                                    text: Locale.getMsg('orderComponents_Qty'),
		                                    dataIndex: 'quantity',
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return record.get('mdtext');
		                                    },
		                                    text: Locale.getMsg('remark'),
		                                    dataIndex: '',
		                                    flex: 1
		                                }
		                            ],*/
		                            listeners: {
		                                cellcontextmenu: myController.onCellContextMenu,
		                                scope: myController
		                            }

		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 30
		                        },
		                        {
		                            xtype: 'label',
		                            flex: 1,
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('historicalMeasDocs')
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 1,
		                            height: 10
		                        },
		                        {
		                            xtype: 'oxdynamicgridpanel',
		                            id: 'histMeasDocGrid',
		                            disableSelection: true,
		                            hideContextMenuColumn: true,
		                            parentController: myController,
                                    owner: me,
		                           /* columns: [
		                                {
		                                    xtype: 'actioncolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return '<img src="resources/icons/measdoc.png"/>';
		                                    },
		                                    id: 'measDocHistImgColumn',
		                                    maxWidth: 80,
		                                    minWidth: 80,
		                                    align: 'center'
		                                },
		                                {
		                                    xtype: 'datecolumn',
		                                    text: Locale.getMsg('date'),
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(record.get('idate'));
		                                    },
		                                    dataIndex: 'idate',
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return record.get('readr');
		                                    },
		                                    dataIndex: 'readr',
		                                    text: Locale.getMsg('reader'),
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return record.get('recdc') + ' ' + record.get('unitr');
		                                    },
		                                    dataIndex: 'quantity',
		                                    text: Locale.getMsg('orderComponents_Qty'),
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		                                        return record.get('mdtext');
		                                    },
		                                    dataIndex: 'mdtext',
		                                    text: Locale.getMsg('remark'),
		                                    flex: 1
		                                }
		                            ],*/
		                            listeners: {
		                                cellcontextmenu: myController.onCellContextMenu,
		                                scope: myController
		                            }
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    flex: 0.05
		                }
		            ]
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMeasDocEditPage', ex);
        }
    },

    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('createNewMeasDoc');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMeasDocEditPage', ex);
        }

        return retval;
    },

    //protected
    //@override
    updatePageContent: function () {
        try {
            //show hist MeasDocs
            var myModel = this.getViewModel();
            var histMeasDocs = myModel.get('measPoint').get('histMeasDocs');

            var histMeasDocGridPanel = Ext.getCmp('histMeasDocGrid');
            histMeasDocGridPanel.setStore(histMeasDocs);

            this.transferModelStateIntoView();

            this.refreshMeasDocPanel();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseMeasDocEditPage', ex);
        }
    },

    refreshMeasDocPanel: function () {
        try {
            var measDocGridPanel = Ext.getCmp('newMeasDocsGridPanel');
            measDocGridPanel.setStore(this.getViewModel().get('measPoint').get('measDocs'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshMeasDocPanel of BaseMeasDocEditPage', ex);
        }
    },

    //transfer current values from screen into model
    transferViewStateIntoModel: function () {
        try {
            var myModel = this.getViewModel();
            var measDoc = myModel.get('measDoc');

            var measPoint = myModel.get('measPoint');
            measDoc.set('point', measPoint.get('point'));
            measDoc.set('mdtext', Ext.getCmp('measDocEditNoticeTextField').getValue());
            measDoc.set('readr', Ext.getCmp('measDocEditReadOffTextField').getValue());
            measDoc.set('recdc', Ext.getCmp('measDocEditReadOffValueTextField').getValue());
            measDoc.set('unitr', Ext.getCmp('measDocEditReadOffUnit').getValue());
            measDoc.set('idate', Ext.getCmp('measDocEditDateField').getValue());
            measDoc.set('itime', Ext.getCmp('measDocEditTimeField').getValue());

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseMeasDocEditPage', ex);
        }
    },


    //TODO 
    transferModelStateIntoView: function () {
        try {
            var myModel = this.getViewModel();
            var measPoint = myModel.get('measPoint');
            var measDoc = myModel.get('measDoc');
            var editMode = myModel.get('isEditMode');

            //show label appending to edit state
            Ext.getCmp('createMeasDocLabel').setText(editMode === true ? Locale.getMsg('editMeasDoc') : Locale.getMsg('createNewMeasDoc'));

            //fill equi field
            if (measPoint.get('equipment') !== null && measPoint.get('equipment') !== undefined) {
                Ext.getCmp('measDocEditEquiTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(measPoint.get('equipment').get('equnr'), '0') + " " + measPoint.get('equipment').get('eqktx'));
                Ext.getCmp('measDocEditEquiTextField').setHidden(false);
            }
            else
                Ext.getCmp('measDocEditEquiTextField').setHidden(true);


            if (measPoint.get('funcLoc') !== null && measPoint.get('funcLoc') !== undefined) {
                Ext.getCmp('measDocEditFuncLocTextField').setValue(AssetManagement.customer.utils.StringUtils.trimStart(measPoint.get('funcLoc').getDisplayIdentification(), '0') + " " + measPoint.get('funcLoc').get('pltxt'));
                Ext.getCmp('measDocEditFuncLocTextField').setHidden(false);
            }
            else
                Ext.getCmp('measDocEditFuncLocTextField').setHidden(true);

            //set general data
            Ext.getCmp('measDocEditMeasPointTextField').setValue(measPoint.get('point') + ' ' + measPoint.get('pttxt')); //point
            Ext.getCmp('measDocEditPositionTextField').setValue(measPoint.get('psort')); //position
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measPoint.get('lastRec'))) {
                var lastR = measPoint.get('lastRec');
                Ext.getCmp('measDocEditLastValueTextField').setValue(Number(lastR.replace(/[^\d.,]+/, '').replace(/,/g, '.')) + ' ' + measPoint.get('lastUnit')); //lastRecValue
            } else {
                Ext.getCmp('measDocEditLastValueTextField').setValue('');
            }
            Ext.getCmp('measDocEditDateField').setValue(measDoc.get('idate')); //datefield 
            Ext.getCmp('measDocEditTimeField').setValue(measDoc.get('itime')); //datefield 
            Ext.getCmp('measDocEditNoticeTextField').setValue(measDoc.get('mdtext'));
            Ext.getCmp('measDocEditReadOffValueTextField').setValue(measDoc.get('recdc'));

            var ac = AssetManagement.customer.core.Core.getAppConfig();

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measDoc.get('readr')))
                Ext.getCmp('measDocEditReadOffTextField').setValue(ac.getUserId());
            else
                Ext.getCmp('measDocEditReadOffTextField').setValue(measDoc.get('readr')); //reader


            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measDoc.get('unitr')))
                Ext.getCmp('measDocEditReadOffUnit').setValue(measDoc.get('unitr'));
            else {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(measDoc.get('lastUnit')))
                    Ext.getCmp('measDocEditReadOffUnit').setValue(measPoint.get('lastUnit'));
                else
                    Ext.getCmp('measDocEditReadOffUnit').setValue(measPoint.get('mrngu'));
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferModelStateIntoView of BaseMeasDocEditPage', ex);
        }
    },

    getCurrentInputValues: function () {
        var retval = null;

        try {
            var measPoint = this.getViewModel().get('measPoint');

            retval = {
                point: measPoint.get('point'),
                mdtext: Ext.getCmp('measDocEditNoticeTextField').getValue(),
                readr: Ext.getCmp('measDocEditReadOffTextField').getValue(),
                recdc: Ext.getCmp('measDocEditReadOffValueTextField').getValue(),
                unitr: Ext.getCmp('measDocEditReadOffUnit').getValue(),
                idate: Ext.getCmp('measDocEditDateField').getValue(),
                itime: Ext.getCmp('measDocEditTimeField').getValue()
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentInputValues of BaseMeasDocEditPage', ex);
            retval = null;
        }

        return retval;
    }
});