Ext.define('AssetManagement.base.view.pages.BaseObjectListPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.ObjectListPageViewModel',
        'AssetManagement.customer.controller.pages.ObjectListPageController',
        'Ext.form.Label',
        'AssetManagement.customer.view.OxGridPanel',
        'Ext.grid.column.Column',
        'Ext.grid.View'
    ],
   
    inheritableStatics: {
		_instance: null,
	
	    getInstance: function() {
	        try {
	            if(this._instance === null) {
	               this._instance = Ext.create('AssetManagement.customer.view.pages.ObjectListPage');
	            }
	        } catch (ex) {
		       AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseObjectListPage', ex);
		    }
	        
	        return this._instance;
	    }
   	},

    viewModel: {
        type: 'ObjectListPageModel'
    },
    
    controller: 'ObjectListPageController',
    
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   
    buildUserInterface: function() {
    	try {
		     var items = [
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 5,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            flex: 0.2
		                        },
		                        {
		                            xtype: 'label',
		                            flex: 0.25,
		                            cls: 'oxHeaderLabel',
		                            text: Locale.getMsg('objectListForOrder')
		                        },
		                        {
		                            xtype: 'oxgridpanel',
		                            flex: 1,
		                            scrollable: 'vertical',
		                            columns: [
		                                {
		                                    xtype: 'gridcolumn',
		                                    flex: 0.2
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    dataIndex: 'vornr',
		                                    text: Locale.getMsg('operation'),
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    dataIndex: 'equnr',
		                                    text: Locale.getMsg('equipment'),
		                                    flex: 1
		                                },
		                                {
		                                    xtype: 'gridcolumn',
		                                    dataIndex: 'qmnum',
		                                    text: Locale.getMsg('notif'),
		                                    flex: 1
		                                }
		                            ],      viewConfig: {
		                                    overflowY: 'auto'
		                            }
		                        },
		                        {
		                            xtype: 'component',
		                            flex: 0.5
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    flex: 0.5
		                }
		            ]
		        }
		    ];
   
		    this.add(items);
		} catch (ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseObjectListPage', ex);
		}
    },
    
    getPageTitle: function() {
		var retval = '';
		
        try {
			retval = Locale.getMsg('objectListForOrder');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseObjectListPage', ex);
		}
		
		return retval;
	}
});