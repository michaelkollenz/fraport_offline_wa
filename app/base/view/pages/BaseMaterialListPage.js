Ext.define('AssetManagement.base.view.pages.BaseMaterialListPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.MaterialListPageViewModel',
        'AssetManagement.customer.controller.pages.MaterialListPageController',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Number'
    ],
    
   inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
            try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.MaterialListPage');
                }
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseMaterialListPage', ex);
		    }
            
            return this._instance;
        }
   },

   mixins: {
       rendererMixin: 'AssetManagement.customer.view.mixins.MaterialListPageRendererMixin'
   },
    
    viewModel: {
        type: 'MaterialListPageModel'
    },
    
    controller:'MaterialListPageController',
   
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
	//protected
	//@override
    buildUserInterface: function() {
        try {
        
        	var items = [
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    maxWidth: 10,
		                    minWidth: 10
		                },
		                {
		                    xtype: 'container',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
		                        	xtype: 'container',
		                           	height: 30,
		                            id: 'searchMaterialContainer',
		                           	layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldMaterialList',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,	                                    
			                                listeners: {
			                                	change: {
			                                        fn: 'onMaterialListPageSearchFieldChange'
			                                    }
		                                	}
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
			                                id: 'materialListPageFilterCriteriaCombobox',
			                                editable: false,
			                                store: [Locale.getMsg('materialNumber'), Locale.getMsg('shorttext'), Locale.getMsg('quantity'),
			                                        Locale.getMsg('unit'), Locale.getMsg('matConf_Plant')],
			                                value: Locale.getMsg('materialNumber'),
			                                listeners: {
	                                    		select: 'onMaterialListPageSearchFieldChange'
	                                    	}
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        },
		                        {
			                        xtype: 'container',
			                        id: 'matStockGridContainer',
			                        flex: 1,
			                        layout: {
	                            		type: 'hbox',
	                            		align: 'stretch'
	                            	},
			                        items: [
			                           this.getNewMatStockGridPanel()
			                        ]
			                    },
		                        {
		                            xtype: 'component',
		                            height: 10
		                        }
		                    ]
		                },
		                {
		                    xtype: 'component',
		                    width: 10
		                }
		            ]
		        }
		    ];
		    
		    this.add(items);
		    this.searchFieldVisibility();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseMaterialListPage', ex);
		}
	},
	
	//private
    renewMatStockGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('matStockGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewMatStockGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewMatStockGridPanel of BaseMaterialListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewMatStockGridPanel: function() {
    	var retval = null;
    	
        try {
            var me = this;
    		var myController = this.getController();
    	
    		retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
                id: 'matStockGridPanel',
	            hideContextMenuColumn: true,
	            useLoadingIndicator: true,
	            loadingText: Locale.getMsg('loadingMaterialStocks'),
	            emptyText: Locale.getMsg('noMaterialStocks'),
	            flex: 1,
	            disableSelection: true,
	            scrollable: 'vertical',
	            parentController: myController,
                owner: me,
	            /*columns: [
	                {
	                    xtype: 'actioncolumn',                     
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		//matconf.pngs
	                		return '<img src="resources/icons/matconf.png"/>';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,
	                    align: 'center'
	                },
	                {
	                    xtype: 'gridcolumn',
	                    dataIndex: 'matnr',
	                    text: Locale.getMsg('material'),
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		var material = record.get('material');
	                		var maktx = material ? material.get('maktx') : '';
	                    
	                		return AssetManagement.customer.utils.StringUtils.trimStart(value, '0') + '<p>' + maktx;
	                    },
	                    flex: 1
	                },
	                {
	                    xtype: 'gridcolumn',
	                    text: Locale.getMsg('orderComponents_Qty'),
	                    maxWidth: 160,
	                    minWidth: 160,
	                    dataIndex: 'labst',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                    	var quant = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'), true);
	                    	var unit = record.get('meins');
	                    	
	                		return quant + ' ' + unit;
	                    }
	                },
	                {
	                    xtype: 'gridcolumn',
	                    text: Locale.getMsg('plantStorLoc'),
	                    maxWidth: 160,
	                    minWidth: 160,
	                    dataIndex: 'werks',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                		var plant = record.get('werks');
	                		var lgort = record.get('lgort');
	                		
	                		return AssetManagement.customer.utils.StringUtils.concatenate([ plant, lgort ], '/', false, false, '-');
	                    }
	                },
	                {
	                	xtype: 'actioncolumn',
	                	id: 'banfIconGridColumn',
	                	maxWidth: 80,
	                    minWidth: 80,
	                    items: [{
	                        icon: 'resources/icons/demand_requirement.png',
	                        tooltip: Locale.getMsg('newPreq'),
	                        iconCls: 'oxGridLineActionButton',
	                        handler: function(grid, rowIndex, colIndex, clickedItem, event, record, tableRow) {
	                            event.stopEvent();
	                            this.getController().onDemandRequirementClick.call(this.getController(), grid, rowIndex, colIndex, clickedItem, event, record, tableRow);
	                        },
	                        scope: this
	                    }],
	                    enableColumnHide: false,
	                    align: 'center'	
	                }
	            ], */
	            listeners:{
	        		cellcontextmenu: myController.onCellContextMenu,
	        		scope: myController
	        	}
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewMatStockGridPanel of BaseMaterialListPage', ex);
    	}
    	
    	return retval;
	},

	//protected
	//@override
	getPageTitle: function() {
	    var retval = '';

	    try {
		    retval = Locale.getMsg('stockList');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseMaterialListPage', ex);
		}
		
		return retval;
	}, 

	//protected
	//@override
	updatePageContent: function() {
		try {
			var matStocks = this.getViewModel().get('matStocks');
			
			//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    			this.renewMatStockGridPanel();
			

    		if (this.getViewModel().get('constrainedMaterials')) {
    		    matStocks = this.getViewModel().get('constrainedMaterials');
    		}
			var materialStockGrid = Ext.getCmp('matStockGridPanel');
			materialStockGrid.setStore(matStocks);
			
			////manage last icon column
			//if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ext_scen_active') !== 'X') {
			//	Ext.getCmp('banfIconGridColumn').setHidden(false);
			//} else {
			//	Ext.getCmp('banfIconGridColumn').setHidden(true);
			//}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseMaterialListPage', ex);
		}
	},
	
	//public
	//@override
	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('matStockGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseMaterialListPage', ex);
    	}
	},
	
	//private
	searchFieldVisibility: function(){       
		try {
			var field = Ext.getCmp('searchMaterialContainer');
			var para = true;  
		  
			if(para === false)
				field.setVisible(false );
		    	  
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseMaterialListPage', ex);
		}
	},

	//public
	getSearchValue: function() {
		try {
			var mtStockToSearch = Ext.create('AssetManagement.customer.model.bo.MatStock', {material: Ext.create('AssetManagement.customer.model.bo.Material', {})});
			var currentFilterCriterion = Ext.getCmp('materialListPageFilterCriteriaCombobox').getValue();	

			// Materialien
			if(currentFilterCriterion === Locale.getMsg('materialNumber'))
				mtStockToSearch.get('material').set('matnr', Ext.getCmp('searchFieldMaterialList').getValue());
			// Kurztext
			else if(currentFilterCriterion === Locale.getMsg('shorttext'))
				mtStockToSearch.get('material').set('maktx', Ext.getCmp('searchFieldMaterialList').getValue());
			// Menge
			else if(currentFilterCriterion === Locale.getMsg('quantity'))
				mtStockToSearch.set('labst', Ext.getCmp('searchFieldMaterialList').getValue());
			// Einheit
			else if(currentFilterCriterion === Locale.getMsg('unit'))
				mtStockToSearch.set('meins', Ext.getCmp('searchFieldMaterialList').getValue());
			// Wert / Lagerort
			else if(currentFilterCriterion === Locale.getMsg('matConf_Plant')) {
				mtStockToSearch.set('werks', Ext.getCmp('searchFieldMaterialList').getValue());
				mtStockToSearch.set('lgort', Ext.getCmp('searchFieldMaterialList').getValue());
			}
				
			this.getViewModel().set('searchMatStock', mtStockToSearch);
		
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseMaterialListPage', ex);
		}
	},

    //public
	getFilterValue: function () {
	    var retval = '';

	    try {
	        retval = Ext.getCmp('searchFieldMaterialList').getValue();

	        if (!retval)
	            retval = '';
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseMaterialListPage', ex);
	    }

	    return retval;
	},
    //public
	setFilterValue: function (value) {
	    try {
	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
	            value = '';

	        retval = Ext.getCmp('searchFieldMaterialList').setValue(value);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseMaterialListPage', ex);
	    }
	}
});