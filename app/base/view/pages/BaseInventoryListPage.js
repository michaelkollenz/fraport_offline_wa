Ext.define('AssetManagement.base.view.pages.BaseInventoryListPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.pagemodel.InventoryListPageViewModel',
        'AssetManagement.customer.controller.pages.InventoryListPageController',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Action',
        'Ext.form.Label',
        'AssetManagement.customer.view.utils.FileSelectionButton'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.view.pages.InventoryListPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseInventoryListPage', ex);
            }

            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.InventoryListPageRendererMixin'
    },

    controller: 'InventoryListPageController',

    viewModel: {
        type: 'InventoryListPageModel'
    },

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    //protected
    //@override
    buildUserInterface: function () {
        try {
            var items = [
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'hbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'container',
                            margin: '0 10 0 10',
		                    flex: 1,
		                    layout: {
		                        type: 'vbox',
		                        align: 'stretch'
		                    },
		                    items: [
		                        {
		                            xtype: 'container',
                                    margin: '10 0 0 0',
		                            height: 30,
		                            id: 'searchInventoryContainer',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
		                                {
		                                    xtype: 'oxtextfield',
		                                    id: 'searchFieldInventory',
		                                    margin: '0 0 0 5',
		                                    labelStyle: 'display: none;',
		                                    flex: 1,
		                                    listeners: {
		                                        change: {
		                                            fn: 'onInventoryListPageSearchFieldChange'
		                                        }
		                                    }
		                                },
		                                {
		                                    xtype: 'oxcombobox',
		                                    margin: '0 5 0 10',
		                                    labelStyle: 'display: none;',
		                                    width: 250,
		                                    id: 'inventoryListPageFilterCriteriaCombobox',
		                                    editable: false,
		                                    store: [Locale.getMsg('inventoryDocument'), Locale.getMsg('plant'), Locale.getMsg('storLoc'),
			                                        Locale.getMsg('scheduledDate')],
		                                    value: Locale.getMsg('inventoryDocument'),
		                                    listeners: {
		                                        select: 'onInventoryListPageSearchFieldChange'
		                                    }
		                                }
		                            ]
		                        },
		                        {
		                            xtype: 'container',
		                            id: 'inventoryGridContainer',
		                            flex: 1,
		                            margin: '10 5 0 5',
		                            layout: {
		                                type: 'hbox',
		                                align: 'stretch'
		                            },
		                            items: [
			                           this.getNewInventoryGridPanel()
		                            ]
		                        }
		                    ]
                        }
		            ]
		        }
            ];

            this.add(items);
            this.searchFieldVisibility();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseInventoryListPage', ex);
        }
    },
    //private
    getNewInventoryGridPanel: function () {
        var retval = null;

        try {
            var myController = this.getController();
            var me = this;
            retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
                id: 'inventoryGridPanel',
                hideContextMenuColumn: true,
                useLoadingIndicator: true,
                loadingText: Locale.getMsg('loadingInventory'),
                emptyText: Locale.getMsg('noInventory'),
                flex: 1,
                scrollable: 'vertical',
                parentController: myController,
                listeners: {
                    cellclick: myController.onInventorySelected,
                    cellcontextmenu: myController.onCellContextMenu,
                    scope: myController
                },
                //needed to get the mixins to the grid
                owner: me
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewinventoryGridPanel of BaseInventoryListPage', ex);
        }

        return retval;
    },

    //protected
    //@override
    getPageTitle: function () {
        var retval = '';

        try {
            var title = Locale.getMsg('inventoryDocument');

            var inventories = this.getViewModel().get('inventoryStore');

            if (inventories) {
                title += " (" + inventories.getCount() + ")";
            }

            retval = title;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseInventoryListPage', ex);
        }

        return retval;
    },

    //protected
    //@override
    updatePageContent: function () {
        try {
            if (AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'safari')
                this.renewComboboxContainer();

            var inventoryItemStore = this.getViewModel().get('inventoryStore');

            //needed for search
            if (this.getViewModel().get('constrainedInventory')) {
                inventoryItemStore = this.getViewModel().get('constrainedInventory');
            }
            var inventoryGridPanel = Ext.getCmp('inventoryGridPanel');

            if (inventoryGridPanel.filterStore && inventoryGridPanel.length > 0) {
                this.setComboFilterValues();
            }

            inventoryGridPanel.setStore(inventoryItemStore);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseInventoryListPage', ex);
        }
    },

    renewComboboxContainer: function () {
        try {
            var searchInventoryContainer = Ext.getCmp('searchInventoryContainer');
            var inventoryListPageFilterCriteriaCombobox = Ext.getCmp('inventoryListPageFilterCriteriaCombobox');
            var comboValue = inventoryListPageFilterCriteriaCombobox.getValue();

            var searchRecord = this.getViewModel().get('searchInventory') ? inventoryListPageFilterCriteriaCombobox.findRecordByDisplay(comboValue) : null;

            searchInventoryContainer.remove(inventoryListPageFilterCriteriaCombobox, true);

            var inventoryListPageFilterCriteriaCombo = {
                xtype: 'oxcombobox',
                margin: '0 5 0 10',
                labelStyle: 'display: none;',
                width: 250,
                value: searchRecord,
                id: 'inventoryListPageFilterCriteriaCombobox',
                editable: false,
                store: [Locale.getMsg('inventoryDocument'), Locale.getMsg('plant'), Locale.getMsg('storLoc'),
                    Locale.getMsg('scheduledDate')],
                value: Locale.getMsg('inventoryDocument'),
                listeners: {
                    select: 'onInventoryListPageSearchFieldChange'
                }
            };

            inventoryListPageFilterCriteriaCombo.value = searchRecord;
            searchInventoryContainer.insert(1, inventoryListPageFilterCriteriaCombo);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewComboboxContainer of BaseInventoryListPage', ex);
        }
    },

    //sets the value for dynamic filtering
    setComboFilterValues: function () {
        var comboBox = Ext.getCmp('inventoryListPageFilterCriteriaCombobox');
        var inventoryGridPanel = Ext.getCmp('inventoryGridPanel');
        comboBox.setStore(inventoryGridPanel.filterStore)

    },
    //public
    //@override
    resetViewState: function () {
        this.callParent();

        try {
            Ext.getCmp('inventoryGridPanel').reset();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseInventoryListPage', ex);
        }
    },

    //private
    searchFieldVisibility: function () {
        try {
            var field = Ext.getCmp('searchInventoryContainer');
            var para = true;

            if (para === false)
                field.setVisible(false);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside searchFieldVisibility of BaseInventoryListPage', ex);
        }
    },

    //public
    getSearchValue: function () {
        try {
            var inventoryToSearch = Ext.create('AssetManagement.customer.model.bo.InventoryHead', {});
            var currentFilterCriterion = Ext.getCmp('inventoryListPageFilterCriteriaCombobox').getValue();

            if (currentFilterCriterion === Locale.getMsg('inventoryDocument'))
                inventoryToSearch.set('physinventory', Ext.getCmp('searchFieldInventory').getValue());
            else if (currentFilterCriterion === Locale.getMsg('plant'))
                inventoryToSearch.set('plant', Ext.getCmp('searchFieldInventory').getValue());
            else if (currentFilterCriterion === Locale.getMsg('storLoc'))
                inventoryToSearch.set('stgeLoc', Ext.getCmp('searchFieldInventory').getValue());
            else if (currentFilterCriterion === Locale.getMsg('scheduledDate'))
                inventoryToSearch.set('planDate', Ext.getCmp('searchFieldInventory').getValue());

            this.getViewModel().set('searchInventory', inventoryToSearch);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSearchValue of BaseInventoryListPage', ex);
        }

    },
    //public
    getFilterValue: function () {
        var retval = '';

        try {
            retval = Ext.getCmp('searchFieldInventory').getValue();

            if (!retval)
                retval = '';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFilterValue of BaseInventoryListPage', ex);
        }

        return retval;
    },
    //public
    setFilterValue: function (value) {
        try {
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value))
                value = '';

            retval = Ext.getCmp('searchFieldInventory').setValue(value);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setFilterValue of BaseInventoryListPage', ex);
        }
    }

});