Ext.define('AssetManagement.base.view.pages.BaseCustomerListPage', {
	extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.model.pagemodel.CustomerListPageViewModel',
        'AssetManagement.customer.controller.pages.CustomerListPageController',
        'AssetManagement.customer.model.bo.Customer',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'AssetManagement.customer.view.OxGridPanel',
        'AssetManagement.customer.view.OxDynamicGridPanel',
        'Ext.grid.column.Column',
        'Ext.grid.View',
        'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.view.pages.CustomerListPage');
                }
            } catch(ex) {
    		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseCustomerListPage', ex);
    	    }
            
            return this._instance;
        }
    },

    mixins: {
        rendererMixin: 'AssetManagement.customer.view.mixins.CustomerListPageRendererMixin'
    },
    
    viewModel: {
        type: 'CustomerListPageModel'
    },
    
    controller: 'CustomerListPageController',
    
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
	//protected
	//@override
    buildUserInterface: function() {
    	try {
    		var myController = this.getController();
    		
		    var items = [
		        {
		            xtype: 'component',
		            maxWidth: 10,
		            minWidth: 10
		        },
		        {
                    xtype: 'container',
                    id: 'customerGridContainer',
                    flex: 1,
                    layout: {
                		type: 'hbox',
                		align: 'stretch'
                	},
                	margin: '20 0 0 0',
                    items: [
                       this.getNewCustomerGridPanel()
                    ]
		        },
		        {
		            xtype: 'component',
		            maxWidth: 10,
		            minWidth: 10
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseCustomerListPage', ex);
		}
    },
    
    //private
    renewCustomerGridPanel: function() {
    	var retval = null;
    
    	try {
    		var gridContainer = Ext.getCmp('customerGridContainer');
    		
    		if(gridContainer)
    			gridContainer.removeAll(true);
    	
    		retval = this.getNewCustomerGridPanel();
    		
    		if(gridContainer && retval) {
    			gridContainer.add(retval);
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewCustomerGridPanel of BaseCustomerListPage', ex);
    		retval = null;
    	}
    	
    	return retval;
    },
    
    //private
    getNewCustomerGridPanel: function() {
    	var retval = null;
    	
    	try {
    		var myController = this.getController();
    		var me = this;
    		retval = Ext.create('AssetManagement.customer.view.OxDynamicGridPanel', {
	            id: 'customerGridPanel',
	            flex: 1,
	            useLoadingIndicator: true,
                hideContextMenuColumn: true,
	            loadingText: Locale.getMsg('loadingCustomers'),
	            emptyText: Locale.getMsg('noCustomers'),
	            scrollable: 'vertical', 
	            /*columns: [
	                {
	                    xtype: 'actioncolumn',
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        return '<img src="resources/icons/partner.png"  />';
	                    },
	                    maxWidth: 80,
	                    minWidth: 80,
	                    align: 'right'
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var customerGroup = record.get('kdgrp');
	                        var customerNo = record.get('kunnr');
	                                                                                                                                       
	                        return customerGroup + '<p>' + customerNo + '</p>';                              
	                    },
	                    dataIndex: 'kunnr',
	                    text: Locale.getMsg('customer')
	                },
	                {
	                    xtype: 'gridcolumn',
	                    flex: 1,
	                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
	                        var name = record.getFullName();
	                        var city = AssetManagement.customer.utils.StringUtils.concatenate([ record.get('postlcod1'), record.get('city') ], ' ', true);
	                        return name + '<p>' + city + '</p>';
	                    },
	                    dataIndex: 'postlcod1',
	                    text: Locale.getMsg('nameAndCity')
	                }
	            ],*/
	            parentController: myController,
                owner: me,
	            listeners: {
	            	cellclick: myController.onCustomerSelected,
	            	cellcontextmenu: myController.onCellContextMenu,
	            	scope: myController
	            }
    		});
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewCustomerGridPanel of BaseCustomerListPage', ex);
    	}
    	
    	return retval;
	},
	
	//protected
	//@override
    getPageTitle: function() {
    	var retval = '';
    	
    	try {
	    	retval = Locale.getMsg('customers');
	    	
	    	var customers = this.getViewModel().get('customerStore');
	    	
	    	if(customers) {
	    		retval += " (" + customers.getCount() + ")";
	    	}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseCustomerListPage', ex);
    	}

    	return retval;
	},

	//protected
	//@override
    updatePageContent: function() {
    	try {
    		var customersStore = this.getViewModel().get('customerStore');
    		
    		//workaround for chrome43+ & ExtJS 6.0.0 Bug
    		if(AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'chrome')
    			this.renewCustomerGridPanel();
    		
    		var customerGridPanel = Ext.getCmp('customerGridPanel');
    		customerGridPanel.setStore(customersStore);
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseCustomerListPage', ex);
    	}
	},

	//public
	//@override
	resetViewState: function() {
		this.callParent();
	
		try {
			Ext.getCmp('customerGridPanel').reset();
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetViewState of BaseCustomerListPage', ex);
    	}
	}
});