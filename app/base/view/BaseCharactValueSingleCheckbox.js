﻿Ext.define('AssetManagement.base.view.BaseCharactValueSingleCheckbox', {
    extend: 'Ext.form.field.Checkbox',




    config: {
        xtype: 'checkboxfield',
        flex: 1,
        baseCls: 'oxCheckBox',
        checkedCls: 'checked',
        labelStyle: 'padding-top: 8px;',
        margin: '0 0 0 10',
        labelWidth: 420,
        width: 30,
        height: 35,
        listeners: {
            change: function (checkbox, newVal, oldVal) {
                if (this._einWert && newVal === true && oldVal === false)
                    AssetManagement.customer.view.dialogs.SingularValueCheckboxDialog.getInstance().resetAllCheckBoxesExceptForOne(this._atwrt, this._atwtb, this._atflv);
            }
        }
    },


    _charact: null,
    _charactValue: null,
    _classValue: null,
    _einWert: false,




    constructor: function (config) {
        try {
            this.callParent(arguments);
            if (!config) {
                config = {};

                arguments = [config];
            }

            this._charact = config.charact;
            this._charactValue = config.charactValue;
            this._classValue = config.classValue;
            this._atwrt = config.atwrt;
            this._atwtb = config.atwtb;
            this._atflv = config.atflv;
            this._einWert = config.einWert;
            this._sid = config.sid;
            // Set Merkmal Label
            this.buildFieldLabel();
            
            if (!this.getValue()) {
                var classList = this.protoEl.classList;
                var index = classList.indexOf("checked");
                if (index > -1) {
                    classList.splice(index, 1);
                }
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueSingleCheckbox', ex);
        }
    },

    getClassValue: function () {
        var retval = null;
        try {

            if (this.getValue() === true) {
                retval = this._classValue;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassValue of BaseCharactValueSingleCheckbox', ex);
        }
        return retval;
    },

    buildFieldLabel: function () {
        try {
            if (this._charactValue) {
                this.fieldLabel = this._charactValue.get('atwrt') + " " + this._charactValue.get('atwtb') + " " + this._charactValue.get('atflv');
            } else {
                this.fieldLabel = this._classValue.get('atwrt') + " " + this._classValue.get('atflv');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildFieldLabel of BaseCharactValueSingleCheckbox', ex);

        }
    },

    getAtwrt: function () {
        try {
            return this._atwrt;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAtwrt of BaseCharactValueSingleCheckbox', ex);
        }
    },

    getAtwtb: function () {
        try {
            return this._atwtb;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAtwtb of BaseCharactValueSingleCheckbox', ex);
        }
    },

    getAtflv: function () {
        try {
            return this._atflv;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAtflv of BaseCharactValueSingleCheckbox', ex);
        }
    }
});