﻿Ext.define('AssetManagement.base.view.BaseChecklistCheckbox', {
    extend: 'Ext.form.field.Checkbox',




    config: {
        xtype: 'checkboxfield',
        flex: 1,
        baseCls: 'oxCheckBox',
        checkedCls: 'checked',
        labelStyle: 'padding-top: 8px;',
        margin: '0 0 0 10',
        labelWidth: 420,
        width: 30,
        height: 35

    },

    _auswmenge1: null,
    _auswmgwrk1: null,

    constructor: function (config) {
        try {
            this.callParent(arguments);
            if (!config) {
                config = {};

                arguments = [config];
            }

            this._auswmenge1 = config.auswmenge1;
            this._auswmgwrk1 = config.auswmgwrk1;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseChecklistCheckbox', ex);
        }
    }

});