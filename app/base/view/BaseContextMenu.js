Ext.define('AssetManagement.base.view.BaseContextMenu', {
    extend: 'Ext.menu.Menu',


    requires: [
        'AssetManagement.customer.model.viewmodel.ContextMenuViewModel',
        'AssetManagement.customer.controller.ContextMenuController',
        'Ext.menu.Item', 
        'Ext.util.HashMap'
    ],
    
    config: {
		cls: 'oxContextMenu'
	},

    viewModel: {
        type: 'ContextMenuModel'
    },
    
    controller: 'ContextMenuController',
    width: 120,
    
    items: [
    ],

	//private
	_contextMenuItems: null,
	record: null,

	constructor: function(config) {
		this.callParent(arguments);
		
		try {
		    this.generateContextMenuItems();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseContextMenu', ex);
		}    
	},
	
	setContextMenuItems: function(contextItemIds) {
		try {
			var contextMenu = this;
			//do not destroy the buttons, because they will be used again
			contextMenu.removeAll(false);	
		
			if(contextItemIds && this._contextMenuItems) {
				var menuItem;
				Ext.Array.each(contextItemIds, function(id) {
					menuItem = this._contextMenuItems.get(id);
					
					if(menuItem)
						contextMenu.add(menuItem);
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setContextMenuItems of BaseContextMenu', ex);
		}
	},	
	
	//private
	generateContextMenuItems: function() {
		try {
			var contextMenuItems = Ext.create('Ext.util.HashMap');
			var idPrefix = this.getController().getInternalId() + '-';
			
			var menuItem = Ext.create('Ext.menu.Item', {
          		 id: idPrefix + 'deleteButtonMI',
          		 cls: 'oxContextMenuItem',
           		 text: Locale.getMsg('delete'),
           		 listeners: {
	            	click: 'onContextMenuItemSelected'
	            }
			});
			contextMenuItems.add(menuItem);
			
			menuItem = Ext.create('Ext.menu.Item', {
          		 id: idPrefix + 'editButtonMI',
          		 cls: 'oxContextMenuItem',
            	 text: Locale.getMsg('edit'),
            	 listeners: {
	            	click: 'onContextMenuItemSelected'
	           	 }
			});
			contextMenuItems.add(menuItem);
			
			menuItem = Ext.create('Ext.menu.Item', {
          		 id: idPrefix + 'longtextButtonMI',
          		 cls: 'oxContextMenuItem',
            	 text: Locale.getMsg('longtext'),
            	 listeners: {
	            	click: 'onContextMenuItemSelected'
	           	 }
			});
			contextMenuItems.add(menuItem);

			menuItem = Ext.create('Ext.menu.Item', {
          		 id: idPrefix + 'printChecklistButtonMI',
          		 cls: 'oxContextMenuItem',
            	 text: Locale.getMsg('print'),
            	 listeners: {
	            	click: 'onContextMenuItemSelected'
	           	 }
			});
			contextMenuItems.add(menuItem);
			
			this._contextMenuItems = contextMenuItems;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateOptionsMenuItems of BaseContextMenu', ex);
		}
	},
	
	//private
	onCreateContextMenu: function(items, record) {
		try {
		    this.getController().setMenuItems(items); 
		    this.record = record; 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateContextMenu of BaseContextMenu', ex);
		}    
	}
});