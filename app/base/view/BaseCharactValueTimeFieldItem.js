﻿Ext.define('AssetManagement.base.view.BaseCharactValueTimeFieldItem', {
    extend: 'Ext.form.field.Time',


    requires: [
       'AssetManagement.customer.controller.dialogs.SingularValueLineDialogController',
       'AssetManagement.customer.model.dialogmodel.SingularValueLineDialogViewModel',
       'Ext.container.Container',
       'Ext.form.Label',
       'Ext.form.field.Number',
       'Ext.button.Button',
       'AssetManagement.customer.utils.StringUtils'
    ],

    config: {
        xtype: 'timefield',
        format: 'H:i', 
        flex: 1,
        margin: '0 0 0 10',
        labelWidth: 320,
        width: 30,
        height: 25
    },



    _charact: null,
    _charactValue: null,
    _classValue: null,

    constructor: function (config) {
        try {
            if (!config) {
                config = {};

                arguments = [config];
            }
            this._charact = config.charact;
            this._charactValue = config.charactValue;

            if (this._charact)
                this._classValue = this._charact.get('classValues');

            this.fieldLabel = config.charact.get('mseht') || config.charact.get('msehi');
            this.callParent(arguments);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCharactValueTimeFieldItem', ex);
        }
    }
});