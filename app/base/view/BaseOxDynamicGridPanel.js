Ext.define('AssetManagement.base.view.BaseOxDynamicGridPanel', {
    extend: 'AssetManagement.customer.view.OxGridPanel',



    requires:[
        'AssetManagement.customer.model.bo.ViewLayout'
    ],
    parentController: null,
    //used to pass the renderer mixins from the page
    owner: null,
    //prevent the clicking on the checkbox columns on the grid
    preventCheckColumnClick: false,
    listeners: {
        //we need this listener to block the cellclick listener, if the celltype is a checkbox
        beforecellclick: function (cell, td, cellIndex, record, tr, rowIndex, e, eOpts) {
            try {
                var grid = this;
                //get the handlerMixin from the owner controller
                var clickHandlers = this.owner.getController().mixins.handlerMixin
                //here we are getting the cell column
                var columns = this.columnManager.getColumns();
                var foundColumn = columns[cellIndex];
                //call the handler if there is any handler defined on the column
                if (foundColumn.cellClickHandler && foundColumn.cellClickHandler !== '') {
                    var handlerName = foundColumn.cellClickHandler;
                    //we need to send the grid as a param, as we need some functions from it
                    clickHandlers[handlerName](cell, td, cellIndex, record, tr, rowIndex, e, eOpts, grid);
                    return false;
                }
                if (e.getTarget().className.indexOf('grid-checkcolumn') !== -1) {
					return false;
                }
				if (e.getTarget().className.indexOf('oxGridLineActionButton') !== -1) {
					return false;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the listeners(beforecellclick) of BaseOxDynamicGridPanel', ex);
            }
        },
        //fires contextmenu while longpressing on all of the grids
        //needed to fire the event
        longpress: {
            element: 'el',
            fn: function (ev, node, options, eOpts) {
                try {
                    var grid = Ext.getCmp(this.id);
                    var td = null;
                    var tr = null;
                    var rowIndex = null
                    var cellIndex = null;
                    var record = ev.record
                    grid.getView().fireEvent('cellcontextmenu', grid, td, cellIndex, record, tr, rowIndex, ev, eOpts);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside listeners(longpress) of BaseOxDynamicGridPanel', ex);
                }
            }
        }
        //we will need the resize for the smartphones. if the smartphone is in landscape or in portrait mode to show/hide some columns
        /*,
        resize: function (grid, width, height, oldWidth, oldHeight, eOpts) {
            if (width < height) {

            }

        }*/
    },

    initComponent: function () {
        try {
            var me = this;
            var id = this.id;

            this.callParent(arguments);
            me.columns = me.generateDynamicColumns(id);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initComponent of BaseOxDynamicGridPanel', ex);
        }
    },

    //generate the dynamic Grid Columns
    generateDynamicColumns: function (gridname) {
        try {
            var me = this;
            var retval = null;
            //in the viewLayoutInstance are the records stored which contain the definition of the grids
            var viewLayoutInstance = AssetManagement.customer.model.bo.ViewLayout.getInstance();
            var layoutJson = viewLayoutInstance.getViewFromHash(gridname);

            //if there isn't any layoutName defined fall back to the local json file for grid
            if (!layoutJson) {
                var jsonUrl = 'resources/json/dynamicgrids/' + gridname + '.json'
                Ext.Ajax.request({
                    url: jsonUrl,
                    callback: function (opts, success, response) {
                        try {
                            if (success === true || response && response.status === 0) {
                                me.generateColsForGrid(response.responseText);

                            } else {
                                AssetManagement.customer.helper.OxLogger.logException('failure reading \' '+gridname +'.json\' with status code ' + response.status);
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the request of generateDynamicColumns of BaseOxDynamicGridPanel', ex);
                        }
                    }
                });
            } else {
                me.generateColsForGrid(layoutJson);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the generateDynamicColumns of BaseOxDynamicGridPanel', ex);
        }
    },

    generateColsForGrid: function (layoutJson) {
        try {
            var me = this;
            var resp = Ext.decode(layoutJson);
            var grid = resp;
            /*for (var propName in resp) {
                grid = resp[propName]
            }*/

            var columns = [];
            var filterContainer;
            var filterArray = [];

            var searchArray = [];
            var isMobile = AssetManagement.customer.utils.UserAgentInfoHelper.isMobile();

            if (!me.checkBoolean(grid.SEARCHABLE)) {
                if (me.parentController.setSearchable) {
                    me.parentController.setSearchable(false);
                }

            } else {
                if (me.parentController.setSearchable) {
                    me.parentController.setSearchable(true);
                }
            }


            if (grid.VIEWCOLUMNS.length > 0) {
                //set the column Order based on the sequence attribute
                grid.VIEWCOLUMNS.sort(me.dynamicSort("COLUMN_SEQ"))
                for (var j = 0; j < grid.VIEWCOLUMNS.length; j++) {
                    //build the columns
                    switch (grid.VIEWCOLUMNS[j].UIEDITOR_TYPE.toLowerCase()) {
                        case "text":
                        default:
                            //add searchable columns to the searchArray
                            me.addToSearchArray(grid.VIEWCOLUMNS[j], searchArray);
                            //create the column
                            var col = me.createTextColumn(grid.VIEWCOLUMNS[j], filterArray, isMobile);
                            columns.push(col);
                            break;
                        case "icon":
                            //icon columns are mappend to the actioncolumns
                            //add searchable columns to the searchArray
                            me.addToSearchArray(grid.VIEWCOLUMNS[j], searchArray);
                            //create the column
                            var col = me.createActionColumn(grid.VIEWCOLUMNS[j], filterArray, isMobile);
                            columns.push(col);
                            break;
                        case "date":
                            //add searchable columns to the searchArray
                            me.addToSearchArray(grid.VIEWCOLUMNS[j], searchArray);
                            //create the column
                            var col = me.createDateColumn(grid.VIEWCOLUMNS[j], filterArray, isMobile);
                            columns.push(col);
                            break;
                        case "checkbox":
                            //add searchable columns to the searchArray
                            me.addToSearchArray(grid.VIEWCOLUMNS[j], searchArray);
                            //create the column
                            var col = me.createCheckBoxColumn(grid.VIEWCOLUMNS[j], filterArray, isMobile);
                            columns.push(col);
                            break;
                    }
                }
            } else {
                //in case no column defined, in order not to break the framework, we need t define at least one column
                //this is just to prevent the framework from breaking down
                columns = [{
                    xtype: 'gridcolumn',
                    flex: 1
                }]
            }

            var filterboxStore = []
            if (filterArray.length > 0) {
                filterArray.sort(me.dynamicSort("position"));
                for (var x = 0; x < filterArray.length; x++) {
                    //var objtopush = Locale.getMsg(filterArray[x].id);
                    var objtopush = me.replaceUndefined(filterArray[x].id)
                    filterboxStore.push(objtopush);
                }
            }
            searchArray = searchArray.sort(me.dynamicSort("position"));
            me.searchArray = searchArray;
            me.filterStore = filterboxStore;

            //due to the fact that we are doing in the last step reconfigure (columns), the default columns from the oxGridPanel constructor will be overridden.
            //This is why we need this logic here
            if (columns && me.hideContextMenuColumn !== true) {
                var contextMenuColumnConfig = {
                    xtype: 'actioncolumn',
                    text: Locale.getMsg('menu'),
                    maxWidth: 80,
                    minWidth: 80,
                    items: [{
                        icon: 'resources/icons/menu.png',
                        tooltip: Locale.getMsg('contextMenu'),
                        iconCls: 'oxGridLineActionButton',
                        handler: this.onContextMenuButtonClick,
                        scope: this
                    }],
                    enableColumnHide: false,
                    align: 'center'
                }

                columns.push(contextMenuColumnConfig);
            }

            me.reconfigure(columns);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the generateColsForGrid of BaseOxDynamicGridPanel', ex);
        }
    },

    checkBoolean: function (variable) {
        try {
            if (variable && (variable === true || variable === 'X'))
                return true;
            else
                return false;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseBooleanX of BaseOxDynamicGridPanel', ex);
        }
    },
    dynamicSort: function (property) {
        try {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (Number(a[property]) < Number(b[property])) ? -1 : (Number(a[property]) > Number(b[property])) ? 1 : 0;
                return result * sortOrder;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the dynamicSort of BaseOxDynamicGridPanel', ex);
        }
    },
    //private
    createTextColumn: function (column, filterArray, isMobile) {
        try {
            var me = this;
            //try to set the column Name, avoiding the Undefined.
            var columnName = this.setColumnName(column);

            //try to set visibility of the column based on the visibleInSmall, and if the device is Mobile
            var columnVisibility = this.setColumnVisibility(column, isMobile);

            //set the click handler for the cell
            var cellClickHandler = this.setCellClickHandler(column);

            //normal grid columns
            var col = {
                xtype: 'gridcolumn',
                text: columnName,
                hidden: !columnVisibility,
                sortable: me.checkBoolean(column.SORTABLE),
                resizable: me.checkBoolean(column.RESIZABLE),
                dataIndex: column.DATAINDEX,
                cellClickHandler: cellClickHandler
            }

            //setting column width
            this.setColumnWidth(column, col);

            //setting column renderer
            this.setColumnRenderer(column, col, isMobile)

            //setting the filterrecord
            this.setFilterRecord(column, filterArray);

            //set the column Items
            var columnItems = this.setColumnItems(column);
            if(columnItems && columnItems.length >0){
                col.items = columnItems;
            }

            return col;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the createTextColumn of BaseOxDynamicGridPanel', ex);
        }
    },
    //private
    createActionColumn: function (column, filterArray, isMobile) {
        try {
            var me = this;
            //try to set the column Name, avoiding the Undefined.
            var columnName = this.setColumnName(column);

            //try to set visibility of the column based on the visibleInSmall, and if the device is Mobile
            var columnVisibility = this.setColumnVisibility(column, isMobile);

            //set the click handler for the cell
            var cellClickHandler = this.setCellClickHandler(column);

            //action grid columns
            var col = {
                xtype: 'actioncolumn',
                text: columnName,
                hidden: !columnVisibility,
                sortable: me.checkBoolean(column.SORTABLE),
                resizable: me.checkBoolean(column.RESIZABLE),
                cellClickHandler: cellClickHandler
            }
            //setting column width
            this.setColumnWidth(column, col);

            //setting the google maps functionality if the column has the dataIndex "maps"
            if (column.DATAINDEX) {
                if (column.DATAINDEX.toLowerCase() === "maps") {
                    col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
						var imageSrc = 'resources/icons/maps_small.png';
                        var retval = '<img src="' + imageSrc + '"   style="height:40px;width:40px;margin-left:auto;margin-right:auto;display:block;" />';
						return retval;
					}
                }
            } else {
				//setting column renderer
				this.setColumnRenderer(column, col, isMobile);
			}

            //set the column Items
            var columnItems = this.setColumnItems(column);
            if(columnItems && columnItems.length >0){
                col.items = columnItems;
            }

            return col;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the createActionColumn of BaseOxDynamicGridPanel', ex);
        }
    },
    //private
    createDateColumn: function (column, filterArray, isMobile) {
        try {
            var me = this;
            //try to set the column Name, avoiding the Undefined.
            var columnName = this.setColumnName(column);

            //try to set columnItems based on function
            var colItems = this.setColumnItems

            //try to set visibility of the column based on the visibleInSmall, and if the device is Mobile
            var columnVisibility = this.setColumnVisibility(column, isMobile);

            //set the click handler for the cell
            var cellClickHandler = this.setCellClickHandler(column);

            //date grid columns
            var col = {
                xtype: 'datecolumn',
                text: columnName,
                hidden: !columnVisibility,
                sortable: me.checkBoolean(column.SORTABLE),
                resizable: me.checkBoolean(column.RESIZABLE),
                dataIndex: column.DATAINDEX,
                cellClickHandler: cellClickHandler
            }
            //setting column width
            this.setColumnWidth(column, col);

            //setting column renderer
            this.setColumnRenderer(column, col, isMobile)

            //setting the filterrecord
            this.setFilterRecord(column, filterArray);

            //set the column Items
            var columnItems = this.setColumnItems(column);
            if(columnItems && columnItems.length >0){
                col.items = columnItems;
            }

            return col;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the createDateColumn of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    createCheckBoxColumn: function (column, filterArray, isMobile) {
        try {
            var me = this;
            //try to set the column Name, avoiding the Undefined.
            var columnName = this.setColumnName(column);

            //try to set visibility of the column based on the visibleInSmall, and if the device is Mobile
            var columnVisibility = this.setColumnVisibility(column, isMobile);

            //set the click handler for the cell
            var cellClickHandler = this.setCellClickHandler(column);


            //date grid columns
            var col = {
                xtype: 'checkcolumn',
                text: columnName,
                hidden: !columnVisibility,
                resizable: me.checkBoolean(column.RESIZABLE),
                dataIndex: column.DATAINDEX,
                tdCls: 'oxCheckBoxGridColumnItem',
                //cellClickHandler: cellClickHandler,
                listeners: {
                    beforecheckchange: function () {
                        //check if the click on the checkcolumn should be disabled
                        if (me.preventCheckColumnClick) {
                            return false;
                        }
                    }
                }
            }

            if (!me.preventCheckColumnClick) {
                col.cellClickHandler = cellClickHandler;
            }

            //setting column width
            this.setColumnWidth(column, col);

            //setting column renderer
            if (column.USE_RENDERER) {
                this.setColumnRenderer(column, col, isMobile)
            }

            //setting the filterrecord
            this.setFilterRecord(column, filterArray);

            //set the column Items
            var columnItems = this.setColumnItems(column);
            if(columnItems && columnItems.length >0){
                col.items = columnItems;
            }

            return col;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the createCheckBoxColumn of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the column Name
    setColumnName: function (column) {
        try {
            var columnName;
            var me = this;
            if (column.COLUMN_NAME && column.COLUMN_NAME !== '') {
                columnName = me.replaceUndefined(column.COLUMN_NAME);
            } else {
                columnName = '';
            }
            return columnName;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setColumnName of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //replace Undefined from strings
    replaceUndefined: function(text){
        try {
            var name = Locale.getMsg(text);
            if (name.indexOf('UNDEFINED') !== -1) {
                name = name.replace('.UNDEFINED', '');
            }

            return name;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the replaceUndefined of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the column Width
    setColumnWidth: function (receivedColumn, columnToSet) {
        try {
            if (!receivedColumn.COLUMN_WIDTH) {
                columnToSet.flex = 1;
            } else if (receivedColumn.COLUMN_WIDTH.indexOf("*") !== -1) {
                var flexMinWidth = receivedColumn.COLUMN_WIDTH.split("*");
                var flex = flexMinWidth[0];
                columnToSet.flex = Number(flex);
                if (flexMinWidth.length > 1) {
                    var minWidth = flexMinWidth[1];
                    columnToSet.minWidth = Number(minWidth);
                }
            } else {
                /*columnToSet.minWidth = Number(receivedColumn.COLUMN_WIDTH);
                columnToSet.maxWidth = Number(receivedColumn.COLUMN_WIDTH);*/
                columnToSet.width = Number(receivedColumn.COLUMN_WIDTH);
                columnToSet.minWidth = Number(receivedColumn.COLUMN_WIDTH);
                columnToSet.maxWidth = Number(receivedColumn.COLUMN_WIDTH);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setColumnWidth of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the column Items
    setColumnItems: function (column) {
        try {
            var columnItems
            var me = this;
            var pageItems = this.owner.mixins.itemsMixin
            if (pageItems) {
                if (column.ITEMS && column.ITEMS !== '') {
                    columnItems = pageItems[column.ITEMS](); //me.owner[column.ITEMS];
                }
            }

            return columnItems;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setColumnName of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the filter record for the column
    setFilterRecord: function (receivedColumn, filterArray) {
        try {
            var me = this;
            if (receivedColumn.FILTERABLE && me.checkBoolean(receivedColumn.FILTERABLE)) {
                var filterpos = receivedColumn.FILTER_POS ? receivedColumn.FILTER_POS : 1;
                var filterId
                if(receivedColumn.FILTER_DESC_ID && receivedColumn.FILTER_DESC_ID.toString() !== ''){
                    filterId = receivedColumn.FILTER_DESC_ID;
                }
                if (filterId) {
                    filterId = filterId.split(", ");
                    for (var i = 0; i < filterId.length; i++) {
                        filterArray.push({
                            id: filterId[i],
                            position: filterpos
                        })
                    }
                }

            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setFilterRecord of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the column visibility
    setColumnVisibility: function (receivedColumn, isMobile) {
        try {
            var me = this;
            var columnVisibility;
            if (isMobile) {
                if (receivedColumn && receivedColumn.VISIBLE_IN_SMALL && me.checkBoolean(receivedColumn.VISIBLE_IN_SMALL)) {
                    columnVisibility = true
                } else {
                    columnVisibility = false
                }
            } else {
                if (receivedColumn && receivedColumn.VISIBLE && me.checkBoolean(receivedColumn.VISIBLE)) {
                    columnVisibility = true
                } else {
                    columnVisibility = false
                }
            }
            if (!columnVisibility) {
                columnVisibility = false;
            }

            return columnVisibility
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setColumnVisibility of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the column renderer
    setColumnRenderer: function (receivedColumn, columnToSet, isMobile) {
        try {
            var me = this;
            //getting the page renderers
            var pageRenderers = this.owner.mixins.rendererMixin

            var owner = me.owner
            //var pageRenderers = this.mixins.rendererMixin
            if (isMobile) {
                if (receivedColumn.USE_RENDERER && me.checkBoolean(receivedColumn.USE_RENDERER) && receivedColumn.RENDERER_SMALL && receivedColumn.RENDERER_SMALL !== '') {
                    var rendererToUse = receivedColumn.RENDERER_SMALL;
                    columnToSet.renderer = pageRenderers[rendererToUse](owner);
                } else {
                    switch (receivedColumn.UIEDITOR_TYPE.toLowerCase()) {
                        case "text":
                            if (receivedColumn.BUSOBJ_FIELDNAME || receivedColumn.BUSOBJ_FIELDNAME2 || receivedColumn.BUSOBJ_FIELDNAME3) {
                                pageRenderers.defaultMultipleLineRenderer(receivedColumn.BUSOBJ_FIELDNAME, receivedColumn.BUSOBJ_FIELDNAME2, receivedColumn.BUSOBJ_FIELDNAME3);
                            } else {
                                columnToSet.renderer = pageRenderers.defaultRenderer();
                            }
                            break;
                        case "checkbox":
                            columnToSet.renderer = pageRenderers.defaultCheckBoxRenderer();
                            break;
                        default:
                            break;

                    }
                }
            } else {
                if (receivedColumn.USE_RENDERER && me.checkBoolean(receivedColumn.USE_RENDERER) && receivedColumn.RENDERER && receivedColumn.RENDERER !== '') {
                    var rendererToUse = receivedColumn.RENDERER;
                    columnToSet.renderer = pageRenderers[rendererToUse](owner);
                } else {
                    //Default renderers for diferent column types
                    switch (receivedColumn.UIEDITOR_TYPE.toLowerCase()) {
                        case "text":
                            if (receivedColumn.BUSOBJ_FIELDNAME || receivedColumn.BUSOBJ_FIELDNAME2 || receivedColumn.BUSOBJ_FIELDNAME3) {
                                columnToSet.renderer = pageRenderers.defaultMultipleLineRenderer(receivedColumn.BUSOBJ_FIELDNAME, receivedColumn.BUSOBJ_FIELDNAME2, receivedColumn.BUSOBJ_FIELDNAME3)
                            } else {
                                columnToSet.renderer = pageRenderers.defaultRenderer();
                            }
                            break;
                        case "checkbox":
                            columnToSet.renderer = pageRenderers.defaultCheckBoxRenderer();
                            break;
                        default:
                            break;

                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setColumnRenderer of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //set the column click
    setCellClickHandler: function (column) {
        try {
            var cellClickHandler;
            if (column.CLICK_HANDLER && column.CLICK_HANDLER !== '') {
                cellClickHandler = column.CLICK_HANDLER
            }

            if (!cellClickHandler) {
                cellClickHandler = '';
            }
            return cellClickHandler

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the setCellClickHandler of BaseOxDynamicGridPanel', ex);
        }
    },

    //private
    //add dataIndexes to the searchArray
    addToSearchArray: function (receivedColumn, searchArray) {
        try {
            var me = this;
            if (receivedColumn.SEARCHABLE && me.checkBoolean(receivedColumn.SEARCHABLE)) {
                var searchPos = receivedColumn.SEARCH_POSITION ? receivedColumn.SEARCH_POSITION : 1;
                var searchFunction = receivedColumn.SEARCH_FUNCTION ? receivedColumn.SEARCH_FUNCTION : null;

                //set the name from the searchDescriptionId
                var column = receivedColumn.searchDescriptionId ? receivedColumn.searchDescriptionId : receivedColumn.COLUMN_NAME;
                //var column = receivedColumn.COLUMN_NAME ? receivedColumn.COLUMN_NAME : ''
                if (receivedColumn.DATAINDEX) {
                    searchArray.push({
                        id: receivedColumn.DATAINDEX,
                        columnName: column,
                        searchFunction: searchFunction,
                        position: searchPos
                    })
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside the addToSearchArray of BaseOxDynamicGridPanel', ex);
        }
    }
});