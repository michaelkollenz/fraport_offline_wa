Ext.define('AssetManagement.base.sync.BaseFileUploadManager', {
	mixins: ['Ext.mixin.Observable'],
	
	requires: [
	    'Ext.util.MixedCollection',
		'AssetManagement.customer.sync.ConnectionManager',
	    'AssetManagement.customer.helper.FileHelper',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.manager.DocUploadManager',
		'AssetManagement.customer.helper.OxLogger'
	],
 		
	//private:
	_appConfig: null,
	_connectionManager: null,
	_cancelWorkRequested: false,
	_blockCancelation: false,
	_cancelationInProgress: false,
	_callback: null,
	_callbackScope: null,
	_fileTypeToUpload: null,
	_filesForUploadFound: null,
	_filesSuccessfullyUploadedCount: 0,
	
	constructor: function(config) {
	    try {
	    	this.mixins.observable.constructor.call(this, config);

            this._appConfig = AssetManagement.customer.core.Core.getAppConfig();

            this._connectionManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                listeners: {
                    sendFailed: { fn: this.sendFailed, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            }); 
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseFileUploadManager', ex);
		}
	},
	
	//public methods
	requestWorkCancelation: function() {
		try {
		    this._cancelWorkRequested = true;
		
		    this.fireEvent('cancelationInProgress');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestWorkCancelation of BaseFileUploadManager', ex);
		}    
	},

	//upload files stored inside the database
	//if a specific fileType is provided, only files of this type will be uploaded, else of any type
	//method starts a callback-sequence of private methods
	uploadFiles: function(fileType, callback, callbackScope) {
		try {
			this._filesForUploadFound = null;
			this._filesSuccessfullyUploadedCount = 0;
		
			this._fileTypeToUpload = fileType;
			this._callback = callback;
			this._callbackScope = callbackScope;
			this.getUploadFiles();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside uploadFiles of BaseFileUploadManager', ex);
			this.errorOccured();
		}
	},
	
	//will upload a single file
	uploadFile: function(fileToUpload, callback, callbackScope) {
		try {
			this._filesForUploadFound = [ fileToUpload ];
			this._filesSuccessfullyUploadedCount = 0;
			
			this._fileTypeToUpload = '';
			this._callback = callback;
			this._callbackScope = callbackScope;
		
			if(fileToUpload) {
				var me = this;
			
				var uploadCallback = function(success, response) {
					try {
						if(success === true) {
							me._filesSuccessfullyUploadedCount++;
						}
						
						me.reportWorkFinished();
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside uploadFile of BaseFileUploadManager', ex);
						this.errorOccured();
					}
				};
			
				this.performUpload(fileToUpload, uploadCallback);
			} else {
				this.reportWorkFinished();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside uploadFile of BaseFileUploadManager', ex);
			this.errorOccured();
		}
	},
	
	//private methods
	//previous step: uploadFiles
	//next step: uploadLooper or reportWorkFinished
	getUploadFiles: function(fileType) {
		try {
			if(this._cancelWorkRequested) {
				this.cancelWork();
				return;
			}
	
			var me = this;
			
			var callback = function(docUploads) {
				try {
					if(docUploads === undefined) {
						me.errorOccured('Error uploading files');
					}
				
					if(docUploads && docUploads.getCount() > 0) {
						//do not use the docUploads store! it will get it's records removed, when deleted from database
						me._filesForUploadFound = new Array();
						
						docUploads.each(function(docUpload) {
							me._filesForUploadFound.push(docUpload);
						}, this);
						
						//begin the upload loop
						me.uploadLooper();
					} else {
						me.reportWorkFinished();
					}
				} catch(ex) {
					me.errorOccured('Error uploading files');
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadFiles of BaseFileUploadManager', ex);
				}
			};
			
			var getEventId = AssetManagement.customer.manager.DocUploadManager.getDocUploadsForSync();

			if (getEventId > -1)
			    AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(getEventId, callback);
			else
				me.errorOccured('Error uploading files');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadFiles of BaseFileUploadManager', ex);
			this.errorOccured();
		}
	},
	
	//previous step: getUploadFiles
	//next step: performUpload or reportWorkFinished
	uploadLooper: function() {
		try {
			if(this._cancelWorkRequested) {
				this.cancelWork();
				return;
			}
		
			//prepare the upload, then do the upload
			//for every successfully uploaded and increment the counter and delete the file from the database
			//if done trigger reportWorkFinished
			if(this._filesSuccessfullyUploadedCount < this._filesForUploadFound.length) {
				var me = this;
				
				var fileToUpload = this._filesForUploadFound[this._filesSuccessfullyUploadedCount];
				
				var uploadCallback = function(success, response) {
					try {
						if(success === true) {
							me._filesSuccessfullyUploadedCount++;
							
							//delete the entry from the database and wait for the result
							//it may be ok just to set down the request, if the result does not matter
							var deleteCallback = function(success) {
								try {
									if(success) {
										Ext.defer(me.uploadLooper, 0, me);
									} else {
										me.errorOccured();
									}
								} catch(ex) {
									me.errorOccured();
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside uploadLooper of BaseFileUploadManager', ex);
								}
							};
							
							var deleteEventId = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(fileToUpload);
							
							if(deleteEventId > 0)
								AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEventId, deleteCallback);
							else
								me.errorOccured();
						} else {
                            //failed but try the next to avoid files with a huge size to block following ones to be sent also
						    Ext.defer(me.uploadLooper, 0, me);
						}
					} catch(ex) {
						me.errorOccured();
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside uploadLooper of BaseFileUploadManager', ex);
					}
				};
				
				this.performUpload(fileToUpload, uploadCallback);
			} else {
				//done reportWorkFinished
				this.reportWorkFinished();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside uploadLooper of BaseFileUploadManager', ex);
			this.errorOccured();
		}
	},

	performUpload: function(fileToUpload, uploadCallback) {
		try {
			if(this._cancelWorkRequested) {
				this.cancelWork();
				return;
			}
		
			if(!fileToUpload && uploadCallback) {
				uploadCallback(true);
			}
			
			//build the http header values
			var httpHeaderValues = this.buildHttpHeaderValues(fileToUpload);
			
			//build the binary content header
			var boundary = '-----------------------------' + AssetManagement.customer.utils.DateTimeUtils.getCurrentTime().getTime().toString(16);
			var contentPrefix = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer('\r\n--' + boundary + '\r\n');
			
			var contentHeader = this.buildBinaryContentHeader(fileToUpload);
			
			//extract the actual data
			var fileContent = fileToUpload.get('contentAsArrayBuffer');
			
			var contentTrailer = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer('\r\n--' + boundary + '--\r\n');
			
			//put all arrays together
			var arrayBufferToPost = this.joinArrayBuffers([ contentPrefix, contentHeader, new Uint8Array(fileContent), contentTrailer ]);
		
			//trigger the post
			this._connectionManager.sendFileToBackend(httpHeaderValues, new Uint8Array(arrayBufferToPost), boundary, uploadCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performUpload of BaseFileUploadManager', ex);
			uploadCallback(false);
		}
	},
	
	buildHttpHeaderValues: function(fileToUpload) {
		var nameValuePairList = Ext.create('Ext.util.MixedCollection');
	    
		try {
		    var ac = this._appConfig;
		    nameValuePairList.add("sap-language", ac.getSAPLanguage());
		    nameValuePairList.add("sap-client", ac.getMandt());
		    nameValuePairList.add("sap-user", ac.getUserId());
		    nameValuePairList.add("sap-password", ac.getUserPassword());
		    nameValuePairList.add("foruser", fileToUpload.get('userId'));
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildHttpHeaderValues of BaseFileUploadManager', ex);
		}		
		
		return nameValuePairList;
	},
	
	getContentHeaderValuesOfDocUploadRecord: function(fileToUpload) {
		var retval = null;
		
		
		try {
			var nameValuePairList = Ext.create('Ext.util.MixedCollection');
			
		    nameValuePairList.add("FILENAME", fileToUpload.get('filename'));
			nameValuePairList.add("SCENARIO", fileToUpload.get('scenario'));
		    nameValuePairList.add("FORUSER", fileToUpload.get('userId'));
		    nameValuePairList.add("DOC_SIZE", fileToUpload.get('docSize'));
		    nameValuePairList.add("ORG_FILENAME", fileToUpload.get('orgFilename'));
		    nameValuePairList.add("ID", fileToUpload.get('docId'));
		    nameValuePairList.add("DOC_TYPE", fileToUpload.get('docType'));
		    nameValuePairList.add("REF_TYPE", fileToUpload.get('refType'));
		    nameValuePairList.add("REF_OBJECT", fileToUpload.get('refObject'));
		    nameValuePairList.add("DESCRIPTION", fileToUpload.get('description'));
		    nameValuePairList.add("SPRAS", fileToUpload.get('spras'));
		    nameValuePairList.add("EMAIL", fileToUpload.get('email'));
		    nameValuePairList.add("MOBILEKEY", fileToUpload.get('mobileKey'));
		    nameValuePairList.add("RECORDNAME", fileToUpload.get('recordname'));
		    nameValuePairList.add("RECORDNAME_BIN", fileToUpload.get('recordnameBin'));
		    nameValuePairList.add("UPTIMESTAMP", AssetManagement.customer.utils.DateTimeUtils.getDBTimestamp());
			
			retval = nameValuePairList
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getContentHeaderValuesOfDocUploadRecord of BaseFileUploadManager', ex);
		}
		
		return retval;
	},
	
	buildBinaryContentHeader: function(fileToUpload) {		
	    var asArray = null;
		var asString = '';
		var nextPart = '';
		
		try {
			var headerValues = this.getContentHeaderValuesOfDocUploadRecord(fileToUpload);
			//first part
			asString += 'Content-Disposition: form-data; ';
			
			//second part
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fileToUpload.get('filename'))) {
				var mimeType = AssetManagement.customer.helper.FileHelper.getMimeTypeOfFile(fileToUpload);
			
				nextPart = 'name=\"file\"; filename=\"' + fileToUpload.get('filename') + '\"\r\nContent-Type: ' + mimeType + '\r\n';
			} else {
				nextPart = 'name=\"UPSTREAM\";\r\n';
			}
			
			asString += nextPart;
			
			nextPart = '';
			
			//third part (a join of all header values)
			headerValues.eachKey(function(key, item) {
				nextPart += key + ': ' + item + '\r\n';
			}, this);
			
			asString += nextPart;
			
			//last part
			nextPart = '\r\n';
			
			asString += nextPart;
			
			//transform into array
			asArray = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer(asString);
			
			var test = AssetManagement.customer.utils.StringUtils.getStringFromBytes(asArray);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildBinaryContentHeader of BaseFileUploadManager', ex);
		}
		
		return asArray;
	},
	
	joinArrayBuffers: function (arrayBuffers) {
        var retval = null;

        try {
            var requiredLength = 0;

            Ext.Array.each(arrayBuffers, function (arrayBuffer) {
                requiredLength += arrayBuffer.byteLength;
            }, this);

            var joinedArrayBuffer = new ArrayBuffer(requiredLength);
            var joinedArrayBufferView = new Uint8Array(joinedArrayBuffer);
            var pointer = 0;

            Ext.Array.each(arrayBuffers, function (arrayBuffer) {
                var arrayBufferView = new Uint8Array(arrayBuffer);

                for (var index = 0; index < arrayBufferView.length; index++) {
                    joinedArrayBufferView[pointer] = arrayBufferView[index];
                    pointer++;
                }
            }, this);

            retval = joinedArrayBuffer;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside joinArrayBuffers of BaseFileUploadManager', ex);
        }

        return retval;
    },

	reportWorkFinished: function() {
		try {
			var allCount = this._filesForUploadFound ? this._filesForUploadFound.length : 0;
		
			if(this._callback) {
				this._callback.call(this._callbackScope ? this._callbackScope : this, allCount, this._filesSuccessfullyUploadedCount);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkFinished of BaseFileUploadManager', ex);
			this.errorOccured();
		}
	},
	
	cancelWork: function() {
	    try {
		    this._cancelWorkRequested = false;
		    this._cancelationInProgress = false;
		    this._blockCancelation = false;
		    this._callback = null;
		    this._callbackScope = null;
		
		    var allCount = this._filesForUploadFound ? this._filesForUploadFound.length : 0;
		
		    this.fireEvent('workCancelled', allCount, this._filesSuccessfullyUploadedCount);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseFileUploadManager', ex);
		}
	},

	sendFailed: function (failedType) {
        try {
            var message = '';

            //determine the kind of error, that occured and get the adequate message for it
            switch (failedType) {
                case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                    message = Locale.getMsg('browserIsOffline');
                    break;
                case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                    message = Locale.getMsg('connectionTimeoutOccured');
                    break;
                case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                default:
                    message = Locale.getMsg('generalConnectionErrorOccured');
                    break;
            }

            //trigger an error occurence to stop the current file upload
            this.errorOccured(message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendFailed of BaseFileUploadManager', ex);
        }
    },	
	
	errorOccured: function(message) {
	    try {
		    this._cancelWorkRequested = false;
		    this._cancelationInProgress = false;
		    this._blockCancelation = false;
		    this._callback = null;
		    this._callbackScope = null;
		
		    if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
			   message = 'A general file-upload error occured';
		
       	    this.fireEvent('errorOccured', message);
       	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccured of BaseFileUploadManager', ex);
		}    
	}
});