Ext.define('AssetManagement.base.sync.BaseDataPackageManager', {
    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.controller.EventController',
        'Ext.data.Store',
		'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        //public
        //registers a new data package using a data json info object
        //returns a boolean indicating the success
        registerNewDataPackage: function (dataInfoObject) {
            var retval = -1;

            try {
                if (dataInfoObject) {
                    var eventController = AssetManagement.customer.controller.EventController.getInstance();
                    retval = eventController.getNextEventId();

                    //generate the real data package object from info object
                    var dataPackage = this.buildDataPackageFromInfoObject(dataInfoObject);

                    var saveCallback = function (success) {
                        try {
                            eventController.fireEvent(retval, success === true);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerNewDataPackage of DataPackageManager', ex);
                            eventController.fireEvent(retval, false);
                        }
                    };

                    var saveEventId = this.saveDataPackage(dataPackage);

                    if (saveEventId > 0) {
                        eventController.registerOnEventForOneTime(saveEventId, saveCallback);
                    } else {
                        eventController.requestEventFiring(retval, false);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerNewDataPackage of DataPackageManager', ex);
                retval = -1;
            }

            return retval;
        },

        //generates an data packages array using all present data packages
        //on error UNDEFINED will be returned
        getUploadDataPackages: function () {
            var retval = -1;

            try {
                retval = this.getDataPackages();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataPackages of DataPackageManager', ex);
                retval = -1;
            }

            return retval;
        },

        //confirms all the data packages, whose guids are found inside the passed array
        //returns a boolean indicating the success
        confirmDataPackages: function (arrayOfGuidsToConfirm) {
            var retval = -1;

            try {
                retval = this.deleteDataPackagesByGuids(arrayOfGuidsToConfirm);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmDataPackages of DataPackageManager', ex);
            }

            return retval;
        },

        //private
        //allocates all present datapackages from database
        //returns an arry of data package objects
        getDataPackages: function () {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var dataPackages = [];

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                        if (cursor) {
                            dataPackages.push(cursor.value);
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            eventController.fireEvent(retval, dataPackages);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataPackages of DataPackageManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('S_DATAPACKAGE', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_DATAPACKAGE', keyRange, successCallback, null, false);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataPackages of DataPackageManager', ex);
                retval = -1;
            }

            return retval;
        },

        //saves a datapackage instance to database
        //returns a boolean indicating the success
        saveDataPackage: function (dataPackage) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!dataPackage || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(dataPackage['GUID'])) {
                    eventController.requestEventFiring(retval, false);
                    return retval;
                }

                var saveCallback = function (eventArgs) {
                    try {
                        var success = eventArgs && eventArgs.type === "success";
                        eventController.fireEvent(retval, success);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDataPackage of DataPackageManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                AssetManagement.customer.core.Core.getDataBaseHelper().put('S_DATAPACKAGE', dataPackage, saveCallback, saveCallback);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveDataPackage of DataPackageManager', ex);
                retval = -1;
            }

            return retval;
        },

        //deletes all datapackages, whose guids are found inside the passed array
        //returns a boolean indicating the success
        deleteDataPackagesByGuids: function (arrayOfGuids) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                if (!arrayOfGuids || arrayOfGuids.length < 1) {
                    eventController.requestEventFiring(retval, true);
                    return;
                }

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                        if (cursor) {
                            //delete the entry, if it is affected
                            if (Ext.Array.contains(arrayOfGuids, cursor.value['GUID'])) {
                                AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor);
                            }

                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        } else {
                            //done
                            eventController.fireEvent(retval, true);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDataPackagesByGuids of DataPackageManager', ex);
                        eventController.fireEvent(retval, false);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('S_DATAPACKAGE', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('S_DATAPACKAGE', keyRange, successCallback, null, false);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDataPackagesByGuids of DataPackageManager', ex);
                retval = -1;
            }

            return retval;
        },

        //builds a data package object from an data info object
        buildDataPackageFromInfoObject: function (dataInfoObject) {
            var retval = null;

            try {
                //perpare meta level of record
                var guid = AssetManagement.customer.manager.KeyManager.createGUID(true);
                var scenario = dataInfoObject.scenario;
                var mobileUser = dataInfoObject.userId.toUpperCase();
                var timeStamp = AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(dataInfoObject.timeStamp);

                var data = dataInfoObject.data;
                var uploadCount = data.length;

                var dataPackage = {};
                dataPackage['GUID'] = guid;
                dataPackage['SCENARIO'] = scenario;
                dataPackage['MOBILEUSER'] = mobileUser;
                dataPackage['TIMESTAMP'] = timeStamp;
                dataPackage['DATA'] = data;
                dataPackage['UPLOADCOUNT'] = uploadCount;

                //not used with indexedDB API
                dataPackage['COUNTER'] = '0';

                retval = dataPackage;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDataPackageFromInfoObject of DataPackageManager', ex);
            }

            return retval;
        }
    }
});