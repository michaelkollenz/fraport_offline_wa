﻿Ext.define('AssetManagement.base.sync.BaseUploadManager', {
    mixins: ['Ext.mixin.Observable'],

    requires: [
	    'Ext.util.MixedCollection',
	    'Ext.util.HashMap',
	    'AssetManagement.customer.helper.DbKeyRangeHelper',
	    'AssetManagement.customer.helper.CursorHelper',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.sync.DataPackageManager',
		'AssetManagement.customer.sync.ParsingManager',
		'AssetManagement.customer.helper.OxLogger'
    ],

    //public
    config: {
        linkedSyncManager: null,
        dataBaseHelper: null
    },

    //private:
    _appConfig: null,
    _cancelWorkRequested: false,
    _blockCancelation: false,
    _cancelationInProgress: false,
    _usersByScenario: null,
    _timeStampUp: null,
    _stores: null,
    _callback: null,
    _callbackScope: null,
    _dirtyIndexRange: null,

    constructor: function (config) {
        try {
            this.mixins.observable.constructor.call(this, config);

            this._appConfig = AssetManagement.customer.core.Core.getAppConfig();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseUploadManager', ex);
        }
    },

    //public methods
    requestWorkCancelation: function () {
        try {
            this._cancelWorkRequested = true;

            this.fireEvent('cancelationInProgress');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestWorkCancelation of BaseUploadManager', ex);
        }
    },

    //provides an upload JSON object
    // -PACKAGES as holding an array of BaseUpload packages
    // -REC_COUNT as total count of BaseUpload record
    //stores is an Ext.util.HashMap
    //methods starts a callback-sequence of private methods
    getUploadData: function (stores, callback, callbackScope) {
        try {
            if (!callback)
                return;

            this._blockCancelation = false;
            this._timeStampUp = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');
            this._stores = stores;
            this._usersByScenario = null;
            this._callback = callback;
            this._callbackScope = callbackScope;
            this.getDeltaUsers(true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadData of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //confirms upload data
    //methods starts a callback-sequence of private methods
    confirmUploadData: function (confGuids, callback, callbackScope) {
        try {
            this._callback = callback;
            this._callbackScope = callbackScope;
            this._blockCancelation = true;

            if (!confGuids || confGuids.length === 0) {
                Ext.defer(this.reportConfirmWorkComplete, 1, this);
            } else {
                Ext.defer(this.confirmDataPackages, 1, this, [confGuids]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadDataPackages of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //private methods
    //previous step: -
    //next step: getDeltaStores or getUploadDataForUsers or getUploadDataPackages
    getDeltaUsers: function (upload) {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }
            var me = this;
            me._usersByScenario = Ext.create('Ext.util.HashMap');

            var successCallback = function (eventArgs) {
                try {
                    var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                    if (cursor) {
                        var entry = cursor.value;
                        var userId = entry['USERID'];
                        var scenario = entry['SCENARIO'];

                        if (!me._usersByScenario.containsKey(userId)) {
                            if (upload === true && entry['UPLOAD'] === 'X') {
                                var userScenario = Ext.create('Ext.util.HashMap');
                                userScenario.add(scenario, scenario);
                                me._usersByScenario.add(userId, userScenario);
                            } else if ((!upload || upload === false) && entry['DOWNLOAD'] === 'X') {
                                var userScenario = Ext.create('Ext.util.HashMap');
                                userScenario.add(scenario, scenario);
                                me._usersByScenario.add(userId, userScenario);
                            }
                        } else if (!me._usersByScenario.get(userId).containsKey(scenario)) {
                            if (upload === true && entry['UPLOAD'] === 'X') {
                                me._usersByScenario.get(userId).add(scenario, scenario);

                            } else if ((!upload || upload === false) && entry['DOWNLOAD'] === 'X') {
                                var userScenario = Ext.create('Ext.util.HashMap');
                                me._usersByScenario.get(userId).add(scenario, scenario);
                            }
                        }

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    } else {
                        //check if no upload is required. If so, finish getUploadData by jumping to it's last step
                        if (me._usersByScenario.getCount() === 0)
                            me.getUploadDataPackages.call(me);
                        else if (me._stores === null || me._stores === undefined)
                            Ext.defer(me.getDeltaStores, 1, me, [upload, me.getUploadDataForUsers]);
                        else
                            Ext.defer(me.getUploadDataForUsers, 1, me);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaUsers of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var failureCallback = function (eventArgs) {
                try {
                    console.log("Error processing database request (getDeltaUsers): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                    me.errorOccured('Error building UploadStream');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaUsers of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            //build the keyRange
            //all users for the current mandt are required
            var mandt = AssetManagement.customer.core.Core.getAppConfig().getMandt();
            var oSI = this.getDataBaseHelper().getObjectStoreInformation('S_SYNCDELTA');

            var indexRange = IDBKeyRange.bound([mandt, '', '', 'X'], [mandt, oSI.getAttributeDetails('SCENARIO').getMaxValue(), oSI.getAttributeDetails('USERID').getMaxValue(), 'X']);
            var indexName = upload === true ? 'UPLOAD' : 'DOWNLOAD';

            this.getDataBaseHelper().queryUsingAnIndex('S_SYNCDELTA', indexName, indexRange, successCallback, failureCallback, false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaUsers of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: confirmUploadData or getDeltaUsers
    //next step: as passed (ususaly getUploadDataForUsers or confirmUploadStores)
    getDeltaStores: function (upload, nextStep) {
        try {
            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                if (!this._blockCancelation) {
                    this.cancelWork();
                    return;
                } else {
                    this._cancelationInProgress = true;
                    this.fireEvent('cancelationInProgress', 'Canceling...');
                }
            }

            var me = this;
            me._stores = Ext.create('Ext.util.HashMap');

            var successCallback = function (eventArgs) {
                try {
                    var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                    if (cursor) {
                        var record = cursor.value;
                        var storeName = record['STORENAME'];

                        if (!me._stores.containsKey(storeName)) {
                            if (upload === true && record['UPLOAD'] === 'X') {
                                me._stores.add(storeName, storeName);
                            } else if ((!upload || upload === false) && record['DOWNLOAD'] === 'X') {
                                me._stores.add(storeName, storeName);
                            }
                        }

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    } else {
                        Ext.defer(nextStep, 1, me);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaStores of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var failureCallback = function (eventArgs) {
                try {
                    console.log("Error processing database request (getDeltaStores): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                    me.errorOccured('Error building UploadStream');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaStores of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            //build the keyRange
            //all storesnames for the current mandt are required
            var mandt = AssetManagement.customer.core.Core.getAppConfig().getMandt();
            var oSI = this.getDataBaseHelper().getObjectStoreInformation('S_SYNCDELTA');

            var keyRange = IDBKeyRange.bound([mandt, '', '', ''], [mandt, oSI.getAttributeDetails('SCENARIO').getMaxValue(), oSI.getAttributeDetails('USERID').getMaxValue(), oSI.getAttributeDetails('STORENAME').getMaxValue()]);
            this.getDataBaseHelper().query('S_SYNCDELTA', keyRange, successCallback, failureCallback, false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaStores of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: getDeltaUsers or getDeltaStores
    //next step: getUploadDataForUser or registerNewUploadDataForPacketizing
    getUploadDataForUsers: function () {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            if (this._stores === null || this._stores === undefined) {
                Ext.defer(this.returnResults, 1, this);
                return;
            }

            var me = this;
            var users = this._usersByScenario.getKeys();
            var length = this._usersByScenario.length;
            var counter = 0;

            callback = function (success) {
                try {
                    // users = null means, work has been cancelled, callbacks may not be run
                    if (me._usersByScenario === null)
                        return;

                    if (success !== true) {
                        me.errorOccured();
                        return;
                    }

                    // if the last callback has been called proceed with registerNewUploadDataForPacketizing
                    if (++counter === length) {
                        Ext.defer(me.registerNewUploadDataForPacketizing, 1, me);
                    } else {
                        Ext.defer(me.getUploadDataForUser, 1, me, [users[counter], callback]);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForUsers of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            Ext.defer(me.getUploadDataForUser, 1, me, [users[counter], callback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForUsers of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: getUploadDataForUsers
    //next step: confirmPacketizedData
    registerNewUploadDataForPacketizing: function () {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            var me = this;

            this._blockCancelation = true;

            var timeStamp = this._timeStampUp;
            var errorOccured = false;
            var returned = false;
            var pendingRegistrations = 0;

            var afterSaveIterationFunction = function () {
                if (pendingRegistrations == 0 && !errorOccured) {
                    returned = true;
                    Ext.defer(me.confirmPacketizedData, 1, me);
                } else if (errorOccured && !returned) {
                    returned = true;
                    me.errorOccured();
                }
            };

            var registerCallback = function (success) {
                try {
                    pendingRegistrations--;

                    if (!success) {
                        success = false;
                    }

                    errorOccured &= !success;

                    afterSaveIterationFunction.call(me);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerNewUploadDataForPacketizing of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            this._usersByScenario.each(function (user, scenarios) {
                if (scenarios && scenarios.getCount() > 0) {
                    scenarios.each(function (scenario, uploadData) {
                        if (uploadData) {
                            var registrationEventId = AssetManagement.customer.sync.DataPackageManager.registerNewDataPackage(uploadData);

                            if (registrationEventId >= 0) {
                                pendingRegistrations++;

                                eventController.registerOnEventForOneTime(registrationEventId, registerCallback);
                            } else {
                                errorOccured = true;
                                afterSaveIterationFunction.call(this);
                            }
                        }
                    }, this);
                }
            }, this);

            if (pendingRegistrations === 0) {
                afterSaveIterationFunction.call(this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerNewUploadDataForPacketizing of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: registerNewUploadDataForPacketizing
    //next step: getUploadDataPackages
    confirmPacketizedData: function () {
        try {
            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                this._cancelationInProgress = true;
                this.fireEvent('cancelationInProgress');
            }

            var confirmationCallback = function () {
                try {
                    //clear the caches for moving data next - customizing can not have changed here
                    AssetManagement.customer.manager.CacheManager.clearCaches(true, false);
                    this._blockCancelation = false;

                    //now continue with getting the upload stream to use
                    Ext.defer(this.getUploadDataPackages, 1, this);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmPacketizedData of BaseUploadManager', ex);
                    this.errorOccured();
                }
            };

            //confirm all stores, for which data has been packetized (this._stores)
            this.confirmUploadStores(confirmationCallback, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmPacketizedData of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: getDeltaUsers or registerNewUploadDataForPacketizing
    //next step: returnResults
    getUploadDataPackages: function () {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            var me = this;

            var packetizedDataCallback = function (packetizedData) {
                try {
                    if (packetizedData === undefined) {
                        me.errorOccured();
                    } else {
                        Ext.defer(me.returnResults, 1, me, [packetizedData]);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataPackages of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var eventId = AssetManagement.customer.sync.DataPackageManager.getUploadDataPackages();

            if (eventId > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, packetizedDataCallback);
            } else {
                this.errorOccured();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataPackages of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: getUploadDataPackages
    //next step: -
    returnResults: function (packetizedData) {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            var retval = this.getUploadDataObject(packetizedData);

            this._callback.call(this._callbackScope || this.getLinkedSyncManager(), retval);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside returnResults of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //previous step: confirmUploadData
    //following step: reportConfirmWorkComplete
    confirmDataPackages: function (arrayOfDataPackageGuids) {
        try {
            //can not be cancelled
            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                this._cancelationInProgress = true;
                this.fireEvent('cancelationInProgress');
            }

            var me = this;
            var confirmCallback = function (success) {
                try {
                    Ext.defer(me.reportConfirmWorkComplete, 1, me);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmDataPackages of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var confirmId = AssetManagement.customer.sync.DataPackageManager.confirmDataPackages(arrayOfDataPackageGuids);

            if (confirmId > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(confirmId, confirmCallback);
            } else {
                this.errorOccured('An error occured confirming data packages.');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmDataPackages of BaseUploadManager', ex);
            this.errorOccured('An error occured confirming data packages.');
        }
    },

    // previous step: confirmUploadedData or confirmDataPackages
    // following step: -
    reportConfirmWorkComplete: function () {
        try {
            this._blockCancelation = false;

            // if a cancelation has been requested now it can be confirmed
            if (this._cancelWorkRequested) {
                me.cancelWork();
                return;
            }

            this.fireEvent('confirmUploadDataComplete');

            if (this._callback !== null && this._callback !== undefined)
                this._callback.call(this._callbackScope || this.getLinkedSyncManager());
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportConfirmWorkComplete of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    getUploadDataForUser: function (userId, callback) {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            var me = this;
            var usersScenarios = this._usersByScenario.get(userId);
            var scenariosArray = usersScenarios.getKeys();
            var length = scenariosArray.length;
            var counter = 0;

            var storeDataForUserAndScenarioFunction = function (dataForUserAndScenario) {
                if (dataForUserAndScenario) {
                    usersScenarios.replace(scenariosArray[counter], dataForUserAndScenario);
                } else {
                    usersScenarios.replace(scenariosArray[counter], '');
                }
            };

            var innerCallback = function (dataForUserAndScenario) {
                try {
                    storeDataForUserAndScenarioFunction(dataForUserAndScenario);
                    // if the last innerCallback has been called, call the outerCallback with results
                    if (++counter === length) {
                        Ext.defer(callback, 1, me, [true]);
                    } else {
                        Ext.defer(me.getUploadDataForUserAndScenario, 1, me, [userId, scenariosArray[counter], innerCallback]);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForUser of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            Ext.defer(me.getUploadDataForUserAndScenario, 1, me, [userId, scenariosArray[counter], innerCallback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForUser of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    getUploadDataForUserAndScenario: function (userId, scenario, callback) {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            var dataInfoObject = this.getDataInfoObject(userId, scenario);

            var stores = this._stores.getKeys();
            var length = this._stores.length;
            var counter = 0;

            var includeStoreDataFunction = function (storeData) {
                if (storeData) {
                    dataInfoObject['data'] = dataInfoObject['data'].concat(storeData);
                }
            };

            var me = this;

            var innerCallback = function (storeData) {
                try {
                    includeStoreDataFunction(storeData);

                    // if the last innerCallback has been called, call the outerCallback with results
                    if (++counter === length) {
                        Ext.defer(callback, 1, me, [dataInfoObject]);
                    } else {
                        Ext.defer(me.getUploadDataForStoreOfUserAndScenario, 1, me, [userId, scenario, stores[counter], innerCallback]);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForUserAndScenario of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            Ext.defer(me.getUploadDataForStoreOfUserAndScenario, 1, me, [userId, scenario, stores[counter], innerCallback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForUserAndScenario of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    getUploadDataForStoreOfUserAndScenario: function (userId, scenario, storeName, callback) {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            var uploadDataForStore = [];
            var objectStoreInfo;
            var uploadAttributes;
            var deleteAttributes;
            objectStoreInfo = this.getDataBaseHelper().getObjectStoreInformation(storeName);

            if (!objectStoreInfo) {
                console.log("Unable to get upload data for " + storeName + ". There is no store info");
                callback.call(this, retval);
                return;
            }

            uploadAttributes = objectStoreInfo.getUploadAttributes();

            //if a store does not use a update flag it cannot be considered for upload
            if (!Ext.Array.contains(uploadAttributes, 'UPDFLAG')) {
                console.log("Stop generating of BaseUpload data for " + storeName + ". This store has no update flag field");
                callback.call(this, retval);
                return;
            }

            deleteAttributes = objectStoreInfo.getSyncDeleteAttributes();

            var cursorEnded = false;
            var totalComparisons = 0;
            var completeComparisons = 0;

            var includeRecordFunction = function (recordToInclude) {
                if (recordToInclude) {
                    uploadDataForStore.push(recordToInclude);
                }
            };

            var me = this;

            var successCallback = function (eventArgs) {
                try {
                    if (me._cancelWorkRequested) {
                        me.cancelWork();
                        return;
                    }

                    var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                    if (cursor) {
                        var record = cursor.value;

                        if (record) {
                            var updFlag = record.UPDFLAG;

                            //determine if delta check is needed
                            if (updFlag === 'U') {
                                var innerCallback = function (deltaResult) {
                                    includeRecordFunction(deltaResult);

                                    if (++completeComparisons === totalComparisons && cursorEnded) {
                                        callback.call(me, uploadDataForStore);
                                    }
                                };

                                totalComparisons++;

                                var storeReference = eventArgs.target.source.objectStore;
                                me.getDeltaUploadRecord.call(me, storeReference, objectStoreInfo, record, innerCallback);
                            } else if (updFlag === 'D') {
                                //if the entry is flagged for delete, only the key fields and the updateflag itself have to be set
                                includeRecordFunction(me.buildUploadRecord.call(me, objectStoreInfo, record, deleteAttributes));
                            } else if (updFlag === 'I') {
                                includeRecordFunction(me.buildUploadRecord.call(me, objectStoreInfo, record, uploadAttributes));
                            } else if (updFlag === 'A' || updFlag === 'C' || updFlag === 'R') {
                                includeRecordFunction(me.buildUploadRecord.call(me, objectStoreInfo, record, uploadAttributes));
                            }
                        }

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    } else {
                        //no more entries available
                        cursorEnded = true;

                        if (totalComparisons === completeComparisons)
                            callback.call(me, uploadDataForStore);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForStoreOfUserAndScenario of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var failureCallback = function (eventArgs) {
                try {
                    console.log("Error processing database request (getUploadDataForStoreOfUserAndScenario): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                    me.errorOccured('Error building upload data');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForStoreOfUserAndScenario of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            //request only datasets with an updateflag between A and U (ommits case X)
            var indexMapLower = Ext.create('Ext.util.HashMap');
            indexMapLower.add('USERID', userId);
            indexMapLower.add('SCENARIO', scenario);
            indexMapLower.add('UPDFLAG', 'A');

            var indexMapUpper = Ext.create('Ext.util.HashMap');
            indexMapUpper.add('USERID', userId);
            indexMapUpper.add('SCENARIO', scenario);
            indexMapUpper.add('UPDFLAG', 'U');

            var indexRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbIndexRange(storeName, 'UPDFLAG', indexMapLower, indexMapUpper);
            this.getDataBaseHelper().rawQueryUsingAnIndex(storeName, 'UPDFLAG', indexRange, successCallback, null, false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataForStoreOfUserAndScenario of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //generates an delta upload record by comparison to the dirty record, if existing
    getDeltaUploadRecord: function (databaseStore, storeInfo, object, callback) {
        try {
            var me = this;
            var isRelevant = false;
            var uploadAttributes = storeInfo.getUploadAttributes();
            var keyAttributes = storeInfo.get('keyAttributes');

            var storeName = storeInfo.get('storeName');

            var successCallback = function (eventArgs) {
                try {
                    var deltaRecord = null;
                    var dirtyRecord = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                    //check, if dirty record existing at all
                    if (dirtyRecord) {
                        var dataObject = {};

                        var formerValue;
                        var currentValue;
                        var curAttribute;

                        for (var i = 0; i < uploadAttributes.length; i++) {
                            curAttribute = uploadAttributes[i];
                            currentValue = object[curAttribute];
                            formerValue = dirtyRecord[curAttribute];

                            if (currentValue === undefined || currentValue === null)
                                currentValue = '';

                            if (Ext.Array.contains(keyAttributes, curAttribute)) {
                                dataObject[curAttribute] = currentValue;
                            } else if (currentValue !== formerValue) {
                                dataObject[curAttribute] = currentValue;

                                if (!('UPDFLAG' === curAttribute))
                                    isRelevant = true;
                            } else if (curAttribute === 'MOBILEKEY' || curAttribute === 'CHILDKEY' || curAttribute === 'CHILDKEY2') {
                                dataObject[curAttribute] = currentValue;
                            }
                        }

                        if (isRelevant) {
                            deltaRecord = me.getUploadRecord(storeName.substring(2), 'U', dataObject);
                        }
                    } else {
                        deltaRecord = me.buildUploadRecord.call(me, storeInfo, object, uploadAttributes);
                    }

                    callback.call(me, deltaRecord);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaUploadRecord of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var failureCallback = function (eventArgs) {
                try {
                    console.log("Error processing database request (compareToDirtyRow): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                    me.errorOccured('Error building upload data');
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaUploadRecord of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var dirtyKeyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject(storeName, object, true);
            var request = databaseStore.get(dirtyKeyRange.lower);

            request.onsuccess = successCallback;
            request.onerror = failureCallback;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeltaUploadRecord of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    buildUploadRecord: function (objectStoreInfo, object, relevantAttributes) {
        var retval = null;

        try {
            var storeName = objectStoreInfo.get('storeName');
            var recordUpdFlag = object['UPDFLAG'];
            var dataObject = {};

            //only transform counter values of mobile created records, if the record is ready to be posted to SAP
            var requiresCounterTransform = recordUpdFlag === 'I';

            Ext.Array.each(relevantAttributes, function (attribute, i) {
                var currentValue = object[attribute];

                if (requiresCounterTransform) {
                    if (objectStoreInfo.getAttributeDetails(attribute).get('isKey')) {
                        if (AssetManagement.customer.utils.StringUtils.startsWith(currentValue, '%')) {
                            currentValue = currentValue.replace(currentValue.charAt(0), "0");
                        }
                    }
                }

                //do not send undefined or null values
                if (currentValue !== null || currentValue !== undefined) {
                    dataObject[attribute] = currentValue;
                }
            }, this);

            retval = this.getUploadRecord(storeName.substring(2), recordUpdFlag, dataObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUploadRecord of BaseUploadManager', ex);
        }

        return retval;
    },

    cancelWork: function () {
        try {
            this._cancelWorkRequested = false;
            this._cancelationInProgress = false;
            this._blockCancelation = false;
            this._callback = null;
            this._callbackScope = null;
            this._usersByScenario = null;
            this._stores = null;
            this._timeStampUp = null;
            this.fireEvent('workCancelled');
            return;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseUploadManager', ex);
        }
    },

    errorOccured: function (message) {
        try {
            this._cancelWorkRequested = false;
            this._cancelationInProgress = false;
            this._blockCancelation = false;
            this._callback = null;
            this._usersByScenario = null;
            this._stores = null;
            this._timeStampUp = null;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = 'A general upload error occured';

            this.fireEvent('errorOccured', message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccured of BaseUploadManager', ex);
        }
    },

    // will confirm every entry in uploaded marked stores
    // can not be cancelled!
    confirmUploadStores: function (onDoneCallback, onDoneCallbackScope) {
        try {
            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                this._cancelationInProgress = true;
                this.fireEvent('cancelationInProgress');
            }

            if (this._stores === null || this._stores === undefined) {
                if (onDoneCallback)
                    Ext.defer(onDoneCallback, 1, onDoneCallbackScope);

                return;
            }

            var me = this;

            var stores = this._stores.getValues();
            var length = stores.length;
            var counter = 0;

            if (length === 0) {
                if (onDoneCallback)
                    Ext.defer(onDoneCallback, 1, onDoneCallbackScope);

                return;
            }

            var callback = function () {
                try {
                    innerCallback = function () {
                        try {
                            // stores = null means, work has been already cancelled, callbacks may not be run
                            if (me._stores === null)
                                return;

                            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                                me._cancelationInProgress = true;
                                me.fireEvent('cancelationInProgress');
                            }

                            // if the last callback has been called proceed with calling the passed done callback
                            if (++counter === length && onDoneCallback)
                                Ext.defer(onDoneCallback, 1, onDoneCallbackScope);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadStores of BaseUploadManager', ex);
                            me.errorOccured();
                        }
                    };

                    for (var i = 0; i < length; i++) {
                        me.confirmUploadStore(stores[i], innerCallback);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadStores of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            //first set back the FLAG for UPLOAD
            this.resetUploadedStoresOnSyncDelta(stores, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadStores of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    resetUploadedStoresOnSyncDelta: function (stores, callback) {
        try {
            var mandt = AssetManagement.customer.core.Core.getAppConfig().getMandt();
            var oSI = this.getDataBaseHelper().getObjectStoreInformation('S_SYNCDELTA');

            var me = this;

            var updateCallback = function (success, cursor) {
                try {
                    if (success) {
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    } else {
                        me.errorOccured('Error resetting sync delta upload flags');
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetUploadedStoresOnSyncDelta of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var successCallback = function (eventArgs) {
                if (eventArgs && eventArgs.target && eventArgs.target.result) {
                    var cursor = eventArgs.target.result;
                    var entry = cursor.value;

                    Ext.Array.each(stores, function (store) {
                        if (store === entry['STORENAME']) {
                            entry['UPLOAD'] = '';
                            return false;
                        }
                    }, this);

                    AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, entry, updateCallback);
                } else if (callback) {
                    callback();
                }
            };

            var failureCallback = function (eventArgs) {
                console.log("Error processing a database request (resetUploadedStoresOnSyncDelta): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                me.errorOccured('Error resetting sync delta upload flags');
            };

            var indexRange = IDBKeyRange.bound([mandt, '', '', 'X'], [mandt, oSI.getAttributeDetails('SCENARIO').getMaxValue(), oSI.getAttributeDetails('USERID').getMaxValue(), 'X']);
            this.getDataBaseHelper().queryUsingAnIndex('S_SYNCDELTA', 'UPLOAD', indexRange, successCallback, failureCallback, false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetUploadedStoresOnSyncDelta of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    //cleans a store for a given mandt
    //delete entries with updflag D or DIRTY flag X and change those with updflag U or I to X ones
    confirmUploadStore: function (storeName, callback) {
        try {
            var me = this;

            var innerCallback = function () {
                try {
                    //do a defer to let the database transaction to fade out
                    Ext.defer(me.deleteDirtyFlagAffectedDataRecordsOfStore, 1, me, [storeName, callback]);
                } catch (ex) {
                    me.errorOccured();
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadStore of BaseUploadManager', ex);
                }
            };

            Ext.defer(this.confirmUpdateFlagAffectedDataRecordsOfStore, 1, this, [storeName, innerCallback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadStore of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    confirmUpdateFlagAffectedDataRecordsOfStore: function (storeName, callback) {
        try {
            var dataBaseHelper = this._dataBaseHelper;
            var storeInfo = dataBaseHelper.getObjectStoreInformation(storeName);

            if (!storeInfo.hasAttribute('UPDFLAG')) {
                if (callback) {
                    callback();
                }
            }

            var me = this;

            var delUpdateCallback = function (success, cursor) {
                try {
                    if (success) {
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    } else {
                        me.errorOccured('Error confirming data of store ' + storeName);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUpdateFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var successCallback = function (eventArgs) {
                try {
                    var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                    if (cursor) {
                        var record = cursor.value;

                        if (record['UPDFLAG'] === 'D')
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor, delUpdateCallback);
                        else if (record['UPDFLAG'] === 'I' || record['UPDFLAG'] === 'U') {
                            record['UPDFLAG'] = 'X';
                            AssetManagement.customer.helper.CursorHelper.doCursorUpdate(cursor, record, delUpdateCallback);
                        } else {
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        }
                    } else if (callback) {
                        callback();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUpdateFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var failureCallback = function (eventArgs) {
                try {
                    console.log("Error processing a database request (confirmUploadStore): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                    me.errorOccured('Error confirming data of store ' + storeName);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUpdateFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            //DBKeyRangeHelper may not be used, since it will set MANDT, SCENARIO, USERID and DIRTY keys
            var indexRange = me.getDbUpdateFlagIndexRangeForMandtFixOnly(storeInfo);
            dataBaseHelper.queryUsingAnIndex(storeName, 'UPDFLAG', indexRange, successCallback, failureCallback, false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUpdateFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    deleteDirtyFlagAffectedDataRecordsOfStore: function (storeName, callback) {
        try {
            var dataBaseHelper = this._dataBaseHelper;
            var storeInfo = dataBaseHelper.getObjectStoreInformation(storeName);

            if (!storeInfo.hasAttribute('DIRTY')) {
                if (callback) {
                    callback();
                }
            }

            var me = this;

            var deleteCallback = function (success, cursor) {
                try {
                    if (success) {
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    } else {
                        me.errorOccured('Error deleting dirty data of store ' + storeName);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUpdateFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var successCallback = function (eventArgs) {
                try {
                    var cursor = eventArgs && eventArgs.target ? eventArgs.target.result : null;

                    if (cursor) {
                        var record = cursor.value;

                        if (record['DIRTY'] === 'X') {
                            AssetManagement.customer.helper.CursorHelper.doCursorDelete(cursor, deleteCallback);
                        } else {
                            AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                        }
                    } else if (callback) {
                        callback();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDirtyFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var failureCallback = function (eventArgs) {
                try {
                    console.log("Error processing a database request (confirmUploadStore): " + eventArgs.message + ' (Code ' + eventArgs.code + ')');
                    me.errorOccured('Error deleting dirty data of store ' + storeName);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDirtyFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
                    me.errorOccured();
                }
            };

            var indexRange = me.getDirtyIndexRange.call(me);
            dataBaseHelper.queryUsingAnIndex(storeName, 'DIRTY', indexRange, successCallback, failureCallback, false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDirtyFlagAffectedDataRecordsOfStore of BaseUploadManager', ex);
            this.errorOccured();
        }
    },

    getDbUpdateFlagIndexRangeForMandtFixOnly: function (storeInfo) {
        var retval = null;

        try {
            var lowerBound = [];
            var upperBound = [];
            var attrIndex = 0;

            var index = storeInfo.getIndex('UPDFLAG');
            allIndexAttributes = index.attributes;

            Ext.Array.each(allIndexAttributes, function (attrName) {
                if ('MANDT' === attrName) {
                    lowerBound[attrIndex] = AssetManagement.customer.core.Core.getAppConfig().getMandt();
                    upperBound[attrIndex] = lowerBound[attrIndex];
                } else if ('SCENARIO' === attrName) {
                    lowerBound[attrIndex] = '';
                    upperBound[attrIndex] = storeInfo.getAttributeDetails('SCENARIO').getMaxValue();
                } else if ('DIRTY' === attrName) {
                    lowerBound[attrIndex] = '';
                    upperBound[attrIndex] = lowerBound[attrIndex];
                } else if ('UPDFLAG' === attrName) {
                    lowerBound[attrIndex] = 'D';
                    upperBound[attrIndex] = 'U';
                } else {
                    lowerBound[attrIndex] = '';
                    upperBound[attrIndex] = storeInfo.getAttributeDetails(attrName).getMaxValue();
                }

                attrIndex++;
            }, this);

            retval = IDBKeyRange.bound(lowerBound, upperBound);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDbUpdateFlagIndexRangeForMandtFixOnly of BaseUploadManager', ex);
        }

        return retval;
    },

    getDataInfoObject: function (userId, scenario) {
        var retval = null;

        try {
            retval = {
                mandt: AssetManagement.customer.core.Core.getAppConfig().getMandt(),
                scenario: scenario,
                userId: userId.toUpperCase(),
                timeStamp: this._timeStampUp,
                data: []
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataInfoObject of BaseUploadManager', ex);
        }

        return retval;
    },

    getUploadRecord: function (recordName, updateFlag, dataObject) {
        var retval = null;

        try {
            retval = {
                RECORDNAME: recordName,
                UPDFLAG: updateFlag,
                //the data itself needs to be an escaped json (backend requirement)
                JSON_DATA: dataObject
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadRecord of BaseUploadManager', ex);
        }

        return retval;
    },

    getUploadDataObject: function (uploadPackages) {
        var retval = null;

        try {
            var totalUploadRecordsCount = 0;

            if (uploadPackages && uploadPackages.length > 0) {
                Ext.Array.each(uploadPackages, function (uploadPackage) {
                    totalUploadRecordsCount += uploadPackage['UPLOADCOUNT'];
                }, this);
            }

            retval = {
                PACKAGES: uploadPackages,
                REC_COUNT: totalUploadRecordsCount + ''
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadDataObject of BaseUploadManager', ex);
        }

        return retval;
    },

    getDirtyIndexRange: function () {
        try {
            if (!this._dirtyIndexRange) {
                var mandt = AssetManagement.customer.core.Core.getAppConfig().getMandt();

                var bound = [mandt, 'X'];
                this._dirtyIndexRange = IDBKeyRange.bound(bound, bound)
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDirtyIndexRange of BaseUploadManager', ex);
        }

        return this._dirtyIndexRange;
    }
});