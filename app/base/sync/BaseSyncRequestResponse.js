﻿Ext.define('AssetManagement.base.sync.BaseSyncRequestResponse', {
    extend: 'AssetManagement.base.sync.BaseRequestResponse',

    requires: [
		'Ext.Array'
    ],

    //private:	
    constructor: function (config) {
        this.callParent(arguments);
    },

    getSyncData: function () {
        var retval = null;

        try {
            retval = this.getFullResponseJSON().SYNC_DATA;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncData of BaseSyncRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the sync timestamp
    getSyncTimeStamp: function () {
        var retval = null;

        try {
            retval = this.getFullResponseJSON().SYNC_TIMESTAMP;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncTimeStamp of BaseSyncRequestResponse', ex);
        }

        return retval;
    },

    //public
    getSyncGUID: function () {
        var retval = '';

        try {
            retval = this.getFullResponseJSON().SYNC_GUID;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncGUID of BaseSyncRequestResponse', ex);
        }

        return retval;
    },

    //public
    getConfData: function () {
        var retval = null;

        try {
            retval = this.getFullResponseJSON().CONF_DATA;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getConfData of BaseSyncRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the conf guids relevant for the device
    getConfGUIDs: function () {
        var retval = null;

        try {
            var confGuids = new Array();
            var confData = this.getConfData();

            if (confData && confData.length > 0) {
                var deviceId = AssetManagement.customer.core.Core.getAppConfig().getDeviceId();

                Ext.Array.each(confData, function (confObject) {
                    if (confObject['DEVICE_ID'] === deviceId)
                        confGuids.push(confObject['GUID']);
                }, this);
            }

            retval = confGuids;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getConfGUIDs of BaseSyncRequestResponse', ex);
        }

        return retval;
    },

    //public
    getClientReset: function () {
        var retval = false;

        try {
            retval = this.getFullResponseJSON().RESETCLIENT === 'X';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClientReset of BaseSyncRequestResponse', ex);
        }

        return retval;
    },

    //public
    getSyncDataRecordCount: function () {
        var retval = 0;

        try {
            var syncData = this.getSyncData();

            if (syncData)
                retval = syncData.length;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncDataRecordCount of BaseSyncRequestResponse', ex);
        }

        return retval;
    }
});