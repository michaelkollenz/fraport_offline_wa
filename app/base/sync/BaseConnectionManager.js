Ext.define('AssetManagement.base.sync.BaseConnectionManager', {
    mixins: ['Ext.mixin.Observable'],

    requires: [
	    'Ext.util.MixedCollection',
	    'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.sync.UserLoginInfo',
		'AssetManagement.customer.sync.UserChangePasswordResult',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        REQUEST_TYPES: {
            DATASYNC: 0,
            FILEUPLOAD: 1,
            INFO: 2,
            CHANGEPW: 3,
            ONLINEREQ: 4,
            DEVICECHECK: 5
        },

        FAILURE_TYPES: {
            OFFLINE: 0,
            TIMEOUT: 1,
            UNKNOWN: 2
        }
    },

    //public
    config: {
        linkedSyncManager: null
    },

    //private:
    _cancelNetworkActionRequested: false,

    constructor: function (config) {
        try {
            this.mixins.observable.constructor.call(this, config);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseConnectionManager', ex);
        }
    },

    //public methods
    requestNetworkActionCancelation: function () {
        try {
            this._cancelNetworkActionRequested = true;

            this.fireEvent('cancelationInProgress');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestNetworkActionCancelation of BaseConnectionManager', ex);
        }
    },

    errorOccured: function (message) {
        try {
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = 'A general network error occured';

            this._cancelNetworkActionRequested = false;
            this.fireEvent('errorOccured', message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccured of BaseConnectionManager', ex);
        }
    },

    //will try to contact the sap system and return a boolean, if it was possible
    tryContactSapSystem: function (callback, callbackScope) {
        try {
            if (!callback)
                return;

            AssetManagement.customer.helper.OxLogger.logSyncMessage('A sap system connectivity check has been triggerd.');

            //register events on error events to also track error cases
            var me = this;

            var failureCallback = function () {
                try {
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('A sap system connectivity check returned.');
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('System could not be reached.');

                    callback.call(callbackScope ? callbackScope : this, false);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryContactSapSystem of BaseConnectionManager', ex);
                    callback.call(callbackScope ? callbackScope : this, false);
                } finally {
                    me.removeListener('sendFailed', failureCallback, this);
                    me.removeListener('errorOccured', failureCallback, this);
                }
            };

            this.addListener('sendFailed', failureCallback, this);
            this.addListener('errorOccured', failureCallback, this);

            //the check calls the user-details api
            var userDetailsCallback = function (responseAsString) {
                try {
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('A sap system connectivity check returned.');

                    var retval = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(responseAsString);

                    if (retval) {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('Got a response.');
                    } else {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('Got an empty response.');
                    }

                    callback.call(callbackScope ? callbackScope : this, retval);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryContactSapSystem of BaseConnectionManager', ex);
                    callback.call(callbackScope ? callbackScope : this, false);
                } finally {
                    me.removeListener('sendFailed', failureCallback, this);
                    me.removeListener('errorOccured', failureCallback, this);
                }
            };

            var ac = AssetManagement.customer.core.Core.getAppConfig();
            this.getUserInfo(ac.getMandt(), ac.getUserId(), ac.getUserPassword(), null, userDetailsCallback, true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryContactSapSystem of BaseConnectionManager', ex);
            callback.call(callbackScope ? callbackScope : this, false);
        }
    },

    sendToBackEndBasic: function (requestType, postValues, callback) {
        try {
            var me = this;

            var onlineCallback = function (isOnline) {
                try {
                    if (!isOnline) {
                        me.fireEvent('sendFailed', me.self.FAILURE_TYPES.OFFLINE);
                    } else {
                        var httpRequest = new XMLHttpRequest();
                        var url = me.buildUrl(requestType);

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(url)) {
                            me.errorOccured();
                            return;
                        }

                        me.enhancePostValues(requestType, postValues);

                        var postParamsAsString = me.buildPostString(postValues);
                        httpRequest.open("POST", url, true, "", "");

                        //send the proper header information along with the request
                        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                        var intervalID = 0;

                        httpRequest.onreadystatechange = function () {
                            if (httpRequest.readyState == 4) {
                                window.clearInterval(intervalID);
                                //results go here
                                if (callback)
                                    callback.call(me, httpRequest.responseText);
                            }
                        }

                        var abortFunction = function () {
                            if (this._cancelNetworkActionRequested) {
                                this.fireEvent('networkActionCancelled', null);
                                httpRequest.abort();
                                httpRequest.onreadystatechange = null;
                                this._cancelNetworkActionRequested = false;
                                window.clearInterval(intervalID);
                                return;
                            }
                        };

                        intervalID = setInterval(abortFunction, 250);
                        // this call is asynchronous, therefore UI isn't blocked until the response will return
                        httpRequest.send(postParamsAsString);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEndBasic of BaseConnectionManager', ex);
                    me.errorOccured();
                }
            };

            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEndBasic of BaseConnectionManager', ex);
            this.errorOccured();
        }
    },

    sendToBackEnd: function (requestType, postValues, callback) {
        try {
            var me = this;

            var onlineCallback = function (isOnline) {
                try {
                    if (!isOnline) {
                        me.fireEvent('sendFailed', me.self.FAILURE_TYPES.OFFLINE);
                    } else {
                        var url = me.buildUrl(requestType);
                        var timeOutValue = me.getTimeoutValue(requestType);

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(url)) {
                            me.errorOccured();
                            return;
                        }

                        me.enhancePostValues(requestType, postValues);

                        var postParamsAsString = me.buildPostString(postValues);

                        var httpRequest;
    
                        var abortFunction = function () {
                            if (me._cancelNetworkActionRequested) {
                                Ext.Ajax.abort(httpRequest);
                                me.fireEvent('networkActionCancelled', null);
                                me._cancelNetworkActionRequested = false;
                                window.clearInterval(intervalID);
                                return;
                            }
                        };

                        var intervalID = window.setInterval(abortFunction, 250);

                        //this call is asynchronous, therefore UI isn't blocked until the response will return	
                        httpRequest = Ext.Ajax.request({
                            method: "POST",
                            url: url,
                            timeout: timeOutValue,
                            scope: me,
                            withCredentials: false,				//sometimes errors occur
                            useDefaultXhrHeader: false,			//try set second boolean true to fix...

                            headers: {
                                'Accept': 'application/json'
                                //do not use content type for json, because this will disable the form field conversion from the params (string)
                                //this would result in all existing calls to be adjusted to work with a complete json object as params
                                //'Content-Type': 'application/json'
                            },

                            params: postParamsAsString,

                            success: function (response, options) {
                                try {
                                    window.clearInterval(intervalID);
                                    if (callback !== undefined && callback !== null)
                                        callback.call(this, response.responseText);
                                } catch (ex) {
                                    me.errorOccured();
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEnd of BaseConnectionManager', ex);
                                }
                            },

                            failure: function (response, options) {
                                try {
                                    if (!response.aborted) {
                                        AssetManagement.customer.helper.NetworkHelper.logNetworkFailure(response);

                                        var failureType = this.self.FAILURE_TYPES.UNKNOWN;

                                        if (!navigator.onLine)
                                            failureType = this.self.FAILURE_TYPES.OFFLINE;

                                        if (response.timedout) {
                                            failureType = this.self.FAILURE_TYPES.TIMEOUT;
                                        }

                                        me.fireEvent('sendFailed', failureType, response.responseText);
                                    }
                                } catch (ex) {
                                    me.errorOccured(response.responseText);
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEnd of BaseConnectionManager', ex);
                                }
                            },

                            callback: function (options, success, response) {
                                window.clearInterval(intervalID);
                            }
                        });
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEnd of BaseConnectionManager', ex);
                    me.errorOccured();
                }
            };

            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);
        } catch (ex) {
            this.errorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEnd of BaseConnectionManager', ex);
        }
    },

    //extends post values by additional information
    enhancePostValues: function (requestType, postValues) {
        try {
            var ac = AssetManagement.customer.core.Core.getAppConfig();

            //add general application information
            postValues.add("pa_client_os", ac.getOS());
            postValues.add("pa_appl", ac.getAppl());
            postValues.add("pa_version", ac.getVersion());
            postValues.add("pa_device_id", ac.getDeviceId());

            //add the current scenario, if required
            if (requestType === this.self.REQUEST_TYPES.DATASYNC
                || requestType === this.self.REQUEST_TYPES.FILEUPLOAD
                    || requestType === this.self.REQUEST_TYPES.ONLINEREQ
                        || requestType === this.self.REQUEST_TYPES.DEVICECHECK) {
                postValues.add('SCENARIO', ac.getScenario());
            }
        } catch (ex) {
            this.errorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside enhancePostValues of BaseConnectionManager', ex);
        }
    },

    sendFileToBackend: function (postValues, binaryConent, boundary, callback) {
        try {
            var me = this;

            var onlineCallback = function (isOnline) {
                try {
                    if (!isOnline) {
                        me.fireEvent('sendFailed', me.self.FAILURE_TYPES.OFFLINE);
                        return;
                    } else {
                        var requestType = me.self.REQUEST_TYPES.FILEUPLOAD;

                        var url = me.buildUrl(requestType);
                        var timeOutValue = me.getTimeoutValue(requestType);

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(url)) {
                            me.errorOccured();
                            return;
                        }

                        //extending the PostValues with the scenario
                        me.enhancePostValues(requestType, postValues);

                        var postParamsAsString = me.buildPostString(postValues);

                        var httpRequest;
                        var abortFunction = function () {
                            if (me._cancelNetworkActionRequested) {
                                Ext.Ajax.abort(httpRequest);
                                me.fireEvent('networkActionCancelled', null);
                                me._cancelNetworkActionRequested = false;
                                window.clearInterval(intervalID);
                                return;
                            }
                        };

                        var intervalID = window.setInterval(abortFunction, 250);

                        //there is an additional header to set, to define, where the send file contents begin inside of the messages content
                        var headers = { 'Content-Type': 'multipart/form-data; boundary=' + boundary };

                        // this call is asynchronous, therefore UI isn't blocked until the response will return
                        httpRequest = Ext.Ajax.request({
                            method: "POST",
                            url: url,
                            timeout: timeOutValue,
                            disableCaching: true,
                            scope: me,
                            withCredentials: false,				//sometimes errors occur
                            useDefaultXhrHeader: false,			//try set second boolean true to fix...

                            params: postParamsAsString,

                            headers: headers,
                            binary: true,						//additional config for sending a file
                            binaryData: binaryConent,			//additional config for sending a file

                            success: function (response, options) {
                                try {
                                    window.clearInterval(intervalID);
                                    if (callback) {
                                        //the response is interpreted as bytes from ExtJS
                                        //it has to be transformed to text again
                                        callback.call(this, true, AssetManagement.customer.utils.StringUtils.getStringFromBytes(response.responseBytes));
                                    }
                                } catch (ex) {
                                    me.errorOccured();
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendFileToBackend of BaseConnectionManager', ex);
                                }
                            },

                            failure: function (response, options) {
                                try {
                                    if (!response.aborted) {
                                        AssetManagement.customer.helper.NetworkHelper.logNetworkFailure(response);

                                        var failureType = this.self.FAILURE_TYPES.UNKNOWN;

                                        if (!navigator.onLine)
                                            failureType = this.self.FAILURE_TYPES.OFFLINE;

                                        if (response.timedout) {
                                            failureType = this.self.FAILURE_TYPES.TIMEOUT;
                                        }

                                        //the response is interpreted as bytes from ExtJS
                                        //it has to be transformed to text again
                                        this.fireEvent('sendFailed', failureType, AssetManagement.customer.utils.StringUtils.getStringFromBytes(response.responseBytes));
                                    }
                                } catch (ex) {
                                    me.errorOccured();
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendFileToBackend of BaseConnectionManager', ex);
                                }
                            },

                            callback: function (options, success, response) {
                                window.clearInterval(intervalID);
                            }
                        });
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendToBackEndBasic of BaseConnectionManager', ex);
                    me.errorOccured();
                }
            };

            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);

        } catch (ex) {
            this.errorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendFileToBackend of BaseConnectionManager', ex);
        }
    },

    buildPostString: function (postValuesCollection) {
        var retval = '';

        try {
            if (postValuesCollection) {
                postValuesCollection.eachKey(function (key, item) {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(item)) {
                        //check for parameter upstream, which may already be in the parameter value format - check for '&'
                        if (key.toLowerCase() === 'upstream' && item[0] === '&') {
                            //it is, so just append it (another encoding is not possible, because it would remove the parameters '&' indicators)
                            retval += item;
                        } else {
                            retval += '&' + (encodeURIComponent(key) + '=' + encodeURIComponent(item));
                        }
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildPostString of BaseConnectionManager', ex);
        }

        return retval;
    },

    //builds the URL for the SAP System to access depending on the requestType
    buildUrl: function (requestType) {
        var retval = '';

        try {
            var ac = AssetManagement.customer.core.Core.getAppConfig();
            var service = '';
            var resource = '';

            var syncAlias = ac.getGatewayAliasSync();
            var infoAlias = ac.getGatewayAliasInfo();

            switch (requestType) {
                case this.self.REQUEST_TYPES.DATASYNC:
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAlias)) {
                        service = ac.getGatewayServiceSync();
                    } else {
                        service = syncAlias;
                    }


                    resource = ac.getGatewayControllerSync();
                    break;

                case this.self.REQUEST_TYPES.FILEUPLOAD:
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAlias)) {
                        service = ac.getGatewayServiceSync();
                    } else {
                        service = syncAlias;
                    }

                    resource = ac.getGatewayControllerFileUpload();
                    break;

                case this.self.REQUEST_TYPES.INFO:
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infoAlias)) {
                        service = ac.getGatewayServiceInfo();
                    } else {
                        service = infoAlias;
                    }

                    resource = ac.getGatewayControllerInfo();
                    break;

                case this.self.REQUEST_TYPES.CHANGEPW:
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infoAlias)) {
                        service = ac.getGatewayServiceInfo();
                    } else {
                        service = infoAlias;
                    }

                    resource = ac.getGatewayControllerChangePw();
                    break;

                case this.self.REQUEST_TYPES.ONLINEREQ:
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAlias)) {
                        service = ac.getGatewayServiceSync();
                    } else {
                        service = syncAlias;
                    }
                    resource = ac.getGatewayControllerOnlineRequest();
                    break;

                case this.self.REQUEST_TYPES.DEVICECHECK:
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAlias)) {
                        service = ac.getGatewayServiceSync();
                    } else {
                        service = syncAlias;
                    }

                    resource = ac.getGatewayControllerDeviceCheck();
                    break;

            }

            if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncAlias) && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infoAlias)) {
                retval = ac.getSyncProtocol() + "://" + ac.getSyncGateway() + ":" + ac.getSyncGatewayPort()
                    + "/sap" + ac.getSyncGatewayIdent() + service + resource;
            } else {
                retval = ac.getSyncProtocol() + "://" + ac.getSyncGateway() + ":" + ac.getSyncGatewayPort()
                    + service + resource;
            }


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUrl of BaseConnectionManager', ex);
        }

        return retval;
    },

    //returns the timeout value to use for the passed requestType
    getTimeoutValue: function (requestType) {
        //default value is 20sec
        var retval = 20000;

        try {
            switch (requestType) {
                case this.self.REQUEST_TYPES.DATASYNC:
                    retval = 180000; //3min
                    break;
                case this.self.REQUEST_TYPES.FILEUPLOAD:
                    retval = 180000; //3min
                    break;

                case this.self.REQUEST_TYPES.INFO:
                    retval = 20000; //20sec
                    break;

                case this.self.REQUEST_TYPES.CHANGEPW:
                    retval = 40000; //40sec
                    break;

                case this.self.REQUEST_TYPES.ONLINEREQ:
                    retval = 180000; //3min
                    break;

                case this.self.REQUEST_TYPES.DEVICECHECK:
                    retval = 180000; //3min
                    break;

            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeoutValue of BaseConnectionManager', ex);
        }

        return retval;
    },

    //nameValuePairList takes additional parameters to be send via the HTTP Post
    getUserInfo: function (mandt, userid, password, nameValuePairList, callback, asString) {
        try {
            var me = this;

            if (!nameValuePairList)
                nameValuePairList = Ext.create('Ext.util.MixedCollection');

            var innerCallback = null;
            var ac = AssetManagement.customer.core.Core.getAppConfig();

            var lang = ac.getLocale().substr(0, 2)

            nameValuePairList.add("sap-client", mandt);
            nameValuePairList.add("userid", userid);
            nameValuePairList.add("pw", password);
            nameValuePairList.add("language", lang);

            //nameValuePairList.add("pa_ip1", "n/a");
            //nameValuePairList.add("pa_ip2", "n/a");
            //nameValuePairList.add("pa_ping", "");

            if (callback) {
                innerCallback = function (responseString) {
                    try {
                        var userLoginInfo;

                        if (!asString)
                            userLoginInfo = Ext.create('AssetManagement.customer.sync.UserLoginInfo', { infoString: responseString });

                        callback.call(me._linkedSyncManager || me, asString ? responseString : userLoginInfo);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserInfo of BaseConnectionManager', ex);
                        me.errorOccured();
                    }
                };
            }

            this.sendToBackEnd.call(this, this.self.REQUEST_TYPES.INFO, nameValuePairList, innerCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserInfo of BaseConnectionManager', ex);
            this.errorOccured();
        }
    },

    // nameValuePairList takes additional parameters to be send via the HTTP Post
    getCurrentUserInfo: function (nameValuePairList, callback) {
        try {
            var ac = AssetManagement.customer.core.Core.getAppConfig();
            this.getUserInfo(ac.getMandt(), ac.getUserId(), ac.getUserPassword(), nameValuePairList, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentUserInfo of BaseConnectionManager', ex);
            this.errorOccured();
        }
    },

    //will drop a change password request
    //the result will be returned as a UserChangePasswordResult instance, holding information
    changeUserPassword: function (mandt, userId, password, newPassword, callback) {
        try {
            var me = this;

            var nameValuePairList = Ext.create('Ext.util.MixedCollection');

            var innerCallback = null;
            var ac = AssetManagement.customer.core.Core.getAppConfig();

            nameValuePairList.add("pa_ip1", "n/a");
            nameValuePairList.add("pa_ping", "client_ping");
            nameValuePairList.add("sap-client", mandt);
            nameValuePairList.add("pa_user", userId);
            nameValuePairList.add("pa_pwold", password);
            nameValuePairList.add("pa_pwnew1", newPassword);
            nameValuePairList.add("pa_pwnew2", newPassword);

            if (callback !== undefined && callback !== null) {
                innerCallback = function (responseString) {
                    try {
                        var result = Ext.create('AssetManagement.customer.sync.UserChangePasswordResult', { responseString: responseString });

                        callback.call(me, result);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside changeUserPassword of BaseConnectionManager', ex);
                        me.errorOccured();
                    }
                };
            }

            this.sendToBackEnd.call(me, me.self.REQUEST_TYPES.CHANGEPW, nameValuePairList, innerCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside changeUserPassword of BaseConnectionManager', ex);
            this.errorOccured();
        }
    }
});