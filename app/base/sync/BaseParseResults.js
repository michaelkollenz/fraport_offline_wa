Ext.define('AssetManagement.base.sync.BaseParseResults', {
    extend: 'Ext.data.Model',
    
	requires: [
	    'Ext.util.HashMap',
		'AssetManagement.customer.helper.OxLogger'
	],
 
    config: {
        fields: [
            { name: 'missingStores', type: 'auto' }, //a HashMap of missing tables
            { name: 'totalCounter', type: 'int' },
            { name: 'counter', type: 'auto' }, //a HashMap of counters for each table
			{ name: 'layout', type:'boolean'}
       ]
    },
    
    constructor: function(config) {
    	this.callParent(config);
        
    	try {
    	    this.set('missingStores', Ext.create('Ext.util.HashMap'));
    	    this.set('totalCounter', 0);
    	    this.set('counter', Ext.create('Ext.util.HashMap'));
    	    this.set('layout', false);
        } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseParseResults', ex);
	    }
	},

	incrementStoreCounter: function(storeName, updateFlag) {
		try {
			var counterMap = this.get('counter');
		
			if(!counterMap.containsKey(storeName))
				counterMap.add(storeName, {
					insertCount: 0,
					updateCount: 0,
					deleteCount: 0,
					getTotal: function() { return this.insertCount + this.updateCount + this.deleteCount }
				});
			
			var tableCounter = counterMap.get(storeName);
			
			switch(updateFlag) {
				case 'U':
					tableCounter.updateCount++;
					break;
				case 'I':
					tableCounter.insertCount++;
					break;
				case 'D':
					tableCounter.deleteCount++;
				default:
					break;
			}
		} catch(ex) {
			this.errorOccured();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside incrementStoreCounter of BaseParseResults', ex);
		}
	}
});