﻿Ext.define('AssetManagement.base.sync.BaseParsingManager', {
    mixins: ['Ext.mixin.Observable'],

    requires: [
	    'Ext.util.MixedCollection',
	    'AssetManagement.customer.utils.UserAgentInfoHelper',
	    'AssetManagement.customer.helper.DbKeyRangeHelper',
	    'AssetManagement.customer.manager.KeyManager',
		'AssetManagement.customer.sync.ParseResults',
		'AssetManagement.customer.helper.CursorHelper',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.helper.OxLogger'
    ],

    inheritableStatics: {
        SEMICOLON: ";",
        PIPE: "|",
        MAXIMUM_DB_COMMAND_QUEUE_SIZE: 200,

        adjustDataMaximumDbCommandQueueSize: function () {
            try {
                var browser = AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(browser)) {
                    browser = browser.toLowerCase();

                    if (browser === 'chrome') {
                        this.MAXIMUM_DB_COMMAND_QUEUE_SIZE = 1000;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adjustDataTrashHoldValue of BaseParsingManager', ex);
            }
        }
    },

    config: {
        linkedSyncManager: null,
        dataBaseHelper: null
    },

    //private:
    _cancelWorkRequested: false,
    _blockCancelation: false,
    _cancelationInProgress: false,
    _appConfig: null,
    _objectStoreInfoMap: null,
    _objectStoreNamesMap: null,
    _parseResults: null,
    _callback: null,
    _callbackScope: null,
    _currentJob: null,
    _databaseManipulationQueue: null,
    _consoleLoggingEnabled: false,
    _acrToSyncDeltaRecords: null,
    _timeStampDB: '',
    _onlineRequest: false,
    _parseIndex: 0,
    _deletedMobileKeys: null,
    _deletedChildKeys: null,
    _commitRequired: false,

    constructor: function (config) {
        try {
            this.mixins.observable.constructor.call(this, config);

            this.self.adjustDataMaximumDbCommandQueueSize();

            this._appConfig = AssetManagement.customer.core.Core.getAppConfig();
            this._objectStoreInfoMap = this._dataBaseHelper.getObjectStoreInformationMap();
            this._objectStoreNamesMap = Ext.create('Ext.util.HashMap');
            this._acrToSyncDeltaRecords = Ext.create('Ext.util.HashMap');

            this._objectStoreInfoMap.eachKey(function (key, value) {
                if (key.match(/._.*/)) {
                    this._objectStoreNamesMap.add(key.substring(2), key);
                }
            }, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseParsingManager', ex);
        }

    },

    //public
    requestWorkCancelation: function () {
        try {
            this._cancelWorkRequested = true;

            this.fireEvent('cancelationInProgress');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestWorkCancelation of BaseParsingManager', ex);
        }
    },

    //public
    //parses an array of sync data records to the database
    //uses the passed timestamp for the receive time of the records
    //onlineRequest controls, if conflicting records get ommited
    parseData: function (data, timeStamp, onlineRequest, callback, callbackScope) {
        try {
            this._callback = callback;
            this._callbackScope = callbackScope;
            this._currentJob = data;
            this._onlineRequest = onlineRequest;
            this._timeStampDB = this.generateTimeStampForDB(timeStamp);
            this._blockCancelation = false;
            this._consoleLoggingEnabled = AssetManagement.customer.core.Core.getAppConfig().isDebugMode();

            this.prepareParsing();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseData of BaseParsingManager', ex);
            this.onErrorOccured();
        }
    },

    //private
    //previous step: parseData
    //next step: loopParser or returnResults
    prepareParsing: function () {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            this._databaseManipulationQueue = new Array();
            this._acrToSyncDeltaRecords.clear();
            this._deletedMobileKeys = new Array();
            this._deletedChildKeys = new Array();
            this._commitRequired = false;

            this._parseResults = Ext.create('AssetManagement.customer.sync.ParseResults');
            this._parseIndex = 0;

            if (!this._currentJob || this._currentJob.length === 0) {
                if (this._consoleLoggingEnabled)
                    console.log('Data to parse is empty! No parsing action required.');

                Ext.defer(this.returnResults, 1, this);
            } else {
                this._blockCancelation = true;

                //every iteration over the stream parts will be defered to stay responsive
                //interation completes, when there is nothing left to parse
                if (this._consoleLoggingEnabled)
                    console.log('Total records to parse: ' + this._currentJob.length);

                Ext.defer(this.loopParser, 1, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareParsing of BaseParsingManager', ex);
            this.onErrorOccured();
        }
    },

    //private
    //previous step: prepareParsing
    //next step: commitQueue or updateStoresOnSyncDelta
    loopParser: function () {
        try {
            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                if (!this._blockCancelation) {
                    this.cancelWork();
                    return;
                } else {
                    this._cancelationInProgress = true;
                    this.fireEvent('cancelationInProgress', 'Canceling...');
                }
            }

            var endPosition = this._currentJob.length;
            var parseIndex = this._parseIndex;

            //check, if there is any data left
            while (parseIndex < endPosition) {
                //collect database commands
                this.processSyncDataRecord(this._currentJob[parseIndex]);

                parseIndex++;

                //check, if the maximum database queue size has been reached
                if (this._databaseManipulationQueue.length == this.self.MAXIMUM_DB_COMMAND_QUEUE_SIZE) {
                    break;
                    //check if a database commit is required due to the latest generated command(s)
                } else if (this._commitRequired) {
                    break;
                }
            }

            //store the current position in the class variable
            this._parseIndex = parseIndex;

            if (this._databaseManipulationQueue.length > 0) {
                //pass the database commands
                this.commitQueue();
            } else {
                //end of parsing reached
                this.updateStoresOnSyncDelta();
            }
        } catch (ex) {
            this.onErrorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loopParser of BaseParsingManager', ex);
        }
    },

    //private
    //previous step: loopParser
    //next step: loopParser
    commitQueue: function () {
        try {
            if (this._cancelWorkRequested && !this._cancelationInProgress) {
                if (!this._blockCancelation) {
                    this.cancelWork();
                    return;
                } else {
                    this._cancelationInProgress = true;
                    this.fireEvent('cancelationInProgress', 'Canceling...');
                }
            }

            if (this._consoleLoggingEnabled) {
                console.log('Committing a part of data to the database. Command count: ' + this._databaseManipulationQueue.length);
            }

            var me = this;

            var returnToParsingRoutine = function () {
                var workDonePercent = Math.floor((me._parseIndex / me._currentJob.length) * 100);
                me.fireEvent('progressChanged', workDonePercent, Locale.getMsg('parsingReceivedData'));

                Ext.defer(me.loopParser, 1, me);
            };

            if (this._databaseManipulationQueue.length > 0) {
                var transactionCompleteCallback = function () {
                    try {
                        if (me._cancelWorkRequested && !me._cancelationInProgress) {
                            if (!me._blockCancelation) {
                                me.cancelWork();
                                return;
                            } else {
                                me._cancelationInProgress = true;
                                me.fireEvent('cancelationInProgress', 'Canceling...');
                            }
                        }

                        if (!me._databaseManipulationQueue) {
                            return;
                        }

                        me._databaseManipulationQueue = new Array();
                        me._commitRequired = false;

                        if (me._consoleLoggingEnabled)
                            console.log('Successfully committed a part of parsed data to database. Remaining ' + Math.ceil((1 - me._parseIndex / me._currentJob.length) * 100) + '%');

                        returnToParsingRoutine();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside commitQueue of BaseParsingManager', ex);
                        me.onErrorOccured();
                    }
                };

                var transactionErrorCallback = function (eventArgs) {
                    try {
                        var errorMessage = "";
                        var errorCode = "";

                        if (eventArgs && eventArgs.target && eventArgs.target.error) {
                            errorMessage = eventArgs.target.error.message;
                            errorCode = eventArgs.target.error.code;
                        }

                        if (me._consoleLoggingEnabled)
                            console.log("Transaction-error processing database commands while sync parsing: " + errorMessage + ' (Code ' + errorCode + ')');

                        if (me._cancelWorkRequested) {
                            me.cancelWork();
                            return false;
                        }

                        this.onErrorOccured.call(me, "Transaction-error processing database commands while sync parsing: " + errorMessage + ' (Code ' + errorCode + ')');
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside commitQueue of BaseParsingManager', ex);
                        me.onErrorOccured();
                    }
                };

                this._dataBaseHelper.commitSyncCommandSet(this._databaseManipulationQueue, transactionCompleteCallback, transactionErrorCallback, this);
            } else {
                if (this._consoleLoggingEnabled)
                    console.log('Database commit triggered, but no commands queued. Remaining ' + Math.ceil((1 - me._parseIndex / me._currentJob.length) * 100) + '%');

                returnToParsingRoutine();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside commitQueue of BaseParsingManager', ex);
            this.onErrorOccured();
        }
    },

    //private
    //previous step: commitQueue
    //next step: returnResults
    updateStoresOnSyncDelta: function () {
        try {
            var stores = this._parseResults.get('counter').getKeys();

            var mandt = this._appConfig.getMandt();
            var userId = this._appConfig.getUserId();
            var me = this;

            var successCallback = function (eventArgs) {
                try {
                    if (eventArgs && eventArgs.target && eventArgs.target.result) {
                        var entry = eventArgs.target.result.value;

                        if (Ext.Array.contains(stores, entry['STORENAME'])) {
                            entry['DOWNLOAD'] = 'X';

                            if (me._acrToSyncDeltaRecords.containsKey(entry['STORENAME']))
                                entry['UPLOAD'] = 'X';

                            AssetManagement.customer.helper.CursorHelper.doCursorUpdate(eventArgs.target.result, entry);
                        }

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(eventArgs.target.result);
                    } else {
                        this._blockCancelation = false;

                        if (me._consoleLoggingEnabled)
                            console.log('All parsed data successfully committed to database');

                        Ext.defer(me.returnResults, 1, me);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateStoresOnSyncDelta of BaseParsingManager', ex);
                    me.onErrorOccured();
                }
            };

            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('S_SYNCDELTA', null);
            this._dataBaseHelper.query('S_SYNCDELTA', keyRange, successCallback, this.buildCommandErrorCallback(), false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateStoresOnSyncDelta of BaseParsingManager', ex);
            this.onErrorOccured();
        }
    },

    //private
    //previous step: checkForResetRequirement or commitQueue
    //next step: -
    returnResults: function () {
        try {
            if (this._cancelWorkRequested) {
                this.cancelWork();
                return;
            }

            this._callback.call(this._callbackScope || this, this._parseResults);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside returnResults of BaseParsingManager', ex);
            this.onErrorOccured();
        }
    },

    //private
    //genenrates the time stamp in db format for the passed value
    generateTimeStampForDB: function (timeStamp) {
        var retval = '';

        try {
            if (timeStamp) {
                var type = typeof timeStamp;

                switch (type) {
                    case 'object':
                        retval = AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(timeStamp);
                        break;

                    case 'string':
                    case 'number':
                        retval += timeStamp;
                        break;

                }
            }

            if (!retval) {
                retval = AssetManagement.customer.utils.DateTimeUtils.getDBTimestamp('UTC');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateTimeStampForDB of BaseParsingManager', ex);
        }

        return retval;
    },

    //private
    //processes one sync data record
    processSyncDataRecord: function (syncDataRecord) {
        try {
            if (!syncDataRecord) {
                return;
            }

            var recordName = syncDataRecord['RECORDNAME'];

            //there are some record names, which needs extraordinary handling
            if ("MC_MOBILEKEY" === recordName) {
                this.processMobileKeySyncDataRecord(syncDataRecord);
            } else if ("MC_CHILDKEY" === recordName) {
                this.processChildKeySyncDataRecord(syncDataRecord);
            } else {
                this.processUsualSyncDataRecord(syncDataRecord);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processSyncDataRecord of BaseParsingManager', ex);
        }
    },

    //private
    //processes a sync data record the ususal way
    processUsualSyncDataRecord: function (syncDataRecord) {
        try {
            //determine the target store
            var recordName = syncDataRecord['RECORDNAME'];
            var curStoreName = this._objectStoreNamesMap.get(recordName);

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(curStoreName)) {
                this._parseResults.get('missingStores').replace(recordName, recordName);
                return;
            }

            if(!this._parseResults.get('layout') && curStoreName === 'C_LAYOUT') {
                this._parseResults.set('layout', true);
            }

            //extract the update flag
            var updateFlag = syncDataRecord['UPDFLAG'];
            var curStoreInfo = this._objectStoreInfoMap.get(curStoreName);

            //build object to store
            //this is necessary, because additional key fields identifying the user needs to be added
            var objectToStore = this.buildObjectToStore(syncDataRecord, curStoreInfo);

            //if a dataset with updflag A/C/R has been received, the SYNCDELTA-Store will have to be reset to UPLOAD: 'X' for this store
            var isACR = updateFlag === 'A' || updateFlag === 'C' || updateFlag === 'R';

            var readBeforeWrite = this._onlineRequest;
            var readBeforeWriteRequired = readBeforeWrite && AssetManagement.customer.utils.StringUtils.startsWith(curStoreName, 'D');

            var commandToAdd = null;

            var attributes = curStoreInfo.get('attributes');
            if(attributes && attributes.getCount() > 0 ) {
                attributes.each(function(attribute) {
                    var attributeName = attribute.get('name');
                    if(typeof objectToStore[attributeName] === 'undefined') {
                        objectToStore[attributeName] = '';
                    }
                })
            }

            if (readBeforeWriteRequired) {
                commandToAdd = this.getReadBeforeWriteCommand(curStoreName, objectToStore);
            } else {
                commandToAdd = this.getUsualSyncCommand(curStoreName, objectToStore);
            }

            if (commandToAdd) {
                this._databaseManipulationQueue.push(commandToAdd);

                if (isACR && !this._acrToSyncDeltaRecords.containsKey(curStoreName))
                    this._acrToSyncDeltaRecords.add(curStoreName, curStoreName);

                this._parseResults.set('totalCounter', this._parseResults.get('totalCounter') + 1);
                this._parseResults.incrementStoreCounter(curStoreName, updateFlag);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processUsualSyncDataRecord of BaseParsingManager', ex);
        }
    },

    //private
    //processes a mobileKey delete record
    processMobileKeySyncDataRecord: function (syncDataRecord) {
        try {
            if (!syncDataRecord) {
                return;
            }

            //a sequence of deletion commands has to be added to the databaseManipulationQueue
            //these commands are provided by the KeyManager

            //extract the mobile key from the record
            var dataObject = syncDataRecord['JSON_DATA'];
            var mobileKey = dataObject['MOBILEKEY'];

            //check, if this mobilekey already has been procecessed
            //if yes, SAP send a duplicate entry - processing this key again has to be prevented, because it results in database errors (same transaction)
            var alreadyProcessed = Ext.Array.contains(this._deletedMobileKeys, mobileKey);

            if (!alreadyProcessed) {
                var commands = AssetManagement.customer.manager.KeyManager.getDeleteMobileKeyDataCommands(mobileKey, this.buildCommandSuccessCallback(), this.buildCommandErrorCallback(), this);

                if (commands && commands.length > 0) {
                    Ext.Array.each(commands, function (item) {
                        this._databaseManipulationQueue.push(item);
                    }, this);
                }

                this._deletedMobileKeys.push(mobileKey);

                //only one mobile/childkey can be processed per transaction
                //because it gets in conflict with other mobile key commands
                //or following insert commands for objects with same keys than the objects which are removed by a mobile key delete command (happens at immediately repush on post of system record)
                this._commitRequired = true;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processMobileKeySyncDataRecord of BaseParsingManager', ex);
        }
    },

    //private
    //processes a childKey delete record
    processChildKeySyncDataRecord: function (syncDataRecord) {
        try {
            if (!syncDataRecord) {
                return;
            }

            //a sequence of deletion commands has to be added to the databaseManipulationQueue
            //these commands are provided by the KeyManager

            //extract the child key from the record
            var dataObject = syncDataRecord['JSON_DATA'];
            var childKey = dataObject['CHILDKEY'];

            //check, if this childkey already has been procecessed
            //if yes, SAP has sent a duplicate entry - processing this key again has to be prevented, because it results in database errors (same transaction)
            var alreadyProcessed = Ext.Array.contains(this._deletedChildKeys, childKey);

            if (!alreadyProcessed) {
                var commands = AssetManagement.customer.manager.KeyManager.getDeleteChildKeyDataCommands(childKey, this.buildCommandSuccessCallback(), this.buildCommandErrorCallback(), this);

                if (commands && commands.length > 0) {
                    Ext.Array.each(commands, function (item) {
                        this._databaseManipulationQueue.push(item);
                    }, this);
                }

                this._deletedChildKeys.push(childKey);

                //only one mobile/childkey can be processed per transaction
                //because it gets in conflict with other mobile key commands
                //or following insert commands for objects with same keys than the objects which are removed by a mobile key delete command (happens at immediately repush on post of system record)
                this._commitRequired = true;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processChildKeySyncDataRecord of BaseParsingManager', ex);
        }
    },

    //private
    buildObjectToStore: function (syncDataRecord, storeInfo) {
        var retval = null;

        try {
            //extract the object to store from the sync data record
            var objectToStore = syncDataRecord['JSON_DATA'];

            //first begin with mapping the update flag from the sync data record first level into the database record to store
            objectToStore['UPDFLAG'] = syncDataRecord['UPDFLAG'];

            //write the SAP client, if field part of the store
            if (storeInfo.hasAttribute('MANDT')) {
                objectToStore['MANDT'] = this._appConfig.getMandt();
            }

            //write the user id, if field part of the store
            if (storeInfo.hasAttribute('SCENARIO')) {
                objectToStore['SCENARIO'] = this._appConfig.getScenario();
            }

            /*//write the mobile scenario, if field part of the store and field not used for something else (not a position 2)
            var scenarioAttributeDetails = storeInfo.getAttributeDetails('SCENARIO');

            if (scenarioAttributeDetails && scenarioAttributeDetails.get('order') === 2) {
                objectToStore['SCENARIO'] = this._appConfig.getScenario();
            }*/

            //write the user id, if field part of the store
            if (storeInfo.hasAttribute('USERID')) {
                objectToStore['USERID'] = this._appConfig.getUserId();
            }

            //write the default dirty value, if field part of the store
            if (storeInfo.hasAttribute('DIRTY')) {
                objectToStore['DIRTY'] = '0';
            }

            //write the receive time of the record, if field part of the store
            if (storeInfo.hasAttribute('RCVTIME')) {
                var rcvTimeString = this._timeStampDB;
                objectToStore['RCVTIME'] = rcvTimeString;
            }

            //supply any missing key attribute with an initial value
            var keyAttributes = storeInfo.get('keyAttributes');

            if (keyAttributes && keyAttributes.length > 0) {
                Ext.Array.each(keyAttributes, function (keyAttribute) {
                    if (!(keyAttribute in objectToStore)) {
                        objectToStore[keyAttribute] = '';
                    }
                }, this);
            }

            retval = objectToStore;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildObjectToStore of BaseParsingManager', ex);
        }

        return retval;
    },

    //private
    getUsualSyncCommand: function (storeName, objectToStore) {
        var retval = null;

        try {
            var updateFlag = objectToStore['UPDFLAG'];

            switch (updateFlag) {
                case 'X':
                case 'U':
                    objectToStore['UPDFLAG'] = 'X';
                    retval = this._dataBaseHelper.getSyncUpdateCommand(storeName, objectToStore, this.buildCommandSuccessCallback(), this.buildCommandErrorCallback());
                    break;

                case 'I':
                    objectToStore['UPDFLAG'] = 'X';
                    retval = this._dataBaseHelper.getSyncPutCommand(storeName, objectToStore, this.buildCommandSuccessCallback(), this.buildCommandErrorCallback());
                    break;

                case 'A':
                case 'C':
                case 'R':
                    objectToStore['UPDFLAG'] = updateFlag;
                    retval = this._dataBaseHelper.getSyncUpdateCommand(storeName, objectToStore, this.buildCommandSuccessCallback(), this.buildCommandErrorCallback());
                    break;

                case 'D':
                    //extract the key of the object
                    var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject(storeName, objectToStore);
                    retval = this._dataBaseHelper.getSyncDeleteCommand(storeName, keyRange, null, this.buildCommandSuccessCallback(objectToStore), this.buildCommandErrorCallback());
                    break;

                default:
                    break;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUsualSyncCommand of BaseParsingManager', ex);
        }

        return retval;
    },

    //private
    getReadBeforeWriteCommand: function (storeName, objectToStore) {
        var retval = null;

        try {
            var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.extractStoreKeyOfObject(storeName, objectToStore);
            var me = this;

            var querySuccessCallback = function (eventArgs) {
                try {
                    var retval = false;

                    if (eventArgs && eventArgs.target) {
                        var store = eventArgs.target.source;
                        var cursor = eventArgs.target.result;

                        var isConflicted = !!(cursor && cursor.value && cursor.value['UPDFLAG'] !== 'X');

                        if (!isConflicted) {
                            //determine what to do, analysing the update flag
                            var updateFlag = objectToStore['UPDFLAG'];

                            switch (updateFlag) {
                                case 'U':
                                case 'I':
                                    objectToStore['UPDFLAG'] = 'X';
                                    me._dataBaseHelper.manageTrackingGUID(storeName, objectToStore);
                                    store.put(objectToStore);
                                    break;

                                case 'D':
                                    AssetManagement.customer.helper.ObjectStoreHelper.doObjectStoreDelete(store, keyRange);
                                    break;
                                case 'A':
                                case 'C':
                                case 'R':
                                    objectToStore['UPDFLAG'] = updateFlag;
                                    me._dataBaseHelper.manageTrackingGUID(storeName, objectToStore);
                                    store.put(objectToStore);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getReadBeforeWriteCommand of BaseParsingManager', ex);
                    retval = true;
                }

                return retval;
            };

            retval = this._dataBaseHelper.getSyncQueryCommand(storeName, keyRange, querySuccessCallback, this.buildCommandErrorCallback());
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getReadBeforeWriteCommand of BaseParsingManager', ex);
        }

        return retval;
    },

    //private
    cancelWork: function () {
        try {
            this._cancelWorkRequested = false;
            this._cancelationInProgress = false;
            this._blockCancelation = false;
            this._callback = null;
            this._callbackScope = null;
            this._currentJob = null;
            this._parseResults = null;
            this._databaseManipulationQueue = null;
            this.fireEvent('workCancelled');
            return;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseParsingManager', ex);
        }
    },

    //private
    onErrorOccured: function (message) {
        try {
            this._cancelWorkRequested = false;
            this._callback = null;
            this._callbackScope = null;
            this._currentJob = null;
            this._parseResults = null;
            this._databaseManipulationQueue = null;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = 'A general parsing error occured';

            this.fireEvent('errorOccured', message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onErrorOccured of BaseParsingManager', ex);
        }
    },

    //callback encapsulation
    //private
    buildCommandSuccessCallback: function () {
        var me = this;
        var retval = null;

        try {
            retval = function () {
                return me.commandSuccessCallback.call(me);
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCommandSuccessCallback of BaseParsingManager', ex);
        }

        return retval;
    },

    //private
    buildCommandErrorCallback: function () {
        var me = this;
        var retval = null;

        try {
            retval = function (eventArgs) {
                return me.commandErrorCallback.call(me, eventArgs);
            };
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildCommandErrorCallback of BaseParsingManager', ex);
        }

        return retval;
    },

    //private
    commandSuccessCallback: function () {
    },

    //private
    commandErrorCallback: function (eventArgs) {
        try {
            if (this._consoleLoggingEnabled)
                console.log("Command-error processing a database request while sync parsing");

            if (this._cancelWorkRequested) {
                this.cancelWork();
                return true;
            }

            this.onErrorOccured.call(this, "Command-error processing a database request while sync parsing");
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside commandErrorCallback of BaseParsingManager', ex);
        }
    }
});