Ext.define('AssetManagement.base.sync.BaseUserChangePasswordResult', {
	requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	//public:
	config: {
		responseString: '',
		sapUser: '',
		message: '',
		success: false
	},
	
	//private:	
	constructor: function(config) {
		try {
			this.initConfig(arguments);
		    
            if(config && config.responseString)
	           this.setResponseString(config.responseString);

            this.validateResponse();
	    } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseUserChangePasswordResult', ex);
	    }
	},
	
	//private methods
	validateResponse: function() {
		try {
			var response = this.getResponseString();
			
			var sapAccepted = false;
			var sapMessage = '';
			var sapUser = '';
			var sapPing = '';
			
			var parts = response.split('|');
			
			if(parts && parts.length > 0) {
				for(var i = 0; i < parts.length; i++) {
					if(i === 0) {
						sapAccepted = parts[0] === 'X';
					} else if(i === 1 && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parts[1])) {
						sapMessage = parts[1].trim();
					} else if(i === 2 && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parts[2])) {
						sapUser = parts[2].trim();
					} else if(i === 3 && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(parts[3])) {
						sapPing = parts[3].trim();
					}
				}
			} else {
				sapMessage = Locale.getMsg('sapSystemNotResponding');
			}
			
			
			this.setSapUser(sapUser);
			this.setMessage(sapMessage);
			
			this.setSuccess(sapAccepted === true && sapPing === 'client_ping');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateResponse of BaseUserChangePasswordResult', ex);
		}
	}
});