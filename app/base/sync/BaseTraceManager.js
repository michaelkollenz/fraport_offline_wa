Ext.define('AssetManagement.base.sync.BaseTraceManager', {
	mixins: ['Ext.mixin.Observable'],

	requires: [
	    'Ext.util.MixedCollection',
	    'AssetManagement.customer.helper.FileHelper',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.DbKeyRangeHelper',
		'AssetManagement.customer.helper.CursorHelper',
		'AssetManagement.customer.manager.DocUploadManager',
		'AssetManagement.customer.sync.SyncManager',
		'AssetManagement.customer.sync.FileUploadManager',
		'AssetManagement.customer.utils.UserAgentInfoHelper',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.DatabaseContentExtractor'
	],

	//private:
	_appConfig: null,
	_connectionManager: null,
    _fileUploadManager: null,
	_callback: null,
	_callbackScope: null,

	_curProcessPercent: NaN,
	_cancelWorkRequested: false,
	_errorOccured: false,
	_lastErrorMessage: '',

	_zipLibraryLoaded: false,
	_zipLibraryLoadingRequestIsPending: false,

    //objects for extended trace upload
	_errorDateTime: null,
	_errorRemarks: '',
    _includeDatabaseContent: false,

	_traceTimestampFormat: 'd.m.Y H:i:s.u',

	inheritableStatics: {
		_instance: null,

		getInstance: function() {
			if (this._instance == null)
				this._instance = Ext.create('AssetManagement.customer.sync.TraceManager');

			return this._instance;
		}
	},

	constructor: function(config) {
	    try {
	    	this.mixins.observable.constructor.call(this, config);
	        this.loadZipLibrary();

	        this._appConfig = AssetManagement.customer.core.Core.getAppConfig();
	        this._connectionManager = Ext.create('AssetManagement.customer.sync.ConnectionManager');

            this._fileUploadManager = Ext.create('AssetManagement.customer.sync.FileUploadManager', {
                listeners: {
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });
	    } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseTraceManager', ex);
	    }
	},

	loadZipLibrary: function(callback) {
		try {
			this._zipLibraryLoadingRequestIsPending = true;

			var innerCallback = function(success) {
				try {
					this._zipLibraryLoadingRequestIsPending = false;

					if(!success)
						success = false;

					this._zipLibraryLoaded = success;

					if(success === false)
						console.log('Failure loading zip library');

					if(callback)
						callback.call(this);
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadZipLibrary of BaseTraceManager', ex);
				}
			};

			//load the library synchronous
			AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/jszip.js', true, innerCallback, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadZipLibrary of BaseTraceManager', ex);
		}
	},

	//public methods
	requestCancelation: function() {
		try {
			if (!this._cancelWorkRequested) {
				this._cancelWorkRequested = true;
				this.fireEvent('cancelationInProgress', Locale.getMsg('cancellationInProgress'));
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestCancelation of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//extracts trace from the database, builds a zip archive and uploads it to the sap system
	//this function will trigger a sequence of private methods
    //this sequence is cancelable until database extraction in "generateDatabseJsonFile"
	sendLogFiles: function(argumentsObject, callback, callbackScope) {
	    try {
			//before the task may start, ensure the zip library has been loaded
			//if the library is still being loaded, wait for it (retry in 250ms)
			if(this._zipLibraryLoadingRequestIsPending) {
				this.reportProgress(5, Locale.getMsg('generatingLogFiles'));
				Ext.defer(this.sendLogFiles, 250, this, [ argumentsObject, callback, callbackScope ]);
				return;
			} else if(this._zipLibraryLoaded === false) {
				//the library loading failed, abort the process
				this._callback = callback;
				this._callbackScope = callbackScope;

				this.errorOccured('ZIP Library could not be loaded!');

				return;
			}

			this.resetState();

			this._callback = callback;
			this._callbackScope = callbackScope;

			if (argumentsObject) {
			    this._errorRemarks = argumentsObject.errorRemarks || '';
			    this._errorDateTime = argumentsObject.errorDateTime || null;
			    this._includeDatabaseContent = argumentsObject.includeDatabaseContent === true || false;
			}

			this.reportProgress(5, Locale.getMsg('generatingLogFiles'));
			Ext.defer(this.manageCountOfLogEntries, 1, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendLogFiles of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//private methods
	resetState: function() {
		try {
			//status fields
			this._curProcessPercent = 0;
			this._cancelWorkRequested = false;
			this._errorOccured = false;
			this._lastErrorMessage = '';

			this._errorDateTime = null;
			this._errorRemarks = '';
			this._includeDatabaseContent = false;

			this._callback = null;
			this._callbackScope = null;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetState of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//before the stack trace will be extracted trigger manageLogDatabaseSize
	//this will manage the maximum count of log entries
	manageCountOfLogEntries: function() {
		try {
			if(this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var me = this;

			var normalizeSizeCallback = function(success) {
				try {
					if(me._cancelWorkRequested || this._errorOccured) {
						me.cancelWork();
						return;
					}

					if(success === true) {
						me.reportProgress(10, Locale.getMsg('generatingLogFiles'));
						Ext.defer(me.extractStackTrace, 1, me);
					} else {
						me.errorOccured();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageCountOfLogEntries of BaseTraceManager', ex);
					me.errorOccured();
				}
			};

			var eventId = AssetManagement.customer.helper.OxLogger.manageLogDatabaseSize();

			if(eventId > -1) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, normalizeSizeCallback);
			} else {
				this.errorOccured();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageCountOfLogEntries of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//reads the current stack trace from the database and will pass an array of stacktrace objects to generateTraceTextFile
	extractStackTrace: function() {
		try {
			if(this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var me = this;
			var traceArray = new Array();

			var traceEntriesCallback = function(traceArray) {
				try {
					if(traceArray !== undefined) {
						me.reportProgress(20, Locale.getMsg('generatingLogFiles'));
						Ext.defer(me.generateTraceTextFile, 1, me, [ traceArray ]);
					} else {
						me.errorOccured();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractStackTrace of BaseTraceManager', ex);
					me.errorOccured();
				}
			};

			var eventId = AssetManagement.customer.helper.OxLogger.getAllTraceEntries();

			if(eventId > -1) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, traceEntriesCallback);
			} else {
				this.errorOccured();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractStackTrace of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//will generate an array buffer representing the content of the trace text file - entries of type SYNC will be exluded and go into an extra trace file
	//this array buffer will be passed to generateZipArchive (using blobs is slower and will not be accepted by JSZip)
	//the textfiles content will be in UTF-8 format
	generateTraceTextFile: function(arrayOfTraceObjects) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var traceFileContent;

			if(arrayOfTraceObjects && arrayOfTraceObjects.length > 0) {
				//build an array of arraybuffers for each string first
				//it is neccessary to do it this way, because the results arrays buffer size is unknown before conversion (due to multi byte charset UTF-8)
				var arrayOfArrayBuffers = new Array();
				var requiredSize = 0;

				Ext.Array.each(arrayOfTraceObjects, function(traceObject) {
					if(traceObject && traceObject.type !== AssetManagement.customer.helper.OxLogger.MESSAGE_TYPES.SYNC) {
						var entryString = this.getLogFileEntryForTraceObject(traceObject);
						var entriesArrayBuffer = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer(entryString);

						arrayOfArrayBuffers.push(entriesArrayBuffer);
						requiredSize += entriesArrayBuffer.byteLength;
					}
				}, this);

				traceFileContent = new ArrayBuffer(requiredSize);
				var bufView = new Uint8Array(traceFileContent);

				//build the target arraybuffer by merging all the arrayBuffers for each string
				var position = 0;
				var tempView = null;

				Ext.Array.each(arrayOfArrayBuffers, function(arrayBuffer) {
					for (var i=0; i < arrayBuffer.byteLength; i++) {
						tempView = new Uint8Array(arrayBuffer);
					    bufView[position] = tempView[i];

					    position++;
					}
				}, this);
			} else {
				traceFileContent = new ArrayBuffer(0);
			}

			var filesToZip = new Array();
			filesToZip.push(traceFileContent);

			this.reportProgress(30, Locale.getMsg('generatingLogFiles'));
			Ext.defer(this.generateSyncTraceTextFile, 1, this, [ arrayOfTraceObjects, filesToZip ]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateTraceTextFile of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//will generate a sync trace text file - only trace entries of type SYNC will be included
	//the added file will be in UTF-8 format
	generateSyncTraceTextFile: function(arrayOfTraceObjects, filesToZip) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var syncTraceFileContent;

			if(arrayOfTraceObjects && arrayOfTraceObjects.length > 0) {
				//build an array of arraybuffers for each string first
				//it is neccessary to do it this way, because the results arrays buffer size is unknown before conversion (due to multi byte charset UTF-8)
				var arrayOfArrayBuffers = new Array();
				var requiredSize = 0;

				Ext.Array.each(arrayOfTraceObjects, function(traceObject) {
					if(traceObject && traceObject.type === AssetManagement.customer.helper.OxLogger.MESSAGE_TYPES.SYNC) {
						var entryString = this.getLogFileEntryForTraceObject(traceObject);
						var entriesArrayBuffer = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer(entryString);

						arrayOfArrayBuffers.push(entriesArrayBuffer);
						requiredSize += entriesArrayBuffer.byteLength;
					}
				}, this);

				syncTraceFileContent = new ArrayBuffer(requiredSize);
				var bufView = new Uint8Array(syncTraceFileContent);

				//build the target arraybuffer by merging all the arrayBuffers for each string
				var position = 0;
				var tempView = null;

				Ext.Array.each(arrayOfArrayBuffers, function(arrayBuffer) {
					for (var i=0; i < arrayBuffer.byteLength; i++) {
						tempView = new Uint8Array(arrayBuffer);
					    bufView[position] = tempView[i];

					    position++;
					}
				}, this);
			} else {
				syncTraceFileContent = new ArrayBuffer(0);
			}

			if(!filesToZip)
				filesToZip = new Array();

			filesToZip.push(syncTraceFileContent);

			this.reportProgress(40, Locale.getMsg('generatingLogFiles'));
			Ext.defer(this.generateMetaInfoForClientTextFile, 1, this, [ filesToZip ]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateSyncTraceTextFile of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	//will generate a text file, which describes the current client and browser informations, and add it to the passed array
	//the added file will be in UTF-8 format
	generateMetaInfoForClientTextFile: function(filesToZip) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var metaInfoFileContent;

			var arrayOfLines = new Array();

			//write current client information
			var ac = AssetManagement.customer.core.Core.getAppConfig();
			var hostInfo = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();

			var protocol = window.location.protocol;

			if(protocol[protocol.length - 1] === ':')
				protocol = protocol.substring(0, protocol.length - 1);

			arrayOfLines.push('------------------------');
			arrayOfLines.push('Application Information:');
			arrayOfLines.push('Client-Version: ' + AssetManagement.customer.core.Core.getVersion());
			arrayOfLines.push('Operating on database "' +  AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseName() + '" of version: ' + AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseVersion());
			arrayOfLines.push('Using protocol: ' + protocol);
			arrayOfLines.push('Current SAP client: ' + ac.getMandt());
			arrayOfLines.push('Current user: ' + ac.getUserId());
			arrayOfLines.push('Using locale: ' + ac.getLocale());
			arrayOfLines.push('------------------------');
			arrayOfLines.push('Host information:');
			arrayOfLines.push('Webbrowser: ' + hostInfo.browser + ' (' + hostInfo.browserVersion + ')');
			arrayOfLines.push('Webbrowser language: ' + hostInfo.browserLanguage);
			arrayOfLines.push('Webbrowser allows cookies: ' + (navigator.cookieEnabled ? 'true' : 'false'));
			arrayOfLines.push('Operating System: ' + hostInfo.OS);
			arrayOfLines.push('Supports GPS: ' + (navigator.geolocation ? 'true' : 'false'));
			arrayOfLines.push('Device maximum Screen-Resolution: (' + hostInfo.screenWidth + 'x' + hostInfo.screenHeight +')px');
			arrayOfLines.push('------------------------');

			//build an array of arraybuffers for each string first
			//it is neccessary to do it this way, because the results arrays buffer size is unknown before conversion (due to multi byte charset UTF-8)
			var arrayOfArrayBuffers = new Array();
			var requiredSize = 0;

			var isFirstLine = true;
			var lastLineIndex = arrayOfLines.length - 1;

			for(var i = 0; i <= lastLineIndex; i++) {
				var affectedLine = arrayOfLines[i];

				if(i != lastLineIndex)
					affectedLine += '\r\n';

				var linesArrayBuffer = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer(affectedLine);

				arrayOfArrayBuffers.push(linesArrayBuffer);
				requiredSize += linesArrayBuffer.byteLength;
			}

			metaInfoFileContent = new ArrayBuffer(requiredSize);
			var bufView = new Uint8Array(metaInfoFileContent);

			//build the target arraybuffer by merging all the arrayBuffers for each string
			var position = 0;
			var tempView = null;

			Ext.Array.each(arrayOfArrayBuffers, function(arrayBuffer) {
				for (var i=0; i < arrayBuffer.byteLength; i++) {
					tempView = new Uint8Array(arrayBuffer);
				    bufView[position] = tempView[i];

				    position++;
				}
			}, this);

			filesToZip.push(metaInfoFileContent);

			this.reportProgress(45, Locale.getMsg('generatingLogFiles'));
			Ext.defer(this.generateErrorReportFile, 1, this, [filesToZip]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateTraceTextFile of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

    //will generate a textfile with user provided error data:
    //****** time of event -- required
    //****** descriptive informations from user -- optional
	//and add it to the passed array
	//the added file will be in UTF-8 format
	generateErrorReportFile: function (filesToZip) {
	    try {
	        if (this._cancelWorkRequested || this._errorOccured) {
	            this.cancelWork();
	            return;
	        }

	        if (AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(this._errorDateTime)) {
	            this._errorDateTime = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');
	        } else {
	            this._errorDateTime = AssetManagement.customer.utils.DateTimeUtils.getInDifferentTimeZone(this._errorDateTime, '', 'UTC');
	        }

	        var errorReportFileContent;
	        var arrayOfLines = new Array();

	        arrayOfLines.push('------------------------');
	        arrayOfLines.push('Time of reported error: ' + AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(this._errorDateTime, true));
	        arrayOfLines.push('');
	        arrayOfLines.push('User remarks:');

	        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorRemarks)) {
	            arrayOfLines.push(this._errorRemarks);
	        }
	        arrayOfLines.push('------------------------');

	        //build an array of arraybuffers for each string first
	        //it is neccessary to do it this way, because the results arrays buffer size is unknown before conversion (due to multi byte charset UTF-8)
	        var arrayOfArrayBuffers = new Array();
	        var requiredSize = 0;

	        var isFirstLine = true;
	        var lastLineIndex = arrayOfLines.length - 1;

	        for (var i = 0; i <= lastLineIndex; i++) {
	            var affectedLine = arrayOfLines[i];

	            if (i != lastLineIndex)
	                affectedLine += '\r\n';

	            var linesArrayBuffer = AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer(affectedLine);

	            arrayOfArrayBuffers.push(linesArrayBuffer);
	            requiredSize += linesArrayBuffer.byteLength;
	        }

	        errorReportFileContent = new ArrayBuffer(requiredSize);
	        var bufView = new Uint8Array(errorReportFileContent);

	        //build the target arraybuffer by merging all the arrayBuffers for each string
	        var position = 0;
	        var tempView = null;

	        Ext.Array.each(arrayOfArrayBuffers, function (arrayBuffer) {
	            for (var i = 0; i < arrayBuffer.byteLength; i++) {
	                tempView = new Uint8Array(arrayBuffer);
	                bufView[position] = tempView[i];

	                position++;
	            }
	        }, this);

	        filesToZip.push(errorReportFileContent);

	        this.reportProgress(50, Locale.getMsg('generatingLogFiles'));
	        Ext.defer(this.generateDatabseJsonFile, 1, this, [filesToZip]);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateErrorReportFile of BaseTraceManager', ex);
	        this.errorOccured();
	    }
	},

    //will generate a database representation in a JSONfile if option is checked
    //and add it to the passed array
	generateDatabseJsonFile: function (filesToZip) {
	    try{
	        if (this._cancelWorkRequested || this._errorOccured) {
	            this.cancelWork();
	            return;
	        }

            //only get database if explicit choosen
	        if (this._includeDatabaseContent) {
	            this.fireEvent('cancelableChanged', false);

	            var me = this;

	            var databaseExtractCallback = function (databaseJSONObject) {
	                try {
	                    if (databaseJSONObject === undefined) {
	                        me.errorOccured();
	                    } else {
	                        //if databaseJSONObject is null assign empty string, push arrayBuffer of json to filesToZip
	                        var arrayBufferConversionCallback = function (data) {
	                            try{
	                                filesToZip.push(data);

	                                me.fireEvent('cancelableChanged', true);

	                                me.reportProgress(65, Locale.getMsg('generatingLogFiles'));
	                                Ext.defer(me.generateZipArchive, 1, me, [filesToZip]);
	                            } catch (ex) {
	                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDatabseJsonFile-databaseExtractCallback-arrayBufferConversionCallback of BaseTraceManager', ex);
	                            }
	                        };
	                        me.generateJSONArrayBufferFromDbObject(databaseJSONObject, arrayBufferConversionCallback);
	                    }
	                } catch (ex) {
	                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDatabseJsonFile-databaseExtractCallback of BaseTraceManager', ex);
	                }
	            };

	            var eventId = AssetManagement.customer.helper.DatabaseContentExtractor.extractContentFromDatabase();

	            if (eventId > -1) {
	                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, databaseExtractCallback);
	            } else {
	                this.errorOccured();
	            }
	        } else {
	            this.reportProgress(65, Locale.getMsg('generatingLogFiles'));
	            Ext.defer(this.generateZipArchive, 1, this, [filesToZip]);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDatabseJsonFile-databaseExtractCallback of BaseTraceManager', ex);
	        this.errorOccured();
	    }
	},

	generateZipArchive: function(arrayOfFiles) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var jsZip = new JSZip(null, { createFolders: true });
			var now = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();

			//add the general trace text file
			var traceFileContent = arrayOfFiles[0];
			var fileOptions = {
           		binary: true,
           		date: now
			};
			jsZip.file('trace.txt', traceFileContent, fileOptions);

			//add the sync trace text file
			var syncTraceFileContent = arrayOfFiles[1];
			var fileOptions = {
           		binary: true,
           		date: now
			};
			jsZip.file('syncTrace.txt', syncTraceFileContent, fileOptions);

			//add the meta text file
			var metaFileContent = arrayOfFiles[2];
			var fileOptions = {
           		binary: true,
           		date: now
			};
			jsZip.file('clientInfo.txt', metaFileContent, fileOptions);

		    //add the error report file
			var errorReportFileContent = arrayOfFiles[3];
			var fileOptions = {
			    binary: true,
			    date: now
			};
			jsZip.file('errorReport.txt', errorReportFileContent, fileOptions);

		    //add the databse JSON representation if requested
			if (this._includeDatabaseContent) {
			    var databaseContent = arrayOfFiles[4];
			    var fileOptions = {
			        binary: true,
			        date: now
			    };
			    jsZip.file('database.json', databaseContent, fileOptions);
			}

			var generateOptions = {
				type: 'arraybuffer',
				compression: 'DEFLATE',
				compressionOptions: {
					level: 6
				}
			};

			var zipArchiveAsArrayBuffer = jsZip.generate(generateOptions);

			this.reportProgress(70, Locale.getMsg('generatingLogFiles'));
			Ext.defer(this.generateDocUploadRecord, 1, this, [ zipArchiveAsArrayBuffer ]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateZipArchive of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	generateDocUploadRecord: function(zipArchiveAsArrayBuffer) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			var now = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
			var fileSize = zipArchiveAsArrayBuffer.byteLength / 1024.0;
			var fileName = 'LogFiles.zip';
			var desc = 'HTML5_LogFiles';

			//create a doc upload record
			var logfilesAsDocUpload = Ext.create('AssetManagement.customer.model.bo.DocUpload', {
				fileName: fileName,
				fileType: 'ZIP',
				fileSize: fileSize,
				fileOrigin: 'local',
				fileDescription: desc,
				createdAt: now,
				lastModified: now,
				contentAsArrayBuffer: zipArchiveAsArrayBuffer,

				//own properties
				userId: AssetManagement.customer.core.Core.getAppConfig().getUserId(),
        scenario: AssetManagement.customer.core.Core.getAppConfig().getScenario(),
				docId: '',
				docType: 'ZIP',
				docSize: fileSize,
				description: desc,
				spras: 'D',
				filename: '!' + fileName,
				recordname: 'MC_DOCU_H_BIN',
				recordnameBin: 'MC_DOCU_BIN',
				orgFilename: fileName,
				timestamp: now
			});

			this.reportProgress(75, Locale.getMsg('contactingSapSystem'));
			Ext.defer(this.sendTraceToBackend, 1, this, [ logfilesAsDocUpload ]);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDocUploadRecord of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	sendTraceToBackend: function(docUpload) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

            var me = this;

			var onlineCallback = function(success) {
				if(!success) {
                    me.errorOccured(Locale.getMsg('browserIsOffline'));
                    return;
				} else {
                    //before the upload is started, check if the sap system does respond on a contact attempt
                    var contactSapCallback = function(success) {
                        try {
                            if (me._cancelWorkRequested || me._errorOccured) {
                                me.cancelWork();
                                return;
                            }

                            if(success === true) {
                                var uploadCallback = function(toUpload, uploaded) {
                                    try {
                                        if(toUpload === uploaded) {
                                            me.reportProgress(100, Locale.getMsg('sendingLogFiles'));
                                            Ext.defer(me.returnResult, 1, me, [ true ]);
                                        } else {
                                            me.errorOccured(Locale.getMsg('sapSystemDidNotProcessData'));
                                        }
                                    } catch(ex) {
                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendTraceToBackend of BaseTraceManager', ex);
                                        me.errorOccured();
                                    }
                                };

                                me.fireEvent('cancelableChanged', false);
                                me.reportProgress(85, Locale.getMsg('sendingLogFiles'));
                                me._fileUploadManager.uploadFile(docUpload, uploadCallback, me);
                            } else {
                                me.errorOccured(Locale.getMsg('sapSystemNotResponding'));
                            }
                        } catch(ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendTraceToBackend of BaseTraceManager', ex);
                            me.errorOccured();
                        }
                    };

                    me._connectionManager.tryContactSapSystem(contactSapCallback, this);
				}
			}

            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);
/*

			//stop, if client is offline at all
            if (!AssetManagement.customer.helper.NetworkHelper.isClientOnline()) {
                this.errorOccured(Locale.getMsg('browserIsOffline'));
                return;
            }



			//before the upload is started, check if the sap system does respond on a contact attempt
			var contactSapCallback = function(success) {
				try {
					if (me._cancelWorkRequested || me._errorOccured) {
						me.cancelWork();
						return;
					}

					if(success === true) {
						var uploadCallback = function(toUpload, uploaded) {
							try {
								if(toUpload === uploaded) {
									me.reportProgress(100, Locale.getMsg('sendingLogFiles'));
									Ext.defer(me.returnResult, 1, me, [ true ]);
								} else {
									me.errorOccured(Locale.getMsg('sapSystemDidNotProcessData'));
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendTraceToBackend of BaseTraceManager', ex);
								me.errorOccured();
							}
						};

						me.fireEvent('cancelableChanged', false);
						me.reportProgress(85, Locale.getMsg('sendingLogFiles'));
						me._fileUploadManager.uploadFile(docUpload, uploadCallback, me);
					} else {
						me.errorOccured(Locale.getMsg('sapSystemNotResponding'));
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendTraceToBackend of BaseTraceManager', ex);
					me.errorOccured();
				}
			};

			this._connectionManager.tryContactSapSystem(contactSapCallback, this);*/
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendTraceToBackend of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	returnResult: function(success) {
		try {
			if (this._cancelWorkRequested || this._errorOccured) {
				this.cancelWork();
				return;
			}

			if(!success)
				success = false;

			if(this._callback) {
				this._callback.call(this._callbackScope ? this._callbackScope : this, success);
			}

			this.reportWorkFinished(success);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside returnResult of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	getLogFileEntryForTraceObject: function(traceObject) {
		var retval = '';

		try {
			if(traceObject) {
				var time = AssetManagement.customer.utils.DateTimeUtils.parseTime(traceObject.time);
				var message = traceObject.message;
				var exception = traceObject.exception;

				retval += '[' + Ext.util.Format.date(time, this._traceTimestampFormat) + ']';

				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
					retval +=  ' ' + message;

				if(exception) {
					retval += '\r\n    Exception:';

					//examine further information
					var hasType = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(exception.type);
					var hasMessage = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(exception.message);
					var hasStack = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(exception.stack);

					var type = hasType ? exception.type : 'Unknown';
					retval += '\r\n        Type: ' + type;

					if(hasMessage)
						retval += '\r\n        Message: ' + exception.message;

					if(hasStack) {
						//only use the first code position information
						var stack = exception.stack;
						var firstPosition = 0;

						if(hasMessage) {
							firstPosition = stack.indexOf(exception.message) + exception.message.length;
						} else if(hasType) {
							firstPosition = stack.indexOf(exception.type) + exception.type.length;
						}

						var lastPosition = exception.stack.indexOf(')');
						var stackToPrint = lastPosition > 0 ? exception.stack.substring(firstPosition + 1, lastPosition + 1).trim() : exception.stack;

						if(stackToPrint.indexOf('at') === 0) {
							stackToPrint = stackToPrint.substring(3);
						}

						retval += '\r\n        Raised at: ' + stackToPrint.trim();
					}

					retval += '\r\n';
				}

				retval += '\r\n';
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLogFileEntryForTraceObject of BaseTraceManager', ex);
		}

		return retval;
	},

	reportProgress: function(curProcessPercent, message) {
		try {
			this._curProcessPercent = curProcessPercent;

			this.fireEvent('progressChanged', curProcessPercent, message);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportProgress of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	reportWorkFinished: function(success) {
		try {
			if(!success)
				success = false;

			this.fireEvent('workFinished', success);
		} catch(ex) {
			this.errorOccured();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkFinished of BaseTraceManager', ex);
		}
	},

	cancelWork: function() {
		try {
			this._cancelWorkRequested = false;
			this._callback = null;
			this._callbackScope = null;

			this.fireEvent('cancelled', null);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseTraceManager', ex);
			this.errorOccured();
		}
	},

	errorOccured: function(message) {
		try {
			this._cancelWorkRequested = false;
			this._errorOccured = true;

			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
				message = Locale.getMsg('generalSendLogFilesErrorOccured');

			this._lastErrorMessage = message;

			this.fireEvent('errorOccured', message);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccured of BaseTraceManager', ex);
		}
	},

    //private
    //generates ArrayBuffer through nativ blob conversion and fileReader api
    //has a massiv performance boost versus AssetManagement.customer.utils.StringUtils.utf8StringToArrayBuffer
	generateJSONArrayBufferFromDbObject: function (databaseObject, callback) {
	    if (!callback) {
	        this.errorOccured();
	        return;
	    }

	    try {
            //simply stringify the whole JSON object results in ui freeze
	        var stringArray = [];

	        for (var element in databaseObject) {
	            var helperObject = {};
	            helperObject[element] = databaseObject[element];
	            stringArray.push(JSON.stringify(helperObject, null, 4), ',');
	        }

            //remove trailing ','
	        stringArray.pop();
            var blob = new Blob(stringArray, { type: 'application/json' });
	        var f = new FileReader();
	        f.onload = function (e) {
	            callback(e.target.result);
	        };
	        f.readAsArrayBuffer(blob);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateJSONStringFromDbObject of BaseTraceManager', ex);
	    }

	}
});