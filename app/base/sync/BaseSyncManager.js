Ext.define('AssetManagement.base.sync.BaseSyncManager', {
    mixins: ['Ext.mixin.Observable'],

    requires: [
	    'Ext.util.MixedCollection',
//	    'AssetManagement.customer.controller.ClientStateController', //CAUSES RING DEPENDENCY
		'AssetManagement.customer.sync.UploadManager',
		'AssetManagement.customer.sync.ParsingManager',
		'AssetManagement.customer.sync.ConnectionManager',
		'AssetManagement.customer.sync.FileUploadManager',
        'AssetManagement.customer.sync.SyncRequestResponse',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.manager.CacheManager',
		'AssetManagement.customer.manager.UserInfoManager',
		'AssetManagement.customer.model.bo.UserInfo',
		'AssetManagement.customer.helper.OxLogger'
    ],

    //public:
    config: {
        syncError: "",
        uploadOnly: false
    },

    //private:
    _appConfig: null,
    _dataBaseHelper: null,
    _connectionManager: null,
    _uploadManager: null,
    _fileUploadManager: null,
    _parsingManager: null,
    _clientResetOccured: false,
    _cancelSyncRequested: false,
    _waitingForCancelation: false,
    _syncInProcess: false,
    _errorOccured: false,
    _curProcessPercent: 0,
    _curProcessStepRange: 0,
    _curProcessStepPercentage: 0,
    _waitingForUploadData: false,
    _waitingForFileUpload: false,
    _waitingForNetwork: false,
    _waitingForParsing: false,

    isSyncRunning: function () {
        var retval = false;

        try {
            retval = this._syncInProcess;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isSyncRunning of BaseSyncManager', ex);
        }

        return retval;
    },

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            if (this._instance === null)
                this._instance = Ext.create('AssetManagement.customer.sync.SyncManager');

            return this._instance;
        },

        resetInstance: function () {
            this._instance = null;
        }
    },

    constructor: function (config) {
        try {
            this.mixins.observable.constructor.call(this, config);

            this._appConfig = AssetManagement.customer.core.Core.getAppConfig();
            this._dataBaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();
            this._connectionManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                linkedSyncManager: this,
                listeners: {
                    networkActionCancelled: { fn: this.cancelRunningSync, scope: this },
                    cancelationInProgress: { fn: this.reportCancelationRequest, scope: this },
                    sendFailed: { fn: this.sendFailed, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });

            this._uploadManager = Ext.create('AssetManagement.customer.sync.UploadManager', {
                dataBaseHelper: this._dataBaseHelper,
                linkedSyncManager: this,
                listeners: {
                    cancelationInProgress: { fn: this.reportCancelationRequest, scope: this },
                    workCancelled: { fn: this.cancelRunningSync, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });

            this._parsingManager = Ext.create('AssetManagement.customer.sync.ParsingManager', {
                dataBaseHelper: this._dataBaseHelper,
                linkedSyncManager: this,
                listeners: {
                    progressChanged: { fn: this.innerProgressReporter, scope: this },
                    cancelationInProgress: { fn: this.reportCancelationRequest, scope: this },
                    workCancelled: { fn: this.cancelRunningSync, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });

            this._fileUploadManager = Ext.create('AssetManagement.customer.sync.FileUploadManager', {
                dataBaseHelper: this._dataBaseHelper,
                linkedSyncManager: this,
                listeners: {
                    cancelationInProgress: { fn: this.reportCancelationRequest, scope: this },
                    workCancelled: { fn: this.cancelRunningSync, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseSyncManager', ex);
        }
    },

    innerProgressReporter: function (percentage, message) {
        try {
            var additionalPercentage = percentage - this._curProcessStepPercentage;
            this._curProcessStepPercentage = percentage;
            this.reportSyncProgress(this._curProcessPercent + this._curProcessStepRange * additionalPercentage / 100, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside innerProgressReporter of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    //public methods
    //setting the upload only flag while a sync is running will be ignored
    setUploadOnly: function (uploadOnly) {
        try {
            if (!this._syncInProcess) {
                if (!uploadOnly)
                    uploadOnly = false;

                this._uploadOnly = uploadOnly;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUploadOnly of BaseSyncManager', ex);
        }
    },

    requestCancelation: function () {
        try {
            AssetManagement.customer.helper.OxLogger.logSyncMessage('A cancelation request has been triggered.');

            if (this._syncInProcess) {
                if (!this._cancelSyncRequested) {
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Passing cancelation signal into running process.');
                    this._cancelSyncRequested = true;

                    if (this._waitingForNetwork)
                        this._connectionManager.requestNetworkActionCancelation();
                    else if (this._waitingForUploadData)
                        this._uploadManager.requestWorkCancelation();
                    else if (this._waitingForPasing)
                        this._parsingManager.requestWorkCancelation();
                    else if (this._waitingForFileUpload)
                        this._fileUploadManager.requestWorkCancelation();
                    else
                        this.reportCancelationRequest();
                } else {
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Current sync. has already a pending cancelation request.');
                    this.reportCancelationRequest();
                }
            } else {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('No sync running, which could be cancelled.');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestCancelation of BaseSyncManager', ex);
            this.errorOccured('An error occured while requesting a cancelation');
        }
    },

    //will try to confirm a sync guid
    confirmSyncGUID: function (syncGuid) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(syncGuid)) {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('Confirming sync guid.');

                var nameValuePairList = this.getConfirmSyncGUIDPostValues(syncGuid);

                var callback = function (responseAsString) {
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Confirm sync guid request returned with a response of length: ' + (responseAsString ? responseAsString.length : 0));
                };

                //because, there is no action to perfom when the data is received,
                //waiting status for network will not be set
                this._connectionManager.sendToBackEnd.call(this._connectionManager,
						AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.DATASYNC, nameValuePairList, callback);
            } else {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('Can not confirm response. No confirm guid has been found');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmSyncGUID of BaseSyncManager', ex);
        }
    },

    //method starts a callback-sequence of private methods
    beginSynchronize: function () {
        try {
            AssetManagement.customer.helper.OxLogger.logSyncMessage('A synchronization has been triggered.');

            if (this._syncInProcess) {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('A synchronization could not be started, because another is already running.');
                return;
            }

            //first step of the sychronization process. Next steps will be started
            //sequentielly by it's corresponding former step
            this.startSync();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginSynchronize of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    reportSyncProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.fireEvent('progressChanged', curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportSyncProgress of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    reportWorkFinished: function () {
        try {
            this.fireEvent('workFinished', null);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkFinished of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    sendFailed: function (failedType) {
        try {
            var message = '';

            //determine the kind of error, that occured and get the adequate message for it
            switch (failedType) {
                case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                    message = Locale.getMsg('browserIsOffline');
                    break;
                case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                    message = Locale.getMsg('connectionTimeoutOccured');
                    break;
                case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                default:
                    message = Locale.getMsg('generalConnectionErrorOccured');
                    break;
            }

            //trigger a error occurence to stop the current sync
            this.errorOccured(message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendFailed of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    errorOccured: function (message) {
        try {
            AssetManagement.customer.helper.OxLogger.logSyncMessage('Aborting current sync, due to an error.');

            this._waitingForNetwork = false;
            this._waitingForUploadData = false;
            this._waitingForParsing = false;
            this._syncInProcess = false;
            this._cancelSyncRequested = false;

            this._errorOccured = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = Locale.getMsg('generalSyncErrorOccured');

            this.setSyncError(message);

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Error message: ' + message);
            AssetManagement.customer.helper.OxLogger.logSyncMessage('---------SYNC END---------');

            AssetManagement.customer.helper.OxLogger.disableCachingForPersistentTrace();

            this.fireEvent('errorOccured', message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOcurred of BaseSyncManager', ex);
        }
    },

    reportCancelationRequest: function () {
        try {
            this.fireEvent('cancelationInProgress', Locale.getMsg('cancellingSync'));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportCancelationRequest of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    cancelRunningSync: function () {
        try {
            AssetManagement.customer.helper.OxLogger.logSyncMessage('Cancelling current syncronization.');
            AssetManagement.customer.helper.OxLogger.logSyncMessage('---------SYNC END---------');

            AssetManagement.customer.helper.OxLogger.disableCachingForPersistentTrace();

            this._syncInProcess = false;
            this._cancelSyncRequested = false;

            this.fireEvent('cancelled', null);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelRunningSync of BaseSyncManager', ex);
            this.errorOccured();
        }
    },

    //previous step: -
    //following step: checkUserStatus
    startSync: function () {
        try {
            //before sync starts, enable caching for trace
            AssetManagement.customer.helper.OxLogger.enableCachingForPersistentTrace();

            AssetManagement.customer.helper.OxLogger.logSyncMessage('---------SYNC START---------');

            if (this._uploadOnly) {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('This synchronization will upload data ONLY.');
            }

            this._clientResetOccured = false;
            this._waitingForNetwork = false;
            this._waitingForUploadData = false;
            this._waitingForParsing = false;
            this._cancelSyncRequested = false;
            this._errorOccured = false;
            this._syncInProcess = true;
            this._curProcessPercent = 0;
            this._curProcessStepRange = 0;
            this._curProcessStepPercentage = 0;

            this.reportSyncProgress(0, Locale.getMsg('startingSync'));

            this.checkUserStatus.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startSync of BaseSyncManager', ex);
            this.errorOccured('An error occured while starting the sync');
        }
    },

    //previous step: startSync
    //following step: performDeviceCheck
    checkUserStatus: function () {
        try {
            var me = this;
            var ac = this._appConfig;
            var mandt = ac.getMandt();
            var userId = ac.getUserId();

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Beginning sync. communication.');
            AssetManagement.customer.helper.OxLogger.logSyncMessage('Validating current user: ' + mandt + '/' + userId);

            var callback = function (userInfo) {
                try {
                    this._waitingForNetwork = false;
                    if (this._cancelSyncRequested || this._errorOccured) {
                        this.cancelRunningSync();
                        return;
                    }

                    if (userInfo) {
                        if (userInfo.isValid()) {
                            //checkUserStatus completes here
                            //next step: perform a device check
                            AssetManagement.customer.helper.OxLogger.logSyncMessage('User validation has been successful.');

                            this.reportSyncProgress(15, Locale.getMsg('deviceCheckRunning'));
                            Ext.defer(this.performDeviceCheck, 1, this);
                        } else {
                            AssetManagement.customer.helper.OxLogger.logSyncMessage('User validation failed.');

                            var errorMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('authFailedWithMsg'), [userInfo.getErrorMessage()]);
                            this.errorOccured(errorMessage);
                        }
                    } else {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('Backend did not respond on suer validation.');
                        this.errorOccured(Locale.getMsg('errorContactingAuth'));
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserStatus of BaseSyncManager', ex);
                    me.errorOccured('An error occured while validating the current user');
                }
            };

            this._waitingForNetwork = true;
            this.reportSyncProgress(10, Locale.getMsg('authenticatingUser'));
            Ext.defer(this._connectionManager.getUserInfo, 1, this._connectionManager, [mandt, userId, ac.getUserPassword(), null, callback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkUserStatus of BaseSyncManager', ex);
            this.errorOccured('An error occured while validating the current user');
        }
    },

    //previous step: checkUserStatus
    //following step: perpareUploadData
    performDeviceCheck: function () {
        try {
            var me = this;

            var deviceCheckCallback = function (response) {
                try {
                    var resp = null;

                    if (response) {
                        var decodedResp = Ext.decode(response);
                        resp = decodedResp.Results;
                    }

                    if (resp && resp.TYPE === 'S') {
                        //device check successfull
                        //next step: perform a device check
                        me.reportSyncProgress(25, Locale.getMsg('prepareSyncData'));
                        Ext.defer(me.perpareUploadData, 1, me);
                    } else {
                        //log all messages and extract the first error message
                        var returnObjects = resp.RETURN_TAB;
                        var firstReturnMessage = '';

                        if (returnObjects && returnObjects.length > 0) {
                            Ext.Array.each(returnObjects, function (returnObject) {
                                var retMessage = returnObject.MESSAGE;

                                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retMessage)) {
                                    if (firstReturnMessage === '')
                                        firstReturnMessage = retMessage;

                                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Error message: ' + retMessage);
                                }
                            });
                        }

                        var errorMessage = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(firstReturnMessage) ? Locale.getMsg('deviceCheckNo') : firstReturnMessage;
                        me.errorOccured(errorMessage);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDeviceCheck of BaseSyncManager', ex);
                    me.errorOccured('An error occured while checking the device');
                }
            };

            var ac = this._appConfig;
            var nameValuePairList = Ext.create('Ext.util.MixedCollection');
            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("pa_device_id", ac.getDeviceId());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-language", ac.getSAPLanguageToSet());
            nameValuePairList.add("sap-password", ac.getUserPassword());

            Ext.defer(this._connectionManager.sendToBackEnd, 1, this._connectionManager,
            [AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.DEVICECHECK, nameValuePairList, deviceCheckCallback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDeviceCheck of BaseSyncManager', ex);
            this.errorOccured('An error occured while preparing upload data');
        }
    },

    //previous step: performDeviceCheck
    //following step: synchronizeWithBackEnd
    perpareUploadData: function () {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            var nextStep = function (uploadData) {
                try {
                    this._waitingForUploadData = false;
                    this.reportSyncProgress(35, Locale.getMsg('sendingDataToSAPSystem'));

                    AssetManagement.customer.helper.OxLogger.logSyncMessage('End get upload data.');

                    this.logForUploadData(uploadData);

                    Ext.defer(this.synchronizeWithBackEnd, 1, this, [uploadData]);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside perpareUploadData of BaseSyncManager', ex);
                    this.errorOccured('An error occured while preparing upload data');
                }
            };

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Begin get upload data.');

            this._waitingForUploadData = true;
            Ext.defer(this._uploadManager.getUploadData, 1, this._uploadManager, [null, nextStep, this]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside perpareUploadData of BaseSyncManager', ex);
            this.errorOccured('An error occured while preparing upload data');
        }
    },

    //previous step: perpareUploadData
    //following step: parseResponse
    synchronizeWithBackEnd: function (uploadData) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Preparing backend call.');

            var me = this;
            var nameValuePairList = this.getUploadPostValues(uploadData, this._uploadOnly);

            var callback = function (inboundStream) {
                try {
                    me._waitingForNetwork = false;

                    if (me._cancelSyncRequested || me._errorOccured) {
                        me.cancelRunningSync();
                        return;
                    }

                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Calling backend returned.');
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Received a response of length: ' + (inboundStream ? inboundStream.length : 0));

                    me.reportSyncProgress(45, Locale.getMsg('examinigSyncResponse'));
                    Ext.defer(me.parseResponse, 1, me, [inboundStream]);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside synchronizeWithBackEnd of BaseSyncManager', ex);
                    me.errorOccured('An error occured while calling the sap system');
                }
            };

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Calling backend.');

            this._waitingForNetwork = true;
            Ext.defer(this._connectionManager.sendToBackEnd, 1, this._connectionManager,
					[AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.DATASYNC, nameValuePairList, callback]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside synchronizeWithBackEnd of BaseSyncManager', ex);
            this.errorOccured('An error occured while calling the sap system');
        }
    },

    //previous step: synchronizeWithBackEnd
    //following step: examineReturnCodeAndMessages
    parseResponse: function (responseStream) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Converting response');

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(responseStream)) {
                var parsedResponse = null;
                var JSONresponse = null;

                try {
                    parsedResponse = Ext.decode(responseStream);

                    JSONresponse = Ext.create('AssetManagement.customer.sync.SyncRequestResponse', {
                        data: parsedResponse
                    });
                } catch (ex) {
                }

                this.logForSyncResponse(responseStream, parsedResponse);

                if (JSONresponse) {
                    Ext.defer(this.examineReturnCodeAndMessages, 1, this, [JSONresponse]);
                } else {
                    this.errorOccured('Invalid response structure.');
                }
            } else {
                //if the inboundStream contains nothing stop the process
                this.errorOccured('Aborting sync - Response is empty.');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseResponse of BaseSyncManager', ex);
            this.errorOccured('An error occured while calling the sap system');
        }
    },

    //previous step: synchronizeWithBackEnd
    //following step: checkForAndHandleClientResetRequirement
    examineReturnCodeAndMessages: function (JSONresponse) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            this.fireEvent('cancelableChanged', false);

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Begin examining sync. response.');

            var me = this;

            //evaluate the retrun code and messages
            //if there is an error stop the sync process
            var hasSyncError = JSONresponse.isErronous();
            var returnMessagesArray = JSONresponse.getGeneralReturnMessages();

            Ext.Array.each(returnMessagesArray, function (returnMessage) {
                if (returnMessage.TYPE === 'E') {
                    var stringInfo = returnMessage.ID + ' - ' + returnMessage.MESSAGE;
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Sync. response contains an error: ' + stringInfo);
                } else if (returnMessage.TYPE === 'S') {
                    var stringInfo = returnMessage.MESSAGE;
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Sync. response contains a success message: ' + stringInfo);
                }
            }, this);

            if (hasSyncError) {
                var firstErrorMessage = JSONresponse.getFirstGeneralErrorReturnMessage();
                var stringInfo = returnMessageObject.ID + ' - ' + returnMessageObject.MESSAGE;
                var errorStringMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('syncReturnedWithError'), [stringInfo]);

                AssetManagement.customer.helper.OxLogger.logSyncMessage('Aborting sync. because a sync error has been found on the sync response.');
                AssetManagement.customer.helper.OxLogger.logSyncMessage('End examining sync. response.');
                this.errorOccured(errorStringMessage);
                return;
            } else {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('End examining sync. response.');

                Ext.defer(this.checkForAndHandleClientResetRequirement, 1, this, [JSONresponse]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside examineReturnCodeAndMessages of BaseSyncManager', ex);
            this.errorOccured('An error occured while examing the sync response');
        }
    },

    //previous step: examineReturnCodeAndMessages
    //next step: confirmUploadData or parseData
    checkForAndHandleClientResetRequirement: function (JSONresponse) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            if (JSONresponse.getClientReset()) {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('Sync. response contains the reset client flag.');
                this._clientResetOccured = true;
            } else {
                this._clientResetOccured = false;
            }

            var me = this;

            var clientResetHandled = function (success) {
                try {
                    if (me._cancelSyncRequested || me._errorOccured) {
                        me.cancelRunningSync();
                        return;
                    }

                    if (!success) {
                        me.errorOccured('Client reset failed');
                        return;
                    }

                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Client reset has been successful.');

                    //check what to do next
                    var uploadConfRequired = false;
                    var confGuids = JSONresponse.getConfGUIDs();

                    if (confGuids && confGuids.length > 0) {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('Found packet guids to confirm.');
                        me.reportSyncProgress(50, Locale.getMsg('confirmingUploadedData'));

                        Ext.defer(me.confirmUploadData, 1, me, [JSONresponse]);
                    } else {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('No packet guids to confirm found.');

                        me.reportSyncProgress(60, Locale.getMsg('parsingReceivedData'));
                        Ext.defer(me.parseData, 1, me, [JSONresponse]);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleClientResetRequirement of BaseSyncManager', ex);
                    me.errorOccured('An error occured while checking for reseting client requirement');
                }
            };

            if (this._clientResetOccured) {
                //all data of the current user will be deleted from the data stores and the sync delta store will be reinitialized
                //assuming the backend only sends the triggering flag if it successfully processed all uploaded data
                //docuploads may not be deleted - uploading documents will be executed after the parsing job
                AssetManagement.customer.helper.OxLogger.logSyncMessage('Triggering a client reset.');

                this.reportSyncProgress(48, Locale.getMsg('resettingClient'));
                Ext.defer(AssetManagement.customer.controller.ClientStateController.resetClient, 1, AssetManagement.customer.controller.ClientStateController, [clientResetHandled, this, ['S_DOCUPLOAD']]);
            } else {
                clientResetHandled(true);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleClientResetRequirement of BaseSyncManager', ex);
            this.errorOccured('An error occured while checking for reseting client requirement');
        }
    },

    //previous step: checkForAndHandleClientResetRequirement
    //following step: parseData
    confirmUploadData: function (JSONresponse) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Begin confirming uploaded data.');

            var me = this;
            var callback = function () {
                try {
                    this._waitingForUploadData = false;

                    if (this._cancelSyncRequested || this._errorOccured) {
                        this.cancelRunningSync();
                        return;
                    }

                    AssetManagement.customer.helper.OxLogger.logSyncMessage('End confirming uploaded data.');

                    this.reportSyncProgress(60, Locale.getMsg('parsingReceivedData'));
                    Ext.defer(this.parseData, 1, this, [JSONresponse]);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadData of BaseSyncManager', ex);
                    me.errorOccured('An error occured while confirming uploaded data');
                }
            };

            this._waitingForUploadData = true;
            Ext.defer(this._uploadManager.confirmUploadData, 1, this._uploadManager, [JSONresponse.getConfGUIDs(), callback, this]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmUploadData of BaseSyncManager', ex);
            this.errorOccured('An error occured while confirming uploaded data');
        }
    },

    //previous step: checkForAndHandleClientResetRequirement or confirmUploadData
    //following step: confirmSync
    parseData: function (JSONresponse) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Begin parsing incoming data.');

            this._curProcessStepRange = 25;

            var me = this;
            var callback = function (parseResults) {
                try {

                    if(parseResults.get('layout')) {
                        AssetManagement.customer.controller.ClientStateController.resetAllViews()
                    }
                    me._waitingForParsing = false;

                    if (me._cancelSyncRequested || me._errorOccured) {
                        me.cancelRunningSync();
                        return;
                    }

                    AssetManagement.customer.helper.OxLogger.logSyncMessage('End parsing incoming data.');
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Parsed records count: ' + (parseResults ? parseResults.get('totalCounter') : 0));

                    if (parseResults) {
                        var missingStoresMap = parseResults.get('missingStores');

                        if (missingStoresMap && missingStoresMap.getCount() > 0) {
                            var missingStoresString = "Couldn't find store schema for stores:";

                            missingStoresMap.each(function (storeName) { missingStoresString += ' ' + storeName + ','; });
                            missingStoresString = AssetManagement.customer.utils.StringUtils.removeLastCharacterOfString(missingStoresString);

                            AssetManagement.customer.helper.OxLogger.logSyncMessage(missingStoresString);
                        }
                    }

                    me._curProcessStepRange = 0;
                    me._curProcessStepPercentage = 0;
                    me.reportSyncProgress(85, Locale.getMsg('confirmingSyncResponse'));
                    Ext.defer(me.confirmSync, 1, me, [JSONresponse, parseResults]);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseData of BaseSyncManager', ex);
                    me.errorOccured('An error occured while parsing incoming data');
                }
            };

            //parsing data starts here
            this._waitingForParsing = true;
            Ext.defer(this._parsingManager.parseData, 1, this._parsingManager, [JSONresponse.getSyncData(), JSONresponse.getSyncTimeStamp(), false, callback, this]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseData of BaseSyncManager', ex);
            this.errorOccured('An error occured while parsing incoming data');
        }
    },

    //previous step: parseData 
    //following step: adaptClientToSync
    confirmSync: function (JSONresponse) {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            var me = this;

            this.confirmSyncGUID(JSONresponse.getSyncGUID());

            me.reportSyncProgress(86, Locale.getMsg('adaptingClientToNewData'));
            Ext.defer(this.adaptClientToSync, 1, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmSync of BaseSyncManager', ex);
            this.errorOccured('An error occured while confirming sync response');
        }
    },

    //previous step: confirmSync
    //following step: manageLoggingSize
    adaptClientToSync: function () {
        try {
            this._waitingForNetwork = false;

            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Begin adapting client to new data state.');

            var me = this;

            if (this._clientResetOccured)
                this._appConfig.setSyncResetClient(false);

            AssetManagement.customer.manager.CacheManager.clearCaches();

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Client successfully adapted to new data state.');

            var triggerNextStep = function () {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('End adapting client to new data state.');
                Ext.defer(me.manageLoggingSize, 1, me);
            };

            //on an upload only sync customizing will only change, if the response contained the client reset flag
            //reinitialization will only be neccessary in this case
            if (this._uploadOnly === true && !this._clientResetOccured) {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('Reinitialization of UserInfo and FuncPara is not neccessary.');
                triggerNextStep();
            } else {
                var custInitCallback = function (success) {
                    try {
                        if (success === true) {
                            AssetManagement.customer.helper.OxLogger.logSyncMessage('Reinitialization of runtime customizing successful.');
                            triggerNextStep();
                        } else {
                            AssetManagement.customer.helper.OxLogger.logSyncMessage('Reinitialization of UserInfo and FuncPara failed.');
                            AssetManagement.customer.helper.OxLogger.logSyncMessage('Missing essential user customizing.');

                            me.errorOccured('Reinitialization of UserInfo and FuncPara failed. Missing essential user customizing.');
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adaptClientToSync of BaseSyncManager', ex);
                        me.errorOccured('An error occured while adaopting client to new data state');
                    }
                };

                AssetManagement.customer.helper.OxLogger.logSyncMessage('Reinitializing runtime customizing.');
                AssetManagement.customer.controller.ClientStateController.initializeRuntimeCustomizing(custInitCallback, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adaptClientToSync of BaseSyncManager', ex);
            this.errorOccured('An error occured while adapting client to new data state');
        }
    },

    //previous step: adaptClientToSync
    //following step: performFileUploads
    manageLoggingSize: function () {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            var me = this;

            var logManagementCallback = function (success) {
                try {
                    AssetManagement.customer.helper.OxLogger.logSyncMessage('Logging size management done.');

                    me.reportSyncProgress(90, Locale.getMsg('uploadingFiles'));
                    Ext.defer(me.performFileUploads, 1, me);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLoggingSize of BaseSyncManager', ex);
                    this.errorOccured('An error occured while managing logging size');
                }
            };

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Triggering logging size management.');
            var logManagementEventId = AssetManagement.customer.helper.OxLogger.manageLogDatabaseSize();

            if (logManagementEventId > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(logManagementEventId, logManagementCallback);
            } else {
                logManagementCallback(false);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLoggingSize of BaseSyncManager', ex);
            this.errorOccured('An error occured while managing logging size');
        }
    },

    //previous step: manageLoggingSize
    //following step: setSyncComplete
    performFileUploads: function () {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Starting file uploads.');

            var me = this;

            var fileUploadCallback = function (filesUploadCount, filesSuccessCount) {
                try {
                    if (this._cancelSyncRequested || this._errorOccured) {
                        this.cancelRunningSync();
                        return;
                    }

                    if (filesUploadCount != filesSuccessCount) {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('Could not upload all files. ' + filesSuccessCount + '/' + filesUploadCount + ' done.');
                    } else {
                        AssetManagement.customer.helper.OxLogger.logSyncMessage('All files successfully uploaded (' + filesUploadCount + ').');
                    }

                    Ext.defer(me.setSyncComplete, 1, me);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performFileUploads of BaseSyncManager', ex);
                    me.errorOccured('An error occured while uploading files');
                }
            };

            this._waitingForFileUpload = true;
            Ext.defer(this._fileUploadManager.uploadFiles, 1, this._fileUploadManager, ['', fileUploadCallback, this]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performFileUploads of BaseSyncManager', ex);
            this.errorOccured('An error occured while uploading files');
        }
    },

    //previous step: performFileUploads
    //following step: -
    setSyncComplete: function () {
        try {
            if (this._cancelSyncRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Setting sync complete.');

            this._waitingForUploadData = false;
            this._waitingForParsing = false;
            this._syncInProcess = false;
            this._cancelSyncRequested = false;

            this._appConfig.setLastSync(AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC'));

            AssetManagement.customer.helper.OxLogger.logSyncMessage('Sync finished.');
            AssetManagement.customer.helper.OxLogger.logSyncMessage('---------SYNC END---------');

            AssetManagement.customer.helper.OxLogger.disableCachingForPersistentTrace();

            this.reportSyncProgress(100, Locale.getMsg('syncComplete'));
            this.reportWorkFinished();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncComplete of BaseSyncManager', ex);
            this.errorOccured('An error occured while setting sync complete');
        }
    },

    getUploadPostValues: function (uploadData, forUploadOnly) {
        var nameValuePairList = Ext.create('Ext.util.MixedCollection');

        try {
            var ac = this._appConfig;
            var lang = ac.getLocale().substr(0, 2)

            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());
            //nameValuePairList.add("OnInputProcessing", "post");
            nameValuePairList.add("sap-language", ac.getSAPLanguageToSet());

            //set the upload data if present
            if (uploadData) {
                nameValuePairList.add("PAYLOAD", JSON.stringify(uploadData));
            }

            var flagUploadOnly = forUploadOnly ? 'X' : '';
            var flagResetData = forUploadOnly && ac.getSyncResetClient() ? 'X' : '';

            nameValuePairList.add("UPLOADONLY", flagUploadOnly);
            nameValuePairList.add("RESETCLIENT", flagResetData);

            nameValuePairList.add("REQSYNCDATA", 'X');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadPostValues of BaseSyncManager', ex);
        }

        return nameValuePairList;
    },

    getConfirmSyncGUIDPostValues: function (confirmGUID) {
        var nameValuePairList = Ext.create('Ext.util.MixedCollection');

        try {
            var ac = this._appConfig;
            var lang = ac.getLocale().substr(0, 2);

            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("CONFIRMSYNCGUID", confirmGUID);
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());
            //nameValuePairList.add("OnInputProcessing", "post");
            nameValuePairList.add("sap-language", ac.getSAPLanguageToSet());
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getConfirmSyncGUIDPostValues of BaseSyncManager', ex);
        }

        return nameValuePairList;
    },

    logForUploadData: function (uploadData) {
        try {
            var recordsCount = uploadData ? uploadData['REC_COUNT'] : 0;

            if (recordsCount > 0) {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('Upload data: ' + recordsCount + ' records');
            } else {
                AssetManagement.customer.helper.OxLogger.logSyncMessage('No data for upload.');
            }

            if (AssetManagement.customer.core.Core.getAppConfig().isDebugMode()) {
                var uploadPackages = uploadData ? uploadData['PACKAGES'] : null;

                if (uploadPackages && uploadPackages.length > 0) {
                    console.log('Upload packages:');
                    console.log(uploadData);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logForUploadData of BaseSyncManager', ex);
        }
    },

    logForSyncResponse: function (responseAsString, jsonParsedResponse) {
        try {
            if (AssetManagement.customer.core.Core.getAppConfig().isDebugMode()) {
                if (jsonParsedResponse) {
                    console.log('Sync response');
                    console.log(jsonParsedResponse);
                } else {
                    console.log('Invalid response structure');
                    console.log(responseAsString);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logForSyncResponse of BaseSyncManager', ex);
        }
    }
});