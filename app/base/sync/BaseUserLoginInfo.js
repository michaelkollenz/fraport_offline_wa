Ext.define('AssetManagement.base.sync.BaseUserLoginInfo', {
	requires: [
	    'Ext.util.MixedCollection',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	//public:
	config: {
	    infoString: '',
        username: ''
	},
	
	//private:
	_lastLogon: '',
	_lastSync: '',
	_pwState1: '',
	_pwState2: '',
	_lockState: '',
	_subrcUser: '',
	_subrcPw: '',
	_locnt: '',
	_deviceId: '',
	_sm02Msg: '',
	_sysId: '',
	_symdt: '',
	_userId: '',
	_username: '',
	_countObj: '',
	_verVersion: '',
	_verMajorRelease: '',
	_verMinorRelease: '',
	_verPatchLevel: '',
	_verValidFrom: '',
	_verValidTo: '',
	_verClientOs: '',
	_verFilename: '',
	_errorMessage: '',
	_changePasswordRequired: false,
	
	
	//private:	
	constructor: function(config) {
		try {
		    this.initConfig(config);
		   
		    var caseOffline = config && config.fromOffline === true;

		    if (config) {
		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(config.infoString)) {
		            this.initPropertiesByInfoJSON(config.infoString);
		            //this.initPropertiesByInfoString(config.infoString);
		        } else if (caseOffline) {
		            this._username = config.username;
		        }
		    }
		    if (!caseOffline)
		        this.initErrorMessage();

	    } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseUserLoginInfo', ex);
	    }
	},
	
	//public methods
	getErrorMessage: function() {
		var retval = '';
		
		try {
		    retval = this._errorMessage;
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getErrorMessage of BaseUserLoginInfo', ex);
	    }
		
	    return retval;
	},
	
	setErrorMessage: function(message) {
	    try {
		    this._errorMessage = message;
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setErrorMessage of BaseUserLoginInfo', ex);
        }
	},
	
	getChangePasswordRequired: function() {
		var retval = '';
		
        try {
            retval = this._changePasswordRequired;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getChangePasswordRequired of BaseUserLoginInfo', ex);
        }

        return retval;
	},

	isValid: function() {
		var retval = '';
		
        try {
            retval = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isValid of BaseUserLoginInfo', ex);
        }

        return retval;
	},
	
	getLastLogon: function() {
		var retval = null;
		
        try {
            retval = this._lastLogon;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLastLogon of BaseUserLoginInfo', ex);
        }

        return retval;

	},

	getLastSync: function() {
		var retval = null;
		
        try {
            retval = this._lastSync;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLastSync of BaseUserLoginInfo', ex);
        }

        return retval;
	},

	getPwState1: function() {
		var retval = '';
		
        try {
            retval = this._pwState1;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPwState1 of BaseUserLoginInfo', ex);
        }

        return retval;
	},

	getPwState2: function() {
	    var retval = '';
	
        try {
            retval = this._pwState2;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPwState2 of BaseUserLoginInfo', ex);
        }

        return retval;
	},

	getLockState: function() {
		var retval = '';
		
        try {
            retval = this._lockState;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLockState of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getSubrcUser: function() {
		var retval = '';
		
        try {
            retval = this._subrcUser;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSubrcUser of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getSubrcPw: function() {
		var retval = '';
		
        try {
            retval = this._subrcPw;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSubrcPw of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getLocnt: function() {
		var retval = '';
		
        try {
            retval = this._locnt;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLocnt of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getDeviceId: function() {
		var retval = '';
		
        try {
            retval = this._deviceId;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeviceId of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getSm02Msg: function() {
		var retval = '';
		
        try {
           retval = this._sm02Msg;
        } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSm02Msg of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getSysId: function() {
		var retval = '';
		
        try {
            retval = this._sysId;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSysId of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getSymdt: function() {
		var retval = '';
		
        try {
            retval = this._symdt;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSymdt of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getUserId: function() {
		var retval = '';
		
        try {
            retval = this._userId;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserId of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getUsername: function() {
		var retval = '';
		
        try {
            retval = this._username;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserName of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getCountObj: function() {		
		var retval = '';
		
        try {
            retval = this._countObj;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCountObj of BaseUserLoginInfo', ex);
        } 

        return retval;
	},         

	getVerVersion: function() {
		var retval = '';
		
        try {
            retval = this._verVersion;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerVersion of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	getVerMajorRelease: function() {
		var retval = '';
		
        try {
            retval = this._verMajorRelease;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerMajorRelease of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	getVerMinorRelease: function() {
		var retval = '';
		
        try {
            retval = this._verMinorRelease;
        } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerMinorRelease of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	getVerPatchLevel: function() {
		var retval = '';
		
        try {
            retval = this._verPatchLevel;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerPatchLevel of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	getVerValidFrom: function() {
		var retval = null;
		
        try {
            retval = this._verValidFrom;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerValidFrom of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	getVerValidTo: function() {
		var retval = null;
		
        try {
            retval = this._verValidTo;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerValidTo of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	getVerClientOs: function() {
		var retval = '';
		
        try {
            retval = this._verClientOs;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerClientOs of BaseUserLoginInfo', ex);
        } 

        return retval;
	},

	getVerFilename: function() {
		var retval = '';
		
        try {
            retval = this._verFilename;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVerFilename of BaseUserLoginInfo', ex);
        } 

        return retval;
	},
	
	//private methods
	initPropertiesByInfoString: function(infoString) {
		try {
			var infos = Ext.Object.fromQueryString(infoString);
				    
			if(infos) {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.last_logon))
					this._lastLogon = AssetManagement.customer.utils.DateTimeUtils.parseTime(infos.last_logon.trim());
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.last_sync))
					this._lastSync = AssetManagement.customer.utils.DateTimeUtils.parseTime(infos.last_sync.trim());
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.pwdstate1))
					this._pwState1 = infos.pwdstate1.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.pwdstate2))
					this._pwState2 = infos.pwdstate2.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.lock_state))
					this._lockState = infos.lock_state.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.subrc_user))
					this._subrcUser = infos.subrc_user.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.subrc_pw))
					this._subrcPw = infos.subrc_pw.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.locnt))
					this._locnt = infos.locnt.trim();
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.device_id))
					this._deviceId = infos.device_id.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.sm02msg))
					this._sm02Msg = infos.sm02msg.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.sysid))
					this._sysId = infos.sysid.trim();
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.symdt))
					this._symdt = infos.symdt.trim();
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.userid))
					this._userId = infos.userid.trim();
						
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.username))
					this._username = infos.username.trim();
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.count_obj))
					this._countObj = infos.count_obj.trim();
					
				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_version))
				    this._verVersion = infos.ver_version.trim();

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_major_release))
				    this._verMajorRelease = infos.ver_major_release.trim();

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_minor_release))
				    this._verMinorRelease = infos.ver_minor_release.trim();

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_patch_lvl))
				    this._verPatchLevel = infos.ver_patch_lvl.trim();

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_valid_from))
				    this._verValidFrom = AssetManagement.customer.utils.DateTimeUtils.parseTime(infos.ver_valid_from.trim());

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_valid_to))
				    this._verValidTo = AssetManagement.customer.utils.DateTimeUtils.parseTime(infos.ver_valid_to.trim());

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_client_os))
				    this._verClientOs = infos.ver_client_os.trim();

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.ver_filename))
				    this._verFilename = infos.ver_filename.trim();

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.privatekey_b64))
				    this._privateDataKey = infos.privatekey_b64.trim();
			}
		} catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initPropertiesByInfoString of BaseUserLoginInfo', ex);
        } 
	},

	initPropertiesByInfoJSON: function (infoJSON) {
	    try {
	        var infos;

	        if (infoJSON !== "") {
	            var informationJSON = Ext.decode(infoJSON);

	            for (var propName in informationJSON) {
	                infos = informationJSON[propName];
	            }
	        }
	        
            if (infos) {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.LAST_LOGON))
                    this._lastLogon = AssetManagement.customer.utils.DateTimeUtils.parseTime(infos.LAST_LOGON);

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.LAST_SYNC))
	                this._lastSync = AssetManagement.customer.utils.DateTimeUtils.parseTime(infos.LAST_SYNC);

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.PWDSTATE1))
	                this._pwState1 = infos.PWDSTATE1;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.PWDSTATE2))
	                this._pwState2 = infos.PWDSTATE2;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.LOCK_STATE))
	                this._lockState = infos.LOCK_STATE;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.SUBRC_USER))
	                this._subrcUser = infos.SUBRC_USER;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.SUBRC_PW))
	                this._subrcPw = infos.SUBRC_PW;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.LOCNT))
	                this._locnt = infos.LOCNT;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.DEVICE_ID))
	                this._deviceId = infos.DEVICE_ID;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.SM02MSG))
	                this._sm02Msg = infos.SM02MSG;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.SYSTEMID))
	                this._sysId = infos.SYSTEMID;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.SYMDT))
	                this._symdt = infos.SYMDT;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.USERID))
	                this._userId = infos.USERID;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.USERNAME))
	                this._username = infos.USERNAME;

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.COUNT_OBJ))
	                this._countObj = infos.COUNT_OBJ;

	            if (infos.ASSIGNED_VERSION) {
	                var version = infos.ASSIGNED_VERSION;

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.VERSION))
	                    this._verVersion = version.VERSION;

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.MAJOR_REL))
	                    this._verMajorRelease = version.MAJOR_REL;

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.MINOR_REL))
	                    this._verMinorRelease = version.MINOR_REL;

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.PATCH_LVL))
	                    this._verPatchLevel = version.PATCH_LVL;

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.VALID_FROM))
	                    this._verValidFrom = AssetManagement.customer.utils.DateTimeUtils.parseTime(version.VALID_FROM);

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.VALID_TO))
	                    this._verValidTo = AssetManagement.customer.utils.DateTimeUtils.parseTime(version.VALID_TO);

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.CLIENT_OS))
	                    this._verClientOs = version.CLIENT_OS;

	                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(version.FILENAME))
	                    this._verFilename = version.FILENAME;
	            }

	            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infos.PRIVATEKEY_B64))
	                this._privateDataKey = infos.PRIVATEKEY_B64;
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initPropertiesByInfoJSON of BaseUserLoginInfo', ex);
	    }
	},
	
	initErrorMessage: function() {
	    try {
	        this._errorMessage = this.getGeneralErrorMessage();
	        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
	            return;

			this._errorMessage = this.getLockStateErrorMessage();
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
				return;
				
			this._errorMessage = this.getSubRcUserErrorMessage();
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
				return;
				
			this._errorMessage = this.getSubRcPwErrorMessage();
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
				return;			

			this._errorMessage = this.getPwStatus1ErrorMessage();
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
				return;
				
			this._errorMessage = this.getPwStatus2ErrorMessage();
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
				return;			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initErrorMessage of BaseUserLoginInfo', ex);
		}	
	},

	getGeneralErrorMessage: function () {
	    var retval = '';

	    try {
	        var infoString = this.getInfoString();

	        //example erroneous info string
	        //"RETURNMESSAGE;E;00;Keine RFC-Berechtigung f�r Benutzer C_SICF.;C_SICF;;;|"

	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(infoString)) {
	            retval = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('generalSAPLoginErrorOccured'), [Locale.getMsg('unknown')]);
	        } else if (AssetManagement.customer.utils.StringUtils.contains(infoString, 'RETURNMESSAGE;E;')) {
	            var messageStartIndex = infoString.indexOf('RETURNMESSAGE;E;') + 19; //16 + 3
	            var messageStopIndex = infoString.indexOf(';', messageStartIndex);
	            var extractedError = '';

	            if (messageStopIndex > -1) {
	                extractedError = infoString.substring(messageStartIndex, messageStopIndex);
	            } else {
	                extractedError = Locale.getMsg('unknown');
	            }

	            retval = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('generalSAPLoginErrorOccured'), [extractedError]);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralErrorMessage of BaseUserLoginInfo', ex);
	    }

	    return retval;
	},
	
	getLockStateErrorMessage: function() {
		var lockState = this._lockState;
	
		try {
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(lockState))
			   return null;
			
		    if(lockState == '32' || lockState == '96' || lockState == '160' || lockState == '224' || lockState == '*0' || lockState == '*4')
			   return Locale.getMsg('globalBlockedByAdmin');
		
		    if(lockState == '64' || lockState == '192' || lockState == '*2')
			   return Locale.getMsg('localBlockedByAdmin');
			
		    if(lockState == '128' || lockState == '*8')
			   return Locale.getMsg('userBlockedBecauseOfFailedLogInLimited');
		
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLockStateErrorMessage of BaseUserLoginInfo', ex);
		}
			
		return null;
	},
	
	getSubRcPwErrorMessage: function() {
		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._subrcPw))
			return null;
		
		try {
			var code = parseInt(this._subrcPw);
			
			switch(code) {
				case 1:
					return Locale.getMsg('passwordOrUsernameWrong');
				case 2:
					return Locale.getMsg('userBlockedByAdminOrFailedLogIn');
				case 3:
					return Locale.getMsg('userOutWithValidityDate');
				case 4:
					return Locale.getMsg('userCanLogInOnlyWithSNC');
				case 5:
					return Locale.getMsg('numberFaultyPasswordLogInExceeded');
				case 6:
					return Locale.getMsg('userMustNotByPasswordLogIn');
				case 7:
					return Locale.getMsg('userHasNotPassword');
				case 8:
					return Locale.getMsg('initialPasswordExpired');
				case 9:
					return Locale.getMsg('productivePasswordExpired');
				case 10:
					return Locale.getMsg('authenticationDataInsecureTransfer');
				case 11:
					return Locale.getMsg('ticketLogInIsInactive');
				case 12:
					return Locale.getMsg('logInTicketSyntacticallyWrongOrPerhapsGenerallySSFError');
				case 13:
					return Locale.getMsg('digitalSignatureTicketInvalidNotCheckable');
				case 14:
					return Locale.getMsg('ticketExhibitingSystemNotInACLTransaction');
				case 15:
					return Locale.getMsg('logInTicketIsExpired');
				case 16:
					return Locale.getMsg('logInInactive');
				case 17:
					return Locale.getMsg('certificationNotInBaseFormatOrPerhapsError');
				case 18:
					return Locale.getMsg('userIDCanAliasUnassigned');
				case 19:
					return Locale.getMsg('logInInactiveTransaction');
				case 20:
					return Locale.getMsg('missingMappingEntryOnTable');
				case 21:
					return Locale.getMsg('ambiguousClassificationOnTable');
				case 22:
					return Locale.getMsg('otherErrorIfSoNewErrorSituations');
				case 23:
					return Locale.getMsg('authenticationMethodIsNotSupportedHere');
				case 24:
					return Locale.getMsg('uncompletedAuthenticationData');
				case 25:
					return Locale.getMsg('aGeneralErrorOccured');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException(ex);
		}
		
		return null;
	},
	
	getSubRcUserErrorMessage: function () {
	    if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._subrcUser))
			return null;
		
		try {
			var code = parseInt(this._subrcUser);
			
			switch(code) {
				case 1:
					return Locale.getMsg('noAuthorizationForThisRequestUserPasswordIncorrect');
				case 2:
					return Locale.getMsg('userIDCanAliasUnassigned');
				case 3:
					return Locale.getMsg('incompleteCallParameters');
				case 4:
					return Locale.getMsg('userNotExistForCurrentClient');
				case 5:
					return Locale.getMsg('otherErrorForExampleByProvidingDataAccess');
				case 6:
					return Locale.getMsg('aGeneralErrorOccured');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException(ex);
		}
	
		return null;
	},
	
	getPwStatus1ErrorMessage: function() {
		var status = this._pwState1;
		
		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(status))
			return null;
		
		status = status.toString();
		try {
			var status = status.trim();
			
			switch(status) {
				case '1':
					this._changePasswordRequired = true;
					return Locale.getMsg('passwordIsInitialAndHasToBeChanged');
				case '2':
					this._changePasswordRequired = true;
					return Locale.getMsg('passwordHasExpiredAnHasToBeChanged');
				case '3':
					this._changePasswordRequired = true;
					return Locale.getMsg('passwordHasToBeChangedBecauseOfNewGuidance');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException(ex);
		}
		
		return null;
	},
	
	getPwStatus2ErrorMessage: function() {
		var status = this._pwState2;
		
		if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(status))
			return null;
		
		status = status.toString();
		try {
			status = status.trim();
			
			switch(status) {
				case '1':
					this._changePasswordRequired = true;
					return Locale.getMsg('passwordIsInitialAndHasToBeChanged');
				case '2':
					this._changePasswordRequired = true;
					return Locale.getMsg('passwordHasExpiredAnHasToBeChanged');
				case '3':
					this._changePasswordRequired = true;
					return Locale.getMsg('passwordHasToBeChangedBecauseOfNewGuidance');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException(ex);
		}
		
		return null;
	}
});