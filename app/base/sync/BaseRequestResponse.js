﻿Ext.define('AssetManagement.base.sync.BaseRequestResponse', {
    requires: [
        'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.helper.OxLogger'
    ],

    //public
    config: {
        //whole response structure of the request
        data: null
    },

    //private
    constructor: function (config) {
        try {
            this.initConfig(config);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseRequestResponse', ex);
        }
    },

    //public
    //returns the whole response JSON structure
    getFullResponseJSON: function () {
        var retval = null;

        try {
            retval = this.getData().RESULT;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFullResponseJSON of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the response generation timestamp
    getTimeStamp: function () {
        var retval = null;

        try {
            retval = this.getFullResponseJSON().RETURN_TIMESTAMP;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeStamp of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns inner results, if available
    getResults: function () {
        var retval = null;

        try {
            retval = this.getFullResponseJSON().RESULTS;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFullResponseJSON of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the general return code on response header level
    getGeneralReturnCode: function () {
        var retval = -1;

        try {
            retval = this.getFullResponseJSON().RETURN_CODE;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralReturnCode of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the return messages of the response header level
    getGeneralReturnMessages: function () {
        var retval = null;

        try {
            retval = this.getFullResponseJSON().RETURN_TAB;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralReturnMessages of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns if the reponse is erronous on response header level
    isGeneralErronous: function () {
        var retval = false;

        try {
            retval = this.getGeneralReturnCode() != 0;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isGeneralErronous of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns if the reponse is erronous in general or on results, if present
    isErronous: function () {
        var retval = false;

        try {
            var generalErronous = this.isGeneralErronous();

            if (generalErronous) {
                retval = generalErronous;
            } else {
                retval = this.hasAnyErronousResult();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isErronous of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns if any result object ob the response is erronous
    hasAnyErronousResult: function () {
        var retval = false;

        try {
            var results = this.getResults();

            if (results && results.length > 0) {
                Ext.Array.each(results, function (result) {
                    //this statement checks if the value is present at all and not zero
                    if (result.RETURN_CODE) {
                        retval = true;
                        return false;
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasAnyErronousResult of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the first error return message of the response header level
    getFirstGeneralErrorReturnMessage: function () {
        var retval = null;

        try {
            retval = this.extractFirstReturnMessageOfType(this.getGeneralReturnMessages(), 'E');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFirstGeneralErrorReturnMessage of BaseRequestResponse', ex);
        }

        return retval;
    },

    //public
    //returns the first non success return message of the response header level
    getFirstGeneralNonSuccessReturnMessage: function () {
        var retval = null;

        try {
            retval = this.extractFirstReturnMessageOfType(this.getGeneralReturnMessages(), 'S');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFirstGeneralNonSuccessReturnMessage of BaseRequestResponse', ex);
        }

        return retval;
    },

    //private
    extractFirstReturnMessageOfType: function (returnMessages, type) {
        var retval = null;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(type) && returnMessages && returnMessages.length > 0) {
                Ext.Array.each(returnObjects, function (returnMessage) {
                    if (returnMessage.TYPE === type) {
                        retval = returnMessage;
                        return false;
                    }
                });
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractFirstReturnMessageOfType of BaseRequestResponse', ex);
        }

        return retval;
    }
});