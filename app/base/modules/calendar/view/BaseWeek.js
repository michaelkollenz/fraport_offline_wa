/**
 * @class AssetManagement.customer.modules.calendar.view.BaseWeek
 * @extends AssetManagement.customer.modules.calendar.DayView
 * <p>Displays a calendar view by week. This class does not usually need ot be used directly as you can
 * use a {@link AssetManagement.customer.modules.calendar.CalendarPanel CalendarPanel} to manage multiple calendar views at once including
 * the week view.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.base.modules.calendar.view.BaseWeek', {
    extend: 'AssetManagement.customer.modules.calendar.view.Day',

    
    /**
     * @cfg {Number} dayCount
     * The number of days to display in the view (defaults to 7)
     */
    dayCount: 7
});
