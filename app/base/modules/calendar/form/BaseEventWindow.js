/**
 * @class AssetManagement.customer.modules.calendar.form.BaseEventWindow
 * @extends Ext.Window
 * <p>A custom window containing a basic edit form used for quick editing of events.</p>
 * <p>This window also provides custom events specific to the calendar so that other calendar components can be easily
 * notified when an event has been edited via this component.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.base.modules.calendar.form.BaseEventWindow', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',
    extend: 'Ext.window.Window',

    
    requires: [
        'Ext.form.Panel',
        'AssetManagement.customer.modules.calendar.util.Date',
        'AssetManagement.customer.modules.calendar.data.EventModel',
        'AssetManagement.customer.modules.calendar.data.EventMappings'
    ],

    constructor: function(config) {
        var formPanelCfg = {
            xtype: 'form',
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            frame: false,
            bodyStyle: 'background:transparent;padding:5px 10px 10px;',
            bodyBorder: false,
            border: false,
            items: [
            /*{
            	itemId: 'Description',
            	xtype: 'calendarEditDescription',
            	name: 'description',
            	anchor: '100%'
            },*/

			{
			    itemId: 'aufnr',
			    name: AssetManagement.customer.modules.calendar.data.EventMappings.Description.name,
			    fieldLabel: Locale.getMsg('order'),                        
                autoHeight: true,
                xtype: 'displayfield',
			    allowBlank: false,
			    emptyText: '',
			    anchor: '100%',
			  	listeners: {
				    afterrender: function(component) {
						var me = this;
            			component.getEl().on('click', function() { 
            				me.showOrder();
            			});   
				    },
					scope: this
			  	}			
			},
            {
                itemId: 'title',
                name: AssetManagement.customer.modules.calendar.data.EventMappings.Title.name,
                fieldLabel: Locale.getMsg('description'),                        
                
                xtype: 'displayfield',
                allowBlank: true,
                height: 50,
                emptyText: Locale.getMsg('description')
            },
            {
                itemId: 'cal_statusCb',
                name: AssetManagement.customer.modules.calendar.data.EventMappings.Title.name,
                fieldLabel: Locale.getMsg('status'),
                xtype: 'oxcombobox',
                allowBlank: false,
                displayField: 'text',
                valueField: 'value',
                editable: false,
                queryMode: 'local',
                emptyText: Locale.getMsg('description'),
                anchor: '100%'
            },
            {
                xtype: 'daterangefield',
                itemId: 'date-range',
                name: 'dates',
                anchor: '100%',
                fieldLabel: Locale.getMsg('date')
            }]
        };
    
        this.callParent([Ext.apply({
            titleTextAdd: Locale.getMsg('addEvent'),
            titleTextEdit: Locale.getMsg('editEvent'),
            width: 650,
            autocreate: true,
            border: true,
            closeAction: 'hide',
            modal: false,
            resizable: false,
            buttonAlign: 'right',
            savingMessage: Locale.getMsg('save'),
            deletingMessage: Locale.getMsg('delete'),
            layout: 'fit',
    
            defaultFocus: 'title',
            onEsc: function(key, event) {
                        event.target.blur(); // Remove the focus to avoid doing the validity checks when the window is shown again.
                        this.onCancel();
                    },

            fbar: {

            	cls: 'toolbar_background',
            	items: [
            /*{
                xtype: 'tbtext',
                text: '<a href="#" id="tblink">'+Locale.getMsg('edit') +'</a>'
            },
            '->',*/
		            {
		                itemId: 'delete-btn',
		                cls: 'oxDialogButton',
		                text: Locale.getMsg('delete'),
		                disabled: false,
		                handler: this.onDelete,
		                scope: this,
		                minWidth: 150,
		                hideMode: 'offsets'
		            },
		            {
		                text: Locale.getMsg('save'),
		                cls: 'oxDialogButton',
		                disabled: false,
		                handler: this.onSave,
		                scope: this
		            },
		            {
		                text: Locale.getMsg('cancel'),
		                cls: 'oxDialogButton',
		                disabled: false,
		                handler: this.onCancel,
		                scope: this
		            }
	            ]
	         },
            
            items: formPanelCfg
        },
        config)]);
    },

    // private
    newId: 10000,
    _rec: null,
    _stsma: null,

    /**
     * @event eventadd
     * Fires after a new event is added
     * @param {AssetManagement.customer.modules.calendar.form.BaseEventWindow} this
     * @param {AssetManagement.customer.modules.calendar.EventRecord} rec The new {@link AssetManagement.customer.modules.calendar.EventRecord record} that was added
     */

    /**
     * @event eventupdate
     * Fires after an existing event is updated
     * @param {AssetManagement.customer.modules.calendar.form.BaseEventWindow} this
     * @param {AssetManagement.customer.modules.calendar.EventRecord} rec The new {@link AssetManagement.customer.modules.calendar.EventRecord record} that was updated
     */

    /**
     * @event eventdelete
     * Fires after an event is deleted
     * @param {AssetManagement.customer.modules.calendar.form.BaseEventWindow} this
     * @param {AssetManagement.customer.modules.calendar.EventRecord} rec The new {@link AssetManagement.customer.modules.calendar.EventRecord record} that was deleted
     */

    /**
     * @event eventcancel
     * Fires after an event add/edit operation is canceled by the user and no store update took place
     * @param {AssetManagement.customer.modules.calendar.form.BaseEventWindow} this
     * @param {AssetManagement.customer.modules.calendar.EventRecord} rec The new {@link AssetManagement.customer.modules.calendar.EventRecord record} that was canceled
     */

    /**
     * @event editdetails
     * Fires when the user selects the option in this window to continue editing in the detailed edit form
     * (by default, an instance of {@link AssetManagement.customer.modules.calendar.EventEditForm}. Handling code should hide this window
     * and transfer the current event record to the appropriate instance of the detailed form by showing it
     * and calling {@link AssetManagement.customer.modules.calendar.EventEditForm#loadRecord loadRecord}.
     * @param {AssetManagement.customer.modules.calendar.form.BaseEventWindow} this
     * @param {AssetManagement.customer.modules.calendar.EventRecord} rec The {@link AssetManagement.customer.modules.calendar.EventRecord record} that is currently being edited
     */

    // private
    initComponent: function() {
        this.callParent();

        this.formPanel = this.items.items[0];
    },

    // private
    afterRender: function() {
        this.callParent();

        this.el.addCls('ext-cal-event-win');

        //Ext.get('tblink').on('click', this.onEditDetailsClick, this);
        
        this.titleField = this.down('#title');
        this.dateRangeField = this.down('#date-range');
        this.calendarField = this.down('#calendar');
        this.deleteButton = this.down('#delete-btn');
        this.statusCb = this.down('#cal_statusCb');
    },
    
    // private
    onEditDetailsClick: function(e){
        e.stopEvent();
        this.updateRecord(this.activeRecord, true);
        this.fireEvent('editdetails', this, this.activeRecord, this.animateTarget);
    },

    /**
     * Shows the window, rendering it first if necessary, or activates it and brings it to front if hidden.
	 * @param {Ext.data.Record/Object} o Either a {@link Ext.data.Record} if showing the form
	 * for an existing event in edit mode, or a plain object containing a StartDate property (and 
	 * optionally an EndDate property) for showing the form in add mode. 
     * @param {String/Element} animateTarget (optional) The target element or id from which the window should
     * animate while opening (defaults to null with no animation)
     * @return {Ext.Window} this
     */
    show: function(o, animateTarget, stsma) {
        // Work around the CSS day cell height hack needed for initial render in IE8/strict:
        var me = this,
            anim = (Ext.isIE8 && Ext.isStrict) ? null: animateTarget,
            M = AssetManagement.customer.modules.calendar.data.EventMappings,
            data = {};

        this.callParent([anim, function(){
            me.titleField.focus(true);
        }]);
                
        this.deleteButton[o.data && o.data[M.RecNo.name] ? 'show': 'hide']();

        var rec,
        f = this.formPanel.form;

        if (o.data) {
            rec = o;
            this._rec = rec;
            this.setTitle(rec.phantom ? this.titleTextAdd : this.titleTextEdit);
            f.loadRecord(rec);
        }
        else {
        	
            this.setTitle(this.titleTextAdd);

            var start = o[M.StartDate.name],
                end = o[M.EndDate.name] || AssetManagement.customer.modules.calendar.util.Date.add(start, {hours: 1});
            
            data[M.StartDate.name] = start;
            data[M.EndDate.name] = end;
            data[M.IsAllDay.name] = !!o[M.IsAllDay.name] || start.getDate() != AssetManagement.customer.modules.calendar.util.Date.add(end, {millis: 1}).getDate();
            rec = new AssetManagement.customer.modules.calendar.data.EventModel(data);

            f.reset();
            f.loadRecord(rec);
        }
        
        this.titleField.setHeight('auto');
        
        stsma.filter('stsma', rec.data.Operation.get('stsma'));        
        this._stsma = stsma;
        var activeStatusList = rec.data.Operation.get('objectStatusList');
        var activeStatus = this.getActiveStatus(activeStatusList);
        var activeStatusRecord = null;
		var store = Ext.create('Ext.data.Store', {
		    fields: ['text', 'value']
		});
		stsma.each(function(status) {
			store.add({ "text": status.get('txt04') + " " + status.get('txt30'), "value": status.get('estat')});
			if(activeStatus != null) {
				if(status.get('estat') == activeStatus.get('stat'))
					activeStatusRecord = status.get('estat');
			}
		}, this);
        if(activeStatusList.data.length >= 1) {
        	this.statusCb.clearValue();
        	this.statusCb.bindStore(store);	  

    		var statusRecord = activeStatusRecord ? this.statusCb.findRecordByValue(activeStatusRecord) : null;	

			if(statusRecord)
				this.statusCb.setValue(statusRecord);
        	this.statusCb.show();
        }
        else 
        	this.statusCb.hide();

        this.dateRangeField.setValue(rec.data);
        this.activeRecord = rec;

        return this;
    },
    
    getActiveStatus: function(statusList) {
    	var activeStatus = null;
    	try{
    		for(var i=0;i<statusList.data.length;i++) {
    			if(statusList.data.items[i].get('inact') != 'X') {
    				activeStatus = statusList.data.items[i];
    				break;
    			}
    		}
    		
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActiveStatus of BaseEventWindow', ex);
    	}
    	return activeStatus;
    },
    
    setCurrentActiveStatusOnRecord: function() {
		try{
			var setRecord = this.statusCb.getValue();
			var activeStatusList = this._rec.data.Operation.get('objectStatusList');
	        var activeStatus = this.getActiveStatus(activeStatusList);
	        
	        if(activeStatus != null && setRecord == activeStatus.get('stat')) {
	        	//no changes 
	        }
	        else {
	        	//set all active status to INACT
	        	var statusSetActive = false;
				for(var i=0;i<activeStatusList.data.length;i++) {
					if(activeStatusList.data.items[i].get('inact') == '' && activeStatusList.data.items[i].get('estat') != setRecord) {
						activeStatusList.data.items[i].set('inact', 'X');
					}
					else if(activeStatusList.data.items[i].get('estat') == setRecord) {
						activeStatusList.data.items[i].set('inact', '');
						activeStatusList = true;
					}
				}
				
				var statusFromStatusList = null; 
				for(var i=0;i<this._stsma.data.length;i++) {
					if(this._stsma.data.items[i].get('estat') == setRecord) {
						statusFromStatusList = this._stsma.data.items[i];
						break;
					}
				}
				
				if(!statusSetActive && statusFromStatusList != null) {
					//status was not in the statuslist of the operation => add it
					var retval = Ext.create('AssetManagement.customer.model.bo.ObjectStatus', {
						objnr: this._rec.data.Operation.get('objnr'),
						stat: statusFromStatusList.get('estat'),
						inact: '',
						chgnr: '',
						changed: '',
						txt04: statusFromStatusList.get('txt04'),
						txt30: statusFromStatusList.get('txt30'),
						status_int: '',
						user_status_code: '',
						user_status_desc: '',
						
					    updFlag: '',
					    mobileKey: this._rec.data.Operation.get('mobileKey'),
					    childKey: this._rec.data.Operation.get('childKey')
					});
					
					retval.set('id', retval.get('objnr') + retval.get('stat'));
					
					activeStatusList.add(retval);
				}	
	        }
	        this._rec.data.Operation.set('objectStatusList', activeStatusList);
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActiveStatus of BaseEventWindow', ex);
		}
    },

    // private
    roundTime: function(dt, incr) {
        incr = incr || 15;
        var m = parseInt(dt.getMinutes(), 10);
        return dt.add('mi', incr - (m % incr));
    },

    // private
    onCancel: function() {
        this.cleanup(true);
        this.fireEvent('eventcancel', this);
    },

    // private
    cleanup: function(hide) {
        if (this.activeRecord && this.activeRecord.dirty) {
            this.activeRecord.reject();
        }
        delete this.activeRecord;

        if (hide === true) {
            // Work around the CSS day cell height hack needed for initial render in IE8/strict:
            //var anim = afterDelete || (Ext.isIE8 && Ext.isStrict) ? null : this.animateTarget;
            this.hide();
        }
    },

    // private
    updateRecord: function(record, keepEditing) {
        var fields = record.getFields(),
            values = this.formPanel.getForm().getValues(),
            name,
            M = AssetManagement.customer.modules.calendar.data.EventMappings,
            obj = {};
        /*
        Ext.Array.each(fields, function(f) {
            name = f.name;
            if (name in values) {
                obj[name] = values[name];
            }
        });*/
        
        var dates = this.dateRangeField.getValue();
        obj[M.StartDate.name] = dates[0];
        obj[M.EndDate.name] = dates[1];
        obj[M.IsAllDay.name] = dates[2];

        this.setCurrentActiveStatusOnRecord();

        record.beginEdit();
        record.set(obj);
        
        if (!keepEditing) {
            record.endEdit();
        }

        return this;
    },
    
    // private
    onSave: function(){
        if(!this.formPanel.form.isValid()){
            return;
        }
        if(!this.updateRecord(this.activeRecord)){
            this.onCancel();
            return;
        }
        this.fireEvent(this.activeRecord.phantom ? 'eventadd' : 'eventupdate', this, this.activeRecord, this.animateTarget);

        // Clear phantom and modified states.
        this.activeRecord.commit();
    },
    
    // private
    onDelete: function(){
        this.fireEvent('eventdelete', this, this.activeRecord, this.animateTarget);
    },
    
    showOrder: function()  {
    	try{
    		this.onCancel();
            this.fireEvent('eventShowOrder', this, this.activeRecord, this.animateTarget);
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getActiveStatus of BaseEventWindow', ex);
    	}
    }

});