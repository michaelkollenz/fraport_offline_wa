/*
 * Internal drag zone implementation for the calendar components. This provides base functionality
 * and is primarily for the month view -- DayViewDD adds day/week view-specific functionality.
 */
Ext.define('AssetManagement.base.modules.calendar.dd.BaseUnplannedDragZone', {
    extend: 'Ext.dd.DragZone',

    requires: [
        'AssetManagement.customer.modules.calendar.dd.StatusProxy'/*,
        'AssetManagement.customer.modules.calendar.data.EventMappings'*/
    ],
    
    ddGroup: 'CalendarDD',
    eventSelector: '.x-grid-item',

    constructor: function(el, config) {
        if (!AssetManagement.customer.modules.calendar._unplannedStatusProxyInstance) {
        	AssetManagement.customer.modules.calendar._unplannedStatusProxyInstance = new AssetManagement.customer.modules.calendar.dd.StatusProxy();
        }
        this.proxy = AssetManagement.customer.modules.calendar._unplannedStatusProxyInstance;
        this.callParent(arguments);
    },

    getDragData: function(e) {
        // Check whether we are dragging on an event first
        var t = e.getTarget();
        var rowEl  = e.getTarget(this.view.itemSelector, 10);
        //var rowEl = view.findItemByChild(t);
        var rec = this.grid.component.getView().getRecord(rowEl);

        if (rec) {
            return {
                type: 'eventdrag',
                ddel: t,
                eventStart: rec.data[AssetManagement.customer.modules.calendar.data.EventMappings.StartDate.name],
                eventEnd: rec.data[AssetManagement.customer.modules.calendar.data.EventMappings.EndDate.name],
                eventData: rec.data,
                proxy: this.proxy
            };
        }

        // If not dragging an event then we are dragging on
        // the calendar to add a new event
//    	t = this.view.getDayAt(e.getX(), e.getY());
//        if (t.el) {
//            return {
//                type: 'caldrag',
//                start: t.date,
//                proxy: this.proxy
//            };
//        }
        
        return null;
    },

    onInitDrag: function(x, y) {
        if (this.dragData.ddel) {
            var ghost = this.dragData.ddel.cloneNode(true),
            child = Ext.fly(ghost).down('dl');

            Ext.fly(ghost).setWidth('auto');

            if (child) {
                // for IE/Opera
                child.setHeight('auto');
            }
            this.proxy.update(ghost);
            this.onStartDrag(x, y);
        }
        else if (this.dragData.start) {
            this.onStartDrag(x, y);
        }
        //this.grid.component.getView().onInitDrag();
        return true;
    },

    afterRepair: function() {
        if (Ext.enableFx && this.dragData.ddel) {
            Ext.fly(this.dragData.ddel).highlight(this.hlColor || 'c3daf9');
        }
        this.dragging = false;
    },

    getRepairXY: function(e) {
        if (this.dragData.ddel) {
            return Ext.fly(this.dragData.ddel).getXY();
        }
    },

    afterInvalidDrop: function(e, id) {
        Ext.select('.ext-dd-shim').hide();
    },
    
	destroy: function() {
	    this.callParent(arguments);
	    delete AssetManagement.customer.modules.calendar._unplannedStatusProxyInstance;
	}
});