/*
 * Internal drop zone implementation for the calendar components. This provides base functionality
 * and is primarily for the month view -- DayViewDD adds day/week view-specific functionality.
 */
Ext.define('AssetManagement.base.modules.calendar.dd.BaseDropZone', {
    extend: 'Ext.dd.DropZone',
    
    requires: [
        'AssetManagement.customer.modules.calendar.util.Date',
        'AssetManagement.customer.modules.calendar.data.EventMappings'
    ],

    ddGroup: 'CalendarDD',
    eventSelector: '.ext-cal-evt',

    // private
    shims: [],

    getTargetFromEvent: function(e) {
        var dragOffset = this.dragOffset || 0,
        y = e.getY() - dragOffset,
        d = this.view.getDayAt(e.getX(), y);

        return d.el ? d: null;
    },

    onNodeOver: function(n, dd, e, data) {
        var D = AssetManagement.customer.modules.calendar.util.Date,
        start = data.type == 'eventdrag' ? n.date: D.min(data.start, n.date),
        end = data.type == 'eventdrag' ? D.add(n.date, {days: D.diffDays(data.eventStart, data.eventEnd)}) :
        D.max(data.start, n.date);

        if (!this.dragStartDate || !this.dragEndDate || (D.diffDays(start, this.dragStartDate) != 0) || (D.diffDays(end, this.dragEndDate) != 0)) {
            this.dragStartDate = start;
            this.dragEndDate = D.add(end, {days: 1, millis: -1, clearTime: true});
            this.shim(start, end);

            var range = Ext.Date.format(start, 'n/j');
            if (D.diffDays(start, end) > 0) {
                range += '-' + Ext.Date.format(end, 'n/j');
            }
            var msg = Ext.util.Format.format(data.type == 'eventdrag' ? this.moveText: this.createText, range);
            data.proxy.updateMsg(msg);
        }
        return this.dropAllowed;
    },

    shim: function(start, end) {
        this.currWeek = -1;
        this.DDMInstance.notifyOccluded = true;
        var dt = Ext.Date.clone(start),
            i = 0,
            shim,
            box,
            D = AssetManagement.customer.modules.calendar.util.Date,
            cnt = D.diffDays(dt, end) + 1;

        Ext.each(this.shims,
            function(shim) {
                if (shim) {
                    shim.isActive = false;
                }
            }
        );

        while (i++<cnt) {
            var dayEl = this.view.getDayEl(dt);

            // if the date is not in the current view ignore it (this
            // can happen when an event is dragged to the end of the
            // month so that it ends outside the view)
            if (dayEl) {
                var wk = this.view.getWeekIndex(dt);
                shim = this.shims[wk];

                if (!shim) {
                    shim = this.createShim();
                    this.shims[wk] = shim;
                }
                if (wk != this.currWeek) {
                    shim.boxInfo = dayEl.getBox();
                    this.currWeek = wk;
                }
                else {
                    box = dayEl.getBox();
                    shim.boxInfo.right = box.right;
                    shim.boxInfo.width = box.right - shim.boxInfo.x;
                }
                shim.isActive = true;
            }
            dt = D.add(dt, {days: 1});
        }

        Ext.each(this.shims, function(shim) {
            if (shim) {
                if (shim.isActive) {
                    shim.show();
                    shim.setBox(shim.boxInfo);
                }
                else if (shim.isVisible()) {
                    shim.hide();
                }
            }
        });
    },

    createShim: function() {
        if (!this.shimCt) {
            this.shimCt = Ext.get('ext-dd-shim-ct');
            if (!this.shimCt) {
                this.shimCt = document.createElement('div');
                this.shimCt.id = 'ext-dd-shim-ct';
                Ext.getBody().appendChild(this.shimCt);
            }
        }
        var el = document.createElement('div');
        el.className = 'ext-dd-shim';
        this.shimCt.appendChild(el);

        el = Ext.get(el);

        el.setVisibilityMode(2);

        return el;
    },

    clearShims: function() {
        Ext.each(this.shims,
        function(shim) {
            if (shim) {
                shim.hide();
            }
        });
        this.DDMInstance.notifyOccluded = false;
    },

    onContainerOver: function(dd, e, data) {
        return this.dropAllowed;
    },

    onCalendarDragComplete: function() {
        if (this.dragStartDate) delete this.dragStartDate;
        if (this.dragEndDate) delete this.dragEndDate;
        this.clearShims();
    },

    onNodeDrop: function(n, dd, e, data) {
        if (n && data) {
            if (data.type == 'eventdrag') {
        		var rec = null;
        		var dt = null;        		
            	if(data.eventData){
            		rec = data.eventData;
            		dt = AssetManagement.customer.modules.calendar.util.Date.copyTime(rec.StartDate, n.date);
            	}
            	else {
            		rec = this.view.getEventRecordFromEl(data.ddel);
            		dt = AssetManagement.customer.modules.calendar.util.Date.copyTime(rec.data[AssetManagement.customer.modules.calendar.data.EventMappings.StartDate.name], n.date);
            	}
            	
                this.view.onEventDrop(rec, dt);
                this.onCalendarDragComplete();
                return true;
            }
            if (data.type == 'caldrag') {
                this.view.onCalendarEndDrag(this.dragStartDate, this.dragEndDate,
                Ext.bind(this.onCalendarDragComplete, this));
                //shims are NOT cleared here -- they stay visible until the handling
                //code calls the onCalendarDragComplete callback which hides them.
                return true;
            }
        }
        this.onCalendarDragComplete();
        return false;
    },

    onContainerDrop: function(dd, e, data) {
        this.onCalendarDragComplete();
        return false;
    }


});
    //Bug inside the Framework, this is the current workaround found
    Ext.require('Ext.dom.Element', function () {
        Ext.isGarbage = function (dom) {
            // determines if the dom element is in the document or in the detached body element
            // use by collectGarbage and Ext.get()
            return dom &&
                // Must be an element. window, document and documentElement can never be garbage.
                dom.nodeType === 1 &&
                // if the element does not have a parent node, it is definitely not in the
                // DOM - we can exit immediately
                (!dom.parentNode ||
                // If the element has an offset parent we can bail right away, it is
                // definitely in the DOM.
                (!dom.offsetParent &&
                    // if the element does not have an offsetParent it can mean the element is
                    // either not in the dom or it is hidden.  The next step is to check to see
                    // if it can be found by id using either document.all or getElementById(),
                    // whichever is faster for the current browser.  Normally we would not
                    // include IE-specific checks in the sencha-core package, however,  in this
                    // case the function will be inlined and therefore cannot be overridden in
                    // the ext package.
                    ((Ext.isIE8 ? document.all[dom.id] : document.getElementById(dom.id)) !== dom) &&
                    // finally if the element was not found in the dom by id, we need to check
                    // the detachedBody element
                    !(Ext.detachedBodyEl && Ext.detachedBodyEl.isAncestor(dom))));
        };
    });
