/**
 * This is the {@link Ext.data.Record Record} specification for calendar event data used by the
 * {@link AssetManagement.customer.modules.calendar.CalendarPanel CalendarPanel}'s underlying store. It can be overridden as
 * necessary to customize the fields supported by events, although the existing column names should
 * not be altered. If your model fields are named differently you should update the <b>mapping</b>
 * configs accordingly.
 *
 * The only required fields when creating a new event record instance are StartDate and
 * EndDate.  All other fields are either optional are will be defaulted if blank.
 *
 * Here is a basic example for how to create a new record of this type:
 *
 *      rec = new AssetManagement.customer.modules.calendar.data.BaseEventModel({
 *          StartDate: '2101-01-12 12:00:00',
 *          EndDate: '2101-01-12 13:30:00',
 *          Title: 'My cool event',
 *          Notes: 'Some notes'
 *      });
 *
 * If you have overridden any of the record's data mappings via the AssetManagement.customer.modules.calendar.data.EventMappings object
 * you may need to set the values using this alternate syntax to ensure that the fields match up correctly:
 *
 *      var M = AssetManagement.customer.modules.calendar.data.EventMappings;
 *
 *      rec = new AssetManagement.customer.modules.calendar.data.BaseEventModel();
 *      rec.data[M.StartDate.name] = '2101-01-12 12:00:00';
 *      rec.data[M.EndDate.name] = '2101-01-12 13:30:00';
 *      rec.data[M.Title.name] = 'My cool event';
 *      rec.data[M.Notes.name] = 'Some notes';
 */
Ext.define('AssetManagement.base.modules.calendar.data.BaseEventModel', {
    extend: 'Ext.data.Model',
    
    requires: [
        'AssetManagement.customer.modules.calendar.data.EventMappings'
    ],
    
    identifier: 'sequential',
    
    inheritableStatics: {
        /**
         * Reconfigures the default record definition based on the current {@link AssetManagement.customer.modules.calendar.data.EventMappings EventMappings}
         * object. See the header documentation for {@link AssetManagement.customer.modules.calendar.data.EventMappings} for complete details and
         * examples of reconfiguring an EventRecord.
         *
         * **NOTE**: Calling this method will *not* update derived class fields. To ensure
         * updates are made before derived classes are defined as an override. See the
         * documentation of `AssetManagement.customer.modules.calendar.data.EventMappings`.
         *
         * @static
         * @return {Class} The updated BaseEventModel
         */
        reconfigure: function() {
        	var me = null;
        	var Mappings = null;
        	
        	try {
                me = this;
                Mappings = AssetManagement.customer.modules.calendar.data.EventMappings;

                // It is critical that the id property mapping is updated in case it changed, since it
                // is used elsewhere in the data package to match records on CRUD actions:
                me.prototype.idProperty = Mappings.RecNo.name || 'id';

                me.replaceFields(Ext.Object.getValues(Mappings), true);
            } catch(ex) {
	           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reconfigure of BaseEventModel', ex)
            } 
            
            return me;
        }
    }
},
function(){
	try {
        this.reconfigure();
	} catch(ex) {
		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside function of BaseEventModel', ex)
	}    
});
