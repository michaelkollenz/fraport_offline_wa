//@define AssetManagement.customer.modules.calendar.data.BaseEventMappings
/**
 * @class AssetManagement.customer.modules.calendar.data.BaseEventMappings
 * A simple object that provides the field definitions for Event records so that they can
 * be easily overridden.
 *
 * To ensure the proper definition of AssetManagement.customer.modules.calendar.data.EventModel the override should be
 * written like this:
 *
 *      Ext.define('MyApp.data.BaseEventMappings', {
 *          override: 'AssetManagement.customer.modules.calendar.data.BaseEventMappings'
 *      },
 *      function () {
 *          // Update "this" (this === AssetManagement.customer.modules.calendar.data.BaseEventMappings)
 *      });
 */
//Ext.ns('AssetManagement.base.modules.calendar.data');

Ext.define('AssetManagement.base.modules.calendar.data.BaseEventMappings', {
    inheritableStatics: {
        RecNo: {
            name: 'RecNo',
            mapping: 'id',
            type: 'string'
        },
        Aufnr: {
            name: 'Aufnr',
            mapping: 'aufnr',
            type: 'string'
        },
        Title: {
            name: 'Title',
            mapping: 'title',
            type: 'string'
        },

        StartDate: {
            name: 'StartDate',
            mapping: 'start',
            type: 'date',
            dateFormat: 'c'
        },
        EndDate: {
            name: 'EndDate',
            mapping: 'end',
            type: 'date',
            dateFormat: 'c'
        },
        Fsav: {
            name: 'Fsav',
            mapping: 'fsav',
            type: 'date',
            dateFormat: 'c'
        },
        Fsed: {
            name: 'Fsed',
            mapping: 'fsed',
            type: 'date',
            dateFormat: 'c'
        },
        Arbpl: {
            name: 'Arbpl',
            mapping: 'arbpl',
            type: 'string'
        },
        Werks: {
            name: 'Werks',
            mapping: 'werks',
            type: 'string'
        },
        IsAllDay: {
            name: 'IsAllDay',
            mapping: 'ad',
            type: 'boolean'
        },
        IsNew: {
            name: 'IsNew',
            mapping: 'n',
            type: 'boolean'
        },
        IsModified: {
            name: 'IsModified',
            mapping: 'modified',
            type: 'boolean'
        },
        Description:{
            name: 'Description',
            mapping: 'description'
        },
        Operation: {
            name: 'Operation',
            mapping: 'operation'
        },
        Order: {
            name: 'Order',
            mapping: 'order'
        },
        FuncLocDesc: {
            name: 'FuncLocDesc',
            mapping: 'funcLocDesc'
        },
        PostalCode: {
            name: 'PostalCode',
            mapping: 'postalCode'
        },
        City: {
            name: 'City',
            mapping: 'city'
        }
    }



})/*,

AssetManagement.base.modules.calendar.data.BaseEventMappings = {
    RecNo: {
        name: 'RecNo',
        mapping: 'id',
        type: 'int'
    },
    Aufnr: {
        name: 'Aufnr',
        mapping: 'aufnr',
        type: 'string'
    },
    Title: {
        name: 'Title',
        mapping: 'title',
        type: 'string'
    },
    
    StartDate: {
        name: 'StartDate',
        mapping: 'start',
        type: 'date',
        dateFormat: 'c'
    },
    EndDate: {
        name: 'EndDate',
        mapping: 'end',
        type: 'date',
        dateFormat: 'c'
    },
    Fsav: {
        name: 'Fsav',
        mapping: 'fsav',
        type: 'date',
        dateFormat: 'c'
    },
    Fsed: {
        name: 'Fsed',
        mapping: 'fsed',
        type: 'date',
        dateFormat: 'c'
    },
    Arbpl: {
        name: 'Arbpl',
        mapping: 'arbpl',
        type: 'string'
    },
    Werks: {
        name: 'Werks',
        mapping: 'werks',
        type: 'string'
    },
    IsAllDay: {
        name: 'IsAllDay',
        mapping: 'ad',
        type: 'boolean'
    },
    IsNew: {
        name: 'IsNew',
        mapping: 'n',
        type: 'boolean'
    },
    IsModified: {
        name: 'IsModified',
        mapping: 'modified',
        type: 'boolean'
    },
    Description:{
        name: 'Description',
        mapping: 'description'
    },    	
    Operation: {
        name: 'Operation',
        mapping: 'operation'
    },    	
    Order: {
        name: 'Order',
        mapping: 'order'
    },
    FuncLocDesc: {
        name: 'FuncLocDesc',
        mapping: 'funcLocDesc'
    },
    PostalCode: {
        name: 'PostalCode',
	    mapping: 'postalCode'
	},
	City: {
        name: 'City',
	    mapping: 'city'
	}
};*/
