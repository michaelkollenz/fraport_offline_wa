Ext.define('AssetManagement.base.modules.calendar.data.BaseEvents', {

    inheritableStatics: {
		/**create new calendar events from order operations */
        getPlannedOperations: function(orders) {
			var evts= null;
			
			try {
				evts = AssetManagement.customer.modules.calendar.data.Events.getRetVal();
				var count = 0;
				for(var i =0;i<orders.data.items.length;i++)
				{
					for(var j=0;j<orders.data.items[i].data.operations.data.length;j++){
						//get the operation data and map the values to variables
						var operation = orders.data.items[i].data.operations.data.items[j];
						var order = orders.data.items[i];

						var funcLocDesc = '';
						var city = '';
						var postalCode = '';

						if(order.get('address')){
							city = order.get('address').get('city1');
							postalCode = order.get('address').get('postCode1');
						}
						if(order.get('funcLoc')){
							funcLocDesc = order.get('funcLoc').get('pltxt');
						}

						//if dates are undefined set the date to max SAP value
						var start = operation.get('ntan');
						var end = operation.get('nten');
						if(start == null){
							continue;
							start = new DateDate(9999,11,31,0,0,0);
							end = new DateDate(9999,11,31,0,0,0);
						}

						//get address data
						var address = null;
						var addressString = '';
						var order = orders.data.items[i];
						if(order.get('address') != null)
							addressString = order.get('address').get('postCode1') + ' ' + order.get('address').get('city1') + ' ' + order.get('address').get('name1');

						var dtStart = start;
						var dtEnd = end;
						var allDay = false;
						if(dtStart.getDate() != dtEnd.getDate() || dtStart.getMonth() != dtEnd.getMonth() || dtStart.getFullYear() != dtEnd.getFullYear())
							allDay = true;
						var aufnr = operation.get('aufnr');
						var vornr = operation.get('vornr');
						var split = operation.get('split');
						var ktext = '';
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressString))
							ktext += addressString + '<br/>';
						ktext += operation.get('ltxa1') + ' ' + operation.get('aufnr');
						//var ktext = operation.get('ltxa1');
						var id =  aufnr + vornr + split;
						var arbpl = operation.get('arbpl');
						var werks = operation.get('werks');
						var description = AssetManagement.customer.utils.StringUtils.trimStart(operation.get('aufnr'), '0') + ' / ' + operation.get('vornr');

						//map data to an event array (event object will be created in the calendar framework)
						evts.evts[count++] = {
		                    "RecNo": id,
		                    "Title": ktext,
		                    "StartDate": dtStart,
		                    "EndDate": dtEnd,
		                    "Arbpl": arbpl,
		                    "Werks": werks,
		                    "IsAllDay": allDay,
		                    "IsNew": false,
		                    "IsModified": false,
		                    "Operation": operation,
		                    "Description": description,
		                    "Order": orders.data.items[i],
		                    "Fsav": operation.get('fsav'),
		                    "Fsed": operation.get('fsed'),
		                    "Aufnr": aufnr,
							"FuncLocDesc": funcLocDesc,
		                    "PostalCode": postalCode,
		                   	"City": city
		                };
					}
				}
            } catch(ex) {
	           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPlannedOperations of CalendarPageController', ex)
            }
			
			return evts;
        },
        
        /*
         * returns all operations as events for which ntan is null (not fixed in the planning)
         */
        getUnplannedOperations: function(orders) {
        	var evts = null;
        	
        	try {
        		evts = AssetManagement.customer.modules.calendar.data.Events.getRetVal();
    			var count = 0;
    			for(var i =0;i<orders.data.items.length;i++)
    			{
    				for(var j=0;j<orders.data.items[i].data.operations.data.length;j++){
    					//get the operation data and map the values to variables
    					var operation = orders.data.items[i].data.operations.data.items[j];
    					var order = orders.data.items[i]; 
    					var funcLocDesc = ''; 
    					var city = ''; 
    					var postalCode = ''; 
    					
    					if(order.get('address')){
    						city = order.get('address').get('city1'); 
    						postalCode = order.get('address').get('postCode1'); 
    					}
    					if(order.get('funcLoc')){
    						funcLocDesc = order.get('funcLoc').get('pltxt'); 	
    					}
    						
    					//if dates are undefined set the date to max SAP value
    					var start = operation.get('ntan');
    					var end = operation.get('nten');
    					if(start != null){
    						continue;
    					}
    					start = new Date(9999,11,31);
    					end = new Date(9999,11,31);
    					var dtStart = start;
    					var dtEnd = end;
    					var allDay = false;
    					if(dtStart.getDate() != dtEnd.getDate() || dtStart.getMonth() != dtEnd.getMonth() || dtStart.getFullYear() != dtEnd.getFullYear())
    						allDay = true;
    					var aufnr = operation.get('aufnr');
    					var vornr = operation.get('vornr');
    					var split = operation.get('split');
    					var ktext = operation.get('ltxa1');
    					var id =  aufnr + vornr + split;
    					var arbpl = operation.get('arbpl');
    					var werks = operation.get('werks');
    					var description = AssetManagement.customer.utils.StringUtils.trimStart(operation.get('aufnr'), '0') + ' / ' + operation.get('vornr');
    					
    					//map data to an event array (event object will be created in the calendar framework)
    					evts.evts[count++] = {
    			            "RecNo": id,
    			            "Title": ktext,
    			            "StartDate": dtStart,
    			            "EndDate": dtEnd, 
    			            "Arbpl": arbpl,
    			            "Werks": werks,
    			            "IsAllDay": allDay,
    			            "IsNew": false,
    			            "IsModified": false,
    			            "Operation": operation,
    	                    "Description": description,
    	                    "Order": orders.data.items[i],
    	                    "Fsav": operation.get('fsav'),
    	                    "Fsed": operation.get('fsed'),
    	                    "Aufnr": aufnr,
    	                    "FuncLocDesc": funcLocDesc,
    	                    "PostalCode": postalCode,
    	                   	"City": city
    	                    
    			        };
    				}
    			}			
            } catch(ex) {
	            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUnplannedOperations of CalendarPageController', ex)
            }
    			
			return evts;
        },

        //retunrs an array stub for the list of events (needed by the calendar framework)
        getRetVal: function(){
        	return {"evts": []};
        },
	
		/**
		 * determines the color of an event
		 * for Franke this depends on the user status of the operation
		 * returns the color class for css
		 */
        getColorCls: function(event) {
			var retVal = '-cal-evt-blue';
			try{
				var classRed = '-cal-evt-red';
				var classGreen = '-cal-evt-green';
				var classYellow = '-cal-evt-yellow';
				var classBlue = '-cal-evt-blue';
				//get the parameters for the status colors
				var funcParas = AssetManagement.customer.model.bo.FuncPara.getInstance();
				//get statuslist
				var statuslist = event.Operation.get('objectStatusList');
				var statusCount = statuslist.data.items.length;
				for(var i=0;i<statusCount;i++) {
					var statusObject = statuslist.data.items[i];
					if (funcParas.get('calendar_color_red') != undefined &&
							funcParas.get('calendar_color_red').get('fieldval1') == event.Operation.get('stsma') &&
							funcParas.get('calendar_color_red').get('fieldval2') == statusObject.get('stat')) {
						retval = classRed;
					}						
					else if (funcParas.get('calendar_color_green') != undefined &&
							funcParas.get('calendar_color_green').get('fieldval1') == event.Operation.get('stsma') &&
							funcParas.get('calendar_color_green').get('fieldval2') == statusObject.get('stat')) {
						retval = classGreen
					}		
					else if (funcParas.get('calendar_color_yellow') != undefined &&
							funcParas.get('calendar_color_yellow').get('fieldval1') == event.Operation.get('stsma') &&
							funcParas.get('calendar_color_yellow').get('fieldval2') == statusObject.get('stat')) {
						retval = classYellow;
					}
					else if (funcParas.get('calendar_color_blue') != undefined &&
							funcParas.get('calendar_color_blue').get('fieldval1') == event.Operation.get('stsma') &&
							funcParas.get('calendar_color_blue').get('fieldval2') == statusObject.get('stat')) {
						retval = classBlue;
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getChangedObjectsFromStore of CalendarPageController', ex)
			}	
			return retVal;
        }


    }
});
