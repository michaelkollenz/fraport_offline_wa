Ext.define('AssetManagement.base.modules.googleMaps.BaseGoogleMapsRequest', {
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'Ext.util.MixedCollection',
	    'AssetManagement.customer.helper.OxLogger'
	],

    inheritableStatics: {
		//public
		REQUEST_TYPES: {
			SHOW_OWN_POSITION: 0,
			SHOW_ADDRESSES: 1,
			SHOW_ROUTES: 2,
			OPTIMIZE_ROUTE: 3
		}
	},
	
	config: {
		type: 0,
		includeOwnPosition: false,
		addresses: null,
		hintsCollection: null
	},
	
    constructor: function(config) {
	    try {
		    this.initConfig(config);
		
		    this._addresses = new Array();
		    this._hintsCollection = new Ext.util.MixedCollection();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseGoogleMapsRequest', ex);
		}    
	},
	
	addAddress: function(address, hint) {
		try {
			if(address) {
				if(!Ext.Array.contains(this._addresses, address))
					this._addresses.push(address);
					
				if(hint)
					this._hintsCollection.add(address.getId(), hint);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addAddress of BaseGoogleMapsRequest', ex);
		}
	},
	
	addAddressWithHint: function(address, hint) {
		try {
			this.addAddress(address, hint);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addAddressWithHint of BaseGoogleMapsRequest', ex);
		}
	},
	
	getHintForAddress: function(address) {
		var retval = '';
		
		try {
			retval = this._hintsCollection.get(address.getId());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHintForAddress of BaseGoogleMapsRequest', ex);
		}
		
		return retval;
	}
});