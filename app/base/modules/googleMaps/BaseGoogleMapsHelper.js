Ext.define('AssetManagement.base.modules.googleMaps.BaseGoogleMapsHelper', {
	requires: [
	    'AssetManagement.customer.modules.googleMaps.GoogleMap',
	    'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		//public
		ERROR_TYPES: {
			BROWSER_OFFLINE: 0,
			API_NOT_RESPONDING: 1,
			GEO_NOT_SUP: 2,
			GEO_DECLINED: 3,
			INVALID_REQUEST: 4,
			INCOMPLETE_REQUEST: 5,
			GEO_CODING_FAILED: 6
		},
	
		//private:
		LIBRARY_URL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC8dR0vNHtl-jbWxuVkSG7QAAyBYd7y4QY&sensor=true&callback=onGoogleMapsLibraryLoaded',
	    _instance: null,
		
		//public:
	    getInstance: function() {
	        if(this._instance == null) {
				this._instance = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsHelper');
				
				window.onGoogleMapsLibraryLoaded = this._instance.onLibraryLoaded;
	        }
				
			return this._instance;
	    }
	},
	
	_libraryLoaded: false,
	_libraryLoadingRequestIsPending: false,
	_geocoder: null,
	
	constructor: function() {
		this.callParent(arguments);
		
		try {
			this.loadMapsLibrary();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseGoogleMapsHelper', ex);
		}
	},
	
	//public
	//initializes a div and returns the corresponding maps object
	getMapsObjectForDiv: function(div, callback, callbackScope) {
		try {
			if(!div || !callback)
				return;
			
			//ensure the libraray is loaded
			if(this._libraryLoaded === false) {
				var me = this;
				
				var loadedCallback = function(success, errorType) {
					try {
						if(!success) {
							callback.call(callbackScope ? callbackScope : me, null, errorType);
						} else {
							Ext.defer(me.getMapsObjectForDiv, 0, me, [ div, callback, callbackScope ]);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMapsObjectForDiv of BaseGoogleMapsHelper', ex);
						callback.call(callbackScope ? callbackScope : me, null);
					}
				};
				
				this.loadMapsLibrary(loadedCallback);
			} else {
				this.getMapForDiv(div, callback, callbackScope);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMapsObjectForDiv of BaseGoogleMapsHelper', ex);
			callback.call(callbackScope ? callbackScope : this, null);
		}
	},
	
	//will execute the maps request and render it's result into the map
	executeMapsRequest: function(mapsRequest, map, callback, callbackScope) {
		try {
			if(!mapsRequest || !map)
				return;
			
			//ensure the libraray is loaded
			if(this._libraryLoaded === false) {
				var me = this;
			
				var loadedCallback = function(success, errorType) {
					try {
						if(!success) {
							callback.call(callbackScope ? callbackScope : me, false, errorType);
						} else {
							Ext.defer(me.executeMapsRequest, 0, me, [ mapsRequest, map, callback, callbackScope ]);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside executeMapsRequest of BaseGoogleMapsHelper', ex);
						callback.call(callbackScope ? callbackScope : me, null);
					}
				};
			
				this.loadMapsLibrary(loadedCallback);
			} else {
				//analyze the request type and start the adequate action
				var requestType = mapsRequest.getType();
				
				switch(requestType) {
					case AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_OWN_POSITION:
						this.showOwnPositionOnMap(map, callback, callbackScope);
						break;
					
					case AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES:
						this.showAddressesOnMap(mapsRequest, map, callback, callbackScope);
						break;
					
					case AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ROUTES:
						break;
						
					case AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.OPTIMIZE_ROUTE:
						break;
				
					default:
						callback.call(callbackScope ? callbackScope : this, false, this.self.ERROR_TYPES.INVALID_REQUEST);
						break;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside executeMapsRequest of BaseGoogleMapsHelper', ex);
			
			if(callback)
				callback.call(callbackScope ? callbackScope : this, false);
		}
	},
	
	showOwnPositionOnMap: function(map, callback, callbackScope) {
		try {
			//try to use the users current position
			var positionCallback = function(position, errorType) {
				try {
					if(position) {
						map.setCenter(position);
						map.setOwnPosition(position);
						map.showOwnPosition();
					}
					
					if(callback)
						callback.call(callbackScope ? callbackScope : this, position ? true : false, position ? undefined : errorType);
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showOwnPositionOnMap of BaseGoogleMapsHelper', ex);
					if(callback)
						callback.call(callbackScope ? callbackScope : this, false);
				}
			};
			
			this.getCurrentLocation(positionCallback, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showOwnPositionOnMap of BaseGoogleMapsHelper', ex);
			
			if(callback)
				callback.call(callbackScope ? callbackScope : this, false);
		}
	},
	
	showAddressesOnMap: function(mapsRequest, map, callback, callbackScope) {
		try {
			var passedAddresses = mapsRequest.getAddresses();
			
			if(passedAddresses && passedAddresses.length > 0) {
				//to show the addresses, they have to be transformed into LatLng coordinates first
				var me = this;
				var positions = new Array();
				
				var pendingRequests = 0;
				var errorOccured = false;
				var reported = false;
				
				var completeFunction = function() {
					try {
				
						pendingRequests--;
					
						if(errorOccured === true && reported === false) {
							if(callback)
								callback.call(callbackScope ? callbackScope : me, false, me.self.ERROR_TYPES.GEO_CODING_FAILED);
						
							reported = true;
						} else if(pendingRequests === 0 && errorOccured === false) {
							map.reset();
						
							//show the positions on the map
							Ext.Array.each(passedAddresses, function(address, index) {
								var positionOfAddress = positions[index];
								var hintOfAddress = mapsRequest.getHintForAddress(address);
								
								var marker = new google.maps.Marker({
					    			position: positionOfAddress,
					    			title: hintOfAddress
								});
								
								map.addMarkerWithHint(marker, hintOfAddress);
							}, me);
							
							//set the maps center to the first address position
							map.setZoom(passedAddresses.length > 1 ? 11 : 14);
							
							//if the own position is requested also, update it
							if(mapsRequest.getIncludeOwnPosition()) {
								var showOwnPositionCallback = function(success, errorType) {
									try {
										Ext.defer(function() { map.setCenter(positions[0]); }, 200, this);
										callback.call(callbackScope ? callbackScope : this, true);
									} catch(ex) {
										AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAddressesOnMap of BaseGoogleMapsHelper', ex);
										callback.call(callbackScope ? callbackScope : this, false);
									}
								};
							
								me.showOwnPositionOnMap(map, showOwnPositionCallback, me);
							} else if(callback) {
								Ext.defer(function() { map.setCenter(positions[0]); }, 200, this);
								callback.call(callbackScope ? callbackScope : me, true);
							}
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAddressesOnMap of BaseGoogleMapsHelper', ex);
						
						if(callback)
							callback.call(callbackScope ? callbackScope : this, false);
					}
				};
				
				var positionCallback = function(position) {
					try {
						errorOccured = !position;
						
						if(!errorOccured)
							positions.push(position);
						
						completeFunction.call(this);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAddressesOnMap of BaseGoogleMapsHelper', ex);
						
						if(callback)
							callback.call(callbackScope ? callbackScope : this, false);
					}
				};
				
				Ext.Array.each(passedAddresses, function(address) {
					this.getPositionForAddress(address, positionCallback, this);
					pendingRequests++;
				}, this);
			} else {
				if(callback)
					callback.call(callbackScope ? callbackScope : this, false, this.self.ERROR_TYPES.INCOMPLETE_REQUEST);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAddressesOnMap of BaseGoogleMapsHelper', ex);
			
			if(callback)
				callback.call(callbackScope ? callbackScope : this, false);
		}
	},
	
	//will try to get a position as LatLng for an address
	getPositionForAddress: function(address, callback, callbackScope) {
		try {
			if(!callback)
				return;
		
			var myGeocoder = this.getGeocoder();
			
			if(myGeocoder && address) {
				var addressAsString = this.buildGeocodeQueryStringFromAddress(address);
				
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressAsString)) {
					var geocodeCallback = function(results, status) {
						try {
							if (status == google.maps.GeocoderStatus.OK) {
								var retval = results[0].geometry.location;
								
								callback.call(callbackScope ? callbackScope : this, retval);
							} else {
								callback.call(callbackScope ? callbackScope : this, null);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPositionForAddress of BaseGoogleMapsHelper', ex);
							callback.call(callbackScope ? callbackScope : this, null);
						}
					};
					
					myGeocoder.geocode( { 'address': addressAsString }, geocodeCallback);
				} else {
					callback.call(callbackScope ? callbackScope : this, null);
				}
			} else {
				callback.call(callbackScope ? callbackScope : this, null);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPositionForAddress of BaseGoogleMapsHelper', ex);
			callback.call(callbackScope ? callbackScope : this, null);
		}
	},
	
	//private
	getGeocoder: function() {
		var retval = null;
	
		try {
			if(!this._geocoder && this._libraryLoaded === true)
				this._geocoder = new google.maps.Geocoder();
		
			retval = this._geocoder;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeocoder of BaseGoogleMapsHelper', ex);
		}
		
		return retval;
	},
	
	buildGeocodeQueryStringFromAddress: function(address) {
		var retval = '';
		
		try {
			if(address) {
				retval = AssetManagement.customer.utils.StringUtils.concatenate([ address.get('country'),
							address.get('postCode1'), address.get('city1'), address.get('city2'), address.get('street'),
								address.get('houseNum1')], null, true).trim();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildGeocodeQueryStringFromAddress of BaseGoogleMapsHelper', ex);
		}
		
		return retval;
	},
	
	loadMapsLibrary: function(callback) {
		try {
			var me = this;

			if(this._libraryLoadingRequestIsPending === true) {
				this.waitForLibraryLoading(callback);
				return;
			} else if(this._libraryLoaded === true) {
				if(callback)
					callback.call(this);
				
				return;
			} else  {
				var onlineCallback = function(success) {
					if(!success) {
                        callback.call(this, false, me.self.ERROR_TYPES.BROWSER_OFFLINE);
                        return;
					} else {
                        me._libraryLoadingRequestIsPending = true;

                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.src = me.self.LIBRARY_URL; //contains the name of the callback
                        document.body.appendChild(script);

                        me.waitForLibraryLoading(callback);
					}
				}

                AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);



				// callback.call(this, false, this.self.ERROR_TYPES.BROWSER_OFFLINE);
				// return;
			}
		
			// this._libraryLoadingRequestIsPending = true;
			//
			// var script = document.createElement("script");
			// script.type = "text/javascript";
			// script.src = this.self.LIBRARY_URL; //contains the name of the callback
			// document.body.appendChild(script);
			//
			// this.waitForLibraryLoading(callback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMapsLibrary of BaseGoogleMapsHelper', ex);
		}
	},
	
	onLibraryLoaded: function() {
		try {
			AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.getInstance()._libraryLoadingRequestIsPending = false;
			AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.getInstance()._libraryLoaded = google !== null && google !== undefined;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLibraryLoaded of BaseGoogleMapsHelper', ex);
		}
	},
	
	//will check, if the library is available and call the callback, when the library has been loaded or the load failed
	waitForLibraryLoading: function(callback) {
		if(!callback)
			return;
	
		try {
			//if there still is a loading pending, it is neccessary to wait until it has completed
			if(this._libraryLoadingRequestIsPending === true) {
				Ext.defer(this.waitForLibraryLoading, 250, this, [ callback ]);
			} else {
				callback.call(this, this._libraryLoaded, !this._libraryLoaded ? this.self.ERROR_TYPES.API_NOT_RESPONDING : undefined);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside waitForLibraryLoading of BaseGoogleMapsHelper', ex);
		}
	},
	
	//expects the library already loaded
	getMapForDiv: function(mapsDiv, callback, callbackScope) {
		if(!callback)
			return;
	
		try {
			if(mapsDiv) {
				var mapCreationCallback = function(position, errorType) {
					try {
						var useOwnLocation = errorType === undefined;
						
						var mapOptions = {
						    zoom: 6,
						    center: new google.maps.LatLng(51, 10),
						    mapTypeId: google.maps.MapTypeId.ROADMAP
						}
						
						var map = new google.maps.Map(mapsDiv, mapOptions);
						retval = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMap', { map: map });
						
						//set a marker for own position
						if(useOwnLocation) {
							retval.setCenter(position);
							retval.setZoom(14);
							retval.setOwnPosition(position);
							retval.showOwnPosition();
						}
						
						callback.call(callbackScope ? callbackScope : this, retval);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMapForDiv of BaseGoogleMapsHelper', ex);
						callback.call(callbackScope ? callbackScope : this, null);
					}
				};
				
				this.getCurrentLocation(mapCreationCallback, this);
				
				//why the map is not created and "showOwnPositionOnMap" is called afterwards:
				//it causes a rendering bug, because the map will try to inflate directly after it's creation (when code runs into getCurrentLocation)
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMapForDiv of BaseGoogleMapsHelper', ex);
			callback.call(callbackScope ? callbackScope : this, null);
		}
	},
	
	//returns the current position as LatLng object
	getCurrentLocation: function(callback, callbackScope) {
		try {
			if(!callback)
				return;
			
			if(navigator.geolocation) {
				var me = this;
			
				var acceptedCallback = function(position) {
					try {
						var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
						callback.call(callbackScope ? callbackScope : me, pos);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentLocation of BaseGoogleMapsHelper', ex);
					}
				};
				
				var declineCallback = function() {
					try {
						callback.call(callbackScope ? callbackScope : me, null, me.self.ERROR_TYPES.GEO_DECLINED);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentLocation of BaseGoogleMapsHelper', ex);
					}
				};
			
				navigator.geolocation.getCurrentPosition(acceptedCallback, declineCallback);
			} else {
				callback.call(callbackScope ? callbackScope : this, null, this.self.ERROR_TYPES.GEO_NOT_SUP);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentLocation of BaseGoogleMapsHelper', ex);
		}
	}
});