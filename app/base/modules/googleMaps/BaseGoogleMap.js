Ext.define('AssetManagement.base.modules.googleMaps.BaseGoogleMap', {
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.helper.OxLogger'
	],

	config: {
		map: null,
		ownPosition: null,
		markers: [],
		infoWindows: []
	},
	
	_ownPositionVisible: false,
	_ownPositionMarker: null,
	_ownPositionInfoWindow: null,
	
	constructor: function(config) {
        this.initConfig(config);
    },
    
    reset: function() {
    	try {
    		this.setZoom(5);
    		this.hideOwnPosition();
    		this.removeAllMarkers();
    		this.removeAllInfoWindows();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reset of BaseGoogleMap', ex);
		}
	},
	
	setCenter: function(position) {
	   	try {
    		if(position) {
    			this.getMap().setCenter(position);
    		}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setCenter of BaseGoogleMap', ex);
		}
	},
	
	setZoom: function(zoomAsInt) {
	   	try {
	   		if(zoomAsInt && zoomAsInt > 18) {
	   			zoomAsInt = 18;
	   		} else if(zoomAsInt && zoomAsInt < 0) {
	   			zoomAsInt = 0;
	   		}
	   	
    		if(zoomAsInt) {
    			this.getMap().setZoom(zoomAsInt);
    		}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setZoom of BaseGoogleMap', ex);
		}
	},
    
    addMarker: function(marker) {
    	try {
    		if(marker) {
	    		marker.setMap(this._map);
	    	
	    		if(!Ext.Array.contains(this._markers, marker))
	    			this._markers.push(marker);
    		}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addMarker of BaseGoogleMap', ex);
		}
	},
	
	addMarkerWithHint: function(marker, hint) {
    	try {
    		if(marker) {
    			this.addMarker(marker);
    			
    			if(hint) {
    				this.showInfoWindowAtMarker(hint, marker);
    			}
    		}
    		
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addMarkerWithHint of BaseGoogleMap', ex);
		}
	},
	
	removeMarker: function(marker) {
    	try {
    		if(Ext.Array.contains(this._markers, marker)) {
    			Ext.Array.remove(this._markers, marker);
    			
    			marker.setMap(null);
    		}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeMarker of BaseGoogleMap', ex);
		}
	},
	
	removeAllMarkers: function() {
		try {
    		Ext.Array.each(this._markers, function(marker) {
    			marker.setMap(null);
    		}, this);
    		
    		this._markers = new Array();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeAllMarkers of BaseGoogleMap', ex);
		}
	},
	
	showInfoWindowAtPosition: function(content, position) {
		try {
			if(position && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(content)) {
				var infoWindow = new google.maps.InfoWindow({
				    content: content,
				    position: position
				});
				
				infoWindow.open(this.getMap());
				this._infoWindows.push(infoWindow);
			}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showInfoWindowAtPosition of BaseGoogleMap', ex);
		}
	},
	
	showInfoWindowAtMarker: function(content, marker) {
		try {
			if(marker && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(content)) {
				var infoWindow = new google.maps.InfoWindow({
				    content: content
				});
				
				infoWindow.open(this.getMap(), marker);
				this._infoWindows.push(infoWindow);
			}
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showInfoWindowAtMarker of BaseGoogleMap', ex);
		}
	},
	
	removeAllInfoWindows: function() {
		try {
    		Ext.Array.each(this._infoWindows, function(infoWindow) {
    			infoWindow.close();
    		}, this);
    		
    		this._infoWindows = new Array();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeAllInfoWindows of BaseGoogleMap', ex);
		}
	},
	
	//the passed position must be of type LatLng
	setOwnPosition: function(position) {
		try {
    		if(!position) {
    			this.hideOwnPosition();
    		} else if(this._ownPositionVisible === true) {
    			//update position of own position visuals
    			this._ownPositionMarker.setPosition(position);
    			this._ownPositionInfoWindow.open(this.getMap(), this._ownPositionMarker);
    		}
    		
    		this._ownPosition = position;
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setOwnPosition of BaseGoogleMap', ex);
		}
	},
	
	//will show a marker on the own position, including an info window
	showOwnPosition: function() {
		try {
    		if(!this._ownPosition) {
    			return;
    		} else if(this._ownPositionVisible === true) {
    			//show the infoWindow again and return
    			this._ownPositionInfoWindow.open(this.getMap(), this._ownPositionMarker);
    			return;
    		}
    		
    		
    		if(!this._ownPositionMarker) {
	    		this._ownPositionMarker = new google.maps.Marker({
	    			position: this._ownPosition,
	    			icon: 'resources/icons/myLocationMarker.png',
	    			title: Locale.getMsg('yourPosition')
				});
    		} else {
    			this._ownPositionMarker.setPosition(this._ownPosition);
    		}
    		
    		this.addMarker(this._ownPositionMarker);
    		
			if(!this._ownPositionInfoWindow) {
	    		this._ownPositionInfoWindow = new google.maps.InfoWindow({
				    content: Locale.getMsg('yourPosition')
				});
	    		
				this._infoWindows.push(this._ownPositionInfoWindow);
    		}
			
			this._ownPositionInfoWindow.open(this.getMap(), this._ownPositionMarker);
    		
    		this._ownPositionVisible = true;
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showOwnPosition of BaseGoogleMap', ex);
		}
	},
	
	//will hide the own position marker (and it's info window)
	hideOwnPosition: function() {
		try {
			if(this._ownPositionVisible === false) {
    			return;
    		}
			
    		this._ownPositionMarker.setMap(null);
    		this._ownPositionInfoWindow.close();
    		
    		this._ownPositionVisible = false;
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideOwnPosition of BaseGoogleMap', ex);
		}
	}
});