﻿Ext.define('AssetManagement.base.modules.search.BaseSearchDynamicCriteriaProvider', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.modules.search.SearchCriteria',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store'
    ],

    inheritableStatics: {
        //public
        generateDynamicSearchCriterias: function (searchArray, searchHandlersMixin) {
            var retval = null;
            try {
                var me = this;
                var criteriasStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.search.SearchCriteria',
                    autoLoad: false
                });

                if (searchArray && searchArray.length && searchArray.length > 0) {
                    for (var i = 0; i < searchArray.length; i++) {
                        criteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                            attribute: searchArray[i].id,
                            description: Locale.getMsg(searchArray[i].columnName),
                            searchFunction: searchArray[i].searchFunction,
                            searchHandlers: searchHandlersMixin
                        }));
                    }
                }

                retval = criteriasStore;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateDynamicSearchCriterias of BaseSearchDynamicCriteriaProvider', ex);
            }

            return retval;
        }
    }


});