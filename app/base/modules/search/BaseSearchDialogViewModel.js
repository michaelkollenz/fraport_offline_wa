﻿Ext.define('AssetManagement.base.modules.search.BaseSearchDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        source: null,
        criterias: null,
        autoCompletionMap: null
    },

    resetData: function () {
        try {
            this.callParent();

            this.setData({
                source: null,
                criterias: null,
                autoCompletionMap: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSearchDialogViewModel', ex);
        }
    }
});