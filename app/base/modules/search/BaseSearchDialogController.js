﻿Ext.define('AssetManagement.base.modules.search.BaseSearchDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
       'AssetManagement.customer.modules.search.SearchCriteriaProvider',
       'AssetManagement.customer.modules.search.AutoCompletionProvider',
       'AssetManagement.customer.modules.search.SearchExecutioner'
    ],

    config: {
        contextReadyLevel: 1
    },

    inheritableStatics: {
        //interface
        ON_SEARCH_APPLIED: 'onSearchApplied'
    },

    //public
    onApplySearchClicked: function () {
        try {
            this.applySearch();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onApplySearchClicked of BaseSearchDialogController', ex);
        }
    },

    //public
    onClearSearchClicked: function () {
        try {
            this.clearSearch();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchClicked of BaseSearchDialogController', ex);
        }
    },

    //protected
    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var source = null;
            var criteriaType = 0;
            var criterias = null;

            if (argumentsObject) {
                if (argumentsObject.source)
                    source = argumentsObject.source;

                if (argumentsObject.criteriaType)
                    criteriaType = argumentsObject.criteriaType;
                
                if (argumentsObject.criterias)
                    criterias = argumentsObject.criterias;
            }

            var myModel = this.getViewModel();

            myModel.set('source', source);

            if (criterias) {
                myModel.set('criterias', criterias);
            } else {
                this.loadSearchCriterias(criteriaType);
            }

            this.loadAutoCompletionMap();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseSearchDialogController', ex)
        }
    },

    //private
    loadSearchCriterias: function (criteriaType) {
        try {
            var myModel = this.getViewModel();

            var criterias = AssetManagement.customer.modules.search.SearchCriteriaProvider.getCriteriasForType(criteriaType);
            myModel.set('criterias', criterias);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadSearchCriterias of BaseSearchDialogController', ex);
        }
    },

    //private
    loadAutoCompletionMap: function () {
        try {
            var myModel = this.getViewModel();

            var autoCompletionMap = AssetManagement.customer.modules.search.AutoCompletionProvider.getAutoCompletionHashMapForSource(myModel.get('source'), myModel.get('criterias'));
            myModel.set('autoCompletionMap', autoCompletionMap);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadAutoCompletionMap of BaseSearchDialogController', ex);
        }
    },

    //private
    //applies the current criterias to the source and returns the search result to the owner via callback interface
    applySearch: function () {
        try {
            this.getView().transferViewStateIntoModel();

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            //use a time shift to ensure the loading indicator shows up before the heavy search work starts
            Ext.defer(function() {
                try {
                    var myModel = this.getViewModel();
                    var source = myModel.get('source');
                    var criterias = myModel.get('criterias');

                    var result = AssetManagement.customer.modules.search.SearchExecutioner.search(source, criterias);

                    var callbackInterface = this.getOwner()[this.self.ON_SEARCH_APPLIED];

                    if (callbackInterface)
                        callbackInterface.call(this.getOwner(), result);

                    //dismiss
                    this.dismiss();
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applySearch of BaseSearchDialogController', ex);
                } finally {
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            }, 100, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applySearch of BaseSearchDialogController', ex);
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //private
    //clears the view input controls, not the model
    clearSearch: function () {
        try {
            this.getView().clearCriterias();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearch of BaseSearchDialogController', ex);
        }
    }
});