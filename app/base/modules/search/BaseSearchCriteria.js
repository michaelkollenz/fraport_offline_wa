﻿Ext.define('AssetManagement.base.modules.search.BaseSearchCriteria', {
    extend: 'Ext.data.Model',

    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],

    //simple class, which encapsulates a search criteria
    //consist of:
    //  direct search attribute or get function for an attribute
    //  description for the criteria
    //  values to search for

    fields: [
		{ name: 'attribute', type: 'string' },
        { name: 'getAttrFunc', type: 'auto' },
		{ name: 'description', type: 'string' },
		{ name: 'values', type: 'auto', defaultValue: [] },

        //special fields:
        { name: 'allowsContains', type: 'boolean', defaultValue: true },
        { name: 'allowsMultipleValues', type: 'boolean', defaultValue: true },
        { name: 'usesIcons', type: 'boolean', defaultValue: false },
        { name: 'valueIconMap', type: 'auto' }
    ],

    getIconPathForValue: function (value) {
        var retval = '';

        try {
            var map = this.get('valueIconMap');

            retval = map ? map.get('value') : '';            
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getIconPathForValue of BaseSearchCriteria', ex);
        }

        return retval;
    }
});