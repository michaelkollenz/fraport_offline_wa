﻿Ext.define('AssetManagement.base.modules.search.BaseSearchCriteriaProvider', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.modules.search.SearchCriteria',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store',
        'AssetManagement.customer.manager.OrderManager',
        'AssetManagement.customer.manager.LocalOrderStatusManager'
    ],
	
    inheritableStatics: {
        SEARCH_TYPES: {
            ORDER: 1,
            NOTIF: 2,
            FLOC: 3,
            EQUI: 4,
            ASSIGNMENT: 5
        },

        //public
        getCriteriasForType: function (searchType) {
            var retval = null;

            try {
                switch (searchType) {
                    case this.SEARCH_TYPES.ORDER:
                        retval = this.getCriteriasForOrder();
                        break;
                    case this.SEARCH_TYPES.NOTIF:
                        retval = this.getCriteriasForNotif();
                        break;
                    case this.SEARCH_TYPES.FLOC:
                        retval = this.getCriteriasForFuncLoc();
                        break;
                    case this.SEARCH_TYPES.EQUI:
                        retval = this.getCriteriasForEquipment();
                        break;
                    case this.SEARCH_TYPES.ASSIGNMENT:
                        retval = this.getCriteriasForAssignment();
                        break;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCriteriasForType of BaseSearchCriteriaProvider', ex);
            }

            return retval;
        },

        //private
        getCriteriasForOrder: function () {
            var retval = null;

            try {
                var cirteriasStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.search.SearchCriteria',
                    autoLoad: false
                });

                retval = cirteriasStore;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCriteriasForOrder of BaseSearchCriteriaProvider', ex);
            }

            return retval;
        },

        //private
        getCriteriasForNotif: function () {
            var retval = null;

            try {
                var cirteriasStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.search.SearchCriteria',
                    autoLoad: false
                });

                retval = cirteriasStore;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCriteriasForNotif of BaseSearchCriteriaProvider', ex);
            }

            return retval;
        },

        //private
        getCriteriasForFuncLoc: function () {
            var retval = null;

            try {
                var cirteriasStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.search.SearchCriteria',
                    autoLoad: false
                });

                retval = cirteriasStore;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCriteriasForFuncLoc of BaseSearchCriteriaProvider', ex);
            }

            return retval;
        },

        //private
        getCriteriasForEquipment: function () {
            var retval = null;

            try {
                var cirteriasStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.search.SearchCriteria',
                    autoLoad: false
                });

                retval = cirteriasStore;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCriteriasForEquipment of BaseSearchCriteriaProvider', ex);
            }

            return retval;
        },

        //private
        getCriteriasForAssignment: function () {
            var retval = null;

            try {
                var cirteriasStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.search.SearchCriteria',
                    autoLoad: false
                });

                var typeIconMap = AssetManagement.customer.manager.OrderManager.getTypeIconPathMap();

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        return assignment ? (assignment.get('auart') + assignment.get('ilart')) : '';
                    },
                    description: Locale.getMsg('type'),
                    allowsContains: false,
                    usesIcons: true,
                    valueIconMap: typeIconMap
                }));

                var statusIconMap = AssetManagement.customer.manager.LocalOrderStatusManager.getStatusIconPathMap();

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    attribute: 'localStatusCode',
                    description: Locale.getMsg('status'),
                    allowsContains: false,
                    usesIcons: true,
                    valueIconMap: statusIconMap
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        var address = assignment ? assignment.getAddressForDisplay() : null;
                        return address ? address .getName() : '';
                    },
                    description: Locale.getMsg('customer')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        return assignment ? assignment.getWard() : '';
                    },
                    description: Locale.getMsg('mymWard')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        return assignment ? assignment.getPostalCode() : '';
                    },
                    description: Locale.getMsg('postalCode')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        var address = assignment ? assignment.getAddressForDisplay() : null;
                        return address ? address.getCity() : '';
                    },
                    description: Locale.getMsg('city')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        var address = assignment ? assignment.getAddressForDisplay() : null;
                        return address ? address.getStreet() : '';
                    },
                    description: Locale.getMsg('street')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        var reservation = assignment ? assignment.get('reservation') : null;
                        return reservation ? reservation.get('agreement') : '';
                    },
                    description: Locale.getMsg('mymAgreement')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        var reservation = assignment ? assignment.get('reservation') : null;
                        return reservation ? reservation.get('lresn') : '';
                    },
                    description: Locale.getMsg('mymReservation')
                }));

                cirteriasStore.add(Ext.create('AssetManagement.customer.modules.search.SearchCriteria', {
                    getAttrFunc: function (assignment) {
                        var reservation = assignment ? assignment.get('reservation') : null;
                        return reservation ? reservation.get('packageDesc') : '';
                    },
                    description: Locale.getMsg('mymPackage')
                }));

                retval = cirteriasStore;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCriteriasForAssignment of BaseSearchCriteriaProvider', ex);
            }

            return retval;
        }
    }
});