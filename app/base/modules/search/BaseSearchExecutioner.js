﻿Ext.define('AssetManagement.base.modules.search.BaseSearchExecutioner', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store'
    ],

    inheritableStatics: {
        //public
        //applies passed search criterias (as store) on a passed source store of models
        //returns a new store containing the matches
        //undefined will be returned on any error occurrance
        search: function (source, criterias) {
            var retval = undefined;

            try {
                if (source) {
                    var results = Ext.create('Ext.data.Store', {
                        model: source.getModel(),
                        autoLoad: false
                    });

                    if (source.getCount() > 0) {
                        if (!this.checkCriteriasForEmpty(criterias)) {
                            source.each(function (record) {
                                var matches = this.applySearchOnRecord(record, criterias);

                                if (matches)
                                    results.add(record);
                            }, this);
                        } else {
                            source.each(function (record) {
                                results.add(record);
                            }, this);
                        }
                    }

                    retval = results;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside search of BaseSearchExecutioner', ex);
            }

            return retval;
        },

        //public
        //checks if a criteria set has at least one not empty criteria
        //returns true on errors
        checkCriteriasForEmpty: function (criterias) {
            var retval = true;

            try {
                if (criterias && criterias.getCount() > 0) {
                    criterias.each(function (criteria) {
                        var criteriaValues = criteria.get('values');

                        if (criteriaValues && criteriaValues.length > 0) {
                            Ext.Array.each(criteriaValues, function (value) {
                                retval = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value);

                                //abort loop, if there has been any not empty value
                                return retval;
                            }, this);
                        }

                        //abort loop, if there has been any not empty value
                        return retval;
                    }, this);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkCriteriasForEmpty of BaseSearchExecutioner', ex);
            }

            return retval;

        },

        //private
        //checks, if a record matches the search - it has to match every non empty criteria
        applySearchOnRecord: function (record, criterias) {
            var retval = true;

            try {
                var failsAnyCriteria = false;

                criterias.each(function (criteria) {
                    var recordsValue = '';

                    var getFunction = criteria.get('getAttrFunc');

                    //receive the records attribute value
                    if (getFunction) {
                        recordsValue = getFunction(record);
                    } else {
                        recordsValue = record.get(criteria.get('attribute'))
                    }

                    failsAnyCriteria = !this.performMatchesCriteria(recordsValue, criteria);
                    
                    //abort loop, if a mismatch occurred on a criteria
                    return !failsAnyCriteria;
                }, this);

                retval = !failsAnyCriteria;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applySearchOnRecord of BaseSearchExecutioner', ex);
            }

            return retval;
        },

        //private
        //checks a value against a criteria
        //the check returns true, if a criteria contains only empty/null/undefined values
        performMatchesCriteria: function (value, criteria) {
            var retval = true;

            try {
                //matching rule for each criteria:
                // - record attribute value matches one of criterias values (empty values will be ignored)
                var criteriaValues = criteria.get('values');
                var checkFunction = criteria.get('allowsContains') ? this.performContains : this.performEquals;

                if (criteriaValues && criteriaValues.length > 0) {
                    var matchesAnyValue = false;
                    var allValuesEmpty = true;

                    Ext.Array.each(criteriaValues, function (criteriaValue) {
                        //skip empty values
                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(criteriaValue)) {
                            allValuesEmpty = false;
                            matchesAnyValue = checkFunction(value, criteriaValue);
                        }

                        //abort loop, if a match occurred on a criteria value
                        return !matchesAnyValue;
                    }, this);

                    retval = allValuesEmpty || matchesAnyValue;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performMatchesCriteria of BaseSearchExecutioner', ex);
            }

            return retval;
        },

        //private
        performEquals: function (value, anotherValue) {
            var retval = true;

            try {
                //use == operator to include conversion on different types (e.g. string / number)
                retval = value == anotherValue;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performEquals of BaseSearchExecutioner', ex);
            }

            return retval;
        },

        //private
        performContains: function (value, anotherValue) {
            var retval = true;

            try {
                retval = AssetManagement.customer.utils.StringUtils.contains(value + '', anotherValue + '');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContains of BaseSearchExecutioner', ex);
            }

            return retval;
        }
    }
});