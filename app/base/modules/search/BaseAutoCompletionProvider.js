﻿Ext.define('AssetManagement.base.modules.search.BaseAutoCompletionProvider', {
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'Ext.util.HashMap',
        'Ext.Array'
    ],

    inheritableStatics: {
        //public
        //generates a hash map for possible, unique and alphabetically sorted values for each search criteria based on a record store
        //keys of the hashmap will be id's of the criterias
        getAutoCompletionHashMapForSource: function (source, criterias) {
            var retval = null;

            try {

                if (criterias && criterias.getCount() > 0) {
                    var hashMap = this.initializeHashMapForCriterias(criterias);

                    if (hashMap && source && source.getCount() > 0) {
                        //begin value extraction - one criteria after another
                        criterias.each(function (criteria) {

                            var valuesArray = hashMap.get(criteria.getId());
                            var getFunction = criteria.get('getAttrFunc');

                            //receive the records attribute values
                            if (getFunction) {
                                //iterate each source record and extract it's attribute value
                                source.each(function (record) {
                                    valuesArray.push(getFunction(record));
                                }, this);
                            } else {
                                //iterate each source record and extract it's attribute value
                                var attribute = criteria.get('attribute');

                                source.each(function (record) {
                                    valuesArray.push(record.get(attribute));
                                }, this);
                            }
                        }, this);

                        this.removeDuplicateValuesForCriterias(hashMap);
                        this.sortValuesForCriterias(hashMap);

                        retval = hashMap;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAutoCompletionHashMapForSource of BaseAutoCompletionProvider', ex);
            }

            return retval;
        },

        //private
        //initializes a values hashmap with empty arrays for given criterias
        initializeHashMapForCriterias: function (criterias) {
            var retval = null;

            try {
                var hashMap = Ext.create('Ext.util.HashMap');

                criterias.each(function (criteria) {
                    hashMap.add(criteria.getId(), new Array());
                }, this);

                retval = hashMap;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeHashMapForCriterias of BaseAutoCompletionProvider', ex);
            }

            return retval;
        },

        //private
        //uniques each array inside a values hashmap
        removeDuplicateValuesForCriterias: function (hashMap) {
            try {
                //setting while iterating the hashmap causes problems
                //use second hash map to handle this problem
                var tempHashMap = Ext.create('Ext.util.HashMap');

                hashMap.each(function (key, value) {
                    tempHashMap.add(key, Ext.Array.unique(value));
                }, this);

                tempHashMap.each(function (key, value) {
                    hashMap.add(key, value);
                }, this);

                tempHashMap.clear();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeDuplicateValuesForCriterias of BaseAutoCompletionProvider', ex);
            }
        },

        //private
        //sorts each array inside a values hashmap alphabetically ascending
        sortValuesForCriterias: function (hashMap) {
            try {
                //setting while iterating the hashmap causes problems
                //use second hash map to handle this problem
                var tempHashMap = Ext.create('Ext.util.HashMap');

                hashMap.each(function (key, value) {
                    tempHashMap.add(key, Ext.Array.sort(value));
                }, this);

                tempHashMap.each(function (key, value) {
                    hashMap.add(key, value);
                }, this);

                tempHashMap.clear();
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sortValuesForCriterias of BaseAutoCompletionProvider', ex);
            }
        }
    }
});