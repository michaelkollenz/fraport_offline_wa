﻿Ext.define('AssetManagement.base.modules.search.BaseOxSearchIconsBar', {
    extend: 'Ext.form.FieldContainer',

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'Ext.util.HashMap',
        'Ext.Array',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.form.field.Checkbox'
    ],

    config: {
        labelStyle: 'padding-top: 7px;',
        height: 30,
        valueIconMap: null,
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        cls: 'oxSearchIconsBar'
    },

    _iconsContainer: null,
    _valueControlMap: null,

    constructor: function (config) {
        try {
            this.callParent(arguments);

            this._valueControlMap = Ext.create('Ext.util.HashMap');

            this.buildContent();

            if (config.values) {
                this.setValues(config.values);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxSearchIconsBar', ex);
        }
    },

    //private
    //generates a icon, based on a checkbox, for each value inside the value icon map, where the icon path is not empty
    buildContent: function () {
        try {
            var valueIconMap = this.getValueIconMap();

            if (valueIconMap && valueIconMap.getCount() > 0) {
                var isFirst = true;

                valueIconMap.each(function (value, iconPath) {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(iconPath)) {
                        var newCheckBox = Ext.create('Ext.form.field.Checkbox', {
                            margin: isFirst ? '0' : '0 0 0 10',
                            labelStyle: 'display: none',
                            width: 50,
                            cls: 'selectableIcon',
                            checkedCls: 'selected',
                            style: 'background-image: url(' + iconPath + '); '
                        });

                        isFirst = false;

                        this.add(newCheckBox);
                        this._valueControlMap.add(value, newCheckBox);
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent of BaseOxSearchIconsBar', ex);
        }
    },

    //public
    //returns all set values as an array
    getValues: function () {
        var retval = null;

        try {
            if (this._valueControlMap && this._valueControlMap.getCount() > 0) {
                var valuesArray = new Array();

                this._valueControlMap.each(function (value, checkBox) {
                    if (checkBox.getValue()) {
                        valuesArray.push(value);
                    }
                }, this);

                retval = valuesArray;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getValues of BaseOxSearchIconsBar', ex);
        }

        return retval;
    },

    //public
    //sets the passed values
    setValues: function (values) {
        try {
            if (this._valueControlMap && this._valueControlMap.getCount() > 0) {
                if (values && values.length > 0) {
                    this._valueControlMap.each(function (value, control) {
                        control.setValue(Ext.Array.contains(values, value));
                    }, this);
                } else {
                    this._valueControlMap.each(function (value, control) {
                        control.setValue(false);
                    }, this);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setValues of BaseOxSearchIconsBar', ex);
        }
    }
});