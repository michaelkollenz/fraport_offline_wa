﻿Ext.define('AssetManagement.base.modules.search.BaseSearchDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.modules.search.SearchDialogController',
        'AssetManagement.customer.modules.search.SearchDialogViewModel',
        'AssetManagement.customer.modules.search.OxSearchTextField',
        'AssetManagement.customer.modules.search.OxSearchIconsBar',
        'Ext.container.Container',
        'Ext.form.Label',
        'Ext.form.field.Date',
        'Ext.button.Button',
        'Ext.util.HashMap'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.modules.search.SearchDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseSearchDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'SearchDialogModel'
    },

    controller: 'SearchDialogController',
    header: false,
    modal: true,
    width: 450,
    //maxHeight: 400,
    floating: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    //private
    _criteriasContainer: null,
    _controlToCriteriaHashMap: null,

    constructor: function (config) {
        try {
            this.callParent(arguments);

            this._controlToCriteriaHashMap = Ext.create('Ext.util.HashMap');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseSearchDialog', ex);
        }
    },

    buildUserInterface: function () {
        try {
            this._criteriasContainer = Ext.create('Ext.container.Container', {
                flex: 1,
                overflowY: 'auto',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                ]
            });

            var items = [
		        {
		            xtype: 'container',
		            margin: '0 10 0 10',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'label',
		                    cls: 'oxHeaderLabel',
		                    text: Locale.getMsg('search')
		                },
		                {
		                    xtype: 'component',
		                    height: 10
		                },
                        this._criteriasContainer,
		                {
		                    xtype: 'component',
		                    height: 10
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton first',
		                            text: Locale.getMsg('search'),
		                            listeners: {
		                                click: 'onApplySearchClicked'
		                            }
		                        },
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton',
		                            text: Locale.getMsg('delete'),
		                            listeners: {
		                                click: 'onClearSearchClicked'
		                            }
		                        },
		                        {
		                            xtype: 'button',
		                            flex: 1,
		                            cls: 'oxDialogButton',
		                            text: Locale.getMsg('cancel'),
		                            listeners: {
		                                click: 'cancel'
		                            }
		                        }
		                    ]
		                }
		            ]
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseSearchDialog', ex);
        }
    },

    //protected
    //@override
    getDialogTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('assignmentSearch');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseSearchDialog', ex);
        }

        return retval;
    },

    //protected
    //@override
    updateDialogContent: function () {
        try {
            this.buildSearchCriteriaInputControls();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseSearchDialog', ex);
        }
    },

    //private
    buildSearchCriteriaInputControls: function () {
        try {
            var myModel = this.getViewModel();
            var criterias = myModel.get('criterias');

            this._criteriasContainer.removeAll();
            this._controlToCriteriaHashMap.clear();

            if (criterias && criterias.getCount() > 0) {
                //iterate criterias and build the single controls
                criterias.each(function (criteria) {
                    var newControl = null;

                    var usesIcons = criteria.get('usesIcons');

                    if (usesIcons) {
                        newControl = this.getNewSearchIconsBar(criteria);
                    } else {
                        newControl = this.getNewSearchTextField(criteria);
                    }

                    if (newControl) {
                        this._criteriasContainer.add(newControl);
                        this._controlToCriteriaHashMap.add(criteria.getId(), newControl);
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSearchCriteriaInputControls of BaseSearchDialog', ex);
        }
    },

    //private
    //generates a new OxSearchTextField for a given criteria and tries to set it's auto completion
    getNewSearchTextField: function (criteria) {
        var retval = null;

        try {
            var searchField = Ext.create('AssetManagement.customer.modules.search.OxSearchTextField', {
                fieldLabel: criteria.get('description'),
                values: criteria.get('values')
            });

            var autoCompletionMap = this.getViewModel().get('autoCompletionMap');
            var autoCompletionValues = autoCompletionMap ? autoCompletionMap.get(criteria.getId()) : null;

            if (autoCompletionValues && autoCompletionValues.length > 0)
                searchField.setStore(autoCompletionValues);

            retval = searchField;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewSearchTextField of BaseSearchDialog', ex);
        }

        return retval;
    },

    //private
    //generates a new OxSearchIconsBar for a given criteria
    getNewSearchIconsBar: function (criteria) {
        var retval = null;

        try {
            var iconsBar = Ext.create('AssetManagement.customer.modules.search.OxSearchIconsBar', {
                fieldLabel: criteria.get('description'),
                valueIconMap: criteria.get('valueIconMap'),
                values: criteria.get('values')
            });

            retval = iconsBar;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNewSearchIconsBar of BaseSearchDialog', ex);
        }

        return retval;
    },

    //public
    //transfers the current set search criterias on controls layer into the model
    transferViewStateIntoModel: function () {
        try {
            var hashMap = this._controlToCriteriaHashMap;
            
            if (hashMap && hashMap.getCount() > 0) {
                var criterias = this.getViewModel().get('criterias');

                hashMap.each(function (criteriaId, control) {
                    var criteria = criterias.getById(criteriaId);

                    criteria.set('values', control.getValues());
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferViewStateIntoModel of BaseSearchDialog', ex);
        }
    },

    //public
    //clears the criterias on control layer
    clearCriterias: function () {
        try {
            var hashMap = this._controlToCriteriaHashMap;

            if (hashMap && hashMap.getCount() > 0) {
                hashMap.each(function (criteriaId, control) {
                    control.setValues(null);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearCriterias of BaseSearchDialog', ex);
        }
    }
});