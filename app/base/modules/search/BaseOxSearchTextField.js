﻿Ext.define('AssetManagement.base.modules.search.BaseOxSearchTextField', {
    extend: 'AssetManagement.customer.view.utils.OxAutoCompleteTextField',

    config: {
        cls: 'OxSearchTextFieldStyle',
        labelStyle: 'padding-top: 7px;',
        height: 30,
        useAutoCompletion: true,
        typeAhead: true,
        hideTrigger: false,
        includeSubMatches: true
    },

    inheritableStatics: {
        DELIMITER: ';'
    },

    constructor: function (config) {
        try {
            //search text field uses auto completion by default
            //if useAutoCompletion is disabled at config, extend it by additional parameters, which will disable all features at the base class
            if (config.useAutoCompletion === false) {
                config.typeAhead = false;
                config.hideTrigger = true;
                config.includeSubMatches = false;
            }

            this.callParent(arguments);

            if (config.values)
                this.setValues(config.values);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxSearchTextField', ex);
        }
    },

    //public
    //returns all set values as an array - splits the text fields value by the delimiter
    getValues: function () {
        var retval = null;

        try {
            var currentValue = this.getValue();
            retval = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(currentValue) ? currentValue.split(this.self.DELIMITER) : [];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getValues of BaseOxSearchTextField', ex);
        }

        return retval;
    },

    //public
    //sets the passed values - joins the values with the delimiter
    setValues: function (values) {
        try {
            var newValue = '';

            if (values && values.length > 0) {
                newValue = values.join(this.self.DELIMITER);
            }

            this.setValue(newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setValues of BaseOxSearchTextField', ex);
        }
    }
});