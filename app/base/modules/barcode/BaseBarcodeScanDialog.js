Ext.define('AssetManagement.base.modules.barcode.BaseBarcodeScanDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',

    requires: [
        'AssetManagement.customer.modules.barcode.BarcodeScanDialogController',
        'AssetManagement.customer.modules.barcode.BarcodeScanDialogViewModel',
        'Ext.button.Button',
        'Ext.form.Label',
        'Ext.container.Container',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.FileSelectionButton'
    ],

    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
                if(this._instance === null) {
                   this._instance = Ext.create('AssetManagement.customer.modules.barcode.BarcodeScanDialog');
                }            
            } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseBarcodeScanDialog', ex);
		    }
            
            return this._instance;
        }
    },

    viewModel: {
        type: 'BarcodeScanDialogModel'
    },
    
    controller: 'BarcodeScanDialogController',
    minWidth: 600,
    header: false,
    modal: true,
    resizable: false,
    
    _videoElementsContainer: null,
    _headerLabel: null,				//reference for workaround
    _videoElement: null,
    _manualInputTextField: null,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    buildUserInterface: function() {
    	try {
    		this._headerLabel = Ext.create('Ext.form.Label', {
                cls: 'oxHeaderLabel',
	            height: 25,
	            text: Locale.getMsg('scanForBarcode')
			});
    	
    		this._videoElementsContainer = Ext.create('Ext.container.Container', {
            	id: 'scanDialogVideoElementContainer'
    		});

    		this._manualInputTextField = Ext.create('AssetManagement.customer.view.utils.OxTextField', {
    		    width: 280,
    		    margin: '0 0 0 2',
    		    fieldLabel: Locale.getMsg('manualInput'),
    		    labelStyle: 'color: black;',
    		    labelWidth: 110,
    		    maxLength: 18
    		});
    		
    		var fileButton = Ext.create('AssetManagement.customer.view.utils.FileSelectionButton', {
				flex: 1,
				labelText: Locale.getMsg('fromFile'),
				padding: '9 5 5 5',
				height: 36,
				imageSrc: '',
				textAlignment: 'center',
				useEdgedCorners: true,
	            selectionCallback: this.getController().onFileSelected,
	            selectionCallbackScope: this.getController()
			});
    		
		    var items = [
		        {
			        xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                 {
		                    xtype: 'component',
		                    height: 20
		                },
		                this._headerLabel,
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                this._videoElementsContainer,
                        {
                            xtype: 'component',
                            height: 15
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'center'
                            },
                            items: [
                                this._manualInputTextField,
                                {
                                    type: 'component',
                                    width: 15
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    style: 'border-radius: 5px;',
                                    text: Locale.getMsg('barcodeApplyManualInput'),
                                    listeners: {
                                        click: 'onUseManualInputSelected'
                                    }
                                }
                            ]
                        },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'stretch'
		                    },
		                    cls: 'oxDialogButtonBar',
		                    items: [
		                        fileButton,
		                        {
				                    xtype: 'button',
				                    flex: 1,
				                    cls: 'oxDialogButton',
				                    text: Locale.getMsg('cancel'),
					                listeners: {
				                		click: 'cancel'
				                	}
				                }
		                    ]
		                }
		            ],
		            listeners: {
	    				boxready: this.includeVideoElement,
			            widthChange: this.adoptNewSizeToVideoElement,
		        		scope: this
		            }
		        },
		        {
			        xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
		    ];
		    
		    this.add(items);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseBarcodeScanDialog', ex);
		}
	},
    
	//protected
	//@override
	getDialogTitle: function() {
	    var retval = '';
	    
	    try {
	        retval = Locale.getMsg('scanForBarcode');
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseBarcodeScanDialog', ex);
		}
	    
		return retval;
	},
   
	//public
	//will set the video element and include it in the view
	//if the dialog has not yet been included in the dom, this will not be possible and be done later
	setVideoElement: function(videoElement) {
	    try {
	    	this._videoElement = videoElement;
	    	
	    	if(!!this.getEl())
	    		this.includeVideoElement();
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setVideoElement of BaseBarcodeScanDialog', ex);
		}
	},

    //public
	getManualInput: function () {
	    var retval = '';

	    try {
	        retval = this._manualInputTextField.getValue();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getManualInput of BaseBarcodeScanDialog', ex);
	    }

	    return retval;
	},

    //public
	resetManualInput: function () {
	    try {
	        this._manualInputTextField.setValue('');
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetManualInput of BaseBarcodeScanDialog', ex);
	    }
	},
	
	//private
	//puts the passed video element inside it's video container
	includeVideoElement: function() {
	    try {
	    	var widthToSet = this._videoElementsContainer.getWidth();
	    
	    	//workaround, for case containers width is broken
	    	if(widthToSet < 2) {
	    		//draw correct size from header label
	    		widthToSet = this._headerLabel.getWidth();
	    		this._videoElementsContainer.setWidth(widthToSet);
	    	}
	    
			this._videoElement.width = widthToSet;
			
			var heightToUse = this._videoElement.width * 0.75;
			
			this._videoElementsContainer.setHeight(heightToUse);
			this._videoElement.height = heightToUse;
			
			var videoContainer = document.getElementById('scanDialogVideoElementContainer');
			var appendNode = videoContainer;
			
			while(appendNode.firstChild) {
				appendNode = appendNode.firstChild;
			};
			
			appendNode.appendChild(this._videoElement);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside includeVideoElement of BaseBarcodeScanDialog', ex);
		}
	},
	
	//private
	//will set the new width on the video element and adjust it's height
	adoptNewSizeToVideoElement: function(mainVerticalContainer, newWidth, oldWidth) {
	    try {
	    	//set width
	    	//ensure video elements container has the full available width - sometimes it is broken
	    	if(this._videoElementContainer.getWidth() !== newWidth) {
	    		this._videoElementContainer.setWidth(newWidth);
	    	}
	    
	    	this._videoElement.width = newWidth;
			
	    	//set height
			var heightToUse = this._videoElement.width * 0.75;
			
			this._videoElementContainer.setHeight(heightToUse)
			this._videoElement.height = heightToUse;
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adoptNewSizeToVideoElement of BaseBarcodeScanDialog', ex);
		}
	}
});