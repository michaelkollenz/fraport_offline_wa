Ext.define('AssetManagement.base.modules.barcode.BaseBarcodeScanButton', {
    extend: 'Ext.Container',

    
	requires: [
	   'AssetManagement.customer.view.utils.FileSelectionButton',
	   'AssetManagement.customer.helper.FileHelper',
	   'AssetManagement.customer.modules.barcode.BarcodeManager',
	   'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
		cls: 'oxBarcodeScanButton',
	    labelText: '',
	    barcodeScanCallback: false,
	    barcodeScanCallbackScope: null,
	    height: 50,
	    width: 50,
	    imageSrc: 'resources/icons/rfid.png',
	    useVideoStream: true,
	    layout: {
			type: 'vbox',
			align: 'stretch'
		}
	},
	
	//private
	_buttonPadding: '5',
	
    constructor: function(config) {
		try {
			if(config) {
				if(config.padding) {
					this._buttonPadding = config.padding;
					config.padding = 0;
				}
			}
		
			this.callParent(arguments);
			
			this.buildUiContent();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseBarcodeScanButton', ex);
		}
	},
	
	//private
	//sets up ui content depending on how the barcode process is requested and/or able to work
	buildUiContent: function() {
		try {
			navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
			
			//check, if the video stream api is requested and available
			if(this.getUseVideoStream() && navigator.getUserMedia) {
				var useWidthAttribute = this.width < this.height;
				
				var htmlForButton = '<img src="' + this.getImageSrc() + '"';
				
				if(useWidthAttribute) {
					//constant 2 includes buttons border width of 1
					htmlForButton += ' width="' + (this.width - this.getPaddingWidthRequirements() - 2) + 'px"';
				} else {
					//constant 2 includes buttons border width of 1
					htmlForButton += ' height="' + (this.height - this.getPaddingHeightRequirements() - 2) + 'px"';
				}
				
				var hasTextPhrase = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getLabelText());
				
				if(hasTextPhrase) {
					htmlForButton += ' style="vertical-align: middle;"'
				}
				
				htmlForButton += '/>';
				
				if(hasTextPhrase) {
					htmlForButton +='<span style="font-size: 13px; color: black; padding-left: 5px;">' + this.getLabelText() + '</span>';
				}
				
				var scanButton = Ext.create('Ext.button.Button', {
					cls: 'barcodeScanButton',
					padding: this._buttonPadding,
	                html: htmlForButton,
		            height: this.height,
		            width: this.width,
		            listeners: {
		            	click: this.onScanButtonClicked,
		            	scope: this
		            }
				});
				
				this.add(scanButton);
			} else {
				//else use the manual source selection mode using the file api
				var fileSelectButton = Ext.create('AssetManagement.customer.view.utils.FileSelectionButton', {
					height: this.height,
					width: this.width,
					padding: this._buttonPadding,
					imageSrc: this.getImageSrc(),
	                labelText: this.getLabelText(),
	                selectionCallback: this.onSourceSelected,
	                selectionCallbackScope: this
				});
				
				this.add(fileSelectButton);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUiContent of BaseBarcodeScanButton', ex);
		}
	},
	
	onSourceSelected: function(file) {
		try {
			if(!file) {
				this.callScanCallback(null);
			} else {
				var mimeType = AssetManagement.customer.helper.FileHelper.getMimeTypeOfFile(file);
				
				//check if the chosen file is of type image
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mimeType) && mimeType.toLowerCase().indexOf('image') > -1) {
					AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
					
					var innerCallback = function(result) {
						try {
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							
							if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(result)) {
								this.callScanCallback.call(this, result);
							} else {
								this.askForRetryWithAnotherSourceOrManualInput();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSourceSelected of BaseBarcodeScanButton', ex);
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					};
					
					//convert the file data into base64 representation
					var base64 = AssetManagement.customer.helper.FileHelper.getBase64StringForFile(file);
				
					if(base64) {
						AssetManagement.customer.modules.barcode.BarcodeManager.getInstance().processImage(base64, innerCallback, this);
					} else {
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						this.callScanCallback(null);
					}
				} else {
					//user did not select an image file, stop processing (else barcode reader would run into an infinite loop)
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('chosenFiletypeNotSupported'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					
					this.callScanCallback.call(this, null);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSourceSelected of BaseBarcodeScanButton', ex);
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	//private
    //will trigger a non success dialog asking, if the user wants to try with another source or wants to enter input manually
	askForRetryWithAnotherSourceOrManualInput: function () {
		try {
			var dialogArgs = {
			    sourceCallback: this.onSourceSelected,
			    manualInputCallback: this.callScanCallback,
				scope: this
			};
			
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.BARCODE_RETRY_DIALOG, dialogArgs);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside askForRetryWithAnotherSourceOrManualInput of BaseBarcodeScanButton', ex);
		}
	},
	
	onScanButtonClicked: function() {
		try {
			var dialogArgs = {
				resultCallback: this.callScanCallback,
				resultCallbackScope: this
			};

			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.BARCODE_SCAN_DIALOG, dialogArgs);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onScanButtonClicked of BaseBarcodeScanButton', ex);
		}
	},
	
	callScanCallback: function(result) {
		try {
			var callback = this.getBarcodeScanCallback();
			var scope = this.getBarcodeScanCallbackScope();
			
			if(callback)
				callback.call(scope ? scope : window, result);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callScanCallback of BaseBarcodeScanButton', ex);
		}
	},
	
	getPaddingHeightRequirements: function() {
		var retval = 0;
		
		try {
			var values = this._buttonPadding.split(' ');
			
			if(values && values.length > 0) {
				switch(values.length) {
					case 1:
						retval = parseFloat(values[0]) * 2;
						break;
					case 2:
						retval = parseFloat(values[0]) * 2;
						break;
					case 4:
						retval = parseFloat(values[0]) + parseFloat(values[2]);
						break;
					default:
						break;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPaddingHeightRequirements of BaseBarcodeScanButton', ex);
			retval = 0;
		}
	
		return retval;
	},
	
	getPaddingWidthRequirements: function() {
		var retval = 0;
		
		try {
			var values = this._buttonPadding.split(' ');
			
			if(values && values.length > 0) {
				switch(values.length) {
					case 1:
						retval = parseFloat(values[0]) * 2;
						break;
					case 2:
						retval = parseFloat(values[1]) * 2;
						break;
					case 4:
						retval = parseFloat(values[1]) + parseFloat(values[3]);
						break;
					default:
						break;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPaddingWidthRequirements of BaseBarcodeScanButton', ex);
			retval = 0;
		}
	
		return retval;
	}
});