Ext.define('AssetManagement.base.modules.barcode.BaseBarcodeManager', {
	requires: [
	    'AssetManagement.customer.helper.FileHelper',
		'AssetManagement.customer.helper.OxLogger'
	],
 		
	//private:
	_libraryLoaded: false,
	_libraryLoadingPending: false,
	_currentlyScanning: false,
	
	_liveVideoElement: null,
	_currentDetectionCallback: null,
	_currentDetectionCallbackScope: null,
	
	inheritableStatics: {
		PATCH_SIZE_ORDER: [ 'medium', 'small', 'large', 'x-small', 'x-large' ],
	
		_instance: null,

		getInstance: function() {
			if (this._instance == null)
				this._instance = Ext.create('AssetManagement.customer.modules.barcode.BarcodeManager');

			return this._instance;
		}
	},
	
	constructor: function(config) {	    
	    try {
	        this.initialize();
	    } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseBarcodeManager', ex);
	    }
	},
	
	//private
	initialize: function() {
		try {
			this._libraryLoadingPending = true;
			
			navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

			//generate the video element for livestreaming and add it to the document
			if(navigator.getUserMedia) {
				this._liveVideoElement = document.createElement('video');
				this._liveVideoElement.id = 'barcodeVideoStream';
			}
		
			var libraryLoadingCallback = function(success) {
				try {
					if(!success)
						success = false;
				
					this._libraryLoaded = success;
				
					if(success === true) {
				    	//register the callbacks
						var me = this;
				    	
				    	Quagga.onProcessed(function(data) {
				    		me.processionCallback.call(me, data)
				    	});
				    	Quagga.onDetected(function(data) {
				    		me.detectionCallback.call(me, data)
				    	});
					} else {
						console.log('Failure loading barcode library');
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseBarcodeManager', ex);
				} finally {
					this._libraryLoadingPending = false;
				}
			};
			
			//load the library
			AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/quagga.js', true, libraryLoadingCallback, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseBarcodeManager', ex);
			this._libraryLoadingPending = false;
		}
	},
	
	//public
	//processes a single image
	processImage: function(imageBase64Data, processCallback, callbackScope) {
		try {
			//do not start working, if processCallback is missing
			if(!processCallback)
				return;
		
			//return, if there currently is another scan running
			if(this._currentlyScanning) {
				processCallback.call(callbackScope ? callbackScope : window, null, Locale.getMsg('anotherBarcodeScanIsRunning'));
				return;
			}
			
			if(!imageBase64Data)
				processCallback.call(callbackScope ? callbackScope : window, null);
			
			//before the scan may start, ensure the barcode library has been loaded
			//if the loading is still pending, wait for it (retry in 250ms)
			if(this._libraryLoadingPending) {
				Ext.defer(this.processImage, 250, this, [ imageBase64Data, processCallback, callbackScope ]);
				return;
			} else if(this._libraryLoaded === false) {
				//the module could not be loaded, call errorCallback and stop the process
				if(errorCallback)
					processCallback.call(callbackScope ? callbackScope : window, null);
			} else {
				var innerProcessCallback = function(result) {
					try {
						this._currentlyScanning = false;
						processCallback.call(callbackScope ? callbackScope : window, result);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processImage of BaseBarcodeManager', ex);
						processCallback.call(callbackScope ? callbackScope : window, null);
					}
				};
				
				this._currentlyScanning = true;
				
				this.processImageInternal(imageBase64Data, innerProcessCallback);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processImage of BaseBarcodeManager', ex);
			processCallback.call(callbackScope ? callbackScope : window, null);
		}
	},
	
	//public
	//will try to start scanning for a barcode using the clients CAMERA STREAM
	//to use a live feed of the camera, provide a callback for videoCallback. It will be called with an video element attached to the used video stream
	startScanning: function(detectionCallback, errorCallback, videoCallback, callbackScope) {
		try {
			//do not start scanning, if detectionCallback is missing
			if(!detectionCallback)
				return;
		
			//return, if there currently is another scan running
			if(this._currentlyScanning) {
				detectionCallback.call(callbackScope ? callbackScope : window, null, Locale.getMsg('anotherBarcodeScanIsRunning'));
				return;
			}
				
			//before the scan may start, ensure the barcode library has been loaded
			//if the loading is still pending, wait for it (retry in 250ms)
			if(this._libraryLoadingPending) {
				Ext.defer(this.startScanning, 250, this, [ detectionCallback, errorCallback, videoCallback, callbackScope ]);
				return;
			} else if(this._libraryLoaded === false) {
				//the module could not be loaded, call errorCallback and stop the process
				if(errorCallback)
					errorCallback.call(callbackScope ? callbackScope : window, Locale.getMsg('barcodeReaderCouldNotBeInitialized'));
			} else {
				//try to setup the video stream next
				var setupCallback = function(success, errorMessage) {
					try {
						if(success) {
							this._currentlyScanning = true;
							
							//set the current callbacks
							this._currentDetectionCallback = detectionCallback;
							this._currentDetectionCallbackScope = callbackScope;
							
							if(videoCallback) {
								videoCallback.call(callbackScope ? callbackScope : window, this._liveVideoElement);
							}
							
							//start the scanning process
							Quagga.start();
						} else {
							if(!errorMessage)
								errorMessage = Locale.getMsg('barcodeReaderCouldNotBeInitialized');
						
							if(errorCallback)
								errorCallback.call(callbackScope ? callbackScope : window, errorMessage);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startScanning of BaseBarcodeManager', ex);
						
						if(errorCallback)
							errorCallback.call(callbackScope ? callbackScope : window, Locale.getMsg('barcodeReaderCouldNotBeInitialized'));
					}
				}
				
				this.setupLiveVideoStream(setupCallback);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startScanning of BaseBarcodeManager', ex);
			
			if(errorCallback)
				errorCallback.call(callbackScope ? callbackScope : window, Locale.getMsg('barcodeReaderCouldNotBeInitialized'));
		}
	},
	
	//public
	//will cancel a currently running camera based barcode scan
	cancelScanning: function() {
		try {
			if(this._currentlyScanning) {
				Quagga.stop();
				
				this._currentDetectionCallback = null;
				this._currentDetectionCallbackScope = null;
				this._currentlyScanning = false;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelScanning of BaseBarcodeManager', ex);
		}
	},
	
	//private
	//processes a single image - will try different patch sizes
	processImageInternal: function(imageBase64Data, processCallback) {
		try {
			if(!imageBase64Data || !processCallback)
				return;
			
			var me = this;
				
			//the library is only capable of processing at one patch size for once (size of the barcode inside the image)
			//there are 5 patch sizes supported - the processing order is defines as constant
			var patchOrder = this.self.PATCH_SIZE_ORDER;
			
			var interationsDone = 0;
			var doneCount = patchOrder.length;
			
			var decodeSingleOptions = {
				decoder: {
		        	readers: ["code_128_reader"]
			    },
			    locate: true,
			    src: imageBase64Data,
			    locator: {
			    	patchSize: patchOrder[interationsDone]
			    }
			};
			
			//declare variable here, to use it reuse it inside itself
			var innerProcessCallback = null;
			
			innerProcessCallback = function(data) {
				try {
					interationsDone++;
				
					if(data && data.codeResult) {
						//detected a barcode - abort the loop and return
						var result = data.codeResult.code;
						processCallback.call(me, result);
					} else if(interationsDone == doneCount) {
						//no barcode detected for any patch size - return
						processCallback.call(me, null);
					} else {
						//no barcode detected, try next patch size
						decodeSingleOptions.locator.patchSize = patchOrder[interationsDone];
						Quagga.decodeSingle(decodeSingleOptions, innerProcessCallback);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processImageInternal of BaseBarcodeManager', ex);
					processCallback.call(me, null);
				}
			};
			
			//start the first procession
			Quagga.decodeSingle(decodeSingleOptions, innerProcessCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processImageInternal of BaseBarcodeManager', ex);
			processCallback.call(this, null);
		}
	},
	
	//private
	//tries to setup a video live stream
	//will uses the managers liveVideoElement to transfer the stream over
	setupLiveVideoStream: function(callback) {
		try {
			var me = this;
			
			if(!callback)
				return;
				
			//the liveVideoElement may be part of the dom already, because some ui element already used it
			//first remove it from the dom again
			var videoElement = this._liveVideoElement;
			
			if(videoElement.parentNode)
				videoElement.parentNode.removeChild(videoElement);
			
			//put the video element at top of the body, that ensures, the barcode library will use it to transfer it's stream over it
			videoElement.style.display = 'none';
			var body = document.getElementsByTagName('body')[0];
			body.insertBefore(videoElement, body.firstChild);
			
			var me = this;

			var initCallback = function(errorObject) {
				try {
					var success = !errorObject;
					var errorMessage = '';
				
					if(success) {
						//remove the video element again, it will be passed to the scan process initiator again
						//removing stops the video, so resume it
						body.removeChild(videoElement);
						videoElement.style.display = '';
						
						videoElement.play();
					
						console.log("Barcode reader is ready to start.");
				    } else {
				    	errorMessage = me.getErrorMessageForErrorObject(errorObject);
				    	console.log('Barcode live video setup failed: ' + errorMessage);
				    }
					
				    callback.call(me, success, errorMessage);
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupLiveVideoStream of BaseBarcodeManager', ex);
					
					callback.call(me, false);
				}
			};

			var inputOptions = {
				name : "Live",
			    type : "LiveStream",
			    constraints: {
					width: 800,
					height: 600,
					facing: "environment"
			  	}
			};
			
			var decodeOptions = {
				readers : ["code_128_reader"]
			};
			
			var initObject = {
				inputStream: inputOptions,
				decoder: decodeOptions
			};
			
			Quagga.init(initObject, initCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupLiveVideoStream of BaseBarcodeManager', ex);
			
			callback.call(this, false);
		}
	},
	
	//private
	//inner callback for processed frames
	processionCallback: function(data) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processionCallback of BaseBarcodeManager', ex);
		}
	},
	
	//private
	//inner callback for code detection
	detectionCallback: function(data) {
		try {
			//buffer references to current callback options before cancelling scanning
			var callback = this._currentDetectionCallback;
			var callbackScope = this._currentDetectionCallbackScope;
		
			//stop scanning
			this.cancelScanning();
		
			//use the current set callback to pass the result
			if(callback) {
				var scopeToUse = callbackScope ? callbackScope : window;
				
				var result = null;

				if(data && data.codeResult)
					result = data.codeResult.code;
				
				callback.call(scopeToUse, result);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside detectionCallback of BaseBarcodeManager', ex);
		}
	},
	
	//private
	//will examine a userMediaErrorObject and get localized error message for it
	getErrorMessageForErrorObject: function(errorObject) {
		var retval = '';
	
		try {
			if(errorObject) {
				switch(errorObject.name) {
					case 'PermissionDeniedError':
						retval = Locale.getMsg('cameraAccessHasBeenBlocked');
						break;
					default:
						retval = errorObject.message;
						break;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getErrorMessageForErrorObject of BaseBarcodeManager', ex);
		}
		
		return retval;
	}
});