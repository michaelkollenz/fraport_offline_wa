Ext.define('AssetManagement.base.modules.barcode.BaseBarcodeRetryDialog', {
	extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.modules.barcode.BarcodeRetryDialogController',
        'AssetManagement.customer.modules.barcode.BarcodeRetryDialogViewModel',
        'Ext.container.Container',
        'Ext.Component',
        'Ext.button.Button',
        'AssetManagement.customer.view.utils.OxTextField',
        'AssetManagement.customer.view.utils.FileSelectionButton',
        'AssetManagement.customer.utils.StringUtils'
    ],
	
    viewModel: {
        type: 'BarcodeRetryDialogModel'
    },
    
    controller: 'BarcodeRetryDialogController',
    cls: 'oxDialogButtonBar',
    width: 430,
    header: false,
    modal: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    //members
    _icon: null,
    _messageLabel: null,
    _manualInputTextField: null,
    _retryButton: null,
    _cancelButton: null,
    
    inheritableStatics: {
    	_instance: null,
    
        getInstance: function() {
	        try {
    		    if(!this._instance)
    		    	this._instance = Ext.create('AssetManagement.customer.modules.barcode.BarcodeRetryDialog');
    		} catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseBarcodeRetryDialog', ex);
		    }
    			
            return this._instance;
        }
    },
   
    buildUserInterface: function() {
    	try {
    		this._icon = Ext.create('Ext.Img', {
    			width: 40,
    			height: 40,
    			cls: 'alertDialogIcon',
    		    src: 'resources/icons/rfid.png'
    		});
    	
        	this._messageLabel = Ext.create('Ext.form.Label', {
        		flex: 1,
        		cls: 'alertDialogMessageLabel',
        		html: Locale.getMsg('noBarcodeDetected') + '<br>' + Locale.getMsg('retryAnotherPictureQuestion')
        	});

        	this._manualInputTextField = Ext.create('AssetManagement.customer.view.utils.OxTextField', {
        	    width: 280,
                margin: '0 0 0 2',
        	    fieldLabel: Locale.getMsg('manualInput'),
                labelStyle: 'color: black;',
        	    labelWidth: 110,
                maxLength: 18
        	});
        	
        	this._retryButton = Ext.create('AssetManagement.customer.view.utils.FileSelectionButton', {
				flex: 1,
				labelText: Locale.getMsg('barcodeRetryPictureText'),
				padding: '9 5 5 5',
				height: 36,
				imageSrc: '',
				textAlignment: 'center',
				useEdgedCorners: true,
	            selectionCallback: this.getController().onRetryFileSelected,
	            selectionCallbackScope: this.getController()
			});
        	
	    	this._cancelButton = Ext.create('Ext.button.Button', {
        		flex: 1,
	            cls: 'oxDialogButton',
	            text: Locale.getMsg('cancel'),
	            listeners: {
	        		click: 'cancel'
	        	}
	    	});
    	
	    	var items =  [
		        {
		            xtype: 'component',
		            width: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'component',
		                    height: 30
		                },
		                {
		                    xtype: 'container',
		                    layout: {
		                        type: 'hbox',
		                        align: 'center',
		                        pack: 'stretch'
		                    },
		                    items: [
		                        this._icon,
		                        {
		                        	type: 'component',
		                        	width: 15
		                        },
		                        this._messageLabel
		                    ]
		                },
                        {
                        	xtype: 'component',
                        	height: 15
                        },
                        {
                        	xtype: 'container',
                        	layout: {
                        		type: 'hbox',
                        		align: 'center'
                        	},
                        	items: [
                                this._manualInputTextField,
                                {
                                    type: 'component',
                                    width: 15
                                },
                                {
                                    xtype: 'button',
                                    flex: 1,
                                    cls: 'oxDialogButton first',
                                    text: Locale.getMsg('barcodeApplyManualInput'),
                                    listeners: {
                                        click: 'onUseManualInputSelected'
                                    }
                                }
                        	]
                        },
		                {
		                    xtype: 'component',
		                    height: 15
		                },
		                {
		                    xtype: 'container',
		                    cls: 'oxDialogButtonBar',
		                    layout: {
		                        type: 'hbox'
		                    },
		                    items: [
		                        this._retryButton,
		                        this._cancelButton
		                    ]
		                }
		            ]
		        },
		        {
		            xtype: 'component',
		            width: 10
		        }
		    ];
		   
	    	this.add(items);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseBarcodeRetryDialog', ex);
		}
    },

    //public
    getManualInput: function () {
        var retval = '';

        try {
            retval = this._manualInputTextField.getValue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getManualInput of BaseBarcodeRetryDialog', ex);
        }

        return retval;
    },

    //public
    resetManualInput: function() {
        try {
            this._manualInputTextField.setValue('');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetManualInput of BaseBarcodeRetryDialog', ex);
        }
    }
});