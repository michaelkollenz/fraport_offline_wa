Ext.define('AssetManagement.base.modules.barcode.BaseBarcodeScanDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	  'AssetManagement.customer.modules.barcode.BarcodeManager',
	  'AssetManagement.customer.utils.StringUtils',
	  'AssetManagement.customer.helper.OxLogger'
	],
	
	_resultCallback: null,
	_resultCallbackScope: null,
	_errorMessage: '',
	
	//public
	//@override
	//redefines the ox dialog request api, since this dialog does not load any data
	requestDialog: function(argumentsObject, dialogRequestCallback, dialogRequestCallbackScope, onHiddenCallback) {
		try {
			this._dialogRequestCallback = dialogRequestCallback;
			this._dialogRequestCallbackScope = dialogRequestCallbackScope;
			this._currentArguments = argumentsObject;
			this._onHiddenCallback = onHiddenCallback;
			this._errorMessage = '';
			
			this.prepareDialog(argumentsObject);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestDialog of BaseBarcodeScanDialogController', ex);
			this.errorOccurred();
		}
	},
	
	//@override
	//public
	update: function(argumentsObject) {
	},
	
	//public
	//@override
	cancel: function() {
		try {
			//stop scanning, before dismissing the dialog
			AssetManagement.customer.modules.barcode.BarcodeManager.getInstance().cancelScanning();
			
			this.callParent();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseBarcodeScanDialogController', ex);
		}
	},

    //public
	onUseManualInputSelected: function () {
	    try {
	        var manualInput = this.getView().getManualInput();

	        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(manualInput)) {
	            this.stopScanning();
	            this.onScanResult(manualInput);
	        } else {
	            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('youHaveToProvideASerialNumber'), false, 2);
	            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onUseManualInputSelected of BarcodeRetryDialogController', ex);
	    }
	},
	
	//public
	onFileSelected: function(file) {
		try {
			if(file) {
				var mimeType = AssetManagement.customer.helper.FileHelper.getMimeTypeOfFile(file);
				
				//check if the chosen file is of type image
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mimeType) && mimeType.toLowerCase().indexOf('image') > -1) {
					//stop scanning to evaluate the selected file
					this.dismiss();
					this.stopScanning();
				
					AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
					
					var innerCallback = function(result) {
						try {
							if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(result)) {
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								this.onScanResult.call(this, result);
							} else {
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							
								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noBarcodeDetected'), false, 2);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);

								this.askForRetryWithAnotherSourceOrManualInput();
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSourceSelected of BaseBarcodeScanDialogController', ex);
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					};
					
					//convert the file data into base64 representation
					var base64 = AssetManagement.customer.helper.FileHelper.getBase64StringForFile(file);
				
					if(base64) {
						AssetManagement.customer.modules.barcode.BarcodeManager.getInstance().processImage(base64, innerCallback, this);
					} else {
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						this.callScanCallback(null);
					}
				} else {
					//user did not select an image file, stop processing (else barcode reader would run into an infinite loop)
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('chosenFiletypeNotSupported'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirm of BaseBarcodeScanDialogController', ex);
		}
	},
	
	//public
	//maps initialization error against the standard ox dialog api
	getDataLoadingErrorMessage: function() {
		var retval = '';
		
		try {
			retval = this.getInitializationErrorMessage();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataLoadingErrorMessage of BaseBarcodeScanDialogController', ex);
		}

		return retval;
	},
	
	//private
	prepareDialog: function(argumentsObject) {
		try {
			if(argumentsObject) {
				this._resultCallback = argumentsObject.resultCallback;
				this._resultCallbackScope = argumentsObject.resultCallbackScope;

				this.getView().resetManualInput();
	
				AssetManagement.customer.modules.barcode.BarcodeManager.getInstance().startScanning(this.onScanResult, this.onError, this.onVideoElementReady, this);
			} else {
				this.errorOccured();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareDialog of BaseBarcodeScanDialogController', ex);
			this.errorOccured();
		}
	},

    //private
    //will trigger a non success dialog asking, if the user wants to try with another source or wants to enter input manually
	askForRetryWithAnotherSourceOrManualInput: function() {
	    try {
	        var dialogArgs = {
	            sourceCallback: this.onFileSelected,
	            manualInputCallback: this.onScanResult,
	            scope: this
	        };

	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.BARCODE_RETRY_DIALOG, dialogArgs);
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside askForRetryWithAnotherSourceOrManualInput of BaseBarcodeScanDialogController', ex);
	    }
	},
	
	//private
	stopScanning: function() {
		try {
			AssetManagement.customer.modules.barcode.BarcodeManager.getInstance().cancelScanning();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stopScanning of BaseBarcodeScanDialogController', ex);
		}
	},
	
	//private
	onScanResult: function(result, errorMessage) {
		try {
			if(result) {
				this.dismiss();
				this._resultCallback.call(this._resultCallbackScope ? this._resultCallbackScope : window, result);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onScanResult of BaseBarcodeScanDialogController', ex);
		}
	},
	
	//private
	onError: function(errorMessage) {
		try {
			this._errorMessage = errorMessage;
			
			this.errorOccurred();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onVideoElementReady of BaseBarcodeScanDialogController', ex);
		}
	},
	
	//private
	onVideoElementReady: function(videoElement) {
		try {
			if(videoElement) {
				this.getView().setVideoElement(videoElement);
				
				this.dialogReady();
			} else {
				this.onError();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onVideoElementReady of BaseBarcodeScanDialogController', ex);
		}
	},
	
	//private
	getInitializationErrorMessage: function() {
		var retval = '';
				
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._errorMessage))
				retval = this._errorMessage;
			else
				retval = Locale.getMsg('barcodeReaderCouldNotBeInitialized');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInitializationErrorMessage of BaseBarcodeScanDialogController', ex);
		}
		
		return retval;
	}
});