Ext.define('AssetManagement.base.modules.barcode.BaseBarcodeRetryDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	  'AssetManagement.customer.helper.OxLogger'
	],
	
	_sourceCallback: null,
	_manualInputCallback: null,
	_callbackScope: null,

	//@override
	//protected
	transferDataFromConfigObjectToViewModel: function(argumentsObject) {
		try {
		    if(argumentsObject) {
		        this._sourceCallback = argumentsObject.sourceCallback;
			    this._manualInputCallback = argumentsObject.manualInputCallback;
				this._callbackScope = argumentsObject.scope;
			}

			this.getView().resetManualInput();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferDataFromConfigObjectToViewModel of BaseBarcodeRetryDialogController', ex);
		}
	},
	
	//@override
	//public
	update: function(argumentsObject) {
	},
	
	//public
	onRetryFileSelected: function(file) {
		try {
			this.dismiss();
		
			if (this._sourceCallback) {
			    this._sourceCallback.call(this._callbackScope ? this._callbackScope : window, file);
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onRetryFileSelected of BaseBarcodeRetryDialogController', ex);
		}
	},

    //public
	onUseManualInputSelected: function() {
	    try {
	        var manualInput = this.getView().getManualInput();

	        if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(manualInput)) {
	            this.dismiss();

	            if (this._manualInputCallback) {
	                this._manualInputCallback.call(this._callbackScope ? this._callbackScope : window, manualInput);
	            }
	        } else {
	            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('youHaveToProvideASerialNumber'), false, 2);
	            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onUseManualInputSelected of BaseBarcodeRetryDialogController', ex);
	    }
	},

	//public
	//@override
	cancel: function() {
		this.callParent();
	
		try {
			if(this._callback) {
				this._callback.call(this._callbackScope ? this._callbackScope : window, null);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseBarcodeRetryDialogController', ex);
		}
	}
});