﻿Ext.define('AssetManagement.base.modules.reportFramework.view.dialog.BaseBuildReportDialog', {
    extend: 'AssetManagement.customer.view.dialogs.CancelableProgressDialog',


    requires: [
        'AssetManagement.customer.modules.reportFramework.controller.dialog.BuildReportDialogController',
        'AssetManagement.customer.modules.reportFramework.model.BuildReportDialogViewModel',
        'AssetManagement.customer.helper.OxLogger'
    ],

    viewModel: {
        type: 'BuildReportDialogViewModel'
    },

    controller: 'BuildReportDialogController',

    inheritableStatics: {
        _instance: null,

        getInstance: function (arguments) {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.view.dialog.BuildReportDialog', arguments);
                }

                this._instance.setInitialWidth(arguments ? arguments.width : undefined);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseBuildReportDialog', ex);
            }

            return this._instance;
        }
    }
});