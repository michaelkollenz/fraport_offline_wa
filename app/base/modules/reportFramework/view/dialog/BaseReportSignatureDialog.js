Ext.define('AssetManagement.base.modules.reportFramework.view.dialog.BaseReportSignatureDialog', {
  extend: 'AssetManagement.customer.view.dialogs.OxDialog',


  requires: [
    'AssetManagement.customer.modules.reportFramework.controller.dialog.ReportSignatureDialogController',
    'AssetManagement.customer.modules.reportFramework.model.ReportSignatureDialogViewModel',
    'Ext.container.Container',
    'AssetManagement.customer.modules.paint.OxPaintingComponent',
    'Ext.button.Button',
    'Ext.form.Label',
    'Ext.form.TextField',
    'AssetManagement.customer.view.OxGridPanel'
  ],

  config: {
    supportsDynamicWidth: true,
    supportsDynamicHeight: true,
    minWidth: 270,
    maxWidth: 500,
    minHeight: 280,
    maxHeight: 370,
    modal: true,
    header: false,
    centered: true,
    floating: true,

    layout: {
      type: 'vbox',
      align: 'stretch'
    },

    cls: 'oxDialog'
  },


  inheritableStatics: {
    _instance: null,

    getInstance: function () {
      try {
        if (this._instance == null) {
          this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.view.dialog.ReportSignatureDialog');
        }
      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseReportSignatureDialog', ex);
      }

      return this._instance;
    }
  },

  viewModel: {
    type: 'ReportSignatureDialogViewModel'
  },

  controller: 'ReportSignatureDialogController',

  buildUserInterface: function () {
    try {
      var myController = this.getController();
      var items = [
        {
          xtype: 'component',
          minWidth: 10,
          maxWidth: 10
        },
        {
          xtype: 'container',
          //id:'signatureDialogMainContainer',
          flex: 1,
          layout: {
            type: 'vbox',
            align: 'stretch'
          },
          items: []
        }
      ];

      this.add(items);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseReportSignatureDialog', ex);
    }
  },

  //protected
  //@override
  getDialogTitle: function (signatureDetails) {
    var retval = '';

    try {
      if (signatureDetails) {
        if (signatureDetails.sign1Required) {
          retval = Locale.getMsg('customerSignature');
        } else if (signatureDetails.sign2Required) {
          retval = Locale.getMsg('technicianSignature');
        } else if (signatureDetails.sign3Required) {
          retval = Locale.getMsg('engineerSignature');
        } else if (signatureDetails.sign4Required) {
          retval = '';
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseReportSignatureDialog', ex);
    }

    return retval;
  },


  //protected
  //@override
  updateDialogContent: function () {
    try {
      this.buildSignatureDialog();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateDialogContent of BaseReportSignatureDialog', ex);
    }
  },


  buildSignatureDialog: function () {
    try {

      var myModel = this.getViewModel();
      var myController = this.getController();
      var isMailModeActive = myModel.get('isMailModeActive');

      var panel = this.down('container');
      panel.removeAll();

      var signatureDetails = myController.getSignatureDetails();

      var reportId = myController.getReportId();

      //create title
      var dialogTitleText = reportId ? Locale.getMsg('report') + ': ' + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(reportId) + '\n' + this.getDialogTitle(signatureDetails) : this.getDialogTitle(signatureDetails);
      var dialogTitle = Ext.create('Ext.form.Label', {
        cls: 'oxDialogTitle',
        text: dialogTitleText,
        width: 340
      });
      panel.add(dialogTitle);

      // if(!isMailModeActive){
      /////we are in signature mode
      var signatureSection = Ext.create('AssetManagement.customer.modules.paint.OxPaintingComponent', {
        itemId: 'reportsSignatureDialog',
        flex: 1
      });

      var signaturePlainText = Ext.create('Ext.form.TextField', {
        itemId: 'signaturePlainTextX',
        width: 340,
        cls: 'signatureDialogContactPersonLabel'
      });

      var signatureClearButtonSection = Ext.create('Ext.container.Container', {
        //width: 340,
        height: 45,
        layout: {
          type: 'vbox',
          align: 'stretch'
        },
        items: [
          {
            xtype: 'button',
            cls: 'clearSignatureDialogButton',
            handler: function () {
              myController.onRemoveSignature();
            },
            text: Locale.getMsg('clearSignature')
          }
        ]
      });

      var ifPlainTextIsNeededAndWhere = this.onPlainTextSignatureIsNeeded(signatureDetails);
      if (ifPlainTextIsNeededAndWhere.show === true) {
        if (ifPlainTextIsNeededAndWhere.top === true) {
          panel.add(signaturePlainText)
          panel.add(signatureSection);
          panel.add(signatureClearButtonSection);
        } else {
          panel.add(signatureSection);
          panel.add(signatureClearButtonSection);
          panel.add(signaturePlainText)
        }
      } else {
        panel.add(signatureSection);
        panel.add(signatureClearButtonSection);
      }

      // } else{
      // ////we are providing possibility to add e-mail addresses
      //  var verticalScrollableContainer = Ext.create('Ext.container.Container',{
      //     flex: 1,
      //     scrollable: 'vertical',
      //     layout: {
      //         type: 'vbox',
      //         align: 'stretch'
      //     },
      //     items:[]
      // });
      //
      // var emailSection = Ext.create('AssetManagement.customer.view.OxGridPanel', {
      //     itemId: 'emailAddressesPanel',
      //     margin: '10 0 10 0',
      //     hideHeaders: true,
      //     hideContextMenuColumn: true,
      //     hidden: false,
      //     width: 340,
      //     columns: [
      //         {
      //             xtype: 'gridcolumn',
      //             flex: 4,
      //             dataIndex: 'email',
      //             enableColumnHide: false
      //         },
      //         {
      //             xtype: 'gridcolumn',
      //             flex: 1,
      //             align: 'right',
      //             enableColumnHide: false,
      //             renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
      //                 return '<img src="resources/icons/delete.png"';
      //             }
      //         }
      //     ],
      //     listeners: {
      //         select : myController.deleteEmailAddress,
      //         scope: myController
      //     }
      // });
      //
      // emailSection.setStore(myController._emailAddressesStore);
      //
      // var emailAdditionSection = Ext.create('Ext.panel.Panel', {
      //     minWidth: 340,
      //     maxWidth: 490,
      //     layout: {
      //         type: 'hbox',
      //         align: 'stretch',
      //         vertical: false
      //     },
      //     items: [
      //         {
      //             xtype: 'label',
      //             text: Locale.getMsg('email'),
      //             cls: 'signatureDialogEmailLabel',
      //             margin: '13 10 0 0'
      //         },
      //         {
      //             //input
      //             xtype: 'textfield',
      //             flex:1,
      //             itemId: 'signatureDialogEmailTextField',
      //             margin : '10 0 10 0'
      //         },
      //         {
      //             //////icon
      //             xtype: 'button',
      //             iconCls: 'signatureDialogAddButton',
      //             width: 45,
      //             height: 45,
      //             handler: function () {
      //                 myController.addEmailAddress();
      //             }
      //         }
      //     ]
      // });
      //
      // verticalScrollableContainer.add(emailSection);
      // verticalScrollableContainer.add(emailAdditionSection);
      // panel.add(verticalScrollableContainer);

      // }


      var nextButton = Ext.create('Ext.button.Button', {
        flex: 1,
        height: '32px',
        cls: 'oxDialogButton first',
        text: Locale.getMsg('dialogContinue'),
        handler: this.getController().onNextButtonClicked,
        scope: myController
      });

      var okButton = Ext.create('Ext.button.Button', {
        flex: 1,
        height: '32px',
        cls: 'oxDialogButton first',
        text: Locale.getMsg('ok'),
        handler: this.getController().onOkButtonClicked,
        scope: myController
      });

      var cancelButton = Ext.create('Ext.button.Button', {
        flex: 1,
        height: '32px',
        cls: 'oxDialogButton',
        text: Locale.getMsg('cancel'),
        handler: this.getController().onCancelButtonClicked,
        scope: myController
      });

      buttonBar = Ext.create('Ext.container.Container', {
        layout: {
          type: 'hbox'
        },
        cls: 'oxDialogButtonBar',
        style: 'margin-top: 10px'
      });

      // if(!isMailModeActive){
      //     buttonBar.add(nextButton);
      // } else{
      buttonBar.add(okButton);
      // }
      buttonBar.add(cancelButton);

      panel.add(buttonBar);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSignatureDialog of BaseReportSignatureDialog', ex);
    }
  },

  onPlainTextSignatureIsNeeded: function (signatureDetails) {
    var retval = {
      'show': false,
      'top': null
    }
    try {
      if (signatureDetails) {
        if (signatureDetails.sign1Required) {
          retval = {
            'show': signatureDetails.sign1Required[1] === 'X' ? true : false,
            'top': signatureDetails.sign1Required[3] === 'BELLOW' ? false : true
          }
        } else if (signatureDetails.sign2Required) {
          retval = {
            'show': signatureDetails.sign2Required[1] === 'X' ? true : false,
            'top': signatureDetails.sign2Required[3] === 'BELLOW' ? false : true
          }
        } else if (signatureDetails.sign3Required) {
          retval = {
            'show': signatureDetails.sign3Required[1] === 'X' ? true : false,
            'top': signatureDetails.sign3Required[3] === 'BELLOW' ? false : true
          }
        } else if (signatureDetails.sign4Required) {
          retval = {
            'show': signatureDetails.sign4Required[1] === 'X' ? true : false,
            'top': signatureDetails.sign4Required[3] === 'BELLOW' ? false : true
          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPlainTextSignatureIsNeeded of BaseReportSignatureDialog', ex);
    }

    return retval;
  }
});
