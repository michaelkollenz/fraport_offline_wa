﻿Ext.define('AssetManagement.base.modules.reportFramework.view.dialog.BaseReportCostsDialog', {
    extend: 'AssetManagement.customer.view.dialogs.OxDialog',


    requires: [
        'AssetManagement.customer.modules.reportFramework.controller.dialog.ReportCostsDialogController',
        'AssetManagement.customer.modules.reportFramework.model.ReportCostsDialogViewModel',
        'Ext.container.Container',
        'Ext.button.Button',
        'Ext.form.Label',
        'Ext.form.TextField',
        'AssetManagement.customer.view.OxGridPanel'
    ],

    config: {
        minWidth: 300,
        maxWidth: 500,
        modal: true,
        header: false,
        centered: true,
        floating: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        cls: 'oxDialog'
    },

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance == null) {
                    this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.view.dialog.ReportCostsDialog');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseReportCostsDialog', ex);
            }

            return this._instance;
        }
    },

    viewModel: {
        type: 'ReportCostsDialogViewModel'
    },

    controller: 'ReportCostsDialogController',


    buildUserInterface: function () {
        try {

            var myController = this.getController();

            var items = [
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        },
		        {
		            xtype: 'container',
		            flex: 1,
		            layout: {
		                type: 'vbox',
		                align: 'stretch'
		            },
		            items: [
		                {
		                    xtype: 'label',
		                    cls: 'oxDialogTitle',
		                    text: this.getDialogTitle()
		                },
                        {
                            xtype: 'container',
                            id: 'reportTypeContainer',
                            width: 340,
                            height: 'auto',
                            items: [
                                {
                                    xtype: 'panel',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'radiogroup',
                                            items: [
                                                {
                                                    xtype: 'radiofield',
                                                    id: 'singleEntryReport',
                                                    boxLabel: Locale.getMsg('singleEntryReport'),
                                                    name: 'framework',
                                                    checked: true,
                                                    inputValue: 'singleEntry'
                                                },
                                                {
                                                    xtype: 'radiofield',
                                                    id: 'summaryReport',
                                                    boxLabel: Locale.getMsg('summaryReport'),
                                                    name: 'framework',
                                                    checked: false,
                                                    inputValue: 'summary'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'oxgridpanel',
                                            id: 'reportCostsPanel',
                                            margin: '10 0 10 0',
                                            hideHeaders: true,
                                            hideContextMenuColumn: true,
                                            columns: [
                                                {
                                                    xtype: 'gridcolumn',
                                                    flex: 1,
                                                    dataIndex: 'id',
                                                    hidden: true,
                                                    enableColumnHide: true
                                                },
                                                {
                                                    xtype: 'gridcolumn',
                                                    flex: 1,
                                                    dataIndex: 'reportTypeDescription',
                                                    enableColumnHide: false
                                                }
                                            ],
                                            listeners: {
                                                select: myController.onReportTypeSelection,
                                                scope: myController
                                            }

                                        }

                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'button',
                            cls: 'doneSignatureDialogButton',
                            text: Locale.getMsg('cancel'),
                            listeners: {
                                click: myController.onCancelPrintReports,
                                scope: myController
                            }
                        }

		            ]
		        },
		        {
		            xtype: 'component',
		            minWidth: 10,
		            maxWidth: 10
		        }
            ];

            this.add(items);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of BaseReportCostsDialog ', ex);
        }
    },

    //protected
    //@override
    getDialogTitle: function (typeOfSignature) {
        var retval = '';
        try {
            retval = Locale.getMsg('reportType');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDialogTitle of BaseReportCostsDialog ', ex);
        }

        return retval;
    }
});