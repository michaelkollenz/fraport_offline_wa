Ext.define('AssetManagement.base.modules.reportFramework.view.page.BaseReportsPreviewPage', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.modules.reportFramework.model.ReportsPreviewPageViewModel',
        'AssetManagement.customer.modules.reportFramework.controller.page.ReportsPreviewPageController',
        'AssetManagement.customer.modules.reportFramework.ReportFramework',
        'Ext.ux.IFrame',
        'Ext.Img',
        'AssetManagement.customer.modules.reportFramework.ReportPdfMaker',
        'AssetManagement.customer.view.utils.OxComboBox'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.view.page.ReportsPreviewPage');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseReportsPreviewPage', ex);
            }

            return this._instance;
        }
    },
    
    config: {
		cls: 'oxPage',
		bodyCls: 'pageStandardBody',
		header: false,
		scrollable: false
	},

    viewModel: {
        type: 'ReportsPreviewPageModel'
    },

    controller: 'ReportsPreviewPageController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    _languageMappingObject : {
        'E' : 'english',
        'D' : 'german',
        'L' : 'polish',
        '1' : 'chinese'
    },

    _copyOfDataToShow : null,

    buildUserInterface: function () {
        var myController = this.getController();
        var myModel = this.getViewModel();


        //var items = [
        var items = [
          {
                xtype: 'container',
                flex: 1,
                scrollable: false,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                {
                  xtype: 'container',
                  id : 'languageComboBoxSelection',
                  margin: '10 0 10 0',
                  items: [
                      {
                            xtype: 'oxcombobox',
                            id: 'docLanguageComboBox',
                            fieldLabel: Locale.getMsg('reportLanguageDialogTitle'),
                            displayField: 'reportTypeDescription',
                            valueField: 'id',
                            queryMode: 'local',
                            width: 400,
                            maxHeight: 30,
                            editable: false,
                            labelWidth: 200,
                            value: Locale.getMsg('optionalParameter'),
                            listeners: {
                                select: myController.onDocumentLanguageSelect,
                                scope: myController
                            }
                      }
                  ]},
                {
                    xtype: 'container',
                    id: 'verticalReportsPreview',
                    flex: 1,
                    scrollable: false
                },
                {
                    xtype: 'tabpanel',
                    id: 'tabReportsPreviewPanel',
                    style: 'border: 1px solid #808080',
                    flex: 1,
                    tabBar: {
                        defaults: {
                            flex: 1
                        }
                    },
                    activeTab: 0,

                    items: [
                    ]
                }]
            }
        ];

        this.add(items);
    },

    //protected
    //@override
    
    updatePageContent: function () {
        try {
            var myController = this.getController();

            if (AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser(true).toLowerCase() === 'safari'){
                this.renewComboboxContainer();
            }

            this.manageLanguageComboboxSection();
            this.manageReportsDisplaySection();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseReportsPreviewPage', ex);
        }
    },

    manageLanguageComboboxSection: function() {
        try {
            var myController = this.getController();
            var backUpData = myController._backUpOfTheData;
            if(backUpData.length === 0){
                backUpData = myController.getDataToShow();
            }

            var lengthOfDataToShow = backUpData.length;
            var langCombobox = Ext.getCmp('docLanguageComboBox');
            var langComboSection = Ext.getCmp('languageComboBoxSelection');
            langComboSection.setVisible(false);
            // if(lengthOfDataToShow > 1){
            //     ///go for parameter of report framework
            //     if(AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance()._preventLangChange) {
            //         langComboSection.setVisible(false);
            //         //langCombobox.setVisible(false);
            //     } else {
            //         langComboSection.setVisible(true);
            //         //langCombobox.setVisible(true);
            //     }
            // } else {
            //     var availableLangs = backUpData[0]['object']['mainObject'].get('layoutType').get('docData').get('docLang') ? backUpData[0]['object']['mainObject'].get('layoutType').get('docData').get('docLang') : null;
            //     if(availableLangs && availableLangs.getCount() > 1){
            //         var languagesStore = this.createLanguageStore(availableLangs);
            //         //langCombobox.setVisible(true);
            //         langComboSection.setVisible(true);
            //         langCombobox.setStore(languagesStore);
            //         langCombobox.select(Locale.getCurrentLanguage());

            //     } else {
            //         //langCombobox.setVisible(false);
            //         langComboSection.setVisible(false);
            //     }
            // }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLanguageComboboxSection of BaseReportsPreviewPage', ex);
        }
    },

    createLanguageStore : function(availableLangs){
        try {
            var me = this;
            var langsStore =  Ext.create('Ext.data.Store', {
                storeId: 'reportLangStore',
                fields: ['id','reportTypeDescription'],
                autoLoad: false
            });

            availableLangs.each(function(language){
                switch(language.get('lang')){
                    case 'E':
                        langsStore.add({
                            'id': 'en_US',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['E'])
                        })
                        break;
                    case 'D':
                        langsStore.add({
                            'id': 'de_DE',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['D'])
                        })
                        break;
                    case 'L':
                        langsStore.add({
                            'id': 'pl_PL',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['L'])
                        })
                        break;
                    case '1':
                        langsStore.add({
                            'id': 'zh_zh',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['1'])
                        })
                        break;
                }
            });
            
            return langsStore;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createLanguageStore of BaseReportsPreviewPage', ex);
        }
    },

    manageReportsDisplaySection : function() {
        try {
            var reportFramework = AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance();
            if(reportFramework._isVerticalView){
                this.showVerticalLayout();
            } else {
                this.showTabLayout();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageReportsDisplaySection of BaseReportsPreviewPage', ex);
        }
    },

    renewComboboxContainer: function() {
        try {
            var myController = this.getController();

            var languageComboBoxSelection = Ext.getCmp('languageComboBoxSelection');           
            languageComboBoxSelection.removeAll();
            languageComboBoxSelection.add({
                xtype: 'oxcombobox',
                id: 'docLanguageComboBox',
                fieldLabel: Locale.getMsg('reportLanguageDialogTitle'),
                displayField: 'reportTypeDescription',
                valueField: 'id',
                queryMode: 'local',
                width: 400,
                hidden: true,
                maxHeight: 30,
                editable: false,
                labelWidth: 200,
                value: Locale.getMsg('optionalParameter'),
                listeners: {
                    select: myController.onDocumentLanguageSelect,
                    scope: myController
                }
            });                   
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewComboboxContainer of BaseReportsPreviewPage', ex);
        }
    },

    showVerticalLayout: function(){
        Ext.getCmp('tabReportsPreviewPanel').setVisible(false);
        this.buildVerticaInterface();
        Ext.getCmp('verticalReportsPreview').setVisible(true);
    },

    showTabLayout : function(){
        Ext.getCmp('tabReportsPreviewPanel').setVisible(true);
        this.buildTabsUserInterface();
        Ext.getCmp('verticalReportsPreview').setVisible(false);
    },

    //protected
    //@override
    getPageTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('reportsPreview');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of MaterialListPage', ex);
        }

        return retval;
    },

    _makeId: function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < 18; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    buildTabsUserInterface: function () {

        try{
            var retval = [];

            var myController = this.getController();
            var reportsToShow = myController.getDataToShow();
            var documentSignatures = myController.getRequiredSignatures();

            if(myController._backUpOfTheData.length === 0){
                for(var j = 0; j < reportsToShow.length; j++){
                    myController._backUpOfTheData.push({
                        object : $.extend(false, {}, reportsToShow[j]['object']),
                        pdf : $.extend(false, {}, reportsToShow[j]['pdf'])
                    });
                }
            }

            reportsToShow = myController._backUpOfTheData;

            var me = this;

            var tabsContainer = Ext.getCmp('tabReportsPreviewPanel');

            for (idx in reportsToShow) {
                var randomId = this._makeId();
                var title = reportsToShow[idx]['object']['mainObject'].get('id');

                retval.push(
	                Ext.create('Ext.Panel',
                        {
                            hideContextMenuColumn: true,
                            title: title,
                            margin: '0 0 0 0',
                            flex: 1,
                            icon: myController.decideIfSignatureNeeded,
                            items: [
                                {
                                    xtype: 'uxiframe',
                                    width: '100%',
                                    height: 840,
                                    id: randomId
                                }
                            ]

                        }
                    )
	            );

            }

            var lastActiveElement = this.getIndexOfActiveTab(tabsContainer);
            tabsContainer.removeAll();

            var counter = 0;

            var countOfProperties = Object.keys(reportsToShow).length;


            var successCallback = function (counter) {
                try{
                    counter++;
                    if (counter < countOfProperties) {
                        fillIFrame(counter);
                    } else {
                        tabsContainer.add(retval);
                        if (lastActiveElement) {
                            tabsContainer.setActiveItem(lastActiveElement);
                        } else {
                            tabsContainer.setActiveTab(0);
                        }
                        return;
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTabsUserInterface-successCallback of BaseReportsPreviewPage', ex);
                }
            }

            var fillIFrame = function (counter) {
                try {

                    var reportToShow = $.extend(true, {}, reportsToShow[counter].pdf);

                    if (reportsToShow[counter].signatures ){ //&& !reportsToShow[counter].signedByAll) {
                        reportToShow.docDefinition.content.push('\n\n\n\n');
                        reportToShow.docDefinition.content.push(myController.prepareSignatureSection(reportsToShow[counter].signatures), myController._reportLanguage);

                        reportsToShow[counter].signedByAll = true;
                    }
                    reportToShow.getDataUrl(function (outDoc) {

                        retval[counter].items.items[0].src = outDoc;
                        successCallback(counter);
                    });

                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTabsUserInterface-fillIFrame of BaseReportsPreviewPage', ex);
                }
            }
            fillIFrame(counter);
           

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTabsUserInterface of BaseReportsPreviewPage', ex);
        }
    },

    getIndexOfActiveTab : function(tabsContainer){
        var panelIndex = null;
        var selectedItem = tabsContainer.getActiveTab();
        var items = tabsContainer.items.items;
        if (selectedItem) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].title === selectedItem.title) {
                    panelIndex = i;
                }
            }
        }
        return panelIndex;
    },

    buildVerticaInterface: function () {

        try{
            var me = this;
            var myController = this.getController();

            var dataToShow = this.getController().getDataToShow();
            

            var superReport = {};
            var copyOfOriginalDataArray = [];

            if(myController._backUpOfTheData.length === 0){
                copyOfOriginalDataArray = $.extend(true, [], dataToShow);
                
            } else{
                AssetManagement.customer.modules.reportFramework.ReportPdfMaker.getInstance().prepareAlreadySignedDocuments(myController._backUpOfTheData, Locale);
                copyOfOriginalDataArray = $.extend(true, [], myController._backUpOfTheData);
            }

            var verticalContainer = Ext.getCmp('verticalReportsPreview');
            verticalContainer.removeAll();

            for (var i = 0; i < copyOfOriginalDataArray.length; i++) {
                if ($.isEmptyObject(superReport)) {
                    // add first report
                    superReport = jQuery.extend(true, {}, copyOfOriginalDataArray[0].pdf);
                } else {
                    //workaround to make library to merge multiple docs into just one
                    superReport.docDefinition.content.push({
                        text: '',
                        pageBreak: 'after'
                    });
                    contentOfReport = copyOfOriginalDataArray[i].pdf.docDefinition.content;

                    for (var j = 0; j < contentOfReport.length; j++) {
                        superReport.docDefinition.content.push(contentOfReport[j]);
                    }
                }

                if (copyOfOriginalDataArray[i].signatures && copyOfOriginalDataArray[i].signatures.length > 0) {
                    superReport.docDefinition.content.push('\n\n\n');
                    var signatureSection = myController.prepareSignatureSection(copyOfOriginalDataArray[i].signatures);
                    superReport.docDefinition.content.push(signatureSection);
                }
            }

            var userAgentInfo = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();
            var isSafari = userAgentInfo.OS === 'iOS';


             var fillIFrame = function (outDoc) {
                 
                 var retval = {
                    xtype: 'uxiframe',
                    layout: 'fit',
                    height: '100%',
                    width: '100%',
                    src: outDoc/*,
                    listeners: {
                        afterrender: function () {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        }
                    }*/
                }
                
                 if (isSafari) {
                     retval.cls = 'iframe-cont';
                 }

                 verticalContainer.add(retval);
            }

            if(myController._backUpOfTheData.length === 0){
                for(var j = 0; j < dataToShow.length; j++){
                    myController._backUpOfTheData.push({
                        object : $.extend(false, {}, dataToShow[j]['object']),
                        pdf : $.extend(false, {}, dataToShow[j]['pdf'])
                    });
                }
                //this.getController()._backUpOfTheData = $.extend(true, [], dataToShow);
            }
            
            if (!superReport) {
                //reportError
            } else {
                superReport.getDataUrl(function (outDoc) {
                    fillIFrame(outDoc);
                });
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildVerticaInterface of BaseReportsPreviewPage', ex);
        }


    }
});