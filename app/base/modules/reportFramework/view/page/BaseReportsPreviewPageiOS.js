Ext.define('AssetManagement.base.modules.reportFramework.view.page.BaseReportsPreviewPageiOS', {
    extend: 'AssetManagement.customer.view.pages.OxPage',


    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.modules.reportFramework.model.ReportsPreviewPageiOSViewModel',
        'AssetManagement.customer.modules.reportFramework.controller.page.ReportsPreviewPageiOSController',
        'Ext.Img'
    ],

    inheritableStatics: {
        _instance: null,

        getInstance: function () {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.view.page.ReportsPreviewPageiOS');
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseReportsPreviewPageiOS', ex);
            }

            return this._instance;
        }
    },

    config: {
		cls: 'oxPage',
		bodyCls: 'pageStandardBody',
		header: false,
		scrollable: false
	},

    viewModel: {
        type: 'ReportsPreviewPageiOSModel'
    },

    controller: 'ReportsPreviewPageiOSController',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    _languageMappingObject : {
        'E' : 'english',
        'D' : 'german',
        'L' : 'polish',
        '1' : 'chinese'
    },

    _copyOfDataToShow : null,

    buildUserInterface: function () {
        var myController = this.getController();
        var myModel = this.getViewModel();

        //var items = [
        var items = [
            {
                xtype: 'container',
                flex: 1,
                scrollable: false,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                {
                    xtype: 'container',
                    id: 'verticalReportsPreviewiOSContainer',
                    flex: 1,
                    scrollable: false
                },
                {
                    xtype: 'tabpanel',
                    id: 'tabReportsPreviewPaneliOSContainer',
                    cls: 'tabReportsContent',
                    style: 'border: 1px solid #808080',
                    flex: 1,
                    tabBar: {
                        defaults: {
                            flex: 1
                        }
                    },
                    activeTab: 0,
                    items: [
                    ]

                }]

            }];

        this.add(items);
    },

    //public
    //@override
    getPageTitle: function () {
        var retval = '';

        try {
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPageTitle of BaseReportsPreviewPageiOS', ex);
        }

        return retval;
    },

    showVerticalLayout: function(){
        Ext.getCmp('tabReportsPreviewPaneliOSContainer').setVisible(false);
        Ext.getCmp('verticalReportsPreviewiOSContainer').setVisible(true);
    },

    showTabLayout : function(){
        Ext.getCmp('tabReportsPreviewPaneliOSContainer').setVisible(true);
        Ext.getCmp('verticalReportsPreviewiOSContainer').setVisible(false);
    },

    //protected
    //@override
    updatePageContent: function () {
        try {

            var myController = this.getController();
            var items = myController._dataToShow;

            if (items) {
                var tabLayoutType = items[0]['object']['mainObject'].get('layoutType').get('docData').get('displaytabs');

                if (tabLayoutType && tabLayoutType === "X") {
                    this.showTabLayout();
                    this.buildTabsUserInterface();
                } else{
                    this.showVerticalLayout();
                    this.buildVerticaInterface();
                }

                var viewReportsLabel = Ext.getCmp('viewReportLabel');
                var acceptedReportLabel = Ext.getCmp('acceptedReportLabel');

                if(viewReportsLabel && acceptedReportLabel){
                    if(items.length > 1) {
                        viewReportsLabel.setText(Locale.getMsg('mymViewReports'));
                        acceptedReportLabel.setText(Locale.getMsg('mymReportsAccepted'));
                    } else {
                        viewReportsLabel.setText(Locale.getMsg('mymViewReport'));
                        acceptedReportLabel.setText(Locale.getMsg('mymReportAccepted'));
                    }
                }
            }

            var isSaveMode = this.getViewModel().get('isSaveMode');
            var checkbox = Ext.getCmp('acceptReportsCheckboxIOS');

            if(isSaveMode){
                if(checkbox){
                    checkbox.setValue(true);
                    checkbox.disable();
                }
            } else {
                if(checkbox) {
                    checkbox.enable();
                }
            }


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of BaseReportsPreviewPageiOS', ex);
        }
    },


     manageLanguageComboboxSection: function() {
        try {
            var myController = this.getController();
            var backUpData = myController._backUpOfTheData;
            if(backUpData.length === 0){
                backUpData = myController.getDataToShow();
            }

            var lengthOfDataToShow = backUpData.length;
            var langCombobox = Ext.getCmp('docLanguageComboBox');
            if(lengthOfDataToShow > 1){
                ///go for parameter of report framework
                if(AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance()._preventLangChange) {
                    langCombobox.setVisible(false);
                } else {
                    langCombobox.setVisible(true);
                }
            } else {
                var availableLangs = backUpData[0]['object']['mainObject'].get('layoutType').get('docData').get('docLang') ? backUpData[0]['object']['mainObject'].get('layoutType').get('docData').get('docLang') : null;
                if(availableLangs && availableLangs.getCount() > 1){
                    var languagesStore = this.createLanguageStore(availableLangs);
                    langCombobox.setVisible(true);
                    langCombobox.setStore(languagesStore);
                    langCombobox.select(Locale.getCurrentLanguage());

                } else {
                    langCombobox.setVisible(false);
                }
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageLanguageComboboxSection of BaseReportsPreviewPageiOS', ex);
        }
    },

    manageReportsDisplaySection : function() {
        try {
            var reportFramework = AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance();
            if(reportFramework._isVerticalView){
                this.showVerticalLayout();
            } else {
                this.showTabLayout();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageReportsDisplaySection of BaseReportsPreviewPageiOS', ex);
        }
    },

    createLanguageStore : function(availableLangs){
        try {
            var me = this;
            var langsStore =  Ext.create('Ext.data.Store', {
                storeId: 'reportLangStore',
                fields: ['id','reportTypeDescription'],
                autoLoad: false
            });

            availableLangs.each(function(language){
                switch(language.get('lang')){
                    case 'E':
                        langsStore.add({
                            'id': 'en_US',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['E'])
                        })
                        break;
                    case 'D':
                        langsStore.add({
                            'id': 'de_DE',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['D'])
                        })
                        break;
                    case 'L':
                        langsStore.add({
                            'id': 'pl_PL',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['L'])
                        })
                        break;
                    case '1':
                        langsStore.add({
                            'id': 'zh_zh',
                            'reportTypeDescription': Locale.getMsg(me._languageMappingObject['1'])
                        })
                        break;
                }
            });

            return langsStore;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createLanguageStore of BaseReportsPreviewPageiOS', ex);
        }
    },

    renewComboboxContainer: function() {
        try {
            var languageComboBoxSelection = Ext.getCmp('languageComboBoxSelection');
            var myController = this.getController();
            languageComboBoxSelection.removeAll();
            languageComboBoxSelection.add({
                xtype: 'oxcombobox',
                id: 'docLanguageComboBox',
                fieldLabel: Locale.getMsg('reportLanguageDialogTitle'),
                displayField: 'reportTypeDescription',
                valueField: 'id',
                queryMode: 'local',
                width: 400,
                hidden: true,
                maxHeight: 30,
                editable: false,
                labelWidth: 200,
                value: Locale.getMsg('optionalParameter'),
                listeners: {
                    select: myController.onDocumentLanguageSelect,
                    scope: myController
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside renewComboboxContainer of BaseReportsPreviewPageiOS', ex);
        }
    },


    buildTabsUserInterface: function () {
        try{
            var retval = [];

            var myController = this.getController();
            var reportsToShow = myController.getDataToShow();
            var documentSignatures = myController.getRequiredSignatures();

            var me = this;

            if(myController._backUpOfTheData.length === 0){
                for(var j = 0; j < reportsToShow.length; j++){
                    myController._backUpOfTheData.push({
                        object : $.extend(false, {}, reportsToShow[j]['object']),
                        pdf : $.extend(false, {}, reportsToShow[j]['pdf'])
                    });
                }
            }

            if(myController._backUpOfTheData.length !== 0){
                reportsToShow = myController._backUpOfTheData;
            }

            var tabsContainer = Ext.getCmp('tabReportsPreviewPaneliOSContainer');

            for (idx in reportsToShow) {
                var randomId = this._makeId();
                var title  = reportsToShow[idx].object['mainObject'].get('id');

                retval.push(
                    Ext.create('Ext.container.Container',
                        {
                            hideContextMenuColumn: true,
                            cls: 'reportsPreviewPageIOSMainContainer',
                            title: AssetManagement.customer.utils.StringUtils.filterLeadingZeros(title),
                            items: [{
                                xtype: 'container',
                                flex: 1,
                                scrollable: false,
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                items: [{
                                    xtype: 'label',
                                    cls: 'reportsPreviewIOSLabel',
                                    margin: '20 0 0 0',
                                    text: Locale.getMsg('mymViewReport'),
                                    width: 150
                                },
                                {
                                    xtype: 'button',
                                    margin: '0 0 0 0',
                                    html: '<div><img width="100%" src="resources/icons/viewDocument.png"></img></div>',
                                    cls: 'circledBorder',
                                    height: 60,
                                    width: 60,
                                    text: '',
                                    listeners: {
                                        click: 'onViewDocumentIconClicked'
                                    }
                                }]

                            },
                            {
                                xtype: 'container',
                                flex: 1,
                                margin: '20 0 0 0',
                                scrollable: false,
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                items: [{
                                    xtype: 'label',
                                    margin: '8 0 0 0',
                                    cls: 'reportsPreviewIOSLabel',
                                    text: Locale.getMsg('mymReportAccepted'),
                                    width: 150
                                },
                                {
                                    xtype: 'checkbox',
                                    baseCls: 'oxCheckBox',
                                    checkedCls: 'checked',
                                    itemId: 'acceptedReportCheckbox',
                                    margin: '0 0 0 15',
                                    width: 30,
                                    height: 35,
                                    listeners: {
                                        change: 'onAcceptedReportCheckboxClicked'
                                    }
                                }]
                            }]
                        }
                    )
                );
            }

            var lastActiveElement = this.getIndexOfActiveTab(tabsContainer);
            tabsContainer.removeAll();

            tabsContainer.add(retval);
            if (lastActiveElement) {
                tabsContainer.setActiveItem(lastActiveElement);
            } else {
                tabsContainer.setActiveTab(0);
            }
            return;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildTabsUserInterface of BaseReportsPreviewPageiOS', ex);
        }
    },

    _makeId: function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < 18; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    getIndexOfActiveTab : function(tabsContainer){
        var panelIndex = null;
        var selectedItem = tabsContainer.getActiveTab();
        var items = tabsContainer.items.items;
        if (selectedItem) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].title === selectedItem.title) {
                    panelIndex = i;
                }
            }
        }
        return panelIndex;
    },

    buildVerticaInterface: function () {

        try{

            var me = this;

            var myController = this.getController();

            var dataToShow = this.getController().getDataToShow();


            var superReport = {};
            var copyOfOriginalDataArray = [];

            if(myController._backUpOfTheData.length === 0){
                copyOfOriginalDataArray = $.extend(true, [], dataToShow);

            } else{
                AssetManagement.customer.modules.reportFramework.ReportPdfMaker.getInstance().prepareAlreadySignedDocuments(myController._backUpOfTheData, Locale);
                copyOfOriginalDataArray = $.extend(true, [], myController._backUpOfTheData);
            }

            var verticalContainer = Ext.getCmp('verticalReportsPreviewiOSContainer');

            verticalContainer.removeAll();

            for (var i = 0; i < copyOfOriginalDataArray.length; i++) {
                if ($.isEmptyObject(superReport)) {
                    // add first report
                    superReport = jQuery.extend(true, {}, copyOfOriginalDataArray[0].pdf);
                } else {
                    //workaround to make library to merge multiple docs into just one
                    superReport.docDefinition.content.push({
                        text: '',
                        pageBreak: 'after'
                    });
                    contentOfReport = copyOfOriginalDataArray[i].pdf.docDefinition.content;

                    for (var j = 0; j < contentOfReport.length; j++) {
                        superReport.docDefinition.content.push(contentOfReport[j]);
                    }
                }

                if (copyOfOriginalDataArray[i].signatures && copyOfOriginalDataArray[i].signatures.length > 0) {
                    superReport.docDefinition.content.push('\n\n\n');
                    var signatureSection = myController.prepareSignatureSection(copyOfOriginalDataArray[i].signatures);
                    superReport.docDefinition.content.push(signatureSection);
                }
            }

            if(myController._backUpOfTheData.length === 0){
                for(var j = 0; j < dataToShow.length; j++){
                    myController._backUpOfTheData.push({
                        object : $.extend(false, {}, dataToShow[j]['object']),
                        pdf : $.extend(false, {}, dataToShow[j]['pdf'])
                    });
                }
            }

            var retval = Ext.create('Ext.container.Container',{
                    hideContextMenuColumn: true,
                    cls: 'reportsPreviewPageIOSMainContainer',
                    items: [
                    {
                        xtype: 'container',
                        flex: 1,
                        scrollable: false,
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'label',
                            cls: 'reportsPreviewIOSLabel',
                            id: 'viewReportLabel',
                            margin: '20 0 0 0',
                            text: '',//Locale.getMsg('mymViewReport'),
                            width: 150
                        },
                        {
                            xtype: 'button',
                            margin: '0 0 0 0',
                            html: '<div><img width="100%" src="resources/icons/viewDocument.png"></img></div>',
                            cls: 'circledBorder',
                            height: 60,
                            width: 60,
                            text: '',
                            listeners: {
                                click: 'onViewDocumentIconClicked'
                                // afterrender: function(btn){
                                //     myController.onAddTouchEndEventReports(btn);

                                // }
                            }
                        }]

                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        margin: '20 0 0 0',
                        scrollable: false,
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'label',
                            margin: '8 0 0 0',
                            id: 'acceptedReportLabel',
                            cls: 'reportsPreviewIOSLabel',
                            text: '',//Locale.getMsg('mymReportAccepted'),
                            width: 150
                        },
                        {
                            xtype: 'checkbox',
                            baseCls: 'oxCheckBox',
                            checkedCls: 'checked',
                            margin: '0 0 0 15',
                            id: 'acceptReportsCheckboxIOS',
                            width: 30,
                            height: 35,
                            listeners: {
                                change: 'onAcceptedReportCheckboxClicked'
                            }
                        }]
                    }]
                }
            );

            verticalContainer.add(retval);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildVerticaInterface of BaseReportsPreviewPageiOS', ex);
        }
    }
});