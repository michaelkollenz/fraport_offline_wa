Ext.define('AssetManagement.base.modules.reportFramework.controller.page.BaseReportsPreviewPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.FuncPara',
        'AssetManagement.customer.model.bo.File',
        'AssetManagement.customer.manager.DocUploadManager',
        'AssetManagement.customer.modules.reportFramework.ReportFramework',
        'AssetManagement.customer.sync.UserLoginInfo',
        'AssetManagement.customer.modules.reportFramework.layout.signaturesLayout.SignaturesSectionLayout'
        //'AssetManagement.customer.controller.NavigationController'
        //'AssetManagement.customer.controller.ClientStateController'
    ],

    _signatures: [],
    _dataToShow: [],
    _documentType: null,
    _frameworkSuccessCallback: null,
    _reportLanguage: null,
    _signingType: null,
    //function
    _enhancement: null,

    _emailDetails : null,
    _signaturesNeeded: [],
    _saveRunning: false,

    _backUpOfTheData : [],

    // public
    requestPage: function (pageRequestCallback, pageRequestCallbackScope, argumentsObject) {
        try {

            this._pageRequestCallback = pageRequestCallback;
            this._pageRequestCallbackScope = pageRequestCallbackScope;
            //this._currentArguments = argumentsObject;
            /////
            this._dataToShow = argumentsObject.reportsData;
            this._documentType = argumentsObject.documentType;
            this._frameworkSuccessCallback = argumentsObject.masterSuccessCallback;
            this._signingType = argumentsObject.signingType;
            this._enhancement = argumentsObject.enhancement;
            this._reportLanguage = argumentsObject.reportLang;

            this._backUpOfTheData = [];

            this._signaturesNeeded = this.determineCountOfSignatureReq();
            this.setHasUnsavedChanges(false);

            if (!this.getIsAsynchronous()) {
                //do not use update to avoid it's defering
                this.refreshData(true);
            } else {
                this.getViewModel().resetData();
                this.signalizeReload(argumentsObject);
                this.callPageRequestCallback(true);
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestPage of BaseReportsPreviewPageController', ex);
            this.errorOccurred();
        }
    },

    // private
    getDataToShow: function () {
        return this._dataToShow;
    },

    // private
    getReportType: function() {
        return this._documentType;
    },

    // private
    getRequiredSignatures : function(){
        return this._signaturesNeeded;
    },

    onCreateOptionMenu: function () {
        try {
            this._optionMenuItems = new Array();
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SIGNING_BUTTON);
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE_REPORTS);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseReportsPreviewPageController', ex)
        }
    },

    onOptionMenuItemSelected: function (optionID, arg1) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SIGNING_BUTTON) {
                this.onSignDocument();
            } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE_REPORTS) {
                this.onSaveObjects();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseReportsPreviewPageController', ex);
        }
    },

    onSignDocument: function () {
        try {
            //check how much signatures are required
            var tabPanel = Ext.getCmp('tabReportsPreviewPanel');
            var verticalPanel = Ext.getCmp('verticalReportsPreview');

            var onCountOfSignatures = this.determineCountOfSignatureReq();

            if (tabPanel.isVisible()) {
                var activeTab = tabPanel.getActiveTab();
                this.checkForSignature(activeTab);

            } else {
                this.showSignatureDialog()
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSignDocument of BaseReportsPreviewPageController', ex);
        }
    },

    onSaveObjects: function () {
        try {
            if (!this.checkIfAllIsSigned()) return;
            if (this._saveRunning === false) {
                AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
                this._saveRunning = true;
                this.saveObjectsToDatabase();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaveObjects of BaseReportsPreviewPageController', ex);
        }
    },

     // signing each document separetally despite the view
    performSignaturePerDocAction: function (itemId, panel) {
        try {

            var signaturesNeeded = this._signaturesNeeded;
            var countOfSignaturesNeeded = signaturesNeeded.length;
            var counter = 0;
            var me = this;

            if (itemId) {
                this.performSignaturePerDocFromTabView(itemId);
            } else {
                this.performSignaturePerDocFromVerticalView();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocAction of BaseReportsPreviewPageController', ex);
        }
    },

    performSignaturePerDocFromVerticalView: function() {
        try {
            var signaturesNeeded = this._signaturesNeeded;
            var countOfSignaturesNeeded = signaturesNeeded.length;
            var counter = 0;
            var me = this;

            var data = this._backUpOfTheData;
            var dataCounter = 0;

            var id = data[dataCounter]['object']['mainObject'].get('id');
            var objectToSign = this.onFindObjectBasedOnId(id);
            var assignment = data[dataCounter]['object']['mainObject'];

            var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
            var pickupPara = funcPara.get('pick_mnt_act_type');
            var replenishmentPara = funcPara.get('rpln_mnt_act_type');

            var auart = assignment.get('auart');
            var ilart = assignment.get('ilart');

            var isPickUp = pickupPara && auart === pickupPara.get('fieldval1') && ilart === pickupPara.get('fieldval2');
            var isBSRepl = !isPickUp && replenishmentPara && auart === replenishmentPara.get('fieldval1') && ilart === replenishmentPara.get('fieldval2');

            var reservation = assignment.get('reservation');
            var combinedRun = reservation ? reservation.get('combinedRun') : '';

            var sbPartner = combinedRun ? combinedRun.getPartnerSB() : '';

            var customerEmail = '';

            if (isBSRepl) {
                var partnerWe = assignment.getPartnerWE();

                customerEmail = partnerWe ? partnerWe.get('email') : '';
            } else {
                customerEmail = sbPartner ? sbPartner.get('email') : '';
            }

            var mymContactPerson = combinedRun ? combinedRun.get('wardContactName') : '';

            if (isPickUp) {
                //try to use end contact person, if set
                var endContactName = reservation ? reservation.get('endContact') : '';

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(endContactName)) {
                    mymContactPerson = endContactName;
                }
            }

            var openSignDialog = function (counter, itemId) {
                try {
                    if (dataCounter < data.length) {

                        successCallbackPerSign = function (signature, emailAddresses, signPlainText, signatureCounter) {
                            try {
                                if (signature) {
                                    signaturesNeeded[signatureCounter].signature = signature;
                                    signaturesNeeded[signatureCounter].emailAddresses = emailAddresses;
                                    signaturesNeeded[signatureCounter].signPlainText = signPlainText;
                                    me.addSignatureToSingleReport(signaturesNeeded[signatureCounter], itemId);//signature, emailAddresses);

                                    counter++;

                                    me.getViewModel().set('isSaveMode', counter === signaturesNeeded.length);
                                    me.refreshView();
                                    me.setHasUnsavedChanges(true);

                                    if (counter < signaturesNeeded.length) {
                                        openSignDialog(counter, data[dataCounter]['object']['mainObject'].get('id'));
                                    } else {
                                        counter = 0;
                                        dataCounter++;
                                        if (data[dataCounter]) {
                                            openSignDialog(counter, data[dataCounter]['object']['mainObject'].get('id'));
                                        } else {
                                        }
                                    }

                                } else {
                                    //var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorCreatingOrderReport'), false, 2);
                                    //AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocFromVerticalView-openSignDialog-successCallbackPerSign of BaseReportsPreviewPageController', ex);
                            }
                        };

                        cancelCallbackPerSign = function () {
                            try {

                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocFromVerticalView-openSignDialog-cancelCallbackPerSign of BaseReportsPreviewPageController', ex);
                            }
                        };

                        var signType = me.determineNeededSignature();
                        var signatureCounter = 0;

                        if (!signType){
                            return;
                        } else {
                            if(signType['sign1Required']){
                                signatureCounter = 0;
                            } else if(signType['sign2Required']){
                                signatureCounter = 1;
                            } else if(signType['sign3Required']){
                                signatureCounter = 2;
                            } else if(signType['sign4Required']){
                                signatureCounter = 3;
                            }
                        }

                        var request = {
                            emailDetails: me._emailDetails,
                            signatureInstance: signType,
                            itemId: itemId,
                            signatureCounter: signatureCounter,
                            customerEmail: customerEmail,
                            mymContactPerson: mymContactPerson,
                            assignment: objectToSign
                        };

                        var builderDialogArguments = {
                            successCallback: successCallbackPerSign,
                            cancelCallback: cancelCallbackPerSign,
                            owner: me,
                            arguments: [request]
                        };

                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.REPORT_SIGNATURE_DIALOG, builderDialogArguments);

                    } else {
                        //dataCounter++;
                        //openSignDialog(dataCounter, data[dataCounter].get('aufnr'));
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocFromVerticalView-openSingleSignatureDialog of BaseReportsPreviewPageController', ex);
                }
            };

            openSignDialog(dataCounter, id);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocFromVerticalView of BaseReportsPreviewPageController', ex);
        }
    },

    performSignaturePerDocFromTabView: function(itemId) {
        try {
            var counter = 0;
            var me = this;

            var data = this._backUpOfTheData;
            var dataCounter = 0;

            var id = data[dataCounter]['object']['mainObject'].get('id');
            var objectToSign = this.onFindObjectBasedOnId(id);

            var signaturesNeeded = this._signaturesNeeded;
            var countOfSignaturesNeeded = signaturesNeeded.length;
            // var id = data[dataCounter]['object']['mainObject'].get('id');
            // var objectToSign = this.onFindObjectBasedOnId(id);
            var counter = 0;
            var me = this;

            var successCallback = function (signature, emailAddresses, signPlainText, signatureCounter) {
                try {
                    if (signature) {
                        signaturesNeeded[signatureCounter].signature = signature;
                        signaturesNeeded[signatureCounter].emailAddresses = emailAddresses;
                        signaturesNeeded[signatureCounter].signPlainText = signPlainText;
                        me.addSignatureToSingleReport(signaturesNeeded[signatureCounter], itemId, emailAddresses);//signature, emailAddresses);
                        me.refreshView();
                        counter++;
                        openSignatureDialog(counter);
                        me.setHasUnsavedChanges(true);

                    } else {
                        //var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorCreatingOrderReport'), false, 2);
                        //AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }

                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-successCallback of BaseReportsPreviewPageController', ex);
                }
            };

            var cancelCallback = function () {
                try {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsCreationCancelled'), true, 1);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-cancelCallback of BaseReportsPreviewPageController', ex);
                }
            };

            var workDone = function () {
                try {

                    //me.addSignatureToReports();//signature, emailAddresses);
                    // me.refreshView();
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-workDone of BaseReportsPreviewPageController', ex);
                }
            };

            var openSignatureDialog = function (counter, itemId) {
                try {
                    if (counter < signaturesNeeded.length) {

                        var signType = me.determineNeededSignature();
                        var signatureCounter = 0;

                        if (!signType){
                            return;
                        } else {
                            if(signType['sign1Required']){
                                signatureCounter = 0;
                            } else if(signType['sign2Required']){
                                signatureCounter = 1;
                            } else if(signType['sign3Required']){
                                signatureCounter = 2;
                            } else if(signType['sign4Required']){
                                signatureCounter = 3;
                            }
                        }

                        var request = {
                            emailDetails: me._emailDetails,
                            signatureInstance: signType,
                            itemId: itemId,
                            signatureCounter: signatureCounter,
                            customerEmail: '',
                            mymContactPerson: '',
                            assignment: objectToSign
                        };

                        var builderDialogArguments = {
                            successCallback: successCallback,
                            cancelCallback: cancelCallback,
                            owner: me,
                            arguments: [request]
                        };

                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.REPORT_SIGNATURE_DIALOG, builderDialogArguments);

                    } else {
                        workDone();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-openSingleSignatureDialog of BaseReportsPreviewPageController', ex);
                }
            };

            openSignatureDialog(counter, itemId);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocFromTabView of BaseReportsPreviewPageController', ex);
        }
    },

    performSingleSignatureForAllDocsAction: function (scope) {
        try {

            //determine which signatures are required
            var signatureRequired = this._signaturesNeeded;
            var counter = 0;

            var me = this;

            var successCallback = function (signature, emailAddresses, signaturePlainText, signCounter) {
                try {
                    if (signature) {
                        signatureRequired[signCounter].signature = signature;
                        signatureRequired[signCounter].emailAddresses = emailAddresses;
                        signatureRequired[signCounter].signPlainText = signaturePlainText;
                        me.addSignatureToReports(signatureRequired[signCounter]);//signature, emailAddresses);
                        me.getViewModel().set('isSaveMode', true);
                        me.refreshView();
                        counter++;
                        openSingleSignatureDialog(counter);
                        me.setHasUnsavedChanges(true);

                    } else {
                        //var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorCreatingOrderReport'), false, 2);
                        //AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }

                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-successCallback of BaseReportsPreviewPageController', ex);
                }
            }

            var cancelCallback = function () {
                try {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsCreationCancelled'), true, 1);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-cancelCallback of BaseReportsPreviewPageController', ex);
                }
            }

            var workDone = function () {
                try {

                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-workDone of BaseReportsPreviewPageController', ex);
                }
            }


            var openSingleSignatureDialog = function (counter) {
                try{
                    if (counter < signatureRequired.length) {

                        var signType = me.determineNeededSignature();
                        var signatureCounter = 0;
                        if (!signType) {
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsAlreadySigned'), true, 1);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                            return;
                        } else {
                            if(signType['sign1Required']){
                                signatureCounter = 0;
                            } else if(signType['sign2Required']){
                                signatureCounter = 1;
                            } else if(signType['sign3Required']){
                                signatureCounter = 2;
                            } else if(signType['sign4Required']){
                                signatureCounter = 3;
                            }
                        }

                        var request = {
                            emailDetails: me._emailDetails,
                            signatureInstance: signType, //signatureRequired[counter] //me.determineNeededSignature(signatureRequired) //signatureRequired[counter]
                            signatureCounter : signatureCounter
                        }
                        var builderDialogArguments = {
                            successCallback: successCallback,
                            cancelCallback: cancelCallback,
                            owner: me,
                            arguments: [ request ]
                        };

                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.REPORT_SIGNATURE_DIALOG, builderDialogArguments);

                    } else {
                        workDone();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-openSingleSignatureDialog of BaseReportsPreviewPageController', ex);
                }
            }

            openSingleSignatureDialog(counter)

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction of BaseReportsPreviewPageController', ex);
        }
    },

    addSignatureToReports: function (signature, emailAddresses) {
        try {
            var data = this._backUpOfTheData;
            var signaturesNeeded = this._signaturesNeeded;
            for (var i = 0; i < data.length; i++) {
                if(!data[i].signatures) data[i].signatures = [];
                data[i].signatures.push({
                    signatureObject: signature,
                    signPlainText: signature.signaturePlainText,
                    emailAddresses: emailAddresses
                });
                if (data[i].signatures.length === signaturesNeeded.length) {
                    data[i].signedByAll = true;
                } else {
                    data[i].signedByAll = false;
                }
            }
        this._dataToShow = data;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addSignaturesToReports of BaseReportsPreviewPageController', ex);
        }
    },

    addSignatureToSingleReport: function (signature, itemId, emailAddresses) {
        try {
            var data = this._backUpOfTheData;
            var signaturesNeeded = this._signaturesNeeded;
            // var itemIdWithLeadingZeros = this.addLeadingZeros(itemId);
            // if (itemIdWithLeadingZeros === -1) {
            //     //point error here
            //     return false;
            // } else {
                for (var i = 0; i < data.length; i++) {
                    if (data[i]['object']['mainObject'].get('id') === itemId) {
                        if (!data[i].signatures) data[i].signatures = [];
                        data[i].signatures.push({
                            signatureObject: $.extend(true, {}, signature),
                            signPlainText : $.extend(true, {}, signature.signPlainText),
                            emailAddresses: $.extend(true, {}, emailAddresses)
                        });
                        if (data[i].signatures.length === signaturesNeeded.length) {
                            data[i].signedByAll = true;
                        } else {
                            data[i].signedByAll = false;
                        }
                    }
                }
                this._dataToShow = data;

            //}
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addSignatureToSingleReport of BaseReportsPreviewPageController', ex);
        }
    },

    showSignatureDialog: function (itemId, panel) {
        try {
            var me = this;

            if (this._signingType) {
                this.performSingleSignatureForAllDocsAction(me);
            } else {
                this.performSignaturePerDocAction(itemId);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSignatureDialog of BaseReportsPreviewPageController', ex);
        }
    },



    onFindObjectBasedOnId : function(itemId){
        try{
            var data = this.getDataToShow();

            for (var i = 0; i < data.length; i++) {
                if (data[i]['object']['mainObject'].get('id') === itemId) {
                    return data[i]['object']['mainObject'];
                }
            }
        } catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFindObjectBasedOnId of BaseReportsPreviewPageController', ex);
        }
    },

    checkForSignature: function (activeTab) {
        try {
            // additional action - if document has to be signed but it is not - change tab color background
            var itemId = activeTab['title'];
            this.showSignatureDialog(itemId);
            //TODO check if signs are still need
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForSignature of BaseReportsPreviewPageController', ex);
        }
    },

    // signaturePresent : function(itemId){
    //     var retVal = false;
    //     try {
    //         var itemIdWithLeadingzeros = this.addLeadingZeros(itemId);
    //         var data = this.getDataToShow();
    //         for (var i = 0 ; i < data.length; i++) {
    //             if (data[i].object.get('id') === itemIdWithLeadingzeros && data[i].signatures) return true;
    //         }
    //     } catch (ex) {
    //         AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside signaturePresent of BaseReportsPreviewPageController', ex);
    //     }
    //     return retVal;
    // },

    addLeadingZeros: function (itemId) {
        var retVal = -1;
        try {
            var leadingZero = "0";
            while (itemId.length < 12) {
                itemId = leadingZero.concat(itemId);
            }
            retVal = itemId;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addLeadingZeros of BaseReportsPreviewPageController', ex);
        }
        return retVal;
    },

    addLeadingZerosOperation: function (itemId) {
        var retVal = -1;
        try {
            var leadingZero = "0";
            while (itemId.length < 34) {
                itemId = leadingZero.concat(itemId);
            }
            retVal = itemId;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addLeadingZerosOperation of BaseReportsPreviewPageController', ex);
        }
        return retVal;
    },

    saveObjectsToDatabase: function () {
        //exctract items from view
        var me = this;

        var tabLayout = Ext.getCmp('tabReportsPreviewPanel');
        var verticalLayout = Ext.getCmp('verticalReportsPreview');

        var filesToSave = [];

        var frameworkSuccessCallback = this._frameworkSuccessCallback;

        //debugger;
        if (tabLayout.isVisible()) {
            var tabPanelReports = tabLayout.items.items;
            for (var i = 0; i < tabPanelReports.length; i++) {
                filesToSave.push({
                    file: me.createFile(tabPanelReports[i]),
                    objectId: me.addLeadingZeros(tabPanelReports[i].title)
                });
            }
            if (filesToSave.length > 0) {
                this.assignGeneratedReportToObject(filesToSave);
                this.saveFilesIntoDB();
            }
        }

        if (verticalLayout.isVisible()) {
            this.prepareSingleReportsFromVerticalView();
        }
    },

    prepareSingleReportsFromVerticalView : function(){
        try {
            var me = this;
            var dataToProcess = this._backUpOfTheData;
            AssetManagement.customer.modules.reportFramework.ReportPdfMaker.getInstance().prepareAlreadySignedDocuments(dataToProcess, Locale);

            var counter = 0;

            var assignGeneratedReportToObject = function (pdfAsBuffer) {
                try {
                    if (pdfAsBuffer) {
                        var pdfAsFile = me.createFile(null, pdfAsBuffer, me._dataToShow[counter]['object']['mainObject']);
                        me._dataToShow[counter].generatedReport = {
                            file: pdfAsFile
                        }
                    }
                    counter++
                    if (counter < dataToProcess.length) {
                        generateReport(counter);
                    } else {
                        me.saveFilesIntoDB();
                    }

                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSingleReportsFromVerticalView-assignGeneratedReportToObject of BaseReportsPreviewPageController', ex);
                }
            }

            var generateReport = function (counter) {
                try {

                    dataToProcess[counter].pdf.docDefinition.content.push('\n\n\n');
                    dataToProcess[counter].pdf.docDefinition.content.push(me.prepareSignatureSection(dataToProcess[counter].signatures, me._reportLanguage));

                    dataToProcess[counter].pdf.getBuffer(function (buffer){
                        assignGeneratedReportToObject(buffer);
                    });
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSingleReportsFromVerticalView-generateReport of BaseReportsPreviewPageController', ex);
                }
            }

            generateReport(counter);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSingleReportsFromVerticalView of BaseReportsPreviewPageController', ex);
        }
    },

    assignGeneratedReportToObject : function(generatedReports){
        try {

            if (generatedReports) {
                //for orders
                for(var i = 0; i < generatedReports.length; i++){
                    if(generatedReports[i].objectId === this._dataToShow[i]['object']['mainObject'].get('aufnr')){
                        this._dataToShow[i].generatedReport = generatedReports[i];
                    }
                }
                // for operations
                for (var i = 0; i < generatedReports.length; i++) {
                    var idOfAnObject = "00000" + generatedReports[i].objectId;
                    if (idOfAnObject === this._dataToShow[i]['object']['mainObject'].id) {
                        this._dataToShow[i].generatedReport = generatedReports[i];
                    }
                }
            }
        } catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignGeneratedReportToObject of BaseReportsPreviewPageController', ex);
        }
    },


    saveFilesIntoDB : function(){
        try{
            var counter = 0;
            var objectsToSave = this._backUpOfTheData.length;

            var me = this;

            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var enhancemenmtCallback = function (answer) {
                try {
                    if (answer) {
                        counter++;
                        if (counter < objectsToSave) {
                            saveObject(me._backUpOfTheData[counter]);
                        } else {
                            //success
                            me.setHasUnsavedChanges(false);
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            if(me._frameworkSuccessCallback)
                                me._frameworkSuccessCallback();
                            //AssetManagement.customer.core.Core.navigateBack();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsSuccessfullyCreated'), true, 0);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                            me._saveRunning = false;
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-enhancemenmtCallback of BaseReportsPreviewPageController', ex);
                }
            }

            var processingFiles = function (docUploaded) {
                try {
                    var eventId = me._enhancement(me._backUpOfTheData[counter]['object']['mainObject']);

                    if (eventId > -1) {
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, enhancemenmtCallback, me);
                    }

                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-processingFiles of BaseReportsPreviewPageController', ex);
                }
            }

            var saveObject = function (object) {
                try {
                    if(object){
                        var mainObject = object['object']['mainObject'];
                        var parentObject = object['object']['parentBO'];
                        var layoutType = mainObject ? mainObject.get('layoutType') : null;
                        var documentDetails = layoutType ? layoutType.get('docData') : null;
                        var targetObjects = documentDetails ? documentDetails.get('docTarget') : null;
                        var emailAddressesString = me.prepareEmailAdressesString(object.signatures[0].signatureObject.emailAddresses);

                        if(targetObjects && targetObjects.getCount() > 0){
                            var saveCounter = 0;
                            var targets = targetObjects.getCount();

                            var saveSingleObject = function(){
                                try {
                                    if(saveCounter < targets) {
                                        var eventId = -1;
                                        switch(targetObjects.getAt(saveCounter).get('templateId')){
                                            case 1:
                                            //order
                                                eventId = AssetManagement.customer.manager.DocUploadManager.addOrderReport(object.generatedReport.file, mainObject, emailAddressesString);
                                                break;
                                            case 2:
                                                //checklistreports are also added to order, but main object is different
                                                eventId = AssetManagement.customer.manager.DocUploadManager.addOrderReport(object.generatedReport.file, parentObject, emailAddressesString);
                                                break;
                                            default:
                                                //this shouldn't happen
                                                eventId = AssetManagement.customer.manager.DocUploadManager.addOrderReport(object.generatedReport.file, mainObject, emailAddressesString);
                                                break;
                                        }
                                        if (eventId > -1) {
                                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, processingFiles, me);
                                        } else {
                                            //error occured
                                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorOccurredDuringSavingReports'), false, 2);
                                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                        }
                                        saveCounter++;
                                        saveSingleObject();
                                    }
                                } catch(ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-saveObject of BaseReportsPreviewPageController', ex);
                                }
                            }

                            saveSingleObject();
                        } else {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorOccurredDuringSavingReports'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } else {
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorOccurredDuringSavingReports'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-saveObject of BaseReportsPreviewPageController', ex);
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            }

            saveObject(this._backUpOfTheData[counter]);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB of BaseReportsPreviewPageController', ex);
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }

    },

    prepareEmailAdressesString : function(emailAdressesStore){
        retval = "";
        try{
            if(emailAdressesStore && emailAdressesStore.getCount() > 0){
                emailAdressesStore.each(function(emailAddress){
                    retval += emailAddress.get('email') + ";";
                }, this);
            }
        } catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareEmailAdressesString of BaseReportsPreviewPageController', ex);
        }
        return retval;
    },


    createFile: function (panel, pdfAsBuffer, object) {
        try {
            var dateTime = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();

            var pdfAsArrayBuffer = null;
            var desc = null;
            var docId = object.get('id');

            if (panel) {
                var report = panel.items.items[0];
                pdfAsArrayBuffer = this._base64ToArrayBuffer(report.src.replace('data:application/pdf;base64,', ''));
                desc = Locale.getMsg('report') + ' ' + panel.title;
            } else {
                pdfAsArrayBuffer = pdfAsBuffer;
                desc = Locale.getMsg('report') + ' ' + docId;
            }

            //if the array buffer is actually an array buffer view, convert it down to the underlying buffer
            if (pdfAsArrayBuffer.buffer)
                pdfAsArrayBuffer = pdfAsArrayBuffer.buffer;

            desc = desc.replace(' ', '_');

            var fileName = desc + '_' + AssetManagement.customer.utils.DateTimeUtils.getDBTimestamp() + '.pdf';

            var file = Ext.create('AssetManagement.customer.model.bo.File', {
                fileName: fileName,
                fileType: 'PDF',
                fileSize: pdfAsArrayBuffer.byteLength / 1024.0,
                fileOrigin: 'local',
                fileDescription: desc,
                createdAt: dateTime,
                lastModified: dateTime,
                contentAsArrayBuffer: pdfAsArrayBuffer
            });

            return file;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createFile of BaseReportsPreviewPageController', ex);
        }

        return null;
    },

    _base64ToArrayBuffer : function (base64) {
        var binary_string =  window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array( len );
        for (var i = 0; i < len; i++){
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    },


    prepareSignatureSection: function (signatures, reportLanguage) {
        try {

          var signatureLayoutInstnce = AssetManagement.customer.modules.reportFramework.layout.signaturesLayout.SignaturesSectionLayout.getInstance();
          return signatureLayoutInstnce.buildSigntureSection(signatures, reportLanguage, this._dataToShow[0]['object']['mainObject']);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSignatureSection of BaseReportsPreviewPageController', ex);
        }
    },

    prepareEmailAddressesSection: function (emailAddress) {
        try {
            var singleEmail = {
                text: emailAddress.get('email'),
                style: {
                    fontSize: 9,
                    normal: true
                }
            };

            return singleEmail;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareEmailAddressesSection of BaseReportsPreviewPageController', ex);
        }
    },

    onDocumentLanguageSelect: function (combo, record, eOpts) {
        try {
            var me = this;
            var selectedLang = record.get('id');
            this._reportSelectedLang = record.get('id');
            var reportFramework = AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance();
            var objectsToPrint = [];
            for (var i = 0; i < this._backUpOfTheData.length; i++) {
                objectsToPrint.push(me._backUpOfTheData[i].object['mainObject']);
            }

            //var signatureType = 2;

            //AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').removeSinglePageFromHistoryStack();
            //reportFramework.createReports(null, objectsToPrint, this._frameworkSuccessCallback, null, signatureType, null, selectedLang);


            var reportFrameworkConfig = {
                isOneSignatureForAllDocuments : true,
                isVerticalView: true,
                actionAfterReportCreation: this._frameworkSuccessCallback,
                reportLanguage: selectedLang,
                signedObjets: objectsToPrint,
                preventLangChange: false
            };

            AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').removeSinglePageFromHistoryStack();
            AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance().createReports(reportFrameworkConfig, objectsToPrint, this._frameworkSuccessCallback, null);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDocumentLanguageSelect of BaseReportsPreviewPageController', ex);
        }
    },

    determineCountOfSignatureReq : function(){
        try {

            var me = this;
            var docDef = this._documentType && this._documentType.get('docData') ? this._documentType.get('docData') : null;
            if (docDef) {
                var signatureTypes = [];
                for (var property in docDef.data) {
                    if (docDef.data.hasOwnProperty(property)) {
                        if (property === 'sign1Required' && docDef.data[property] === 'X') {
                            signatureTypes.push({
                                'sign1Required': [docDef.data['sign1Name'], docDef.data['sign1CustName'], docDef.data['sign1CustNameRequired'], docDef.data['sign1CustNamePos']]
                            });
                        } else if (property === 'sign2Required' && docDef.data[property] === 'X') {
                            signatureTypes.push({
                                'sign2Required': [docDef.data['sign2Name'], docDef.data['sign2CustName'], docDef.data['sign2CustNameRequired'], docDef.data['sign2CustNamePos']]
                            });
                        } else if (property === 'sign3Required' && docDef.data[property] === 'X') {
                            signatureTypes.push({
                                'sign3Required': [docDef.data['sign3Name'], docDef.data['sign3CustName'], docDef.data['sign3CustNameRequired'], docDef.data['sign3CustNamePos']]
                            });
                        } else if (property === 'sign4Required' && docDef.data[property] === 'X') {
                            signatureTypes.push({
                                'sign4Required': [docDef.data['sign4Name'], docDef.data['sign4CustName'], docDef.data['sign4CustNameRequired'], docDef.data['sign4CustNamePos']]
                            });
                        }
                    }
                }
                return signatureTypes;
            }
        }catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineCountOfSignatureReq of BaseReportsPreviewPageController', ex);
        }
    },

    determineNeededSignature : function(signatures){
        try {
            var me = this;
            var signsTable = this._signaturesNeeded;
            var data = this.getDataToShow();
            if (data && data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    if (!data[i].signatures) {
                        return signsTable[0]
                    }
                    if (data[i].signatures && data[i].signatures.length < signsTable.length) {
                        //determine which signature should be used
                        return me.determineMissingSignatureIndex(data[i].signatures, signsTable);
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineNeededSignature of BaseReportsPreviewPageController', ex);
        }
    },

    determineMissingSignatureIndex : function(obj, signaturesTable){
        try{
            for (var i = 0; i < signaturesTable.length; i++) {
                if (!obj[i].signatureObject.hasOwnProperty('sign1Required')) {
                    for (var el in signaturesTable) {
                        if (signaturesTable[el].hasOwnProperty('sign1Required')) {
                            return signaturesTable[el];
                        }
                    }
                }
                if(!obj[i].signatureObject.hasOwnProperty('sign2Required')){
                    for (var el in signaturesTable) {
                        if (signaturesTable[el].hasOwnProperty('sign2Required')) {
                            return signaturesTable[el];
                        }
                    }
                }
                if(!obj[i].signatureObject.hasOwnProperty('sign3Required')){
                    for (var el in signaturesTable) {
                        if (signaturesTable[el].hasOwnProperty('sign3Required')) {
                            return signaturesTable[el];
                        }
                    }
                }
                if(!obj[i].signatureObject.hasOwnProperty('sign4Required')){
                    for (var el in signaturesTable) {
                        if (signaturesTable[el].hasOwnProperty('sign4Required')) {
                            return signaturesTable[el];
                        }
                    }
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineMissingSignatureIndex of BaseReportsPreviewPageController', ex);
        }
    },

    determineIfEmailRequired : function(){
        try{
            var docDef = this._documentType && this._documentType.get('docData') ? this._documentType.get('docData') : null;
            if (docDef) {
                var emailDetails = {
                    numberOfEmailsAllowed: docDef.data['numEmail'],
                    defaultCustEmail: docDef.data['defaultCustEmail']
                };
                return emailDetails;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineIfEmailRequired of BaseReportsPreviewPageController', ex);
        }
    },


    checkIfAllIsSigned: function () {
        try{
            //only check if at least one signature is missing
            var data = this.getDataToShow();
            var countOfSignatureNeeded = this._signaturesNeeded.length;

            for (var i = 0; i < data.length; i++) {
                if (!data[i].signatures && countOfSignatureNeeded !== 0) {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('pleaseSignAllDoc'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    return false;
                }
                if (data[i].signatures && data[i].signatures.length < countOfSignatureNeeded) {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('pleaseSignAllDoc'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    return false;
                }
            }
            return true;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkIfAllIsSigned of BaseReportsPreviewPageController', ex);
        }
    }
});
