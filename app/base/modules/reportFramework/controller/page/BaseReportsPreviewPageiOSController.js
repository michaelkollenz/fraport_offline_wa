Ext.define('AssetManagement.base.modules.reportFramework.controller.page.BaseReportsPreviewPageiOSController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',


  requires: [
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.model.bo.FuncPara',
    'AssetManagement.customer.model.bo.File',
    'AssetManagement.customer.manager.DocUploadManager',
    'AssetManagement.customer.modules.reportFramework.ReportFramework',
    'AssetManagement.customer.sync.UserLoginInfo',
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.modules.reportFramework.layout.signaturesLayout.SignaturesSectionLayout'
  ],

  _signatures: [],
  _dataToShow: [],
  _documentType: null,
  _frameworkSuccessCallback: null,
  _reportSelectedLang: null,
  _signingType: null,
  //function
  _enhancement: null,

  _emailDetails: null,
  _signaturesNeeded: [],
  _saveRunning: false,

  _backUpOfTheData: [],

  requestPage: function (pageRequestCallback, pageRequestCallbackScope, argumentsObject) {
    try {

      this._pageRequestCallback = pageRequestCallback;
      this._pageRequestCallbackScope = pageRequestCallbackScope;

      this._currentArguments = argumentsObject;

      this._dataToShow = this.getCurrentArguments().reportsData;
      this._documentType = this.getCurrentArguments().documentType;
      this._frameworkSuccessCallback = this.getCurrentArguments().masterSuccessCallback;
      this._signingType = this.getCurrentArguments().signingType;
      this._enhancement = this.getCurrentArguments().enhancement;
      this._reportLang = this._dataToShow[0]['reportLangInstance'];
      this._backUpOfTheData = [];

      this._signaturesNeeded = this.determineCountOfSignatureReq();
      this.setHasUnsavedChanges(false);
      this._saveRunning = false;

      if (!this.getIsAsynchronous()) {
        //do not use update to avoid it's defering
        this.refreshData(true);
      } else {
        this.getViewModel().resetData();
        this.signalizeReload(argumentsObject);
        this.callPageRequestCallback(true);
      }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestPage of ReportsPreviewPageController', ex);
      this.errorOccurred();
    }
  },

  onCreateOptionMenu: function () {
    try {
      this._optionMenuItems = new Array();
      this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SIGNING_BUTTON);
      this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE_REPORTS);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of ReportsPreviewPageController', ex)
    }
  },

  onOptionMenuItemSelected: function (optionID, arg1) {
    try {
      if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SIGNING_BUTTON) {
        this.onSignDocument();
      } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE_REPORTS) {
        this.onSaveObjects();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of ReportsPreviewPageController', ex);
    }
  },

  getReportType: function () {
    return this._documentType;
  },

  getDataToShow: function () {
    return this._dataToShow;
  },

  getRequiredSignatures: function () {
    return this._signaturesNeeded;
  },

  onSignDocument: function () {
    try {
      //if (this.checkIfAllIsSigned()) return;

      //check how much signatures are required
      var tabPanel = Ext.getCmp('tabReportsPreviewPaneliOSContainer');
      var verticalPanel = Ext.getCmp('verticalReportsPreviewiOSContainer');
      var myModel = this.getViewModel();
      var acceptedArray = myModel.get('accepted');

      var onCountOfSignatures = this.determineCountOfSignatureReq();

      if (tabPanel.isVisible()) {
        var activeTab = tabPanel.getActiveTab();
        this.checkForSignature(activeTab);

      } else {
        if (acceptedArray.length === this._dataToShow.length) {
          this.showSignatureDialog()
        } else {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('acceptReportsBeforeSigning'), false, 0);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSignDocument of BaseReportsPreviewPageiOSController', ex);
    }
  },

  checkForSignature: function (activeTab) {
    //check if chexbox has been marked
    var checkbox = activeTab['items']['items'][1]['items']['items'][1];
    if (!checkbox['checked']) {
      var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('acceptReportBeforeSigning'), false, 0);
      AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      return;
    }
    // additional action - if document has to be signed but it is not - change tab color background
    var itemId = activeTab['title'];
    this.showSignatureDialog(itemId, checkbox);
    //TODO check if signs are still need
  },

  showSignatureDialog: function (itemId, checkbox) {
    try {
      var me = this;
      if (this._signingType) {
        //check if is tab view and all tabs have been marked as accepted
        var allChecked = this.checkIfAllReportsAccepted();
        if (allChecked) {
          this.performSingleSignatureForAllDocsAction(me);
        } else {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('acceptReportsBeforeSigning'), false, 0);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        }
      } else {
        this.performSignaturePerDocAction(itemId, checkbox);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSignatureDialog of BaseReportsPreviewPageiOSController', ex);
    }
  },

  // signing each document separetally despite the view
  performSignaturePerDocAction: function (itemId, checkbox) {
    try {

      var signaturesNeeded = this._signaturesNeeded;
      var countOfSignaturesNeeded = signaturesNeeded.length;
      var myModel = this.getViewModel();
      var counter = 0;
      var me = this;

      if (itemId) {
        // means that it is tab view
        var successCallback = function (signature, emailAddresses, signPlainText, signatureCounter) {
          try {
            if (signature) {
              signaturesNeeded[signatureCounter].signature = signature;
              signaturesNeeded[signatureCounter].emailAddresses = emailAddresses;
              signaturesNeeded[signatureCounter].signPlainText = signPlainText;
              me.addSignatureToSingleReport(signaturesNeeded[signatureCounter], itemId, emailAddresses, me._reportLang);//signature, emailAddresses);

              counter++;
              me.getViewModel().set('isSaveMode', myModel.get('accepted').length === me._backUpOfTheData.length);

              me.setHasUnsavedChanges(true);
              if (myModel.get('accepted').length === me._backUpOfTheData.length) {
                me.refreshView();
                var checkboxes = Ext.ComponentQuery.query('#acceptedReportCheckbox');
                if (checkboxes && checkboxes.length > 0) {
                  for (var i = 0; i < checkboxes.length; i++) {
                    if (!checkboxes[i]['checked'])
                      checkboxes[i].setValue(true);
                    checkboxes[i].disable();
                  }
                }
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
              } else {
                checkbox ? checkbox.disable() : '';
                openSignatureDialog(counter);
              }
            } else {
              //var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorCreatingOrderReport'), false, 2);
              //AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }

          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-successCallback of BaseReportsPreviewPageiOSController', ex);
          }
        }

        var cancelCallback = function () {
          try {
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsCreationCancelled'), true, 1);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-cancelCallback of BaseReportsPreviewPageiOSController', ex);
          }
        }

        var workDone = function () {
          try {
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            //me.addSignatureToReports();//signature, emailAddresses);
            // me.refreshView();
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-workDone of BaseReportsPreviewPageiOSController', ex);
          }
        }

        var openSignatureDialog = function (counter, itemId) {
          try {
            if (counter < signaturesNeeded.length) {

              var signType = me.determineNeededSignature();
              var signatureCounter = 0;

              if (!signType) {
                return;
              } else {
                if (signType['sign1Required']) {
                  signatureCounter = 0;
                } else if (signType['sign2Required']) {
                  signatureCounter = 1;
                } else if (signType['sign3Required']) {
                  signatureCounter = 2;
                } else if (signType['sign4Required']) {
                  signatureCounter = 3;
                }
              }

              var request = {
                emailDetails: me._emailDetails,
                signatureInstance: signType,
                itemId: itemId,
                signatureCounter: signatureCounter,
                customerEmail: sbPartnerEmail,
                mymContactPerson: mymContactPerson,
                assignment: objectToSign
              }
              var builderDialogArguments = {
                successCallback: successCallback,
                cancelCallback: cancelCallback,
                owner: me,
                arguments: [request]
              };

              AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.REPORT_SIGNATURE_DIALOG, builderDialogArguments);

            } else {
              workDone();
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-openSingleSignatureDialog of BaseReportsPreviewPageiOSController', ex);
          }
        }

        openSignatureDialog(counter, itemId);

      } else {
        var data = this._backUpOfTheData;
        var dataCounter = 0;

        var openSignDialog = function (counter, itemId) {
          try {
            if (dataCounter < data.length) {

              successCallbackPerSign = function (signature, emailAddresses, signPlainText, signatureCounter) {
                try {
                  if (signature) {
                    signaturesNeeded[signatureCounter].signature = signature;
                    signaturesNeeded[signatureCounter].emailAddresses = emailAddresses;
                    signaturesNeeded[signatureCounter].signPlainText = signPlainText;
                    me.addSignatureToSingleReport(signaturesNeeded[signatureCounter], itemId, me._reportLang);//signature, emailAddresses);

                    counter++;

                    me.getViewModel().set('isSaveMode', counter === signaturesNeeded.length);

                    me.refreshView();

                    me.setHasUnsavedChanges(true);

                    if (counter < signaturesNeeded.length) {
                      openSignDialog(counter, data[dataCounter]['object']['mainObject'].get('id'));
                    } else {
                      counter = 0;
                      dataCounter++;
                      if (data[dataCounter]) {
                        openSignDialog(counter, data[dataCounter]['object']['mainObject'].get('id'));
                      } else {
                        //me.setHasUnsavedChanges(false);
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        // if(me._frameworkSuccessCallback)
                        //     me._frameworkSuccessCallback();
                        // var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsSuccessfullyCreated'), true, 0);
                        // AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        // me._saveRunning = false;
                      }
                    }

                  } else {
                    //var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorCreatingOrderReport'), false, 2);
                    //AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                  }
                } catch (ex) {
                  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-openSignDialog-successCallbackPerSign of BaseReportsPreviewPageiOSController', ex);
                }
              }

              cancelCallbackPerSign = function () {
                try {

                } catch (ex) {
                  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-openSignDialog-cancelCallbackPerSign of BaseReportsPreviewPageiOSController', ex);
                }
              }

              var signType = me.determineNeededSignature();
              var signatureCounter = 0;

              if (!signType) {
                return;
              } else {
                if (signType['sign1Required']) {
                  signatureCounter = 0;
                } else if (signType['sign2Required']) {
                  signatureCounter = 1;
                } else if (signType['sign3Required']) {
                  signatureCounter = 2;
                } else if (signType['sign4Required']) {
                  signatureCounter = 3;
                }
              }


              var request = {
                emailDetails: me._emailDetails,
                signatureInstance: signType,
                itemId: itemId,
                signatureCounter: signatureCounter,
                customerEmail: sbPartnerEmail,
                mymContactPerson: mymContactPerson,
                assignment: objectToSign
              }
              var builderDialogArguments = {
                successCallback: successCallbackPerSign,
                cancelCallback: cancelCallbackPerSign,
                owner: me,
                arguments: [request]
              };

              AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.REPORT_SIGNATURE_DIALOG, builderDialogArguments);

            } else {
              //dataCounter++;
              //openSignDialog(dataCounter, data[dataCounter].get('aufnr'));
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocAction-openSingleSignatureDialog of BaseReportsPreviewPageiOSController', ex);
          }
        }

        var mymContactPersonType = null;

        var objectToSign = this.onFindObjectBasedOnId(data[dataCounter]['object']['mainObject'].get('id'));


        var reservation = data[dataCounter]['object']['mainObject'].get('reservation');
        var combinedRun = reservation ? reservation.get('combinedRun') : '';

        var sbPartner = combinedRun ? combinedRun.getPartnerSB() : '';

        var sbPartnerEmail = '';
        var assignment = data[dataCounter]['object']['mainObject'];

        if (reservation && reservation.usesOnetimeAddress()) {
          sbPartnerEmail = sbPartner ? sbPartner.get('email') : '';
        } else {
          if (assignment.get('ilart') === 'RE4') {
            var partners = assignment.get('partners');
            var partnerWe = null;
            if (partners && partners.getCount() > 0) {
              partners.each(function (partner) {
                if (partner.get('parvw') === 'WE') {
                  partnerWe = partner;
                  return false;
                }
              })
            }
            sbPartnerEmail = partnerWe ? partnerWe.get('email') : '';
          } else {
            sbPartnerEmail = sbPartner ? sbPartner.get('email') : '';
          }
        }

        var mymContactPerson = combinedRun ? combinedRun.get('wardContactName') : '';
        openSignDialog(dataCounter, data[dataCounter]['object']['mainObject'].get('aufnr'))
      }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSignaturePerDocAction of BaseReportsPreviewPageiOSController', ex);
    }
  },

  checkIfAllReportsAccepted: function () {
    var retval = true;
    try {
      var tabView = Ext.getCmp('tabReportsPreviewPaneliOSContainer') ? Ext.getCmp('tabReportsPreviewPaneliOSContainer').isVisible() : false;
      if (tabView) {
        var checkboxes = Ext.ComponentQuery.query('#acceptedReportCheckbox');
        for (var i = 0; i < checkboxes.length; i++) {
          if (!checkboxes[i]['checked'])
            retval = false;
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkIfAllReportsAccepted of BaseReportsPreviewPageiOSController', ex);
    }

    return retval;
  },

  performSingleSignatureForAllDocsAction: function (scope) {
    try {

      //determine which signatures are required
      var signatureRequired = this._signaturesNeeded;
      var counter = 0;

      var me = this;

      var successCallback = function (signature, emailAddresses, signaturePlainText, signCounter) {
        try {
          if (signature) {
            signatureRequired[signCounter].signature = signature;
            signatureRequired[signCounter].emailAddresses = emailAddresses;
            signatureRequired[signCounter].signPlainText = signaturePlainText;
            me.addSignatureToReports(signatureRequired[signCounter], me._reportLang);//signature, emailAddresses);
            me.getViewModel().set('isSaveMode', true);
            me.refreshView();
            counter++;
            openSingleSignatureDialog(counter);
            me.setHasUnsavedChanges(true);

          } else {
            //var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorCreatingOrderReport'), false, 2);
            //AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-successCallback of BaseReportsPreviewPageiOSController', ex);
        }
      }

      var cancelCallback = function () {
        try {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsCreationCancelled'), true, 1);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction-cancelCallback of BaseReportsPreviewPageiOSController', ex);
        }
      }

      var workDone = function () {
        try {
          var checkboxes = Ext.ComponentQuery.query('#acceptedReportCheckbox');
          if (checkboxes && checkboxes.length > 0) {
            for (var i = 0; i < checkboxes.length; i++) {
              if (!checkboxes[i]['checked'])
                checkboxes[i].setValue(true);
              checkboxes[i].disable();
            }
          }

          AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-workDone of ReportsPreviewPageController', ex);
        }
      }

      var openSingleSignatureDialog = function (counter) {
        try {
          if (counter < signatureRequired.length) {

            var signType = me.determineNeededSignature();
            var signatureCounter = 0;

            if (!signType) {
              var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsAlreadySigned'), true, 1);
              AssetManagement.customer.core.Core.getMainView().showNotification(notification);
              return;
            } else {
              if (signType['sign1Required']) {
                signatureCounter = 0;
              } else if (signType['sign2Required']) {
                signatureCounter = 1;
              } else if (signType['sign3Required']) {
                signatureCounter = 2;
              } else if (signType['sign4Required']) {
                signatureCounter = 3;
              }
            }

            var request = {
              emailDetails: me._emailDetails,
              signatureInstance: signType, //signatureRequired[counter] //me.determineNeededSignature(signatureRequired) //signatureRequired[counter]
              signatureCounter: signatureCounter
            }
            var builderDialogArguments = {
              successCallback: successCallback,
              cancelCallback: cancelCallback,
              owner: me,
              arguments: [request]
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.REPORT_SIGNATURE_DIALOG, builderDialogArguments);

          } else {
            workDone();
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignatureForAllDocsAction-openSingleSignatureDialog of BaseReportsPreviewPageiOSController', ex);
        }
      }

      openSingleSignatureDialog(counter)

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSingleSignaturePerAllDocsAction of BaseReportsPreviewPageiOSController', ex);
    }
  },

  addSignatureToReports: function (signature, reportLanguage) {
    try {
      var data = this._backUpOfTheData;
      var signaturesNeeded = this._signaturesNeeded;
      for (var i = 0; i < data.length; i++) {
        if (!data[i].signatures) data[i].signatures = [];
        data[i].signatures.push({
          signatureObject: signature,
          signPlainText: signature.signaturePlainText,
          emailAddresses: [],
          signatureLanguage: reportLanguage
        });
        if (data[i].signatures.length === signaturesNeeded.length) {
          data[i].signedByAll = true;
        } else {
          data[i].signedByAll = false;
        }
      }
      this._backUpOfTheData = data;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addSignaturesToReports of BaseReportsPreviewPageiOSController', ex);
    }
  },

  addSignatureToSingleReport: function (signature, itemId, emailAddresses, reportLanguage) {
    try {
      var data = this._backUpOfTheData;
      var signaturesNeeded = this._signaturesNeeded;
      var itemIdWithLeadingZeros = this.addLeadingZeros(itemId);
      if (itemIdWithLeadingZeros === -1) {
        //point error here
        return false;
      } else {
        for (var i = 0; i < data.length; i++) {
          if (data[i]['object']['mainObject'].get('id') === itemIdWithLeadingZeros) {
            if (!data[i].signatures) data[i].signatures = [];
            data[i].signatures.push({
              signatureObject: $.extend(true, {}, signature),
              signPlainText: $.extend(true, {}, signature.signPlainText),
              emailAddresses: $.extend(true, {}, emailAddresses),
              signatureLanguage: $.extend(true, {}, reportLanguage)
            });
            if (data[i].signatures.length === signaturesNeeded.length) {
              data[i].signedByAll = true;
            } else {
              data[i].signedByAll = false;
            }
          }
        }
        this._backUpOfTheData = data;

      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addSignatureToSingleReport of BaseReportsPreviewPageiOSController', ex);
    }
  },

  onFindObjectBasedOnId: function (itemId) {
    try {
      var data = this._backUpOfTheData;

      // var itemIdWithLeadingZeros = this.addLeadingZeros(itemId);
      // if (itemIdWithLeadingZeros === -1) {
      //     //point error here
      //     return false;
      // } else {
      for (var i = 0; i < data.length; i++) {
        if (AssetManagement.customer.utils.StringUtils.filterLeadingZeros(data[i]['object']['mainObject'].get('id')) === itemId) {
          return data[i];//['object']['mainObject'];
        }
      }
      // }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFindObjectBasedOnId of BaseReportsPreviewPageiOSController', ex);
    }
  },

  determineNeededSignature: function (signatures) {
    try {
      var me = this;
      var signsTable = this._signaturesNeeded;
      var data = this.getDataToShow();
      if (data && data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          if (!data[i].signatures) {
            return signsTable[0]
          }
          if (data[i].signatures && data[i].signatures.length < signsTable.length) {
            //determine which signature should be used
            return me.determineMissingSignatureIndex(data[i].signatures, signsTable);
          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineNeededSignature of BaseReportsPreviewPageiOSController', ex);
    }
  },


  prepareSignatureSection: function (signatures, reportLanguage) {
    try {

      var signatureLayoutInstnce = AssetManagement.customer.modules.reportFramework.layout.signaturesLayout.SignaturesSectionLayout.getInstance();
      return signatureLayoutInstnce.buildSigntureSection(signatures, reportLanguage, this._dataToShow[0]['object']['mainObject']);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSignatureSection of BaseReportsPreviewPageiOSController', ex);
    }
  },

  prepareEmailAddressesSection: function (emailAddress) {
    try {
      var singleEmail = {
        text: emailAddress.get('email'),
        style: {
          fontSize: 9,
          normal: true
        }
      };

      return singleEmail;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareEmailAddressesSection of BaseReportsPreviewPageiOSController', ex);
    }

  },

  onSaveObjects: function () {
    try {
      if (!this.checkIfAllIsSigned()) return;
      if (this._saveRunning === false) {
        AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
        this._saveRunning = true;
        this.determineLayoutAndTryToSave();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaveObjects of BaseReportsPreviewPageiOSController', ex);
    }
  },

  determineLayoutAndTryToSave: function () {
    try {
      //exctract items from view
      var me = this;

      var tabLayout = Ext.getCmp('tabReportsPreviewPaneliOSContainer');
      var verticalLayout = Ext.getCmp('verticalReportsPreviewiOSContainer');

      var filesToSave = [];

      var frameworkSuccessCallback = this._frameworkSuccessCallback;

      this.prepareSingleReportsFromVerticalView();

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineLayoutAndTryToSave of BaseReportsPreviewPageiOSController', ex);
    }
  },

  createFile: function (panel, pdfAsBuffer, doc) {
    try {

      var dateTime = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();

      var pdfAsArrayBuffer = null;
      var desc = null;
      var docId = doc.get('deliveryStore').getAt(0).get('belnr');

      if (panel) {
        var report = panel.items.items[0];
        pdfAsArrayBuffer = this._base64ToArrayBuffer(report.src.replace('data:application/pdf;base64,', ''));
        desc = Locale.getMsg('report') + ' ' + panel.title;
      } else {
        pdfAsArrayBuffer = pdfAsBuffer;
        desc = Locale.getMsg('report') + ' ' + docId;
      }

      desc = desc.replace(' ', '_');

      var fileName = desc + '_' + AssetManagement.customer.utils.DateTimeUtils.getDBTimestamp() + '.pdf';

      var file = Ext.create('AssetManagement.customer.model.bo.File', {
        fileName: fileName,
        fileType: 'PDF',
        fileSize: pdfAsArrayBuffer.byteLength / 1024.0,
        fileOrigin: 'local',
        fileDescription: desc,
        createdAt: dateTime,
        lastModified: dateTime,
        contentAsArrayBuffer: pdfAsArrayBuffer
      });

      return file;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createFile of BaseReportsPreviewPageiOSController', ex);
    }

    return null;
  },

  _base64ToArrayBuffer: function (base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  },

  prepareSingleReportsFromVerticalView: function () {
    try {
      var me = this;
      var dataToProcess = this._backUpOfTheData;
      AssetManagement.customer.modules.reportFramework.ReportPdfMaker.getInstance().prepareAlreadySignedDocuments(dataToProcess, Locale);

      var counter = 0;

      var assignGeneratedReportToObject = function (pdfAsBuffer) {
        try {
          if (pdfAsBuffer) {
            var pdfAsFile = me.createFile(null, pdfAsBuffer, me._dataToShow[counter]['object']['mainObject']);
            me._backUpOfTheData[counter].generatedReport = {
              file: pdfAsFile
            }
          }
          counter++
          if (counter < dataToProcess.length) {
            generateReport(counter);
          } else {
            me.saveFilesIntoDB();
          }

        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSingleReportsFromVerticalView-assignGeneratedReportToObject of BaseReportsPreviewPageiOSController', ex);
        }
      }

      var generateReport = function (counter) {
        try {

          dataToProcess[counter].pdf.docDefinition.content.push('\n');
          dataToProcess[counter].pdf.docDefinition.content.push(me.prepareSignatureSection(dataToProcess[counter].signatures));

          dataToProcess[counter].pdf.getBuffer(function (buffer) {
            assignGeneratedReportToObject(buffer);
          });
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSingleReportsFromVerticalView-generateReport of BaseReportsPreviewPageiOSController', ex);
        }
      }

      generateReport(counter);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSingleReportsFromVerticalView of BaseReportsPreviewPageiOSController', ex);
    }
  },

  assignGeneratedReportToObject: function (generatedReports) {
    try {

      if (generatedReports) {
        //for orders
        for (var i = 0; i < generatedReports.length; i++) {
          if (generatedReports[i].objectId === this._backUpOfTheData[i]['object']['mainObject'].get('id')) {
            this._dataToShow[i].generatedReport = generatedReports[i];
          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignGeneratedReportToObject of BaseReportsPreviewPageiOSController', ex);
    }
  },

  saveFilesIntoDB: function () {
    try {
      var counter = 0;
      var objectsToSave = this._backUpOfTheData.length;

      var me = this;

      var eventController = AssetManagement.customer.controller.EventController.getInstance();
      this.setHasUnsavedChanges(false);

      var enhancemenmtCallback = function (answer) {
        try {
          if (answer) {
            counter++;
            if (counter < objectsToSave) {
              saveObject(me._backUpOfTheData[counter]);
            } else {
              //success
              me.setHasUnsavedChanges(false);
              AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
              if (me._frameworkSuccessCallback)
                me._frameworkSuccessCallback();
              //AssetManagement.customer.core.Core.navigateBack();
              var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsSuccessfullyCreated'), true, 0);
              AssetManagement.customer.core.Core.getMainView().showNotification(notification);
              me._saveRunning = false;
            }
            me._saveRunning = false;
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-enhancemenmtCallback of BaseReportsPreviewPageiOSController', ex);
        }
      }

      var processingFiles = function (docUploaded) {
        try {
          var eventId = me._enhancement(me._backUpOfTheData[counter]['object']['mainObject']);

          if (eventId > -1) {
            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, enhancemenmtCallback, me);
          }

        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-processingFiles of BaseReportsPreviewPageiOSController', ex);
        }
      }

      var saveObject = function (object) {
        try {
          if (object) {
            var mainObject = object['object']['mainObject'];
            var layoutType = mainObject ? mainObject.get('layoutType') : null;
            var documentDetails = layoutType ? layoutType.get('docData') : null;
            var targetObjects = documentDetails ? documentDetails.get('docTarget') : null;
            var emailAddressesString = me.prepareEmailAdressesString(object.signatures[0].signatureObject.emailAddresses);

            if (targetObjects && targetObjects.getCount() > 0) {
              var saveCounter = 0;
              var targets = targetObjects.getCount();

              var saveSingleObject = function () {
                try {
                  if (saveCounter < targets) {
                    var eventId = -1;
                    switch (targetObjects.getAt(saveCounter).get('templateId')) {
                      case 1: //TODO add Case 5 if they need another BO
                        //order
                        eventId = AssetManagement.customer.manager.DocUploadManager.addOrderReport(object.generatedReport.file, mainObject, emailAddressesString);
                        break;
                      case 2:
                        //checklistreports are also added to order, but main object is different
                        eventId = AssetManagement.customer.manager.DocUploadManager.addOrderReport(object.generatedReport.file, parentObject, emailAddressesString);
                        break;
                      case 5:
                        //deliveryReport
                        eventId = AssetManagement.customer.manager.DocUploadManager.addDeliveryReport(object.generatedReport.file, mainObject, emailAddressesString);
                        break;
                      default:
                        //this shouldn't happen
                        eventId = AssetManagement.customer.manager.DocUploadManager.addOrderReport(object.generatedReport.file, mainObject, emailAddressesString);
                        break;
                    }
                    if (eventId > -1) {
                      AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, processingFiles, me);
                    } else {
                      //error occured
                      var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorOccurredDuringSavingReports'), false, 2);
                      AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                    saveCounter++;
                    saveSingleObject();
                  }
                } catch (ex) {
                  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-saveObject of BaseReportsPreviewPageiOSController', ex);
                }
              }

              saveSingleObject();
            } else {
              AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
              var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorOccurredDuringSavingReports'), false, 2);
              AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
          } else {
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorOccurredDuringSavingReports'), false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB-saveObject of BaseReportsPreviewPageiOSController', ex);
          AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
      }

      saveObject(this._backUpOfTheData[counter]);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveFilesIntoDB of BaseReportsPreviewPageiOSController', ex);
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }

  },


  checkIfAllIsSigned: function () {
    try {
      //only check if at least one signature is missing
      var data = this._backUpOfTheData;
      var countOfSignatureNeeded = this._signaturesNeeded.length;

      for (var i = 0; i < data.length; i++) {
        if (!data[i].signatures && countOfSignatureNeeded !== 0) {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('pleaseSignAllDoc'), false, 2);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          return false;
        }
        if (data[i].signatures && data[i].signatures.length < countOfSignatureNeeded) {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('pleaseSignAllDoc'), false, 2);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          return false;
        }
      }
      return true;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkIfAllIsSigned of BaseReportsPreviewPageiOSController', ex);
    }
  },

  addLeadingZeros: function (itemId) {
    var retVal = -1;
    try {
      var leadingZero = "0";
      while (itemId.length < 12) {
        itemId = leadingZero.concat(itemId);
      }
      retVal = itemId;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addLeadingZeros of BaseReportsPreviewPageiOSController', ex);
    }
    return retVal;
  },

  prepareEmailAdressesString: function (emailAdressesStore) {
    retval = "";
    try {
      if (emailAdressesStore && emailAdressesStore.getCount() > 0) {
        emailAdressesStore.each(function (emailAddress) {
          retval += emailAddress.get('email') + ";";
        }, this);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareEmailAdressesString of ReportsPreviewPageController', ex);
    }
    return retval;
  },

  onViewDocumentIconClicked: function () {
    try {
      var tabLayout = Ext.getCmp('tabReportsPreviewPaneliOSContainer');
      var verticalLayout = Ext.getCmp('verticalReportsPreviewiOSContainer');
      var dataToShow = this._backUpOfTheData;// this.getDataToShow();
      AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
      if (tabLayout.isVisible()) {
        var tabTitle = Ext.getCmp('tabReportsPreviewPaneliOSContainer').getActiveTab()['title'];
        var foundObject = this.onFindObjectBasedOnId(tabTitle);
        var selectedReport = jQuery.extend(true, {}, foundObject['pdf']);
        if (foundObject.signatures && foundObject.signatures.length > 0) {
          selectedReport.docDefinition.content.push('\n\n\n');
          var signatureSection = this.prepareSignatureSection(foundObject.signatures);
          selectedReport.docDefinition.content.push(signatureSection);
        }
        selectedReport.getBuffer(function (databuffer) {
          var blob = new Blob([databuffer], {type: 'application/pdf'});
          var objectURL = URL.createObjectURL(blob);
          AssetManagement.customer.helper.NetworkHelper.openUrl(objectURL);
        });

      } else {
        var superReport = {};
        var copyOfOriginalDataArray = $.extend(true, [], dataToShow);

        for (var i = 0; i < copyOfOriginalDataArray.length; i++) {
          if ($.isEmptyObject(superReport)) {
            // add first report
            superReport = jQuery.extend(true, {}, copyOfOriginalDataArray[0].pdf);
          } else {
            //workaround to make library to merge multiple docs into just one
            superReport.docDefinition.content.push({
              text: '',
              pageBreak: 'after'
            });
            contentOfReport = copyOfOriginalDataArray[i].pdf.docDefinition.content;

            for (var j = 0; j < contentOfReport.length; j++) {
              superReport.docDefinition.content.push(contentOfReport[j]);
            }
          }

          if (copyOfOriginalDataArray[i].signatures && copyOfOriginalDataArray[i].signatures.length > 0) {
            superReport.docDefinition.content.push('\n\n\n');
            var signatureSection = this.prepareSignatureSection(copyOfOriginalDataArray[i].signatures);
            superReport.docDefinition.content.push(signatureSection);
          }
        }
        superReport.getBuffer(function (databuffer) {
          var blob = new Blob([databuffer], {type: 'application/pdf'});
          //polyfill for chrome
          var objectURL = URL.createObjectURL(blob);
          AssetManagement.customer.helper.NetworkHelper.openUrl(objectURL);
        });
      }
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onViewDocumentIconClicked of BaseReportsPreviewPageiOSController', ex);
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }
  },

  onAcceptedReportCheckboxClicked: function (checkbox, newValue, oldValue, eOpts) {
    try {

      var tabLayout = Ext.getCmp('tabReportsPreviewPaneliOSContainer');
      var verticalLayout = Ext.getCmp('verticalReportsPreviewiOSContainer');
      var myModel = this.getViewModel();

      if (tabLayout.isVisible()) {
        var tabTitle = Ext.getCmp('tabReportsPreviewPaneliOSContainer').getActiveTab()['title'];
        var foundObject = this.onFindObjectBasedOnId(tabTitle);
        var accepted = myModel.get('accepted');
        var isAlreadyAccepted = false;
        for (var i = 0; i < accepted.length; i++) {
          if (accepted[i]['object']['mainObject'].get('aufnr') === foundObject['object']['mainObject'].get('aufnr')) {
            isAlreadyAccepted = true;
          }
        }
        if (!isAlreadyAccepted) {
          myModel.get('accepted').push(foundObject);
        }
      } else {
        if (newValue) {
          myModel.set('accepted', this._backUpOfTheData);
        } else {
          myModel.set('accepted', []);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onViewDocumentIconClicked of BaseReportsPreviewPageiOSController', ex);
    }
  },

  onDocumentLanguageSelect: function (combo, record, eOpts) {
    try {
      var me = this;
      var selectedLang = record.get('id');
      this._reportSelectedLang = record.get('id');
      var reportFramework = AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance();
      var objectsToPrint = [];
      for (var i = 0; i < this._backUpOfTheData.length; i++) {
        objectsToPrint.push(me._backUpOfTheData[i].object['mainObject']);
      }


      AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').removeSinglePageFromHistoryStack();
      var reportFrameworkConfig = {
        isOneSignatureForAllDocuments: true,
        isVerticalView: true,
        actionAfterReportCreation: this._frameworkSuccessCallback,
        reportLanguage: selectedLang,
        signedObjets: objectsToPrint,
        preventLangChange: false
      };

      AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance().createReports(reportFrameworkConfig, objectsToPrint, this._frameworkSuccessCallback, null);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDocumentLanguageSelect of ReportsPreviewPageController', ex);
    }
  },

  determineCountOfSignatureReq: function () {
    try {

      var me = this;
      var docDef = this._documentType && this._documentType.get('docData') ? this._documentType.get('docData') : null;
      if (docDef) {
        var signatureTypes = [];
        for (var property in docDef.data) {
          if (docDef.data.hasOwnProperty(property)) {
            if (property === 'sign1Required' && docDef.data[property] === 'X') {
              signatureTypes.push({
                'sign1Required': [docDef.data['sign1Name'], docDef.data['sign1CustName'], docDef.data['sign1CustNameRequired'], docDef.data['sign1CustNamePos']]
              });
            } else if (property === 'sign2Required' && docDef.data[property] === 'X') {
              signatureTypes.push({
                'sign2Required': [docDef.data['sign2Name'], docDef.data['sign2CustName'], docDef.data['sign2CustNameRequired'], docDef.data['sign2CustNamePos']]
              });
            } else if (property === 'sign3Required' && docDef.data[property] === 'X') {
              signatureTypes.push({
                'sign3Required': [docDef.data['sign3Name'], docDef.data['sign3CustName'], docDef.data['sign3CustNameRequired'], docDef.data['sign3CustNamePos']]
              });
            } else if (property === 'sign4Required' && docDef.data[property] === 'X') {
              signatureTypes.push({
                'sign4Required': [docDef.data['sign4Name'], docDef.data['sign4CustName'], docDef.data['sign4CustNameRequired'], docDef.data['sign4CustNamePos']]
              });
            }
          }
        }
        return signatureTypes;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineCountOfSignatureReq of ReportsPreviewPageController', ex);
    }
  },

  determineNeededSignature: function (signatures) {
    try {
      var me = this;
      var signsTable = this._signaturesNeeded;
      var data = this._backUpOfTheData;
      if (data && data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          if (!data[i].signatures) {
            return signsTable[0]
          }
          if (data[i].signatures && data[i].signatures.length < signsTable.length) {
            //determine which signature should be used
            return me.determineMissingSignatureIndex(data[i].signatures, signsTable);
          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineNeededSignature of ReportsPreviewPageController', ex);
    }
  },

  determineMissingSignatureIndex: function (obj, signaturesTable) {
    try {
      for (var i = 0; i < signaturesTable.length; i++) {
        if (!obj[i].signatureObject.hasOwnProperty('sign1Required')) {
          for (var el in signaturesTable) {
            if (signaturesTable[el].hasOwnProperty('sign1Required')) {
              return signaturesTable[el];
            }
          }
        }
        if (!obj[i].signatureObject.hasOwnProperty('sign2Required')) {
          for (var el in signaturesTable) {
            if (signaturesTable[el].hasOwnProperty('sign2Required')) {
              return signaturesTable[el];
            }
          }
        }
        if (!obj[i].signatureObject.hasOwnProperty('sign3Required')) {
          for (var el in signaturesTable) {
            if (signaturesTable[el].hasOwnProperty('sign3Required')) {
              return signaturesTable[el];
            }
          }
        }
        if (!obj[i].signatureObject.hasOwnProperty('sign4Required')) {
          for (var el in signaturesTable) {
            if (signaturesTable[el].hasOwnProperty('sign4Required')) {
              return signaturesTable[el];
            }
          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineMissingSignatureIndex of ReportsPreviewPageController', ex);
    }
  }
});
