﻿Ext.define('AssetManagement.base.modules.reportFramework.controller.dialog.BaseReportCostsDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	  'AssetManagement.customer.helper.OxLogger',
       'AssetManagement.customer.modules.reportFramework.mapping.BoToClassMapping'
    ],


    _parentSuccessCallback: null,
    _parentCancelCallback: null,
    _documentTypesStore: null,

    onDialogReady: function () {

        this._parentSuccessCallback = this._currentArguments.successCallback;
        this._parentCancelCallback = this._currentArguments.cancelCallback;

        var preparedDocTypeDialogStore = this.mapDocTypeToReportTypes();

        var reportTypeDialogGrid = Ext.getCmp('reportCostsPanel');
        if (reportTypeDialogGrid && preparedDocTypeDialogStore) {
            reportTypeDialogGrid.setStore(preparedDocTypeDialogStore);
        }
    },

    onCancelPrintReports: function () {
        try {
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelPrintReports of BaseReportCostsDialogController', ex);
        }

    },

    onReportTypeSelection: function (row, record, index, eOpts) {
        try {
            var me = this;
            var summaryReport = Ext.getCmp('summaryReport');
            var singleReport = Ext.getCmp('singleEntryReport');
            var typeOfOpReport = summaryReport.checked ? 'SR' : 'ER';

            var typeOfReport = record.get('id');
            if (typeOfReport && typeOfReport === '0') {
                this._parentSuccessCallback(true, typeOfOpReport);
            } else if (typeOfReport && typeOfReport === '1') {
                this._parentSuccessCallback(false, typeOfOpReport);
            }
            this.dismiss();
            
            return undefined;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelPrintReports of BaseReportCostsDialogController', ex);
        }
    },

    mapDocTypeToReportTypes: function () {
        try {

            var reportTypesStore = this.createStore();

            //var reportTypeMapper = AssetManagement.customer.modules.reportFramework.mapping.BoToClassMapping.getInstance();

            reportTypesStore.add({
                'id': '0',
                'reportTypeDescription': Locale.getMsg('reportWithGeneralCosts')
            });

            reportTypesStore.add({
                'id': '1',
                'reportTypeDescription': Locale.getMsg('reportWithoutGeneralCosts')
            });

            return reportTypesStore;


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapDocTypeToReportTypes of BaseReportCostsDialogController', ex);
        }
    },

    createStore: function () {
        return Ext.create('Ext.data.Store', {
            storeId: 'documentTypesStore',
            fields: ['id', 'reportTypeDescription']
        });
    }

});