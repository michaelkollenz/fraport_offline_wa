﻿Ext.define('AssetManagement.base.modules.reportFramework.controller.dialog.BaseReportTypeDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	  'AssetManagement.customer.helper.OxLogger',
       'AssetManagement.customer.modules.reportFramework.mapping.BoToClassMapping'
    ],


    _parentSuccessCallback: null,
    _parentCancelCallback: null,
    _documentTypesStore : null,
    
    onDialogReady: function () {

        this._parentSuccessCallback = this._currentArguments.successCallback;
        this._parentCancelCallback = this._currentArguments.cancelCallback;
        this._documentTypesStore = this._currentArguments.arguments[0].documentTypes;

        var preparedDocTypeDialogStore = this.mapDocTypeToReportTypes();

        var reportTypeDialogGrid = Ext.getCmp('reportTypePanel');
        if (reportTypeDialogGrid && preparedDocTypeDialogStore) {
            reportTypeDialogGrid.setStore(preparedDocTypeDialogStore);
        }
    },

    onCancelPrintReports: function () {
        try{
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelPrintReports of BaseReportTypeDialogController', ex);
        }

    },

    onReportTypeSelection: function (row, record, index, eOpts) {
        try{
            var me = this;

            var typeOfReport = record.get('id');
            if (typeOfReport && this._documentTypesStore) {
                this._documentTypesStore.each(function (item) {
                    if (item.get('objectType') === typeOfReport) {
                        me._parentSuccessCallback(item);
                        me.dismiss();
                    }
                })

            }
            return undefined;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelPrintReports of BaseReportTypeDialogController', ex);
        }
    },

    mapDocTypeToReportTypes: function () {
        try {

            var reportTypesStore = this.createStore();

            var reportTypeMapper = AssetManagement.customer.modules.reportFramework.mapping.BoToClassMapping.getInstance();
            
            var dataToMapp = this._documentTypesStore;
            dataToMapp.each(function (item) {
                switch (item.get('objectType')) {
                    case reportTypeMapper.getOrderReportLayout():
                        reportTypesStore.add({
                            'id': item.get('objectType'),
                            'reportTypeDescription': Locale.getMsg('orderReport')
                        });
                        break;
                    case reportTypeMapper.getScheduleReportLayout():
                        reportTypesStore.add({
                            'id': item.get('objectType'),
                            'reportTypeDescription': Locale.getMsg('scheduleReport')
                        });
                        break;
                    case reportTypeMapper.getServiceReportLayout():
                        reportTypesStore.add({
                            'id': item.get('objectType'),
                            'reportTypeDescription': Locale.getMsg('checklistReport')
                        });
                        break;
                    case reportTypeMapper.getServiceReportLayout():
                        reportTypesStore.add({
                            'id': item.get('objectType'),
                            'reportTypeDescription': Locale.getMsg('serviceReport')
                        });
                        break;
                }

            });

            return reportTypesStore;


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside mapDocTypeToReportTypes of BaseReportTypeDialogController', ex);
        }
    },

    createStore: function () {
        return Ext.create('Ext.data.Store', {
            storeId: 'documentTypesStore',
            fields: ['id','reportTypeDescription']
        });
    }

});