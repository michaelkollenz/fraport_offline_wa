﻿Ext.define('AssetManagement.base.modules.reportFramework.controller.dialog.BaseReportSignatureDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	  'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.manager.CustomerManager'
    ],


    _emailAddresses: [],
    _emailAddressesStore: null,
    _signatureData: null,
    _parentSuccessCallback: null,
    _parentErrorCallback: null,
    _reportId: null,
    _emailDetails: null,
    _signCounter: null,
    _customerDefaultEmail: null,
    _mymContactPerson: null,
    _assignment: null,


    //public
    requestDialog: function (argumentsObject, dialogRequestCallback, dialogRequestCallbackScope, onHiddenCallback) {
        try {

            this.getViewModel().set('isMailModeActive', false);

            this._dialogRequestCallback = dialogRequestCallback;
            this._dialogRequestCallbackScope = dialogRequestCallbackScope;
            this._currentArguments = argumentsObject;
            this._onHiddenCallback = onHiddenCallback;

            this._signatureData = this.getCurrentArguments().arguments[0].signatureInstance;
            this._parentSuccessCallback = this.getCurrentArguments().successCallback;
            this._parentErrorCallback = this.getCurrentArguments().cancelCallback;
            this._signCounter = this.getCurrentArguments().arguments[0].signatureCounter;

            this._reportId = this.getCurrentArguments().arguments[0].itemId;
            this._emailDetails = this.getCurrentArguments().arguments[0].emailDetails;
            this._customerDefaultEmail = this.getCurrentArguments().arguments[0].customerEmail;
            this._mymContactPerson = this.getCurrentArguments().arguments[0].mymContactPerson;
            this._assignment = this.getCurrentArguments().arguments[0].assignment;
            //clear addresses
            this._emailAddressesStore = this.createStore(this._customerDefaultEmail);
            this._emailAddresses = [];

            this.getView().resetViewState();

            //do not use update to avoid it's defering
            this.refreshData(true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestDialog of ' + this.getClassName(), ex);
            this.errorOccurred();
        }
    },

    getSignatureDetails : function(){
        return this._signatureData;
    },

    getReportId : function(){
        return this._reportId;
    },

    getAssignment : function(){
        return this._assignment;
    },

    onOkButtonClicked: function (button, e, eOpts) {
        try {
            //hide keyboard
            document.activeElement.blur();

          //take the signature
          var canvasContainer = Ext.ComponentQuery.query('#reportsSignatureDialog')[0];
            //AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;
            var myModel = this.getViewModel();
            var parentSuccessCallback = me._parentSuccessCallback;
            var parentErrorCallback = me._parentErrorCallback;

            var emailAdresses = this._emailAddressesStore ? this._emailAddressesStore.getData() : null;
            myModel.set('isMailModeActive', false);
            var signature = canvasContainer.getController().getCurrentContentAsDataUrl();
            var contactPerson = myModel.get('contactPerson');
            this.dismiss();
            parentSuccessCallback(signature, emailAdresses, contactPerson, this._signCounter);
        } catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOkButtonClicked of BaseReportSignatureDialogController', ex);
        }
    },

    addEmailAddress: function (button, e, eOpts) {
        try {
            //hide keyboard
            document.activeElement.blur();

            //add address to return table
            var emailAddress = Ext.ComponentQuery.query("#signatureDialogEmailTextField")[0].getValue();
            //this.showAddressTable();

            if (!this.validateEmail(emailAddress)) {
                notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('emailAddressIsInvalid'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                return;
            }

            if (!this._emailAddressesStore) {
                this._emailAddressesStore = this.createStore();
            }

            if (this._emailDetails && this._emailDetails['numberOfEmailsAllowed'] && parseInt(this._emailDetails['numberOfEmailsAllowed']) !== 0) {
                var maxNumberOfEmails = parseInt(this._emailDetails['numberOfEmailsAllowed']);
                if (maxNumberOfEmails <= this._emailAddressesStore.getCount()) {
                    notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('maximumCountOfEmailAddressesReached'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    return;
                }
            }

            this._emailAddressesStore.add({
                'email': emailAddress
            });

            Ext.ComponentQuery.query('#emailAddressesPanel')[0].setStore(this._emailAddressesStore);

            Ext.ComponentQuery.query('#signatureDialogEmailTextField')[0].setValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addEmailAddress of BaseReportSignatureDialogController', ex);
        }
    },

    validateEmail : function (email) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
    },

    createStore: function (defaultCustomerEmail) {
        var me = this;

        var mailStore = Ext.create('Ext.data.Store', {
            storeId: 'emailAdressesStore',
            fields: ['email']
        });

        callback = function (customers) {
            try{
                return mailStore;
            } catch (ex) {

            }
        }

        if(defaultCustomerEmail){
            mailStore.add({
                'email': defaultCustomerEmail
            })
        }

        return mailStore;
    },

    deleteEmailAddress: function (row, record, index, eOpts) {
        try {
            if (this._emailAddressesStore && record) {
                this._emailAddressesStore.remove(record);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteEmailAddress of BaseReportSignatureDialogController', ex);
        }
    },

    onRemoveSignature: function (row, record, index, eOpts) {
        try {
            //hide keyboard
            document.activeElement.blur();

            var canvasContainer = Ext.ComponentQuery.query('#reportsSignatureDialog')[0];
            if (canvasContainer) canvasContainer.getController().clear();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onRemoveSignature of BaseReportSignatureDialogController', ex);
        }
    },

    onCancelButtonClicked: function () {
        try {
            //hide keyboard
            document.activeElement.blur();

            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelButtonClicked of BaseReportSignatureDialogController', ex);
        }
    },

    onNextButtonClicked : function(){
        try {
            //hide keyboard
            document.activeElement.blur();

            var myModel = this.getViewModel();

            //take the signature
            var canvasContainer = Ext.ComponentQuery.query('#reportsSignatureDialog')[0];
            var signature = canvasContainer.getController().getCurrentContentAsDataUrl();

            var signaturePlainText = null;
            signaturePlainText = this.checkIfPlainTextProvided();

            var signatureRequired = true;

            if(this.getView().onPlainTextSignatureIsNeeded(this._signatureData)['show'] && !signaturePlainText) {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('signatureHasToBeProvideAsText'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                return;
            }

            if (signatureRequired && !signature) {
                var typeOfSignature = 'customer';
                var message = ""
                switch (typeOfSignature) {
                    case 'customer':
                        message = Locale.getMsg('customerHasToSign');
                        break;
                    case 'engineer':
                        message = Locale.getMsg('engineerHasToSign');

                }
                notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(message, false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            } else{
                myModel.set('isMailModeActive', true);
                myModel.set('providedSignature', signature);
                myModel.set('contactPerson', signaturePlainText);
                this.refreshView();
            }
        }  catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNextButtonClicked of BaseReportSignatureDialogController', ex);
        }
    },

    checkIfPlainTextProvided: function () {
        try{
            var signPlainTextBox = Ext.ComponentQuery.query('#signaturePlainTextX')[0];
            if(signPlainTextBox && signPlainTextBox.isVisible() && signPlainTextBox.getValue()){
                return signPlainTextBox.getValue();
            } else {

                return false;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelButtonClicked of BaseReportSignatureDialogController', ex);
        }
    }
});