﻿Ext.define('AssetManagement.base.modules.reportFramework.controller.dialog.BaseBuildReportDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',


    requires: [
	  'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.manager.OrderManager',
      'AssetManagement.customer.manager.OrderReportManager',
      'AssetManagement.customer.modules.reportFramework.PdfCreator'
	  //'AssetManagement.customer.manager.ReportManager'
    ],

    //private
    _reportType: 0,
    _reportsToPrint: [],

    _orderReportRequest: null,
    _serviceReportRequest: null,
    _scheduleReportRequest: null,
    _addReportToOrder: false,
    _builder: null,

    //process management fields
    _registeredCallbacksOnBuilder: false,
    _processRunning: true,
    _curProcessPercent: 0,
    _curProcessStepRange: 0,
    _curProcessStepPercentage: 0,
    _waitingForBuilder: false,
    _cancelWorkRequested: false,
    _errorOccured: false,

    //@override
    //public
    requestCancelation: function () {
        try {
            if (this._processRunning && !this._cancellationRequested) {
                if (this._waitingForBuilder) {
                    this._builder.requestCancelation();
                }

                this._cancelWorkRequested = true;
                this.onWorkerCancelled();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestCancelation of BaseBuildReportDialogController', ex);
        }
    },

    //protected
    //@override
    beforeInitialization: function (argumentsObject) {
        try {
            var pdfCreator = AssetManagement.customer.modules.reportFramework.PdfCreator.getInstance();
            var reportsBuilder = pdfCreator._reportsBuilder;

            argumentsObject.title = Locale.getMsg('dataProcessing');
            argumentsObject.worker = reportsBuilder;
            argumentsObject.startMethod = pdfCreator.createReports;
            argumentsObject.differingEntryEntity = pdfCreator;

            //extend the arguments by an anonymous callback and a callbackscope, which the startMethod want's to be not undefined to start
            //these parameters are used for usual way of returning the result (not the work finished event way)
            argumentsObject.arguments.push(function () { });
            argumentsObject.arguments.push(this);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseBuildReportDialogController', ex);
        }
    }
});