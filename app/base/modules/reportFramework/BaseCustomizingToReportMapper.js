﻿Ext.define('AssetManagement.base.modules.reportFramework.BaseCustomizingToReportMapper', {

mapping : [
	// installation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE1' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '3'
	},
	// pickUp report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE2' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '5'
	},
	// service call report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE3' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '7'
	},
	// bedstore replenishment report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE4' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '8'
	}, 
	// on-site installation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE5' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '10'
	},
	// on-site pick up report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE6' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '12'
	},
	// on-site service call report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '14'
	},
	// CS operation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '1'
	},
	// CS china operation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '2'
	},
	// CS UK operation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '4'
	},
	// CS US operation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '6'
	}, 
	// CS POLAND operation report
	{ 
		bo : 'BUS2007',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '15'
	},
	// CS order report
	{ 
		bo : 'BUS2088',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '16'
	},
	// CS china order report
	{ 
		bo : 'BUS2088',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '17'
	},
	// CS UK order report
	{ 
		bo : 'BUS2088',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '18'
	},
	// CS US order report
	{ 
		bo : 'BUS2088',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '19'
	},
	// CS POLAND order report
	{ 
		bo : 'BUS2088',
		fieldName1:  'AUART',
		filedValue1: 'ZRPE',
		fieldName2:  'ILART',
		filedValue2: 'RE7' ,
		fieldName3:  '',
		filedValue3: '',
		fieldName4:  '',
		filedValue4: '',
		reportId: '20'
	}],

	inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.CustomizingToReportMapper');
            }
            return this._instance;
        }
    }
});