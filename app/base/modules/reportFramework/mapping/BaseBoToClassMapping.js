﻿Ext.define('AssetManagement.base.modules.reportFramework.mapping.BaseBoToClassMapping', {
    

    _orderReportLayout: 'BUS2007',
    _scheduleReportLayout : 'PMX1111',
    _serviceReportLayout  : 'BUS2006',
    _operationReportLayout: 'OPV01',
    _installtionReportLayout: 'INS2007',
    _pickUpReportLayout: 'PUP2007',
    _serviceCallReportLayout: 'CAL2007',


    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.mapping.BoToClassMapping');
            }
            return this._instance;
        }
    },

    getOrderReportLayout : function(){
        return this._orderReportLayout;
    },

    getScheduleReportLayout: function () {
        return this._scheduleReportLayout;
    },

    getServiceReportLayout: function () {
        return this._serviceReportLayout;
    },

    getOperationReportLayout: function () {
        return this._operationReportLayout;
    },

    getInstallationReportLayout: function () {
        return this._installtionReportLayout;
    },

    getPickUpReportLayout: function () {
        return this._pickUpReportLayout;
    },

    getServiceCallReportLayout: function () {
        return this._serviceCallReportLayout;
    }


});