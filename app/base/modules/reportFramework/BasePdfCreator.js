Ext.define('AssetManagement.base.modules.reportFramework.BasePdfCreator', {
    requires: [
	    'AssetManagement.customer.controller.EventController',
	    'AssetManagement.customer.helper.LibraryHelper',
	    'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.OxLogger'
        
    ],

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.PdfCreator');
            }

            return this._instance;
        }
    },

    
    _libraryLoaded: false,
    _libraryLoadingRequestIsPending: false,

    _reportsBuilder: null,

    constructor: function () {
        this.callParent(arguments);

        try {
            this.loadPdfLibrary();

            this._reportsBuilder = Ext.create('AssetManagement.customer.modules.reportFramework.ReportFramework');

           
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of reportFramework-BasePdfCreator', ex);
        }
    },

    //public
    createReports: function (reportsRequest, callback, callbackScope) {
        var retval = null;

        try {
            if (reportsRequest) {
                retval = this._reportsBuilder;

                //if the library is still being loaded, wait for it (retry in 250ms)
                if (this._libraryLoadingRequestIsPending) {
                    Ext.defer(this.createReports, 250, this, [reportsRequest, callback, callbackScope]);
                } else if (this._libraryLoaded === false) {
                    //the library loading failed, return undefined/null
                    retval = null;

                    callback.call(callbackScope ? callbackScope : this, undefined);
                } else {
                    var me = this;

                    var innerCallback = function (pdfFile) {
                        try {
                            callback.call(callbackScope ? callbackScope : me, pdfFile);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createReport of BasePdfCreator', ex);
                            callback.call(callbackScope ? callbackScope : me, undefined);
                        }
                    };

                    var eventId = this._reportsBuilder.prepareDataAndDocuments(reportsRequest);

                    if (eventId > 0) {
                        if (callback)
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, innerCallback);
                    } else {
                        retval = null;

                        if (callback)
                            callback.call(callbackScope ? callbackScope : this, undefined);
                    }
                }
            } else if (callback) {
                callback.call(callbackScope ? callbackScope : this, undefined);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createReport of reportFramework-BasePdfCreator', ex);

            retval = null;

            if (callback)
                callback.call(callbackScope ? callbackScope : this, undefined);
        }

        return retval;
    },

    //private
    loadPdfLibrary: function (callback) {
        try {
            this._libraryLoadingRequestIsPending = true;

            var coreLibLoaded = false;
            var fontsLoaded = false;
            var errorOccured = false;

            var afterLoadingStepReturnedAction = function () {
                try {
                    if (errorOccured) {
                        this._libraryLoadingRequestIsPending = false;

                        if (callback)
                            callback.call(this);
                    } else if (coreLibLoaded && fontsLoaded) {
                        this._libraryLoadingRequestIsPending = false;
                        this._libraryLoaded = true;

                        if (callback)
                            callback.call(this);
                    }
                } catch (ex) {
                    this._libraryLoadingRequestIsPending = false;

                    if (callback)
                        callback.call(this);
                }
            };

            var coreLibraryLoadCallback = function (success) {
                try {
                    if (success === true) {
                        coreLibLoaded = true;
                        AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/vfs_fonts.js', true, fontsLoadCallback, this);
                    } else {
                        errorOccured = true;
                        AssetManagement.customer.helper.OxLogger.logMessage('Failure loading pdf core library');
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPdfLibrary of reportFramework-BasePdfCreator', ex);
                    errorOccured = true;
                } finally {
                    afterLoadingStepReturnedAction.call(this);
                }
            };

            var fontsLoadCallback = function (success) {
                try {
                    if (success === true) {
                        fontsLoaded = true;
                    } else {
                        errorOccured = true;
                        AssetManagement.customer.helper.OxLogger.logMessage('Failure loading pdf fonts library');
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPdfLibrary of reportFramework-BasePdfCreator', ex);
                    errorOccured = true;
                } finally {
                    afterLoadingStepReturnedAction.call(this);
                }
            };

            //load the library files asynchronous
            AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/pdfmake.js', true, coreLibraryLoadCallback, this);
            //AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/vfs_fonts.js', true, fontsLoadCallback, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPdfLibrary of reportFramework-BasePdfCreator', ex);
        }
    }
});