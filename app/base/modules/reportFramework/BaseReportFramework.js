Ext.define('AssetManagement.base.modules.reportFramework.BaseReportFramework', {

    mixins: ['Ext.mixin.Observable'],

    requires: [
      'AssetManagement.customer.controller.EventController',
      'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.manager.OrderManager',
      'AssetManagement.customer.manager.OrderReportManager',
      'AssetManagement.customer.modules.reportFramework.CustomizingToReportMapper',
      'AssetManagement.customer.modules.reportFramework.ReportPdfMaker',
      'AssetManagement.customer.modules.reportFramework.dataHandler.ChecklistReportDataHandler',
      'AssetManagement.customer.modules.reportFramework.dataHandler.DeliveryReportDataHandler',
      'AssetManagement.customer.modules.reportFramework.dataHandler.OrderReportDataHandler',
      'AssetManagement.customer.modules.reportFramework.manager.DocumentDataManager',
      'AssetManagement.customer.modules.reportFramework.manager.DocumentUsageManager',
      'AssetManagement.customer.modules.reportFramework.model.DocumentUsage'
    ],


    //status fields
    _currentRequest: null,
    _completeEventId: NaN,
    _currentPdfObject: null,
    _currentReport: null,
    _curProcessPercent: NaN,
    _cancelRequested: false,
    _errorOccured: false,
    _lastErrorMessage: '',
    _processRunning: false,

    //data fields
    _objectsToPrint: [],
    _typeOfReport: null,
    _reportsCount: null,
    _reportCounter: null,
    _preparedObjectsAndDocuments: [],

    //reportFramework maintanance fields
    _reportLanguage:                null,
    _parentSuccessCallback:         null,
    _parentErrorSuccessCallback:    null,
    _signatureType:                 null,
    _isVerticalView:                      null,
    _enhancementPoint:              null,
    _logosArray:                 [],
    _preventLangChange:             true,

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.ReportFramework');
            }
            return this._instance;
        }
    },

    constructor: function (config) {
        try {
            this.mixins.observable.constructor.call(this, config);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseReportFramework', ex);
        }
    },

    //public
    //Method responsible for creating the layouts and and displaying the reports.
    //Method has a workaround for displaying the reports on iOS
    createReports: function (config, objectsToPrint, parentSuccessCallback, parentErrorCallback) {
        try {
            this._parentSuccessCallback = parentSuccessCallback;
            this._parentErrorSuccessCallback = parentErrorCallback;

            if(config) {
                this._reportLanguage = config['reportLanguage'];
                this._enhancementPoint = config['actionAfterReportCreation'];
                this._preventLangChange = config['preventLangChange'];
                this._signatureType = config['isOneSignatureForAllDocuments'];
                this._isVerticalView = config['isVerticalView'];
                this._signedObjects = config['signedObjets'];
            } else {
                this._parentErrorSuccessCallback();
                return;
            }

            var me = this;

            if(!this._reportLanguage || !(this._reportLanguage && this._reportLanguage !== Locale.getCurrentLanguage())){
                this._reportLanguage = Locale.getCurrentLanguage();
            }

            // workaround for damage/infection images in reports - PDFMAKE bug - instead of pics we got '$$$pdfmake1$$$'
            // if(this._signedObjects && this._signedObjects.length > 0) {
            //     me._preparedObjectsAndDocuments = AssetManagement.customer.modules.reportFramework.ReportPdfMaker.getInstance().preparePdfDocuments(me._objectsToPrint, objectsType, me._reportLanguage, opCosts, typeOfOpReport);

            //     var isiOS = me.getTargetPage();
            //     if(isiOS) {
            //         AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_REPORTS_PREVIEW_PAGE_IOS, {
            //             reportsData: me._preparedObjectsAndDocuments,
            //             documentType: docsType,
            //             masterSuccessCallback: frameworkSuccessCallback,
            //             signingType: me._signatureType,
            //             enhancement: me._enhancementPoint
            //         });
            //     } else {
            //         AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_REPORTS_PREVIEW_PAGE, {
            //             reportsData: me._preparedObjectsAndDocuments,
            //             documentType: docsType,
            //             masterSuccessCallback: frameworkSuccessCallback,
            //             signingType: me._signatureType,
            //             enhancement: me._enhancementPoint
            //         });
            //     }
            //     return;
            // } else {
                this.startProcess(objectsToPrint);
            //}

            //this.retrieveCustomizingData(objectsToPrint, parentSuccessCallback, reportLanguage);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createReports of BaseReportFramework', ex);
        }
    },

    //private
    startProcess: function (objectsToPrint){//docsType, objects, frameworkSuccessCallback, reportLanguage) {
        try{
            var me = this;

            var successCallback = function (dataAndReports) {
                try {
                    if (dataAndReports) {
                        // needed to determine the type of view
                        var oneOfLayoutTypes = dataAndReports[0]['object']['mainObject'].get('layoutType');

                        var isMobile = me.getTargetPage();
                        if(true) {//TODO isMobile
                            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_REPORTS_PREVIEW_PAGE_IOS, {
                                reportsData: dataAndReports,
                                documentType: oneOfLayoutTypes,
                                masterSuccessCallback: me._parentSuccessCallback,
                                signingType: me._signatureType,
                                enhancement: me._enhancementPoint,
                                reportLang: me._reportLanguage,
                                isViewLayout: me._isVerticalView
                            });
                        } else {
                            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_REPORTS_PREVIEW_PAGE, {
                                reportsData: dataAndReports,
                                documentType: oneOfLayoutTypes,
                                masterSuccessCallback: me._parentSuccessCallback,
                                signingType: me._signatureType,
                                enhancement: me._enhancementPoint,
                                reportLang: me._reportLanguage,
                                isViewDisplay: me._isVerticalView
                            });
                        }
                    } else {
                        me.errorOccured(Locale.getMsg('generalErrorCreatingReportsOccured'));
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildReports-successCallback of BaseReportFramework', ex);
                }
            };

            var cancelCallback = function () {
                try {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('reportsCreationCancelled'), true, 1);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startProcess-cancelCallback of BaseReportFramework', ex);
                }
            };

            var request = {
                dataToPrint: objectsToPrint,
                reportLanguage: this._reportLanguage
            };

            var builderDialogArguments = {
                successCallback: successCallback,
                cancelCallback: cancelCallback,
                owner: this,
                arguments: [request]
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.BUILD_REPORT_DIALOG, builderDialogArguments);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startProcess of BaseReportFramework', ex)
        }
    },

     // public
    // First method called by the progress dialog to gather the necessery data and
    prepareDataAndDocuments: function (request) {
        try {
            var me = this;
            this._processRunning = true;
            this.reportWorkProgress(5, Locale.getMsg('obtainingCustomizing'));

            var objectsToProcessAndPrint = request.dataToPrint;
            this._reportLanguage = request.reportLanguage;

            this.retrieveCustomizingData(objectsToProcessAndPrint);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareDataAndDocuments of BaseReportFramework', ex);
        }
    },

    // private
    // retrieve customizing data from IndexedDB tables:
    // document's bo / target layout/ available languages etc.
    retrieveCustomizingData: function (objectsToProcessAndPrint) {
        try{
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var callback = function(documentDefinitions){
                try {
                    if (documentDefinitions) {
                        me.reportWorkProgress(25, Locale.getMsg('obtainingData'));

                        me.extractLogos(documentDefinitions);

                        me.assignDetailsToObject(objectsToProcessAndPrint, documentDefinitions);
                    } else {
                       me.errorOccured(Locale.getMsg('errorOccuredDuringObtainingCustomizingData'));
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveCustomizingData-callback of BaseReportFramework', ex)
                }
            }

            var eventId = AssetManagement.customer.modules.reportFramework.manager.DocumentUsageManager.getDocumentsTypes(false);
            if (eventId > -1) {
                eventController.registerOnEventForOneTime(eventId, callback);
            }
            else {
                this.errorOccured(Locale.getMsg('errorOccuredDuringObtainingCustomizingData'));
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveCustomizingData of BaseReportFramework', ex);
        }
    },

    assignDetailsToObject: function (objects, documentDefinitions) {
        try {
            var me = this;
            var customizingDefinitions = documentDefinitions;

            // assign document definition to each and every object
            var objectsWithAssignedDefinitions = this.assignDetailsBasedOnObjectType(objects, customizingDefinitions);

            if(objects.length !== objectsWithAssignedDefinitions.length) {
                this.errorOccured(Locale.getMsg('generalErrorCreatingReportsOccured'));
            } else {
                this.loadReportDependantData(objectsWithAssignedDefinitions);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDetailsToObject of BaseReportFramework', ex);
        }
    },

    loadReportDependantData : function(objectsWithAssignedDefinitions) {
        try {
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var counter = 0;
            var amountObjToProcess = objectsWithAssignedDefinitions.length;

            var allDataLoaded = function() {
                try {
                    me.reportWorkProgress(80, Locale.getMsg('obtainingData'));
                    me._preparedObjectsAndDocuments = AssetManagement.customer.modules.reportFramework.ReportPdfMaker.getInstance().preparePdfDocuments(me._objectsToPrint, me._reportLanguage, me._logosArray);
                    me.returnResult();
                    me._objectsToPrint = [];
                    me._logosArray = [];
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside allDataLoaded of BaseReportFramework', ex);
                }
            };

            var loadDependantObjectData = function () {
                try {
                    if(counter < amountObjToProcess) {

                        var continueLoadingDependantData = function(objectWithDependantData) {
                            try {
                                if (objectWithDependantData) {
                                    me._objectsToPrint.push(objectWithDependantData);
                                    counter++;
                                    return loadDependantObjectData();
                                } else {
                                   me.errorOccured();
                                }

                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside pushElementToAnArray of BaseReportFramework', ex);
                            }
                        };

                        var reportEventId = me.retrieveReportDataBasedOnType(objectsWithAssignedDefinitions[counter]);
                        me.reportWorkProgress(10, Locale.getMsg('obtainingData'));

                        if (reportEventId > -1) {
                            eventController.registerOnEventForOneTime(reportEventId, continueLoadingDependantData, me);
                            me.reportWorkProgress(30, Locale.getMsg('obtainingData'));
                        } else {
                            me.errorOccured();
                        }
                    } else {
                        allDataLoaded();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside processObject of BaseReportFramework', ex);
                }
            };

            loadDependantObjectData();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadReportDependantData of BaseReportFramework', ex);
        }
    },



    // private
    // method assigns details like: count of signatures, languages in which report can be created etc.
    assignDetailsBasedOnObjectType: function (objects, customDocDefinitions) {
        var retval = [];

        try {
            var copyOfObjects = $.extend(true, [], objects);
            // new code ///
            for(var z=0; z < copyOfObjects.length; z++) {
                var singleObject = copyOfObjects[z];
                var sapObjectType = singleObject.self.SAP_OBJECT_TYPE;

                var relevantCustomizingArray = [];
                if(customDocDefinitions && customDocDefinitions.getCount() > 0) {
                     customDocDefinitions.each(function(custDocDef){
                        if(sapObjectType === custDocDef.get('objectType')){
                            relevantCustomizingArray.push(custDocDef);
                        }
                    });
                } else {
                    this.errorOccured();
                }

                var maxCounter = 0;

                for(var i = 0; i < relevantCustomizingArray.length; i++) {
                    var counter = 0;
                    var fieldName1 = relevantCustomizingArray[i].get('fieldName1') ? relevantCustomizingArray[i].get('fieldName1').toLowerCase() : null;
                    var fieldValue1 = relevantCustomizingArray[i].get('objectParam1');

                    var fieldName2 = relevantCustomizingArray[i].get('fieldName2') ? relevantCustomizingArray[i].get('fieldName2').toLowerCase() : null;
                    var fieldValue2 = relevantCustomizingArray[i].get('objectParam2');

                    var fieldName3 = relevantCustomizingArray[i].get('fieldName3') ? relevantCustomizingArray[i].get('fieldName3').toLowerCase() : null;
                    var fieldValue3 = relevantCustomizingArray[i].get('objectParam3');

                    var fieldName4 = relevantCustomizingArray[i].get('fieldName4') ? relevantCustomizingArray[i].get('fieldName4').toLowerCase() : null;
                    var fieldValue4 = relevantCustomizingArray[i].get('objectParam4');

                    if(fieldName1) {
                        counter++
                    }
                    if(fieldName2) {
                        counter++
                    }
                    if(fieldName3) {
                        counter++
                    }
                    if(fieldName4) {
                        counter++
                    }

                    if(counter > maxCounter) {
                        maxCounter = counter;
                    }
                }

                for(var j = 0; j < relevantCustomizingArray.length; j++){
                    var newCounter = 0;
                    var fieldName1 = relevantCustomizingArray[j].get('fieldName1') ? relevantCustomizingArray[j].get('fieldName1').toLowerCase() : null;
                    var fieldValue1 = relevantCustomizingArray[j].get('objectParam1');

                    var fieldName2 = relevantCustomizingArray[j].get('fieldName2') ? relevantCustomizingArray[j].get('fieldName2').toLowerCase() : null;
                    var fieldValue2 = relevantCustomizingArray[j].get('objectParam2');

                    var fieldName3 = relevantCustomizingArray[j].get('fieldName3') ? relevantCustomizingArray[j].get('fieldName3').toLowerCase() : null;
                    var fieldValue3 = relevantCustomizingArray[j].get('objectParam3');

                    var fieldName4 = relevantCustomizingArray[j].get('fieldName4') ? relevantCustomizingArray[j].get('fieldName4').toLowerCase() : null;
                    var fieldValue4 = relevantCustomizingArray[j].get('objectParam4');

                    if(fieldName1 && (newCounter < maxCounter) && (singleObject.get(fieldName1) && (singleObject.get(fieldName1) === fieldValue1))) {
                        newCounter++;

                        if(fieldName2 && (newCounter < maxCounter) && (singleObject.get(fieldName2) && (singleObject.get(fieldName2) === fieldValue2))) {
                            newCounter++;
                            if(fieldName3 && (newCounter < maxCounter) && (singleObject.get(fieldName3) && (singleObject.get(fieldName3) === fieldValue3))) {
                                newCounter++;
                                if(fieldName4 && (newCounter < maxCounter) && (singleObject.get(fieldName4) && (singleObject.get(fieldName4) === fieldValue4))) {
                                    newCounter++;
                                    if(newCounter === maxCounter)
                                        singleObject.set('layoutType', relevantCustomizingArray[j]);
                                }else {
                                    if(newCounter === maxCounter)
                                        singleObject.set('layoutType', relevantCustomizingArray[j]);
                                }
                            } else {
                                if(newCounter === maxCounter)
                                    singleObject.set('layoutType', relevantCustomizingArray[j]);
                            }
                        } else {
                            if(newCounter === maxCounter)
                                singleObject.set('layoutType', relevantCustomizingArray[j]);
                        }
                    } else {
                        if(relevantCustomizingArray.length === 1)
                            singleObject.set('layoutType', relevantCustomizingArray[0]);
                    }

                }

                retval.push(singleObject);

            }
            /// end of new code ///
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDetailsBasedOnObjectType of BaseReportFramework', ex);
        }

        return retval;
    },

    //private
    extractLogos: function(customizingData) {
        try {
            var me = this;

            var logosNames = AssetManagement.customer.modules.reportFramework.manager.DocumentUsageManager.logos;
            for(var i=0; i < logosNames.length; i++) {
                customizingData.each(function(customizingDefinition){
                    if(customizingDefinition.get('logoName') && customizingDefinition.get('logoName') === logosNames[i]){
                        me._logosArray.push(customizingDefinition);
                    }
                });
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractLogos of BaseReportFramework', ex);
        }
    },

    // private
    // method determines if application has been opened in iOS environment
    getTargetPage: function(){
        var retval = false;

        try {
            var userAgentInfo = AssetManagement.customer.utils.UserAgentInfoHelper.getFullInfoObject();
            var isMobile = userAgentInfo.OS === 'iOS';
            if(!isMobile){
                isMobile = userAgentInfo.OS === 'Android';
            }

            retval = isMobile;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getViewOfRequestedMode of BaseReportFramework', ex);
        }

        return retval;
    },

    // private
    // retrieve relevant data based on the passed object
    retrieveReportDataBasedOnType : function(object){
        try {
            var determinedLayout = object.get('layoutType');

            // check if layoutType was determine
            if(!determinedLayout){
                this.errorOccured(Locale.getMsg('layoutForThisReportCouldNotBeDeterminedBecauseOfCustomizingIssue'));
                return;
            }

            switch(determinedLayout.get('reportId')) {

                // STANDARD SCENARIO STARTS
                case 1:
                    return this.retrieveOrderReportData(object);
                    break;
                case 3:
                    return this.retrieveChecklistReportData(object);
                    break;
              case 5:
                return this.retrieveDeliverylistReportData(object);
                break;
                // STANDARD SCENARIO ENDS
                default:
                    this.errorOccured(Locale.getMsg('UnknownTypeOfReportRequested'));
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveReportDataBasedOnType of BaseReportFramework', ex);
        }
    },

    //private
    retrieveOrderReportData: function (object) {
        var retval = -1;
        try {

            retval = AssetManagement.customer.modules.reportFramework.dataHandler.OrderReportDataHandler.getInstance().obtainOrder(object);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveOperationReportData of BaseReportFramework', ex);
        }
        return retval;
    },

    //private
    retrieveChecklistReportData: function (object) {
        var retval = -1;
        try {

            retval = AssetManagement.customer.modules.reportFramework.dataHandler.ChecklistReportDataHandler.getInstance().obtainChecklistData(object);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveOperationReportData of BaseReportFramework', ex);
        }
        return retval;
    },

  //private
  retrieveDeliverylistReportData: function (object) {
    var retval = -1;
    try {

      retval = AssetManagement.customer.modules.reportFramework.dataHandler.DeliveryReportDataHandler.getInstance().obtainOrder(object);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveOperationReportData of BaseReportFramework', ex);
    }
    return retval;
  },
    ///////////////////////////////////// Cancelable progress dialog helpers //////////////////////////////////////////////////////////

    //private
    returnResult: function () {
        try {
            this.reportWorkFinished();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside returnResult of BaseOrderReportBuilder', ex);
            this.errorOccured();
        }
    },

    //private
    reportWorkFinished: function () {
        try {
            this.fireEvent('workFinished', this._preparedObjectsAndDocuments);
        } catch (ex) {
            this.errorOccured();

            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkFinished of BaseOrderReportBuilder', ex);
        }
    },

    //private
    reportWorkProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.reportProgress(curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkProgress of BaseReportFramework', ex);
            this.errorOccured();
        }
    },

    //private
    reportProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.fireEvent('progressChanged', curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportProgress of BaseReportFramework', ex);
            this.errorOccured();
        }
    },

    //private
    errorOccured: function (message) {
        try {

            this._processRunning = false;
            this._waitingForBuilder = false;
            this._cancelWorkRequested = false;

            this._errorOccured = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = Locale.getMsg('layoutForThisReportCouldNotBeDeterminedBecauseOfCustomizingIssue');

            this.fireEvent('errorOccured', message);
            this.unregisterListenersFromBuilder();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOcurred of BaseReportFramework', ex);
        }
    },

    //private
    cancelWork: function () {
        try {
            this._processRunning = false;
            this._cancelWorkRequested = false;
            this._waitingForBuilder = false;

            this.unregisterListenersFromBuilder();

            this.onWorkerCancelled();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseReportFramework', ex);
            this.errorOccured();
        }
    },

    //public
    registerListenersOnBuilder: function () {
        try {
            if (!this._registeredCallbacksOnBuilder) {
                this._builder.addListener('progressChanged', this.innerProgressReporter, this);
                this._builder.addListener('workFinished', this.onBuilderFinished, this);
                this._builder.addListener('cancelationInProgress', this.onCancelationRequestConfirmed, this);
                this._builder.addListener('cancelled', this.cancelWork, this);
                this._builder.addListener('errorOccured', this.errorOccured, this);
                this._builder.addListener('cancelableChanged', this.cancelableChanged, this);

                this._registeredCallbacksOnBuilder = true;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerListenersOnWorker of BaseReportFramework', ex);
        }
    },

    //public
    unregisterListenersFromBuilder: function () {
        try {
            if (this._registeredCallbacksOnBuilder === true) {
                this._builder.removeListener('progressChanged', this.innerProgressReporter, this);
                this._builder.removeListener('workFinished', this.onBuilderFinished, this);
                this._builder.removeListener('cancelationInProgress', this.onCancelationRequestConfirmed, this);
                this._builder.removeListener('cancelled', this.cancelWork, this);
                this._builder.removeListener('errorOccured', this.errorOccured, this);
                this._builder.removeListener('cancelableChanged', this.onBuilderCancelableChanged, this);

                this._registeredCallbacksOnBuilder = false;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unregisterListenersFromWorker of BaseReportFramework', ex);
        }
    },

    // private
    innerProgressReporter: function (percentage, message) {
        try {
            var additionalPercentage = percentage - this._curProcessStepPercentage;
            this._curProcessStepPercentage = percentage;
            this.reportWorkProgress(this._curProcessPercent + this._curProcessStepRange * additionalPercentage / 100, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside innerProgressReporter of BaseReportFramework', ex);
            this.errorOccured();
        }
    },

    // private
    onCancelationRequestConfirmed: function (message) {
        try {
            this.reportCancelationRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelationRequestConfirmed of BaseReportFramework', ex);
            this.errorOccured();
        }
    },

    // private
    onBuilderCancelableChanged: function (cancelable) {
        try {
            this.cancelableChanged(cancelable);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelationRequestConfirmed of BaseReportFramework', ex);
            this.errorOccured();
        }
    }
});
