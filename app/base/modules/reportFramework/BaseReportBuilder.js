﻿//this class builds a report pdf - this may be a heavy task
//that is why this procedure has a lot of defer calls
//this procedure may be cancelled
//it also provides progress, error and cancelation events
Ext.define('AssetManagement.base.modules.reportFramework.BaseReportBuilder', {
    mixins: ['Ext.mixin.Observable'],

    requires: [
       'AssetManagement.customer.controller.EventController',
       'AssetManagement.customer.utils.StringUtils',
       'AssetManagement.customer.helper.OxLogger'
    ],

    //status fields
    _currentRequest: null,
    _completeEventId: NaN,
    _currentPdfObject: null,
    _currentReport: null,
    _curProcessPercent: NaN,
    _cancelRequested: false,
    _errorOccured: false,
    _lastErrorMessage: '',

    //data fields
    _headerTextContent: null,
    _headerLogo: null,
    _footerTextContent: null,

    //report language handler
    _reportLanguage: null,


    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.ReportBuilder');
            }

            return this._instance;
        }
    },

    PAGE_FORMATS : {
        DINA4 : 'A4',
        LETTER : 'LETTER'
    },

    PAGE_WIDTH: 595,

    //public
    buildReportTemplate: function (objectToPrint, reportLang, logos) {
        var retval = -1;

        try {
            var extractedMainObject = objectToPrint['mainObject'];

            var objType = extractedMainObject.get('layoutType');
            var SAPPageFormat = objType && objType.get('docData') && objType.get('docData').get('pageFormat') ? objType.get('docData').get('pageFormat') : '';
            var pageFormat = 'A4'
            if(SAPPageFormat && this.PAGE_FORMATS[SAPPageFormat]){
                pageFormat = this.PAGE_FORMATS[SAPPageFormat];
                if(this.PAGE_FORMATS[SAPPageFormat] === 'LETTER'){
                   this.PAGE_WIDTH = 612;
                }
            }

            var docDefinition = {
                pageSize: pageFormat,
                pageMargins: [40, 100, 40, 65],
                header: {},
                content: [],
                footer: {},
                pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
                    //check if signature part is completely on the last page, add pagebreak if not
                    if (currentNode.id === 'signature' ){//&& (currentNode.pageNumbers.length != 1 || currentNode.pageNumbers[0] != currentNode.pages)) {
                        var innerPageHeight = currentNode['startPosition']['pageInnerHeight'];
                        var howFarFromTop = currentNode['startPosition']['top'];
                        if((parseInt(innerPageHeight) - parseInt(howFarFromTop)) < 120){
                            return true;
                        } else {
                            return false;
                        }
                    }
                    //check if last paragraph is entirely on a single page, add pagebreak if not
                    // else if (currentNode.id === 'closingParagraph' && currentNode.pageNumbers.length != 1) {
                    //   return true;
                    // }
                    return false;
                }

            }

            var documentTemplate = pdfMake.createPdf(docDefinition);

            this.prepareFooterTemplate(documentTemplate, objectToPrint, reportLang, logos);

            this.prepareHeaderTemplate(documentTemplate, objectToPrint, reportLang, logos);

            this._reportLanguage = Locale;

            return documentTemplate;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildReportTemplate of reportFramework-BaseReportBuilder', ex);
            retval = -1;
        }

        return retval;
    },

    prepareFooterTemplate: function (documentTemplate, objectToPrint, reportLang, logos) {
        try {
            var that = this;

            var footerStyle = {
                fontSize: 7,
                color: 'grey'
            };

            var extractedMainObject = objectToPrint['mainObject'];
            var objType = extractedMainObject.get('layoutType');
            var footerTextsString = objType && objType.get('docData') && objType.get('docData').get('footerContent') ? objType.get('docData').get('footerContent') : null;
            if(!footerTextsString){
                footerTextsString = 'E-Mail: info@oxando.com\nInternet: www.oxando.com\nTel +49-621-860 860 0\nFax +49-621-860 860 29';
            }
            var footerContetnArray = footerTextsString ? footerTextsString.split('\n') : null;

            //gather the footer long texts
            var footerLeftLongtext = {
                //margin: [ 0, 10, 0, 10],
                stack:[]
                //width: 137
            };

            // footer address section
            footerLeftLongtext.stack.push({
                margin: [0 , 0, 0, 1],
                text: footerContetnArray && footerContetnArray[0] ? footerContetnArray[0] : ' ',
                alignment: 'left',
                style: footerStyle
            });

            footerLeftLongtext.stack.push({
                margin: [0 , 0, 0, 1],
                text: footerContetnArray && footerContetnArray[1] ? footerContetnArray[1] : ' ',
                alignment: 'left',
                style: footerStyle
            });

            footerLeftLongtext.stack.push({
                margin: [0 , 0, 0, 1],
                text: footerContetnArray && footerContetnArray[2] ? footerContetnArray[2] : ' ',
                alignment: 'left',
                style: footerStyle
            });

            // var footerMiddleLongtext = {
            //     margin: [ 0, 10, 0, 10],
            //     stack:[],
            //     width: '*'
            // };

            // // footer additinal text sections
            // footerMiddleLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: footerContetnArray && footerContetnArray[4] ? footerContetnArray[4] : ' ',
            //     alignment: 'left',
            //     style: footerStyle
            // });

            // footerMiddleLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: footerContetnArray && footerContetnArray[5] ? footerContetnArray[5] : ' ',
            //     alignment: 'left',
            //     style: footerStyle
            // });

            // footerMiddleLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: footerContetnArray && footerContetnArray[6] ? footerContetnArray[6] : ' ',
            //     alignment: 'left',
            //     style: footerStyle
            // });

            // var footerRightLongtext = {
            //     margin: [0, 0, 0, 0],
            //     stack:[],
            //     width: 'auto'
            // };

            // //footer right section
            // footerRightLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: ' ',
            //     alignment: 'right',
            //     style: footerStyle
            // });

            // footerRightLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: ' ',
            //     alignment: 'right',
            //     style: footerStyle
            // });

            // footerRightLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: ' ',
            //     alignment: 'right',
            //     style: footerStyle
            // });

            // footerRightLongtext.stack.push({
            //     margin: [0 , 0, 0, 1],
            //     text: 'www.getingegroup.com',
            //     alignment: 'right',
            //     style: footerStyle
            // });

            // var logo1 = '';
            // if (logos[1])
            //     logo1 = logos[1].get('logo');
            // var logo2 = '';
            // if (logos[2])
            //     logo2 = logos[2].get('logo');
            // var logo3 = '';
            // if (logos[3])
            //     logo3 = logos[3].get('logo');

            var docWidth = this.PAGE_WIDTH-2*40;
            // documentTemplate.docDefinition.footer = function () {
            //     return {
            //         margin: [40, 0, 0, 0],
            //         stack: [
            //             //{margin: [0, 10, 0, 0], canvas: [{ type: 'line', x1: 0, y1: 5, x2: docWidth, y2: 5, lineWidth: 0.5 , lineColor: 'gray'}]},
            //
            //
            //                     footerLeftLongtext
            //                     // footerMiddleLongtext,
            //                     // footerRightLongtext
            //
            //
            //
            //
            //             //     {
            //             //     margin: [0, 5, 0, 0],
            //             //     columns: [
            //             //         {
            //             //             image: logo1,
            //             //             width: 60
            //             //         },
            //             //         {
            //             //             image: logo2,
            //             //             width: 90
            //             //         },
            //             //         {
            //             //             image: logo3,
            //             //             width: 60
            //             //         }
            //             //     ],
            //             //     columnGap: 30
            //             // },
            //
            //             //{margin: [0, 5, 0, 0], canvas: [{ type: 'line', x1: 0, y1: 5, x2: docWidth, y2: 5, lineWidth: 0.5 , lineColor: 'gray'}]}
            //         ]
            //     }
            // };

            return documentTemplate;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareFooterTemplate of reportFramework-BaseReportBuilder', ex);
            this.showMissingDataDialog();
        }
    },

    showMissingDataDialog : function(){
        try{
            var callback = function(confirmed) {
                try {
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showMissingDataDialog of reportFramework-BaseReportBuilder', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('error'),
                icon: 'resources/icons/alert.png',
                message: Locale.getMsg('errorOccuredReportIncompletePleaseTryGenerateReportAgain'),
                alternateConfirmText: Locale.getMsg('close'),
                hideDeclineButton : true,
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch(ex){

        }
    },

    prepareHeaderTemplate: function (documentTemplate, objectToPrint, reportLang, logos) {
        try {
            var that = this;

            var headerStyle = {
                fontSize: 8,
                color: 'grey'
            };

            var logo = null;
            ///
            var extractedMainObject = objectToPrint['mainObject'];
            var objType = extractedMainObject.get('layoutType');
            var headerTextsString = objType && objType.get('docData') && objType.get('docData').get('headerContent') ? objType.get('docData').get('headerContent') : null;
            if(!headerTextsString){
                headerTextsString = 'oxando GmbH\nKonrad-Zuse-Ring 12\nD-68163 Mannheim';
            }
            var headerContetnArray = headerTextsString ? headerTextsString.split('\n') : null;

             //gather the footer long texts
            var headerLongtext = {
                margin: [ 0, 10, 0, 10],
                stack:[],
                width: 137
            };

            if(headerContetnArray){
                for(var i = 0; i < headerContetnArray.length; i++) {
                    headerLongtext.stack.push({
                        text: headerContetnArray[i],
                        style: headerStyle
                    })
                }
            }


            if (logos[0])
                logo = logos[0].get('logo');

            // if (true) {
            //     var docWidth = this.PAGE_WIDTH-2*40;
            //     documentTemplate.docDefinition.header = function(){
            //         return {
            //             margin: [40, 20, 40, 10],
            //             stack: [
            //                 //{margin: [0, 6, 0, 10], canvas: [{ type: 'line', x1: 0, y1: 5, x2: docWidth, y2: 5, lineWidth: 0.5 , lineColor: 'gray'}]},
            //                 {
            //                     margin: [0, 0, 40, 10],
            //                     columns: [
            //                         {
            //                             columns: [
            //                                 headerLongtext
            //                             ]
            //                         },
            //                         {
            //                         margin: [35, 10 , 0, 0],
            //                         image: logo,
            //                         width: 160
            //                         }
            //                     ]
            //                 }
            //             ]
            //         }
            //     };
            // }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeHeader of reportFramework-BaseReportBuilder', ex);
            this.showMissingDataDialog();
            retval = false;
        }
    }
});
