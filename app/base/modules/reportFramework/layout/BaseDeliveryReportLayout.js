﻿Ext.define('AssetManagement.base.modules.reportFramework.layout.BaseDeliveryReportLayout', {


  mixins: ['Ext.mixin.Observable'],

  requires: [
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.controller.EventController',
    'AssetManagement.customer.model.bo.UserInfo'

  ],

  inheritableStatics: {
    //private:
    _instance: null,

    //public:
    getInstance: function () {
      if (this._instance == null) {
        this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.layout.DeliveryReportLayout');
      }

      return this._instance;
    }
  },

  _reportLocale: null,
  _newLangLoaded: false,


  buildContent: function (object, doc, reportLang) {
    try {

      var me = this;


      // var reportLanguageCallback = function () {
      //     try {
      //         me._newLangLoaded = true;
      //         me.writeOrderGeneralData(object, doc);
      //         me.writeObjectList(object, doc);
      //         me.writeTimeConfs(object, doc);
      //         me.writeMatConfs(object, doc);
      //         me.writeComponents(object, doc);

      //         var temp = {
      //             pdfDocument : doc,
      //             reportLangInstance : me._reportLocale
      //         }
      //         return temp;

      //     } catch (ex) {
      //         AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent-reportLanguageCallback of BaseDeliveryReportLayout', ex)
      //     }
      // }
      // create report locale
      if (!this._reportLocale) {
        this._reportLocale = $.extend(true, {}, Locale);
        //this._newLangLoaded = true;
      }

      if (reportLang && reportLang !== this._reportLocale['currentLang']) {
        //this._newLangLoaded = false;
        this._reportLocale.setCurrentLanguage(reportLang);
        this._reportLocale.loadSync();
      }

      // } else {
      // if (this._newLangLoaded === false) {
      //     Ext.defer(me.buildContent, 1, me, [object, doc, reportLang]);
      // } else {
      this.writeOrderGeneralData(object, doc);
      this.writeObjectList(object, doc);
      var temp = {
        pdfDocument: doc,
        reportLangInstance: me._reportLocale
      }
      return temp;
      // }
      // }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent of BaseDeliveryReportLayout', ex)
    }
  },


  writeOrderGeneralData: function (object, doc) {
    try {
      if (this._cancelRequested || this._errorOccured) {
        this.cancelBuild();
        return;
      }

      var me = this;

      var object = object['mainObject'];
      var objectList = object.get('deliveryStore');
      var firstItem = objectList.getAt(0);
      var building = firstItem.get('gebnr');
      var room = firstItem.get('raum');
      var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();


      // doc.docDefinition.content.push('\n\n\n\n');

      doc.docDefinition.content.push({
        text: me._reportLocale.getMsg('deliveryReport'),
        style: {
          fontSize: 18,
          bold: true,
          alignment: 'center'
        }
      });

      doc.docDefinition.content.push('\n\n\n');


      //write purchase order number, date, engineer and order type

      var tempStyle = {
        fontSize: 10,
        normal: true
      }


      doc.docDefinition.content.push({
        columns: [
          {
            width: '30%',
            text: me._reportLocale.getMsg('date'),
            style: tempStyle
          },
          {
            width: '70%',
            text: AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(
              AssetManagement.customer.utils.DateTimeUtils.getCurrentTime()),
            style: tempStyle
          }
        ]
      });


      doc.docDefinition.content.push({
        columns: [
          {
            width: '30%',
            text: me._reportLocale.getMsg('room'),
            style: tempStyle
          },
          {
            width: '70%',
            text: AssetManagement.customer.utils.StringUtils.filterLeadingZeros(room),
            style: tempStyle
          }
        ]
      });

      doc.docDefinition.content.push({
        columns: [
          {
            width: '30%',
            text: me._reportLocale.getMsg('building'),
            style: tempStyle
          },
          {
            width: '70%',
            text: AssetManagement.customer.utils.StringUtils.filterLeadingZeros(building),
            style: tempStyle
          }
        ]
      });

      doc.docDefinition.content.push({
          canvas: [{
            type: 'line', x1: 0, y1: 5, x2: 595 - 80, y2: 5, lineWidth: 0.75
          }]
        }
      );


      doc.docDefinition.content.push({
        text: me._reportLocale.getMsg('deliveryPickUp'),
        style: {
          fontSize: 16,
          bold: true,
          alignment: 'center'
        }
      });


      doc.docDefinition.content.push({
          canvas: [{
            type: 'line', x1: 0, y1: 5, x2: 595 - 80, y2: 5, lineWidth: 1
          }]
        }
      );

      doc.docDefinition.content.push('\n');

      return doc;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeOrderGeneralData of BaseDeliveryReportLayout', ex);
    }
  },

  writeObjectList: function (object, doc) {
    try {

      var me = this;
      // var objectList = object['mainObject'].get('objectList');

      var object = object['mainObject'];
      var objectList = object.get('deliveryStore');
      var reasonStore = object.get('reasonStore');

      var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
      var spras = userInfo.get('sprache');

      //only write the objectlist section, if there is anything in it
      if (objectList && objectList.getCount() > 0) {
        //prepare the document for writing the object list section


        //begin table drawing
        //start with the header

        var tableHeaderStyle = {
          fontSize: 8,
          bold: true
          // fillColor: '#91cf4f'
        }

        doc.docDefinition.content.push({
          table: {
            headerRows: 1,
            widths: ['*', '*', '*', '*'],
            body: [
              [{
                text: me._reportLocale.getMsg('record') + ' ' + me._reportLocale.getMsg('name') ,
                style: tableHeaderStyle
              }, {
                text: me._reportLocale.getMsg('orgKz'),
                style: tableHeaderStyle
              }
                , {
                text: me._reportLocale.getMsg('quantity'),
                style: tableHeaderStyle
              }, {
                text: me._reportLocale.getMsg('statusReason'),
                style: tableHeaderStyle
              }]
            ]
          },
          layout: 'headerLineOnly'
        });
        //end of the header

        //start with content
        {

          var tempStyle = {
            fontSize: 8,
            normal: true
          };

          var lastIndexOfThePdfTable = doc.docDefinition.content.length - 1;

          objectList.each(function (objectListItem) {
            var belnr = objectListItem.get('belnr');
            var empf = objectListItem.get('empf');
            var short1 = objectListItem.get('short');
            var menge = objectListItem.get('menge');
            var ia_status = objectListItem.get('ia_status');
            var grund = objectListItem.get('grund');
            var barcode = objectListItem.get('barcode');
            var status = '';

            if(ia_status === 'Z'){
              status = Locale.getMsg('delivered');
            }else if(ia_status === 'XX'){

              reasonStore.each(function (reason) {
                if (reason.get('grund') === grund && reason.get('spras') === spras) {
                  status = reason.get('text');
                }
              });
            }

            var row = ["", "", "", ""];

            //use 5px left padding to cell beginning
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(belnr))
              row[0] = {

                stack: [
                  {
                    text: belnr + '    ' + empf,
                    style: tempStyle
                  },
                  {
                    image: barcode,
                    width: 150
                  }
                ]
              };

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(short1))
              row[1] = {
                text: short1,
                style: tempStyle
              };

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(menge))
              row[2] = {
                text: menge,
                style: tempStyle
              };

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ia_status) ||!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(grund)  )
              row[3] = {
                // text: ia_status && ia_status !== 'XX' ? ia_status : grund ,
                text: status,
                style: tempStyle
              };

            doc.docDefinition.content[lastIndexOfThePdfTable].table.body.push(row);
          }, this);
        }
        //end content

      }

      return doc;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeObjectList of BaseDeliveryReportLayout', ex);
    }
  }

});
