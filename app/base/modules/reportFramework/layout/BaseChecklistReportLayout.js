Ext.define('AssetManagement.base.modules.reportFramework.layout.BaseChecklistReportLayout', {


    mixins: ['Ext.mixin.Observable'],

    requires: [
      'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.controller.EventController',
      'AssetManagement.customer.model.bo.QPlosChar'

    ],

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.layout.ChecklistReportLayout');
            }

            return this._instance;
        }
    },

    _reportLocale: null,
    _newLangLoaded : false,


    buildContent: function (object, doc, reportLang) {
        try {

             var me = this;


            // var reportLanguageCallback = function () {
            //     try {
            //         me._newLangLoaded = true;
            //         me.writeChecklistGeneralData(object, doc);
            //         return doc;
            //     } catch (ex) {
            //         AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent-reportLanguageCallback of BaseChecklistReportLayout', ex)
            //     }
            // }
            // create report locale
            if (!this._reportLocale) {
                this._reportLocale = $.extend(true, {}, Locale);
                this._newLangLoaded = true;
            }

            if (reportLang && reportLang !== this._reportLocale['currentLang']) {
                this._newLangLoaded = false;
                this._reportLocale.setCurrentLanguage(reportLang);
                this._reportLocale.loadSync();
            }

            //} else {
                //if (this._newLangLoaded === false) {
                //    Ext.defer(me.buildContent, 1, me, [object, doc, reportLang]);
                //} else {
                    this.writeChecklistGeneralData(object, doc);
                    var temp = {
                        pdfDocument : doc,
                        reportLangInstance : me._reportLocale
                    }
                    return temp;
                //}
            //}

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent of BaseChecklistReportLayout', ex)
        }
    },



    writeChecklistGeneralData: function (object, doc) {
        try {
            var me = this;

            var qplos = object['mainObject'];
            var qplosOpers = qplos.get('qplosOpers');
            var qplosTechObj = qplos.get('techObj');
            var qplosViewChars = object['qplosViewChars'];

            doc.docDefinition.content.push('\n\n');

            doc.docDefinition.content.push({
                text: me._reportLocale.getMsg('inspectionReport'),
                style: {
                    fontSize: 12,
                    bold: true
                }
            });

            doc.docDefinition.content.push('\n');

            doc.docDefinition.content.push({
                text: me._reportLocale.getMsg('order') + ' ' + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(qplos.get('aufnr')),
                style: {
                    fontSize: 10,
                    bold: true
                }
            });

            var mainDataId = '';
            var mainDataValue = '';
            var techObjClassName = Ext.getClassName(qplosTechObj);

            if (techObjClassName === 'AssetManagement.customer.model.bo.Equipment') {
                mainDataId = 'equipment';
                mainDataValue = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(qplosTechObj.get('equnr')) + ' ' + qplosTechObj.get('eqktx');
            } else if (techObjClassName === 'AssetManagement.customer.model.bo.FuncLoc') {
                mainDataId = 'funcLoc_Short';
                mainDataValue = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(qplosTechObj.get('tplnr')) + ' ' + qplosTechObj.get('tptxt');
            }

            if(mainDataId && mainDataValue){
                doc.docDefinition.content.push({
                    text: me._reportLocale.getMsg(mainDataId) + ' ' + mainDataValue,
                    style: {
                        fontSize: 10,
                        bold: true
                    }
                });
            }

            doc.docDefinition.content.push('\n\n');
            var me = this;
            if (qplosOpers && qplosOpers.getCount() > 0) {
                qplosOpers.each(function (qplosOper) {
                    var qplosChars = qplosOper.get('qplosChars');
                    var opertext = qplosOper.get('ltxa1');
                    if (qplosChars && qplosChars.getCount() > 0) {
                        me.writeCharacteristics(qplosViewChars, opertext, doc);
                    }

                });
            }
            //write qualitative characteristics
            return doc;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeOrderGeneralData of BaseChecklistReportLayout', ex);
        }
    },

    writeCharacteristics : function(qplosViewChars, sortField, doc){
        try {
            var me = this;

            doc.docDefinition.content.push({
                text: sortField,
                style: {
                    fontSize: 10,
                    bold: true
                }
            });

            doc.docDefinition.content.push('\n');

            if(qplosViewChars && qplosViewChars.getCount() > 0){
                doc.docDefinition.content.push({
                    table: {
                        widths: [150, 150, 200],
                        headerRows: 1,
                        body: [
                        [{
                            text: me._reportLocale.getMsg('characteristic'),
                            style: { fontSize: 9, fillColor: '#91cf4f' }
                        }, {
                            text: me._reportLocale.getMsg('characteristicValue'),
                            style: { fontSize: 9, fillColor: '#91cf4f' }
                        }, {
                            text: me._reportLocale.getMsg('remark'),
                            style: { fontSize: 9, fillColor: '#91cf4f' }
                        }]
                        ]
                    }
                });

                var indexOfLastElement = doc.docDefinition.content.length - 1;

                qplosViewChars.filter('qpOpertxt', sortField);
                this.prepareChecklistReportRow(qplosViewChars, doc, indexOfLastElement);
                qplosViewChars.clearFilter(true);          
            }
            

            doc.docDefinition.content.push('\n\n');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeQualitativeCharacteristics of BaseChecklistReportLayout', ex);
        }
    },

    prepareChecklistReportRow : function(qplosViewChars, doc, tableIndex) {
        try {
            var me = this;

            qplosViewChars.each(function (characteristic) {
                var tableRow = [];
                var description = me.prepareDescriptionValue(characteristic); //.get('kurztext');
                var value = me.determineValue(characteristic);
                var remark = characteristic.get('remark');
                var condition = ' ';

                if (characteristic.get('checkBoxNOK') === true) {
                    condition = me._reportLocale.getMsg('notInOrderLong');
                } else if (characteristic.get('checkBoxOK') === true) {
                    condition = me._reportLocale.getMsg('inOrderLong');
                } else if (characteristic.get('checkBoxNREL') === true) {
                    condition = me._reportLocale.getMsg('notRelevantLong');
                }
                if (condition != ' ' && value != ' ')
                    condition = ' / ' + condition
                tableRow.push(description);
                tableRow.push({ text: value + condition, style: { fontSize: 8 } });
                tableRow.push({ text: remark, style: { fontSize: 8 } });

                doc.docDefinition.content[tableIndex].table.body.push(tableRow);
            }, this);

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareChecklistReportRow of BaseChecklistReportLayout', ex);
        }
    },

    prepareDescriptionValue: function(charact) {
        try {
            var kurztext = charact.get('kurztext');

            var tableRowStyle = {
                fontSize: 8
            };

            var toleranzText = "";
            //tolerance boundries used for toleranzText
            var toleranzun = charact.get('toleranzun');
            var toleranzob = charact.get('toleranzob');
            var sollwert = charact.get('sollwert');

            if (toleranzun)
                toleranzText += this._reportLocale.getMsg('toleranzun') + ": " + toleranzun + "  ;";

            if (sollwert)
                toleranzText += this._reportLocale.getMsg('sollwert') + ": " + sollwert + "  ;";

            if (toleranzob)
                toleranzText += this._reportLocale.getMsg('toleranzob') + ": " + toleranzob + "  ;";

            toleranzText = toleranzText.substr(0, toleranzText.length - 1);

            var description = {
                stack:[
                    {
                        text: kurztext, 
                        style : tableRowStyle
                    },{
                        text: toleranzText, 
                        style : {
                            fontSize: 7
                        }
                    }
                ]
            }

            return description;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareDescriptionValue of BaseChecklistReportLayout', ex);
        }
    },

    determineValue : function(charact) {
        var retval = ' ';

        try {
            var category  = charact.get('category');
            if(category === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_STANDARD){
                var codes = charact.get('codes'); 
                if(codes && codes.getCount() > 0){
                    var auwmenge2 = charact.get('auswmenge2');
                    var auswmgwrk2 = charact.get('auswmgwrk2');

                    if (auwmenge2 && auswmgwrk2) {
                        codes.each(function (code) {
                            if (code.get('code') === auswmgwrk2 &&
                                code.get('codegruppe') === auwmenge2) {
                                retval = code.get('kurztext');
                            }
                        });
                    }
                }
            } else if(category === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUANTITATIVE){
                retval = charact.get('meanValue') +' '+ charact.get('masseinhsw');
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside determineValue of BaseChecklistReportLayout', ex);
        }

        return retval;
    }
});
