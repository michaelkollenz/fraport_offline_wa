﻿Ext.define('AssetManagement.base.modules.reportFramework.layout.BaseOrderReportLayout', {


    mixins: ['Ext.mixin.Observable'],

    requires: [
      'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.controller.EventController',
      'AssetManagement.customer.model.bo.UserInfo'

    ],

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.layout.OrderReportLayout');
            }

            return this._instance;
        }
    },

    _reportLocale: null,
    _newLangLoaded : false,


    buildContent: function (object, doc, reportLang) {
        try {

            var me = this;


            // var reportLanguageCallback = function () {
            //     try {
            //         me._newLangLoaded = true;
            //         me.writeOrderGeneralData(object, doc);
            //         me.writeObjectList(object, doc);
            //         me.writeTimeConfs(object, doc);
            //         me.writeMatConfs(object, doc);
            //         me.writeComponents(object, doc);

            //         var temp = {
            //             pdfDocument : doc,
            //             reportLangInstance : me._reportLocale
            //         }
            //         return temp;

            //     } catch (ex) {
            //         AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent-reportLanguageCallback of BaseOrderReportLayout', ex)
            //     }
            // }
            // create report locale
            if (!this._reportLocale) {
                this._reportLocale = $.extend(true, {}, Locale);
                //this._newLangLoaded = true;
            }

            if (reportLang && reportLang !== this._reportLocale['currentLang']) {
                //this._newLangLoaded = false;
                this._reportLocale.setCurrentLanguage(reportLang);
                this._reportLocale.loadSync();
            }

            // } else {
                // if (this._newLangLoaded === false) {
                //     Ext.defer(me.buildContent, 1, me, [object, doc, reportLang]);
                // } else {
                    this.writeOrderGeneralData(object, doc);
                    this.writeObjectList(object, doc);
                    this.writeTimeConfs(object, doc);
                    this.writeMatConfs(object, doc);
                    me.writeComponents(object, doc);
                    var temp = {
                        pdfDocument : doc,
                        reportLangInstance : me._reportLocale
                    }
                    return temp;
                // }
            // }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContent of BaseOrderReportLayout', ex)
        }
    },



    writeOrderGeneralData: function (object, doc) {
        try {
            if (this._cancelRequested || this._errorOccured) {
                this.cancelBuild();
                return;
            }

            var me = this;

            var order = object['mainObject'];
            var orderType = order.get('orderType');
            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
            var customerAsPartner = null;
            var objectAddress = null;

            //get the customer and the object address to use from the order
            if (order.get('partners')) {
                var customerAsPartner = null;

                order.get('partners').each(function (partner) {
                    if (partner.get('parvw') === 'AG') {
                        customerAsPartner = partner;
                        return false;
                    }
                });
            }

            if (order.get('address')) {
                objectAddress = order.get('address');
            } else if (order.get('equipment') && order.get('equipment').get('address')) {
                objectAddress = order.get('equipment').get('address');
            } else if (order.get('funcLoc') && order.get('funcLoc').get('address')) {
                objectAddress = order.get('funcLoc').get('address');
            }




            doc.docDefinition.content.push('\n\n\n\n');

            doc.docDefinition.content.push({
                text: me._reportLocale.getMsg('orderReport'),
                style: {
                    fontSize: 12,
                    bold: true
                }
            });

            doc.docDefinition.content.push({
                text: me._reportLocale.getMsg('orderNumShort') + ' ' + AssetManagement.customer.utils.StringUtils.filterLeadingZeros(order.get('aufnr')),
                style: {
                    fontSize: 10,
                    bold: true
                }
            });


            doc.docDefinition.content.push('\n\n');

            //write purchase order number, date, engineer and order type

            var tempStyle = {
                fontSize: 10,
                normal: true
            }

            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('purchOrderNo'),
                        style: tempStyle
                    },
                    {
                        width: '70%',
                        text: order.get('bstkd'),
                        style: tempStyle
                    }
                ]
            });


            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('date'),
                        style: tempStyle
                    },
                    {
                        width: '70%',
                        text: AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(
                            AssetManagement.customer.utils.DateTimeUtils.getCurrentTime()),
                        style: tempStyle
                    }
                ]
            });

            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('orderReport_Inspector'),
                        style: tempStyle
                    },
                    {
                        width: '70%',
                        text: userInfo.get('arbplKtext'),
                        style: tempStyle
                    }
                ]
            });

            var orderTypeString = order.get('auart');

            if (orderType)
                orderTypeString += ' ' + order.get('orderType').get('txt');

            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('orderType'),
                        style: tempStyle
                    },
                    {
                        width: '70%',
                        text: orderTypeString,
                        style: tempStyle
                    }
                ]
            });

            doc.docDefinition.content.push('\n\n');

            //write customer data & object address

            tempStyle = {
                fontSize: 9,
                bold: true
            };


            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: ''
                    },
                    {
                        width: '35%',
                        text: me._reportLocale.getMsg('orderReport_Customer'),
                        style: tempStyle
                    },
                    {
                        width: '35%',
                        text: me._reportLocale.getMsg('objectAddress'),
                        style: tempStyle
                    }

                ]
            });

            tempStyle = {
                fontSize: 9,
                normal: true
            };

            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('name'),
                        style: tempStyle
                    }
                ]
            });


            var idxOfContentLastElement = doc.docDefinition.content.length - 1;
            if (customerAsPartner)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                    {
                        width: '35%',
                        text: customerAsPartner.get('name1'),
                        style: tempStyle
                    }
                );

            if (objectAddress)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                   {
                       width: '35%',
                       text: objectAddress.get('name1'),
                       style: tempStyle
                   }
               );

            idxOfContentLastElement++;
            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: '',
                        style: tempStyle
                    }
                ]
            });

            if (customerAsPartner)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                {
                    width: '35%',
                    text: customerAsPartner.get('name2'),
                    style: tempStyle
                });

            if (objectAddress)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                    {
                        width: '35%',
                        text: objectAddress.get('name2'),
                        style: tempStyle
                    });



            idxOfContentLastElement++;
            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('street'),
                        style: tempStyle
                    }
                ]
            });

            if (customerAsPartner)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                 {
                     width: '35%',
                     text: customerAsPartner.get('street'),
                     style: tempStyle
                 });
            if (objectAddress)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                {
                    width: '35%',
                    text: objectAddress.get('street'),
                    style: tempStyle
                });


            idxOfContentLastElement++;
            doc.docDefinition.content.push({
                columns: [
                    {
                        width: '30%',
                        text: me._reportLocale.getMsg('country') + '/' + me._reportLocale.getMsg('zipCode') + '/' + me._reportLocale.getMsg('city'),
                        style: tempStyle
                    }
                ]
            });

            if (customerAsPartner)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                    {
                        width: '35%',
                        text: customerAsPartner.get('country') + ' ' + customerAsPartner.get('postcode1') + ' ' + customerAsPartner.get('city1'),
                        style: tempStyle
                    });
            if (objectAddress)
                doc.docDefinition.content[idxOfContentLastElement].columns.push(
                {
                    width: '35%',
                    text: objectAddress.get('country') + ' ' + objectAddress.get('postCode1') + ' ' + objectAddress.get('city1'),
                    style: tempStyle
                });

            doc.docDefinition.content.push("\n\n");

            return doc;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeOrderGeneralData of BaseOrderReportLayout', ex);
        }
    },

    writeObjectList: function (object, doc) {
        try {
            if (this._cancelRequested || this._errorOccured) {
                this.cancelBuild();
                return;
            }

            var me = this;
            var objectList = object['mainObject'].get('objectList');

            //only write the objectlist section, if there is anything in it
            if (objectList && objectList.getCount() > 0) {
                //prepare the document for writing the object list section


                //add objectlist header
                doc.docDefinition.content.push({
                    text: me._reportLocale.getMsg('repairedProducts'),
                    style: {
                        fontSize: 9,
                        bold: true
                    }
                });
                doc.docDefinition.content.push('\n');

                //begin table drawing
                //start with the header

                var tableHeaderStyle = {
                    fontSize: 8,
                    bold: true,
                    fillColor: '#91cf4f'
                }

                doc.docDefinition.content.push({
                    table: {
                        headerRows: 1,
                        widths: ['20%', '20%', '25%', '35%'],
                        body: [
                            [{
                                text: me._reportLocale.getMsg('operation'),
                                style: tableHeaderStyle
                            }, {
                                text: me._reportLocale.getMsg('equipment'),
                                style: tableHeaderStyle
                            }
                            , {
                                text: me._reportLocale.getMsg('material'),
                                style: tableHeaderStyle
                            }, {
                                text: me._reportLocale.getMsg('materialDescription'),
                                style: tableHeaderStyle
                            }]
                        ]
                    }
                });
                //end of the header

                //start with content
                {

                    var tempStyle = {
                        fontSize: 8,
                        normal: true
                    };

                    var lastIndexOfThePdfTable = doc.docDefinition.content.length - 1;

                    objectList.each(function (objectListItem) {
                        var vornr = objectListItem.get('vornr');
                        var equnr = objectListItem.get('equnr');
                        var matnr = objectListItem.get('matnr');
                        var maktx = objectListItem.get('maktx');

                        var row = ["", "", "", ""];

                        //use 5px left padding to cell beginning
                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr))
                            row[0] = {
                                text: vornr,
                                style: tempStyle
                            };

                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr))
                            row[1] = {
                                text: equnr,
                                style: tempStyle
                            };

                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr))
                            row[2] = {
                                text: matnr,
                                style: tempStyle
                            };

                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(maktx))
                            row[3] = {
                                text: maktx,
                                style: tempStyle
                            };

                        doc.docDefinition.content[lastIndexOfThePdfTable].table.body.push(row);
                    }, this);
                }
                //end content

            }

            return doc;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeObjectList of BaseOrderReportLayout', ex);
        }
    },

    writeTimeConfs: function (object, doc) {
        try {

            var timeConfs = object['mainObject'].get('timeConfs');
            var isForInternalUser = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ext_scen_active') !== 'X';
            var me = this;
            //only write the timeConfList section, if there are any timeConfs
            if (timeConfs && timeConfs.getCount() > 0) {
                //prepare the document for writing the time conf list section


                //add timeConfsList header
                var tempStyle = {
                    fontSize: 9,
                    bold: true
                };

                doc.docDefinition.content.push('\n');

                doc.docDefinition.content.push({
                    text: me._reportLocale.getMsg('workTimes_Travel_SmallParts'),
                    style: tempStyle
                });


                doc.docDefinition.content.push('\n');

                var tableHeaderStyle = {
                    fontSize: 8,
                    bold: true,
                    fillColor: '#91cf4f'
                };

                doc.docDefinition.content.push({
                    table: {
                        headerRows: 1,
                        body: []
                    },
                    style: {
                        fontSize: 8
                    }

                });

                var tableHeader = [];
                var indexOfLastElement = doc.docDefinition.content.length - 1;

                if (isForInternalUser) {
                    //(10% - 16,67% - 30% - 16,67% - 10% - 16,67%)
                    doc.docDefinition.content[indexOfLastElement].table.widths = ['10%', '17%', '30%', '17%', '10%', '16%'];

                    tableHeader = [{
                        text: me._reportLocale.getMsg('operation'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('accountIndication'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('remarks'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('effort'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('date'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('complexity'),
                        style: tableHeaderStyle
                    }];

                } else {
                    //(22,5% - 41% - 14% - 22,5%)
                    doc.docDefinition.content[indexOfLastElement].table.widths = ['22%', '41%', '14%', '23%'];

                    tableHeader = [{
                        text: me._reportLocale.getMsg('effort'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('remarks'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('date'),
                        style: tableHeaderStyle
                    }, {
                        text: me._reportLocale.getMsg('complexity'),
                        style: tableHeaderStyle
                    }];
                }

                doc.docDefinition.content[indexOfLastElement].table.body.push(tableHeader);


                //end header

                //start with content
                {

                    timeConfs.each(function (timeConf) {
                        var vornr = timeConf.get('vornr');
                        var remarks = timeConf.get('ltxa1');
                        var unit = timeConf.get('ismne');

                        var bemot = timeConf.get('bemotRef');
                        var bemotText = bemot ? bemot.get('bemottxt') : '';

                        var actType = timeConf.get('activityType');
                        var learrTxt = actType ? actType.get('ktext') : '';

                        var date = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(timeConf.get('ied'));

                        //only write work, if the unit of the time conf is min or hours
                        //else this time conf encapsulates data of an expense
                        var work = '';

                        unit = unit.toUpperCase();

                        if (unit === 'MIN' || unit === 'H' || unit === 'STD') {
                            work = timeConf.getWorkValueForDisplay();
                        } else {
                            work = AssetManagement.customer.utils.StringUtils.concatenate([AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(timeConf.get('ismnw').trim()), unit]);
                        }

                        // table content style
                        var tableContentStyle = {
                            fontSize: 8
                        };

                        var tableContentRow = [];
                        var indexOfLastElement = doc.docDefinition.content.length - 1;

                        if (isForInternalUser) {
                            //(10% - 16,67% - 30% - 16,67% - 10% - 16,67%)
                            doc.docDefinition.content[indexOfLastElement].table.widths = ['10%', '17%', '30%', '17%', '10%', '16%'];
                        } else {
                            //(22,5% - 41% - 14% - 22,5%)
                            doc.docDefinition.content[indexOfLastElement].table.widths = ['22%', '41%', '14%', '23%'];
                        }

                        //write row content
                        if (isForInternalUser) {
                            //(10% - 16,67% - 30% - 16,67% - 10% - 16,67%)
                            tableContentRow[0] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr) ? vornr : "";
                            tableContentRow[1] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(bemotText) ? bemotText : "";
                            tableContentRow[2] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(remarks) ? remarks : "";
                            tableContentRow[3] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(learrTxt) ? learrTxt : "";
                            tableContentRow[4] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(date) ? date : "";
                            tableContentRow[5] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(work) ? work : "";
                        } else {
                            //(22,5% - 41% - 14% - 22,5%)
                            tableContentRow[0] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(learrTxt) ? learrTxt : "";
                            tableContentRow[1] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(remarks) ? remarks : "";
                            tableContentRow[2] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(date) ? date : "";
                            tableContentRow[3] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(work) ? work : "";
                        }
                        // pushing each row to table
                        doc.docDefinition.content[indexOfLastElement].table.body.push(tableContentRow);

                    }, this);
                }
            }
            //end content

            return doc;
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeTimeConfs of BaseOrderReportLayout', ex);
        }
    },

    writeMatConfs: function (object, doc) {
        try {

            var matConfs = object['mainObject'].get('matConfs');
            var me = this;
            //only write the matConfList section, if there ary any matConfs
            if (matConfs && matConfs.getCount() > 0) {


                doc.docDefinition.content.push('\n');

                doc.docDefinition.content.push({
                    text: me._reportLocale.getMsg('materialConsumption'),
                    style: {
                        fontSize: 9,
                        bold: true
                    }
                });

                doc.docDefinition.content.push('\n');

                var tableHeaderStyle = {
                    fontSize: 8,
                    bold: true,
                    fillColor: '#91cf4f'
                };

                doc.docDefinition.content.push({
                    table: {
                        headers: 1,
                        body: []
                    }
                });

                var indexOfLastElement = doc.docDefinition.content.length - 1;

                var headerRow = [];

                ////begin table drawing
                ////start with header
                {
                    //draw cell seperators (10% - 16,67% - 16,67% - 30% - 10% - 16,67%)
                    doc.docDefinition.content[indexOfLastElement].table.widths = ['10%', '17%', '17%', '30%', '10%', '16%'];

                    //write header contents
                    headerRow[0] = { text: me._reportLocale.getMsg('operation'), style: tableHeaderStyle };
                    headerRow[1] = { text: me._reportLocale.getMsg('accountIndication'), style: tableHeaderStyle };
                    headerRow[2] = { text: me._reportLocale.getMsg('material'), style: tableHeaderStyle };
                    headerRow[3] = { text: me._reportLocale.getMsg('materialDescription'), style: tableHeaderStyle };
                    headerRow[4] = { text: me._reportLocale.getMsg('date'), style: tableHeaderStyle };
                    headerRow[5] = { text: me._reportLocale.getMsg('quantityUnit'), style: tableHeaderStyle };
                }
                //end header

                doc.docDefinition.content[indexOfLastElement].table.body.push(headerRow);
                //start with content
                {
                    var contentRow = [];
                    var rowStyle = {
                        fontSize: 8,
                        normal: true
                    };

                    matConfs.each(function (matConf) {
                        contentRow = [];
                        var vornr = matConf.get('vornr');

                        var bemot = matConf.get('bemotRef');
                        var bemotText = bemot ? bemot.get('bemottxt') : '';

                        var matnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(matConf.get('matnr'));
                        var maktx = '';

                        if (matConf.get('material')) {
                            maktx = matConf.get('material').get('maktx');
                        }
                        var date = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(matConf.get('isdd'));

                        var consumption = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(matConf.get('quantity')) + ' ' + matConf.get('unit');

                        contentRow[0] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr) ? { text: vornr, style: rowStyle } : "";
                        contentRow[1] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(bemotText) ? { text: bemotText, style: rowStyle } : "";
                        contentRow[2] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr) ? { text: matnr, style: rowStyle } : "";
                        contentRow[3] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(maktx) ? { text: maktx, style: rowStyle } : "";
                        contentRow[4] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(date) ? { text: date, style: rowStyle } : "";
                        contentRow[5] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(consumption) ? { text: consumption, style: rowStyle } : "";

                        doc.docDefinition.content[indexOfLastElement].table.body.push(contentRow);

                    }, this);
                }
                ////end content
            }

            return doc;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeMatConfs of BaseOrderReportLayout', ex);
        }
    },

    writeComponents: function(object, doc) {
        try {
            var components = object['mainObject'].get('components');
            var me = this;
            //only write the matConfList section, if there ary any matConfs
            if (components && components.getCount() > 0) {


                doc.docDefinition.content.push('\n');

                doc.docDefinition.content.push({
                    text: me._reportLocale.getMsg('components'),
                    style: {
                        fontSize: 9,
                        bold: true
                    }
                });

                doc.docDefinition.content.push('\n');

                var tableHeaderStyle = {
                    fontSize: 8,
                    bold: true,
                    fillColor: '#91cf4f'
                };

                doc.docDefinition.content.push({
                    table: {
                        headers: 1,
                        body: []
                    }
                });

                var indexOfLastElement = doc.docDefinition.content.length - 1;

                var headerRow = [];

                ////begin table drawing
                ////start with header
                {
                    //draw cell seperators (10% - 16,67% - 16,67% - 30% - 10% - 16,67%)
                    doc.docDefinition.content[indexOfLastElement].table.widths = ['10%', '17%', '17%', '30%', '10%', '16%'];

                    //write header contents
                    headerRow[0] = { text: me._reportLocale.getMsg('operation'), style: tableHeaderStyle };
                    headerRow[1] = { text: me._reportLocale.getMsg('accountIndication'), style: tableHeaderStyle };
                    headerRow[2] = { text: me._reportLocale.getMsg('material'), style: tableHeaderStyle };
                    headerRow[3] = { text: me._reportLocale.getMsg('materialDescription'), style: tableHeaderStyle };
                    headerRow[4] = { text: me._reportLocale.getMsg('date'), style: tableHeaderStyle };
                    headerRow[5] = { text: me._reportLocale.getMsg('quantityUnit'), style: tableHeaderStyle };
                }
                //end header

                doc.docDefinition.content[indexOfLastElement].table.body.push(headerRow);
                //start with content
                {
                    var contentRow = [];
                    var rowStyle = {
                        fontSize: 8,
                        normal: true
                    };

                    components.each(function (component) {
                        contentRow = [];
                        var vornr = component.get('vornr');

                        var bemot = component.get('bemotRef');
                        var bemotText = bemot ? bemot.get('bemottxt') : '';

                        var matnr = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(component.get('matnr'));
                        var maktx = '';

                        if (component.get('material')) {
                            maktx = component.get('material').get('maktx');
                        }
                        var date = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(component.get('isdd'));

                        var consumption = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(component.get('meins')) + ' ' + component.get('enmng');

                        contentRow[0] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(vornr) ? { text: vornr, style: rowStyle } : "";
                        contentRow[1] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(bemotText) ? { text: bemotText, style: rowStyle } : "";
                        contentRow[2] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr) ? { text: matnr, style: rowStyle } : "";
                        contentRow[3] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(maktx) ? { text: maktx, style: rowStyle } : "";
                        contentRow[4] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(date) ? { text: date, style: rowStyle } : "";
                        contentRow[5] = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(consumption) ? { text: consumption, style: rowStyle } : "";

                        doc.docDefinition.content[indexOfLastElement].table.body.push(contentRow);

                    }, this);
                }
                ////end content
            }

            return doc;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeComponents of BaseOrderReportLayout', ex);
        }
    }

});
