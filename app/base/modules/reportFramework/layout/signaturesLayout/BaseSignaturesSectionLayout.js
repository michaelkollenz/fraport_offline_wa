Ext.define('AssetManagement.base.modules.reportFramework.layout.signaturesLayout.BaseSignaturesSectionLayout', {
  mixins: ['Ext.mixin.Observable'],

  requires: [
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.controller.EventController',
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.manager.MaterialManager'
  ],

  inheritableStatics: {
    //private:
    _instance: null,

    //public:
    getInstance: function () {
      if (this._instance == null) {
        this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.layout.signaturesLayout.SignaturesSectionLayout');
      }

      return this._instance;
    }
  },

  _reportLocale: null,

  //global style constans
  _labelColor: '#0046AD',
  _textColor: '#787878',
  _sectionHeaderColor: '#787878',

  buildSigntureSection: function (signatures, reportLanguage, object) {
    try {

      var me = this;

      //return me.writeOperationHeaderData(object, doc, reportLang, opCosts);
      var reportLanguageCallback = function () {
        try {
          me._newLangLoaded = true;
          return me.buildSection(signatures, reportLanguage, object);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSigntureSection-reportLanguageCallback of BaseSignaturesSectionLayout', ex);
        }
      }
      // create report locale
      if (!this._reportLocale) {
        this._reportLocale = $.extend(true, {}, Locale);
        this._newLangLoaded = true;
      }

      //this.prepareSummaryReport(object, doc, opCosts);

      if (reportLanguage && reportLanguage !== this._reportLocale['currentLang']) {
        this._newLangLoaded = false;
        this._reportLocale.setCurrentLanguage(reportLanguage);
        this._reportLocale.loadAsync(reportLanguageCallback);

      } else {
        if (this._newLangLoaded === false) {
          Ext.defer(me.buildSigntureSection, 1, me, [signatures, reportLanguage, object]);
        } else {
          return me.buildSection(signatures, reportLanguage, object);
        }
      }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildSigntureSection of BaseSignaturesSectionLayout', ex)
    }
  },

  buildSection: function (signatures, reportLang, object) {
    try {

      var signaturesSection = {}

      // var object = object['mainObject'];
      var kostl = object.get('kostl');
      var persNo = object.get('persNo');

      if (signatures && signatures.length > 0) {

        var customerSignature = null;
        var technicianSignature = null;
        var engineerSignature = null;
        var additionalSignature = null;

        for (var i = 0; i < signatures.length; i++) {
          if (signatures[i].signatureObject && signatures[i].signatureObject['sign1Required']) {
            customerSignature = signatures[i].signatureObject
          } else if (signatures[i].signatureObject && signatures[i].signatureObject['sign2Required']) {
            technicianSignature = signatures[i].signatureObject
          } else if (signatures[i].signatureObject && signatures[i].signatureObject['sign3Required']) {
            engineerSignature = signatures[i].signatureObject
          } else if (signatures[i].signatureObject && signatures[i].signatureObject['sign4Required']) {
            additionalSignature = signatures[i].signatureObject;
          }
        }

        var reportLanguage = signatures[0]['signatureLanguage'] ? signatures[0]['signatureLanguage'] : Locale;


        signaturesSection = {
          id: 'signature',
          columns: [
            {
              width: '*',
              stack: [{text: ' '}, {text: ' '}, {text: ' '},
                {
                  text: AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(
                    AssetManagement.customer.utils.DateTimeUtils.getCurrentTime())
                },
                {
                  canvas: [{
                    type: 'line', x1: 0, y1: 5, x2: 100, y2: 5, lineWidth: 0.75
                  }]
                },
                {text: reportLanguage.getMsg('date')}]
            },
            {
              width: '*',
              stack: [{text: ' '}, {text: ' '}, {text: ' '},
                {text: kostl},
                {
                  canvas: [{
                    type: 'line', x1: 0, y1: 5, x2: 100, y2: 5, lineWidth: 0.75
                  }]
                },
                {text: reportLanguage.getMsg('costCenter')}]
            },
            {
              width: '*',
              stack: [{text: ' '}, {text: ' '}, {text: ' '},
                {text: persNo},
                {
                  canvas: [{
                    type: 'line', x1: 0, y1: 5, x2: 100, y2: 5, lineWidth: 0.75
                  }]
                },
                {text: reportLanguage.getMsg('personalNumber')}]
            },
            {
              width: '*',
              stack: []
            }]
          // columnGap: 256
        };


        if (customerSignature) {

          signaturesSection.columns[3].stack.push({
            table: {
              body: [
                [{
                  image: customerSignature['signature'],
                  width: 120,
                  height: 50
                }]
              ]
            }
          });

          //add the phrase for customers signature
          signaturesSection.columns[3].stack.push({
            text: reportLanguage.getMsg('signature'),
            style: {
              // fontSize: 10,
              // bold: true
            },
            margin: [0, 10, 0, 0]
          });

        }

        if (engineerSignature) {

        }
      }
      return signaturesSection;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareSignatureSection of ReportsPreviewPageController', ex);
    }

  },

  prepareEmailAddressesSection: function (emailAddress) {
    try {
      var singleEmail = {
        text: emailAddress.get('email'),
        style: {
          fontSize: 9,
          normal: true
        }
      };

      return singleEmail;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareEmailAddressesSection of ReportsPreviewPageController', ex);
    }
  }
});
