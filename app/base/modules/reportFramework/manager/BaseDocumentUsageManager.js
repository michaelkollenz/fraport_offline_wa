﻿Ext.define('AssetManagement.base.modules.reportFramework.manager.BaseDocumentUsageManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.modules.reportFramework.model.DocumentUsage',
        'AssetManagement.customer.modules.reportFramework.manager.DocumentDataManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

    _logoName : 'pdf_logo.gif',

    inheritableStatics: {

        //public methods
        getDocumentsTypes: function (useBatchProcessing) {
            var retval = -1;

            try {
                var me = this;

                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.reportFramework.model.DocumentUsage',
                    autoLoad: false
                });

                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildDocumentsTypesStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            fromDataBase = me.loadDependendData.call(me, fromDataBase, retval);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsTypes of BaseDocumentUsageManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_047', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_047', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMaterials of BaseDocumentUsageManager', ex);
            }

           return retval;
        },

        buildDocumentsTypesStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var docType = this.buildDocumentsTypesFromDbResultObject(cursor.value);
                        store.add(docType);
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialStoreFromDataBaseQuery of BaseDocumentUsageManager', ex);
            }
        },

        _makeId: function () {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            for (var i = 0; i < 18; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },

        buildDocumentsTypesFromDbResultObject: function (dbResult) {
            var retval = null;
            var me = this;
            
            try {
                retval = Ext.create('AssetManagement.customer.modules.reportFramework.model.DocumentUsage', {
                    userId: dbResult['USERID'],
                    dirty: dbResult['DIRTY'],
                    updflag: dbResult['UPDLFLAG'],
                    scenario: dbResult['SCENARIO'],
                    objectType: dbResult['USAGE_BO'],
                    reportId: dbResult['REPORT_ID'] ? dbResult['REPORT_ID'].toString() : '',
                    templateId: dbResult['TEMPLATE_ID'] ? dbResult['TEMPLATE_ID'].toString() : '',
                    fieldName1: dbResult['OBJECT_PARAM_NAME_1'],
                    objectParam1: dbResult['OBJECT_PARAM_1'],
                    fieldName2: dbResult['OBJECT_PARAM_NAME_2'],
                    objectParam2: dbResult['OBJECT_PARAM_2'],
                    fieldName3: dbResult['OBJECT_PARAM_NAME_3'],
                    objectParam3: dbResult['OBJECT_PARAM_3'],
                    fieldName4: dbResult['OBJECT_PARAM_NAME_4'],
                    objectParam4: dbResult['OBJECT_PARAM_4']
                });

                retval.set('id', me._makeId());

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialFromDbResultObject of BaseDocumentUsageManager', ex);
            }

            return retval;
        },

        loadDependendData: function (store, retval) {
            try {

                var me = this;
                var storeItemsLength = store.getData().getCount();
                var index = 0;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var allProcessed = function () {

                    me.loadLogos(retval, store);

                    //eventController.requestEventFiring(retval, store);
                }

                var obtainDocumentsDetails = function (index) {

                    if (index < storeItemsLength) {
                        if (store.data.items[index]) {

                            var docType = store.data.items[index];
                            var documentData = null;

                            var eventController = AssetManagement.customer.controller.EventController.getInstance();
                            var done = 0;
                            var counter = 0;
                            var erroroccurred = null;

                            var completeFunction = function () {
                                if (erroroccurred === true) {
                                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(retval, undefined);
                                } else if (counter === done && erroroccurred === false) {
                                    me.assignDependendData.call(me, retval, docType, documentData);
                                    index++;
                                    return obtainDocumentsDetails(index);
                                }
                            };

                            if (true) {
                                done++;

                                var docDataSuccessCallback = function (docData) {
                                    erroroccurred = docData === undefined;

                                    documentData = docData;
                                    counter++;

                                    completeFunction();
                                };

                                var eventId = AssetManagement.customer.modules.reportFramework.manager.DocumentDataManager.getDocumentsData(false);
                                eventController.registerOnEventForOneTime(eventId, docDataSuccessCallback, me);
                            }

                            if (done > 0) {
                                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                            } else {
                                completeFunction();
                            }

                        }
                    } else {
                        allProcessed();
                    }
                }

                obtainDocumentsDetails(index);


            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependentData of BaseDocumentDataManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);

            }
        },

        assignDependendData: function (eventIdToFireWhenComplete, docType, docData) {
            try {

                if (docData) {
                    docData.each(function (item) {
                        if (item.get('reportId') === docType.get('reportId')) {

                            docType.set('docData', item);
                        }
                    });
                }

                return docType;

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendData of BaseDocumentusageManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        },


        obtainDefaultEmailAddress: function (retval, store) {
            try{

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var innerCallback = function (customers) {
                    try {
                        if (customers) {

                        }

                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadHeaderLogo of BaseDocumentUsageManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var eventId = AssetManagement.customer.manager.CustomerManager.getCustomers(true, false);
                if (eventId > -1) {
                    evenController.registerOnEventForOneTime(eventId, innerCallback);
                }

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainDefaultEmailAddress of BaseDocumentUsageManager', ex)
            }
        },

        loadLogos : function(retval, store){
            try{
                var amountOfLogos = this.logos.length;
                var counter = 0;

                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var obtainLogo = function(counter){
                    try{
                        if(counter < amountOfLogos){
                            var innerXCallback = function (data) {
                                try {
                                    if(data){
                                        var logo = data;

                                        store.add({
                                            'logo': logo,
                                            'logoName': me.logos[counter]
                                        });

                                        counter++;
                                        obtainLogo(counter);
                                    }

                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLogos of DocumentUsageManager', ex);
                                    eventController.fireEvent(retval, undefined);
                                }
                            };

                            var logoName = me.logos[counter];
                            me.dataRequest('resources/icons/' + logoName, innerXCallback);
                        } else{
                            eventController.fireEvent(retval, store);
                        }
                    } catch(ex){
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLogos of DocumentUsageManager', ex);
                    }
                }

                obtainLogo(counter);
            }  catch(ex){
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLogos of DocumentUsageManager', ex);
            }
        },


        dataRequest : function (url, callback){
            try{
                var xhr = new XMLHttpRequest();
                xhr.responseType = 'blob';
                xhr.onload = function() {
                    var reader  = new FileReader();
                    reader.onloadend = function () {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.send();

            } catch(ex){
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataRequest of DocumentUsageManager', ex);
            }
        }
    }
});
