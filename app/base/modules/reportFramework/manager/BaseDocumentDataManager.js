﻿Ext.define('AssetManagement.base.modules.reportFramework.manager.BaseDocumentDataManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.modules.reportFramework.model.DocumentData',
        'AssetManagement.customer.modules.reportFramework.manager.DocumentLanguageManager',
        'AssetManagement.customer.modules.reportFramework.manager.DocumentTargetManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store',
        'AssetManagement.customer.manager.TextComponentManager'
    ],

    inheritableStatics: {


        //public methods
        getDocumentsData: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.reportFramework.model.DocumentData',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildDocumentsDataStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
                        if (done) {
                            fromDataBase = me.loadDependentData.call(me, fromDataBase, retval);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsData of BaseDocumentDataManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_045', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_045', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsData of BaseDocumentDataManager', ex);
            }

            return retval;
        },

        buildDocumentsDataStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var docData = this.buildDocumentsDataFromDbResultObject(cursor.value);
                        store.add(docData);
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialStoreFromDataBaseQuery of BaseDocumentDataManager', ex);
            }
        },

        buildDocumentsDataFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.modules.reportFramework.model.DocumentData', {
                    userId: dbResult['USERID'],
                    dirty: dbResult['DIRTY'],
                    updflag: dbResult['UPDLFLAG'],
                    scenario: dbResult['SCENARIO'],
                    reportId: dbResult['REPORT_ID'] ? dbResult['REPORT_ID'].toString() : '',
                    templateId: dbResult['TEMPLATE_ID'] ? dbResult['TEMPLATE_ID'].toString() : '',
                    reportName: dbResult['REPORT_NAME'],
                    templateName: dbResult['TEMPLATE_NAME'],
                    allowMultiObj: dbResult['ALLOW_MULT_OBJ'],
                    summaryReport: dbResult['SUMMARY_REPORT'],
                    displaytabs: dbResult['DISPLAY_TABS'],
                    sign1Name: dbResult['SIGNATURE1_NAME'],
                    sign1Required: dbResult['SIGNATURE1_REQUIRED'],
                    sign1CustName: dbResult['SIGNATURE1_CUSTNAME'],
                    sign1CustNameRequired: dbResult['SIGNATURE1_CUSTNAME_REQ'],
                    sign1CustNamePos: dbResult['SIGNATURE1_CUSTNAME_POS'],
                    sign2Name: dbResult['SIGNATURE2_NAME'],
                    sign2Required: dbResult['SIGNATURE2_REQUIRED'],
                    sign2CustName: dbResult['SIGNATURE2_CUSTNAME'],
                    sign2CustNameRequired: dbResult['SIGNATURE2_CUSTNAME_REQ'],
                    sign2CustNamePos: dbResult['SIGNATURE2_CUSTNAME_POS'],
                    sign3Name: dbResult['SIGNATURE3_NAME'],
                    sign3Required: dbResult['SIGNATURE3_REQUIRED'],
                    sign3CustName: dbResult['SIGNATURE3_CUSTNAME'],
                    sign3CustNameRequired: dbResult['SIGNATURE3_CUSTNAME_REQ'],
                    sign3CustNamePos: dbResult['SIGNATURE3_CUSTNAME_POS'],
                    sign4Name: dbResult['SIGNATURE4_NAME'],
                    sign4Required: dbResult['SIGNATURE4_REQUIRED'],
                    sign4CustName: dbResult['SIGNATURE4_CUSTNAME'],
                    sign4CustNameRequired: dbResult['SIGNATURE4_CUSTNAME_REQ'],
                    sign4CustNamePos: dbResult['SIGNATURE4_CUSTNAME_POS'],
                    numEmail: dbResult['NUM_EMAIL'],
                    defaultCustEmail: dbResult['DEFAULT_CUST_EMAIL'],
                    customerParentRole: dbResult['CUSTOMER_PARTER_ROLE'],
                    partnerSourceBo: dbResult['PARTNER_SOURCE_BO'],
                    defaultUserEmail: dbResult['DEFAULT_USER_EMAIL'],
                    txnam_kop: dbResult['TXNAM_KOP'],
                    txnam_fus: dbResult['TXNAM_FUS'],
                    tdpageform: dbResult['TDPAGEFORM'],
                    tdpageortn: dbResult['TDPAGEORTN']

                });

                retval.set('id', dbResult['REPORT_ID']);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDocumentsDataFromDbResultObject of BaseDocumentDataManager', ex);
            }

            return retval;
        },

        loadDependentData: function (store, retval) {
            try {

                var me = this;

                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                var storeItemsLength = store.getData().getCount();
                var index = 0;

                var allProcessed = function () {
                    eventController.requestEventFiring(retval, store);
                }

               var obtainDocumentsDetails = function (index) {
                    if (index < storeItemsLength) {
                        if (store.data.items[index]) {

                            var docData = store.data.items[index];
                            var documentTarget = null;
                            var documentLangs = null;
                            var footerTextsString = null;
                            var headerTextsString = null;
                            var done = 0;
                            var counter = 0;
                            var erroroccurred = false;

                            var completeFunction = function () {
                                if (erroroccurred === true) {
                                    AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
                                } else if (counter === done && erroroccurred === false) {
                                    store.data.items[index] = me.assignDependendData.call(me, retval, docData, documentLangs, documentTarget, headerTextsString, footerTextsString);
                                    index++;
                                    return obtainDocumentsDetails(index);
                                }
                            };

                            if (true) {
                                done++;

                                var documentTargetSuccessCallback = function (docTarget) {
                                    erroroccurred = docTarget === undefined;

                                    documentTarget = docTarget;
                                    counter++;

                                    completeFunction();
                                };

                                var eventId = AssetManagement.customer.modules.reportFramework.manager.DocumentTargetManager.getDocumentsTargets(false);
                                eventController.registerOnEventForOneTime(eventId, documentTargetSuccessCallback, me);
                            }

                            if (true) {
                                done++;

                                var documentLangsSuccessCallback = function (docLangs) {
                                    erroroccurred = docLangs === undefined;

                                    documentLangs = docLangs;
                                    counter++;

                                    completeFunction();
                                };

                                var eventId = AssetManagement.customer.modules.reportFramework.manager.DocumentLanguageManager.getDocumentsLangs(false);
                                eventController.registerOnEventForOneTime(eventId, documentLangsSuccessCallback, me);
                            }

                            if (true) {
                                done++;

                                var documentFooterSuccessCallback = function (footerTexts) {
                                    erroroccurred = footerTexts === undefined;

                                    footerTextsString = footerTexts;
                                    counter++;

                                    completeFunction();
                                };

                                var footerTextId = docData.get('footerTextId');

                                var eventId = AssetManagement.customer.manager.TextComponentManager.getTextComponent('TEXT', 'ADRS', footerTextId, AssetManagement.customer.core.Core.getAppConfig().getCurrentLanguageShort());
                                eventController.registerOnEventForOneTime(eventId, documentFooterSuccessCallback, me);
                            }

                            if (true) {
                                done++;

                                var documentHeaderSuccessCallback = function (headerTexts) {
                                    erroroccurred = headerTexts === undefined;

                                    headerTextsString = headerTexts;
                                    counter++;

                                    completeFunction();
                                };

                                var headerTextId = docData.get('headerTextId');

                                var eventId = AssetManagement.customer.manager.TextComponentManager.getTextComponent('TEXT', 'ADRS', headerTextId, AssetManagement.customer.core.Core.getAppConfig().getCurrentLanguageShort());
                                eventController.registerOnEventForOneTime(eventId, documentHeaderSuccessCallback, me);
                            }

                            if (done > 0) {
                                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
                            } else {
                                completeFunction();
                            }

                        }

                    } else {
                        allProcessed();
                    }
                }

                obtainDocumentsDetails(index);


            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependentData of BaseDocumentDataManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);

            }
        },


        assignDependendData: function (eventIdToFireWhenComplete, docData, documentLangs, documentTarget, headerTextsString ,footerTextsString) {
            try {
                if (documentLangs) {
                    var docLang = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.modules.reportFramework.model.DocumentLanguage',
                        autoLoad: false
                    });
                    documentLangs.each(function (item) {
                        if (item.get('reportId') === docData.get('reportId')) {
                            docLang.add(item);
                        }
                    });
                    docData.set('docLang', docLang);
                }

                if (documentTarget) {
                    var docTarget = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.modules.reportFramework.model.DocumentTarget',
                        autoLoad: false
                    });
                    documentTarget.each(function (item) {
                        if (item.get('reportId') === docData.get('reportId')) {
                            docTarget.add(item);
                        }
                    });
                    docData.set('docTarget', docTarget);
                }

                if(footerTextsString){
                    docData.set('footerContent', footerTextsString);
                }

                if(headerTextsString){
                    docData.set('headerContent', headerTextsString);
                }

                return docData;

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignDependendData of BaseDocumentusageManager', ex);
                AssetManagement.customer.controller.EventController.getInstance().requestEventFiring(eventIdToFireWhenComplete, undefined);
            }
        }
    }


});
