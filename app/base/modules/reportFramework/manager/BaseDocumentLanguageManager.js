Ext.define('AssetManagement.base.modules.reportFramework.manager.BaseDocumentLanguageManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.modules.reportFramework.model.DocumentLanguage',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

    inheritableStatics: {


        //public methods
        getDocumentsLangs: function (useBatchProcessing) {
            var retval = -1;

            try {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.reportFramework.model.DocumentLanguage',
                    autoLoad: false
                });

                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildDocumentsLangsStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, fromDataBase);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsLangs of BaseDocumentLanguageManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_048', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_048', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsLangs of BaseDocumentLanguageManager', ex);
            }

            return retval;
        },

        buildDocumentsLangsStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var docType = this.buildDocumentsLangsFromDbResultObject(cursor.value);
                        store.add(docType);
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDocumentsLangsStoreFromDataBaseQuery of BaseDocumentLanguageManager', ex);
            }
        },

        _makeId: function () {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            for (var i = 0; i < 18; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },
        
        buildDocumentsLangsFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.modules.reportFramework.model.DocumentLanguage', {
                    userId: dbResult['USERID'],
                    dirty: dbResult['DIRTY'],
                    updflag: dbResult['UPDFLAG'],
                    scenario: dbResult['SCENARIO'],
                    lang: dbResult['LANGU'],
                    reportId: dbResult['REPORT_ID'],
                    templateId: dbResult['TEMPLATE_ID']
                });

                retval.set('id', this._makeId());

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildDocumentsLangsFromDbResultObject of BaseDocumentLanguageManager', ex);
            }

            return retval;
        }
    }
});