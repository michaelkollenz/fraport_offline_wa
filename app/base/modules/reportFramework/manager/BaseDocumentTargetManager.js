Ext.define('AssetManagement.base.modules.reportFramework.manager.BaseDocumentTargetManager', {
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.modules.reportFramework.model.DocumentTarget',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

    inheritableStatics: {


        //public methods
        getDocumentsTargets: function (useBatchProcessing, retval) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();
                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.modules.reportFramework.model.DocumentTarget',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildDocumentsTargetsStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);
                        if (done) {
                            

                            eventController.requestEventFiring(retval, fromDataBase);
                        }
                        

                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsTypes of BaseDocumentTargetManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };
                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_AM_CUST_046', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_AM_CUST_046', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                retval = -1;
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDocumentsTargets of BaseDocumentTargetManager', ex);
            }

            return retval;
        },

        buildDocumentsTargetsStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var docType = this.buildDocumentsTargetsFromDbResultObject(cursor.value);
                        store.add(docType);
                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialStoreFromDataBaseQuery of BaseDocumentTargetManager', ex);
            }
        },
        
        _makeId: function () {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            for (var i = 0; i < 18; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },

        buildDocumentsTargetsFromDbResultObject: function (dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.modules.reportFramework.model.DocumentTarget', {
                    userId: dbResult['USERID'],
                    dirty: dbResult['DIRTY'],
                    updflag: dbResult['UPDLFLAG'],
                    scenario: dbResult['SCENARIO'],
                    targetObject: dbResult['TARGET_BO'],
                    reportId: dbResult['REPORT_ID'],
                    seqnr: dbResult['SEQNR'],
                    templateId: dbResult['TEMPLATE_ID']
                });

                retval.set('id', this._makeId());

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMaterialFromDbResultObject of BaseDocumentTargetManager', ex);
            }

            return retval;
        }
    }
});