Ext.define('AssetManagement.base.modules.reportFramework.dataHandler.BaseDeliveryReportDataHandler', {


    mixins: ['Ext.mixin.Observable'],

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.IAReasonManager',
        'AssetManagement.customer.manager.OrderReportManager',
        'AssetManagement.customer.manager.ComponentManager',
        'AssetManagement.customer.controller.EventController'

    ],

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.dataHandler.DeliveryReportDataHandler');
            }

            return this._instance;
        }
    },

    obtainOrder: function (object) {
        var retval = -1;

        try {
            var that = this;

            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            var retval = eventController.getNextEventId();

            var callback = function (order) {
                try {
                    // if (order) {
                    //     order.set('layoutType', object.get('layoutType'));
                    //     that.loadTimeConfs(retval, order);
                    // }
                  var toReturn = {'mainObject': object}
                  eventController.fireEvent(retval, toReturn);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainOrder of BaseDeliveryReportDataHandler', ex);
                }
            }
            var orderRequest = AssetManagement.customer.manager.IAReasonManager.getAllIAReason(false);

            if (orderRequest > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(orderRequest, callback);
            } else {
                this.errorOccured();
            }


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainOrder of BaseDeliveryReportDataHandler', ex);
        }

        return retval;

    }
});
