Ext.define('AssetManagement.base.modules.reportFramework.dataHandler.BaseChecklistReportDataHandler', {


    mixins: ['Ext.mixin.Observable'],

    requires: [
      'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.controller.EventController',
      'AssetManagement.customer.manager.QPlosManager',
      'AssetManagement.customer.model.bo.ChecklistViewChar'

    ],

    _operations : [],

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.dataHandler.ChecklistReportDataHandler');
            }

            return this._instance;
        }
    },

    obtainChecklistData: function(prueflos){
        var retval = -1;

        try {
            var me = this;

            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            var retval = eventController.getNextEventId();
            var qplos = prueflos;

            var gatherRelevantChecklistData = function (qplos) {
                try {
                    if (qplos) {
                        //var qplosCloned = qplos.copy(null);
                        if (!qplos.get('layoutType'))
                            qplos.set('layoutType', prueflos.get('layoutType'));

                        var qplosViewChars = me.generateViewChars(qplos);

                        var aufnr = qplos.get('aufnr');
                        var getOrderCallback = function (order) {
                            try {
                                if (order) {
                                    var toReturn = {
                                        mainObject: qplos,
                                        qplosViewChars: qplosViewChars,
                                        parentBO: order
                                    }
                                    eventController.fireEvent(retval, toReturn);
                                } else {
                                    eventController.fireEvent(retval, undefined);
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainChecklistData of BaseChecklistReportDataHandler', ex);
                            }
                        }

                        var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
                        if (eventId > -1) {
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(orderRequest, getOrderCallback, me);
                        }
                   } else {
                       eventController.fireEvent(retval, undefined);
                   }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainChecklistData of BaseChecklistReportDataHandler', ex);
                }
            }

            var prueflosNumber = prueflos.get('prueflos');

            var eventId = AssetManagement.customer.manager.QPlosManager.getQPlos(prueflosNumber, false);
            if (eventId > -1) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, gatherRelevantChecklistData, me);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainChecklistData of BaseChecklistReportDataHandler', ex);
        }

        return retval;
    },

    generateViewChars: function (qplosCloned) {
        try {
            var viewCharStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ChecklistViewChar',
                autoLoad: false,
                groupField: 'qpOpertxt'
            });

            var date = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(); //getter for current date, that will be used as a start date and time, if there are no results

            var qplosOperStore = qplosCloned.get('qplosOpers');
            if (qplosOperStore && qplosOperStore.getCount() > 0) {
                qplosOperStore.each(function (qplosOper) {
                    var qplosCharStore = qplosOper.get('qplosChars');

                    if (qplosCharStore && qplosCharStore.getCount() > 0) {
                        qplosCharStore.each(function (qplosChar) {
                            var qpresult = qplosChar.get('qpResult');
                            //viewChars are a dummy model, not assigned to a qplos Char, but created based on it, for the page runtime. 
                            //viewChars are used as display records in the grid. Also, any user input gets to viewChars first.
                            //therefore, any changes in the concept of  the checklist must be included in viewChars
                            var qplosViewChar = Ext.create('AssetManagement.customer.model.bo.ChecklistViewChar', {
                                kzeinstell: qplosChar.get('kzeinstell'),
                                steuerkz: qplosChar.get('steuerkz'),
                                kurztext: qplosChar.get('kurztext'),
                                katab1: qplosChar.get('katab1'),
                                katalgart1: qplosChar.get('katalgart1'),
                                auswmenge1: qplosChar.get('auswmenge1'),
                                auswmgwrk1: qplosChar.get('auswmgwrk1'),
                                katab2: qplosChar.get('katab2'),
                                katalgart2: qplosChar.get('katalgart2'),
                                auswmenge2: qplosChar.get('auswmenge2'),
                                auswmgwrk2: qplosChar.get('auswmgwrk2'),
                                sollwert: qplosChar.get('sollwert'),
                                toleranzob: qplosChar.get('toleranzob'),
                                toleranzun: qplosChar.get('toleranzun'),
                                masseinhsw: qplosChar.get('masseinhsw'),
                                stellen: qplosChar.get('stellen'),
                                char_type: qplosChar.get('char_type'),
                                vorglfnr: qplosChar.get('vorglfnr'),
                                merknr: qplosChar.get('merknr'),
                                codes: qplosChar.get('codes'),
                                codeMeanings: qplosChar.get('codeMeanings'),
                                category: qplosChar.get('category'),

                                qpOpertxt: qplosOper.get('ltxa1'),
                                qpOperVornr: qplosOper.get('vornr')

                            });

                            if (qpresult) {

                                qplosViewChar.set('meanValue', qpresult.get('meanValue'));
                                qplosViewChar.set('remark', qpresult.get('remark'));
                                qplosViewChar.set('auswmgwrk2', qpresult.get('code1'));
                                qplosViewChar.set('auswmenge2', qpresult.get('codegrp1'));
                                qplosViewChar.set('char_attr', qpresult.get('char_attr'));
                                qplosViewChar.set('closed', qpresult.get('closed'));
                                qplosViewChar.set('startDate', qpresult.get('startDate'));
                                qplosViewChar.set('startTime', qpresult.get('startTime'));
                                //checkBox__ properties are first set based on the evaluation

                                if (qplosViewChar.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_CHECKBOX) {
                                    var codeMeanings = qplosViewChar.get('codeMeanings');
                                    var funktion = '';
                                    if (codeMeanings && codeMeanings.getCount() > 1) {
                                        codeMeanings.each(function (code) {
                                            if (code.get('code') === qpresult.get('code1') &&
                                                code.get('codegruppe') === qpresult.get('codegrp1')) {
                                                funktion = code.get('funktion');
                                            }
                                        });
                                    }

                                    qplosViewChar.set('checkBoxOK', funktion === 'OK')
                                    qplosViewChar.set('checkBoxNOK', funktion === 'NOK');
                                    qplosViewChar.set('checkBoxNREL', funktion === 'NA');

                                }
                                else {
                                    qplosViewChar.set('checkBoxOK', qpresult.get('evaluation') === 'A');
                                    qplosViewChar.set('checkBoxNOK', qpresult.get('evaluation') === 'R');
                                    qplosViewChar.set('checkBoxNREL', qpresult.get('evaluation') === 'N');
                                }
                            }
                            else {
                                
                                qplosViewChar.set('startDate',date);
                                qplosViewChar.set('startTime',date);
                            }
                            
                            viewCharStore.add(qplosViewChar);
                        });
                    }
                });
            }

            return viewCharStore
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateViewChar of BaseChecklistReportDataHandler', ex);
        }
    }
});