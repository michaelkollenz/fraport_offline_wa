Ext.define('AssetManagement.base.modules.reportFramework.dataHandler.BaseOperationReportDataHandler', {


    mixins: ['Ext.mixin.Observable'],

    requires: [
      'AssetManagement.customer.helper.OxLogger',
      'AssetManagement.customer.controller.EventController',
      'AssetManagement.customer.manager.OperationManager',
      'AssetManagement.customer.manager.OrderManager'

    ],

    _operations : [],


    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.dataHandler.OperationReportDataHandler');
            }

            return this._instance;
        }
    },

    obtainOperationsData: function (operation) {
        var retval = -1;

        try{
            var me = this;

            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            var retval = eventController.getNextEventId();
            
            var gatherRelevantOperations = function (dbOperations) {
                try {
                    if (operation) {
                        if(dbOperations && dbOperations.getCount() > 0){
                            dbOperations.each(function(dbOperation){
                                if (operation.get('vornr') === dbOperation.get('vornr') &&
                                    operation.get('aufnr') === dbOperation.get('aufnr')) {
                                        dbOperation.set('layoutType', operation.get('layoutType'))
                                        me.loadDependantData(retval, dbOperation);
                                        return false;
                            }
                            });
                        } else {
                            eventController.fireEvent(retval, undefined);
                        }
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside retrieveOperationData of BaseOperationReportDataHandler', ex);

                }
            }

            var eventId = AssetManagement.customer.manager.OperationManager.getOperations(true, false);

            if (eventId > -1) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, gatherRelevantOperations, me);
            } 
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainOperationsData of BaseOperationReportDataHandler', ex);
        }

        return retval;
    },

    loadDependantData: function (retval, operation) {
        try {
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var aufnr = operation.get('aufnr');

            //get order-object
            var loadedOrderCallback = function (order) {
                try {
                    if (order) {
                            objItem = AssetManagement.customer.manager.ObjectListManager.getObjectListItemForOperation(order, operation);
                            operation.set('objItem', objItem);
                            operation.set('order', order);
                            var toReturn = {
                                mainObject: operation
                            }
                            eventController.fireEvent(retval, toReturn);
                    } else {
                        eventController.fireEvent(retval, undefined);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependantData of BaseOperationReportDataHandler', ex);
                }
            };

            var eventOrderId = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);

            if (eventOrderId > -1) {
                eventController.registerOnEventForOneTime(eventOrderId, loadedOrderCallback);
            } else {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependantData of BaseOperationReportDataHandler', ex);
                eventController.fireEvent(retval, undefined);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependantData of BaseOperationReportDataHandler', ex)
        }
    }
});