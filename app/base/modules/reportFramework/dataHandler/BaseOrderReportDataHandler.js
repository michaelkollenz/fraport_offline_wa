Ext.define('AssetManagement.base.modules.reportFramework.dataHandler.BaseOrderReportDataHandler', {


    mixins: ['Ext.mixin.Observable'],

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.OrderManager',
        'AssetManagement.customer.manager.OrderReportManager',
        'AssetManagement.customer.manager.ComponentManager',
        'AssetManagement.customer.controller.EventController'

    ],

    inheritableStatics: {
        //private:
        _instance: null,

        //public:
        getInstance: function () {
            if (this._instance == null) {
                this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.dataHandler.OrderReportDataHandler');
            }

            return this._instance;
        }
    },

    obtainOrder: function (object) {
        var retval = -1;

        try {
            var that = this;

            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            var retval = eventController.getNextEventId();

            var callback = function (order) {
                try {
                    if (order) {
                        order.set('layoutType', object.get('layoutType'));
                        that.loadTimeConfs(retval, order);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainOrder of BaseOrderReportDataHandler', ex);
                }
            }
            var aufnr = object.get('aufnr');
            var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, false);

            if (orderRequest > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(orderRequest, callback);
            } else {
                this.errorOccured();
            }


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside obtainOrder of BaseOrderReportDataHandler', ex);
        }

        return retval;

    },

    loadTimeConfs: function (retval, order) {

        try {
            if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ext_scen_active') === 'X') {
                retval = this.loadPreparedTimeConfsForExternals(retval, order);
            } else {
                var me = this;
                var eventController = AssetManagement.customer.controller.EventController.getInstance();

                var timeConfs = null;
                var operations = order.get('operations');

                if (operations && operations.getCount() > 0) {
                    //extract timeConfs
                    var arrayOfTimeConfStores = [];

                    operations.each(function (operation) {
                        arrayOfTimeConfStores.push(operation.get('timeConfs'));
                    });

                    timeConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfTimeConfStores);
                }


                if (timeConfs && timeConfs.getCount() > 0) {
                    var bemotAssignmentCallback = function (bemots) {
                        try {
                            if (bemots === undefined) {
                                retval = -1;
                                eventController.fireEvent(retval, undefined);
                                return;
                            }

                            var preparedTimeConfs = null;

                            if (bemots) {
                                var preparedTimeConfs = Ext.create('Ext.data.Store', {
                                    model: 'AssetManagement.customer.model.bo.TimeConf',
                                    autoLoad: false
                                });

                                timeConfs.each(function (timeConf) {
                                    var clone = Ext.create('AssetManagement.customer.model.bo.TimeConf', {
                                        vornr: timeConf.get('vornr'),
                                        ismne: timeConf.get('ismne'),
                                        ismnw: timeConf.get('ismnw'),
                                        bemot: timeConf.get('bemot'),
                                        learr: timeConf.get('learr'),
                                        activityType: timeConf.get('activityType'),
                                        ltxa1: timeConf.get('ltxa1'),
                                        ied: timeConf.get('ied')
                                    });

                                    preparedTimeConfs.add(clone);

                                    bemots.each(function (bemot) {
                                        if (bemot.get('bemot') === timeConf.get('bemot')) {
                                            clone.set('bemotRef', bemot);
                                            return false;
                                        }
                                    }, this);
                                });
                            } else {
                                preparedTimeConfs = timeConfs;
                            }
                            order.timeConfs = preparedTimeConfs;
                            me.loadObjectList(retval, order);
                            //eventController.fireEvent(retval, order);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadTimeConfs of BaseBaseOrderReportDataHandler', ex);
                            eventController.fireEvent(retval, undefined);
                        }
                    };

                    var bemotRequestId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(false);
                    eventController.registerOnEventForOneTime(bemotRequestId, bemotAssignmentCallback);
                } else {
                    order.timeConfs = timeConfs;
                    me.loadObjectList(retval, order);
                    // eventController.fireEvent(retval, order);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadTimeConfs of BaseBaseOrderReportDataHandler', ex);
        }
    },

    loadPreparedTimeConfsForExternals: function (retval, order) {
        var retval = retval;

        try {
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();


            //external users does not use time confs, but banfs for confirmations
            var banfsSuccessCallback = function (banfItems) {
                try {
                    if (banfItems && banfItems.getCount() > 0) {
                        //map the banf positions into timeconf objects for the orderreport
                        var preparedTimeConfs = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.TimeConf',
                            autoLoad: false
                        });

                        banfItems.each(function (banfItem) {
                            var mapped = Ext.create('AssetManagement.customer.model.bo.TimeConf', {
                                //vornr: timeConf.get('vornr'),
                                ismne: banfItem.get('meins'),
                                ismnw: banfItem.get('menge'),
                                //bemot: timeConf.get('bemot'),
                                learr: banfItem.get('matnr'),
                                ltxa1: banfItem.get('txz01'),
                                ied: banfItem.get('badat')
                            });

                            if (banfItem.get('material')) {
                                var banfsMaterial = banfItem.get('material');

                                mapped.set('activityType', Ext.create('AssetManagement.customer.model.bo.ActivityType', {
                                    lstar: banfsMaterial.get('matnr'),
                                    ktext: banfsMaterial.get('maktx')
                                }));
                            }

                            preparedTimeConfs.add(mapped);
                        }, this);

                        order.timeConfs = preparedTimeConfs;
                        me.loadObjectList(retval, order);
                        //eventController.fireEvent(retval, order);
                    } else {
                        order.timeConfs = null;
                        me.loadObjectList(retval, order);
                        //eventController.fireEvent(retval, order);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfsForExternals of BaseOrderReportDataHandler', ex);
                    eventController.fireEvent(retval, undefined);
                }
            };

            var banfRequest = AssetManagement.customer.manager.BanfManager.getBanfPositions(order.get('aufnr'));
            eventController.registerOnEventForOneTime(banfRequest, banfsSuccessCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedTimeConfsForExternals of BaseOrderReportDataHandler', ex);
            retval = -1;
        }

        return retval;
    },

    loadObjectList: function (retval, order) {
        try {
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var objectList = order.get('objectList');

            if (!objectList || objectList.getCount() === 0) {
                order.objectList = null;
                //eventController.fireEvent(retval, order);
                me.loadMatConfs(retval, order);
            }

            var dataBaseRequests = 0;
            var completeCounter = 0;
            var errorOccured = false;
            var aborted = false;

            var preparedObjectList = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjectListItem',
                autoLoad: false
            });

            var requestReturned = function () {
                if (errorOccured === false && completeCounter === dataBaseRequests) {
                    order.set('objectList', preparedObjectList);
                    //eventController.fireEvent(retval, order);
                } else if (errorOccured === true && aborted === false) {
                    aborted = true;
                    order.set('objectList', null);
                    //eventController.fireEvent(retval, order);
                }
                me.loadMatConfs(retval, order);
            };


            objectList.each(function (objectListItem) {
                var clone = Ext.create('AssetManagement.customer.model.bo.ObjectListItem', {
                    equnr: objectListItem.get('equnr'),
                    eqtxt: objectListItem.get('eqtxt'),
                    matnr: objectListItem.get('matnr'),
                    maktx: objectListItem.get('maktx'),
                    equipment: objectListItem.get('equipment'),
                    material: objectListItem.get('material'),
                    vornr: ''
                });

                preparedObjectList.add(clone);

                var equnr = clone.get('equnr');
                var matnr = clone.get('matnr');
                var maktx = clone.get('maktx');
                var qmnumOfOli = objectListItem.get('ihnum');

                var loadEquiDataFromNotif = false;
                var loadMatDataFromNotif = false;

                //get the connected operation
                var operations = order.get('operations');

                if (operations && operations.getCount() > 0) {
                    var obknr = objectListItem.get('obknr').trim();
                    var obznr = objectListItem.get('obzae').trim();

                    operations.each(function (operation) {
                        if (operation.get('obknr') === obknr && operation.get('obzae') === obznr) {
                            clone.set('vornr', operation.get('vornr'));
                            return false;
                        }
                    });
                }


                //callback for database request on notif dependend information
                var notifDataCallback = function (notif) {
                    try {
                        errorOccured = notif === undefined;

                        if (notif) {
                            if (loadEquiDataFromNotif === true) {
                                clone.set('equnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(notif.get('equnr')));
                            }

                            if (loadMatDataFromNotif === true) {
                                clone.set('matnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(notif.get('matnr')));
                                clone.set('maktx', notif.get('maktx'));
                            }
                        }

                        completeCounter++;
                        requestReturned();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedObjectList of BaseBaseOrderReportDataHandler', ex);
                        errorOccured = true;
                        requestReturned();
                    }
                };

                //callback for material shorttext databse request
                var maktxDataCallback = function (material) {
                    try {
                        errorOccured = material === undefined;

                        if (material) {
                            clone.set('maktx', material.get('maktx'));
                        }

                        completeCounter++;
                        requestReturned();
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedObjectList of BaseOrderReportDataHandler', ex);
                        errorOccured = true;
                        requestReturned();
                    }
                };

                //get equnr
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                    clone.set('equnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(equnr));
                } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnumOfOli)) {
                    //try to get the equnr from the notif
                    loadEquiDataFromNotif = true;
                    dataBaseRequests++;

                    var notifRequestId = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnumOfOli, true);
                    eventController.registerOnEventForOneTime(notifRequestId, notifDataCallback);
                }

                //get material infos
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(matnr)) {
                    clone.set('matnr', AssetManagement.customer.utils.StringUtils.filterLeadingZeros(matnr));

                    //check, if there is any material shorttext given
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(maktx)) {
                        //get it from the database
                        dataBaseRequests++;

                        var maktxRequestId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr, true);
                        eventController.registerOnEventForOneTime(maktxRequestId, maktxDataCallback);
                    }
                } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnumOfOli)) {
                    //try to get the material data from the notif
                    loadMatDataFromNotif = true;

                    //only
                    if (loadEquiDataFromNotif === false) {
                        dataBaseRequests++;

                        var notifRequestId = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnumOfOli, true);
                        eventController.registerOnEventForOneTime(notifRequestId, notifDataCallback);
                    }
                }
            });

            if (dataBaseRequests === 0) {
                order.set('objectList', preparedObjectList);
                me.loadMatConfs(retval, order);

            } else {
                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadPreparedObjectList of BaseOrderReportDataHandler', ex)
            retval = -1;
        }
    },


    loadMatConfs: function (retval, order) {
        try {
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var matConfs = null;
            var operations = order.get('operations');

            if (operations && operations.getCount() > 0) {
                //extract matConfs
                var arrayOfMatConfStores = [];

                operations.each(function (operation) {
                    arrayOfMatConfStores.push(operation.get('matConfs'));
                });

                matConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfMatConfStores);
            }

            if (!matConfs || matConfs.getCount() === 0) {
                order.set('matConfs',  matConfs);
                //eventController.fireEvent(retval, order);
                this.loadComponents(retval, order);
            } else {

                var bemotAssignmentCallback = function (bemots) {
                    try {
                        if (bemots === undefined) {
                            order.set('matConfs', matConfs);
                            eventController.fireEvent(retval, {
                                mainObject: order
                            });
                            return;
                        }

                        var preparedMatConfs = null;

                        if (bemots) {
                            var preparedMatConfs = Ext.create('Ext.data.Store', {
                                model: 'AssetManagement.customer.model.bo.MaterialConf',
                                autoLoad: false
                            });

                            matConfs.each(function (matConf) {
                                var clone = Ext.create('AssetManagement.customer.model.bo.MaterialConf', {
                                    matnr: matConf.get('matnr'),
                                    vornr: matConf.get('vornr'),
                                    quantity: matConf.get('quantity'),
                                    unit: matConf.get('unit'),
                                    bemot: matConf.get('bemot'),
                                    material: matConf.get('material'),
                                    isdd: matConf.get('isdd')
                                });

                                preparedMatConfs.add(clone);

                                bemots.each(function (bemot) {
                                    if (bemot.get('bemot') === matConf.get('bemot')) {
                                        clone.set('bemotRef', bemot);
                                        return false;
                                    }
                                }, this);
                            });
                        } else {
                            preparedMatConfs = matConfs;
                        }

                        order.set('matConfs', preparedMatConfs);

                        me.loadComponents(retval, order);
                        //eventController.fireEvent(retval, toReturn);
                        //eventController.fireEvent(retval, preparedMatConfs);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMatConfs of BaseOrderReportDataHandler', ex);
                        order.set('matConfs', null);
                        eventController.fireEvent(retval, {
                            mainObject: order
                        });
                        //eventController.fireEvent(retval, undefined);
                    }
                };

                var bemotRequestId = AssetManagement.customer.manager.CustBemotManager.getCustBemots(false);
                eventController.registerOnEventForOneTime(bemotRequestId, bemotAssignmentCallback);
            }
        } catch (ex) {
            order.set('matConfs', null);
            eventController.fireEvent(retval, {
                mainObject: order
            });
        }
    },

    loadComponents: function(retval, order) {
        try {
            var me = this;
            var eventController = AssetManagement.customer.controller.EventController.getInstance();

            var componentsCallback = function(components) {
                try {
                    if(components){
                        order.set('components', components);
                        var toReturn = {
                            mainObject: order
                        }
                        eventController.fireEvent(retval, toReturn);
                    } else {
                        order.set('components', null);
                        eventController.fireEvent(retval, {
                            mainObject: order
                        });
                    }
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside componentsCallback of BaseOrderReportDataHandler', ex);
                }
            }

            var aufnr = order.get('aufnr');
            var eventId = AssetManagement.customer.manager.ComponentManager.getComponentsForOrder(aufnr, false);
            if(eventId > -1){
                eventController.registerOnEventForOneTime(eventId, componentsCallback);
            }else {
                order.set('components', null);
                eventController.fireEvent(retval, {
                    mainObject: order
                });
            }

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMatConfs of BaseOrderReportDataHandler', ex);
            order.set('components', null);
            eventController.fireEvent(retval, {
                mainObject: order
            });
        }
    }
});
