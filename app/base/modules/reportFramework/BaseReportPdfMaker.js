Ext.define('AssetManagement.base.modules.reportFramework.BaseReportPdfMaker', {

  mixins: ['Ext.mixin.Observable'],

  requires: [
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.controller.EventController',
    'AssetManagement.customer.modules.reportFramework.ReportBuilder',
    'AssetManagement.customer.modules.reportFramework.layout.OrderReportLayout',
    'AssetManagement.customer.modules.reportFramework.layout.ChecklistReportLayout',
    'AssetManagement.customer.modules.reportFramework.layout.DeliveryReportLayout'
  ],

  inheritableStatics: {
    // private:
    _instance: null,

    // public:
    getInstance: function () {
      if (this._instance == null) {
        this._instance = Ext.create('AssetManagement.customer.modules.reportFramework.ReportPdfMaker');
      }

      return this._instance;
    }
  },

  // public
  preparePdfDocuments: function (objectsToPrint, reportLang, logos) {
    try {
      var preparedObjectsWithData = objectsToPrint;
      var pdfs = [];

      for (var i = 0; i < preparedObjectsWithData.length; i++) {
        var pdfMakeDoc = AssetManagement.customer.modules.reportFramework.ReportBuilder.getInstance().buildReportTemplate(preparedObjectsWithData[i], reportLang, logos);
        if (pdfMakeDoc) {

          var documentDetails = this.addContentBasedOnType(preparedObjectsWithData[i], pdfMakeDoc, reportLang);

          pdfs.push({
            object: preparedObjectsWithData[i],
            pdf: documentDetails['pdfDocument'],
            reportLangInstance: documentDetails['reportLangInstance']
          });
        }
      }
      return pdfs;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside preparePdfDocuments of BaseReportPdfMaker', ex);
    }
  },

  // public
  // method only required by regenerate layout after signing due to the image bugs in PDFmake - image are overriden by the library !!!!
  prepareAlreadySignedDocuments: function (objectsToPrint, reportLang) {
    try {
      // var arrayOfObjects = [];
      // for(var j= 0; j < objectsToPrint.length; j++){
      //     arrayOfObjects.push(objectsToPrint[j]['object']);
      // }

      // for(var i = 0; i < objectsToPrint.length; i++){
      //     var auart = objectsToPrint[i]['object']['mainObject'].get('auart');
      //     var ilart = objectsToPrint[i]['object']['mainObject'].get('ilart');

      //     var doc = AssetManagement.customer.modules.reportFramework.ReportBuilder.getInstance().buildReportTemplate(null, arrayOfObjects);

      //     if (auart === 'ZRPE') {
      //         if (ilart === 'RE1' || ilart === 'RE5') {
      //             this.injectInstallationReportLayout(objectsToPrint[i]['object'], doc, reportLang);
      //         }
      //         if (ilart === 'RE2' || ilart === 'RE6') {
      //             this.injectPickUpReportLayout(objectsToPrint[i]['object'], doc, reportLang);
      //         }
      //         if (ilart === 'RE3' || ilart === 'RE7') {
      //             this.injectServiceCallReportLayout(objectsToPrint[i]['object'], doc, reportLang);
      //         }
      //         if (ilart === 'RE4') {
      //             this.injectBedstoreReplReportLayout(objectsToPrint[i]['object'], doc, reportLang);
      //         }
      //     }

      //     objectsToPrint[i]['pdf'] = doc;
      // }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareAlreadySignedDocuments of BaseReportPdfMaker', ex);
    }
  },

  // private
  addContentBasedOnType: function (object, pdfMakeDoc, reportLang) {
    try {


      var extractedObject = object['mainObject'];

      var determinedLayout = extractedObject.get('layoutType');

      switch (determinedLayout.get('reportId')) {
        // STANDARD SCENARIO STARTS
        case 1:
          return this.injectOrderReportLayout(object, pdfMakeDoc, reportLang);
          break;
        case 3:
          return this.injectChecklistReportLayout(object, pdfMakeDoc, reportLang);
          break;
        case 5:
          return this.injectDeliveryReportLayout(object, pdfMakeDoc, reportLang);
          break;
        // STANDARD SCENARIO ENDS
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addContentBasedOnType of BaseReportPdfMaker', ex)
    }
  },

  // private
  injectOrderReportLayout: function (object, doc, reportLang) {
    try {
      return AssetManagement.customer.modules.reportFramework.layout.OrderReportLayout.getInstance().buildContent(object, doc, reportLang);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside injectOperationReportLayout of BaseReportPdfMaker', ex)
    }
  },

  // private
  injectChecklistReportLayout: function (object, doc, reportLang) {
    try {
      return AssetManagement.customer.modules.reportFramework.layout.ChecklistReportLayout.getInstance().buildContent(object, doc, reportLang);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside injectOperationReportLayout of BaseReportPdfMaker', ex)
    }
  },

  // private
  injectDeliveryReportLayout: function (object, doc, reportLang) {
    try {
      return AssetManagement.customer.modules.reportFramework.layout.DeliveryReportLayout.getInstance().buildContent(object, doc, reportLang);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside injectOperationReportLayout of BaseReportPdfMaker', ex)
    }
  }
});
