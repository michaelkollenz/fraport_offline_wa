﻿Ext.define('AssetManagement.base.modules.reportFramework.utils.BasePictureUtils', {
    inheritableStatics: {
	    resizeBase64Img: function (base64, width, height, matnr, sernr) {
	        try{
                var canvas = document.createElement("canvas");
                canvas.width = width;
                canvas.height = height;
                var context = canvas.getContext("2d");
                var deferred = $.Deferred();
                $("<img/>").attr("src", base64).load(function() {
                    context.scale(width/this.width,  height/this.height);
                    context.drawImage(this, 0, 0); 
                    deferred.resolve(canvas.toDataURL(), matnr, sernr);               
                });
                return deferred.promise();    


	        } catch (ex){

	        }
	    }
	}
});