Ext.define('AssetManagement.base.modules.reportFramework.model.BaseReportsPreviewPageiOSViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
       isSaveMode: false,
       accepted: [],
       docLanguageComboBox: false
    },

    resetData: function () {
       this.callParent();

       try {
           this.setData({
               isSaveMode: false,
               accepted:[],
               docLanguageComboBox: false
           });
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseReportsPreviewPageiOSViewModel', ex);
       }
    }
});