﻿Ext.define('AssetManagement.base.modules.reportFramework.model.BaseReportSignatureDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
    data: {
        isMailModeActive : false,
        providedSignature: null,
        contactPerson: null
    },
    
     resetData: function () {
        this.callParent();

        try {
            this.setData({
                isMailModeActive: false,
                providedSignature: null,
                contactPerson: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseReportSignatureDialogViewModel', ex);
        }
    }
});