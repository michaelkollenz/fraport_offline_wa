﻿Ext.define('AssetManagement.base.modules.reportFramework.model.BaseDocumentLanguage', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
		    //key fields
		    { name: 'scenario', type: 'string' },
            { name: 'lang', type: 'string' },
		    { name: 'reportId', type: 'int' },
		    { name: 'templateId', type: 'int' }

    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});