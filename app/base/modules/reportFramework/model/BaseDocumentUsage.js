﻿Ext.define('AssetManagement.base.modules.reportFramework.model.BaseDocumentUsage', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
		    //key fields
		    { name: 'scenario',   type: 'string' },
            { name: 'objectType', type: 'string' },
		    { name: 'reportId',   type: 'int' },
		    { name: 'templateId', type: 'int' },

	        //ordinary fields 
	        { name: 'fieldName1', type: 'string' },
			{ name: 'fieldName2', type: 'string' },
			{ name: 'fieldName3', type: 'string' },
            { name: 'fieldName4', type: 'string' },

	        { name: 'objectParam1', type: 'string' },
			{ name: 'objectParam2', type: 'string' },
			{ name: 'objectParam3', type: 'string' },
            { name: 'objectParam4', type: 'string' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});