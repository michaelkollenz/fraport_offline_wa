﻿Ext.define('AssetManagement.base.modules.reportFramework.model.BaseDocumentTarget', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
		    //key fields
		    { name: 'scenario', type: 'string' },
            { name: 'targetObject', type: 'string' },
            { name: 'seqnr', type: 'string' },
		    { name: 'reportId', type: 'int' },
		    { name: 'templateId', type: 'int' }

    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});