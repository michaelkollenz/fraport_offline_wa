﻿Ext.define('AssetManagement.base.modules.reportFramework.model.BaseReportsPreviewPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
       isSaveMode: false,
       hideLangCombobox: true
    },

    resetData: function () {
       this.callParent();

       try {
           this.setData({
               isSaveMode: false,
               hideLangCombobox: true
           });
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseReportsPreviewPageViewModel', ex);
       }
    }
});