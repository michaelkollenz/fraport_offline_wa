﻿Ext.define('AssetManagement.base.modules.reportFramework.model.BaseDocumentData', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
		    //key fields
		    { name: 'scenario',   type: 'string' },
		    { name: 'reportId',   type: 'int' },
		    { name: 'templateId', type: 'int' },

	        //ordinary fields 
	        { name: 'reportName', type: 'string' },
			{ name: 'templateName', type: 'string' },
			{ name: 'allowMultiObj', type: 'string' },
            { name: 'summaryReport', type: 'string'},
            { name: 'displaytabs', type: 'string'},
            { name: 'sign1Name', type: 'string'},
            { name: 'sign1Required', type: 'string'},
            { name: 'sign1CustName', type: 'string'},
            { name: 'sign1CustNameRequired', type: 'string'},
            { name: 'sign1CustNamePos', type: 'string'},
            { name: 'sign2Name', type: 'string'},
            { name: 'sign2Required', type: 'string'},
            { name: 'sign2CustName', type: 'string'},
            { name: 'sign2CustNameRequired', type: 'string'},
            { name: 'sign2CustNamePos', type: 'string'},
            { name: 'sign3Name', type: 'string'},
            { name: 'sign3Required', type: 'string'},
            { name: 'sign3CustName', type: 'string'},
            { name: 'sign3CustNameRequired', type: 'string'},
            { name: 'sign3CustNamePos', type: 'string'},
            { name: 'sign4Name', type: 'string'},
            { name: 'sign4Required', type: 'string'},
            { name: 'sign4CustName', type: 'string'},
            { name: 'sign4CustNameRequired', type: 'string'},
            { name: 'sign4CustNamePos', type: 'string'},
            { name: 'numEmail', type: 'string'},
            { name: 'defaultCustEmail', type: 'string'},
            { name: 'customerParentRole', type: 'string'},
            { name: 'partnerSourceBo', type: 'string'},
            { name: 'defaultUserEmail', type: 'string'},
            { name: 'txnam_kop', type: 'string'},
            { name: 'txnam_fus', type: 'string'},
            { name: 'tdpageform', type: 'string'},
            { name: 'tdpageortn', type: 'string'}

            
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});