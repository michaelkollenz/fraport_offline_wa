Ext.define('AssetManagement.base.modules.paint.BaseOxPaintingController', {
	extend: 'Ext.app.ViewController',

	
	requires: [
	  'AssetManagement.customer.utils.StringUtils',
	  'AssetManagement.customer.helper.OxLogger'
	],
	
	_myCanvas: null,
	_isPainting: false,
	_lastX: 0,
	_lastY: 0,
	_hasAnyData: false,

	//private
	setCanvas: function(canvas) {
		try {
			if(canvas) {
				var me = this;
				
				//mouse support
				canvas.addEventListener('mousedown', function(e, eOpts) { me.onMouseDown.call(me, e, eOpts); }, false);
				canvas.addEventListener('mouseup', function(e, eOpts) { me.onMouseUp.call(me, e, eOpts); }, false);
				canvas.addEventListener('mousemove', function(e, eOpts) { me.onMouseMove.call(me, e, eOpts); }, false);
				canvas.addEventListener('mouseenter', function(e, eOpts) { me.onMouseEnter.call(me, e, eOpts); }, false);
				canvas.addEventListener('mouseleave', function(e, eOpts) { me.onMouseLeave.call(me, e, eOpts); }, false);
				
				//touch support
				canvas.addEventListener('touchstart', function(e, eOpts) { me.onTouchStart.call(me, e, eOpts); }, false);
				canvas.addEventListener('touchcancel', function(e, eOpts) { me.onTouchCancel.call(me, e, eOpts); }, false);
				canvas.addEventListener('touchend', function(e, eOpts) { me.onTouchEnd.call(me, e, eOpts); }, false);
				canvas.addEventListener('touchmove', function(e, eOpts) { me.onTouchMove.call(me, e, eOpts); }, false);
				canvas.addEventListener('touchleave', function(e, eOpts) { me.onTouchLeave.call(me, e, eOpts); }, false);
			}
			
			this._myCanvas = canvas;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setCanvas of BaseOxPaintingController', ex);
		}
	},
	
	getCanvas: function() {
	    var retval = null;
	    
	    try {
		    retval = this._myCanvas;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCanvas of BaseOxPaintingController', ex);
		}  
		
		return retval;
	},

	//public
	hasContent: function() {
		try {
			return this._hasAnyData;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasAnyData of BaseOxPaintingController', ex);
		}
	},
	
	//returns the dcontent as data url (type image/png)
	//other types are possible, like jpg
	//also blobs can be returned, using other apis of the canvas
	getCurrentContentAsDataUrl: function() {
		var retval = '';
	
		try {
			if(this._hasAnyData) {
				var canvas = this._myCanvas;
				
				if(canvas) {
					retval = canvas.toDataURL();
					
					if(retval === 'data:,')
						retval = '';
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentContentAsJpg of BaseOxPaintingController', ex);
		}
		
		return retval;
	},
	
	loadBase64Data: function(base64StringOfImageData) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(base64StringOfImageData)) {
				if(this._hasAnyData)
					this.clear();
					
				var canvas = this._myCanvas;

				if(canvas) {
					var context = canvas.getContext("2d");

					var image = new Image();
					image.src = base64StringOfImageData;
					
					var me = this;

					image.onload = function() {
						try {
							me._hasAnyData = true;
							context.drawImage(image, 0, 0);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadBase64Data of BaseOxPaintingController', ex);
						}
					};
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadBase64Data of BaseOxPaintingController', ex);
		}
	},
	
	clear: function() {
		try {
			this._hasAnyData = false;
		
			if(this._myCanvas) {
				var context = this._myCanvas.getContext('2d');
			
				context.clearRect(0, 0, this._myCanvas.width, this._myCanvas.height);
				context.beginPath();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSurface of BaseOxPaintingController', ex);
		}
	},
	
	drawPoint: function(point) {
		try {
			if(this._myCanvas && point) {

				//hide keyboard
	            document.activeElement.blur();
				var context = this._myCanvas.getContext('2d');
				
				context.fillRect(point.x, point.y, 2, 2);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside drawPoint of BaseOxPaintingController', ex);
		}
	},
	
	drawPath: function(from, to) {
		try {
			if(this._myCanvas && from && to) {
				var context = this._myCanvas.getContext('2d');
				
				context.beginPath();
				context.moveTo(from.x, from.y);
				context.lineTo(to.x, to.y);
				context.lineWidth = 2;
				context.closePath();
		
				// set line color - not neccessary for black
				//context.strokeStyle = '#000000';
				context.stroke();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside drawPath of BaseOxPaintingController', ex);
		}
	},
	
	//private
	//event handler for mouse events
	onMouseDown: function(event, eOpts) {
		try {
			var point = this.getRelativeDrawingPointFromMouseEvent(event);

			if(point) {
				this._isPainting = true;
				this._hasAnyData = true;
			
				this.drawPoint(point);
			
				this._lastX = point.x;
				this._lastY = point.y;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMouseDown of BaseOxPaintingController', ex);
		}
	},
	
	onMouseUp: function(event, eOpts) {
		try {
			this._isPainting = false;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMouseUp of BaseOxPaintingController', ex);
		}
	},
	
	onMouseMove: function(event, eOpts) {
		try {
			//draw a path, if mouse is down
			if(this._isPainting === true) {
				var from = { x: this._lastX, y: this._lastY };
				var to = this.getRelativeDrawingPointFromMouseEvent(event);
			
				if(to) {
					this.drawPath(from, to);
					
					this._lastX = to.x;
					this._lastY = to.y;
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMouseMove of BaseOxPaintingController', ex);
		} finally {
			
		}
	},
	
	onMouseEnter: function(event, eOpts) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMouseEnter of BaseOxPaintingController', ex);
		}
	},
	
	onMouseLeave: function(event, eOpts) {
		try {
			this._isPainting = false;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMouseLeave of BaseOxPaintingController', ex);
		}
	},
	
	//event handler for touch events
	onTouchStart: function(event, eOpts) {
		try {
			//stop the event to avoid triggering of gesture/scrolling etc. 
			event.stopPropagation();
			event.preventDefault();
			
			var touch = event.targetTouches[0];
			
			var touch = event.targetTouches[0];

			if (!touch)
			    return;

			var point = this.getRelativeDrawingPointFromTouch(touch);

			if (point) {
			    this._isPainting = true;
			    this._hasAnyData = true;

			    this.drawPoint(point);

			    this._lastX = point.x;
			    this._lastY = point.y;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTouchStart of BaseOxPaintingController', ex);
		}
	},
	
	onTouchEnd: function(event, eOpts) {
		try {
			//stop the event to avoid triggering of gesture/scrolling etc. 
			event.stopPropagation();
			event.preventDefault();
		
			this._isPainting = false;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTouchEnd of BaseOxPaintingController', ex);
		}
	},
	
	onTouchCancel: function(event, eOpts) {
		try {
			event.stopPropagation();
			event.preventDefault();
		
			this._isPainting = false;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTouchCancel of BaseOxPaintingController', ex);
		}
	},
	
	onTouchMove: function(event, eOpts) {
		try {
			//stop the event to avoid triggering of gesture/scrolling etc. 
			event.stopPropagation();
			event.preventDefault();
			
			//draw a path, if touch phase has not yet ended
			if(this._isPainting === true) {
				var touch = event.targetTouches[0];
				
				if(!touch) {
					this._isPainting = false;
					return;
				}
				
				var from = { x: this._lastX, y: this._lastY };
				var to = this.getRelativeDrawingPointFromTouch(touch);
			
				this.drawPath(from, to);
				
				this._lastX = to.x;
				this._lastY = to.y;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTouchMove of BaseOxPaintingController', ex);
		}
	},
	
	onTouchLeave: function(event, eOpts) {
		try {
			//stop the event to avoid triggering of gesture/scrolling etc. 
			event.stopPropagation();
			event.preventDefault();
			
			this._isPainting = false;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTouchLeave of BaseOxPaintingController', ex);
		}
	},
	
	//will convert mouse event coordinates to coordinates relative to the canvas layer
	//returns a point object with x and y
	getRelativeDrawingPointFromMouseEvent: function(event) {
		var retval = null;
	
		try {
			if(event && this._myCanvas) {
				//use events clientX and clientY properties
				var eventX = event.clientX;
				var eventY = event.clientY;
				
				var myViewPortRect = this._myCanvas.getBoundingClientRect();
				
				if(myViewPortRect) {
					retval =  { x: eventX - myViewPortRect.left, y: eventY - myViewPortRect.top };
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelativeDrawingPointFromMouseEvent of BaseOxPaintingController', ex);
		}
		
		return retval;
	},
	
	//will convert touch coordinates to coordinates relative to the canvas layer
	//returns a point object with x and y
	getRelativeDrawingPointFromTouch: function(touchObject) {
		var retval = null;
	
		try {
			if(touchObject && this._myCanvas) {
				//use events clientX and clientY properties
				var touchX = touchObject.clientX;
				var touchY = touchObject.clientY;
				
				var myViewPortRect = this._myCanvas.getBoundingClientRect();
				
				if(myViewPortRect) {
					retval =  { x: touchX - myViewPortRect.left, y: touchY - myViewPortRect.top };
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelativeDrawingPointFromTouch of BaseOxPaintingController', ex);
		}
		
		return retval;
	}
});