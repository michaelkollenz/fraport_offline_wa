Ext.define('AssetManagement.base.modules.paint.BaseOxPaintingComponent', {
	extend: 'Ext.Container',


    requires: [
        'Ext.button.Button',
        'AssetManagement.customer.modules.paint.OxPaintingController',
        'AssetManagement.customer.modules.paint.OxPaintingModel',
        'AssetManagement.customer.helper.OxLogger'
    ],
    
	viewModel: {
	    type: 'OxPaintingModel'
	},
	
	layout: 'absolute',
	
	style: 'border: 1px solid black',
	
	controller: 'OxPaintingController',
	
	_canvasComponent: null,
	_resetButton: null,

	listeners: {
		boxready: {
	        fn: function() {
				try {
					if(this._canvasComponent) {
						var heightToApply = this.getHeight();
						var widthToApply = this.getWidth();
					
						this._canvasComponent.setHeight(heightToApply);
						this._canvasComponent.setWidth(widthToApply);
						
						var canvas = document.getElementById('canvas-' + this.getId());
											
						if(canvas) {
							//set canvas' size to components size
							canvas.height = heightToApply;
							canvas.width= widthToApply;
							
							//add the reset button add the correct place
							if(this._resetButton) {
								var absoluteX = widthToApply - (this._resetButton.width + 2);
								var absoluteY = 0;
								
								if(absoluteX > 0 && heightToApply > this._resetButton.height) {
									this._resetButton.x = absoluteX;
									this._resetButton.y = absoluteY;
									
									this.add(this._resetButton);
								}
							}
							
						}
						
						this.getController().setCanvas.call(this.getController(), canvas);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside boxready of BaseOxPaintingComponent', ex);
				}
			}
		}
	},
	
	constructor: function(args) {
		this.callParent(arguments);
        
		try {
		    //generate the components content
		    this.buildContainersContent(args);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxPaintingComponent', ex);
		}    
	},
	
	buildContainersContent: function(configObject) {
		try {
			//generate the canvas
			var idToSet = (configObject && configObject.id) ? configObject.id : 'canvas-' + this.getId();
			
			this._canvasComponent = Ext.create('Ext.Component', { x: 0, y: 0 });
			this._canvasComponent.setHtml('<canvas class="oxPaint" id="canvas-' + this.getId() + '" width="' + 10 +'" height="' + 10 + '">');
			
			this.add(this._canvasComponent);
			
			if(configObject.resetButton === true) {
				this._resetButton = Ext.create('Ext.Button', {
					margin: '0',
					padding: '0',
                    html: '<img width="100%" src="resources/icons/trash_red.png"></img>',
	                height: 40,
	                width: 40,
	                listeners: {
	                	click: 'clear'
	                }
				});
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildContainersContent of BaseOxPaintingComponent', ex);
		}
	}
});