Ext.define('AssetManagement.base.BaseAppConfig', {
    mixins: ['Ext.mixin.Observable'],

    requires: [
	   'AssetManagement.customer.helper.OxLogger',
	   'AssetManagement.customer.helper.LocalStorageHelper',
	   'AssetManagement.customer.utils.DateTimeUtils'
    ],

    inheritableStatics: {
        //private
        _instance: null,

        //public
        getInstance: function (config) {
            try {
                if (this._instance === null) {
                    this._instance = Ext.create('AssetManagement.customer.AppConfig', config);
                    this._instance.initialize();
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseAppConfig', ex);
            }

            return this._instance;
        },

        resetInstance: function () {
            try {
                this._instance = null;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetInstance of BaseAppConfig', ex);
            }
        }
    },

    //private
    _userPassword: '',
    _userIsLoggedIn: false,

    //private
    _scenario: '',

    constructor: function (config) {
        try {
            this.mixins.observable.constructor.call(this, config);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseAppConfig', ex);
        }
    },

    //private
    initialize: function () {
        try {
            this.setLocalization("app.localization.string.de");
            if (AssetManagement.customer.helper.LocalStorageHelper.isLocalStorageAvailable())
                this.loadFromLS();
            else
                this.fireEvent('initialized', false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initialize of BaseAppConfig', ex);
            this.fireEvent('initialized', false);
        }
    },

    writeDefaultToLS: function (xmlAppSettingsNode) {
        try {
            var children = xmlAppSettingsNode.childNodes;
            var curChild = null;
            var curKey = "";
            var curValue = "";

            Ext.each(children, function (item) {
                curKey = item.nodeName.toLowerCase();
                curValue = item.textContent;

                if (curKey === 'debugmode') {
                    this.setDebugMode(curValue);
                } else if (curKey === 'userid') {
                    this.setUserId(curValue);
                } else if (curKey === 'password') {
                    this.setUserPassword(curValue);
                } else if (curKey === 'mandt') {
                    this.setMandt(curValue);
                } else if (curKey === 'autologon') {
                    this.setAutoLoginUser(curValue);
                } else if (curKey === 'gateway') {
                    this.setSyncGateway(curValue);
                } else if (curKey === 'system.systemid') {
                    this.setSyncSystemId(curValue);
                } else if (curKey === 'gatewayport') {
                    this.setSyncGatewayPort(curValue);
                } else if (curKey === 'gatewayident') {
                    this.setSyncGatewayIdent(curValue);
                } else if (curKey === 'gatewayservicesync') {
                    this.setGatewayServiceSync(curValue);
                } else if (curKey === 'gatewayserviceinfo') {
                    this.setGatewayServiceInfo(curValue);
                } else if (curKey === 'gatewayservicesvm') {
                    this.setGatewayServiceSVM(curValue);
                } else if (curKey === 'gatewayaliasinfo') {
                    this.setGatewayAliasInfo(curValue);
                } else if (curKey === 'gatewayaliassync') {
                    this.setGatewayAliasSync(curValue);
                } else if (curKey === 'gatewaycontrollersync') {
                    this.setGatewayControllerSync(curValue);
                } else if (curKey === 'gatewaycontrollerfiledownload') {
                    this.setGatewayControllerFileDownload(curValue);
                } else if (curKey === 'gatewaycontrollerfileupload') {
                    this.setGatewayControllerFileUpload(curValue);
                } else if (curKey === 'gatewaycontrollerinfo') {
                    this.setGatewayControllerInfo(curValue);
                } else if (curKey === 'gatewaycontrollerping') {
                    this.setGatewayControllerPing(curValue);
                } else if (curKey === 'gatewaycontrollerdevicecheck') {
                    this.setGatewayControllerDeviceCheck(curValue);
                } else if (curKey === 'gatewaycontrollerchangepw') {
                    this.setGatewayControllerChangePw(curValue);
                } else if (curKey === 'gatewaycontrolleronlinerequest') {
                    this.setGatewayControllerOnlineRequest(curValue);
                } else if (curKey === 'protocol') {
                    this.setSyncProtocol(curValue);
                } else if (curKey === 'language') {
                    this.setLocale(curValue);
                } else if (curKey === 'saplanguage') {
                    this.setSAPLanguage(curValue);
                }
            }, this);

            AssetManagement.customer.helper.LocalStorageHelper.putSettingsBoolean('HaveIBeenHereBeFore', true);
            this.fireEvent('initialized', true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside writeDefaultToLS of BaseAppConfig', ex);
            this.fireEvent('initialized', false);
        }
    },

    getCurrentLanguageShort: function () {
        var retval = 'D';

        try {
            var localeShort = this.getLocale().substr(0, 2).toUpperCase();

            switch (localeShort) {
                case 'EN':
                    retval = 'E';
                    break;
                case 'DE':
                    retval = 'D';
                    break;
                default:
                    retval = 'D';
                    break;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentLanguageShort of BaseAppConfig', ex);
        }

        return retval;
    },

    loadFromLS: function () {
        try {
            if (!AssetManagement.customer.helper.LocalStorageHelper.getSettingsBoolean('HaveIBeenHereBeFore', false)) {
                Ext.Ajax.request({
                    url: 'resources/xml/appConfig.xml',
                    scope: this,
                    disableCaching: true,
                    callback: function (opts, success, response) {
                        if (success === true || response && response.status === 0) {
                            try {
                                var d = Ext.DomQuery.select("configuration", response.responseXML);
                                var configNode = response.responseXML.childNodes[0];
                                Ext.each(configNode.childNodes, function (item) {
                                    if (item.nodeName.toLowerCase() == 'appsettings') {
                                        this.writeDefaultToLS(item);
                                        return false;
                                    }
                                }, this);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('XML Parsing error on \'appConfig.config\'', ex);
                                this.fireEvent('initialized', false);
                            }
                        } else {
                            AssetManagement.customer.helper.OxLogger.logMessage('server-side failure reading \'appConfig.config\' with status code ' + response.status);
                            this.fireEvent('initialized', false);
                        }
                    }
                });
            } else {
                this.fireEvent('initialized', true);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadFromLS of BaseAppConfig', ex);
            this.fireEvent('initialized', false);
        }
    },

    //public
    isUserLoggedIn: function () {
        var retval = false;

        try {
            retval = this._userIsLoggedIn;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isUserLoggedIn of BaseAppConfig', ex);
        }

        return retval;
    },

    setUserLoggedIn: function (newValue) {
        try {
            this._userIsLoggedIn = newValue;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUserLoggedIn of BaseAppConfig', ex);
        }
    },

    isDebugMode: function () {
        var retval = false;

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsBoolean('DebugMode', false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isDebugMode of BaseAppConfig', ex);
        }

        return retval;
    },

    setDebugMode: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsBoolean('DebugMode', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDebugMode of BaseAppConfig', ex);
        }
    },

    getUserId: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('UserId');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserID of BaseAppConfig', ex);
        }

        return retval;
    },

    setUserId: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('UserId', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUserID of BaseAppConfig', ex);
        }
    },

    setLocalization: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('language', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setLocalization of BaseAppConfig', ex);
        }
    },

    getLocalization: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('language');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLocalization of BaseAppConfig', ex);
        }

        return retval;
    },

    getUserPassword: function () {
        var retval = '';

        try {
            //if working in debug mode, fetch the password from local storage
            if (this.isDebugMode())
                retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('UserPassword');
            else
                retval = this._userPassword;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserPassword of BaseAppConfig', ex);
        }

        return retval;
    },

    setUserPassword: function (newValue) {
        try {
            if (!newValue)
                newValue = '';

            //if working in debug mode, set the password in local storage to be capable for auto login
            if (this.isDebugMode())
                AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('UserPassword', newValue);
            else
                this._userPassword = newValue;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUserPassword of BaseAppConfig', ex);
        }
    },

    getScenario: function () {
        var retval = '';

        try {
            //if working in debug mode, fetch the password from local storage
            retval = this._scenario;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUserPassword of BaseAppConfig', ex);
        }

        return retval;
    },

    setScenario: function (newValue) {
        try {
            if (!newValue)
                newValue = '';
            this._scenario = newValue;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUserPassword of BaseAppConfig', ex);
        }
    },


    getMandt: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('Mandt');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMandt of BaseAppConfig', ex);
        }

        return retval;
    },

    setMandt: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('Mandt', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMandt of BaseAppConfig', ex);
        }
    },

    getAutoLoginUser: function () {
        var retval = false;

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsBoolean('AutoLogin', false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAutoLoginUser of BaseAppConfig', ex);
        }

        return retval;
    },

    setAutoLoginUser: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsBoolean('AutoLogin', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setAutoLoginUser of BaseAppConfig', ex);
        }
    },

    getLocale: function () {
        var locale = '';

        try {
            //include fallback scenario
            locale = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('Locale');

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(locale))
                locale = 'en-US';
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLocale of BaseAppConfig', ex);
        }

        return locale;
    },

    getSAPLanguageToSet: function () {
        var lang = '';

        try {
            lang = this.getLocale().substr(0, 2).toUpperCase();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLocale of BaseAppConfig', ex);
        }

        return lang;
    },

    setLocale: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('Locale', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setLocale of BaseAppConfig', ex);
        }
    },

    getBackendLocale: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('BackendLocale');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getBackendLocale of BaseAppConfig', ex);
        }

        return retval;
    },

    setBackendLocale: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('BackendLocale', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setBackendLocale of BaseAppConfig', ex);
        }
    },

    getSAPLanguage: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('SAPLanguage');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSAPLanguage of BaseAppConfig', ex);
        }

        return retval;
    },

    setSAPLanguage: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('SAPLanguage', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSAPLanguage of BaseAppConfig', ex);
        }
    },

    getSyncProtocol: function () {
        var retval = '';

        try {
            if ('https:' == window.location.protocol)
                retval = 'https';
            else
                retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('SyncProtocol');

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncProtocol of BaseAppConfig', ex);
        }

        return retval;
    },

    setSyncProtocol: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('SyncProtocol', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncProtocol of BaseAppConfig', ex);
        }
    },

    getSyncSystemId: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('SyncSystemId');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncSystemId of BaseAppConfig', ex);
        }

        return retval;

    },

    setSyncSystemId: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('SyncSystemId', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncSystemId of BaseAppConfig', ex);
        }
    },

    getSyncGateway: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('SyncGateway');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncGateway of BaseAppConfig', ex);
        }

        return retval;
    },

    setSyncGateway: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('SyncGateway', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncGateway of BaseAppConfig', ex);
        }
    },

    getSyncGatewayPort: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('SyncGatewayPort');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncGatewayPort of BaseAppConfig', ex);
        }

        return retval;
    },

    setSyncGatewayPort: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('SyncGatewayPort', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncGatewayPort of BaseAppConfig', ex);
        }
    },

    getSyncGatewayIdent: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('SyncGatewayIdent');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncGatewayIdent of BaseAppConfig', ex);
        }

        return retval;
    },

    setSyncGatewayIdent: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('SyncGatewayIdent', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncGatewayIdent of BaseAppConfig', ex);
        }
    },

    getGatewayServiceSync: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayServiceSync');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayServiceSync of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayServiceSync: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayServiceSync', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayServiceSync of BaseAppConfig', ex);
        }
    },

    getGatewayServiceInfo: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayServiceInfo');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayServiceInfo of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayServiceInfo: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayServiceInfo', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayServiceInfo of BaseAppConfig', ex);
        }
    },

    getGatewayAliasInfo: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayAliasInfo');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayAliasInfo of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayAliasInfo: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayAliasInfo', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayAliasInfo of BaseAppConfig', ex);
        }
    },

    getGatewayAliasSync: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayAliasSync');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayAliasSync of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayAliasSync: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayAliasSync', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayAliasSync of BaseAppConfig', ex);
        }
    },

    getGatewayServiceSVM: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayServiceSVM');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayServiceSVM of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayServiceSVM: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayServiceSVM', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayServiceSVM of BaseAppConfig', ex);
        }
    },

    getGatewayControllerSync: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerSync');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerSync of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerSync: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerSync', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerSync of BaseAppConfig', ex);
        }
    },

    getGatewayControllerFileDownload: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerFileDownload');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerFileDownload of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerFileDownload: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerFileDownload', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerFileDownload of BaseAppConfig', ex);
        }
    },

    getGatewayControllerFileUpload: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerFileUpload');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerFileUpload of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerFileUpload: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerFileUpload', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerFileUpload of BaseAppConfig', ex);
        }
    },

    getGatewayControllerPing: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerPing');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerPing of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerPing: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerPing', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerPing of BaseAppConfig', ex);
        }
    },

    getGatewayControllerInfo: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerInfo');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerInfo of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerInfo: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerInfo', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerInfo of BaseAppConfig', ex);
        }
    },

    getGatewayControllerDeviceCheck: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerDeviceCheck');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerInfo of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerDeviceCheck: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerDeviceCheck', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerInfo of BaseAppConfig', ex);
        }
    },

    getGatewayControllerChangePw: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerChangePw');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerChangePw of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerChangePw: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerChangePw', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerChangePw of BaseAppConfig', ex);
        }
    },

    getGatewayControllerOnlineRequest: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('GatewayControllerOnlineRequest');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGatewayControllerOnlineRequest of BaseAppConfig', ex);
        }

        return retval;
    },

    setGatewayControllerOnlineRequest: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('GatewayControllerOnlineRequest', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setGatewayControllerOnlineRequest of BaseAppConfig', ex);
        }
    },

    getSyncResetClient: function () {
        var retval = false;

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsBoolean('SyncResetClient', false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSyncResetClient of BaseAppConfig', ex);
        }

        return retval;
    },

    setSyncResetClient: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsBoolean('SyncResetClient', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncResetClient of BaseAppConfig', ex);
        }
    },

    getDeviceId: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('DeviceID');

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval))
                retval = "";

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeviceID of BaseAppConfig', ex);
        }

        return retval;
    },

    setDeviceId: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('DeviceID', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDeviceID of BaseAppConfig', ex);
        }
    },

    getOS: function () {
        return "HTML5";
    },

    getAppl: function () {
        return "WM";
    },

    getVersion: function () {
        var retval = '0';

        try {
            retval = AssetManagement.customer.core.Core.getVersion();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVersion of BaseAppConfig', ex);
        }

        return retval;
    },

    getUploadEncoding: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('UploadEncoding');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadEncoding of BaseAppConfig', ex);
        }

        return retval;
    },

    getDownloadEncoding: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('DownloadEncoding');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDownloadEncoding of BaseAppConfig', ex);
        }

        return retval;
    },

    getLastSync: function () {
        var retval = null;

        try {
            retval = AssetManagement.customer.utils.DateTimeUtils.parseTime(AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('LastSync' + this.getMandt() + this.getUserId()));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getLastSync of BaseAppConfig', ex);
        }

        return retval;
    },

    setLastSync: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('LastSync' + this.getMandt() + this.getUserId(), AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(newValue));
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setLastSync of BaseAppConfig', ex);
        }
    },

    getUsername: function () {
        var retval = '';

        try {
            retval = AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('Username');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUsername of BaseAppConfig', ex);
        }

        return retval;
    },

    setUsername: function (newValue) {
        try {
            AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('Username', newValue);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUsername of BaseAppConfig', ex);
        }
    }
});