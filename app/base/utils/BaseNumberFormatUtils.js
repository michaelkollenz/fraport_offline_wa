Ext.define('AssetManagement.base.utils.BaseNumberFormatUtils', {
	requires: [
	   'AssetManagement.customer.helper.OxLogger',
	   'Ext.util.Format',
	   'AssetManagement.customer.utils.StringUtils'
	],

	inheritableStatics: {
		//will check the provided input string against the current locale
		//if the check is successful, true will be returned, else false
		checkNumberInput: function(input) {
			var retval = false;
			input = input.toString();
			
			try {
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(input))
					return retval;
			
				var curLocale = AssetManagement.customer.core.Core.getAppConfig().getLocale();

				//check for correct number format
				//first check for number without any delimiter (locale independent)
				if(input.match(/^[0123456789]+$/) === null) {
					//not found, check for with delimiter
					var temp = false;
				
					switch(curLocale) {
						case 'de-DE':
							temp = input.match(/^[0123456789]+[,][0123456789]+$/) !== null;
							break;
						case 'en-US':
							temp = input.match(/^[0123456789]+[.][0123456789]+$/) !== null;
							break;
						default:
							break;
					}
					
					if(temp === false)
						return retval;
				}
				
				//check for too much decimal delimiters
				var decimalDelimiter = this.getDecimalDelimiterForLocale(curLocale);
				if(input.split(decimalDelimiter).length > 2) {
					return retval;
				}
			
				retval = true;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkNumberInput of BaseNumberFormatUtils', ex)
			}
			
			return retval;
		},
		
		getDecimalDelimiterForLocale: function(locale) {
			var retval = '';
			
			try {
				switch(locale) {
					case 'de-DE':
						retval = ',';
						break;
					case 'en-US':
						retval = '.';
						break;
					default:
						retval = '.';
						break;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDecimalDelimiterForLocale of BaseNumberFormatUtils', ex);
			}			

			return retval;
		},
		
		//parses a number for a provided string using the client locale
		//short version for parseUsingClientLocale
		parseNumber: function(numberAsString) {
			var retval = null;
			
			try {
			    retval = this.parseUsingClientLocale(numberAsString);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseNumber of BaseNumberFormatUtils', ex);
			} 
			
			return retval;
		},
		
		//tries to parse the provieded string value, assuming the string matches client locale format
		//return value is a number
		parseUsingClientLocale: function(numberAsString) {
			var retval = Number.NaN;
		
			try {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(numberAsString)) {
					var clientLocale = AssetManagement.customer.core.Core.getAppConfig().getLocale();
				
					numberAsString = this.convertNumber(numberAsString, clientLocale, 'en-US');
					retval = parseFloat(numberAsString);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseUsingClientLocale of BaseNumberFormatUtils', ex);
			}
			
			return retval;
		},
		
		//tries to parse the provieded string value, assuming the string matches backend locale format
		//return value is a number
		parseUsingBackendLocale: function(numberAsString) {
			var retval = Number.NaN;
		
			try {
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(numberAsString)) {
					var backendLocale = 'en-US';
				
					numberAsString = this.convertNumber(numberAsString, backendLocale, 'en-US');
					retval = parseFloat(numberAsString);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseUsingBackendLocale of BaseNumberFormatUtils', ex);
			}
			
			return retval;
		},
	
		//will convert a string based number provided by input to the backend locale
		//return value is a STRING also
		parseForBackendLocale: function(number) {
			var retval = number;
			
			try {
				//if the client locale does not match en-US, the string value has to be formatted to en-US for javascripts parsing algorithm
				var clientLocale = AssetManagement.customer.core.Core.getAppConfig().getLocale();

				number = this.convertNumber(number, clientLocale, 'en-US');
			
				try {
					number = parseFloat(number) + '';
				} catch(ex) {
					number = Number.NaN + '';
				}
			
				//if the backend locale does not match en-US the parsed value has to be formatted to the backend locale
				if(retval !== 'NaN') {
					var backendLocale = 'en-US';
					retval = this.convertNumber(number, 'en-US', backendLocale);
				} else {
					retval = number;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseForBackendLocale of BaseNumberFormatUtils', ex)
			}

			return retval;
		},
		
		getNumberAsStringForBackend: function(number) {
			var retval = '';
			
			try {
				if(isNaN(number) || number === null)
					number = '';
			
				var backendLocale = 'en-US';

				retval = this.convertNumber(number + '', 'en-US', backendLocale);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseForBackendLocale of BaseNumberFormatUtils', ex)
			}			

			return retval;
		},
		
		formatBackendValueForDisplay: function(number, cutOffDecimals) {
			var retval = '';
			
			try {
				//if a number is passed directly, convert it to string first
				if((typeof number) === "number")
					number = number + '';
			
				//trim the value
				number = number.trim();
				
				if(cutOffDecimals === true) {
					var backendLocale = 'en-US';
					number = this.cutOffDecimals(number, backendLocale);
				} else {
					//get rid of trailing zeros at the end - pass the backend locale
					var backendLocale = 'en-US';
					number = this.removeTrailingZeros(number, backendLocale);
				}
				
				var clientLocale = AssetManagement.customer.core.Core.getAppConfig().getLocale();
				retval = this.convertNumber(number, backendLocale, clientLocale);
				if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval))
				    retval = '0';
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside formatBackendValueForDisplay of BaseNumberFormatUtils', ex)
			}

			return retval;
		},
		
		//get rid of all decimals
		//has the same effect as Math.Floor - but works string based
		cutOffDecimals: function(number, locale) {
			var retval = '';
		
			try {
				//if a number is passed directly, convert it to string first
				if((typeof number) === "number") {
					locale = 'en-US';
					number = number + '';
				}
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(number) && number.length > 1) {
					//determine, what character starts the trailing part (e.g. '.' in US or ','	in german)
					var trailerStartingChar = '.'; //default
					
					if(locale === 'de-DE') {
						trailerStartingChar = ',';
					}
					
					number = number.split(trailerStartingChar)[0];
				}
				
				retval = number;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cutOffDecimals of BaseNumberFormatUtils', ex)
			}
			
			return retval;
		},
		
		//get rid of trailing zeros at the end
		removeTrailingZeros: function(number, locale, minimumDecimals) {
			var retval = '';
		
			try {
				//if a number is passed directly, convert it to string first
				if((typeof number) === "number") {
					locale = 'en-US';
					number = number + '';
				}
					
				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(number) && number[number.length - 1] !== '0') {
					return number;
				}
				
				if(!minimumDecimals)
					minimumDecimals = 0;
				
				//determine, what character starts the trailing part (e.g. '.' in US or ','	in german)
				var trailerStartingChar = '.'; //default
				
				if(locale === 'de-DE') {
					trailerStartingChar = ',';
				}
				
				//only do something, if the number contains the trailerStartingChar
				if(number.indexOf(trailerStartingChar) > -1) {
					var decimals = (number.length - 1) - number.lastIndexOf(trailerStartingChar);
					
					while(decimals > minimumDecimals && number[number.length - 1] === '0') {
						number = number.substring(0, number.length - 1);
						decimals--;
					}
					
					//at the end eat the trailerStartingChar, if it is the string's last
					if(number[number.length - 1] === trailerStartingChar) {
						number = number.substring(0, number.length - 1);
					}
				}
				
				retval = number;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeTrailingZeros of BaseNumberFormatUtils', ex)
			}
			
			return retval;
		},
		
		//expects a number as string
		//converts it from it's current format to another
		convertNumber: function(number, localeFrom, localeTo) {
			var retval = '';
			
			try {
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(number) ||
						AssetManagement.customer.utils.StringUtils.isNullOrEmpty(localeFrom)
						|| AssetManagement.customer.utils.StringUtils.isNullOrEmpty(localeTo))
					return number ? number : '';
				
				if(typeof(number) !== 'string')
					number = number.toString(); 
			
				switch(localeFrom) {
					case 'de-DE':
						switch(localeTo) {
							case 'de-DE':
								break;
							case 'en-US':
								number = number.replace(/\./g, '|');
								number = number.replace(/,/g, '.');
								number = number.replace(/\|/g, ',');
								break;
						}
						break;
					case 'en-US':
						switch(localeTo) {
							case 'de-DE':
								number = number.replace(/\./g, '|');
								number = number.replace(/,/g, '.');
								number = number.replace(/\|/g, ',');
								break;
							case 'en-US':
								break;
						}
						break;
					default:
						break;
				}
				
				retval = number;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside convertNumber of BaseNumberFormatUtils', ex)
			}
			
			return retval;
		},

	    //public
        //converts a negative number in STRING format between usual format (minus in front) to database reprensation of SAP (minus in the end)
	    convertNegativeNumber: function (number, fromDatabase) {
	        var retval = '';

	        try {
	            if (number) {
	                if (fromDatabase && number.substr(number.length - 1) === '-') {
	                    retval = '-' + number.substr(0, number.length - 1);
	                } else if (!fromDatabase && number.substr(0, 1) === '-') {
	                    retval = number.substr(1) + '-';
	                } else {
	                    retval = number;
	                }
	            }
	        } catch (ex) {
	            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside convertNegativeNumber of BaseNumberFormatUtils', ex);
	        }

	        return retval;
	    }
	}
});