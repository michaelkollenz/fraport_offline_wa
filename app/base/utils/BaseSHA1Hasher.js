Ext.define('AssetManagement.base.utils.BaseSHA1Hasher', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
	],

	inheritableStatics: {
		//public
		hash: function(toHash) {
			 try {
				// convert string to UTF-8, as SHA only deals with byte-streams
			    toHash = unescape(encodeURIComponent(toHash));

			    // constants [§4.2.1]
			    var K = [ 0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6 ];

			    // PREPROCESSING
			    toHash += String.fromCharCode(0x80);  // add trailing '1' bit (+ 0's padding) to string [§5.1.1]

			    // convert string toHash into 512-bit/16-integer blocks arrays of ints [§5.2.1]
			    var l = toHash.length/4 + 2; // length (in 32-bit integers) of toHash + ‘1’ + appended length
			    var N = Math.ceil(l/16);  // number of 16-integer-blocks required to hold 'l' ints
			    var M = new Array(N);

			    for (var i=0; i<N; i++) {
			        M[i] = new Array(16);
			        for (var j=0; j<16; j++) {  // encode 4 chars per integer, big-endian encoding
			            M[i][j] = (toHash.charCodeAt(i*64+j*4)<<24) | (toHash.charCodeAt(i*64+j*4+1)<<16) |
			                (toHash.charCodeAt(i*64+j*4+2)<<8) | (toHash.charCodeAt(i*64+j*4+3));
			        } // note running off the end of toHash is ok 'cos bitwise ops on NaN return 0
			    }
			    // add length (in bits) into final pair of 32-bit integers (big-endian) [§5.1.1]
			    // note: most significant word would be (len-1)*8 >>> 32, but since JS converts
			    // bitwise-op args to 32 bits, we need to simulate this by arithmetic operators
			    M[N-1][14] = ((toHash.length-1)*8) / Math.pow(2, 32); M[N-1][14] = Math.floor(M[N-1][14]);
			    M[N-1][15] = ((toHash.length-1)*8) & 0xffffffff;

			    // set initial hash value [§5.3.1]
			    var H0 = 0x67452301;
			    var H1 = 0xefcdab89;
			    var H2 = 0x98badcfe;
			    var H3 = 0x10325476;
			    var H4 = 0xc3d2e1f0;

			    // HASH COMPUTATION [§6.1.2]
			    var W = new Array(80); var a, b, c, d, e;
			    for (var i=0; i<N; i++) {

			        // 1 - prepare message schedule 'W'
			        for (var t=0;  t<16; t++) W[t] = M[i][t];
			        for (var t=16; t<80; t++) W[t] = this.ROTL(W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16], 1);

			        // 2 - initialise five working variables a, b, c, d, e with previous hash value
			        a = H0; b = H1; c = H2; d = H3; e = H4;

			        // 3 - main loop
			        for (var t=0; t<80; t++) {
			            var s = Math.floor(t/20); // seq for blocks of 'f' functions and 'K' constants
			            var T = (this.ROTL(a,5) + this.f(s,b,c,d) + e + K[s] + W[t]) & 0xffffffff;
			            e = d;
			            d = c;
			            c = this.ROTL(b, 30);
			            b = a;
			            a = T;
			        }

			        // 4 - compute the new intermediate hash value (note 'addition modulo 2^32')
			        H0 = (H0+a) & 0xffffffff;
			        H1 = (H1+b) & 0xffffffff;
			        H2 = (H2+c) & 0xffffffff;
			        H3 = (H3+d) & 0xffffffff;
			        H4 = (H4+e) & 0xffffffff;
			    }

			    var hashString = this.toHexStr(H0) + this.toHexStr(H1) + this.toHexStr(H2) + this.toHexStr(H3) + this.toHexStr(H4);
			    retval = hashString.toUpperCase();
			 } catch(ex) {
				 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hash of BaseSHA1Hasher', ex);
			 }
			 
			 return retval;
		},
		
		//private
		f: function(s, x, y, z)  {
			try {
		        switch (s) {
		           case 0: return (x & y) ^ (~x & z);           // Ch()
		           case 1: return  x ^ y  ^  z;                 // Parity()
		           case 2: return (x & y) ^ (x & z) ^ (y & z);  // Maj()
		           case 3: return  x ^ y  ^  z;                 // Parity()
		        }
		    } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside f of BaseSHA1Hasher', ex);
			}
		},

		ROTL: function(x, n) {
		    return (x<<n) | (x>>>(32-n));
		},


		toHexStr: function(n) {
		    // note can't use toString(16) as it is implementation-dependant,
		    // and in IE returns signed numbers when used on full words
		    var s = '', v;
		    
		    try {
		        for (var i=7; i>=0; i--) { v = (n>>>(i*4)) & 0xf; s += v.toString(16); }
		    } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside toHexStr of BaseSHA1Hasher', ex);
			}
		        
		    return s;
		}
	}
});