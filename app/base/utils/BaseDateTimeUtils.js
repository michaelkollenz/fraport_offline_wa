Ext.define('AssetManagement.base.utils.BaseDateTimeUtils', {
	requires: [
	   'Ext.util.Format',
	   'AssetManagement.customer.utils.StringUtils'
//	   'AssetManagement.customer.helper.OxLogger'		CAUSES RING DEPENDENY
	],

	inheritableStatics: {
		//public
		YYYYMMDD:  'Ymd',
		RFC2445_FORMAT: 'YmdTHisu',
	
		DEFAULT_DATE_DISPLAY_FORMAT: 'd.m.Y',
		DEFAULT_TIME_DISPLAY_FORMAT: 'H:i',

		SAP_IANA_TIMEZONE_MAP: {
		    'ALA': 'America/Anchorage',
		    'ALAW': 'America/Adak',
		    'AST': 'America/Halifax',
		    'AUSNSW': 'Australia/Sydney',
		    'AUSQLD': 'Australia/Brisbane',
		    'AUSSA': 'Australia/Adelaide',
		    'AUSTAS': 'Australia/Hobart',
		    'AUSVIC': 'Australia/Melbourne',
		    'AUSWA': 'Australia/Perth',
		    'CET': 'Europe/Berlin',
		    'CST': 'America/Chicago',
		    'CSTNO': 'America/Winnipeg',
		    'EST': 'America/New_York',
		    'ESTNO': 'America/Indiana/Indianapolis',
		    'EST_': 'America/Toronto',
		    'GMTUK': 'Europe/London',
		    'HAW': 'Pacific/Honolulu',
		    'INDIA': 'Asia/Kolkata',
		    'ISRAEL': 'Asia/Jerusalem',
		    'JAPAN': 'Asia/Tokyo',
		    'MST': 'America/Denver',
		    'MSTNO': 'America/Phoenix',
		    'NFDL': 'America/St_Johns',
		    'NST': 'America/St_Johns',
		    'PIERRE': 'America/Miquelon',
		    'PST': 'America/Los_Angeles',
		    'UK': 'Etc/GMT',
		    'UTC': 'Etc/UTC',
		    'UTC+8': 'Etc/GMT+8',
		    'WET': 'Europe/Luxembourg'
		},
	
	    //timeZone - SAP standard timezone from ZZTT database table
		getCurrentTime: function (timeZone) {
		    var retval = null;

		    try {
		        //check if time zone has been provided - if not use user's default timezone from UserInfo	
		        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeZone)) {
		            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
		            timeZone = userInfo.get('timeZone');
		        }

		        var ianaAcronym = this.SAP_IANA_TIMEZONE_MAP[timeZone];

		        //if the iana acronym for the time zone could not be found, fall back to UTC
		        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ianaAcronym))
		            ianaAcronym = this.SAP_IANA_TIMEZONE_MAP['UTC'];

		        //generate a moment instance for now in the corresponding time zone
		        var momentInstance = moment();
		        var adjustedMoment = momentInstance.tz(ianaAcronym);

		        //extract the local time from this moment instance into a new date instance
		        retval = new Date(adjustedMoment.year(), adjustedMoment.month(), adjustedMoment.date(), adjustedMoment.hours(), adjustedMoment.minutes(), adjustedMoment.seconds(), adjustedMoment.milliseconds());
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentTime of BaseDateTimeUtils', ex);
		    }

		    return retval;
		},

		getCurrentDate: function(timeZone) {
			var retval = null;
		
			try {
				var now = this.getCurrentTime(timeZone);

			    now.setHours(0);
			    now.setMinutes(0);
			    now.setSeconds(0);
			    now.setMilliseconds(0);

			    retval = now;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentDate of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		isNullOrEmpty: function(timeObject) {
	        var retval = false;
	        
	        try {
			    if(timeObject === null || timeObject === undefined)
				   retval = true;
				
			    if(!this.hasDate(timeObject) && !this.hasTime(timeObject))
				   retval = true;			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isNullOrEmpty of BaseDateTimeUtils', ex);
			}
				
			return retval;
		},
		
		//will check, if two date instances are equal by their values
		//by default seconds and milli seconds values are excluded on this check
		areEqual: function(dateTime, anotherDateTime, includeSeconds, includeMilliseconds) {
			var retval = false;
		
			try {
				//first check, if both are not null
				var dateTimeIsNullOrEmpty = this.isNullOrEmpty(dateTime);
				var anotherDateTimeIsNullOrEmpty = this.isNullOrEmpty(anotherDateTime);
				
				//if one of each is nullOrEmpty, they may only be equal by being so both
				if(dateTimeIsNullOrEmpty || anotherDateTimeIsNullOrEmpty) {
					retval = dateTimeIsNullOrEmpty && anotherDateTimeIsNullOrEmpty;
				} else {
					//it may be easier to check on ticks always, but this would rely on the fact,
					//seconds and milli seconds have been set, even if a instance actually do not care about second and millisecond values
					//so if not explicit requested go saver and do only value checks on the used fields
					if(includeSeconds === true && includeMilliseconds === true) {
						retval = dateTime.getTime() == anotherDateTime.getTime();
					} else {
						//next check, if instances are equal on their date values
						if(this.areEqualByDate(dateTime, anotherDateTime)) {
							//last check, if they are even equal on time
							retval = this.areEqualByTime(dateTime, anotherDateTime);
						}
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside areEqual of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		//will check, if two date instances are equal by their date values
		areEqualByDate: function(dateTime, anotherDateTime) {
			var retval = false;
		
			try {
				//first check, if both are not null
				var dateTimeIsNullOrEmpty = this.isNullOrEmpty(dateTime);
				var anotherDateTimeIsNullOrEmpty = this.isNullOrEmpty(anotherDateTime);
				
				//if one of each is nullOrEmpty, they may only be equal by being so both
				if(dateTimeIsNullOrEmpty || anotherDateTimeIsNullOrEmpty) {
					retval = dateTimeIsNullOrEmpty && anotherDateTimeIsNullOrEmpty;
				} else {
					//check their date field values
					retval = dateTime.getFullYear() == anotherDateTime.getFullYear() &&
									dateTime.getMonth() == anotherDateTime.getMonth() &&
											dateTime.getDate() == anotherDateTime.getDate();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside areEqualByDate of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		//will check, if two date instances are equal by their time values
		areEqualByTime: function(dateTime, anotherDateTime, includeSeconds, includeMilliseconds) {
			var retval = false;
		
			try {
				//first check, if both are not null
				var dateTimeIsNullOrEmpty = this.isNullOrEmpty(dateTime);
				var anotherDateTimeIsNullOrEmpty = this.isNullOrEmpty(anotherDateTime);
				
				//if one of each is nullOrEmpty, they may only be equal by being so both
				if(dateTimeIsNullOrEmpty || anotherDateTimeIsNullOrEmpty) {
					retval = dateTimeIsNullOrEmpty && anotherDateTimeIsNullOrEmpty;
				} else {
					//check their time field values
					retval = dateTime.getHours() == anotherDateTime.getHours() && dateTime.getMinutes() == anotherDateTime.getMinutes();
					
					if(retval && includeSeconds === true)
						retval = dateTime.getSeconds() == anotherDateTime.getSeconds();
						
					if(retval && includeMilliseconds === true)
						retval = dateTime.getMilliseconds() == anotherDateTime.getMilliseconds();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside areEqualByTime of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		isBeforeToday: function(dateTime) {
			var retval = false;
		
			try {
				if(dateTime) {
					var today = this.getCurrentTime();
				
					if(dateTime.getFullYear() < today.getFullYear()) {
						retval = true;
					} else if(dateTime.getFullYear() === today.getFullYear()) {
						if(dateTime.getMonth() < today.getMonth()) {
							retval = true;
						} else if(dateTime.getMonth() === today.getMonth()) {
							retval = dateTime.getDate() < today.getDate();
						}
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isBeforeToday of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		formatTimeForDb: function(timeObject, includeMilliSeconds) {
			var retval = '';
			
			try {
			    if(timeObject === null || timeObject === undefined)
				   return retval;
				
			    retval += this.getDateString(timeObject);
			    retval += this.getTimeString(timeObject, includeMilliSeconds);		
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside formatTimeForDb of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		getDBTimestamp: function(timeZone) {
	        var retval = '';
	        
	        try {
	            retval = this.formatTimeForDb(this.getCurrentTime(timeZone));
		    } catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDBTimestamp of BaseDateTimeUtils', ex);
		    }
		    
		    return retval;
		},
		
		parseTime: function(dateTimeStamp, secondPart) {
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(dateTimeStamp))
				return null;
				
			if(dateTimeStamp === '00000000000000000' || dateTimeStamp === '00000000000000' || dateTimeStamp === '00000000' || dateTimeStamp === '000000')
				return null;
		
			var retval = new Date();
			var dateString;
			var timeString;
			
			try {
				var firstArgumentLength = dateTimeStamp.length;

                if(firstArgumentLength === 14 || firstArgumentLength === 17) {
	               dateString = dateTimeStamp.substring(0, 8);
	               timeString = dateTimeStamp.substring(8);
                } else if(dateTimeStamp.length === 8) {
	               dateString = dateTimeStamp;
	               timeString = secondPart;
                } else if(dateTimeStamp.length === 6) {
	               timeString = dateTimeStamp;
                }

                if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(dateString)) {
	               retval.setFullYear(parseInt(dateString.substring(0,4)), parseInt(dateString.substring(4,6))-1, parseInt(dateString.substring(6)));
                }

                if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeString)) {
	               var usesMilliseconds = timeString.length > 6;

	               retval.setHours(parseInt(timeString.substring(0,2)), parseInt(timeString.substring(2,4)), parseInt(timeString.substring(4, 6)));
	
	               if(usesMilliseconds)
		              retval.setMilliseconds(parseInt(timeString.substring(6,9)));
                } else {
	                retval.setHours(0, 0, 0, 0);
                }
			} catch(ex) {
			   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseTime of BaseDateTimeUtils', ex);
		    }
			
			return retval;
		
		},
		
		getDateString: function(timeObject) {
			var retval = '';
		
			try {
				if(this.hasDate(timeObject)) {
					var format = this.YYYYMMDD;
					retval = Ext.util.Format.date(timeObject, format);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDateString of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		getTimeString: function(timeObject, includeMilliSeconds) {
			var retval = '';
		
			try {
				if(this.hasTime(timeObject)) {
					var hours = AssetManagement.customer.utils.StringUtils.padLeft(timeObject.getHours() + '', '0', 2);
					var minutes = AssetManagement.customer.utils.StringUtils.padLeft(timeObject.getMinutes() + '', '0', 2);
					var seconds = AssetManagement.customer.utils.StringUtils.padLeft(timeObject.getSeconds() + '', '0', 2);
					
			
					retval += hours + minutes + seconds;
					
					if(includeMilliSeconds === true) {
						retval += AssetManagement.customer.utils.StringUtils.padLeft(timeObject.getMilliseconds() + '', '0', 3)
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeString of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		getFullTimeForDisplay: function(timeObject, includeSeconds, includeMilliseconds) {
	        var retval = '';
	        
	        try {
	        	retval = this.getDateStringForDisplay(timeObject);
				
                if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval)) {
	               retval += ' ' + this.getTimeStringForDisplay(timeObject, includeSeconds, includeMilliseconds)
                }			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFullTimeForDisplay of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		getDateStringForDisplay: function (timeObject, format) {
		    var retval = '';

		    try {
		        if (this.hasDate(timeObject)) {
		            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(format))
		                format = this.getDateDisplayFormat();

		            retval += Ext.util.Format.date(timeObject, format);
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDateStringForDisplay of BaseDateTimeUtils', ex);
		    }

		    return retval;
		},

		getTimeStringForDisplay: function (timeObject, includeSeconds, includeMilliseconds, timeZone) {
		    var retval = '';

		    try {
		        if (!timeObject) {
		            timeObject = new Date(0);
		        }

		        var formatToUse = this.getTimeDisplayFormat(includeSeconds, includeMilliseconds);

		        var tempString = Ext.util.Format.date(timeObject, formatToUse);

		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeZone)) {
		            var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
		           // var displayTZPara = funcPara ? funcPara.get('display_timezones') : null;
                   //
		           // var timesZonesEnabled = displayTZPara ? displayTZPara.get('fieldval1') === 'X' : false;

		            if (funcPara.getValue1For('display_timezones') === 'X') {
		                tempString += ' ' + timeZone;
		            }
		        }

		        retval = tempString;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeStringForDisplay of BaseDateTimeUtils', ex);
		    }

		    return retval;
		},
		
		//accepts floats or strings
		getTimeFromFloat: function(numberAsString, unit) {
			var retval = new Date();
			
			try {
			    retval.setHours(0, 0, 0);
			
			    var number = parseFloat(numberAsString);
			
			    if(number === null || number === undefined || number === 0.0)
				   return retval;
				
			    var hours;
			    var minutes;
			
			    if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit))
				   unit = unit.toLowerCase();
				
			    if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit) && unit !== 'h') {
				   if(unit.toLowerCase() === 'min') {
					  hours = 0;
					  minutes = Math.floor(number);
				   }
			    } else {
				   hours = Math.floor(number);
				   var exact = (number - hours) * 60
				   var fractionInPercent = (exact - Math.floor(exact))*100;
				
				   if(fractionInPercent > 50)
					  minutes = Math.ceil(exact);
				   else
					  minutes = Math.floor(exact);
			    }
			
			    retval.setHours(hours, minutes, 0);			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeFromFloat of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},
		
		getWorkAsFloat: function(timeObject, unit) {
			var retval = 0.0;
			
			try {
				if(this.hasTime(timeObject)) {
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit))
						unit = unit.toLowerCase();
				
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit) && unit !== 'h') {
						if(unit.toLowerCase() === 'min') {
							retval += timeObject.getHours() * 60;
							retval += timeObject.getMinutes();
						}
					} else {
						retval += timeObject.getHours();
						retval += timeObject.getMinutes() / 60;
					}
				}			
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkAsFloat of BaseDateTimeUtils', ex);
			}
			
			return retval;
		},

		getDateTime: function(date, dateTime) {
			try {
			    date.setHours(dateTime.getHours());
			    date.setMinutes(dateTime.getMinutes());
	
			    return date; 
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDateTime of BaseDateTimeUtils', ex);
			}
		},
		
		setTimeValueToMin: function(date) {
			try {
			    if(date) {
			    	date.setHours(0);
			    	date.setMinutes(0);
			    	date.setSeconds(0);
			    	date.setMilliseconds(0);
			    }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setTimeValueToMin of BaseDateTimeUtils', ex);
			}
		},
		
		setTimeValueToMax: function(date){
			try {
			    if(date) {
			    	date.setHours(23);
			    	date.setMinutes(59);
			    	date.setSeconds(59);
			    	date.setMilliseconds(999);
			    }
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setTimeValueToMax of BaseDateTimeUtils', ex);
			}
		},
		
		hasDate: function(timeObject) {
			try {
			    return timeObject !== null && timeObject !== undefined && !(timeObject.getYear() === 1970 && timeObject.getMonth() === 0 && timeObject.getDay() == 1);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasDate of BaseDateTimeUtils', ex);
			}
		},

		hasTime: function(timeObject) {
	        try {
			    return timeObject !== null && timeObject !== undefined && !(timeObject.getHours() === 0 && timeObject.getMinutes() === 0 && timeObject.getSeconds() === 0);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasTime of BaseDateTimeUtils', ex);
			}    
		},

		getUTCNormalized: function (date, timeZone) {
		    var retval = null;

		    try {
		        if (date) {
		            //check if time zone has been provided - if not use user's default timezone from UserInfo	
		            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeZone)) {
		                var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
		                timeZone = userInfo.get('timeZone');
		            }

		            var ianaAcronym = this.SAP_IANA_TIMEZONE_MAP[timeZone];

		            //if the iana acronym for the time zone could not be found, fall back to UTC
		            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ianaAcronym))
		                ianaAcronym = this.SAP_IANA_TIMEZONE_MAP['UTC'];

		            //generate a moment instance in the corresponding time zone
		            var momentInstance = moment();
		            var adjustedMoment = momentInstance.tz(ianaAcronym);

		            //set the local time of the moment instance
		            adjustedMoment.year(date.getFullYear());
		            adjustedMoment.month(date.getMonth());
		            adjustedMoment.date(date.getDate());
		            adjustedMoment.hours(date.getHours());
		            adjustedMoment.minutes(date.getMinutes());
		            adjustedMoment.seconds(date.getSeconds());
		            adjustedMoment.milliseconds(date.getMilliseconds());

		            //extract the new UTC value and generate a new date instance with it
		            retval = new Date(adjustedMoment.valueOf());
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUTCNormalized of BaseDateTimeUtils', ex);
		    }

		    return retval;
		},

		getInDifferentTimeZone: function (date, sourceTimeZone, targetTimeZone) {
		    var retval = null;

		    try {
                //date and target time zone are mandatory
		        if (date && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(targetTimeZone)) {
		            if (sourceTimeZone === targetTimeZone) {
		                //on equal time zones just return a copy of the provided date instance
		                retval = new Date(date.getTime());
		            } else {
		                //check if source time zone has been provided - if not use user's default timezone from UserInfo	
		                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sourceTimeZone)) {
		                    var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
		                    sourceTimeZone = userInfo.get('timeZone');
		                }

		                var ianaAcronymSource = this.SAP_IANA_TIMEZONE_MAP[sourceTimeZone];

		                //if the iana acronym for the source time zone could not be found, fall back to UTC
		                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ianaAcronymSource))
		                    ianaAcronymSource = this.SAP_IANA_TIMEZONE_MAP['UTC'];

		                var ianaAcronymTarget = this.SAP_IANA_TIMEZONE_MAP[targetTimeZone];

		                //if the iana acronym for the target time zone could not be found, fall back to UTC
		                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ianaAcronymTarget))
		                    ianaAcronymTarget = this.SAP_IANA_TIMEZONE_MAP['UTC'];

		                //generate a moment instance in the corresponding time zone
		                var momentInstance = moment();
		                var adjustedMoment = momentInstance.tz(ianaAcronymSource);

		                //set the local time of the moment instance
		                adjustedMoment.year(date.getFullYear());
		                adjustedMoment.month(date.getMonth());
		                adjustedMoment.date(date.getDate());
		                adjustedMoment.hours(date.getHours());
		                adjustedMoment.minutes(date.getMinutes());
		                adjustedMoment.seconds(date.getSeconds());
		                adjustedMoment.milliseconds(date.getMilliseconds());

		                //set the time zone to the target time zone
		                adjustedMoment = momentInstance.tz(ianaAcronymTarget);

		                //extract the local time from this moment instance into a new date instance
		                retval = new Date(adjustedMoment.year(), adjustedMoment.month(), adjustedMoment.date(), adjustedMoment.hours(), adjustedMoment.minutes(), adjustedMoment.seconds(), adjustedMoment.milliseconds());
		            }
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInDifferentTimeZone of BaseDateTimeUtils', ex);
		    }

		    return retval;
		},

		getFullTimeWithTimeZoneForDisplay: function (timeObject, timeZone, includeSeconds, includeMilliseconds) {
			var retval = '';

			try {
				retval = this.getDateStringForDisplay(timeObject);

				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval)) {
					retval += ' ' + this.getTimeStringWithTimeZoneForDisplay(timeObject, timeZone, includeSeconds, includeMilliseconds);
				}
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFullTimeWithTimeZoneForDisplay of BaseDateTimeUtils', ex);
			}

			return retval;
		},

		getTimeStringWithTimeZoneForDisplay: function (timeObject, timeZone, includeSeconds, includeMilliseconds) {
			var retval = '';

			try {
				retval = this.getTimeStringForDisplay(timeObject, includeSeconds, includeMilliseconds, timeZone);
			} catch (ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeStringWithTimeZoneForDisplay of BaseDateTimeUtils', ex);
			}

			return retval;
		},

		getDateDisplayFormat: function () {
		    var retval = '';

		    try {
		        var formatToUse = this.DEFAULT_DATE_DISPLAY_FORMAT;

		        var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
		        //var dateDisplayFormatPara = funcPara ? funcPara.get('date_display_format') : null;

		        if (funcPara) {
		            var fromPara = funcPara.getValue1For('date_display_format');

		            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fromPara)) {
		                formatToUse = fromPara;
		            }
		        }

		        retval = formatToUse;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDateDisplayFormat of BaseDateTimeUtils', ex);
		    }

		    return retval;
		},

		getTimeDisplayFormat: function (includeSeconds, includeMilliseconds) {
		    var retval = '';

		    try {
		        var formatToUse = this.DEFAULT_TIME_DISPLAY_FORMAT;

		        var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
		        //var timeDisplayFormatPara = funcPara ? funcPara.get('time_display_format') : null;

		        if (funcPara) {
		            var fromPara = funcPara.getValue1For('time_display_format');

		            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fromPara)) {
		                formatToUse = fromPara;
		            }
		        }

		        //check, if seconds are requested
		        if (includeSeconds === true) {
		            //seconds requested - validate, if the format string needs to be extended
		            if (!AssetManagement.customer.utils.StringUtils.contains(formatToUse, 's'))
                        formatToUse = formatToUse.replace(':i', ':i:s');

		            //check, if milliseconds are requested also
		            if (includeMilliseconds === true) {
		                //milliseconds requested - validate, if the format string needs to be extended
		                if (!AssetManagement.customer.utils.StringUtils.contains(formatToUse, 'u'))
                            formatToUse = formatToUse.replace(':s', ':s.u');
                        //formatToUse += '.u';
		            } else {
		                //no milliseconds requested
		                //validate, if the format string needs to be reduced for milliseconds
		                if (AssetManagement.customer.utils.StringUtils.contains(formatToUse, 'u')) {
		                    //reduce the format string by 'd' and its delimiter prefix
		                    formatToUse = formatToUse.replace(/.u/, '');
		                }
		            }
		        } else {
		            //no seconds requested at all
		            //validate, if the format string needs to be reduced for seconds and milliseconds
		            if (AssetManagement.customer.utils.StringUtils.contains(formatToUse, 's')) {
		                //reduce the format string by 's' and its delimiter prefix
		                formatToUse = formatToUse.replace(/.s/, '');
		            }

		            if (AssetManagement.customer.utils.StringUtils.contains(formatToUse, 'u')) {
		                //reduce the format string by 'd' and its delimiter prefix
		                formatToUse = formatToUse.replace(/.u/, '');
		            }
		        }

		        retval = formatToUse;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTimeDisplayFormat of BaseDateTimeUtils', ex);
		    }

		    return retval;
		}
	}
});