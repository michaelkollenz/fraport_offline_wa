Ext.define('AssetManagement.base.utils.BaseOxException', {
    config: {
        message: '',
        method: '',
        clazz: '',
        time: null,
        handled: false
    },

    constructor: function(config) {
        this.initConfig(config);
        
        try {
             this.setTime(new Date());
        } catch(ex) {
		     AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxException', ex);
	    }    
    }
});