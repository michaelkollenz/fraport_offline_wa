Ext.define('AssetManagement.base.utils.BaseStringUtils', {
    requires: [
	//	    'AssetManagement.customer.helper.OxLogger' WOULD CAUSE RING DEPENDENCY
    ],

    inheritableStatics: {
        //checks, if a string is empty
        //returns false for a not empty string or a passed number
        isNullOrEmpty: function (theString) {
            var retval = false;

            try {
                var typeOfPara = typeof (theString);

                switch (typeOfPara) {
                    case 'string':
                        //check is straight foward
                        retval = '' === theString;
                        break;

                    case 'number':
                        //consider NaN as an empty value - because it is not valid from business perspektive
                        retval = isNaN(theString);
                        break;

                    default:
                        //everything else is considered as string incompatible from a business perspective and therefore returned as there is nothing
                        //'undefined', 'function', 'object', 'boolean' etc.
                        retval = true;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isNullOrEmpty of BaseStringUtils', ex);
            }

            return retval;
        },

        format: function (toFormat, argsArray) {
            var retval = toFormat;

            if (!this.isNullOrEmpty(toFormat)) {
                var pattern = /\{\{|\}\}|\{(\d+)\}/g;

                retval = toFormat.replace(pattern, function (match, group) {
                    var value;
                    if (match === "{{")
                        return "{";
                    if (match === "}}")
                        return "}";

                    value = argsArray[parseInt(group, 10)];
                    return value ? value.toString() : "";
                });
            }

            return retval;
        },

        insert: function (toInsertInto, index, toInsert) {
            var retval = toInsertInto;

            try {
                if (index > 0)
                    retval = toInsertInto.substring(0, index) + toInsert + toInsertInto.substring(index, toInsertInto.length);
                else
                    retval = toInsert + toInsertInto;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside insert of BaseStringUtils', ex);
            }

            return retval;
        },

        trimStart: function (theString, trimChar) {
            var retval = theString;

            try {
                if (this.isNullOrEmpty(theString) || this.isNullOrEmpty(trimChar))
                    return theString;

                while (retval.charAt(0) === trimChar) {
                    retval = retval.substring(1);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trimStart of BaseStringUtils', ex);
            }

            return retval;
        },

        filterLeadingZeros: function (theString) {
            var retval = '';

            try {
                retval = this.trimStart(theString, '0');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside filterLeadingZeros of BaseStringUtils', ex);
            }

            return retval;
        },

        concatenateSimple: function (stringArray) {
            var retval = "";

            try {
                if (stringArray === null || stringArray === undefined)
                    return retval;

                for (var i = 0; i < stringArray.length; i++) {
                    if (!this.isNullOrEmpty(stringArray[i])) {
                        retval += stringArray[i];
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside concatenateSimple of BaseStringUtils', ex);
            }

            return retval;
        },

        //concatenates stings using an concatChar
        //if the concat parameter is omitted, a space will be used
        concatenate: function (stringArray, concatChar, ignoreEmptyStrings, doNotExtendConcatChar, emptyReplacement) {
            var retval = "";

            try {
                if (stringArray === null || stringArray === undefined)
                    return "";

                if (stringArray.length === 1)
                    return stringArray[0];

                var connectingString;

                if (concatChar !== undefined && concatChar !== null && concatChar !== " ") {
                    if (!doNotExtendConcatChar)
                        connectingString = " " + concatChar + " ";
                    else
                        connectingString = concatChar;
                } else {
                    connectingString = " ";
                }

                if (this.isNullOrEmpty(emptyReplacement))
                    emptyReplacement = '';

                var offset = 1;

                if (!this.isNullOrEmpty(stringArray[0]))
                    retval = stringArray[0];
                else if (!ignoreEmptyStrings && emptyReplacement)
                    retval = emptyReplacement;

                var nextString;

                if (ignoreEmptyStrings && retval === "") {
                    do {
                        retval = stringArray[offset];
                        offset++;
                    } while (retval === "" && offset < stringArray.length);
                }

                for (var i = offset; i < stringArray.length; i++) {
                    nextString = stringArray[i];

                    if (!this.isNullOrEmpty(nextString)) {
                        retval += connectingString + nextString;
                    } else if (!ignoreEmptyStrings) {
                        retval += connectingString + emptyReplacement;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside concatenate of BaseStringUtils', ex);
            }

            return retval;
        },

        contains: function (stringToTest, criteria, caseInsensetive) {
            var retval = false;

            try {
                if (this.isNullOrEmpty(criteria)) {
                    retval = true;
                } else if (!this.isNullOrEmpty(stringToTest)) {
                    if (caseInsensetive === true) {
                        stringToTest = stringToTest.toUpperCase();
                        criteria = criteria.toUpperCase();
                    }

                    retval = stringToTest.search(criteria) != -1;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside contains of BaseStringUtils', ex);
            }

            return retval;
        },

        //regex has to be passed as a regex object or as /xxxx/
        matches: function (stringToTest, regex) {
            var retval = false;

            try {
                if (!this.isNullOrEmpty(stringToTest)) {
                    var matches = stringToTest.match(regex);

                    retval = matches && matches.length === 1;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside matches of BaseStringUtils', ex);
            }

            return retval;
        },

        startsWith: function (stringToTest, criteria) {
            var retval = false;

            try {
                if (!this.isNullOrEmpty(stringToTest)) {
                    if (!this.isNullOrEmpty(criteria)) {
                        retval = stringToTest.substring(0, criteria.length) === criteria;
                    } else {
                        retval = true;
                    }
                } else {
                    retval = this.isNullOrEmpty(criteria);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startsWith of BaseStringUtils', ex);
            }

            return retval;
        },

        endsWith: function (stringToTest, criteria) {
            var retval = false;

            try {
                if (!this.isNullOrEmpty(stringToTest)) {
                    if (!this.isNullOrEmpty(criteria) && criteria.length <= stringToTest.length) {
                        retval = stringToTest.substring(stringToTest.length - criteria.length) === criteria;
                    } else {
                        retval = true;
                    }
                } else {
                    retval = this.isNullOrEmpty(criteria);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside endsWith of BaseStringUtils', ex);
            }

            return retval;
        },

        countOccurences: function (stringToTest, toSearchFor) {
            var retval = 0;
            try {
                if (!this.isNullOrEmpty(stringToTest)) {
                    retval = stringToTest.split(toSearchFor).length - 1;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside countOccurences of BaseStringUtils', ex);
            }

            return retval;
        },

        removeLastCharacterOfString: function (theString) {
            var retval = '';

            try {
                retval = theString.substring(0, theString.length - 1);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeLastCharacterOfString of BaseStringUtils', ex);
            }

            return retval;
        },

        padLeft: function (theString, padChar, length) {
            var temp = "";

            try {
                for (var i = 0; i < length; i++) {
                    temp += padChar;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside padLeft of BaseStringUtils', ex);
            }

            return (temp + theString).slice(-length);
        },

        countInstances: function (theString, charOccurence) {
            var retval = 0;

            try {
                if (!this.isNullOrEmpty(theString)) {
                    for (var i = 0; i < theString.length; i++) {
                        if (theString.charAt(i) === charOccurence)
                            retval++;
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside countInstances of BaseStringUtils', ex);
            }

            return retval;
        },

        splitStringHalves: function (toSplit, splitChar) {
            var retval = ["", ""];

            try {
                if (!this.isNullOrEmpty(toSplit) && !this.isNullOrEmpty(splitChar)) {
                    var index = toSplit.indexOf(splitChar);

                    if (index > -1) {
                        retval[0] = toSplit.substring(0, index);
                        retval[1] = toSplit.substring(Math.min(toSplit.length, index + 1));
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside splitStringHalves of BaseStringUtils', ex);
            }

            return retval;
        },

        filterCR: function (toFilter) {
            var retval = null;

            try {
                retval = toFilter.replace(/\n/g, ' ');
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside filterCR of BaseStringUtils', ex);
            }

            return retval.replace(/\r/g, '');
        },

        //converts the passed bytes (as array or array buffer) into a string of the passed encoding
        //if encoding is left blank, UTF-8 will be used
        getStringFromBytes: function (arrayBuffer, encoding) {
            var retval = '';

            try {
                if (Array.isArray(arrayBuffer)) {
                    var arrayBufferView = new Uint8Array(arrayBuffer);
                    arrayBuffer = arrayBufferView.buffer;
                }

                if (this.isNullOrEmpty(encoding))
                    encoding = 'UTF-8';

                switch (encoding) {
                    case 'UTF-8':
                        retval = AssetManagement.customer.utils.StringUtils.getStringFromUTF8ArrayBuffer(arrayBuffer);
                        break;

                    case 'UTF-16':
                    default:
                        retval = AssetManagement.customer.utils.StringUtils.getStringFromUTF16ArrayBuffer(arrayBuffer);
                        break;

                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStringFromBytes of BaseStringUtils', ex);
            }

            return retval;
        },

        //builds a string out of an array buffer using UTF-8 encoding
        getStringFromUTF8ArrayBuffer: function (arrayBuffer) {
            var retval = '';

            try {
                var arrayBufferView = new Uint8Array(arrayBuffer);

                var out, i, len, c;
                var char2, char3;

                out = "";
                len = arrayBufferView.byteLength;
                i = 0;

                while (i < len) {
                    c = arrayBufferView[i++];

                    switch (c >> 4) {
                        case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                            // 0xxxxxxx
                            out += String.fromCharCode(c);
                            break;
                        case 12: case 13:
                            // 110x xxxx   10xx xxxx
                            char2 = arrayBufferView[i++];
                            out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                            break;
                        case 14:
                            // 1110 xxxx  10xx xxxx  10xx xxxx
                            char2 = arrayBufferView[i++];
                            char3 = arrayBufferView[i++];
                            out += String.fromCharCode(((c & 0x0F) << 12) |
                                           ((char2 & 0x3F) << 6) |
                                           ((char3 & 0x3F) << 0));
                            break;
                    }
                }

                retval = out;
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStringFromUTF8Bytes of BaseStringUtils', ex);
            }

            return retval;
        },

        //builds a string out of an array buffer using UTF-16 encoding
        getStringFromUTF16ArrayBuffer: function (arrayBuffer) {
            var retval = '';

            try {
                var arrayBufferView = new Uint16Array(arrayBuffer);
                retval = String.fromCharCode.apply(null, arrayBufferView);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStringFromUTF16ArrayBuffer of BaseStringUtils', ex);
            }

            return retval;
        },

        //will decode a string to bytes based on utf8
        //returns an arraybuffer
        utf8StringToArrayBuffer: function (stringToEncode) {
            var retval = null;

            try {
                retval = this.stringToUTF8Bytes(stringToEncode);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside utf8StringToArrayBuffer of BaseStringUtils', ex);
            }

            return retval;
        },

        //will decode a string to bytes based on utf16
        //returns an arraybuffer
        utf16StringToArrayBuffer: function (stringToEncode) {
            var retval = null;

            try {
                if (!this.isNullOrEmpty(stringToEncode)) {
                    var arrayBuffer = new ArrayBuffer(stringToEncode.length * 2); // 2 bytes for each char
                    var arrayBufferView = new Uint16Array(arrayBuffer);

                    for (var i = 0, strLen = stringToEncode.length; i < strLen; i++) {
                        arrayBufferView[i] = stringToEncode.charCodeAt(i);
                    }

                    retval = arrayBuffer;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside utf16StringToArrayBuffer of BaseStringUtils', ex);
            }

            return retval;
        },

        //will decode a string to utf-8 bytes
        //returns an array buffer
        stringToUTF8Bytes: function (stringToEncode) {
            var retval = null;

            try {
                var buffer = new Array();

                var errorOccured = false;
                var byteLength = 0;

                for (var i = 0; i < stringToEncode.length; i++) {
                    //get the unicode value of the current character
                    var arrayBufferOfChar = this.characterToUTF8Bytes(stringToEncode[i]);

                    //if the arrayBuffer is null, an error occured
                    if (!arrayBufferOfChar) {
                        errorOccured = true;
                        break;
                    }

                    buffer.push(arrayBufferOfChar);
                    byteLength += arrayBufferOfChar.byteLength;
                }

                if (!errorOccured) {
                    //build one array buffer out of the buffers for each char
                    retval = new ArrayBuffer(byteLength);
                    var view = new Uint8Array(retval);

                    var index = 0;
                    var tempView = null;

                    Ext.Array.each(buffer, function (charArrayBuffer) {
                        var tempView = new Uint8Array(charArrayBuffer);

                        //copy the values inside the target arraybuffer
                        for (var j = 0; j < charArrayBuffer.byteLength; j++) {
                            view[index] = tempView[j];
                            index++;
                        }
                    }, this);
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside stringToUTF8Bytes of BaseStringUtils', ex);
                retval = null;
            }

            return retval;
        },

        //returns an arraybuffer representating one unicode char
        characterToUTF8Bytes: function (character) {
            var retval = null;

            try {
                var uniCodeValue = character.charCodeAt(0);
                //all characters with code from 0 to 127 => 1 byte
                if (uniCodeValue < 128) {
                    retval = new ArrayBuffer(1);
                    var view = new Uint8Array(retval);

                    view[0] = uniCodeValue;
                } else if ((uniCodeValue > 127) && (uniCodeValue < 2048)) { //all characters with code from 128 to 2047 => 2 bytes
                    retval = new ArrayBuffer(2);
                    var view = new Uint8Array(retval);

                    //get the first byte
                    view[0] = (uniCodeValue >> 6) | 192;
                    //get the last byte
                    view[1] = (uniCodeValue & 63) | 128;
                } else if ((uniCodeValue > 2047) && (uniCodeValue < 66536)) { //all characters with code from 2048 to 66536 => 3 bytes
                    retval = new ArrayBuffer(3);
                    var view = new Uint8Array(retval);

                    //get the first byte
                    view[0] = (uniCodeValue >> 12) | 224;
                    //get the second byte
                    view[1] = ((uniCodeValue >> 6) & 63) | 128;
                    //get the last byte
                    view[2] = (uniCodeValue & 63) | 128;
                } else { //all characters with code from 66536 to 2097152 => 4 bytes
                    retval = new ArrayBuffer(4);
                    var view = new Uint8Array(retval);

                    //get the first byte
                    view[0] = (uniCodeValue >> 18) | 224;
                    //get the second byte
                    view[1] = ((uniCodeValue >> 12) & 63) | 128;
                    //get the third byte
                    view[2] = ((uniCodeValue >> 6) & 63) | 128;
                    //get the last byte
                    view[3] = (uniCodeValue & 63) | 128;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside characterToUTF8Bytes of BaseStringUtils', ex);
                retval = null;
            }

            return retval;
        },

        canvas: null,
        getTextWidth: function (text, font) {
            var retval = 0;

            try {
                if (!this.isNullOrEmpty(text) && !this.isNullOrEmpty(font)) {
                    // re-use canvas object for better performance
                    var canvas = this.canvas || (this.canvas = document.createElement("canvas"));
                    var context = canvas.getContext("2d");
                    context.font = font;
                    var metrics = context.measureText(text);
                    retval = metrics.width;
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTextWidth of BaseStringUtils', ex);
            }

            return retval;
        },

        getTextHeight: function (text, font) {
            var retval = 0;

            try {
                var body = document.getElementsByTagName("body")[0];
                var dummy = document.createElement("div");
                var dummyText = document.createTextNode(text);
                dummy.appendChild(dummyText);
                dummy.setAttribute("style", font);
                body.appendChild(dummy);
                retval = dummy.offsetHeight;
                body.removeChild(dummy);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTextHeight of BaseStringUtils', ex);
            }

            return retval;
        }
    }
});