Ext.define('AssetManagement.base.utils.BaseUserAgentInfoHelper', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
    ],	

	inheritableStatics: {
		_fullObjectCache: null,
	
		//will do a complete analysis and return it as an object
		getFullInfoObject: function() {
			var retval = null;
		
			try {
				//parameters to set on every request
				var screenWidth = NaN;
				var screenHeight = NaN;
				
				var ratio = window.devicePixelRatio || 1;
				screenWidth = screen.width * ratio;
				screenHeight = screen.height * ratio;
				
				//try to use the cache
				if(!this._fullObjectCache) {
					var ua = navigator.userAgent.toLowerCase();
					
					var check = function(r) {
					    return r.test(ua);
					};
					
					var DOC = document;
					var isStrict = DOC.compatMode == "CSS1Compat";
					var isWindows = !isAndroid && check(/windows|win32/);
					var isMac = check(/macintosh|mac os x/);
					var isAir = check(/adobeair/);
					var isLinux = check(/linux/);
					var isIOS = !isLinux && check(/ipad|iphone|ipod/);
					var isAndroid = !isIOS && check(/android/);
					var isWinMobile = !isAndroid && check(/Windows Phone/);
					var isBlackberry = !isWinMobile && check(/BlackBerry/);
					var isOpera = check(/opera/);
					var isChrome = check(/chrome/);
					var isWebKit = check(/webkit/);
					var isAndroidNative = !isChrome && isLinux && isWebKit;
					var isSafari = !isChrome && check(/safari/);
					var isSafari2 = isSafari && check(/applewebkit\/4/);
					var isSafari3 = isSafari && check(/version\/3/);
					var isSafari4 = isSafari && check(/version\/4/);
					var isIE = !isOpera && check(/msie/);
					var isIE7 = isIE && check(/msie 7/);
					var isIE8 = isIE && (check(/msie 8/) || check(/trident\/4.0/));
					var isIE9 = isIE && (check(/msie 9/) || check(/trident\/5.0/));
					var isIE10 = isIE && (check(/msie 10/) || check(/trident\/6.0/));
					var isIE11 = isIE && (check(/msie 11/) || check(/trident\/7.0/));
					var isIE6 = isIE && !isIE7 && !isIE8 && !isIE9 && !isIE10 && !isIE11;
					var isGecko = !isWebKit && check(/gecko/);
					var isGecko2 = isGecko && check(/rv:1\.8/);
					var isGecko3 = isGecko && check(/rv:1\.9/);
					var isBorderBox = isIE && !isStrict;
					var isSecure = /^https/i.test(window.location.protocol);
					var isIE7InIE8 = isIE7 && DOC.documentMode == 7;
	
					var jsType = '', browserType = '', browserVersion = '', osName = '';
	
					if(isIOS) {
						osName = 'iOS';
						
						var start = ua.indexOf('OS ');
						
						if(start > -1) {
							start += 3;
					        osName += ' ' + ua.substring(start, 3).replace( '_', '.' );
						}
					} else if(isAndroid) {
						osName = 'Android';
						
						// var start = ua.indexOf('android');
						
						// if(start > -1) {
					 //        var end = ua.indexOf(';', start + 8);
					 //        osName += ' ' + ua.substring(start, end);
						// }
					} else if(isWindows) {
						if(isWinMobile) {
							osName = 'Windows Mobile';
						} else {
							osName = 'Windows';
						}
	
					    /*if(check(/windows nt/)){
					        var start = ua.indexOf('windows nt');
					        var end = ua.indexOf(';', start);
					        osName = ua.substring(start, end);
					    }*/
					} else if(isLinux) {
						osName = 'Linux';
					} else if(isBlackberry) {
						osName = 'Blackberry';
					} else if(isMac) {
						osName = 'Mac';
					} else {
					    osName = 'Other';
					} 
	
					if(isIE) {
					    browserType = 'IE';
					    jsType = 'IE';
					    
					    if(ua.indexOf('msie') > -1) {
					    	var versionStart = ua.indexOf('msie') + 5;
					    	var versionEnd = ua.indexOf(';', versionStart);
						    
						    browserVersion = ua.substring(versionStart, versionEnd);
					    } else if(ua.indexOf('trident') > -1) {
					    	tridentVersionStart = ua.indexOf('trident/') + 9;
					    	tridentVersionEnd = ua.indexOf(' ', versionStart);
					    	
					    	tridentVersion = ua.substring(tridentVersionStart, tridentVersionEnd);
					    	
					    	//convert trident version to IE version
					    	switch(tridentVersion) {
					    		case '3.1':
					    			browserVersion = '7';
					    			break;
					    		case '4.0':
					    			browserVersion = '8';
					    			break;
					    		case '5.0':
					    			browserVersion = '9';
					    			break;
					    		case '6.0':
					    			browserVersion = '10';
					    			break;
					    		case '7.0':
					    			browserVersion = '11';
					    			break;
					    		default:
					    			browserVersion = 'unknown';
					    			break;
					    	}
					    } else {
					    	browserVersion = 'unknown';
					    }
	
					    jsType = 'IE';
					} else if(isGecko) {
					    var isFF =  check(/firefox/);
					    browserType = isFF ? 'Firefox' : 'Others';;
					    jsType = isGecko2 ? 'Gecko2' : isGecko3 ? 'Gecko3' : 'Gecko';
	
					    if(isFF) {
					        var versionStart = ua.indexOf('firefox') + 8;
					        var versionEnd = ua.indexOf(' ', versionStart);
					        if(versionEnd == -1){
					            versionEnd = ua.length;
					        }
					        browserVersion = ua.substring(versionStart, versionEnd);
					    }
					} else if(isChrome) {
					    browserType = 'Chrome';
					    jsType = isWebKit ? 'Web Kit' : 'Other';
	
						var versionStart = ua.indexOf('chrome') + 7;
					    var versionEnd = ua.indexOf(' ', versionStart);
					    browserVersion = ua.substring(versionStart, versionEnd);
					} else if(isAndroidNative) {
						browserType = 'Android Native';
						
					    var versionStart = ua.indexOf('webkit/') + 7;
					    var versionEnd = ua.indexOf(' ', versionStart);
					    browserVersion = ua.substring(versionStart, versionEnd);
					} else if (isSafari) {
					    browserType = 'Safari';

					    var versionStart = ua.indexOf('version/') + 8;
					    var versionEnd = ua.indexOf(' ', versionStart);
					    browserVersion = ua.substring(versionStart, versionEnd);
					} else if(isOpera) {
					    browserType = 'Opera';

					    var versionStart = ua.indexOf('version/') + 8;
					    var versionEnd = ua.indexOf(' ', versionStart);
					    if (versionEnd == -1) {
					        versionEnd = ua.length;
					    }
					    browserVersion = ua.substring(versionStart, versionEnd);
					}
					
					var retval = {
						browser: browserType,
						browserVersion: browserVersion,
						browserLanguage: navigator.language,
						hasWebkit: isWebKit,
						OS: osName,
						isMobile: isIOS || isAndroid || isWinMobile || isBlackberry,
						screenWidth: screenWidth,
						screenHeight: screenHeight
					};
					
					this._fullObjectCache = retval;
				} else {
					this._fullObjectCache.screenWidth = screenWidth;
					this._fullObjectCache.screenHeight = screenHeight;
					
					retval = this._fullObjectCache;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFullInfoObject of BaseUserAgentInfoHelper', ex);
			}
			
			return retval;
		},
	
		getWebbrowser: function(omitVersion) {
			var retval = '';
		
			try {
				var fullObject = this._fullObjectCache ? this._fullObjectCache : this.getFullInfoObject();
				
				if(fullObject) {
					retval = fullObject.browser
					
					if(!omitVersion)
						retval += ' ' + fullObject.browserVersion;
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWebbrowser of BaseUserAgentInfoHelper', ex);
			}
			
			return retval;
		},
		
		getOS: function() {
			var retval = '';
		
			try {
				var fullObject = this._fullObjectCache ? this._fullObjectCache : this.getFullInfoObject();
				
				if(fullObject)
					retval = fullObject.OS;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOS of BaseUserAgentInfoHelper', ex);
			}
			
			return retval;
		},
		
		getWebbrowserLanguage: function() {
			var retval = '';
		
			try {
				retval = navigator.language;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWebbrowserLanguage of BaseUserAgentInfoHelper', ex);
			}
			
			return retval;
		},
		
		isMobile: function() {
			var retval = '';
		
			try {
				var fullObject = this._fullObjectCache ? this._fullObjectCache : this.getFullInfoObject();
				
				if(fullObject)
					retval = fullObject.isMobile;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOS of BaseUserAgentInfoHelper', ex);
			}
			
			return retval;
		}
	}
});