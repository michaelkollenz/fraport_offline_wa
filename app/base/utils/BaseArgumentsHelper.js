Ext.define('AssetManagement.base.utils.BaseArgumentsHelper', {
	inheritableStatics: {
		extractArguments: function(argumentsArray, parameterCount) {
			var retval = new Array();
			
			try {
				if(argumentsArray !== null && argumentsArray !== undefined) {
					var min = Math.min(argumentsArray.length, parameterCount);
				
					for(var i = 0; i < min; i++) {
						retval.push(argumentsArray[i]);
					}
					
					if(min < parameterCount) {
						for(var i = min; i < parameterCount; i++) {
							retval.push(null);
						}
					}
				} else {
					for(var i = 0; i < parameterCount; i++) {
						retval.push(null);
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractArguments of ArgumentsHelper', ex);
			}
			
			return retval;
		}
	}
});