﻿Ext.define('AssetManagement.base.controller.mixins.BaseOrderListPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',

    orderMapsColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        e.stopEvent();
        if (grid.parentController) {
            grid.parentController.navigateToAddress(record);
        }
    },

    orderListCheckColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        if (grid.parentController) {
            grid.parentController.onCheckBoxChange(rowIndex);
        }
    }
})