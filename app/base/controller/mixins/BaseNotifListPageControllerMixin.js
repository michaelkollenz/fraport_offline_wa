﻿Ext.define('AssetManagement.base.controller.mixins.BaseNotifListPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',
    customSearchFunction: function (value, criteria, record) {
        var retVal = false
        if (value > criteria) {
            retVal = true
        }
        return retVal
    },

    notifMapsColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        e.stopEvent();
        if (grid.parentController) {
            grid.parentController.navigateToAddress(record);
        }
    }

})