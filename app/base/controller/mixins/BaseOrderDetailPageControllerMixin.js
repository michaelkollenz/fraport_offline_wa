﻿Ext.define('AssetManagement.base.controller.mixins.BaseOrderDetailPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',

    onFuncLocSelected: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        try {
            var tplnr = record.get('tplnr');
            if (tplnr)
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: tplnr });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseOrderDetailPageControllerMixin', ex);
        }
    },
    onNotifSelected: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        try {
            var ihnum = record.get('ihnum');
            if (ihnum)
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_DETAILS, { qmnum: ihnum });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifSelected of BaseOrderDetailPageControllerMixin', ex);
        }
    },

    onEquiSelected: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        try {
            var equnr = record.get('equnr');
            if (equnr)
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: equnr });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseOrderDetailPageControllerMixin', ex);
        }
    },

    onSelectChecklistClick: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        try {
            var controller = grid.owner.getController();
            if (controller) {
                controller.navigateToChecklist (grid, null, rowIndex);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectChecklistClick of BaseOrderDetailPageControllerMixin', ex);
        }
    },

    //onSelectChecklistClick: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
    //    try {
    //        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CHECKLIST);
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectChecklistClick of OrderDetailPageController', ex);
    //    }
    //},

    //onOperationCheckboxClick: function (table, td, cellIndex, record, checkbox, rowIndex, checked, eOpts, grid) {
    //    try {
    //        if (grid.owner.getController()) {
    //            grid.owner.getController().onCheckBoxChange(rowIndex, checkbox, checked, record, eOpts);
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationColumnClick of BaseOrderDetailPageControllerMixin', ex);
    //    }
    //},

    //onOperationCompletedCheckboxClick: function (table, td, cellIndex, record, checkbox, rowIndex, e, eOpts, grid) {
    //    try {
    //        if (grid.owner.getController()) {
    //            var checked = true;
    //            if (e.getTarget().className.indexOf('grid-checkcolumn-checked') !== -1) {
    //                checked = false;
    //            }

    //            if (e.getTarget().className.indexOf('grid-checkcolumn-disabled') === -1) {
    //                grid.owner.getController().onOperationCompletedCheckBoxChange(rowIndex, checkbox, checked, record, eOpts);
    //            }
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationColumnClick of BaseOrderDetailPageControllerMixin', ex);
    //    }
    //},

    onPartnerNavigateToAdress: function (table, td, cellIndex, record, checkbox, rowIndex, checked, eOpts, grid) {
        try {
            if (grid.owner.getController()) {
                grid.owner.getController().navigateToPartnerAddress(record);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationColumnClick of BaseOrderDetailPageControllerMixin', ex);
        }
    },

    partnerMapsColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        try {
            e.stopEvent();
            if (grid.parentController) {
                grid.parentController.navigateToAddress(record);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationColumnClick of BaseOrderDetailPageControllerMixin', ex);
        }
    }
})