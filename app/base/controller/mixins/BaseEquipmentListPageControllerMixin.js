﻿Ext.define('AssetManagement.base.controller.mixins.BaseEquipmentListPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',

    equipmentListMapsColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        e.stopEvent();
        if (grid.parentController) {
            grid.parentController.navigateToAddress(record);
        }
    }
})