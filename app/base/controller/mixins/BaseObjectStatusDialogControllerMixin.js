﻿Ext.define('AssetManagement.base.controller.mixins.BaseObjectStatusDialogControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',

    onObjectStatusWithOrdNoCheckboxClick: function (table, td, cellIndex, record, checkbox, rowIndex, checked, eOpts, grid) {
        try {
            if (!record.get('checked')) {
                record.set('checked', true);
                return;
            }
            if (grid.owner.getController()) {
                //grid.owner.getController().onObjectStatusCheckBoxChange(rowIndex, checkbox, checked, record, eOpts);
                grid.owner.getController().onObjectStatusWithOrdNoCheckBoxChange(rowIndex, checkbox, checked, record, eOpts); 
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onObjectStatusWithOrdNoCheckboxClick of BaseObjectStatusDialogControllerMixin', ex);
        }
    },

    onObjectStatusWithoutOrdNoCheckboxClick: function (table, td, cellIndex, record, checkbox, rowIndex, checked, eOpts, grid) {
        try {
            if (!record.get('checked')) {
                record.set('checked', true);
                return;
            }
            if (grid.owner.getController()) {
                grid.owner.getController().onObjectStatusCheckBoxChange(rowIndex, checkbox, checked, record, eOpts);
                //grid.owner.getController().onObjectStatusWithOrdNoCheckBoxChange(rowIndex, checkbox, checked, record, eOpts);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onObjectStatusWithoutOrdNoCheckboxClick of BaseObjectStatusDialogControllerMixin', ex);
        }
    }
})