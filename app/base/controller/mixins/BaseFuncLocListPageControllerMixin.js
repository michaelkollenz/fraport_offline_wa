﻿Ext.define('AssetManagement.base.controller.mixins.BaseFuncLocListPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',

    funcLocListMapsColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        e.stopEvent();
        if (grid.parentController) {
            grid.parentController.navigateToAddress(record);
        }
    }
})
