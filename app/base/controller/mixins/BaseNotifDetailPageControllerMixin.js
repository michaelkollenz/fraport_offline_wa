﻿Ext.define('AssetManagement.base.controller.mixins.BaseNotifDetailPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',

    notifPartnerMapsColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        var partner = grid.getStore().getAt(rowIndex);
        if (grid.parentController) {
            grid.parentController.navigateToPartnerAddress(partner);
        }
    }
})