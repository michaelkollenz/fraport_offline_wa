﻿Ext.define('AssetManagement.base.controller.mixins.BaseMaterialListPageControllerMixin', {
    extend: 'AssetManagement.customer.controller.mixins.PageControllerMixin',
    materialListBanfIconColumnHandler: function (table, td, cellIndex, record, tr, rowIndex, e, eOpts, grid) {
        e.stopEvent();
        if (grid.parentController) {
            grid.parentController.onDemandRequirementClick.call(grid.parentController, grid, rowIndex, cellIndex, td, e, record, table);
        }
    }
})