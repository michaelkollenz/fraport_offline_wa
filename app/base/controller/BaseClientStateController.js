﻿Ext.define('AssetManagement.base.controller.BaseClientStateController', {
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.manager.CacheManager',
	    'AssetManagement.customer.manager.UserManager',
		'AssetManagement.customer.controller.AppUpdateController',
	    'AssetManagement.customer.controller.LoadingIndicatorController',
	    'AssetManagement.customer.manager.DocUploadManager',
	    'AssetManagement.customer.controller.NavigationController',
	    'AssetManagement.customer.helper.LocalStorageHelper',
	    'AssetManagement.customer.model.helper.ReturnMessage',
	    'AssetManagement.customer.controller.EventController',
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.ViewLayoutManager',
		'AssetManagement.customer.helper.EventHelper'
    ],

	inheritableStatics: {
		//private
		_isCurrentlyRunningASync: false,
		_inLoadingState: false,
		_visualLoadingStateFeedbackDefaultDelay: 300,   //in ms
		_logoutTimeStamp: 0,
		_logoutTimeoutIntervall: 14400000,              //in ms
        _mask: null,
        _isMonkeyBarrierEnabled: false,
		//public
		EVENTS: {
			USER_LOGOUT: 'userLogOut'
		},

		//public
		isClientInLoadingState: function () {
		    var retval = false;

		    try {
		        retval = this._inLoadingState;
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isClientInLoadingState of BaseClientStateController', ex);
		    }

		    return retval;
		},

	    //public
        disableMonkeyBarrier: function () {
            try {
                if (this._mask != null)
                {
                    this._mask.setStyle({
                        opacity: 0
                    });
                    if (!this._mask.isHidden())
                        this._mask.hide();
                }
                this._isMonkeyBarrierEnabled = false;
            }
            catch (ex) {
                AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }
        },

	    //public
        enableMonkeyBarrier: function() {
            try {
                var view = AssetManagement.customer.core.Core.getMainView();
                if (this._mask == null) {
                    this._mask = new Ext.LoadMask({
                        baseCls: 'maskMainView',
                        target: view
                        // useMsg: true,
                        //msg: 'loading..'
                    });
                }

                this._mask.show();
                this._mask.setStyle({
                    opacity: 1
                });
                this._isMonkeyBarrierEnabled = true;
            } catch (ex) {
                AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }
        },

	    //public
        isMonkeyBarrierEnabled: function () {
            var retval = false;

            try {
                retval = this._isMonkeyBarrierEnabled;
            }
            catch (ex) {
                AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            }

            return retval;
        },

		//public
		enterLoadingState: function(delayToUseForIndicator, enableMonkeyBarrier) {
			try {
				if(this._inLoadingState === false) {
                    var _delayToUseForIndicator = delayToUseForIndicator;

                    if (!delayToUseForIndicator && delayToUseForIndicator !== 0 || delayToUseForIndicator < 0 || delayToUseForIndicator === true)
                        _delayToUseForIndicator = this._visualLoadingStateFeedbackDefaultDelay;

                    if (delayToUseForIndicator === true || !(enableMonkeyBarrier == null) && enableMonkeyBarrier === true) {
                        this.enableMonkeyBarrier();
                    }

                    this._inLoadingState = true;
                    AssetManagement.customer.controller.LoadingIndicatorController.showLoadingIndicator(_delayToUseForIndicator);
					// Ext.defer(showCallback, delayToUseForIndicator, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside enterLoadingState of BaseClientStateController', ex);
			}
		},

        //public
		leaveLoadingState: function(doNotDisableMkyBarr) {
			try {
                if(!doNotDisableMkyBarr)
                    this.disableMonkeyBarrier();

				AssetManagement.customer.controller.LoadingIndicatorController.hideLoadingIndicator();

				this._inLoadingState = false;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside leaveLoadingState of BaseClientStateController', ex);
			}
		},

	    //public
		setSyncIsRunning: function(value) {
			try {
				if(value !== undefined && value !== null) {
					this._isCurrentlyRunningASync = value;

					if(value === true) {
						this.enablePageLeavingHandler();
					} else if(value === false && AssetManagement.customer.core.Core.getAppConfig().isDebugMode() === true) {
						this.disablePageLeavingHandler();
					}
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSyncIsRunning of BaseClientStateController', ex);
			}
		},

		//callback for browsers, before they unload the page
		//if the user is logged in, inform him, he is going to be logged off
		pageLeavingHandler: function(e) {
			var retval = 'Are you sure you want to leave?';

			try {
				if(AssetManagement.customer.controller.ClientStateController._isCurrentlyRunningASync === true)
					retval = Locale.getMsg('closeAppWhileRunningSync');
				else
					retval = Locale.getMsg('navigationWillLogOutContinue');

	    	    // For IE and Firefox
	    	    var e = e || window.event;
	    	    if(e) {
	    	        e.returnValue = retval;
	    	    }
    	    } catch(ex) {
    	    	//we may not log as usual here, because the page will be unloaded while writing to the database
    	    	//just write to the console:
    	    	console.log('Exception occurred inside pageLeavingHandler of BaseClientStateController');
    	    }

    	    // For Safari / chrome
    	    return retval;
    	},

    	//sets the clients language
    	//the passed parameter has to be of format "xx-XX" (e.g. en-US or de-DE)
    	setClientLocale: function(locale, callback) {
			try {
				AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Trying to set client locale to: ' + locale, true);

				if(AssetManagement.customer.utils.StringUtils.matches(locale, /^[a-z]{2}-[A-Z]{2}$/)) {
					//Locale only expects the format as xx_XX, so change it
					var newLocale = locale.split('-');
					newLocale = newLocale[0] + '_' + newLocale[1];

					var formerLocale = '';

					try {
						//will throw exception, if no locale yet set
						formerLocale = Locale.getCurrentLanguage();
					} catch(ex) {
					}

					var isInitialApplicationSet = formerLocale === '';

					if(formerLocale !== newLocale) {
						Locale.setLanguageUrls([
						    'resources/localization/lang_en_US.txt',
						    'resources/localization/lang_de_DE.txt'
						]);

						Locale.setCurrentLanguage(newLocale);

						var me = this;

						var innerCallback = function() {
							try {
								AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Setting client locale to: ' + locale, true);

								AssetManagement.customer.core.Core.getAppConfig().setLocale(locale);

								if(isInitialApplicationSet === false) {
									me.adoptViewsToNewLocale();
								}

								if(callback)
									callback();
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setClientLocale of BaseClientStateController', ex);

								if(callback)
									callback();
							}
						};

						Locale.loadAsync(innerCallback);
					} else {
						AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Cancel setting client locale. Current and target locale are the same.', true);
					}
				} else {
					if(callback)
						callback();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setClientLocale of BaseClientStateController', ex);

				if(callback)
					callback();
			}
		},

		resetAllViews: function() {
            try {
                AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Resetting Views', true);
                var arrayOfViews = AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').getAllShownSingletonViewsOfThisSession();

				if(arrayOfViews && arrayOfViews.length > 0) {
                    //clear the views if there are some layout records coming in druing sync.
                    Ext.Array.each(arrayOfViews, function(view) {
                    	if(view.xtype === 'pageshomescreenpage' || view.xtype === 'synchronizationdialog') {
                            //view.self.resetInstance()
						} else {
                            view.self.resetInstance()
						}

                    }, this);
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetAllViews of BaseClientStateController', ex);
            }
		},

		//resets the client for the current user
		//including database, caches and funcpara/userInfo
		//use the storesToKeep parameter to exclude stores from deletion
		resetClient: function(callback, callbackScope, storesToKeep) {
			try {
				AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Performing client reset.', true);

				var innerCallback = function(success) {
					try {
						var notification = null;

						if(success) {
							var ac = AssetManagement.customer.core.Core.getAppConfig();

							ac.setSyncResetClient(true);
							ac.setLastSync(null);

							AssetManagement.customer.manager.CacheManager.clearCaches();
							AssetManagement.customer.model.bo.UserInfo.getInstance(true);
							AssetManagement.customer.model.bo.FuncPara.getInstance(true);
						} else {
							AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Client reset failed. User\'s data could not be deleted.', true);
						}

						if(callback)
							callback.call(callbackScope, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetClient of BaseClientStateController', ex);

						if(callback)
							callback.call(callbackScope, false);
					}
				};

				AssetManagement.customer.core.Core.getDataBaseHelper().wipeUserData(innerCallback,storesToKeep);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetClient of BaseClientStateController', ex);

				if(callback)
					callback.call(callbackScope, false);
			}
		},

		tryLogInUser: function(mandt, userId, password, successCallback, failureCallback) {
			try {
		        var me = this;

		        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mandt) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userId) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(password)) {
		            failureCallback();
					return;
		        }

		        //userid works non cases sensetive, to ensure this setting it to lowercase
		        userId = userId.toLowerCase();

		        AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Try to login user: ' + mandt + '/' + userId, true);

		        var callback = function (valid, userInfo) {
		            try {
		                if (valid) {
		                    //user is valid, next check, if the user has to update the application
		                    var applicationUpdateCallback = function (mayContinue) {
		                        try {
		                            if (mayContinue) {
		                                //there was no need to update the application, next the a scenario needs to be selected
		                                var onScenarioSelected = function (scenario) {
		                                    try {
		                                        me.enterLoadingState(0);

		                                        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(scenario)) {
		                                            //enhancement spot for database upgrade check
		                                            var databaseUpgradeCallback = function (mayContinue) {
		                                                try {
		                                                    if (mayContinue === true) {
		                                                        me.logInUser(mandt, userId, password, scenario, userInfo);

		                                                        if (successCallback)
		                                                            successCallback();
		                                                    } else if (failureCallback) {
		                                                        failureCallback(Locale.getMsg('applicationUpdateCouldNotBeCompleted') + ' ' + Locale.getMsg('completeUpdateBeforeLogin'));
		                                                    }
		                                                } catch (ex) {
		                                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLogInUser of BaseClientStateController', ex);

		                                                    if (failureCallback) {
		                                                        failureCallback();
		                                                    }
		                                                }
		                                            };

		                                            me.performDataBaseUpgradeCheck(mandt, userId, password, scenario, databaseUpgradeCallback);
		                                        } else if (failureCallback) {
                                                    //no scenario available - show a corresponding error message
		                                            failureCallback.call(me, Locale.getMsg('missingAnyScenarioAssignment'));
		                                        }
		                                    } catch (ex) {
		                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLogInUser of BaseClientStateController', ex);

		                                        if (failureCallback)
		                                            failureCallback.call(me, userInfo);
		                                    }
		                                };

                                    var cancelcallback = function(){
                                        failureCallback.call(me, "");
                                    }

		                                me.getScenario(mandt, userId, userInfo, onScenarioSelected, cancelcallback);
		                            } else {
		                                AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Login failed. Mandatory application update could not be done.', true);

		                                if (failureCallback)
		                                    failureCallback.call(me, userInfo);
		                            }
		                        } catch (ex) {
		                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLogInUser of BaseClientStateController', ex);

		                            if (failureCallback)
		                                failureCallback.call(me, userInfo);
		                        }
		                    };

		                    AssetManagement.customer.controller.AppUpdateController.checkSVMForApplicationUpdate(userInfo, applicationUpdateCallback);
		                } else {
		                    AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Login failed. Authentication not successful.', true);

		                    if (failureCallback)
		                        failureCallback.call(me, userInfo);
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLogInUser of BaseClientStateController', ex);

		                if (failureCallback)
		                    failureCallback.call(me, userInfo);
		            }
		        };

		        AssetManagement.customer.manager.UserManager.checkUserIsValid(mandt, userId, password, callback);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLogInUser of BaseClientStateController', ex);

		        if (failureCallback)
		            failureCallback.call(this);
		    }
		},

		logOutUser: function() {
			try {
				var ac = AssetManagement.customer.core.Core.getAppConfig();
				 var mandt = ac && ac.getMandt();
			     var userId = ac && ac.getUserId();

				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userId))
					AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Logout for user: ' + mandt + '/' + userId,true);
					else {
						AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] A logout has been triggered, but no user has been logged in.', true);
					}
				//mandt is not set back, for preset value at next login
				ac.setUserLoggedIn(false);
                //The userid shouldn't be cleared after logout.  discussed with Dirk about this.
                //ac.setUserId("");
				ac.setUserPassword("");
				AssetManagement.customer.manager.CacheManager.clearCaches();

				this.disablePageLeavingHandler();

				AssetManagement.customer.model.bo.UserInfo.resetInstance();


			    //in opposite to the previous approach, the toolbar is hidden before navigating to login page.
			    //This prevents blue screen on logout
				AssetManagement.customer.core.Core.getMainView().getController().hideOptionsToolbar();
				AssetManagement.customer.core.Core.getMainView().getController().hideStatusBar();

				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_LOGINSCREEN, null, false);

				if (!ac.isDebugMode())
				    AssetManagement.customer.helper.OxLogger.clearConsole();

			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logOutUser of BaseClientStateController', ex);
			}
		},

		//private
		getScenario: function (mandt, userId, userInfo, callback, cancelcallback) {
		    if (!callback)
		        return;

		    try {
		        var me = this;

		        var selectScenarioAction = function (scenarios) {
		            try {
		                //if there are more than one scenarios available, the user is prompted to select one
		                if (scenarios.length > 1) {
		                    var dialogArgs = {
		                        scenarios: scenarios,
		                        callback: callback,
		                        callbackScope: me,
								cancelcallback: cancelcallback
		                    };

		                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SCENARIO_DIALOG, dialogArgs);
		                } else {
		                    //only one scenario available, so just continue automatically
                            //or even nothing (error case)
		                    callback.call(this, scenarios[0] ? scenarios[0].SCENARIO : '');
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScenario of BaseClientStateController', ex);
		                callback.call(me, '');
		            }
		        };

		        //if the user info object contains scenario objects, use them for the selection
		        if (userInfo._infoString !== '') {
		            var userInformation = Ext.decode(userInfo._infoString);

		            var info;

		            for (var propName in userInformation) {
		                info = userInformation[propName];
		            }

		            var scenarios = info.USER_SCENARIOS;

		            //scenarios extracted
		            //next save them in the database, so for following offline logins the user can use this for selection
		            var dbHelper = AssetManagement.customer.core.Core.getDataBaseHelper();

		            for (var i = 0 ; i < scenarios.length; i++) {
		                //write the user id to the scenarios objects
		                scenarios[i].USERID = scenarios[i].MOBILEUSER.toLowerCase();
		                dbHelper.put('S_SCENARIO', scenarios[i])
		            }

		            //now call the selection action
		            selectScenarioAction.call(this, scenarios);
		        } else {
		            //the user info object does not contain scenario objects (offline case)
		            //use the scenario objects from the last succesful online login (from database)
		            var scenarios = [];

		            var getScenariosDBCallback = function (eventArgs) {
		                try {
		                    var cursor = eventArgs.target.result;

		                    if (cursor) {
		                        scenarios.push(eventArgs.target.result.value);

		                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
		                    } else {
		                        selectScenarioAction.call(me, scenarios);
		                    }
		                } catch (ex) {
		                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScenario of BaseClientStateController', ex);
		                }
		            };

		            //do not use the DbKeyRangeHelper, because it internally fills the scenario property
		            var lower = [mandt, userId, ""];
		            var upper = [mandt, userId, "ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½"];

		            keyRange = IDBKeyRange.bound(lower, upper, false, false);
		            AssetManagement.customer.core.Core.getDataBaseHelper().query('S_SCENARIO', keyRange, getScenariosDBCallback, null, false);
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getScenario of BaseClientStateController', ex);
		        callback.call(this, '');
		    }
		},

		performDataBaseUpgradeCheck: function (mandt, userId, password, scenario, callback, cancelcallback) {
		    if (!callback)
		        return;

		    try {
		        var ac = AssetManagement.customer.core.Core.getAppConfig();

		        //do a temporary slim login, because this is needed for a potential synchronisation
		        ac.setMandt(mandt);
		        ac.setScenario(scenario);
		        ac.setUserId(userId);
		        ac.setUserPassword(password);
		        ac.setUserLoggedIn(true);

		        this.enablePageLeavingHandler();

		        var databaseUpgradeCallback = function (mayContinue) {
		            //undo the temporary login
		            //this has to be executed also on exceptions...
		            ac.setMandt('');
		            ac.setScenario('');
		            ac.setUserId('');
		            ac.setUserPassword('');
		            ac.setUserLoggedIn(false);

		            this.disablePageLeavingHandler();

		            try {
		                callback.call(this, mayContinue);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDataBaseUpgradeCheck of BaseClientStateController', ex);
		            }
		        };

		        AssetManagement.customer.controller.AppUpdateController.checkForAndHandleDataBaseUpgrade(databaseUpgradeCallback, this);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDataBaseUpgradeCheck of BaseClientStateController', ex);
		        callback.call(this, false);
		    }
		},

        //private
		logInUser: function (mandt, userId, password, scenario, userInfo) {
		    try {
		        //userid works non cases sensetive, to ensure this setting it to lowercase
		        userId = userId.toLowerCase();

		        AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Logging in user: ' + mandt + '/' + userId, true);

		        var me = this;
		        AssetManagement.customer.manager.CacheManager.clearCaches();
		        AssetManagement.customer.manager.UserManager.updateUser(mandt, userId, password, userInfo);

		        var ac = AssetManagement.customer.core.Core.getAppConfig();

		        //set appConfig values
		        ac.setMandt(mandt);
		        ac.setScenario(scenario);
		        ac.setUserId(userId);
		        ac.setUserPassword(password);
		        ac.setUserLoggedIn(true);
		        //ac.setDataKey(userInfo.getPrivateDataKey());

		        ac.setUsername(userInfo.getUsername());

		        if (ac.isDebugMode() === false)
		            this.enablePageLeavingHandler();

		        var loggingManagementCallback = function (loggingSuccess) {
		            try {
		                var custInitCallback = function (custInitSuccess) {
		                    try {
		                        //load the home screen for one time, to make sure, that the time gap between showing top bar and navigation to home screen is as small as possible
		                        var homeScreenPrepareCallback = function () {
		                            try {
		                                if (!AssetManagement.customer.core.Core.getMainView().getMainMenuPanel())
		                                    AssetManagement.customer.core.Core.getMainView().initialize();

		                                //prepare main menu before show
		                                AssetManagement.customer.core.Core.getMainView().refresh();

		                                //show the elements
		                                AssetManagement.customer.core.Core.getMainView().getController().showOptionsToolbar();
		                                AssetManagement.customer.core.Core.getMainView().getController().showStatusBar(true);
		                                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HOMESCREEN);

		                                var deviceIdCallback = function () {

		                                	var onlineCallback = function(success) {
		                                		if(success) {
                                                    var lang = ac.getLocale().substr(0, 2)
                                                    var nameValuePairList = Ext.create('Ext.util.MixedCollection');
                                                    nameValuePairList.add("sap-client", ac.getMandt());
                                                    nameValuePairList.add("pa_device_id", ac.getDeviceId());
                                                    nameValuePairList.add("sap-user", ac.getUserId());
                                                    nameValuePairList.add("sap-language", ac.getSAPLanguageToSet());
                                                    nameValuePairList.add("sap-password", ac.getUserPassword());
                                                    var syncManager = AssetManagement.customer.sync.SyncManager.getInstance()

                                                    var connectionManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                                                        linkedSyncManager: syncManager
                                                    });

                                                    Ext.defer(connectionManager.sendToBackEnd, 1, connectionManager,
                                                        [AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.DEVICECHECK, nameValuePairList, null]);
												}
											}

                                            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback)

		                                    /*if (AssetManagement.customer.helper.NetworkHelper.isClientOnline()) {
		                                        var lang = ac.getLocale().substr(0, 2)
		                                        var nameValuePairList = Ext.create('Ext.util.MixedCollection');
		                                        nameValuePairList.add("sap-client", ac.getMandt());
		                                        nameValuePairList.add("pa_device_id", ac.getDeviceId());
		                                        nameValuePairList.add("sap-user", ac.getUserId());
		                                        nameValuePairList.add("sap-language", ac.getSAPLanguageToSet());
		                                        nameValuePairList.add("sap-password", ac.getUserPassword());
		                                        var syncManager = AssetManagement.customer.sync.SyncManager.getInstance()

		                                        var connectionManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
		                                            linkedSyncManager: syncManager
		                                        });

		                                        Ext.defer(connectionManager.sendToBackEnd, 1, connectionManager,
                                                [AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.DEVICECHECK, nameValuePairList, null]);
		                                    }*/
		                                };

		                                AssetManagement.customer.core.Core.getDataBaseHelper().createDeviceId(AssetManagement.customer.core.Core.getAppConfig().getMandt(), AssetManagement.customer.core.Core.getAppConfig().getUserId(), deviceIdCallback);

		                                if (!custInitSuccess) {
		                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noDataForUserYet'), true, 1);
		                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
		                                } else {
		                                    //wait until the homescreen navigation is complete (else this checks can not request dialogs to be shown)
		                                    var pollingAction = function () {
		                                        try {
		                                            if (!me.isClientInLoadingState()) {
		                                                me.doAfterLoginChecks.call(me);
		                                            } else {
		                                                Ext.defer(pollingAction, 50, me);
		                                            }
		                                        } catch (ex) {
		                                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logInUser of BaseClientStateController', ex);
		                                        }
		                                    };

		                                    pollingAction();
		                                }
		                            } catch (ex) {
		                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logInUser of BaseClientStateController', ex);
		                            }
		                        };

		                        //necessary workaround to avoid an issue with page ready callbacks
		                        var littleDeferer = function () {
		                            try {
		                                Ext.defer(homeScreenPrepareCallback, 1, this);
		                            } catch (ex) {
		                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logInUser of BaseClientStateController', ex);
		                            }
		                        };

		                        AssetManagement.customer.view.pages.HomeScreenPage.getInstance().getController().requestPage(littleDeferer, this);
		                    } catch (ex) {
		                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logInUser of BaseClientStateController', ex);
		                    }
		                };

		                me.initializeRuntimeCustomizing(custInitCallback, me);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logInUser of BaseClientStateController', ex);
		            }
		        };

		        var loggingManagementEventId = AssetManagement.customer.helper.OxLogger.manageLogDatabaseSize();

		        if (loggingManagementEventId > 0) {
		            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(loggingManagementEventId, loggingManagementCallback);
		        } else {
              loggingManagementCallback(false);
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside logInUser of BaseClientStateController', ex);
		    }
		},

	    //performs a set of checks
		doAfterLoginChecks: function () {
		    try {
		        //do avoid stacking dialogs, do one check after another
				this.doFilesAndDocumentsSizeLimitCheck();

		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doAfterLoginChecks of BaseClientStateController', ex);
		    }
		},

		//will check, if the data amount for files and documents of the database has passed a defined limit
		//if so, the user will be informed, to synchronize
		doFilesAndDocumentsSizeLimitCheck: function() {
			try {
				var sizeCallback = function(sizeInBytes) {
					try {
						if(sizeInBytes) {
							//TODO read maximum size from AppConfig or Customizing Parameter (currently hardcoded 30 MB)
							if(sizeInBytes > 30 * 1024 * 1024) {
								var alertDialogConfig = {
									message: Locale.getMsg('sizeOfFilesToBigSync')
								};

								AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ALERT_DIALOG, alertDialogConfig);
							}
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doFilesAndDocumentsSizeLimitCheck of BaseClientStateController', ex);
					}
				};

				var sizeEventId = AssetManagement.customer.manager.DocUploadManager.getSizeOfStoredFilesForUpload();

				if(sizeEventId > 0)
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(sizeEventId, sizeCallback);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doFilesAndDocumentsSizeLimitCheck of BaseClientStateController', ex);
			}
		},

		//will get the name of the main database the application is currently working with
		getCurrentMainDataBaseName: function() {
			var retval = '';

			try {
				//the value will an empty string, if any main database has ever been opened yet
				retval = AssetManagement.customer.helper.LocalStorageHelper.getString('currentMainDataBaseNameInUse');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentMainDataBaseName of BaseClientStateController', ex);
			}

			return retval;
		},

		//will get the version number of the main database the application is currently working with
		getCurrentMainDataBaseVersion: function() {
			var retval = 0;

			try {
				//the value will be zero, if any main database has ever been opened yet
				retval = AssetManagement.customer.helper.LocalStorageHelper.getNumber('currentMainDataBaseVersionInUse');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentMainDataBaseVersion of BaseClientStateController', ex);
			}

			return retval;
		},

		//will set values at local storage, describing the main database this application is currently working with
		setCurrentMainDataBaseData: function(dataBaseName, dataBaseVersion) {
			try {
				if(!dataBaseName || !dataBaseVersion || dataBaseVersion < 0) {
					dataBaseName = '';
					dataBaseVersion = 0;
				}

				AssetManagement.customer.helper.LocalStorageHelper.putString('currentMainDataBaseNameInUse', dataBaseName);
				AssetManagement.customer.helper.LocalStorageHelper.putNumber('currentMainDataBaseVersionInUse', dataBaseVersion);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setCurrentMainDataBaseData of BaseClientStateController', ex);
			}
		},

		//will try to (re)initialize the runtime customizing for the current user
		//the provided callback will be called with an boolean, if the (re)initialization has been successful
		initializeRuntimeCustomizing: function(callback, callbackScope) {
		    try {
		        var me = this;
		        //first initialize func para - initialization of userInfo requires func. paras
				var funcParaInitCallback = function (funcParaSuccess) {
			    	try {
			    	    if (funcParaSuccess) {
			    	        //continue with initialization of user info
			    	        var userInfoInitCallback = function (userInfoSuccess) {
			    	            try {
			    	                //continue with initialization of layout
			    	                if (userInfoSuccess) {
			    	                    var layoutInitCallback = function (layoutSuccess) {
			    	                        //no try catch here, because this code has to be executed anyway
			    	                        if (callback)
			    	                            callback.call(callbackScope, layoutSuccess);
			    	                    };

			    	                    me.initializeLayout(layoutInitCallback, this);

			    	                } else if (callback) {
			    	                    callback.call(callbackScope, false);
			    	                }
			    	            } catch(ex) {
			    	                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeRuntimeCustomizing of BaseClientStateController', ex);

			    	                if(callback)
			    	                    callback.call(callbackScope, false);
			    	            }
			    	        };

			    		    this.initializeUserInfo(userInfoInitCallback, this);
			    		} else if (callback) {
                            callback.call(callbackScope, false);
			    		}
			    	} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeRuntimeCustomizing of BaseClientStateController', ex);

						if(callback)
							callback.call(callbackScope, false);
					}
			    };

				this.initializeFuncPara(funcParaInitCallback, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeRuntimeCustomizing of BaseClientStateController', ex);

				if(callback)
					callback.call(callbackScope, false);
			}
		},

		enablePageLeavingHandler: function() {
			try {
				window.onbeforeunload = AssetManagement.customer.controller.ClientStateController.pageLeavingHandler;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside enablePageLeavingHandler of BaseClientStateController', ex);
			}
		},

		disablePageLeavingHandler: function() {
			try {
				window.onbeforeunload = null;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside disablePageLeavingHandler of BaseClientStateController', ex);
			}
		},

		initializeAutoLogout: function () {
		    try {
		        var logoutTimeout = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('logout_timeout');

		        if (logoutTimeout) {
		            this._logoutTimeoutIntervall = Number(logoutTimeout) * 1000;
		        }

		        var utcNow = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');

		        //set logout time stamp
		        this._logoutTimeStamp = utcNow.getTime() + this._logoutTimeoutIntervall;

		        var me = this;

		        document.body.addEventListener('click', function (event) {
		            me.checkAutoLogOutTimeOut(event);
		        }, false);

		        document.body.addEventListener('touchstart', function (event) {
		            me.checkAutoLogOutTimeOut(event);
		        });

		        window.addEventListener('focus', function (event) {
		            me.checkAutoLogOutTimeOut(event);
		        });
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeAutoLogout of BaseClientStateController', ex);
		    }
		},

		//will call an api on all shown controls of this session, so they adopt to the new locale
		adoptViewsToNewLocale: function() {
			try {
				var arrayOfViews = AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').getAllShownSingletonViewsOfThisSession();

				if(!arrayOfViews)
					arrayOfViews = new Array();

				arrayOfViews.push(AssetManagement.customer.core.Core.getMainView());

				if(arrayOfViews && arrayOfViews.length > 0) {
					//the usual way is, to call the 'onLocaleChanged' API of these controls, do so, if they have this API
					Ext.Array.each(arrayOfViews, function(view) {
						var viewsController = view.getController();
						var target = viewsController ? viewsController : view;

						if(typeof target['onLocaleChanged'] === 'function') {
							target['onLocaleChanged'].call(target);
						}
					}, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adoptViewsToNewLocale of BaseClientStateController', ex);
			}
		},

		//private
	    //will try to (re)initialize userInfo instance
	    //the provided callback will be called with an boolean, if the (re)initialization has been successful
		initializeUserInfo: function (callback, callbackScope) {
		    try {
		        var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance(true);

		        var userInfoInitCallback = function (success) {
		            try {
		                userInfo.removeListener('initialized', userInfoInitCallback, this);

		                if (callback)
		                    callback.call(callbackScope, success);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeUserInfo of BaseClientStateController', ex);

		                if (callback)
		                    callback.call(callbackScope, false);
		            }
		        };

		        userInfo.addListener('initialized', userInfoInitCallback, this);
		        AssetManagement.customer.manager.UserInfoManager.initializeUserInfoInstance(userInfo);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeUserInfo of BaseClientStateController', ex);

		        if (callback)
		            callback.call(callbackScope, false);
		    }
		},

	    //private
	    //will try to (re)initialize funcPara instance
	    //the provided callback will be called with an boolean, if the (re)initialization has been successful
		initializeFuncPara: function (callback, callbackScope) {
		    try {
		        var custPara = AssetManagement.customer.model.bo.FuncPara.getInstance(true);

		        var custParaInitCallback = function (success) {
		            try {
		                custPara.removeListener('initialized', custParaInitCallback, this);

		                if (callback)
		                    callback.call(callbackScope, success);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeFuncPara of BaseClientStateController', ex);

		                if (callback)
		                    callback.call(callbackScope, false);
		            }
		        };

		        custPara.addListener('initialized', custParaInitCallback, this);
		        AssetManagement.customer.manager.CustomizingManager.initializeCustParaInstance(custPara);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeFuncPara of BaseClientStateController', ex);

		        if (callback)
		            callback.call(callbackScope, false);
		    }
		},

	    //private
	    //checks the auto logoff timeout
		checkAutoLogOutTimeOut: function (event) {
		    try {
		        var appConfig = AssetManagement.customer.core.Core.getAppConfig();
		        var userIsLoggedIn = false;

		        if (appConfig) {
		            userIsLoggedIn = appConfig.isUserLoggedIn();
		        }

		        var logoutTimeout = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('logout_timeout');
		        if (logoutTimeout) {
		            this._logoutTimeoutIntervall = Number(logoutTimeout) * 1000;
		        }

		        var utcNow = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');
		        var utcNowTimeStamp = utcNow.getTime();

		        if (utcNowTimeStamp >= this._logoutTimeStamp) {
		            if (userIsLoggedIn) {
		                var me = this;

		                //stop the firing user action event to trigger anything on the application
                        AssetManagement.customer.helper.EventHelper.stopEventPropagation(event);

		                var performAutoLogOffAction = function () {
		                    try {
		                        me.logOutUser();

		                        var alertDialogConfig = {
		                            message: Locale.getMsg('logoutAutomatic')
		                        };

		                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ALERT_DIALOG, alertDialogConfig);

		                        utcNow = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime('UTC');
		                        me._logoutTimeStamp = utcNow.getTime() + me._logoutTimeoutIntervall;
		                    } catch (ex) {
		                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkAutoLogOutTimeOut of BaseClientStateController', ex);
		                    }
		                };

		                //before the log out can be performed all open dialogs have to be closed first
		                var currentlyShownDialogs = AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').getCurrentShownDialogs();

		                //closing open dialogs requires polling for cancellation - direct closing is harmful (e.g. running synchronization)
		                var closeDialogsAction = function () {
		                    try {
		                        if (currentlyShownDialogs && currentlyShownDialogs.length > 0) {
		                            //request cancelation on all currently shown dialogs
		                            Ext.Array.each(currentlyShownDialogs, function (dialog) {
		                                dialog.getController().requestCancellation();
		                            });

		                            //continue polling until all dialogs have been closed
		                            Ext.defer(closeDialogsAction, 50, this);
		                        } else {
		                            performAutoLogOffAction();
		                        }
		                    } catch (ex) {
		                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkAutoLogOutTimeOut of BaseClientStateController', ex);
		                    }
		                };

		                //begin with first cancelation request poll
		                closeDialogsAction();
		            } else {
		                this._logoutTimeStamp = utcNowTimeStamp + this._logoutTimeoutIntervall;
		            }
		        } else {
		            this._logoutTimeStamp = utcNowTimeStamp + this._logoutTimeoutIntervall;
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkAutoLogOutTimeOut of BaseClientStateController', ex);
		    }
		},

	    //private
		initializeLayout: function (callback, callbackScope) {
		    try {
		        var viewLayout = AssetManagement.customer.model.bo.ViewLayout.getInstance(true);

		        var viewLayoutInitCallback = function (success) {
		            try {
		                viewLayout.removeListener('initialized', viewLayoutInitCallback, this);

		                if (callback)
		                    callback.call(callbackScope, success);
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeLayout of BaseClientStateController', ex);

		                if (callback)
		                    callback.call(callbackScope, false);
		            }
		        };

		        viewLayout.addListener('initialized', viewLayoutInitCallback, this);
		        AssetManagement.customer.manager.ViewLayoutManager.initializeViewLayoutInstance(viewLayout);
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeLayout of BaseClientStateController', ex);

		        if (callback)
		            callback.call(callbackScope, false);
		    }
		}
	}
});