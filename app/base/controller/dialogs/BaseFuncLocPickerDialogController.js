Ext.define('AssetManagement.base.controller.dialogs.BaseFuncLocPickerDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

    
    requires: [
       'AssetManagement.customer.manager.FuncLocManager',
       'AssetManagement.customer.utils.StringUtils',
 	   'AssetManagement.customer.helper.OxLogger'
    ],
    
	config: {
		contextReadyLevel: 1
	},
	
	inheritableStatics: {
		//interface
		ON_FUNCLOC_SELECTED: 'onFuncLocSelected'
	},
	
	//@override
	startBuildingDialogContext: function(argumentsObject) {
		try {
			var funcLocs = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(true);
			this.addRequestToDataBaseQueue(funcLocs, "funcLocs");
		
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseFuncLocPickerDialogController', ex)
		}
	},

	onFuncLocCellSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts){
		try {
			this.onFuncLocSelected(record);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiCellSelected of BaseFuncLocPickerDialogController', ex)
		}
	}, 

	onFuncLocSelected: function(funcLocInfoObject) {
		try {
			var callbackInterface = this.getOwner()[this.self.ON_FUNCLOC_SELECTED];
			
			if(callbackInterface)
				callbackInterface.call(this.getOwner(), funcLocInfoObject);
			
			//dismiss
			this.dismiss();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseFuncLocPickerDialogController', ex)
		}
    },
	
	//@override
	getContextLoadingErrorMessage: function() {
		return "Data error while loading funcLocs.";
	},

	onFuncLocSearchButtonClick: function () {
	    try {
	        this.getView().getSearchValues();

	        var funcLocToSearch = this.getViewModel().get('searchFuncLoc');
	        var funcLocStore = this.getViewModel().get('funcLocs');

	        funcLocStore.clearFilter();

	        //filter equis
	        if (funcLocToSearch.get('tplnr'))
	            funcLocStore.filterBy(function (record) {
	                try {
	                    if (record.get('tplnr').toUpperCase().indexOf(funcLocToSearch.get('tplnr').toUpperCase()) > -1) {
	                        return true;
	                    }
	                } catch (ex) {
	                    AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocPickerDialogController', ex)
	                }
	            });
	        if (funcLocToSearch.get('eqfnr'))
	            funcLocStore.filterBy(function (record) {
	                try {
	                    if (record.get('eqfnr').toUpperCase().indexOf(funcLocToSearch.get('eqfnr').toUpperCase()) > -1) {
	                        return true;
	                    }
	                } catch (ex) {
	                    AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocPickerDialogController', ex)
	                }
	            });	              

	        this.getViewModel().set('funcLocs', funcLocStore);

	        this.getView().refreshView();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSearchButtonClick of BaseFuncLocPickerDialogController', ex)
	    }
	}
});