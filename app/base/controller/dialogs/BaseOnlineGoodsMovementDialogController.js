Ext.define('AssetManagement.base.controller.dialogs.BaseOnlineGoodsMovementDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',
    alias: 'controller.BaseOnlineGoodsMovementDialogController',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.NetworkHelper',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.sync.SyncManager',
        'AssetManagement.customer.sync.ParsingManager',
        'AssetManagement.customer.model.bo.PoItem'
    ],

    config: {
        cancelable: false,
        infinite: true
    },

  inheritableStatics: {
        REQUEST_DATATYPE: 'MM_GOODS_MOVEMENT'
    },

    _currentRequestCriteria : null,
    _syncManager: null,

    _goodsMovementHeader: null,
    _goodsMovementItems: null,

    //protected
    //@override
    beforeInitialization: function(argumentsObject) {
        try {
            this._goodsMovementHeader = argumentsObject['goodsMovementHeader'];
            this._goodsMovementItems = argumentsObject['goodsMovementItems'];

            var syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
            this._uploadManager = Ext.create('AssetManagement.customer.sync.UploadManager', {
                dataBaseHelper: null,
                linkedSyncManager: syncManager,
                listeners: {
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });

            this._parsingManager = Ext.create('AssetManagement.customer.sync.ParsingManager', {
                dataBaseHelper: AssetManagement.customer.core.Core.getDataBaseHelper(),
                linkedSyncManager: syncManager,
                listeners: {
                    progressChanged: { fn: this.innerProgressReporter, scope: this },
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }

            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseOnlineGoodsMovementDialogController', ex);
        }
    },

    //protected
    //@override
    //prevent starting the process on show
    onStartProcess: function () {
        var retval = false;

        try {
            retval = true;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStartProcess of BaseOnlineGoodsMovementDialogController', ex);
            this.reportError();
        }

        return retval;
    },

    //protected
    //@override
    doWork: function () {
        try {
            this.prepareOnlineOrderCreationRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doWork of BaseOnlineGoodsMovementDialogController', ex);
            this.errorOccured();
        }
    },

    prepareOnlineOrderCreationRequest: function() {
        try {
            //check if client is online
            var me = this;

            var onlineCallback = function(success) {
                if(success) {
                    //get the upstream for the selected assignments
                    var upstream = me.getUploadStreamForDownloadAssingments();

                    //if there is no upstream, stop with an error
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(upstream)) {
                        me.reportError();
                        return;
                    }
                    //do the actual online call next
                    me.callOnlineRequestAPI(upstream);
                }else{
                    var errorMessage = Locale.getMsg('browserIsOffline');
                    me.reportError(errorMessage);
                    return;
                }
            };
            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback)
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareOnlineOrderCreationRequest of BaseOnlineGoodsMovementDialogController', ex);
            this.reportError();
        }
    },

    callOnlineRequestAPI: function(upstream) {
        try {
            var me = this;

            var postValues = this.getRequestPostValues(upstream);

            if(!postValues) {
                me.errorOccured();
                return;
            }

            var callback = function (jsonResponse) {
                try {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(jsonResponse)) {
                        me._syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
                        me._timeStampDown = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
                        var jsonResponseObject = Ext.decode(jsonResponse);
                        var onlineOrderCreationResponse = jsonResponseObject ? jsonResponseObject['ONLINE_REQUEST_RESULT'] : undefined;

                        if(onlineOrderCreationResponse.RESULT_CODE === '0' ) {
                            me.onWorkFinished(onlineOrderCreationResponse.RESULT_MESSAGE);
                            // me.confirmResponse(onlineOrderCreationResponse);
                        } else {
                            me.examineResponse(onlineOrderCreationResponse);
                        }
                    } else{
                        me.reportError();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineGoodsMovementDialogController', ex);
                    me.reportError();
                }
            };

            var errorCallback = function (failureTypeOrErrorMessage) {
                try {
                    var message = '';

                    switch(failureTypeOrErrorMessage) {
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                            message = Locale.getMsg('browserIsOffline');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                            message = Locale.getMsg('connectionTimeoutOccured');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                            message = Locale.getMsg('mymUnknownError');
                            break;
                    }

                    me.reportError(message);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineGoodsMovementDialogController', ex);
                    me.reportError();
                }
            };

            var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                listeners: {
                    sendFailed: { fn: errorCallback, scope: this },
                    errorOccured: { fn: errorCallback, scope: this }
                }
            });

            connManager.sendToBackEnd(AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.ONLINEREQ, postValues, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineGoodsMovementDialogController', ex);
            this.reportError()
        }
    },

    confirmResponse: function (responseObject) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelWork();
                return;
            }

            var syncGuid = responseObject['SYNC_RESPONSE'] ? responseObject['SYNC_RESPONSE']['SYNC_GUID'] : '';
            if(syncGuid) {
                AssetManagement.customer.sync.SyncManager.getInstance().confirmSyncGUID(syncGuid);
            }

            this.examineResponse(responseObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmResponse of CreateQuotationDialogController', ex);
            this.errorOccured();
        }
    },

    examineResponse: function (responseObject) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logMessage('Begin examining response.');

            var mainResultCode = responseObject['RESULT_CODE'];
            var hasResponseError = false;
            var firstErrorMessage = '';
            if(mainResultCode && mainResultCode !== "0") {
                hasResponseError = true;
                var returnErrorResults = responseObject.RESULTS[0];
                var returnErrorTabs = returnErrorResults.RETURN_TAB;
                var errorMessageArray = [];
                for(var i = 0; i < returnErrorTabs.length; i++){
                    if(errorMessageArray.length === 0){
                        errorMessageArray.push(returnErrorTabs[i].MESSAGE)
                    }else{
                        if(errorMessageArray.indexOf(returnErrorTabs[i].MESSAGE) < 0){ //Filter for duplicate Messages
                            errorMessageArray.push(returnErrorTabs[i].MESSAGE);
                        }
                    }
                }
            }

            if (hasResponseError) {
                AssetManagement.customer.helper.OxLogger.logMessage('Aborting request because an error has been found on the response.');
                AssetManagement.customer.helper.OxLogger.logMessage('End examining response.');
                this.reportErrorArray(errorMessageArray);
                return;
            } else {
                AssetManagement.customer.helper.OxLogger.logMessage('End examining response.');
                var messagesArray = responseObject ? responseObject['RESULTS'] : null;
                this.onWorkFinished(returnMessagesStore);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside examineResponse of CreateQuotationDialogController', ex);
            this.errorOccurred();
        }
    },

    buildResponseMessagesStore: function (messagesArray) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelWork();
                return;
            }

            var returnMessagesStore = Ext.create('Ext.data.Store', {
                extend: 'Ext.data.Model',
                fields: [
                    { name: 'busobject',            type: 'auto' },
                    { name: 'busobjkey',            type: 'string' },
                    { name: 'resultMessage',        type: 'string' },
                    { name: 'resultCode',           type: 'string' }

                ]
            });

            if(messagesArray && messagesArray.length > 0) {
                var i = 0;
                var msgArrayLength = messagesArray.length;
                for(i ; i < msgArrayLength; i++) {
                    if(messagesArray[i]['BUSOBJECT_KEYS'].length > 0) {
                        var busObject = messagesArray[i]['BUSOBJECT_KEYS'][0]['BUSOBJECT'];
                        var busObjectKey = messagesArray[i]['BUSOBJECT_KEYS'][0]['BUSOBJKEY'];

                    }

                    var returnMessage = Ext.create('Ext.data.Model', {
                        busobject: busObject ? busObject : '',
                        busobjkey: busObjectKey ? busObjectKey : '',
                        resultMessage: messagesArray[i]['RESULT_MESSAGE'],
                        resultCode: messagesArray[i]['RESULT_CODE']
                    });

                    returnMessagesStore.add(returnMessage);
                }
            }
            this.onWorkFinished(returnMessagesStore);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred loadStockDataFromDatabase buildResponseMessagesStore of OrderCreationProgressDialogController', ex);
            this.reportError();
        }
    },
    //private
    //previous step: postVanChange
    //next step: examineResponse
    getUploadStreamForDownloadAssingments: function () {
        var retval = '';

        try {
            var goodsMovementHeader = this._goodsMovementHeader;
            var goodsMovementItems = this._goodsMovementItems;
            var objectItemsArray = [];

            if(goodsMovementItems && goodsMovementItems.getCount() > 0){
                goodsMovementItems.each(function(goodsMovementItem){
                    var objectListItem = {
                        "MATNR" : goodsMovementItem.get('matnr'),
                        "WERKS" : goodsMovementItem.get('werks'),
                        "LGORT" : goodsMovementItem.get('lgort'),
                        "BWART" : goodsMovementItem.get('bwart'),
                        "CHARG" : goodsMovementItem.get('charg'),
                        "MENGE" : goodsMovementItem.get('menge'),
                        "MEINS" : goodsMovementItem.get('meins'),
                        "EBELN" : goodsMovementItem.get('ebeln'),
                        "EBELP" : goodsMovementItem.get('ebelp'),
                        "WEMPF" : goodsMovementItem.get('wempf'),
                        "KDAUF" : goodsMovementItem.get('kdauf'),
                        "ABLAD" : goodsMovementItem.get('ablad'),
                        "KOSTL" : goodsMovementItem.get('kostl'),
                        "AUFNR" : goodsMovementItem.get('aufnr'),
                        "AUFPS" : goodsMovementItem.get('aufps'),
                        "RSNUM" : goodsMovementItem.get('rsnum'),
                        "RSPOS" : goodsMovementItem.get('rspos'),
                        "PS_POSID" : goodsMovementItem.get('ps_posid'),
                        "ITEM_TEXT" : goodsMovementItem.get('item_text'),
                        "KZBEW" : goodsMovementItem.get('kzbew'),
                        "LIFNR" : goodsMovementItem.get('lifnr'),
                        "MOVE_REAS" : goodsMovementItem.get('move_reas'),
                        "UMGLO" : goodsMovementItem.get('umglo')
                    };
                    objectItemsArray.push(objectListItem);
                });
            }

            var order = {
                "MOVEMENT" : {
                    "HEADER" : {
                        "PSTNG_DATE" : goodsMovementHeader.get('pstng_date'),
                        "DOC_DATE" : goodsMovementHeader.get('doc_date'),
                        "REF_DOC_NO" : goodsMovementHeader.get('ref_doc_no'),
                        "BILL_OF_LADING" : goodsMovementHeader.get('bill_of_lading'),
                        "GR_GI_SLIP_NO" : goodsMovementHeader.get('gr_gi_slip_no'),
                        "PR_UNAME" : goodsMovementHeader.get('pr_uname'),
                        "HEADER_TXT" : goodsMovementHeader.get('header_txt'),
                        "VER_GR_GI_SLIP" : goodsMovementHeader.get('ver_gr_gi_slip'),
                        "VER_GR_GI_SLIPX" : goodsMovementHeader.get('ver_gr_gi_slipx'),
                        "EXT_WMS" : goodsMovementHeader.get('ext_wms'),
                        "REF_DOC_NO_LONG" : goodsMovementHeader.get('ref_doc_no_long'),
                        "BILL_OF_LADING_LONG" : goodsMovementHeader.get('bill_of_lading_long'),
                        "BAR_CODE" : goodsMovementHeader.get('bar_code'),
                        "PRINT_VERSION" : goodsMovementHeader.get('print_version'),
                        "GM_CODE" : goodsMovementHeader.get('gm_code')
                    },
                    "ITEMS" : objectItemsArray
                }
            };
            var orderAsJSON = JSON.stringify(order);
            return orderAsJSON;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadStreamForAssignments of BaseOnlineGoodsMovementDialogController', ex);
        }

        return retval;
    },

    //private
    //builds the post values for the online request
    getRequestPostValues: function (upstream) {
        var retval = null;

        try {
            var nameValuePairList = Ext.create('Ext.util.MixedCollection');

            var ac = AssetManagement.customer.core.Core.getAppConfig();
            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("UPTIMESTAMP", AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(AssetManagement.customer.utils.DateTimeUtils.getCurrentTime()));
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());

            //set datatype for online api
            nameValuePairList.add("DATATYPE", this.self.REQUEST_DATATYPE);

            //set upstream
            nameValuePairList.add("UPSTREAM", upstream);

            retval = nameValuePairList;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRequestPostValues of BaseOnlineGoodsMovementDialogController', ex);
        }

        return retval;
    },

    //protected
    //@override
    getGeneralProcessErrorMessage: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('generalErrorOccurredDuringDownloadingAssignments');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralProcessErrorMessage of BaseOnlineGoodsMovementDialogController', ex);
        }

        return retval;
    },

    //private
    reportWorkProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.reportProgress(curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkProgress of BaseOnlineGoodsMovementDialogController', ex);
            this.errorOccured(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    errorOccured: function (message) {
        try {
            this._processRunning = false;
            this._waitingForNetwork = false;
            this._waitingForParsing = false;
            this._cancelWorkRequested = false;

            this._errorOccured = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = Locale.getMsg('newQuotationGeneralProcessingError');

            this.reportError(message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOcurred of BaseOnlineGoodsMovementDialogController', ex);
        }
    },

    //private
    cancelWork: function () {
        try {
            this._processRunning = false;
            this._cancelWorkRequested = false;

            this.onWorkerCancelled();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseOnlineGoodsMovementDialogController', ex);
            this.errorOccured(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    innerProgressReporter: function (percentage, message) {
        try {
            var additionalPercentage = percentage - this._curProcessStepPercentage;
            this._curProcessStepPercentage = percentage;
            this.reportWorkProgress(this._curProcessPercent + this._curProcessStepRange * additionalPercentage / 100, message);
        } catch (ex) {
            this.errorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside innerProgressReporter of BaseOnlineGoodsMovementDialogController', ex);
        }
    },


    reportErrorArray: function(messageArray) {
        try {
            this._errorOnWorker = true;
            var message = '';
            if (messageArray.length === 0)
                message = this.getGeneralProcessErrorMessage();


            var func = function(messageArray) {
                this.cancel(messageArray);
            };

            Ext.defer(func, 2000, this, [messageArray]);

            if (this.getInfinite())
                this.getView().stopProgressBar();

            this.getView().refreshView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportError of BaseCancelableProgressDialogController', ex);
        }
    }

});