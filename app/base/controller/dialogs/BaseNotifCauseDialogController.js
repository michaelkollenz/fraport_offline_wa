Ext.define('AssetManagement.base.controller.dialogs.BaseNotifCauseDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.ArgumentsHelper',
	    'AssetManagement.customer.manager.NotifManager',
	    'AssetManagement.customer.manager.EquipmentManager',
	    'AssetManagement.customer.manager.FuncLocManager',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
	    'AssetManagement.customer.model.bo.Notif',
	    'AssetManagement.customer.model.bo.NotifItem',
	    'AssetManagement.customer.model.bo.NotifCause',
	    'AssetManagement.customer.manager.NotifCauseManager',
	    'AssetManagement.customer.helper.OxLogger'
    ],
    
    
	config: {
		contextReadyLevel: 3
	},
	
	inheritableStatics: {
		//interface
		ON_CAUSE_SAVED: 'onNotifCauseSaved'
	},
	
	//private
	_saveRunning: false,

    //protected
    startBuildingDialogContext: function(argumentsObject) {
        try {
            //extract data from the arguments
            //invalid calls are:
            //no qmnum or notif
            //fenum but missing notif or qmnum
            var myModel = this.getViewModel();
            var qmnum = '', fenum = '', urnum = '';

            //the notification is always required - the item is optional (create case)
            var loadNotifRequired = true;
            var loadNotifItemRequired = true;
            var loadNotifCauseRequired = false;

            if (typeof argumentsObject['qmnum'] === 'string') {
                qmnum = argumentsObject['qmnum'];
            } else if (Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
                myModel.set('notif', argumentsObject['notif']);
                qmnum = argumentsObject['notif'].get('qmnum');
                loadNotifRequired = false;
            }

            if (typeof argumentsObject['fenum'] === 'string') {
                fenum = argumentsObject['fenum'];
                loadNotifItemRequired = true;
            } else if (Ext.getClassName(argumentsObject['notifItem']) === 'AssetManagement.customer.model.bo.NotifItem') {
                myModel.set('notifItem', argumentsObject['notifItem']);
                fenum = argumentsObject['notifItem'].get('fenum');

                if (loadNotifRequired)
                    qmnum = argumentsObject['notifItem'].get('qmnum');
            }

            if (typeof argumentsObject['urnum'] === 'string') {
                urnum = argumentsObject['urnum'];
                loadNotifCauseRequired = true;
            } else if (Ext.getClassName(argumentsObject['notifCause']) === 'AssetManagement.customer.model.bo.NotifCause') {
                myModel.set('notifCause', argumentsObject['notifCause']);

                if (loadNotifRequired)
                    qmnum = argumentsObject['notifCause'].get('qmnum');

                if (loadNotifItemRequired)
                    fenum = argumentsObject['notifCause'].get('fenum');
            }

            if (loadNotifRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)
                || loadNotifItemRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)
                || loadNotifCauseRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(urnum)) {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.NotifCauseDialogController'
                });
            }

            if (loadNotifRequired) {
                this.loadNotif(qmnum);
            }

            if (loadNotifItemRequired) {
                this.loadNotifItem(qmnum, fenum);
            }

            if (loadNotifCauseRequired) {
                this.loadNotifCause(qmnum, fenum, urnum);
            }

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.errorOccurred();
        }

    },

    //public
    onSaveButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.trySaveNotifCause();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //@override
    onCancelButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotif: function (qmnum) {
        try {
            var notifRequest = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnum, true);
            this.addRequestToDataBaseQueue(notifRequest, 'notif');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotifItem: function (qmnum, fenum) {
        try {
            var notifItemRequest = AssetManagement.customer.manager.NotifItemManager.getNotifItem(qmnum, fenum, true);
            this.addRequestToDataBaseQueue(notifItemRequest, 'notifItem');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotifCause: function (qmnum, fenum, urnum) {
        try {
            var notifCauseRequest = AssetManagement.customer.manager.NotifManager.getNotifCause(qmnum, fenum, urnum, true);
            this.addRequestToDataBaseQueue(notifCauseRequest, 'notifCause');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //checks if the data of the notifItem object is complete
    //returns a boolean, if the checks have been passed successfully
    //if any check fails it will also trigger a corresponding error message
    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var currentInputValues = this.getView().getCurrentInputValues();

            if (!currentInputValues.causeCustCode) {
                errorMessage = Locale.getMsg('provideCauseCode');
                retval = false;
            }

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }

                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    //determine correct rbnr
    getRbnrConnectedObjects: function() {
        try {
            var myModel = this.getViewModel();
            var notif = myModel.get('notif');

            var equi = notif.get('equipment');
            var equnr = notif.get('equnr');

            if (equi) {
                myModel.set('equipment', equi);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
                this.addRequestToDataBaseQueue(equiRequest, 'equipment');
            }

            var funcLoc = notif.get('funcLoc');
            var tplnr = notif.get('tplnr');

            if (funcLoc) {
                myModel.set('funcLoc', funcLoc);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
                this.addRequestToDataBaseQueue(funcLocRequest, 'funcLoc');
            }

            var notifType = notif.get('notifType');
            var qmart = notif.get('qmart');

            if (notifType) {
                myModel.set('notifType', notifType);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
                var notifTypeRequest = AssetManagement.customer.manager.CustNotifTypeManager.getNotifType(qmart, true);
                this.addRequestToDataBaseQueue(notifTypeRequest, 'notifType');
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    getCodes: function() {
        try {
            var myModel = this.getViewModel();
            var notifType = myModel.get('notifType');

            var rbnr = AssetManagement.customer.manager.NotifManager.getRbnrForNotif(myModel.get('notif'), myModel.get('equipment'), myModel.get('funcLoc'));

            var notifCauseCustCodesGroupRequest = AssetManagement.customer.manager.CustCodeManager.getCustCodesGroups(rbnr, notifType.get('urkat'));
            this.addRequestToDataBaseQueue(notifCauseCustCodesGroupRequest, "causeCodeGroupsMap");
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },


    trySaveNotifCause: function() {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;

            var saveAction = function(checkWasSuccessfull) {
                try {
                    if(checkWasSuccessfull === true) {
                        me.prepareNotifCauseForSave();

                        var notifCause = this.getViewModel().get('notifCause');

                        var saveCallback = function(success) {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

                            try {
                                if(success === true) {
                                    me.onNotifItemCauseSaved(notifCause);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifCauseSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifCause'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifCause of NotifCauseDialogController', ex);
                            } finally {
                                me._saveRunning = false;
                            }
                        };

                        var eventId = AssetManagement.customer.manager.NotifCauseManager.saveNotifCause(notifCause);
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifCause of NotifCauseDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            //check if data is complete first
            this.checkInputValues(saveAction, this);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotifCause of NotifCauseDialogController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //protected
    //@override
    afterDataBaseQueueComplete: function () {
        try {
            if(this._contextLevel === 1) {
                this.performContextLevel1Requests();
            }
            else if(this._contextLevel === 2) {
                this.performContextLevel2Requests();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    performContextLevel1Requests: function() {
        try {
            this.getRbnrConnectedObjects();

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    performContextLevel2Requests: function() {
        try {
            this.getCodes();

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    onCauseGrpSelected: function (combo, records, eOpts ) {
        try {
            this.getView().fillCauseCodeCombobox();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    onNotifItemCauseSaved: function(notifCauseObject) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_CAUSE_SAVED];

            if(callbackInterface)
                callbackInterface.call(this.getOwner(), notifCauseObject);

            //dismiss
            this.dismiss();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    prepareNotifCauseForSave: function () {
        try {
            var currentInputValues = this.getView().getCurrentInputValues();
            var myModel = this.getViewModel();
            var notifCause = myModel.get('notifCause');
            var notif = myModel.get('notif');
            var notifItem = myModel.get('notifItem');

            if (!notifCause) {
                notifCause = Ext.create('AssetManagement.customer.model.bo.NotifCause', {
                    notif: myModel.get('notif'),
                    notifItem: myModel.get('notifItem')
                });
            }

            notifCause.set('urtxt', currentInputValues.shorttext);

            var causeCustCode = currentInputValues.causeCustCode ? currentInputValues.causeCustCode : null;
            notifCause.set('urkat', causeCustCode ? causeCustCode.get('katalogart') : '');
            notifCause.set('urgrp', causeCustCode ? causeCustCode.get('codegruppe') : '');
            notifCause.set('urcod', causeCustCode ? causeCustCode.get('code') : '');
            notifCause.set('causeCustCode', causeCustCode);

            notifCause.set('mobileKey', notif ? notif.get('mobileKey'): '');

            if(notifCause) {
                notifCause.set('childKey', notifItem.get('childKey'));

            }

            myModel.set('notifCause', notifCause);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    }

});