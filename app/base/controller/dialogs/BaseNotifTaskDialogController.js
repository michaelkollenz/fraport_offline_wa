Ext.define('AssetManagement.base.controller.dialogs.BaseNotifTaskDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.utils.ArgumentsHelper',
	    'AssetManagement.customer.manager.NotifManager',
	    'AssetManagement.customer.manager.CustomizingManager',
	    'AssetManagement.customer.manager.EquipmentManager',
	    'AssetManagement.customer.manager.FuncLocManager',
	    'AssetManagement.customer.model.bo.Notif',
	    'AssetManagement.customer.model.bo.NotifItem',
	    'AssetManagement.customer.model.bo.NotifTask',
	    'AssetManagement.customer.helper.OxLogger'
    ],
    
    config: {
		contextReadyLevel: 3
	},
	
	inheritableStatics: {
		//interface
		ON_TASK_SAVED: 'onNotifTaskSaved'
	},

	//private
	_saveRunning: false,

    //protected
    startBuildingDialogContext: function(argumentsObject) {
        try {
            //extract data from the arguments
            //invalid calls are:
            //no qmnum or notif
            //fenum but missing notif or qmnum
            var myModel = this.getViewModel();
            var qmnum = '', fenum = '', manum = '';

            //the notification is always required - the item is optional (create case)
            var loadNotifRequired = true;
            var loadNotifItemRequired = false;
            var loadNotifTaskRequired = false;

            if (typeof argumentsObject['qmnum'] === 'string') {
                qmnum = argumentsObject['qmnum'];
            } else if (Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
                myModel.set('notif', argumentsObject['notif']);
                qmnum = argumentsObject['notif'].get('qmnum');
                loadNotifRequired = false;
            }

            if (typeof argumentsObject['fenum'] === 'string') {
                fenum = argumentsObject['fenum'];
                loadNotifTaskRequired = true;
            } else if (Ext.getClassName(argumentsObject['notifItem']) === 'AssetManagement.customer.model.bo.NotifItem') {
                myModel.set('notifItem', argumentsObject['notifItem']);
                fenum = argumentsObject['notifItem'].get('fenum');

                if (loadNotifRequired)
                    qmnum = argumentsObject['notifItem'].get('qmnum');
            } else {
                fenum = '0000';
            }

            if (typeof argumentsObject['manum'] === 'string') {
                manum = argumentsObject['manum'];
                loadNotifTaskRequired = true;
            } else if (Ext.getClassName(argumentsObject['notifTask']) === 'AssetManagement.customer.model.bo.NotifTask') {
                myModel.set('notifTask', argumentsObject['notifTask']);

                if (loadNotifRequired)
                    qmnum = argumentsObject['notifTask'].get('qmnum');
                if (argumentsObject['notifTask'].get('fenum') !== '0000' && !argumentsObject['notifItem']) {
                    fenum = argumentsObject['fenum'];
                    loadNotifItemRequired = true;
                }
            }

            if (loadNotifRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)
                || loadNotifItemRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)
                || loadNotifTaskRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(manum)) {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.NotifTaskDialogController'
                });
            }

            if (loadNotifRequired) {
                this.loadNotif(qmnum);
            }

            if (loadNotifItemRequired) {
                this.loadNotifItem(qmnum, fenum);
            }

            if (loadNotifTaskRequired) {
                this.loadNotifTask(qmnum,fenum,manum);
            }

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.errorOccurred();
        }
    },

    //public
    onSaveButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.trySaveNotifTask();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //@override
    onCancelButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotif: function (qmnum) {
        try {
            var notifRequest = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnum, true);
            this.addRequestToDataBaseQueue(notifRequest, 'notif');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotifItem: function (qmnum, fenum) {
        try {
            var notifItemRequest = AssetManagement.customer.manager.NotifItemManager.getNotifItem(qmnum, fenum, true);
            this.addRequestToDataBaseQueue(notifItemRequest, 'notifItem');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },
    //private
    loadNotifTask: function (qmnum) {
        try {
            var notifTaskRequest = AssetManagement.customer.manager.NotifManager.getNotifTask(qmnum,fenum,manum, true);
            this.addRequestToDataBaseQueue(notifTaskRequest, 'notifTask');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //checks if the data of the notifItem object is complete
    //returns a boolean, if the checks have been passed successfully
    //if any check fails it will also trigger a corresponding error message
    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var currentInputValues = this.getView().getCurrentInputValues();

            if (!currentInputValues.taskCustCode) {
                errorMessage = Locale.getMsg('provideTaskCode');
                retval = false;
            }

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }

                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of NotifItemDialogController', ex)

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    getRbnrConnectedObjects: function() {
        try {
            var myModel = this.getViewModel();
            var notif = myModel.get('notif');

            var equi = notif.get('equipment');
            var equnr = notif.get('equnr');

            if (equi) {
                myModel.set('equipment', equi);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
                this.addRequestToDataBaseQueue(equiRequest, 'equipment');
            }

            var funcLoc = notif.get('funcLoc');
            var tplnr = notif.get('tplnr');

            if (funcLoc) {
                myModel.set('funcLoc', funcLoc);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
                this.addRequestToDataBaseQueue(funcLocRequest, 'funcLoc');
            }

            var notifType = notif.get('notifType');
            var qmart = notif.get('qmart');

            if (notifType) {
                myModel.set('notifType', notifType);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
                var notifTypeRequest = AssetManagement.customer.manager.CustNotifTypeManager.getNotifType(qmart, true);
                this.addRequestToDataBaseQueue(notifTypeRequest, 'notifType');
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    trySaveNotifTask: function() {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;

            var saveAction = function(checkWasSuccessfull) {
                try {
                    if (checkWasSuccessfull === true) {
                        me.prepareNotifTaskForSave();

                        var notifTask = this.getViewModel().get('notifTask');

                        var saveCallback = function(success) {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

                            try {
                                if(success === true) {
                                    me.onNotifTaskSaved(notifTask);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifTaskSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifTask'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifTask of NotifActivityDialogController', ex);
                            } finally {
                                me._saveRunning = false;
                            }
                        };

                        var eventId = AssetManagement.customer.manager.NotifTaskManager.saveNotifTask(notifTask);
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifTask of NotifActivityDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            //check if data is complete first
            this.checkInputValues(saveAction, this);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifTask of NotifActivityDialogController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //@override
    afterDataBaseQueueComplete: function () {
        try {
            if(this._contextLevel === 1) {
                this.performContextLevel1Requests();
            }
            else if(this._contextLevel === 2) {
                this.performContextLevel2Requests();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    performContextLevel1Requests: function() {
        try {
            this.getRbnrConnectedObjects();

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    performContextLevel2Requests: function() {
        try {
            this.getCodes();

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    getCodes: function () {
        try {
            var myModel = this.getViewModel();
            var notifType = myModel.get('notifType');

            var rbnr = AssetManagement.customer.manager.NotifManager.getRbnrForNotif(myModel.get('notif'), myModel.get('equipment'), myModel.get('funcLoc'));

            var notifTaskCustCodesGroupRequest = AssetManagement.customer.manager.CustCodeManager.getCustCodesGroups(rbnr, notifType.get('mfkat'));
            this.addRequestToDataBaseQueue(notifTaskCustCodesGroupRequest, "taskCodeGroupsMap");
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    onTaskGrpSelected: function (combo, records, eOpts ) {
        try {
            this.getView().fillTaskCodeCombobox();

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    onNotifTaskSaved: function(notifTaskObject) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_TASK_SAVED];

            if(callbackInterface)
                callbackInterface.call(this.getOwner(), notifTaskObject);

            //dismiss
            this.dismiss();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    prepareNotifTaskForSave: function () {
        try {
            var currentInputValues = this.getView().getCurrentInputValues();
            var myModel = this.getViewModel();
            var notifTask = myModel.get('notifTask');

            var notif = myModel.get('notif');

            var notifItem = myModel.get('notifItem');

            if (!notifTask) {
                notifTask = Ext.create('AssetManagement.customer.model.bo.NotifTask', {
                    notif: myModel.get('notif'),
                    notifItem: myModel.get('notifItem')
                });
            }

            notifTask.set('matxt', currentInputValues.shorttext);

            var taskCustCode = currentInputValues.taskCustCode ? currentInputValues.taskCustCode : null;
            notifTask.set('mnkat', taskCustCode ? taskCustCode.get('katalogart') : '');
            notifTask.set('mngrp', taskCustCode ? taskCustCode.get('codegruppe') : '');
            notifTask.set('mncod', taskCustCode ? taskCustCode.get('code') : '');
            notifTask.set('taskCustCode', taskCustCode);
            notifTask.set('mobileKey', notif ? notif.get('mobileKey'): '');

            if(notifItem) {
                notifTask.set('childKey', notifItem.get('childKey'));

            }


            myModel.set('notifTask', notifTask);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    }
});