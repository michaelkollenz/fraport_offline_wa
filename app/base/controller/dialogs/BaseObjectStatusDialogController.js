﻿Ext.define('AssetManagement.base.controller.dialogs.BaseObjectStatusDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	   'AssetManagement.customer.manager.ObjectStatusManager',
	   'AssetManagement.customer.utils.StringUtils',
	   'AssetManagement.customer.helper.OxLogger'
    ],

    config: {
        contextReadyLevel: 2
    },

    inheritableStatics: {
        //interface
        ON_STATUS_SAVED: 'onStatusSaved'
    },

    //private
    _saveRunning: false,

    mixins: {
       handlerMixin: 'AssetManagement.customer.controller.mixins.ObjectStatusDialogControllerMixin'
    },

    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var myModel = this.getViewModel();
            var objectStatusList = null;
            var obj = argumentsObject;

            if (obj) {
                //objectStatusList = obj.get('objectStatusList');
                myModel.set('object', obj);

                var objectStatusListReq = null;

                if (Ext.getClassName(argumentsObject) === 'AssetManagement.customer.model.bo.Notif') {
                    objectStatusListReq = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject('QM' + obj.get('qmnum'), true);
                } else if (Ext.getClassName(argumentsObject) === 'AssetManagement.customer.model.bo.Order') {
                    objectStatusListReq = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject('OR' + obj.get('aufnr'), true);
                } else {
                    //myModel.set('objectStatusList', objectStatusList);
                    objectStatusListReq = AssetManagement.customer.manager.ObjectStatusManager.getAllStatusStoreForObject(obj.get('objnr'), true);
                }
                //var stsmaListReq = AssetManagement.customer.manager.StatusManager.getStatusListForSchema(obj.get('stsma'), true);
                this.addRequestToDataBaseQueue(objectStatusListReq, "objectStatusList");
            } else{
                throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.dialogs.ObjectStatusDialogController'
				});
            }


            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseObjectStatusDialogController', ex)
            this.errorOccurred();
        }
    },

    //@override
    dataBaseQueueComplete: function () {
        try {
            this.callParent();

            if (this._contextLevel === 1) {
                this.performContextLevel1Requests();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseObjectStatusDialogController', ex);
        }
    },

    performContextLevel1Requests: function () {
        try {
            var myModel = this.getViewModel();
            var object = myModel.get('object');
            var objectStatusList = myModel.get('objectStatusList');

            if (object) {
                var stsmaListReq = AssetManagement.customer.manager.StatusManager.getStatusListForSchema(object.get('stsma'), true);
                this.addRequestToDataBaseQueue(stsmaListReq, "stsmaList");
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel2Requests of BaseObjectStatusDialogController', ex)
        }
    },

    beforeDataReady: function () {
        try {
            var myModel = this.getViewModel();
            var stsmaList = myModel.get('stsmaList');
            var objectStatusList = myModel.get('objectStatusList');

            var displayedlistWithOrdNr = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.Stsma',
                autoLoad: false
            });
            var displayedlistWithoutOrdNr = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.Stsma',
                autoLoad: false
            });

            if (stsmaList && stsmaList.getCount() >0) {
                stsmaList.each(function (item) {
                    var test = false;
                    var stsmaListItem = item;
                    item.set('checked', false);
                    if (objectStatusList) {
                        objectStatusList.each(function (objStatus) {
                            if (objStatus.get('stat') === item.get('estat') &&
                                objStatus.get('inact') !== "X") {
                                item.set('checked', true);
                                return false;
                            }
                        });
                    }

                    if (item.get('stonr') === "00" || item.get('stonr') === "0" || item.get('stonr') === '')
                        displayedlistWithoutOrdNr.add(item);
                    else
                        displayedlistWithOrdNr.add(item);
                });
            }

            myModel.set('displayedlistWithOrdNr', displayedlistWithOrdNr);
            myModel.set('displayedlistWithoutOrdNr', displayedlistWithoutOrdNr);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseObjectStatusDialogController', ex);
        }
    },

    //@override
    save: function () {
        try {
            if (!this._saveRunning)
                this.trySaveObjectStatus();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseObjectStatusDialogController', ex)
        }
    },

    onSaved: function () {
        try {
            if (!this._saveRunning){
                this.onStatusSaved();
                this.dismiss();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaved of BaseObjectStatusDialogController', ex)
        }
    },

    //@override
    cancel: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseObjectStatusDialogController', ex)
        }
    },

    trySaveObjectStatus: function () {
        try {
            this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
            var me = this;
            var myModel = this.getViewModel();
            var object = myModel.get('object');
            var objectStatusList = myModel.get('objectStatusList');
            var displayedlistWithOrdNr = myModel.get('displayedlistWithOrdNr');
            var displayedlistWithoutOrdNr = myModel.get('displayedlistWithoutOrdNr');

            this._saveRunning = false;

            var newObjectStatusList = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjectStatus',
                autoLoad: false
            });

            displayedlistWithOrdNr.each(function (item) {
                var objectStatus = Ext.create('AssetManagement.customer.model.bo.ObjectStatus', {
                    objnr: object.get('objnr'),
                    stat: item.get('estat'),
                    inact: item.get('checked') ? '' : 'X',
                    //chgnr: dbResult['CHNGR'],
                    //changed: dbResult['CHANGED'],
                    txt04: item.get('txt04'),
                    txt30: item.get('txt30'),
                    //status_int: dbResult['STATUS_INT'],
                    //user_status_code: dbResult['USER_STATUS_CODE'],
                    //user_status_desc: dbResult['USER_STATUS_DESC'],

                    //updFlag: dbResult['UPDFLAG'],
                    mobileKey: object.get('mobileKey'),
                    childKey: object.get('childKey')
                });

                //AssetManagement.customer.manager.ObjectStatusManager.saveSingleObjectStatus(objectStatus);
                newObjectStatusList.add(objectStatus);
            });

            displayedlistWithoutOrdNr.each(function (item) {
                var objectStatus = Ext.create('AssetManagement.customer.model.bo.ObjectStatus', {
                    objnr: object.get('objnr'),
                    stat: item.get('estat'),
                    inact: item.get('checked') ? '' : 'X',
                    //chgnr: dbResult['CHNGR'],
                    //changed: dbResult['CHANGED'],
                    txt04: item.get('txt04'),
                    txt30: item.get('txt30'),
                    //status_int: dbResult['STATUS_INT'],
                    //user_status_code: dbResult['USER_STATUS_CODE'],
                    //user_status_desc: dbResult['USER_STATUS_DESC'],

                    //updFlag: dbResult['UPDFLAG'],
                    mobileKey: object.get('mobileKey'),
                    childKey: object.get('childKey')
                });

                newObjectStatusList.add(objectStatus);
            });

            //set id for already existing items
            newObjectStatusList.each(function (newObjectStatus) {
                objectStatusList.each(function (objectStatus) {
                    if (newObjectStatus.get('stat') === objectStatus.get('stat')) {
                        newObjectStatus.set('id', objectStatus.get('id'));
                        return false;
                    }
                });
            });

            var saveCallback = function (success) {
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                try {
                    if (success === true) {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('objectStatusListSaved'), true, 0);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        me.onSaved();
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingObjectList'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveObjectStatus of BaseObjectStatusDialogController', ex);
                } finally {
                    me._saveRunning = false;
                }
            };
            var eventId = AssetManagement.customer.manager.ObjectStatusManager.saveObjectStatusList(newObjectStatusList, false);
            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveObjectStatus of BaseObjectStatusDialogController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    onObjectStatusCheckBoxChange: function (rowIndex, checkbox, checked, record, eOpts) {
        try {
            var myModel = this.getViewModel();
            var object = myModel.get('object');

            var objectStatus = Ext.create('AssetManagement.customer.model.bo.ObjectStatus', {
                objnr: object.get('objnr'),
                stat: record.get('estat'),
                inact: record.get('checked') ? '' : 'X',
                //chgnr: dbResult['CHNGR'],
                //changed: dbResult['CHANGED'],
                txt04: record.get('txt04'),
                txt30: record.get('txt30'),
                //status_int: dbResult['STATUS_INT'],
                //user_status_code: dbResult['USER_STATUS_CODE'],
                //user_status_desc: dbResult['USER_STATUS_DESC'],

                //updFlag: dbResult['UPDFLAG'],
                mobileKey: object.get('mobileKey'),
                childKey: object.get('childKey')
            });

            this.startOnlineStatusChange(objectStatus, record);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onObjectStatusWithOrdNoCheckBoxChange of BaseObjectStatusDialogController', ex);
        }
    },

    onObjectStatusWithOrdNoCheckBoxChange: function (rowIndex, checkbox, checked, record, eOpts) {
        try {
            var me = this;
            if (checked) {
                var elements = Ext.getCmp('objectStatusWithOrdNoGridPanel').getStore();
                elements.each(function (element) {
                    if (element.get('checked') === true && element !== record) {
                        element.set('checked', false);
                    }
                });
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onObjectStatusWithOrdNoCheckBoxChange of BaseObjectStatusDialogController', ex);
        }
    },

    onStatusSaved: function (userStatus) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_STATUS_SAVED];

            if (callbackInterface) {
                callbackInterface.call(this.getOwner(), userStatus);
            }

            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStatusSaved of BaseObjectStatusDialogController', ex)
        }
    }

});
