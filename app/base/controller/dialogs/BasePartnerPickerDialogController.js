Ext.define('AssetManagement.base.controller.dialogs.BasePartnerPickerDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
       'AssetManagement.customer.manager.CustomerManager',
       'AssetManagement.customer.manager.ContactManager'
    ],

    config: {
        contextReadyLevel: 1
    },

    inheritableStatics: {
        //interface
        ON_PARTNERAP_SELECTED: 'onPartnerApSelected',
        ON_PARTNERZR_SELECTED: 'onPartnerZrSelected'
    },

    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var myModel = this.getViewModel();
            if (argumentsObject['order'] && Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(argumentsObject['order'].get('kunnr')))
                    myModel.set('kunnr', argumentsObject['order'].get('kunnr'));
            }
            if (argumentsObject['partner'] && Ext.getClassName(argumentsObject['partner']) === 'AssetManagement.customer.model.bo.Partner') {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(argumentsObject['partner'].get('parnr')))
                    myModel.set('kunnr', argumentsObject['partner'].get('parnr'));
            }
            if (argumentsObject['parVw'] && (typeof argumentsObject['parVw']) === 'string') {
                myModel.set('parVw', argumentsObject['parVw']);
            }
            if (!myModel.get('kunnr'))
            {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.dialogs.CustomerPickerDialogController'
			    });
            }
            this.performContextLevel1Requests();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of CustomerPickerDialogController', ex)
            this.errorOccurred();
        }
    },

    performContextLevel1Requests: function () {
        try {
            //load partner
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('kunnr'))) {
                var partners = AssetManagement.customer.manager.ContactManager.getAllContactsForCustomer(this.getViewModel().get('kunnr'), true);
                this.addRequestToDataBaseQueue(partners, "partners");

                this.waitForDataBaseQueue();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of CustomerPickerDialogController', ex)
        }
    },

    onPartnerCellSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            this.onPartnerSelected(record);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerCellSelected of CustomerPickerDialogController', ex)
        }
    },

    onPartnerSelected: function (partnerInfoObject) {
        try {
            var callbackInterface = null;
            if (this.getViewModel().get('parVw') == 'AP')
                callbackInterface = this.getOwner()[this.self.ON_PARTNERAP_SELECTED];
            if (this.getViewModel().get('parVw') == 'ZR')
                callbackInterface = this.getOwner()[this.self.ON_PARTNERZR_SELECTED];

            if (callbackInterface)
                callbackInterface.call(this.getOwner(), partnerInfoObject);

            //dismiss
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerSelected of CustomerPickerDialogController', ex)
        }
    },

    //@override
    getContextLoadingErrorMessage: function () {
        return "Data error while loading customers.";
    }
});