Ext.define('AssetManagement.base.controller.dialogs.BaseSynchronizationDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',
	
	requires: [
	  'AssetManagement.customer.sync.SyncManager',
	  'AssetManagement.customer.helper.OxLogger'
	],
	
	//protected
	//@override
	beforeInitialization: function(argumentsObject) {
	    try {
	        if (argumentsObject)
	        {        
			    //set the default parameters on the progress dialog for the sync process
			     var syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
			     syncManager.setUploadOnly(argumentsObject.uploadOnly === true ? true : false);
			
			     argumentsObject.title = Locale.getMsg('synchronization');
			     argumentsObject.worker = syncManager;
			     argumentsObject.startMethod = syncManager.beginSynchronize;
	        } else {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.dialogs.SynchronizationDialogController'
			    });
	        }
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseSynchronizationDialogController', ex);
		    this.errorOccurred();
		}
	}
});