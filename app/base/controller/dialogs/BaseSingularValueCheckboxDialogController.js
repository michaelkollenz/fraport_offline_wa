﻿Ext.define('AssetManagement.base.controller.dialogs.BaseSingularValueCheckboxDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
'AssetManagement.customer.utils.StringUtils',
'AssetManagement.customer.manager.EquipmentManager',
'AssetManagement.customer.manager.FuncLocManager',
'AssetManagement.customer.manager.ClassificationManager',
'AssetManagement.customer.controller.ToolbarController',
'Ext.data.Store',
'AssetManagement.customer.helper.OxLogger',
'AssetManagement.customer.model.bo.Charact',
'AssetManagement.customer.controller.EventController'
    ],

    config: {
        contextReadyLevel: 1
    },
    inheritableStatics: {
        //interface
        ON_CLASSVALUE_SAVED: 'onClassValueSaved'
    },


    //private
    _saveRunning: false,

    //protected
    //@Override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var myViewModel = this.getViewModel();

            if (Ext.getClassName(argumentsObject['charact']) === 'AssetManagement.customer.model.bo.Charact')
                myViewModel.set('charact', argumentsObject['charact']);

            if (Ext.getClassName(argumentsObject['objClass']) === 'AssetManagement.customer.model.bo.ObjClass')
                myViewModel.set('objClass', argumentsObject['objClass']);

            if(!myViewModel.get('charact') && !myViewModel.get('objClass'))
            {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.dialogs.SingularValueCheckboxDialogController'
			    });
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseSingularValueCheckboxDialogController', ex);
            this.errorOccurred();
        }
    },

    save: function () {
        try {
            if (!this._saveRunning)
                this.trySaveClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseSingularValueCheckboxDialogController', ex);
        }
    },

    dismiss: function () {
        this.callParent();

        try {
            //this.unregisterListenersFromWorker();
            //if (this.getInfinite())
            //    this.getView().stopProgressBar();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseSingularValueCheckboxDialogController', ex);
        }
    },

    //@override
    cancel: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseSingularValueCheckboxDialogController', ex)
        }
    },

    trySaveClassValue: function () {
        try {
            this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;
            var saveItem = function (checkWasSuccessful) {
                try {
                    if (checkWasSuccessful === true) {
                        me.prepareValuesForSave();
                        var callback = function (success) {
                            try {
                                if (success === true) {
                                    //to-do
                                    me.onClassValueSaved();

                                    me.dismiss();
                                    Ext.defer(function () {
                                        me._saveRunning = false;
                                    }, 2000, me);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseSingularValueCheckboxDialogController', ex);
                            }
                            finally {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        };
                        this.getView().transferInputValuesIntoModel();

                        var myModel = this.getViewModel();
                        var classValues = myModel.get('charact').get('classValues');
                        var newValues = myModel.get('newValues');

                        var eventId = AssetManagement.customer.manager.ClassificationManager.updateClassValues(classValues, newValues);

                        if (eventId > 0) {
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                        } else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseSingularValueCheckboxDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            this.checkInputValues(saveItem, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseSingularValueCheckboxDialogController', ex);
            me._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';


            var values = this.getView().getCurrentInputValues();


            var atflv = values.atflv;
            var atwrt = values.atwrt;

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }
                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseSingularValueCheckboxDialogController', ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },


    prepareValuesForSave: function () {
        var retval = null;
        try {
            var myModel = this.getViewModel();
            var objClass = myModel.get('objClass');
            var charact = myModel.get('charact');

            if (charact === null || charact === undefined)
                return;

            var classValues = charact.get('classValues');


            var newValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            var delValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            Ext.getCmp('itemContainerForSingularCheckbox').items.each(function (checkboxItem) {
                var classValue = null;

                if (checkboxItem.checked === true) {
                    var matchesFound = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.ObjClassValue',
                        autoLoad: false
                    });

                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValues)) {
                        var newClassValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, checkboxItem.atwrt, checkboxItem.atflv, checkboxItem._charactValue.get('atzhl'));
                        // matchesFound.add(newClassValue);
                    } else {
                        // filter classValues 
                        classValues.each(function (clValues) {
                            if (clValues.get('atwrt') === checkboxItem.atwrt && clValues.get('atflv') === checkboxItem.atflv)
                                matchesFound.add(clValues);

                        });
                    }
                    if (matchesFound.getCount() > 0) {

                        if (matchesFound.data.items[0].get('updFlag') === 'D')
                            matchesFound.data.items[0].set('updFlag', 'X');

                        classValue = matchesFound.data.items[0];
                    } else {
                        classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, checkboxItem.atwrt, checkboxItem.atflv, checkboxItem._charactValue.get('atzhl'));

                    }
                }

                if (classValue !== null) {
                    classValue.set('clint', objClass.get('clint'));
                    classValue.set('atzhl', checkboxItem.atzhl);
                    classValue.set('mobileKey', objClass.get('mobileKey'));
                    newValues.add(classValue);
                    myModel.set('newValues', newValues);
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareValuesForSave of BaseSingularValueCheckboxDialogController', ex);
        }
    },

    //checks, if there are any differences between the current input state and the model state
    isInputEqualToModel: function () {
        var retval = true;

        try {
            var inputValues = this.getView().getCurrentInputValues();
            var funcLoc = this.getViewModel().get('objClass');
            var charact = this.getViewModel().get('charact');

            if (funcLoc.get('objnr') !== inputValues.objnr) {
                retval = false;
            } else if (!AssetManagement.customer.utils.DateTimeUtils.areEqual(funcLoc.get('atflv'), inputValues.atflv)) {
                retval = false;
            } else if (funcLoc.get('atwrt') !== inputValues.atwrt) {
                retval = false;
            } else if (funcLoc.get('atinn') !== inputValues.atinn) {
                retval = false;
            } else if (funcLoc.get('atflv') !== inputValues.atflv) {
                retval = false;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isInputEqualToModel of BaseSingularValueCheckboxDialogController', ex);
        }

        return retval;
    },

    //refresh NotifDetailPage GridPanel, if there is a new notifItem in notifItemStore
    onClassValueSaved: function () {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_CLASSVALUE_SAVED];

            if (callbackInterface) {
                var values = null;
                var newValues = this.getViewModel().get('newValues');
                var classValue = this.getViewModel().get('charact').get('classValues')
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValue))
                    values = this.getViewModel().get('charact').set('classValues', newValues);
                else
                    values = classValue
                callbackInterface.call(this.getOwner(), values);
            }

            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSaved of BaseSingularValueCheckboxDialogController', ex)
        }
    },

    //apply the current filter criteria on the store, before it is transferred into the view
    beforeDataReady: function () {
        try {
            this.backUpClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseSingularValueCheckboxDialogController', ex);
        }
    },

    //add to newValues store the cloned classValues(the current view values)
    backUpClassValue: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var classValues = charact.get('classValues');

            if (classValues && classValues.getCount() > 0) {
                var newValues2 = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });
                classValues.each(function (classVal) {
                    var value = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classVal);
                    newValues2.add(value);
                }, this);
            }

            myModel.set('newValues', newValues2);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside backUpClassValue of BaseSingularValueCheckboxDialogController', ex);
        }
    }
});