Ext.define('AssetManagement.base.controller.dialogs.BaseAlertDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	  'AssetManagement.customer.helper.OxLogger'
	],

	//@override
	//protected
	transferDataFromConfigObjectToViewModel: function(argumentsObject) {
		try {
			if(argumentsObject) {
				if(argumentsObject.icon) {
					this.getViewModel().set('icon', argumentsObject.icon);
				}

				if(argumentsObject.message) {
					this.getViewModel().set('message', argumentsObject.message);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferDataFromConfigObjectToViewModel of BaseAlertDialogController', ex);
		}
	}
});