Ext.define('AssetManagement.base.controller.dialogs.BaseSDOrderItemDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

    
    requires: [
        'AssetManagement.customer.manager.SDOrderItemManager',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
        'AssetManagement.customer.helper.OxLogger'
    ],
               
    config: {
		contextReadyLevel: 1
	},

	inheritableStatics: {
		//interface
		ON_SDORDER_ITEM_SAVED: 'onSDOrderItemSaved'
	},
	
	//private
	_saveRunning: false,
	
	//@override
	startBuildingDialogContext: function(argumentsObject) {
		try {
			var myModel = this.getViewModel();
		
			//extract parameters, if any given
			var vbeln = '';
            
            if(typeof argumentsObject['vbeln'] === 'string') {
            	vbeln = argumentsObject['vbeln'];
            } else if(Ext.getClassName(argumentsObject['sdOrder']) === 'AssetManagement.customer.model.bo.SDOrder') {
            	myModel.set('sdOrder', argumentsObject['sdOrder']);
            } else {
               throw Ext.create('AssetManagement.customer.utils.OxException', {
                      message: 'Insufficient parameters',
                      method: 'startBuildingDialogContext',
                      clazz: 'AssetManagement.customer.controller.pages.SDOrderItemDialogController'
               });
			}
            
			if(Ext.getClassName(argumentsObject['sdOrderItem']) === 'AssetManagement.customer.model.bo.SDOrderItem') {
				var sdOrderItem = argumentsObject['sdOrderItem'];
				myModel.set('sdOrderItem', sdOrderItem); 
				myModel.set('isEditMode', true); 
			} else {
				myModel.set('isEditMode', false); 
			}
			
            
			var vbelnPassed = vbeln !== '';
			
			if(vbelnPassed) {
            	this.getSDOrder(vbeln);
            }
            
           	this.waitForDataBaseQueue();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseSDOrderItemDialogController', ex)
		    this.errorOccurred();
		}
	},
	
	getSDOrder: function(vbeln) {
		try {
            var sdOrderRequest = AssetManagement.customer.manager.SDOrderManager.getSDOrder(vbeln, true);
            this.addRequestToDataBaseQueue(sdOrderRequest, 'sdOrder');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrder of BaseSDOrderItemDialogController', ex);
        }
	},
	
	//@override
	dataBaseQueueComplete: function() {
		try {
			if(this._contextLevel + 1 === 1) {
				if(this.getViewModel().get('isEditMode') === false)
					this.initializeNewSdOrderItem();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseSDOrderItemDialogController', ex)
		}
		
		//base call will increase the level and trigger a refreshView
		this.callParent();
	},
	
	initializeNewSdOrderItem: function() {
		try {
			var myModel = this.getViewModel();
			var newSdOrderItem = AssetManagement.customer.manager.SDOrderItemManager.getNewSDOrderItem(myModel.get('sdOrder'));

			myModel.set('sdOrderItem', newSdOrderItem);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewSdOrderItem of BaseSDOrderItemDialogController', ex)
		}
	},
	
	//@override
	save: function() {
		try {
			if(!this._saveRunning)
				this.trySaveSDOrderItem();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseSDOrderItemDialogController', ex)
		}
	},
	
	//@override
	cancel: function() {
		try {
			if(!this._saveRunning)
				this.dismiss();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseSDOrderItemDialogController', ex)
		}
	},

	trySaveSDOrderItem: function() {
		try {
			this._saveRunning = true;
		
			//first check if data is complete
			if(this.checkInputValues() === true) {
				AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
			
				//transfer view state into model
				this.getView().transferViewStateIntoModel();
			
				var sdOrderItem = this.getViewModel().get('sdOrderItem');
			
				var me = this; 
				var saveCallback = function(success) {
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				
					try {
						if(success === true) {
							me.onSDOrderItemSaved(sdOrderItem);
							
							var isEditMode = me.getViewModel().get('isEditMode');
							
							var message = isEditMode === true ? Locale.getMsg('sdOrderItemChanged') : Locale.getMsg('sdOrderItemAdded');
							
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(message, true, 0);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						} else {
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingSDOrderItem'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItem of BaseSDOrderItemDialogController', ex);
					} finally {
						me._saveRunning = false;
					}
				};
				
				var eventId = AssetManagement.customer.manager.SDOrderItemManager.saveSDOrderItem(sdOrderItem);
				
				if(eventId > 0) {
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
				} else {
					this._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingSDOrderItem'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			} else {
				this._saveRunning = false;
			}
		} catch(ex) {
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveSDOrderItem of BaseSDOrderItemDialogController', ex);
		}
	},
	
	//performs a set of checks for the current sd order item input 
	checkInputValues: function() {
		var retval = true;	
	
		try {
			var errorMessage = '';
					
			var values = this.getView().getCurrentInputValues(); 
		
			var matnr = values.matnr;
			var amount = values.amount;
			var unit = values.unit;
			var remark = values.remark;
			var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
			
			if(shortHand(matnr)) {
				//provide material
				errorMessage = Locale.getMsg('provideMaterial');
				retval = false;
			} else if(shortHand(unit)) {
				//provide unit
				errorMessage = Locale.getMsg('provideUnit');
				retval = false;
			} else if(amount === null || amount === undefined || amount <= 0) {
				errorMessage = Locale.getMsg('provideValidQuantity');
				retval = false;
			}
			
			if(retval === false) {
				var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(message);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValuesForOrderItem of NewSDOrderPageController', ex)
		}
		
		return retval;
	},
	
	onSDOrderItemSaved: function(sdOrderItem) {
		try {
			var callbackInterface = this.getOwner()[this.self.ON_SDORDER_ITEM_SAVED];
			
			if(callbackInterface)
				callbackInterface.call(this.getOwner(), sdOrderItem);
			
			this.dismiss(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemSaved of BaseSDOrderItemDialogController', ex)
		}
	}
});