﻿Ext.define('AssetManagement.base.controller.dialogs.BaseSingularValueLineDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.manager.ClassificationManager',
        'AssetManagement.customer.controller.ToolbarController',
        'Ext.data.Store',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Charact',
        'AssetManagement.customer.controller.EventController'

    ],

    config: {
        contextReadyLevel: 1
    },
    inheritableStatics: {
        //interface
        ON_CLASSVALUE_SAVED: 'onClassValueSaved'
    },


    //private
    _saveRunning: false,

    //protected
    //@Override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var myViewModel = this.getViewModel();
            
            if (Ext.getClassName(argumentsObject['charact']) === 'AssetManagement.customer.model.bo.Charact')
                myViewModel.set('charact', argumentsObject['charact']);

            if (Ext.getClassName(argumentsObject['objClass']) === 'AssetManagement.customer.model.bo.ObjClass')
                myViewModel.set('objClass', argumentsObject['objClass']);

            if (!myViewModel.get('charact') && !myViewModel.get('objClass'))
            {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.dialogs.NotifCauseDialogController'
			    });
            }
            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseSingularValueLineDialogController', ex);
              this.errorOccurred();
        }
    },

    save: function () {
        try {
            if (!this._saveRunning)
                this.trySaveClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseSingularValueLineDialogController', ex);
        }
    },

    dismiss: function () {
        this.callParent();

        try {
            //this.unregisterListenersFromWorker();
            //if (this.getInfinite())
            //    this.getView().stopProgressBar();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseSingularValueLineDialogController', ex);
        }
    },

    //@override
    cancel: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseSingularValueLineDialogController', ex)
        }
    },

    trySaveClassValue: function () {
        try {
            this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
            var me = this;
            var saveItem = function (checkWasSuccessful) {
                try {
                    if (checkWasSuccessful === true) {
                        var callback = function (success) {
                            try {
                                if (success === true) {
                                    me.onClassValueSaved();

                                    me.dismiss();
                                    Ext.defer(function () {
                                        me._saveRunning = false;
                                    }, 2000, me);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseSingularValueLineDialogController', ex);
                            }
                            finally {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        };
                        this.getView().transferInputValuesIntoModel();
                        me.prepareValuesForSave();

                        var classValues = this.getViewModel().get('charact').get('classValues').getAt(0);
                        var eventId = AssetManagement.customer.manager.ClassificationManager.saveClassValue(classValues);
                        if (eventId > 0) {
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                        } else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseSingularValueLineDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            this.checkInputValues(saveItem, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseSingularValueLineDialogController', ex);
            me._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }
                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseSingularValueLineDialogController', ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    //prepare Values for to Save 
    prepareValuesForSave: function () {
        var retval = null;
        try {
            var myModel = this.getViewModel();
            var objClass = myModel.get('objClass');
            var charact = myModel.get('charact');

            if (charact === null || charact === undefined)
              return;

            var inputField = Ext.getCmp('itemContainerSingular').down();

            var classValue = inputField._charact.get('classValues').getAt(0);


            var inputFieldValue = inputField.getValue();
            var atfor =charact.get('atfor');

            classValue.set('mobileKey', objClass.get('mobileKey'));
            classValue.set('clint', objClass.get('clint'));


            if(inputFieldValue) {
                if (classValue.get('updFlag') === "I" || classValue.get('updFlag') === "X")
                    classValue.set('updFlag', 'U');
            } else {
                if (classValue.get('updFlag') === "I") {
                    classValue.set('updFlag', 'U');
                } else {
                    classValue.set('updFlag', 'D');

                }
            }

            switch (atfor) {
                case "DATE":
                    classValue.set('atflv', AssetManagement.customer.utils.DateTimeUtils.getDateString(inputFieldValue));
                    break;
                case "TIME":
                    classValue.set('atflv', AssetManagement.customer.utils.DateTimeUtils.getTimeString(inputFieldValue, false));
                    break;
                case "CHAR":
                case "NUM":
                case "CURR":
                    classValue.set('atflv', inputFieldValue);
                    break;
                default:
                    break;
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareValuesForSave of BaseSingularValueLineDialogController', ex);
        }
        return retval;
    },

    //refresh NotifDetailPage GridPanel, if there is a new notifItem in notifItemStore
    onClassValueSaved: function () {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_CLASSVALUE_SAVED];

            if (callbackInterface)
                var classValue = this.getViewModel().get('charact').get('classValue');
            callbackInterface.call(this.getOwner());

            this.dismiss();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSaved of BaseSingularValueLineDialogController', ex)
        }
    },

    //apply the current filter criteria on the store, before it is transferred into the view
    beforeDataReady: function () {
        try {
            this.backUpClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseSingularValueLineDialogController', ex);
        }
    },

    //add to newValues store the cloned classValues(the current view values)
    backUpClassValue: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var classValues = charact.get('classValues');
            if (classValues && classValues.getCount() > 0) {
                var newValues2 = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });
                classValues.each(function (classVal) {
                    var value = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classVal);
                    newValues2.add(value);
                }, this);
            }

            myModel.set('newValues', newValues2);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside backUpClassValue of BaseSingularValueLineDialogController', ex);
        }
    }

});