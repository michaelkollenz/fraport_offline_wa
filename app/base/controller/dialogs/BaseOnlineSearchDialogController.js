﻿Ext.define('AssetManagement.base.controller.dialogs.BaseOnlineSearchDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',

    requires: [
      'Ext.util.HashMap',
      'AssetManagement.customer.utils.StringUtils',
      'AssetManagement.customer.helper.NetworkHelper',
      'AssetManagement.customer.sync.ConnectionManager',
      'AssetManagement.customer.sync.ParsingManager'
    ],

    config: {
        cancelable: true,
        infinite: true,
        //set dummy-wise to level one to allow easy extension for dialogs with data loading
        contextReadyLevel: 1,
        //by default found matches will be forwarded - set to false to present results, retry search and select a specific result (TODO)
        forwardResults: true
    },

    //enum for state control of dialog
    DIALOG_MODES: {
        SELECTION: 0,
        REQUEST: 1,
        PICK: 2 //TODO - use grid customizing with injection or based on search criteria (but how to do multiline)
    },

    inheritableStatics: {
        //0: field key
        //1: value
        //2: search operator
        REQUEST_STREAM_TEMPLATE: 'NAME_VALUE_SEARCH;I;{0};{1};{2}',
        MAX_HIT_COUNT_DEF: 500
    },

    //abstract - must be implemented
    //protected
    //returns the request data typ to use
    //this controls, which kind of data is searched for
    getRequestDataType: function () {
    },

    //abstract function - must be implemented
    //protected
    //returns the model class representing the corresponding SAP object on the client
    getObjectModelClassName: function () {
    },

    //abstract function - must be implemented
    //protected
    //returns the manager administrating the corresponding SAP object on the client
    //must be the manager itself not the name
    getObjectManagerClass: function () {
    },

    //protected
    //returns the default name for the conversion API called during the response conversion on class returned by getObjectManagerClass
    getConversionAPIName: function () {
        return 'buildFromDbResultObject';
    },

    //abstract - must be implemented
    //protected
    //returns the search criteria definitions as array of objects structures like:
    //LABEL - localization string to be used for the label in the view
    //ATTR - the attribute to be searched on
    //MAX_LENGTH - maximum allowed input length for the criterion
    //DEF_OPER - default operation to be applied for the criterion
    getSearchCriterionDefinitions: function () {
    },

    //protected
    //returns the default title to be shown for the dialog
    getDialogTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('onlineSearch');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //protected
    //@override
    //prevent starting the immediate request on show
    onStartProcess: function () {
        var retval = false;

        try {
            retval = this.getViewModel().get('mode') === this.DIALOG_MODES.REQUEST;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.reportError();
        }

        return retval;
    },

    //protected
    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            if(!argumentsObject.title)
                argumentsObject.title = this.getDialogTitle();

            this.transferDataFromConfigObjectToViewModel(argumentsObject);

            //override the base classes standard initial message for empty messages
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(argumentsObject.initialMessage))
                this.getViewModel().set('message', '');

            this.setupSearchCriteria(argumentsObject.useLastCriteria === true);

            this.defaultMaximumHitsValue();

            //drop the dummy database wait to reach context level 1
            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    setupSearchCriteria: function(useLastCriteria) {
        try {
            var myModel = this.getViewModel();
            var searchCriteria = null;
            var formerCriteria = myModel.get('lastSearchCriteria');

            if (useLastCriteria && formerCriteria) {
                searchCriteria = formerCriteria;
            } else {
                searchCriteria = this.generateNewSearchCriteriaFromDefinitions();
            }
            
            myModel.set('searchCriteria', searchCriteria);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    defaultMaximumHitsValue: function () {
        try {
            var myModel = this.getViewModel();
            myModel.set('maxHitCount', this.self.MAX_HIT_COUNT_DEF);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    generateNewSearchCriteriaFromDefinitions: function () {
        var retval = null;

        try {
            var searchCriteria = Ext.create('Ext.util.HashMap');

            var searchCriteriaDefinitions = this.getSearchCriterionDefinitions();

            if (searchCriteriaDefinitions) {
                Ext.Array.each(searchCriteriaDefinitions, function (definitionObject) {
                    var criteriaObject = {};

                    criteriaObject['LABEL'] = definitionObject['LABEL'];
                    criteriaObject['ATTR'] = definitionObject['ATTR'];
                    criteriaObject['MAX_LENGTH'] = definitionObject['MAX_LENGTH'];
                    criteriaObject['OPER'] = definitionObject['DEF_OPER'];
                    criteriaObject['VALUE'] = '';

                    searchCriteria.add(definitionObject['ATTR'], criteriaObject);
                }, this);

                retval = searchCriteria;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //public
    cancelButtonClicked: function () {
        try {
            var mode = this.getViewModel().get('mode');

            if (mode === this.DIALOG_MODES.SENDING) {
                this.callParent();
            } else {
                this.dismiss();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    onSearchButtonClicked: function () {
        try {
            if (this.checkCurrentInputValues()) {
                this.updateModelWithInput();

                var myModel = this.getViewModel();

                myModel.set('mode', this.DIALOG_MODES.REQUEST);
                myModel.set('lastSearchCriteria', myModel.get('searchCriteria'));
                myModel.set('message', Locale.getMsg('performingOnlineSearch'));

                this.refreshView();
                this.startProcess();
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('provideAnySearchCriterion'), true, 1);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //extracts the input values from the view and updates the model with it
    updateModelWithInput: function () {
        var retval = true;

        try {
            var myModel = this.getViewModel();
            var inputValues = this.getView().getCurrentInputValues();

            //update search criterias
            var searchCriteria = myModel.get('searchCriteria');

            if (searchCriteria && searchCriteria.getCount() > 0) {
                var searchCriteriaFromInput = inputValues.searchCriteria;

                searchCriteria.each(function (attribute, searchCriterion) {
                    var searchCriterionFromInput = searchCriteriaFromInput.get(attribute);

                    if (searchCriterionFromInput) {
                        searchCriterion['VALUE'] = searchCriterionFromInput['VALUE'];
                        searchCriterion['OPER'] = searchCriterionFromInput['OPER'];
                    } else {
                        searchCriterion['VALUE'] = '';
                        searchCriterion['OPER'] = searchCriterion['DEF_OPER'];
                    }
                }, this);
            }

            //update maximum hit count
            myModel.set('maxHitCount', inputValues.maxHitCount ? inputValues.maxHitCount : 0)
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    checkCurrentInputValues: function () {
        var retval = true;

        try {
            var inputValues = this.getView().getCurrentInputValues();
            var searchCriteria = inputValues.searchCriteria;

            var anyReasonaleCriterion = false;

            if (searchCriteria && searchCriteria.getCount() > 0) {
                searchCriteria.each(function (attribute, criterionObjectFromInput) {
                    anyReasonaleCriterion = this.isCriterionReasonable(criterionObjectFromInput);

                    //break the loop, when at least one has been found
                    return !anyReasonaleCriterion;
                }, this);
            }

            retval = anyReasonaleCriterion;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    isCriterionReasonable: function(searchCriterion) {
        var retval = false;

        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(searchCriterion['VALUE'])) {
                retval = true;
            } else if (searchCriterion['OPER'] === 'NE') {
                retval = true;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //protected
    //@override
    doWork: function () {
        try {
            this.prepareSearchRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.reportError();
        }
    },

    //private
    //previous step: onStartProcess
    //next step: callPostingOnlineAPI
    prepareSearchRequest: function () {
        try {
            var me = this;
            var calleName = arguments.callee.name;

            //check if client is online
            var onlineCallback = function (isOnline) {
                try {
                    if (!isOnline) {
                        var errorMessage = Locale.getMsg('browserIsOffline');
                        me.reportError(errorMessage);
                    } else {
                        //get the upstream for the selected assignments
                        var upstream = me.getUploadStreamForSearchRequest();

                        //if there is no upstream, stop with an error
                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(upstream)) {
                            me.reportError();
                            return;
                        }

                        var maxHitCount = me.getMaxRequestedHitCount();

                        //do the actual online call next
                        me.postSearchRequest(upstream, maxHitCount);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException(calleName, me.getClassName(), ex);
                    me.reportError();
                }
            };
            
            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.reportError();
        }
    },

    //private
    //previous step: postSearchRequest
    //next step: extractResultsFromResponse
    postSearchRequest: function (upstream, maxHitCount) {
        try {
            var postValues = this.getRequestPostValues(upstream, maxHitCount);

            if (!postValues) {
                this.reportError();
                return;
            }

            var me = this;
            var calleName = arguments.callee.name;

            var callback = function (responseJSONStream) {
                try {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(responseJSONStream)) {
                        me.extractResultsFromResponse(responseJSONStream)
                    } else {
                        me.reportError();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException(calleName, me.getClassName(), ex);
                    me.reportError();
                }
            };

            var errorCallback = function (failureTypeOrErrorMessage) {
                try {
                    var message = '';

                    switch (failureTypeOrErrorMessage) {
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                            message = Locale.getMsg('browserIsOffline');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                            message = Locale.getMsg('connectionTimeoutOccured');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                            message = Locale.getMsg('mymUnknownError');
                            break;
                    }

                    me.reportError(message);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException(calleName, me.getClassName(), ex);
                    me.reportError();
                }
            };

            var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                listeners: {
                    sendFailed: { fn: errorCallback, scope: this },
                    errorOccured: { fn: errorCallback, scope: this }
                }
            });

            connManager.sendToBackEnd(AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.ONLINEREQ, postValues, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.reportError();
        }
    },

    //private
    //previous step: postSearchRequest
    //next step: proceedWithResults
    extractResultsFromResponse: function (responseJSONStream) {
        try {
            var parsedObject = Ext.decode(responseJSONStream);

            var resultObjects = Ext.create('Ext.data.Store', {
                model: this.getObjectModelClassName(),
                autoLoad: false
            });

            var results = parsedObject['ONLINE_ANY_RESULT'];

            if (results && results.length > 0) {
                for (var i = 0; i < results.length; i++) {
                    var resultObject = this.convertSearchResultToRuntimeInstance(results[i]);

                    if (resultObject)
                        resultObjects.add(resultObject);
                }
            }

            this.includeResultsIntoCaching(resultObjects);

            this.proceedWithResults(resultObjects);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.reportError();
        }
    },

    //private
    //previous step: extractResultsFromResponse
    //next step: returnToSelectionMode, onWorkFinished (or show results for picking) TODO
    proceedWithResults: function (results) {
        try {
            if (!results || results.getCount() === 0) {
                //no results at all
                //notify the user and go back into search mode
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noSearchHitsFound'), true, 1);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);

                this.returnToSelectionMode();
            } else {
                if (this.getForwardResults()) {
                    this.onWorkFinished(results);
                } else {
                    //TODO begin picking sequence
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.reportError()
        }
    },

    //private
    returnToSelectionMode: function () {
        try {
            var myModel = this.getViewModel();

            myModel.set('mode', this.DIALOG_MODES.SELECTION);
            myModel.set('message', '');

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //generates the upload stream for the search request out of the specified search criteria
    getUploadStreamForSearchRequest: function () {
        var retval = '';

        try {
            var myModel = this.getViewModel();
            var searchCriteria = myModel.get('searchCriteria');

            var uploadStream = '';

            if (searchCriteria && searchCriteria.getCount() > 0) {
                searchCriteria.each(function (attribute, searchCriteriaObject) {
                    if (this.isCriterionReasonable(searchCriteriaObject)) {
                        uploadStream += this.getUploadStreamForSearchCriterion(searchCriteriaObject);
                    }
                }, this);
            }

            retval = uploadStream;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    //generates the upload stream for the search request out of the specified search criteria
    getUploadStreamForSearchCriterion: function (searchCriterionObject) {
        var retval = '';

        try {
            if (searchCriterionObject) {
                var stream = '';

                var searchAttributeName = searchCriterionObject['ATTR'];
                var searchValue = searchCriterionObject['VALUE'];
                var searchOperator = searchCriterionObject['OPER'];

                //in case of a contains search (operator = LIKE) the value must be converted to SAP format
                if (searchOperator === 'LIKE' && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(searchValue)) {
                    searchValue = searchValue.replace(/\*/g, '%');

                    if (!AssetManagement.customer.utils.StringUtils.startsWith(searchValue, '%'))
                        searchValue = '%' + searchValue;

                    if (!AssetManagement.customer.utils.StringUtils.endsWith(searchValue, '%'))
                        searchValue += '%';
                }

                stream += AssetManagement.customer.utils.StringUtils.format(this.self.REQUEST_STREAM_TEMPLATE, [searchAttributeName, searchValue, searchOperator]);
                stream += AssetManagement.customer.sync.ParsingManager.PIPE;

                retval = stream;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //protected
    //returns the maximum requested hit count
    getMaxRequestedHitCount: function() {
        var retval = this.self.MAX_HIT_COUNT_DEF;

        try {
            var myModel = this.getViewModel();
            var maxHitCount = myModel.get('maxHitCount');

            if (maxHitCount)
                retval = maxHitCount;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    //builds the post values for the online request
    getRequestPostValues: function (upstream, maxHitCount) {
        var retval = null;

        try {
            var nameValuePairList = Ext.create('Ext.util.MixedCollection');

            var ac = AssetManagement.customer.core.Core.getAppConfig();
            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());

            //set datatype for online api
            nameValuePairList.add("DATATYPE", this.getRequestDataType());

            //set upstream
            nameValuePairList.add("UPSTREAM", upstream);

            //set maximum hit count
            nameValuePairList.add("MAX_RESULTS", maxHitCount);

            retval = nameValuePairList;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //protected
    //@override
    getGeneralProcessErrorMessage: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('generalErrorOccurredDuringOnlineSearch');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    //converts a received search result object into a runtime model instance
    convertSearchResultToRuntimeInstance: function(searchResult) {
        var retval = null;

        try {
            if (searchResult) {
                var manager = this.getObjectManagerClass();
                var conversionAPI = manager ? manager[this.getConversionAPIName()] : null;

                if (conversionAPI)
                    retval = conversionAPI(searchResult);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //private
    includeResultsIntoCaching: function (searchResults) {
        try {
            if (searchResults && searchResults.getCount() > 0) {
                var managerClass = this.getObjectManagerClass();

                if (managerClass && managerClass.includeToCache) {
                    searchResults.each(function (resultRuntimeInstance) {
                        managerClass.includeToCache(resultRuntimeInstance);
                    }, this);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    }
});