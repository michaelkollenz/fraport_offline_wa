Ext.define('AssetManagement.base.controller.dialogs.BaseSingleTextLineInputDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	  'AssetManagement.customer.utils.StringUtils'
    ],

    _callback: null,
    _callbackScope: null,

    //protected
    //@override
    transferDataFromConfigObjectToViewModel: function (argumentsObject) {
        try {
            if (argumentsObject) {
                this._callback = argumentsObject.callback;
                this._callbackScope = argumentsObject.callbackScope;

                var myModel = this.getViewModel();

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(argumentsObject.title)) {
                    myModel.set('title', argumentsObject.title);
                }

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(argumentsObject.message)) {
                    myModel.set('message', argumentsObject.message);
                }

                if (argumentsObject.maxLength) {
                    myModel.set('maxLength', argumentsObject.maxLength);
                }

                if (argumentsObject.emptyAllowed === false)
                    myModel.set('emptyAllowed', false);

                if (argumentsObject.onlyUpperCase === true)
                    myModel.set('onlyUpperCase', true);

                if (argumentsObject.readOnly === true)
                    myModel.set('readOnly', true);

                    myModel.set('value', argumentsObject.value);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferDataFromConfigObjectToViewModel of BaseSingleTextLineInputDialogController', ex);
        }
    },

    //@override
    confirm: function () {
        try {
            var currentValue = this.getTextFieldValue();

            //do not return, if empty value is not allowed, but provided
            if (!this.getViewModel().get('emptyAllowed') && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(currentValue)) {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('provideAnyInputFirst'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                return;
            }

            this.dismiss();

            if (this._callback) {
                this._callback.call(this._callbackScope ? this._callbackScope : this, currentValue);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirm of BaseSingleTextLineInputDialogController', ex);
        }
    },

    getTextFieldValue: function () {
        var retval = '';

        try {
            var myView = this.getView();
            retval = myView.getCurrentTextLineValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTextFieldValue of BaseSingleTextLineInputDialogController', ex);
        }

        return retval;
    },

    onSpecialkey: function (field, e) {
        try {
            if (e.getKey() == e.ENTER) {
                this.confirm();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSpecialkey of BaseSingleTextLineInputDialogController', ex);
        }
    },

    onValueChange: function (textfield, newValue, oldValue, eOpts) {
        try {
            if (this.getViewModel().get('onlyUpperCase')) {
                var upperCaseValue = newValue.toUpperCase();
                textfield.setValue(upperCaseValue);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onValueChange of BaseSingleTextLineInputDialogController', ex);
        }
    }
});