Ext.define('AssetManagement.base.controller.dialogs.BaseLongtextDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	    'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.LongtextManager',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
		'AssetManagement.customer.helper.OxLogger'
    ],

    config: {
        contextReadyLevel: 1
    },

    inheritableStatics: {
        //interface
        ON_LONGTEXT_SAVED: 'onLongtextSaved'
    },

    //private
    _saveRunning: false,

    //protected
    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var myModel = this.getViewModel();
            
            if (argumentsObject['order'] && Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
                myModel.set('bo', argumentsObject['order']);
            } else if (argumentsObject['notif'] && Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
                myModel.set('bo', argumentsObject['notif']);
            } else if (argumentsObject['equi'] && Ext.getClassName(argumentsObject['equi']) === 'AssetManagement.customer.model.bo.Equipment') {
                myModel.set('bo', argumentsObject['equi']);
            } else if (argumentsObject['funcLoc'] && Ext.getClassName(argumentsObject['funcLoc']) === 'AssetManagement.customer.model.bo.FuncLoc') {
                myModel.set('bo', argumentsObject['funcLoc']);
            } else if (argumentsObject['bo'] && (Ext.getClassName(argumentsObject['bo']) === 'AssetManagement.customer.model.bo.NotifTask' || Ext.getClassName(argumentsObject['bo']) === 'AssetManagement.customer.model.bo.NotifCause' || Ext.getClassName(argumentsObject['bo']) === 'AssetManagement.customer.model.bo.NotifItem' || Ext.getClassName(argumentsObject['bo']) === 'AssetManagement.customer.model.bo.NotifActivity' || Ext.getClassName(argumentsObject['bo']) === "AssetManagement.customer.model.bo.TimeConf" || Ext.getClassName(argumentsObject['bo']) === 'AssetManagement.customer.model.bo.Operation')) {
                myModel.set('bo', argumentsObject['bo']);
            }

            if (!myModel.get('bo'))
            {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.LongtextDialogController'
                });
            }

            myModel.set('readOnly', argumentsObject['readOnly'] === true);

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseLongtextDialogController', ex);
            this.errorOccurred();
        }
    },

    //protected
    //@override
    beforeDataReady: function () {
        try {
            var myModel = this.getViewModel();
            var bo = myModel.get('bo');

            if (bo) {
                var backendLongtext = bo.get('backendLongtext');
                var localLongtext = bo.get('localLongtext');

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(backendLongtext))
                    myModel.set('backendLongtext', backendLongtext);

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(localLongtext))
                    myModel.set('localLongtext', localLongtext);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseLongtextDialogController', ex)
        }
    },

    //public
    save: function () {
        try {
            if (!this._saveRunning)
                this.trySaveLongtext();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseLongtextDialogController', ex)
        }
    },

    //public
    //@override
    cancel: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseLongtextDialogController', ex)
        }
    },
    
    //private
    //tries to save the currently put longtext object from View
    trySaveLongtext: function () {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var myModel = this.getViewModel();
            var myView = this.getView();
            
            var bo = myModel.get('bo');
            var newLocalLongtext = myView.getCurrentLocalLongtextInput();

            var me = this;

            var eventId = AssetManagement.customer.manager.LongtextManager.saveLocalLongtext(newLocalLongtext, bo, true);

            if (eventId > -1) {
                var saveCallback = function (success) {
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

                    try {
                        if (success === true) {
                            me.onLongtextSaved(newLocalLongtext);

                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('longtextSaved'), true, 0);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        } else {
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingLongtext'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveLongtext of BaseLongtextDialogController', ex);
                    } finally {
                        me._saveRunning = false;
                    }
                };

                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingLongtext'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveLongtext of BaseLongtextDialogController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //private
    onLongtextSaved: function (newLocalLongtext) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_LONGTEXT_SAVED];

            if (callbackInterface) {
                var bo = this.getViewModel().get('bo');

                callbackInterface.call(this.getOwner(), bo, newLocalLongtext);
            }

            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLongtextSaved of BaseLongtextDialogController', ex)
        }
    }
});
