Ext.define('AssetManagement.base.controller.dialogs.BaseOperationDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

    
    requires: [
        'AssetManagement.customer.manager.OperationManager',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.OxLogger'
    ],
               
    config: {
		contextReadyLevel: 2
	},

	inheritableStatics: {
		//interface
		ON_OPERATION_SAVED: 'onOperationSaved'
	},
	
	//private
	_saveRunning: false,

	//protected
	startBuildingDialogContext: function(argumentsObject) {
		try {
			//extract parameters, if any given
			//if strings are passed, load business objects them with passedStringsParametersToBOs
		    //else proceed first database query
		    var order = null;
		    var operation = null;

			if(Ext.getClassName(argumentsObject['operation']) === 'AssetManagement.customer.model.bo.Operation') {
				operation = argumentsObject['operation'];
				this.getViewModel().set('operation', operation); 
			}
				
			if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
				order = argumentsObject['order'];
				this.getViewModel().set('order', order); 
			} 
			
			if (!order)
			{
                throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.dialogs.OperationDialogController'
				});
			}
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseOperationDialogController', ex)
		}
	},
	
	//@override
	dataBaseQueueComplete: function() {
		try {
			this.callParent();
			
			if(this._contextLevel == 1) {
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseOperationDialogController', ex)
		}
	},
	
	performContextLevel1Requests: function() {
		try {
			this.getOperDependendData();
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseOperationDialogController', ex)
		}
	},
	
	getOperDependendData: function(){
		try {
			var workCenters = AssetManagement.customer.manager.WorkCenterManager.getWorkCenters(true);
			this.addRequestToDataBaseQueue(workCenters, 'workCenters');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOperDependendData of BaseOperationDialogController', ex)
		}
	},
	
	//@override
	save: function() {
		try {
			if(!this._saveRunning)
				this.trySaveOperation();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseOperationDialogController', ex)
		}
	},
	
	//@override
	cancel: function() {
		try {
			if(!this._saveRunning)
				this.dismiss();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseOperationDialogController', ex)
		}
	},

	trySaveOperation: function() {
		try {
			var me = this;
			
			this.getView().getOperationValues(); 
			var wrappedValues = this.getViewModel().get('operation'); 
	
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
						
						var saveCallback = function(success) {
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						
							try {
								if(success === true) {
									me.onOperationSaved(wrappedValues);
									me.getViewModel().set('operation', null); 
									
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('operationSaved'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingOperation'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveOperation of BaseOperationDialogController', ex);
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							} finally {
								me._saveRunning = false;
							}
						};
						
						var eventId = AssetManagement.customer.manager.OperationManager.saveOperation(wrappedValues);
						AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveOperation of BaseOperationDialogController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveOperation of BaseOperationDialogController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	/**
	 * checks if the data of the operation object is complete 
	 * returns null if the data is complete or an error message if data is missing
	 */
	checkInputValues: function(callback, callbackScope) {
		try {
			var retval = true; 
			var errorMessage = ''; 
		
			var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
		
			var operation = this.getViewModel().get('operation'); 
			
			var arbpl = operation.get('arbpl');
			var werks = operation.get('werks');
			var worktime = operation.get('arbei');
			var startDate = operation.get('fsav');
			var endDate = operation.get('fsed');
	
			var value = parseFloat(worktime);

			if(value <= 0.0 || isNaN(value)) {
				errorMessage = Locale.getMsg('noWorkTimeOper');
				retval = false;
			} else if(startDate > endDate) {
				errorMessage = Locale.getMsg('startAfterEndTime');
				retval = false;
			} else if(isNullOrEmpty(arbpl) || isNullOrEmpty(werks)) {
				errorMessage = Locale.getMsg('provideWorkcenter');
				retval = false;
			}
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			returnFunction.call(this);
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseOperationDialogController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	//refresh NotifDetailPage GridPanel, if there is a new notifItem in notifItemStore
	onOperationSaved: function(operationObject) {
		try {
			var callbackInterface = this.getOwner()[this.self.ON_OPERATION_SAVED];
			
			if(callbackInterface)
				callbackInterface.call(this.getOwner(), operationObject);
			
			//dismiss

			this.dismiss(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSaved of BaseOperationDialogController', ex)
		}
	}
});