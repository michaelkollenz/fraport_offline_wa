Ext.define('AssetManagement.base.controller.dialogs.BaseOnlineDispositionDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',
    alias: 'controller.BaseOnlineDispositionDialogController',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.NetworkHelper',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.sync.SyncManager',
        'AssetManagement.customer.sync.ParsingManager',
        'AssetManagement.customer.model.bo.PoItem'
    ],

    config: {
        cancelable: false,
        infinite: true
    },

  inheritableStatics: {
        REQUEST_DATATYPE: 'ZFRAPORT_IAUS'
    },

    _currentRequestCriteria : null,
    _syncManager: null,

    _DispositionItems: null,

    //protected
    //@override
    beforeInitialization: function(argumentsObject) {
        try {
            this._DispositionItems = argumentsObject['DispositionItems'];

            var syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
            this._uploadManager = Ext.create('AssetManagement.customer.sync.UploadManager', {
                dataBaseHelper: null,
                linkedSyncManager: syncManager,
                listeners: {
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });

            this._parsingManager = Ext.create('AssetManagement.customer.sync.ParsingManager', {
                dataBaseHelper: AssetManagement.customer.core.Core.getDataBaseHelper(),
                linkedSyncManager: syncManager,
                listeners: {
                    progressChanged: { fn: this.innerProgressReporter, scope: this },
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }

            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseOnlineDispositionDialogController', ex);
        }
    },

    //protected
    //@override
    //prevent starting the process on show
    onStartProcess: function () {
        var retval = false;

        try {
            retval = true;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStartProcess of BaseOnlineDispositionDialogController', ex);
            this.reportError();
        }

        return retval;
    },

    //protected
    //@override
    doWork: function () {
        try {
            this.prepareOnlineOrderCreationRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doWork of BaseOnlineDispositionDialogController', ex);
            this.errorOccured();
        }
    },

    prepareOnlineOrderCreationRequest: function() {
        try {
            //check if client is online
            var me = this;

            var onlineCallback = function(success) {
                if(success) {
                    //get the upstream for the selected assignments
                    var upstream = me.getUploadStreamForDownloadAssingments();

                    //if there is no upstream, stop with an error
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(upstream)) {
                        me.reportError();
                        return;
                    }
                    //do the actual online call next
                    me.callOnlineRequestAPI(upstream);
                }else{
                    var errorMessage = Locale.getMsg('browserIsOffline');
                    me.reportError(errorMessage);
                    return;
                }
            };
            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback)
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareOnlineOrderCreationRequest of BaseOnlineDispositionDialogController', ex);
            this.reportError();
        }
    },

    callOnlineRequestAPI: function(upstream) {
        try {
            var me = this;

            var postValues = this.getRequestPostValues(upstream);

            if(!postValues) {
                me.errorOccured();
                return;
            }

            var callback = function (jsonResponse) {
                try {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(jsonResponse)) {
                        me._syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
                        me._timeStampDown = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
                        var jsonResponseObject = Ext.decode(jsonResponse);
                        var onlineOrderCreationResponse = jsonResponseObject ? jsonResponseObject['ONLINE_REQUEST_RESULT'] : undefined;

                        if(onlineOrderCreationResponse.RESULT_CODE === '0' ) {
                            // me.onWorkFinished(onlineOrderCreationResponse.RESULT_MESSAGE);
                            me.confirmResponse(onlineOrderCreationResponse);
                        } else {
                            me.examineResponse(onlineOrderCreationResponse);
                        }
                    } else{
                        me.reportError();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineDispositionDialogController', ex);
                    me.reportError();
                }
            };

            var errorCallback = function (failureTypeOrErrorMessage) {
                try {
                    var message = '';

                    switch(failureTypeOrErrorMessage) {
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                            message = Locale.getMsg('browserIsOffline');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                            message = Locale.getMsg('connectionTimeoutOccured');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                            message = Locale.getMsg('mymUnknownError');
                            break;
                    }

                    me.reportError(message);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineDispositionDialogController', ex);
                    me.reportError();
                }
            };

            var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                listeners: {
                    sendFailed: { fn: errorCallback, scope: this },
                    errorOccured: { fn: errorCallback, scope: this }
                }
            });

            connManager.sendToBackEnd(AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.ONLINEREQ, postValues, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineDispositionDialogController', ex);
            this.reportError()
        }
    },

    confirmResponse: function (responseObject) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelWork();
                return;
            }

            var syncGuid = responseObject['SYNC_RESPONSE'] ? responseObject['SYNC_RESPONSE']['SYNC_GUID'] : '';
            if(syncGuid) {
                AssetManagement.customer.sync.SyncManager.getInstance().confirmSyncGUID(syncGuid);
            }

            this.examineResponse(responseObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirmResponse of BaseOnlineDispositionDialogController', ex);
            this.errorOccured();
        }
    },

    examineResponse: function (responseObject) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelRunningSync();
                return;
            }

            AssetManagement.customer.helper.OxLogger.logMessage('Begin examining response.');

            var mainResultCode = responseObject['RESULT_CODE'];
            var hasResponseError = false;
            var firstErrorMessage = '';
            if(mainResultCode && mainResultCode !== "0") {
                hasResponseError = true;
                var returnErrorResults = responseObject.RESULTS[0];
                var returnErrorTabs = returnErrorResults.RETURN_TAB;
                var errorMessageArray = [];
                for(var i = 0; i < returnErrorTabs.length; i++){
                    if(errorMessageArray.length === 0){
                        errorMessageArray.push(returnErrorTabs[i].MESSAGE)
                    }else{
                        if(errorMessageArray.indexOf(returnErrorTabs[i].MESSAGE) < 0){ //Filter for duplicate Messages
                            errorMessageArray.push(returnErrorTabs[i].MESSAGE);
                        }
                    }
                }
            }

            if (hasResponseError) {
                AssetManagement.customer.helper.OxLogger.logMessage('Aborting request because an error has been found on the response.');
                AssetManagement.customer.helper.OxLogger.logMessage('End examining response.');
                this.reportErrorArray(errorMessageArray);
                return;
            } else {
                AssetManagement.customer.helper.OxLogger.logMessage('End examining response.');
                var messagesArray = responseObject ? responseObject['RESULTS'] : null;
                this.parseReceivedData(responseObject, messagesArray);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside examineResponse of BaseOnlineDispositionDialogController', ex);
            this.errorOccurred();
        }
    },

  parseReceivedData: function (JSONresponse, messagesArray) {
    try {
      if (this._cancelWorkRequested || this._errorOccured) {
        this.cancelWork();
        return;
      }

      var me = this;
      var callback = function (parseResults) {
        try {
          me._waitingForParsing = false;

          if (me._cancelWorkRequested || me._errorOccured) {
            me.cancelWork();
            return;
          }

          me.resetCashes(parseResults, messagesArray);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseReceivedData of BaseOnlineDispositionDialogController', ex);
          me.reportError();
        }
      };

      var dataToParse = JSONresponse['SYNC_RESPONSE']['SYNC_DATA'];
      var syncTimestamp = JSONresponse['SYNC_RESPONSE']['RETURN_TIMESTAMP'];
      //parsing data starts here
      this._waitingForParsing = true;
      //parsing data starts here
      this._waitingForParsing = true;
      Ext.defer(this._parsingManager.parseData, 1, this._parsingManager, [dataToParse, syncTimestamp, true, callback, this]);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside parseReceivedData of BaseOnlineDispositionDialogController', ex);
      this.reportError();
    }
  },

  resetCashes: function (parseResults, messagesArray) {
    try {
      if (this._cancelWorkRequested || this._errorOccured) {
        this.cancelWork();
        return;
      }

      if (parseResults) {
        AssetManagement.customer.manager.CacheManager.clearCaches();
      }

      this.onWorkFinished(true,messagesArray);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetCashes of BaseOnlineDispositionDialogController', ex);
      this.reportError();
    }
  },

    buildResponseMessagesStore: function (messagesArray) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelWork();
                return;
            }

            var returnMessagesStore = Ext.create('Ext.data.Store', {
                extend: 'Ext.data.Model',
                fields: [
                    { name: 'busobject',            type: 'auto' },
                    { name: 'busobjkey',            type: 'string' },
                    { name: 'resultMessage',        type: 'string' },
                    { name: 'resultCode',           type: 'string' }

                ]
            });

            if(messagesArray && messagesArray.length > 0) {
                var i = 0;
                var msgArrayLength = messagesArray.length;
                for(i ; i < msgArrayLength; i++) {
                    if(messagesArray[i]['BUSOBJECT_KEYS'].length > 0) {
                        var busObject = messagesArray[i]['BUSOBJECT_KEYS'][0]['BUSOBJECT'];
                        var busObjectKey = messagesArray[i]['BUSOBJECT_KEYS'][0]['BUSOBJKEY'];

                    }

                    var returnMessage = Ext.create('Ext.data.Model', {
                        busobject: busObject ? busObject : '',
                        busobjkey: busObjectKey ? busObjectKey : '',
                        resultMessage: messagesArray[i]['RESULT_MESSAGE'],
                        resultCode: messagesArray[i]['RESULT_CODE']
                    });

                    returnMessagesStore.add(returnMessage);
                }
            }
            this.onWorkFinished(true, returnMessagesStore);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred loadStockDataFromDatabase buildResponseMessagesStore of BaseOnlineDispositionDialogController', ex);
            this.reportError();
        }
    },
    //private
    //previous step: postVanChange
    //next step: examineResponse
    getUploadStreamForDownloadAssingments: function () {
        var retval = '';

        try {
            var DispositionItems = this._DispositionItems;
            var objectItemsArray = [];

            if(DispositionItems && DispositionItems.getCount() > 0){
                DispositionItems.each(function(DispositionItem){
                    var objectListItem = {
                        "BELNR" : DispositionItem.get('belnr'),
                        "USNAM" : DispositionItem.get('usnam'),
                        "IA_STATUS" : DispositionItem.get('ia_status')
                    };
                    objectItemsArray.push(objectListItem);
                });
            }

            var order = {
                    "IAUS" : objectItemsArray
            };
            var orderAsJSON = JSON.stringify(order);
            return orderAsJSON;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadStreamForAssignments of BaseOnlineDispositionDialogController', ex);
        }

        return retval;
    },

    //private
    //builds the post values for the online request
    getRequestPostValues: function (upstream) {
        var retval = null;

        try {
            var nameValuePairList = Ext.create('Ext.util.MixedCollection');

            var ac = AssetManagement.customer.core.Core.getAppConfig();
            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("UPTIMESTAMP", AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(AssetManagement.customer.utils.DateTimeUtils.getCurrentTime()));
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());

            //set datatype for online api
            nameValuePairList.add("DATATYPE", this.self.REQUEST_DATATYPE);

            //set upstream
            nameValuePairList.add("UPSTREAM", upstream);

            retval = nameValuePairList;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRequestPostValues of BaseOnlineDispositionDialogController', ex);
        }

        return retval;
    },

    //protected
    //@override
    getGeneralProcessErrorMessage: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('generalErrorOccurredDuringDownloadingAssignments');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralProcessErrorMessage of BaseOnlineDispositionDialogController', ex);
        }

        return retval;
    },

    //private
    reportWorkProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.reportProgress(curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkProgress of BaseOnlineDispositionDialogController', ex);
            this.errorOccured(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    errorOccured: function (message) {
        try {
            this._processRunning = false;
            this._waitingForNetwork = false;
            this._waitingForParsing = false;
            this._cancelWorkRequested = false;

            this._errorOccured = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = Locale.getMsg('newQuotationGeneralProcessingError');

            this.reportError(message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOcurred of BaseOnlineDispositionDialogController', ex);
        }
    },

    //private
    cancelWork: function () {
        try {
            this._processRunning = false;
            this._cancelWorkRequested = false;

            this.onWorkerCancelled();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseOnlineDispositionDialogController', ex);
            this.errorOccured(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    innerProgressReporter: function (percentage, message) {
        try {
            var additionalPercentage = percentage - this._curProcessStepPercentage;
            this._curProcessStepPercentage = percentage;
            this.reportWorkProgress(this._curProcessPercent + this._curProcessStepRange * additionalPercentage / 100, message);
        } catch (ex) {
            this.errorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside innerProgressReporter of BaseOnlineDispositionDialogController', ex);
        }
    },


    reportErrorArray: function(messageArray) {
        try {
            this._errorOnWorker = true;
            var message = '';
            if (messageArray.length === 0)
                message = this.getGeneralProcessErrorMessage();


            var func = function(messageArray) {
                this.cancel(messageArray);
            };

            Ext.defer(func, 2000, this, [messageArray]);

            if (this.getInfinite())
                this.getView().stopProgressBar();

            this.getView().refreshView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportError of BaseCancelableProgressDialogController', ex);
        }
    }

});