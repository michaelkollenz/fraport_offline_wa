Ext.define('AssetManagement.base.controller.dialogs.BaseMaterialPickerDialogController', {
  extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


  requires: [
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.manager.BillOfMaterialManager',
    'AssetManagement.customer.manager.MaterialManager',
    'AssetManagement.customer.manager.MaterialStockageManager',
    'AssetManagement.customer.manager.OrderManager',
    'AssetManagement.customer.utils.StringUtils'
  ],

  config: {
    contextReadyLevel: 2
  },

  inheritableStatics: {
    //interface
    ON_MATERIAL_SELECTED: 'onMaterialSelected'
  },

  //@override
  startBuildingDialogContext: function (argumentsObject) {
    try {
      if (!argumentsObject) {
        throw Ext.create('AssetManagement.customer.utils.OxException', {
          message: 'Insufficient parameters',
          method: 'startBuildingPageContext',
          clazz: 'AssetManagement.customer.controller.dialogs.MaterialPickerDialogController'
        });
      }

      var aufnr = '';

      if (argumentsObject['aufnr'] && typeof argumentsObject['aufnr'] === 'string') {
        aufnr = argumentsObject['aufnr'];
      } else if (argumentsObject['order'] && Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
        this.getViewModel().set('order', argumentsObject['order']);
      }

      if (argumentsObject.useComponents === false) {
        this.getViewModel().set('useComponents', false);
      }

      if (argumentsObject.useBillOfMaterial === false) {
        this.getViewModel().set('useBillOfMaterial', false);
      }

      if (argumentsObject.useMatStocks === false) {
        this.getViewModel().set('useMatStocks', false);
      }

      var aufnrPassed = aufnr !== '';

      if (aufnrPassed) {
        this.getOrder(aufnr);
      } else {
        this.waitForDataBaseQueue();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseMaterialPickerDialogController', ex);
      this.errorOccurred();
    }
  },

  getOrder: function (aufnr) {
    try {
      var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
      this.addRequestToDataBaseQueue(orderRequest, 'order');

      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrder of BaseMaterialPickerDialogController', ex);
    }
  },

  performContextLevel1Requests: function () {
    try {
      var viewModel = this.getViewModel();
      var order = viewModel.get('order');

      if (order) {
        //extract components
        if (viewModel.get('useComponents'))
          viewModel.set('components', order.get('components'));
      }

      //load bom
      if (viewModel.get('useBillOffMaterial')) {
        if (order) {
          var bom = AssetManagement.customer.manager.BillOfMaterialManager.getBillOfMaterialForOrder(order, true, true);
          this.addRequestToDataBaseQueue(bom, "billOfMaterial");
        }
      }

      //load matstocks
      if (viewModel.get('useMatStocks')) {
        var unplanned = AssetManagement.customer.manager.MaterialStockageManager.getMatStocks();
        this.addRequestToDataBaseQueue(unplanned, "matStocks");
      }

      //load all materials
      if (viewModel.get('useAllMaterials')) {
        var allMaterials = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
        this.addRequestToDataBaseQueue(allMaterials, "allMaterials");
      }

      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseMaterialPickerDialogController', ex);
    }
  },

  //list handler
  plannedSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      this.onMaterialSelected(record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside plannedSelected of BaseMaterialPickerDialogController', ex);
    }
  },

  unplannedSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      this.onMaterialSelected(record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unplannedSelected of BaseMaterialPickerDialogController', ex);
    }
  },

  onlineMaterialSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      this.onMaterialSelected(record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlineMaterialSelected of BaseMaterialPickerDialogController', ex);
    }
  },

  bomEntrySelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      this.onMaterialSelected(record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside bomEntrySelected of BaseMaterialPickerDialogController', ex);
    }
  },

  materialSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      this.onMaterialSelected(record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside materialSelected of BaseMaterialPickerDialogController', ex);
    }
  },

  onMaterialSelected: function (materialInfoObject) {
    try {
      var callbackInterface = this.getOwner()[this.self.ON_MATERIAL_SELECTED];

      if (callbackInterface)
        callbackInterface.call(this.getOwner(), materialInfoObject);

      //dismiss
      this.dismiss();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialSelected of BaseMaterialPickerDialogController', ex);
    }
  },

  //@override
  dataBaseQueueComplete: function () {
    try {
      this.callParent();

      if (this._contextLevel === 1) {
        this.performContextLevel1Requests();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseMaterialPickerDialogController', ex);
    }
  },

  //@override
  getContextLoadingErrorMessage: function () {
    return "Data error while loading materials.";
  },

  ///MATERIAL SERACH
  onMaterialPickerDialogSearchFieldChange: function () {
    try {
      this.getView().refreshView();
      this.applyCurrentFilterCriteriasToStore();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialListPageSearchFieldChange of BaseMaterialPickerDialogController', ex);
    }
  },

  applyCurrentFilterCriteriasToStore: function () {
    try {
      this.getView().getSearchValue();

      var storeToSearch = null;

      var tabIndex = Ext.getCmp('materialDialogTabPanel').getActiveTab();

      var viewModel = this.getViewModel();
      if (tabIndex.itemId === 'plannedMaterialTab')
        storeToSearch = viewModel.get('components');
      if (tabIndex.itemId === 'bomTab')
        storeToSearch = viewModel.get('billOfMaterial');
      if (tabIndex.itemId === 'matStockTab')
        storeToSearch = viewModel.get('matStocks');
      if (tabIndex.itemId === 'allMaterialsTab')
        storeToSearch = viewModel.get('allMaterials');

      var materialToSearch = viewModel.get('searchMaterial');

      if (storeToSearch) {
        storeToSearch.clearFilter();

        // Material
        if (materialToSearch.get('material').get('matnr')) {
          storeToSearch.filterBy(function (record) {
            try {
                var filteredValue = record.get('matnr');

                if (filteredValue && filteredValue.toUpperCase().indexOf(materialToSearch.get('material').get('matnr').toUpperCase()) > -1) {
                  return true;
                }
            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          // Kurztext
        } else if (materialToSearch.get('material').get('maktx')) {
          storeToSearch.filterBy(function (record) {
            try {
              var filteredValue = record.get('maktx');

              if (filteredValue && filteredValue.toUpperCase().indexOf(materialToSearch.get('material').get('maktx').toUpperCase()) > -1) {
                return true;
              }

            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          //  Menge
        } else if (materialToSearch.get('labst')) {
          storeToSearch.filterBy(function (record) {
            try {
              var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'));
              if (amount.indexOf(materialToSearch.get('labst').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          // Einheit
        } else if (materialToSearch.get('meins')) {
          storeToSearch.filterBy(function (record) {
            try {
              if (record.get('meins').toUpperCase().indexOf(materialToSearch.get('meins').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          // Werk / Lagerort
        } else if (materialToSearch.get('werks') || materialToSearch.get('lgort')) {
          storeToSearch.filterBy(function (record) {
            try {
              if (record.get('werks').toUpperCase().indexOf(materialToSearch.get('werks').toUpperCase()) > -1 ||
                record.get('lgort').toUpperCase().indexOf(materialToSearch.get('lgort').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          // Vorgang
        } else if (materialToSearch.get('vornr')) {
          storeToSearch.filterBy(function (record) {
            try {
              if (record.get('vornr').toUpperCase().indexOf(materialToSearch.get('vornr').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          //Menge / Einheit
        } else if (materialToSearch.get('bdmng') || materialToSearch.get('meins')) {
          storeToSearch.filterBy(function (record) {
            try {
              if (record.get('bdmng').toUpperCase().indexOf(materialToSearch.get('bdmng').toUpperCase()) > -1 ||
                record.get('meins').toUpperCase().indexOf(materialToSearch.get('bdmng').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          //Einsatzmenge
        } else if (materialToSearch.get('menge')) {
          storeToSearch.filterBy(function (record) {
            try {
              if (record.get('menge').toUpperCase().indexOf(materialToSearch.get('menge').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
          //Positionsnummer/ Pos-Typ
        } else if (materialToSearch.get('posnr') || materialToSearch.get('postp')) {
          storeToSearch.filterBy(function (record) {
            try {
              if (record.get('posnr').toUpperCase().indexOf(materialToSearch.get('posnr').toUpperCase()) > -1 ||
                record.get('postp').toUpperCase().indexOf(materialToSearch.get('postp').toUpperCase()) > -1) {
                return true;
              }
            } catch (ex) {
              AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseMaterialPickerDialogController', ex);
            }
          });
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseMaterialPickerDialogController', ex);
    }
  },

  beforeDataReady: function () {
    try {
      //apply the current filter criteria on the store, before it is transferred into the view
      this.applyCurrentFilterCriteriasToStore();

      //clear online material store
      this.getViewModel().set('onlineMaterialStore', null);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of MaterialListController', ex);
    }
  },

  onTabChannelChanged: function (tabPanel, newCard, oldCard, eOpts) {
    try {
      this.getView().setCriteriaComboBox(tabPanel.getActiveTab());
      var newTabId = newCard['itemId'];
      if (newTabId === 'onlineMatTab') {
        this.openOnlineMaterialSearchDialog();
        //AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_MAT_SEARCH_DIALOG);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onTabChannelChanged of MaterialListController', ex);
    }
  },

  openOnlineMaterialSearchDialog: function () {
    try {
      var myModel = this.getViewModel();

      var me = this;

      var successCallback = function (resultsStore) {
        try {
          myModel.set('onlineMaterialStore', resultsStore);
          me.refreshView();
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOnlineAssignmentsButtonClicked-successCallback of OnlineAssignmentListPageController', ex);
        }
      };

      var failureCancelCallback = function (message) {
        try {
          if (message) {
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(message, false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOnlineAssignmentsButtonClicked-failureCancelCallback of OnlineAssignmentListPageController', ex);
        }
      };

      var passedArguments = {
        successCallback: successCallback,
        cancelCallback: failureCancelCallback,
        scope: this
      };

      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_MAT_SEARCH_DIALOG, passedArguments);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openOnlineMaterialSearchDialog of MaterialListController', ex);
    }
  }

});
