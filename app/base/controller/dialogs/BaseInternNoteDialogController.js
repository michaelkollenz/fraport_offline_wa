﻿Ext.define('AssetManagement.base.controller.dialogs.BaseInternNoteDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	    'AssetManagement.customer.utils.StringUtils',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
		'AssetManagement.customer.helper.OxLogger'
    ],

    config: {
        contextReadyLevel: 1
    },

    //protected
    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            var myModel = this.getViewModel();
            if (argumentsObject['equi'] && Ext.getClassName(argumentsObject['equi']) === 'AssetManagement.customer.model.bo.Equipment')
                myModel.set('bo', argumentsObject['equi']);
            else
            {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.InternNoteDialogController'
                });
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseInternNoteDialogController', ex);
            this.errorOccurred();
        }
    },

    //protected
    //@override
    beforeDataReady: function () {
        try {
            var myModel = this.getViewModel();
            var bo = myModel.get('bo');

            if (bo) {
                var internNote = bo.get('internNote');

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(internNote))
                    myModel.set('internNote', internNote);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseInternNoteDialogController', ex)
        }
    }
});
