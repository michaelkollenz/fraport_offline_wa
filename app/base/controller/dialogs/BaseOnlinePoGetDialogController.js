Ext.define('AssetManagement.base.controller.dialogs.BaseOnlinePoGetDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',
    alias: 'controller.BaseOnlinePoGetDialogController',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.NetworkHelper',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.sync.SyncManager',
        'AssetManagement.customer.sync.ParsingManager',
        'AssetManagement.customer.model.bo.PoItem'
    ],

    config: {
        cancelable: false,
        infinite: true
    },

  inheritableStatics: {
        REQUEST_DATATYPE: 'MM_PURCHASE_ORDER_SEARCH'
    },

    _currentRequestCriteria : null,
    _syncManager: null,

    _poNumber: null,
    _poPos: null,

    //protected
    //@override
    beforeInitialization: function(argumentsObject) {
        try {
            this._poNumber = argumentsObject['poNumber'];
            this._poPos = argumentsObject['poPos'];

            var syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
            this._uploadManager = Ext.create('AssetManagement.customer.sync.UploadManager', {
                dataBaseHelper: null,
                linkedSyncManager: syncManager,
                listeners: {
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });

            this._parsingManager = Ext.create('AssetManagement.customer.sync.ParsingManager', {
                dataBaseHelper: AssetManagement.customer.core.Core.getDataBaseHelper(),
                linkedSyncManager: syncManager,
                listeners: {
                    progressChanged: { fn: this.innerProgressReporter, scope: this },
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccured: { fn: this.errorOccured, scope: this }
                }
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseOnlinePoGetDialogController', ex);
        }
    },

    //protected
    //@override
    //prevent starting the process on show
    onStartProcess: function () {
        var retval = false;

        try {
            retval = true;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStartProcess of BaseOnlinePoGetDialogController', ex);
            this.reportError();
        }
        return retval;
    },

    //protected
    //@override
    doWork: function () {
        try {
            this.prepareOnlinePoGetRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doWork of BaseOnlinePoGetDialogController', ex);
            this.errorOccured();
        }
    },

    prepareOnlinePoGetRequest: function() {
        try {
            //check if client is online
            var me = this;

            var onlineCallback = function(success) {
                if(success) {
                    //get the upstream for the selected assignments
                    var upstream = me.getUploadStreamForDownloadAssingments();

                    //if there is no upstream, stop with an error
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(upstream)) {
                        me.reportError();
                        return;
                    }
                    //do the actual online call next
                    me.callOnlineRequestAPI(upstream);
                }else{
                    var errorMessage = Locale.getMsg('browserIsOffline');
                    me.reportError(errorMessage);
                    return;
                }
            };
            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback)

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareOnlinePoGetRequest of BaseOnlinePoGetDialogController', ex);
            this.reportError();
        }
    },

    callOnlineRequestAPI: function(upstream) {
        try {
            var me = this;
            var postValues = this.getRequestPostValues(upstream);

            if(!postValues) {
                me.errorOccured();
                return;
            }

            var callback = function (jsonResponse) {
                try {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(jsonResponse)) {
                        me._syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
                        me._timeStampDown = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
                        var jsonResponseObject = Ext.decode(jsonResponse);
                        var onlinePoGetResponse = jsonResponseObject ? jsonResponseObject['ONLINE_ANY_RESULT'] : undefined;

                        if(onlinePoGetResponse) {
                            me.buildResponseMessagesStore(onlinePoGetResponse);
                        } else {
                            me.reportError();
                        }
                    } else{
                        me.reportError();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlinePoGetDialogController', ex);
                    me.reportError();
                }
            };

            var errorCallback = function (failureTypeOrErrorMessage) {
                try {
                    var message = '';

                    switch(failureTypeOrErrorMessage) {
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                            message = Locale.getMsg('browserIsOffline');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                            message = Locale.getMsg('connectionTimeoutOccured');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                            message = Locale.getMsg('mymUnknownError');
                            break;
                    }

                    me.reportError(message);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlinePoGetDialogController', ex);
                    me.reportError();
                }
            };

            var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                listeners: {
                    sendFailed: { fn: errorCallback, scope: this },
                    errorOccured: { fn: errorCallback, scope: this }
                }
            });

            connManager.sendToBackEnd(AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.ONLINEREQ, postValues, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlinePoGetDialogController', ex);
            this.reportError()
        }
    },

    buildResponseMessagesStore: function (messagesArray) {
        try {
            if (this._cancelWorkRequested || this._errorOccured) {
                this.cancelWork();
                return;
            }

            var returnMessagesStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.PoItem',
                autoLoad: false
            });

            if(messagesArray && messagesArray.length > 0) {
                var msgArrayLength = messagesArray.length;
                for(var i = 0 ; i < msgArrayLength; i++) {
                    var returnMessage = Ext.create('AssetManagement.customer.model.bo.PoItem', {
                        ebeln: messagesArray[i]['EBELN'],
                        ebelp: messagesArray[i]['EBELP'],
                        txz01: messagesArray[i]['TXZ01'],
                        matnr: messagesArray[i]['MATNR'],
                        werks: messagesArray[i]['WERKS'],
                        lgort: messagesArray[i]['LGORT'],
                        menge: messagesArray[i]['MENGE'],
                        meins: messagesArray[i]['MEINS'],
                        // elikz: messagesArray[i]['ELIKZ'] === 'X' ? true : false, //if changed, check if needed in fraport!
                        menge_del: messagesArray[i]['MENGE_DEL']
                    });

                    var toDeliver = returnMessage.get('menge') * 1; //*1 is an int conversion
                    var alreadyDelivered = (returnMessage.get('menge_del') ? returnMessage.get('menge_del') : 0) * 1;
                    returnMessage.set('menge_ist', toDeliver - alreadyDelivered); //default value of the locally delivered quantity
                    if((returnMessage.get('menge_ist') *1) > 0)
                    returnMessagesStore.add(returnMessage);
                }
            }
            this.onWorkFinished(returnMessagesStore);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred loadStockDataFromDatabase buildResponseMessagesStore of BaseOnlinePoGetDialogController', ex);
            this.reportError();
        }
    },

    //private
    //previous step: postVanChange
    //next step: examineResponse
    getUploadStreamForDownloadAssingments: function () {
        var retval = '';

        try {
            var objectItemsArray = [];
            var objectListItem = {
                "FIELDNAME" : "EBELN",
                "VALUE" : this._poNumber,
                "SEARCH_OPERATOR" : "="
            };
            objectItemsArray.push(objectListItem);

            if(this._poPos){
                objectListItem = {
                    "FIELDNAME" : "EBELP",
                    "VALUE" : this._poPos,
                    "SEARCH_OPERATOR" : "="
                };
                objectItemsArray.push(objectListItem);
            }

            var order = {
                "MM_PURCHASE_ORDER_SEARCH" :  objectItemsArray
            };

          var orderAsJSON = JSON.stringify(order);
            return orderAsJSON;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadStreamForAssignments of BaseOnlinePoGetDialogController', ex);
        }

        return retval;
    },

    //private
    //builds the post values for the online request
    getRequestPostValues: function (upstream) {
        var retval = null;

        try {
            var nameValuePairList = Ext.create('Ext.util.MixedCollection');
            var ac = AssetManagement.customer.core.Core.getAppConfig();
            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("UPTIMESTAMP", AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(AssetManagement.customer.utils.DateTimeUtils.getCurrentTime()));
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());
            //set datatype for online api
            nameValuePairList.add("DATATYPE", this.self.REQUEST_DATATYPE);
            //set upstream
            nameValuePairList.add("UPSTREAM", upstream);

            retval = nameValuePairList;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRequestPostValues of BaseOnlinePoGetDialogController', ex);
        }
        return retval;
    },

    //protected
    //@override
    getGeneralProcessErrorMessage: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('generalErrorOccurredDuringDownloadingAssignments');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralProcessErrorMessage of BaseOnlinePoGetDialogController', ex);
        }
        return retval;
    },

    //private
    reportWorkProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.reportProgress(curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkProgress of BaseOnlinePoGetDialogController', ex);
            this.errorOccured(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    errorOccured: function (message) {
        try {
            this._processRunning = false;
            this._waitingForNetwork = false;
            this._waitingForParsing = false;
            this._cancelWorkRequested = false;
            this._errorOccured = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = Locale.getMsg('newQuotationGeneralProcessingError');

            this.reportError(message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOcurred of BaseOnlinePoGetDialogController', ex);
        }
    },

    //private
    cancelWork: function () {
        try {
            this._processRunning = false;
            this._cancelWorkRequested = false;
            this.onWorkerCancelled();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseOnlinePoGetDialogController', ex);
            this.errorOccured(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    innerProgressReporter: function (percentage, message) {
        try {
            var additionalPercentage = percentage - this._curProcessStepPercentage;
            this._curProcessStepPercentage = percentage;
            this.reportWorkProgress(this._curProcessPercent + this._curProcessStepRange * additionalPercentage / 100, message);
        } catch (ex) {
            this.errorOccured();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside innerProgressReporter of BaseOnlinePoGetDialogController', ex);
        }
    }

});