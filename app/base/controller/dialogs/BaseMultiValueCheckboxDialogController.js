﻿Ext.define('AssetManagement.base.controller.dialogs.BaseMultiValueCheckboxDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.controller.ToolbarController',
        'Ext.data.Store',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Charact',
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.manager.ClassificationManager'
    ],

    config: {
        contextReadyLevel: 1
    },
    inheritableStatics: {
        //interface
        ON_CLASSVALUE_SAVED: 'onClassValuesSaved'
    },


    //private
    _saveRunning: false,

    //protected
    //@Override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            if(Ext.getClassName(argumentsObject['charact']) !== 'AssetManagement.customer.model.bo.Charact' &&
                argumentsObject['objClass'] !== 'AssetManagement.customer.model.bo.ObjClass')
            {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.MultiValueCheckboxDialogController'
                });
            }

            if (Ext.getClassName(argumentsObject['charact']) === 'AssetManagement.customer.model.bo.Charact')
                this.getViewModel().set('charact', argumentsObject['charact']);

            if (Ext.getClassName(argumentsObject['objClass']) === 'AssetManagement.customer.model.bo.ObjClass')
                this.getViewModel().set('objClass', argumentsObject['objClass']);

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseMultiValueCheckboxDialogController', ex);
            this.errorOccurred();
        }
    },

    save: function () {
        try {
            if (!this._saveRunning)
                this.trySaveClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseMultiValueCheckboxDialogController', ex);
        }
    },

    dismiss: function () {
        try {
            this.callParent();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseMultiValueCheckboxDialogController', ex);
        }
    },

    //@override
    cancel: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseMultiValueCheckboxDialogController', ex)
        }
    },

    trySaveClassValue: function () {
        try {
            this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;
            var saveItem = function (checkWasSuccessful) {
                try {
                    if (checkWasSuccessful === true) {
                        var callback = function (success) {

                            try {
                                if (success === true) {
                                    me.onClassValuesSaved();

                                    me.dismiss();
                                    Ext.defer(function () {
                                        me._saveRunning = false;
                                    }, 2000, me);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseMultiValueCheckboxDialogController', ex);
                            }
                            finally {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        };
                        this.getView().transferInputValuesIntoModel();
                        me.prepareValuesForSave();
                        var myModel = this.getViewModel();
                        var classValues = myModel.get('charact').get('classValues');
                        var newValues = myModel.get('newValues');
                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValues)) {
                            classValues = Ext.create('Ext.data.Store', {
                                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                                autoLoad: false
                            });
                        }

                        var eventId = AssetManagement.customer.manager.ClassificationManager.updateClassValues(classValues, newValues);

                        if (eventId > 0) {
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                        } else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseMultiValueCheckboxDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            this.checkInputValues(saveItem, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseMultiValueCheckboxDialogController', ex);
            me._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }
                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseMultiValueCheckboxDialogController', ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    //prepare values for saving
    //seperate checked values from unchecked values
    //newValues are the current values, which are checked
    //classValues are the values before clicking 'save'
    prepareValuesForSave: function () {
        try {
            var myModel = this.getViewModel();
            var objClass = myModel.get('objClass');
            var charact = myModel.get('charact');
            
            if (charact === null || charact === undefined)
                return;

            var classValues = charact.get('classValues');
            var newValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            myModel.set('newValues', newValues);

            Ext.getCmp('itemContainer').items.each(function (checkboxItem) {
                var classValue = null;

                if (checkboxItem.checked === true) {
                    var match = null;
                    // filter classValues 
                    if (classValues && classValues.getCount() > 0) {
                        classValues.each(function (clValue) {
                            if (clValue.get('atwrt') === checkboxItem._charactValue.get('atwrt') && clValue.get('atflv') === checkboxItem._charactValue.get('atflv') && clValue.get('atzhl') === checkboxItem._charactValue.get('atzhl')) {
                                match = clValue;
                                return false;
                            }
                        });
                    }

                    if (match) {
                        classValue = match;
                    } else {
                        classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, checkboxItem._charactValue.get('atwrt'), checkboxItem._charactValue.get('atflv'), checkboxItem._charactValue.get('atzhl'));
                    }

                }

                if (classValue !== null) {
                    newValues.add(classValue);
                }
            });

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareValuesForSave of BaseMultiValueCheckboxDialogController', ex);
        }
    },

    //checks, if there are any differences between the current input state and the model state
    isInputEqualToModel: function () {
        var retval = true;

        try {
            var inputValues = this.getView().getCurrentInputValues();
            var funcLoc = this.getViewModel().get('objClass');
            var charact = this.getViewModel().get('charact');

            if (funcLoc.get('objnr') !== inputValues.objnr) {
                retval = false;
            } else if (!AssetManagement.customer.utils.DateTimeUtils.areEqual(funcLoc.get('atflv'), inputValues.atflv)) {
                retval = false;
            } else if (funcLoc.get('atwrt') !== inputValues.atwrt) {
                retval = false;
            } else if (funcLoc.get('atinn') !== inputValues.atinn) {
                retval = false;
            } else if (funcLoc.get('atflv') !== inputValues.atflv) {
                retval = false;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isInputEqualToModel of BaseMultiValueCheckboxDialogController', ex);
        }

        return retval;
    },

    onClassValuesSaved: function (classValues) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_CLASSVALUE_SAVED];

            if (callbackInterface) {
                var values = null;
                var newValues = this.getViewModel().get('newValues');
                var classValue = this.getViewModel().get('charact').get('classValues')
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValue))
                    values = this.getViewModel().get('charact').set('classValues', newValues);
                else
                    values = classValue
                callbackInterface.call(this.getOwner(), values);
            }

            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassValuesSaved of BaseMultiValueCheckboxDialogController', ex)
        }
    },

    //apply the current filter criteria on the store, before it is transferred into the view
    beforeDataReady: function () {
        try {
            this.backUpClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseMultiValueCheckboxDialogController', ex);
        }
    },

    //add to newValues store the cloned classValues(the current view values)
    backUpClassValue: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var classValues = charact.get('classValues');

            if (classValues && classValues.getCount() > 0) {
                var newValues = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });
                classValues.each(function (classVal) {
                    var value = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classVal);
                    newValues.add(value);
                }, this);
            }

            myModel.set('newValues', newValues);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside backUpClassValue of BaseMultiValueCheckboxDialogController', ex);
        }
    }
});