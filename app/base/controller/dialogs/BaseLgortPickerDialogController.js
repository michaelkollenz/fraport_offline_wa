Ext.define('AssetManagement.base.controller.dialogs.BaseLgortPickerDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',
    alias: 'controller.BaseLgortPickerDialogController',

    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.sync.ParsingManager',
        'AssetManagement.customer.manager.LgortManager',
        'AssetManagement.customer.sync.SyncManager',
        'AssetManagement.customer.manager.CacheManager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.sync.ConnectionManager'

    ],

    config: {
        contextReadyLevel: 1
    },

    //protected
    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            // argumentsObject.title = Locale.getMsg('selectVan');
            // argumentsObject.initialMessage = ' ';
            this._callback = argumentsObject['callback'];

            var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
            this.getViewModel().set('selectedLgort', selectedLgort);
            var eventId = AssetManagement.customer.manager.LgortManager.getLgorts(false);
            this.addRequestToDataBaseQueue(eventId, 'lgortStore');
            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseLgortPickerDialogController', ex);
        }
    },

    //public
    onConfirmSelection: function(){
        try {
            var inputValues = this.getView().getInputValues();
            var selectedLgort = null;
            if (inputValues){
                selectedLgort = inputValues;
            }

            if(selectedLgort){
                // var myModel = this.getViewModel();
                // myModel.set('selectedLgort', selectedLgort);
                this.dismiss();

                if(this._callback) {
                    //if the callback contains direct navigation, this will crash the navigation state
                    //to avoid this, defer the callback

                    Ext.defer(this._callback, 100, this._callbackScope ? this._callbackScope : this, [ selectedLgort ]);
                }
            }else{
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);

            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNewVanSelected of BaseLgortPickerDialogController', ex);
            this.reportError();
        }
    }
});