﻿Ext.define('AssetManagement.base.controller.dialogs.BaseSerialNoEditDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
       'AssetManagement.customer.utils.StringUtils',
 	   'AssetManagement.customer.helper.OxLogger',
       'Ext.data.Store',
       'AssetManagement.customer.utils.ArgumentsHelper'
    ],

    config: {
        contextReadyLevel: 1
    },

    inheritableStatics: {
        //interface
        ON_SERIALNO_SAVED: 'onSerialNoSaved',
        ON_SERIALNO_DELETE: 'onSerialNoDelete'
    },

    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            if (argumentsObject.serialNumberList != null && argumentsObject.serialNumberList.getCount() > 0) {
                this.getViewModel().set('serialNumberList', argumentsObject.serialNumberList);
                //this.getViewModel().set('alreadyAddedSerialNo', serialNumberList.getCount().toString());
            }
            if (typeof argumentsObject['totalSerialNoAllowed'] === 'string') {
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(argumentsObject['totalSerialNoAllowed'])) {
                    this.getViewModel().set('totalSerialNoAllowed', argumentsObject['totalSerialNoAllowed']);
                }
            }
            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseSerialNoEditDialogController', ex)
        }
    },
    //@override
    save: function () {
        try {
            this.trySaveSerialNo();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseSerialNoEditDialogController', ex)
        }
    },

    //@override
    cancel: function () {
        try {
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseSerialNoEditDialogController', ex)
        }
    },

    trySaveSerialNo: function () {
        try {
            var me = this;
            me.onSerialNoSaved(me);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveSerialNo of BaseSerialNoEditDialogController', ex);
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //add serialnumber to serialnumber list
    onSerialNoSaved: function (serialNumbersInfoObject) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_SERIALNO_SAVED];

            if (callbackInterface)
                callbackInterface.call(this.getOwner(), serialNumbersInfoObject);

            //dismiss
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSerialNoSaved of BaseSerialNoEditDialogController', ex)
        }
    },

    //delete serialnumber from serialnumber list
    onSerialNumberDelete: function (deleteSerialNo, metaData, rowIndex, celIndex) {
        try {
            var record = deleteSerialNo.getStore().getAt(rowIndex);

            var serialNoList = this.getViewModel().get('serialNumberList');

            serialNoList.remove(record);

            this.getViewModel().set('alreadyAddedSerialNo', this.getViewModel().get('serialNumberList').getCount().toString());

            this.getView().refreshView();

            /*var callbackInterface = this.getOwner()[this.self.ON_SERIALNO_DELETE];

            if (callbackInterface)
                callbackInterface.call(this.getOwner(), deleteSerialNo);*/
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkItemsCompleted of BaseSerialNoEditDialogController', ex);
        }
    },

    onSerialNumberAdd: function () {
        try {
            //you have to check if the quantity of the object is equal of the added serial numbers
            /*if (parseInt(this.getViewModel().get('totalSerialNoAllowed')) === parseInt(this.getViewModel().get('alreadyAddedSerialNo'))) {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('allSerialNumbersAdded'), true, 0);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                return;
            }*/
            //hide keyboard
            document.activeElement.blur();

            var serialNumbers = this.getView();
            serialNumbers.getSerialNoTextFieldValue();

            // get the new added value of the serial number textfield
            var addedSerialNo = this.getViewModel().get('serialNoValue');
            var serialNoList = this.getViewModel().get('serialNumberList');
            var doubleSerialNo = '';

            //check first if the new added serial number is already added to the serial number list
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addedSerialNo)) {
                if (serialNoList && serialNoList.getCount() > 0) {
                    serialNoList.each(function (serial) {
                        if (serial.get('serialNo') === addedSerialNo) {
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('alreadyAddedSerialNumber'), true, 0);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                            doubleSerialNo = 'doubleSerialNo';
                            return;
                        }
                    });
                }
            }

            if (doubleSerialNo === 'doubleSerialNo') {
                return;
            }

            //add the new added serial number to the existing serial number list or create a new serial number list
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addedSerialNo)) {
                if (serialNoList && serialNoList.getCount() > 0) {
                    var toAdd = Ext.create('Ext.data.Model', {
                        serialNo: addedSerialNo
                    });
                    serialNoList.add(toAdd);
                    this.getViewModel().set('serialNumberList', serialNoList);

                } else {
                    var serialListStore = Ext.create('Ext.data.Store', {
                        storeId: 'serialListStore',
                        fields: ['serialNo']
                    });
                    var toAdd = Ext.create('Ext.data.Model', {
                        serialNo: addedSerialNo
                    });
                    serialListStore.add(toAdd);
                    this.getViewModel().set('serialNumberList', serialListStore);
                }

                this.getViewModel().set('alreadyAddedSerialNo', this.getViewModel().get('serialNumberList').getCount().toString());

                this.getView().refreshView();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSerialNumberAdd of BaseSerialNoEditDialogController', ex)
        }
    },

    //@override
    getContextLoadingErrorMessage: function () {
        return "Data error while loading serialnumbers.";
    }
});