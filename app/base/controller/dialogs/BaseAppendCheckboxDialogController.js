﻿Ext.define('AssetManagement.base.controller.dialogs.BaseAppendCheckboxDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
'AssetManagement.customer.utils.StringUtils',
'AssetManagement.customer.manager.EquipmentManager',
'AssetManagement.customer.manager.FuncLocManager',
'AssetManagement.customer.manager.ClassificationManager',
'AssetManagement.customer.controller.ToolbarController',
'Ext.data.Store',
'AssetManagement.customer.helper.OxLogger',
'AssetManagement.customer.model.bo.Charact',
'AssetManagement.customer.controller.EventController',
'AssetManagement.customer.view.CharactValueCheckboxItem'
    ],

    config: {
        contextReadyLevel: 1
    },
    inheritableStatics: {
        //interface
        ON_CLASSVALUE_SAVED: 'onClassValuesSaved'
    },


    //private
    _saveRunning: false,

    //protected
    //@Override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            if (Ext.getClassName(argumentsObject['charact']) !== 'AssetManagement.customer.model.bo.Charact' &&
                   Ext.getClassName(argumentsObject['objClass']) !== 'AssetManagement.customer.model.bo.ObjClass') {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.AppendCheckboxDialogController'
                });
            }

            if (Ext.getClassName(argumentsObject['charact']) === 'AssetManagement.customer.model.bo.Charact')
                this.getViewModel().set('charact', argumentsObject['charact']);

            if (Ext.getClassName(argumentsObject['objClass']) === 'AssetManagement.customer.model.bo.ObjClass')
                this.getViewModel().set('objClass', argumentsObject['objClass']);

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseAppendCheckboxDialogController', ex);
            this.errorOccurred();
        }
    },

    save: function () {
        try {
            if (!this._saveRunning)
                this.trySaveClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseAppendCheckboxDialogController', ex);
        }
    },

    dismiss: function () {
        this.callParent();

        try {
            //this.unregisterListenersFromWorker();
            //if (this.getInfinite())
            //    this.getView().stopProgressBar();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseAppendCheckboxDialogController', ex);
        }
    },

    //@override
    cancel: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseAppendCheckboxDialogController', ex)
        }
    },

    trySaveClassValue: function () {
        try {
            this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
            var me = this;
            var saveItem = function (checkWasSuccessful) {
                try {
                    if (checkWasSuccessful === true) {
                        var callback = function (success) {
                            try {
                                if (success === true) {
                                    me.onClassValuesSaved();

                                    me.dismiss();
                                    Ext.defer(function () {
                                        me._saveRunning = false;
                                    }, 2000, me);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseAppendCheckboxDialogController', ex);
                            }
                            finally {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        };
                        me.transferInputValuesIntoModel();
                        me.prepareValuesForSave();

                        var classValues = this.getViewModel().get('charact').get('classValues');
                        var newValues = this.getViewModel().get('newValues');

                        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValues.data.items[0].get('atwrt'))) {
                            classValues = Ext.create('Ext.data.Store', {
                                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                                autoLoad: false
                            });
                        }
                        var eventId = AssetManagement.customer.manager.ClassificationManager.updateClassValues(classValues, newValues);

                        if (eventId > 0) {
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                        } else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseAppendCheckboxDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            this.checkInputValues(saveItem, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseAppendCheckboxDialogController', ex);
            me._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';


            var values = this.getView().getCurrentInputValues();


            var atflv = values.atflv;
            var atwrt = values.atwrt;

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }
                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseAppendCheckboxDialogController', ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    transferInputValuesIntoModel: function () {
        var retval = false;
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var viewValue = Ext.getCmp('itemContainer2').items;
            var classValues = charact.get('classValues');
            var objClass = myModel.get('objClass');
            var trueClassValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            if (!classValues) {
                classVal = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });

                charact.set('classValues', classVal);
            }
            var newClassValue = null;
            viewValue.each(function (classV) {
                if (classV.fieldLabel === '') {
                    var classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, classV.lastValue, '');
                    newClassValue = classValue;
                } else {
                    newClassValue = classV.getClassValue();

                }
                if (newClassValue) {
                    newClassValue.set('clint', objClass.get('clint'));
                    trueClassValues.add(newClassValue);
                }

            }, this);
            myModel.set('newValues', trueClassValues);
            retval = true;

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferInputValuesIntoModel of BaseAppendCheckboxDialogController', ex);
        }
        return retval;
    },

    prepareValuesForSave: function () {
        var retval = null;
        try {
            var myModel = this.getViewModel();
            var objClass = myModel.get('objClass');
            var charact = myModel.get('charact');
            var classValues = charact.get('classValues');

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(charact))
                return;

            var newValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            var delValues = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ObjClassValue',
                autoLoad: false
            });

            Ext.getCmp('itemContainer2').items.each(function (checkboxItem) {
                var classValue = null;

                if (checkboxItem.checked === true) {
                    var matchesFound = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.ObjClassValue',
                        autoLoad: false
                    });

                    // filter classValues 
                    classValues.each(function (clValues) {
                        if (clValues.get('atwrt') === checkboxItem.atwrt && clValues.get('atflv') === checkboxItem.atflv)
                            matchesFound.add(clValues);

                    });
                    if (matchesFound.getCount() > 0) {

                        if (matchesFound.data.items[0].get('updFlag') === 'D')
                            matchesFound.data.items[0].set('updFlag', 'X');

                        classValue = matchesFound.data.items[0];
                    } else {
                        classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, checkboxItem.atwrt, checkboxItem.atflv);

                    }
                } else {
                    var matchesFound = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.ObjClassValue',
                        autoLoad: false
                    });

                    var textInput = '';
                    if (checkboxItem.usesTextInput === true) {
                        textInput = checkboxItem.lastValue;
                    }

                    if (classValues !== undefined) {
                        classValues.each(function (clValues) {
                            if (clValues.get('atwrt') === checkboxItem.atwrt && clValues.get('atflv') === checkboxItem.atflv)
                                matchesFound.add(clValues);
                        });
                    }

                    if (matchesFound.getCount() > 0) {
                        if (matchesFound.data.items[0].get('updFlag') === 'I') {
                            delValues.add(matchesFound.data.items[0]);
                        }
                        else {
                            matchesFound.data.items[0].set('updFlag', 'D');
                            classValue = matchesFound.data.items[0];
                        }
                    } else {
                        classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValueSingular(charact, objClass, textInput, checkboxItem.atflv);

                    }
                }

                if (classValue !== null) {
                    classValue.set('clint', objClass.get('clint'));
                    classValue.set('atzhl', checkboxItem.atzhl);
                    classValue.set('mobileKey', objClass.get('mobileKey'));
                    newValues.add(classValue);
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareValuesForSave of BaseAppendCheckboxDialogController', ex);
        }
    },

    //refresh NotifDetailPage GridPanel, if there is a new notifItem in notifItemStore
    onClassValuesSaved: function (classValues) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_CLASSVALUE_SAVED];

            if (callbackInterface) {
                var values = null;
                var newValues = this.getViewModel().get('newValues');
                var classValue = this.getViewModel().get('charact').get('classValues')

                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(classValue) || classValue.data.items[0].get('atwrt') === '')
                    values = this.getViewModel().get('charact').set('classValues', newValues);
                else
                    values = classValue
                callbackInterface.call(this.getOwner(), values);
            }

            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassValuesSaved of BaseAppendCheckboxDialogController', ex)
        }
    },

    //apply the current filter criteria on the store, before it is transferred into the view
    beforeDataReady: function () {
        try {
            this.backUpClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseAppendCheckboxDialogController', ex);
        }
    },

    //add to newValues store the cloned classValues(the current view values)
    backUpClassValue: function () {
        try {
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var classValues = charact.get('classValues');
            if (classValues && classValues.getCount() > 0) {
                var newValues = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });
                classValues.each(function (classVal) {
                    var value = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classVal);
                    newValues.add(value);
                }, this);
            }

            myModel.set('newValues', newValues);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside backUpClassValue of BaseAppendCheckboxDialogController', ex);
        }
    }
});