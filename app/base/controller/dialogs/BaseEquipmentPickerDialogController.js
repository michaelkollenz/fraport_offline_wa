Ext.define('AssetManagement.base.controller.dialogs.BaseEquipmentPickerDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

    
	requires: [
	   'AssetManagement.customer.manager.EquipmentManager',
	   'AssetManagement.customer.utils.StringUtils',
	   'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
		contextReadyLevel: 1
	},
	
	inheritableStatics: {
		//interface
		ON_EQUI_SELECTED: 'onEquiSelected'
	},
	
	//@override
	startBuildingDialogContext: function(argumentsObject) {
		try {
			//extract superior func loc id, if passed
			var superiorTplnr = ''; 
			
			if(Ext.getClassName(argumentsObject['funcLoc']) === 'AssetManagement.customer.model.bo.FuncLoc') {
				superiorTplnr = argumentsObject['funcLoc'].get('tplnr');
			} else {
				superiorTplnr = argumentsObject['tplnr'];
			}

			var equipmentRequest;
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(superiorTplnr)) {
				equipmentRequest = AssetManagement.customer.manager.EquipmentManager.getEquipmentsForFuncLoc(superiorTplnr);
			} else {
				superiorTplnr = '';
				equipmentRequest = AssetManagement.customer.manager.EquipmentManager.getEquipments();
			}
			
			this.getViewModel().set('superiorTplnr', superiorTplnr);
				
			this.addRequestToDataBaseQueue(equipmentRequest, 'equipmentStore');
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseEquipmentPickerDialogController', ex)
		}
	},

	onEquiCellSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts){
		try {
			this.onEquiSelected(record);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiCellSelected of BaseEquipmentPickerDialogController', ex)
		}
	}, 

	onEquiSelected: function(equiInfoObject) {
		try {
			var callbackInterface = this.getOwner()[this.self.ON_EQUI_SELECTED];
               
			if(callbackInterface)
				callbackInterface.call(this.getOwner(), equiInfoObject);
			
			//dismiss
			this.dismiss();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseEquipmentPickerDialogController', ex)
		}
	},
	
	//@override
	getContextLoadingErrorMessage: function() {
		return "Data error while loading equipments.";
	}, 
	
	onEquiSearchButtonClick: function() {
		try {
			this.getView().getSearchValues();
			
			var equiToSearch = this.getViewModel().get('searchEqui'); 
			var equiStore = this.getViewModel().get('equipmentStore');
			
			equiStore.clearFilter(); 
			
			//filter equis
			if(equiToSearch.get('eqktx'))
				equiStore.filterBy(function (record) {
					try {
					    if (record.get('eqktx').toUpperCase().indexOf(equiToSearch.get('eqktx').toUpperCase()) > -1) {
					    	return true; 
					    }
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseEquipmentPickerDialogController', ex)
					}
				});
			if(equiToSearch.get('sernr'))
				equiStore.filterBy(function (record) {
					try {
					    if (record.get('sernr').toUpperCase().indexOf(equiToSearch.get('sernr').toUpperCase()) > -1) {
					    	return true; 
					    }
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseEquipmentPickerDialogController', ex)
					}
				});
			if(equiToSearch.get('matnr'))
				equiStore.filterBy(function (record) {
					try {
					    if (record.get('matnr').toUpperCase().indexOf(equiToSearch.get('matnr').toUpperCase()) > -1) {
					    	return true; 
					    }
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseEquipmentPickerDialogController', ex)
					}
				});
			
			if(equiToSearch.get('tplnr'))
				equiStore.filterBy(function (record) {
					try {
					    if (record.get('tplnr').toUpperCase().indexOf(equiToSearch.get('tplnr').toUpperCase()) > -1) {
					    	return true; 
					    }
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseEquipmentPickerDialogController', ex)
					}
				});
			 
			this.getViewModel().set('equipmentStore', equiStore);
			
			this.getView().refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSearchButtonClick of BaseEquipmentPickerDialogController', ex)
		}
	}
});