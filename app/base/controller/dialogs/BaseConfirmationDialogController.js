Ext.define('AssetManagement.base.controller.dialogs.BaseConfirmationDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	  'AssetManagement.customer.helper.OxLogger'
	],
	
	_callback: null,
	_callbackScope: null,

	//@override
	//protected
	transferDataFromConfigObjectToViewModel: function(argumentsObject) {
		try {
			if(argumentsObject) {
				if(argumentsObject.title) {
					this.getViewModel().set('title', argumentsObject.title);
				}
				
				if(argumentsObject.message) {
					this.getViewModel().set('message', argumentsObject.message);
				}
				
				if(argumentsObject.asHtml) {
					this.getViewModel().set('asHtml', argumentsObject.asHtml);
				}
			
				if(argumentsObject.icon) {
					this.getViewModel().set('icon', argumentsObject.icon);
				}

				if(argumentsObject.hideDeclineButton) {
					this.getViewModel().set('hideDeclineButton', argumentsObject.hideDeclineButton);
				}
				
				if(argumentsObject.alternateConfirmText) {
					this.getViewModel().set('alternateConfirmText', argumentsObject.alternateConfirmText);
				}
				
				if(argumentsObject.alternateDeclineText) {
					this.getViewModel().set('alternateDeclineText', argumentsObject.alternateDeclineText);
				}
				
				this._callback = argumentsObject.callback;
				this._callbackScope = argumentsObject.scope;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferDataFromConfigObjectToViewModel of BaseConfirmationDialogController', ex);
		}
	},
	
	//@override
	confirm: function() {
		try {
			this.dismiss();
		
			if(this._callback) {
				//if the callback contains direct navigation, this will crash the navigation state
				//to avoid this, defer the callback
			
				Ext.defer(this._callback, 100, this._callbackScope ? this._callbackScope : this, [ true ]);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside confirm of BaseConfirmationDialogController', ex);
		}
	},
	
	//@override
	cancel: function() {
		this.callParent();
	
		try {
			if(this._callback) {
				//if the callback contains direct navigation, this will crash the navigation state
				//to avoid this, defer the callback
			
				Ext.defer(this._callback, 100, this._callbackScope ? this._callbackScope : this, [ false ]);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseConfirmationDialogController', ex);
		}
	}
});