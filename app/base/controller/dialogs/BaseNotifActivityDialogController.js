Ext.define('AssetManagement.base.controller.dialogs.BaseNotifActivityDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.ArgumentsHelper',
	    'AssetManagement.customer.manager.NotifManager',
	    'AssetManagement.customer.manager.EquipmentManager',
	    'AssetManagement.customer.manager.FuncLocManager',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
	    'AssetManagement.customer.model.bo.Notif',
	    'AssetManagement.customer.model.bo.NotifItem',
	    'AssetManagement.customer.model.bo.NotifActivity',
	    'AssetManagement.customer.helper.OxLogger'
    ],
    
	config: {
		contextReadyLevel: 3
	},
	
	inheritableStatics: {
		//interface
		ON_ACTIVITY_SAVED: 'onNotifActivitySaved'
	},
	
	//private
	_saveRunning: false,

    //protected
    startBuildingDialogContext: function(argumentsObject) {
        try {
            //extract data from the arguments
            //invalid calls are:
            //no qmnum or notif
            //fenum but missing notif or qmnum
            var myModel = this.getViewModel();
            var qmnum = '', fenum = '', manum = '';

            //the notification is always required - the item is optional (create case)
            var loadNotifRequired = true;
            var loadNotifItemRequired = false;
            var loadNotifActivityRequired = false;

            if(typeof argumentsObject['qmnum'] === 'string') {
                qmnum = argumentsObject['qmnum'];
            } else if(Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
                myModel.set('notif', argumentsObject['notif']);
                qmnum = argumentsObject['notif'].get('qmnum');
                loadNotifRequired = false;
            }

            if (typeof argumentsObject['fenum'] === 'string' && argumentsObject['fenum'] !== '0000') {
                fenum = argumentsObject['fenum'];
                loadNotifItemRequired = true;
            } else if (Ext.getClassName(argumentsObject['notifItem']) === 'AssetManagement.customer.model.bo.NotifItem') {
                myModel.set('notifItem', argumentsObject['notifItem']);
                fenum = argumentsObject['notifItem'].get('fenum');

                if (loadNotifRequired)
                    qmnum = argumentsObject['notifItem'].get('qmnum');
            } else {
                fenum = '0000';
            }

            if (typeof argumentsObject['manum'] === 'string') {
                manum = argumentsObject['manum'];
                loadNotifActivityRequired = true;
            } else if (Ext.getClassName(argumentsObject['notifActivity']) === 'AssetManagement.customer.model.bo.NotifActivity') {
                myModel.set('notifActivity', argumentsObject['notifActivity']);

                if (loadNotifRequired)
                    qmnum = argumentsObject['notifActivity'].get('qmnum');
                if (argumentsObject['notifActivity'].get('fenum') !== '0000' && !argumentsObject['notifItem']) {
                    fenum = argumentsObject['fenum'];
                    loadNotifItemRequired = true;
                }
            }

            if (loadNotifRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)
                || loadNotifItemRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)
                || loadNotifActivityRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(manum)) {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.NotifActivityDialogController'
                });
            }

            if (loadNotifRequired) {
                this.loadNotif(qmnum);
            }

            if (loadNotifItemRequired) {
                this.loadNotifItem(qmnum, fenum);
            }

            if (loadNotifActivityRequired) {
                this.loadNotifActivity(qmnum, fenum, manum);
            }

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this.errorOccurred();
        }
    },

    //public
    onSaveButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.trySaveNotifActivity();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //@override
    onCancelButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotif: function (qmnum) {
        try {
            var notifRequest = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnum, true);
            this.addRequestToDataBaseQueue(notifRequest, 'notif');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotifItem: function (qmnum, fenum) {
        try {
            var notifItemRequest = AssetManagement.customer.manager.NotifItemManager.getNotifItem(qmnum, fenum, true);
            this.addRequestToDataBaseQueue(notifItemRequest, 'notifItem');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotifActivity: function (qmnum, fenum, manum) {
        try {
            var notifActivityRequest = AssetManagement.customer.manager.NotifManager.getNotifActivity(qmnum,fenum,manum,true);
            this.addRequestToDataBaseQueue(notifActivityRequest, 'notifActivity');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //checks if the data of the notifItem object is complete
    //returns a boolean, if the checks have been passed successfully
    //if any check fails it will also trigger a corresponding error message
    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var currentInputValues = this.getView().getCurrentInputValues();

            if (!currentInputValues.activityCustCode) {
                errorMessage = Locale.getMsg('provideActivityCode');
                retval = false;
            }

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }

                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    trySaveNotifActivity: function() {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;

            var saveAction = function(checkWasSuccessfull) {
                try {
                    if (checkWasSuccessfull === true) {
                        me.prepareNotifActivityForSave();

                        var notifActivity = this.getViewModel().get('notifActivity');

                        var saveCallback = function(success) {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

                            try {
                                if(success === true) {
                                    me.onNotifActivitySaved(notifActivity);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifActivitySaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifActivity'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifActivity of NotifActivityDialogController', ex);
                            } finally {
                                me._saveRunning = false;
                            }
                        };

                        var eventId = AssetManagement.customer.manager.NotifActivityManager.saveNotifActivity(notifActivity);
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifActivity of NotifActivityDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            //check if data is complete first
            this.checkInputValues(saveAction, this);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },
    //protected
    //@override
    afterDataBaseQueueComplete: function () {
        try {
            if (this._contextLevel === 1) {
                this.performContextLevel1Requests();
            }
            else if (this._contextLevel === 2) {
                this.performContextLevel2Requests();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    performContextLevel1Requests: function () {
        try {
            this.getRbnrConnectedObjects();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //determine correct rbnr
    getRbnrConnectedObjects: function () {
        try {
            var myModel = this.getViewModel();
            var notif = myModel.get('notif');

            var equi = notif.get('equipment');
            var equnr = notif.get('equnr');

            if (equi) {
                myModel.set('equipment', equi);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
                this.addRequestToDataBaseQueue(equiRequest, 'equipment');
            }

            var funcLoc = notif.get('funcLoc');
            var tplnr = notif.get('tplnr');

            if (funcLoc) {
                myModel.set('funcLoc', funcLoc);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
                this.addRequestToDataBaseQueue(funcLocRequest, 'funcLoc');
            }

            var notifType = notif.get('notifType');
            var qmart = notif.get('qmart');

            if (notifType) {
                myModel.set('notifType', notifType);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
                var notifTypeRequest = AssetManagement.customer.manager.CustNotifTypeManager.getNotifType(qmart, true);
                this.addRequestToDataBaseQueue(notifTypeRequest, 'notifType');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    performContextLevel2Requests: function () {
        try {
            this.getCodes();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    getCodes: function () {
        try {
            var myModel = this.getViewModel();
            var notifType = myModel.get('notifType');

            var rbnr = AssetManagement.customer.manager.NotifManager.getRbnrForNotif(myModel.get('notif'), myModel.get('equipment'), myModel.get('funcLoc'));

            var activityCodeGroupsRequest = AssetManagement.customer.manager.CustCodeManager.getCustCodesGroups(rbnr, notifType.get('makat'));
            this.addRequestToDataBaseQueue(activityCodeGroupsRequest, "activityCodeGroupsMap");
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    onActivityGrpSelected: function (combo, records, eOpts) {
        try {
            this.getView().fillActivityCodeCombobox();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    onNotifActivitySaved: function(notifActivityObject) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_ACTIVITY_SAVED];

            if(callbackInterface)
                callbackInterface.call(this.getOwner(), notifActivityObject);

            //dismiss
            this.dismiss();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    prepareNotifActivityForSave: function () {
        try {
            var currentInputValues = this.getView().getCurrentInputValues();
            var myModel = this.getViewModel();
            var notifActivity = myModel.get('notifActivity');

            var notif = myModel.get('notif');

            var notifItem = myModel.get('notifItem');

            if (!notifActivity) {
                notifActivity = Ext.create('AssetManagement.customer.model.bo.NotifActivity', {
                    notif: myModel.get('notif'),
                    notifItem: myModel.get('notifItem')
                });
            }

            notifActivity.set('matxt', currentInputValues.shorttext);

            var activityCustCode = currentInputValues.activityCustCode ? currentInputValues.activityCustCode : null;
            notifActivity.set('mnkat', activityCustCode ? activityCustCode.get('katalogart') : '');
            notifActivity.set('mngrp', activityCustCode ? activityCustCode.get('codegruppe') : '');
            notifActivity.set('mncod', activityCustCode ? activityCustCode.get('code') : '');
            notifActivity.set('activityCustCode', activityCustCode);
            notifActivity.set('mobileKey', notif ? notif.get('mobileKey'): '');

            if(notifItem) {
                notifActivity.set('childKey', notifItem.get('childKey'));

            }

            myModel.set('notifActivity', notifActivity);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    }
});