﻿Ext.define('AssetManagement.base.controller.dialogs.BaseClassificationEditDialogController', {
   extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


   requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.manager.ClassificationManager',
        //'AssetManagement.customer.controller.ToolbarController',
        //		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
        'Ext.data.Store',
        'Ext.Date',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Charact',
        'AssetManagement.customer.model.bo.ObjClass',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.controller.EventController'
        //'AssetManagement.customer.controller.NavigationController'
    ],

   config: {
    contextReadyLevel: 1
   },
   inheritableStatics: {
       //interface
       ON_CLASSVALUE_SAVED: 'onClassValueSaved'
   },


   //private
   _saveRunning: false,

   //protected
   //@Override
   startBuildingDialogContext: function (argumentsObject) {
       try {
           if (Ext.getClassName(argumentsObject['charact']) !== 'AssetManagement.customer.model.bo.Charact' &&
               Ext.getClassName(argumentsObject['objClass']) !== 'AssetManagement.customer.model.bo.ObjClass')
           {
               throw Ext.create('AssetManagement.customer.utils.OxException', {
                   message: 'Insufficient parameters',
                   method: 'startBuildingPageContext',
                   clazz: 'AssetManagement.customer.controller.dialogs.ClassificationEditDialogController'
               });
           }

           if (Ext.getClassName(argumentsObject['charact']) === 'AssetManagement.customer.model.bo.Charact')
               this.getViewModel().set('charact', argumentsObject['charact']);

           if (Ext.getClassName(argumentsObject['objClass']) === 'AssetManagement.customer.model.bo.ObjClass')
               this.getViewModel().set('objClass', argumentsObject['objClass']);

           this.waitForDataBaseQueue();
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseClassificationEditDialogController', ex);
           this.errorOccurred();
       }
   },

   //@override
   save: function () {
       try {
           if (!this._saveRunning)
               this.trySaveClassValue();
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseClassificationEditDialogController', ex)
       }
   },

   dismiss: function () {
       this.callParent();

       try {
           //this.unregisterListenersFromWorker();
           //if (this.getInfinite())
           //    this.getView().stopProgressBar();
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseClassificationEditDialogController', ex);
       }
   },

   //@override
   cancel: function () {
       try {
           if (!this._saveRunning)
               this.dismiss();
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseClassificationEditDialogController', ex)
       }
   },

   trySaveClassValue: function () {
       try {
           this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

           var me = this;

           var saveItem = function(checkWasSuccessful) {
               try {
                   if (checkWasSuccessful === true) {
                       var callback = function (success) {
                           me.prepareValuesForSave();
                           try {
                               if (success === true) {
                                   me.onClassValueSaved();

                                   me.dismiss();
                                   Ext.defer(function() {
                                       me._saveRunning = false;
                                   }, 2000, me);

                                   var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                   AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                               }else {
                                   me._saveRunning = false;
                                   AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                   var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                                   AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                               }
                           } catch (ex) {
                               me._saveRunning = false;
                               AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                               AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of ClassificationEditDialofController', ex);
                           }
                           finally {
                               me._saveRunning = false;
                               AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                           }
                       };

                       var classValues = this.getViewModel().get('charact').get('classValues');

                       var eventId = AssetManagement.customer.manager.ClassificationManager.saveClassValue(classValues);

                       if (eventId > 0) {
                           AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                       }else {
                           me._saveRunning = false;
                           AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                           var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                           AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                       }
                   }else {
                       me._saveRunning = false;
                       AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                   }
               } catch (ex) {
                   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseClassificationEditDialogController', ex);
                   me._saveRunning = false;
                   AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
               }
           };

           this.checkInputValues(saveItem, this);
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveClassValue of BaseClassificationEditDialogController', ex);
           me._saveRunning = false;
           AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
       }
   },

   prepareValuesForSave: function () {
       var retval = null;
       try {
           var myModel = this.getViewModel();
           var objClass = myModel.get('objClass');
           var charact = myModel.get('charact');

           if (charact === null || charact === undefined)
              return;

           var charactList = charact.get('charactValues');
           var classValues = charact.get('classValues');

           var newValues = Ext.create('Ext.data.Store', {
               model: 'AssetManagement.customer.model.bo.ObjClassValue',
               autoLoad: false
           });

           var delValues = Ext.create('Ext.data.Store', {
               model: 'AssetManagement.customer.model.bo.ObjClassValue',
               autoLoad: false
           });

           if ((charactList && charactList.getCount() > 0) || (charact && charact.get('atein') !== "X")) {
               //    // MEHRWERTIG

                Ext.getCmp('itemContainer').items.each(function (checkboxItem) {
                    var classValue = null;

                    if (checkboxItem.checked === true) {
                        var matchesFound = Ext.create('Ext.data.Store', {
                                                            model: 'AssetManagement.customer.model.bo.ObjClassValue',
                                                            autoLoad: false
                                                        });

                        // filter classValues
                        classValues.each(function (clValues) {
                            if (clValues.get('atwrt') === checkboxItem.atwrt && clValues.get('atflv') === checkboxItem.atflv)
                                matchesFound.add(clValues);

                        });
                        if (matchesFound.getCount() > 0) {

                            if (matchesFound.data.items[0].get('updFlag') === 'D')
                                matchesFound.data.items[0].set('updFlag', 'X');

                            classValue = matchesFound.data.items[0];
                        } else {
                            classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass, checkboxItem.atwrt, checkboxItem.atflv);
                            if(charact.get('classValues'))
                                charact.get('classValues').add(classValue);
                            else
                                charact.set('classValues', classValue);
                        }
                    } else {
                        var matchesFound = Ext.create('Ext.data.Store', {
                            model: 'AssetManagement.customer.model.bo.ObjClassValue',
                            autoLoad: false
                        });

                        var textInput = '';
                        if (checkboxItem.usesTextInput === true) {
                            textInput = checkboxItem.lastValue;
                        }

                        if (classValues !== undefined)
                        {
                            classValues.each(function (clValues) {
                                if (clValues.get('atwrt') === checkboxItem.atwrt && clValues.get('atflv') === checkboxItem.atflv)
                                    matchesFound.add(clValues);
                            });
                        }

                        if (matchesFound.getCount() > 0) {
                            if (matchesFound.data.items[0].get('updFlag') === 'I') {
                                delValues.add(matchesFound.data.items[0]);
                            }
                            else {
                                matchesFound.data.items[0].set('updFlag','D');
                                classValue = matchesFound.data.items[0];
                            }
                        }
                        else {
                            classValue = AssetManagement.customer.manager.ClassificationManager.CreateNewClassValue(charact, objClass, textInput, checkboxItem.atflv);
                        }
                    }

                    if (classValue !== null) {
                        classValue.set('clint', objClass.get('clint'));
                        classValue.set('atzhl', checkboxItem.atzhl);
                        classValue.set('mobileKey', objClass.get('mobileKey'));
                        newValues.add(classValue);

                    }
                });

             //singular value
           } else {
               var items = Ext.getCmp('itemContainerSingular').query();

               // Single TextField value
               if (items && items.length === 1 && Ext.getClassName(items[0]) === 'AssetManagement.customer.view.CharactValueTextFieldItem') {
                   var item = items[0];
                   if (charact && charact.get('atfor') === 'CHAR') {
                       var atwrt = item.getValue();
                       var classValue = item._charact.get('classValues').data.items[0];
                       classValue.set('mobileKey', objClass.get('mobileKey'));
                       classValue.set('clint', objClass.get('clint'));

                       if (atwrt !== null) {      // Wenn leer, dann löschen
                           if (classValue.get('updFlag') === "I") {
                               AssetManagement.customer.manager.ClassificationManager.deleteClass(classValue);
                               classValue.set('updFlag', 'U');
                           } else {
                               classValue.set('updFlag', 'D');

                           }
                           classValue.set('atwrt', atwrt);

                       } else {
                           if (!charact.get('atfor') === 'CHAR') {
                               if (charact.get('atfor') === 'DATE' || charact.get('atfor') === 'TIME') {

                                   classValue.set('atflv', atwrt);
                               } else {
                                   classValue.set('atwrt', atwrt);
                               }

                               if (classValue.get('updFlag') === "D")
                                   classValue.set('updFlag', 'U');

                           }
                       }

                   }
                   // DATE
               } else if (items && items.length === 1 && Ext.getClassName(items[0]) === 'AssetManagement.customer.view.CharactValueDateItem') {
                   var item = items[0];

                   if (charact && charact.get('atfor') === 'DATE') {

                       var classValue = item._charact.get('classValues').data.items[0];
                       classValue.set('mobileKey', objClass.get('mobileKey'));
                       classValue.set('clint', objClass.get('clint'));

                       // Ist ein Datum ausgewählt?
                       if (item.getValue() !== null) {
                           if (classValue.get('updFlag') === "D" || classValue.get('updFlag') === "X")
                               classValue.set('updFlag', 'U');
                           classValue.set('atflv', AssetManagement.customer.utils.DateTimeUtils.getDateString(item.getValue(), false));
                           classValue.set('atflv', Ext.Date.format(item.getValue(), 'Ymd'));
                       }
                       else {
                           if (classValue.get('updFlag') === "I") {
                               AssetManagement.customer.manager.ClassificationManager.deleteClass(classValue);
                               classValue.set('updFlag', 'U');
                           } else {
                               classValue.set('updFlag', 'D');

                           }
                       }
                   }
                   // TIME
               } else if (items && items.length === 1 && Ext.getClassName(items[0]) === 'AssetManagement.customer.view.CharactValueTimeFieldItem') {
                   var item = items[0];
                   if (charact && charact.get('atfor') === 'TIME') {
                       var atflv = item.getValue();
                       var classValue = item._charact.get('classValues').data.items[0];
                       classValue.set('mobileKey', objClass.get('mobileKey'));
                       classValue.set('clint', objClass.get('clint'));

                       // Ist ein Datum ausgewählt?
                       if (atflv !== null) {

                           if (classValue.get('updFlag') === "D")
                               classValue.set('updFlag', 'U');

                           classValue.set('atflv', AssetManagement.customer.utils.DateTimeUtils.getTimeString(atflv,false));
                       }
                       else {
                           if (classValue.get('updFlag') === "I") {
                               AssetManagement.customer.manager.ClassificationManager.deleteClassValue(classValue);
                               classValue.set('updFlag', 'U');
                           } else {
                               classValue.set('updFlag', 'D');

                           }
                       }
                   } else if (charact && charact.get('atfor') === 'NUM' || charact && charact.get('atfor') === 'CURR') {

                       var atflv = item.getValue();
                       var classValue = item._charact.get('classValues').data.items[0];
                       classValue.set('mobileKey', objClass.get('mobileKey'));
                       classValue.set('clint', objClass.get('clint'));

                       // Ist ein Datum ausgewählt?
                       if (atflv !== null) {

                           if (classValue.get('updFlag') === "D")
                               classValue.set('updFlag', 'U');

                           classValue.set('atflv', AssetManagement.customer.utils.NumberFormatUtils.cutOffDecimals(atflv));
                       }
                       else {
                           if (classValue.get('updFlag') === "I") {
                               AssetManagement.customer.manager.ClassificationManager.deleteClassValue(classValue);
                               classValue.set('updFlag', 'U');
                           } else {
                               classValue.set('updFlag', 'D');

                           }
                       }
                   }
               }

           }

       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveClassValueCriteria of BaseClassificationEditDialogController', ex);
       }

       return retval;
   },

   checkInputValues: function(callback,callbackScope){
       try{
           var retval = true;
           var errorMessage = '';


           var values = this.getView().getCurrentInputValues();


           var atflv = values.atflv;
           var atwrt = values.atwrt;

           var returnFunction = function(){
               if(retval === false){
                   var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage,false,2);
                   AssetManagement.customer.core.Core.getMainView().showNotification(message);
               }
               callback.call(callbackScope? callbackScope:me,retval);
           };

           returnFunction.call(this);
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseClassificationEditDialogController', ex);

           if (callback) {
               callback.call(callbackScope ? callbackScope : this, false);
           }
       }
   },

    //checks, if there are any differences between the current input state and the model state
   isInputEqualToModel: function () {
       var retval = true;

       try {
           var inputValues = this.getView().getCurrentInputValues();
           var funcLoc = this.getViewModel().get('objClass');
           var charact = this.getViewModel().get('charact');

           if (funcLoc.get('objnr') !== inputValues.objnr) {
               retval = false;
           } else if (!AssetManagement.customer.utils.DateTimeUtils.areEqual(funcLoc.get('atflv'), inputValues.atflv)) {
               retval = false;
           } else if (funcLoc.get('atwrt') !== inputValues.atwrt) {
               retval = false;
           } else if (funcLoc.get('atinn') !== inputValues.atinn) {
               retval = false;
           } else if (funcLoc.get('atflv') !== inputValues.atflv) {
               retval = false;
           }
       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isInputEqualToModel of BaseClassificationEditDialogController', ex);
       }

       return retval;
   },

    //refresh NotifDetailPage GridPanel, if there is a new notifItem in notifItemStore
   onClassValueSaved: function () {
       try {
           var callbackInterface = this.getOwner()[this.self.ON_CLASSVALUE_SAVED];

           if (callbackInterface)
               callbackInterface.call(this.getOwner());


       } catch (ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSaved of BaseClassificationEditDialogController', ex)
       }
   },

   deleteClassValue: function(classValues) {
            try {
                var currentClassValue = this.getViewModel().get('charact');
                var currentObjClass = this.getViewModel().get('objClass');

                var charactList = currentClassValue.get('charactValues');


                var deletingCurrentEditingClassValue = classValues.get('atflv') === currentClassValue.get('atflv') && classValues.get('atwrt') === currentClassValue.get('atwrt');

                var me = this;
                var callback = function(success) {
                    try {
                        if(success === true) {
                            if (deletingCurrentEditingClassValue)
                                me.initializeNewClass();

                            me.deleteClassValueFromListStore(classValues);

                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('extBanfDeleted'), true, 0);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        } else {
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingExtBanf'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } catch(ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClassValue of BaseClassificationEditDialogController', ex);
                    }
                };

                var eventId = AssetManagement.customer.manager.ClassificationManager.deleteClass(classValues,false);
                if(eventId > 0) {
                    AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                } else {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingExtBanf'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClassValue of BaseClassificationEditDialogController', ex);
            }
    },

   deleteClassValueFromListStore: function (objClass) {
        try {
            var temp = this.getViewModel().get('objClass').getById(objClass.get('id'));

            if (temp)
                this.getViewModel().get('objClass').remove(objClass);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteClassValueFromListStore of BaseClassificationEditDialogController', ex);
        }
   },

    //apply the current filter criteria on the store, before it is transferred into the view
    beforeDataReady: function () {
        try {
            this.backUpClassValue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseClassificationEditDialogController', ex);
        }
    },

    //add to newValues store the cloned classValues(the current view values)
    backUpClassValue: function () {
        try{
            var myModel = this.getViewModel();
            var charact = myModel.get('charact');
            var classValues = charact.get('classValues');

            if (classValues && classValues.getCount() > 0) {
                var newValues = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.ObjClassValue',
                    autoLoad: false
                });
                classValues.each(function (classVal) {
                    var value = AssetManagement.customer.manager.ClassificationManager.cloneClassValue(classVal);
                    newValues.add(value);
                }, this);
            }

            myModel.set('newValues', newValues);

        }catch(ex){
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside backUpClassValue of BaseClassificationEditDialogController', ex);
        }
    }

});