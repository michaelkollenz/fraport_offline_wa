Ext.define('AssetManagement.base.controller.dialogs.BaseObjectListEditDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.ArgumentsHelper',
	    'AssetManagement.customer.manager.ObjectListManager',
	    'AssetManagement.customer.model.bo.Notif',
	    'AssetManagement.customer.model.bo.ObjectListItem',
	    'AssetManagement.customer.helper.OxLogger'
    ],
    
    
	config: {
		contextReadyLevel: 1
	},
	
	inheritableStatics: {
		//interface
		ON_OBJECTLISTITEM_SAVED: 'onObjectListItemSaved'
	},
	
	//private
	_saveRunning: false,

	//protected
	startBuildingDialogContext: function(argumentsObject) {
	    try {
            var qmnum = '';
			if(Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
				this.getViewModel().set('notif', argumentsObject['notif']);
			} 
			
			if(Ext.getClassName(argumentsObject['objectListItem']) === 'AssetManagement.customer.model.bo.ObjectListItem') {
			    this.getViewModel().set('objectListItem', argumentsObject['objectListItem']);
                qmnum = this.getViewModel().get('objectListItem').get('ihnum');
			} 
			
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum))
                this.getNotificationForObjectListItem(qmnum);
            else if (!this.getViewModel().get('notif'))
            {
              throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.dialogs.ObjectListEditDialogController'
				});
            }

            this.waitForDataBaseQueue();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseObjectListEditDialogController', ex);
            this.errorOccurred();
		}
			
	},
	
    /*
     * Loads the notification for the qmnum
     */
	getNotificationForObjectListItem: function(qmnum) {
		try {
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum)) {
				var notifRequest = AssetManagement.customer.manager.NotifManager.getNotif(qmnum, true);
				this.addRequestToDataBaseQueue(notifRequest, 'notif');
			} 
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside passedStringsParametersToBOs of NotifCauseDialogController', ex);
		}
	},
		
	//@override
	dataBaseQueueComplete: function() {
	    try {
		    this.callParent();

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of NotifCauseDialogController', ex);
		}
	},
	
	//@override
	save: function() {
		try {
			if(!this._saveRunning)
				this.trySaveObjectListItem();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside save of BaseObjectListEditDialogController', ex)
		}
	},
	
	//@override
	cancel: function() {
		try {
			if(!this._saveRunning)
				this.dismiss();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseObjectListEditDialogController', ex)
		}
	},
	
    /*
     * changes the data in the notif and the object list to the data on the view
     * The maktx needs to be loaded in the manager prior to saving the object list item to the database
     */
	trySaveObjectListItem: function() {
		try {
			this._saveRunning = true;	
		
			var me = this;
			this.getView().getObjectListItemValues(); 
	
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
						
						var saveCallback = function(success) {
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						
							try {
								if(success === true) {
									me.onObjectListItemSaved(me.getViewModel().get('objectListItem'));
							
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('objectListChanged'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
									//AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: me.getViewModel().get('objectListItem').get('aufnr') }, true);
								} else {
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingObjectListChanges'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveObjectListeItem of BaseObjectListEditDialogController', ex);
							} finally {
								me._saveRunning = false;
							}
						};
						
						var eventId = AssetManagement.customer.manager.ObjectListManager.saveObjectListChanges(this.getViewModel().get('notif'), this.getViewModel().get('objectListItem'));
						AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveObjectListItem of BaseObjectListEditDialogController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveObjectListItem of BaseObjectListEditDialogController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	/**
	 * checks if the data of the object list item is complete 
	 * returns null if the data is complete or an error message if data is missing
	 */
	checkInputValues: function(callback, callbackScope)  {
		try {
			
			var retval = true; 
			var errorMessage = ''; 
		
			var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
		
			var notif = this.getViewModel().get('notif'); 
	
			if(isNullOrEmpty(notif.get('sernr')))
			{
				errorMessage = Locale.getMsg('provideSerialNr');
				retval = false;
			}
			else if(isNullOrEmpty(notif.get('matnr')))
			{
				errorMessage = Locale.getMsg('provideMaterial');
				retval = false;
			}
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			returnFunction.call(this);
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseObjectListEditDialogController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},

	onObjectListItemSaved: function(objectListItem) {
		try {
			var callbackInterface = this.getOwner()[this.self.ON_OBJECTLISTITEM_SAVED];
			
			if(callbackInterface)
				callbackInterface.call(this.getOwner(), objectListItem);
			
			//dismiss
			this.dismiss(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onObjectListItemSaved of BaseObjectListEditDialogController', ex);
		}
	},

	/**
	 * Event fired when equi search button is clicked
	 */
	onSelectEquiClick: function() {
	    try {
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.EQUIPMENTPICKER_DIALOG, { tplnr: this.getViewModel().get('objectListItem').get('tplnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectEquiClick of BaseObjectListEditDialogController', ex);
		}
	},
    
	/**
	 * Event fired when equi has been selected in dialog
	 */
	onEquiSelected: function(record) {
	    try {
			var myModel = this.getViewModel();
			var objListItem = myModel.get('objectListItem');
			var notif = myModel.get('notif');

			objListItem.set('sernr', record.get('sernr'));
			objListItem.set('matnr', record.get('matnr'));
			objListItem.set('equnr', record.get('equnr'));
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif)) {
                notif.set('sernr', record.get('sernr'));
                notif.set('matnr', record.get('matnr'));
                notif.set('equnr', record.get('equnr'));
            }
            this.getView().updateDialogContent();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseObjectListEditDialogController', ex);
		}
	}
});