Ext.define('AssetManagement.base.controller.dialogs.BaseOnlineBoCheckDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',
    alias: 'controller.BaseOnlineBoCheckDialogController',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.helper.NetworkHelper',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.sync.SyncManager',
        'AssetManagement.customer.sync.ParsingManager',
        'AssetManagement.customer.model.bo.PoItem'
    ],

    config: {
        cancelable: false,
        infinite: true
    },

  inheritableStatics: {
        REQUEST_DATATYPE: 'BUSOBJECT_CHECK_JSON',
        REQUEST_DATATYPE_READ: 'BUSOBJECT_READ_JSON'
    },

    _currentRequestCriteria : null,
    _syncManager: null,

    _busobject: null,
    _busobjectKey: null,
    _read: null,
    _selectedLgort: null,

    //protected
    //@override
    beforeInitialization: function(argumentsObject) {
        try {
            this._busobject = argumentsObject['busobject'];
            this._busobjectKey = argumentsObject['busobjectKey'];
            this._read = argumentsObject['readObject'];

            var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
            this._selectedLgort = selectedLgort;

            var syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
            this._uploadManager = Ext.create('AssetManagement.customer.sync.UploadManager', {
                dataBaseHelper: null,
                linkedSyncManager: syncManager,
                listeners: {
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccurred: { fn: this.errorOccurred, scope: this }
                }
            });

            this._parsingManager = Ext.create('AssetManagement.customer.sync.ParsingManager', {
                dataBaseHelper: AssetManagement.customer.core.Core.getDataBaseHelper(),
                linkedSyncManager: syncManager,
                listeners: {
                    progressChanged: { fn: this.innerProgressReporter, scope: this },
                    cancelationInProgress: { fn: this.onCancelationRequestConfirmed, scope: this },
                    workCancelled: { fn: this.workCancelled, scope: this },
                    errorOccurred: { fn: this.errorOccurred, scope: this }
                }
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseOnlineBoCheckDialogController', ex);
        }
    },

    //protected
    //@override
    //prevent starting the process on show
    onStartProcess: function () {
        var retval = false;

        try {
            retval = true;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStartProcess of BaseOnlineBoCheckDialogController', ex);
            this.reportError();
        }

        return retval;
    },

    //protected
    //@override
    doWork: function () {
        try {
            this.prepareOnlineOrderCreationRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doWork of BaseOnlineBoCheckDialogController', ex);
            this.errorOccurred();
        }
    },

    prepareOnlineOrderCreationRequest: function() {
        try {
            //check if client is online
            var me = this;

            var onlineCallback = function(success) {
                if(success) {
                    //get the upstream for the selected assignments
                    var upstream = me.getUploadStreamForDownloadAssingments();

                    //if there is no upstream, stop with an error
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(upstream)) {
                        me.reportError();
                        return;
                    }
                    //do the actual online call next
                    me.callOnlineRequestAPI(upstream);
                }else{
                    var errorMessage = Locale.getMsg('browserIsOffline');
                    me.reportError(errorMessage);
                    return;
                }
            };
            AssetManagement.customer.helper.NetworkHelper.isClientOnline(onlineCallback);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareOnlineOrderCreationRequest of BaseOnlineBoCheckDialogController', ex);
            this.reportError();
        }
    },

    callOnlineRequestAPI: function(upstream) {
        try {
            var me = this;
            var postValues = this.getRequestPostValues(upstream);

            if(!postValues) {
                me.errorOccurred();
                return;
            }

            var callback = function (jsonResponse) {
                try {
                    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(jsonResponse)) {
                        me._syncManager = AssetManagement.customer.sync.SyncManager.getInstance();
                        me._timeStampDown = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
                        var jsonResponseObject = Ext.decode(jsonResponse);
                        var onlineOrderCreationResponse = jsonResponseObject ? jsonResponseObject['ONLINE_REQUEST_RESULT'] : undefined;

                        if(onlineOrderCreationResponse) {
                            if(onlineOrderCreationResponse.RESULT_CODE !== '0'){
                                me.onWorkFinished(false, onlineOrderCreationResponse.RESULT_MESSAGE);

                            }else {
                                if (!me._read) {
                                    me.onWorkFinished(true);
                                } else if (me._busobject === 'BUS1001') {
                                    me.buildMaterialResponseMessagesStore(onlineOrderCreationResponse.ANY_DATA);
                                }else if(onlineOrderCreationResponse.ANY_DATA && onlineOrderCreationResponse.ANY_DATA.RETURN_ITEMS){
                                  me.buildGoodsMovementResponseMessagesStore(onlineOrderCreationResponse.ANY_DATA.RETURN_ITEMS);
                                }else{
                                  me.onWorkFinished(true);
                                }
                            }
                        } else {
                            me.reportError();
                        }
                    } else{
                        me.reportError();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineBoCheckDialogController', ex);
                    me.reportError();
                }
            };

            var errorCallback = function (failureTypeOrErrorMessage) {
                try {
                    var message = '';

                    switch(failureTypeOrErrorMessage) {
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.OFFLINE:
                            message = Locale.getMsg('browserIsOffline');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.TIMEOUT:
                            message = Locale.getMsg('connectionTimeoutOccured');
                            break;
                        case AssetManagement.customer.sync.ConnectionManager.FAILURE_TYPES.UNKNOWN:
                            message = Locale.getMsg('mymUnknownError');
                            break;
                    }

                    me.reportError(message);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineBoCheckDialogController', ex);
                    me.reportError();
                }
            };

            var connManager = Ext.create('AssetManagement.customer.sync.ConnectionManager', {
                listeners: {
                    sendFailed: { fn: errorCallback, scope: this },
                    errorOccurred: { fn: errorCallback, scope: this }
                }
            });

            connManager.sendToBackEnd(AssetManagement.customer.sync.ConnectionManager.REQUEST_TYPES.ONLINEREQ, postValues, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPostingOnlineAPI of BaseOnlineBoCheckDialogController', ex);
            this.reportError()
        }
    },

    buildMaterialResponseMessagesStore: function (materialResponseObject) {
        try {
            if (this._cancelWorkRequested || this._errorOccurred) {
                this.cancelWork();
                return;
            }
            var ls_mara = materialResponseObject.LS_MARA;
            var unit = ls_mara ? ls_mara.MEINS : null;
            var matnr = ls_mara ? ls_mara.MATNR : null;
            var druck = ls_mara ? ls_mara.KZUMW : null;
            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
            var lt_maktx = materialResponseObject.LT_MAKT;
            var materialKText = null;
            if(lt_maktx && lt_maktx.length > 0){
                Ext.Array.each(lt_maktx, function (maktx) {
                    if(maktx.SPRAS === userInfo.get('sprache')){
                        materialKText = maktx.MAKTX;
                        return false;
                    }
                });
            }
          var lt_marc = materialResponseObject.LT_MARC;
          if(lt_marc && lt_marc.length > 0){
            Ext.Array.each(lt_marc, function (marc) {
              if(!marc.AUSME == ''){
                unit = marc.AUSME;
                return false;
              }
            });
          }
            this.onWorkFinished(true, matnr, unit, materialKText, druck);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred loadStockDataFromDatabase buildMaterialResponseMessagesStore of BaseOnlineBoCheckDialogController', ex);
            this.reportError();
        }
    },



  buildGoodsMovementResponseMessagesStore: function (retunItemsObject) {
    try {
      if (this._cancelWorkRequested || this._errorOccurred) {
        this.cancelWork();
        return;
      }

      var returnMessagesStore = Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.GoodsMovementItem',
        autoLoad: false
      });

      if(retunItemsObject && retunItemsObject.length > 0) {
        var msgArrayLength = retunItemsObject.length;
        for(var i = 0 ; i < msgArrayLength; i++) {
          var returnMessage = Ext.create('AssetManagement.customer.model.bo.GoodsMovementItem', {
            matnr: retunItemsObject[i]['MATNR'],
            werks: retunItemsObject[i]['WERKS'],
            lgort: retunItemsObject[i]['LGORT'],
            bwart: retunItemsObject[i]['BWART'],
            charg: retunItemsObject[i]['CHARG'],
            menge: retunItemsObject[i]['MENGE'],
            meins: retunItemsObject[i]['MEINS'],
            ebeln: retunItemsObject[i]['EBELN'],
            ebelp: retunItemsObject[i]['EBELP'],
            wempf: retunItemsObject[i]['WEMPF'],
            kdauf: retunItemsObject[i]['KDAUF'],
            ablad: retunItemsObject[i]['ABLAD'],
            kostl: retunItemsObject[i]['KOSTL'],
            aufnr: retunItemsObject[i]['AUFNR'],
            aufps: retunItemsObject[i]['AUFPS'],
            rsnum: retunItemsObject[i]['RSNUM'],
            rspos: retunItemsObject[i]['RSPOS'],
            ps_posid: retunItemsObject[i]['PS_POSID'],
            matkt: retunItemsObject[i]['ITEM_TEXT'],
            kzbew: retunItemsObject[i]['KZBEW'],
            lifnr: retunItemsObject[i]['LIFNR'],
            prefilled: true
          });

          returnMessagesStore.add(returnMessage);
        }
      }
      this.onWorkFinished(true, returnMessagesStore);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred loadStockDataFromDatabase buildMaterialResponseMessagesStore of BaseOnlineBoCheckDialogController', ex);
      this.reportError();
    }
  },


  //private
    //previous step: postVanChange
    //next step: examineResponse
    getUploadStreamForDownloadAssingments: function () {
        var retval = '';

        try {
            var busobjectKey = null;

            if(this._busobject === 'BUS0012'){

                if (this._busobjectKey) {
                    for (var i = this._busobjectKey.length; i < 10; i++) {
                        this._busobjectKey = "0" + this._busobjectKey;
                    }
                }
                else{
                    this._busobject = '';
                }
                busobjectKey =  this._selectedLgort.get('kokrs') + this._busobjectKey

            }else if(this._busobject === 'MI_LGORT'){
              busobjectKey =  this._selectedLgort.get('werks')+ '-' + this._busobjectKey

            }else{
                busobjectKey =  this._busobjectKey

            }
            var order = {
                "REQUEST" : {
                    "BUSOBJECT" : this._busobject,
                    "BUSOBJKEY" : busobjectKey
                }
            };

          var orderAsJSON = JSON.stringify(order);
            return orderAsJSON;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getUploadStreamForAssignments of BaseOnlineBoCheckDialogController', ex);
        }
        return retval;
    },

    //private
    //builds the post values for the online request
    getRequestPostValues: function (upstream) {
        var retval = null;

        try {
            var nameValuePairList = Ext.create('Ext.util.MixedCollection');
            var ac = AssetManagement.customer.core.Core.getAppConfig();
            nameValuePairList.add("sap-client", ac.getMandt());
            nameValuePairList.add("sap-user", ac.getUserId());
            nameValuePairList.add("sap-password", ac.getUserPassword());
            nameValuePairList.add("UPTIMESTAMP", AssetManagement.customer.utils.DateTimeUtils.formatTimeForDb(AssetManagement.customer.utils.DateTimeUtils.getCurrentTime()));
            nameValuePairList.add("DEVICE_ID", ac.getDeviceId());
            //set datatype for online api
            if(this._read){
                nameValuePairList.add("DATATYPE", this.self.REQUEST_DATATYPE_READ);

            }else{
                nameValuePairList.add("DATATYPE", this.self.REQUEST_DATATYPE);
            }

            //set upstream
            nameValuePairList.add("UPSTREAM", upstream);
            retval = nameValuePairList;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRequestPostValues of BaseOnlineBoCheckDialogController', ex);
        }
        return retval;
    },

    //protected
    //@override
    getGeneralProcessErrorMessage: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('generalErrorOccurredDuringDownloadingAssignments');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralProcessErrorMessage of BaseOnlineBoCheckDialogController', ex);
        }
        return retval;
    },

    //private
    reportWorkProgress: function (curProcessPercent, message) {
        try {
            this._curProcessPercent = curProcessPercent;

            this.reportProgress(curProcessPercent, message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportWorkProgress of BaseOnlineBoCheckDialogController', ex);
            this.errorOccurred(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    errorOccurred: function (message) {
        try {
            this._processRunning = false;
            this._waitingForNetwork = false;
            this._waitingForParsing = false;
            this._cancelWorkRequested = false;
            this._errorOccurred = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = Locale.getMsg('newQuotationGeneralProcessingError');

            this.reportError(message);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOcurred of BaseOnlineBoCheckDialogController', ex);
        }
    },

    //private
    cancelWork: function () {
        try {
            this._processRunning = false;
            this._cancelWorkRequested = false;
            this.onWorkerCancelled();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelWork of BaseOnlineBoCheckDialogController', ex);
            this.errorOccurred(Locale.getMsg('newQuotationGeneralProcessingError'));
        }
    },

    //private
    innerProgressReporter: function (percentage, message) {
        try {
            var additionalPercentage = percentage - this._curProcessStepPercentage;
            this._curProcessStepPercentage = percentage;
            this.reportWorkProgress(this._curProcessPercent + this._curProcessStepRange * additionalPercentage / 100, message);
        } catch (ex) {
            this.errorOccurred();
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside innerProgressReporter of BaseOnlineBoCheckDialogController', ex);
        }
    }
});