Ext.define('AssetManagement.base.controller.dialogs.BaseCustomerPickerDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


    requires: [
	    'AssetManagement.customer.manager.CustomerManager',
	    'AssetManagement.customer.helper.OxLogger'
    ],

    config: {
        contextReadyLevel: 1
    },

    inheritableStatics: {
        //interface
        ON_CUSTOMER_SELECTED: 'onCustomerSelected'
    },

    //@override
    startBuildingDialogContext: function (argumentsObject) {
        try {
            this.performContextLevel1Requests();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingDialogContext of BaseCustomerPickerDialogController', ex)
        }
    },

    performContextLevel1Requests: function () {
        try {
            //load customers
            var customers = AssetManagement.customer.manager.CustomerManager.getCustomers(true);
            this.addRequestToDataBaseQueue(customers, "customers");

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseCustomerPickerDialogController', ex)
        }
    },

    onCustomerCellSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            this.onCustomerSelected(record);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerCellSelected of BaseCustomerPickerDialogController', ex)
        }
    },

    onCustomerSelected: function (customerInfoObject) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_CUSTOMER_SELECTED];

            if (callbackInterface)
                callbackInterface.call(this.getOwner(), customerInfoObject);

            //dismiss
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerSelected of BaseCustomerPickerDialogController', ex)
        }
    },

    //@override
    getContextLoadingErrorMessage: function () {
        return "Data error while loading customers.";
    },

    onCustomerSearchButtonClick: function () {
        try {
            this.getView().getSearchValues();

            var customerToSearch = this.getViewModel().get('searchCustomer');
            var customerStore = this.getViewModel().get('customers');

            customerStore.clearFilter();

            //filter equis
            if (customerToSearch.get('lastname'))
                customerStore.filterBy(function (record) {
                    try {
                        if (record.get('lastname').toUpperCase().indexOf(customerToSearch.get('lastname').toUpperCase()) > -1) {
                            return true;
                        }
                        if (record.get('firstname').toUpperCase().indexOf(customerToSearch.get('lastname').toUpperCase()) > -1) {
                            return true;
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseCustomerPickerDialogController', ex)
                    }
                });
            if (customerToSearch.get('postlcod1'))
                customerStore.filterBy(function (record) {
                    try {
                        if (record.get('postlcod1').toUpperCase().indexOf(customerToSearch.get('postlcod1').toUpperCase()) > -1) {
                            return true;
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseCustomerPickerDialogController', ex)
                    }
                });
            if (customerToSearch.get('city'))
                customerStore.filterBy(function (record) {
                    try {
                        if (record.get('city').toUpperCase().indexOf(customerToSearch.get('city').toUpperCase()) > -1) {
                            return true;
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseCustomerPickerDialogController', ex)
                    }
                });

            this.getViewModel().set('customers', customerStore);

            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSearchButtonClick of BaseCustomerPickerDialogController', ex)
        }
    }
});