Ext.define('AssetManagement.base.controller.dialogs.BaseOxDialogController', {
	extend: 'AssetManagement.customer.controller.DataBaseToContextController',

	requires: [
	  'AssetManagement.customer.helper.OxLogger'
	],
    
    //abstract class for dialog controllers
    //one primary task is managing the dialog context to load, before show

	//private
    config: {
		contextReadyLevel: 0,
		owner: null,
		currentArguments: null,
		dialogRequestCallback: null,
		dialogRequestCallbackScope: null,
		onHiddenCallback: null,
		hasUnsavedChanges: false
	},
	
	//protected
	getClassName: function() {
    	var retval = 'OxDialogController';
    
    	try {
    		var className = Ext.getClassName(this);
    		
    		if(AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
    			var splitted = className.split('.');
    			retval = splitted[splitted.length - 1];
    		} else {
    			retval = className;
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseOxDialogController', ex);
    	}
    	
    	return retval;
	},
	
	//public
	cancel: function() {
		try {
		    this.dismiss();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of ' + this.getClassName(), ex);
		}    
	},
	
	//public
	save: function() {
	},
	
	//public
	dismiss: function() {
		try {
			this._owner = null;
			this.getView().hide();
			
			if(this._onHiddenCallback)
				this._onHiddenCallback();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of ' + this.getClassName(), ex);
		}
	},
	
	//public
	//may be overridden by derivates
	confirm: function() {
	},
	
	//public
	requestDialog: function(argumentsObject, dialogRequestCallback, dialogRequestCallbackScope, onHiddenCallback) {
		try {
			this._dialogRequestCallback = dialogRequestCallback;
			this._dialogRequestCallbackScope = dialogRequestCallbackScope;
			this._currentArguments = argumentsObject;
			this._onHiddenCallback = onHiddenCallback;
			
			this.getView().resetViewState();
			
			//do not use update to avoid it's defering
			this.refreshData(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestDialog of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},
	
	//API to use, if a dialog has to be cancelled from outside
	//the returned value corresponds, if the cancellation was accepted and will be performed in the next javascript idle phase
	requestCancellation: function() {
		var canCancelNow = false;
	
		try {
			var canCancelNow = this.onCancellationRequest();
			
			if(canCancelNow === true) {
				//cancel the dialog, after the return signal has been proccessed by caller
				Ext.defer(this.cancel, 1, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestCancellation of ' + this.getClassName(), ex);
		}
		
		return canCancelNow;
	},
	
	//public
	//checks if the there is unsaved data or input for the dialog
	hasUnsavedChanges: function() {
	    return this._hasUnsavedChanges;
	},
	
	//may be overriden by derivates
	getDataLoadingErrorMessage: function() {
		var retval = '';
		
		try {
		    retval = this.getContextLoadingErrorMessage();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataLoadingErrorMessage of ' + this.getClassName(), ex);
		}
		
		return retval;
	},
	
	//protected
	update: function(doRefreshViewOnly) {
		try {
			AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
		
			if(doRefreshViewOnly === true) {
				Ext.defer(this.refreshView, 100, this);
			} else {
				Ext.defer(this.refreshData, 100, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside update of ' + this.getClassName(), ex);
		}
	},
	
	//protected
	//to override, if a dialog may not be closed, because of inconsistent data state
	onCancellationRequest: function() {
		return true;
	},
	
	//private
	//will reload all data
	refreshData: function(triggerDialogRequestCallbackWhenDone) {
		try {
			if(!triggerDialogRequestCallbackWhenDone) {
				this._dialogRequestCallback = null;
				this._dialogRequestCallbackScope = null;
			}
			
			this.getViewModel().resetData();
			
			//set to zero
			this._contextLevel = 0;
			
			if(this._contextReadyLevel === 0) {
				if(this.getCurrentArguments())
					this.transferDataFromConfigObjectToViewModel(this.getCurrentArguments());
					
				this.contextReady();
			} else {
				//start the loading cycle
				this.startBuildingDialogContext(this.getCurrentArguments());
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
		}
	},
	
	//protected
	//must be implemented by derivates, if context ready level is higher than 0
	startBuildingDialogContext: function(argumentsObject) {
	},
	
	//protected
	//may be implemented if context ready level is 0 and parameters are passed
	transferDataFromConfigObjectToViewModel: function(argumentsObject) {
	},
	
	//protected
	//may be overridden by derivates
	//default behavior is to rebuild the view, so that label contents will be set corresponding to the current locale 
	onLocaleChanged: function() {
		try {
			this.getView().rebuildDialog();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLocaleChanged of ' + this.getClassName(), ex);
		}
	},
	
	//private
	contextReady: function() {
		try {
			this.dataReady();
			this.refreshView();
			this.dialogReady();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside contextReady of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},
	
	//protected
	dataBaseQueueComplete: function() {
		this.callParent();
		
		try {
			if(this._contextLevel === this._contextReadyLevel || this.getViewModel().getInitialized() === true) {
				this._contextLevel = this._contextReadyLevel;
				this.getViewModel().setInitialized(true);
				
				this.afterDataBaseQueueComplete();

				this.contextReady();
			} else if(this._contextLevel === -1) {
				this.errorOccurred();
			} else {
				this.afterDataBaseQueueComplete();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},
	
	//may be implemented by derivates
	checkBaseContext: function() {
		return true;
	},
	
	//protected
	//may be implemented by derivates
	//use this callback to drop database requests of the current context level
	afterDataBaseQueueComplete: function() {
	},
	
	//private
	dataReady: function() {
		try {
			this.beforeDataReady();
			
			this.onDataReady();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataReady of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},
	
	//protected
	//may be implemented by derivates
	//can be used to do data initialization, which depends on loaded data
	beforeDataReady: function() {
	},
	
	//protected
	//may be used by derivates
	onDataReady: function() {
	},
	
	//private
	refreshView: function() {
		try {
			this.beforeRefreshView();
			
			//suspend layouts before refreshing ui components - this gains a lot of performance
			Ext.suspendLayouts();
		
			this.getView().refreshView();
			
			this.afterRefreshView();
			
			//resume layouts - all changes will be rendered in one operation
			Ext.resumeLayouts(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshView of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},
	
	//protected
	//may be used by derivates
	beforeRefreshView: function() {
	},
	
	//protected
	//may be used by derivates
	afterRefreshView: function() {
	},
	
	//private
	dialogReady: function() {
	    try {
		    this.onDialogReady();
		    
		    if(this._dialogRequestCallback) {
		    	this.callDialogRequestCallback(true);
		    } else {
		    	AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		    }
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dialogReady of ' + this.getClassName(), ex);
		}    
	},
	
	//protected
	//may be used by derivates
	onDialogReady: function() {
	},
	
	//private
	callDialogRequestCallback: function(requestSuccessFullyProcessed) {
	    try {
		    if(this._dialogRequestCallback) {
			   this._dialogRequestCallback.call(this._dialogRequestCallbackScope ? this._dialogRequestCallbackScope : this, requestSuccessFullyProcessed);
		    }
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callDialogRequestCallback of ' + this.getClassName(), ex);
		}    
	},
	
	//private
	errorOccurred: function() {
	    try {
	    	this.callDialogRequestCallback(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccurred of ' + this.getClassName(), ex);
		}    
	},
	
	removeHiddenCallBack: function () {
        try {
            this._onHiddenCallback = null;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccurred of ' + this.getClassName(), ex);
		} 
	}
});