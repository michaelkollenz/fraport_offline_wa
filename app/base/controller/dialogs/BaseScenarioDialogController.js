Ext.define('AssetManagement.base.controller.dialogs.BaseScenarioDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',

	
	requires: [
	  'AssetManagement.customer.helper.OxLogger'
	],

	_callback: null,
    _callbackScope: null,
	_cancelcallback: null,

	//@override
	//protected
	transferDataFromConfigObjectToViewModel: function(argumentsObject) {
	    try {
	        this._callback = null;
	        this._callbackScope = null;
			this._cancelcallback = null;		

	        if(argumentsObject) {
	            if (argumentsObject.scenarios) {
	                var scenarios = this.createScenariosStore(argumentsObject.scenarios);
	                this.getViewModel().set('scenarios', scenarios);
				}
				   
				if (argumentsObject.callback){
			    	this._callback = argumentsObject.callback;
				}

				if (argumentsObject.callbackScope){
 					this._callbackScope = argumentsObject.callbackScope;
				}
	
				if(argumentsObject.cancelcallback)
				{
					this._cancelcallback = argumentsObject.cancelcallback;
				}
	        }
	        else {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.dialogs.ScenarioDialogController'
			    })
	        }
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferDataFromConfigObjectToViewModel of BaseScenarioDialogController', ex);
		    this.errorOccurred();
		}
	},

    //private
	createScenariosStore: function (scenarios) {
	    var retval = null;

	    try {
	        var scenariosStore = Ext.create('Ext.data.Store', {
	            model: 'AssetManagement.customer.model.bo.Scenario'
	        });

	        for (var i = 0; i < scenarios.length; i++) {
	            var myScenario = Ext.create('AssetManagement.customer.model.bo.Scenario', {
	                mandt: scenarios[i].MANDT,
	                scenario : scenarios[i].SCENARIO,
	                userId: scenarios[i].MOBILEUSER,
	                default_scenario: scenarios[i].DEFAULT_SCENARIO,
                    description: scenarios[i].DESCRIPTION
	            });

	            scenariosStore.add(myScenario);
	        }

	        retval = scenariosStore;
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createScenariosStore of BaseScenarioDialogController', ex);
	    }

	    return retval;
	},

    //public
	onItemSelected: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
	    try {
	        var ac = AssetManagement.customer.core.Core.getAppConfig();
	        var scenario = record.get('scenario');

	        if (this._callback)
	            this._callback.call(this._callbackScope, scenario);
	        
	        this.dismiss();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onItemSelected of BaseScenarioDialogController', ex);
	    }
	},

    //public
	cancel: function () {
	    try {
	        if (this._cancelcallback)
	            this._cancelcallback.call(this._callbackScope, '');

	        this.dismiss();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseScenarioDialogController', ex);
	    }
	}
});