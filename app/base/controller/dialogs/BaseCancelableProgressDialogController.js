Ext.define('AssetManagement.base.controller.dialogs.BaseCancelableProgressDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',
    
    requires: [
        'AssetManagement.customer.model.helper.ReturnMessage',
        'AssetManagement.customer.helper.OxLogger'
    ],
    
    config: {
        infinite: false,
        cancelable: true,
        successCallback: null,
        cancelCallback: null,
        owner: null,
        worker: null,
        startMethod: null,
        arguments: null,
        differingEntryEntity: null
    },
    
    _overAllCancelable: false,
    _registeredCallbacksOnWorker: false,
    _cancellationInProgress: false,
    _cancellationRequested: false,
    _errorOnWorker: false,
    _workFinished: false,
    
    //public
    isOverallCancelable: function() {
        var retval = false;
    
        try {
            retval = this._overAllCancelable;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isOverallCancelable of BaseCancelableProgressDialogController', ex);
        }
        
        return retval;
    },
    
    //public
    //@override
    dismiss: function() {
        this.callParent();
    
        try {
            this.unregisterListenersFromWorker();

            if (this.getInfinite())
                this.getView().stopProgressBar();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dismiss of BaseCancelableProgressDialogController', ex);
        }
    },

    //@override
    //protected
    transferDataFromConfigObjectToViewModel: function(argumentsObject) {
        try {
            if(!argumentsObject)
                argumentsObject = { };
            
            this.beforeInitialization(argumentsObject);
            
            if (argumentsObject) {
                this._worker = argumentsObject.worker ? argumentsObject.worker : this;
                this._startMethod = argumentsObject.startMethod ? argumentsObject.startMethod : this.doWork;
                this._arguments = argumentsObject.arguments;
                this._differingEntryEntity = argumentsObject.differingEntryEntity;
                this._owner = argumentsObject.owner;
                this._overAllCancelable = this._cancelable && argumentsObject.cancelable !== false;
                this._successCallback = argumentsObject.successCallback;
                this._cancelCallback = argumentsObject.cancelCallback;
                
                var myModel = this.getViewModel();
                
                if(argumentsObject.title) {
                    myModel.set('title', argumentsObject.title);
                }

                myModel.set('currentlyCancelable', this._overAllCancelable);
                myModel.set('message', argumentsObject.initialMessage ? argumentsObject.initialMessage : (Locale.getMsg('startingProcess') + '...'));
                
                this.afterInitialization();
                
                //start the process, when the applications goes inside idle mode
                Ext.defer(this.startProcess, 100, this);
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferDataFromConfigObjectToViewModel of BaseCancelableProgressDialogController', ex);
        }
    },
    
    //protected
    //may be overriden by derivates to manipulate the progress dialogs configuration
    beforeInitialization: function(argumentsObject) {
    },
    
    //protected
    //may be overriden by derivates
    afterInitialization: function() {
    },

    //private
    startProcess: function () {
        try {
            if (this.onStartProcess() === false)
                return;

            this._cancellationInProgress = false;
            this._cancellationRequested = false;
            this._errorOnWorker = false;
            this._workFinished = false;

            this.registerListenersOnWorker();

            if (this.getInfinite())
                this.getView().startProgressBar();

            var scopeToStartWith = this._differingEntryEntity ? this._differingEntryEntity : this._worker;

            Ext.defer(this._startMethod, 0, scopeToStartWith, this._arguments);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startProcess of BaseCancelableProgressDialogController', ex);
        }
    },
    
    //protected
    //may be overriden by derivates to cancel or postpone the start of the process
    //return false to stop the process start
    onStartProcess: function() {
        return true;
    },

    //protected
    //override this method in subclasses, if the dialog controller may contain the action to perform itself
    //this method will only be called, if no worker and startMethod has been provided via arguments
    doWork: function () {
        try {
            this.onWorkFinished();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside doWork of BaseCancelableProgressDialogController', ex);
        }
    },
    
    //@override
    //protected
    onCancellationRequest: function() {
        try {
            var notification = null;    
        
            if(this._overAllCancelable === false || this.getViewModel().get('currentlyCancelable') === false) {
                notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('currentProcessCanNotBeCancelled'), true, 1);
            } else {
                this.cancelButtonClicked();
                
                notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('currentProcessWillBeCancelled'), true, 1);
            }
            
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancellationRequest of BaseCancelableProgressDialogController', ex);
        }
        
        return false;
    },
    
    //protected
    requestCancelation: function() {
        try {
            if(!this._cancellationRequested) {
                this._worker.requestCancelation();
            }
        
            this._cancellationRequested = true;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestCancelation of BaseCancelableProgressDialogController', ex);
        }
    },
    
    //BEGIN worker callbacks
    reportProgress: function(percentage, message) {
        try {
            var myModel = this.getViewModel();
            
            if(percentage)
                myModel.set('percentage', percentage);
            
            if(message)
                myModel.set('message', message);
        
            this.refreshView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportProgress of BaseCancelableProgressDialogController', ex);
        }
    },
    
    reportCancelationRequest: function() {
        try {
            this._cancellationInProgress = true;
            
            var myModel = this.getViewModel();

            myModel.set('message', Locale.getMsg('cancellationInProgress'));
            myModel.set('currentlyCancelable', false);
            
            this.getView().refreshView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportCancelationRequest of BaseCancelableProgressDialogController', ex);
        }
    },
    
    cancelableChanged: function(cancelable) {
        try {
            if(cancelable !== true)
                cancelable = false;
        
            this.getViewModel().set('currentlyCancelable', cancelable);
            
            this.getView().refreshView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelableChanged of BaseCancelableProgressDialogController', ex);
        }
    },

    reportError: function(message) {
        try {
            this._errorOnWorker = true;

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message))
                message = this.getGeneralProcessErrorMessage();

            var errorMessage = Locale.getMsg('errorOccuredOnProcess');

            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(message)) {
                errorMessage = errorMessage + ': ' + message;
            } else {
                errorMessage += '.';
            }

            this.getViewModel().set('message', errorMessage);

            if(this._overAllCancelable === true) {
                this.getViewModel().set('currentlyCancelable', true);
            } else {
                var func = function(message) {
                    this.cancel(message);
                };

                Ext.defer(func, 2000, this, [message]);
            }

            if (this.getInfinite())
                this.getView().stopProgressBar();

            this.getView().refreshView();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reportError of BaseCancelableProgressDialogController', ex);
        }
    },

    //protected
    //this method can be overriden to place a general error message to use for the hosted process
    //it will be used, whenever reportError is called without a message
    getGeneralProcessErrorMessage: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('aGeneralErrorOccured');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getGeneralProcessErrorMessage of BaseCancelableProgressDialogController', ex);
        }

        return retval;
    },
    
    onWorkerCancelled: function() {
        try {
            //if an error occured before the workers cancellation do NOT trigger the cancel function
            if(this._errorOnWorker === false) {
                this.cancel();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onWorkerCancelled of CancelableProgressDialog', ex);
        }
    },
    
    onWorkFinished: function() {
        try {
            this._workFinished = true;
            
            this.getViewModel().set('currentlyCancelable', false);
            this.getView().refreshView();
        
            var args = arguments;

            var func = function() {
                this.dismiss();
                
                if (this._successCallback) {
                    //use defer to call it correctly with arguments and scope
                    Ext.defer(this._successCallback, 0, this._owner, args);
                }
            };
            
            Ext.defer(func, 300, this);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onWorkFinished of CancelableProgressDialog', ex);
        }
    },
    //END worker callbacks
    
    cancelButtonClicked: function() {
        try {
            if(this.getViewModel().get('currentlyCancelable') === true) {
                if(!this._errorOnWorker && !this._workFinished) {
                    this.requestCancelation();
                    
                    this.getViewModel().set('currentlyCancelable', false);
                    this.getView().refreshView();
                } else {
                    this.cancel();
                }
            }   
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelButtonClicked of BaseCancelableProgressDialogController', ex);
        }
    },
    
    //@override
    //protected
    cancel: function(message) {
        try {
            this.dismiss();
            
            if(this._cancelCallback)
                this._cancelCallback.call(this._owner, message);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancel of BaseCancelableProgressDialogController', ex);
        }
    },
    
    registerListenersOnWorker: function() {
        try {
            if(this._registeredCallbacksOnWorker === false && this._worker !== this) {
                this._worker.addListener('progressChanged', this.reportProgress, this);
                this._worker.addListener('workFinished', this.onWorkFinished, this);
                this._worker.addListener('cancelationInProgress', this.reportCancelationRequest, this);
                this._worker.addListener('cancelled', this.onWorkerCancelled, this);
                this._worker.addListener('errorOccured', this.reportError, this);
                this._worker.addListener('cancelableChanged', this.cancelableChanged, this);
                
                this._registeredCallbacksOnWorker = true;
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerListenersOnWorker of BaseCancelableProgressDialogController', ex);
        }
    },
    
    unregisterListenersFromWorker: function() {
        try {
            if(this._registeredCallbacksOnWorker === true) {
                this._worker.removeListener('progressChanged', this.reportProgress, this);
                this._worker.removeListener('workFinished', this.onWorkFinished, this);
                this._worker.removeListener('cancelationInProgress', this.reportCancelationRequest, this);
                this._worker.removeListener('cancelled', this.onWorkerCancelled, this);
                this._worker.removeListener('errorOccured', this.reportError, this);
                this._worker.removeListener('cancelableChanged', this.cancelableChanged, this);
                
                this._registeredCallbacksOnWorker = false;
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unregisterListenersFromWorker of BaseCancelableProgressDialogController', ex);
        }
    }
});