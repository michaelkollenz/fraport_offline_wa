Ext.define('AssetManagement.base.controller.dialogs.BaseSendLogFilesDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.CancelableProgressDialogController',

	
	requires: [
	  'AssetManagement.customer.sync.TraceManager',
	  'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
	    cancelable: true,
	    infinite: true
	},

    //enum for state control of dialog
	DIALOG_MODES: {
	    SELECTION: 0,
	    SENDING: 1
	},

    //public
	cancelButtonClicked: function () {
	    try {
	        var mode = this.getViewModel().get('mode');
	        if (mode === this.DIALOG_MODES.SENDING) {
	            this.callParent();
	        } else {
	            this.dismiss();
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside cancelButtonClicked of BaseSendLogFilesDialogController', ex);
	    }
	},

    //public
	sendButtonClicked: function () {
	    try {
	        this.getView().transferViewValuesIntoModel();
	        
	        if (this.checkCurrentInputValues()) {
	            var myModel = this.getViewModel();

	            var errorDateTime = myModel.get('errorDateTime');
	            var errorRemarks = myModel.get('errorRemarks');
	            var includeDatabase = myModel.get('includeDatabase');

	            myModel.set('mode', this.DIALOG_MODES.SENDING);
	            myModel.set('title', Locale.getMsg('sendingLogFiles'));
	            myModel.set('message', '');

	            this.refreshView();
	            this.startProcess();
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSendLogFilesButtonClicked of BaseSendLogFilesDialogController', ex);
	    }
	},

    //public
	includeDatabaseCheckBoxChanged: function (checkbox, newValue, oldValue, eOpts) {
        try {
	        if (newValue === true) {
	            var alertDialogConfig = {
	                message: Locale.getMsg('sendLogFilesIncDBMessage')
	            };

	            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ALERT_DIALOG, alertDialogConfig);
	        }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside includeDatabaseCheckBoxChanged of BaseSendLogFilesDialogController', ex);
        }
	},

    //protected
    //@override
	beforeInitialization: function (argumentsObject) {
	    try {
	        //set the default parameters on the progress dialog for sending logfiles process		
	        argumentsObject.title = Locale.getMsg('sendLogFiles');
	        argumentsObject.worker = AssetManagement.customer.sync.TraceManager.getInstance();
	        argumentsObject.startMethod = this.onStartWorker;
	        //setting reference to this controller for "onStartWorker" method
	        argumentsObject.differingEntryEntity = this;
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeInitialization of BaseSendLogFilesDialogController', ex);
	    }
	},

    //protected
    //@override
    //prevent starting the process on show
	onStartProcess: function () {
	    var retval = false;

	    try {
	        retval = this.getViewModel().get('mode') === this.DIALOG_MODES.SENDING;
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStartProcess of BaseSendLogFilesDialogController', ex);
	        this.reportError();
	    }

	    return retval;
	},

    //protected
    //@override
	beforeDataReady: function () {
	    try {
	        var myModel = this.getViewModel();

            //set the error date time to the current time as default
	        myModel.set('errorDateTime', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseSendLogFilesDialogController', ex);
	    }
	},

    //private
    //checks if date and time are present
	checkCurrentInputValues: function () {
	    var retval = true;

	    try {
	        var myModel = this.getViewModel();
	        var errorDateTime = myModel.get('errorDateTime');

	        if (!errorDateTime) {
	            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('pleaseProvideDateTime'), false, 2);
	            AssetManagement.customer.core.Core.getMainView().showNotification(notification);

	            retval = false;
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkCurrentInputValues of BaseSendLogFilesDialogController', ex);
	    }

	    return retval;
	},

    //private
    //@override
	onStartWorker: function () {
	    try{
	        var traceManager = AssetManagement.customer.sync.TraceManager.getInstance();
	        var myModel = this.getViewModel();

	        var argumentsObjectForTraceManager = {
	            errorRemarks: myModel.get('errorRemarks'),
	            errorDateTime: myModel.get('errorDateTime'),
	            includeDatabaseContent: myModel.get('includeDatabase')
	        };

	        traceManager.sendLogFiles(argumentsObjectForTraceManager);
	        this.refreshView();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStartWorker of BaseSendLogFilesDialogController', ex);
	        this.errorOccured();
	    }
	}
});