﻿Ext.define('AssetManagement.base.controller.dialogs.BaseOnlineMaterialSearchDialogController', {
    extend: 'AssetManagement.customer.controller.dialogs.OnlineSearchDialogController',

    requires: [
      'AssetManagement.customer.model.bo.Material',
      'AssetManagement.customer.manager.MaterialManager'
    ],

    //@override
    //protected
    //returns the request data typ to use
    //this controls, which kind of data is searched for
    getRequestDataType: function () {
        return 'MATERIAL_SEARCH';
    },

    //@override
    //protected
    //returns the default name for the conversion API called during the response conversion on class returned by getObjectManagerClass
    getConversionAPIName: function () {
        return 'buildMaterialFromDbResultObject';
    },

    //@override
    //protected
    //returns the manager class administrating the corresponding SAP object on the client
    getObjectManagerClass: function () {
        return AssetManagement.customer.manager.MaterialManager;
    },

    //@override
    //protected
    //returns the model class representing the corresponding SAP object on the client
    getObjectModelClassName: function () {
        return 'AssetManagement.customer.model.bo.Material';
    },

    //@override
    //protected
    //returns the search criteria definitions as array of objects structures like:
    getSearchCriterionDefinitions: function () {
        var retval = null;

        try {
            var criteriaArray = [
                {
                    LABEL: 'shorttext',
                    ATTR: 'MAKTX',
                    MAX_LENGTH: '40',
                    DEF_OPER: 'LIKE'
                },
                {
                    LABEL: 'materialNumberShort',
                    ATTR: 'MATNR',
                    MAX_LENGTH: '18',
                    DEF_OPER: 'LIKE'
                },
                {
                    LABEL: 'catalogNumber',
                    ATTR: 'BISMT',
                    MAX_LENGTH: '81',
                    DEF_OPER: 'LIKE'
                },
                {
                    LABEL: 'productHierarchy',
                    ATTR: 'PRDHA',
                    MAX_LENGTH: '18',
                    DEF_OPER: 'LIKE'
                }
            ];

            retval = criteriaArray;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    },

    //@override
    //protected
    //returns the default title to be shown for the dialog
    getDialogTitle: function () {
        var retval = '';

        try {
            retval = Locale.getMsg('onlineMaterialSearch');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }

        return retval;
    }
});