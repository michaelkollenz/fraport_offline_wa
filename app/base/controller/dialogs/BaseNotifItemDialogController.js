Ext.define('AssetManagement.base.controller.dialogs.BaseNotifItemDialogController', {
	extend: 'AssetManagement.customer.controller.dialogs.OxDialogController',


	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.utils.ArgumentsHelper',
	    'AssetManagement.customer.manager.NotifManager',
	    'AssetManagement.customer.manager.NotifItemManager',
	    'AssetManagement.customer.manager.CustNotifTypeManager',
	    'AssetManagement.customer.manager.EquipmentManager',
	    'AssetManagement.customer.manager.CustCodeManager',
	    'AssetManagement.customer.manager.FuncLocManager',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
	    'AssetManagement.customer.model.bo.Notif',
	    'AssetManagement.customer.model.bo.NotifItem',
	    'AssetManagement.customer.model.bo.CustCode',
	    'Ext.data.Store',
	    'AssetManagement.customer.helper.OxLogger'
    ],
    
	config: {
		contextReadyLevel: 3
	},
	
	inheritableStatics: {
		//interface
		ON_ITEM_SAVED: 'onNotifItemSaved'
	},
	
	//private
	_saveRunning: false,

    //public
    onObjGrpSelected: function (combo, records, eOpts) {
        try {
            this.getView().fillObjectCodeComboBox();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    onDamageGrpSelected: function (combo, records, eOpts) {
        try {
            this.getView().fillDamageCodeComboBox();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    onSaveButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.trySaveNotifItem();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //public
    //@override
    onCancelButtonClicked: function () {
        try {
            if (!this._saveRunning)
                this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    startBuildingDialogContext: function(argumentsObject) {
        try {
            //extract data from the arguments
            //invalid calls are:
            //no qmnum or notif
            //fenum but missing notif or qmnum
            var myModel = this.getViewModel();
            var qmnum = '', fenum = '';

            //the notification is always required - the item is optional (create case)
            var loadNotifRequired = true;
            var loadNotifItemRequired = false;

            if(typeof argumentsObject['qmnum'] === 'string') {
                qmnum = argumentsObject['qmnum'];
            } else if(Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
                myModel.set('notif', argumentsObject['notif']);
                qmnum = argumentsObject['notif'].get('qmnum');
                loadNotifRequired = false;
            }

            if(typeof argumentsObject['fenum'] === 'string') {
                fenum = argumentsObject['fenum'];
                loadNotifItemRequired = true;
            } else if(Ext.getClassName(argumentsObject['notifItem']) === 'AssetManagement.customer.model.bo.NotifItem') {
                myModel.set('notifItem', argumentsObject['notifItem']);

                if (loadNotifRequired)
                    qmnum = argumentsObject['qmnum'];
            }

            if (loadNotifRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmnum) || loadNotifItemRequired && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(fenum)) {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.dialogs.NotifItemDialogController'
                });
            }

            if (loadNotifRequired) {
                this.loadNotif(qmnum);
            }

            if (loadNotifItemRequired) {
                this.loadNotifItem(qmnum, fenum);
            }

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotif: function (qmnum) {
        try {
            var notifRequest = AssetManagement.customer.manager.NotifManager.getNotifLightVersion(qmnum, true);
            this.addRequestToDataBaseQueue(notifRequest, 'notif');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    loadNotifItem: function (qmnum, fenum) {
        try {
            var notifItemRequest = AssetManagement.customer.manager.NotifManager.getNotifItem(qmnum, fenum, true);
            this.addRequestToDataBaseQueue(notifItemRequest, 'notifItem');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //protected
    //@override
    afterDataBaseQueueComplete: function () {
        try {
            if (this._contextLevel === 1) {
                this.performContextLevel1Requests();
            }
            else if (this._contextLevel === 2) {
                this.performContextLevel2Requests();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    performContextLevel1Requests: function () {
        try {
            this.getRbnrConnectedObjects();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    //determine correct rbnr
    getRbnrConnectedObjects: function () {
        try {
            var myModel = this.getViewModel();
            var notif = myModel.get('notif');

            var equi = notif.get('equipment');
            var equnr = notif.get('equnr');

            if (equi) {
                myModel.set('equipment', equi);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
                this.addRequestToDataBaseQueue(equiRequest, 'equipment');
            }

            var funcLoc = notif.get('funcLoc');
            var tplnr = notif.get('tplnr');

            if (funcLoc) {
                myModel.set('funcLoc', funcLoc);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
                this.addRequestToDataBaseQueue(funcLocRequest, 'funcLoc');
            }

            var notifType = notif.get('notifType');
            var qmart = notif.get('qmart');

            if (notifType) {
                myModel.set('notifType', notifType);
            } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qmart)) {
                var notifTypeRequest = AssetManagement.customer.manager.CustNotifTypeManager.getNotifType(qmart, true);
                this.addRequestToDataBaseQueue(notifTypeRequest, 'notifType');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    performContextLevel2Requests: function () {
        try {
            this.getCodes();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    getCodes: function () {
        try {
            var myModel = this.getViewModel();
            var notifType = myModel.get('notifType');

            var rbnr = AssetManagement.customer.manager.NotifManager.getRbnrForNotif(myModel.get('notif'), myModel.get('equipment'), myModel.get('funcLoc'));

            var damageCodeGroupsRequest = AssetManagement.customer.manager.CustCodeManager.getCustCodesGroups(rbnr, notifType.get('fekat'));
            this.addRequestToDataBaseQueue(damageCodeGroupsRequest, "damageCodeGroupsMap");

            var objectCodeGroupsRequest = AssetManagement.customer.manager.CustCodeManager.getCustCodesGroups(rbnr, notifType.get('otkat'));
            this.addRequestToDataBaseQueue(objectCodeGroupsRequest, "objectCodeGroupsMap");
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    trySaveNotifItem: function() {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var me = this;

            var saveAction = function(checkWasSuccessfull) {
                try {
                    if(checkWasSuccessfull === true) {
                        me.prepareNotifItemForSave();

                        var notifItem = me.getViewModel().get('notifItem');

                        var saveCallback = function(success) {
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

                            try {
                                if(success === true) {
                                    me.onNotifItemSaved(notifItem);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifItemSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifItem'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch(ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifItem of NotifItemDialogController', ex);
                            } finally {
                                me._saveRunning = false;
                            }
                        };

                        var eventId = AssetManagement.customer.manager.NotifItemManager.saveNotifItem(notifItem);
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch(ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveNotifItem of NotifItemDialogController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            //check if data is complete first
            this.checkInputValues(saveAction, this);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //private
    //checks if the data of the notifItem object is complete
    //returns a boolean, if the checks have been passed successfully
    //if any check fails it will also trigger a corresponding error message
    checkInputValues: function(callback, callbackScope)  {
        try {
            var retval = true;
            var errorMessage = '';

            var currentInputValues = this.getView().getCurrentInputValues();

            if (!currentInputValues.damageCode) {
                errorMessage = Locale.getMsg('provideDamageCode');
                retval = false;
            } else if (!currentInputValues.objectCode) {
                errorMessage = Locale.getMsg('provideObjectCode');
                retval = false;
            }

            var returnFunction = function() {
                if(retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }

                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);

            if(callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    //private
    prepareNotifItemForSave: function () {
        try {
            var currentInputValues = this.getView().getCurrentInputValues();
            var myModel = this.getViewModel();
            var notifItem = myModel.get('notifItem');
            var notif = myModel.get('notif');

            if (!notifItem) {
                notifItem = Ext.create('AssetManagement.customer.model.bo.NotifItem', {
                    notif: myModel.get('notif')
                });
            }

            notifItem.set('fetxt', currentInputValues.shorttext);

            var damageCode = currentInputValues.damageCode ? currentInputValues.damageCode : null;
            notifItem.set('fekat', damageCode ? damageCode.get('katalogart') : '');
            notifItem.set('fegrp', damageCode ? damageCode.get('codegruppe') : '');
            notifItem.set('fecod', damageCode ? damageCode.get('code') : '');
            notifItem.set('damageCustCode', damageCode);

            var objectCode = currentInputValues.objectCode ? currentInputValues.objectCode : null;
            notifItem.set('otkat', objectCode ? objectCode.get('katalogart') : '');
            notifItem.set('otgrp', objectCode ? objectCode.get('codegruppe') : '');
            notifItem.set('oteil', objectCode ? objectCode.get('code') : '');
            notifItem.set('objectPartCustCode', objectCode);

            notifItem.set('mobileKey', notif.get('mobileKey'))

            myModel.set('notifItem', notifItem);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    },

    //private
    onNotifItemSaved: function (notifItemObject) {
        try {
            var callbackInterface = this.getOwner()[this.self.ON_ITEM_SAVED];

            if (callbackInterface)
                callbackInterface.call(this.getOwner(), notifItemObject);

            //dismiss
            this.dismiss();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    }
});