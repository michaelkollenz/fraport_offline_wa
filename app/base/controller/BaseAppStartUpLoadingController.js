Ext.define('AssetManagement.base.controller.BaseAppStartUpLoadingController', {
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.AppConfig',
        'AssetManagement.customer.helper.LocalStorageHelper',
		'AssetManagement.customer.helper.DataBaseHelper',
		'AssetManagement.customer.controller.ClientStateController',
		'AssetManagement.customer.controller.AppUpdateController',
		'AssetManagement.customer.helper.LibraryHelper',
		'AssetManagement.customer.controller.LoadingIndicatorController',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.utils.StringUtils'
    ],

	inheritableStatics: {
		//private
	    _appConfigLoaded: false,
	    _dataBaseLoaded: false,
	    _localeLoaded: false,
		_localeScriptLoaded: false,
		
		_errorOccured: false,
	
		//public
		//will initialize: AppConfig, DatabaseHelper and Locale
		initializeApplication: function() {
			try {
				//initialize AppConfig
				AssetManagement.customer.core.Core.setAppConfig(AssetManagement.customer.AppConfig.getInstance({ listeners: { initialized: { fn: this.onAppConfigInitialized, scope: this } } }));
				
				//initialize DatabaseHelper for Main Database
				var dataBaseConfigObject = {
					dataBaseName: AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseName(),
					dataBaseVersion: AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseVersion(),
					jsonResourceNameOfStrctInfo: 'databasestructure.json',
                    jsonResourceCustomerStrctInfo: 'customer.json',
					listeners: { initialized: { fn: this.onMainDataBaseInitialized, scope: this} }
				};
				
				AssetManagement.customer.core.Core.setDataBaseHelper(Ext.create('AssetManagement.customer.helper.DataBaseHelper', dataBaseConfigObject));
				
				//initialize Locale
				this.initializeLocale();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeApplication of BaseAppStartUpLoadingController', ex);
			}
		},
		
		//private
		//sub routines of initializeApplication
		onLoadingStepReturned: function(success, rootEventString, errorReturnMessage) {
			try {
		    	if(!success) {
		    		var isFirstError = this._errorOccured === false;
		    		this._errorOccured = true;
		    	
		    		var uiMessage = '';
		    		var errorMessage = errorReturnMessage;
		    		var setDefaultErrorTypeMessage = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(errorMessage);
		    	
					if(rootEventString === 'dataBaseInitialized') {
						uiMessage = 'Database could not be opened.';
						errorMessage = setDefaultErrorTypeMessage ? 'General error while opening Database' : errorMessage;
					} else if(rootEventString === 'appConfigInitialized') {
						uiMessage = 'Application settings could not be loaded!';
						errorMessage = setDefaultErrorTypeMessage ? 'General error while initialzing AppConfig' : errorMessage;
					} else if(rootEventString === 'localeInitialized') {
						uiMessage = 'Could not load localization!';
						errorMessage = setDefaultErrorTypeMessage ? 'Failed to load Locale script.' : errorMessage;
					}
					
					AssetManagement.customer.helper.OxLogger.logMessage(errorMessage);
					
					//only report the first error to the user
					if(isFirstError) {
						this.onLoadingFailed(uiMessage, errorMessage);
					}
				} else if(this.loadingSuccessfullyCompleted() && !this._errorOccured) {
					this.onLoadingDone();
				}
	    	} catch(ex) {
	    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLoadingStepReturned of BaseAppStartUpLoadingController', ex);
			}
		},
		
		onLoadingDone: function() {
			try {
				//disable the caching for persistent logging (all cached log entries will be written to persistent log)
				AssetManagement.customer.helper.OxLogger.disableCachingForPersistentTrace();
			
				var ac = AssetManagement.customer.core.Core.getAppConfig();
				
				AssetManagement.customer.controller.LoadingIndicatorController.onApplicationStartUp();
				
				AssetManagement.customer.core.Core.initializeViewPort();
				
				if(ac.isDebugMode() === true && ac.getAutoLoginUser() === true) {
					this.tryLoginUserOnStartUp();
				} else {
					AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_LOGINSCREEN);
				}
				
				if(AssetManagement.customer.controller.AppUpdateController.hasThereBeenAnUpdate()) {
					var message = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('applicationUpdatedTo'), [ AssetManagement.customer.core.Core.getVersion() ]);
				
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(message, true, 1);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLoadingDone of BaseAppStartUpLoadingController', ex);
			}
		},
		
		onLoadingFailed: function(uiMessage, errorMessage) {
			try {
				AssetManagement.customer.helper.OxLogger.disableCachingForPersistentTrace();
				AssetManagement.customer.controller.LoadingIndicatorController.onApplicationStartUp();
				
				//show a dialog with uiMessage and errorMessage
				//reload on confirmation
				var innerCallback = function(confirmed) {
					try {
						location.reload();
					} catch(ex) {
			        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLoadingFailed of BaseAppStartUpLoadingController', ex);
			       	}    				
				};
				
				var message = uiMessage + '<br>' + 'Error message: ' + errorMessage;
				
				var dialogArgs = {
				    title: 'Startup error',
				    icon: 'resources/icons/alert.png',
					message: message,
					asHtml: true,
					alternateConfirmText: 'Reload',
					hideDeclineButton: true,
					callback: innerCallback,
					scope: this
				};
				
				AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLoadingFailed of BaseAppStartUpLoadingController', ex);
			}
		},
		
		loadingSuccessfullyCompleted: function() {
			return this._appConfigLoaded === true && this._dataBaseLoaded === true && this._localeLoaded === true;
		},
	    
	    onMainDataBaseInitialized: function(success, errorMessage) {
			try {
				var test = arguments;
			
				if(errorMessage && (typeof(errorMessage) !== 'string'))
					errorMessage = '';
			
		    	this._dataBaseLoaded = success;
		    	
			    var databaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();
			    
			    if(databaseHelper)
			    	databaseHelper.removeListener('initialized', this.onMainDataBaseInitialized, this);
			    	
			    //if the database has been opened, store this it's meta information in local storage
		    	if(success === true) {
			    	var dataBaseName = AssetManagement.customer.core.Core.getDataBaseHelper().getOpenedDataBaseName();
			    	var dataBaseVersion = AssetManagement.customer.core.Core.getDataBaseHelper().getOpenedDataBaseVersion();
			    	
			    	AssetManagement.customer.controller.ClientStateController.setCurrentMainDataBaseData(dataBaseName, dataBaseVersion);
			    }
			    	
				this.onLoadingStepReturned(success, 'dataBaseInitialized', errorMessage);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMainDataBaseInitialized of BaseAppStartUpLoadingController', ex);
	    		this.onLoadingStepReturned(false, 'dataBaseInitialized');
			}
		},
		
		onAppConfigInitialized: function(success) {
			try {
				this._appConfigLoaded = success;
				AssetManagement.customer.AppConfig.getInstance().removeListener('initialized', this.onAppConfigInitialized, this);
				
				this.onLoadingStepReturned(success, 'appConfigInitialized');
				
				if(this._localeScriptLoaded === true) {
					this.setStartUpLocale();
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAppConfigInitialized of BaseAppStartUpLoadingController', ex);
	    		this.onLoadingStepReturned(false, 'appConfigInitialized');
			}
		},
		
		onLocaleInitialized: function(success) {
			try {
				this._localeLoaded = success;	
			
				this.onLoadingStepReturned(success, 'localeInitialized');
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('An exception occured while initializing the application (Locale).', ex);
	    		this.onLoadingStepReturned(false, 'localeInitialized');
			}
		},
		
		//will load the locale script and wait for the appconfig initialization to complete, before setting the start up locale
		initializeLocale: function() {
			try {
				var scriptLoadedCallback = function(success) {
					try {
						this._localeScriptLoaded = true;
					
						if(this._appConfigLoaded === true) {
							this.setStartUpLocale();
						}
					} catch(ex) {
			    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeLocale of BaseAppStartUpLoadingController', ex);
			    		this.onLocaleInitialized(false);
					}
				};
				
				//first load the script
				AssetManagement.customer.helper.LibraryHelper.loadLibrary('resources/libs/locale.js', true, scriptLoadedCallback, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeLocale of BaseAppStartUpLoadingController', ex);
				this.onLocaleInitialized(false);
			}
		},
		
		setStartUpLocale: function() {
			try {
				var me = this;

				var callback = function() {
					try {
						//locales have been loaded, locale initialization completed
						me.onLocaleInitialized.call(me, true);
					} catch(ex) {
						//do NOT log on database, because it may not be available
			    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setStartUpLocale of BaseAppStartUpLoadingController', ex);
			    		
			    		me.onLocaleInitialized.call(me, false);
					}
				};
			
				AssetManagement.customer.controller.ClientStateController.setClientLocale(AssetManagement.customer.AppConfig.getInstance().getLocale(), callback);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setStartUpLocale of BaseAppStartUpLoadingController', ex);
				this.onLocaleInitialized(false);
			}
		},
		
		tryLoginUserOnStartUp: function() {
			try {
				var ac = AssetManagement.customer.core.Core.getAppConfig();
			
				var failureCallback = function(userInfo) {
			    	try {
			    		var args = { };
			    		
			    		if(userInfo) {
			    			if(userInfo.getErrorMessage)
			    				args.statusMessage = userInfo.getErrorMessage();
			    			else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userInfo))
			    				args.statusMessage = userInfo;
			    		}
			    	
						AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_LOGINSCREEN, args);
				    } catch(ex) {
				    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLoginUserOnStartUp of BaseAppStartUpLoadingController', ex);
					}
				};
				
				AssetManagement.customer.controller.ClientStateController.tryLogInUser(ac.getMandt(), ac.getUserId(), ac.getUserPassword(), null, failureCallback);
		    } catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryLoginUserOnStartUp of BaseAppStartUpLoadingController', ex);
			}
		}
	}
});