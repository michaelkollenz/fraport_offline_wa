Ext.define('AssetManagement.base.controller.BaseLoadingIndicatorController', {
	inheritableStatics: {
		requires: [
		    'AssetManagement.customer.helper.OxLogger'
        ],
        
        //private
        _loadingIndicator: null,
	
		//public
		onApplicationStartUp: function() {
			try {
				this.initializeGeneralLoadingIndicator();
				this.removeStartUpLoadingIndicator();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onApplicationStartUp of BaseLoadingIndicatorController', ex);
			}
		},
		
		 showLoadingIndicator: function(delayInMs) {
            try {
                if (this._loadingIndicator) {
                    this._loadingIndicator.style.transition = delayInMs + 'ms';
                    this._loadingIndicator.style.opacity = 1; 
                    this._loadingIndicator.style.height = '60px';
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLoadingIndicator of BaseLoadingIndicatorController', ex);
            }
        },
        
        hideLoadingIndicator: function() {
            try {
                if (this._loadingIndicator) {
                    this._loadingIndicator.style.transition = '0s';
                    this._loadingIndicator.style.opacity = 0; 
                    this._loadingIndicator.style.height = 0;
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideLoadingIndicator of BaseLoadingIndicatorController', ex);
            }
        },
		
		//private
		removeStartUpLoadingIndicator: function() {
			try {
	    		//destroy the startup loading indicator
	    		var loadingIndicator = Ext.get('startUpLoadingIndicator');
	    		
	    		if(loadingIndicator) {
	    			loadingIndicator.destroy();
	    		}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removeStartUpLoadingIndicator of BaseLoadingIndicatorController', ex);
			}
		},
		
		initializeGeneralLoadingIndicator: function() {
			try {
	    		//adds the the general loading indicator to the dom
	    		var loadingIndicator = document.createElement('div');
	    		loadingIndicator.setAttribute('id', 'loadingIndicator');
	    		loadingIndicator.setAttribute('class', 'spinner loadingIndicator');
	    		
	    		//add the three spinners
	    		for(var i = 1; i < 4; i++) {
	    			var spinner = document.createElement('div');
		    		spinner.setAttribute('class', 'spinner-container container' + i);
		    		
		    		for(var j = 1; j < 5; j++) {
		    			//add the circles to the spinners
		    			var circle = document.createElement('div');
		    			circle.setAttribute('class', 'circle' + j);
		    			
		    			spinner.appendChild(circle);
		    		}
		    		
		    		loadingIndicator.appendChild(spinner);
	    		}
	    		
	    		// loadingIndicator.style.display = 'none';

	    		document.getElementsByTagName('body')[0].appendChild(loadingIndicator);
	    		
	    		this._loadingIndicator = loadingIndicator;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeGeneralLoadingIndicator of BaseLoadingIndicatorController', ex);
			}
		}
	}
});