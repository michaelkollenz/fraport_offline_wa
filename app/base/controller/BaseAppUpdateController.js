Ext.define('AssetManagement.base.controller.BaseAppUpdateController', {
	requires: [
		'AssetManagement.customer.helper.AppCacheHelper',
//		'AssetManagement.customer.controller.ClientStateController',		//WOULD CAUSE RING DEPENDENCY
		'AssetManagement.customer.helper.LocalStorageHelper',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.utils.UserAgentInfoHelper'
	],

	inheritableStatics: {
		_completedAnUpdate: false,

		//public
		//will check, if there is an application update available and if so, get the update (it will be activated automatically - application will restart)
		//else will check, if an update has been activated and perform "after activation steps" (callback will be called, when these steps complete)
		//else call the callback directly
		//callback will be called, with and boolean indicating if this procedure was successful
		checkForAndHandleApplicationUpdate: function(callback, callbackScope) {
		    try {
				var updateReadyCheckCallback = function(updateReady) {
					try {
					    if (updateReady === true) {
					        //set the update ready flag to true (set it only on true cases, else it would falsify the state - example: multiple checking)
					        AssetManagement.customer.helper.LocalStorageHelper.putBoolean('applicationUpdateReady', true);

							//there is an update available, so start application update steps
							this.updateApplication();
						} else {
							//no update available, now check, if there still is an update in progress triggered by a former application start
							var updateInProgress = this.isUpdateInProgress();

							if(updateInProgress) {
								AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] Application has been updated to version: ' + AssetManagement.customer.core.Core.getVersion());

								var innerCallback = function() {
									if(callback)
										callback.call(callbackScope ? callbackScope : this, true);
								};

								this.afterUpdateActivation(innerCallback, this);
							} else {
								if(callback)
									callback.call(callbackScope ? callbackScope : this, true);
							}
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleApplicationUpdate of BaseAppUpdateController', ex);

						if(callback)
							callback.call(callbackScope ? callbackScope : this, false);
					}
				};

		        //first check the local storage, if the update ready flag has been already set
		        //this will always happen, if the automatic application caching (part of page loading) completed already downloading an available update
				var updateReady = AssetManagement.customer.helper.LocalStorageHelper.getBoolean('applicationUpdateReady', false);

				if (updateReady) {
				    updateReadyCheckCallback.call(this, true);
				} else {
				    //flag is not set. Perform an update ready check on the application cache, to determine, if there is an update available
				    AssetManagement.customer.helper.AppCacheHelper.doUpdateReadyCheck(updateReadyCheckCallback, this);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleApplicationUpdate of BaseAppUpdateController', ex);

				if(callback)
					callback.call(callbackScope ? callbackScope : this, false);
			}
		},

		//public
	    //compares the softwares current version to the version, which is assigned to the user
        //if one is detected, the user will be forced to download it
	    //callback will be called, with and boolean indicating if this procedure was passed successfull
		checkSVMForApplicationUpdate: function(userInfo, callback, callbackScope) {
		    try {
		        var compareAction = function (usrInfo) {
		            try {
		                var newVersionAvailable = false;

		                if (usrInfo && typeof (usrInfo) === 'object' && !usrInfo.fromOffline) {
                        newVersionAvailable = this.checkNewAppVersionAvailable(usrInfo);
		                }

		                if (newVersionAvailable) {
		                    //only run into the update cycle, if application is cached at all
		                    //in developement state it is not kept cached, so it has to be skipped
		                    var isApplicationCachingActive = AssetManagement.customer.helper.AppCacheHelper.isApplicationCachingActive();

		                    if (!isApplicationCachingActive) {
		                        AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] Stopping application update, because the application is not cached at all.');

		                        if (callback) {
		                            callback.call(callbackScope ? callbackScope : this, true);
		                        }

		                        return;
		                    }

		                    var confirmCallback = function (confirmed) {
		                        try {
		                            //don't care of confirmed - the update has to be done anyway

		                            //chrome needs special handling for manifest retrieving
		                            if (AssetManagement.customer.utils.UserAgentInfoHelper.getWebbrowser().toLowerCase().indexOf('chrome') > -1) {
		                                //simple reload to get authentification dialog after manual removing of appcache
		                                var chromeUpdateCallback = function () {
		                                    try {
		                                        location.reload();
		                                    } catch (ex) {
		                                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkSVMForApplicationUpdate of BaseAppUpdateController', ex);
		                                    }
		                                };

		                                var message = Locale.getMsg('chromeManualAppClearingP1');
		                                message += '<br><code>chrome://appcache-internals/</code><br>';
		                                message += Locale.getMsg('chromeManualAppClearingP2');

		                                var identifierToUse = this.getAppCacheIdentifier();
		                                message += '<br><code>' + identifierToUse + '</code><br>';
		                                message += Locale.getMsg('chromeManualAppClearingP3');

		                                var dialogArgs = {
		                                    title: Locale.getMsg('applicationUpdate'),
		                                    icon: 'resources/icons/updateApp.png',
		                                    message: message,
		                                    asHtml: true,
		                                    alternateConfirmText: Locale.getMsg('dialogOk'),
		                                    hideDeclineButton: true,
		                                    callback: chromeUpdateCallback,
		                                    scope: this
		                                };

		                                AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
		                            } else {
		                                //simply reload the application, this will trigger the automatic update
		                                //this will result in infinite loop if the maintained software version in SAP does not match the actually build in the software itself!
		                                location.reload();
		                            }
		                        } catch (ex) {
		                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkSVMForApplicationUpdate-confirmCallback of BaseAppUpdateController', ex);

		                            if (callback)
		                                callback.call(callbackScope ? callbackScope : this, false);
		                        }
		                    };

		                    //prompt the user with a dialog, that he needs to download the newest version
		                    //this cannot be skipped
		                    var dialogArgs = {
		                        title: Locale.getMsg('applicationUpdate'),
		                        icon: 'resources/icons/updateApp.png',
		                        message: Locale.getMsg('newApplVersionAvailable') + '</br>' + Locale.getMsg('hasToUpdateApplication'),
		                        asHtml: true,
		                        alternateConfirmText: Locale.getMsg('update'),
		                        hideDeclineButton: true,
		                        callback: confirmCallback,
		                        scope: this
		                    };

		                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
		                } else if (callback) {
		                    callback.call(callbackScope ? callbackScope : this, true);
		                }
		            } catch (ex) {
		                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkSVMForApplicationUpdate of BaseAppUpdateController', ex);

		                if (callback)
		                    callback.call(callbackScope ? callbackScope : this, false);
		            }
		        };

		        //if user info is not given, a new one will be requested for the currently logged in user
		        var ac = AssetManagement.customer.core.Core.getAppConfig();

		        if (!userInfo && ac.isUserLoggedIn()) {
		            AssetManagement.customer.manager.UserManager.checkUserIsValid(ac.getMandt(), ac.getUserId(), ac.getUserPassword(), compareAction);
		        } else {
		            compareAction.call(this, userInfo);
		        }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkSVMForApplicationUpdate of BaseAppUpdateController', ex);

		        if (callback)
		            callback.call(callbackScope ? callbackScope : this, false);
		    }
		},

    checkNewAppVersionAvailable: function(usrInfo) {
      var retval = false;

      try {
        //read the software version number and compare it to the application ones
        var major = usrInfo.getVerMajorRelease();
        var minor = usrInfo.getVerMinorRelease();
        var patchLevel = usrInfo.getVerPatchLevel();

        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(major) &&
              !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(minor) &&
                  !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(patchLevel)) {

          major = AssetManagement.customer.utils.StringUtils.padLeft(major, '0', 3);
          minor = AssetManagement.customer.utils.StringUtils.padLeft(minor, '0', 3);
          patchLevel = AssetManagement.customer.utils.StringUtils.padLeft(patchLevel, '0', 3);

          var currentAppVersion = AssetManagement.customer.core.Core.getVersion();

          currentAppVersion = AssetManagement.customer.utils.StringUtils.padLeft(currentAppVersion, '0', 11);

          var availableAppVersion = major + '.' + minor + '.' + patchLevel;

          retval = availableAppVersion !== currentAppVersion;

          if (retval) {
            AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] Detected necessity for an application update via SVM. Current version: ' + currentAppVersion + ' - Available version: ' + availableAppVersion);
          }
        }
      } catch(ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkNewAppVersionAvailable of BaseAppUpdateController', ex);
      }

      return retval;
    },

		//will return, if there has been an update since the last start of the application
		isUpdateInProgress: function() {
			var retval = false;

			try {
				retval = AssetManagement.customer.helper.LocalStorageHelper.getBoolean('applicationUpdateInProgress', false);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasThereBeenAnUpdate of BaseAppUpdateController', ex);
			}

			return retval;
		},

		//will return, if there has been an update since the last start of the application
		hasThereBeenAnUpdate: function() {
			var retval = false;

			try {
				retval = this._completedAnUpdate;
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasThereBeenAnUpdate of BaseAppUpdateController', ex);
			}

			return retval;
		},

		//will test if a database upgrade is pending and handle this procedure
		//the callback will be called with an boolean, which indicates, if this procedure was susccessful
		checkForAndHandleDataBaseUpgrade: function(callback, callbackScope) {
			try {
				var currentDataBaseVersionInUse = AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseVersion();
				var applicationsDataBaseVersionRequirement = AssetManagement.customer.core.Core.getRequiredDatabaseVersion();
				var upgradeNeccessary = applicationsDataBaseVersionRequirement > currentDataBaseVersionInUse;

				if(upgradeNeccessary) {
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

					//inform user of database upgrade necessity
					var infoCallback = function(confirmed) {
						try {
							//confirmed my only be false, if the user hit the (navigation) back button
							if(!confirmed) {
								if(callback)
									callback.call(callbackScope ? callbackScope : me, false);

								return;
							}

							//try to perform a sync
							var me = this;

							var syncCallback = function(success) {
								try {
									if(success === true) {
										AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

										//database upgrade may be performed
										var databaseUpgradeCallback = function(success) {
											try {
												if(success) {
													AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] Database has been upgraded from version ' + currentDataBaseVersionInUse +' to ' + applicationsDataBaseVersionRequirement + '.', true);
												} else {
													success = false;
												}

												if(callback)
													callback.call(callbackScope ? callbackScope : me, success);
											} catch(ex) {
												AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleDataBaseUpgrade of BaseAppUpdateController', ex);

												if(callback)
													callback.call(callbackScope ? callbackScope : me, false);
											}
										};

										me.performDataBaseUpgrade(databaseUpgradeCallback);
									} else if(callback) {
										callback.call(callbackScope ? callbackScope : me, false);
									}
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleDataBaseUpgrade of BaseAppUpdateController', ex);

									if(callback)
										callback.call(callbackScope ? callbackScope : me, false);
								}
							};

							//start the sync
							this.tryToPerformASynchronization(syncCallback)
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleDataBaseUpgrade of BaseAppUpdateController', ex);

							if(callback)
								callback.call(callbackScope ? callbackScope : this, false);
						}
					};

					this.showDatabaseUpgradeRequirementDialog(infoCallback);
				} else {
					if(callback)
						callback.call(callbackScope ? callbackScope : this, true);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleDataBaseUpgrade of BaseAppUpdateController', ex);

				if(callback)
					callback.call(callbackScope ? callbackScope : this, false);
			}
		},

		//private
		//performs logic before an update and activates the application update at the end
		updateApplication: function() {
		    try {
		        //set back the update ready status flag of the local storage
		        AssetManagement.customer.helper.LocalStorageHelper.putBoolean('applicationUpdateReady', false);

				//before activating the update, write a flag into the local storage
				//this flag is necessary, so the application can perform additional updating steps, after the activating of the update
				AssetManagement.customer.helper.LocalStorageHelper.putBoolean('applicationUpdateInProgress', true);

				//now activate the update, reload the application
				window.location.reload();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateApplication of BaseAppUpdateController', ex);
			}
		},

		//performs logic after an update has been activated
		//the passed callback will be called, after these steps completed
		afterUpdateActivation: function(callback, callbackScope) {
			try {
				//check, if the updated application version uses higher database version
				//if it does, the application update flag may NOT be reset, so the user will have to perform a sync after logging in
				var currentDataBaseVersionInUse = AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseVersion();
				var applicationsDataBaseVersionRequirement = AssetManagement.customer.core.Core.getRequiredDatabaseVersion();
				var dataBaseCreated = currentDataBaseVersionInUse > 0;
				var updatedApplicationUsesHigherDatabaseVersion = applicationsDataBaseVersionRequirement > currentDataBaseVersionInUse;

				if(dataBaseCreated && updatedApplicationUsesHigherDatabaseVersion) {
					AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] The application update includes a database upgrade. Update process will be completed after the user performed a synchronization.', true);
					AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] The application requires a database of version: ' + applicationsDataBaseVersionRequirement, true);
					AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] The application currently works on a database of version: ' + currentDataBaseVersionInUse, true);
				}

				var me = this;
				var pendingSteps = 0;
				var appConfigReinitialized = false;
				var returned = false;

				var activationStepCompleted = function() {
					try {
						if(pendingSteps === 0) {
							//mark the update as complete
							me._completedAnUpdate = true;
							AssetManagement.customer.helper.LocalStorageHelper.putBoolean('applicationUpdateInProgress', false);
							AssetManagement.customer.helper.OxLogger.logMessage('[UPADTE] Application update completed.');

							if(callback)
								callback.call(callbackScope ? callbackScope : me);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterUpdateActivation of BaseAppUpdateController', ex);

						if(!returned && callback) {
							returned = true;
							callback.call(callbackScope ? callbackScope : me);
						}
					}
				};

				var appConfigCallback = function(success) {
					pendingSteps--;

					appConfigReinitialized = !!success;

					activationStepCompleted();
				};

				//update appconfig
				//reinitialize it by values from xml
				pendingSteps++;
				this.reinitializeAppConfig(appConfigCallback, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterUpdateActivation of BaseAppUpdateController', ex);

				if(callback)
					callback.call(callbackScope ? callbackScope : this);
			}
		},

		//will show a dialog, which will inform the user, that the database has to be upgraded before the user can use the updated application
		//this dialog has a confirm option only
		showDatabaseUpgradeRequirementDialog: function(callback) {
			try {
				var innerCallback = function(confirmed) {
					try {
						if(callback)
							callback.call(this, confirmed);
					} catch(ex) {
			        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showDatabaseUpgradeRequirementDialog of NavigationController', ex);
			       	}
				};

				var dialogArgs = {
				    title: Locale.getMsg('applicationUpdate'),
				    icon: 'resources/icons/alert.png',
					message: Locale.getMsg('databaseUpgradeRequired'),
					alternateConfirmText: Locale.getMsg('dialogContinue'),
					hideDeclineButton: true,
					callback: innerCallback,
					scope: this
				};

				AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showDatabaseUpgradeRequirementDialog of BaseAppUpdateController', ex);
			}
		},

		//will try to perform a synchronization, offering the user to retry on failure
		tryToPerformASynchronization: function(callback) {
			try {
				var me = this;

				var syncCallback = function(success) {
					try {
						if(success === true) {
							//call the callback with success
							if(callback)
								callback.call(me, true);
						} else {
							//show a dialog with retrial option
							var retryCallback = function(doRetry) {
								try {
									if(doRetry === true) {
										me.tryToPerformASynchronization(callback)
									} else if(callback) {
										callback.call(me, false);
									}
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleDataBaseUpgrade of BaseAppUpdateController', ex);

									if(callback)
										callback.call(me, false);
								}
							};

							me.showSyncRetrialDialog(retryCallback);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleDataBaseUpgrade of BaseAppUpdateController', ex);

						if(callback)
							callback.call(callbackScope ? callbackScope : me, false);
					}
				};

				//start the sync
				this.showSyncDialog(syncCallback)
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryToPerformASynchronization of BaseAppUpdateController', ex);

				if(callback)
					callback.call(this, false);
			}
		},

		//will trigger a synchronization and call the passed callback with a boolean indicating if the sychronization has been successful
		//this sync will do an upload only
		showSyncDialog: function(callback) {
			try {
				var innerSyncCallback = function(success) {
					try {
						if(!success)
							success = false;

						if(callback)
							callback.call(this, success);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSyncDialog of BaseAppUpdateController', ex);

						if(callback)
							callback.call(this, false);
					}
				};

				//parameters: callback, callbackScope, stayOnCurrentPage, uploadOnly
				AssetManagement.customer.core.Core.getMainView().getController().performSync(innerSyncCallback, this, true, true);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSyncDialog of BaseAppUpdateController', ex);

				if(callback)
					callback.call(this, false);
			}
		},

		showSyncRetrialDialog: function(callback) {
			try {
				var innerCallback = function(doRetry) {
					try {
						if(!doRetry)
							doRetry = false;

						if(callback)
							callback.call(this, doRetry);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSyncRetrialDialog of BaseAppUpdateController', ex);

						if(callback)
							callback.call(this, false);
					}
				};

				var dialogArgs = {
				    title: Locale.getMsg('synchronization'),
				    icon: 'resources/icons/question.png',
					message: Locale.getMsg('syncFailedRetryQuestion'),
					alternateConfirmText: Locale.getMsg('retry'),
					alternateDeclineText: Locale.getMsg('cancel'),
					callback: innerCallback,
					scope: this
				};

				AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showSyncRetrialDialog of BaseAppUpdateController', ex);

				if(callback)
					callback.call(this, false);
			}
		},

		//this function will delete the database and rebuild it
		//all data, except trace, will be lost - ensure the data has been synchronized before calling this method - to ensure it this method is private
		//the provided callback will be called with an success boolean
		performDataBaseUpgrade: function(callback) {
			try {
				//save the contect of the S_TRACE store in memory
				var me = this;

				var traceEntriesCallback = function(traceEntriesArray) {
					try {
						if(traceEntriesArray !== undefined) {
							AssetManagement.customer.helper.OxLogger.enableCachingForPersistentTrace();

							//rebuild database next
							var rebuildCallback = function(success) {
								try {
									if(success === true) {
										//next put the trace entries to database again
										var traceResaveCallback = function(success) {
											try {
												AssetManagement.customer.helper.OxLogger.disableCachingForPersistentTrace();
											} catch(ex) {
												AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDataBaseUpgrade of BaseAppUpdateController', ex);
											}

											if(callback)
												callback.call(me, success === true ? success : false);
										};

										me.resaveTraceContent(traceEntriesArray, traceResaveCallback);
									} else if(callback) {
										callback.call(me, false);
									}
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDataBaseUpgrade of BaseAppUpdateController', ex);

									if(callback)
										callback.call(me, false);
								}
							};

							me.rebuildDataBase(rebuildCallback);
						} else if(callback) {
							callback.call(me, false);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDataBaseUpgrade of BaseAppUpdateController', ex);

						if(callback)
							callback.call(me, false);
					}
				};

				this.getTraceContent(traceEntriesCallback);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDataBaseUpgrade of BaseAppUpdateController', ex);

				if(callback)
					callback.call(this, false);
			}
		},

		getTraceContent: function(callback) {
			if(!callback)
				return;

			try {
				var me = this;

				var traceEntriesCallback = function(traceArray) {
					callback.call(me, traceArray);
				};

				var eventId = AssetManagement.customer.helper.OxLogger.getAllTraceEntries();

				if(eventId > -1) {
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, traceEntriesCallback);
				} else {
					callback.call(this, undefined);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTraceContent of BaseAppUpdateController', ex);
				callback.call(this, undefined);
			}
		},

		rebuildDataBase: function(callback) {
			if(!callback)
				return;

			try {
				//kill the current database first
				var wipeCallback = function(success) {
					try {
						if(!success)
							success = false;

						if(success) {
							AssetManagement.customer.controller.ClientStateController.setCurrentMainDataBaseData('', 0);

							var initializedCallback = function(success) {
								try {
									AssetManagement.customer.core.Core.getDataBaseHelper().removeListener('initialized', initializedCallback, this);

									if(!success)
										success = false;

									var dataBaseName = AssetManagement.customer.core.Core.getDataBaseHelper().getOpenedDataBaseName();
							    	var dataBaseVersion = AssetManagement.customer.core.Core.getDataBaseHelper().getOpenedDataBaseVersion();
									AssetManagement.customer.controller.ClientStateController.setCurrentMainDataBaseData(dataBaseName, dataBaseVersion);

									callback.call(this, success);
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildDataBase of BaseAppUpdateController', ex);
									callback.call(this, false);
								}
							};

							//set a new database helper instance as databasehelper of the application
							var dataBaseConfigObject = {
								dataBaseName: '',
								dataBaseVersion: 0,
								jsonResourceNameOfStrctInfo: 'databasestructure.json',
                                jsonResourceCustomerStrctInfo: 'customer.json',
								listeners: { initialized: { fn: initializedCallback, scope: this} }
							};

							AssetManagement.customer.core.Core.setDataBaseHelper(Ext.create('AssetManagement.customer.helper.DataBaseHelper', dataBaseConfigObject));

							//the sync manager instance uses references to the old DataBaseHelper, so it has to be reset
							AssetManagement.customer.sync.SyncManager.resetInstance();
						} else {
							callback.call(this, false);
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildDataBase of BaseAppUpdateController', ex);
						callback.call(this, false);
					}
				};

				AssetManagement.customer.core.Core.getDataBaseHelper().wipeDataBase(wipeCallback, this);
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildDataBase of BaseAppUpdateController', ex);
				callback.call(this, false);
			}
		},

		resaveTraceContent: function(traceEntries, callback) {
			try {
				if(!traceEntries || traceEntries.length < 1) {
					if(callback)
						callback.call(this, true);

					return;
				}

				var me = this;
				var pendingRequests = 0;
				var errorOccured = false;

				var saveSuccessCallback = function() {
					try {
						pendingRequests--;

						if(pendingRequests == 0 && !errorOccured) {
							if(callback){
								callback.call(me, true);
							}
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resaveTraceContent of BaseAppUpdateController', ex);
						if(callback)
							callback.call(me, false);
					}
				};

				var savedFailedCallback = function() {
					try {
						pendingRequests--;

						if(!errorOccured) {
							errorOccured = true;

							if(callback) {
								callback.call(me, false);
							}
						}
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resaveTraceContent of BaseAppUpdateController', ex);
						if(callback)
							callback.call(me, false);
					}
				};

				Ext.Array.each(traceEntries, function(entry) {
					AssetManagement.customer.core.Core.getDataBaseHelper().put('S_TRACE', entry, saveSuccessCallback, savedFailedCallback, true);

					pendingRequests++;
				}, this);

				AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resaveTraceContent of BaseAppUpdateController', ex);
				if(callback)
					callback.call(this, false);
			}
		},

		//will reinitialize the appconfig - some values will be kept
		//calls the provided callback with an boolean indicating the success
		reinitializeAppConfig: function(callback, callbackScope) {
			try {
				var isUpdate = AssetManagement.customer.helper.LocalStorageHelper.getSettingsBoolean('HaveIBeenHereBeFore', false);

				if(isUpdate) {
					var valuesToKeep = {
					    userId: AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('UserId'),
						locale: AssetManagement.customer.helper.LocalStorageHelper.getSettingsString('Locale')
					};

					//set back the flag, indicating if the appconfig has to be loaded from xml
					AssetManagement.customer.helper.LocalStorageHelper.putSettingsBoolean('HaveIBeenHereBeFore', false);

					var innerCallback = function(success) {
						try {
							AssetManagement.customer.AppConfig.getInstance().removeListener('initialized', innerCallback, this);

							//restore the kept values
							AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('UserId', valuesToKeep.userId);
							AssetManagement.customer.helper.LocalStorageHelper.putSettingsString('Locale', valuesToKeep.locale);

							//set the new instance as application appconfig, if any has already been set
							if(AssetManagement.customer.core.Core.getAppConfig()) {
								AssetManagement.customer.core.Core.setAppConfig(AssetManagement.customer.AppConfig.getInstance());
							} else {
								//reset the currently get instance again
								AssetManagement.customer.AppConfig.resetInstance();
							}

							if(callback)
								callback.call(callbackScope ? callbackScope : this, success);
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reinitializeAppConfig of AppStartUpLoadingController', ex);

							if(callback)
								callback.call(callbackScope ? callbackScope : this, false);
						}
					};

					//trigger the initializing of AppConfig
					AssetManagement.customer.AppConfig.resetInstance();
					AssetManagement.customer.AppConfig.getInstance({ listeners: { initialized: { fn: innerCallback, scope: this } } });
				} else if(callback) {
					//appconfig values has not yet been stored in local storage, so nothing has to be done
					callback.call(callbackScope ? callbackScope : this, true);
				}
			} catch(ex) {
				AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reinitializeAppConfig of BaseAppUpdateController', ex);

				if(callback)
					callback.call(callbackScope ? callbackScope : this, false);
			}
		},

		getAppCacheIdentifier: function () {
		    var retval = '';

		    try {
		        var hostname = window.location.hostname;

		        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(hostname)) {
		            retval = hostname;

		            var port = window.location.port;

		            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(port)) {
		                retval += ':' + port;
		            }
		        } else {
		            retval = window.location.href;
                }
		    } catch (ex) {
		        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAppCacheIdentifier of BaseAppUpdateController', ex);
		    }

		    return retval;
		}
	}
});