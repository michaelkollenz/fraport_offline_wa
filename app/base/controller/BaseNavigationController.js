Ext.define('AssetManagement.base.controller.BaseNavigationController', {
  extend: 'Ext.app.Controller',

  requires: [
    'AssetManagement.customer.controller.EventController',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.helper.model.ModeHistoryEntry',
    'AssetManagement.customer.model.helper.ReturnMessage',
    'AssetManagement.customer.modules.barcode.BarcodeRetryDialog',
    'AssetManagement.customer.modules.barcode.BarcodeScanDialog',
    'AssetManagement.customer.modules.reportFramework.view.dialog.BuildReportDialog',
    'AssetManagement.customer.modules.reportFramework.view.dialog.ReportCostsDialog',
    'AssetManagement.customer.modules.reportFramework.view.dialog.ReportSignatureDialog',
    'AssetManagement.customer.modules.reportFramework.view.dialog.ReportTypeDialog',
    'AssetManagement.customer.modules.reportFramework.view.page.ReportsPreviewPage',
    'AssetManagement.customer.modules.reportFramework.view.page.ReportsPreviewPageiOS',
    'AssetManagement.customer.modules.search.SearchDialog',
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.view.dialogs.AlertDialog',
    'AssetManagement.customer.view.dialogs.AppendCheckboxDialog',
    'AssetManagement.customer.view.dialogs.ConfirmationDialog',
    'AssetManagement.customer.view.dialogs.CustomerPickerDialog',
    'AssetManagement.customer.view.dialogs.EquipmentPickerDialog',
    'AssetManagement.customer.view.dialogs.FuncLocPickerDialog',
    'AssetManagement.customer.view.dialogs.InternNoteDialog',
    'AssetManagement.customer.view.dialogs.LongtextDialog',
    //dialogs
    'AssetManagement.customer.view.dialogs.MaterialPickerDialog',
    'AssetManagement.customer.view.dialogs.MultiValueCheckboxDialog',
    'AssetManagement.customer.view.dialogs.NotifActivityDialog',
    'AssetManagement.customer.view.dialogs.NotifCauseDialog',
    'AssetManagement.customer.view.dialogs.NotifItemDialog',
    'AssetManagement.customer.view.dialogs.NotifTaskDialog',
    'AssetManagement.customer.view.dialogs.ObjectListEditDialog',
    'AssetManagement.customer.view.dialogs.ObjectStatusDialog',
    'AssetManagement.customer.view.dialogs.OnlineMaterialSearchDialog',
    'AssetManagement.customer.view.dialogs.OperationDialog',
    'AssetManagement.customer.view.dialogs.OperationDialog',
    'AssetManagement.customer.view.dialogs.PartnerPickerDialog',
    'AssetManagement.customer.view.dialogs.SDOrderItemDialog',
    'AssetManagement.customer.view.dialogs.ScenarioDialog',
    'AssetManagement.customer.view.dialogs.SendLogFilesDialog',
    'AssetManagement.customer.view.dialogs.SingleTextLineInputDialog',
    'AssetManagement.customer.view.dialogs.SingularValueCheckboxDialog',
    'AssetManagement.customer.view.dialogs.SingularValueLineDialog',
    'AssetManagement.customer.view.dialogs.SynchronizationDialog',
    'AssetManagement.customer.view.pages.BanfEditPage',
    'AssetManagement.customer.view.pages.CalendarPage',
    'AssetManagement.customer.view.pages.CalendarPage',
    'AssetManagement.customer.view.pages.ChecklistPage',
    'AssetManagement.customer.view.pages.ComponentPage',
    'AssetManagement.customer.view.pages.CustomerDetailPage',
    'AssetManagement.customer.view.pages.CustomerListPage',
    'AssetManagement.customer.view.pages.EquiChangePositionPage',
    'AssetManagement.customer.view.pages.EquiDetailPage',
    'AssetManagement.customer.view.pages.EquipmentEditPage',
    'AssetManagement.customer.view.pages.EquipmentListPage',
    'AssetManagement.customer.view.pages.ExtConfPage',
    'AssetManagement.customer.view.pages.FuncLocDetailPage',
    'AssetManagement.customer.view.pages.FuncLocEditPage',
    'AssetManagement.customer.view.pages.FuncLocListPage',
    'AssetManagement.customer.view.pages.GoogleMapsPage',
    'AssetManagement.customer.view.pages.HistNotifDetailPage',
    'AssetManagement.customer.view.pages.HistOrderDetailPage',
    'AssetManagement.customer.view.pages.HomeScreenPage',
    'AssetManagement.customer.view.pages.IntegratedConfPage',
    'AssetManagement.customer.view.pages.InventoryDetailPage',
    'AssetManagement.customer.view.pages.InventoryListPage',
    'AssetManagement.customer.view.pages.LoginPage',
    'AssetManagement.customer.view.pages.MatConfPage',
    'AssetManagement.customer.view.pages.MaterialListPage',
    'AssetManagement.customer.view.pages.MatListPage',
    'AssetManagement.customer.view.pages.MeasDocEditPage',
    'AssetManagement.customer.view.pages.MeasPointListPage',
    'AssetManagement.customer.view.pages.NewEquiPage',
    'AssetManagement.customer.view.pages.NewNotifPage',
    'AssetManagement.customer.view.pages.NewOrderPage',
    'AssetManagement.customer.view.pages.NewSDOrderPage',
    'AssetManagement.customer.view.pages.NotifDetailPage',
    'AssetManagement.customer.view.pages.NotifItemDetailPage',
    'AssetManagement.customer.view.pages.NotifListPage',
    'AssetManagement.customer.view.pages.ObjectListPage',
    'AssetManagement.customer.view.pages.ObjectStructurePage',
    'AssetManagement.customer.view.pages.OrderDetailPage',
    'AssetManagement.customer.view.pages.OrderListPage',
    'AssetManagement.customer.view.pages.SDOrderDetailPage',
    'AssetManagement.customer.view.pages.SDOrderListPage',
    'AssetManagement.customer.view.pages.SettingsPage',
    'AssetManagement.customer.view.pages.TimeConfPage',
    'AssetManagement.customer.view.pages.TimeTableListPage',

    'AssetManagement.customer.view.pages.WeListPage',
    'AssetManagement.customer.view.pages.PoDetailPage',
    'AssetManagement.customer.view.pages.GoodsMovementListPage',
    'AssetManagement.customer.view.pages.WaCreatePage',
    'AssetManagement.customer.view.pages.WaCreateMaterialPage',
    'AssetManagement.customer.view.pages.DeliveryListPage',
    'AssetManagement.customer.view.pages.DeliveryDetailPage',
    'AssetManagement.customer.view.pages.PickupListPage',
    'AssetManagement.customer.view.pages.SelfDispositionPage',


    'AssetManagement.customer.view.dialogs.OnlinePoGetDialog',
    'AssetManagement.customer.view.dialogs.OnlineGoodsMovementDialog',
    'AssetManagement.customer.view.dialogs.OnlineBoCheckDialog',
    'AssetManagement.customer.view.dialogs.OnlineStockDialog',
    'AssetManagement.customer.view.dialogs.LgortPickerDialog',
    'AssetManagement.customer.view.dialogs.OnlineDispositionDialog',
    'Ext.data.Store',
    //'AssetManagement.customer.controller.ClientStateController', WOULD CAUSE A RING DEPENDENCY
    'Ext.util.History'
  ],

  inheritableStatics: {
    //public

    //modes constants
    MODE_LOGINSCREEN: 0,
    MODE_HOMESCREEN: 1,
    MODE_SETTINGS: 2,
    MODE_ORDER_LIST: 3,
    MODE_NOTIF_LIST: 4,
    MODE_EQUIPMENT_LIST: 5,
    MODE_FUNCLOC_LIST: 6,
    MODE_ORDER_DETAILS: 7,
    MODE_NOTIF_DETAILS: 8,
    MODE_EQUIPMENT_DETAILS: 9,
    MODE_FUNCLOC_DETAILS: 10,
    MODE_NOTIFITEM_DETAILS: 11,
    MODE_OPERATION_DETAILS: 12,
    MODE_COMPONENT: 13,
    MODE_TIMECONF: 14,
    MODE_ORDER_MATCONF: 15,
    MODE_HIST_NOTIF_LIST: 16,
    MODE_HIST_ORDER_LIST: 17,
    MODE_NEW_ORDER: 18,
    MODE_NEW_NOTIF: 19,
    MODE_OBJECT_LIST: 20,
    MODE_BANF_EDIT: 21,
    MODE_MEAS_DOC_EDIT: 22,
    MODE_MATERIAL_LIST: 23,
    MODE_EQUI_CHANGE_POSITION: 24,
    MODE_EQUIPMENT_EDIT: 25,
    MODE_CUSTOMER_LIST: 26,
    MODE_CUSTOMER_DETAILS: 27,
    MODE_TIME_TABLE_LIST: 28,
    MODE_FUNCLOC_EDIT: 29,
    MODE_SCHEDULE: 30,
    MODE_CALENDAR: 31,
    MODE_INVENTORY_LIST: 32,
    MODE_INVENTORY_DETAIL: 33,
    MODE_NEW_SDORDER: 34,
    MODE_SDORDER_LIST: 35,
    MODE_SDORDER_DETAILS: 36,
    MODE_EXT_CONF: 37,
    MODE_ORDER_REPORT: 38,
    MODE_NEW_EQUI: 39,
    MODE_HIST_ORDER_DETAIL: 40,
    MODE_HIST_NOTIF_DETAIL: 41,
    MODE_GOOGLEMAPS: 42,
    MODE_OBJ_STRUCTURE: 43,
    MODE_CHECKLIST: 44,
    MODE_MEASPOINT_LIST: 45,
    MODE_INTEGRATEDCONF: 46,
    MODE_REPORTS_PREVIEW_PAGE: 47,
    MODE_REPORTS_PREVIEW_PAGE_IOS: 48,

    MODE_PO_SCAN: 50,
    MODE_WE_LIST: 51,
    MODE_PO_DETAIL: 52,
    MODE_GOODS_MOVEMENT_LIST: 53,
    MODE_WA_CREATE: 54,
    MODE_WA_CREATE_MATERIAL: 55,
    MODE_MAT_STOCK: 56,
    MODE_DELIVERY: 57,
    MODE_PICKUP: 58,
    MODE_REPORT: 59,
    MODE_SELF_DISPOSITION: 60,

    //dialog constants
    ALERT_DIALOG: 100,
    CONFIRM_DIALOG: 101,
    MATERIALPICKER_DIALOG: 102,
    NOTIFTASK_DIALOG: 103,
    NOTIFITEM_DIALOG: 104,
    NOTIFCAUSE_DIALOG: 105,
    NOTIFACTIVITY_DIALOG: 106,
    SEARCH_DIALOG: 107,
    LONGTEXT_DIALOG: 108,
    EQUIPMENTPICKER_DIALOG: 109,
    FUNCLOCPICKER_DIALOG: 110,
    PARTNERPICKER_DIALOG: 111,
    OPERATION_DIALOG: 112,
    SDORDER_ITEM_DIALOG: 113,
    CUSTOMERPICKER_DIALOG: 114,
    SYNC_DIALOG: 115,
    BUILD_ORDER_REPORT_DIALOG: 116,
    BUILD_SCHEDULE_DIALOG: 117,
    SEND_LOGFILES_DIALOG: 118,
    BARCODE_SCAN_DIALOG: 119,
    BARCODE_RETRY_DIALOG: 120,
    OBJECTLISTEDIT_DIALOG: 121,
    CLASSEDIT_DIALOG: 122,
    CLASSEDIT_MULTICHECKBOX_DIALOG: 123,
    CLASSEDIT_SINGULARCHECKBOX_DIALOG: 124,
    CLASSEDIT_APPENDCHECKBOX_DIALOG: 125,
    CLASSEDIT_SINGULARLINE_DIALOG: 126,
    SCENARIO_DIALOG: 127,
    BUILD_REPORT_DIALOG: 129,
    REPORT_SIGNATURE_DIALOG: 130,
    BUILD_REPORT_TYPE_DIALOG: 131,
    REPORT_COSTS: 132,
    OBJECT_STATUS_DIALOG: 133,
    ONLINE_MAT_SEARCH_DIALOG: 134,
    INTERNNOTE_DIALOG: 135,
    SINGLE_TEXT_LINE_DIALOG: 136,

    ONLINE_PO_GET_DIALOG: 200,
    ONLINE_GOODS_MOVEMENT_DIALOG: 201,
    ONLINE_BO_CHECK_DIALOG: 202,
    CHANGE_LGORT_DIALOG: 203,
    ONLINE_STOCK_DIALOG: 204,
    ONLINE_DISPOSITION_DIALOG: 205

  },

  //private
  _processingRequest: false,
  _currentRequestIsGoBack: false,
  _modeHistory: null,
  _historyPointer: -1,
  _currentShownDialogs: null,
  _shownSingletonViewsOfThisSession: null,
  _ignoreNextHashChangeEvent: false,
  _requestRunning: false,

  init: function () {
    try {
      this._modeHistory = Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.helper.model.ModeHistoryEntry',
        autoLoad: false
      });

      this._shownSingletonViewsOfThisSession = Ext.create('Ext.util.HashMap');
      this._currentShownDialogs = new Array();

      AssetManagement.customer.controller.EventController.getInstance().registerOnEvent(AssetManagement.customer.controller.ClientStateController.EVENTS.USER_LOGOUT, this.clearHistory, this);

      this.initializeBrowserForwardBackwardHandling();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside init of BaseNavigationController', ex);
    }
  },

  getCurrentShownDialogs: function () {
    var retval = null;

    try {
      var me = this;

      retval = me._currentShownDialogs;

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentShownDialogs of BaseNavigationController', ex);
    }

    return retval;
  },

  getContentPanel: function () {
    var retval = null;

    try {
      retval = Ext.getCmp('contentPanel');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNavigationController', ex);
    }

    return retval;
  },

  //public
  //removes all history entries except the current one
  //sets history pointer to 0
  clearHistory: function () {
    try {
      //wait with clear history action, if there currently is any request proccessed
      if (!this._processingRequest) {
        if (this._modeHistory && this._modeHistory.getCount() > 0) {
          var currentHistoryEntry = null;

          if (this._historyPointer > -1)
            currentHistoryEntry = this._modeHistory.getAt(this._historyPointer);

          this._modeHistory.removeAll();

          if (currentHistoryEntry)
            this._modeHistory.add(currentHistoryEntry);

          if (this._historyPointer > 0)
            this._historyPointer = 0;
        }
      } else {
        Ext.defer(this.clearHistory, 200, this);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearHistory of BaseNavigationController', ex);
    }
  },

  requestSetMode: function (newMode, argumentsObject, removeCurrentPageFromStack) {
    try {
      var isRunning = this.initRunning();

      if (isRunning) {
        return;
      }

      AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Incoming request to set mode: ' + newMode, true);

      //decline if another navigation request is currently proccessed
      if (this._processingRequest === true) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request declined. Already processing another request.', true);
        return;
      }

      if (!newMode && newMode !== 0) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Cancelling request. Mode to set is unknown.', true);

        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('unknownPageRequested'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        return;
      }

      //add a new history entry to the mode history
      this.addNewEntryToHistory(newMode, argumentsObject);

      this.requestChangeMode(newMode, argumentsObject, false, removeCurrentPageFromStack);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestSetMode of BaseNavigationController', ex);
    }
  },

  requestShowDialog: function (dialogType, argumentsObject) {
    try {
      AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Incoming request to show dialog of type: ' + dialogType, true);

      //decline if another navigation request is currently proccessed
      if (this._processingRequest === true) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request declined. Already processing another request.', true);
        return;
      }

      if (!dialogType) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Cancelling request. Dialog type is unknown.', true);

        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('unknownDialogRequested'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        return;
      }

      this.beginShowDialog(dialogType, argumentsObject);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestShowDialog of BaseNavigationController', ex);
    }
  },

  requestGoBack: function () {
    try {
      var isRunning = this.initRunning();

      if (isRunning) {
        return;
      }

      AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Incoming request to go back.', true);

      //decline if another navigation request is currently proccessed
      if (this._processingRequest === true) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request declined. Already processing another request.', true);
        return;
      }

      //check, if going back is possible anyway
      if (this.canGoBack()) {
        this.goBack();
      } else {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request cancelled. Backwarding not possible.', true);

        if (AssetManagement.customer.core.Core.getMainView()) {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('nothingToNavigateBackTo'), true, 1);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestGoBack of BaseNavigationController', ex);
    }
  },

  requestGoForward: function () {
    try {
      AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Incoming request to go forward.', true);

      //decline if another navigation request is currently proccessed
      if (this._processingRequest === true) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request declined. Already processing another request.', true);
        return;
      }

      //check, if going foward is possible anyway
      if (this.canGoForward()) {
        this.goForward();
      } else {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request cancelled. Forwarding not possible.', true);

        if (AssetManagement.customer.core.Core.getMainView()) {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('nothingToNavigateForwardTo'), true, 1);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestGoForward of BaseNavigationController', ex);
    }
  },

  getCurrentActiveDialog: function () {
    var retval = null;

    try {
      retval = this._currentShownDialogs.pop();

      if (retval)
        this._currentShownDialogs.push(retval);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentActiveDialog of BaseNavigationController', ex);
    }

    return retval;
  },

  getCurrentPage: function () {
    var retval = null;

    try {
      var contentPanel = this.getContentPanel();

      if (contentPanel) {
        retval = contentPanel.layout.getActiveItem();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentPage of BaseNavigationController', ex);
    }

    return retval;
  },

  //adjusts all currently shown dialogs to the new view port configuration
  applyGlobalSizeChangeOnShownDialogs: function () {
    try {
      if (this._currentShownDialogs && this._currentShownDialogs.length > 0) {
        Ext.Array.each(this._currentShownDialogs, function (dialog) {
          this.adjustDialogToViewPort(dialog);
        }, this);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyGlobalSizeChangeOnShownDialogs of BaseNavigationController', ex);
    }
  },

  getAllShownSingletonViewsOfThisSession: function () {
    var retval = null;

    try {
      if (this._shownSingletonViewsOfThisSession && this._shownSingletonViewsOfThisSession.getCount() > 0) {
        retval = this._shownSingletonViewsOfThisSession.getValues();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllShownSingletonViewsOfThisSession of BaseNavigationController', ex);
      retval = null;
    }

    return retval;
  },

  //private
  initializeBrowserForwardBackwardHandling: function () {
    try {
      Ext.History.init();

      location.hash = 'requestBack';
      location.hash = '';
      location.hash = 'requestForward';

      history.back();

      //queue the registration on the changed event, so it will get registered, after the initial changed events have been fired
      Ext.defer(Ext.History.on, 1000, Ext.History, ['change', this.onNavigationHashChanged, this]);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeBrowserForwardBackwardHandling of BaseNavigationController', ex);
    }
  },

  //will add a new entry to the mode history
  //it will be inserted at current pointed position + 1
  //all entries behind that entry will be lost (removed)
  addNewEntryToHistory: function (newMode, argumentsObject) {
    try {
      var insertPosition = this._historyPointer + 1;

      var entry = Ext.create('AssetManagement.customer.helper.model.ModeHistoryEntry', {
        mode: newMode,
        argumentsObject: argumentsObject ? argumentsObject : {}
      });


      //console.log('BEFORE ' +this._modeHistory.getCount())
      this._modeHistory.insert(insertPosition, entry);
      //console.log('AFTER ' +this._modeHistory.getCount())


      //check if there is any entry after the insert position - if so remove all affected entries
      var countToRemove = this._modeHistory.getCount() - (insertPosition + 1);
      //console.log('COUNT TO REMOVE ' +countToRemove)

      while (countToRemove > 0) {
        this._modeHistory.removeAt(insertPosition + countToRemove);

        countToRemove--;
      }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNewEntryToHistory of BaseNavigationController', ex);
    }
  },

  canGoBack: function () {
    var retval = false;

    try {
      retval = this._currentShownDialogs.length > 0 || !!this.getTargetModeRequestForGoBack();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside canGoBack of BaseNavigationController', ex);
    }

    return retval;
  },

  //private
  goBack: function () {
    try {
      var logMessage = '[NAVIGATION] Performing a go back.';

      //check, if there currently are any shown dialogs - if yes, try to close the current one
      var topMostDialog = this.getCurrentActiveDialog();

      if (topMostDialog) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Sending closing request to topmost dialog.', true);
        topMostDialog.getController().requestCancellation();
      } else {
        var targetModeRequest = this.getTargetModeRequestForGoBack();

        if (targetModeRequest) {
          this.requestChangeMode(targetModeRequest.get('mode'), targetModeRequest.get('argumentsObject'), true);
        } else {
          AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Go back not possible. No target found.', true);
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside goBack of BaseNavigationController', ex);
    }
  },

  //private
  getTargetModeRequestForGoBack: function () {
    var retval = null;

    try {
      var targetHistoryIndex = this._historyPointer - 1;

      if (targetHistoryIndex > -1)
        retval = this._modeHistory.getAt(targetHistoryIndex);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTargetModeRequestForGoBack of BaseNavigationController', ex);
    }

    return retval;
  },

  //private
  canGoForward: function () {
    var retval = false;

    try {
      retval = !!this.getTargetModeRequestForGoForward();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside canGoForward of BaseNavigationController', ex);
    }

    return retval;
  },

  //private
  goForward: function () {
    try {
      var targetModeRequest = this.getTargetModeRequestForGoForward();

      if (targetModeRequest) {
        this.requestChangeMode(targetModeRequest.get('mode'), targetModeRequest.get('argumentsObject'));
      } else {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Go foward not possible. No target found.', true);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside goForward of BaseNavigationController', ex);
    }
  },

  //private
  getTargetModeRequestForGoForward: function () {
    var retval = null;

    try {
      var targetHistoryIndex = this._historyPointer + 1;

      if (targetHistoryIndex <= this._modeHistory.getCount())
        retval = this._modeHistory.getAt(targetHistoryIndex);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getTargetModeRequestForGoBack of BaseNavigationController', ex);
    }

    return retval;
  },

  //private
  //determines, if the user requested a backward or forward navigation
  onNavigationHashChanged: function (newHash) {
    try {
      if (this._ignoreNextHashChangedEvent === true) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Ignoring hash change event. Event was former flagged to be ignored.', true);
        this._ignoreNextHashChangedEvent = false;

        return;
      }

      var isBackwardsRequest = newHash === 'requestBack';
      this._ignoreNextHashChangedEvent = true;

      if (isBackwardsRequest) {
        history.forward();
        this.requestGoBack();
      } else {
        history.back();
        this.requestGoForward();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNavigationHashChanged of BaseNavigationController', ex);
    }
  },

  //private
  requestChangeMode: function (newMode, argumentsObject, isGoBack, removeCurrentModeFromStack) {
    try {
      //check for undesired targets, such as login page
      if (this.filterForUndesiredTargets(newMode) === true) {
        return;
      }

      var me = this;
      //before the change mode processing may be started, it is necessary to check for unsaved changes on the current active dialog/page
      //the check will return, if there are no user relevant changes
      var unsavedChangesCheckCallback = function (mayContinue) {
        try {
          if (mayContinue) {
            // before finally begining the navigation sequence for the new mode, set status variable for request in process
            me._processingRequest = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
            this.changeMode(newMode, argumentsObject, isGoBack, removeCurrentModeFromStack);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestChangeMode of BaseNavigationController', ex);
        }
      };

      if (AssetManagement.customer.core.Core.getAppConfig().isUserLoggedIn()) {
        this.checkForAndHandleUnsavedChangesCase(unsavedChangesCheckCallback);
      } else {
        unsavedChangesCheckCallback.call(this, true);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestChangeMode of BaseNavigationController', ex);
    }
  },

  getViewOfRequestedMode: function (requestedMode) {
    var retval = null;

    try {
      if (requestedMode === this.self.MODE_ORDER_LIST) {
        retval = AssetManagement.customer.view.pages.OrderListPage.getInstance();
      } else if (requestedMode === this.self.MODE_ORDER_DETAILS) {
        retval = AssetManagement.customer.view.pages.OrderDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_NOTIFITEM_DETAILS) {
        retval = AssetManagement.customer.view.pages.NotifItemDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_NOTIF_LIST) {
        retval = AssetManagement.customer.view.pages.NotifListPage.getInstance();
      } else if (requestedMode === this.self.MODE_NOTIF_DETAILS) {
        retval = AssetManagement.customer.view.pages.NotifDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_EQUIPMENT_LIST) {
        retval = AssetManagement.customer.view.pages.EquipmentListPage.getInstance();
      } else if (requestedMode === this.self.MODE_EQUIPMENT_DETAILS) {
        retval = AssetManagement.customer.view.pages.EquiDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_FUNCLOC_LIST) {
        retval = AssetManagement.customer.view.pages.FuncLocListPage.getInstance();
      } else if (requestedMode === this.self.MODE_FUNCLOC_DETAILS) {
        retval = AssetManagement.customer.view.pages.FuncLocDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_HOMESCREEN) {
        retval = AssetManagement.customer.view.pages.HomeScreenPage.getInstance();
      } else if (requestedMode === this.self.MODE_SETTINGS) {
        retval = AssetManagement.customer.view.pages.SettingsPage.getInstance();
      } else if (requestedMode === this.self.MODE_TIMECONF) {
        retval = AssetManagement.customer.view.pages.TimeConfPage.getInstance();
      } else if (requestedMode === this.self.MODE_ORDER_MATCONF) {
        retval = AssetManagement.customer.view.pages.MatConfPage.getInstance();
      } else if (requestedMode === this.self.MODE_COMPONENT) {
        retval = AssetManagement.customer.view.pages.ComponentPage.getInstance();
      } else if (requestedMode === this.self.MODE_NEW_ORDER) {
        retval = AssetManagement.customer.view.pages.NewOrderPage.getInstance();
      } else if (requestedMode === this.self.MODE_NEW_NOTIF) {
        retval = AssetManagement.customer.view.pages.NewNotifPage.getInstance();
      } else if (requestedMode === this.self.MODE_OBJECT_LIST) {
        retval = AssetManagement.customer.view.pages.ObjectListPage.getInstance();
      } else if (requestedMode === this.self.MODE_BANF_EDIT) {
        retval = AssetManagement.customer.view.pages.BanfEditPage.getInstance();
      } else if (requestedMode === this.self.MODE_MEAS_DOC_EDIT) {
        retval = AssetManagement.customer.view.pages.MeasDocEditPage.getInstance();
      } else if (requestedMode === this.self.MODE_MATERIAL_LIST) {
        retval = AssetManagement.customer.view.pages.MaterialListPage.getInstance();
      } else if (requestedMode === this.self.MODE_EQUI_CHANGE_POSITION) {
        retval = AssetManagement.customer.view.pages.EquiChangePositionPage.getInstance();
      } else if (requestedMode === this.self.MODE_EQUIPMENT_EDIT) {
        retval = AssetManagement.customer.view.pages.EquipmentEditPage.getInstance();
      } else if (requestedMode === this.self.MODE_CUSTOMER_LIST) {
        retval = AssetManagement.customer.view.pages.CustomerListPage.getInstance();
      } else if (requestedMode === this.self.MODE_CUSTOMER_DETAILS) {
        retval = AssetManagement.customer.view.pages.CustomerDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_TIME_TABLE_LIST) {
        retval = AssetManagement.customer.view.pages.TimeTableListPage.getInstance();
      } else if (requestedMode === this.self.MODE_CALENDAR) {
        retval = AssetManagement.customer.view.pages.CalendarPage.getInstance();
      } else if (requestedMode === this.self.MODE_FUNCLOC_EDIT) {
        retval = AssetManagement.customer.view.pages.FuncLocEditPage.getInstance();
      } else if (requestedMode === this.self.MODE_LOGINSCREEN) {
        retval = AssetManagement.customer.view.pages.LoginPage.getInstance();
      } else if (requestedMode === this.self.MODE_INVENTORY_LIST) {
        retval = AssetManagement.customer.view.pages.InventoryListPage.getInstance();
      } else if (requestedMode === this.self.MODE_INVENTORY_DETAIL) {
        retval = AssetManagement.customer.view.pages.InventoryDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_SDORDER_LIST) {
        retval = AssetManagement.customer.view.pages.SDOrderListPage.getInstance();
      } else if (requestedMode === this.self.MODE_CHECKLIST) {
        retval = AssetManagement.customer.view.pages.ChecklistPage.getInstance();
      } else if (requestedMode === this.self.MODE_SDORDER_DETAILS) {
        retval = AssetManagement.customer.view.pages.SDOrderDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_NEW_SDORDER) {
        retval = AssetManagement.customer.view.pages.NewSDOrderPage.getInstance();
      } else if (requestedMode === this.self.MODE_EXT_CONF) {
        retval = AssetManagement.customer.view.pages.ExtConfPage.getInstance();
      } else if (requestedMode === this.self.MODE_NEW_EQUI) {
        retval = AssetManagement.customer.view.pages.NewEquiPage.getInstance();
      } else if (requestedMode === this.self.MODE_HIST_ORDER_DETAIL) {
        retval = AssetManagement.customer.view.pages.HistOrderDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_HIST_NOTIF_DETAIL) {
        retval = AssetManagement.customer.view.pages.HistNotifDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_GOOGLEMAPS) {
        retval = AssetManagement.customer.view.pages.GoogleMapsPage.getInstance();
      } else if (requestedMode === this.self.MODE_OBJ_STRUCTURE) {
        retval = AssetManagement.customer.view.pages.ObjectStructurePage.getInstance();
      } else if (requestedMode === this.self.MODE_MEASPOINT_LIST) {
        retval = AssetManagement.customer.view.pages.MeasPointListPage.getInstance();
      } else if (requestedMode === this.self.MODE_INTEGRATEDCONF) {
        retval = AssetManagement.customer.view.pages.IntegratedConfPage.getInstance();
      } else if (requestedMode === this.self.MODE_REPORTS_PREVIEW_PAGE) {
        retval = AssetManagement.customer.modules.reportFramework.view.page.ReportsPreviewPage.getInstance();
      } else if (requestedMode === this.self.MODE_REPORTS_PREVIEW_PAGE_IOS) {
        retval = AssetManagement.customer.modules.reportFramework.view.page.ReportsPreviewPageiOS.getInstance();
      } else if (requestedMode === this.self.MODE_WE_LIST) {
        retval = AssetManagement.customer.view.pages.WeListPage.getInstance();
      } else if (requestedMode === this.self.MODE_PO_DETAIL) {
        retval = AssetManagement.customer.view.pages.PoDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_GOODS_MOVEMENT_LIST) {
        retval = AssetManagement.customer.view.pages.GoodsMovementListPage.getInstance();
      } else if (requestedMode === this.self.MODE_WA_CREATE) {
        retval = AssetManagement.customer.view.pages.WaCreatePage.getInstance();
      } else if (requestedMode === this.self.MODE_WA_CREATE_MATERIAL) {
        retval = AssetManagement.customer.view.pages.WaCreateMaterialPage.getInstance();
      } else if (requestedMode === this.self.MODE_MAT_STOCK) {
        retval = AssetManagement.customer.view.pages.MatListPage.getInstance();
      } else if (requestedMode === this.self.MODE_DELIVERY) {
        retval = AssetManagement.customer.view.pages.DeliveryListPage.getInstance();
      } else if (requestedMode === this.self.MODE_PICKUP) {
        retval = AssetManagement.customer.view.pages.PickupListPage.getInstance();
      } else if (requestedMode === this.self.MODE_REPORT) {
        retval = AssetManagement.customer.view.pages.DeliveryDetailPage.getInstance();
      } else if (requestedMode === this.self.MODE_SELF_DISPOSITION) {
        retval = AssetManagement.customer.view.pages.SelfDispositionPage.getInstance();
      }
    }
    catch
      (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getViewOfRequestedMode of BaseNavigationController', ex);
    }

    return retval;
  },

  //this method will check, if the requested target can be called in the current state
  //if the reqested target may not be accepted directly true will be returned
  //do some additional steps on the navigation, if neccessary, to undo the current navigation
  filterForUndesiredTargets: function (newMode) {
    var retval = false;

    try {
      var ac = AssetManagement.customer.core.Core.getAppConfig();
      var userIsLoggedIn = ac && ac.isUserLoggedIn();

      //#1 check - TYPE LOGIN PAGE

      //the login page may never be accessed via backward navigation (forward also, but this case is not possible)
      //another login results in an invalid application state
      //so if the target is the login page, inform the user, he is going to be logged out, if he wishes to continue to it
      if (userIsLoggedIn && newMode === this.self.MODE_LOGINSCREEN) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request declined. User tried to access login page by go back. Show a confirm dialog for logoff.', true);

        //show a confirmation dialog, if accepted, perform the logout (which will redirect to the login page itself)
        var callback = function (confirmed) {
          try {
            if (confirmed === true) {
              AssetManagement.customer.controller.ClientStateController.logOutUser();
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside filterForUndesiredTargets of BaseNavigationController', ex);
          }
        };

        var dialogArgs = {
          title: Locale.getMsg('caution'),
          icon: 'resources/icons/logout.png',
          message: Locale.getMsg('navigationWillLogOutContinue'),
          callback: callback,
          scope: this
        };

        this.requestShowDialog(this.self.CONFIRM_DIALOG, dialogArgs);

        retval = true;
      } else if (newMode == this.self.MODE_SETTINGS) {
        retval = false;
      } else if (newMode !== this.self.MODE_LOGINSCREEN && !userIsLoggedIn) {
        //#2 check - NOT LOGGED IN
        //if the user is not logged in and triggered a back or forward requests not guiding to login page
        //the navigation must be inhibitted, abort and send the user to the login page
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request declined. User is not logged in and tried to go back or forward.', true);
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Tiggering an user logout.', true);

        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('invalidNavigation') + ' ' + Locale.getMsg('pleaseLogonFirst'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);

        AssetManagement.customer.controller.ClientStateController.logOutUser();
        retval = true;
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside filterForUndesiredTargets of BaseNavigationController', ex);
    }

    return retval;
  },

  //checks for unsaved data on the current active dialog or page
  //if there are unsaved changes, the user will be questioned, if these changes may be discarded
  //callback will be called with a boolean, indicating if there are no relevant unsaved changes to the user
  checkForAndHandleUnsavedChangesCase: function (callback) {
    try {
      if (!callback)
        return;

      var noRelevantChanges = true;

      var currentActiveElement = this._currentShownDialogs.pop();

      if (currentActiveElement)
        this._currentShownDialogs.push(currentActiveElement);
      else
        currentActiveElement = this.getCurrentPage();

      if (currentActiveElement) {
        var currentActiveController = currentActiveElement.getController();

        if (currentActiveController.hasUnsavedChanges() === true) {
          noRelevantChanges = false;

          AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Currently active element has unsaved changes.', true);

          //prepare confirmation dialog
          var confirmDialog = AssetManagement.customer.view.dialogs.ConfirmationDialog.getInstance();

          var showConfirmDialogCallback = function () {
            try {
              confirmDialog.show();
            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPageRequested of BaseNavigationController', ex);
              callback.call(this, false);
            }
          };

          var confirmationCallback = function (confirmed) {
            try {
              if (confirmed === true) {
                noRelevantChanges = true;
                AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Continuing request processing. User dismissed current active element\'s changes', true);
              } else {
                AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Request cancelled due to user relevant unsaved changes.', true);
              }

              callback.call(this, noRelevantChanges);
            } catch (ex) {
              AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPageRequested of BaseNavigationController', ex);
              callback.call(this, false);
            }
          };

          var confirmDialogArgs = {
            icon: 'resources/icons/question.png',
            message: Locale.getMsg('proceedUnsaved'),
            callback: confirmationCallback,
            scope: this
          };

          //request the dialog
          confirmDialog.getController().requestDialog(confirmDialogArgs, showConfirmDialogCallback, this, null);
          return;
        } else {
          callback.call(this, noRelevantChanges);
        }
      } else {
        callback.call(this, noRelevantChanges);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkForAndHandleUnsavedChangesCase of BaseNavigationController', ex);
      callback.call(this, false);
    }
  },

  changeMode: function (newMode, argumentsObject, isGoBack, removeCurrentModeFromStack) {
    try {
      var nextPage = this.getViewOfRequestedMode(newMode);

      if (!nextPage) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Cancelling request. Page for the requested mode could not be found.', true);

        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('unknownPageRequested'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        return;
      }

      if (nextPage.self.getInstance)
        this._shownSingletonViewsOfThisSession.add(newMode, nextPage);

      if (!argumentsObject)
        argumentsObject = {};

      this.navigateToNextPage(nextPage, argumentsObject, isGoBack, removeCurrentModeFromStack);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside changeMode of BaseNavigationController', ex);
      this._processingRequest = false;
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }
  },

  navigateToNextPage: function (nextPage, argumentsObject, isGoBack, removeCurrentModeFromStack) {
    try {
      var pagesController = nextPage.getController();

      var pageReadyCallback = function (success) {
        try {
          if (success === true) {
            AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Target page is ready.', true);
            this.performNavigate(nextPage, isGoBack, removeCurrentModeFromStack);
          } else {
            AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Cancelling request. Target page could not be prepared. Triggering a go back.', true);

            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(pagesController.getDataLoadingErrorMessage(), false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);

            // defer setting back the request in process to give the framework a time window to perform change of the DOM
            Ext.defer(function () {
              this._processingRequest = false;
            }, 250, this);

            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToNextPage of BaseNavigationController', ex);
          this._processingRequest = false;
          AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
      };

      AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Requesting target page.', true);
      pagesController.requestPage(pageReadyCallback, this, argumentsObject);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToNextPage of BaseNavigationController', ex);
      this._processingRequest = false;
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }
  },

  performNavigate: function (nextPage, isGoBack, removeCurrentModeFromStack) {
    try {

      if (nextPage) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Setting target page as new active page.', true);

        if (removeCurrentModeFromStack > 0) {
          var count = removeCurrentModeFromStack;

          do {
            this._modeHistory.removeAt(this._historyPointer);
            this._historyPointer--;
            count--;
          } while (count > 0);
        }

        //indicate navigation complete by moving history pointer
        isGoBack ? this._historyPointer-- : this._historyPointer++;

        //remove the focus of the former focused control
        //this also closes the soft keyboard
        AssetManagement.customer.helper.FocusHelper.setFocusToNone();

        this.getContentPanel().layout.setActiveItem(nextPage);
        nextPage.fireEvent('activated');
      }
    } catch (ex) {

      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performNavigate of NavigationController', ex);

    } finally {

      this._processingRequest = false;

      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

    }
  },

  beginShowDialog: function (dialogType, argumentsObject) {
    try {
      this._processingRequest = true;
      AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

      var dialogToShow = this.getViewOfRequestedDialogType(dialogType);

      if (dialogToShow) {
        if (dialogToShow.self.getInstance)
          this._shownSingletonViewsOfThisSession.add(dialogType, dialogToShow);

        var dialogsController = dialogToShow.getController();

        var dialogReadyCallback = function (success) {
          try {
            if (success === true) {
              AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Dataloading of requested dialog completed.', true);

              this.performShowDialog(dialogToShow);
            } else {
              AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Cancelling request. Dataloading of requested dialog failed. Triggering a go back.', true);

              var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(dialogsController.getDataLoadingErrorMessage(), false, 2);
              AssetManagement.customer.core.Core.getMainView().showNotification(notification);

              this._processingRequest = false;
              AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginShowDialog of BaseNavigationController', ex);
            this._processingRequest = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
          }
        };

        var me = this;

        var onHideCallback = function () {
          try {
            me.onDialogHidden.call(me, dialogToShow);
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginShowDialog-onHideCallback of BaseNavigationController', ex);
          }
        };

        if (this.getCurrentPage())
          dialogsController.setOwner(this.getCurrentPage().getController());

        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Requesting datapreload for requested dialog.', true);
        dialogsController.requestDialog(argumentsObject, dialogReadyCallback, this, onHideCallback);
      } else {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Cancelling request. Dialog of the requested type could not be found.', true);

        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('unknowDialogRequested'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);

        this._processingRequest = false;
        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beginShowDialog of BaseNavigationController', ex);
      this._processingRequest = false;
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }
  },

  getViewOfRequestedDialogType: function (dialogType) {
    var retval = null;

    try {
      if (dialogType === this.self.ALERT_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.AlertDialog.getInstance();
      } else if (dialogType === this.self.CONFIRM_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.ConfirmationDialog.getInstance();
      } else if (dialogType === this.self.MATERIALPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.MaterialPickerDialog.getInstance();
      } else if (dialogType === this.self.NOTIFITEM_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.NotifItemDialog.getInstance();
      } else if (dialogType === this.self.NOTIFCAUSE_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.NotifCauseDialog.getInstance();
      } else if (dialogType === this.self.NOTIFACTIVITY_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.NotifActivityDialog.getInstance();
      } else if (dialogType === this.self.NOTIFTASK_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.NotifTaskDialog.getInstance();
      } else if (dialogType === this.self.LONGTEXT_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.LongtextDialog.getInstance();
      } else if (dialogType === this.self.INTERNNOTE_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.InternNoteDialog.getInstance();
      } else if (dialogType === this.self.OPERATION_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OperationDialog.getInstance();
      } else if (dialogType === this.self.SDORDER_ITEM_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.SDOrderItemDialog.getInstance();
      } else if (dialogType === this.self.FUNCLOCPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.FuncLocPickerDialog.getInstance();
      } else if (dialogType === this.self.EQUIPMENTPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.EquipmentPickerDialog.getInstance();
      } else if (dialogType === this.self.SYNC_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.SynchronizationDialog.getInstance();
      } else if (dialogType === this.self.PARTNERPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.PartnerPickerDialog.getInstance();
      } else if (dialogType === this.self.CUSTOMERPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.CustomerPickerDialog.getInstance();
      } else if (dialogType === this.self.CUSTOMERPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.CustomerPickerDialog.getInstance();
      } else if (dialogType === this.self.CUSTOMERPICKER_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.CustomerPickerDialog.getInstance();
      } else if (dialogType === this.self.SEND_LOGFILES_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.SendLogFilesDialog.getInstance();
      } else if (dialogType === this.self.BARCODE_SCAN_DIALOG) {
        retval = AssetManagement.customer.modules.barcode.BarcodeScanDialog.getInstance();
      } else if (dialogType === this.self.BARCODE_RETRY_DIALOG) {
        retval = AssetManagement.customer.modules.barcode.BarcodeRetryDialog.getInstance();
      } else if (dialogType === this.self.OBJECTLISTEDIT_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.ObjectListEditDialog.getInstance();
      } else if (dialogType === this.self.CLASSEDIT_MULTICHECKBOX_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.MultiValueCheckboxDialog.getInstance();
      } else if (dialogType === this.self.CLASSEDIT_SINGULARCHECKBOX_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.SingularValueCheckboxDialog.getInstance();
      } else if (dialogType === this.self.CLASSEDIT_APPENDCHECKBOX_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.AppendCheckboxDialog.getInstance();
      } else if (dialogType === this.self.CLASSEDIT_SINGULARLINE_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.SingularValueLineDialog.getInstance();
      } else if (dialogType === this.self.SCENARIO_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.ScenarioDialog.getInstance();
      } else if (dialogType === this.self.BUILD_REPORT_DIALOG) {
        retval = AssetManagement.customer.modules.reportFramework.view.dialog.BuildReportDialog.getInstance();
      } else if (dialogType === this.self.REPORT_SIGNATURE_DIALOG) {
        retval = AssetManagement.customer.modules.reportFramework.view.dialog.ReportSignatureDialog.getInstance();
      } else if (dialogType === this.self.BUILD_REPORT_TYPE_DIALOG) {
        retval = AssetManagement.customer.modules.reportFramework.view.dialog.ReportTypeDialog.getInstance();
      } else if (dialogType === this.self.REPORT_COSTS) {
        retval = AssetManagement.customer.modules.reportFramework.view.dialog.ReportCostsDialog.getInstance();
      } else if (dialogType === this.self.OBJECT_STATUS_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.ObjectStatusDialog.getInstance();
      } else if (dialogType === this.self.SEARCH_DIALOG) {
        retval = AssetManagement.customer.modules.search.SearchDialog.getInstance();
      } else if (dialogType === this.self.ONLINE_MAT_SEARCH_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OnlineMaterialSearchDialog.getInstance();
      } else if (dialogType === this.self.SINGLE_TEXT_LINE_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.SingleTextLineInputDialog.getInstance();
      } else if (dialogType === this.self.ONLINE_PO_GET_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OnlinePoGetDialog.getInstance();
      } else if (dialogType === this.self.ONLINE_GOODS_MOVEMENT_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OnlineGoodsMovementDialog.getInstance();
      } else if (dialogType === this.self.ONLINE_BO_CHECK_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OnlineBoCheckDialog.getInstance();
      } else if (dialogType === this.self.CHANGE_LGORT_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.LgortPickerDialog.getInstance();
      } else if (dialogType === this.self.ONLINE_STOCK_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OnlineStockDialog.getInstance();
      } else if (dialogType === this.self.ONLINE_DISPOSITION_DIALOG) {
        retval = AssetManagement.customer.view.dialogs.OnlineDispositionDialog.getInstance();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getViewOfRequestedDialogType of BaseNavigationController', ex);
    }

    return retval;
  },

  performShowDialog: function (toShow) {
    try {
      if (toShow) {
        AssetManagement.customer.helper.OxLogger.logMessage('[NAVIGATION] Showing requested dialog.', true);

        this._currentShownDialogs.push(toShow);

        this.adjustDialogToViewPort(toShow);

        //show it
        toShow.show();
      }

      // defer setting back the request in process to give the framework a time window to perform change of the DOM
      Ext.defer(function () {
        this._processingRequest = false;
      }, 250, this);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performShowDialog of BaseNavigationController', ex);
      this._processingRequest = false;
    } finally {
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }
  },

  onDialogHidden: function (hiddenDialog) {
    try {
      var logMessage = '[NAVIGATION] A dialog has been closed/hidden.';

      Ext.Array.remove(this._currentShownDialogs, hiddenDialog);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDialogHidden of BaseNavigationController', ex);
    }
  },

  //adjusts a dialog to the current view port configuration
  //resizes it if supported and recenters it
  adjustDialogToViewPort: function (dialog) {
    try {
      var mainView = AssetManagement.customer.core.Core.getMainView();

      //only apply dynamic width, if the dialog supports it
      if (dialog.supportsDynamicWidth) {
        //use view port size minus 10px for each side
        var availableWidth = (mainView ? mainView.getWidth() : screen.width) - 20;
        dialog.setWidth(availableWidth);
      }

      //only apply dynamic height, if the dialog supports it
      if (dialog.supportsDynamicHeight) {
        //use view port size minus 10px for each side
        var availableHeight = (mainView ? mainView.getHeight() : screen.height) - 20;
        dialog.setHeight(availableHeight);
      }

      //center the dialog
      dialog.center();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adjustDialogToViewPort of BaseNavigationController', ex);
    }
  },

  removeSinglePageFromHistoryStack: function () {
    try {
      this._modeHistory.removeAt(this._historyPointer);
      this._historyPointer--;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside removePageFromHistoryStack of BaseNavigationController', ex)
    }
  },

  initRunning: function () {
    var retval = null;
    try {
      var me = this;
      retval = this._requestRunning;

      this._requestRunning = true;
      setTimeout(function () {
        me._requestRunning = false;
      }, 100);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initRunning of BaseNavigationController', ex)
    }

    return retval;
  }
});