Ext.define('AssetManagement.base.controller.menus.BaseAlternateOptionsMenuController', {
	extend: 'AssetManagement.customer.controller.menus.OxMenuController',



    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'Ext.util.HashMap'
    ],

    //private
	_optionToElementIdMap: null,
	_currentSupportedOptions: null,

	constructor: function(config) {
		this.callParent(arguments);

		try {
		    this.initializeOptionToElementIdMap();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseAlternateOptionsMenuController', ex);
		}
	},

	//public
	setMenuItems: function(arrayOfOptions) {
		try {
			this._currentSupportedOptions = arrayOfOptions;

			//convert options to ids
			var arrayOfOptionsAsIds = [];

			if(arrayOfOptions) {
				Ext.Array.each(arrayOfOptions, function(option) {
					var id = this._optionToElementIdMap.get(option);

					if(id)
						arrayOfOptionsAsIds.push(id);
				}, this);
			}

			this.getView().setMenuItems(arrayOfOptionsAsIds);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setOptionsMenuItems of BaseAlternateOptionsMenuController', ex);
		}
	},

	onMenuItemSelected: function(menuItem) {
    	try {
    		var selectedOption = this.getOptionOfMenuItem(menuItem);
    		AssetManagement.customer.core.Core.getCurrentPage().getController().onOptionMenuItemSelected(selectedOption);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMenuItemSelected of BaseAlternateOptionsMenuController', ex);
		}
	},

	//private
	initializeOptionToElementIdMap: function() {
		try {
			this._optionToElementIdMap = Ext.create('Ext.util.HashMap');

			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH, 'searchMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_EDIT, 'editMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_NEW_SDORDER, 'newSDOrderMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_DUPLICATE, 'duplicateMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_CLASS, 'classMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_ADD, 'addMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_MEASPOINT, 'measpointsMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT, 'longtextMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST, 'longtextExistMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_EQUICHANGE, 'equiChangeMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE, 'saveMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_DELETE, 'deleteMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_RFID, 'rfidMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_REPORT, 'reportMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_PRINT, 'printMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM, 'newItemMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_NEWORDER, 'newOrderMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_NEWNOTIF, 'newNotifMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_SHOW_MY_MAPS_POSITION, 'showMyMapsPositionMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_OBJ_STRUCTURE, 'objStructureMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_BARCODE, 'barcodeMenuItemAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH, 'clearSearchButtonAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_PRINT_MULTIPLE_REPORTS, 'printMultipleReportsAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_SIGNING_BUTTON, 'signingReportsButtonAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE_REPORTS, 'saveReportsButtonAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_DOWNLOAD, 'downloadButtonAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_BANF_EDIT, 'banfButtonAOM');
			this._optionToElementIdMap.add(AssetManagement.customer.controller.ToolbarController.OPTION_INTERNNOTE, 'internNoteButtonAOM');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeOptionToElementIdMap of BaseAlternateOptionsMenuController', ex);
		}
	},

	getOptionOfMenuItem: function(menuItem) {
		var retval = undefined;

		try {
			var toSearchFor = menuItem.id;

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(toSearchFor) && this._optionToElementIdMap) {
				this._optionToElementIdMap.each(function(key, value) {
					if(toSearchFor === value) {
						retval = key;
						return false;
					}
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOptionOfMenuItem of BaseAlternateOptionsMenuController', ex);
		}

		return retval;
	},

	onBarcodeScanned: function(scanResult) {
		try {
			AssetManagement.customer.core.Core.getCurrentPage().getController().onOptionMenuItemSelected(AssetManagement.customer.controller.ToolbarController.OPTION_BARCODE, scanResult);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBarcodeScanned of BaseAlternateOptionsMenuController', ex);
		}
    }
});