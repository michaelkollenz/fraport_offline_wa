Ext.define('AssetManagement.base.controller.BaseMainViewController', {
  extend: 'Ext.app.ViewController',


  requires: [
    'Ext.menu.Manager',
    'AssetManagement.customer.controller.ClientStateController',
    'AssetManagement.customer.view.dialogs.CancelableProgressDialog',
    'AssetManagement.customer.sync.SyncManager',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.controller.NavigationController'
  ],

  _mainToolbarController: null,

  onViewPortSizeChanged: function (mainView, width, height, oldWidth, oldHeight, eOpts) {
    try {
      AssetManagement.app.getController('AssetManagement.customer.controller.NavigationController').applyGlobalSizeChangeOnShownDialogs();

      var toAdjust = mainView.getCurrentVisibleNotifications();

      if (toAdjust && toAdjust.length > 0) {
        var marginBottom = 10;
        var marginOnX = 30;
        var marginBetweenFrames = 6;
        var offsetY = marginBottom - marginBetweenFrames;

        Ext.Array.each(toAdjust, function (frameToAdjust, index, array) {
          offsetX = width - frameToAdjust.getWidth();
          frameToAdjust.setX(offsetX/2);
          offsetY += frameToAdjust.getHeight() + marginBetweenFrames;
          frameToAdjust.setY(height - offsetY);
        }, this);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onViewPortSizeChanged of BaseMainViewController', ex);
    }
  },

  getMainToolbarController: function () {
    try {
      if (this._mainToolbarController === null) {
        this._mainToolbarController = this.getView().queryById('optionsToolbar').getController();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMainToolbarController of BaseMainViewController', ex);
    }

    return this._mainToolbarController;
  },

  onLocaleChanged: function () {
    try {
      this.getView().rebuildMainMenu();
      this.getView().rebuildStatusBar();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLocaleChanged of BaseMainViewController', ex);
    }
  },

  showOptionsToolbar: function () {
    try {
      this.getView().showOptionsToolbar();
      this.enableMainMenu();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showOptionsToolbar of BaseMainViewController', ex);
    }
  },

  hideOptionsToolbar: function () {
    try {
      this.disableMainMenu();
      this.getView().hideOptionsToolbar();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideOptionsToolbar of BaseMainViewController', ex);
    }
  },

  showStatusBar: function (doAlsoUpdate) {
    try {
      if (doAlsoUpdate) {
        this.updateStatusBar();
      }

      this.getView().showStatusBar();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showStatusBar of BaseMainViewController', ex);
    }
  },

  hideStatusBar: function () {
    try {
      this.getView().hideStatusBar();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideStatusBar of BaseMainViewController', ex);
    }
  },

  updateStatusBar: function () {
    try {
      this.getView().updateStatusBar();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updateStatusBar of BaseMainViewController', ex);
    }
  },

  enableMainMenu: function () {
    try {
      this.getMainToolbarController().setMainMenuButtonVisible(true);
      this.getView().enableMainMenuPanel();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside enableMainMenu of BaseMainViewController', ex);
    }
  },

  disableMainMenu: function () {
    try {
      this.getMainToolbarController().setMainMenuButtonVisible(false);
      this.getView().disableMainMenuPanel();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside disableMainMenu of BaseMainViewController', ex);
    }
  },

  showMainMenu: function () {
    try {
      this.getView().showMainMenuPanel();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showMainMenu of BaseMainViewController', ex);
    }
  },

  hideMainMenu: function () {
    try {
      this.getView().hideMainMenuPanel();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hideMainMenu of BaseMainViewController', ex);
    }
  },

  toggleMainMenuVisibility: function () {
    try {
      this.getView().toggleMainMenuVisibility();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside toggleMainMenuVisibility of BaseMainViewController', ex);
    }
  },

  onMainMenuItemClick: function (menu, item, e, eOpts) {
    try {

      if (!item)
        return;


      var homeScreenPageViewModel = this.getViewModel();
      var selectedLgort = homeScreenPageViewModel ? homeScreenPageViewModel.get('selectedLgort'): null;

      if (item.itemId === 'sync') {
        this.performSync();
      } else if (item.itemId === 'orderlist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_LIST);
      } else if (item.itemId === 'notiflist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_LIST);
      } else if (item.itemId === 'equipmentlist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_LIST);
      } else if (item.itemId === 'funcloclist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_LIST);
      } else if (item.itemId === 'measPointlist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_MEASPOINT_LIST);
      }
      else if (item.itemId === 'materiallist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_MATERIAL_LIST);
      } else if (item.itemId === 'banf') {
        //if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ext_scen_active') !== 'X')
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_BANF_EDIT);
        //else
        //	AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_SDORDER);
      } else if (item.itemId === 'sdorderlist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SDORDER_LIST);
        //		} else if(item.itemId === 'timeconf') {
        //     		AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_LIST);
      } else if (item.itemId === 'inventory') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_INVENTORY_LIST);
      } else if (item.itemId === 'customerlist') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CUSTOMER_LIST);
      } else if (item.itemId === 'schedule') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SCHEDULE);
      } else if (item.itemId === 'calendar') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CALENDAR);
      } else if (item.itemId === 'settings') {
        // AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SETTINGS);
      } else if (item.itemId === 'extconf') {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EXT_CONF);
      } else if (item.itemId === 'stacktrace') {
        this.onSendLogFilesClicked();
      } else if (item.itemId === 'logoff') {
        this.onLogoutClicked();
      } else if(item.itemId === 'WE') {
          if (selectedLgort) {
              //argumentobject needs to be transfered as empyt object, and not undefined.
              //If it was undefined, it would not be able to modify it for GoBack compatibility int he view.
              //For further explanation, see
              AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WE_LIST, { });
        }else {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        }
      } else if(item.itemId === 'WA') {
        if (selectedLgort) {
          AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WA_CREATE);
        } else {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        }
      } else if(item.itemId === 'IV') {
          AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_INVENTORY_LIST);
      }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMainMenuItemClick of BaseMainViewController', ex);
    }
  },

  performSync: function (callback, callbackScope, stayOnCurrentPage, uploadOnly) {
    try {
      //before the sync will be started, return to the home screen by default
      //to ensure, that this navigation will be completed, the request for showing the sychronization dialog will be postponed
      if (!stayOnCurrentPage)
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HOMESCREEN);

      AssetManagement.customer.controller.ClientStateController.setSyncIsRunning(true);

      var me = this;

      var successCallback = function () {
        try {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('synchronizationSuccessful'), true, 0);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);

          //refresh menu in MainView and homescreen
          me.getView().refresh();
          AssetManagement.customer.view.pages.HomeScreenPage.getInstance().getController().refreshView();

          if (callback)
            callback.call(callbackScope, true);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSync of BaseMainViewController', ex);

          if (callback)
            callback.call(callbackScope, false);
        } finally {
          AssetManagement.customer.controller.ClientStateController.setSyncIsRunning(false);
        }
      };

      var cancelCallback = function () {
        try {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('synchronizationCancelled'), true, 1);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);

          if (callback)
            callback.call(callbackScope, false);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSync of BaseMainViewController', ex);

          if (callback)
            callback.call(callbackScope, false);
        } finally {
          AssetManagement.customer.controller.ClientStateController.setSyncIsRunning(false);
        }
      };

      var syncDialogArguments = {
        successCallback: successCallback,
        cancelCallback: cancelCallback,
        uploadOnly: uploadOnly === true ? uploadOnly : false,
        owner: this
      };

      //defer the dialog request to ensure the homescreen navigation completes first
      Ext.defer(function () {
        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SYNC_DIALOG, syncDialogArguments);
      }, 100, this);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSync of BaseMainViewController', ex);

      if (callback)
        callback.call(callbackScope, false);
    }
  },

  onLogoutClicked: function () {
    try {
      //ask the user, if he really want's to log out
      var callback = function (confirmed) {
        try {
          if (confirmed === true) {
            AssetManagement.customer.controller.ClientStateController.logOutUser();
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMainMenuItemClick of BaseMainViewController', ex);
        }
      };

      var dialogArgs = {
        title: Locale.getMsg('logout'),
        icon: 'resources/icons/logout.png',
        message: Locale.getMsg('logoutQuestion'),
        alternateConfirmText: Locale.getMsg('toLogout'),
        alternateDeclineText: Locale.getMsg('dialogCancel'),
        callback: callback,
        scope: this
      };

      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMainMenuItemClick of BaseMainViewController', ex);
    }
  },

  onSendLogFilesClicked: function () {
    try {
      this.sendLogFiles();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSendLogFilesClicked of BaseMainViewController', ex);
    }
  },

  sendLogFiles: function () {
    try {
      var successCallback = function (success) {
        try {
          var notification = null;

          if (success === true) {
            notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sendLogFilesSuccessful'), true, 0);
          } else {
            notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sendLogFilesFailed'), false, 2);
          }

          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendLogFiles of BaseMainViewController', ex);
        }
      };

      var cancelCallback = function () {
        try {
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sendLogFilesCancelled'), true, 1);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendLogFiles of BaseMainViewController', ex);
        }
      };

      var sendLogFilesDialogArguments = {
        successCallback: successCallback,
        cancelCallback: cancelCallback,
        owner: this
      };

      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEND_LOGFILES_DIALOG, sendLogFilesDialogArguments);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside sendLogFiles of BaseMainViewController', ex);
    }
  },

  onInfoButtonClick: function () {
    try {
      var user = AssetManagement.customer.core.Core.getAppConfig().getUserId();
      var system = AssetManagement.customer.core.Core.getAppConfig().getSyncSystemId() + '/' + AssetManagement.customer.core.Core.getAppConfig().getMandt();
      var lastSync = AssetManagement.customer.utils.DateTimeUtils.getFullTimeForDisplay(AssetManagement.customer.core.Core.getAppConfig().getLastSync());

      var dialogArgs = {
        title: '',
        icon: 'resources/icons/intern_note.png',
        asHtml: true,
        hideDeclineButton: true,
        alternateConfirmText: Locale.getMsg('close'),
        message: Locale.getMsg('user') + ': ' + user + '<br /><br />'
        + Locale.getMsg('system') + ': ' + system + '<br /><br />' + Locale.getMsg('lastSync') + ': '
        + lastSync
      };

      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onInfoButtonClick of BaseMainViewController', ex);
    }
  }
});