Ext.define('AssetManagement.base.controller.pages.BaseSDOrderListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.manager.SDOrderManager',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},

	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_ADD) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_SDORDER, { });
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseSDOrderListPageController', ex)
		}
	},
		
	//private
	onCreateOptionMenu: function() {
		try {
		    this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH, AssetManagement.customer.controller.ToolbarController.OPTION_ADD ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseSDOrderListPageController', ex)
		}        
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
		    this.loadListStore();
		
		    this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseSDOrderListPageController', ex)
		}
	},
		
	loadListStore: function() {
		try {
			var sdOrderStoreRequest = AssetManagement.customer.manager.SDOrderManager.getSDOrders(true, true);
			this.addRequestToDataBaseQueue(sdOrderStoreRequest, 'sdOrderStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseSDOrderListPageController', ex)
		}
	},
	
	//list handler
	onSDOrderSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SDORDER_DETAILS, { vbeln: record.get('vbeln') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderSelected of BaseSDOrderListPageController', ex)
		}
    },
	
	onOrderSelectedByTabHold: function(sdOrder) {
	},
	
	beforeDataReady: function() {
    	try {
    		//apply the current filter criteria on the store, before it is transferred into the view
    		this.applyCurrentFilterCriteriasToStore();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseSDOrderListPageController', ex);
		}
	},
	
    applyCurrentFilterCriteriasToStore: function() {
    	try {
			this.getView().getSearchValue();

			var sdOrderToSearch = this.getViewModel().get('searchSDOrder');
			var sdOrderStore = this.getViewModel().get('sdOrderStore');
			
			if(sdOrderStore) {
				sdOrderStore.clearFilter();
				
				// Bestellnummer
				if(sdOrderToSearch.get('bstnk')) {
					sdOrderStore.filterBy(function (record) {
						try {
						    if (record.get('bstnk').toUpperCase().indexOf(sdOrderToSearch.get('bstnk').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseSDOrderListPageController', ex)
						}
					});
				// Auftragsnummer
				} else if(sdOrderToSearch.get('aufnr')) {
					sdOrderStore.filterBy(function (record) {
						try {
						    if (record.get('aufnr') && record.get('aufnr').toUpperCase().indexOf(sdOrderToSearch.get('aufnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseSDOrderListPageController', ex)
						}
					});
				// Meldungsnummer
				} else if(sdOrderToSearch.get('qmnum')) {
					sdOrderStore.filterBy(function (record) {
						try {
						    if (record.get('qmnum') && record.get('qmnum').toUpperCase().indexOf(sdOrderToSearch.get('qmnum').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseSDOrderListPageController', ex)
						}
					});
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseSDOrderListPageController', ex)
		}
    },
    
    onSearchFieldChange: function() {
    	try {
			this.applyCurrentFilterCriteriasToStore();
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchFieldChange of BaseSDOrderListPageController', ex)
		}
    }
});