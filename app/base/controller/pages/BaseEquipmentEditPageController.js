Ext.define('AssetManagement.base.controller.pages.BaseEquipmentEditPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
    requires: [
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.model.helper.ReturnMessage',
        'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.OxLogger'
    ], 
    
	config: {
		contextReadyLevel: 1
	},
	
	//private
	_saveRunning: false,
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
			
				this.trySaveMasterData();
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseEquipmentEditPageController', ex);
		}
	},
	
	//public
	//@override
	hasUnsavedChanges: function() {
		var retval = false;
	
		try {
			retval = !this.isInputEqualToModel();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasUnsavedChanges of BaseEquipmentEditPageController', ex);
		}
		
		return retval;
	},
		
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
		    this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseEquipmentEditPageController', ex);
		}    
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var equnr = '';
		
			if(argumentsObject['equnr'] && typeof argumentsObject['equnr'] === 'string') {
				equnr = argumentsObject['equnr'];
			} else if(argumentsObject['equipment'] && Ext.getClassName(argumentsObject['equipment']) === 'AssetManagement.customer.model.bo.Equipment') {
				this.getViewModel().set('equipment', argumentsObject['equipment']);
			} else {
				throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.pages.EquipmentEditPageController'
				});
			}
			
			var equnrPassed = equnr !== '';

			if(equnrPassed) {
				this.getEquipment(equnr);
			}
			
			this.waitForDataBaseQueue();
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of EquipmentDetailPageController', ex);
		    this.errorOccurred();
		}
	},
	
	//get equi by equnr
	getEquipment: function(equnr) {
		try {
			var equipmentRequest = AssetManagement.customer.manager.EquipmentManager.getEquipment(equnr, true);
			this.addRequestToDataBaseQueue(equipmentRequest, 'equipment');
				
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipment of BaseEquipmentEditPageController', ex)
		}
	},
	
	trySaveMasterData: function() {
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
			var me = this; 
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						var saveCallback = function(success) {
							try {
								if(success === true) {
									//goBack after save completed
									AssetManagement.customer.core.Core.navigateBack();
									
									Ext.defer(function() { 
										me._saveRunning = false;
									}, 2000, me);
									
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equipmentSaved'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									me._saveRunning = false;
									AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingEquipment'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMasterData of BaseEquipmentEditPageController', ex)
							}
						};
						
						if(me.isInputEqualToModel()) {
							saveCallback(true);
						} else {
							//transfer values from view into equipment
							me.getView().transferViewStateIntoModel();
							
							var equipment = me.getViewModel().get('equipment'); 
						
							var eventId = AssetManagement.customer.manager.EquipmentManager.saveEquipment(equipment);
							
							if(eventId > 0) {
								AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
							} else {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingEquipment'), false, 2);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							}
						}
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseEquipmentEditPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMasterData of BaseEquipmentEditPageController', ex)
		}
	},
	
	/**
	 * checks if the data of the equipment object is complete
	 */
	checkInputValues: function(callback, callbackScope)  {
		try {
			var retval = true;
			var errorMessage = '';
				
			var values = this.getView().getCurrentInputValues(); 
			
			var gwldt = values.gwldt;
			var gwlen = values.gwlen;

			if(gwldt > gwlen) {
				errorMessage = Locale.getMsg('startGuaranteeAfterEnd');
				retval = false;
			}
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			returnFunction.call(this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of EquipmentDetailPageController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	//checks, if there are any differences between the current input state and the model state
	isInputEqualToModel: function() {
		var retval = true;
	
		try {
			var inputValues = this.getView().getCurrentInputValues();
			var equi = this.getViewModel().get('equipment');
			
			if(equi.get('submt') !== inputValues.submt) {
				retval = false;
			} else if(equi.get('eqktx') !== inputValues.eqktx) {
				retval = false;
			} else if(equi.get('invnr') !== inputValues.invnr) {
				retval = false;
			} else if(!AssetManagement.customer.utils.DateTimeUtils.areEqual(equi.get('ansdt'), inputValues.ansdt)) {
				retval = false;
			} else if(!AssetManagement.customer.utils.DateTimeUtils.areEqual(equi.get('gwldt'), inputValues.gwldt)) {
				retval = false;
			} else if(!AssetManagement.customer.utils.DateTimeUtils.areEqual(equi.get('gwlen'), inputValues.gwlen)) {
				retval = false;
			} else if(equi.get('tidnr') !== inputValues.tidnr) {
				retval = false;
			} else if(equi.get('eqfnr') !== inputValues.eqfnr) {
				retval = false;
			} else if(equi.get('stort') !== inputValues.stort) {
				retval = false;
			} else if(equi.get('msgrp') !== inputValues.msgrp) {
				retval = false;
			} else if(equi.get('herst') !== inputValues.herst) {
				retval = false;
			} else if(equi.get('herld') !== inputValues.herld) {
				retval = false;
			} else if(equi.get('typbz') !== inputValues.typbz) {
				retval = false;
			} else if(equi.get('sernr') !== inputValues.sernr) {
				retval = false;
			} else if(equi.get('baumm') !== inputValues.baumm) {
				retval = false;
			} else if(equi.get('baujj') !== inputValues.baujj) {
				retval = false;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isInputEqualToModel of BaseEquipmentEditPageController', ex);
		}
		
		return retval;
	}
});