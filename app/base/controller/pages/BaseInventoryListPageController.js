Ext.define('AssetManagement.base.controller.pages.BaseInventoryListPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',



    requires: [
        'AssetManagement.customer.controller.ToolbarController',
        'AssetManagement.customer.manager.InventoryManager',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils'
    ],

    config: {
        contextReadyLevel: 1,
        isAsynchronous: true
    },

    //mixins: {
    //    handlerMixin: 'AssetManagement.customer.controller.mixins.InventoryListPageControllerMixin'
    //},
    //public
    //@override
    onOptionMenuItemSelected: function (optionID) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_ADD) {
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_INVENTORY);
            } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
                this.onSearchButtonClicked();
            } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH) {
                this.onClearSearchButtonClicked();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseInventoryListPageController', ex)
        }
    },

    //private
    onCreateOptionMenu: function () {
        this._optionMenuItems = new Array();
        try {
            if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('inventory_create') === 'X')
                this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_ADD);

            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseInventoryListPageController', ex)
        }
    },

    //functions for search
    //public
    onSearchButtonClicked: function () {
        try {
            var myModel = this.getViewModel();

            this.manageSearchCriterias();

            var searchDialogArgs = {
                source: myModel.get('inventoryStore'),
                criterias: myModel.get('searchCriterias')
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //private
    //handles initialization of search criterias
    manageSearchCriterias: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');
            //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
            var searchMixin = this.mixins.handlerMixin

            if (!currentSearchCriterias) {
                var gridpanel = Ext.getCmp('inventoryGridPanel');
                var searchArray = [{
                    id: 'physinventory',
                    columnName: 'inventoryDocument',
                    searchFunction: '',
                    position: 1
                },
	            {
	                id: 'plant',
	                columnName: 'plant',
	                searchFunction: '',
	                position: 1
	            },
                {
                    id: 'stgeLoc',
                    columnName: 'storLoc',
                    searchFunction: '',
                    position: 1
                }
                ];
                //var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray);
                var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray, searchMixin);

                myModel.set('searchCriterias', newCriterias);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias'))) {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseInventoryListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/search_cancel.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
    setupInitialSorting: function () {
        try {
            var myModel = this.getViewModel();
            var constrainedEquipments = myModel.get('constrainedInventory');

            //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
                myModel.set('sortProperty', 'mymAgreement');
                myModel.set('sortDirection', 'DESC');
            }

            //apply the current sort rule
            if (constrainedEquipments) {
                constrainedEquipments.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
                ]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseInventoryListPageController', ex);
        }
    },

    //functions for search
    //public
    onSearchApplied: function (searchResult) {
        try {

            this.getViewModel().set('constrainedInventory', searchResult);

            this.setupInitialSorting();

            this.applyCurrentFilterCriteriasToStore();

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of EquipmentListPageController', ex);
        }
    },

    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            this.loadListStore();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseInventoryListPageController', ex)
        }
    },

    loadListStore: function () {
        try {
            var inventoryStoreRequest = AssetManagement.customer.manager.InventoryManager.getInventories(true, true);
            this.addRequestToDataBaseQueue(inventoryStoreRequest, 'inventoryStore');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseInventoryListPageController', ex)
        }
    },


    onInventorySelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_INVENTORY_DETAIL, { inventory: record });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onInventorySelected of BaseInventoryListPageController', ex)
        }
    },

    onInventorySelectedByTabHold: function (inventory) {
    },


    beforeDataReady: function () {
        try {
            //apply the current filter criteria on the store, before it is transferred into the view
            this.applyCurrentFilterCriteriasToStore();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseInventoryListPageController', ex);
        }
    },

    applyCurrentFilterCriteriasToStore: function () {
        try {
            this.getView().getSearchValue();

            var inventoryToSearch = this.getViewModel().get('searchInventory');
            var inventoryStore = this.getViewModel().get('inventoryStore');

            if (inventoryStore) {
                inventoryStore.clearFilter();

                // inventory no.
                if (inventoryToSearch.get('physinventory')) {
                    inventoryStore.filterBy(function (record) {
                        try {
                            if (record.get('physinventory').toUpperCase().indexOf(inventoryToSearch.get('physinventory').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseInventoryListPageController', ex);
                        }
                    });

                    // plant
                } else if (inventoryToSearch.get('plant')) {
                    inventoryStore.filterBy(function (record) {
                        try {
                            if (record.get('plant').toUpperCase().indexOf(inventoryToSearch.get('plant').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseInventoryListPageController', ex);
                        }
                    });
                    // storage location
                } else if (inventoryToSearch.get('stgeLoc')) {
                    inventoryStore.filterBy(function (record) {
                        try {
                            if (record.get('stgeLoc').toUpperCase().indexOf(inventoryToSearch.get('stgeLoc').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseInventoryListPageController', ex);
                        }
                    });

                    //planned date
                } else if (inventoryToSearch.get('planDate')) {
                    inventoryStore.filterBy(function (record) {
                        try {
                            if (record.get('planDate') && record.get('planDate').indexOf(inventoryToSearch.get('planDate').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseInventoryListPageController', ex);
                        }
                    });
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryListPageController', ex)
        }
    },

    onInventoryListPageSearchFieldChange: function () {
        try {
            this.applyCurrentFilterCriteriasToStore();
            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onInventoryListPageSearchFieldChange of BaseInventoryListPageController', ex)
        }
    },

    onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();
            if (!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [];

            this.rowMenu.onCreateContextMenu(options, record);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseInventoryListPageController', ex);
        }
    }
});
