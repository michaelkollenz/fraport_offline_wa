Ext.define('AssetManagement.base.controller.pages.BaseSelfDispositionPageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',


  requires: [
    'AssetManagement.customer.manager.InternalDeliveryManager',
    'AssetManagement.customer.manager.IAReasonManager',
    'AssetManagement.customer.helper.NetworkHelper',
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.OxLogger',
    // 'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
  ],

  config: {
    contextReadyLevel: 1,
    isAsynchronous: true
  },

  // mixins: {
  //     handlerMixin: 'AssetManagement.customer.controller.mixins.SelfDispositionPageControllerMixin'
  // },

  //public
  //@override
  onOptionMenuItemSelected: function (optionID) {
    try {
      if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
        if (this._saveRunning === true)
          return;

        this.trySaveItem();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseSelfDispositionPageController', ex);
    }
  },

  //private
  onCreateOptionMenu: function () {
    try {

      this._optionMenuItems = new Array();
      this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseSelfDispositionPageController', ex);
    }
  },


  onDeleteClicked: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
      var dispositionStore = this.getViewModel().get('dispositionStore');
      dispositionStore.remove(record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCheckItemSelected of BaseSelfDispositionPageController', ex);
    }
  },


  onAddButtonClick: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
      var myModel = this.getViewModel();
      var inputField = Ext.getCmp('iaDispositionNumber');
      var dispositionStore = myModel.get('dispositionStore');
      var dispValue = inputField.getValue();
      var disposition = {value: dispValue};
      if(dispositionStore.find('value', dispValue) === -1){
        dispositionStore.add(disposition);
      }
      inputField.setValue('')
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCheckItemSelected of BaseSelfDispositionPageController', ex);
    }
  },
  //@override
  startBuildingPageContext: function (argumentsObject) {
    try {
      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseSelfDispositionPageController', ex);
    }
  },

  trySaveItem: function () {
    try {
      var me = this;
      var myModel = this.getViewModel();
      var dispositionStore = myModel.get('dispositionStore');

      var ac = AssetManagement.customer.core.Core.getAppConfig();

      var dispositionRequestStore = Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.InternalDelivery',
        autoLoad: false
      });

      dispositionStore.each(function (disposition) {
        dispositionRequestStore.add(Ext.create('AssetManagement.customer.model.bo.InternalDelivery', {
          belnr: disposition.get('value'),
          ia_status: 'L', // Code for open delivery
          usnam: ac.getUserId()
        }));
      });

      var onlineDispositionDialogCallback = function () {
        try {
          dispositionStore.removeAll();
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dispositionSuccess'), true, 0);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          // AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WA_CREATE, null, 1); // delete the last 1 pages from history stack to have a fresh start
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlineDispositionDialogCallback of BaseSelfDispositionPageController', ex);
        }
      };

      var onlineDispositionErrorDialogCallback = function (returnMessageArray) {
        try {
          for (var i = 0; i < returnMessageArray.length; i++) {
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(returnMessageArray[i], false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlineDispositionErrorDialogCallback of BaseSelfDispositionPageController', ex);
        }
      };

      if (dispositionStore && dispositionStore.getCount() > 0) {

        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_DISPOSITION_DIALOG, {
          DispositionItems: dispositionRequestStore,
          successCallback: onlineDispositionDialogCallback,
          cancelCallback: onlineDispositionErrorDialogCallback
        });
      }
    }

    catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseSelfDispositionPageController', ex);
      this._saveRunning = false;
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }

  }

});