Ext.define('AssetManagement.base.controller.pages.BaseMeasDocEditPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',

    config: {
        contextReadyLevel: 1,
        rowMenu: null
    },

    //private
    _saveRunning: false,

    //public
    //@override
    onOptionMenuItemSelected: function (optionID) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
                if (this._saveRunning === true)
                    return;

                this.trySaveMeasDoc();
            } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
                if (this._saveRunning === true)
                    return;

                this.initializeNewMeasDoc();
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseMeasDocEditPageController', ex);
        }
    },

    //protected
    //@override
    onCreateOptionMenu: function () {
        try {
            this._optionMenuItems = [AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM, AssetManagement.customer.controller.ToolbarController.OPTION_SAVE];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseMeasDocEditPageController', ex);
        }
    },

    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            var myModel = this.getViewModel();
            if (Ext.getClassName(argumentsObject['measPoint']) === 'AssetManagement.customer.model.bo.MeasurementPoint') {
                myModel.set('measPoint', argumentsObject['measPoint']);
            }
            if (Ext.getClassName(argumentsObject['measDoc']) === 'AssetManagement.customer.model.bo.MeasurementDoc') {
                myModel.set('measDoc', argumentsObject['measDoc']);
                myModel.set('isEditMode', true);
            }

            if(!myModel.get('measPoint') && !myModel.get('measDoc')){
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.pages.MeasDocEditPageController'
                });
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseMeasDocEditPageController', ex);
            this.errorOccurred();
        }
    },

    beforeDataReady: function () {
        try {
            //set the operation according to the current data
            var myModel = this.getViewModel();

            var curMeasDoc = myModel.get('measDoc');

            //if no matconf is currently set, initialize a new one
            if (!curMeasDoc)
                this.initializeNewMeasDoc(true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of MatConfPageController', ex);
        }
    },

    //will initialize a new material conf. and transfer it into the model
    //the view will be updated by default - use skipRefreshView to prevent this
    initializeNewMeasDoc: function (skipRefreshView) {
        try {
            if (skipRefreshView === true)
                skipRefreshView = true;
            else
                skipRefreshView = false;

            var myModel = this.getViewModel();
            var measPoint = myModel.get('measPoint');

            var newMeasDoc = Ext.create('AssetManagement.customer.model.bo.MeasurementDoc', {});

            newMeasDoc.set('idate', AssetManagement.customer.utils.DateTimeUtils.getCurrentDate());
            newMeasDoc.set('itime', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
            newMeasDoc.set('point', measPoint.get('point'));
            newMeasDoc.set('unitr', measPoint.get('mrngu'));


            myModel.set('measDoc', newMeasDoc);
            myModel.set('isEditMode', false);

            if (skipRefreshView === false)
                this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewMeasDoc of BaseMeasDocEditPageController', ex);
        }
    },

    //will perform a set of checks on the current input values before those values will be transferred into the current measDoc and saved to the database
    trySaveMeasDoc: function () {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState();

            var me = this;

            var afterInputCheckAction = function (inputCheckWasSuccessfull) {
                try {
                    if (inputCheckWasSuccessfull === true) {

                        try {

                            //get measDoc to save
                            var measDoc = me.getViewModel().get('measDoc');

                            me.getView().transferViewStateIntoModel();
                            measDoc.set('unitr', measDoc.get('unitr').toUpperCase());

                            var callback = function (success) {
                                try {
                                    if (success === true) {
                                        var myMeasPoint = me.getViewModel().get('measPoint')
                                        myMeasPoint.set('lastRec', measDoc.get('recdc'));

                                        me.addMeasDocToListStore(measDoc);
                                        me.initializeNewMeasDoc();
                                        me.getView().refreshView();

                                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedMeasDoc'), true, 0);
                                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                    } else {
                                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingMeasDoc'), false, 2);
                                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                    }
                                } catch (ex) {
                                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMeasDoc of BaseMeasDocEditPageController', ex)
                                } finally {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                }
                            };

                            var eventId = AssetManagement.customer.manager.MeasurementDocManager.saveMeasDoc(measDoc);

                            if (eventId > 0) {
                                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                            } else {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingMeasDoc'), false, 2);
                                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                            }

                        } catch (ex) {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMeasDoc of BaseMeasDocEditPageController', ex)
                        }

                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMeasDoc of BaseMeasDocEditPageController', ex)
                }
            };

            //first check inpit values
            this.checkInputValues(afterInputCheckAction, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMeasDoc of BaseMeasDocEditPageController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //performs a set of checks, returning the overall result by the provided callback
    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var values = this.getView().getCurrentInputValues();

            //Ablesewert
            var rec = values.recdc;
            var unit = values.unitr;

            var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;

            if (isNullOrEmpty(rec)) {
                errorMessage = Locale.getMsg('provideRec');
                retval = false;
            } else if (isNullOrEmpty(unit)) {
                errorMessage = Locale.getMsg('provideUnit');
                retval = false;
            } else if (this.getViewModel().get('measPoint').get('indct') === 'X') {
                if (this.getViewModel().get('measPoint').get('indrv') === 'X') {
                    if (parseFloat(rec) > parseFloat(this.getViewModel().get('measPoint').get('lastRec'))) {
                        errorMessage = Locale.getMsg('entryValueNotBiggerEnteredBefore');
                        retval = false;
                    }
                }
                else {
                    var recMP = this.getViewModel().get('measPoint').get('lastRec');

                    //if (rec != null && rec.indexOf(',') > -1)
                    //    rec = rec.replace(',', '.');

                    if (rec !== null && parseFloat(rec) < parseFloat(recMP)) {
                        errorMessage = Locale.getMsg('entryValueNotLesserEnteredBefore');
                        retval = false;
                    }
                }
            }

            var me = this;

            if (retval === false) {
                var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(message);
            }

            if (callback)
                callback.call(callbackScope ? callbackScope : me, retval);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseMeasDocEditPageController', ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },


    deleteMeasDoc: function (measDoc) {
        try {
            var currentMeasDoc = this.getViewModel().get('measDoc');
            var deletingCurrentEditingMeasDoc = measDoc.get('point') === currentMeasDoc.get('point')
													&& measDoc.get('mdocm') === currentMeasDoc.get('mdocm')

            var me = this;
            var callback = function (success) {
                try {
                    if (success === true) {
                        if (deletingCurrentEditingMeasDoc)
                            me.initializeNewMeasDoc();

                        me.deleteMeasDocFromListStore(measDoc);
                        me.getView().refreshView();

                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('measDocDeleted'), true, 0);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingMeasDoc'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMeasDoc of BaseMeasDocEditPageController', ex);
                }
            };

            var eventId = AssetManagement.customer.manager.MeasurementDocManager.deleteMeasDoc(measDoc);
            if (eventId > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingMeasDoc'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMeasDoc of BaseMeasDocEditPageController', ex);
        }
    },

    editMeasDoc: function (measDoc) {
        try {
            var myModel = this.getViewModel();

            myModel.set('measDoc', measDoc);
            myModel.set('isEditMode', true);

            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editMeasDoc of BaseMeasDocEditPageController', ex);
        }
    },

    addMeasDocToListStore: function (measDoc) {
        try {
            var measDocs = this.getViewModel().get('measPoint').get('measDocs');

            if (!measDocs) {
                measDocs = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MeasurementDoc',
                    autoLoad: false
                });
                this.getViewModel().get('measPoint').set('measDocs', measDocs);
            }

            var temp = measDocs.getById(measDoc.get('id'));

            if (temp) {
                var index = measDocs.indexOf(temp);
                measDocs.removeAt(index);
                measDocs.insert(index, measDoc);
            } else {
                measDocs.insert(0, measDoc);
            }

            //sort List of measDocs descending
            var itime = AssetManagement.customer.utils.DateTimeUtils.getDateTime(measDoc.get('idate'), measDoc.get('itime'));
            measDoc.set('itime', itime);

            measDocs.sort([
                {
                    property: 'itime',
                    direction: 'DESC'
                }
            ]);

            this.getViewModel().get('measPoint').set('lastRec', measDocs.getAt(0).get('recdc'));
            this.getViewModel().get('measPoint').set('lastUnit', measDocs.getAt(0).get('unitr'));

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addMeasDocToListStore of BaseMeasDocEditPageController', ex);
        }
    },

    deleteMeasDocFromListStore: function (measDoc) {
        try {
            var measDocs = this.getViewModel().get('measPoint').get('measDocs');
            var temp = measDocs.getById(measDoc.get('id'));

            if (temp)
                measDocs.remove(measDoc);

            if (measDocs.getCount() > 0) {
                measDocs.sort([
                 {
                     property: 'itime',
                     direction: 'DESC'
                 }
                ]);

                this.getViewModel().get('measPoint').set('lastRec', measDocs.getAt(0).get('recdc'));
                this.getViewModel().get('measPoint').set('lastUnit', measDocs.getAt(0).get('unitr'));
            }
            else {
                this.getViewModel().get('measPoint').set('lastRec', '');
                this.getViewModel().get('measPoint').set('lastUnit', '');
            }


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMeasDocFromListStore of BaseMeasDocEditPageController', ex);
        }
    },

    ///------- CONTEXT MENU ---------///
    //long tap to open context menu
    onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();

            if (!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [];

            if (record.get('updFlag') === 'I') {
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
            }

            this.rowMenu.onCreateContextMenu(options, record);

            if (options.length > 0) {
                this.rowMenu.showAt(e.getXY());
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseMeasDocEditPageController', ex);
        }
    },

    onContextMenuItemSelected: function (itemID, record) {
        try {
            if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {

                this.deleteMeasDoc(record);
            }
            else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
                this.editMeasDoc(record);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseMeasDocEditPageController', ex);
        }
    }
    ///------ CONTEXT MENU END -------///

});