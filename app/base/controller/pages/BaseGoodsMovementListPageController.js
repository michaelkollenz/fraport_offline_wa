Ext.define('AssetManagement.base.controller.pages.BaseGoodsMovementListPageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',
  alias: 'controller.BaseGoodsMovementListPageController',

  requires: [
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.manager.UserManager',
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.LanguageHelper',
    'AssetManagement.customer.model.helper.ReturnMessage',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.model.bo.PoItem'
  ],

  config: {
    contextReadyLevel: 1
  },

  //private
  _requestRunning: false,

  //protected
  //may be implemented by derivates
  onOptionMenuItemSelected: function (optionID) {
    try {
      if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_EDIT) {
        AssetManagement.customer.core.Core.navigateBack();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseWeListPageController', ex);
    }
  },

  // onCreateOptionMenu: function () {
  //     try {
  //         this._optionMenuItems = [];
  //         this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_EDIT);
  //     } catch (ex) {
  //         AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseWeListPageController', ex);
  //     }
  // },

  //protected
  //@override
  startBuildingPageContext: function (argumentsObject) {
    try {

      var goodsMovementsStore = null;
      var myModel = this.getViewModel();

      var getLgortsEventId = AssetManagement.customer.manager.LgortManager.getLgorts(false);
      this.addRequestToDataBaseQueue(getLgortsEventId, 'lgortStore');

      var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
      if (selectedLgort)
        myModel.set('selectedLgort', selectedLgort);

      if (argumentsObject['miProcess'])
        myModel.set('miProcess', argumentsObject['miProcess']);

      if (argumentsObject['reference'])
        myModel.set('reference', argumentsObject['reference']);

      myModel.set('goodsMovementsStore', argumentsObject['goodsMovementItemsStore']); //obligatory
      myModel.set('poItemsStore', argumentsObject['poItemsStore']); //obligatory

      myModel.set('showStorageLocation', false);

      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseGoodsMovementListPageController', ex);
    }
  },

  onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      e.stopEvent();
      if (!this.rowMenu)
        this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

      var options = [];
      options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);

      this.rowMenu.onCreateContextMenu(options, record);

      if (options.length > 0) {
        this.rowMenu.setDisabled(false);
        this.rowMenu.showAt(e.getXY());
      } else {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseGoodsMovementListPageController', ex);
    }
  },

  onSubmitClick: function (field, e, eOpts) {
    try {
      var me = this;
      var myModel = this.getViewModel();
      var poNumber = myModel.get('poNumber');
      var miProcess = myModel.get('miProcess');
      var reference = myModel.get('reference');
      var goodsMovementHeader = Ext.create('AssetManagement.customer.model.bo.GoodsMovementHeader');
      var goodsMovementItems = myModel.get('goodsMovementsStore');

      var onlineGoodsMovementSuccessDialogCallback = function (resultMessage) {
        try {
          goodsMovementItems.removeAll();
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(resultMessage, true, 0);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WE_LIST, null, 2); // delete the last 3 pages from history stack to have a fresh start
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseGoodsMovementListPageController', ex);
        }
      };

      var onlineGoodsMovementFailureDialogCallback = function (returnMessageArray) {
        try {
          for (var i = 0; i < returnMessageArray.length; i++) {
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(returnMessageArray[i], false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
          myModel.set('showStorageLocation', true);
          me.refreshView();
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseGoodsMovementListPageController', ex);
        }
      };

      if (goodsMovementItems) {
        var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();

        goodsMovementHeader.set('pstng_date', AssetManagement.customer.utils.DateTimeUtils.getDateString(AssetManagement.customer.utils.DateTimeUtils.getCurrentDate(userInfo.get('timeZone'))));
        goodsMovementHeader.set('doc_date', AssetManagement.customer.utils.DateTimeUtils.getDateString(AssetManagement.customer.utils.DateTimeUtils.getCurrentDate(userInfo.get('timeZone'))));
        goodsMovementHeader.set('pr_uname', userInfo.get('mobileUser'));
        // goodsMovementHeader.set('header_txt', 'TEST'); //TODO klären ob notwendig
        goodsMovementHeader.set('gm_code', miProcess.get('gm_code'));
        goodsMovementHeader.set('ref_doc_no', reference);

        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_GOODS_MOVEMENT_DIALOG, {
          goodsMovementHeader: goodsMovementHeader,
          goodsMovementItems: goodsMovementItems,
          successCallback: onlineGoodsMovementSuccessDialogCallback,
          cancelCallback: onlineGoodsMovementFailureDialogCallback
        });
      }
    }
    catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSubmitClick of BaseGoodsMovementListPageController', ex);
    }
  },

  onLgortSelected: function (comboBox, selection, eOpts, a, b, c) {
    try {
      var goodsMovementsStore = this.getViewModel().get('goodsMovementsStore');
      var lgort = comboBox.getValue();
      var record = comboBox.getWidgetRecord();

      goodsMovementsStore.each(function (goodsMovementItem) {
        if(goodsMovementItem.get('ebelp') === record.get('ebelp') ){
          goodsMovementItem.set('lgort', lgort);
        }
      });

      // eOpts.record.set('lgort', lgort);
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside onLgortSelected of ChecklistQplanPageController', ex);
    }
  },

  onDeleteClicked: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
      var goodsMovementStore = this.getViewModel().get('goodsMovementsStore');
      goodsMovementStore.remove(record);
      this.uncheckPoItemInStore(record.get('ebelp'));
      if (goodsMovementStore.getCount() == 0)
        AssetManagement.customer.core.Core.navigateBack();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCheckItemSelected of ChecklistQplanPageController', ex);
    }
  },


  onEditClicked: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
      AssetManagement.customer.core.Core.navigateBack();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCheckItemSelected of ChecklistQplanPageController', ex);
    }
  },


  uncheckPoItemInStore: function (ebelp) {
    try {
      var poItemsStore = this.getViewModel().get('poItemsStore');
      if (poItemsStore && poItemsStore.getCount() > 0)
        poItemsStore.each(function (poItem) {
          if (poItem.get('ebelp') === ebelp) {
            var toDeliver = poItem.get('menge') * 1; //*1 is an int conversion
            var alreadyDelivered = (poItem.get('menge_del') ? poItem.get('menge_del') : 0) * 1;
            poItem.set('menge_ist', toDeliver - alreadyDelivered); //default value of the locally delivered quantity
            poItem.set('elikz', false);
          }
        });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCheckItemSelected of ChecklistQplanPageController', ex);
    }
  }
});