Ext.define('AssetManagement.base.controller.pages.BaseDeliveryDetailPageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',


  requires: [
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.NetworkHelper',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.manager.IAReasonManager',
    'AssetManagement.customer.manager.InternalDeliveryManager',
    'Ext.data.Model'
    // 'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
  ],

  uses: [
    'Ext.data.Model'
  ],

  config: {
    contextReadyLevel: 1,
    isAsynchronous: true
  },

  // mixins: {
  //     handlerMixin: 'AssetManagement.customer.controller.mixins.DeliveryDetailPageControllerMixin'
  // },

  //public
  //@override
  onOptionMenuItemSelected: function (optionID) {
    try {
      if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
        if (this._saveRunning === true)
          return;

        this.trySaveItem();
      }else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_REPORT){
        this.printReport();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseDeliveryDetailPageController', ex);
    }
  },

  //private
  onCreateOptionMenu: function () {
    try {

      this._optionMenuItems = new Array();
      // this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE);
      this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_REPORT);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseDeliveryDetailPageController', ex);
    }
  },
//onZugChanged
  onZugChanged: function (radioGroup, newValue, oldValue, eOpts) {
    try {
      if (newValue) {
        var record = radioGroup.up().getWidgetRecord();
        if (radioGroup.up()._rowContext.getWidgets()[1]) {
          var combobox = radioGroup.up()._rowContext.getWidgets()[1].items.items[1];
        }
        var grund = record.get('grund');
        if (grund === '000') {
          grund = '';
        }
        record.set('ia_status', newValue);
        if (combobox) {
          if (newValue !== 'XX') {
            combobox.setReadOnly(true);
            combobox.setValue('');
          } else {
            combobox.setReadOnly(false);
            combobox.setValue(grund);

          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onZugChanged of BaseDeliveryDetailPageController', ex);
    }
  },

  // onReasonChanged: function (field, newValue, oldValue, eOpts) {
  //   try {
  //     var record = field.up().getWidgetRecord();
  //     record.set('grund', newValue);
  //   } catch (ex) {
  //     AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onReasonChanged of BaseDeliveryDetailPageController', ex);
  //   }
  // },
  //@override
  startBuildingPageContext: function (argumentsObject) {
    try {
      var myModel = this.getViewModel();
      myModel.set('deliveryStore', argumentsObject['deliveryStore']);
      myModel.set('reasonStore', argumentsObject['reasonStore']);

      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseDeliveryDetailPageController', ex);
    }
  },

  // loadIAReasons: function () {
  //   try {
  //     var reasonStoreRequest = AssetManagement.customer.manager.IAReasonManager.getAllIAReason(true);
  //     this.addRequestToDataBaseQueue(reasonStoreRequest, 'reasonStore');
  //   } catch (ex) {
  //     AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadIAReasons of BaseDeliveryDetailPageController', ex);
  //   }
  // },

  // loadListStore: function () {
  //   try {
  //     var deliveryStoreRequest = AssetManagement.customer.manager.InternalDeliveryManager.getDeliverys(true);
  //     this.addRequestToDataBaseQueue(deliveryStoreRequest, 'listItems');
  //   } catch (ex) {
  //     AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseDeliveryDetailPageController', ex);
  //   }
  // },

  //list handler
  onDeliverySelected: function (tableview, td, cellIndex, record, tr, rowIndex, event, eOpts) {
    try {

      // AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, {aufnr: record.get('aufnr')});
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeliverySelected of BaseDeliveryDetailPageController', ex);
    }
  },

  onDeliverySelectedByTabHold: function (delivery) {
  },

  onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      e.stopEvent();
      if (!this.rowMenu)
        this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

      var options = [];

      this.rowMenu.onCreateContextMenu(options, record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseDeliveryDetailPageController', ex);
    }
  },


  trySaveItem: function () {
    try {
      this._saveRunning = true;

      AssetManagement.customer.controller.ClientStateController.enterLoadingState();

      var me = this;
      var deliveryItems = me.getViewModel().get('deliveryStore');
      var saveCallback = function (success) {
        try {
          if (success === true) {

            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_DELIVERY, {}, 2);

            Ext.defer(function () {
              me._saveRunning = false;
            }, 2000, me);


            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          } else {
            me._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifItem'), false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          me._saveRunning = false;
          AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseDeliveryDetailPageController', ex);
        }
      };


      var eventId = AssetManagement.customer.manager.InternalDeliveryManager.saveDeliveryItems(deliveryItems);

      if (eventId > 0) {
        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
      } else {
        me._saveRunning = false;
        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifItem'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseDeliveryDetailPageController', ex);
      this._saveRunning = false;
      AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }

  },



  printReport: function (object) {
    try {
      var me = this;
      var myModel = this.getViewModel();
      this.getView().transferViewStateIntoModel();
      var deliveryStore = this.getViewModel().get('deliveryStore');
      var reasonStore = this.getViewModel().get('reasonStore');
      this.createBarcodesForReport(deliveryStore);
      // var dummyObject = Ext.create('AssetManagement.customer.model.bo.InternalDelivery', {});
      var dummyObject = Ext.create('Ext.data.Model', {});
      dummyObject.set('deliveryStore',deliveryStore);
      dummyObject.set('reasonStore',reasonStore);
      dummyObject.set('persNo',myModel.get('persNo'));
      dummyObject.set('kostl',myModel.get('kostl'));
      dummyObject.self.SAP_OBJECT_TYPE = 'DELIVERY';

      var objTable = [];
      objTable.push(dummyObject);


      var enhancementPoint = function () {
        try {
          eventController = AssetManagement.customer.controller.EventController.getInstance();
          var eventId = eventController.getNextEventId();
          if (eventId > -1) {
            eventController.requestEventFiring(eventId, true);
            return eventId;
          } else {

          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-enhancementPoint of BaseDeliveryDetailPageController', ex);
        }
      };

      var parentSuccessCallback = function () {
        try {
          me.trySaveItem();
          // AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_DELIVERY, {}, 2);
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-parentSuccessCallback of BaseDeliveryDetailPageController', ex);
        }
      };

      var parentErrorCallback = function(){
        try{
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('generalErrorCreatingReportsOccured'), false, 2);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        } catch(ex){
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-parentSuccessCallback of BaseDeliveryDetailPageController', ex);
        }
      };

      var reportFrameworkConfig = {
        isOneSignatureForAllDocuments : true,
        isVerticalView: true,
        actionAfterReportCreation: enhancementPoint,
        reportLanguage: null,
        signedObjets: null,
        preventLangChange: true
      };

      AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance().createReports(reportFrameworkConfig, objTable, parentSuccessCallback, parentErrorCallback);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport of BaseDeliveryDetailPageController', ex);
    }
  },


  createBarcodesForReport: function (deliveryStore) {
    try {
      // var barcode = this.getView().getElementById("barcode");
      // var barcode = Ext.getCmp('barcode');
      var barcode = document.getElementById("barcode");
      deliveryStore.each(function(item){
        JsBarcode(barcode, "FRA-IA-001-" + item.get('belnr'));
        item.set('barcode', barcode.src);

        // var image = barcode.getImage();
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createBarcodesForReport of BaseOrderDetailPageController', ex);
    }
  }

});