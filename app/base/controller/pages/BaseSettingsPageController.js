Ext.define('AssetManagement.base.controller.pages.BaseSettingsPageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',

  requires: [],

  config: {
    contextReadyLevel: 0
  },

  //private
  _saveRunning: false,

  beforeDataReady: function() {
    try {
      this.initializeViewModel();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseSettingsPageController', ex);
      this.errorOccurred();
    }
  },

  trySaveSettings: function () {
    try {
      if (this._saveRunning === true) {
        return;
      }
      this._saveRunning = true;
      this.updateViewModel();
      if (this.checkInputValues()) {
        var viewModel = this.getViewModel();
        AssetManagement.customer.core.Core.getAppConfig().setMandt(viewModel.get('sapClient'));
        AssetManagement.customer.core.Core.getAppConfig().setSyncSystemId(viewModel.get('sapSystem'));
        AssetManagement.customer.core.Core.getAppConfig().setSyncGateway(viewModel.get('gateway'));
        AssetManagement.customer.core.Core.getAppConfig().setSyncGatewayPort(viewModel.get('gatewayPort'));
        AssetManagement.customer.core.Core.getAppConfig().setSyncGatewayIdent(viewModel.get('gatewayIdent'));
        AssetManagement.customer.core.Core.getAppConfig().setSyncProtocol(viewModel.get('protocol'));

        Ext.defer(function () {
          AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_LOGINSCREEN);
        }, 200);
      } else {
        alert('fehlt')
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    } finally {
      this._saveRunning = false;
    }
  },

  onSendLogFilesButtonClicked: function () {
    try {
      AssetManagement.customer.core.Core.getMainView().getController().sendLogFiles();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
  },

  onCancelClicked: function () {
    try {
      AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_LOGINSCREEN);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
  },

  ondoClientResetButtonClicked: function () {
    try {
      var callback = function (confirmed) {
        try {
          if (confirmed === true) {
            var wipeCallback = function () {
              window.localStorage.clear();
              location.reload();
            };
            AssetManagement.customer.core.Core.getDataBaseHelper().wipeDataBase(wipeCallback, this);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMainMenuItemClick of BaseSettingsPageController', ex);
        }
      };

      var dialogArgs = {
        title: Locale.getMsg('doClientReset'),
        icon: 'resources/icons/reset.png',
        message: Locale.getMsg('doClientResetQuestion'),
        alternateConfirmText: Locale.getMsg('ok'),
        alternateDeclineText: Locale.getMsg('dialogCancel'),
        callback: callback,
        scope: this
      };

      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
  },


  checkInputValues: function () {
    var retVal = false;
    try {
      var viewModel = this.getViewModel();
      var hasData = function (value) {
        return value && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(value);
      };
      retVal =
        hasData(viewModel.get('sapClient')) && hasData(viewModel.get('sapSystem')) &&
        hasData(viewModel.get('gateway')) && hasData(viewModel.get('gatewayPort')) &&
        hasData(viewModel.get('gatewayIdent'));

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
    return retVal;
  },

  initializeViewModel: function(){
    try {
      var viewModel = this.getViewModel();
      viewModel.set('sapClient', AssetManagement.customer.core.Core.getAppConfig().getMandt());
      viewModel.set('sapSystem', AssetManagement.customer.core.Core.getAppConfig().getSyncSystemId());
      viewModel.set('gateway', AssetManagement.customer.core.Core.getAppConfig().getSyncGateway());
      viewModel.set('gatewayPort', AssetManagement.customer.core.Core.getAppConfig().getSyncGatewayPort());
      viewModel.set('gatewayIdent', AssetManagement.customer.core.Core.getAppConfig().getSyncGatewayIdent());
      viewModel.set('protocol', AssetManagement.customer.core.Core.getAppConfig().getSyncProtocol());
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
  },

  updateViewModel: function () {
    try {
      var viewModel = this.getViewModel();
      viewModel.set('sapClient', Ext.getCmp('settingsPagesapClientIdTextfield').getValue());
      viewModel.set('sapSystem', Ext.getCmp('settingsPageSapSystemIdTextField').getValue());
      viewModel.set('gateway', Ext.getCmp('settingsPageGatewayTextField').getValue());
      viewModel.set('gatewayPort', Ext.getCmp('settingsPageGatewayPortTextfield').getValue());
      viewModel.set('gatewayIdent', Ext.getCmp('settingsPageGatewayIdentTextfield').getValue());
      viewModel.set('protocol', Ext.getCmp('settingsPageProtokollCombobox').getValue());
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
  },

  updateView: function () {
    try {
      var viewModel = this.getViewModel();
      Ext.getCmp('settingsPagesapClientIdTextfield').setValue(viewModel.get('sapClient'));
      Ext.getCmp('settingsPageSapSystemIdTextField').setValue(viewModel.get('sapSystem'));
      Ext.getCmp('settingsPageProtokollCombobox').setValue(viewModel.get('protocol'));
      Ext.getCmp('settingsPageGatewayTextField').setValue(viewModel.get('gateway'));
      Ext.getCmp('settingsPageGatewayPortTextfield').setValue(viewModel.get('gatewayPort'));
      Ext.getCmp('settingsPageGatewayIdentTextfield').setValue(viewModel.get('gatewayIdent'));
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    }
  }

});