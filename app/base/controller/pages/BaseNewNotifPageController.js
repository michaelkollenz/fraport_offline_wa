Ext.define('AssetManagement.base.controller.pages.BaseNewNotifPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',


	requires: [
		'AssetManagement.customer.manager.NotifManager',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger'
	],

	config: {
		contextReadyLevel: 1
	},

	//private
	_saveRunning: false,

	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;

				var retVal = this.saveNotif();
				this.getView().saveNotifCallback(retVal);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseNewNotifPageController', ex);
		}
	},

	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
		    this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseNewNotifPageController', ex);
		}
	},

	/**
	 * tries to save the current notif object
	 * returns null if the notif was saved or an error message if data is missing
	 */
	saveNotif: function(){
		var retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage("", true, 0);
		try {
			//update the models from the data of the view fields
			this.getView().updateModelData();
			//update the notif object and the dependent objects with data from the customizing like werk , arbpl
			if(!this.fillNotifData()){
				retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotif'), false, 2);
			} else if(!this.addPartnersToObject()) { //create the partnerarray from the partner objects
				retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotif'), false, 2);
			} else {
				//check if data is complete
				var checkRetVal = this.checkNotifComplete();
				if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(checkRetVal.message)) {

					var me = this;
					var callback = function(savedNotif) {
						try {
							if(savedNotif) {
								//go to notif details
								AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_DETAILS, { qmnum: me.getViewModel().get('notif').get('qmnum') }, true);
                                if (this.getViewModel().get('order'))
                                    this.getViewModel().get('order').set('qmnum', me.getViewModel().get('notif').get('qmnum'));

								Ext.defer(function() {
									me._saveRunning = false;
								}, 3000, me);

								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifSaved'), true, 0);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							} else {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotif'), false, 2);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotif of BaseNewNotifPageController', ex)
							me._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					};

					var eventId = AssetManagement.customer.manager.NotifManager.saveNotification(this.getViewModel().get('notif'));

					if(eventId > 0) {
						AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback,me);
					} else {
						retval = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotif'), false, 2);
					}
				} else {
					retVal = checkRetVal;
				}

				if(!retVal.success) {
					this._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNotif of BaseNewNotifPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}

		return retVal;
	},

	/**
	 * checks if the data of the notification object is complete
	 * returns null if the data is complete or an error message if data is missing
	 */
	checkNotifComplete: function() {
		var retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage("", true, 0);
		try {
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('notif').get('qmart'))){
				retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectQmart'), false, 2);
			}
			else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('notif').get('qmtxt'))){
				retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectKtext'), false, 2);
			}
			else if(this.getViewModel().get('notif').get('vondt') > this.getViewModel().get('notif').get('bisdt')) {
				retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('startAfterEndTime'), false, 2);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkNotifComplete of BaseNewNotifPageController', ex);
		}
		return retVal;
	},

	/**
	 * sets notif head data from cust001 and notiftype
	 */
	fillNotifData: function() {
		var retVal = false;
		try {
			var myModel = this.getViewModel();
			var cust001 = myModel.get('cust001');
			var notif = myModel.get('notif');

			notif.set('iwerk', cust001.get('notif_plan_plant'));
			notif.set('ingrp', cust001.get('notif_plangroup'));

			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('vaplz')))
				notif.set('vaplz', cust001.get('notif_workcenter'));
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notif.get('wawrk')))
				notif.set('wawrk', cust001.get('notif_plant_wrkc'));

			if(notif.get('notifType'))
				notif.set('rbnr', notif.get('notifType').get('rbnr'));

			retVal = true;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside fillNotifData of BaseNewNotifPageController', ex);
		}

		return retVal;
	},

	/**
	 * adds the partnerobjects to the notification)
	 */
	addPartnersToObject: function() {
		var retVal = false;
		try{
			//create a store for the partner objects
			var partnerStore = Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.Partner',
				autoLoad: false
			});
			//add the partners to the store
			if(this.getViewModel().get('customer'))
				partnerStore.add(this.getViewModel().get('customer'));
			if(this.getViewModel().get('partnerAp'))
				partnerStore.add(this.getViewModel().get('partnerAp'));
			if(this.getViewModel().get('partnerZr'))
				partnerStore.add(this.getViewModel().get('partnerZr'));
			//add the partnerstore to the order
			this.getViewModel().get('notif').set('partners', partnerStore);

			retVal = true;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createPartners of BaseNewNotifPageController', ex);
		}
		return retVal;
	},

	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			//if the notif object is undefined create a new store
			if(this.getViewModel().get('notif') === undefined || this.getViewModel().get('notif') === null) {
				this.getViewModel().set('notif', Ext.create('AssetManagement.customer.model.bo.Notif'));
				this.getViewModel().get('notif').initNewNotif();

				if (argumentsObject != null && argumentsObject.funcLoc != undefined && argumentsObject.funcLoc != null) {
				    this.onFuncLocSelected(argumentsObject.funcLoc);
				}
				if (argumentsObject != null && argumentsObject.equipment != undefined && argumentsObject.equipment != null) {
				    this.onEquiSelected(argumentsObject.equipment);
				}
				if (argumentsObject !== null && argumentsObject.order !== undefined && argumentsObject.order !== null) {
				    this.getViewModel().get('notif').set('order', argumentsObject.order);
				    this.getViewModel().get('notif').set('mobileKeyOrder', argumentsObject.order.get('mobileKey'));

				    //this.getViewModel().get('notif').set('mobileKey', argumentsObject.order.get('mobileKey'));
                    this.getViewModel().get('notif').set('aufnr', argumentsObject.order.get('aufnr'));
				    argumentsObject.order.set('notif', this.getViewModel().get('notif'));
				}
			}

			var notifTypesRequest = AssetManagement.customer.manager.CustNotifTypeManager.getNotifTypes(true);

			this.addRequestToDataBaseQueue(notifTypesRequest, "notifTypes");

			var cust001Request = AssetManagement.customer.manager.Cust_001Manager.getCust_001(AssetManagement.customer.core.Core.getAppConfig().getUserId().toUpperCase(), true);
			this.addRequestToDataBaseQueue(cust001Request, "cust001");

			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseNewNotifPageController', ex);
		}
	},

    /**
     * makes database request for codegroups for selected notif type
     * set data to viewmodel and initiates dropdown filling
     */
    prepareCodes: function () {
        try {
            var me = this;
            var notif = me.getViewModel().get('notif');
            var rbnr = notif.get('rbnr');
            var notifType = notif.get('notifType').get('sakat');

            if(!rbnr || !notifType){
                return;
            }

            var custCodeCallback = function (data) {
                try{
                    me.getViewModel().set('custCodeGroupsMap', data);
                    me.getView().fillCodeGroupDropdown();
                } catch(exception){
                    AssetManagement.helper.OxLogger.logException('Exception occurred inside prepareCodes-custCodeCallback of NewNotifPageController', exception);
                }
            };

            var eventId = AssetManagement.customer.manager.CustCodeManager.getCustCodesGroups(
                rbnr, notifType);

            if (eventId > 0) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, custCodeCallback);
            }

        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside prepareCodes of NewNotifPageController', ex);
        }
    },

    /*
	 * =================================================================================
     * ===================================== Events ====================================
	 * =================================================================================
     */

    /**
     * event handler for codification group drowpDown
     * @param combo
     * @param record
     */
    onNotifCodificationGroupChange: function (combo, record) {
        try {
            if (record) {
                this.getView().fillCodeDropdown(record);
            }
        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside onNotifCodificationGroupChange of NewNotifPageContoller', ex);
        }
    },

    /**
     * event handler for codification drowpDown
     * @param combo
     * @param record
     */
    onNotifCodificationChange: function (combo, record) {
        try {
            if (record) {
                this.getViewModel().get('notif').set('qmkat', record.get('katalogart'));
                this.getViewModel().get('notif').set('qmgrp', record.get('codegruppe'));
                this.getViewModel().get('notif').set('qmcod', record.get('code'));
            }
        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside onNotifCodificationChange of NewNotifPageContoller', ex);
        }
    },

    /**
     * event called after an object in the notiftypeComboBox has been selected
     */
    onNotifTypeSelected: function(combo, record) {
		try {
			if(record != null && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record)) {
                var viewModel = this.getViewModel();
                var notif = viewModel.get('notif');
                notif.set('qmart', record);
				var qmart = viewModel.get('notifTypes').findRecord('qmart', notif.get('qmart'));
                notif.set('notifType', qmart);
                notif.set('rbnr', qmart.get('rbnr'));
                this.prepareCodes();
				this.getView().displayPartners();
			}
    	}
    	catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTypeSelected of NewNotifPageContoller', ex);
    	}
    },

    //Selection through pickers
	/**
	 * Event fired when funcloc search button is clicked
	 */
	onSelectFuncLocClick: function() {
		try {
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.FUNCLOCPICKER_DIALOG, { });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectFuncLocClick of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when equi search button is clicked
	 */
	onSelectEquiClick: function() {
		try {
			var selectedFuncLocTplnr = '';
			var selectedFuncLoc = this.getViewModel().get('funcLoc');

			if(selectedFuncLoc) {
				selectedFuncLocTplnr = selectedFuncLoc.get('tplnr');
			}

			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.EQUIPMENTPICKER_DIALOG, { tplnr: selectedFuncLocTplnr });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectEquiClick of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when Customer search button is clicked
	 */
	onSelectCustomerClick: function() {
		try {
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CUSTOMERPICKER_DIALOG, { });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectCustomerClick of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when Partner search button is clicked
	 */
	onSelectePartnerApClick: function() {
		try {
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.PARTNERPICKER_DIALOG, { partner: this.getViewModel().get('customer'), parVw: 'AP' });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectePartnerApClick of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when Partner search button is clicked
	 */
	onSelectePartnerZrClick: function() {
		try {
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.PARTNERPICKER_DIALOG, { partner: this.getViewModel().get('customer'), parVw: 'ZR' });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectePartnerZrClick of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when equi has been selected in dialog
	 */
	onEquiSelected: function(record) {
		try {
			var myModel = this.getViewModel();
			var notif = myModel.get('notif');

			var equiHasSuperiorFuncLoc = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('tplnr'));

			myModel.set('equi', record);
			notif.set('vaplz', record.get('gewrk'));
			notif.set('wawrk', record.get('wergw'));
			notif.set('equnr', record.get('equnr'));
			notif.set('tplnr', equiHasSuperiorFuncLoc ? record.get('tplnr') : '');

			//manage connected func loc data
			if(equiHasSuperiorFuncLoc) {
				//equi has superior funcloc, check if the current func loc matches equipments funcloc, if it does UI refresh may be done
				//else try to load the funcloc before refreshing the UI
				var tragetTplnr  = record.get('tplnr');
				var currentFuncLoc = myModel.get('funcLoc');
				if(currentFuncLoc && currentFuncLoc.get('tplnr') === tragetTplnr) {
					this.getView().fillEquiFuncLocTextField();
				} else {
					var me = this;

					//equipments func loc does not match the current funcloc, so try to load it
					var funcLocCallback = function(funcLoc) {
						try {
							if(funcLoc) {
								myModel.set('funcLoc', funcLoc);
							} else {
								myModel.set('funcLoc', null);

								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equisFuncLocNotMobile'), true, 1);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							}

							me.getView().fillEquiFuncLocTextField();

							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						} catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of NewOrderPageController', ex);
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					};

					var eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tragetTplnr);

					if(eventId > 0) {
						AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
						AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, funcLocCallback);
					} else {
						myModel.set('funcLoc', null);
						this.getView().fillEquiFuncLocTextField();
					}
				}
			} else {
				this.getViewModel().set('funcLoc', null);
				this.getView().fillEquiFuncLocTextField();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when func has been selected in dialog
	 */
	onFuncLocSelected: function(record) {
		try {
			this.getViewModel().set('funcLoc', record);
			this.getViewModel().set('equi', null);
			this.getViewModel().get('notif').set('vaplz', record.get('gewrk'));
			this.getViewModel().get('notif').set('wawrk', record.get('wergw'));
			this.getViewModel().get('notif').set('equnr', '');
			this.getViewModel().get('notif').set('tplnr', record.get('tplnr'));
			this.getView().fillEquiFuncLocTextField();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when customer has been selected in dialog
	 */
	onCustomerSelected: function(record) {
		try {
			var customer = Ext.create('AssetManagement.customer.model.bo.Partner');
			customer.set('parvw', 'AG');
			customer.set('parnr', record.get('kunnr'));
			customer.set('partnertype', 'KU');
			customer.set('name1', record.get('firstname'));
			customer.set('name2', record.get('lastname'));
			customer.set('postcode1', record.get('postlcod1'));
			customer.set('city1', record.get('city'));
			customer.set('street', record.get('street'));
			customer.set('title', record.get('titlep'));
			customer.set('telnumber', record.get('tel1numbr'));
			customer.set('faxnumber', record.get('faxnumber'));
			this.getViewModel().set('customer', customer);
			this.getViewModel().set('partnerAp', null);
			this.getViewModel().set('partnerZr', null);
			this.getView().fillCustomerTextField();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerSelected of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when a partner for the partnerAp has been selected in dialog
	 */
	onPartnerApSelected: function(record) {
		try {
			var partner = this.createPartnerFromDialogRecord(record);
			partner.set('parvw', 'AP');
			this.getViewModel().set('partnerAp', partner);
			this.getView().fillCustomerTextField();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPartnerApSelected of NewOrderPageController', ex);
		}
	},

	/**
	 * Event fired when a partner for the partnerZr has been selected in dialog
	 */
	onPartnerZrSelected: function(record) {
		try {
			var partner = this.createPartnerFromDialogRecord(record);
			partner.set('parvw', 'ZR');
			this.getViewModel().set('partnerZr', partner);
			this.getView().fillCustomerTextField();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPartnerZrSelected of NewOrderPageController', ex);
		}
	},

	createPartnerFromDialogRecord: function(record) {
		var retval = null;

		try {
			var partner = Ext.create('AssetManagement.customer.model.bo.Partner');
			partner.set('parnr', record.get('parnr'));
			partner.set('kunnr', record.get('kunnr'));
			partner.set('partnertype', '');
			partner.set('name1', record.get('firstname'));
			partner.set('name2', record.get('lastname'));
			partner.set('postcode1', record.get('postlcod1'));
			partner.set('city1', record.get('city'));
			partner.set('street', record.get('street'));
			partner.set('title', record.get('titlep'));
			partner.set('telnumber', record.get('tel1numbr'));
			partner.set('faxnumber', record.get('faxnumber'));

			retval = partner;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createPartnerFromDialogRecord of NewOrderPageController', ex);
		}

		return retval;
	},

	/**
	 * Event fired when forcus of the newNotifTypeComboField is lost
	 */
	onNotifTypeBlur: function(record) {
		try {
			if(record.value != null)
				this.getView().disableNotifTypeComboBox();
		}
    	catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTypeBlur of NewNotifPageContoller', ex);
    	}
	}
});