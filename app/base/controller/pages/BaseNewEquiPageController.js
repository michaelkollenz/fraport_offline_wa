Ext.define('AssetManagement.base.controller.pages.BaseNewEquiPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.manager.FuncLocManager',
        'AssetManagement.customer.manager.EquiTypManager',
        'AssetManagement.customer.model.bo.Equipment',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger'
    ],
               
	config: {
		contextReadyLevel: 3
	},
	
	//private
	_saveRunning: false,
	
	//proteced
	//@override
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseNewEquiPageController', ex)
		}
	},
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
					
				this.trySaveEquipment();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseNewEquiPageController', ex)
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseNewEquiPageController', ex)
		}
	},
	
	//@override
	dataBaseQueueComplete: function() {
		this.callParent();
		
		try {
			if(this._contextLevel === 1) {
				this.performContextLevel1Requests();
			} else if(this._contextLevel === 2) {
				this.initializeNewEquipment();
				this.waitForDataBaseQueue();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseNewEquiPageController', ex)
		}
	},
	
	//private			
	performContextLevel1Requests: function() {
		try {
			var equiTypesRequest = AssetManagement.customer.manager.EquiTypManager.getEquiTypes(false);
			this.addRequestToDataBaseQueue(equiTypesRequest, "equipmentTypes");
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseNewEquiPageController', ex)
		}
	},
	
	initializeNewEquipment: function() {
		try {
			var newEquipment = Ext.create('AssetManagement.customer.model.bo.Equipment');
			newEquipment.set('iwerk', AssetManagement.customer.model.bo.UserInfo.getInstance().get('orderPlanPlant'));
			
			var myModel = this.getViewModel();

			var equiTypes = myModel.get('equipmentTypes');
			
			if(equiTypes && equiTypes.getCount() > 0) {
				newEquipment.set('eqtyp', equiTypes.getAt(0).get('eqtyp'));
			}
			
			myModel.set('equipment', newEquipment);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewEquipment of BaseNewEquiPageController', ex)
		}
	},
	
	//will perform a set of checks on the current input values before those values will be transferred into the new equipment and saved to the database
	trySaveEquipment: function() {
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
		
			var me = this;
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						var equipment = this.getViewModel().get('equipment');
						me.prepareModelForSaving();
						
						var callback = function(success) {
							try {
								if(success === true) {
									//go to equi details
									AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: equipment.get('equnr') }, true);
									
									Ext.defer(function() { 
										me._saveRunning = false;
									}, 2000, me);
									
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('newEquipmentRegistered'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									me._saveRunning = false;
									AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorRegisteringEquipment'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveEquipment of BaseNewEquiPageController', ex);
							}
						};				
						
						var eventId = AssetManagement.customer.manager.EquipmentManager.saveEquipment(equipment);
						
						if(eventId > 0) {
							AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
						} else {
							me._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorRegisteringEquipment'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					this._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveEquipment of BaseNewEquiPageController', ex);
				}
			};
			
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveEquipment of BaseNewEquiPageController', ex);
		}
	},
	
	//performs a set of checks, returning the overall result by the provided callback
	checkInputValues: function(callback, callbackScope) {
		try {
			var retval = true;
			var errorMessage = '';
			
			var values = this.getView().getCurrentInputValues(); 
			
			var shortText = values.eqktx;
			var baumm = values.baumm;
			var baujj = values.baujj;
			var sernr = values.sernr;
			var typbz = values.typbz;
			var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
			
			if(shortHand(shortText)) {
				//provide shorttext
				errorMessage = Locale.getMsg('provideShorttext');
				retval = false;
			} else if(shortHand(typbz)) {
				//provide type identfication
				errorMessage = Locale.getMsg('provideConstructionType');
				retval = false;
			} 
//			else if(shortHand(baujj)) {
//				//provide constr. year
//				errorMessage = Locale.getMsg('provideConstructionYear');
//				retval = false;
//			} else if(shortHand(baumm)) {
//				//provide constr. month
//				errorMessage = Locale.getMsg('provideConstructionMonth');
//				retval = false;
//			}
			
			if(retval === false) {
				var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(message);
				callback.call(callbackScope ? callbackScope : me, false);
				return;
			}
			
			var me = this;
			var pendingChecks = 3;
			var returned = false;
			
			var returnFunction = function() {
				if(retval === false && returned === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				if(returned === false && (pendingChecks === 0 || retval === false)) {
					returned = true;
					callback.call(callbackScope ? callbackScope : me, retval);
				}
			};
			
			//validate equiType
			var equiTypeCallback = function(success, message) {
				try {
					pendingChecks--;
				
					if(!success) {
						retval = false;
						errorMessage = message;
					}
					
					returnFunction();
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseNewEquiPageController', ex)
					
					if(callback) {
						callback.call(callbackScope ? callbackScope : me, false);
					}
				}
			};
			
			this.validateEquiType(equiTypeCallback);
			
			//validate funcLoc
			var funcLocCallback = function(success, message) {
				try {
					pendingChecks--;
				
					if(!success) {
						retval = false;
						errorMessage = message;
					}
					
					returnFunction();
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseNewEquiPageController', ex)
					
					if(callback) {
						callback.call(callbackScope ? callbackScope : me, false);
					}
				}
			};
			
			this.validateFuncLoc(funcLocCallback);
			
			//validate headEqui
			var headEquiCallback = function(success, message) {
				try {
					pendingChecks--;
				
					if(!success) {
						retval = false;
						errorMessage = message;
					}
					
					returnFunction();
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseNewEquiPageController', ex)
					
					if(callback) {
						callback.call(callbackScope ? callbackScope : me, false);
					}
				}
			};
			
			this.validateHeadEqui(headEquiCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseNewEquiPageController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	//validates the current eqtyp input
	validateEquiType: function(callback) {
		if(!callback) {
			return;
		}
		
		try {
			var retval = true;
			
			var currentEquiType = this.getView().getCurrentInputValues().eqtyp;
		
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(currentEquiType)) {
				retval = false;
			} else {
				//check if the provided equi type is a valid value
				var allTypes = this.getViewModel().get('equipmentTypes');
				
				var found = false;

				if(allTypes && allTypes.getCount() > 0) {
					allTypes.each(function(eqType) {
						if(eqType.get('eqtyp') === currentEquiType) {
							found = true;
							return false;
						} else if(eqType.get('txtbz') === currentEquiType) {
							found = true;
							return false;
						}
					}, this);
				}
				
				retval = found;
			}
			
			var message = 'success';
			if(retval === false)
				message = Locale.getMsg('provideValidEquiType');
			
			callback.call(this, retval, message);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateEquiType of BaseNewEquiPageController', ex)
			
			callback.call(this, false, Locale.getMsg('provideValidEquiType'));
		}
	},
	
	//validates the current tplnr input
	validateFuncLoc: function(callback) {
		if(!callback) {
			return;
		}
		
		try {
			var myModel = this.getViewModel();
			var chosenFuncLoc = myModel.get('chosenFuncLoc');
			var currentTplnr = this.getView().getCurrentInputValues().tplnr;
			
			//if there is not input for hequi, null the chosen FuncLoc and return;
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(currentTplnr)) {
				myModel.set('chosenFuncLoc', null);
			
				callback.call(this, true, 'success');
				return;
			}	

			//if there is a chosen FuncLoc, check, if the chosen funcloc still matches the current tplnr input
			if(chosenFuncLoc && currentTplnr === chosenFuncLoc.get('tplnr')) {
				//if they are equal, nothing more is to do
				callback.call(this, true, 'success');
				return;
			}
			
			var me = this;
			//if those are not the same or the user did not choose the funcloc directly, try to load the one for the current tplnr
			var funcLocCallback = function(funcLoc) {
				try {
					//don't bother, if it could not be found, it may even exist at the backend, trust the user
					me.getViewModel().set('chosenFuncLoc', funcLoc);
					callback.call(me, true, 'success');
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateFuncLoc of BaseNewEquiPageController', ex)
					
					callback.call(me, false, Locale.getMsg('funcLocCouldNotBeValidated'));
				}
			};
			
			//load funcLoc from database
			var eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(currentTplnr);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, funcLocCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateFuncLoc of BaseNewEquiPageController', ex)
			
			callback.call(this, false, Locale.getMsg('funcLocCouldNotBeValidated'));
		}
	},

	//validates the current hequi input
	validateHeadEqui: function(callback) {
		if(!callback) {
			return;
		}
		
		try {
			var myModel = this.getViewModel();
			var chosenHeadEqui = myModel.get('chosenHeadEquipment');
			var currentInputValues = this.getView().getCurrentInputValues();
			var currentHequi = currentInputValues.hequi;
			
			//if there is not input for hequi, null the chosen Equipment and return
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(currentHequi)) {
				myModel.set('chosenHeadEquipment', null);
			
				callback.call(this, true, 'success');
				return;
			}
			
			var funcLocCheck = function() {
				var message = 'success';
				var result = true;
			
				//if they are equal, perform the check to the current provided tplnr
				if(chosenHeadEqui && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(currentInputValues.tplnr)) {
					if(chosenHeadEqui.get('tplnr') !== currentInputValues.tplnr) {
						result = false;
						message = Locale.getMsg('equiDoesNotBelongToProvidedFuncLoc');
					}
				}
				
				callback.call(this, result, message);
				return;
			};
			
			//ensure the currentHequi is extended to it's 18 character length
			currentHequi = AssetManagement.customer.utils.StringUtils.padLeft(currentHequi, '0', 18);

			//if there is a chosen headEqui, check, if the chosen headEqui still matches the current hequi input
			if(chosenHeadEqui && currentHequi === chosenHeadEqui.get('equnr')) {
				//if they are equal, perform the funcLoc check
				funcLocCheck();
			} else {
				//else we try to load the equipment referenced by the input
			
				var me = this;
				
				var equiCallback = function(equipment) {
					try {
						//don't bother, if it could not be found, it may even exist at the backend, trust the user
						chosenHeadEqui = equipment;
						me.getViewModel().set('chosenHeadEquipment', equipment);
						
						funcLocCheck.call(me);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateHeadEqui of BaseNewEquiPageController', ex)
						
						callback.call(me, false, Locale.getMsg('headEquiCouldNotBeValidated'));
					}
				};
				
				//load equi from database
				var eventId = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(currentHequi);
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, equiCallback);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside validateHeadEqui of BaseNewEquiPageController', ex)
			
			callback.call(this, false, Locale.getMsg('headEquiCouldNotBeValidated'));
		}
	},
	
	prepareModelForSaving: function() {
		try {
			this.getView().transferViewStateIntoModel();
		
			var equipment = this.getViewModel().get('equipment');
			var submt = equipment.get('submt');
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(submt)) {
				equipment.set('matnr', submt);
			}
			
			//esure head equi number has 18 chars
			var hequi = equipment.get('hequi');
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(hequi)) {
				equipment.set('hequi', AssetManagement.customer.utils.StringUtils.padLeft(hequi, '0', 18));
			}
			
			this.transferRelevantDataFromChosenObjectsIntoEqui();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareModelForSaving of BaseNewEquiPageController', ex)
		}
	},
	
	//draws relevant data from the chosen funcloc/head-equi, if present, and puts it into the new equipment
	transferRelevantDataFromChosenObjectsIntoEqui: function() {
		try {
			var myModel = this.getViewModel();
		
			var newEqui = myModel.get('equipment');
			var chosenHeadEqui = myModel.get('chosenHeadEquipment');
			var chosenFuncLoc = myModel.get('chosenFuncLoc');
			
			if(chosenHeadEqui) {
				//set workcenter
				newEqui.set('gewrk', chosenHeadEqui.get('gewrk'));
				
				//set werk
				newEqui.set('wergw', chosenHeadEqui.get('wergw'));
				
				//set rnbr
				newEqui.set('rbnr', chosenHeadEqui.get('rbnr'));
			} else if(chosenFuncLoc) {
				//set workcenter
				newEqui.set('gewrk', chosenFuncLoc.get('gewrk'));
				
				//set werk
				newEqui.set('wergw', chosenFuncLoc.get('wergw'));
				
				//set rnbr
				newEqui.set('rbnr', chosenFuncLoc.get('rbnr'));
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferRelevantDataFromChosenObjectsIntoEqui of BaseNewEquiPageController', ex)
		}
	},

	onSelectSuperiorFuncLocClick: function() {
		try {
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.FUNCLOCPICKER_DIALOG);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectSuperiorFuncLocClick of BaseNewEquiPageController', ex)
		}
	},
	
	onSelectHeadEquiClick: function() {
		try {
			var tplnr = this.getView().getCurrentInputValues().tplnr;
		
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.EQUIPMENTPICKER_DIALOG, { tplnr: tplnr });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectHeadEquiClick of BaseNewEquiPageController', ex)
		}
	},
	
	onFuncLocSelected: function(funcLoc) {
		try {
			var myModel = this.getViewModel();
			myModel.set('chosenFuncLoc', funcLoc);
			
			if(funcLoc) {
				//if there is a head equi chosen, and this head equi does not belong to the chosen funcLoc reset the chosen headEqui
				var currentHeadEqui = myModel.get('chosenHeadEquipment');
				
				if(currentHeadEqui) {
					if(funcLoc.get('tplnr') !== currentHeadEqui.get('tplnr')) {
						this.onEquiSelected(null);
					}
				}
			}

			this.getView().fillFuncLocTextFields(funcLoc);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseNewEquiPageController', ex)
		}
	},
	
	onEquiSelected: function(equipment) {
		try {
			var myModel = this.getViewModel();
			myModel.set('chosenHeadEquipment', equipment);
			
			if(equipment) {
				//if the chosen equipment does not belong to the current chosen funcLoc, resete the chosen funcLoc
				var currentFuncLoc = myModel.get('chosenFuncLoc');
				
				if(currentFuncLoc) {
					if(equipment.get('tplnr') !== currentFuncLoc.get('tplnr')) {
						this.onFuncLocSelected(null);
					}
				}
			}
			
			this.getView().fillHeadEquipmentTextFields(equipment);			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseNewEquiPageController', ex)
		}
	}
});