Ext.define('AssetManagement.base.controller.pages.BaseWaCreateMaterialPageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',
  alias: 'controller.BaseWaCreateMaterialPageController',

  requires: [
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.LanguageHelper',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.manager.MiGrundManager',
    'AssetManagement.customer.manager.UserManager',
    'AssetManagement.customer.model.bo.GoodsMovementHeader',
    'AssetManagement.customer.model.bo.GoodsMovementItem',
    'AssetManagement.customer.model.helper.ReturnMessage',
    'AssetManagement.customer.utils.StringUtils'
  ],

  config: {
    contextReadyLevel: 1
  },

  //private
  _requestRunning: false,

  //protected
  //@override
  startBuildingPageContext: function (argumentsObject) {
    try {
      var myModel = this.getViewModel();
      var goodsMovementDefaults = null;
      var gm_code = null;
      var title = null;
      var refDoc = null;
      var printKZ = null;

      if (argumentsObject['goodsMovementDefaults']) {
        goodsMovementDefaults = argumentsObject['goodsMovementDefaults'];
      }

      myModel.set('goodsMovementDefaults', goodsMovementDefaults);

      var store = argumentsObject['goodsMovementItems'];
      if (!store) {
        var itemsStore = Ext.create('Ext.data.Store', {
          model: 'AssetManagement.customer.model.bo.GoodsMovementItem',
          autoLoad: false
        });
        myModel.set('goodsMovementItemsStore', itemsStore);
      } else {
        this.setDefaultsForPrefilledGM(store);
        myModel.set('goodsMovementItemsStore', store);
      }

      if (argumentsObject['miProcess']) {
        miProcess = argumentsObject['miProcess'];
      }


      // if (argumentsObject['printKZ']) {
      printKZ = argumentsObject['printKZ'];
      // }

      if (argumentsObject['refDoc']) {
        refDoc = argumentsObject['refDoc'];
      }

      if (argumentsObject['title']) {
        title = argumentsObject['title'];
      }

      myModel.set('miProcess', miProcess);
      this.loadMiReason();
      myModel.set('printKZ', printKZ);
      myModel.set('printKZLastUserValue', printKZ);
      myModel.set('title', title);
      myModel.set('refDoc', refDoc);
      myModel.set('menge', '');
      myModel.set('material', '');
      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWaCreateMaterialPageController', ex);
    }
  },

  setDefaultsForPrefilledGM: function (itemsStore) {
    try {
      var goodsMovementDefaults = this.getViewModel().get('goodsMovementDefaults');
      itemsStore.each(function (goodsMovementItem) {
        goodsMovementItem.set('werks', goodsMovementDefaults.get('werks'));
        goodsMovementItem.set('lgort', goodsMovementDefaults.get('lgort'));
        goodsMovementItem.set('bwart', goodsMovementDefaults.get('bwart'));
        goodsMovementItem.set('wempf', goodsMovementDefaults.get('wempf'));
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setDefaultsForPrefilledGM of BaseWaCreateMaterialPageController', ex);
    }
  },


  afterRefreshView: function () {
    try {
      this.getView().afterRefreshView();

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRefreshView of BaseWaCreateMaterialPageController', ex);
    }
  },


  loadMiReason: function () {
    try {
      var reasonStore = AssetManagement.customer.manager.MiGrundManager.getAllMiGrund(true);
      this.addRequestToDataBaseQueue(reasonStore, "reasonStore");
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMiReason of BaseWaCreateMaterialPageController', ex);
    }
  },

  refreshData: function (triggerPageRequestCallbackWhenDone) {
    try {
      if (!triggerPageRequestCallbackWhenDone) {
        this._pageRequestCallback = null;
        this._pageRequestCallbackScope = null;
      }

      // this.getViewModel().resetData(); //Commentet to have a persistent store when navigating back to page

      //set to zero
      this._contextLevel = 0;

      if (this._contextReadyLevel === 0) {
        this.contextReady();
      } else {
        //start the loading cycle
        this.startBuildingPageContext(this.getCurrentArguments());
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
    }
  },

  onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      e.stopEvent();
      if (!this.rowMenu)
        this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

      var options = [];
      options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);

      this.rowMenu.onCreateContextMenu(options, record);

      if (options.length > 0) {
        this.rowMenu.setDisabled(false);
        this.rowMenu.showAt(e.getXY());
      } else {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseWaCreateMaterialPageController', ex);
    }
  },

  onContextMenuItemSelected: function (itemID, record) {
    try {
      if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
        this.onDeleteItem(record);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseWaCreateMaterialPageController', ex);
    }
  },

  onDeleteItem: function (record) {
    try {
      var goodsMovementItems = this.getViewModel().get('goodsMovementItemsStore');
      goodsMovementItems.remove(record);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteItem of BaseWaCreateMaterialPageController', ex);
    }
  },


  onCheckDeleteClicked: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside onDeleteClicked of BaseWaCreateMaterialPageController', ex);
    }
  },


  onDeleteClicked: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
      var goodsMovementStore = this.getViewModel().get('goodsMovementItemsStore');
      goodsMovementStore.remove(record);
      this.refreshView();
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside onDeleteClicked of BaseWaCreateMaterialPageController', ex);
    }
  },


  onEditClicked: function (checkbox, rowIndex, checked, record, e, eOpts) {
    try {
      var myModel = this.getViewModel();
      //
      // myModel.set('material', record.get('matnr'));
      // myModel.set('menge', record.get('menge'));


      var callback = function (value) {
        try {
          if (value !== '0'){
            record.set('menge', value);
          }else{
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('quantZero'), false, 2); //TODO
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEditClicked of BaseWaCreateMaterialPageController', ex);
        }
      };
      var title = AssetManagement.customer.utils.StringUtils.filterLeadingZeros(record.get('matnr')) + ' ' + record.get('matkt');
      var value = record.get('menge') * 1;
      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SINGLE_TEXT_LINE_DIALOG, {
        value: value,
        message: Locale.getMsg('quantity'),
        emptyAllowed: false,
        maxLength: 40,
        title: title,
        callback: callback,
        callbackScope: this
      });

      this.refreshView();
    } catch (ex) {
      AssetManagement.helper.OxLogger.logException('Exception occurred inside onDeleteClicked of BaseWaCreateMaterialPageController', ex);
    }
  },


  onPrintChangeClick: function (field, newValue, oldValue, eOpts) {
    try {
      this.getViewModel().set('printKZLastUserValue', newValue)
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPrintChangeClick of BaseWaCreateMaterialPageController', ex);
    }
  },

  onAddButtonClick: function () {
    try {
      var myModel = this.getViewModel();
      var menge = myModel.get('menge');
      var quantity = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(menge);
      quantity = quantity + 1;
      myModel.set('menge', quantity.toString());
      this.refreshView();

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddButtonClick of BaseWaCreateMaterialPageController', ex);
    }
  },

  onRemoveButtonClick: function () {
    try {
      var myModel = this.getViewModel();
      var menge = myModel.get('menge');
      var quantity = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(menge);
      quantity = quantity - 1;
      if (quantity >= 0) {
        myModel.set('menge', quantity.toString());
      } else {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('quantZero'), false, 2); //TODO
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
      this.refreshView();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddButtonClick of BaseWaCreateMaterialPageController', ex);
    }
  },


  onMatNoTextKeypress: function (field, e, eOpts) {
    try {
      if (e.keyCode === 13) {
        Ext.getCmp('waCreateQuantityIs').focus();

      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
    }
  },


  onQuantTextKeypress: function (field, e, eOpts) {
    try {
      if (e.keyCode === 13) {
        this.onSaveClick();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
    }
  },


  onReasonChanged: function (field, newValue, oldValue, eOpts) {
    try {
      var record = field.up().getWidgetRecord();
      record.set('move_reas', newValue);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onReasonChanged of BaseWeListPageController', ex);
    }
  },


  // onReasonTextChanged: function (field, event, eOpts) {
  onReasonTextChanged: function (field, newValue, oldValue, eOpts) {
    try {

      var record = field.up().getWidgetRecord();
      record.set('item_text', newValue);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onReasonTextChanged of BaseWeListPageController', ex);
    }
  },


  onSaveClick: function () {
    try {
      this.getView().transferViewStateIntoModel();
      var me = this;
      var busobject = 'BUS1001';
      var myModel = this.getViewModel();
      var matnr = myModel.get('material');
      var menge = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(myModel.get('menge'));
      // var relField = myModel.get('relField');
      var goodsMovementDefaults = myModel.get('goodsMovementDefaults');
      var goodsMovementItemsStore = myModel.get('goodsMovementItemsStore');
      var goodsMovementItem = goodsMovementItemsStore.findRecord('matnr', matnr);

      if (!goodsMovementItem) {
        goodsMovementItem = Ext.create('AssetManagement.customer.model.bo.GoodsMovementItem');
      }

      if (menge > 0) {
        var onlineCheckBoDialogCallback = function (success, matnrRead, unit, materialKText, druck) {
          try {
            if (success) {
              goodsMovementItem.set('matnr', matnrRead);
              goodsMovementItem.set('werks', goodsMovementDefaults.get('werks'));
              goodsMovementItem.set('lgort', goodsMovementDefaults.get('lgort'));
              goodsMovementItem.set('bwart', goodsMovementDefaults.get('bwart'));
              // goodsMovementItem.set('kzbew', goodsMovementDefaults.get('kzbew'));
              goodsMovementItem.set('charg', goodsMovementDefaults.get('charg'));
              goodsMovementItem.set('menge', menge);
              goodsMovementItem.set('meins', unit);
              goodsMovementItem.set('wempf', goodsMovementDefaults.get('wempf'));
              goodsMovementItem.set('kdauf', goodsMovementDefaults.get('kdauf'));
              goodsMovementItem.set('ablad', goodsMovementDefaults.get('ablad'));
              goodsMovementItem.set('kostl', goodsMovementDefaults.get('kostl'));
              goodsMovementItem.set('aufnr', goodsMovementDefaults.get('aufnr'));
              goodsMovementItem.set('aufps', goodsMovementDefaults.get('aufps'));
              goodsMovementItem.set('rsnum', goodsMovementDefaults.get('rsnum'));
              goodsMovementItem.set('rspos', goodsMovementDefaults.get('rspos'));
              goodsMovementItem.set('ps_posid', goodsMovementDefaults.get('ps_posid'));
              goodsMovementItem.set('move_reas', goodsMovementDefaults.get('move_reas'));
              goodsMovementItem.set('umglo', goodsMovementDefaults.get('umglo'));
              goodsMovementItem.set('matkt', materialKText);
              goodsMovementItem.set('elikz', true);
              goodsMovementItem.set('prefilled', false);
              goodsMovementItem.set('kzumw', druck);
              goodsMovementItemsStore.add(goodsMovementItem);
              myModel.set('goodsMovementItemsStore', goodsMovementItemsStore);

              myModel.set('material', '');
              myModel.set('matnr', '');
              myModel.set('menge', '');
            } else {
              var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noMaterialFound'), false, 2);
              AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
            me.refreshView();
          } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlineCheckBoDialogCallback of BaseWaCreateMaterialPageController', ex);
          }
        };

        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_BO_CHECK_DIALOG, {
          busobject: busobject,
          busobjectKey: matnr,
          readObject: true,
          successCallback: onlineCheckBoDialogCallback
        });
      } else {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('quantZero'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchPoClick of BaseWaCreateMaterialPageController', ex);
    }
  },

  checkReasonFilled: function (goodsMovementItems) {
    try {
      var checkOk = true;
      var kzgrund = this.getViewModel().get('miProcess').get('kzgru')
      if (kzgrund !== '-') {
        goodsMovementItems.each(function (goodsMovementItem) {
          var grund = goodsMovementItem.get('move_reas');
          var text = goodsMovementItem.get('item_text');
          if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(grund) || AssetManagement.customer.utils.StringUtils.isNullOrEmpty(text)) {
            checkOk = false;
            return false;
          }
        });
      }
      return checkOk;
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onReasonTextChanged of BaseWeListPageController', ex);
    }
  },


  onSubmitClick: function (field, e, eOpts) {
    try {
      var myModel = this.getViewModel();
      var goodsMovementHeader = Ext.create('AssetManagement.customer.model.bo.GoodsMovementHeader');
      var goodsMovementItems = myModel.get('goodsMovementItemsStore');
      var goodsMovementItemsSelected = Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.GoodsMovementItem',
        autoLoad: false
      });
      goodsMovementItems.each(function (goodsMovementItem) {
        if (goodsMovementItem.get('elikz')) {
          goodsMovementItemsSelected.add(goodsMovementItem);
        }
      });
      if (!this.checkReasonFilled(goodsMovementItemsSelected)) {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fillAllReasons'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        return;
      }


      var onlinePoGetDialogCallback = function (resultMessage) {
        try {
          goodsMovementItemsSelected.removeAll();
          var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(resultMessage, true, 0);
          AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WA_CREATE, null, 1); // delete the last 1 pages from history stack to have a fresh start
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlinePoGetDialogCallback of BaseWaCreateMaterialPageController', ex);
        }
      };

      var onlinePoGetDialogCancelCallback = function (returnMessageArray) {
        try {
          for (var i = 0; i < returnMessageArray.length; i++) {
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(returnMessageArray[i], false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onlinePoGetDialogCancelCallback of BaseWaCreateMaterialPageController', ex);
        }
      };

      if (goodsMovementItemsSelected && goodsMovementItemsSelected.getCount() > 0) {
        var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
        var date = AssetManagement.customer.utils.DateTimeUtils.getDateString(AssetManagement.customer.utils.DateTimeUtils.getCurrentDate(userInfo.get('timeZone')));
        goodsMovementHeader.set('pstng_date', date);
        goodsMovementHeader.set('doc_date', date);
        goodsMovementHeader.set('pr_uname', userInfo.get('mobileUser'));
        goodsMovementHeader.set('gm_code', myModel.get('miProcess').get('gm_code'));
        var printKZ = myModel.get('printKZ');
        if (printKZ)
          goodsMovementHeader.set('print_version', 'X');
        var refDoc = myModel.get('refDoc');
        if (refDoc)
          goodsMovementHeader.set('ref_doc_no', refDoc);

        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_GOODS_MOVEMENT_DIALOG, {
          goodsMovementHeader: goodsMovementHeader,
          goodsMovementItems: goodsMovementItemsSelected,
          successCallback: onlinePoGetDialogCallback,
          cancelCallback: onlinePoGetDialogCancelCallback
        });
      } else {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noItems'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSubmitClick of BaseWaCreateMaterialPageController', ex);
    }
  }
});