Ext.define('AssetManagement.base.controller.pages.BaseIntegratedConfPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.utils.NumberFormatUtils',
		'AssetManagement.customer.manager.ActivityTypeManager',
		'AssetManagement.customer.manager.CustBemotManager',
		'AssetManagement.customer.manager.CustReasonManager',
        'AssetManagement.customer.manager.Cust_041Manager',
        'AssetManagement.customer.manager.Cust_043Manager',
		'AssetManagement.customer.manager.Cust_001Manager',
		'AssetManagement.customer.model.bo.TimeConf',
		'AssetManagement.customer.view.ContextMenu',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],

	mixins: [
	    'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_HeaderController',
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_GeneralTimeConfController',
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_MatConfController',
        'AssetManagement.customer.uiAddIns.IntegratedConf.UIIntegratedConfPage_LongtextNotifController'
	],

	config: {
		contextReadyLevel: 2,
		rowMenu: null
	},
	
	//private
	_saveRunning: false,
	_initialVornr: '',
	_initialSplit: '',
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
			
				this.trySaveTimeConf(); 
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
				if(this._saveRunning === true)
					return;
				
				this.initializeNewTimeConf();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseIntegratedConfPageController', ex);
		}
	},
		
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM, AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseIntegratedConfPageController', ex);
		}
	},
	
	//protected
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			this._initialVornr = '';
			this._initialSplit = '';
		
			var myModel = this.getViewModel();
			
			var aufnr = '', vornr = '', split = '';
			
			if(typeof argumentsObject['aufnr'] === 'string') {
				aufnr = argumentsObject['aufnr'];
			} else if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
				var order = argumentsObject['order'];
				myModel.set('order', order); 
			}
			
			if(typeof argumentsObject['vornr'] === 'string') {
				this._initialVornr = argumentsObject['vornr'];
			} else if(Ext.getClassName(argumentsObject['operation']) === 'AssetManagement.customer.model.bo.Operation') {
				var operation = argumentsObject['operation'];
				this._initialVornr = operation.get('vornr');
				this._initialSplit = operation.get('split');
                
				myModel.set('operation', operation); 
			}
			
			if(typeof argumentsObject['split'] === 'string') {
				this._initialSplit = argumentsObject['split'];
			}
					
			if(Ext.getClassName(argumentsObject['timeConf']) === 'AssetManagement.customer.model.bo.TimeConf') {
				var timeConf = argumentsObject['timeConf'];
				myModel.set('isEditMode', true);
				myModel.set('timeConf', timeConf);
				
				this._initialVornr = timeConf.get('vornr');
				this._initialSplit = timeConf.get('split');
				var foundOper = AssetManagement.customer.manager.OrderManager.getOrderOperation(myModel.get('order'), this._initialVornr, this._initialSplit);
                if(foundOper !== null)
				    myModel.set('operation', foundOper); 
			}
			
			if(aufnr !== '') {
				var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
				this.addRequestToDataBaseQueue(orderRequest, 'order');
			}

			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseIntegratedConfPageController', ex);
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		try {
			if(this._contextLevel === 1) {
				this.setInitialOperation();
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseIntegratedConfPageController', ex);
		}
	},
	
	//will set an initial operation for the model
	setInitialOperation: function() {
		try {
			var myModel = this.getViewModel();
			var ordersOperations = myModel.get('order').get('operations');
			var initialOperation = null;
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._initialVornr)) {
				//iterate the orders operation to locate a match
				var match = null;
				
				if(ordersOperations && ordersOperations.getCount() > 0) {
					ordersOperations.each(function(operation) {
						if(operation.get('vornr') === this._initialVornr && operation.get('split') === this._initialSplit) {
							match = operation;
							return false;
						}
					}, this)
				}
				
				if(match) {
					initialOperation = match;
				} else {
					//the requested operation could not be found
					//if a timeConf has been passed for editing, this timeconf can not be accepted and has to be set back
					//inform the user!
					
					if(myModel.get('timeConf')) {
						myModel.set('timeConf', null);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('requestOperationNotFound'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				}
			}

			//no specific operation requested or found - so use order's first operation
			if(!initialOperation && ordersOperations && ordersOperations.getCount() > 0) {
				initialOperation = ordersOperations.getAt(0);
			}

			myModel.set('operation', initialOperation);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitialOperation of BaseIntegratedConfPageController', ex);
		}
	},
			
	performContextLevel1Requests: function() {
		try {
			var operation = this.getViewModel().get('operation');
			
			//var activityTypesRequest = AssetManagement.customer.manager.ActivityTypeManager.getActivityTypes(operation.get('arbpl'), true);
			//this.addRequestToDataBaseQueue(activityTypesRequest, "activityTypes");
			
			var accountIndicationsRequest = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
			this.addRequestToDataBaseQueue(accountIndicationsRequest, "accountIndications");            

            var cust041Request = AssetManagement.customer.manager.Cust_041Manager.getCust_041(true);
            this.addRequestToDataBaseQueue(cust041Request, 'cust_041')

            var cust043Request = AssetManagement.customer.manager.Cust_043Manager.getCust_043(true);
            this.addRequestToDataBaseQueue(cust043Request, 'cust_043')
            
			var cust001Request = AssetManagement.customer.manager.Cust_001Manager.getCust_001(AssetManagement.customer.core.Core.getAppConfig().getUserId().toUpperCase(), true);
			this.addRequestToDataBaseQueue(cust001Request, 'cust_001');
            
			var storLocsRequest = AssetManagement.customer.manager.Cust_002Manager.getCust_002s(true);
			this.addRequestToDataBaseQueue(storLocsRequest, 'storLocs');
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseIntegratedConfPageController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			//set the operation according to the current data
			var myModel = this.getViewModel();
			
			var curOperation = myModel.get('operation');
			var curTimeConf = myModel.get('timeConf');
			
			if(!curOperation) {
				var operations = myModel.get('order').get('operations');
			
				if(operations && operations.getCount() > 0) {
					var operationToSet = null;
				
					if(curTimeConf) {
						//set the operation convenient to curTimeConf
						var timeConfsVornr = curTimeConf.get('vornr');
						var timeConfsSplit = curTimeConf.get('split');
						
						operations.each(function(operation) {
							if(operation.get('vornr') === timeConfsVornr && operation.get('split') === timeConfsSplit) {
								operationToSet = operation;
								return false;
							}
						}, this);
					} else {
						//set to first operation
						operationToSet = operations.getAt(0);
					}
					
					myModel.set('operation', operationToSet);
				}
			}
		                                                 
            this.getView().initializeNewObjects();
            this.loadOperationNotification();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseIntegratedConfPageController', ex);
		}
	},
	
	//will initialize a new timeConf and transfer it into the model
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewTimeConf: function(skipRefreshView) {
		try {
			if(skipRefreshView === true)
				skipRefreshView = true;
			else
				skipRefreshView = false;
		
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operation = myModel.get('operation');
		
			var newTimeConf = Ext.create('AssetManagement.customer.model.bo.TimeConf', { order: order, operation: operation });
			
			//set default unit, if time conf did not draw one from operation
			var timeConfsUnit = newTimeConf.get('ismne');
			
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsUnit)) {
			    timeConfsUnit = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('default_timeunit');

				newTimeConf.set('ismne', timeConfsUnit);
				newTimeConf.set('idaue', timeConfsUnit);
			}
			
			//set default activityType, if time conf did not draw any from operation
			var timeConfsLearr = newTimeConf.get('learr');
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsLearr)) {
				var activityTypes = myModel.get('activityTypes');
			
				if(activityTypes && activityTypes.getCount() > 0) {
					timeConfsLearr = activityTypes.getAt(0).get('lstar');
					newTimeConf.set('learr', timeConfsLearr);
				}
			}
			
			//set default account indication, if time conf did not draw any from order
			var timeConfsBemot = newTimeConf.get('bemot');
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsBemot)) {
				var accountIndications = myModel.get('accountIndications');
			
				if(accountIndications && accountIndications.getCount() > 0) {
					timeConfsBemot = accountIndications.getAt(0).get('bemot');
					newTimeConf.set('bemot', timeConfsBemot);
				}
			}
			
			//set default time value, matching to it's unit
			var defaultTimeInMinutes = 60.0;
			var asHours = true;
			var toTest = timeConfsUnit.toUpperCase();
			var toSet = 0;
			
			if(toTest === 'M' || toTest === 'MIN')
				asHours = false;
			
			if(asHours) {
				toSet = defaultTimeInMinutes / 60.0;
			} else {
				toSet = defaultTimeInMinutes;
			}
			
			//bring into backend format
			var toSet = AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(toSet);
			
			newTimeConf.set('ismnw', toSet);
			newTimeConf.set('idaur', toSet);

        	newTimeConf.set('ersda', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('isd', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('ied', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('pausesz', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('pauseez', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('budat', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
			newTimeConf.set('ernam', AssetManagement.customer.core.Core.getAppConfig().getUserId());
			newTimeConf.set('pernr', this.getViewModel().get('cust_001').get('order_pernr'));
		
			myModel.set('timeConf', newTimeConf);
			myModel.set('isEditMode', false);
			
			if(skipRefreshView === false)
				this.refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewTimeConf of BaseIntegratedConfPageController', ex);
		}
	},
	
	
	/**
	 * delete timeconf from operation - timeconfs- store
	 */
	deleteTimeConfFromListStore: function(timeConf) {
		try {
			var operation = this.getViewModel().get('operation');
			var temp = operation.get('timeConfs').getById(timeConf.get('id'));
			
			if(temp)
				operation.get('timeConfs').remove(timeConf);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTimeConfFromListStore of BaseIntegratedConfPageController', ex);
		}
	},
	
	onOperationSelected: function(comboBox, record) {
		try {
			var myModel = this.getViewModel();
			if(selectedOperation !== currentOperation) {
				myModel.set('operation', selectedOperation);
			
				//reload activity types
				AssetManagement.customer.controller.ClientStateController.enterLoadingState();
				
				var me = this;
				var loadedCallback = function(activityTypes) {
					try {
						if(!activityTypes)
							activityTypes = null;
					
						myModel.set('activityTypes', activityTypes);

                        //load the matching notification
                        me.loadOperationNotification();
					
						//initialize a new time conf
						me.initializeNewTimeConf();
				        me.refreshView();
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseIntegratedConfPageController', ex);
					    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					} 
				};
				
				var eventId = AssetManagement.customer.manager.ActivityTypeManager.getActivityTypes(selectedOperation.get('arbpl'), true);

				if(eventId > 0) {
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, loadedCallback);
				} else {
					me.initializeNewTimeConf();
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseIntegratedConfPageController', ex);
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},

    loadOperationNotification: function() {
        try {
            //reload activity types
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			var me = this;
			var loadedCallback = function(notif) {
				try {	
                    me.getViewModel().set('notif', notif);
				    me.refreshView();
                    me.getView().updatePageContent();
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseIntegratedConfPageController', ex);
				} finally {
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};

            var operation = this.getViewModel().get('operation'); 
            var order = this.getViewModel().get('order'); 
            var objListItem  = null
            if (operation !== null) {
                objListItem = AssetManagement.customer.manager.ObjectListManager.getObjectListItemForOperation(order, operation);
            }
            if (objListItem !== null) {				
                var eventId = AssetManagement.customer.manager.NotifManager.getNotif(objListItem.get('ihnum'), true);
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, loadedCallback);
            } else {
				this.refreshView();
                this.getView().updatePageContent();
				AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			}
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadOperationNotification of BaseIntegratedConfPageController', ex);
        }   
    },

    isWorkCostOperation: function() {
        var retVal = true;
        try {
            if (this.getViewModel().get('notif') === null || this.getViewModel().get('notif') === 'undefined')
                retVal = false;
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isWorkCostOperation of BaseIntegratedConfPageController', ex);
        }
        return retVal;
    },
	
	onLearrSelected: function(comboBox, newValue, oldValue) {
		try {	
			var selectedLearr = comboBox.getValue();
			
			if(selectedLearr.get('leinh').toUpperCase() === 'H')
				this.getView().showTimeField(); 
			else
				this.getView().showTimeTextField(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseIntegratedConfPageController', ex);
		}
	},
	
	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
		try {
			e.stopEvent(); 
		
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			if(record.get('updFlag') === 'I') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
			}
				
			this.rowMenu.onCreateContextMenu(options, record); 
			
			if(options.length > 0) {
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseIntegratedConfPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
				this.onDeleteClick(record);
			}
			else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
				this.onEditClick(record); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseIntegratedConfPageController', ex);
		}
	},
	///------ CONTEXT MENU END -------///
	
	//on delete item tapped
	onDeleteClick: function(timeConf) {
		try {
			var currentTimeConf = this.getViewModel().get('timeConf');
			var deletingCurrentEditingTimeConf = timeConf.get('aufnr') === currentTimeConf.get('aufnr') &&
													timeConf.get('vornr') === currentTimeConf.get('vornr') &&
														timeConf.get('split') === currentTimeConf.get('split') &&
															timeConf.get('rmzhl') === currentTimeConf.get('rmzhl');
				
			
			var me = this; 
			var callback = function(success) {
				try {
					if(success === true) {
 						if(deletingCurrentEditingTimeConf)
 							me.initializeNewTimeConf();
					
						me.deleteTimeConfFromListStore(timeConf);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('timeConfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteClick of BaseIntegratedConfPageController', ex);
				}
			};
			
			var eventId = AssetManagement.customer.manager.TimeConfManager.deleteTimeConf(timeConf, false);
			
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteClick of BaseIntegratedConfPageController', ex);
		}
	},
	
	onEditClick: function(timeConf) {
		try {
			var myModel = this.getViewModel();
			
			myModel.set('timeConf', timeConf);
			myModel.set('isEditMode', true);
			
			this.refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEditClick of BaseIntegratedConfPageController', ex);
		}
	}
});