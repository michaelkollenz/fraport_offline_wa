Ext.define('AssetManagement.base.controller.pages.BaseLoginPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.manager.UserManager',
	    'AssetManagement.customer.controller.ToolbarController',
//	    'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
	    'AssetManagement.customer.helper.LanguageHelper',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	inheritableStatics: {
		MODE_LOGIN: 0,
		MODE_CHANGE_PASSWORD: 1
	},
	
	config: {
		contextReadyLevel: 1
	},
	
	//private
	_requestRunning: false,
	
	//protected
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			//load the current values from app config into the view model and set mode 0
			var ac = AssetManagement.customer.core.Core.getAppConfig();
			var defaultMandt = ac.getMandt();
			var user = ac.getUserId();

			var myModel = this.getViewModel();
			myModel.set('mode', this.self.MODE_LOGIN);
			myModel.set('mandt', !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(defaultMandt) ? defaultMandt : '001');
			myModel.set('user', user);
			myModel.set('languages', AssetManagement.customer.helper.LanguageHelper.getAvailableLanguages());

			myModel.set('selectedLocale', ac.getLocale());
			
			if(argumentsObject) {
				if(argumentsObject.statusMessage)
					myModel.set('statusMessage', argumentsObject.statusMessage);
			}
		
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseLoginPageController', ex);
		}	
	},
	
	//public
	//trigger a login using the current input values
	onLoginButtonClick: function() {
		try {
			if(this._requestRunning === true)
				return;
		
			//get the current values from controls into the model
			this.getView().transferViewStateIntoModel();
			
			this.requestLogin();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLoginButtonClick of BaseLoginPageController', ex);
		}	
	},
	
	//will open the change password perspective or trigger a change password request
	onChangePasswordButtonClick: function() {
		try {
			if(this._requestRunning === true)
				return;
		
			var myModel = this.getViewModel();
		
			this.getView().transferViewStateIntoModel();
			switch(myModel.get('mode')) {
				case this.self.MODE_LOGIN:
						this.getViewModel().set('mode', this.self.MODE_CHANGE_PASSWORD);
						myModel.set('oldPassword', myModel.get('password'));
						myModel.set('newPassword', '');
						myModel.set('newPasswordRepeat', '');
						myModel.set('statusMessage', Locale.getMsg('enterCurrentPasswordConnectNetworkModifyingPassword'));
									
						this.getView().refreshView();
					break;
				case this.self.MODE_CHANGE_PASSWORD:
					this.requestChangePassword();
					break;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onChangePasswordButtonClick of BaseLoginPageController', ex);
		}	
	},
	
	//will switch to normal login perspective
	onCancelChangePasswordButtonClick: function() {
		try {
			var myModel = this.getViewModel();
			myModel.set('mode', this.self.MODE_LOGIN);
			myModel.set('statusMessage', '');
			
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCancelChangePasswordButtonClick of BaseLoginPageController', ex);
		}
	},
	
	onLocaleSelected: function(combobox, record) {
		try {
			//only do something, if the selected value differs from the current set one
			//else it causes an infinity loop, because it triggers a refresh view, what will trigger a selected event again
			if(record && record !== AssetManagement.customer.core.Core.getAppConfig().getLocale()) {
				AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
			
				this.getView().transferViewStateIntoModel();
				AssetManagement.customer.controller.ClientStateController.setClientLocale(record);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLocaleSelected of BaseLoginPageController', ex);
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	//protected
	onLocaleChanged: function(newLocale) {
		this.callParent();
		
		AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

		try {
			var myModel = this.getViewModel();
			myModel.set('languages', AssetManagement.customer.helper.LanguageHelper.getAvailableLanguages());
			
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLanguageChanged of BaseLoginPageController', ex);
		}
	},
	
	requestLogin: function() {
		try {
			this._requestRunning = true;
		
			var myModel = this.getViewModel();
			var mandt = myModel.get('mandt');
			var user = myModel.get('user');
			var password = myModel.get('password');
			
			var errorMessage = '';

			//prevent empty spaces in username and password (not inside)
			user = user ? user.trim() : null;
			password = password ? password.trim() : null;
		
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mandt)) {
				errorMessage = Locale.getMsg('sapMandtMandatory');
			} else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(user)) {
				errorMessage = Locale.getMsg('pleaseProvideYourUsername');
			} else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(password)) {
				errorMessage = Locale.getMsg('pleaseProvideYourPassword');
			}
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(errorMessage)) {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				
				myModel.set('statusMessage', errorMessage);
				this.getView().refreshView();
				
				this._requestRunning = false;
				
				return;
			}
			
			AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
			
			var me = this;
			
			var successCallback = function() {
			    try {
			        //set back the login blocking variable defered - because the navigation will take a moment also
                    //this avoids unwanted retriggering of another login sequence
			        Ext.defer(function () {
			            me._requestRunning = false;
			        }, 2000, me);
				} catch(ex) {
				    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestLogin of BaseLoginPageController', ex);
				    me._requestRunning = false;
				}
			};
			
			var failedCallback = function(userInfo) {
				try {
					me.onLoginFailed.call(me, userInfo);
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestLogin of BaseLoginPageController', ex);
				} finally {
					me._requestRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			myModel.set('statusMessage', Locale.getMsg('performingLoginPleaseWait'));
			this.getView().refreshView();
			AssetManagement.customer.controller.ClientStateController.tryLogInUser(mandt, user, password, successCallback, failedCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestLogin of BaseLoginPageController', ex);
			this._requestRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}	
	},
	
	onLoginFailed: function(userInfoOrString) {
		try {
			var myModel = this.getViewModel();
			
			var message = '';
			
			if(userInfoOrString) {
				if(userInfoOrString.getErrorMessage)
					message = userInfoOrString.getErrorMessage();
				else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(userInfoOrString))
					message = userInfoOrString;
    		}
			
    		var mode = this.self.MODE_LOGIN;
    		
    		if(userInfoOrString && userInfoOrString.getChangePasswordRequired && userInfoOrString.getChangePasswordRequired())
    			mode = this.self.MODE_CHANGE_PASSWORD;
		
			myModel.set('statusMessage', message);
			myModel.set('mode', mode);
			
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLoginFailed of BaseLoginPageController', ex);
		}
	},
	
	requestChangePassword: function() {
		try {
			this._requestRunning = true;
			
			var myModel = this.getViewModel();
			var mandt = myModel.get('mandt');
			var user = myModel.get('user');
			var oldPassword = myModel.get('oldPassword');
			var newPassword = myModel.get('newPassword');
			var newPasswordRepeat = myModel.get('newPasswordRepeat');
			
			var errorMessage = '';
		
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mandt)) {
				errorMessage = Locale.getMsg('sapMandtMandatory');
			} else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(user)) {
				errorMessage = Locale.getMsg('pleaseProvideYourUsername');
			} else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(oldPassword)) {
				errorMessage = Locale.getMsg('pleaseProvideYourOldPassword');
			} else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(newPassword)) {
				errorMessage = Locale.getMsg('pleaseProvideYourNewPassword');
			} else if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(newPasswordRepeat)) {
				errorMessage = Locale.getMsg('pleaseRepeatYourPassword');
			} else if(newPassword !== newPasswordRepeat) {
				errorMessage = Locale.getMsg('yourPasswortRepeatIsIncorrect');
			}
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(errorMessage)) {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				
				myModel.set('statusMessage', errorMessage);
				this.getView().refreshView();
				
				this._requestRunning = false;

				return;
			}
			
			AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
			
			var me = this;
			
			var callback = function(success, result) {
				try {
					if(success === true) {
						me.onChangePasswordSuccessful();
					} else {
						me.onChangePasswordFailed(result);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestChangePassword of BaseLoginPageController', ex);
				} finally {
					me._requestRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			myModel.set('statusMessage', Locale.getMsg('changeRequestRunning'));
			this.getView().refreshView();
			
			AssetManagement.customer.manager.UserManager.tryChangeUserPassword(mandt, user, oldPassword, newPassword, callback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestChangePassword of BaseLoginPageController', ex);
			this._requestRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	onChangePasswordSuccessful: function(userInfo) {
		try {
			var myModel = this.getViewModel();
		
			myModel.set('password', myModel.get('newPassword'));
			myModel.set('mode', this.self.MODE_LOGIN);
			myModel.set('statusMessage', Locale.getMsg('passwordChangeSuccessful'));
			
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onChangePasswordSuccessful of BaseLoginPageController', ex);
		}
	},
	
	onChangePasswordFailed: function(result) {
		try {
			var myModel = this.getViewModel();
			myModel.set('statusMessage', result._message);
			
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onChangePasswordFailed of BaseLoginPageController', ex);
		}
	}
});