Ext.define('AssetManagement.base.controller.pages.BaseHistOrderDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
	config: {
		contextReadyLevel: 1
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var histOrder = null; 
			if(argumentsObject['histOrder'] && Ext.getClassName(argumentsObject['histOrder']) === 'AssetManagement.customer.model.bo.OrderHistory') {
				this.getViewModel().set('histOrder', argumentsObject['histOrder']);
				histOrder = argumentsObject['histOrder'];
			} else {
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.HistOrderDetailPageController'
			    });
			}
					  			
		    if(histOrder) {
		    	this.loadDependantData(histOrder)
			} else {
				this.waitForDataBaseQueue();
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseHistOrderDetailPageController', ex);
		    this.errorOccurred();
		}
	},
	
	loadDependantData: function(histOrder) {
	  try {  
	        var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLoc(histOrder.get("tplnr"), true);
			this.addRequestToDataBaseQueue(funcLocRequest, 'tpl');
				
			var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipment(histOrder.get("equnr"), true);
			this.addRequestToDataBaseQueue(equiRequest, 'equi');

		    this.waitForDataBaseQueue(); 
	    } catch (ex){
	    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependantData of BaseHistOrderDetailPageController', ex);
	    }
	}	
});