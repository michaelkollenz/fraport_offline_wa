Ext.define('AssetManagement.base.controller.pages.BaseHomeScreenPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],
		
	config: {
		contextReadyLevel: 0,
		isAsynchronous: false
	},
	
	onOrderButtonClick: function(button, e, eOpts) {
	    try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_LIST);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderButtonClick of BaseHomeScreenPageController', ex);
		}
	},

	onInventoryButtonClick: function (button, e, eOpts) {
	    try {
	        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_INVENTORY_LIST);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifButtonClick of BaseHomeScreenPageController', ex);
	    }
	},

	onNotifButtonClick: function(button, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_LIST);
		} catch(ex) {			
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifButtonClick of BaseHomeScreenPageController', ex);
		}
	},
	
	onEquiButtonClick: function(button, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_LIST);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiButtonClick of BaseHomeScreenPageController', ex);
		}
	},
	
	onFuncLocButtonClick: function(button, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_LIST);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocButtonClick of BaseHomeScreenPageController', ex);
		}
	},
	
	onDemandReqButtonClick: function(button, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_BANF_EDIT);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDemandReqButtonClick of BaseHomeScreenPageController', ex);
		}
	},

	onCalendarButtonClick: function(button, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CALENDAR);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCalendarButtonClick of BaseHomeScreenPageController', ex);
		}
	},
	
	onMatOrdersButtonClick: function(button, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SDORDER_LIST);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMatOrdersButtonClick of BaseHomeScreenPageController', ex);
		}
	},
	
	onChecklistButtonClick: function(button, e, eOpts){
	    try{
            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CHECKLIST)
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onChecklistButtonClick of BaseHomeScreenPageController', ex);
	    }
	},

	onSyncButtonClick: function(button, e, eOpts) {
		try {
			var syncCallback = function(success) {
				try {
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSyncButtonClick of BaseHomeScreenPageController', ex);
				}
			};
			
			AssetManagement.customer.core.Core.getMainView().getController().performSync(syncCallback, this, true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSyncButtonClick of BaseHomeScreenPageController', ex);
		}
	},
	
    buttonSize: function() {
	   try {
	       var orderButton = Ext.getCmp('homeScreenOrderButton');
	       var notifButton = Ext.getCmp('homeScreenNotifButton');
	       var equiButton = Ext.getCmp('homeScreenEquiButton');
	       var funclocButton = Ext.getCmp('homeScreenFuncLocButton');
	       var syncButton = Ext.getCmp('homeScreenSyncButton');
	       
	       var sizeDimensions = Ext.getBody().getViewSize();
	       var newHeight = sizeDimensions.height * 0.18;
	       var newWidth = sizeDimensions.width* 0.2;
	       
	       orderButton.setHeight(newHeight);
           orderButton.setWidth(newWidth);
	    
	       notifButton.setHeight(newHeight);
	       notifButton.setWidth(newWidth);

	       equiButton.setHeight(newHeight);
	       equiButton.setWidth(newWidth);
	    
	       funclocButton.setHeight(newHeight);
	       funclocButton.setWidth(newWidth);
	       
	       syncButton.setHeight(newHeight);
	       syncButton.setWidth(newWidth);
       } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buttonSize of BaseHomeScreenPageController', ex);
	   }    
    }
});