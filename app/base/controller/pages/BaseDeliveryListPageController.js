Ext.define('AssetManagement.base.controller.pages.BaseDeliveryListPageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',


  requires: [
    'AssetManagement.customer.manager.InternalDeliveryManager',
    'AssetManagement.customer.manager.IAReasonManager',
    'AssetManagement.customer.helper.NetworkHelper',
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.OxLogger',
    // 'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
  ],

  config: {
    contextReadyLevel: 1,
    isAsynchronous: true
  },

  // mixins: {
  //     handlerMixin: 'AssetManagement.customer.controller.mixins.DeliveryListPageControllerMixin'
  // },

  //public
  //@override
  onOptionMenuItemSelected: function (optionID) {
    try {
      if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
        if (this._saveRunning === true)
          return;

        this.trySaveItem();
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseDeliveryListPageController', ex);
    }
  },
  afterDataBaseQueueComplete: function () {

    try {
      var myModel = this.getViewModel();
      var deliveryItems = myModel.get('listItems');

      if (deliveryItems && deliveryItems.getCount() > 0) {
        deliveryItems.each(function (item) {
          if (item.get('ia_status') !== 'L' || item.get('grund') !== '000') {
            deliveryItems.remove(item);
          }
        });

      }
      deliveryItems.sort('raum', 'ASC');

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseDeliveryListPageController', ex);
    }
  },
  //private
  onCreateOptionMenu: function () {
    try {
      this._optionMenuItems = [AssetManagement.customer.controller.ToolbarController.OPTION_SAVE];
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseDeliveryListPageController', ex);
    }
  },
//onZugChanged
  onZugChanged: function (radioGroup, newValue, oldValue, eOpts) {
    try {
      if (newValue) {
        var record = radioGroup.up().getWidgetRecord();
        if (radioGroup.up()._rowContext.getWidgets()[1]) {
          var combobox = radioGroup.up()._rowContext.getWidgets()[1].items.items[1];
        }
        var grund = record.get('grund');
        if (grund === '000') {
          grund = '';
        }
        record.set('ia_status', newValue);
        if (combobox) {
          if (newValue !== 'XX') {
            combobox.setReadOnly(true);
            combobox.setValue('');
          } else {
            combobox.setReadOnly(false);
            combobox.setValue(grund);

          }
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onZugChanged of BaseDeliveryListPageController', ex);
    }
  },

  onReasonChanged: function (field, newValue, oldValue, eOpts) {
    try {
      var record = field.up().getWidgetRecord();
      record.set('grund', newValue);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onReasonChanged of BaseDeliveryListPageController', ex);
    }
  },
  //@override
  startBuildingPageContext: function (argumentsObject) {
    try {
      this.loadListStore();
      this.loadIAReasons();

      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseDeliveryListPageController', ex);
    }
  },

  loadIAReasons: function () {
    try {
      var reasonStoreRequest = AssetManagement.customer.manager.IAReasonManager.getAllIAReason(true);
      this.addRequestToDataBaseQueue(reasonStoreRequest, 'reasonStore');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadIAReasons of BaseDeliveryListPageController', ex);
    }
  },

  loadListStore: function () {
    try {
      var deliveryStoreRequest = AssetManagement.customer.manager.InternalDeliveryManager.getDeliverys(true);
      this.addRequestToDataBaseQueue(deliveryStoreRequest, 'listItems');
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseDeliveryListPageController', ex);
    }
  },

  //list handler
  onDeliverySelected: function (tableview, td, cellIndex, record, tr, rowIndex, event, eOpts) {
    try {

      // AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, {aufnr: record.get('aufnr')});
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeliverySelected of BaseDeliveryListPageController', ex);
    }
  },

  onDeliverySelectedByTabHold: function (delivery) {
  },

  onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    try {
      e.stopEvent();
      if (!this.rowMenu)
        this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

      var options = [];

      this.rowMenu.onCreateContextMenu(options, record);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseDeliveryListPageController', ex);
    }
  },


  trySaveItem: function () {
    try {
      // this._saveRunning = true;

      // AssetManagement.customer.controller.ClientStateController.enterLoadingState();

      var me = this;

      var saveItem = function (checkWasSuccessful, deliveryItems) {
        try {
          if (checkWasSuccessful === true) {

            var reasonStore = me.getViewModel().get('reasonStore');

            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_REPORT, {
              deliveryStore: deliveryItems,
              reasonStore: reasonStore
            });

          } else {
            // me._saveRunning = false;
            // AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseInventoryDetailPageController', ex);
          // me._saveRunning = false;
          // AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
      };

      var deliveryItems = me.getViewModel().get('listItems');

      //check if data is complete first
      this.checkInputValues(deliveryItems, saveItem, this);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseInventoryDetailPageController', ex);
      // this._saveRunning = false;
      // AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
    }

  },

  checkInputValues: function (deliveryItems, callback, callbackScope) {
    try {
      var retval = true;
      var errorMessage = '';
      var ac = AssetManagement.customer.core.Core.getAppConfig();
      var now = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime();
      var date = AssetManagement.customer.utils.DateTimeUtils.getDateString(now);
      var time = AssetManagement.customer.utils.DateTimeUtils.getTimeString(now);
      var toHash = [deliveryItems.data.items[0].get('gebnr'),ac.getUserId(),ac.getMandt(),date, time];
      var hash = AssetManagement.customer.manager.KeyManager.createKey(toHash);


      var saveItems = Ext.create('Ext.data.Store', {
        model: 'AssetManagement.customer.model.bo.InternalDelivery',
        autoLoad: false
      });


      if (deliveryItems && deliveryItems.getCount() > 0) {
        deliveryItems.each(function (item) {
          var ia_status = item.get('ia_status');
          if ( ia_status === 'Z' || ia_status === 'XX' ) {
            item.set('mobilekey', hash);
            saveItems.add(item);
          } else {
          }
        });
      }

      if (saveItems && saveItems.getCount() > 0) {
        saveItems.each(function (saveItem) {
          if(retval){
            var grund = saveItem.get('grund');
            var iaStatus = saveItem.get('ia_status');
            if (iaStatus === 'XX' && (grund === '' || grund === '000')) {
              errorMessage = Locale.getMsg('errorReason');
              retval = false;
            }
          }
        });
      }
      if(saveItems && saveItems.getCount() === 0){
        errorMessage = Locale.getMsg('noItemsDone');
        retval = false;
      }

      // show notification about current action on the buttom
      var returnFunction = function () {
        if (retval === false) {
          var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
          AssetManagement.customer.core.Core.getMainView().showNotification(message);
        }
        callback.call(callbackScope ? callbackScope : me, retval, saveItems);
      };
      returnFunction.call(this);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseInventoryDetailPageController', ex);

      if (callback) {
        callback.call(callbackScope ? callbackScope : this, false);
      }
    }
  }

});