Ext.define('AssetManagement.base.controller.pages.BaseTimeConfPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.utils.NumberFormatUtils',
		'AssetManagement.customer.manager.ActivityTypeManager',
		'AssetManagement.customer.manager.CustBemotManager',
		'AssetManagement.customer.manager.CustReasonManager',
		'AssetManagement.customer.model.bo.TimeConf',
		'AssetManagement.customer.view.ContextMenu',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],

	config: {
		contextReadyLevel: 2,
		rowMenu: null
	},
	
	//private
	_saveRunning: false,
	_initialVornr: '',
	_initialSplit: '',
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
			
				this.trySaveTimeConf(); 
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
				if(this._saveRunning === true)
					return;
				
				this.initializeNewTimeConf();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseTimeConfPageController', ex);
		}
	},
		
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM, AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseTimeConfPageController', ex);
		}
	},
	
	//protected
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			this._initialVornr = '';
			this._initialSplit = '';
		
			var myModel = this.getViewModel();
			
			var aufnr = '', vornr = '', split = '';
			var order = null;
			
			if(typeof argumentsObject['aufnr'] === 'string') {
				aufnr = argumentsObject['aufnr'];
			} else if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
				order = argumentsObject['order'];
				myModel.set('order', order); 
			}
			
			if(typeof argumentsObject['vornr'] === 'string') {
				this._initialVornr = argumentsObject['vornr'];
			} else if(Ext.getClassName(argumentsObject['operation']) === 'AssetManagement.customer.model.bo.Operation') {
				var operation = argumentsObject['operation'];
				this._initialVornr = operation.get('vornr');
				this._initialSplit = operation.get('split');
			}
			
			if(typeof argumentsObject['split'] === 'string') {
				this._initialSplit = argumentsObject['split'];
			}
					
			if(Ext.getClassName(argumentsObject['timeConf']) === 'AssetManagement.customer.model.bo.TimeConf') {
				var timeConf = argumentsObject['timeConf'];
				myModel.set('isEditMode', true);
				myModel.set('timeConf', timeConf);
				
				this._initialVornr = timeConf.get('vornr');
				this._initialSplit = timeConf.get('split');
			}
			
			if(aufnr !== '') {
				var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
				this.addRequestToDataBaseQueue(orderRequest, 'order');
			}

			if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr) && !order)
			{
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.TimeConfPageController'
			    });
			}

			this.waitForDataBaseQueue();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseTimeConfPageController', ex);
		    this.errorOccurred();
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		try {
			if(this._contextLevel === 1) {
				this.setInitialOperation();
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseTimeConfPageController', ex);
		}
	},
	
	//will set an initial operation for the model
	setInitialOperation: function() {
		try {
			var myModel = this.getViewModel();
			var ordersOperations = myModel.get('order').get('operations');
			var initialOperation = null;
			
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this._initialVornr)) {
				//iterate the orders operation to locate a match
				var match = null;
				
				if(ordersOperations && ordersOperations.getCount() > 0) {
					ordersOperations.each(function(operation) {
						if(operation.get('vornr') === this._initialVornr && operation.get('split') === this._initialSplit) {
							match = operation;
							return false;
						}
					}, this)
				}
				
				if(match) {
					initialOperation = match;
				} else {
					//the requested operation could not be found
					//if a timeConf has been passed for editing, this timeconf can not be accepted and has to be set back
					//inform the user!
					
					if(myModel.get('timeConf')) {
						myModel.set('timeConf', null);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('requestOperationNotFound'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				}
			}

			//no specific operation requested or found - so use order's first operation
			if(!initialOperation && ordersOperations && ordersOperations.getCount() > 0) {
				initialOperation = ordersOperations.getAt(0);
			}
			
			myModel.set('operation', initialOperation);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitialOperation of BaseTimeConfPageController', ex);
		}
	},
			
	performContextLevel1Requests: function() {
		try {
			var operation = this.getViewModel().get('operation');
			
			var activityTypesRequest = AssetManagement.customer.manager.ActivityTypeManager.getActivityTypes(operation.get('arbpl'), true);
			this.addRequestToDataBaseQueue(activityTypesRequest, "activityTypes");
			
			var accountIndicationsRequest = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
			this.addRequestToDataBaseQueue(accountIndicationsRequest, "accountIndications");

            var ordTypesRequest = AssetManagement.customer.manager.Cust_011Manager.getOrderTypes(true);
            this.addRequestToDataBaseQueue(ordTypesRequest, "orderTypes");

			var timeConf = this.getViewModel().get('timeConf');
			if(timeConf) {
				var longtextForTimeConfRequest = AssetManagement.customer.manager.LongtextManager.getLongtext(timeConf, true);
				this.addRequestToDataBaseQueue(longtextForTimeConfRequest, "longtext");
			}
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of BaseTimeConfPageController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			//set the operation according to the current data
			var myModel = this.getViewModel();
			
			var curOperation = myModel.get('operation');
			var curTimeConf = myModel.get('timeConf');

			if(!curOperation) {
				var operations = myModel.get('order').get('operations');
			
				if(operations && operations.getCount() > 0) {
					var operationToSet = null;
				
					if(curTimeConf) {
						//set the operation convenient to curTimeConf
						var timeConfsVornr = curTimeConf.get('vornr');
						var timeConfsSplit = curTimeConf.get('split');
						
						operations.each(function(operation) {
							if(operation.get('vornr') === timeConfsVornr && operation.get('split') === timeConfsSplit) {
								operationToSet = operation;
								return false;
							}
						}, this);
					} else {
						//set to first operation
						operationToSet = operations.getAt(0);
					}
					
					myModel.set('operation', operationToSet);
				}
			}
		
			if(!this.getViewModel().get('timeConf'))
				this.initializeNewTimeConf(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseTimeConfPageController', ex);
		}
	},
	
	//will initialize a new timeConf and transfer it into the model
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewTimeConf: function(skipRefreshView) {
		try {
		    if (skipRefreshView === true)
		        skipRefreshView = true;
		    else
		        skipRefreshView = false;

		    var myModel = this.getViewModel();
		    var order = myModel.get('order');
		    var operation = myModel.get('operation');

		    var newTimeConf = Ext.create('AssetManagement.customer.model.bo.TimeConf', { order: order, operation: operation });

		    //set default activityType, if time conf did not draw any from operation
		    var timeConfsLearr = newTimeConf.get('learr');

		    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsLearr)) {
		        var activityTypes = myModel.get('activityTypes');

		        if (activityTypes && activityTypes.getCount() > 0) {
		            var defaultActivityType = activityTypes.getAt(0);
		            var actLearr = defaultActivityType.get('lstar');
		            var actUnit = defaultActivityType.get('leinh');

		            newTimeConf.set('learr', actLearr);
		            newTimeConf.set('ismne', actUnit);
		            newTimeConf.set('idaue', actUnit);
		        }
		    } else {
		        //override unit of timeconf with unit of it's activity type (if it can be found)
		        var activityTypes = myModel.get('activityTypes');

		        if (activityTypes && activityTypes.getCount() > 0) {
		            activityTypes.each(function (activityType) {
		                if (activityType.get('lstar') === timeConfsLearr) {
		                    var actUnit = activityType.get('leinh')

		                    newTimeConf.set('ismne', actUnit);
		                    newTimeConf.set('idaue', actUnit);

		                    return false;
		                }
		            }, this);
		        }
		    }

		    //set default unit, if time conf could not draw it from it's activity type or operation
		    var timeConfsUnit = newTimeConf.get('ismne');

		    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsUnit)) {
		        timeConfsUnit = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('default_timeunit');
		        newTimeConf.set('ismne', timeConfsUnit);
		        newTimeConf.set('idaue', timeConfsUnit);
		    }
			
			//set default account indication, if time conf did not draw any from order
			var timeConfsBemot = newTimeConf.get('bemot');
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(timeConfsBemot)) {
				var accountIndications = myModel.get('accountIndications');
			
				if(accountIndications && accountIndications.getCount() > 0) {
					timeConfsBemot = accountIndications.getAt(0).get('bemot');
					newTimeConf.set('bemot', timeConfsBemot);
				}
			}
			
			//set default time value, matching to it's unit
			var defaultTimeInMinutes = 60.0;
			var asHours = true;
			var toTest = timeConfsUnit.toUpperCase();
			var toSet = 0;
			
			if(toTest === 'M' || toTest === 'MIN')
				asHours = false;
			
			if(asHours) {
				toSet = defaultTimeInMinutes / 60.0;
			} else {
				toSet = defaultTimeInMinutes;
			}
			
			//bring into backend format
			var toSet = AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(toSet);
			
			newTimeConf.set('ismnw', toSet);
			newTimeConf.set('idaur', toSet);

        	newTimeConf.set('ersda', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('isd', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('ied', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
        	newTimeConf.set('budat', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
			newTimeConf.set('ernam', AssetManagement.customer.core.Core.getAppConfig().getUserId());
			newTimeConf.set('localLongtext', '');

			newTimeConf.set('mobileKey', order ? order.get('mobileKey') : '');
			newTimeConf.set('childKey', operation ? operation.get('childKey'): '');

			myModel.set('timeConf', newTimeConf);
			myModel.set('isEditMode', false);
			
			if(skipRefreshView === false)
				this.refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewTimeConf of BaseTimeConfPageController', ex);
		}
	},
	
	/**
	 * will try to save the current timeconf inside the view
	 * a set of checks will be run before the values are extracted from the view and saved to the database
	 */
	trySaveTimeConf: function() {
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
			var me = this;
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						var timeConf = me.getViewModel().get('timeConf'); 
						
						me.getView().transferViewStateIntoModel();

						var callback = function(success) {
							try {
								if(success === true) {
									me.initializeNewTimeConf();
									me.addTimeConfToListStore(timeConf);
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedTimeConf'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingTimeConf'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseTimeConfPageController', ex);
							} finally {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							}
						};
						
						var eventId = AssetManagement.customer.manager.TimeConfManager.saveTimeConf(timeConf);
						
						if(eventId > 0) {
							AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
						} else {
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingTimeConf'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							me._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseTimeConfPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseTimeConfPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	 
	/**
	 * checks if the data of the timeconf object is complete 
	 * returns null if the data is complete or an error message if data is missing
	 */
	checkInputValues: function(callback, callbackScope)  {
		try {
		    var retval = true;
		    var errorMessage = '';
		    var me = this;

		    var returnFunction = function () {
		        if (retval === false) {
		            var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
		            AssetManagement.customer.core.Core.getMainView().showNotification(message);
		        }

		        callback.call(callbackScope ? callbackScope : me, retval);
		    };

		    var values = this.getView().getCurrentInputValues();

		    var activityType = values.activityTpye;
		    var accountIndication = values.accountIndication;
		    var work = values.work;
		    var startDate = values.startDate;
		    var endDate = values.endDate;
		    var finConf = values.finConf;
		    var localLongtext = values.localLongtext;

		    var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;

		    var workIsObject = typeof (work) === 'object';

		    var workAsString = '';

		    if (workIsObject) {
		        workAsString = AssetManagement.customer.utils.DateTimeUtils.getTimeStringWithTimeZoneForDisplay(work);
		    } else {
		        var workAsInt = 0;

		        try {
		            workAsInt = parseInt(work);
		        } catch (ex) {
		        }

		        if (isNaN(workAsInt) || workAsInt < 0) {
		            errorMessage = Locale.getMsg('provideValidWorkTime');
		            retval = false;
		            returnFunction.call(this);
		            return;
		        }

		        workAsString = workAsInt + '';
		    }

		   //OXMCSTD-284 not needed anymore
		   /* if (!activityType) {
		        errorMessage = Locale.getMsg('chooseActivityType');
		        retval = false;
		    } else if (!accountIndication) {
		        //errorMessage = Locale.getMsg('chooseAccountIndication');
		       // retval = false;
		    } */
		   if (!finConf && (workIsObject && workAsString === '00:00' || !workIsObject && workAsString === '0')) {
		        errorMessage = Locale.getMsg('noWorkTimeOper');
		        retval = false;
		    } else if (startDate > endDate) {
		        errorMessage = Locale.getMsg('startAfterEndTime');
		        retval = false;
		    }
			
			returnFunction.call(this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseTimeConfPageController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	onOperationSelected: function(comboBox, record) {
		try {
			var myModel = this.getViewModel();
			var currentOperation = myModel.get('operation');
			var selectedOperation = comboBox.getValue();
			
			if(selectedOperation !== currentOperation) {
				myModel.set('operation', selectedOperation);
			
				//reload activity types
				AssetManagement.customer.controller.ClientStateController.enterLoadingState();
				
				var me = this;
				var loadedCallback = function(activityTypes) {
					try {
						if(!activityTypes)
							activityTypes = null;
					
						myModel.set('activityTypes', activityTypes);
					
						//initialize a new time conf
						me.initializeNewTimeConf();
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseTimeConfPageController', ex);
					} finally {
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				};
				
				var eventId = AssetManagement.customer.manager.ActivityTypeManager.getActivityTypes(selectedOperation.get('arbpl'), true);

				if(eventId > 0) {
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, loadedCallback);
				} else {
					me.initializeNewTimeConf();
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseTimeConfPageController', ex);
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	onLearrSelected: function(comboBox, newValue, oldValue) {
		try {	
			var selectedLearr = comboBox.getValue();
            if (comboBox.getValue() == "" || comboBox.getValue() == "&nbsp;")
                comboBox.setValue(null);
            else if(selectedLearr.get('leinh').toUpperCase() === 'H')
				this.getView().showTimeField(); 
			else
				this.getView().showTimeTextField(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseTimeConfPageController', ex);
		}
	},


    onBemotSelected: function (comboBox, newValue, oldValue) {
        var selectedBemot = comboBox.getValue();
        if (comboBox.getValue() == "" || comboBox.getValue() == "&nbsp;")
            comboBox.setValue(null);
    },
	
	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
		try {
			e.stopEvent(); 
		
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			if(record.get('updFlag') === 'I') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
			}
			
			this.rowMenu.onCreateContextMenu(options, record); 
			
			if(options.length > 0) {
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseTimeConfPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
				this.onDeleteClick(record);
			}
			else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
				this.onEditClick(record); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseTimeConfPageController', ex);
		}
	},
	///------ CONTEXT MENU END -------///
	
	//on delete item tapped
	onDeleteClick: function(timeConf) {
		try {
			var currentTimeConf = this.getViewModel().get('timeConf');
			var deletingCurrentEditingTimeConf = timeConf.get('aufnr') === currentTimeConf.get('aufnr') &&
													timeConf.get('vornr') === currentTimeConf.get('vornr') &&
														timeConf.get('split') === currentTimeConf.get('split') &&
															timeConf.get('rmzhl') === currentTimeConf.get('rmzhl');
				
			
			var me = this; 
			var callback = function(success) {
				try {
					if(success === true) {
 						if(deletingCurrentEditingTimeConf)
 							me.initializeNewTimeConf();
					
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('timeConfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteClick of BaseTimeConfPageController', ex);
				}
			};
			
			var eventId = AssetManagement.customer.manager.TimeConfManager.deleteTimeConf(timeConf, false);
			
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteClick of BaseTimeConfPageController', ex);
		}
	},
	
	onEditClick: function(timeConf) {
		try {
			var me = this;
			var myModel = this.getViewModel();
			var eventController = AssetManagement.customer.controller.EventController.getInstance();

			var afterLongtextLoadedCallback = function(localLongtext) {
				try {
					timeConf.set('localLongtext', localLongtext);
					myModel.set('timeConf', timeConf);
					myModel.set('isEditMode', true);
					me.refreshView(); 
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEditClick of BaseTimeConfPageController', ex);
				}
			}

			this.loadLongtextFromDatabase(timeConf, afterLongtextLoadedCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEditClick of BaseTimeConfPageController', ex);
		}
	},

	loadLongtextFromDatabase: function(timeConf, callback) {
		try {
			var eventController = AssetManagement.customer.controller.EventController.getInstance();

			var loadLongtextRequest = AssetManagement.customer.manager.LongtextManager.getLongtext(timeConf, false);
			if(loadLongtextRequest > -1) {
				eventController.registerOnEventForOneTime(loadLongtextRequest, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadLongtextFromDatabase of BaseTimeConfPageController', ex);
		}
	},

    addTimeConfToListStore: function(timeConf) {
        try {
            var myModel = this.getViewModel();

            var operation = myModel.get('operation');
            var curOpersTimeConfs = operation.get('timeConfs');

            if (!curOpersTimeConfs) {
                var timeConfs = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.TimeConf',
                    autoLoad: false
                });

                operation.set('timeConfs', timeConfs)
            }

            var temp = curOpersTimeConfs.getById(timeConf.get('id'));

            if (temp) {
                var index = curOpersTimeConfs.indexOf(temp);
                curOpersTimeConfs.removeAt(index);
                curOpersTimeConfs.insert(index, timeConf);
            }
            else {
                curOpersTimeConfs.insert(0, timeConf);
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addTimeConfToListStore of BaseUIIntegratedConfPage_GeneralTimeConfController', ex);
        }
    }

});