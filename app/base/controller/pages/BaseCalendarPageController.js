Ext.define('AssetManagement.base.controller.pages.BaseCalendarPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.manager.CalendarManager',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
		contextReadyLevel: 1
	},

	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				this.saveCalendarChanges(); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseCalendarPageController', ex)
		}
	},
		
	//private
	onCreateOptionMenu: function() {
		try {
		    this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		    
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseCalendarPageController', ex)
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
		    this.loadListStore();
		
		    var cust001Request = AssetManagement.customer.manager.Cust_001Manager.getCust_001(AssetManagement.customer.core.Core.getAppConfig().getUserId().toUpperCase(), true);
		    this.addRequestToDataBaseQueue(cust001Request, "cust001");
		    
		    var stsmaReq = AssetManagement.customer.manager.StatusManager.loadAllStatusIntoCache(true, 'OV');
		    this.addRequestToDataBaseQueue(stsmaReq, "stsma");
		
		    this.waitForDataBaseQueue();		
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseCalendarPageController', ex)
		} 
	},
		
	loadListStore: function() {
		try {
			var calendarItemsStoreRequest = AssetManagement.customer.manager.CalendarManager.getOrdersForCalendar();
			this.addRequestToDataBaseQueue(calendarItemsStoreRequest, 'orderStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseCalendarPageController', ex)
		}
	},
	
	//list handler
	onOrderSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: record.get('aufnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderSelected of BaseCalendarPageController', ex)
		}
    },

	/**
	 * checks if there are modified events (IsModified==true) in the calendar and returns true if any have been found
	 */
	hasUnsavedChanges: function() {
		try {
			if(this.getViewModel().plannedEventStore != null){
				for(var i=0;i<this.plannedEventStore.data.length;i++){
					if(this.plannedEventStore.data.items[i].data.IsModified == true)
						return true;
				}			
			}
			if(this.getViewModel().unplannedEventStore != null){
				for(var i=0;i<this.getViewModel().unplannedEventStore.data.length;i++){
					if(this.getViewModel().unplannedEventStore.data.items[i].data.IsModified == true)
						return true;
				}			
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasUnsavedChanges of BaseCalendarPageController', ex)
		}
	    return false;
	},

	setPlannedEventStore: function(store){
	    try {
		    this.plannedEventStore = store;			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setPlannedEventStore of BaseCalendarPageController', ex)
		}
	},

	setUnplannedEventStore: function(store){
	    try {
		    this.unplannedEventStore = store;		
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setUnplannedEventStore of BaseCalendarPageController', ex)
		}
	},

	/**
	 * saves changes made to calendar objects to the database and resets the IsModified-flag
	 */
	saveCalendarChanges: function() {
		try{
			if(this.getViewModel().plannedEventStore != null){
				for(var i=0;i<this.getViewModel().plannedEventStore.data.length;i++){
					if(this.getViewModel().plannedEventStore.data.items[i].data.IsModified)
						this.saveChangedObject(this.getViewModel().plannedEventStore.data.items[i]);
				}
			}
			if(this.getViewModel().unplannedEventStore != null){
				for(var i=0;i<this.getViewModel().unplannedEventStore.data.length;i++){
					if(this.getViewModel().unplannedEventStore.data.items[i].data.IsModified)
						this.saveChangedObject(this.getViewModel().unplannedEventStore.data.items[i]);
				}
			}
			var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), false, 1);
			AssetManagement.customer.core.Core.getMainView().showNotification(notification);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveCalendarChanges of BaseCalendarPageController', ex)
		}	
	},
	
	/**
	 * saves a single calender item to the database
	 * this function creates an operation-object from the calender event and then saves it to the database
	 */
	saveChangedObject: function(dataItem) {
		try{
			//create a new operation item from the event
			var operation = dataItem.data.Operation;
			operation.set('ntan', dataItem.data.StartDate);
			operation.set('nten', dataItem.data.EndDate);
			
			if(operation != null)
				AssetManagement.customer.manager.OperationManager.saveOperation(operation);
			dataItem.data.IsModified = false;			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveChangedObject of BaseCalendarPageController', ex)
		}	
	},
	 
	loadStatusList: function(rec) {
		try {
	        var stsmaReq = AssetManagement.customer.manager.StatusManager.getStatusListForSchema(rec.data.Operation.get('stsma'), true);
			this.addRequestToDataBaseQueue(stsmaReq, "stsma");
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadStatusList of BaseCalendarPageController', ex)
		}		
	}
});