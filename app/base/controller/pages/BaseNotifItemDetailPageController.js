Ext.define('AssetManagement.base.controller.pages.BaseNotifItemDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.NotifManager'
	],

	config: {
		contextReadyLevel: 1,
		rowMenu: null
	},

    //@override
    onCreateOptionMenu: function() {
        this._optionMenuItems = new Array();

        try {
            var ltxtNotifItem = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ltxt_notif_item');

            if (ltxtNotifItem === 'X') {
                var itemsBLT = this.getViewModel().get('item').get('backendLongtext');
                var itemsLLT = this.getViewModel().get('item').get('localLongtext');

                if(itemsBLT || itemsLLT)
                    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST);
                else
                    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT);
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of NotifItemDetailPageController', ex);
        }
    },

    //@override
    onOptionMenuItemSelected: function(optionID) {
        try {
            if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT || optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST) {
                this.showLongtextDialog();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of NotifItemDetailPageController', ex);
        }
    },

    onContextMenuItemSelected: function(itemID, record) {
        try {
            var className = Ext.getClassName(record);

            if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_LONGTEXT) {
                var customizingParamName = '';

                if (className.indexOf('NotifCause') > -1) {
                    customizingParamName = 'c_ltxt_notif_item_cause';
                } else if (className.indexOf('NotifTask') > -1) {
                    customizingParamName = 'c_ltxt_notif_item_task';
                } else if (className.indexOf('NotifActivity') > -1) {
                    customizingParamName = 'c_ltxt_notif_item_activity';
                }

                this.showLongtextDialogSubObject(record, customizingParamName);
            } else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
                if (className.indexOf('NotifCause') > -1) {
                    this.editCause(record);
                } else if (className.indexOf('NotifTask') > -1) {
                    this.editTask(record);
                } else if (className.indexOf('NotifActivity') > -1) {
                    this.editActivity(record);
                }
            } else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
                if (className.indexOf('NotifCause') > -1) {
                    this.deleteCause(record);
                } else if (className.indexOf('NotifTask') > -1) {
                    this.deleteTask(record);
                } else if (className.indexOf('NotifActivity') > -1) {
                    this.deleteActivity(record);
                }
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of NotifItemDetailPageController', ex);
        }
    },

    startBuildingPageContext: function(argumentsObject) {
        try {
            var myModel = this.getViewModel();
            var qmnum = '';
            if (Ext.getClassName(argumentsObject['item']) === 'AssetManagement.customer.model.bo.NotifItem') {
                myModel.set('item', argumentsObject['item']);
            }else if(typeof argumentsObject['qmnum'] === 'string') {
                qmnum = argumentsObject['qmnum'];
            } else if(Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
                myModel.set('notif', argumentsObject['notif']);
            } else {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.pages.NotifItemDetailPageController'
                });
            }

            var qmnumPassed = qmnum !== '';

            if(qmnumPassed) {
                this.getNotif(qmnum);
            } else {
                this.waitForDataBaseQueue();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of NotifItemDetailPageController', ex);
            this.errorOccurred();
        }
    },

    getNotif: function(qmnum) {
        try {
            var notifRequest = AssetManagement.customer.manager.NotifManager.getNotif(qmnum, true);
            this.addRequestToDataBaseQueue(notifRequest, 'notif');

            this.waitForDataBaseQueue();
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotif of NotifItemDetailPageController', ex)
        }
    },

    addCauseClick: function() {
        try {
            var notifItem = this.getViewModel().get('item');
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFCAUSE_DIALOG, { notif: this.getViewModel().get('notif'), notifItem: notifItem});
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addCauseClick of NotifItemDetailPageController', ex);
        }
    },

    addActivityClick: function() {
        try {
            var notifItem =  this.getViewModel().get('item');
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFACTIVITY_DIALOG, { notif: this.getViewModel().get('notif'), notifItem: notifItem});
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addActivityClick of NotifItemDetailPageController', ex);
        }
    },

    addTaskClick: function() {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFTASK_DIALOG, { notif: this.getViewModel().get('notif'), notifItem: this.getViewModel().get('item') });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addTaskClick of NotifItemDetailPageController', ex);
        }
    },

    onNotifCauseSaved: function(itemObject) {
        try {
            this.addNotifCauseToListStore(itemObject);
            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifCauseSaved of NotifItemDetailPageController', ex);
        }
    },

    addNotifCauseToListStore: function(itemObject) {
        try {
            var notif = this.getViewModel().get('notif');
            var temp = this.getViewModel().get('item').get('notifItemCauses').getById(itemObject.get('id'));

            if(temp === undefined || temp === null)
                this.getViewModel().get('item').get('notifItemCauses').insert(0, itemObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNotifTaskToListStore of NotifItemDetailPageController', ex);
        }
    },

    onNotifActivitySaved: function(activityObject) {
        try {
            this.addNotifActivityToListStore(activityObject);
            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifActivitySaved of NotifItemDetailPageController', ex);
        }
    },

    addNotifActivityToListStore: function(activityObject) {
        try {
            var temp = this.getViewModel().get('item').get('notifItemActivities').getById(activityObject.get('id'));

            if(temp === undefined || temp === null)
                this.getViewModel().get('item').get('notifItemActivities').insert(0, activityObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNotifActivityToListStore of NotifItemDetailPageController', ex);
        }
    },

    onNotifTaskSaved: function(taskObject) {
        try {
            this.addNotifTaskToListStore(taskObject);
            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTaskSaved of NotifItemDetailPageController', ex);
        }
    },

    addNotifTaskToListStore: function(taskObject) {
        try {
            var temp = this.getViewModel().get('item').get('notifItemTasks').getById(taskObject.get('id'));

            if(temp === undefined || temp === null)
                this.getViewModel().get('item').get('notifItemTasks').insert(0, taskObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNotifTaskToListStore of NotifItemDetailPageController', ex);
        }
    },

    //long tap to open context menu
    onCellContextMenuSubOjects: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();

            if(!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [];

            var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();

            var ltxtNotifTask = funcPara.getValue1For('ltxt_notif_item_task'),
                ltxtNotifActivity = funcPara.getValue1For('ltxt_notif_item_activity'),
                ltxtNotifCause = funcPara.getValue1For('ltxt_notif_item_cause');

            var className = Ext.getClassName(record);

            if ((className === 'AssetManagement.customer.model.bo.NotifTask' && ltxtNotifTask === 'X')
                || (className === 'AssetManagement.customer.model.bo.NotifActivity' && ltxtNotifActivity === 'X')
                || ((className === 'AssetManagement.customer.model.bo.NotifCause' && ltxtNotifCause === 'X')))
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_LONGTEXT);

            //for a local created record delete and edit must be available always
            if (record.get('updFlag') === 'I' || record.get('updFlag') === 'A') {
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
            } else if ((className === 'AssetManagement.customer.model.bo.NotifCause' && funcPara.getValue1For('head_cause_change') === 'X')
                || (className === 'AssetManagement.customer.model.bo.NotifActivity' && funcPara.getValue1For('head_activity_change') === 'X')
                || ((className === 'AssetManagement.customer.model.bo.NotifTask' && funcPara.getValue1For('head_task_change') === 'X'))) {
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
            }

            this.rowMenu.onCreateContextMenu(options, record);

            if(options.length > 0) {
                this.rowMenu.setDisabled(false);
                this.rowMenu.showAt(e.getXY());
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuSubOjects of NotifItemDetailPageController', ex);
        }
    },

    editCause: function (cause) {
        try {
            var notifItem = this.getViewModel().get('item');
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFCAUSE_DIALOG, { notif: this.getViewModel().get('notif'), notifItem: notifItem, notifCause: cause });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editItem of NotifDetailPageController', ex);
        }
    },

    editTask: function (task) {
        try {
            var notifItem = this.getViewModel().get('item');
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFTASK_DIALOG, { notif: this.getViewModel().get('notif'), notifItem: notifItem, notifTask: task });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editTask of NotifDetailPageController', ex);
        }
    },

    editActivity: function (activity) {
        try {
            var notifItem = this.getViewModel().get('item');
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFACTIVITY_DIALOG, { notif: this.getViewModel().get('notif'),notifItem: notifItem, notifActivity: activity });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editActivity of NotifDetailPageController', ex);
        }
    },

    showLongtextDialogSubObject: function (busObject, customizingParamName) {
        try {
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customizingParamName)) {
                var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For(customizingParamName) === '';
                AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { bo: busObject, readOnly: readOnly });
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('unknownDialogRequested'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialogSubObject of NotifItemDetailPageController', ex);
        }
    },

    showLongtextDialog: function() {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { bo: this.getViewModel().get('item') });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialog of NotifItemDetailPageController', ex);
        }
    },

    onLongtextSaved: function(affectedObject, newLongtext) {
        try {
            if(affectedObject && affectedObject === this.getViewModel().get('item')) {
                this.rebuildOptionsMenu();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLongtextSaved of NotifItemDetailPageController', ex);
        }
    },

    deleteTask: function(task) {
        try {
            var callback = function(success) {
                try {
                    if(success === true) {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifTaskDeleted'), true, 0);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotifTask'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTask of NotifItemDetailPageController', ex);
                }
            };

            var deleteEvent = AssetManagement.customer.manager.NotifTaskManager.deleteNotifTask(task);

            if(deleteEvent > 0)
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTask of NotifItemDetailPageController', ex);
        }
    },

    deleteActivity: function(activity) {
        try {
            var callback = function(success) {
                try {
                    if(success === true) {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifActivityDeleted'), true, 0);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotifActivity'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteActivity of NotifItemDetailPageController', ex);
                }
            };

            var deleteEvent = AssetManagement.customer.manager.NotifActivityManager.deleteNotifActivity(activity);

            if(deleteEvent > 0)
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteActivity of NotifItemDetailPageController', ex);
        }
    },

    deleteCause: function(cause) {
        try {
            var callback = function(success) {
                try {
                    if(success === true) {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifCauseDeleted'), true, 0);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotifCause'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteCause of NotifItemDetailPageController', ex);
                }
            };

            var deleteEvent = AssetManagement.customer.manager.NotifCauseManager.deleteNotifCause(cause);

            if(deleteEvent > 0)
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteCause of NotifItemDetailPageController', ex);
        }
    }
});