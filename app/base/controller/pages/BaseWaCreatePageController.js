Ext.define('AssetManagement.base.controller.pages.BaseWaCreatePageController', {
  extend: 'AssetManagement.customer.controller.pages.OxPageController',
  alias: 'controller.BaseWaCreatePageController',

  requires: [
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.manager.UserManager',
    'AssetManagement.customer.manager.MiFieldManager',
    'AssetManagement.customer.controller.ToolbarController',
    'AssetManagement.customer.helper.LanguageHelper',
    'AssetManagement.customer.model.helper.ReturnMessage',
    'AssetManagement.customer.helper.OxLogger',
    'AssetManagement.customer.model.bo.GoodsMovementItem',
    'AssetManagement.customer.manager.MiProcessManager'
  ],

  config: {
    contextReadyLevel: 1
  },

  //private
  _requestRunning: false,

  //protected
  //@override
  startBuildingPageContext: function (argumentsObject) {
    try {

      var myModel = this.getViewModel();
      var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
      if (selectedLgort)
        myModel.set('selectedLgort', selectedLgort);

      myModel.set('miProcess', null);
      myModel.set('printKZ', null);
      this.loadMiProcess();
      this.loadMiFields();
      this.waitForDataBaseQueue();
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWaCreatePageController', ex);
    }
  },

  //protected
  //@override
  afterDataBaseQueueComplete: function () {
    try {
      if (this._contextLevel === 1) {
        this.checkMiProcessStore();

      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseWaCreatePageController', ex);
    }
  },

  checkMiProcessStore: function () {
    try {
      var myModel = this.getViewModel();
      var processStore = myModel.get('processStore');

      processStore.each(function (miProcess) {
        if (!AssetManagement.customer.utils.StringUtils.contains(miProcess.get('mi_process'), 'MM_GI', true)) {
          processStore.remove(miProcess);
        }
      });
      myModel.set('processStore', processStore);
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkMiProcessStore of BaseWaCreatePageController', ex);
    }
  },

  refreshData: function (triggerPageRequestCallbackWhenDone) {
    try {
      if (!triggerPageRequestCallbackWhenDone) {
        this._pageRequestCallback = null;
        this._pageRequestCallbackScope = null;
      }

      // this.getViewModel().resetData(); //Commented to have a persistent store when navigating back to page

      //set to zero
      this._contextLevel = 0;

      if (this._contextReadyLevel === 0) {
        this.contextReady();
      } else {
        //start the loading cycle
        this.startBuildingPageContext(this.getCurrentArguments());
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
    }
  },

  onProcessDropdownSelect: function (dropdown, record, eOpts) {
    try {
      var myModel = this.getViewModel();
      myModel.set('miProcess', record);
      this.refreshView();
      this.getView().validateInputs();
      var availableItemsArray = myModel.get('availableItemsArray');
      Ext.getCmp(availableItemsArray[0]).focus(false, 10);

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onProcessDropdownSelect of BaseWaCreatePageController', ex);
    }
  },

  loadMiProcess: function () {
    try {
      var processStore = AssetManagement.customer.manager.MiProcessManager.getAllMiProcess(true);
      this.addRequestToDataBaseQueue(processStore, "processStore");
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMiProcess of BaseWaCreatePageController', ex);
    }
  },

  loadMiFields: function () {
    try {
      var relevantFields = AssetManagement.customer.manager.MiFieldManager.getAllMiField(true);
      this.addRequestToDataBaseQueue(relevantFields, "relevantFields");
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMiProcess of BaseWaCreatePageController', ex);
    }
  },

  onDynfieldKeypress: function (field, e, eOpts) {
    try {
      var myModel = this.getViewModel();
      var availableItemsArray = myModel.get('availableItemsArray');
      var fieldId = field.getId();
      if (e.keyCode === 13) {
        var nextItemId = null;
        for (var i = 0; i < availableItemsArray.length; i++) {
          if (fieldId === availableItemsArray[i]) {
            if ((i + 1) < availableItemsArray.length) {
              nextItemId = availableItemsArray[i + 1]
            }
            else {//last item -> execute search
              this.onSaveClick();
            }
          }
        }
        if (nextItemId) {
          Ext.getCmp(nextItemId).focus();
        }
      }
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
    }
  },

  onSaveClick: function () {
    try {
      this.getView().transferViewStateIntoModel();
      var myModel = this.getViewModel();
      var checkValue = myModel.get('checkValue');
      if (!checkValue) {
        return;
      }
      var miProcess = myModel.get('miProcess');
      if (!miProcess) {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noProcessSelected'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
        return;
      }
      var title = myModel.get('title');
      var goodsMovementDefaults = myModel.get('goodsMovementDefaults');
      var printKZ = myModel.get('printKZ');
      var refDoc = myModel.get('refDoc');
      var selectedLgort = myModel.get('selectedLgort');
      var busobject = miProcess.get('busobject');
      var onlineCheckBoDialogCallback = function (success, returnItems) {
        try {
          if (success) {
            // if(!goodsMovementDefaults.get('lgort'))
            goodsMovementDefaults.set('lgort', selectedLgort.get('lgort'));
            goodsMovementDefaults.set('werks', selectedLgort.get('werks'));
            goodsMovementDefaults.set('bwart', miProcess.get('bwart'));
            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WA_CREATE_MATERIAL, {
              goodsMovementDefaults: goodsMovementDefaults,
              goodsMovementItems: returnItems,
              miProcess: miProcess,
              printKZ: printKZ,
              title: title,
              refDoc: refDoc
            });
          } else {
            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(returnItems, false, 2);
            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
          }
        } catch (ex) {
          AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of PoScanPageController', ex);
        }
      };

      AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_BO_CHECK_DIALOG, {
        busobject: busobject,
        busobjectKey: checkValue,
        readObject: true,
        successCallback: onlineCheckBoDialogCallback
      });

    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaveClick of BaseWaCreatePageController', ex);
    }
  }
});