Ext.define('AssetManagement.base.controller.pages.BaseInventoryDetailPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
       'AssetManagement.customer.controller.ToolbarController',
       'AssetManagement.customer.manager.InventoryItemsManager',
       'AssetManagement.customer.manager.InventoryManager',
       'AssetManagement.customer.helper.OxLogger',
       'AssetManagement.customer.utils.StringUtils',
       'AssetManagement.customer.controller.dialogs.SerialNoEditDialogController',
       'AssetManagement.customer.model.helper.ReturnMessage'
    ],


    config: {
        contextReadyLevel: 1
    },

    _hasUnsavedChanges : null,
    //proteced
    //@override
    onCreateOptionMenu: function () {
        try {
            this._optionMenuItems = [AssetManagement.customer.controller.ToolbarController.OPTION_SAVE];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseInventoryDetailPageController', ex)
        }
    },

    //public
    //@override
    onOptionMenuItemSelected: function (optionID) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
                if (this._saveRunning === true)
                    return;

                this.trySaveItem();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseInventoryDetailPageController', ex)
        }
    },

    //public
    //@override
    hasUnsavedChanges: function() {
        var retval = false;

        try {
            retval = this._hasUnsavedChanges;
            if (this._saveRunning)
                return false;
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasUnsavedChanges of BaseInventoryDetailPageController', ex);
        }

        return retval;
    },


    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            //reset current state of inventory
            //this.getView().resetViewState();
            this._hasUnsavedChanges = false;
            var me = this;

            var inventory = '';

            if (argumentsObject['inventory'] && typeof argumentsObject['inventory'] === 'string') {
                inventory = argumentsObject['inventory'];
            } else if (argumentsObject['inventory'] && Ext.getClassName(argumentsObject['inventory']) === 'AssetManagement.customer.model.bo.InventoryHead') {
                this.getViewModel().set('inventory', argumentsObject['inventory']);

                var myNewStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.InventoryItem',
                    autoLoad: false
                });

                var items = argumentsObject['inventory'].get('items');
                if (items && items.getCount() > 0) {
                    items.each(function (item) {
                        myNewStore.add(item.copy());
                    });
                }
                this.getViewModel().set('listItems', myNewStore);
            } else {
                throw Ext.create('AssetManagment.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.pages.InventoryDetailPageController'
                });
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseInventoryDetailPageController', ex)

        }
    },

    makeId: function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < 18; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();
            if (!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [];

            this.rowMenu.onCreateContextMenu(options, record);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseInventoryDetailPageController', ex);
        }
    },

    applyCurrentFilterCriteriasToStore: function () {
        try {
            var inventoryItems = this.getView();

            inventoryItems.getSearchValue();

            var itemToSearch = this.getViewModel().get('searchItem');

            if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemToSearch.get('item')) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemToSearch.get('material'))
                && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemToSearch.get('entryQnt')) && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(itemToSearch.get('entryQnt')) ) {
                this._hasUnsavedChanges = true;
            }


            var itemStore = this.getViewModel().get('listItems');

            if (itemStore) {
                itemStore.clearFilter();

                if (itemToSearch.get('item')) {
                    itemStore.filterBy(function (record) {
                        try {
                            if (record.get('item').toUpperCase().indexOf(itemToSearch.get('item').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryDetailPageController', ex);
                        }
                    });
                } else if (itemToSearch.get('material')) {
                    itemStore.filterBy(function (record) {
                        try {
                            if (record.get('material').toUpperCase().indexOf(itemToSearch.get('material').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryDetailPageController', ex);
                        }
                    });
                } else if (itemToSearch.get('entryQnt')) {
                    itemStore.filterBy(function (record) {
                        try {
                            if (record.get('entryQnt').toUpperCase().indexOf(itemToSearch.get('entryQnt').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryDetailPageController', ex);
                        }
                    });
                } else if (itemToSearch.get('entryUom')) {
                    itemStore.filterBy(function (record) {
                        try {
                            if (record.get('entryUom').toUpperCase().indexOf(itemToSearch.get('entryUom').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryDetailPageController', ex);
                        }
                    });
                } else if (itemToSearch.get('zerocount')) {
                    itemStore.filterBy(function (record) {
                        try {
                            if (record.get('zerocount').toUpperCase().indexOf(itemToSearch.get('zerocount').toUpperCase()) > -1) {
                                return true;
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryDetailPageController', ex);
                        }
                    });
                }
            }
        } catch (ex) {
            AssetManagemen.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseInventoryDetailPageController', ex);
        }
    },

    trySaveItem: function () {
        try {
            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState();

            var me = this;

            var saveItem = function (checkWasSuccessful) {
                try {
                    if (checkWasSuccessful === true) {
                        var saveCallback = function (success) {
                            try {
                                if (success === true) {
                                    AssetManagement.customer.core.Core.navigateBack();

                                    Ext.defer(function () {
                                        me._saveRunning = false;
                                    }, 2000, me);

                                    me._hasUnsavedChanges = false;

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifItem'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseInventoryDetailPageController', ex);
                            }
                        };

                        var inventoryItems = me.getViewModel().get('listItems');

                        //check if the inventory is completed
                        var isAllCompleted = this.checkItemsCompleted(inventoryItems);

                        var eventId = AssetManagement.customer.manager.InventoryItemsManager.saveInventoryItems(inventoryItems, isAllCompleted);

                        if (eventId > 0) {
                            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
                        } else {
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingNotifItem'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                        //}

                    } else {
                        me._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseInventoryDetailPageController', ex);
                    me._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            };

            //check if data is complete first
            this.checkInputValues(saveItem, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveItem of BaseInventoryDetailPageController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }

    },

    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';
            var values = this.getView().getCurrentInputValues();

            if(values && values.getCount() > 0){
                values.each(function(value){
                    var entryQnt = value.get('entryQnt');
                    var zerocount = value.get('zerocount');
                    if (entryQnt > 0 && zerocount === true) {
                        errorMessage = Locale.getMsg('errorQuantZero');
                        retval = false;
                    }
                });

            }

            // show notification about current action on the buttom
            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }
                callback.call(callbackScope ? callbackScope : me, retval);
            };
            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseInventoryDetailPageController', ex);

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    onInventoryDetailPageSearchFieldChange: function () {
        try {
            this.applyCurrentFilterCriteriasToStore();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onInventoryDetailPageSearchFieldChange of BaseInventoryDetailPageController', ex)
        }
    },

    checkItemsCompleted: function (inventoryItems) {
        var retval = false;
        try {
            var invItemCount = inventoryItems.getCount();
            var counter = 0;
            if (inventoryItems && invItemCount > 0) {
                inventoryItems.each(function (invItem) {
                    var entryQnt = invItem.get('entryQnt');
                    var zerocount = invItem.get('zerocount');

                    if (zerocount === true || entryQnt > 0 ) {
                        counter++;
                    }
                });
            }

            if (invItemCount === counter) {
                retval = true;
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkItemsCompleted of BaseInventoryDetailPageController', ex);
        }
        return retval;
    },

    onSelectInventoryItem: function (inventoryStatus, metaData, rowIndex, celIndex) {
        try {
            var record = inventoryStatus.getStore().getAt(rowIndex);
            this.getViewModel().set('lastSelectedItem', record);

            if (record.get('zerocount') === false) {
                //navigate to serialno-edit dialog only if the item needs serial numbers and the quantity is already added for this item
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('serpr'))) {
                    var serialListStore = Ext.create('Ext.data.Store', {
                        storeId: 'serialListStore',
                        fields: ['serialNo']
                    });

                    var serialList = record.get('seriallist');

                    if (serialList && serialList.getCount() > 0) {
                        serialList.each(function (serial) {
                            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(serial.get('serialNo'))) {
                                var toAdd = Ext.create('Ext.data.Model', {
                                    serialNo: serial.get('serialNo')
                                });
                                serialListStore.add(toAdd);
                            }
                        })
                    } else {
                        serialListStore = null;
                    }
                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SERIALNO_EDIT_DIALOG, { serialNumberList: serialListStore, totalSerialNoAllowed: null });
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkItemsCompleted of BaseInventoryDetailPageController', ex);
        }
    },

    onSerialNoSaved: function (serialNumbersInfoObject) {
        try {
            //save the new added serialnumber list for the current item
            var serialNoList = serialNumbersInfoObject.getView().getSerialNumberList();

            var selectedItem = this.getViewModel().get('lastSelectedItem');
            selectedItem.set('seriallist', serialNoList);
            if (serialNoList && serialNoList.getCount() > 0) {
                this._hasUnsavedChanges = true;
                selectedItem.set('entryQnt', serialNoList.getCount().toString());
            }

            this.getView().refreshView();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSerialNoSaved of BaseInventoryDetailPageController', ex)
        }
    },

    onSerialNoDelete: function (serialNumbersInfoObject) {
        try {
            //remove the serialnumber from serialnumber list for the current item
            var serialNoList = serialNumbersInfoObject.getView().getSerialNumberList();

            var currentItem = this.getViewModel().get('lastSelectedItem');
            currentItem.set('seriallist', serialNoList);
            if (serialNoList && serialNoList.getCount() > 0) {
                currentItem.set('entryQnt', serialNoList.getCount().toString());
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSerialNoDelete of BaseInventoryDetailPageController', ex);
        }
    },

    onZeroCountFieldChange: function ( field, rowIndex, checked, record, e, eOpts) {
        try {
			this._hasUnsavedChanges = true;
            record.set('zerocount', checked);
            if (checked) {
                record.set('entryQnt', 0);
            }
            field.up().view.refresh();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onQuantityValueChanged of BaseInventoryDetailPageController', ex);
        }
    },

    onTextFieldValueChange: function( field, e, eOpts ) {
        try {
            var record =  field.up().getWidgetRecord();
        	var oldValue = record.get('entryQnt');
        	var newValue = ''+field.value;
        	var isZeroCount = record.get('zerocount');

            if ((newValue !== oldValue)) {
                this._hasUnsavedChanges = true;
                if (newValue) {
                    record.set('entryQnt', newValue);
                    record.set('zerocount', false);
                } else {
                    record.set('entryQnt', 0);
                }
            }
            if (newValue === '0') {
            	if (!isZeroCount) {
            		this._hasUnsavedChanges = true;
            		record.set('zerocount', true);
            	}
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onQuantityValueChanged of BaseInventoryDetailPageController', ex);
        }

    }

});
