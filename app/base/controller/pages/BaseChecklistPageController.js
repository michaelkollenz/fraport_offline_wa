﻿Ext.define('AssetManagement.base.controller.pages.BaseChecklistPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
	    'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.model.bo.QPlos',
		'AssetManagement.customer.view.ContextMenu',
		'AssetManagement.customer.model.helper.ReturnMessage',
        'AssetManagement.customer.model.bo.QPResult',
        'AssetManagement.customer.model.bo.ChecklistViewChar',
        'AssetManagement.customer.controller.ToolbarController',
        'Ext.data.Store',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.ObjectListItem',
        'AssetManagement.customer.controller.EventController',
        'AssetManagement.customer.manager.QPlosManager',
        'AssetManagement.customer.manager.QPlanManager',
        'AssetManagement.customer.manager.QPResultManager'
    ],

    config: {
        contextReadyLevel: 1
    },

    //protected
    _saveRunning: false,
    _deleteRunning: false,

    //protected
    //@override
    onCreateOptionMenu: function () {
        try {
            this._optionMenuItems = new Array();

            if (!(this.getViewModel().get('readOnly'))) { //if the qplos is completed, we don't allow saving nor cleaing the checklist
                this._optionMenuItems.push([AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM]);
                this._optionMenuItems.push([AssetManagement.customer.controller.ToolbarController.OPTION_SAVE]);
            } else {
                this._optionMenuItems.push([AssetManagement.customer.controller.ToolbarController.OPTION_REPORT]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseChecklistPageController', ex);
        }
    },

    //protected
    //@override
    onOptionMenuItemSelected: function (optionId) {
        try {
            if (this._saveRunning === true)
                return;

            if (optionId === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
                this.onSaveButtonClicked();
            } else if (optionId === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
                this.onClearButtonClicked();
            } else if (optionId === AssetManagement.customer.controller.ToolbarController.OPTION_REPORT) {
                this.printChecklistReport();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseChecklistPageController', ex)
        }
    },

    //protected
    onSaveButtonClicked: function () {
        try {
            if (this._saveRunning || this._deleteRunning)
                return;
            this.handleSaveRequest(false); // checklist is not completed, just saved, so we pass false
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaveButtonClicked of BaseChecklistPageController', ex)
        }
    },

    //protected
    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            var prueflos = argumentsObject['prueflos'];
            var matnr = argumentsObject['matnr'];

            if (matnr) {
                this.getDataForQPlanCase(argumentsObject);
            } else if (prueflos) {
                this.getQPlosData(argumentsObject);
            } else {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.pages.ComponentPageController'
                });
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseChecklistPageController', ex);
            this.errorOccurred();
        }
    },

    //protected
    getQPlosData: function (argumentsObject) {
        try {
            var prueflos = argumentsObject['prueflos'];
            var aufnr = argumentsObject['aufnr'];
            //if a qplos was already created based on above loaded qplan, it has to be loaded
            if (prueflos) {
                //if pruflos was provided in the signature it can be used for loading it
                var qplosRequest = AssetManagement.customer.manager.QPlosManager.getQPlos(prueflos, true);
                this.addRequestToDataBaseQueue(qplosRequest, 'qplos');
            } else {
                //if the 'back' navigation is used,
                //it might happen that qplos was already created, but the 'back' navigation signature doesn't have a prueflos number provided.
                //then a method to load this qplos based on order number and equipment and funcLoc number.
                var equnr = argumentsObject['equnr'];
                var tplnr = argumentsObject['tplnr'];
                var qmnum = argumentsObject['qmnum'];

                var qplosRequest = AssetManagement.customer.manager.QPlosManager.getQPlosForOrderAndTechObj(aufnr, equnr, tplnr, qmnum, false)
                this.addRequestToDataBaseQueue(qplosRequest, 'qplos');
            }

            //load the parent BO of the checklist
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {
                var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
                this.addRequestToDataBaseQueue(orderRequest, 'parentBO');
            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getQPlosData of BaseChecklistPageController', ex);
        }
    },

    //protected
    getDataForQPlanCase: function (argumentsObject) {
        try {
            var matnr = argumentsObject['matnr'];
            var qmnum = argumentsObject['qmnum'];

            this.getViewModel().set('matnr', matnr);
            this.getViewModel().set('qmnum', qmnum);

            var qplanRequest = AssetManagement.customer.manager.QPlanManager.getNewestQPlanForMaterial(matnr, true);
            this.addRequestToDataBaseQueue(qplanRequest, 'qplan');

            var equnr = argumentsObject['equnr'];
            var tplnr = argumentsObject['tplnr'];

            if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equnr)) {
                var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipmentLightVersion(equnr, true);
                this.addRequestToDataBaseQueue(equiRequest, 'techObj');
            } else if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                var flocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tplnr, true);
                this.addRequestToDataBaseQueue(flocRequest, 'techObj');
            }

            //object list item
            var obknr = argumentsObject['obknr'];
            var obzae = argumentsObject['obzae'];
            var aufnr = argumentsObject['aufnr'];
            if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(obknr)
            && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(obzae)
            && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(aufnr)) {

                var objectListItemRequest = AssetManagement.customer.manager.ObjectListManager.getObjectListItem(aufnr, obknr, obzae, true);
                this.addRequestToDataBaseQueue(objectListItemRequest, 'objListItem');
            }

            this.getQPlosData(argumentsObject);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataForQPlanCase of BaseChecklistPageController', ex);
        }
    },

    //protected
    //@override
    beforeDataReady: function () {
        try {
            var myModel = this.getViewModel();
            var qplos = myModel.get('qplos');

            if (qplos) {
                myModel.set('readOnly', qplos.isCompleted());
                myModel.set('techObj', qplos.get('techObj'));
            }


            var qplosOnHeader = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_obj_list_active') === '';
            if (qplosOnHeader)
                myModel.set('entryObject', myModel.get('parentBO'));
            else
                myModel.set('entryObject', myModel.get('objListItem'));
            myModel.set('viewChars', this.generateViewChars());
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseChecklistPageController', ex);
        }
    },

    //protected
    generateViewChars: function () {
        var retval = null;

        try {
            var myModel = this.getViewModel();

            var viewCharStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.ChecklistViewChar',
                autoLoad: false,
                groupField: 'vornr'
            });

            var qplos = myModel.get('qplos');

            if (qplos) {
                var qplosOperStore = qplos.get('qplosOpers');
                if (qplosOperStore && qplosOperStore.getCount() > 0) {
                    qplosOperStore.each(function (qplosOper) {
                        var qplosCharStore = qplosOper.get('qplosChars');
                        this.assignCharProperties(qplosOper, qplosCharStore, viewCharStore);
                    }, this);
                }
            } else {
                var qplanOperStore = myModel.get('qplan').get('qplanOpers');
                if (qplanOperStore && qplanOperStore.getCount() > 0) {
                    qplanOperStore.each(function (qplanOper) {
                        var qplanCharStore = qplanOper.get('qplanChars');
                        this.assignCharProperties(qplanOper, qplanCharStore, viewCharStore)
                    }, this);
                }
            }

            retval = viewCharStore;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateViewChars of BaseChecklistPageController', ex);
        }

        return retval;
    },

    //protected
    assignCharProperties: function (qpOper, charStore, viewCharStore) {
        try {
            var myModel = this.getViewModel();
            var hasResults = false;
            if (charStore && charStore.getCount() > 0) {
                charStore.each(function (qpChar) {

                    var viewChar = this.createSignleViewChar(qpOper, qpChar);
                    if (qpChar.get('qpResult'))
                        hasResults = true;

                    viewCharStore.add(viewChar);
                }, this);
                myModel.set('checklistHasResults', hasResults);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside assignCharProperties of BaseChecklistPageController', ex);
        }
    },

    //protected
    //creates a signle viewChar entry
    createSignleViewChar: function (qpOper, qpChar) {
        var retval;
        var date = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(); //getter for current date, that will be used as a start date and time, if there are no results

        try {
            //viewChars are a dummy model, not assigned to a qplos Char, but created based on it, for the page runtime.
            //viewChars are used as display records in the grid. Also, any user input gets to viewChars first.
            //therefore, any changes in the concept of  the checklist must be included in viewChars
            var viewChar = Ext.create('AssetManagement.customer.model.bo.ChecklistViewChar', {
                kzeinstell: qpChar.get('kzeinstell'),
                steuerkz: qpChar.get('steuerkz'),
                kurztext: qpChar.get('kurztext'),
                katab1: qpChar.get('katab1'),
                katalgart1: qpChar.get('katalgart1'),
                auswmenge1: qpChar.get('auswmenge1'),
                auswmgwrk1: qpChar.get('auswmgwrk1'),
                katab2: qpChar.get('katab2'),
                katalgart2: qpChar.get('katalgart2'),
                auswmenge2: qpChar.get('auswmenge2'),
                auswmgwrk2: qpChar.get('auswmgwrk2'),
                sollwert: qpChar.get('sollwert'),
                toleranzob: qpChar.get('toleranzob'),
                toleranzun: qpChar.get('toleranzun'),
                masseinhsw: qpChar.get('masseinhsw'),
                stellen: qpChar.get('stellen'),
                char_type: qpChar.get('char_type'),
                vorglfnr: qpChar.get('vorglfnr'),
                merknr: qpChar.get('merknr'),
                codes: qpChar.get('codes'),
                plnty: qpChar.get('plnty'),
                plnnr: qpChar.get('plnnr'),
                plnnk: qpChar.get('plnnk'),
                codeMeanings: qpChar.get('codeMeanings'),
                category: qpChar.get('category'),

                qpOpertxt: qpOper.get('ltxa1'),
                vornr: qpOper.get('vornr')
            });

            var childKey = qpChar.get('childKey');

            if (childKey) {
                viewChar.set('childKey', childKey)
            }

            var qpresult = qpChar.get('qpResult');

            if (qpresult) {
                this.transferResultToViewChar(viewChar, qpresult)
            } else {
                viewChar.set('startDate', date);
                viewChar.set('startTime', date);

                viewChar.set('checkBoxOK', false)
                viewChar.set('checkBoxNOK', false);
                viewChar.set('checkBoxNREL', false);
            }
            retval = viewChar;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createSignleViewChar of BaseChecklistPageController', ex);
        }
        return retval;
    },


    //protected
    transferResultToViewChar: function (viewChar, qpresult) {
        try {
            viewChar.set('meanValue', qpresult.get('meanValue'));
            viewChar.set('remark', qpresult.get('remark'));
            viewChar.set('char_attr', qpresult.get('char_attr'));
            viewChar.set('closed', qpresult.get('closed'));
            viewChar.set('updFlag', qpresult.get('updFlag'));
            viewChar.set('startDate', qpresult.get('startDate'));
            viewChar.set('startTime', qpresult.get('startTime'));
            viewChar.set('endDate', qpresult.get('endDate'));
            viewChar.set('endTime', qpresult.get('endTime'));

            //extra handling for checkboxes
            if (viewChar.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_CHECKBOX) {
                var codeMeanings = viewChar.get('codeMeanings');
                var funktion = '';

                if (codeMeanings && codeMeanings.getCount() > 1) {
                    codeMeanings.each(function (code) {
                        if (code.get('code') === qpresult.get('code1') &&
                            code.get('codegruppe') === qpresult.get('codegrp1')) {
                            funktion = code.get('funktion');
                        }
                    });
                }

                viewChar.set('checkBoxOK', funktion === 'OK')
                viewChar.set('checkBoxNOK', funktion === 'NOK');
                viewChar.set('checkBoxNREL', funktion === 'NA');
            } else if (viewChar.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUANTITATIVE) {
                viewChar.set('checkBoxOK', qpresult.get('evaluation') === 'A');
                viewChar.set('checkBoxNOK', qpresult.get('evaluation') === 'R');
                viewChar.set('checkBoxNREL', qpresult.get('evaluation') === 'N');
            } else {
                viewChar.set('auswmgwrk2', qpresult.get('code1'));
                viewChar.set('auswmenge2', qpresult.get('codegrp1'));
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferResultToViewChar of BaseChecklistPageController', ex);
        }
    },

    //private
    //method for transfering the user input from the view, to the qpResults
    extractResults: function (complete) { //extract results
        var retval = null;
        try {
            var myModel = this.getViewModel();
            var viewChars = myModel.get('viewChars');
            var qplos = myModel.get('qplos');
            var techObj = qplos.get('techObj');

            var qpResultStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.QPResult',
                autoLoad: false
            });


            if (viewChars && viewChars.getCount() > 0) {
                viewChars.each(function (viewChar) {
                    var qpresult = this.extractResultFromSingleViewChar(viewChar, complete);
                    qpResultStore.add(qpresult);
                }, this);
            }

            retval = qpResultStore;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractResults of BaseChecklistPageController', ex);
        }

        return retval;
    },

    //protected
    //extracts a single
    extractResultFromSingleViewChar: function (viewChar, complete) {
        var retval = null;

        try {

            var myModel = this.getViewModel();
            var qplos = myModel.get('qplos');
            var techObj = qplos.get('techObj');
            var date = AssetManagement.customer.utils.DateTimeUtils.getCurrentTime(); //getter for current date, that will be used as a start date and time, if there are no results
            var qpresult = Ext.create('AssetManagement.customer.model.bo.QPResult', {
                qmnum: '',
                zaehl: viewChar.get('zaehl'),
                inspchar: viewChar.get('merknr'),
                inspoper: viewChar.get('vornr'),
                insplot: qplos.get('prueflos'),
                meanValue: viewChar.get('meanValue'),
                remark: viewChar.get('remark'),
                code1: viewChar.get('auswmgwrk2'),
                codegrp1: viewChar.get('auswmenge2'),
                endTime: date,
                endDate: date,
                tplnr: techObj.get('tplnr'),
                equnr: techObj.get('equnr'),
                kurztext: viewChar.get('kurztext'),
                closed: viewChar.get('closed'),
                mobileKey: qplos.get('mobileKey'),
                childKey_char: viewChar.get('childKey'),
                plnty: viewChar.get('plnty'),
                plnnr: viewChar.get('plnnr'),
                plnnk: viewChar.get('plnnk')
            });

            if (complete)
                qpresult.set('updFlag', 'I');
            else
                qpresult.set('updFlag', 'A');

            //this is used if the quualitive characteristics use the checkboxes instead of the dropdown (customizing)
            //it's used to determine the code and codegruppe values based on the checkbox
            //***//
            if (viewChar.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_CHECKBOX) {
                var funktion = '';

                if (viewChar.get('checkBoxOK')) {
                    funktion = 'OK';
                } else if (viewChar.get('checkBoxNOK')) {
                    funktion = 'NOK';
                } else if (viewChar.get('checkBoxNREL')) {
                    funktion = 'NA';
                }

                var codeMeanings = viewChar.get('codeMeanings')

                if (funktion === '') {
                    qpresult.set('code1', '');
                    qpresult.set('codegrp1', '');
                } else {
                    if (codeMeanings && codeMeanings.getCount() > 1) {
                        codeMeanings.each(function (code) {
                            if (code.get('funktion') === funktion) {
                                qpresult.set('code1', code.get('code'));
                                qpresult.set('codegrp1', code.get('codegruppe'));
                            }
                        });
                    }
                }
            } else {
                //setting the evaluation based on checkboxes
                if (viewChar.get('checkBoxOK')) {
                    qpresult.set('evaluation', 'A');
                } else if (viewChar.get('checkBoxNOK')) {
                    qpresult.set('evaluation', 'R');
                } else if (viewChar.get('checkBoxNREL')) {
                    qpresult.set('evaluation', 'N');
                }
            }
            retval = qpresult;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractResults of ChecklistPageController', ex);
        }
        return retval;
    },

    //public
    onSelectChecklistRemark: function (checklistremark, metaData, rowIndex, celIndex) {
        try {
            var record = checklistremark.getStore().getAt(rowIndex);
            var qplos = this.getViewModel().get('qplos');
            var readOnly = qplos ? qplos.isCompleted() : false;
            var me = this;

            var callback = function (value) {
                try {
                    record.set('remark', value);
                    me.setHasUnsavedChanges(true);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectChecklistRemark of BaseChecklistPageController', ex);
                }
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SINGLE_TEXT_LINE_DIALOG, { value: record.get('remark'), readOnly: readOnly, maxLength: 40, title: 'Remark', callback: callback, callbackScope: this });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectChecklistRemark of BaseChecklistPageController', ex)
        }
    },

    //public
    //this method checks if the checkbox can be changed (based on the record char_type, the bounds etc)
    beforeCheckboxCheckChange: function (checkbox, rowIndex, checked, eOpts) {
        try {
            var myModel = this.getViewModel();
            var record = Ext.getCmp('inGridPanel').getStore().getAt(rowIndex);
            var qplos = myModel.get('qplos');

            var disabled = qplos ? qplos.isCompleted() : false;

            if (disabled || record.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_STANDARD || this._saveRunning || this._deleteRunning) {   //if the record is disabled, don't allow change
                return false;
            }

            myModel.get('viewChars').suspendEvents();
            //this method is only run, if the 'beforeCheckboxCheckChange' didn't return false
            //therefore, no further checks needed

            var dataIndex = checkbox.dataIndex;

            //all other checkboxes, except the one selected, should be cleared. going through all the checkboxes of the record
            //if dataIndex === checkboxId, it means checkbox with that Id was not the one selected, so it should be cleared by setting 'false'
            //otherwise, so if the checkbox with that ID is the one selected. no operation is needed.
            if (dataIndex !== 'checkBoxOK') record.set('checkBoxOK', false);
            if (dataIndex !== 'checkBoxNOK') record.set('checkBoxNOK', false);
            if (dataIndex !== 'checkBoxNREL') record.set('checkBoxNREL', false);

            myModel.get('viewChars').resumeEvents(true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeCheckboxCheckChange of BaseChecklistPageController', ex)
        }
    },

    //private
    checkboxCheckChange: function (checkboxColumn, rowIndex, checked, record, eOpts) {
        try {
            var value = record.get(dataIndex);
            var dataIndex = checkboxColumn.dataIndex;
            var value = record.get(dataIndex);
            //value = previous value, before change. therefore, it should be false before,  which means it will be changed to true
            if (value && (record.get('meanValue') || record.get('category') === AssetManagement.customer.model.bo.QPlosChar.CATEGORIES.QUALITATIVE_CHECKBOX))
                record.set('closed', 'X');
            else
                record.set('closed', '');
            this.setHasUnsavedChanges(true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside chechboxCheckChange of ChecklistPageController', ex);
        }
    },

    //public
    onMeanValueChanged: function (field, value, value2, eOpts) {
        try {
            var record = eOpts.record;
            var myModel = this.getViewModel();
            myModel.get('viewChars').suspendEvents();

            if (!field.isValid()) {
                record.set('meanValue', '');

                if (!((value == '-') || (field.getValue() >= field.maxValue) || (field.getValue() <= field.minValue)))
                    field.setValue('');
            } else {
                record.set('meanValue', value);

                if (record.get('meanValue') && (record.get('checkBoxOK') || record.get('checkBoxNOK') || record.get('checkBoxNREL')))
                    record.set('closed', 'X');
                else
                    record.set('closed', '');

                this.setHasUnsavedChanges(true);
            }

            myModel.get('viewChars').resumeEvents(false);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMeanValueChanged of BaseChecklistPageController', ex);
        }
    },

    //public
    //method run onn change selection of the combobox
    //it's needed to assign proper code, code group and catalog to the record, based on the selection
    onDropdownItemSelected: function (combo, record, eOpts) {
        try {
            var myModel = this.getViewModel();
            var dropdownEntry = record.get('entry');

            var selectedCode = dropdownEntry.get('code');
            var selectedCGrp = dropdownEntry.get('codegruppe');
            var selectedKatalog = dropdownEntry.get('katalogart');

            var charRecord = eOpts.entry;
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(selectedCode))
                return false;
            myModel.get('viewChars').suspendEvents(); //suppresing events started
            charRecord.set('auswmgwrk2', selectedCode);
            charRecord.set('auswmenge2', selectedCGrp);
            charRecord.set('katalgart2', selectedKatalog);
            charRecord.set('closed', 'X');
            myModel.get('viewChars').resumeEvents(false); //suppresing events finished

            this.setHasUnsavedChanges(true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDropdownItemSelected of BaseChecklistPageController', ex);
        }
    },


    //private
    //starts saving requests
    handleSaveRequest: function (complete, followUpDate) {
        try {

            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
            this._saveRunning = true;

            var myModel = this.getViewModel();
            var qplos = myModel.get('qplos');

            var me = this;

            if (me.checklistHasEntries()) {
                if (me.hasUnsavedChanges() || complete) {
                    me.qplosSavingAction.call(me, true, complete);
                } else {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistNoChanges'), true, 0);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    this._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                }
            } else {
                var deleteCallback = function (success) {
                    try {
                        if (success) {
                            me.setHasUnsavedChanges(false);
                            me.setQpResultsToQPlosChars(null);
                            myModel.set('readOnly', false);

                            var entryObject = myModel.get('entryObject');
                            if (entryObject)
                                entryObject.set('qplos', null);

                            me.clearViewChars();
                            me.refreshView();

                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistCleared'), true, 0);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        } else {
                            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistDeleteError'), false, 2);
                            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside handleSaveRequest-deleteCallback of ChecklistPageController', ex);
                    } finally {
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                };
                me.performDelete(deleteCallback); //if checklist has no entries, clicking save is the same as clearing it
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside handleSaveRequest of ChecklistPageController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    qplosSavingAction: function (success, complete) {
        try {
            var me = this;
            if (success) {
                var qplos = this.getViewModel().get('qplos');
                var qplosSavedCallback = function (success) {
                    if (success)
                        me.resultsSavingAction.call(me, complete)
                    else
                        me.finallSaveCallback.call(me, false);
                }
                if (!qplos) {
                    this.createQPlos(complete);
                } else if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(qplos.get('mobileKey')) && complete) {
                    var eventId = AssetManagement.customer.manager.QPlosManager.saveQPlos(qplos, complete);
                    if (eventId > -1) {
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, qplosSavedCallback);
                    } else {
                        this.finallSaveCallback.call(this, false);
                    }
                } else {
                    this._saveRunning = false;
                    qplosSavedCallback.call(this, true);
                }
            } else {
                this._saveRunning = false;
                this.finallSaveCallback.call(this, false);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside qplosSavingAction of ChecklistPageController', ex);
            this.finallSaveCallback.call(this, false);
        }
    },

    //private
    //creates a qplos based on the current qplan and on success stores this in the view model
    //complete controls, if the qplos has to be saved as completed
    //callback returns, if saving was successful
    //both transfer parameters are mandatory
    createQPlos: function (complete) {
        try {

            var myModel = this.getViewModel();
            var qplan = myModel.get('qplan');
            var techObj = myModel.get('techObj');
            var matnr = myModel.get('matnr');
            var qmnum = myModel.get('qmnum');
            var parentBO = myModel.get('parentBO');

            var me = this;

            var qplosSaveSuccessCallback = function (savedQPlos) {
                var success = savedQPlos && true;
                try {
                    if (success) {
                        myModel.set('qplos', savedQPlos);
                        me.resultsSavingAction.call(me, complete);
                    }
                    else {
                        me.finallSaveCallback.call(me, false);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createQPlos-successCallback of ChecklistPageController', ex);
                } finally {
                }
            };

            var eventId = AssetManagement.customer.manager.QPlosManager.saveNewQPlos(qplan, techObj, matnr, parentBO, qmnum, complete);

            if (eventId > -1) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, qplosSaveSuccessCallback);
            } else {
                this.finallSaveCallback.call(this, false);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createQPlos of ChecklistPageController', ex);
            this.finallSaveCallback.call(this, false);
        }
    },


    resultsSavingAction: function (complete) {
        try {
            var myModel = this.getViewModel();
            //extract the current input from the view chars into the real result objects
            var qpresults = this.extractResults(complete);
            var me = this;

            this.performSaveResults(qpresults, complete, this.finallSaveCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resultsSavingAction of ChecklistPageController', ex);
            this.finallSaveCallback.call(this, false);
        }
    },

    //private
    //extracts the current results from the view chars into real objects and saveds these into the database
    //qpresults is the data to save
    //complete controls, if the checklist has to be saved as completed
    //callback returns, if saving was successful
    //both transfer parameters are mandatory
    performSaveResults: function (qpresults, complete, callback) {
        if (!callback)
            return;

        try {
            this._saveRunning = true;
            var me = this;

            var returnFunction = function (success) {
                try {
                    me._saveRunning = false;
                    callback.call(me, success === true, qpresults, complete);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSaveResults-returnFunction of BaseChecklistPageController', ex);
                }
            };

            var eventId = AssetManagement.customer.manager.QPResultManager.saveQPResults(qpresults);

            if (eventId > -1) {
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, returnFunction);
            } else {
                returnFunction.call(this, false);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performSaveResults of BaseChecklistPageController', ex);
            this._saveRunning = false;
            callback.call(this, false);
        }
    },

    //private
    //this is a finall callback whuch happens after the successfull save, or if any error during saving happened.
    finallSaveCallback: function (success, qpresults, complete) {
        try {
            var myModel = this.getViewModel();

            if (success === true) {
                this.setHasUnsavedChanges(false);
                this.setQpResultsToQPlosChars(qpresults);


                if (complete) {
                    myModel.set('readOnly', true);
                    this.refreshView();
                }
                var entryObject = myModel.get('entryObject');
                if (entryObject)
                    entryObject.set('qplos', myModel.get('qplos'));
                var myView = this.getView();
                myView.fillMainDataSection();
                myView.resetViewState();

                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistSaved'), true, 0);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistSaveError'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside finallSaveCallback of ChecklistPageController', ex);
        } finally {
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            this._saveRunning = false;
        }
    },

    //private
    setQpResultsToQPlosChars: function (qpresults) {
        try {
            var qplos = this.getViewModel().get('qplos');
            var qplosOperStore = qplos ? qplos.get('qplosOpers') : null;

            if (qplosOperStore && qplosOperStore.getCount() > 0) {
                qplosOperStore.each(function (qplosOper) {
                    var qplosCharStore = qplosOper.get('qplosChars');

                    if (qplosCharStore && qplosCharStore.getCount() > 0)
                        qplosCharStore.each(function (qplosChar) {
                            if (qpresults && qpresults.getCount() > 0) {
                                qpresults.each(function (qpResult) {
                                    if ((qpResult.get('inspchar') === qplosChar.get('merknr')) && (qpResult.get('inspoper') === qplosChar.get('vornr'))) {
                                        qplosChar.set('qpResult', qpResult);
                                        return false;
                                    }
                                });
                            }
                            else qplosChar.set('qpResult', null);
                        });
                });
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setQpResultsToQPlosChars of BaseChecklistPageController', ex);
        }
    },



    //private
    //deleting qpResults entries in the DB
    onClearButtonClicked: function () {
        try {
            if (this._saveRunning || this._deleteRunning)
                return;

            this.handleDeleteRequest();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearButtonClicked of BaseChecklistPageController', ex);
        }
    },

    //private
    //handles a delete request from the delete button
    handleDeleteRequest: function () {
        try {
            var me = this;
            var myModel = this.getViewModel();
            var confirmCallback = function (confirmed) {
                try {
                    if (confirmed) {
                        AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

                        var deleteCallback = function (success) {
                            try {
                                if (success) {
                                    me.setHasUnsavedChanges(false);
                                    me.setQpResultsToQPlosChars(null);
                                    myModel.set('readOnly', false);

                                    var entryObject = myModel.get('entryObject');
                                    if (entryObject)
                                        entryObject.set('qplos', null);

                                    me.clearViewChars();
                                    me.refreshView();

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistCleared'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistDeleteError'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside handleDeleteRequest-deleteCallback of BaseChecklistPageController', ex);
                            } finally {
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        };
                        me.performDelete(deleteCallback); //if checklist has no entries, clicking save is the same as clearing it
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside handleDeleteRequest of BaseChecklistPageController', ex);
                }
            };

            var dialogArgs = {
                icon: 'resources/icons/question.png',
                message: Locale.getMsg('checklistClear'),
                callback: confirmCallback,
                scope: this
            };
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside handleDeleteRequest of BaseChecklistPageController', ex);
        }
    },

    //private
    //returns success through returnFunction calback
    performDelete: function (callback) {
        if (!callback)
            return;

        try {
            this._deleteRunning = true;
            var me = this;
            var returnFunction = this.deleteSuccessCallback;

            var qplos = this.getViewModel().get('qplos');
            var prueflos = qplos ? qplos.get('prueflos') : '';
            var mobileKey = qplos ? qplos.get('mobileKey') : '';


            var eventController = AssetManagement.customer.controller.EventController.getInstance();


            var deleteResultsCallback = function (success) {
                try {
                    if (success === true) {
                        var deleteQPlosCallback = function (success) {
                            try {
                                if (success)
                                me.getViewModel().set('qplos', null);
                                returnFunction.call(me, success === true, callback);
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDelete-deleteResultsCallback of BaseChecklistPageController', ex);
                                returnFunction.call(me, false, callback);
                            }
                        };

                        if (!(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobileKey))) {
                            var eventId = AssetManagement.customer.manager.QPlosManager.deleteQPlos(prueflos);

                            if (eventId > -1) {
                                eventController.registerOnEventForOneTime(eventId, deleteQPlosCallback);
                            } else {
                                returnFunction.call(me, false, callback);
                            }
                        } else {
                            returnFunction.call(me, true, callback);
                        }
                    } else {
                        returnFunction.call(me, false, callback);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDelete-deleteResultsCallback of BaseChecklistPageController', ex);
                    returnFunction.call(me, false, callback);
                }
            };
            if (prueflos) {
                var eventId = AssetManagement.customer.manager.QPResultManager.deleteQPResults(prueflos);
                if (eventId > -1) {
                    eventController.registerOnEventForOneTime(eventId, deleteResultsCallback);
                } else {
                    returnFunction.call(this, false, callback);
                }
            } else {
                returnFunction.call(this, true, callback);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDelete of BaseChecklistPageController', ex);
            this._deleteRunning = false;
            callback.call(me, false, callback);
        }
    },

    deleteSuccessCallback: function (success, callback) {
        try {
            this._deleteRunning = false;
            callback.call(this, success === true);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performDelete-returnFunction of BaseChecklistPageController', ex);
        }
    },

    //public
    //on checklist completion checkbox click
    completeChecklist: function (checkbox, newValue, oldValue, eOpts) {
        try {
            if (this._saveRunning || this._deleteRunning)
                return;

            this.handleChecklistCompletionRequest(checkbox);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside completeChecklist of BaseChecklistPageController', ex);
        }
    },

    handleChecklistCompletionRequest: function (checkbox) {
        try {
            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

            var canBeCompleted = true;
            var myModel = this.getViewModel();
            var viewChars = myModel.get('viewChars');

            viewChars.each(function (viewChar) { //check entries of viewChars
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(viewChar.get('closed'))) {
                    canBeCompleted = false;
                    return false;
                }
            });

            if (canBeCompleted) {
                var confirmCallback = function (confirmed) {
                    try {
                        if (confirmed === true) {
                            this.handleSaveRequest(true); //save the checklists with updateflag change
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside completeChecklist-confirmCallback of BaseChecklistPageController', ex);
                    }
                };

                var dialogArgs = {
                    icon: 'resources/icons/question.png',
                    message: Locale.getMsg('checklistSetComplQuestion'),
                    callback: confirmCallback,
                    scope: this
                };

                AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
            } else { //if can't be completed
                //chaning the state of the completion checkbox back to unselected and showing notification
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();

                var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistNotAllCharsEval'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(message);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside completeChecklist of BaseChecklistPageController', ex);
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        } finally {
            if (checkbox) {
                Ext.defer(function () {
                    checkbox.setRawValue(false);
                    checkbox.lastValue = false;
                }, 10);
            }
        }
    },

    //private
    //returns, if there are any results currently entered for the checklist
    checklistHasEntries: function () {
        var hasEntries = false;

        try {
            var viewChars = this.getViewModel().get('viewChars');
            viewChars.each(function (viewChar) {
                if (viewChar.get('meanValue') || viewChar.get('remark') || viewChar.get('auswmgwrk2') || viewChar.get('checkBoxOK') || viewChar.get('checkBoxNOK') || viewChar.get('checkBoxNREL')) {
                    hasEntries = true;
                    return false;
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checklistHasEntries of BaseChecklistPageController', ex);
        }

        return hasEntries;
    },

    //private
    clearViewChars: function () {
        try {
            var viewChars = this.getViewModel().get('viewChars');

            viewChars.each(function (viewChar) {
                viewChar.set('meanValue', '');
                viewChar.set('remark', '');
                viewChar.set('auswmgwrk2', '');
                viewChar.set('checkBoxOK', false);
                viewChar.set('checkBoxNOK', false);
                viewChar.set('checkBoxNREL', false);
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearViewChars of BaseChecklistPageController', ex);
        }
    },

    //private
    printChecklistReport: function (record) {
        try {
            var qplos = this.getViewModel().get('qplos');

            if (qplos) {
                this.printReport(qplos);
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistNotMobile'), false, 1);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printChecklistReport of BaseChecklistPageController', ex);
        }
    },

    //private
    printReport: function (object) {
        try {
            var me = this;
            object.self.SAP_OBJECT_TYPE = 'BUS2045';
            var objTable = [];
            objTable.push(object);

            var enhancementPoint = function () {
                try {
                    eventController = AssetManagement.customer.controller.EventController.getInstance();
                    var eventId = eventController.getNextEventId();
                    if (eventId > -1) {
                        eventController.requestEventFiring(eventId, true);
                        return eventId;
                    } else {

                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-enhancementPoint of BaseChecklistPageController', ex);
                }
            };

            var parentSuccessCallback = function () {
                try {
                    AssetManagement.customer.core.Core.navigateBack();
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-parentSuccessCallback of BaseChecklistPageController', ex);
                }
            };

            var parentErrorCallback = function () {
                try {
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('generalErrorCreatingReportsOccured'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-parentErrorCallback of BaseChecklistPageController', ex);
                }
            };

            var reportFrameworkConfig = {
                isOneSignatureForAllDocuments: true,
                isVerticalView: true,
                actionAfterReportCreation: enhancementPoint,
                reportLanguage: null,
                signedObjets: null,
                preventLangChange: true
            };

            AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance().createReports(reportFrameworkConfig, objTable, parentSuccessCallback, parentErrorCallback);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport of BaseChecklistPageController', ex);
        }
    }
});