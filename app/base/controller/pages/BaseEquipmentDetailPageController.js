Ext.define('AssetManagement.base.controller.pages.BaseEquipmentDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.EquipmentManager',
		'AssetManagement.customer.manager.FuncLocManager',
		'AssetManagement.customer.manager.AddressManager',
		'AssetManagement.customer.manager.DMSDocumentManager',
		'AssetManagement.customer.manager.DocUploadManager',
		'AssetManagement.customer.model.bo.Equipment',
		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.manager.HistOrderManager',
		'AssetManagement.customer.manager.HistNotifManager',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],

	config: {
		contextReadyLevel: 1
	},
	
	//private
	_deleteRunning: false,
		
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
		    if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_DELETE) {
		        this.onDeleteEquipmentClicked();
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_EDIT) {
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_EDIT, { equipment: this.getViewModel().get('equipment') });
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_EQUICHANGE) {
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUI_CHANGE_POSITION, { equipment: this.getViewModel().get('equipment') });
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWORDER) {
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_ORDER, { equipment: this.getViewModel().get('equipment') });
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWNOTIF) {
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_NOTIF, { equipment: this.getViewModel().get('equipment') });
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_OBJ_STRUCTURE) {
		        AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_OBJ_STRUCTURE, { equipment: this.getViewModel().get('equipment') });
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT || optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST) {
		        this.showLongtextDialog();
		    } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_INTERNNOTE)
		        this.showInternNoteDialog();

		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseEquipmentDetailPageController', ex);
		}
	},
		
	//private
	//@override
	onCreateOptionMenu: function() {
		this._optionMenuItems = new Array();
	
		try {
		    var myEqui = this.getViewModel().get('equipment');
		    var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
		
		    var equiBLT = myEqui.get('backendLongtext');
		    var equiLLT = myEqui.get('localLongtext');
		    var equiInternNote = myEqui.get('internNote');

			if(myEqui.get('updFlag') === 'I')
				this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_DELETE);
				
			if (funcParaInstance.getValue1For('equi_change')  === 'X')
				this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_EDIT);
			
			if (funcParaInstance.getValue1For('equi_change_position') === 'X')
				this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_EQUICHANGE);


			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEWORDER);
			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEWNOTIF);
			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_OBJ_STRUCTURE);

			if(AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ltxt_equi') === 'X'){
				if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equiBLT) || !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equiLLT))
			    	this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST);
				else
				    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT);
			}

			if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('intv_equi') === 'X')
		    {
			    if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(equiInternNote))
			    {
			        this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_INTERNNOTE);
			    }
			}
			
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseEquipmentDetailPageController', ex);
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var equnr = '';
		
			if(argumentsObject['equnr'] && typeof argumentsObject['equnr'] === 'string') {
				equnr = argumentsObject['equnr'];
			} else if(argumentsObject['equipment'] && Ext.getClassName(argumentsObject['equipment']) === 'AssetManagement.customer.model.bo.Equipment') {
				this.getViewModel().set('equipment', argumentsObject['equipment']);
			} else {
				throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.pages.EquipmentDetailPageController'
				});
			}
			
			var equnrPassed = equnr !== '';
			
			if(equnrPassed) {
				this.getEquipment(equnr);
			} else {
				this.waitForDataBaseQueue();
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseEquipmentDetailPageController', ex);
		    this.errorOccurred();
		}
	},
	
	//get equi by equnr
	getEquipment: function(equnr) {
		try {
			var equipmentRequest = AssetManagement.customer.manager.EquipmentManager.getEquipment(equnr, true);
			this.addRequestToDataBaseQueue(equipmentRequest, 'equipment');
				
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipment of BaseEquipmentDetailPageController', ex)
		}
	},
	
	//navigate to funcLoc
	navigateToFuncLoc: function() {
		try {
			var funcLoc = this.getViewModel().get('equipment').get('funcLoc');
		
			if(funcLoc) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: funcLoc.get('tplnr') });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('funcLocNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToFuncLoc of BaseEquipmentDetailPageController', ex);
		}
	},
	
    //private
	showLongtextDialog: function () {
	    try {
	    	var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('c_ltxt_equi') === '';
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { equi: this.getViewModel().get('equipment') , readOnly : readOnly});
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialog of BaseEquipmentDetailPageController', ex);
	    }
	},

	showInternNoteDialog: function () {
	    try {
	        var readOnly = true;
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.INTERNNOTE_DIALOG, { equi: this.getViewModel().get('equipment'), readOnly: readOnly });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialog of BaseEquipmentDetailPageController', ex);
	    }
	},

    //public
	onLongtextSaved: function (affectedObject, newLongtext) {
	    try {
	        if (affectedObject && affectedObject === this.getViewModel().get('equipment')) {
	            this.rebuildOptionsMenu();
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLongtextSaved of BaseEquipmentDetailPageController', ex);
	    }
	},

	//navigate to superior equipment
	navigateToSuperiorEquipment: function() {
		try {
			var superiorEquipment = this.getViewModel().get('equipment').get('superiorEquipment');
		
			if(superiorEquipment) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: superiorEquipment.get('equnr') });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equipmentNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToSuperiorEquipment of BaseEquipmentDetailPageController', ex);
		}
	},
	
	//navigate to address in maps from general data
	openEquipmentAddress: function() {
		try {
			var address = this.getViewModel().get('equipment').get('address');
					
			if(address && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(address.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(address.get('postCode1'))) {
				//AssetManagement.customer.helper.NetworkHelper.openAddress(address);
				
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
				});

				//generate the hint
				equi = this.getViewModel().get('equipment');
				hint = AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0');

				request.addAddressWithHint(address, hint);
				
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openEquipmentAddress of BaseEquipmentDetailPageController', ex);
		}
	},
	
	onFileSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
		    //if the record represents an DMS document, download it from SAP system
		    //else try open its binary content
		    if (record.get('fileOrigin') === 'online') {
		        AssetManagement.customer.manager.DMSDocumentManager.downloadDMSDocument(record);
		    } else {
		        AssetManagement.customer.manager.DocUploadManager.openDocument(record);
		    }
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileSelected of BaseEquipmentDetailPageController', ex);
		}
	},
	
    onClassificationSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            var myModel = this.getViewModel();
            var equi = myModel.get('equipment');
            var classifications = equi.get('classifications');
            var objClassOfCharact = null;
            // filtered the charact to show the correct dialog
            classifications.each(function (objClass) {
                if (objClass.get('clint') === record.get('clint')) {
                    objClass.get('characts').each(function (charact) {
                        if (charact === record) {
                            objClassOfCharact = objClass;
                        }
                    });
                    if (record.get('atfor') === 'CHAR' && record.get('atein') === '' && record.get('atint') === '' && record.get('atwme') === 'X') {
                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_MULTICHECKBOX_DIALOG, { objClass: objClassOfCharact, charact: record });
                    } else if (record.get('atfor') === 'CHAR' && record.get('atein') === '' && record.get('atint') === '' && record.get('atwme') === '') {
                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_APPENDCHECKBOX_DIALOG, { objClass: objClassOfCharact, charact: record });
                    } else if (record.get('atein') === 'X' && record.get('atint') !== 'X' && record.get('atwme') === '' || record.get('atein') === 'X' && record.get('atint') === 'X' && record.get('msehi') === 'KWH') {
                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_SINGULARLINE_DIALOG, { objClass: objClassOfCharact, charact: record });
                    } else if (record.get('atein') === 'X' && record.get('atint') === 'X' && record.get('atwme') === ''
                        || record.get('atein') === 'X' && record.get('atwme') === 'X' && record.get('atfor') === 'CHAR') {
                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_SINGULARCHECKBOX_DIALOG, { objClass: objClassOfCharact, charact: record });
                    }
                }
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassificationSelected of BaseEquipmentDetailPageController', ex)
        }
    },

	onFileForDocUploadSelected: function(file) {
		try {
			if(!file) {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		
			var equi = this.getViewModel().get('equipment');
		
			var callback = function(docUpload) {
				try {
					if(docUpload) {
						//not neccessary, because the documents store of the equipment will automatically refresh itself
						//the docUpload will be added to this store by the manager
						//this.getView().refreshView();
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileAdded'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseEquipmentDetailPageController', ex);
				}
			};
		
			var addEvent = AssetManagement.customer.manager.DocUploadManager.addNewDocUploadForEqui(file, equi);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(addEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseEquipmentDetailPageController', ex);
		}
	},
	
	deleteDocUploadClicked: function(docUpload) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseEquipmentDetailPageController', ex);
   				}
   			};
   			
   			var deleteEvent = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(docUpload);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseEquipmentDetailPageController', ex);
		}
	},
	
	histOrderSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HIST_ORDER_DETAIL, { histOrder: record });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histOrderSelected of BaseEquipmentDetailPageController', ex);
		}
	},

	histNotifSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HIST_NOTIF_DETAIL, { histNotif: record });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histNotifSelected of BaseEquipmentDetailPageController', ex);
		}
	},
	
	onDeleteEquipmentClicked: function() {
		try {
			if(this._deleteRunning === true)
				return;
		
			//ask the user, if he really want's to delete this equipment
			var callback = function(confirmed) {
				try {
					if(confirmed === true) {						
						this.tryDeleteEquipment();
					}
				} catch(ex) {
		        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteEquipmentClicked of BaseEquipmentDetailPageController', ex);
		       	}    				
			};
			
			var dialogArgs = {
			    icon: 'resources/icons/trash.png',
				message: Locale.getMsg('deleteEquiQuestion'),
				callback: callback,
				scope: this
			};
			
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteEquipmentClicked of BaseEquipmentDetailPageController', ex);
		}
	},

	tryDeleteEquipment: function() {
		try {
			this._deleteRunning = true;
		
			var equi = this.getViewModel().get('equipment');
		
			if(equi) {
				AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
				
				var me = this;

				var callback = function(success) {
					try {
						if(success === true) {
							AssetManagement.customer.core.Core.navigateBack();
							
							Ext.defer(function() { 
										me._deleteRunning = false;
								}, 100, me);
							
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equipmentDeleted'), false, 0);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						} else {
							me._deleteRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingEquipment'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
					} catch (ex) {
						me._deleteRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteEquipment of BaseEquipmentDetailPageController', ex);
					}
				};
		
				var deleteEvent = AssetManagement.customer.manager.EquipmentManager.deleteEquipment(equi);
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				me._deleteRunning = false;
				AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingEquipment'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			this._deleteRunning = false;
       	 	AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteEquipment of BaseEquipmentDetailPageController', ex);
		}
	},

	onClassValuesSaved: function (classValue) {
	    try {

	        this.getView().refreshView();
	      
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassValuesSaved of BaseEquipmentDetailPageController', ex);
	    }
	},

	onClassValueSaved: function (classValue) {
	    try {

	        this.getView().refreshView();

	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassValueSaved of BaseEquipmentDetailPageController', ex);
	    }
	},
	
	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenuFileList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();
			
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
			
			var options = [ ];				
			
			if(record.get('fileOrigin') === 'local') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}
			
			this.rowMenu.onCreateContextMenu(options, record);
			
			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuFileList of BaseEquipmentDetailPageController', ex);
		}
    },

    onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
			
			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseEquipmentDetailPageController', ex);
		}
    },
    
    onContextMenuItemSelected: function(itemID, record) {
       try {
    	   if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
    		   var className = Ext.getClassName(record);
    	   
    		   if(className.indexOf('DocUpload') > -1) {
    			   this.deleteDocUploadClicked(record);
    		   }
    	   }
       } catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseEquipmentDetailPageController', ex);
       }
    },

    beforeDataReady: function(){
        try {

            var myModel = this.getViewModel();
            var equipment = myModel.get('equipment');
            var classifications = equipment.get('classifications');

            var preparedClassifications = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.Charact',
                autoLoad: false
            });

            if (classifications && classifications.getCount() > 0) {
                classifications.each(function (objClass) {
                    objClass.get('characts').each(function (charact) {
                        preparedClassifications.add(charact);
                    });
                });
            }

            myModel.set('preparedClassifications', preparedClassifications);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseEquipmentDetailPageController', ex);
        }
    },

    measPointSelected: function (tableview, td, cellIndex, record, tr, rowIndex, event, eOpts) {
        try {
            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_MEAS_DOC_EDIT, { measPoint: record });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMeasPointSelected of BaseEquipmentDetailPageController', ex);
        }
    }
	///------ CONTEXT MENU END -------///
});