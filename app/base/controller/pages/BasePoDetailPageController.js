Ext.define('AssetManagement.base.controller.pages.BasePoDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',
	alias: 'controller.BasePoDetailPageController',

	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.manager.UserManager',
	    'AssetManagement.customer.controller.ToolbarController',
	    'AssetManagement.customer.helper.LanguageHelper',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.GoodsMovementItem'
	],

	config: {
		contextReadyLevel: 1
	},

	//private
	_requestRunning: false,

	//protected
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {

            var myModel = this.getViewModel();
            var poItem = null;
            var store = null;

            if(argumentsObject['miProcess'])
                myModel.set('miProcess',argumentsObject['miProcess']);

            var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
            if (selectedLgort)
                myModel.set('selectedLgort', selectedLgort);


            if (argumentsObject['goodsMovementItemsStore'] ) {
                store = argumentsObject['goodsMovementItemsStore'];
            }else{
                store = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.GoodsMovementItem',
                autoLoad: false
                });

            }
            myModel.set('goodsMovementItemsStore', store);

            if (argumentsObject['poItem'] ) {
                poItem = argumentsObject['poItem'];
            }
            myModel.set('poItem', poItem);

			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BasePoDetailPageController', ex);
		}
	},

    refreshData: function(triggerPageRequestCallbackWhenDone) {
        try {
            if(!triggerPageRequestCallbackWhenDone) {
                this._pageRequestCallback = null;
                this._pageRequestCallbackScope = null;
            }

            // this.getViewModel().resetData(); //Commented to have a persistent store when navigating back to page

            //set to zero
            this._contextLevel = 0;

            if(this._contextReadyLevel === 0) {
                this.contextReady();
            } else {
                //start the loading cycle
                this.startBuildingPageContext(this.getCurrentArguments());
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
        }
    },

    onAddButtonClick: function () {
        try {
            var myModel = this.getViewModel();
            var poItem = myModel.get('poItem');
            if (poItem) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(poItem.get('menge_ist'));
                var soll_menge =  AssetManagement.customer.utils.NumberFormatUtils.parseNumber(poItem.get('menge')) -  AssetManagement.customer.utils.NumberFormatUtils.parseNumber(poItem.get('menge_del'));
                    quantity = quantity + 1;
                    if(quantity <= soll_menge ){
                        poItem.set('menge_ist', quantity.toString());
                    } else{
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('orderQuantReached'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                myModel.set('poItem', poItem);
                this.refreshView();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddButtonClick of BasePoDetailPageController', ex);
        }
    },

    onRemoveButtonClick: function () {
        try {
            var myModel = this.getViewModel();
            var poItem = myModel.get('poItem');
            if (poItem) {
                var quantity = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(poItem.get('menge_ist'));
                quantity = quantity - 1;
                if(quantity >= 0 ){
                    poItem.set('menge_ist', quantity.toString());
                } else{
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage('Menge = 0', false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                }
                myModel.set('poItem', poItem);
                this.refreshView();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onRemoveButtonClick of BasePoDetailPageController', ex);
        }
    },

    onSaveClick: function () {
        try {
            var myModel = this.getViewModel();
            var poItem = myModel.get('poItem');
            var miProcess = myModel.get('miProcess');
            var selectedLgort = myModel.get('selectedLgort');
            var goodsMovementItemsStore = myModel.get('goodsMovementItemsStore');
            var goodsMovementItem = goodsMovementItemsStore.findRecord('matnr', poItem.get('matnr'));
            if(!goodsMovementItem){
                goodsMovementItem = Ext.create('AssetManagement.customer.model.bo.GoodsMovementItem');
            }
            if(poItem.get('menge_ist') && AssetManagement.customer.utils.NumberFormatUtils.parseNumber(poItem.get('menge_ist')) > 0) {
                goodsMovementItem.set('matnr', poItem.get('matnr'));
                goodsMovementItem.set('werks', poItem.get('werks'));
                goodsMovementItem.set('lgort', selectedLgort.get('lgort'));
                goodsMovementItem.set('ebeln', poItem.get('ebeln'));
                goodsMovementItem.set('ebelp', poItem.get('ebelp'));
                goodsMovementItem.set('menge', poItem.get('menge_ist'));
                goodsMovementItem.set('meins', poItem.get('meins'));
                goodsMovementItem.set('item_text', poItem.get('txz01'));
                goodsMovementItem.set('bwart', miProcess.get('bwart'));
                goodsMovementItem.set('kzbew', miProcess.get('kzbew'));
                goodsMovementItemsStore.add(goodsMovementItem);
            }
            myModel.set('goodsMovementItemsStore', goodsMovementItemsStore);

            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WE_LIST, {goodsMovementItemsStore: goodsMovementItemsStore }, 2); // remove 2 Pages from history (Detail Page and first list page) to be clear in navigation
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaveClick of BasePoDetailPageController', ex);
        }
    }
});