Ext.define('AssetManagement.base.controller.pages.BaseFuncLocListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.FuncLocManager',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
	],
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},

	mixins: {
	    handlerMixin: 'AssetManagement.customer.controller.mixins.FuncLocListPageControllerMixin'
	},
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
	    try {

	        if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
	            this.onSearchButtonClicked();
	        } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH) {
	            this.onClearSearchButtonClicked();
	        }
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseFuncLocListPageController', ex);
		}
	},
		
	//private
	onCreateOptionMenu: function() {
		try {
		    //this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH ];
		    this._optionMenuItems = new Array();

		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseFuncLocListPageController', ex);
		}
	},
    //functions for search
    //public
	onSearchButtonClicked: function () {
	    try {
	        var myModel = this.getViewModel();

	        this.manageSearchCriterias();

	        var searchDialogArgs = {
	            source: myModel.get('funcLocStore'),
	            criterias: myModel.get('searchCriterias')
	        };

	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseFuncLocListPageController', ex);
	    }
	},

    ////functions for search
    ////private
    ////handles initialization of search criterias
	//manageSearchCriterias: function () {
	//    try {
	//        var myModel = this.getViewModel();
	//        var currentSearchCriterias = myModel.get('searchCriterias');
	//        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
	//        var searchMixin = this.mixins.handlerMixin

	//        if (!currentSearchCriterias) {
	//            var searchArray = [{
	//                id: 'tplnr',
	//                columnName: 'funcLoc_Short',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            {
	//                id: 'eqfnr',
	//                columnName: 'sortField',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            {
	//                id: 'postalCode',
	//                columnName: 'address',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            ];

	//            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(searchArray, searchMixin);

	//            myModel.set('searchCriterias', newCriterias);
	//        }
	//    } catch (ex) {
	//        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseFuncLocListPageController', ex);
	//    }
	//},

    //private
    //handles initialization of search criterias
	manageSearchCriterias: function () {
	    try {
	        var myModel = this.getViewModel();
	        var currentSearchCriterias = myModel.get('searchCriterias');
	        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
	        var searchMixin = this.mixins.handlerMixin

	        if (!currentSearchCriterias) {
	            var gridpanel = Ext.getCmp('funcLocGridPanel');

	            //var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray);
	            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray, searchMixin);

	            myModel.set('searchCriterias', newCriterias);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseFuncLocListPageController', ex);
	    }
	},
    //functions for search
    //public
	onSearchApplied: function (searchResult) {
	    try {

	        this.getViewModel().set('constrainedFuncLocs', searchResult);

	        this.setupInitialSorting();

	        //this.setupAutoCompletion();

	        this.applyCurrentFilterCriteriasToStore();

	        this.refreshView();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of BaseFuncLocListPageController', ex);
	    }
	},

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
	setupInitialSorting: function () {
	    try {
	        var myModel = this.getViewModel();
	        var constrainedEquipments = myModel.get('constrainedFuncLocs');

	        //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
	        if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
	            myModel.set('sortProperty', 'mymAgreement');
	            myModel.set('sortDirection', 'DESC');
	        }

	        //apply the current sort rule
	        if (constrainedEquipments) {
	            constrainedEquipments.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
	            ]);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseFuncLocListPageController', ex);
	    }
	},

	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			this.loadListStore();
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseFuncLocListPageController', ex);
		}	
	},
		
	loadListStore: function() {
		try {
			var funcLocStoreRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLocs(true);
			this.addRequestToDataBaseQueue(funcLocStoreRequest, 'funcLocStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseFuncLocListPageController', ex);
		}
	},
		
	//list handler
	onFuncLocSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: record.get('tplnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseFuncLocListPageController', ex);
		}
	},
	
	onFuncLocSelectedByTabHold: function(funcLoc) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: record.get('tplnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelectedByTabHold of BaseFuncLocListPageController', ex);
		}
	},

	navigateToAddress: function(funcLoc) {
       try {
			var addressInformation = null;
		
			if(funcLoc) {
				addressInformation = funcLoc.get('address');
			}
				
			if(addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postCode1'))) {
				// AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);
				
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES});
	
				//generate the hint
				
				hint = funcLoc.get('tplnr');
				
				request.addAddressWithHint(addressInformation, hint);
				
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToAddress of BaseFuncLocListPageController', ex);
		}
	},

   
    beforeDataReady: function() {
    	try {
    		//apply the current filter criteria on the store, before it is transferred into the view
    		this.applyCurrentFilterCriteriasToStore();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseFuncLocListPageController', ex);
		}
	},
    
	applyCurrentFilterCriteriasToStore: function() {
    	try {
			this.getView().getSearchValue();

			var funcLocToSearch = this.getViewModel().get('searchFuncLoc');
			var funcLocStore = this.getViewModel().get('funcLocStore');
			
			if(funcLocStore) {
				funcLocStore.clearFilter();
	
				// Techn. Platz Nr.
				if(funcLocToSearch.get('tplnr')) {
					funcLocStore.filterBy(function (record) {
						try {
						    if (record.get('tplnr').toUpperCase().indexOf(funcLocToSearch.get('tplnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocListPageController', ex);
						}
					});
				// Kurztext
				} else if(funcLocToSearch.get('pltxt')) {
					funcLocStore.filterBy(function (record) {
						try {
						    if (record.get('pltxt') && record.get('pltxt').toUpperCase().indexOf(funcLocToSearch.get('pltxt').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocListPageController', ex);
						}
					});
				// Sortiernummer
				} else if(funcLocToSearch.get('eqfnr')) {
					funcLocStore.filterBy(function (record) {
						try {
						    if (record.get('eqfnr') && record.get('eqfnr').toUpperCase().indexOf(funcLocToSearch.get('eqfnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocListPageController', ex);
						}
					});
				// Standort
				} else if(funcLocToSearch.get('stort')) {
					funcLocStore.filterBy(function (record) {
						try {
						    if (record.get('stort') && record.get('stort').toUpperCase().indexOf(funcLocToSearch.get('stort').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocListPageController', ex);
						}
					});
				// Raum
				} else if(funcLocToSearch.get('msgrp')) {
					funcLocStore.filterBy(function (record) {
						try {
						    if (record.get('msgrp') && record.get('msgrp').toUpperCase().indexOf(funcLocToSearch.get('msgrp').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseFuncLocListPageController', ex);
						}
					});
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseFuncLocListPageController', ex)
		}
    },
    
    onFuncLocListPageSearchFieldChange: function() {
    	try {
			this.applyCurrentFilterCriteriasToStore();
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocListPageSearchFieldChange of BaseFuncLocListPageController', ex)
		}
    },
    
    onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
			
			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseFuncLocListPageController', ex);
		}
    },

    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias'))) {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias    
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseFuncLocListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/clear_search.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseFuncLocListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseFuncLocListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseFuncLocListPageController', ex);
        }
    },
    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of EquipmentListPageController', ex);
        }
    }
    
});