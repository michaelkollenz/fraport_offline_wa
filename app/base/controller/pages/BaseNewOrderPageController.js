Ext.define('AssetManagement.base.controller.pages.BaseNewOrderPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',

    requires: [
		'AssetManagement.customer.manager.OrderManager',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger'
    ],

    config: {
        contextReadyLevel: 1
    },

    //private
    _saveRunning: false,

    //public
    //@override
    onOptionMenuItemSelected: function (optionID) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
                if (this._saveRunning === true)
                    return;

                var retVal = this.saveOrder();
                this.getView().saveOrderCallback(retVal);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseNewOrderPageController', ex);
        }
    },

    //protected
    //@override
    onCreateOptionMenu: function () {
        try {
            this._optionMenuItems = [AssetManagement.customer.controller.ToolbarController.OPTION_SAVE];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * tries to save the current order object
	 * returns null if the order was saved or an error message if data is missing
	 */
    saveOrder: function () {
        var retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage("", true, 0);
        try {
            this._saveRunning = true;
            AssetManagement.customer.controller.ClientStateController.enterLoadingState();

            //update the models from the data of the view fields
            this.getView().updateModelData();
            //update the order object and the dependent objects with data from the customizing like werk, arbpl
            if (!this.fillOrderData()) {
                retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingOrder'), false, 2);
                return retVal;
            } else if (!this.addPartnersToObject()) { //create the partnerarray from the partner objects
                retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingOrder'), false, 2);
            } else {
                //check if data is complete
                var checkRetVal = this.checkOrderComplete();
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(checkRetVal.message)) {
                    //get the steus for the order type
                    var ordType = this.getViewModel().get('orderTypes').findRecord('auart', this.getViewModel().get('order').get('auart'));

                    if (ordType)
                        this.getViewModel().get('operation').set('steus', ordType.get('steus'));
                    else
                        this.getViewModel().get('operation').set('steus', '');

                    //update the operations store in the order store with the viewmodel operation store
                    this.getViewModel().get('order').get('operations').insert(0, this.getViewModel().get('operation'));

                    var me = this;
                    var callback = function (savedOrder) {
                        try {
                            if (savedOrder) {
                                //go to order details
                                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: me.getViewModel().get('order').get('aufnr') }, true);

                                Ext.defer(function () {
                                    me._saveRunning = false;
                                }, 2000, me);

                                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('orderSaved'), true, 0);
                                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                            } else {
                                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingOrder'), false, 2);
                                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOrder of BaseNewOrderPageController', ex)
                            me._saveRunning = false;
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        }
                    };

                    var eventId = AssetManagement.customer.manager.OrderManager.saveNewOrder(this.getViewModel().get('order'));

                    if (eventId > 0) {
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
                    } else {
                        retval = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingOrder'), false, 2);
                    }
                } else {
                    //order data is incomplete
                    retVal = checkRetVal;
                }
            }

            if (!retVal.success) {
                this._saveRunning = false;
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveOrder of BaseNewOrderPageController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }

        return retVal;
    },

    /**
	 * checks if the data of the order object is complete
	 * returns null if the data is complete or an error message if data is missing
	 */
    checkOrderComplete: function () {
        var retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage("", true, 0);
        try {
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('order').get('auart'))) {
                retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectOrderType'), false, 2);
            }
            else if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('order').get('ilart'))) {
                retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectIlart'), false, 2);
            }
            else if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getViewModel().get('order').get('ktext'))) {
                retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectKtext'), false, 2);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkOrderComplete of BaseNewOrderPageController', ex);
        }
        return retVal;
    },

    fillOrderData: function () {
        var retVal = false;
        try {
            //order data
            var myModel = this.getViewModel()
            var order = myModel.get('order');
            var operation = myModel.get('operation');

            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();

            order.set('iwerk', userInfo.get('orderPlanPlant'));
            order.set('ingrp', userInfo.get('orderPlanGroup'));

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('vaplz')))
                order.set('vaplz', userInfo.get('orderWorkcenter'));

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(order.get('wawrk')))
                order.set('wawrk', userInfo.get('orderPlantWrkc'));

            //operation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('arbpl')))
                operation.set('arbpl', order.get('vaplz'));

            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(operation.get('werks')))
                operation.set('werks', order.get('wawrk'));

            retVal = true;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkOrderComplete of BaseNewOrderPageController', ex);
        }
        return retVal;

    },

    /**
	 * adds the partnerobjects to the order (if a notification will be created, the store will be shifted to the notification later)
	 */
    addPartnersToObject: function () {
        var retVal = false;
        try {
            //create a store for the partner objects
            var partnerStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.Partner',
                autoLoad: false
            });
            //add the partners to the store
            if (this.getViewModel().get('customer'))
                partnerStore.add(this.getViewModel().get('customer'));
            if (this.getViewModel().get('partnerAp'))
                partnerStore.add(this.getViewModel().get('partnerAp'));
            if (this.getViewModel().get('partnerZr'))
                partnerStore.add(this.getViewModel().get('partnerZr'));
            //add the partnerstore to the order
            this.getViewModel().get('order').set('partners', partnerStore);

            retVal = true;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createPartners of BaseNewOrderPageController', ex);
        }
        return retVal;
    },

    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            var myViewModel = this.getViewModel();
            //if the order object is undefined create a one
            if (myViewModel.get('order') === undefined || myViewModel.get('order') === null) {
                myViewModel.set('order', Ext.create('AssetManagement.customer.model.bo.Order'));
                myViewModel.get('order').initNewOrder();

		        if (argumentsObject != null && argumentsObject.funcLoc != undefined && argumentsObject.funcLoc != null) {
		            this.onFuncLocSelected(argumentsObject.funcLoc);
		        }
		        if (argumentsObject != null && argumentsObject.equipment != undefined && argumentsObject.equipment != null) {
		            this.onEquiSelected(argumentsObject.equipment);
		        }
		        if (argumentsObject !== null && argumentsObject.notif !== undefined && argumentsObject.notif !== null) {
                    myViewModel.get('order').set('notif', argumentsObject.notif);
                    myViewModel.get('order').set('mobileKeyNotif', argumentsObject.notif.get('mobileKey'));
                    myViewModel.get('order').set('qmnum', argumentsObject.notif.get('qmnum'));
                    myViewModel.get('order').set('ktext', argumentsObject.notif.get('qmtxt'));
		        }
		    }

            //if the operation object is undefined create a new operation and create an operation store for the order
            if (myViewModel.get('order').get('operations') === undefined ||
                myViewModel.get('order').get('operations').length === 0) {

                var operation = Ext.create('AssetManagement.customer.model.bo.Operation');
                operation.initNewOperation();
                operation.set('vornr', '0010');

                myViewModel.set('operation', operation);
                var operationList = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.Operation',
                    autoLoad: false
                });

                operationList.add(operation);
                myViewModel.get('order').set('operations', operationList);
            } else {
                operation = this.getViewModel().get('order').get('operations').data.items[0];
                myViewModel.set('operation', operation);
            }

            var ordTypesRequest = AssetManagement.customer.manager.Cust_011Manager.getOrderTypes(true);
            this.addRequestToDataBaseQueue(ordTypesRequest, "orderTypes");

            var ilartRequest = AssetManagement.customer.manager.CustIlartOrTypeManager.getCustIlartOrTypes(true);
            this.addRequestToDataBaseQueue(ilartRequest, "ilartTypes");

            var bemotRequest = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
            this.addRequestToDataBaseQueue(bemotRequest, "bemotTypes");

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseNewOrderPageController', ex);
        }
    },

    //Selection through pickers
    /**
	 * Event fired when funcloc search button is clicked
	 */
    onSelectFuncLocClick: function () {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.FUNCLOCPICKER_DIALOG, {});
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectFuncLocClick of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when equi search button is clicked
	 */
    onSelectEquiClick: function () {
        try {
            var selectedFuncLocTplnr = '';
            var selectedFuncLoc = this.getViewModel().get('funcLoc');

            if (selectedFuncLoc) {
                selectedFuncLocTplnr = selectedFuncLoc.get('tplnr');
            }

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.EQUIPMENTPICKER_DIALOG, { tplnr: selectedFuncLocTplnr });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectEquiClick of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when Customer search button is clicked
	 */
    onSelectCustomerClick: function () {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CUSTOMERPICKER_DIALOG, {});
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectCustomerClick of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when Partner search button is clicked
	 */
    onSelectePartnerApClick: function () {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.PARTNERPICKER_DIALOG, { partner: this.getViewModel().get('customer'), parVw: 'AP' });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectePartnerApClick of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when Partner search button is clicked
	 */
    onSelectePartnerZrClick: function () {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.PARTNERPICKER_DIALOG, { partner: this.getViewModel().get('customer'), parVw: 'ZR' });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectePartnerZrClick of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when equi has been selected in dialog
	 */
    onEquiSelected: function (record) {
        try {
            var myModel = this.getViewModel();
            var order = myModel.get('order');

            var equiHasSuperiorFuncLoc = !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(record.get('tplnr'));

            myModel.set('equi', record);
            order.set('vaplz', record.get('gewrk'));
            order.set('wawrk', record.get('wergw'));
            order.set('equnr', record.get('equnr'));
            order.set('tplnr', equiHasSuperiorFuncLoc ? record.get('tplnr') : '');

            //manage connected func loc data
            if (equiHasSuperiorFuncLoc) {
                //equi has superior funcloc, check if the current func loc matches equipments funcloc, if it does UI refresh may be done
                //else try to load the funcloc before refreshing the UI
                var tragetTplnr = record.get('tplnr');
                var currentFuncLoc = myModel.get('funcLoc');
                if (currentFuncLoc && currentFuncLoc.get('tplnr') === tragetTplnr) {
                    this.getView().fillEquiFuncLocTextField();
                } else {
                    var me = this;

                    //equipments func loc does not match the current funcloc, so try to load it
                    var funcLocCallback = function (funcLoc) {
                        try {
                            if (funcLoc) {
                                myModel.set('funcLoc', funcLoc);
                            } else {
                                myModel.set('funcLoc', null);
                                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equisFuncLocNotMobile'), true, 1);
                                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                            }

                            me.getView().fillEquiFuncLocTextField();

                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseNewOrderPageController', ex);
                            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                        }
                    };

                    var eventId = AssetManagement.customer.manager.FuncLocManager.getFuncLocLightVersion(tragetTplnr);

                    if (eventId > 0) {
                        AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
                        AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, funcLocCallback);
                    } else {
                        myModel.set('funcLoc', null);
                        this.getView().fillEquiFuncLocTextField();
                    }
                }
            } else {
                this.getViewModel().set('funcLoc', null);
                this.getView().fillEquiFuncLocTextField();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when func has been selected in dialog
	 */
    onFuncLocSelected: function (record) {
        try {
            this.getViewModel().set('funcLoc', record);
            this.getViewModel().set('equi', null);
            this.getViewModel().get('order').set('vaplz', record.get('gewrk'));
            this.getViewModel().get('order').set('wawrk', record.get('wergw'));
            this.getViewModel().get('order').set('equnr', '');
            this.getViewModel().get('order').set('tplnr', record.get('tplnr'));
            this.getView().fillEquiFuncLocTextField();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when the order shorttext is changed
	 */
    onOrderTextChanged: function (data) {
        try {
            this.getViewModel().get('order').set('ktext', data.value);
            this.getView().setOperShortText();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderTextChanged of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when customer has been selected in dialog
	 */
    onCustomerSelected: function (record) {
        try {
            var customer = Ext.create('AssetManagement.customer.model.bo.Partner');
            customer.set('parvw', 'AG');
            customer.set('parnr', record.get('kunnr'));
            customer.set('partnertype', 'KU');
            customer.set('name1', record.get('firstname'));
            customer.set('name2', record.get('lastname'));
            customer.set('postcode1', record.get('postlcod1'));
            customer.set('city1', record.get('city'));
            customer.set('street', record.get('street'));
            customer.set('title', record.get('titlep'));
            customer.set('telnumber', record.get('tel1numbr'));
            customer.set('faxnumber', record.get('faxnumber'));
            this.getViewModel().set('customer', customer);
            this.getView().fillCustomerTextField();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerSelected of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when a partner for the partnerAp has been selected in dialog
	 */
    onPartnerApSelected: function (record) {
        try {
            var partner = this.createPartnerFromDialogRecord(record);
            partner.set('parvw', 'AP');
            this.getViewModel().set('partnerAp', partner);
            this.getView().fillCustomerTextField();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPartnerApSelected of BaseNewOrderPageController', ex);
        }
    },

    /**
	 * Event fired when a partner for the partnerZr has been selected in dialog
	 */
    onPartnerZrSelected: function (record) {
        try {
            var partner = this.createPartnerFromDialogRecord(record);
            partner.set('parvw', 'ZR');
            this.getViewModel().set('partnerZr', partner);
            this.getView().fillCustomerTextField();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPartnerZrSelected of BaseNewOrderPageController', ex);
        }
    },

    createPartnerFromDialogRecord: function (record) {
        var retval = null;

        try {
            var partner = Ext.create('AssetManagement.customer.model.bo.Partner');
            partner.set('parnr', record.get('parnr'));
            partner.set('kunnr', record.get('kunnr'));
            partner.set('partnertype', '');
            partner.set('name1', record.get('firstname'));
            partner.set('name2', record.get('lastname'));
            partner.set('postcode1', record.get('postlcod1'));
            partner.set('city1', record.get('city'));
            partner.set('street', record.get('street'));
            partner.set('title', record.get('titlep'));
            partner.set('telnumber', record.get('tel1numbr'));
            partner.set('faxnumber', record.get('faxnumber'));

            retval = partner;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside createPartnerFromDialogRecord of BaseNewOrderPageController', ex);
        }

        return partner;
    }
});
