Ext.define('AssetManagement.base.controller.pages.BaseFuncLocEditPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	    
	requires: [
	    'AssetManagement.customer.manager.FuncLocManager',
	    'AssetManagement.customer.model.helper.ReturnMessage',
	    'AssetManagement.customer.utils.DateTimeUtils',
		'AssetManagement.customer.helper.OxLogger'
	], 
	
	config: {
		contextReadyLevel: 1
	},
	
	//private
	_saveRunning: false,
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;

				this.trySaveMasterData();
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseFuncLocEditPageController', ex);
		}
	},
	
	//public
	//@override
	hasUnsavedChanges: function() {
		var retval = false;
	
		try {
			retval = !this.isInputEqualToModel();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasUnsavedChanges of BaseFuncLocEditPageController', ex);
		}
		
		return retval;
	},
	
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
		    this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseFuncLocEditPageController', ex);
	    }
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var tplnr = '';
		
			if(argumentsObject['tplnr'] && typeof argumentsObject['tplnr'] === 'string') {
				tplnr = argumentsObject['tplnr'];
			} else if(argumentsObject['funcLoc'] && Ext.getClassName(argumentsObject['funcLoc']) === 'AssetManagement.customer.model.bo.FuncLoc') {
				this.getViewModel().set('funcLoc', argumentsObject['funcLoc']);
			} else {
				throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.pages.FuncLocEditPageController'
				});
			}
			
			var tplnrPassed = tplnr !== '';
			
			if(tplnrPassed) {
				this.getFuncLoc(tplnr);
			}
			
			this.waitForDataBaseQueue();
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseFuncLocEditPageController', ex);
		    this.errorOccurred();
		}
	},
	
	getFuncLoc: function(tplnr) {
		try {
			var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLoc(tplnr, true);
			this.addRequestToDataBaseQueue(funcLocRequest, 'funcLoc');
				
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLoc of BaseFuncLocEditPageController', ex)
		}
	},
	
	trySaveMasterData: function(){
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
			var me = this; 
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						var saveCallback = function(success) {
							try {
								if(success === true) {
									//goBack after save completed
									AssetManagement.customer.core.Core.navigateBack();
									
									Ext.defer(function() { 
										me._saveRunning = false;
									}, 2000, me);
									
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('funcLocSaved'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									me._saveRunning = false;
									AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingFuncLoc'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMasterData of BaseFuncLocEditPageController', ex)
							}
						};
						
						if(me.isInputEqualToModel()) {
							saveCallback(true);
						} else {
							//transfer values from view into funcLoc
							me.getView().transferViewStateIntoModel();
							
							var funcLoc = me.getViewModel().get('funcLoc');
						
							var eventId = AssetManagement.customer.manager.FuncLocManager.saveFuncLoc(funcLoc);
							
							if(eventId > 0) {
								AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
							} else {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingFuncLoc'), false, 2);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							}
						}
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveTimeConf of BaseFuncLocEditPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMasterData of BaseFuncLocEditPageController', ex)
		}
	},
	
	/**
	 * checks if the data of the funcloc object is complete 
	 */
	checkInputValues: function(callback, callbackScope)  {
		try {
			var retval = true;
			var errorMessage = '';
				
			var values = this.getView().getCurrentInputValues();
			
			//currently no checks implemented
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			returnFunction.call(this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseFuncLocEditPageController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	//checks, if there are any differences between the current input state and the model state
	isInputEqualToModel: function() {
		var retval = true;
	
		try {
			var inputValues = this.getView().getCurrentInputValues();
			var funcLoc = this.getViewModel().get('funcLoc');
			
			if(funcLoc.get('invnr') !== inputValues.invnr) {
				retval = false;
			} else if(!AssetManagement.customer.utils.DateTimeUtils.areEqual(funcLoc.get('ansdt'), inputValues.ansdt)) {
				retval = false;
			} else if(funcLoc.get('eqfnr') !== inputValues.eqfnr) {
				retval = false;
			} else if(funcLoc.get('stort') !== inputValues.stort) {
				retval = false;
			} else if(funcLoc.get('msgrp') !== inputValues.msgrp) {
				retval = false;
			} else if(funcLoc.get('herst') !== inputValues.herst) {
				retval = false;
			} else if(funcLoc.get('herld') !== inputValues.herld) {
				retval = false;
			} else if(funcLoc.get('typbz') !== inputValues.typbz) {
				retval = false;
			} else if(funcLoc.get('baumm') !== inputValues.baumm) {
				retval = false;
			} else if(funcLoc.get('baujj') !== inputValues.baujj) {
				retval = false;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isInputEqualToModel of BaseFuncLocEditPageController', ex);
		}
		
		return retval;
	}
});