Ext.define('AssetManagement.base.controller.pages.BaseExtConfPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
    requires: [
        'AssetManagement.customer.manager.BanfManager',
        'AssetManagement.customer.model.bo.BanfPosition',
        'AssetManagement.customer.model.bo.BanfHeader',
        'AssetManagement.customer.model.bo.UserInfo',
        'AssetManagement.customer.manager.Cust_017Manager',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger'
    ], 
    
	config: {
		contextReadyLevel: 3
	},
	
	//private
	_saveRunning: false,
	_DEFAULT_CURRENCY: 'EUR',
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
			
				this.trySaveExtConf();
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
				if(this._saveRunning === true)
					return;
			
				this.initializeNewExtConf();
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseExtConfPageController', ex);
		}
	},
	
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM, AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseExtConfPageController', ex);
		}
	},
	
	isCurrentlySelectedMaterialSmallPartsExpense: function() {
		var retval = false;

		try {
			if(this.getViewModel().get('selectedMaterial')) {
				var extMaterials = AssetManagement.customer.model.bo.FuncPara.getInstance().get('ext_conf_material');
				
				if(extMaterials && extMaterials.getCount() > 0) {
					var curMaterial = this.getViewModel().get('selectedMaterial');
					var curMatnr = curMaterial ? curMaterial.get('matnr') : '';
				
					if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(curMatnr)) {
						extMaterials.each(function(extMaterial) {
							if(extMaterial.get('fieldname') === curMatnr) {
								var flag = extMaterial.get('fieldval1');	
								if(flag === 'X') {
									retval = true;
									return false;
								}
							}
						}, this);
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isCurrentlySelectedMaterialSmallPartsExpense of BaseExtConfPageController', ex);
		}
		
		return retval;
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var myModel = this.getViewModel();
			
			//extract parameters, if any given
			if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
				myModel.set('order', order);
				myModel.set('operation', order.get('operations').getAt(0)); 
			} else if(Ext.getClassName(argumentsObject['extConf']) === 'AssetManagement.customer.model.bo.BanfPosition') {
				myModel.set('extConf', argumentsObject['extConf']);
				myModel.set('selectedMaterial', argumentsObject['extConf'].get('material'));
				myModel.set('isEditMode', true);
			} else {
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.ExtConfPageController'
			    });
			}
              			
			//get BanfHeader - for saving positions // if updFlag X create a new one // if updFlag I use this one to save new positions
			this.waitForDataBaseQueue(); 
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseExtConfPageController', ex);
		    this.errorOccurred();
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		this.callParent();
		
		try {
			if(this._contextLevel === 1) {
				this.performContextLevel1Requests();
			} else if(this._contextLevel === 2) {
				this.performContextLevel2Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseExtConfPageController', ex);
		}
	},
	
	/**
	 * Loads banfPositions, banfHeader, cust017, custParas, materials and timeconfs from database
	 */
	performContextLevel1Requests: function() {
		try {
			var banfPositionsRequest = AssetManagement.customer.manager.BanfManager.getBanfPositions(order.get('aufnr'));
			this.addRequestToDataBaseQueue(banfPositionsRequest, 'banfPositions');
			
			var banfHeaderRequest = AssetManagement.customer.manager.BanfManager.getBanfHeader(order.get('aufnr'));
			this.addRequestToDataBaseQueue(banfHeaderRequest, 'banfHeader');
			
		    var cust017Request = AssetManagement.customer.manager.Cust_017Manager.getCust_017(true);
		    this.addRequestToDataBaseQueue(cust017Request, 'cust017');

		    var materialListRequest = AssetManagement.customer.manager.MaterialManager.getMaterials(true);
		    this.addRequestToDataBaseQueue(materialListRequest, 'materials');
		    
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependendData of BaseExtConfPageController', ex);
		}
	},
	
	//does additional preparation steps
	performContextLevel2Requests: function() {
		try {
			var myModel = this.getViewModel();
		
			//if there is no banfheader for saving new banf items present, create one
			var banfHeader = myModel.get('banfHeader');
			
			if(!banfHeader || banfHeader.get('updFlag') === 'X') {
		    	banfHeader = Ext.create('AssetManagement.customer.model.bo.BanfHeader');
		    	
		    	//Bestellanforderungsart
		    	if(myModel.get('cust017'))
		    		banfHeader.set('bsart', myModel.get('cust017').get('bsart'));
		    	
		    	//Lieferantennummer
	    		banfHeader.set('lifnr', AssetManagement.customer.model.bo.UserInfo.getInstance().get('lifnr'));
		    	
				banfHeader.set('aufnr', order.get('aufnr'));
				banfHeader.set('mobileKey_ref', order.get('mobileKey'));
				
				myModel.set('banfHeader', banfHeader);
			}
			
			//prepare the operation
			var curOperation = myModel.get('operation');
			
			if(!curOperation) {
				var operations = myModel.get('order').get('operations');
			
				if(operations && operations.getCount() > 0) {
					myModel.set('operation', operations.getAt(0));
				}
			}
			
			//prepare the materials for ext. confs.
			var materials = myModel.get('materials'); 
			
			var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
			var extMaterials = funcPara.get('ext_conf_material'); 

			var extConfMaterials = Ext.create('Ext.data.Store', {
				model: 'AssetManagement.customer.model.bo.Material',
				autoLoad: false
			});

			if(extMaterials && extMaterials.getCount() > 0) {
				extMaterials.each(function(extMaterial) {
					var id = extMaterial.get('fieldname');
					var index = materials.findExact('id', id); 
					var material = materials.getAt(index); 
					
					extConfMaterials.add(material);
				}, this);
			}
			
			myModel.set('extConfMaterials', extConfMaterials);
			
			//prepare final conf flag
			curOperation = myModel.get('operation');
			if(curOperation) {
				var operationsTimeConfs = curOperation.get('timeConfs');
				
				if(operationsTimeConfs && operationsTimeConfs.getCount() > 0) {
					operationsTimeConfs.each(function(timeConf) {
						if(timeConf.get('aueru') === 'X') {
							myModel.set('hasFinalConf', true);
							return false;
						}
					}, this);
				}
			}
		    
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel2Requests of BaseExtConfPageController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			var curExtConf = this.getViewModel().get('extConf');
		
			//if no extConf is currently set, initialize a new one
			if(!curExtConf)
				this.initializeNewExtConf(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseExtConfPageController', ex);
		}
	},
	
	//will initialize a new ext conf with default values
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewExtConf: function(skipRefreshView) {
		try {
			if(skipRefreshView === true)
				skipRefreshView = true;
			else
				skipRefreshView = false;
		
			var myModel = this.getViewModel();
			var myView = this.getView();
			var banfHeader = myModel.get('banfHeader');
			var extConfMaterials = myModel.get('extConfMaterials');
			
			var ac = AssetManagement.customer.core.Core.getAppConfig();
			var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
			var cust017 = myModel.get('cust017');
		
			var newBanfPosition = Ext.create('AssetManagement.customer.model.bo.BanfPosition');
			newBanfPosition.set('menge', '1');
			
			//use a default material
			var defaultMaterial = null;
			
			if(extConfMaterials && extConfMaterials.getCount() > 0) {
				defaultMaterial = extConfMaterials.getAt(0);
			}
			
			if(defaultMaterial) {
				newBanfPosition.set('material', defaultMaterial);
				newBanfPosition.set('matnr', defaultMaterial.get('matnr'));
				newBanfPosition.set('meins', defaultMaterial.get('meins'));
			} else {
				newBanfPosition.set('meins', 'ST');
			}
			
	    	newBanfPosition.set('bedat', AssetManagement.customer.utils.DateTimeUtils.getCurrentDate());
	    	newBanfPosition.set('badat', AssetManagement.customer.utils.DateTimeUtils.getCurrentDate());
	    	newBanfPosition.set('werks', order.get('iwerk'));
	    	newBanfPosition.set('aufnr', order.get('aufnr'));
	    	
	    	if(cust017) {
		    	newBanfPosition.set('pstyp', cust017.get('pstyp'));			//Positionstyp
		    	newBanfPosition.set('ekorg', cust017.get('ekorg')); 		//Einkäuferorganisation
		    	newBanfPosition.set('ekgrp', cust017.get('ekgrp'));			//Einkäufergruppe
		    	newBanfPosition.set('knttp', cust017.get('knttp'));			//Kontierungstyp
	    	}
	    	
	    	//Einkäufergruppe
	    	newBanfPosition.set('afnam', ac.getUserId());					//Name des Anforderers
	    	newBanfPosition.set('sakto', funcPara.getValue1For('sakto')); 			//Sachkonto
	    	newBanfPosition.set('matkl', funcPara.getValue1For('matkl'));			//Warengruppe
	   													
	
			myModel.set('extConf', newBanfPosition);
			myModel.set('selectedMaterial', defaultMaterial);
			myModel.set('isEditMode', false);
			
			if(this.isCurrentlySelectedMaterialSmallPartsExpense()) {
				newBanfPosition.set('waers', this._DEFAULT_CURRENCY);
			}
			
			if(skipRefreshView === false)
				myView.refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewExtConf of BaseExtConfPageController', ex);
		}
	},

	/**
	 * calls save header, if there isn't already a saved header available, 
	 * if header is available save banf position gets called
	 */
	trySaveExtConf: function() {
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();

			var me = this;
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						this.getView().transferViewStateIntoModel();
						
						var banfHeader = this.getViewModel().get('banfHeader');
						
						var bandHeaderNeedsToBeSavedFirst = AssetManagement.customer.utils.StringUtils.isNullOrEmpty(banfHeader.get('mobileKey'));
						
						if(bandHeaderNeedsToBeSavedFirst) {
							var callback = function(banfHeader) {
								try {
									if(banfHeader) {
										//do not set back save is running, because a next step follows
										me.getViewModel().set('banfHeader', banfHeader); 
										me.saveBanfPosition();
									} else {
										var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingExtBanf'), false, 2);
										AssetManagement.customer.core.Core.getMainView().showNotification(notification);
										me._saveRunning = false;
										AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
									}
								} catch(ex) {
									AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveExtConf of BaseExtConfPageController', ex);
									me._saveRunning = false;
									AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								}
							};
							
							var eventId = AssetManagement.customer.manager.BanfManager.saveBanfHeader(banfHeader);
							
							if(eventId > 0) {
								AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
							} else {
								var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingExtBanf'), false, 2);
								AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							}
						} else {
							//do not set back save is running, because a next step follows
							this.saveBanfPosition();
						}
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveExtConf of BaseExtConfPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveExtConf of BaseExtConfPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	}, 
		
	/**
	 * saves a banf position to the database and refreshs the view after the banf item has been added to store
	 * do not call directly, always use trySaveExtConf
	 */
	saveBanfPosition: function() {
		try {
			var myModel = this.getViewModel();
			var banfHeader = myModel.get('banfHeader')
			var selectedMaterial = this.getViewModel().get('selectedMaterial');
			var banfPosition = myModel.get('extConf');
			
			var me = this; 
			var callback = function(success) {
				try {
					if(success === true) {
						var values = me.getView().getCurrentInputValues(); 
	
						//check if a final flagged timeconf has to be saved also
						if(values.finalConf === 'X' && !myModel.get('hasFinalConf')) {
							//do not set back save is running, because a next step follows
							me.saveEndConf(banfPosition); 
						} else {
							//add new Position to banfstore
							me.addBanfToListStore(banfPosition);
							me.initializeNewExtConf();
							/*if(selectedMaterial) {
								banfPosition.set('material', selectedMaterial);
							}*/
						
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedBanfExt'), true, 0);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							
							me._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingExtBanf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfPosition of BaseExtConfPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
	
			var eventId = AssetManagement.customer.manager.BanfManager.saveBanfPosition(banfPosition, banfHeader);
			
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingExtConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				this._saveRunning = false;
				AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveBanfPosition of BaseExtConfPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	/**
	 * saves an end confirmation for the choosen operation without worktime
	 */
	saveEndConf: function(banfPosition) {
		try {
			var myModel = this.getViewModel();
			var order = myModel.get('order')
			var operation = myModel.get('operation'); 
		
			var timeConf = Ext.create('AssetManagement.customer.model.bo.TimeConf', { order: order, operation: operation });
			
			timeConf.set('aueru', 'X'); 
			timeConf.set('isd', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
			timeConf.set('ied', AssetManagement.customer.utils.DateTimeUtils.getCurrentTime());
			timeconf.set('childKey', operation.get('childKey'));


			var me = this; 
			var callback = function(success) {
				try {
					if(success === true) {
						me.afterFinalFlagTimeConfSaved(timeConf);
					
						//add new Position to banfstore
						me.addBanfToListStore(banfPosition);
						me.initializeNewExtConf();
					
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedBanfExt'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingExtConf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveEndConf of BaseExtConfPageController', ex);
				} finally {
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			var eventId = AssetManagement.customer.manager.TimeConfManager.saveTimeConf(timeConf);
			
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingExtConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				this._saveRunning = false;
				AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveEndConf of BaseExtConfPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	 	}
	},
	
	//adds the timeConf to operations timeConfStore and sets the flag inside the model
	afterFinalFlagTimeConfSaved: function(timeConf) {
		try {
			var myModel = this.getViewModel();
		
			var operationsTimeConfs = myModel.get('operation').get('timeConfs');
		
			var temp = operationsTimeConfs.getById(timeConf.get('id'));
			
			if(!temp)
				operationsTimeConfs.insert(0, timeConf);
				
			myModel.set('hasFinalConf', true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterFinalFlagTimeConfSaved of BaseExtConfPageController', ex);
	 	}
	},

	/**
	 * checks if the data of the ext conf object is complete 
	 * returns null if the data is complete or an error message if data is missing
	 */
	checkInputValues: function(callback, callbackScope) {
		 try {	
	 		var retval = true; 
			var errorMessage = ''; 
			 
			//get values from textfields of view
			var values = this.getView().getCurrentInputValues(); 
			matnr = values.matnr;
			menge = values.menge;
			meins = values.meins;
			preis = values.preis;
			waers = values.waers;
			txz01 = values.txz01;
			
			var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
		 
			if(isNullOrEmpty(matnr)) {
				 errorMessage = Locale.getMsg('provideMaterial');
				 retval = false;
			} else if(isNullOrEmpty(meins)) {
				 errorMessage = Locale.getMsg('provideUnit');
				 retval = false;
			} else if(menge === null || menge === undefined || menge <= 0) {
				errorMessage = Locale.getMsg('provideValidQuantity');
				retval = false;
            }
			
            //checks for small parts data
			if(this.isCurrentlySelectedMaterialSmallPartsExpense()) {
				if(isNullOrEmpty(txz01)) {
					 errorMessage = Locale.getMsg('provideSmallPartsDescription');
					 retval = false;
				} else if(isNullOrEmpty(waers)) {
					 errorMessage = Locale.getMsg('provideCurrency');
					 retval = false;
	            } else if(preis === null || preis === undefined || preis < 0) {
					errorMessage = Locale.getMsg('provideValidPrice');
					retval = false;
	            }
			}
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			if(retval === false) {
				returnFunction.call(this);
			} else {
				//last do the material check - this means:
				//if the chosen material does no longer match the matnr from the input value, try to load the now connected material
				//if this material exist and has a diffrent unit, return false, inform the user and tell him, what the correct unit is
				//if the material could not be loaded, assume that it exists at the sap system and pass the check
				var me = this; 
				
				var values = this.getView().getCurrentInputValues(); 
				
				var unit = values.meins;
				var matnr = values.matnr; 
				
				var unitCheckFunction = function(material) {
					try {
						if(material) {
							if(material.get('meins').toUpperCase() !== unit.toUpperCase()) {
								errorMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('unitDoesNotMatchMaterial'), [ material.get('meins') ]);
								retval = false;
							}
						}
						
						me.getViewModel().set('selectedMaterial', material);
						
						returnFunction.call(this);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseExtConfPageController', ex);
						
						if(callback) {
							callback.call(callbackScope ? callbackScope : me, false);
						}
					}
				};
			
				//load material from database
				var eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr);
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, unitCheckFunction);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseExtConfPageController', ex);
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	/**
	 * Called after material has been selected in dropDown 
	 */
	onMaterialSelected: function(comboBox, newValue, oldValue) {
		try {
			var myModel = this.getViewModel();
			var myView = this.getView();
			var selectedObject = comboBox.getValue();
			var currentMaterial = this.getViewModel().get('selectedMaterial');
			
			if(selectedObject !== currentMaterial) {
				myModel.set('selectedMaterial', selectedObject);
				myView.fillMaterialSpecFields(selectedObject);
				
				var curExtConf = myModel.get('extConf');
				
				//clear small parts fields, if not required
				if(this.isCurrentlySelectedMaterialSmallPartsExpense()) {
					//default value
					myView.resetSmallPartsInputFields(this._DEFAULT_CURRENCY);
				} else {
					myView.resetSmallPartsInputFields();
				}
				
				myView.manageSmallPartsInputFieldsVisibility();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialSelected of BaseExtConfPageController', ex);
		}
	},
	
	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
		try {
			e.stopEvent(); 
		
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			if(record.get('updFlag') === 'I') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
			}
				
			this.rowMenu.onCreateContextMenu(options, record); 
			
			if(options.length > 0) {
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseExtConfPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
				this.deleteExtConf(record);
			}
			else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
				this.editExtConf(record); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseExtConfPageController', ex);
		}
	},
	///------ CONTEXT MENU END -------///
	
	deleteExtConf: function(extConf) {
		try {
			var currentExtConf = this.getViewModel().get('extConf');
			var deletingCurrentEditingExtConf = extConf.get('banfn') === currentExtConf.get('banfn') && extConf.get('bnfpo') === currentExtConf.get('bnfpo');
			
			var me = this; 
			var callback = function(success) {
				try {
					if(success === true) {
 						if(deletingCurrentEditingExtConf)
							me.initializeNewExtConf();
					
						me.deleteBanfFromListStore(extConf);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('extBanfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingExtBanf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteExtConf of BaseExtConfPageController', ex);
				}
			};
			
			var eventId = AssetManagement.customer.manager.BanfManager.deleteExtBanf(extConf);
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingExtBanf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteExtConf of BaseExtConfPageController', ex);
		}
	},
	
	editExtConf: function(extConf) {
		try {
			var myModel = this.getViewModel();
			myModel.set('extConf', extConf);
			myModel.set('selectedMaterial', extConf.get('material'));
			myModel.set('isEditMode', true);
			
			this.getView().refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editExtConf of BaseExtConfPageController', ex);
		}
	},	
	
	addBanfToListStore: function(banf) {
		try {
			var banfPositions = this.getViewModel().get('banfPositions'); 
			var temp = banfPositions.getById(banf.get('id'));
			
			if(temp) {
				var index = banfPositions.indexOf(temp);
				banfPositions.removeAt(index);
				banfPositions.insert(index, banf);
			} else {
				banfPositions.insert(0, banf);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addBanfToListStore of BaseExtConfPageController', ex);
		}
	},	

	deleteBanfFromListStore: function(banf) {
		try {
			var banfPositions = this.getViewModel().get('banfPositions'); 
			var temp = banfPositions.getById(banf.get('id'));
			
			if(temp)
				banfPositions.remove(banf);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConfFromListStore of BaseExtConfPageController', ex);
		}
	}
});