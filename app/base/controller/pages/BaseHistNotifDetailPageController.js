Ext.define('AssetManagement.base.controller.pages.BaseHistNotifDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
	config: {
		contextReadyLevel: 1
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {			
			var histNotif = null; 
			if(argumentsObject['histNotif'] && Ext.getClassName(argumentsObject['histNotif']) === 'AssetManagement.customer.model.bo.NotifHistory') {
				this.getViewModel().set('histNotif', argumentsObject['histNotif']);
				histNotif = argumentsObject['histNotif'];
			} else {
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.HistNotifDetailPageController'
			    });
			}
					  			
		    if(histNotif) {
		    	this.loadDependantData(histNotif)
			} else {
				this.waitForDataBaseQueue();
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseHistNotifDetailPageController', ex);
		    this.errorOccurred();
		}
	},
	
	loadDependantData:function(histNotif) {
		try {
		        var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLoc(histNotif.get("tplnr"), true);
				this.addRequestToDataBaseQueue(funcLocRequest, 'tpl');
					
				var equiRequest = AssetManagement.customer.manager.EquipmentManager.getEquipment(histNotif.get("equnr"), true);
				this.addRequestToDataBaseQueue(equiRequest, 'equi');
			
	            var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(histNotif.get("aufnr"), true);
				this.addRequestToDataBaseQueue(orderRequest, 'ord');
			
				this.waitForDataBaseQueue();
		} catch(ex)	{
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadDependantData of BaseHistNotifDetailPageController', ex);
		}
	}	
});