Ext.define('AssetManagement.base.controller.pages.BaseBanfEditPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
	requires: [
		'AssetManagement.customer.manager.OrderManager',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.manager.BanfManager',
		'AssetManagement.customer.manager.Cust_002Manager',
		'AssetManagement.customer.manager.Cust_001Manager',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],
	
	config: {
		contextReadyLevel: 2,
		rowMenu: null
	},
	
	//private
	_saveRunning: false,
	         
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;

				this.trySaveBanf();
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
				if(this._saveRunning === true)
					return;
				
				this.initializeNewBanf();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseBanfEditPageController', ex);
		}
	},
	
	//protected
	//@override
	onCreateOptionMenu: function() {
		this._optionMenuItems = new Array();
		try {
			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM);
			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseBanfEditPageController', ex);
		}		
	},
	
	//private
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			if(argumentsObject) {
				 if(Ext.getClassName(argumentsObject['material']) === 'AssetManagement.customer.model.bo.MatStock') {
					 var matStock = argumentsObject['material'];
					 
					 this.initializeNewBanf(true, matStock.get('material'));
					 var initBanf = this.getViewModel().get('banf');
					 
					 initBanf.set('werks', matStock.get('werks'));
					 initBanf.set('lgort', matStock.get('lgort'));
				 } else if (Ext.getClassName(argumentsObject['banf']) === 'AssetManagement.customer.model.bo.BanfItem') {
				     var banf = argumentsObject['banf'];
				     this.getViewModel().set('isEditMode', true);
				     this.getViewModel().set('banf', banf);
				 }
			}
						
			var cust001Request = AssetManagement.customer.manager.Cust_001Manager.getCust_001(AssetManagement.customer.core.Core.getAppConfig().getUserId().toUpperCase(), true);
			this.addRequestToDataBaseQueue(cust001Request, "cust001");
		
			this.waitForDataBaseQueue();
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseBanfEditPageController', ex);
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		this.callParent();
		
		try {
			if(this._contextLevel === 1) {
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseBanfEditPageController', ex);
		}
	},
	
	//private			
	performContextLevel1Requests: function() {
		try {
			this.loadListData();
			this.loadStorageLocations();
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseBanfEditPageController', ex);
		}
	},
		
	loadListData: function() {
		try {
			var banfStoreRequest = AssetManagement.customer.manager.BanfManager.getBanfs(true);
			this.addRequestToDataBaseQueue(banfStoreRequest, 'banfStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseBanfEditPageController', ex);
		}
	},
	
	loadStorageLocations: function() {
		try {
			var storLocsRequest = AssetManagement.customer.manager.Cust_002Manager.getCust_002s(true);
			this.addRequestToDataBaseQueue(storLocsRequest, "storLocs");
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadStorageLocations of BaseBanfEditPageController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			if(!this.getViewModel().get('banf'))
				this.initializeNewBanf(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseBanfEditPageController', ex);
		}
	},
	
	//will initialize a new banf using the provided material to gain default values and transfer it into the model
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewBanf: function(skipRefreshView, material) {
		try {
			if(skipRefreshView === true)
				skipRefreshView = true;
			else
				skipRefreshView = false;
		
			var myModel = this.getViewModel();
		
			var newBanfItem = Ext.create('AssetManagement.customer.model.bo.BanfItem');
			
			newBanfItem.set('lfdat', AssetManagement.customer.utils.DateTimeUtils.getCurrentDate());
			newBanfItem.set('menge', '1');
			
			//set a default werk - using first entry of the storage locations
			//this will work, as long as only one plant is customized
			var storLocs = myModel.get('storLocs');
			
			if(storLocs && storLocs.getCount() > 0) {
				newBanfItem.set('werks', storLocs.getAt(0).get('werks'));
			}
			
			if(material) {
				newBanfItem.set('matnr', material.get('matnr'));
				newBanfItem.set('meins', material.get('meins'));
				newBanfItem.set('material', material);
			} else {
				newBanfItem.set('meins', 'ST');
			}
		
			myModel.set('banf', newBanfItem);
			myModel.set('selectedMaterial', null);
			myModel.set('isEditMode', false);
			
			if(skipRefreshView === false)
				this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewBanf of BaseBanfEditPageController', ex);
		}
	},
	
	trySaveBanf: function() {
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
			var me = this;
			var saveAction = function(checkWasSuccessfull) {
				try {
				    if (checkWasSuccessfull === true) {
						this.getView().transferViewStateIntoModel();
					
						//get banf to save
						var myModel = this.getViewModel();
						var banf = myModel.get('banf');
						
						banf.set('meins', banf.get('meins').toUpperCase());
						
						var selectedMaterial = myModel.get('selectedMaterial');
						banf.set('material', selectedMaterial ? selectedMaterial : null);
						banf.set('reswk', banf.get('werks'));
						
						var callback = function(success) {
							try {
								if(success) {
									me.addBanfToListStore(banf);
									me.initializeNewBanf(); 
									
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('banfSaved'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingBanf'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveBanf of BaseBanfEditPageController', ex);
							} finally {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							}
						};
						
						var eventId = AssetManagement.customer.manager.BanfManager.saveBanf(banf);
						
						if(eventId > 0) {
							AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
						} else {
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingBanf'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
							this._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						}
					} else {
						this._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveBanf of BaseBanfEditPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveBanf of BaseBanfEditPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	//performs a set of checks, returning the overall result by the provided callback
	checkInputValues: function(callback, callbackScope) {
		try {
			var retval = true;
			var errorMessage = '';
					
			var values = this.getView().getCurrentInputValues(); 
		
			var lfdat = values.lfdat;
			var matnr = values.matnr;
			var unit = values.meins;
			var plant = values.werks;
			var storLoc = values.lgort;	
		    var quantity = values.menge;
		    
		    var formofaddr = values.formofaddr;
			var name = values.name;	
		    var name2 = values.name2;
		    var street = values.street;
		    var houseNo = values.houseNo;
		    var postalCode = values.postalCode;
		    var city = values.city;
		    var country = values.country;
		    
			var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
			
			if(AssetManagement.customer.utils.DateTimeUtils.isBeforeToday(lfdat)) {
				//delivery date is in the past
				errorMessage = Locale.getMsg('deliveryDateInPast');
				retval = false;
			} else if(shortHand(matnr)) {
				//provide material
				errorMessage = Locale.getMsg('provideMaterial');
				retval = false;
			} else if(shortHand(unit)) {
				//provide unit
				errorMessage = Locale.getMsg('provideUnit');
				retval = false;
			}  else if(shortHand(plant)) {
				//provide plant
				errorMessage = Locale.getMsg('providePlant');
				retval = false;
			} else if(shortHand(storLoc)) {
				//provide storloc
				errorMessage = Locale.getMsg('provideStorageLocation');
				retval = false;
			} else if(quantity === null || quantity === undefined || quantity <= 0) {
				errorMessage = Locale.getMsg('provideValidQuantity');
				retval = false;
			}
			
			//check for incomplete address data, if any has been provided
			if(retval === true) {
				var anyDataProvided = !shortHand(formofaddr) || !shortHand(name) || !shortHand(name2) || !shortHand(street)
										|| !shortHand(houseNo) || !shortHand(postalCode) || !shortHand(city) || !shortHand(country);
				
				if(anyDataProvided === true) {
					//check for missing required data
					if(shortHand(name) && shortHand(name2) || shortHand(street) || shortHand(houseNo) || shortHand(postalCode) || shortHand(city)) {
						errorMessage = Locale.getMsg('incompleteAddressData');
						retval = false;
					}
				}
			}
          
			var me = this;
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			if(retval === false) {
				returnFunction.call(this);
			} else {
				//last do the material check - this means:
				//if the chosen material does no longer match the matnr from the input value, try to load the now connected material
				//if this material exist and has a diffrent unit, return false, inform the user and tell him, what the correct unit is
				//if the material could not be loaded, assume that it exists at the sap system and pass the check
				
				var selectedMaterial = this.getViewModel().get('selectedMaterial');
				
				var unitCheckFunction = function(material) {
					try {
						if(material) {
							if(material.get('meins').toUpperCase() !== unit.toUpperCase()) {
								errorMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('unitDoesNotMatchMaterial'), [ material.get('meins') ]);
								retval = false;
							}
						}
						
						me.getViewModel().set('selectedMaterial', material);
						
						returnFunction.call(this);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseBanfEditPageController', ex);
						
						if(callback) {
							callback.call(callbackScope ? callbackScope : me, false);
						}
					}
				};
				
				if(selectedMaterial && selectedMaterial.get('matnr') === matnr) {
					unitCheckFunction.call(this, selectedMaterial);
				} else {
					//load material from database
					var eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr);
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, unitCheckFunction);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseBanfEditPageController', ex);
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	addBanfToListStore: function(banf) {
		try {
			var banfStore = this.getViewModel().get('banfStore');
			var temp = banfStore.getById(banf.get('id'));
			
			if(temp) {
				var index = banfStore.indexOf(temp);
				banfStore.removeAt(index);
				banfStore.insert(index, banf);
			} else {
				banfStore.insert(0, banf);
			}			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addBanfToListStore of BaseBanfEditPageController', ex);
		}
	}, 

	deleteBanfFromListStore: function(banf) {
		try {
			var temp = this.getViewModel().get('banfStore').getById(banf.get('id'));
			
			if(temp)
				this.getViewModel().get('banfStore').remove(banf);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteBanfFromListStore of BaseBanfEditPageController', ex);
		}
	},
	
	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
		try {
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			if(record.get('updFlag') === 'X') {	
				this.rowMenu.setDisabled(true); 
			} else {
				this.rowMenu.setDisabled(false); 
			}
			
			this.rowMenu.onCreateContextMenu([AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE, AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT], record);
			
			e.stopEvent();
			this.rowMenu.showAt(e.getXY());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseBanfEditPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
				this.onDeleteClick(record);
			}
			else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
				this.onEditClick(record); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseBanfEditPageController', ex);
		}
	},
	///------ CONTEXT MENU END -------///
	
	//on delete item tapped
	onDeleteClick: function(banf) {
		try {
			var currentBanf = this.getViewModel().get('banf');
			var deletingCurrentEditingBanf = banf.get('banfn') === currentBanf.get('banfn') && banf.get('bnfpo') === currentBanf.get('bnfpo');
			
			var me = this; 
			var callback = function(success) {
				try {
					if(success === true) {
 						if(deletingCurrentEditingBanf)
 							me.initializeNewBanf();
					
						me.deleteBanfFromListStore(banf);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('banfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingBanf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteClick of BaseBanfEditPageController', ex);
				}
			};
			
			var eventId = AssetManagement.customer.manager.BanfManager.deleteBanf(banf, false);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteClick of BaseBanfEditPageController', ex);
		}
	},
	
	onEditClick: function( banf ) {
		try {
			var myModel = this.getViewModel();
			myModel.set('banf', banf);
			myModel.set('isEditMode', true);
			
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEditClick of BaseBanfEditPageController', ex);
		}
	},
	
	onMaterialSearchButtonClick: function() {
		try {
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
		
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.MATERIALPICKER_DIALOG, {useComponents: false, useBillOfMaterial:false });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialSearchButtonClick of BaseBanfEditPageController', ex);
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	onMaterialSelected: function(materialInfoObject) {
	    try {
            //the slected object can be a direct material master, but also a component, matstock etc.
	        //in case a mat stock has been selected, the material master has to be extracted first
	        var materialMaster = materialInfoObject.get('material') ? materialInfoObject.get('material') : materialInfoObject;

	        this.getViewModel().set('selectedMaterial', materialMaster);
	        this.getView().fillMaterialSpecFields(materialInfoObject);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialSelected of BaseBanfEditPageController', ex);
		}
	}, 
	
	onPlantTextChanged: function(field, e, eOpts ) {
		try {
			this.getView().fillStorageLocationComboBox(field.getValue());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPlantTextChanged of BaseBanfEditPageController', ex);
		}
	}
});