Ext.define('AssetManagement.base.controller.pages.BaseObjectStructurePageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
    requires: [
        'AssetManagement.customer.manager.StructureManager'
    ],
    
    config: {
		contextReadyLevel: 1
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
	    try {
	        var myModel = this.getViewModel();
			var equipment = null;
			var funcLoc = null;
			if(Ext.getClassName(argumentsObject['equipment']) === 'AssetManagement.customer.model.bo.Equipment')
			{
				equipment = argumentsObject['equipment'];
				myModel.set('equipment', equipment); 
			} 
			if(Ext.getClassName(argumentsObject['funcLoc']) === 'AssetManagement.customer.model.bo.FuncLoc')
			{
				funcLoc = argumentsObject['funcLoc'];
				myModel.set('funcLoc', funcLoc); 
			}
			if (!equipment && !funcLoc)
			{
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.ObjectStructurePageController'
			    });
			}
			this.loadObjectStructureStore();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseObjectStructurePageController', ex);
		    this.errorOccurred();
 		}
	},
	
	loadObjectStructureStore: function()
	{
		try {
	
			if(this.getViewModel().get('equipment'))
			{
				var objStructureStoreRequest = AssetManagement.customer.manager.StructureManager.getStructure(this.getViewModel().get('equipment'));
				this.addRequestToDataBaseQueue(objStructureStoreRequest, 'objStructureStore');
			}
			if(this.getViewModel().get('funcLoc'))
			{
				var objStructureStoreRequest = AssetManagement.customer.manager.StructureManager.getStructure(this.getViewModel().get('funcLoc'));
				this.addRequestToDataBaseQueue(objStructureStoreRequest, 'objStructureStore');
			}
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadObjectStructureStore of BaseObjectStructurePageController', ex);
		}
	},
	onRecordClicked: function (viewtable, td, cellIndex, record, tr, rowIndex, e, eOpts) {
	    var obj = record.get('object');
	    if (obj) {
	        if (Ext.getClassName(obj) === 'AssetManagement.customer.model.bo.Equipment') {
	            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: obj.get('equnr') });
	        }
	        if (Ext.getClassName(obj) === 'AssetManagement.customer.model.bo.FuncLoc') {
	            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: obj.get('tplnr') });
	        }
	    }
	}
});