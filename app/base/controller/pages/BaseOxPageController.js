Ext.define('AssetManagement.base.controller.pages.BaseOxPageController', {
	extend: 'AssetManagement.customer.controller.DataBaseToContextController',

    //abstract class for page controllers
	//one primary task is to (pre)load a pages data context

	config: {
		isAsynchronous: false,
		contextReadyLevel: 0,
		currentArguments: null,
		pageRequestCallback: null,
		pageRequestCallbackScope: null,
		optionMenuItems: null,
		hasUnsavedChanges: false
	},

    _searchable: false,

	//protected
	getClassName: function() {
    	var retval = 'OxPageController';

    	try {
    		var className = Ext.getClassName(this);

    		if(AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
    			var splitted = className.split('.');
    			retval = splitted[splitted.length - 1];
    		} else {
    			retval = className;
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseOxPageController', ex);
    	}

    	return retval;
	},

	//public
	//requests the page, triggers data loading and refreshes the view before the provided request callback will be called
	requestPage: function(pageRequestCallback, pageRequestCallbackScope, argumentsObject) {
		try {
			this._pageRequestCallback = pageRequestCallback;
			this._pageRequestCallbackScope = pageRequestCallbackScope;
			this._currentArguments = argumentsObject;

			if(!this.getIsAsynchronous()) {
				//do not use update to avoid it's defering
				this.refreshData(true);
			} else {
				this.signalizeReload(argumentsObject);
				this.callPageRequestCallback(true);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestPage of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},

	//public
	//only to use for asynchronous pages
	signalizeReload: function(argumentsObject) {
		try {
			this._currentArguments = argumentsObject;

			this.rebuildOptionsMenu(true);

			this.getView().resetViewState();

			//begin loading data when applications switchs into idle state
			Ext.defer(this.refreshData, 1, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside signalizeReload of ' + this.getClassName(), ex);
		}
	},

	//public
	//checks if the there is unsaved data or input for the page
	hasUnsavedChanges: function() {
	    return this._hasUnsavedChanges;
	},

	//may be overriden by derivates
	getDataLoadingErrorMessage: function() {
		var retval = '';

		try {
		    retval = this.getContextLoadingErrorMessage();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDataLoadingErrorMessage of ' + this.getClassName(), ex);
		}

		return retval;
	},

	//protected
	update: function(doRefreshViewOnly) {
		try {
			AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);

			if(doRefreshViewOnly === true) {
				Ext.defer(this.refreshView, 100, this);
			} else {
				Ext.defer(this.refreshData, 100, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside update of ' + this.getClassName(), ex);
		}
	},

	//may be overridden by derivates
	//default behavior is to rebuild the view, so that label contents will be set corresponding to the current locale
	onLocaleChanged: function() {
		try {
			if(this.rowMenu)
				this.rowMenu = null;

			this.getView().rebuildPage();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLocaleChanged of ' + this.getClassName(), ex);
		}
	},

	//private
	rebuildOptionsMenu: function(bypassActiveCheck) {
		try {
			this.onCreateOptionMenu();

			if (this._optionMenuItems && this._optionMenuItems.length > 0) {
			    if (this._searchable !== true) {
			        Ext.Array.remove(this._optionMenuItems, AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
			        Ext.Array.remove(this._optionMenuItems, AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);
			    } else {
			        if (!Ext.Array.contains(this._optionMenuItems, AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH))
			            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH)

			        if (!Ext.Array.contains(this._optionMenuItems, AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH))
			            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH)
			    }
			}

			//extention for asychronous pages
			//only adjust the options menu, if this page is currently active one
			if(!bypassActiveCheck && this.getIsAsynchronous() && this.getView() !== AssetManagement.customer.core.Core.getCurrentPage()) {
				return;
			} else {
				this.adjustOptionsMenu();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside rebuildOptionsMenu of ' + this.getClassName(), ex);
		}
	},

	//private
	adjustOptionsMenu: function() {
		try {
		    AssetManagement.customer.core.Core.setMainToolbarOptions(this._optionMenuItems);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adjustOptionsMenu of ' + this.getClassName(), ex);
		}
	},
	//protected
	//may be implemented by derivates
	onCreateOptionMenu: function() {
	},

	//protected
	//may be implemented by derivates
	onOptionMenuItemSelected: function(optionID) {
	},

	//private
	//will reload all data and refresh the view
	refreshData: function(triggerPageRequestCallbackWhenDone) {
		try {
			if(!triggerPageRequestCallbackWhenDone) {
				this._pageRequestCallback = null;
				this._pageRequestCallbackScope = null;
			}

			this.getViewModel().resetData();

			//set to zero
			this._contextLevel = 0;

			if(this._contextReadyLevel === 0) {
				this.contextReady();
			} else {
				//start the loading cycle
				this.startBuildingPageContext(this.getCurrentArguments());
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
		}
	},

	//protected
	//may be implemented by derivates
	startBuildingPageContext: function(argumentsObject) {
	},

	//protected
	//may be implemented by derivates
	checkBaseContext: function() {
		return true;
	},

	//private
	contextReady: function() {
		try {
			this.dataReady();
			this.refreshView();
			this.pageReady();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside contextReady of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},

	//protected
	dataBaseQueueComplete: function() {
		this.callParent();

		try {
			if(this._contextLevel === this._contextReadyLevel || this.getViewModel().getInitialized() === true) {
				this._contextLevel = this._contextReadyLevel;
				this.getViewModel().setInitialized(true);

				this.afterDataBaseQueueComplete();

				this.contextReady();
			} else if(this._contextLevel === -1) {
				this.errorOccurred();
			} else {
				this.afterDataBaseQueueComplete();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},

	//protected
	//may be implemented by derivates
	//use this callback to drop database requests of the current context level
	afterDataBaseQueueComplete: function() {
	},

	//private
	dataReady: function() {
		try {
			this.beforeDataReady();

			this.onDataReady();
			AssetManagement.customer.helper.OxLogger.logMessage('[OXPAGE] Data is ready!');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataReady of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},

	//protected
	//may be implemented by derivates
	//can be used to do data initialization, which depends on loaded data
	beforeDataReady: function() {
	},

	//protected
	//may be used by derivates
	onDataReady: function() {
	},

	//private
	refreshView: function() {
		try {
			this.beforeRefreshView();

			//suspend layouts before refreshing ui components - this gains a lot of performance
			Ext.suspendLayouts();

			this.getView().refreshView();

			//resume layouts - all changes will be rendered in one operation
			Ext.resumeLayouts(true);

			AssetManagement.customer.helper.OxLogger.logMessage('[OXPAGE] View has been updated!');

            //rebuilding options menu needs the page's title to be rendered first
			this.rebuildOptionsMenu();

			AssetManagement.customer.helper.OxLogger.logMessage('[OXPAGE] Options Menu is ready!');

			this.afterRefreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshView of ' + this.getClassName(), ex);
			this.errorOccurred();
		}
	},

	//protected
	//may be used by derivates
	beforeRefreshView: function() {
	},

	//protected
	//may be used by derivates
	afterRefreshView: function() {
	},

	//private
	pageReady: function() {
	    try {
		    this.onPageReady();

		    if(this._pageRequestCallback) {
		    	this.callPageRequestCallback(true);
		    } else {
		    	AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		    }
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside pageReady of ' + this.getClassName(), ex);
		}
	},

	//protected
	//may be used by derivates
	onPageReady: function() {
	},

	//private
	callPageRequestCallback: function(requestSuccessFullyProcessed) {
	    try {
		    if(this._pageRequestCallback) {
			   this._pageRequestCallback.call(this._pageRequestCallbackScope ? this._pageRequestCallbackScope : this, requestSuccessFullyProcessed);
		    }
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside callPageRequestCallback of ' + this.getClassName(), ex);
		}
	},

	//private
	errorOccurred: function() {
	    try {
	    	this.callPageRequestCallback(false);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside errorOccurred of ' + this.getClassName(), ex);
		}
	},

    //sets the search Option for the page
	setSearchable: function (isSearchable) {
	    try {
	        if (isSearchable) {
	            this._searchable = true;
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setSearchable of ' + this.getClassName(), ex);
	    }
	}
});