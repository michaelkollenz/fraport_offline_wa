Ext.define('AssetManagement.base.controller.pages.BaseGoogleMapsPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.modules.googleMaps.GoogleMapsHelper',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.manager.AddressManager',
		'AssetManagement.customer.helper.OxLogger'
	],

	config: {
		contextReadyLevel: 1
	},
	
	_map: null,
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SHOW_MY_MAPS_POSITION) {
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_OWN_POSITION
				});
				
				this.executeMapsRequest(request);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseGoogleMapsPageController', ex);
		}
	},
		
	//private
	//@override
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SHOW_MY_MAPS_POSITION ];
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseGoogleMapsPageController', ex);
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			//extract the data from the argumentsobject and generate a maps request
			var mapsRequest = this.getRequestFromArguments(argumentsObject);
			
			var executeRequestCallback = function() {
				try {
					//its mandatory to let the map be rendered before the request will be executed - else it results in graphical bugs
					this.contextReady();
					this.executeMapsRequest(mapsRequest);
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseGoogleMapsPageController', ex);
				}
			};
			
			//if the page has not yet initialized it's map, this has to be done first
			if(!this._map) {
				var mapAssignmentCallback = function(map, errorType) {
					try {
						if(map) {
							this._map = map;
							this._contextLevel = 1;
							
							//its mandatory to let the map be rendered before the request will be executed - else it results in graphical bugs
							executeRequestCallback.call(this);
						} else {
							this.onMapsRequestFailed(errorType);
						}
					} catch (ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseGoogleMapsPageController', ex);
					}
				};
				
				AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.getInstance().getMapsObjectForDiv(this.getView().getMapsDiv(), mapAssignmentCallback, this);
			} else {
				this._contextLevel = 1;
				executeRequestCallback.call(this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseGoogleMapsPageController', ex);
		}
	},
	
	getRequestFromArguments: function(argumentsObject) {
		var retval = null;
	
		try {
			if(argumentsObject && argumentsObject.request) {
				retval = argumentsObject.request;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRequestFromArguments of BaseGoogleMapsPageController', ex);
		}
		
		return retval;
	},
	
	executeMapsRequest: function(mapsRequest) {
		try {
			if(!mapsRequest) {
				this.onMapsRequestSuccessfullyExecuted();
			}
		
			var requestCallback = function(success, errorType) {
				try {
					if(success === true) {
						this.onMapsRequestSuccessfullyExecuted();
					} else {
						this.onMapsRequestFailed(errorType);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside executeMapsRequest of BaseGoogleMapsPageController', ex);
				}
			};
			
			AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.getInstance().executeMapsRequest(mapsRequest, this._map, requestCallback, this);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside executeMapsRequest of BaseGoogleMapsPageController', ex);
		}
	},
	
	onMapsRequestSuccessfullyExecuted: function() {
		try {
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMapsRequestSuccessfullyExecuted of BaseGoogleMapsPageController', ex);
		}
	},
	
	onMapsRequestFailed: function(errorType) {
		try {
			var errorMessage = '';
		
			//examine what error has occured
			switch(errorType) {
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.BROWSER_OFFLINE:
					errorMessage = Locale.getMsg('browserIsOffline');
					break;
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.API_NOT_RESPONDING:
					errorMessage = Locale.getMsg('googleMapsNotResponding');
					break;
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.GEO_NOT_SUP:
					errorMessage = Locale.getMsg('browserDoesNotSupportGPSTracking');
					break;
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.GEO_DECLINED:
					errorMessage = Locale.getMsg('youDidNotAcceptGPS');
					break;
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.INVALID_REQUEST:
					errorMessage = Locale.getMsg('invalidMapsRequest');
					break;
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.INCOMPLETE_REQUEST:
					errorMessage = Locale.getMsg('incompleteMapsRequest');
					break;
				case AssetManagement.customer.modules.googleMaps.GoogleMapsHelper.ERROR_TYPES.GEO_CODING_FAILED:
					errorMessage = Locale.getMsg('addressResolutionFailed');
					break;
				default:
					errorMessage = Locale.getMsg('generalMapsErrorOccured');
					break;
			}
			
			var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
			AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			
			//if the initial request failed, undo the navigation to the maps page
			if(this._contextLevel === 0) {
				AssetManagement.customer.core.Core.navigateBack();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMapsRequestFailed of BaseGoogleMapsPageController', ex);
		}
	},
	
	//will adjust the map object, so it will fit the new size
	adjustMapToNewSize: function(newWidth, newHeight) {
		try {
			google.maps.event.trigger(this._map.getMap(), 'resize');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside adjustMapToNewSize of BaseGoogleMapsPageController', ex);
		}
	}
});