Ext.define('AssetManagement.base.controller.pages.BaseCustomerDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.manager.CustomerManager',
		'AssetManagement.customer.manager.AddressManager',
		'AssetManagement.customer.helper.OxLogger'
	],

	config: {
		contextReadyLevel: 1
	},
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseCustomerDetailPageController', ex);
		}
	},
		
	//private
	//@override
	onCreateOptionMenu: function() {
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var kunnr = '';
		
			if(argumentsObject['kunnr'] && typeof argumentsObject['kunnr'] === 'string') {
				kunnr = argumentsObject['kunnr'];
			} else if(argumentsObject['customer'] && Ext.getClassName(argumentsObject['customer']) === 'AssetManagement.customer.model.bo.Customer') {
				this.getViewModel().set('customer', argumentsObject['customer']);
			} else {
				throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.pages.CustomerDetailPageController'
				});
			}
			
			var kunnrPassed = kunnr !== '';
			
			if(kunnrPassed) {
				this.getCustomer(kunnr);
			} else {
				this.waitForDataBaseQueue();
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseCustomerDetailPageController', ex);
		    this.errorOccurred();
		}
	},
	
	getCustomer: function(kunnr) {
		try {
			var customerRequest = AssetManagement.customer.manager.CustomerManager.getCustomer(kunnr, true);
			this.addRequestToDataBaseQueue(customerRequest, 'customer');
				
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCustomer of BaseCustomerDetailPageController', ex)
		}
	},
	
	openCustomerAddress: function() {
		try {
			var customer = this.getViewModel().get('customer');
					
			if(customer && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('city')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customer.get('postlcod1'))) {
				var address = AssetManagement.customer.manager.AddressManager.mapCustomerIntoAddress(customer);
				
				if(address)
					//AssetManagement.customer.helper.NetworkHelper.openAddress(address);
					var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
						type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
					});

					//generate the hint
					hint = AssetManagement.customer.utils.StringUtils.trimStart(customer.get('kunnr'), '0');

					request.addAddressWithHint(address, hint);

					AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openCustomerAddress of BaseCustomerDetailPageController', ex);
		}
	}
});