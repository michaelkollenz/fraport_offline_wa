Ext.define('AssetManagement.base.controller.pages.BaseSDOrderDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

       
    requires: [
     	'AssetManagement.customer.controller.ToolbarController',
    	'AssetManagement.customer.utils.StringUtils',
    	'AssetManagement.customer.manager.AddressManager',
    	'AssetManagement.customer.manager.SDOrderManager',
    	'AssetManagement.customer.manager.SDOrderItemManager',
		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.model.helper.ReturnMessage'
    ],

    config: {
    	contextReadyLevel: 1
    },
    
    _deleteRunning: false,
       
    //public
    onOptionMenuItemSelected: function(optionID) {
    	try {
	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_DELETE) {
				this.onDeleteSDOrderClicked();
			}
		} catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseSDOrderDetailPageController', ex);
		}
    },
       
    //@override
    onCreateOptionMenu: function() {
    	this._optionMenuItems = new Array();
    
    	try {
    		if(this.getViewModel().get('sdOrder').get('updFlag') === 'I')
    			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_DELETE);
    	} catch (ex) {
     	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseSDOrderDetailPageController', ex);
 		}
    },
       
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
            //extract parameters, if any given
            //if vbeln is passed, load the sd order
            //else proceed first database query
            var vbeln = '';
            
            if(typeof argumentsObject['vbeln'] === 'string') {
            	vbeln = argumentsObject['vbeln'];
            } else if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.SDOrder') {
                   this.getViewModel().set('sdOrder', argumentsObject['sdOrder']);
            } else {
                   throw Ext.create('AssetManagement.customer.utils.OxException', {
                          message: 'Insufficient parameters',
                          method: 'startBuildingPageContext',
                          clazz: 'AssetManagement.customer.controller.pages.SDOrderDetailPageController'
                   });
            }
            
            var vbelnPassed = vbeln !== '';
            
            if(vbelnPassed) {
            	this.getSDOrder(vbeln);
            } 
            
           	this.waitForDataBaseQueue();
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseSDOrderDetailPageController', ex);
             this.errorOccurred();
         }
	},

	getSDOrder: function(vbeln) {
		try {
            var sdOrderRequest = AssetManagement.customer.manager.SDOrderManager.getSDOrder(vbeln, true);
            this.addRequestToDataBaseQueue(sdOrderRequest, 'sdOrder');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getSDOrder of BaseSDOrderDetailPageController', ex);
        }
	},

    //@override
    getContextLoadingErrorMessage: function() {
		return Locale.getMsg('errorLoadingSDOrderContext');
    },
       
    navigateToOrder: function() {
    	try {
            if(this.getViewModel().get('sdOrder').get('order')) {
            	AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: this.getViewModel().get('sdOrder').get('aufnr') });
            } else {
            	var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('orderNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch(ex) {
        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToOrder of BaseSDOrderDetailPageController', ex);
        }
    },
       
    navigateToNotif: function() {
    	try {
    		if(this.getViewModel().get('sdOrder').get('notif')) {
    			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_DETAILS, { qmnum: this.getViewModel().get('sdOrder').get('qmnum') });
            } else {
            	var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
         } catch(ex) {
        	 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToNotif of BaseSDOrderDetailPageController', ex);
         }
    },
    
    openDeliveryAddress: function() {
		try {
			var sdOrder = this.getViewModel().get('sdOrder');
					
			if(sdOrder && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('city')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(sdOrder.get('postalCode1'))) {
				var address = AssetManagement.customer.manager.AddressManager.mapSDOrderIntoAddress(sdOrder);
				
				if(address)
					AssetManagement.customer.helper.NetworkHelper.openAddress(address);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openDeliveryAddress of BaseSDOrderDetailPageController', ex);
		}
	},
	
	onDeleteSDOrderClicked: function() {
		try {
			//ask the user, if he really want's to delete this sd order
			var callback = function(confirmed) {
				try {
					if(confirmed === true) {						
						this.tryDeleteSDOrder();
					}
				} catch(ex) {
		        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteSDOrderClicked of BaseSDOrderDetailPageController', ex);
		       	}    				
			};
			
			var dialogArgs = {
			    icon: 'resources/icons/trash.png',
				message: Locale.getMsg('deleteSDOrderQuestion'),
				callback: callback,
				scope: this
			};
			
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDeleteSDOrderClicked of BaseSDOrderDetailPageController', ex);
		}
	},
    
    tryDeleteSDOrder: function() {
    	try {
			if(this._deleteRunning === true) {
				return;
			}
			
			this._deleteRunning = true;
    	
    		var sdOrder = this.getViewModel().get('sdOrder');
    		
    		if(sdOrder) {
    			AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
    		
    			var me = this;
    			
				var callback = function(success) {
					try {
						if(success === true) {
							AssetManagement.customer.core.Core.navigateBack();
							
							Ext.defer(function() { 
									me._deleteRunning = false;
							}, 100, me);
							
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sdOrderDeleted'), false, 0);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						} else {
							me._deleteRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingSDOrder'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
					} catch (ex) {
						me._deleteRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteSDOrder of BaseSDOrderDetailPageController', ex);
					}
				};
				
				var deleteEvent = AssetManagement.customer.manager.SDOrderManager.deleteSDOrder(sdOrder);
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
    		} else {
    			this._deleteRunning = false;
    			var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingSDOrder'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
    		}
         } catch(ex) {
        	 this._deleteRunning = false;
        	 AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        	 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside tryDeleteSDOrder of BaseSDOrderDetailPageController', ex);
         }
    },
       
    ///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenuSDOrderItemList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
   		try {
   			e.stopEvent(); 
   			
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			if(record.get('updFlag') === 'I') {
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}
				
			this.rowMenu.onCreateContextMenu(options, record); 
			
			if(options.length > 0) {
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuSDOrderItemList of BaseSDOrderDetailPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
       try {
    	   if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
    		   this.deleteSDOrderItem(record);
    	   } else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
    		   this.editSDOrderItem(record);
		   }
       } catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseSDOrderDetailPageController', ex);
       }
	},
	///------ CONTEXT MENU END -------///
	
	editSDOrderItem: function(sdOrderItem) {
       try {
    	   if(sdOrderItem) {
    		   var dialogArguments = {
    				sdOrder: this.getViewModel().get('sdOrder'),
    				sdOrderItem: sdOrderItem
    		   };

    		   AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SDORDER_ITEM_DIALOG, dialogArguments);
    	   }
       } catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItem of BaseSDOrderDetailPageController', ex);
       }
	},

	deleteSDOrderItem: function(sdOrderItem) {
       try {
    	   if(sdOrderItem) {
       			//check if this is the last item, it may not be deleted. Empty SDOrders do not make sense
     		    if(this.getViewModel().get('sdOrder').get('items').getCount() === 1) {
     		    	var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('mayNotDeleteLastSDOrderItem'), false, 2);
     		    	AssetManagement.customer.core.Core.getMainView().showNotification(notification);
     		    	
     		    	return;
    		    }
    	   
				AssetManagement.customer.controller.ClientStateController.enterLoadingState();
				
				me = this;
				var callback = function(success) {
					try {
						if(success === true) {
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sdOrderItemDeleted'), true, 0);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						} else {
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingSDOrderItem'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
						
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItem of BaseSDOrderDetailPageController', ex)
					} finally {
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				};
				
				var eventId = AssetManagement.customer.manager.SDOrderItemManager.deleteSDOrderItem(sdOrderItem);
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
    	   }
       } catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItem of BaseSDOrderDetailPageController', ex);
       }
	},
	   	
	onAddSDOrderItemClick: function() {
   		try {
   			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SDORDER_ITEM_DIALOG, { sdOrder: this.getViewModel().get('sdOrder') });
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNewSDOrderItemClick of BaseSDOrderDetailPageController', ex);
		}
	},
	
	onSDOrderItemSaved: function(sdOrderItem) {
		try {
			var sdOrderItems = this.getViewModel().get('sdOrder').get('items');
		
			var temp = sdOrderItems.getById(sdOrderItem.get('id'));
			
			if(temp === undefined || temp === null)
				sdOrderItems.add(sdOrderItem);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemSaved of BaseSDOrderDetailPageController', ex);
		}
	}
});