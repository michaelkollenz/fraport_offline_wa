Ext.define('AssetManagement.base.controller.pages.BaseEquiChangePositionPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
         'AssetManagement.customer.manager.EquipmentManager'
    ],

    config: {
        contextReadyLevel: 1
    },

    //private
    _saveRunning: false,

    //public
    //@override
    onOptionMenuItemSelected: function (optionID) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
                this.trySaveEquiChange();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseEquiChangePositionPageController', ex);
        }
    },

    //protected
    //@override
    onCreateOptionMenu: function () {
        try {
            this._optionMenuItems = [AssetManagement.customer.controller.ToolbarController.OPTION_SAVE];
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseEquiChangePositionPageController', ex);
        }
    },

    //protected
    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            var equnr = '';

            if (argumentsObject['equnr'] && typeof argumentsObject['equnr'] === 'string') {
                equnr = argumentsObject['equnr'];
            } else if (argumentsObject['equipment'] && Ext.getClassName(argumentsObject['equipment']) === 'AssetManagement.customer.model.bo.Equipment') {
                this.getViewModel().set('equipment', argumentsObject['equipment']);
            } else {
                throw Ext.create('AssetManagement.customer.utils.OxException', {
                    message: 'Insufficient parameters',
                    method: 'startBuildingPageContext',
                    clazz: 'AssetManagement.customer.controller.pages.EquiChangePositionPageController'
                });
            }

            if (equnr) {
                this.getEquipment(equnr);
            }

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseEquiChangePositionPageController', ex);
            this.errorOccurred();
        }
    },

    //protected
    //@override
    beforeDataReady: function () {
        try {
            this.getViewModel().set('changeDate', new Date());
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseEquiChangePositionPageController', ex);
        }
    },

    //get equi by equnr
    getEquipment: function (equnr) {
        try {
            var equipmentRequest = AssetManagement.customer.manager.EquipmentManager.getEquipment(equnr, true);
            this.addRequestToDataBaseQueue(equipmentRequest, 'equipment');

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getEquipment of BaseEquiChangePositionPageController', ex);
        }
    },

    onSelectSuperiorFuncLocClick: function () {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.FUNCLOCPICKER_DIALOG, {});
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectSuperiorFuncLocClick of BaseEquiChangePositionPageController', ex);
        }
    },

    onSelectSuperiorEquipmentClick: function () {
        try {
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.EQUIPMENTPICKER_DIALOG, {});
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectSuperiorEquipmentClick of BaseEquiChangePositionPageController', ex);
        }
    },

    onFuncLocSelected: function (funcLoc) {
        try {
            var myModel = this.getViewModel();

            myModel.set('funcLoc', funcLoc);
            myModel.set('superiorEqui', null);
            this.getView().refreshNewFuncLocTextField();
            this.getView().refreshNewSuperiorEquiTextField();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseEquiChangePositionPageController', ex);
        }
    },

    onEquiSelected: function (equipment) {
        try {
            var myModel = this.getViewModel();

            var selectedEquisFloc = equipment.get('funcLoc');

            if (!selectedEquisFloc) {
                var tplnr = equipment.get('tplnr')

                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(tplnr)) {
                    selectedEquisFloc = Ext.create('AssetManagement.customer.model.bo.FuncLoc', {
                        tplnr: tplnr
                    });
                }
            }

            myModel.set('funcLoc', selectedEquisFloc);
            myModel.set('superiorEqui', equipment);

            this.getView().refreshNewFuncLocTextField();
            this.getView().refreshNewSuperiorEquiTextField();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseEquiChangePositionPageController', ex);
        }
    },

    //will try to save/perform the equipment change position
    trySaveEquiChange: function () {
        try {
            if (this._saveRunning === true)
                return;

            this._saveRunning = true;

            AssetManagement.customer.controller.ClientStateController.enterLoadingState();

            var saveAction = function (inputValuesCheckPassed) {
                try {
                    //only continue for valid input values
                    if (inputValuesCheckPassed) {
                        //transfer current input values into the model
                        this.getView().transferViewStateIntoModel();

                        var me = this;
                        var callback = function (success) {
                            try {
                                if (success === true) {
                                    //goBack to remove this page from navigation stack
                                    AssetManagement.customer.core.Core.navigateBack();

                                    Ext.defer(function () {
                                        me._saveRunning = false;
                                    }, 2000, me);

                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('changingsSaved'), true, 0);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                } else {
                                    me._saveRunning = false;
                                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveEquiChange of BaseEquiChangePositionPageController', ex);
                                me._saveRunning = false;
                                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                            }
                        };

                        //next perform the actual position change
                        this.performChangePosition(callback);
                    } else {
                        this._saveRunning = false;
                        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    }
                } catch (ex) {
                    this._saveRunning = false;
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSaving'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                }
            };

            this.checkInputValues(saveAction, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveEquiChange of BaseEquiChangePositionPageController', ex);
            this._saveRunning = false;
            AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
        }
    },

    //private
    checkInputValues: function (callback, callbackScope) {
        try {
            var retval = true;
            var errorMessage = '';

            var values = this.getView().getCurrentInputValues();

            var tplnr = values.tplnr;
            var hequi = values.hequi;
            var heqnr = values.heqnr;
            var changeDate = values.changeDate;
            var changeTime = values.changeTime;

            if (typeof (changeDate) !== 'object' || AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(changeDate)) {
                errorMessage = Locale.getMsg('enterValidDate');
                retval = false;
            } else if (typeof (changeTime) !== 'object' || AssetManagement.customer.utils.DateTimeUtils.isNullOrEmpty(changeTime)) {
                errorMessage = Locale.getMsg('enterValidTime');
                retval = false;
            } else {
                //check if the passed heqnr is a valid integer
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(heqnr)) {
                    if (heqnr.search(/^\d+$/) > -1) {
                        //check if the passed number is 0
                        try {
                            var integ = parseInt(heqnr);

                            if (!integ) {
                                errorMessage = Locale.getMsg('positionMayNotBeZero');
                                retval = false;
                            }
                        } catch (ex) {

                        }
                    } else {
                        errorMessage = Locale.getMsg('positionHasToBeANumber');
                        retval = false;
                    }
                }
            }

            var returnFunction = function () {
                if (retval === false) {
                    var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(message);
                }

                callback.call(callbackScope ? callbackScope : me, retval);
            };

            returnFunction.call(this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseEquiChangePositionPageController', ex)

            if (callback) {
                callback.call(callbackScope ? callbackScope : this, false);
            }
        }
    },

    //private
    //will perform the position change of the equipment
    //returns a boolean, indicating success
    //callback is mandatory
    performChangePosition: function (callback) {
        try {
            if (!callback)
                return;

            //get the new position data in a dummy equipment
            var adjustedDummyEquipment = this.generateAdjustedDummyEquipment();

            if (adjustedDummyEquipment) {
                var myModel = this.getViewModel();
                var changeDate = myModel.get('changeDate');

                var eventId = AssetManagement.customer.manager.EquipmentManager.performEquipmentPositionChange(adjustedDummyEquipment, changeDate);

                if (eventId > -1) {
                    var me = this;
                    var innerCallback = function (adjustedOriginalEquipment) {
                        try {
                            success = adjustedOriginalEquipment !== undefined;

                            callback.call(me, success);
                        } catch (ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performChangePosition of BaseEquiChangePositionPageController', ex);

                            callback.call(me, false);
                        }
                    };

                    AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, innerCallback);
                } else {
                    callback.call(this, false);
                }
            } else {
                callback.call(this, false);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performChangePosition of BaseEquiChangePositionPageController', ex);

            callback.call(this, false);
        }
    },

    //private
    //generates an adjusted dummy equipment model for saving
    generateAdjustedDummyEquipment: function () {
        var retval = null;

        try {
            var myModel = this.getViewModel();
            var equipment = myModel.get('equipment');

            var funcLoc = myModel.get('funcLoc');
            var superiorEqui = myModel.get('superiorEqui');
            var newPosition = myModel.get('newPosition');
            var changeDate = myModel.get('changeDate');

            var newTplnr = funcLoc ? funcLoc.get('tplnr') : '';
            var newHequi = superiorEqui ? superiorEqui.get('equnr') : '';

            var newHeqnr = (newPosition && (superiorEqui || funcLoc)) ? newPosition : '';

            retval = Ext.create('AssetManagement.customer.model.bo.Equipment', {
                equnr: equipment.get('equnr'),
                tplnr: newTplnr,
                hequi: newHequi,
                heqnr: newHeqnr
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateAdjustedDummyEquipment of BaseEquiChangePositionPageController', ex);
        }

        return retval;
    }
});