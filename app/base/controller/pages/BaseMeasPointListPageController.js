﻿Ext.define('AssetManagement.base.controller.pages.BaseMeasPointListPageController', {
    extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
   		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.MeasurementPointManager',
        'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
    ],

    config: {
        contextReadyLevel: 1,
        isAsynchronous: true
    },

    //mixins: {
    //    handlerMixin: 'AssetManagement.customer.controller.mixins.MeasPointListPageControllerMixin'
    //},

    //public
    //@override
    onOptionMenuItemSelected: function (optionID) {
        try {
            if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
                this.onSearchButtonClicked();
            } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH) {
                this.onClearSearchButtonClicked();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseMeasPointListPageController', ex);
        }
    },

    //private
    onCreateOptionMenu: function () {
        this._optionMenuItems = new Array();
        try {
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseMeasPointListPageController', ex);
        }
    },

    //@override
    startBuildingPageContext: function (argumentsObject) {
        try {
            this.loadListStore();

            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseMeasPointListPageController', ex);
        }
    },

    loadListStore: function () {
        try {

            var measPointStoreRequest = AssetManagement.customer.manager.MeasurementPointManager.getMeasurementPoints(true, true);
            this.addRequestToDataBaseQueue(measPointStoreRequest, 'measPointStore');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseMeasPointListPageController', ex);
        }
    },

    //list handler
    onMeasPointSelected: function (tableview, td, cellIndex, record, tr, rowIndex, event, eOpts) {
        try {
            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_MEAS_DOC_EDIT, { measPoint: record });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMeasPointSelected of BaseMeasPointListPageController', ex);
        }
    },

    onMeasPointSelectedByTabHold: function (measPoint) {
    },

    onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();
            if (!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [];

            this.rowMenu.onCreateContextMenu(options, record);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseMeasPointListPageController', ex);
        }
    },

    beforeDataReady: function () {
        try {
            //apply the current filter criteria on the store, before it is transferred into the view
            this.applyCurrentFilterCriteriasToStore();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseMeasPointListPageController', ex);
        }
    },

    applyCurrentFilterCriteriasToStore: function () {
        try {
            try {
                this.getView().getSearchValue();

                var measPointToSearch = this.getViewModel().get('searchMeasPoint');
                var measPointStore = this.getViewModel().get('measPointStore');

                if (measPointStore) {
                    measPointStore.clearFilter();

                    // Equino
                    if (measPointToSearch.get('point')) {

                        measPointStore.filterBy(function (record) {
                            try {
                                if (record.get('point')) {
                                    if (record.get('point').toUpperCase().indexOf(measPointToSearch.get('point').toUpperCase()) > -1) {
                                        return true;
                                    }
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMeasPointListPageController', ex);
                            }
                        });

                        // Kurztext
                    } else if (measPointToSearch.get('pttxt')) {
                        measPointStore.filterBy(function (record) {
                            try {
                                if (record.get('pttxt') && record.get('pttxt').toUpperCase().indexOf(measPointToSearch.get('pttxt').toUpperCase()) > -1) {
                                    return true;
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMeasPointListPageController', ex);
                            }
                        });
                    } else if ((measPointToSearch.get('equipment') && measPointToSearch.get('equipment').get('eqktx'))) {
                        var tempEqunr = measPointToSearch.get('equipment').get('equnr') ? measPointToSearch.get('equipment').get('equnr').toUpperCase() : '';
                        var tempEqktx = measPointToSearch.get('equipment').get('eqktx') ? measPointToSearch.get('equipment').get('eqktx').toUpperCase() : '';
                        tempEqktx = tempEqktx ? tempEqktx.toUpperCase() : '';

                        measPointStore.filterBy(function (record) {
                            try {

                                if (tempEqunr && record.get('equipment')!=null && record.get('equipment').get('equnr') && record.get('equipment').get('equnr').toUpperCase().indexOf(tempEqunr) > -1) {
                                    return true;
                                } else if (tempEqktx && record.get('equipment')!=null  && record.get('equipment').get('eqktx').toUpperCase().indexOf(tempEqktx) > -1) {
                                    return true;
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMeasPointListPageController', ex);
                            }
                        });
                        // Techn. Platz
                    } else if ((measPointToSearch.get('funcLoc') && measPointToSearch.get('funcLoc').get('pltxt'))) {
                        var tempTplnr = measPointToSearch.get('funcLoc').get('tplnr') ? measPointToSearch.get('funcLoc').get('tplnr').toUpperCase() : '';
                        var tempPltxt = measPointToSearch.get('funcLoc').get('pltxt') ? measPointToSearch.get('funcLoc').get('pltxt').toUpperCase() : '';
                        tempPltxt = tempPltxt ? tempPltxt.toUpperCase() : '';

                        measPointStore.filterBy(function (record) {
                            try {
                                if (record.get('funcLoc')) {
                                    if (tempTplnr && record.get('funcLoc').get('tplnr') && record.get('funcLoc').get('tplnr').toUpperCase().indexOf(tempTplnr) > -1) {
                                        return true;
                                    } else if (tempTplnr && record.get('tplnr') && record.get('funcLoc') && record.get('funcLoc').getDisplayIdentification().toUpperCase().indexOf(tempTplnr) > -1) {
                                        return true;
                                    } else if (tempPltxt && record.get('funcLoc') && record.get('funcLoc').get('pltxt').toUpperCase().indexOf(tempPltxt) > -1) {
                                        return true;
                                    }
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMeasPointListPageController', ex);
                            }
                        });
                        //Sortierfeld
                    } else if (measPointToSearch.get('equipment') && measPointToSearch.get('equipment').get('eqfnr')) {
                        measPointStore.filterBy(function (record) {
                            try {
                                if (record.get('equipment') && record.get('equipment').get('eqfnr').toUpperCase().indexOf(measPointToSearch.get('equipment').get('eqfnr').toUpperCase()) > -1) {
                                    return true;
                                }
                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMeasPointListPageController', ex);
                            }
                        });
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseMeasPointListPageController', ex)
            }


        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseMeasPointListPageController', ex)
        }
    },

    onMeasPointListPageSearchFieldChange: function () {
        try {
            this.applyCurrentFilterCriteriasToStore();
            this.getView().refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMeasPointListPageSearchFieldChange of BaseMeasPointListPageController', ex)
        }
    },

    //functions for search
    //public
    onSearchButtonClicked: function () {
        try {
            var myModel = this.getViewModel();

            this.manageSearchCriterias();

            var searchDialogArgs = {
                source: myModel.get('measPointStore'),
                criterias: myModel.get('searchCriterias')
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseMeasPointListPageController', ex);
        }
    },

    ////functions for search
    ////private
    ////handles initialization of search criterias
    //manageSearchCriterias: function () {
    //    try {
    //        var myModel = this.getViewModel();
    //        var currentSearchCriterias = myModel.get('searchCriterias');
    //        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
    //        var searchMixin = this.mixins.handlerMixin

    //        if (!currentSearchCriterias) {
    //            var searchArray = [{
    //                id: 'point',
    //                columnName: 'measPoint',
    //                searchFunction: '',
    //                position: 1
    //            },
    //            {
    //                id: 'mpobj',
    //                columnName: 'object',
    //                searchFunction: '',
    //                position: 1
    //            }
    //            ];

    //            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(searchArray, searchMixin);

    //            myModel.set('searchCriterias', newCriterias);
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseMeasPointListPageController', ex);
    //    }
    //},

    //private
    //handles initialization of search criterias
    manageSearchCriterias: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');
            //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
            var searchMixin = this.mixins.handlerMixin

            if (!currentSearchCriterias) {
                var gridpanel = Ext.getCmp('measPointGridPanel');

                //var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray);
                var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray, searchMixin);

                myModel.set('searchCriterias', newCriterias);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseMeasPointListPageController', ex);
        }
    },

    //functions for search
    //public
    onSearchApplied: function (searchResult) {
        try {

            this.getViewModel().set('constrainedMeaspoints', searchResult);

            this.setupInitialSorting();

            //this.setupAutoCompletion();

            this.applyCurrentFilterCriteriasToStore();

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of BaseMeasPointListPageController', ex);
        }
    },

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
    setupInitialSorting: function () {
        try {
            var myModel = this.getViewModel();
            var constrainedMeaspoints = myModel.get('constrainedMeaspoints');

            //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
                myModel.set('sortProperty', 'mymAgreement');
                myModel.set('sortDirection', 'DESC');
            }

            //apply the current sort rule
            if (constrainedMeaspoints) {
                constrainedMeaspoints.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
                ]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseMeasPointListPageController', ex);
        }
    },

    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias'))) {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias    
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseMeasPointListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/clear_search.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseMeasPointListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseMeasPointListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseMeasPointListPageController', ex);
        }
    },
    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of BaseMeasPointListPageController', ex);
        }
    }

});