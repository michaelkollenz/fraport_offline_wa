Ext.define('AssetManagement.base.controller.pages.BaseEquipmentListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',


	requires: [
	    'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.EquipmentManager',
        'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
	],
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},
	
	mixins: {
	    handlerMixin: 'AssetManagement.customer.controller.mixins.EquipmentListPageControllerMixin'
	},
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_ADD) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_EQUI);
	    	} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
	    	    this.onSearchButtonClicked();
	    	} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH){
	    	    this.onClearSearchButtonClicked();
	    	}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseEquipmentListPageController', ex)
		}
	},
		
	//private
	onCreateOptionMenu: function() {
		this._optionMenuItems = new Array();
	    try {
	        var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
	        if (funcParaInstance.getValue1For('equi_create') === 'X')
		    	this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_ADD);
		    
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);

		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseEquipmentListPageController', ex)
		}	   
	},
	
    //functions for search
    //public
	onSearchButtonClicked: function () {
	    try {
	        var myModel = this.getViewModel();

	        this.manageSearchCriterias();

	        var searchDialogArgs = {
	            source: myModel.get('equipmentStore'),
	            criterias: myModel.get('searchCriterias')
	        };

	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseEquipmentListPageController', ex);
	    }
	},

    ////functions for search
    ////private
    ////handles initialization of search criterias
	//manageSearchCriterias: function () {
	//    try {
	//        var myModel = this.getViewModel();
	//        var currentSearchCriterias = myModel.get('searchCriterias');
	//        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
	//        var searchMixin = this.mixins.handlerMixin

	//        if (!currentSearchCriterias) {
	//            var searchArray = [{
	//                id: 'equnr',
	//                columnName: 'equipment',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            {
	//                id: 'tidnr',
	//                columnName:'technicalIdentNr',
	//                searchFunction: '',
	//                position: 1
	//            },
    //            {
    //                id: 'matnr',
    //                columnName: 'material',
    //                searchFunction: '',
    //                position: 1
    //            },
	//            {
	//                id: 'eqfnr',
	//                columnName: 'sortField',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            {
	//                id: 'postalCode',
	//                columnName: 'address',
	//                searchFunction: '',
	//                position: 1
	//            },
	//         ];

	//            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(searchArray, searchMixin);

	//            myModel.set('searchCriterias', newCriterias);
	//        }
	//    } catch (ex) {
	//        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseEquipmentListPageController', ex);
	//    }
	//},


    //functions for search
    //private
    //handles initialization of search criterias
	manageSearchCriterias: function () {
	    try {
	        var myModel = this.getViewModel();
	        var currentSearchCriterias = myModel.get('searchCriterias');
	        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
	        var searchMixin = this.mixins.handlerMixin;

	        if (!currentSearchCriterias) {
	            var gridpanel = Ext.getCmp('equipmentGridPanel');

	            //var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray);
	            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray, searchMixin);

	            myModel.set('searchCriterias', newCriterias);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseEquipmentListPageController', ex);
	    }
	},

	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
		    this.loadListStore();
		    
		    this.waitForDataBaseQueue();		    
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseEquipmentListPageController', ex)
		}    
	},
		
	loadListStore: function() {
	    try {
		    var equipmentStoreRequest = AssetManagement.customer.manager.EquipmentManager.getEquipments(true);
		    this.addRequestToDataBaseQueue(equipmentStoreRequest, 'equipmentStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseEquipmentListPageController', ex)
		}    
	},
		
	//list handler
	onEquipmentSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: record.get('equnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquipmentSelected of BaseEquipmentListPageController', ex)
		}
    },
	
	onEquipmentSelectedByTabHold: function(equipment) {
	},
			
	navigateToAddress: function(equi) {
       try {
			var addressInformation = null;
		
			if(equi) {
				addressInformation = equi.get('address');
			}
				
			if(addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postCode1'))) {
				// AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);
				
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES});
	
				//generate the hint
				
				hint = AssetManagement.customer.utils.StringUtils.trimStart(equi.get('equnr'), '0');
				
				request.addAddressWithHint(addressInformation, hint);
				
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToAddress of NotifListPageController', ex);
		}
    },
    
    beforeDataReady: function() {
    	try {
    		//apply the current filter criteria on the store, before it is transferred into the view
    		this.applyCurrentFilterCriteriasToStore();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of OrderListPageController', ex);
		}
	},
    
	applyCurrentFilterCriteriasToStore: function() {
    	try {
			this.getView().getSearchValue();

			var equiToSearch = this.getViewModel().get('searchEqui');
			var equiStore = this.getViewModel().get('equipmentStore');
			
			if(equiStore) {
				equiStore.clearFilter();			

				// Equipmentnummer
				if(equiToSearch.get('equnr')) {
					equiStore.filterBy(function (record) {
						try {
						    if (record.get('equnr') && record.get('equnr').toUpperCase().indexOf(equiToSearch.get('equnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				// Kurztext
				}else if (equiToSearch.get('eqktx')) {
					equiStore.filterBy(function (record) {
						try {
						    if (record.get('eqktx') && record.get('eqktx').toUpperCase().indexOf(equiToSearch.get('eqktx').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				// Tech. Indentnummer
				}else if (equiToSearch.get('tidnr')) {
					equiStore.filterBy(function (record) {
						try {
							
						    if ( record.get('tidnr') && record.get('tidnr').toUpperCase().indexOf(equiToSearch.get('tidnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				// Techn. Platz
				}else if (equiToSearch.get('tplnr') || (equiToSearch.get('funcLoc') && equiToSearch.get('funcLoc').get('pltxt'))) {
					var tempTplnr = equiToSearch.get('tplnr') ? equiToSearch.get('tplnr').toUpperCase() : '';
					var tempPltxt = equiToSearch.get('funcLoc') ? equiToSearch.get('funcLoc').get('pltxt') : '';
					tempPltxt = tempPltxt ? tempPltxt.toUpperCase() : '';
				
					equiStore.filterBy(function(record) {
						try {
							if(tempTplnr && record.get('tplnr') && record.get('tplnr').toUpperCase().indexOf(tempTplnr) > -1) {
								return true; 
							} else if(tempPltxt && record.get('funcLoc') && record.get('funcLoc').get('pltxt').toUpperCase().indexOf(tempPltxt) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				// Sortierfeld
				}else if (equiToSearch.get('eqfnr')) {
					equiStore.filterBy(function (record) {
						try {
						    if (record.get('eqfnr').toUpperCase().indexOf(equiToSearch.get('eqfnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				// Materialnumber
				} else if(equiToSearch.get('matnr')) {
					equiStore.filterBy(function (record) {
						try {
						    if (record.get('matnr').toUpperCase().indexOf(equiToSearch.get('matnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
	
				// Standort
				}else if (equiToSearch.get('stort')) {
					equiStore.filterBy(function (record) {
						try {
						    if (record.get('stort').toUpperCase().indexOf(equiToSearch.get('stort').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				// Raum
				} else if(equiToSearch.get('msgrp')) {
					equiStore.filterBy(function (record) {
						try {
						    if (record.get('msgrp').toUpperCase().indexOf(equiToSearch.get('msgrp').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in EquiListPageController', ex);
						}
					});
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of EquiListPageController', ex)
		}
    },
    
    onEquiListPageSearchFieldChange: function() {
    	try {
			this.applyCurrentFilterCriteriasToStore();
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiListPageSearchFieldChange of EquiListPageController', ex)
		}
    },
    
    onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
			
			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of EquiListPageController', ex);
		}
    },

    //functions for search
    //public
    onSearchApplied: function (searchResult) {
        try {

            this.getViewModel().set('constrainedEquipments', searchResult);

            this.setupInitialSorting();

            //this.setupAutoCompletion();

            this.applyCurrentFilterCriteriasToStore();

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of BaseEquipmentListPageController', ex);
        }
    },

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
    setupInitialSorting: function () {
        try {
            var myModel = this.getViewModel();
            var constrainedEquipments = myModel.get('constrainedEquipments');

            //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
                myModel.set('sortProperty', 'mymAgreement');
                myModel.set('sortDirection', 'DESC');
            }

            //apply the current sort rule
            if (constrainedEquipments) {
                constrainedEquipments.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
                ]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseEquipmentListPageController', ex);
        }
    },


    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias')))
            {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias    
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseEquipmentListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/clear_search.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseEquipmentListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseEquipmentListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseEquipmentListPageController', ex);
        }
    },
    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of BaseEquipmentListPageController', ex);
        }
    }
});