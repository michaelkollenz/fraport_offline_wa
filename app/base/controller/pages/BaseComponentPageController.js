Ext.define('AssetManagement.base.controller.pages.BaseComponentPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
    requires: [
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.utils.NumberFormatUtils',
        'AssetManagement.customer.manager.Cust_002Manager',
        'AssetManagement.customer.model.bo.OrdComponent',
        'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger'
    ],
               
	config: {
		contextReadyLevel: 2,
		rowMenu: null
	},
	
	//private
	_saveRunning: false,
	
	//protected
	//@override
	onCreateOptionMenu: function() {
		this._optionMenuItems = new Array();
		
		try {
			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM);
			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SAVE);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseComponentPageController', ex);
		}
	},
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
			
				this.trySaveComponent();
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
				if(this._saveRunning === true)
					return;
					
				this.initializeNewComponent();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseComponentPageController', ex);
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			//extract parameters, if any given
			if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
				this.getViewModel().set('order', argumentsObject['order']); 
			} else if(Ext.getClassName(argumentsObject['component']) === 'AssetManagement.customer.model.bo.OrdComponent') {
				this.getViewModel().set('isEditMode', true);
				this.getViewModel().set('component', argumentsObject['component']);
			} else {
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.ComponentPageController'
			    });
			}
			
			this.waitForDataBaseQueue();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseComponentPageController', ex);
		    this.errorOccurred();
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		try {
			if(this._contextLevel === 1) {
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseComponentPageController', ex);
		}
	},
	
	//private			
	performContextLevel1Requests: function() {
		try {
			var storLocsRequest = AssetManagement.customer.manager.Cust_002Manager.getCust_002s();
			this.addRequestToDataBaseQueue(storLocsRequest, 'storLocs');
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseComponentPageController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			if(!this.getViewModel().get('component'))
				this.initializeNewComponent(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseComponentPageController', ex);
		}
	},
	
	//will initialize a new component and transfer it into the model
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewComponent: function(skipRefreshView) {
		try {
			if(skipRefreshView === true)
				skipRefreshView = true;
			else
				skipRefreshView = false;
		
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operation = order.get('operations').getAt(0);
		
			var newComponent = Ext.create('AssetManagement.customer.model.bo.OrdComponent', { order: order });
			newComponent.set('vornr', operation.get('vornr'));
			newComponent.set('meins', 'ST');
			newComponent.set('postp', 'L');
		
			myModel.set('component', newComponent);
			myModel.set('chosenMaterial', null);
			myModel.set('isEditMode', false);
			
			if(skipRefreshView === false)
				this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewComponent of BaseComponentPageController', ex);
		}
	},
	
	onPlantTextChanged: function(field, e, eOpts) {
		try {
			this.getView().fillStorageLocationComboBox(field.getValue());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPlantTextChanged of BaseComponentPageController', ex);
		}
	},
	
	//will perform a set of checks on the current input values before those values will be transferred into the current component and saved to the database
	trySaveComponent: function() {
		try {
			this._saveRunning = true;
		
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
		
			var me = this;
			var saveAction = function(checkWasSuccessfull) {
				try {
					if(checkWasSuccessfull === true) {
						var component = me.getViewModel().get('component');
					
						me.getView().transferViewStateIntoModel();
						component.set('meins', component.get('meins').toUpperCase());
						
						var chosenMaterial = me.getViewModel().get('chosenMaterial');
						component.set('material', chosenMaterial ? chosenMaterial : null);
						
						var callback = function(success) {
							try {
								if(success === true) {
									me.addComponentToListStore(component);
									me.initializeNewComponent();
									
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('componentSaved'), true, 0);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								} else {
									var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingComponent'), false, 2);
									AssetManagement.customer.core.Core.getMainView().showNotification(notification);
								}
							} catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveComponent of BaseComponentPageController', ex);
							} finally {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							}
						};				
						
						var eventId = AssetManagement.customer.manager.ComponentManager.saveComponent(component);
						
						if(eventId > 0) {
							AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
						} else {
							me._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingComponent'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveComponent of BaseComponentPageController', ex);
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
				}
			};
			
			//check if data is complete first
			this.checkInputValues(saveAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveComponent of BaseComponentPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	//performs a set of checks, returning the overall result by the provided callback
	checkInputValues: function(callback, callbackScope) {
		try {
			var retval = true;
			var errorMessage = '';
					
			var values = this.getView().getCurrentInputValues(); 
		
			var matnr = values.matnr;
			var unit = values.meins;
			var plant = values.werks;
			var storLoc = values.lgort;	
		    var quantity = values.bdmng;
		    
			var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
			
			if(shortHand(matnr)) {
				//provide material
				errorMessage = Locale.getMsg('provideMaterial');
				retval = false;
			} else if(shortHand(unit)) {
				//provide unit
				errorMessage = Locale.getMsg('provideUnit');
				retval = false;
			}  else if(shortHand(plant)) {
				//provide plant
				errorMessage = Locale.getMsg('providePlant');
				retval = false;
			} else if(shortHand(storLoc)) {
				//provide storloc
				errorMessage = Locale.getMsg('provideStorageLocation');
				retval = false;
			} else if(quantity === null || quantity === undefined || quantity <= 0) {
				//invalid number
				errorMessage = Locale.getMsg('provideValidQuantity');
				retval = false;
			}
			
			var me = this;
			
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			if(retval === false) {
				returnFunction.call(this);
			} else {
				//last do the material check - this means:
				//if the chosen material does no longer match the matnr from the input value, try to load the now connected material
				//if this material exist and has a diffrent unit, return false, inform the user and tell him, what the correct unit is
				//if the material could not be loaded, assume that it exists at the sap system and pass the check
				var chosenMaterial = this.getViewModel().get('chosenMaterial');
				
				var unitCheckFunction = function(material) {
					try {
						if(material) {
						
							if(material.get('meins').toUpperCase() !== unit.toUpperCase()) {
								errorMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('unitDoesNotMatchMaterial'), [ material.get('meins') ]);
								retval = false;
							}
						}
						
						me.getViewModel().set('chosenMaterial', material);
						
						returnFunction.call(this);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseComponentPageController', ex);
						
						if(callback) {
							callback.call(callbackScope ? callbackScope : me, false);
						}
					}
				};
				
				if(chosenMaterial && chosenMaterial.get('matnr') === matnr) {
					unitCheckFunction.call(this, chosenMaterial);
				} else {
					//load material from database
					var eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr);
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, unitCheckFunction);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseComponentPageController', ex);
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	deleteComponent: function(component) {
		try {
			var currentComponent = this.getViewModel().get('component');
			var deletingCurrentEditingComponent = component.get('rsnum') === currentComponent.get('rsnum') && component.get('rspos') === currentComponent.get('rspos');
				
			var me = this;
			var callback = function(success) {
				try {
					if(success === true) {
 						if(deletingCurrentEditingComponent)
							me.initializeNewComponent();
							
						me.deleteComponentFromListStore(component);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('componentDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingComponent'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponent of BaseComponentPageController', ex);
				}
			};
			
			var eventId = AssetManagement.customer.manager.ComponentManager.deleteComponent(component);
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingComponent'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponent of BaseComponentPageController', ex);
		}
	},
	
	editComponent: function(component) {
		try {
			var myModel = this.getViewModel();
		
			myModel.set('component', component);
			myModel.set('chosenMaterial', component.get('material'));
			myModel.set('isEditMode', true);
			
			this.getView().refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editComponent of BaseComponentPageController', ex);
		}
	},

	onSelectMaterialClick: function() {
		try {
			var order = this.getViewModel().get('order');
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.MATERIALPICKER_DIALOG, { order: order, useComponents: false });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectMaterialClick of BaseComponentPageController', ex);
		}
	},
	
	onMaterialSelected: function(selectedObject) {
		try {
			var material = null;
			var viewTransferMethod = null;
			
			var typeOfSelectedObject = Ext.getClassName(selectedObject);
			
			//distinguish the type (diffrent methods)
			if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.Stpo') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().fillMaterialSpecFieldsFromStpo;
			} else if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.MatStock') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().fillMaterialSpecFieldsFromMatStock;
			} else if (typeOfSelectedObject === 'AssetManagement.customer.model.bo.Material') {
			    material = selectedObject;
			    viewTransferMethod = this.getView().fillMaterialSpecFieldsFromMaterial;
			}
			
			this.getViewModel().set('chosenMaterial', material);
			
			if(viewTransferMethod) {
				viewTransferMethod.call(this.getView(), selectedObject);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialSelected of BaseComponentPageController', ex);
		}
	},
	
	addComponentToListStore: function(component) {
		try {
			var components = this.getViewModel().get('order').get('components');
			var temp = components.getById(component.get('id'));
			
			if(temp) {
				var index = components.indexOf(temp);
				components.removeAt(index);
				components.insert(index, component);
			} else {
				components.insert(0, component);
			}	
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addComponentToListStore of BaseComponentPageController', ex);
		}
	},
	
	deleteComponentFromListStore: function(component) {
		try {
			var components = this.getViewModel().get('order').get('components');
			var temp = components.getById(component.get('id'));

			if(temp)
				components.remove(component);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponentFromListStore of BaseComponentPageController', ex);
		}
	},

	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
		try {
			e.stopEvent(); 
		
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			if(record.get('updFlag') === 'I') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
			}
				
			this.rowMenu.onCreateContextMenu(options, record); 
			
			if(options.length > 0) {
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseComponentPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
				this.deleteComponent(record);
			}
			else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
				this.editComponent(record); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseComponentPageController', ex);
		}
	}
	///------ CONTEXT MENU END -------///
});