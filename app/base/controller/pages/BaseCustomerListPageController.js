Ext.define('AssetManagement.base.controller.pages.BaseCustomerListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.CustomerManager',
		'AssetManagement.customer.helper.OxLogger'
	],
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseCustomerListPageController', ex);
		}
	},
		
	//private
	onCreateOptionMenu: function() {
		try {
			//this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseCustomerListPageController', ex);
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			this.loadListStore();
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseCustomerListPageController', ex);
		}	
	},
		
	loadListStore: function() {
		try {
			var customerStoreRequest = AssetManagement.customer.manager.CustomerManager.getCustomers(true);
			this.addRequestToDataBaseQueue(customerStoreRequest, 'customerStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseCustomerListPageController', ex);
		}
	},
		
	//list handler
	onCustomerSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CUSTOMER_DETAILS, { customer: record });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerSelected of BaseCustomerListPageController', ex);
		}
	},
	
	onCustomerSelectedByTabHold: function(funcLoc) {
		try {
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCustomerSelectedByTabHold of BaseCustomerListPageController', ex);
		}
	},
	
	onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
   		try {
   			e.stopEvent();
				if(!this.rowMenu)
					this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
				var options = [ ];

				this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseCustomerListPageController', ex);
		}
	}
});