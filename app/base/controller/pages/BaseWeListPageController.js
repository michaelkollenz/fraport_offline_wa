Ext.define(
    'AssetManagement.base.controller.pages.BaseWeListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',
	alias: 'controller.BaseWeListPageController',

	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.manager.UserManager',
	    'AssetManagement.customer.controller.ToolbarController',
	    'AssetManagement.customer.helper.LanguageHelper',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.PoItem'
	],

	config: {
		contextReadyLevel: 1
    },

	//private
	_requestRunning: false,
	_argumentsObject: null,

	//protected
	//@override
	startBuildingPageContext: function (argumentsObject) {
	    try {
	        var myModel = this.getViewModel();
	        this._argumentsObject = argumentsObject; //this argumentsObject will be modified in the view later,
	        //it is a unique js object which is will be reused as arguments object for back navigation
	        //so, it can be altered so that the next 'back navigation' has extra arguments from the user input on the view.
            //each independent entry to the view creates a new object { } and therefore, will not use the old values - the view will be cleared.

	        this.transferArgumentsToModel(argumentsObject);

	        if (!myModel.get('miProcess')) {
	            this.loadMiProcess();
	        }

	        this.waitForDataBaseQueue(); //start the page without the data, becaue only after we start it we can get an onlineRequest
	        this.getView().transferModelToViewState();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
	    }
	},

	transferArgumentsToModel: function (argumentsObject) {
	    try {
	        var myModel = this.getViewModel();
	        var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');

	        myModel.set('selectedLgort', selectedLgort); //always exists
	        myModel.set('poInput', argumentsObject['poInput'] ? argumentsObject['poInput'] : '');
	        myModel.set('reference', argumentsObject['reference'] ? argumentsObject['reference'] : '');
	        myModel.set('poItemsStore', argumentsObject['poItemsStore'] ? argumentsObject['poItemsStore'] : null);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferArgumentsToModel of BaseWeListPageController', ex);
	    }
	},

    refreshData: function(triggerPageRequestCallbackWhenDone) {
        try {
            if(!triggerPageRequestCallbackWhenDone) {
                this._pageRequestCallback = null;
                this._pageRequestCallbackScope = null;
            }
            // this.getViewModel().resetData(); //Deactivated to have a persistent store when navigating back to page
            //set to zero
            this._contextLevel = 0;

            if(this._contextReadyLevel === 0) {
                this.contextReady();
            } else {
                //start the loading cycle
                this.startBuildingPageContext(this.getCurrentArguments());
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
        }
    },



    afterRefreshView: function () {
      try {
        this.getView().afterRefreshView();

      } catch (ex) {
        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterRefreshView of BaseWeListPageController', ex);
      }
    },

        //protected
    //may be implemented by derivates
    onOptionMenuItemSelected: function(optionID) {
        try {
            if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_COMMIT) {
                this.onCommitButtonClick();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseWeListPageController', ex);
        }
    },

    loadMiProcess: function () {
        try {
            var miProcess = AssetManagement.customer.manager.MiProcessManager.getMiProcess("MM_GR", true);
            this.addRequestToDataBaseQueue(miProcess, "miProcess");
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadMiProcess of WaCreatePageController', ex);
        }
    },

    onCreateOptionMenu: function() {
        try {
            this._optionMenuItems = [];
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_COMMIT);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseWeListPageController', ex);
        }
    },

    onCommitButtonClick: function () {
        try {
            var myModel = this.getViewModel()
            var goodsMovementItemsStore = this.wrapGoodsMovementItems();
            if (!goodsMovementItemsStore || goodsMovementItemsStore.getCount() === 0) {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noItems'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                return false;
            }
            var reference = myModel.get('reference');
            var miProcess = myModel.get('miProcess');
            var poItemsStore = myModel.get('poItemsStore');

            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOODS_MOVEMENT_LIST, {
                goodsMovementItemsStore: goodsMovementItemsStore,
                poItemsStore: poItemsStore,
                miProcess: miProcess,
                reference: reference
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCommitButtonClick of BaseWeListPageController', ex);
        }
    },

    wrapGoodsMovementItems: function () {
        var retval = null;
        try {
            var myModel = this.getViewModel();
            var poItems = myModel.get('poItemsStore');
            var goodsMovementStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.GoodsMovementItem',
                autoLoad: false
            });

            poItems.each(function (poItem) {
                goodsMovementStore.add(this.createGoodsMovementItem(poItem));
            }, this);

            retval = goodsMovementStore;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCommitButtonClick of BaseWeListPageController', ex);
        }
        return retval;
    },

    createGoodsMovementItem: function (poItem) {
        var retval = null;
        try {
            var myModel = this.getViewModel();
            var miProcess = myModel.get('miProcess');
            var selectedLgort = myModel.get('selectedLgort');

            if (poItem.get('menge_ist') * 1 > 0 && poItem.get('elikz') === true) { //if the ammount delivered is bigger then 0 and entry is marked

                var goodsMovementItem = Ext.create('AssetManagement.customer.model.bo.GoodsMovementItem', {
                    matnr: poItem.get('matnr'),
                    werks: poItem.get('werks'),
                    lgort: selectedLgort.get('lgort'),
                    ebeln: poItem.get('ebeln'),
                    ebelp: poItem.get('ebelp'),
                    menge: poItem.get('menge_ist'),
                    meins: poItem.get('meins'),
                    item_text: poItem.get('txz01'),
                    bwart: miProcess.get('bwart'),
                    kzbew: miProcess.get('kzbew')
                });
            }
            retval = goodsMovementItem;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSaveClick of BaseWeListPageController', ex);
        }
        return retval;
    },

    onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();
            if(!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [ ];

            this.rowMenu.onCreateContextMenu(options, record);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseWeListPageController', ex);
        }
    },
    onPoNoTextKeypress: function (field, e, eOpts) {
        try {
            if (e.keyCode === 13) {
              Ext.getCmp('poScanReferenceNumber').focus();

            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
        }
    },

    onReferenceNoTextKeypress: function (field, e, eOpts) {
        try {
            if (e.keyCode === 13) {
               this.onSearchPoClick();
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
        }
    },

    onSearchPoClick: function (field, e, eOpts) {
        try {
            var me = this;
            this.getView().transferViewStateIntoModel();
            var myModel = this.getViewModel();
            var poNumber = myModel.get('poNumber');
            var poPos = myModel.get('poPos');
            var reference = myModel.get('reference');
            var selectedLgort = myModel.get('selectedLgort');
            var onlinePoGetDialogCallback = function (poItemsStore) {
                try {
                    if (poItemsStore && poItemsStore.getCount() > 0) {
                        poItemsStore.sort('ebelp', 'ACS');
                        myModel.set('poItemsStore', poItemsStore);
                        me._argumentsObject['poItemsStore'] = poItemsStore;
                        me.refreshView();
                    } else {
                      myModel.set('poItemsStore', null);
                      me.refreshView();
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOrderFound'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
                }
            };
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_PO_GET_DIALOG, {
                poNumber: poNumber,
                poPos: poPos,
                successCallback: onlinePoGetDialogCallback
            });
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseWeListPageController', ex);
        }
    },

    onDeliveredAmmountChange: function (field, value, value2, eOpts) {
        try {
            var record = field.getWidgetRecord();
            if (field.isValid()) {
                record.set('menge_ist', value);
            } else {
                //if it's not valid, it can't be considered delvier, so the record field has to be cleared and unmarked
                record.set('menge_ist', '');
                record.set('elikz', false);
            }
        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside onDeliveredAmmountChange of BaseWeListPageController', ex);
        }
    },

    onDeliveredCheckChange: function (checkbox, rowIndex, checked, record, e, eOpts) {
        try {
            var toDeliver = record.get('menge') * 1; //*1 menge is an int, so this conversion works
            var alreadyDelivered = (record.get('menge_del') ? record.get('menge_del') : 0) * 1;

            if (alreadyDelivered == toDeliver) //if all items has been already delivered before, the checkchange handling should be stopped
                return false;

            if (toDeliver === 1) { //if there is just one item, there is no numberfield, so marking it and unmarking the checkbox should handle the quantity field too
                if (checked)
                    record.set('menge_ist', '1');
                else
                    record.set('menge_ist', '0');
            }
        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside onDeliveredAmmountChange of BaseWeListPageController', ex);
        }
    }
});