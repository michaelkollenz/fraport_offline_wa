Ext.define('AssetManagement.base.controller.pages.BaseOrderListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.manager.OrderManager',
   		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.helper.OxLogger',
       'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
	],
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},

	mixins: {
	    handlerMixin: 'AssetManagement.customer.controller.mixins.OrderListPageControllerMixin'
	},

	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_ADD) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_ORDER, { });
	    	} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
	    	    this.onSearchButtonClicked();
	    	} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH) {
	    	    this.onClearSearchButtonClicked();
	    	}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseOrderListPageController', ex);
		}
	},
		
	//private
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = new Array();
			var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
			if (funcParaInstance.getValue1For('ord_create') === 'X') // && funcParaInstance.getValue1For('ext_scen_active') !== 'X') //ext_scen is customer specific!!
		    	this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_ADD);
		    	
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);
        } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseOrderListPageController', ex);
		}	   
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
		    this.loadListStore();
		
		    this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseOrderListPageController', ex);
		}    
	},
		
	loadListStore: function() {
		try {
			var orderStoreRequest = AssetManagement.customer.manager.OrderManager.getOrders(true, true);
			this.addRequestToDataBaseQueue(orderStoreRequest, 'orderStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseOrderListPageController', ex);
		}
	},
	
	//list handler
	onOrderSelected: function(tableview, td, cellIndex, record, tr, rowIndex, event, eOpts) {
	    try {
	  
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: record.get('aufnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderSelected of BaseOrderListPageController', ex);
		}
    },
	
	onOrderSelectedByTabHold: function(order) {
	},
	
	onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
   		try {
   			e.stopEvent();
				if(!this.rowMenu)
					this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
				var options = [ ];

				this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseOrderListPageController', ex);
		}
	},
	
	navigateToAddress: function(order) {
       try {
			var addressInformation = null;
			if(order) {
				addressInformation = order.get('address');
			}
				
			if(addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postCode1'))) {
				// AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);
				
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES});
	
				//generate the hint
				hint = AssetManagement.customer.utils.StringUtils.trimStart(order.get('aufnr'), '0');
				
				request.addAddressWithHint(addressInformation, hint);
				
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToAddress of BaseOrderListPageController', ex);
		}
    },
    
    beforeDataReady: function() {
    	try {
    		//apply the current filter criteria on the store, before it is transferred into the view
    		this.applyCurrentFilterCriteriasToStore();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseOrderListPageController', ex);
		}
	},
	
	applyCurrentFilterCriteriasToStore: function() {
	    try {       
			this.getView().getSearchValue();

			var orderToSearch = this.getViewModel().get('searchOrder');
			var orderStore = this.getViewModel().get('orderStore');
			
			if(orderStore) {
				orderStore.clearFilter();
				
				// Auftragsnummer
				if (orderToSearch.get('aufnr')) {

					orderStore.filterBy(function(record) {
						try {
						    if (record.get('aufnr').toUpperCase().indexOf(orderToSearch.get('aufnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Kurztext
				} else if(orderToSearch.get('ktext')) {
					orderStore.filterBy(function(record) {
						try {
						    if (record.get('ktext') && record.get('ktext').toUpperCase().indexOf(orderToSearch.get('ktext').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Meldungsnummer
				} else if(orderToSearch.get('qmnum')) {
					orderStore.filterBy(function(record) {
						try {
						    if (record.get('qmnum') && record.get('qmnum').toUpperCase().indexOf(orderToSearch.get('qmnum').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Equipment
				} else if(orderToSearch.get('equnr') || (orderToSearch.get('equipment') && orderToSearch.get('equipment').get('eqktx'))) {
					var tempEqunr = orderToSearch.get('equnr') ? orderToSearch.get('equnr').toUpperCase() : '';
					var tempEqktx = orderToSearch.get('equipment') ? orderToSearch.get('equipment').get('eqktx') : '';
					tempEqktx = tempEqktx ? tempEqktx.toUpperCase() : '';
				
					orderStore.filterBy(function(record) {
						try {
							if(tempEqunr && record.get('equnr') && record.get('equnr').toUpperCase().indexOf(tempEqunr) > -1) {
								return true; 
							} else if(tempEqktx && record.get('equipment') && record.get('equipment').get('eqktx').toUpperCase().indexOf(tempEqktx) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Techn. Platz
				} else if(orderToSearch.get('tplnr') || (orderToSearch.get('funcLoc') && orderToSearch.get('funcLoc').get('pltxt'))) {
					var tempTplnr = orderToSearch.get('tplnr') ? orderToSearch.get('tplnr').toUpperCase() : '';
					var tempPltxt = orderToSearch.get('funcLoc') ? orderToSearch.get('funcLoc').get('pltxt') : '';
					tempPltxt = tempPltxt ? tempPltxt.toUpperCase() : '';
				
					orderStore.filterBy(function(record) {
						try {
							if(tempTplnr && record.get('tplnr') && record.get('tplnr').toUpperCase().indexOf(tempTplnr) > -1) {
								return true; 
							} else if(tempPltxt && record.get('funcLoc') && record.get('funcLoc').get('pltxt').toUpperCase().indexOf(tempPltxt) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Verantw. Arbeitsplatz
				} else if(orderToSearch.get('vaplz')) {
					var criteria = orderToSearch.get('vaplz').toUpperCase();
				
					orderStore.filterBy(function(record) {
						try {
						    if(record.get('vaplz') && record.get('vaplz').toUpperCase().indexOf(criteria) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Sortierfeld
				} else if(orderToSearch.get('equipment') && orderToSearch.get('equipment').get('eqfnr')) {
					orderStore.filterBy(function(record) {
						try {
						    if(record.get('equipment') && record.get('equipment').get('eqfnr').toUpperCase().indexOf(orderToSearch.get('equipment').get('eqfnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});

                    // Datum
                } else if(orderToSearch.get('operations') && orderToSearch.get('operations').getCount() === 1 && orderToSearch.get('operations').getAt(0).get('ntanSearch')) {
                    orderStore.filterBy(function(record) {
                        try {
                        	var operation = orderToSearch.get('operations').getAt(0);
                        	var opNtan = operation.get('ntanSearch');
                        	
                        	var recNtan = record.getNtan();
                        	var recFsav = record.getFsav();

                        	if (recNtan && recNtan.indexOf(opNtan) > -1) {
                                return true;
                            } else if (recFsav && recFsav.indexOf(opNtan) > -1){
                                return true;
							}
                        } catch(ex) {
                            AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
                        }
                    });
                    // PLZ
                }



				// // Datum
				// } else if(orderToSearch.get('ntan')) {
				// 	orderStore.filterBy(function(record) {
				// 		try {
				// 			if (record.get('ntan') && AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(record.get('ntan')).indexOf(orderToSearch.get('ntan').toUpperCase()) > -1) {
				// 		    	return true;
				// 		    }
				// 	    } catch(ex) {
				// 			AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
				// 		}
				// 	});
				// // PLZ
				// }
				else if(orderToSearch.get('address') && orderToSearch.get('address').getPostalCode()) {
					orderStore.filterBy(function(record) {
						try {
							if(record.get('address') && record.get('address').getPostalCode().toUpperCase().indexOf(orderToSearch.get('address').getPostalCode().toUpperCase()) > -1) {
								return true;
							}
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				// Ort
				} else if(orderToSearch.get('address') && orderToSearch.get('address').getCity()) {
					orderStore.filterBy(function(record) {
						try {
							if(record.get('address') && record.get('address').getCity().toUpperCase().indexOf(orderToSearch.get('address').getCity().toUpperCase()) > -1) {
								return true;
							}
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseOrderListPageController', ex);
						}
					});
				}
			}
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseOrderListPageController', ex)
		}
	},

    onOrderListPageSearchFieldChange: function() {
        try {
            
			this.applyCurrentFilterCriteriasToStore();
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderListPageSearchFieldChange of BaseOrderListPageController', ex)
		}
    },
    //functions for search
    //public
    onSearchButtonClicked: function () {
        try {
            var myModel = this.getViewModel();

            this.manageSearchCriterias();

            var searchDialogArgs = {
                source: myModel.get('orderStore'),
                criterias: myModel.get('searchCriterias')
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseOrderListPageController', ex);
        }
    },

    //private
    //handles initialization of search criterias
    manageSearchCriterias: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');
            //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
            var searchMixin = this.mixins.handlerMixin

            if (!currentSearchCriterias) {
                var gridpanel = Ext.getCmp('orderGridPanel');

                //var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray);
                var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray, searchMixin);

                myModel.set('searchCriterias', newCriterias);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseOrderListPageController', ex);
        }
    },
    ////functions for search
    ////private
    ////handles initialization of search criterias
    //manageSearchCriterias: function () {
    //    try {
    //        var myModel = this.getViewModel();
    //        var currentSearchCriterias = myModel.get('searchCriterias');
    //        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
    //        var searchMixin = this.mixins.handlerMixin

    //        if (!currentSearchCriterias) {
    //            var searchArray = [{
    //                id: 'aufnr',
    //                columnName: 'order',
    //                searchFunction: '',
    //                position: 1
    //            },
	//            {
	//                id: 'equnr',
	//                columnName: 'equipment',
	//                searchFunction: '',
	//                position: 1
	//            },
    //            {
    //                id: 'tplnr',
    //                columnName: 'funcLoc_Short',
    //                searchFunction: '',
    //                position: 1
    //            },
	//            {
	//                id: 'qmnum',
	//                columnName: 'notif',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            {
	//                id: 'postalCode',
	//                columnName: 'address',
	//                searchFunction: '',
	//                position: 1
	//            },
    //            ];

    //            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(searchArray, searchMixin);

    //            myModel.set('searchCriterias', newCriterias);
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseOrderListPageController', ex);
    //    }
    //},

    //functions for search
    //public
    onSearchApplied: function (searchResult) {
        try {

            this.getViewModel().set('constrainedOrders', searchResult);

            this.setupInitialSorting();

            //this.setupAutoCompletion();

            this.applyCurrentFilterCriteriasToStore();

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of BaseOrderListPageController', ex);
        }
    },

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
    setupInitialSorting: function () {
        try {
            var myModel = this.getViewModel();
            var constrainedOrders = myModel.get('constrainedOrders');

            //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
                myModel.set('sortProperty', 'mymAgreement');
                myModel.set('sortDirection', 'DESC');
            }

            //apply the current sort rule
            if (constrainedOrders) {
                constrainedOrders.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
                ]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseOrderListPageController', ex);
        }
    },


    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias'))) {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias    
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseOrderListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/clear_search.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseOrderListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseOrderListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseOrderListPageController', ex);
        }
    },
    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of BaseOrderListPageController', ex);
        }
    }
});