Ext.define('AssetManagement.base.controller.pages.BaseFuncLocDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',


	requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.FuncLocManager',
		'AssetManagement.customer.manager.DMSDocumentManager',
		'AssetManagement.customer.manager.DocUploadManager',
		'AssetManagement.customer.model.bo.FuncLoc',
		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.manager.HistOrderManager',
		'AssetManagement.customer.manager.HistNotifManager',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],

	config: {
		contextReadyLevel: 1
	},

	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_EDIT) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_EDIT, { funcLoc: this.getViewModel().get('funcLoc') });
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWORDER) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_ORDER, { funcLoc: this.getViewModel().get('funcLoc') });
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWNOTIF) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_NOTIF, { funcLoc: this.getViewModel().get('funcLoc') });
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_OBJ_STRUCTURE) {
				AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_OBJ_STRUCTURE, { funcLoc: this.getViewModel().get('funcLoc') });
			} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT || optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST) {
			    this.showLongtextDialog();
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseFuncLocDetailPageController', ex);
		}
	},

	//protected
	//@override
	onCreateOptionMenu: function() {
		this._optionMenuItems = new Array();
		var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
	    try {

	        var myFuncLoc = this.getViewModel().get('funcLoc');
	        var funcLocBLT = myFuncLoc.get('backendLongtext');
	        var funcLocLLT = myFuncLoc.get('localLongtext');

		    if (funcParaInstance.getValue1For('floc_change') === 'X')
				this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_EDIT);

			this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEWORDER);
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEWNOTIF);
		    this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_OBJ_STRUCTURE);

		    if(AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ltxt_iflo') === 'X'){
		    	if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(funcLocBLT) || !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(funcLocLLT))
		        	this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST);
			    else
			        this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT);
			}
		   
	    } catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseFuncLocDetailPageController', ex);
		}
	},

	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			var tplnr = '';

			if(argumentsObject['tplnr'] && typeof argumentsObject['tplnr'] === 'string') {
				tplnr = argumentsObject['tplnr'];
			} else if(argumentsObject['funcLoc'] && Ext.getClassName(argumentsObject['funcLoc']) === 'AssetManagement.customer.model.bo.FuncLoc') {
				this.getViewModel().set('funcLoc', argumentsObject['funcLoc']);
			} else {
				throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.pages.FuncLoDetailPageController'
				});
			}

			var tplnrPassed = tplnr !== '';

			if(tplnrPassed) {
				this.getFuncLoc(tplnr);
			} else {
				this.waitForDataBaseQueue();
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseFuncLocDetailPageController', ex);
		    this.errorOccurred();
		}
	},

	getFuncLoc: function(tplnr) {
		try {
			var funcLocRequest = AssetManagement.customer.manager.FuncLocManager.getFuncLoc(tplnr, true);
			this.addRequestToDataBaseQueue(funcLocRequest, 'funcLoc');

			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFuncLoc of BaseFuncLocDetailPageController', ex)
		}
	},

    //protected
    //@override
	beforeDataReady: function () {
	    try {

	        var myModel = this.getViewModel();
	        var funcLoc = myModel.get('funcLoc');
	        var classifications = funcLoc ? funcLoc.get('classifications') : null;

	        var preparedClassifications = Ext.create('Ext.data.Store', {
	            model: 'AssetManagement.customer.model.bo.Charact',
	            autoLoad: false
	        });

	        if (classifications && classifications.getCount() > 0) {
	            classifications.each(function (objClass) {
					var characteristics = objClass.get('characts');
					if(characteristics && characteristics.getCount() > 0){
						characteristics.each(function (charact) {
							preparedClassifications.add(charact);
						});
					}
	            });
	        }

	        myModel.set('preparedClassifications', preparedClassifications);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseFuncLocDetailPageController', ex);
	    }
	},

    //private
	showLongtextDialog: function () {
	    try {
	    	var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('c_ltxt_iflo') === '';
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { funcLoc: this.getViewModel().get('funcLoc'), readOnly: readOnly });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialog of BaseFuncLocDetailPageController', ex);
	    }
	},

    //public
	onLongtextSaved: function (affectedObject, newLongtext) {
	    try {
	        if (affectedObject && affectedObject === this.getViewModel().get('funcLoc')) {
	            this.rebuildOptionsMenu();
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLongtextSaved of BaseFuncLocDetailPageController', ex);
	    }
	},

	onClassificationSelected: function (tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
	    try {
	        var myModel = this.getViewModel();
	        var equi = myModel.get('funcLoc');
	        var classifications = equi.get('classifications');
	        var objClassOfCharact = null;
	        // filtered the charact to show the correct dialog

	        classifications.each(function (objClass) {
	            if (objClass.get('clint') === record.get('clint')) {
	                objClass.get('characts').each(function (charact) {
	                    if (charact === record) {
	                        objClassOfCharact = objClass;
	                    }
	                });
	                if (record.get('atfor') === 'CHAR' && record.get('atein') === '' && record.get('atint') === '' && record.get('atwme') === 'X') {
	                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_MULTICHECKBOX_DIALOG, { objClass: objClassOfCharact, charact: record });
	                } else if (record.get('atfor') === 'CHAR' && record.get('atein') === '' && record.get('atint') === '' && record.get('atwme') === '') {
	                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_APPENDCHECKBOX_DIALOG, { objClass: objClassOfCharact, charact: record });
	                } else if (record.get('atein') === 'X' && record.get('atwme') === '' || record.get('atein') === 'X' && record.get('atint') === 'X' && record.get('msehi') === 'KWH') {
	                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_SINGULARLINE_DIALOG, { objClass: objClassOfCharact, charact: record });
	                } else if (record.get('atein') === 'X' && record.get('atint') === 'X' && record.get('atwme') === ''
                        || record.get('atein') === 'X' && record.get('atwme') === 'X' && record.get('atfor') === 'CHAR') {
	                    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CLASSEDIT_SINGULARCHECKBOX_DIALOG, { objClass: objClassOfCharact, charact: record });
	                }
	            }
	        });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassificationSelected of BaseFuncLocDetailPageController', ex)
	    }
	},

	openFuncLocAddress: function() {
		try {
			var address = this.getViewModel().get('funcLoc').get('address');

			if(address && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(address.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(address.get('postCode1'))) {
				//AssetManagement.customer.helper.NetworkHelper.openAddress(address);
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
				});

				//generate the hint
				funcLoc = this.getViewModel().get('funcLoc')
				hint = funcLoc.get('tplnr');
				request.addAddressWithHint(address, hint);

				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });

			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside openFuncLocAddress of BaseFuncLocDetailPageController', ex);
		}
	},

	onFileSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
		    //if the record represents an DMS document, download it from SAP system
		    //else try open its binary content
		    if (record.get('fileOrigin') === 'online') {
		        AssetManagement.customer.manager.DMSDocumentManager.downloadDMSDocument(record);
		    } else {
		        AssetManagement.customer.manager.DocUploadManager.openDocument(record);
		    }
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileSelected of BaseFuncLocDetailPageController', ex);
		}
   	},

   	onFileForDocUploadSelected: function(file) {
		try {
			if(!file) {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}

			var funcLoc = this.getViewModel().get('funcLoc');

			var callback = function(docUpload) {
				try {
					if(docUpload)
					{
						//not neccessary, because the documents store of the funcLoc will automatically refresh itself
						//the docUpload will be added to this store by the manager
						//this.getView().refreshView();

						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileAdded'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}

				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseFuncLocDetailPageController', ex);
				}
			};

			var addEvent = AssetManagement.customer.manager.DocUploadManager.addNewDocUploadForFuncLoc(file, funcLoc);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(addEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseFuncLocDetailPageController', ex);
		}
	},

	deleteDocUploadClicked: function(docUpload) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseFuncLocDetailPageController', ex);
   				}
   			};

   			var deleteEvent = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(docUpload);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseFuncLocDetailPageController', ex);
		}
	},

	histOrderSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HIST_ORDER_DETAIL, { histOrder: record });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histOrderSelected of BaseFuncLocDetailPageController', ex)
		}
	},

    histNotifSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
	   try {
		  AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HIST_NOTIF_DETAIL, { histNotif: record });
	   } catch(ex) {
		  AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside histNotifSelected of BaseFuncLocDetailPageController', ex)
	   }
	},

	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenuFileList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			if(record.get('fileOrigin') === 'local') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}

			this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuFileList of BaseFuncLocDetailPageController', ex);
		}
    },

    onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseFuncLocDetailPageController', ex);
		}
    },

    onContextMenuItemSelected: function(itemID, record) {
       try {
    	   if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
    		   var className = Ext.getClassName(record);

    		   if(className.indexOf('DocUpload') > -1) {
    			   this.deleteDocUploadClicked(record);
    		   }
    	   }
       } catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseFuncLocDetailPageController', ex);
       }
    },

    //list handler
    measPointSelected: function (tableview, td, cellIndex, record, tr, rowIndex, event, eOpts) {
        try {
            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_MEAS_DOC_EDIT, { measPoint: record });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMeasPointSelected of BaseFuncLocDetailPageController', ex);
        }
    },
    ///------ CONTEXT MENU END -------///

    onClassValueSaved: function () {
        try {

            this.getView().refreshView();

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSaved of BaseFuncLocDetailPageController', ex);
        }
    },
    onClassValuesSaved: function (classValue) {
	    try {

	        this.getView().refreshView();

	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClassValuesSaved of BaseFuncLocDetailPageController', ex);
	    }
	}
});
