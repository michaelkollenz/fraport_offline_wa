Ext.define('AssetManagement.base.controller.pages.BaseMatConfPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

    
    requires: [
	   'AssetManagement.customer.manager.WorkCenterManager',
	   'AssetManagement.customer.manager.Cust_002Manager',
	   'AssetManagement.customer.utils.NumberFormatUtils',
	   'AssetManagement.customer.model.helper.ReturnMessage',
	   'AssetManagement.customer.helper.OxLogger'
    ],
               
	config: {
		contextReadyLevel: 2,
		rowMenu: null
	},
	
	//private
	_saveRunning: false,
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
					
				this.trySaveMatConf();
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM) {
				if(this._saveRunning === true)
					return;
					
				this.initializeNewMatConf();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseMatConfPageController', ex);
		}
	},
		
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
			this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_NEWITEM, AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseMatConfPageController', ex);
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			//extract parameters, if any given
			//if strings are passed, load business objects them with passedStringsParametersToBOs
			//else proceed first database query
			var myModel = this.getViewModel();
		
			if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
				myModel.set('order', argumentsObject['order']); 
			} else {
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.HistOrderDetailPageController'
			    });
			}
			
			if(Ext.getClassName(argumentsObject['operation']) === 'AssetManagement.customer.model.bo.Operation') {
				myModel.set('operation', argumentsObject['operation']); 
			}
			
			if(Ext.getClassName(argumentsObject['matConf']) === 'AssetManagement.customer.model.bo.MaterialConf') {
				myModel.set('matConf', argumentsObject['matConf']);
				myModel.set('isEditMode', true);
			}
			
			this.waitForDataBaseQueue();
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseMatConfPageController', ex);
		    this.errorOccurred();
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		try {
			if(this._contextLevel === 1) {
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseMatConfPageController', ex);
		}
	},
			
	performContextLevel1Requests: function() {
		try {
			this.getMatConfDependendData();
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseMatConfPageController', ex);
		}
	},
	
	getMatConfDependendData: function() {
		try {
			var accountIndicationsRequest = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
			this.addRequestToDataBaseQueue(accountIndicationsRequest, 'accountIndications');
			
			var storLocsRequest = AssetManagement.customer.manager.Cust_002Manager.getCust_002s(true);
			this.addRequestToDataBaseQueue(storLocsRequest, 'storLocs');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMatConfDependendData of BaseMatConfPageController', ex);
		}
	},
	
	//protected
	//@override
	beforeDataReady: function() {
		try {
			//set the operation according to the current data
			var myModel = this.getViewModel();
			
			var curOperation = myModel.get('operation');
			var curMatConf = myModel.get('matConf');
			
			if(!curOperation) {
				var operations = myModel.get('order').get('operations');
			
				if(operations && operations.getCount() > 0) {
					var operationToSet = null;
				
					if(curMatConf) {
						//set the operation convenient to curMatconf
						var matConfsVornr = curMatConf.get('vornr');
						
						operations.each(function(operation) {
							if(operation.get('vornr') === matConfsVornr) {
								operationToSet = operation;
								return false;
							}
						}, this);
					} else {
						//set to first operation
						operationToSet = operations.getAt(0);
					}
					
					myModel.set('operation', operationToSet);
				}
			}
		
			//if no matconf is currently set, initialize a new one
			if(!curMatConf)
				this.initializeNewMatConf(true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseMatConfPageController', ex);
		}
	},
	
	//will initialize a new material conf. and transfer it into the model
	//the view will be updated by default - use skipRefreshView to prevent this
	initializeNewMatConf: function(skipRefreshView) {
		try {
			if(skipRefreshView === true)
				skipRefreshView = true;
			else
				skipRefreshView = false;
		
			var myModel = this.getViewModel();
			var order = myModel.get('order');
			var operation = myModel.get('operation');
		
			var newMatConf = Ext.create('AssetManagement.customer.model.bo.MaterialConf', { order: order, operation: operation });
			newMatConf.set('moveType', '261');
			newMatConf.set('isdd', AssetManagement.customer.utils.DateTimeUtils.getCurrentDate());
			newMatConf.set('unit', 'ST');
			newMatConf.set('quantity', '1');
			
			//set a default werk, if non yet set - using first entry of the storage locations
			//this will work, as long as only one plant is customized
			if(AssetManagement.customer.utils.StringUtils.isNullOrEmpty(newMatConf.get('werks'))) {
				var storLocs = myModel.get('storLocs');
				
				if(storLocs && storLocs.getCount() > 0) {
					newMatConf.set('werks', storLocs.getAt(0).get('werks'));
				}
			}
		
			myModel.set('matConf', newMatConf);
			myModel.set('chosenMaterial', null);
			myModel.set('isEditMode', false);
			
			if(skipRefreshView === false)
				this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewMatConf of BaseMatConfPageController', ex);
		}
	},
	
	onPlantTextChanged: function(field, e, eOpts ) {
		try {
			this.getView().fillStorageLocationComboBox(field.getValue()); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onPlantTextChanged of BaseMatConfPageController', ex);
		}
	},
	
	//public
	onOperationSelected: function(comboBox, newValue, oldValue) {
		try {
			var myModel = this.getViewModel();
			var currentOperation = myModel.get('operation');
			var selectedOperation = comboBox.getValue();
			
			if(selectedOperation !== currentOperation) {
				operation = selectedOperation; 
				myModel.set('operation', selectedOperation);
				this.initializeNewMatConf();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseMatConfPageController', ex);
		}
	},
	
	//will perform a set of checks on the current input values before those values will be transferred into the current matConf and saved to the database
	trySaveMatConf: function(){
		try {
			this._saveRunning = true;
			
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			
			var me = this;
			
			var afterInputCheckAction = function(inputCheckWasSuccessfull) {
				try {
					if(inputCheckWasSuccessfull === true) {
						var afterStorLocQuantityCheckAction = function(doContinue) {
							try {
								if(doContinue === true) {
									//get matconf to save
									var matConf = me.getViewModel().get('matConf');
									
									me.getView().transferViewStateIntoModel();
									matConf.set('unit', matConf.get('unit').toUpperCase());
									
									var chosenMaterial = me.getViewModel().get('chosenMaterial');
									matConf.set('material', chosenMaterial ? chosenMaterial : null);
								
									var callback = function(success) {
										try {
											if(success === true) {
												me.addMatConfToListStore(matConf);
												me.initializeNewMatConf();
												
												var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('savedMatConf'), true, 0);
												AssetManagement.customer.core.Core.getMainView().showNotification(notification);
											} else {
												var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingMatConf'), false, 2);
												AssetManagement.customer.core.Core.getMainView().showNotification(notification);
											}
										} catch(ex) {
											AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of BaseMatConfPageController', ex)
										} finally {
											me._saveRunning = false;
											AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
										}
									};
									
									var eventId = AssetManagement.customer.manager.MatConfManager.saveMatConf(matConf);
									
									if(eventId > 0) {
										AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
									} else {
										me._saveRunning = false;
										AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
										var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingMatConf'), false, 2);
										AssetManagement.customer.core.Core.getMainView().showNotification(notification);
									}
								} else {
									var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('saveCancelled'), true, 1);
									AssetManagement.customer.core.Core.getMainView().showNotification(message);
								
									me._saveRunning = false;
									AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								}
							} catch(ex) {
								me._saveRunning = false;
								AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
								AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of BaseMatConfPageController', ex)
							}
						};
					
						//second perform a check against the selected storlocs quantity
						me.performStorLocsQuantityCheck(afterStorLocQuantityCheckAction, me);
					} else {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					}
				} catch(ex) {
					me._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of BaseMatConfPageController', ex)
				}
			};
			
			//first check inpit values
			this.checkInputValues(afterInputCheckAction, this);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveMatConf of BaseMatConfPageController', ex);
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
		}
	},
	
	//performs a set of checks, returning the overall result by the provided callback
	checkInputValues: function(callback, callbackScope)  {
		try {
			var retval = true; 
			var errorMessage = '';
			
			var values = this.getView().getCurrentInputValues(); 
		
			var vornr = values.vornr;
			var isdd = values.isdd;
			var matnr = values.matnr;
			var unit = values.unit;
		    var quantity = values.quantity;
			var plant = values.plant;
			var storLoc = values.storLoc;	
		    var accountIndication = values.accountIndication;
		
			var isNullOrEmpty = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
		 
			if(isNullOrEmpty(plant)) {
				errorMessage = Locale.getMsg('providePlant');
				retval = false;
			} else if(isNullOrEmpty(matnr)) {
				errorMessage = Locale.getMsg('selectMaterial');
				retval = false;
			} else if(isNullOrEmpty(storLoc)) {
				errorMessage = Locale.getMsg('provideStorageLocation');
				retval = false;
			} else if(quantity === null || quantity === undefined || quantity <= 0) {
	            errorMessage = Locale.getMsg('provideValidQuantity');
	            retval = false;
			} else if(!accountIndication) {
				errorMessage = Locale.getMsg('chooseAccountIndication');
				retval = false;
			}
			
            var me = this;
			var returnFunction = function() {
				if(retval === false) {
					var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(message);
				}
				
				if(callback)
					callback.call(callbackScope ? callbackScope : me, retval);
			};
			
			if(retval === false) {
				returnFunction.call(this);
			} else {
				//last do the material check - this means:
				//if the chosen material does no longer match the matnr from the input value, try to load the now connected material
				//if this material exist and has a diffrent unit, return false, inform the user and tell him, what the correct unit is
				//if the material could not be loaded, assume that it exists at the sap system and pass the check
				var chosenMaterial = this.getViewModel().get('chosenMaterial');
				
				var unitCheckFunction = function(material) {
					try {
						if(material) {
							if(material.get('meins').toUpperCase() !== unit.toUpperCase()) {
								errorMessage = AssetManagement.customer.utils.StringUtils.format(Locale.getMsg('unitDoesNotMatchMaterial'), [ material.get('meins') ]);
								retval = false;
							}
						}
						
						me.getViewModel().set('chosenMaterial', material);
						returnFunction.call(me);
					} catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseMatConfPageController', ex)
						
						if(callback) {
							callback.call(callbackScope ? callbackScope : me, false);
						}
					}
				};
				
				if(chosenMaterial && chosenMaterial.get('matnr') === matnr) {
					unitCheckFunction.call(this, chosenMaterial);
				} else {
					//load material from database
					var eventId = AssetManagement.customer.manager.MaterialManager.getMaterial(matnr);
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, unitCheckFunction);
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseMatConfPageController', ex);
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	//checks the current set amount in the view against the available of the current selected storage Location
	//returns a boolean, indicating, if this check has been passed
	performStorLocsQuantityCheck: function(callback, callbackScope) {
		try {
			var values = this.getView().getCurrentInputValues(); 
			
			var matnr = values.matnr;
			var quantity = values.quantity;
			var plant = values.plant;
			var storLoc = values.storLoc;
			var material = this.getViewModel().get('chosenMaterial');
			
			var returnFunction = function(retval) {
				if(callback)
					callback.call(callbackScope ? callbackScope : me, retval);
			};
		
			var me = this;
				
			var quanitityCheckResultCallback = function(quantityResult) {
				try {
					if(quantityResult == Number.NaN) {
						var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorWhileCheckingAvailableStorLocQuant'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(message);
						
						returnFunction.call(me, false);
					} else if(quantityResult == 0 || quantityResult == 2) {
						//user has to be informed and asked for proceeding
						var innerCallback = function(confirmed) {
							try {
								if(confirmed === false) {
									returnFunction.call(me, false);
								} else {
									AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
									returnFunction.call(me, true);
								}
							} catch(ex) {
					        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseMatConfPageController', ex);
					        	
					        	returnFunction.call(me, false);
					       	}    				
						};
						
						var dialogMessage = quantityResult == 0 ? Locale.getMsg('storLocQuantLessRequested') : Locale.getMsg('storLocEmpty');
						dialogMessage += '<br>' +  Locale.getMsg('postAnywayQuestion');
						
						var dialogArgs = {
						    title: Locale.getMsg('caution'),
						    icon: 'resources/icons/alert.png',
							message: dialogMessage,
							asHtml: true,
							callback: innerCallback,
							scope: this
						};
						
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
					} else {
						returnFunction.call(me, true);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValues of BaseMatConfPageController', ex)
					returnFunction.call(me, false);
				}
			};
			
			var matnrToUse = material ? material.get('matnr') : matnr;
			var requestedQuantity = AssetManagement.customer.utils.NumberFormatUtils.parseNumber(quantity);

			var eventId = AssetManagement.customer.manager.MaterialStockageManager.checkForAvailableQuantity(matnrToUse, plant, storLoc, requestedQuantity);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, quanitityCheckResultCallback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performStorLocsQuantityCheck of BaseMatConfPageController', ex)
			
			if(callback) {
				callback.call(callbackScope ? callbackScope : this, false);
			}
		}
	},
	
	deleteMatConf: function(matConf) {
		try {
			var currentMatConf = this.getViewModel().get('matConf');
			var deletingCurrentEditingMatConf = matConf.get('mblnr') === currentMatConf.get('mblnr')
													&& matConf.get('mjahr') === currentMatConf.get('mjahr')
														&& matConf.get('aufnr') === currentMatConf.get('aufnr');
				
			var me = this;
			var callback = function(success) {
				try {
					if(success === true) {
 						if(deletingCurrentEditingMatConf)
							me.initializeNewMatConf();
							
						me.deleteMatConfFromListStore(matConf);
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('matConfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingMatConf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConf of BaseMatConfPageController', ex);
				}
			};
			
			var eventId = AssetManagement.customer.manager.MatConfManager.deleteMatConf(matConf);
			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingMatConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConf of BaseMatConfPageController', ex);
		}
	},
	
	editMatConf: function(matConf) {
		try {
			var myModel = this.getViewModel();
		
			myModel.set('matConf', matConf);
			myModel.set('chosenMaterial', matConf.get('material'));
			myModel.set('isEditMode', true);
			
			this.getView().refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editMatConf of BaseMatConfPageController', ex);
		}
	},
	
	addMatConfToListStore: function(matConf) {
		try {
			var matConfs = this.getViewModel().get('operation').get('matConfs');
			
			if(!matConfs)
			{
				matConfs = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.MaterialConf',
					autoLoad: false
				});
				this.getViewModel().get('operation').set('matConfs', matConfs); 
			}
			
			var temp = matConfs.getById(matConf.get('id'));
			
			if(temp) {
				var index = matConfs.indexOf(temp);
				matConfs.removeAt(index);
				matConfs.insert(index, matConf);
			} else {
				matConfs.insert(0, matConf);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addMatConfToListStore of BaseMatConfPageController', ex);
		}
	},
	
	deleteMatConfFromListStore: function(matConf) {
		try {
			var matConfs = this.getViewModel().get('operation').get('matConfs');
			var temp = matConfs.getById(matConf.get('id'));
			
			if(temp)
				matConfs.remove(matConf);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConfFromListStore of BaseMatConfPageController', ex);
		}
	},
	
	onMaterialSelected: function(selectedObject) {
		try {
			var material = null;
			var viewTransferMethod = null;
			
			var typeOfSelectedObject = Ext.getClassName(selectedObject);
			
			//distinguish the type (diffrent methods)
			if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.Stpo') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().fillMaterialSpecFieldsFromStpo;
			} else if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.MatStock') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().fillMaterialSpecFieldsFromMatStock;
			} else if(typeOfSelectedObject === 'AssetManagement.customer.model.bo.OrdComponent') {
				material = selectedObject.get('material');
				viewTransferMethod = this.getView().fillMaterialSpecFieldsFromComponent;
			} else if (typeOfSelectedObject === 'AssetManagement.customer.model.bo.Material') {
			    material = selectedObject;
			    viewTransferMethod = this.getView().fillMaterialSpecFieldsFromMaterial;
			}
			
			this.getViewModel().set('chosenMaterial', material);
			
			if(viewTransferMethod) {
				viewTransferMethod.call(this.getView(), selectedObject);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialSelected of BaseMatConfPageController', ex);
		}
	},
	
	onSelectMaterialClick: function(){
		try {
            var myModel = this.getViewModel();
            var order = myModel.get('order');
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.MATERIALPICKER_DIALOG, { order: order });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSelectMaterialClick of BaseMatConfPageController', ex);
		}
	},

	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ){
		try {
			e.stopEvent(); 
			
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			if(record.get('updFlag') === 'I') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
			}
				
			this.rowMenu.onCreateContextMenu(options, record); 
			
			if(options.length > 0) {
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
			
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseMatConfPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
		
				this.deleteMatConf(record);
			}
			else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
				this.editMatConf(record); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseMatConfPageController', ex);
		}
	}
	///------ CONTEXT MENU END -------///
});