Ext.define('AssetManagement.base.controller.pages.BaseMaterialListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
        'AssetManagement.customer.controller.ToolbarController',
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.manager.MaterialStockageManager',
        'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
    ],

    mixins: {
        handlerMixin: 'AssetManagement.customer.controller.mixins.MaterialListPageControllerMixin'
    },
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},

    //public
    //@override
	onOptionMenuItemSelected: function (optionID) {
	    try {
	        if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
	            this.onSearchButtonClicked();
	        } else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH) {
	            this.onClearSearchButtonClicked();
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of EquipmentListPageController', ex)
	    }
	},

    //private
	onCreateOptionMenu: function () {
	    this._optionMenuItems = new Array();
	    try {
	        this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH);
	        this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of EquipmentListPageController', ex)
	    }
	},
	startBuildingPageContext: function(argumentsObject) {
		try {
			this.loadListStore();
		
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of MaterialListController', ex);
		}
	},
		
	loadListStore: function() {
		try {
			var unplanned = AssetManagement.customer.manager.MaterialStockageManager.getMatStocks(true);
			this.addRequestToDataBaseQueue(unplanned, "matStocks");
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of MaterialListController', ex);
		}
	}, 
	
	onDemandRequirementClick: function (tableview, td, cellIndex,rowIndex , tr, record, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_BANF_EDIT, { material: record });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onDemandRequirementClick of MaterialListController', ex);
		}
	},
	
	beforeDataReady: function() {
    	try {
    		//apply the current filter criteria on the store, before it is transferred into the view
    		this.applyCurrentFilterCriteriasToStore();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of MaterialListController', ex);
		}
	},

	applyCurrentFilterCriteriasToStore: function() {
    	try {
			this.getView().getSearchValue();
			
			var mtStockToSearch = this.getViewModel().get('searchMatStock');
			var matStocks = this.getViewModel().get('matStocks');
			
			if(matStocks) {
				matStocks.clearFilter();
			
				// Material
				if(mtStockToSearch.get('material').get('matnr')) {
					matStocks.filterBy(function (record) {
						try {
						    if (record.get('material').get('matnr').toUpperCase().indexOf(mtStockToSearch.get('material').get('matnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialListPageController', ex);
						}
					});
				// Kurztext
				} else if(mtStockToSearch.get('material').get('maktx')) {
					matStocks.filterBy(function (record) {
						try {
						    if (record.get('material').get('maktx').toUpperCase().indexOf(mtStockToSearch.get('material').get('maktx').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialListPageController', ex);
						}
					});
				//  Menge
				} else if(mtStockToSearch.get('labst')) {
					matStocks.filterBy(function (record) {
						try {
							var amount = AssetManagement.customer.utils.NumberFormatUtils.formatBackendValueForDisplay(record.get('labst'));
						    if (amount.indexOf(mtStockToSearch.get('labst').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialListPageController', ex);
						}
					});
				// Einheit
				} else if(mtStockToSearch.get('meins')) {
				matStocks.filterBy(function (record) {
					try {
					    if (record.get('meins').toUpperCase().indexOf(mtStockToSearch.get('meins').toUpperCase()) > -1) {
					    	return true; 
					    }
				    } catch(ex) {
						AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseMaterialListPageController', ex);
					}
				});
				// Werk / Lagerort
				} else if(mtStockToSearch.get('werks') || mtStockToSearch.get('lgort')) {
					matStocks.filterBy(function (record) {
						try {
							if(record.get('werks').toUpperCase().indexOf(mtStockToSearch.get('werks').toUpperCase()) > -1 ||
							   record.get('lgort').toUpperCase().indexOf(mtStockToSearch.get('lgort').toUpperCase()) > -1	) {
								return true;
							}
						} catch(ex) {
							AssetManagement.customer.helper.OxLagger.logException('Exception occured while filtering in BaseMaterialListPageController', ex);
						}
					});
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseMaterialListPageController', ex)
		}
    },
    
    onMaterialListPageSearchFieldChange: function() {
    	try {
			this.applyCurrentFilterCriteriasToStore();
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMaterialListPageSearchFieldChange of BaseMaterialListPageController', ex)
		}
    },
    
    onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
   		try {
   			e.stopEvent();
   			
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseMaterialListPageController', ex);
		}
    },

    //functions for search
    //public
    onSearchButtonClicked: function () {
        try {
            var myModel = this.getViewModel();

            this.manageSearchCriterias();

            var searchDialogArgs = {
                source: myModel.get('matStocks'),
                criterias: myModel.get('searchCriterias')
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseMaterialListPageController', ex);
        }
    },

    //functions for search
    //private
    //handles initialization of search criterias
    manageSearchCriterias: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');
            //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
            var searchMixin = this.mixins.handlerMixin

            if (!currentSearchCriterias) {
                var searchArray = [{
                    id: 'matnr',
                    columnName: 'material',
                    searchFunction: '',
                    position: 1
                },
	            {
	                id: 'labst',
	                columnName: 'orderComponents_Qty',
	                searchFunction: '',
	                position: 1
	            },
                {
                    id: 'werks',
                    columnName: 'plantStorLoc',
                    searchFunction: '',
                    position: 1
                }
                ];

                var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(searchArray, searchMixin);

                myModel.set('searchCriterias', newCriterias);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseMaterialListPageController', ex);
        }
    },

    //functions for search
    //public
    onSearchApplied: function (searchResult) {
        try {

            this.getViewModel().set('constrainedMaterials', searchResult);

            this.setupInitialSorting();

            //this.setupAutoCompletion();

            this.applyCurrentFilterCriteriasToStore();

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of BaseMaterialListPageController', ex);
        }
    },

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
    setupInitialSorting: function () {
        try {
            var myModel = this.getViewModel();
            var constrainedMaterials = myModel.get('constrainedMaterials');

            //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
                myModel.set('sortProperty', 'mymAgreement');
                myModel.set('sortDirection', 'DESC');
            }

            //apply the current sort rule
            if (constrainedMaterials) {
                constrainedMaterials.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
                ]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseMaterialListPageController', ex);
        }
    },


    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias'))) {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias    
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseMaterialListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/clear_search.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseMaterialListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseMaterialListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseMaterialListPageController', ex);
        }
    },
    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of BaseMaterialListPageController', ex);
        }
    }
});