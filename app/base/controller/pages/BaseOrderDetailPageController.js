Ext.define('AssetManagement.base.controller.pages.BaseOrderDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',


    requires: [
     	'AssetManagement.customer.controller.ToolbarController',
    	'AssetManagement.customer.utils.StringUtils',
    	'AssetManagement.customer.helper.StoreHelper',
    	'AssetManagement.customer.manager.AddressManager',
		'AssetManagement.customer.manager.DMSDocumentManager',
		'AssetManagement.customer.manager.DocUploadManager',
		'AssetManagement.customer.manager.MatConfManager',
        'AssetManagement.customer.manager.QPlosManager',
		'AssetManagement.customer.manager.BanfManager',
        'AssetManagement.customer.manager.OrderManager',
		'AssetManagement.customer.model.pagemodel.OrderDetailPageViewModel',
		'AssetManagement.customer.model.bo.Order',
		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.manager.Cust_001Manager',
		'AssetManagement.customer.modules.googleMaps.GoogleMapsRequest',
		'AssetManagement.customer.model.helper.ReturnMessage'
    ],

    config: {
    	contextReadyLevel: 1,
    	rowMenu: null
    },

    mixins: {
        handlerMixin: 'AssetManagement.customer.controller.mixins.OrderDetailPageControllerMixin'
    },

    _objectsToPrint: [],

    //public
    onOptionMenuItemSelected: function(optionID) {
    	try {
	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT || optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST) {
				this.showLongtextDialogFromOrder();
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEW_SDORDER) {
                var order = this.getViewModel().get('order');
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_SDORDER, { aufnr: order.get('aufnr') });
			} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_DELETE) {
			    if (this._deleteRunning === true)
			        return;

			    this.deleteOrder();
			}
		} catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseOrderDetailPageController', ex);
		}
    },

    //@override
    onCreateOptionMenu: function() {
      this._optionMenuItems = new Array();

    	try {
            var order = this.getViewModel().get('order');
    	    if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ltxt_order') === 'X') {
				var ordersBLT = order.get('backendLongtext');
				var ordersLLT = order.get('localLongtext');

				if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ordersBLT) || !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(ordersLLT))
					this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST);
				else
					this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT);
    	    }

    	    if (order.get('aufnr').indexOf('MO') !== -1) {
    	        this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_DELETE);
    	    }

    		//if(AssetManagement.customer.model.bo.FuncPara.getInstance().get('ext_scen') ==='X') {
    		//	this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEW_SDORDER);
    		//}
    	} catch (ex) {
     	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseOrderDetailPageController', ex);
 		}
    },

	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
            //extract parameters, if any given
            //if aufnr is passed, load the order
            //else proceed first database query
            var aufnr = '';
            var myModel = this.getViewModel();
            if(typeof argumentsObject['aufnr'] === 'string') {
                   aufnr = argumentsObject['aufnr'];
            } else if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
                   myModel.set('order', argumentsObject['order']);
            } else {
                   throw Ext.create('AssetManagement.customer.utils.OxException', {
                          message: 'Insufficient parameters',
                          method: 'startBuildingPageContext',
                          clazz: 'AssetManagement.customer.controller.pages.OrderDetailPageController'
                  });
            }

            var aufnrPassed = aufnr !== '';

            if(aufnrPassed) {
            	if(myModel.get('bemot') !== null && myModel.get('bemot').data.length === 1 &&
            			myModel.get('bemot').data.items[0].get('order') !== aufnr)

            	myModel.set('bemot', null);
            	this.getOrder(aufnr);
            } else {
            	this.waitForDataBaseQueue();
            }
         } catch (ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseOrderDetailPageController', ex);
             this.errorOccurred();
         }
	},

	getOrder: function(aufnr) {
		try {
            var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
            this.addRequestToDataBaseQueue(orderRequest, 'order');

			var bemotRequest = AssetManagement.customer.manager.CustBemotManager.getCustBemots(true);
			this.addRequestToDataBaseQueue(bemotRequest, "bemotTypes");

            var ordTypesRequest = AssetManagement.customer.manager.Cust_011Manager.getOrderTypes(true);
            this.addRequestToDataBaseQueue(ordTypesRequest, "orderTypes");

            this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrder of BaseOrderDetailPageController', ex);
        }
	},

	//protected
	//@override
	beforeDataReady: function() {
		try {
			this.extractDataOfOperations();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseOrderDetailPageController', ex);
		}
	},

	//private
	//extracts and merges timeConfs and matConfs for view model
	extractDataOfOperations: function() {
	    try {
	        var me = this;
			var myModel = me.getViewModel();
			var operations = myModel.get('order').get('operations');

			var timeConfs = null;
			var matConfs = null;

			if(operations && operations.getCount() > 0) {
				//extract timeConfs
				var arrayOfTimeConfStores = [];

				operations.each(function(operation) {
					arrayOfTimeConfStores.push(operation.get('timeConfs'));
				});

				timeConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfTimeConfStores);

				//extract matConfs
				var arrayOfMatConfStores = [];

				operations.each(function(operation) {
					arrayOfMatConfStores.push(operation.get('matConfs'));
				});

				matConfs = AssetManagement.customer.helper.StoreHelper.mergeStores(arrayOfMatConfStores);
			}

			if(!timeConfs) {
				timeConfs = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.TimeConf',
					autoLoad: false
				});
			}

			myModel.set('timeConfs', timeConfs);

			if(!matConfs) {
				matConfs = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.MaterialConf',
					autoLoad: false
				});
			}

			myModel.set('matConfs', matConfs);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside extractDataOfOperations of BaseOrderDetailPageController', ex);
		}
	},

    //@override
    getContextLoadingErrorMessage: function() {
    	var retval = '';

    	try {
		    retval = Locale.getMsg('errorLoadingOrderContext');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside  getContextLoadingErrorMessage of BaseOrderDetailPageController', ex);
        }

        return retval;
    },

    navigateToFuncLoc: function() {
    	try {
            var order = this.getViewModel().get('order');
            if(order.get('funcLoc')) {
            	AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: order.get('tplnr')  });
            } else {
            	var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('funcLocNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
	    } catch(ex) {
	    	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToFuncLoc of BaseOrderDetailPageController', ex);
	    }
    },

    navigateToEquipment: function() {
    	try {
    		var order = this.getViewModel().get('order');
            if(order.get('equipment')) {
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: order.get('equnr')  });
            } else {
            	var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equipmentNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch(ex) {
        	AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToEquipment of BaseOrderDetailPageController', ex);
        }
    },

    navigateToNotif: function() {
        try {
            var order = this.getViewModel().get('order');
    		if(order.get('qmnum')) {
    			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_DETAILS, { qmnum: order.get('qmnum') });
    		} else if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('noti_create') === 'X') {
    		    AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_NOTIF, { order: order, equipment: order.get('equipment'), funcLoc: order.get('funcLoc') });
    		} else {
            	var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
         } catch(ex) {
             AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToNotif of BaseOrderDetailPageController', ex);
         }
    },

    //handles navigation to checklist from both: objectList and Header level
    navigateToChecklist: function (grid, metaData, rowIndex, celIndex) {
        try {
            var me = this;
            var order = this.getViewModel().get('order');
            var objListItem = grid ? grid.getStore().getAt(rowIndex) : null; //objectListItem :or: order
            var entryObject = null;
            var aufnr = order.get('aufnr');
            var qmnum = '';
            var obknr = '';
            var obzae = '';

            if (objListItem) {
                entryObject = objListItem;
                obknr = objListItem.get('obknr');
                obzae = objListItem.get('obzae')
                qmnum = entryObject.get('ihnum');
            } else {
                entryObject = order;
                qmnum = entryObject.get('qmnum');
            }

            var qplos = entryObject.get('qplos');
            var qplanActive = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('use_qplan') === 'X';

            if (!(qplos || qplanActive))
                return false;
            var prueflos = qplos ? qplos.get('prueflos') : '';
            var mobileKey = qplos ? qplos.get('mobileKey') : '';

            //there are 3 possible scenarios of the checklist page:
            if (prueflos && AssetManagement.customer.utils.StringUtils.isNullOrEmpty(mobileKey)) { //qplos from SAP scenario
                var argumentObject = { prueflos: prueflos, aufnr: aufnr };
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CHECKLIST, argumentObject);

            } else {
                //qplos avaliable (pruflos is not empty) is mobile created OR
                //there is no qplos - qplan scenario
                var ihnum = entryObject.get('ihnum');

                var tplnr = entryObject.get('tplnr');
                var equnr = entryObject.get('equnr');

                var matnr = entryObject.get('matnr');
                var techObj = entryObject.get('equipment');
                if (techObj) {
                    matnr = matnr ? matnr : techObj.get('matnr') //if objListItem doesn't have a matnr, maybe the equipment does have matnr

                    matnr = matnr ? matnr : techObj.get('submt') //if objListItem equipment doesn't have matnr, maybe the equipment has submt
                } else {
                    techObj = entryObject.get('funcLoc');
                    if (techObj) {
                        matnr = matnr ? matnr : techObj ? techObj.get('submt') : '';//if objListItem doesn't have a matnr, maybe the funcloc does
                    }
                }

                var argumentObject = {
                    matnr: matnr,
                    equnr: equnr,
                    tplnr: tplnr,
                    aufnr: aufnr,
                    qmnum: qmnum,
                    prueflos: prueflos,
                    obknr: obknr,
                    obzae: obzae
                };
                this.navigateToChecklistForQPlan(argumentObject);

            }

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToChecklist of BaseOrderDetailPageController', ex);
        }
    },

    //protected
    navigateToChecklistForQPlan: function (argumentObject) {
        try {
            var qplanSuccessCallback = function (qplan) {
                try {
                    if (qplan) {
                        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_CHECKLIST, argumentObject);
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistNotMobile'), false, 1);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToChecklist of OrderDetailPageController', ex);
                }
            };
            var matnr = argumentObject['matnr'];
            var qplanRequest = AssetManagement.customer.manager.QPlanManager.getNewestQPlanForMaterial(matnr ? matnr : '', false);
            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(qplanRequest, qplanSuccessCallback);

        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checklistNavigation of BaseOrderDetailPageController', ex);
        }
    },

	onOperationSelected:  function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
    	try {
            var order = this.getViewModel().get('order');
			//if(AssetManagement.customer.model.bo.FuncPara.getInstance().get('ext_scen') !== 'X')
	    		AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_TIMECONF, { order: order, operation: record  });
	    	//else
	    	//	AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EXT_CONF, { order: order, operation: record });
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSelected of BaseOrderDetailPageController', ex);
		}
	},

	onEquiSelected: function (tableview, td, cellIndex,rowIndex , tr, record, e, eOpts) {
	    try {
	        var equnr = record.get('equnr');
	        if (equnr)
	            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: equnr });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onEquiSelected of BaseOrderDetailPageController', ex);
		}
	},

	onFuncLocSelected: function (tableview, td, cellIndex,rowIndex , tr, record, e, eOpts) {
	    try {
	        var tplnr = record.get('tplnr');
	        if (tplnr)
	            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: tplnr });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFuncLocSelected of BaseOrderDetailPageController', ex);
		}
	},

	onNotifSelected: function (tableview, td, cellIndex,rowIndex , tr, record, e, eOpts) {
	    try {
	        var ihnum = record.get('ihnum');
	        if (ihnum)
	            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_DETAILS, { qmnum: ihnum });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifSelected of BaseOrderDetailPageController', ex);
		}
	},

	showStatusDialog: function() {
	},

	onNewMatConfClick: function() {
		try {
            var order = this.getViewModel().get('order');
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_MATCONF, { order: order });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNewMatConfClick of BaseOrderDetailPageController', ex);
		}
	},

	onNewComponentClick: function() {
		try {
            var order = this.getViewModel().get('order');
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_COMPONENT, { order: order });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNewComponentClick of BaseOrderDetailPageController', ex);
		}
	},

	onOrderBemotSelected: function(combo, record) {
		try {
		    if (record) {
		        var selectedBemot = record.get('value');
		        var order = this.getViewModel().get('order');

		        if (order) {
		            var currentBemot = order.get('bemot');

		            if (selectedBemot !== currentBemot) {
		                this.saveNewOrderAccountIndication(selectedBemot);
		            }
		        }
		    }
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderBemotSelected of BaseOrderDetailPageController', ex);
		}
	},

	_saveIsRunning: false,

	saveNewOrderAccountIndication: function (newBemot) {
	    try {
	        if (this._saveIsRunning) {
	            return;
	        }

	        this._saveIsRunning = true;

	        AssetManagement.customer.controller.ClientStateController.enterLoadingState();

	        var me = this;

	        var saveCallback = function (success) {
	            try {
	                var notification = null;

	                if (success === true) {
	                    notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('orderAccountIndiChanged'), true, 0);
	                } else {
	                    notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorChangeOrderAccountIndi'), false, 2);
	                }

	                AssetManagement.customer.core.Core.getMainView().showNotification(notification);

	                //refresh view to adjust combobox to current value (readjust after save fail or another selection while saving)
	                me.refreshView();
	            } catch (ex) {
	                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrderBemot of BaseOrderDetailPageController', ex);
	            } finally {
	                me._saveIsRunning = false;
	                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	            }
	        };

	        var order = this.getViewModel().get('order');

	        var eventId = AssetManagement.customer.manager.OrderManager.saveOrderBemotChange(order, newBemot);

	        if (eventId > -1) {
	            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveCallback);
	        } else {
	            saveCallback(false);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside saveNewOrderBemot of BaseOrderDetailPageController', ex);
	        this._saveIsRunning = false;
	        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	    }
	},

	setContextBemot: function(bemotText) {
	    try {
		    if(this.getViewModel().get('bemot') === null){
			   var store = Ext.create('Ext.data.Store', {
			       fields: ['order', 'bemot']
			   });
		    }

		     this.getViewModel().get('bemot').add({order: this.getViewModel().get('order').get('aufnr'), bemot: bemotText});
	    } catch(ex) {
	    	 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setContextBemot of BaseOrderDetailPageController', ex);
	    }
	},

	navigateToPartnerAddress: function(partner) {
	   try {
			var addressInformation = null;
			if(partner) {
				addressInformation = AssetManagement.customer.manager.AddressManager.mapPartnerIntoAddress(partner);
			}

			if(addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postCode1'))) {
						//AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);
						var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
							type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
						});

						//generate the hint
						hint = AssetManagement.customer.utils.StringUtils.trimStart(partner.get('parvw'), '0');

						request.addAddressWithHint(addressInformation, hint);

						AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToPartnerAddress of BaseOrderDetailPageController', ex);
		}
	},

	showLongtextDialogFromOrder: function() {
		try {
			var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('c_ltxt_order') === '';
			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { order: this.getViewModel().get('order'), readOnly: readOnly });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialog of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuOperationList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
   		try {
   			e.stopEvent();
   			if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('oper_change_active') === 'X')
			{
				if(!this.rowMenu)
					this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

				var options = [];

				if (record.get('updFlag') === 'I' || record.get('updFlag') === 'A') {
					options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
					options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				}

				if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ltxt_operation') === 'X') {
					options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_LONGTEXT);
				}

				this.rowMenu.onCreateContextMenu(options, record);

				if(options.length > 0) {
					this.rowMenu.setDisabled(false);
					this.rowMenu.showAt(e.getXY());
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			} else {
   				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
   			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuOperationList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuComponentList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			if(record.get('updFlag') === 'I') {
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}

			this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuComponentList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuFileList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			if(record.get('fileOrigin') === 'local') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}

			this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuFileList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuTimeConfList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			if(record.get('updFlag') === 'I') {
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}

			this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuTimeConfList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuExtConfList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			if(record.get('updFlag') === 'I') {
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}

			this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuMatConfList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuPartnerList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
   			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuPartnerList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuMatConfList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			if(record.get('updFlag') === 'I') {
				options.push( AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}

			this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuMatConfList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuObjectList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
   			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [];

			var qplos = record.get('qplos');
			if (qplos && qplos.isCompleted())
			    options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_PRINT_CHECKLIST)

            this.rowMenu.onCreateContextMenu(options, record);

			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuObjectList of BaseOrderDetailPageController', ex);
		}
	},

	onCellContextMenuSDOrderList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
   			e.stopEvent();

			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuSDOrderList of BaseOrderDetailPageController', ex);
		}
	},

	onContextMenuItemSelected: function(itemID, record) {
	    try {
	        if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_LONGTEXT) {
	            this.showLongtextDialogSub(record);
	        } else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
	            var className = Ext.getClassName(record);

	            if (className.indexOf('DocUpload') > -1) {
	                this.deleteDocUploadClicked(record);
	            } else if (className.indexOf('Operation') > -1) {
	                this.deleteOperationClicked(record);
	            } else if (className.indexOf('TimeConf') > -1) {
	                this.deleteTimeConfClicked(record);
	            } else if (className.indexOf('Component') > -1) {
	                this.deleteComponentClicked(record);
	            } else if (className.indexOf('MaterialConf') > -1) {
	                this.deleteMatConfClicked(record);
	            } else if (className.indexOf('BanfPosition') > -1) {
	                this.deleteExtConfClicked(record);
	            }
	        } else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
	            var className = Ext.getClassName(record);
				var order = this.getViewModel().get('order');

	            if (className.indexOf('Operation') > -1) {
	                AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.OPERATION_DIALOG, { order: order, operation: record });
	            } else if (className.indexOf('TimeConf') > -1) {
	                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_TIMECONF, { order: order, timeConf: record });
	            } else if (className.indexOf('Component') > -1) {
	                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_COMPONENT, { order: order, component: record });
	            } else if (className.indexOf('MaterialConf') > -1) {
	                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_MATCONF, { order: order, matConf: record });
	            } else if (className.indexOf('BanfPosition') > -1) {
	                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EXT_CONF, { order: order, extConf: record });
	            }
	        } else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_PRINT_CHECKLIST){
	        	this.printChecklistReport(record);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseOrderDetailPageController', ex);
	    }
	},
	///------ CONTEXT MENU END -------///

	showLongtextDialogSub: function(record) {
	    try {
	        var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('c_ltxt_operation') === '';
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { bo: record, readOnly: readOnly });
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialogForOperation of BaseOrderDetailPageController', ex);
		}
	},

	onFileSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			//if the record represents an DMS document, download it from SAP system
			//else try open its binary content
			if(record.get('fileOrigin') === 'online') {
				AssetManagement.customer.manager.DMSDocumentManager.downloadDMSDocument(record);
			} else {
				AssetManagement.customer.manager.DocUploadManager.openDocument(record);
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileSelected of BaseOrderDetailPageController', ex);
		}
	},

	onFileForDocUploadSelected: function(file) {
		try {
			if(!file) {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}

			var order = this.getViewModel().get('order');

			var callback = function(docUpload) {
				try {
					if(docUpload) {
						//not neccessary, because the documents store of the order will automatically refresh itself
						//the docUpload will be added to this store by the manager
						//this.getView().refreshView();

						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileAdded'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseOrderDetailPageController', ex);
				}
			};

			var addEvent = AssetManagement.customer.manager.DocUploadManager.addNewDocUploadForOrder(file, order);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(addEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseOrderDetailPageController', ex);
		}
	},

	//will show an order report preview page, with signature fields and other input elements
	onAddOrderReportClick: function() {
	    try {

	        this.printReport(this.getViewModel().get('order'));

			//AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_REPORT, { order: this.getViewModel().get('order') });
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddOrderReportClick of BaseOrderDetailPageController', ex);
		}
	},

	printChecklistReport: function(record) {
	    try {
	        var qplos = '';
	        if (record) {
	            var objectItem = record;
	            qplos = objectItem ? objectItem.get('qplos') : null;
	        }
	        else {
	            var order = this.getViewModel().get('order');
	            qplos = order ? order.get('qplos') : null;
	        }

            if (qplos) {
                this.printReport(qplos);
            } else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('checklistNotMobile'), false, 1);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddOrderReportClick of BaseOrderDetailPageController', ex);
		}
	},

	onNewOperClick: function() {
   		try {
   			AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.OPERATION_DIALOG, { order: this.getViewModel().get('order') });
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNewOperClick of BaseOrderDetailPageController', ex);
		}
	},

	onOperationSaved: function(operation) {
	    try {
	    	var order = this.getViewModel().get('order');
	        var temp = order.get('operations').getById(operation.get('id'));

			if(temp === undefined || temp === null)
				order.get('operations').add(operation);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOperationSaved of BaseOrderDetailPageController', ex);
		}
	},

	onLongtextSaved: function(affectedObject, newLongtext) {
		try {
			if(affectedObject && affectedObject === this.getViewModel().get('order')) {
				this.rebuildOptionsMenu();
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLongtextSaved of BaseOrderDetailPageController', ex);
		}
	},

	deleteOperationClicked: function(operation) {
		try {
			var callback = function(success) {
		   		try {
		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('operationDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingOperation'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOperationClicked of BaseOrderDetailPageController', ex);
				}
			};

			var deleteEvent = AssetManagement.customer.manager.OperationManager.deleteOperation(operation);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingOperation'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOperationClicked of BaseOrderDetailPageController', ex);
		}
	},


	deleteDocUploadClicked: function(docUpload) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseOrderDetailPageController', ex);
   				}
   			};

   			var deleteEvent = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(docUpload);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseOrderDetailPageController', ex);
		}
	},

	deleteTimeConfClicked: function(timeConf) {
		try {
			var callback = function(success) {
				try {
					if(success === true) {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('timeConfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTimeConfClicked of BaseOrderDetailPageController', ex);
				}
			};

			var eventId = AssetManagement.customer.manager.TimeConfManager.deleteTimeConf(timeConf, false);

			if(eventId > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingTimeConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponentClicked of BaseOrderDetailPageController', ex);
		}
	},

	deleteComponentClicked: function(component) {
		try {
			var callback = function(success) {
		   		try {
		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('componentDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingComponent'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponentClicked of BaseOrderDetailPageController', ex);
				}
			};

			var deleteEvent = AssetManagement.customer.manager.ComponentManager.deleteComponent(component);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingComponent'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteComponentClicked of BaseOrderDetailPageController', ex);
		}
	},

	deleteMatConfClicked: function(matConf) {
		try {
			var callback = function(success) {
		   		try {
		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('matConfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingMatConf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConfClicked of BaseOrderDetailPageController', ex);
				}
			};

			var deleteEvent = AssetManagement.customer.manager.MatConfManager.deleteMatConf(matConf);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingMatConf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteMatConfClicked of BaseOrderDetailPageController', ex);
		}
	},

	deleteExtConfClicked: function(extConf) {
		try {
			var callback = function(success) {
		   		try {
		   			if(success === true) {
		   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('extBanfDeleted'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingExtBanf'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteExtConfClicked of BaseOrderDetailPageController', ex);
				}
			};

			var deleteEvent = AssetManagement.customer.manager.BanfManager.deleteExtBanf(extConf);

			if(deleteEvent > 0) {
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingExtBanf'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteExtConfClicked of BaseOrderDetailPageController', ex);
		}
	},

	onSDOrderItemSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SDORDER_DETAILS, { vbeln: record.get('vbeln') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemSelected of BaseOrderDetailPageController', ex)
		}
	},

	onAddSDOrderItemClick: function() {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_SDORDER, { aufnr: this.getViewModel().get('order').get('aufnr') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddSDOrderItemClick of BaseOrderDetailPageController', ex)
		}
	},

	showAddressesOnMap: function() {
		try {

            var order = this.getViewModel().get('order')
			var customerAddress;
			var partners = order.get('partners');

			if(partners.getData().length === 1) {
				customerAddress = order.get('partners').getData().items[0];
			} else {
				if(partners && partners.getCount() > 0) {
					partners.each(function(partner) {
						var parvw = partner.get('parvw');

						if(parvw === 'AG')
							customerAddress = partner;
					});
				}
			}

			var addressInformationCustomer = null;

			if(customerAddress) {
				customerAddress = AssetManagement.customer.manager.AddressManager.mapPartnerIntoAddress(customerAddress);
			}

			var objectAddress = null;
			var funcLoc = order.get('funcLoc');
			var equi = order.get('equipment');

			// get object address
			if(equi !== null && equi !== undefined && equi.get('address')) {
				objectAddress = equi.get('address');
			} else if(funcLoc !== null && funcLoc !== undefined && funcLoc.get('address')) {
				objectAddress = funcLoc.get('address');
			}

			if(objectAddress === null || objectAddress === undefined) {
				if(partners && partners.getCount() > 0) {

					partners.each(function(partner) {
						objectAddress = partner.get('address');

						if(objectAddress)
							return false;
					});
				}
			}

			var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
				type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES,
				includeOwnPosition: true
			});

			if(customerAddress) {

				request.addAddressWithHint(customerAddress, Locale.getMsg('customerAddress'));
			}

			if(objectAddress) {
				request.addAddressWithHint(objectAddress, Locale.getMsg('objectAddress'));
			}

			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAddressesOnMap of BaseOrderDetailPageController', ex);
		}
	},

	printReport: function (object) {
	    try {

	        var objTable = [];
	        objTable.push(object);

	        var order = this.getViewModel().get('order');

	        var enhancementPoint = function () {
	            try {
	                eventController = AssetManagement.customer.controller.EventController.getInstance();
	                var eventId = eventController.getNextEventId();
	                if (eventId > -1) {
	                    eventController.requestEventFiring(eventId, true);
	                    return eventId;
	                } else {

	                }
	            } catch (ex) {
	                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-enhancementPoint of BaseOrderDetailPageController', ex);
	            }
	        };

	        var parentSuccessCallback = function () {
	            try {
	                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: order.get('aufnr') }, 2);
	            } catch (ex) {
	                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-parentSuccessCallback of BaseOrderDetailPageController', ex);
	            }
	        };

	         var parentErrorCallback = function(){
                try{
                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('generalErrorCreatingReportsOccured'), false, 2);
                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                } catch(ex){
                   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport-parentSuccessCallback of AssignmentConfInstallPageController', ex);
                }
            };

	         var reportFrameworkConfig = {
                isOneSignatureForAllDocuments : true,
                isVerticalView: true,
                actionAfterReportCreation: enhancementPoint,
                reportLanguage: null,
                signedObjets: null,
                preventLangChange: true
            };

            AssetManagement.customer.modules.reportFramework.ReportFramework.getInstance().createReports(reportFrameworkConfig, objTable, parentSuccessCallback, parentErrorCallback);

	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside printReport of BaseOrderDetailPageController', ex);
	    }
	},

	navigateToUserStatus: function () {
	    try {
	        if (this.getViewModel().get('order').get('objectStatusList')) {
	            if (!this.rowMenu) {
	                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
	            }
	            var options = [];
	            if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_change') === 'X') {
	                options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_OBJECT_STATUS)
	            }
	            var order = this.getViewModel().get('order');
	            this.rowMenu.onCreateContextMenu(options, order);

	            if (options.length > 0) {
	                this.rowMenu.setDisabled(false);
	                //  this.rowMenu.showAt(e.getXY());
	            } else {
	                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
	                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	            }

	            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.OBJECT_STATUS_DIALOG, order);
	        } else {
	            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equipmentNotMobile'), false, 1);
	            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToUserStatus of NotifDetailPageController', ex)
	    }
	},

	onStatusSaved: function (objectStatusList) {
	    try {
			var me = this;
	        var order = me.getViewModel().get('order');
			var statusLoadCallback = function (objectStatusList) {
                try {
					var updateObjectStatusStore = Ext.create('Ext.data.Store', {
			            model: 'AssetManagement.customer.model.bo.ObjectStatus',
			            autoLoad: false
			        });
			        objectStatusList.each(function (objStatus) {
			            if (objStatus.get('inact') === '' && objStatus.get('stat').indexOf('I') !== 0) {
			                updateObjectStatusStore.add(objStatus);
			            }
			        });
					order.set('objectStatusList', updateObjectStatusStore);
                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
                    me.getView().refreshView();
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStatusSaved-statusLoadCallback of BaseOrderDetailPageController', ex);
                }
            };

			var eventId = AssetManagement.customer.manager.ObjectStatusManager.getStatusStoreForObject('OR' + order.get('id'), '0', false);
            if (eventId === -1) {
                notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorLoadingObjectStatusList'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                this._saveRunning = false;
                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
            } else{
                AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, statusLoadCallback);
			}
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStatusSaved of BaseOrderDetailPageController', ex)
	    }
	},

	navigateToAddress: function (partner) {
	    try {
	        var addressInformation = null;
	        if (partner) {
	            addressInformation = partner;
	        }

	        if (addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postcode1'))) {
	            // AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);

	            var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
	                type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
	            });

	            //generate the hint
	            var hint = AssetManagement.customer.utils.StringUtils.trimStart(partner.get('aufnr'), '0');

	            request.addAddressWithHint(addressInformation, hint);

	            AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
	        } else {
	            var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
	            AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToAddress of OrderListPageController', ex);
	    }
	},

	deleteOrder: function () {
	    var retVal = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage("", true, 0);

	    try {

	        //check if data is complete
	        var me = this;
	        var callback = function (deletedOrder) {
	            try {
	                if (deletedOrder) {
	                    //go back to notiflist
	                    AssetManagement.customer.core.Core.navigateBack();

	                    Ext.defer(function () {
	                        me._saveRunning = false;
	                    }, 3000, me);

	                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('orderDeleted'), true, 0);
	                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	                } else {
	                    me._saveRunning = false;
	                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	                    notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingOrder'), false, 2);
	                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	                }
	            } catch (ex) {
	                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOrder of OrderListPageController', ex)
	                me._saveRunning = false;
	                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	            }
	        };

	        var eventId = AssetManagement.customer.manager.OrderManager.deleteOrder(this.getViewModel().get('order'));

	        if (eventId > 0) {
	            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
	            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback, me);
	        } else {
	            retval = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingOrder'), false, 2);
	        }

	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteOrder of OrderListPageController', ex);
	        this._saveRunning = false;
	        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	    }

	    return retVal;
	}
});
