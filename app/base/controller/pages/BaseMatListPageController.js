Ext.define(
    'AssetManagement.base.controller.pages.BaseMatListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',
	alias: 'controller.BaseMatListPageController',

	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.manager.UserManager',
	    'AssetManagement.customer.controller.ToolbarController',
	    'AssetManagement.customer.helper.LanguageHelper',
		'AssetManagement.customer.model.helper.ReturnMessage',
		'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.PoItem'
	],

	config: {
		contextReadyLevel: 1
    },

	//private
	_requestRunning: false,
	_argumentsObject: null,

	//protected
	//@override
	startBuildingPageContext: function (argumentsObject) {
	    try {
        var myModel = this.getViewModel();
        var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');

        myModel.set('selectedLgort', selectedLgort); //always exists
        myModel.set('materialStore', null); //clear on load
        myModel.set('materialInput', ''); //clear on load
	        this._argumentsObject = argumentsObject; //this argumentsObject will be modified in the view later,
	        //it is a unique js object which is will be reused as arguments object for back navigation
	        //so, it can be altered so that the next 'back navigation' has extra arguments from the user input on the view.
            //each independent entry to the view creates a new object { } and therefore, will not use the old values - the view will be cleared.

	        this.waitForDataBaseQueue(); //start the page without the data, becaue only after we start it we can get an onlineRequest
	        this.getView().transferModelToViewState();
	        argumentsObject = this._argumentsObject;
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseMatListPageController', ex);
	    }
	},

	transferArgumentsToModel: function (argumentsObject) {
	    try {
	        var myModel = this.getViewModel();
	        var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');

	        myModel.set('selectedLgort', selectedLgort); //always exists
	        myModel.set('materialInput', argumentsObject['materialInput'] ? argumentsObject['materialInput'] : '');
	        myModel.set('materialStore', argumentsObject['materialStore'] ? argumentsObject['materialStore'] : null);
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferArgumentsToModel of BaseMatListPageController', ex);
	    }
	},

    refreshData: function(triggerPageRequestCallbackWhenDone) {
        try {
            if(!triggerPageRequestCallbackWhenDone) {
                this._pageRequestCallback = null;
                this._pageRequestCallbackScope = null;
            }
            // this.getViewModel().resetData(); //Deactivated to have a persistent store when navigating back to page
            //set to zero
            this._contextLevel = 0;

            if(this._contextReadyLevel === 0) {
                this.contextReady();
            } else {
                //start the loading cycle
                this.startBuildingPageContext(this.getCurrentArguments());
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside refreshData of ' + this.getClassName(), ex);
        }
    },

        //protected
    //may be implemented by derivates
    onOptionMenuItemSelected: function(optionID) {
        try {
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseMatListPageController', ex);
        }
    },

    onCreateOptionMenu: function() {
        try {
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseMatListPageController', ex);
        }
    },

    onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        try {
            e.stopEvent();
            if(!this.rowMenu)
                this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

            var options = [ ];

            this.rowMenu.onCreateContextMenu(options, record);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseMatListPageController', ex);
        }
    },
    onMatNumberTextKeypress: function (field, e, eOpts) {
        try {
            if (e.keyCode === 13) {
               this.onSearchMaterialClick();

            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMatNumberTextKeypress of BaseMatListPageController', ex);
        }
    },

    onSearchMaterialClick: function (field, e, eOpts) {
        try {
            var me = this;
            this.getView().transferViewStateIntoModel();
            var myModel = this.getViewModel();
            var material = myModel.get('materialInput');
            var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
            var lgort = selectedLgort.get('lgort');
            var werks = selectedLgort.get('werks');
            var onlineStockDialogCallback = function (materialStore) {
                try {
                    if (materialStore && materialStore.getCount() > 0) {
                        myModel.set('materialStore', materialStore);
                        me._argumentsObject['materialStore'] = materialStore;
                        me.refreshView();
                    } else {
                        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noMaterialFound'), false, 2);
                        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchMaterialClick of BaseMatListPageController', ex);
                }
            };
            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.ONLINE_STOCK_DIALOG, {
                material: material,
                lgort: lgort,
                werks: werks,
                successCallback: onlineStockDialogCallback
            });
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchMaterialClick of BaseMatListPageController', ex);
        }
    },

    onActualAmmountChange: function (field, value, value2, eOpts) {
        try {
            var record = field.getWidgetRecord();
            if (field.isValid()) {
                record.set('menge_ist', value);
            }
        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside onDeliveredAmmountChange of BaseMatListPageController', ex);
        }
    }
});