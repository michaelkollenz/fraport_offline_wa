Ext.define('AssetManagement.base.controller.pages.BaseNewSDOrderPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.utils.NumberFormatUtils',
	    'AssetManagement.customer.manager.OrderManager',
	    'AssetManagement.customer.manager.NotifManager',
		'AssetManagement.customer.manager.SDOrderManager',
		'AssetManagement.customer.model.bo.SDOrderItem',
		'AssetManagement.customer.manager.SDOrderTypeManager',
		'AssetManagement.customer.controller.ToolbarController',
//		'AssetManagement.customer.controller.ClientStateController',		CAUSES RING DEPENDENCY
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],
    
	config: {
		contextReadyLevel: 2
	},
	
	//private
	_saveRunning: false,
	         
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
			if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SAVE) {
				if(this._saveRunning === true)
					return;
			
				this.trySaveSDOrder(); 
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseNewSDOrderPageController', ex);
		}
	},
	
	//protected
	//@override
	onCreateOptionMenu: function() {
		try {
		    this._optionMenuItems = [ AssetManagement.customer.controller.ToolbarController.OPTION_SAVE ];
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseNewSDOrderPageController', ex);
		}    
	},
	
	//protected
	//@override
	startBuildingPageContext: function(argumentsObject) {
	    try {

	        var myModel = this.getViewModel();
			//draw order or notif from passed arguments
            if(typeof argumentsObject['aufnr'] === 'string' && argumentsObject['aufnr'] !== '') {
            	var aufnr = argumentsObject['aufnr'];
            	this.getOrder(aufnr);
            } else if(Ext.getClassName(argumentsObject['order']) === 'AssetManagement.customer.model.bo.Order') {
            	myModel.set('order', argumentsObject['order']);
            } else if(typeof argumentsObject['qmnum'] === 'string' && argumentsObject['qmnum'] !== '') {
				var qmnum = argumentsObject['qmnum'];
				this.getNotif(qmnum);
			} else if(Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
				myModel.set('notif', argumentsObject['notif']);
			} else {
			    throw Ext.create('AssetManagement.customer.utils.OxException', {
			        message: 'Insufficient parameters',
			        method: 'startBuildingPageContext',
			        clazz: 'AssetManagement.customer.controller.pages.HistOrderDetailPageController'
			    });
			}


			this.waitForDataBaseQueue();			
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseNewSDOrderPageController', ex);
		    this.errorOccurred();
		}
	},
	
	getOrder: function(aufnr) {
		try {
            var orderRequest = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, true);
            this.addRequestToDataBaseQueue(orderRequest, 'order');
                   
            this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOrder of BaseNewSDOrderPageController', ex);
        }
	},
	
	getNotif: function(qmnum) {
		try {
			var notifRequest = AssetManagement.customer.manager.NotifManager.getNotif(qmnum, true);
			this.addRequestToDataBaseQueue(notifRequest, 'notif');
				
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotif of BaseNewSDOrderPageController', ex);
		}
	},
	
	//protected
	//@override
	afterDataBaseQueueComplete: function() {
		try {
			if(this._contextLevel === 1) {
				this.performContextLevel1Requests();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside afterDataBaseQueueComplete of BaseNewSDOrderPageController', ex);
		}
	},
	
	//private			
	performContextLevel1Requests: function() {
		try {
			//get the sd order types for initialization of a new sd order
			var orderTypesRequest = AssetManagement.customer.manager.SDOrderTypeManager.getSDOrderTypes();
			this.addRequestToDataBaseQueue(orderTypesRequest, "sdOrderTypes");
			
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside performContextLevel1Requests of BaseNewSDOrderPageController', ex);
		}
	},
	
	beforeDataReady: function() {
		try {
			if(!this.getViewModel().get('sdOrder'))
				this.initializeNewSdOrder();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseNewSDOrderPageController', ex);
		}
	},
	
	initializeNewSdOrder: function() {
		try {
			var myModel = this.getViewModel();
			var newSdOrder = AssetManagement.customer.manager.SDOrderManager.getNewSDOrder();
			
			//set the orderType for the new order
			var orderTypes = myModel.get('sdOrderTypes');
			var orderTypeToUse = null;
			
			if(orderTypes) {
				orderTypeToUse = orderTypes.getAt(0);
			}
			
			if(orderTypeToUse) {
				newSdOrder.set('orderType', orderTypeToUse);
				newSdOrder.set('auart', orderTypeToUse.get('auart'));
				newSdOrder.set('augru', orderTypeToUse.get('augru'));
			}
			
			//draw data from order or notif
			var order = myModel.get('order');
			var notif = myModel.get('notif');
			
			if(order) {
				newSdOrder.set('aufnr', order.get('aufnr'));
				newSdOrder.set('mobileKey_ref', order.get('mobileKey'));
			} else if(notif) {
				newSdOrder.set('qmnum', notif.get('qmnum'));
				newSdOrder.set('mobileKey_ref', notif.get('mobileKey'));
			}
			
			myModel.set('sdOrder', newSdOrder);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeNewSdOrder of BaseNewSDOrderPageController', ex);
		}
	},
	
	trySaveSDOrder: function() {
		try {
			this._saveRunning = true;
			
			AssetManagement.customer.controller.ClientStateController.enterLoadingState();
			if(this.checkInputValuesForSDOrder() === true) {
				var sdOrder = this.getViewModel().get('sdOrder');
			
				this.getView().transferViewStateIntoModel();
						
				var me = this;

				var callback = function(success) {
					try {
						if(success === true) {
							//go to sd order detail page
							AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SDORDER_DETAILS, { vbeln: sdOrder.get('vbeln') }, true);
							
							Ext.defer(function() { 
								me._saveRunning = false;
							}, 2000, me);
							
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sdOrderSaved'), true, 0);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						} else {
							me._saveRunning = false;
							AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
							var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingSDOrder'), false, 2);
							AssetManagement.customer.core.Core.getMainView().showNotification(notification);
						}
					} catch(ex) {
						me._saveRunning = false;
						AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
						AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveOrder of BaseNewSDOrderPageController', ex);
					}
				};				
				
				var eventId = AssetManagement.customer.manager.SDOrderManager.saveSDOrder(sdOrder);
				
				if(eventId > 0) {
					AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback);
				} else {
					this._saveRunning = false;
					AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorSavingSDOrder'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
			} else {
				this._saveRunning = false;
				AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			}
		} catch(ex) {
			this._saveRunning = false;
			AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside trySaveOrder of BaseNewSDOrderPageController', ex);
		}
	},
	
	//performs a set of checks for the order itself
	checkInputValuesForSDOrder: function() {
		try {
			var retval = true;
			var errorMessage = '';
					
			var values = this.getView().getCurrentInputValues(); 
		
			var bstnk = values.bstnk;
			var vdatu = values.vdatu;
			
			var useDifferentDeliveryAddress = values.useDifferentDeliveryAddress;
			var title = values.title;
			var name = values.name;	
		    var name2 = values.name2;
		    var street = values.street;
		    var houseNo = values.houseNo;
		    var postalCode = values.postalCode;
		    var city = values.city;
		    var country = values.country;


			var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
			
			if(shortHand(bstnk)) {
				//provide material
				errorMessage = Locale.getMsg('provideOrderingNumber');
				retval = false;
			} else if(AssetManagement.customer.utils.DateTimeUtils.isBeforeToday(vdatu)) {
				//delivery date is in the past
				errorMessage = Locale.getMsg('desiredDeliveryInPast');
				retval = false;
			}
			
			//check for incomplete address data, if any has been provided
			if(retval === true && useDifferentDeliveryAddress) {
				var anyDataProvided = !shortHand(title) || !shortHand(name) || !shortHand(name2) || !shortHand(street)
										|| !shortHand(houseNo) || !shortHand(postalCode) || !shortHand(city) || !shortHand(country);
				
				if(anyDataProvided === true) {
					//check for missing required data
					if(shortHand(name) && shortHand(name2) || shortHand(street) || shortHand(houseNo) || shortHand(postalCode) || shortHand(city)) {
						errorMessage = Locale.getMsg('incompleteAddressData');
						retval = false;
					}
				} else {
					errorMessage = Locale.getMsg('provideDifferentDeliveryAddressData');
					retval = false;
				}
			}
			
			//check if there has already been captured any order item
			if(retval === true) {
				var orderItems = this.getViewModel().get('sdOrder').get('items');
				
				if(orderItems.getCount() === 0) {
					errorMessage = Locale.getMsg('provideAtLeastOneOrderItem');
					retval = false;
				}
			}
			
			//check if there is still input on new sd order item input fields
			if(retval === true) {
				var matnr = values.matnr;
				var amount = values.amount;
				var remark = values.remark;
				
				var anyDataProvided = !shortHand(matnr) || (!shortHand(amount) && amount != 1) || !shortHand(remark);
				
				if(anyDataProvided === true) {
					errorMessage = Locale.getMsg('unsavedSDOrderItemData');
					retval = false;
				}
			}
			
			if(retval === false) {
				var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(message);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValuesForSDOrder of BaseNewSDOrderPageController', ex);
			retval = false;
		}
		
		return retval;
	},
	
	onAddressCheckBoxClicked: function(checkbox, newValue, oldValue, eOpts) {
		try {
			var myView = this.getView();
			
			if(newValue === true) {
				myView.showDifferentDeliveryAddressSection();
			} else {
				myView.hideDifferentDeliveryAddressSection();
				myView.resetDifferentDeliveryAddressInputFields();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddressCheckBoxClicked of BaseNewSDOrderPageController', ex);
		}
	},
	
	onAddSDOrderItemClick: function() {
		try {
			if(this.checkInputValuesForSdOrderItem() === true) {
				var myView = this.getView();
			
				var inputValues = myView.getCurrentInputValuesForOrderItem();
			
				var newSDOrderItem = Ext.create('AssetManagement.customer.model.bo.SDOrderItem', {
					matnr: inputValues.matnr,
					zmeng: AssetManagement.customer.utils.NumberFormatUtils.getNumberAsStringForBackend(inputValues.amount),
					zieme: inputValues.unit,
					arktx: inputValues.remark
				});
			
				var sdOrderItems = this.getViewModel().get('sdOrder').get('items');
				sdOrderItems.insert(0, newSDOrderItem);
				
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sdOrderItemAdded'), true, 0);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				
				myView.resetSDOrderItemInputFields();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddSDOrderItemClick of BaseNewSDOrderPageController', ex);
		}
	},
	
	//performs a set of checks for the current sd order item input 
	//returns a boolean indicating if the checks has been passed
	checkInputValuesForSdOrderItem: function() {
		var retval = true;	
	
		try {
			var errorMessage = '';
					
			var values = this.getView().getCurrentInputValuesForOrderItem(); 
		
			var matnr = values.matnr;
			var amount = values.amount;
			var unit = values.unit;
			var remark = values.remark;
			var shortHand = AssetManagement.customer.utils.StringUtils.isNullOrEmpty;
			
			if(shortHand(matnr)) {
				//provide material
				errorMessage = Locale.getMsg('provideMaterial');
				retval = false;
			} else if(shortHand(unit)) {
				//provide unit
				errorMessage = Locale.getMsg('provideUnit');
				retval = false;
			} else if(amount === null || amount === undefined || amount <= 0) {
				errorMessage = Locale.getMsg('provideValidQuantity');
				retval = false;
			}
			
			if(retval === false) {
				var message = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(errorMessage, false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(message);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside checkInputValuesForSdOrderItem of BaseNewSDOrderPageController', ex);
			retval = false;
		}
		
		return retval;
	},
	
	///------- CONTEXT MENU ---------///
	//long tap to open context menu
	onCellContextMenu: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
		try {
			e.stopEvent(); 
		
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
			var options = [ ];
			
			options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
				
			this.rowMenu.onCreateContextMenu(options, record); 
			
			this.rowMenu.showAt(e.getXY());
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseNewSDOrderPageController', ex);
		}
	},
	
	onContextMenuItemSelected: function(itemID, record) {
		try {
			if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
				this.deleteSDOrderItem(record);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseNewSDOrderPageController', ex);
		}
	},
	///------ CONTEXT MENU END -------///
	
	deleteSDOrderItem: function(sdOrderItem) {
		try {
			if(sdOrderItem) {
				this.getViewModel().get('sdOrder').get('items').remove(sdOrderItem);
			
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('sdOrderItemDeleted'), true, 0);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteSDOrderItem of BaseNewSDOrderPageController', ex);
		}
	}
});