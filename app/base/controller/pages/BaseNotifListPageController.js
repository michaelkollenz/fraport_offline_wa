Ext.define('AssetManagement.base.controller.pages.BaseNotifListPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.controller.ToolbarController',
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.NotifManager',
        'AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider'
	],
	
	config: {
		contextReadyLevel: 1,
		isAsynchronous: true
	},

	mixins: {
	    handlerMixin: 'AssetManagement.customer.controller.mixins.NotifListPageControllerMixin'
	},
	
	//public
	//@override
	onOptionMenuItemSelected: function(optionID) {
		try {
	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_ADD) {
	    		AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_NOTIF);
	    	} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH) {
	    	    this.onSearchButtonClicked();
	    	} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH) {
	    	    this.onClearSearchButtonClicked();
	    	}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseNotifListPageController', ex);
		}
	},
		
	//private
	onCreateOptionMenu: function() {		
	    try {
	        this._optionMenuItems = [AssetManagement.customer.controller.ToolbarController.OPTION_ADD, AssetManagement.customer.controller.ToolbarController.OPTION_SEARCH, AssetManagement.customer.controller.ToolbarController.OPTION_CLEAR_SEARCH];

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseNotifListPageController', ex);
		}
	},
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
		    this.loadListStore();
		
		    this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseNotifListPageController', ex);
		}    
	},
		
	loadListStore: function() {
	    try {
		    var notifStoreRequest = AssetManagement.customer.manager.NotifManager.getNotifs(true);
		    this.addRequestToDataBaseQueue(notifStoreRequest, 'notifStore');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside loadListStore of BaseNotifListPageController', ex);
		}
	},
		
	//list handler
	onNotifSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIF_DETAILS, { qmnum: record.get('qmnum') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifSelected of BaseNotifListPageController', ex);
		}
    },
	
	onNotifSelectedByTabHold: function(notif) {
	},
	
	navigateToAddress: function(notif) {
       try {
			var addressInformation = null;
		
			if(notif) {
				addressInformation = notif.getObjectAddress();
			}
				
			if(addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postCode1'))) {
				// AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);
				
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES});
	
				//generate the hint
				
				hint = AssetManagement.customer.utils.StringUtils.trimStart(notif.get('qmnum'), '0');
				
				request.addAddressWithHint(addressInformation, hint);
				
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToAddress of BaseNotifListPageController', ex);
		}
    },
    
    beforeDataReady: function() {
    	try {
    		//apply the current filter criteria on the store, before it is transferred into the view
    		this.applyCurrentFilterCriteriasToStore();
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside beforeDataReady of BaseNotifListPageController', ex);
		}
	},

	applyCurrentFilterCriteriasToStore: function() {
    	try {
			this.getView().getSearchValue();

			var notifToSearch = this.getViewModel().get('searchNotif');
			var notifStore = this.getViewModel().get('notifStore');
			
			if(notifStore) {
				notifStore.clearFilter();
	
				// Meldungsnummer
				if(notifToSearch.get('qmnum')) {
					notifStore.filterBy(function(record) {
						try {
						    if (record.get('qmnum') && record.get('qmnum').toUpperCase().indexOf(notifToSearch.get('qmnum').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// Kurztext
				} else if(notifToSearch.get('qmtxt')) {
					notifStore.filterBy(function(record) {
						try {
						    if (record.get('qmtxt') && record.get('qmtxt').toUpperCase().indexOf(notifToSearch.get('qmtxt').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// Auftragsnummer
				} else if(notifToSearch.get('aufnr')) {
					notifStore.filterBy(function(record) {
						try {
						    if (record.get('aufnr').toUpperCase().indexOf(notifToSearch.get('aufnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// Equipment
				} else if(notifToSearch.get('equnr') || (notifToSearch.get('equipment') && notifToSearch.get('equipment').get('eqktx'))) {
					var tempEqunr = notifToSearch.get('equnr') ? notifToSearch.get('equnr').toUpperCase() : '';
					var tempEqktx = notifToSearch.get('equipment') ? notifToSearch.get('equipment').get('eqktx') : '';
					tempEqktx = tempEqktx ? tempEqktx.toUpperCase() : '';
				
					notifStore.filterBy(function(record) {
						try {
							if(tempEqunr && record.get('equnr') && record.get('equnr').toUpperCase().indexOf(tempEqunr) > -1) {
								return true; 
							} else if(tempEqktx && record.get('equipment') && record.get('equipment').get('eqktx').toUpperCase().indexOf(tempEqktx) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// Techn. Platz
				} else if(notifToSearch.get('tplnr') || (notifToSearch.get('funcLoc') && notifToSearch.get('funcLoc').get('pltxt'))) {
					var tempTplnr = notifToSearch.get('tplnr') ? notifToSearch.get('tplnr').toUpperCase() : '';
					var tempPltxt = notifToSearch.get('funcLoc') ? notifToSearch.get('funcLoc').get('pltxt') : '';
					tempPltxt = tempPltxt ? tempPltxt.toUpperCase() : '';
				
					notifStore.filterBy(function(record) {
						try {
							if(tempTplnr && record.get('tplnr') && record.get('tplnr').toUpperCase().indexOf(tempTplnr) > -1) {
								return true; 
							} else if(tempPltxt && record.get('funcLoc') && record.get('funcLoc').get('pltxt').toUpperCase().indexOf(tempPltxt) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
					// Verantw. Arbeitsplatz
				} else if(notifToSearch.get('gewrk')) {
					var criteria = notifToSearch.get('gewrk').toUpperCase();
				
					notifStore.filterBy(function(record) {
						try {
						    if(record.get('gewrk') && record.get('gewrk').toUpperCase().indexOf(criteria) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// Sortierfeld
				} else if(notifToSearch.get('equipment') && notifToSearch.get('equipment').get('eqfnr')) {
					notifStore.filterBy(function(record) {
						try {
						    if ( record.get('equipment') && record.get('equipment').get('eqfnr').toUpperCase().indexOf(notifToSearch.get('equipment').get('eqfnr').toUpperCase()) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// Datum
				} else if(notifToSearch.get('strdt')) {
					var criteria = notifToSearch.get('strdt').toUpperCase();
				
					notifStore.filterBy(function(record) {
						try {
							if (record.get('strdt') && AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(record.get('strdt')).indexOf(criteria) > -1) {
						    	return true; 
						    }
					    } catch(ex) {
							AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
						}
					});
				// PLZ
				} else if(notifToSearch.get('address')) {
					var searchAddress = notifToSearch.get('address');
				
					//PLZ
					if(searchAddress.getPostalCode()) {
						var criteria = searchAddress.getPostalCode().toUpperCase();
					
						notifStore.filterBy(function(record) {
							try {
								var objectAddress = record.getObjectAddress();
							
								if(objectAddress && objectAddress.getPostalCode().toUpperCase().indexOf(criteria) > -1) {
									return true;
								}
						    } catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
							}
						});
					// Ort
					} else if(searchAddress.getCity()) {
						var criteria = searchAddress.getCity().toUpperCase();
					
						notifStore.filterBy(function(record) {
							try {
								var objectAddress = record.getObjectAddress();
							
								if(objectAddress && objectAddress.getCity().toUpperCase().indexOf(criteria) > -1) {
									return true;
								}
						    } catch(ex) {
								AssetManagement.customer.helper.OxLogger.logException('Exception occured while filtering in BaseNotifListPageController', ex);
							}
						});
					}
				}
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside applyCurrentFilterCriteriasToStore of BaseNotifListPageController', ex);
		}
    },
    
    onNotifSearchFieldChange: function() {
    	try {
			this.applyCurrentFilterCriteriasToStore();
			this.getView().refreshView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifSearchFieldChange of BaseNotifListPageController', ex)
		}
    },
   
    onCellContextMenu: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			e.stopEvent();
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
			
			var options = [ ];

			this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseNotifListPageController', ex);
		}
    },

    //functions for search
    //public
    onSearchButtonClicked: function () {
        try {
            var myModel = this.getViewModel();

            this.manageSearchCriterias();

            var searchDialogArgs = {
                source: myModel.get('notifStore'),
                criterias: myModel.get('searchCriterias')
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.SEARCH_DIALOG, searchDialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchButtonClicked of BaseNotifListPageController', ex);
        }
    },


    //private
    //handles initialization of search criterias
    manageSearchCriterias: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');
            //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
            var searchMixin = this.mixins.handlerMixin

            if (!currentSearchCriterias) {
                var gridpanel = Ext.getCmp('notifGridPanel');

                //var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray);
                var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(gridpanel.searchArray, searchMixin);

                myModel.set('searchCriterias', newCriterias);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of OrderListPageController', ex);
        }
    },
    ////functions for search
    ////private
    ////handles initialization of search criterias
    //manageSearchCriterias: function () {
    //    try {
    //        var myModel = this.getViewModel();
    //        var currentSearchCriterias = myModel.get('searchCriterias');
    //        //need to pass the search functions to the generateDynamicSearchCriterias so that the criterias are built with custom search functions
    //        var searchMixin = this.mixins.handlerMixin

    //        if (!currentSearchCriterias) {
    //            var searchArray = [{
    //                id: 'qmnum',
    //                columnName: 'notif',
    //                searchFunction: '',
    //                position: 1
    //            },
	//              {
	//                  id: 'equnr',
	//                  columnName: 'equipment',
	//                  searchFunction: '',
	//                  position: 1
	//              },
    //            {
    //                id: 'tplnr',
    //                columnName: 'funcLoc_Short',
    //                searchFunction: '',
    //                position: 1
    //            },
    //             {
    //                 id: 'aufnr',
    //                 columnName: 'order',
    //                 searchFunction: '',
    //                 position: 1
    //             },
    //            {
    //                id: 'matnr',
    //                columnName: 'material',
    //                searchFunction: '',
    //                position: 1
    //            },
	//            {
	//                id: 'eqfnr',
	//                columnName: 'sortField',
	//                searchFunction: '',
	//                position: 1
	//            },
	//            {
	//                id: 'postalCode',
	//                columnName: 'address',
	//                searchFunction: '',
	//                position: 1
	//            },
    //            ];

    //            var newCriterias = AssetManagement.customer.modules.search.SearchDynamicCriteriaProvider.generateDynamicSearchCriterias(searchArray, searchMixin);

    //            myModel.set('searchCriterias', newCriterias);
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSearchCriterias of BaseNotifListPageController', ex);
 //    }
    //},
 
    
    //functions for search
    //public
    onSearchApplied: function (searchResult) {
        try {

            this.getViewModel().set('constrainedNotifs', searchResult);

            this.setupInitialSorting();

            //this.setupAutoCompletion();

            this.applyCurrentFilterCriteriasToStore();

            this.refreshView();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSearchApplied of BaseNotifListPageController', ex);
        }
    },

    //functions for search
    //private
    //sets up the initial sorting on the constrained store
    setupInitialSorting: function () {
        try {
            var myModel = this.getViewModel();
            var constrainedEquipments = myModel.get('constrainedNotifs');

            //if there isn't any sort direction yet, setup an intial default sorting by agreement/reservation
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(myModel.get('sortProperty'))) {
                myModel.set('sortProperty', 'mymAgreement');
                myModel.set('sortDirection', 'DESC');
            }

            //apply the current sort rule
            if (constrainedEquipments) {
                constrainedEquipments.sort([
                    {
                        property: myModel.get('sortProperty'),
                        direction: myModel.get('sortDirection')
                    }
                ]);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setupInitialSorting of BaseNotifListPageController', ex);
        }
    },
    //functions for search
    //private
    onClearSearchButtonClicked: function () {
        try {
            //first check, if there is anything set, if not just return - do not annoy user by popup
            if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.getView().getFilterValue()) &&
                AssetManagement.customer.modules.search.SearchExecutioner.checkCriteriasForEmpty(this.getViewModel().get('searchCriterias'))) {
                return;
            }


            //ask the user, if he really want's to reset search and filter criterias    
            var callback = function (confirmed) {
                try {
                    if (confirmed === true) {
                        this.clearFilterAndSearchCriteria();
                        this.update();
                    }
                } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseNotifListPageController', ex);
                }
            };

            var dialogArgs = {
                title: Locale.getMsg('clearSearch'),
                icon: 'resources/icons/clear_search.png',
                message: Locale.getMsg('clearSearchAndFilterConf'),
                alternateConfirmText: Locale.getMsg('delete'),
                alternateDeclineText: Locale.getMsg('cancel'),
                callback: callback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CONFIRM_DIALOG, dialogArgs);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onClearSearchButtonClicked of BaseNotifListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears filter and search criterias
    clearFilterAndSearchCriteria: function () {
        try {
            this.clearSearchCriteria();
            this.clearFilter();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilterAndSearchCriteria of BaseNotifListPageController', ex);
        }
    },

    //functions for search
    //private
    //clears the filter
    clearFilter: function () {
        try {
            this.getView().setFilterValue('');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearFilter of BaseNotifListPageController', ex);
        }
    },
    //functions for search
    //private
    //clears all search criterias
    clearSearchCriteria: function () {
        try {
            var myModel = this.getViewModel();
            var currentSearchCriterias = myModel.get('searchCriterias');

            if (currentSearchCriterias && currentSearchCriterias.getCount() > 0) {
                currentSearchCriterias.each(function (criteria) {
                    criteria.set('values', []);
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside clearSearchCriteria of BaseNotifListPageController', ex);
        }
    }
});