Ext.define('AssetManagement.base.controller.pages.BaseNotifDetailPageController', {
	extend: 'AssetManagement.customer.controller.pages.OxPageController',

	
	requires: [
		'AssetManagement.customer.utils.StringUtils',
		'AssetManagement.customer.manager.AddressManager',
		'AssetManagement.customer.manager.DMSDocumentManager',
		'AssetManagement.customer.manager.DocUploadManager',
		'AssetManagement.customer.model.bo.Notif',
		'AssetManagement.customer.controller.dialogs.NotifItemDialogController',
		'AssetManagement.customer.controller.dialogs.NotifTaskDialogController',
		'AssetManagement.customer.controller.dialogs.NotifActivityDialogController',
		'AssetManagement.customer.helper.NetworkHelper',
		'AssetManagement.customer.helper.OxLogger',
		'AssetManagement.customer.model.helper.ReturnMessage'
	],

	config: {
		contextReadyLevel: 1,
		rowMenu: null
	},

	mixins: {
	    handlerMixin: 'AssetManagement.customer.controller.mixins.NotifDetailPageControllerMixin'
	},
	
    _deleteRunning: false,

	//public		
	//@override
	onOptionMenuItemSelected: function(optionID) {
	    try {
	        if (this._deleteRunning)
	            return;

	    	if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT || optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST) {
	    	    this.showLongtextDialog();
			} else if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_NEW_SDORDER) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_SDORDER, { qmnum: this.getViewModel().get('notif').get('qmnum') });
			} else if (optionID === AssetManagement.customer.controller.ToolbarController.OPTION_DELETE) {
			    this.deleteNotif();
			}
		} catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of BaseNotifDetailPageController', ex);
		}
	},
	
    onContextMenuItemSelected: function(itemID, record) {
        try {
            if (this._deleteRunning)
                return;

            var className = Ext.getClassName(record);

            if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_LONGTEXT) {
                var customizingParamName = '';

                if (className.indexOf('NotifItem') > -1) {
    	            customizingParamName = 'c_ltxt_notif_item';
                } else if (className.indexOf('NotifTask') > -1) {
    	            customizingParamName = 'c_ltxt_notif_task';
                } else if (className.indexOf('NotifActivity') > -1) {
    	            customizingParamName = 'c_ltxt_notif_activity';
                }

                this.showLongtextDialogSubObject(record, customizingParamName);
            } else if(itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT) {
                if (className.indexOf('NotifItem') > -1) {
    	            this.editItem(record);
                } else if (className.indexOf('NotifTask') > -1) {
                    this.editTask(record);
                } else if (className.indexOf('NotifActivity') > -1) {
                    this.editActivity(record);
                }
            } else if (itemID === AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE) {
                if (className.indexOf('DocUpload') > -1) {
                    this.deleteDocUploadClicked(record);
                } else if (className.indexOf('NotifItem') > -1) {
                    this.deleteItem(record);
                } else if (className.indexOf('NotifTask') > -1) {
                    this.deleteTask(record);
                } else if (className.indexOf('NotifActivity') > -1) {
                    this.deleteActivity(record);
                }
            }
       } catch (ex) {
    	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseNotifDetailPageController', ex);
       }
	},
	
	//protected
	//@override
    onCreateOptionMenu: function() {
    	this._optionMenuItems = new Array();
    
    	try {
    	    if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ltxt_notif_head') === 'X') {
    	        var notifsBLT = this.getViewModel().get('notif').get('backendLongtext');
    	        var notifsLLT = this.getViewModel().get('notif').get('localLongtext');

    	        if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifsBLT) || !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(notifsLLT)){
    	            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT_EXIST);
    	        } else {
    	            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LONGTEXT);
    	        }
    	    }

    	    if(this.getViewModel().get('notif').get('qmnum').indexOf('MO') !== -1){
    	        this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_DELETE);
    	    }

    	    //ext_scen is customer specific!!
    	    //if (funcParaInstance.getValue1For('ext_scen_active') === 'X') {
    		//	this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_NEW_SDORDER);
    		//}
    		
    	} catch (ex) {
     	   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of BaseNotifDetailPageController', ex);
 		}
    },
	
	//@override
	startBuildingPageContext: function(argumentsObject) {
		try {
			//extract parameters, if any given
			//if strings are passed, load business objects them with passedStringsParametersToBOs
			//else proceed first database query
			var qmnum = '';
			
			if(typeof argumentsObject['qmnum'] === 'string') {
				qmnum = argumentsObject['qmnum'];
			} else if(Ext.getClassName(argumentsObject['notif']) === 'AssetManagement.customer.model.bo.Notif') {
				this.getViewModel().set('notif', argumentsObject['notif']);
			} else {
				throw Ext.create('AssetManagement.customer.utils.OxException', {
					message: 'Insufficient parameters',
					method: 'startBuildingPageContext',
					clazz: 'AssetManagement.customer.controller.pages.NotifDetailPageController'
				});
			}
			var qmnumPassed = qmnum !== '';
			
			if(qmnumPassed) {
				this.getNotif(qmnum);
			} else {
				this.waitForDataBaseQueue();
			}
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside startBuildingPageContext of BaseNotifDetailPageController', ex);
		    this.errorOccurred();
		}
	},
	
	getNotif: function(qmnum) {
		try {
			AssetManagement.customer.manager.CustCodeManager.getCustCodes(true);
		
			var notifRequest = AssetManagement.customer.manager.NotifManager.getNotif(qmnum, true);
			this.addRequestToDataBaseQueue(notifRequest, 'notif');
				
			this.waitForDataBaseQueue();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNotif of BaseNotifDetailPageController', ex)
		}
	},
	
    //protected
	//@override
    getContextLoadingErrorMessage: function() {
		return Locale.getMsg('errorLoadingNotifContext');
    },
	
	navigateToFuncLoc: function() {
		try {
			if(this.getViewModel().get('notif').get('funcLoc')) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_FUNCLOC_DETAILS, { tplnr: this.getViewModel().get('notif').get('tplnr')  });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('funcLocNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToFuncLoc of BaseNotifDetailPageController', ex)
		}
	},
	
	navigateToEquipment: function() {
		try {
			if(this.getViewModel().get('notif').get('equipment')) {
				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_EQUIPMENT_DETAILS, { equnr: this.getViewModel().get('notif').get('equnr')  });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('equipmentNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToEquipment of BaseNotifDetailPageController', ex)
		}
	},
	
	navigateToOrder: function() {
	    try {
            if (this.getViewModel().get('notif').get('aufnr')) {
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_ORDER_DETAILS, { aufnr: this.getViewModel().get('notif').get('aufnr') });
            } else if (AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('ord_create') === 'X') {
		        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_ORDER, { notif: this.getViewModel().get('notif'), equipment: this.getViewModel().get('notif').get('equipment'), funcLoc: this.getViewModel().get('notif').get('funcLoc') });
		    } else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('orderNotMobile'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
        	
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToOrder of BaseNotifDetailPageController', ex)
		}
	},
	
	onItemSelected: function(tableview, td, cellIndex, record, tr,  rowIndex, e, eOpts) {
	    try {
		     AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NOTIFITEM_DETAILS, { item: record, qmnum: record.get('qmnum')});
		} catch(ex) {
			 AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onItemSelected of BaseNotifDetailPageController', ex)
		}
	},
	
	addItemClick: function(){
	    try {
		    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFITEM_DIALOG, { notif: this.getViewModel().get('notif') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addItemClick of BaseNotifDetailPageController', ex)
		}
	},
	
	addActivityClick: function() {
	    try {
		    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFACTIVITY_DIALOG, { notif: this.getViewModel().get('notif') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addActivityClick of BaseNotifDetailPageController', ex)
		}
	},
	
	addTaskClick: function() {
	    try {
		    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFTASK_DIALOG, { notif: this.getViewModel().get('notif') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addTaskClick of BaseNotifDetailPageController', ex)
		}    
	},
	
	showLongtextDialog: function() {
	    try {
	    	var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For('c_ltxt_notif_head') === '';
		    AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { notif: this.getViewModel().get('notif') , readOnly: readOnly });
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialog of BaseNotifDetailPageController', ex)
		}    
	},
	
	onNotifItemSaved: function(itemObject){
	    try {
		    this.addNotifItemToListStore(itemObject); 
		    this.getView().refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifItemSaved of BaseNotifDetailPageController', ex)
		}     
	},


	addNotifItemToListStore: function(itemObject) {
	    try {
		    var temp = this.getViewModel().get('notif').get('notifItems').getById(itemObject.get('id'));
		    if(temp === undefined || temp === null)
			   this.getViewModel().get('notif').get('notifItems').insert(0, itemObject);
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNotifItemToListStore of BaseNotifDetailPageController', ex)
		}	   	
	},
	
	onNotifActivitySaved: function(activityObject){
	    try {
		    this.addNotifActivityToListStore(activityObject); 
		    this.getView().refreshView(); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifActivitySaved of BaseNotifDetailPageController', ex)
		}   
	},
	
	
	addNotifActivityToListStore: function(activityObject) {
	    try {
		    var temp = this.getViewModel().get('notif').get('notifActivities').getById(activityObject.get('id'));
		    if(temp === undefined || temp === null)
			    this.getViewModel().get('notif').get('notifActivities').insert(0, activityObject);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNotifActivityToListStore of BaseNotifDetailPageController', ex)
		}	    	
	},
	
	onNotifTaskSaved: function(taskObject){
	    try {
		    this.addNotifTaskToListStore(taskObject); 
		    this.getView().refreshView(); 
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onNotifTaskSaved of BaseNotifDetailPageController', ex)
		}    
	},
		
	addNotifTaskToListStore: function(taskObject) {
	    try {
		    var temp = this.getViewModel().get('notif').get('notifTasks').getById(taskObject.get('id'));
		    if(temp === undefined || temp === null)
			   this.getViewModel().get('notif').get('notifTasks').insert(0, taskObject);
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addNotifTaskToListStore of BaseNotifDetailPageController', ex)
		}	   
	},
	
	navigateToPartnerAddress: function(partner) {
		try {
			var addressInformation = null;
		
			if(partner) {
				addressInformation = AssetManagement.customer.manager.AddressManager.mapPartnerIntoAddress(partner);
			}
			
			if(addressInformation && !AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('city1')) &&
					!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(addressInformation.get('postCode1'))) {
				//AssetManagement.customer.helper.NetworkHelper.openAddress(addressInformation);
				var request = Ext.create('AssetManagement.customer.modules.googleMaps.GoogleMapsRequest', {
					type: AssetManagement.customer.modules.googleMaps.GoogleMapsRequest.REQUEST_TYPES.SHOW_ADDRESSES
				});

				//generate the hint					
				hint = AssetManagement.customer.utils.StringUtils.trimStart(partner.get('parvw'), '0');

				request.addAddressWithHint(addressInformation, hint);

				AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_GOOGLEMAPS, { request: request });
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('dataForSearchInsufficient'), false, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateToPartnerAddress of BaseNotifDetailPageController', ex);
		}
	},

	//long tap to open context menu
	onCellContextMenuSubOjects: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
		e.stopEvent();
		
		if(!this.rowMenu)
			this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
			
		var options = [ ];
				
        var funcPara = AssetManagement.customer.model.bo.FuncPara.getInstance();
	    var className = Ext.getClassName(record);

	    if ((className === 'AssetManagement.customer.model.bo.NotifTask' && funcPara.getValue1For('c_ltxt_notif_task') === 'X')
            || (className === 'AssetManagement.customer.model.bo.NotifActivity' && funcPara.getValue1For('c_ltxt_notif_activity') === 'X')
                || ((className === 'AssetManagement.customer.model.bo.NotifItem' && funcPara.getValue1For('c_ltxt_notif_item') === 'X')))
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_LONGTEXT);
		
        //for a local created record delete and edit must be available always
	    if (record.get('updFlag') === 'I' || record.get('updFlag') === 'A') {
	        options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
	        options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
	    } else if ((className === 'AssetManagement.customer.model.bo.NotifTask' && funcPara.getValue1For('head_task_change') === 'X')
            || (className === 'AssetManagement.customer.model.bo.NotifActivity' && funcPara.getValue1For('head_activity_change') === 'X')
                || ((className === 'AssetManagement.customer.model.bo.NotifItem' && funcPara.getValue1For('head_item_change') === 'X'))) {
	        options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
	        options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_EDIT);
	    }
		
		this.rowMenu.onCreateContextMenu(options, record); 
			
		if(options.length > 0) {
			this.rowMenu.setDisabled(false);
			this.rowMenu.showAt(e.getXY());
		} else {
			var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
			AssetManagement.customer.core.Core.getMainView().showNotification(notification);
		}
	},
	
	//long tap to open context menu
	onCellContextMenuFileList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
		try {
			e.stopEvent();
			
			if(!this.rowMenu)
				this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});

			var options = [ ];				

			if(record.get('fileOrigin') === 'local') {
				options.push(AssetManagement.customer.controller.ContextMenuController.OPTION_MENU_DELETE);
			}
			
			this.rowMenu.onCreateContextMenu(options, record);
			
			if(options.length > 0) {
				this.rowMenu.setDisabled(false);
				this.rowMenu.showAt(e.getXY());
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('noOptionsForListItem'), true, 1);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuFileList of BaseNotifDetailPageController', ex)
		}
	},
	
	onCellContextMenuPartner: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
		try {
   			e.stopEvent();
				if(!this.rowMenu)
					this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
				var options = [ ];

				this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenu of BaseNotifDetailPageController', ex);
		}
	},
	
	onCellContextMenuSDOrderList: function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
   			e.stopEvent();
				if(!this.rowMenu)
					this.rowMenu = Ext.create('AssetManagement.customer.view.ContextMenu', {});
				
				var options = [ ];

				this.rowMenu.onCreateContextMenu(options, record); 
		} catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCellContextMenuObjectList of BaseNotifDetailPageController', ex);
		}
	},
	
	showLongtextDialogSubObject: function(busObject, customizingParamName) {
		try {
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(customizingParamName)){
				var readOnly = AssetManagement.customer.model.bo.FuncPara.getInstance().getValue1For(customizingParamName) === '';
				AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.LONGTEXT_DIALOG, { bo: busObject , readOnly: readOnly});
			} else {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('unknownDialogRequested'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showLongtextDialogSubObject of BaseNotifDetailPageController', ex);
		}
	},

	onFileSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
		    //if the record represents an DMS document, download it from SAP system
		    //else try open its binary content
		    if (record.get('fileOrigin') === 'online') {
		        AssetManagement.customer.manager.DMSDocumentManager.downloadDMSDocument(record);
		    } else {
		        AssetManagement.customer.manager.DocUploadManager.openDocument(record);
		    }
		} catch (ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileSelected of BaseNotifDetailPageController', ex);
		}
	},
   	
	onFileForDocUploadSelected: function(file) {
		try {
			if(!file) {
				var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
				AssetManagement.customer.core.Core.getMainView().showNotification(notification);
			}
		
			var notif = this.getViewModel().get('notif');
		
			var callback = function(docUpload) {
				try {
					if(docUpload) {
						//not neccessary, because the documents store of the notif will automatically refresh itself
						//the docUpload will be added to this store by the manager
						//this.getView().refreshView();
						
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileAdded'), true, 0);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					} else {
						var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorAddingFile'), false, 2);
						AssetManagement.customer.core.Core.getMainView().showNotification(notification);
					}
				} catch (ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseNotifDetailPageController', ex);
				}
			};
		
			var addEvent = AssetManagement.customer.manager.DocUploadManager.addNewDocUploadForNotif(file, notif);
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(addEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onFileForDocUploadSelected of BaseNotifDetailPageController', ex);
		}
	},

	editItem: function (item) {
	    try {
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFITEM_DIALOG, { notif: this.getViewModel().get('notif'), notifItem: item });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editItem of BaseNotifDetailPageController', ex);
	    }
	},

	editTask: function (task) {
	    try {
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFTASK_DIALOG, { notif: this.getViewModel().get('notif'), notifTask: task });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editTask of BaseNotifDetailPageController', ex);
	    }
	},

	editActivity: function (activity) {
	    try {
	        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.NOTIFACTIVITY_DIALOG, { notif: this.getViewModel().get('notif'), notifActivity: activity });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside editActivity of BaseNotifDetailPageController', ex);
	    }
	},
	
	deleteDocUploadClicked: function(docUpload) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   		if(success === true) {
	   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('fileDeleted'), true, 0);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingFile'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseNotifDetailPageController', ex);
   				}
   			};
   			
   			var deleteEvent = AssetManagement.customer.manager.DocUploadManager.deleteDocUpload(docUpload);

			if(deleteEvent > 0)
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteDocUploadClicked of BaseNotifDetailPageController', ex);
		}
	},
	
	deleteItem: function(item) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   		if(success === true) {
	   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifItemDeleted'), true, 0);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotifItem'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteItem of BaseNotifDetailPageController', ex);
   				}
   			};
   			
   			var deleteEvent = AssetManagement.customer.manager.NotifItemManager.deleteNotifItem(item);

			if(deleteEvent > 0)
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteItem of BaseNotifDetailPageController', ex);
		}
	},
	
	deleteTask: function(task) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   		if(success === true) {
	   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifTaskDeleted'), true, 0);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotifTask'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTask of BaseNotifDetailPageController', ex);
   				}
   			};
   			
   			var deleteEvent = AssetManagement.customer.manager.NotifTaskManager.deleteNotifTask(task);

			if(deleteEvent > 0)
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteTask of BaseNotifDetailPageController', ex);
		}
	},
	
	deleteActivity: function(activity) {
   		try {
   			var callback = function(success) {
   		   		try {
   		   		if(success === true) {
	   		   		var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifActivityDeleted'), true, 0);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				} else {
					var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotifActivity'), false, 2);
					AssetManagement.customer.core.Core.getMainView().showNotification(notification);
				}
   				} catch (ex) {
   					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteActivity of BaseNotifDetailPageController', ex);
   				}
   			};
   			
   			var deleteEvent = AssetManagement.customer.manager.NotifActivityManager.deleteNotifActivity(activity);

			if(deleteEvent > 0)
				AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(deleteEvent, callback);
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteActivity of BaseNotifDetailPageController', ex);
		}
	},
	
	
	onLongtextSaved: function(affectedObject, newLongtext) {
		try {
			if(affectedObject && affectedObject === this.getViewModel().get('notif')) {
				this.rebuildOptionsMenu();
			}
		} catch (ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onLongtextSaved of BaseNotifDetailPageController', ex);
		}
	},
	
	onSDOrderItemSelected: function(tableview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SDORDER_DETAILS, { vbeln: record.get('vbeln') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onSDOrderItemSelected of BaseNotifDetailPageController', ex)
		}
	},

	onAddSDOrderItemClick: function() {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_NEW_SDORDER, { qmnum: this.getViewModel().get('notif').get('qmnum') });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onAddSDOrderItemClick of BaseNotifDetailPageController', ex)
		}
	},

	deleteNotif: function () {
	    try {
	        var me = this;
	        var callback = function (deletedNotif) {
	            try {
	                if (deletedNotif) {
	                    //go back to notiflist
	                    AssetManagement.customer.core.Core.navigateBack();

	                    Ext.defer(function () {
	                        me._deleteRunning = false;
	                    }, 3000, me);

	                    var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('notifDeleted'), true, 0);
	                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	                } else {
	                    me._deleteRunning = false;
	                    AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	                    notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotif'), false, 2);
	                    AssetManagement.customer.core.Core.getMainView().showNotification(notification);
	                }
	            } catch (ex) {
	                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotif of BaseNotifDetailPageController', ex)
	                me._deleteRunning = false;
	                AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	            }
	        };

	        var eventId = AssetManagement.customer.manager.NotifManager.deleteNotification(this.getViewModel().get('notif'));

	        if (eventId > 0) {
	            AssetManagement.customer.controller.ClientStateController.enterLoadingState(0);
	            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, callback, me);
	        } else {
	            retval = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('errorDeletingNotif'), false, 2);
	        }
	          
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside deleteNotif of BaseNotifDetailPageController', ex);
	        this._deleteRunning = false;
	        AssetManagement.customer.controller.ClientStateController.leaveLoadingState();
	    }
	}
});