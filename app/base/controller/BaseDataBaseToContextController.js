Ext.define('AssetManagement.base.controller.BaseDataBaseToContextController', {
	extend: 'Ext.app.ViewController',
	
	requires: [
        'Ext.util.HashMap',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.controller.EventController'
    ],
	
    //this class is used by page and dialog controllers to interact with the database
    //task of this class is to fill the context object with values requested from the database
    //every request consists of: ID returned from DataBaseHelper, targetProperty of context, a value outReference
	
    config: {
		contextLevel: 0,
		dataBaseRequestQueue: null,
		lastDataBaseRequestId: 0
	},
	
	constructor: function(config) {
	    try {
		    this.initConfig(config);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseDataBaseToContextController', ex);
		}    
	},
	
	//protected
	getClassName: function() {
    	var retval = 'BaseDataBaseToContextController';
    
    	try {
    		var className = Ext.getClassName(this);
    		
    		if(AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
    			var splitted = className.split('.');
    			retval = splitted[splitted.length - 1];
    		} else {
    			retval = className;
    		}
    	} catch(ex) {
    		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of ' + this.getClassName(), ex);
    	}
    	
    	return retval;
	},
	
	//protected
	addRequestToDataBaseQueue: function(requestId, propertyToSet) {
		try {
			if(requestId === -1) {
				AssetManagement.customer.helper.OxLogger.logMessage('BaseDataBaseToContextController: Tried to add a database request with requestId -1');
				var myClassName = Ext.getClassName(this);
				
				AssetManagement.customer.helper.OxLogger.logMessage('Occured on class "' + myClassName + '" - For property: "' + propertyToSet + '"');
				
				//if this is the first error, set the flag and trigger the complete callback
				if(this._contextLevel != -1) {
					//set the flag, indicating, that an error occured
					this._contextLevel = -1;
				}
					
				return;
			} else if(this._contextLevel == -1) {
				//do not add requests, if there already occured any error
				return;
			}
		
			var me = this;
			
			if(this._dataBaseRequestQueue === null || this._dataBaseRequestQueue === undefined)
				this._dataBaseRequestQueue = Ext.create('Ext.util.HashMap');
				
			this._dataBaseRequestQueue.add(requestId, propertyToSet);
			
			var setterCallBack = function(passedArguments) {
				try {
					if(passedArguments !== undefined) {
						//only set property if it is not empty and there did not already occure any error
						if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(propertyToSet) && this._contextLevel != -1)
							this.getViewModel().set(propertyToSet, passedArguments);
					} else {
						var myClassName = Ext.getClassName(this);
						AssetManagement.customer.helper.OxLogger.logMessage('BaseDataBaseToContextController: A database request encountered an error and returned undefined');
						AssetManagement.customer.helper.OxLogger.logMessage('Occured on class "' + myClassName + '" - For property: "' + propertyToSet + '"');
						
						//set the flag, indicating, that an error occured
						this._contextLevel = -1;
					}
					
					this._dataBaseRequestQueue.removeAtKey(requestId);
				
					//do check, if work completed
					if(this._dataBaseRequestQueue.getCount() === 0) {
						this.dataBaseQueueComplete();
					}
				} catch(ex) {
					AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addRequestToDataBaseQueue of ' + this.getClassName(), ex);
					this._contextLevel = -1;
				}
			};
			
			AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(requestId, setterCallBack, this);
			
			this._lastDataBaseRequestId = requestId;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addRequestToDataBaseQueue of ' + this.getClassName(), ex);
			this._contextLevel = -1;
		}
	},
	
	//protected
	waitForDataBaseQueue: function() {
	    try {
	    	if(this._contextLevel == -1 || !this._dataBaseRequestQueue || this._dataBaseRequestQueue.getCount() === 0) {
				this.dataBaseQueueComplete();
			} else {
				AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
			}
	    } catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside waitForDataBaseQueue of ' + this.getClassName(), ex);
		}	
	},
	
	//protected
	//the following method has to be overridden by derivates
	//override must call the base version
	dataBaseQueueComplete: function() {
		try {
			if(this._contextLevel == -1 || (this._contextLevel == 0 && !this.checkBaseContext())) {
				if(this._dataBaseRequestQueue)
					this._dataBaseRequestQueue.clear();
				
				AssetManagement.customer.helper.OxLogger.logMessage('BaseDataBaseToContextController: An error occured. Abort loading of further data.');
				AssetManagement.customer.helper.OxLogger.logMessage(this.getContextLoadingErrorMessage());
				this._contextLevel = -1;
			} else {
				this._contextLevel++;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside dataBaseQueueComplete of ' + this.getClassName(), ex);
			this._contextLevel = -1;
		}
	},
	
	//protected
	//use this method to perform get requests on all base context objects
	//if any object is missing, return false
	//will be executed when context level 1 has been reached
	checkBaseContext: function() {
		return true;
	},
	
	//protected
	getContextLoadingErrorMessage: function() {
		var retval = 'Error while loading data for view';
		
		try {
			retval = Locale.getMsg('dataLoadingErrorOccured');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getContextLoadingErrorMessage of ' + this.getClassName(), ex);
		}

		return retval;
	}
});