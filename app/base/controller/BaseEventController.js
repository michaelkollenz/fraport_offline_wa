Ext.define('AssetManagement.base.controller.BaseEventController', {
	mixins: ['Ext.mixin.Observable'],

	requires: [
		'AssetManagement.customer.utils.StringUtils'
	],

	config: {
		runningEventID: 0
	},

	constructor: function(config) {
	    try {
	    	this.mixins.observable.constructor.call(this, config);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseEventController', ex);
		}
	},

	inheritableStatics: {
		//private:
	    _instance: null,

		//public:
	    getInstance: function() {
			try {
	            if(this._instance == null)
				   this._instance = Ext.create('AssetManagement.customer.controller.EventController');

			} catch(ex) {
			    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getInstance of BaseEventController', ex);
		    }

			return this._instance;
	    }
	},

	//public
	getNextEventId: function() {
		var retval = '';

		try {
		    retval = ((this._runningEventID++ % 10000) + 1) + '';
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNextEventID of BaseEventController', ex);
		}

		return retval;
	},

	requestEventFiring: function(eventIdToFire, args, scope, delay) {
	    try {
	    	var workaround = function() {
	    	    try {
				    this.fireEvent(eventIdToFire, args);
				} catch(ex) {
				    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestEventFiring of BaseEventController', ex);
			    }
			};

			if(delay === undefined || delay === null || delay <= 0)
				delay = 1;

			if(!scope)
				scope = this;

			Ext.defer(workaround, delay, scope);
	    } catch(ex) {
		   AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside requestEventFiring of BaseEventController', ex);
	    }
	},

	registerOnEvent: function(eventIdToListenTo, callback, scope, options) {
	    try {
		    this.addListener(eventIdToListenTo, callback, scope ? scope : undefined, options ? options : undefined);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerOnEvent of BaseEventController', ex);
		}
	},

	registerOnEventForOneTime: function(eventIdToListenTo, callback, scope) {
	    try {
		    this.addListener(eventIdToListenTo, callback, scope ? scope : undefined, { single: true });
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside registerOnEventForOneTime of BaseEventController', ex);
		}
	},

	unregisterFromEvent: function(eventIdToListenTo, callback) {
	    try {
		    this.removeListener(eventIdToListenTo, callback);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside unregisterFromEvent of BaseEventController', ex);
		}
	}
});