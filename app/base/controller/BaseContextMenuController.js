Ext.define('AssetManagement.base.controller.BaseContextMenuController', {
    extend: 'Ext.app.ViewController',

   
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'Ext.util.HashMap'
    ],
    
    inheritableStatics: {
		//public
		OPTION_MENU_EDIT: '2',
		OPTION_MENU_DELETE: '3',
		OPTION_LONGTEXT: '4',
		OPTION_MENU_PRINT_CHECKLIST: '5',
			
		
		_instanceCounter: 0,
		
		getNextContextMenuId: function() {
			return ++this._instanceCounter;
		}
	},
	
	//private 
	_internalId: '',
	_itemToElementIdMap: null,
	_currentSupportedItems: null,
	
	constructor: function(config) {
		this.callParent(arguments);
		
		try {
		    this._internalId = 'contextMenu' + this.self.getNextContextMenuId();
		
		    this.initializeOptionToElementIdMap();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseContextMenuController', ex);
		}    
	},
	
	onContextMenuItemSelected: function(menu, item, e, eOpts ){
		try {
			var selectedItem = this.getOptionOfItem(menu);
    		
    		AssetManagement.customer.core.Core.getCurrentPage().getController().onContextMenuItemSelected(selectedItem, this.getView().record);
    		
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onContextMenuItemSelected of BaseContextMenuController', ex);
		}
	},
	
	getInternalId: function() {
		return this._internalId;
	},

 	//private
	initializeOptionToElementIdMap: function() {
		try {
			this._itemToElementIdMap = Ext.create('Ext.util.HashMap');
			
			var idPrefix = this._internalId + '-';
			
			this._itemToElementIdMap.add(this.self.OPTION_MENU_DELETE, idPrefix + 'deleteButtonMI');
			this._itemToElementIdMap.add(this.self.OPTION_MENU_EDIT, idPrefix + 'editButtonMI');
			this._itemToElementIdMap.add(this.self.OPTION_LONGTEXT, idPrefix + 'longtextButtonMI');
			this._itemToElementIdMap.add(this.self.OPTION_MENU_PRINT_CHECKLIST, idPrefix + 'printChecklistButtonMI');
	
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeOptionToElementIdMap of BaseContextMenuController', ex);
		}
	},
	
	setMenuItems: function(arrayOfItems) {
		try {
			this._currentSupportedItems = arrayOfItems;
		
			this.transferItemsIntoView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMenuItems of BaseContextMenuController', ex);
		}
	},
	
	transferItemsIntoView: function() {
		try {
			var itemsToShow;
			
			itemsToShow = this._currentSupportedItems;
			
			//convert options to ids
			var arrayOfOptionsAsIds = [];
			
			if(itemsToShow) {
				Ext.Array.each(itemsToShow, function(item) {
					var id = this._itemToElementIdMap.get(item);
					
					if(id)
						arrayOfOptionsAsIds.push(id);
				}, this);
			}
		
			this.getView().setContextMenuItems(arrayOfOptionsAsIds);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferOptionsIntoView of BaseContextMenuController', ex);
		}
	},
	
	getOptionOfItem: function(item) {
		var retval = undefined;
	
		try {
			var toSearchFor = item.id;
		
			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(toSearchFor) && this._itemToElementIdMap) {
				this._itemToElementIdMap.each(function(key, value) {
					if(toSearchFor === value) {
						retval = key;
						return false;
					}
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOptionOfItem of BaseContextMenuController', ex);
		}
		
		return retval;
	}
    
});
