Ext.define('AssetManagement.base.controller.BaseToolbarController', {
    extend: 'Ext.app.ViewController',


    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.view.menus.AlternateOptionsMenu',
        'Ext.menu.Manager',
        'Ext.util.HashMap'
    ],

    inheritableStatics: {
		//public
		OPTION_SEARCH: '2',
		OPTION_EDIT: '3',
		OPTION_DUPLICATE: '4',
		OPTION_CLASS: '5',
		OPTION_ADD: '6',
		OPTION_MEASPOINT: '7',
	    OPTION_LONGTEXT: '8',
		OPTION_EQUICHANGE: '9',
		OPTION_SAVE: '10',
		OPTION_DELETE: '11',
		OPTION_RFID: '12',
		OPTION_REPORT: '13',
		OPTION_PRINT: '14',
		OPTION_NEWITEM: '15',
		OPTION_NEW_SDORDER: '16',
		OPTION_SHOW_MY_MAPS_POSITION: '18',
		OPTION_NEWORDER: '19',
		OPTION_NEWNOTIF: '20',
		OPTION_LONGTEXT_EXIST: '21',
		OPTION_OBJ_STRUCTURE: '22',
		OPTION_BARCODE: '23',
		OPTION_PRINT_MULTIPLE_REPORTS: '24',
		OPTION_SIGNING_BUTTON: '25',
		OPTION_SAVE_REPORTS: '26',
		OPTION_CLEAR_SEARCH: '27',
    OPTION_INTERNNOTE: '28',

    OPTION_COMMIT: '50',
    OPTION_LGORT_CHANGE: '51',

		//private
    OPTION_HOME: '0',
    OPTION_BACK: '1',
		OPTION_MENU: '99',
		OPTION_MAINMENU: '17'
	},

	//private
	_optionToElementIdMap: null,
	_currentSupportedOptions: null,
	_titleLabel: null,
	_alternateOptionsMenu: null,
	_mainView: null,

	constructor: function(config) {
		this.callParent(arguments);

		try {
		    this._alternateOptionsMenu =  Ext.create('AssetManagement.customer.view.menus.AlternateOptionsMenu');
		    this.initializeOptionToElementIdMap();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseToolbarController', ex);
		}
	},

	//public
    setTitle: function(title) {
		try {
			this.getView().getTitleLabel().setHtml(title);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setTitle of BaseToolbarController', ex);
		}
    },

   	onOptionSelected: function(firingButton) {
    	try {

    		var selectedOption = this.getOptionOfButton(firingButton);

    		     firingButton.removeCls('x-btn-over');
    			 firingButton.removeCls('x-btn-focus');

    		if(selectedOption === this.self.OPTION_HOME) {
    			this.onHomeOptionSelected();
    		} else if(selectedOption === this.self.OPTION_BACK) {
    			this.onBackOptionSelected();
    		} else if(selectedOption === this.self.OPTION_MAINMENU) {
    			this.onMainMenuOptionSelected();
    		} else {
    			AssetManagement.customer.core.Core.getCurrentPage().getController().onOptionMenuItemSelected(selectedOption);
    		}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionSelected of BaseToolbarController', ex);
		}
	},

	setOptionsMenuItems: function(arrayOfOptions) {
		try {
			this._currentSupportedOptions = arrayOfOptions;

			this.prepareAlternateOptionsMenu();
			this.transferOptionsIntoView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setOptionsMenuItems of BaseToolbarController', ex);
		}
	},

	setHomeButtonVisible: function(visible) {
		try {
			var homeButton = this.getView().getHomeButton();

			if(visible)
				homeButton.show();
			else
				homeButton.hide();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setHomeButtonVisible of BaseToolbarController', ex);
		}
	},

	setBackButtonVisible: function(visible) {
		try {
			var backButton = this.getView().getBackButton();

			if(visible)
				backButton.show();
			else
				backButton.hide();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setBackButtonVisible of BaseToolbarController', ex);
		}
	},

	setMainMenuButtonVisible: function(visible) {
		try {
			var mainMenuButton = this.getView().getMainMenuButton();

			if(visible)
				mainMenuButton.show();
			else
				mainMenuButton.hide();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMainMenuButtonVisible of BaseToolbarController', ex);
		}
	},

	reevaluateMenuReprensetation: function() {
		try {
			if(this._alternateOptionsMenu && this._alternateOptionsMenu.isVisible())
				this._alternateOptionsMenu.hide();

			this.transferOptionsIntoView();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside reevaluateMenuReprensetation of BaseToolbarController', ex);
		}
	},

	showAlternateOptionsMenu: function() {
		try {
			if(this._alternateOptionsMenu) {
				this._alternateOptionsMenu.showBy(this.getView().getAlternateOptionsMenuButton());
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showAlternateOptionsMenu of BaseToolbarController', ex);
		}
	},

	//private
	initializeOptionToElementIdMap: function() {
		try {
			this._optionToElementIdMap = Ext.create('Ext.util.HashMap');

			this._optionToElementIdMap.add(this.self.OPTION_MAINMENU, 'mainMenuButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_HOME, 'homeButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_BACK, 'backButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_SEARCH, 'searchButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_CLEAR_SEARCH, 'clearSearchButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_EDIT, 'editButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_NEW_SDORDER, 'newSDOrderButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_DUPLICATE, 'duplicateButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_CLASS, 'classCharButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_ADD, 'addButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_MEASPOINT, 'measpointsButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_LONGTEXT, 'longtextButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_LONGTEXT_EXIST, 'longtextButtonExistOM');
			this._optionToElementIdMap.add(this.self.OPTION_EQUICHANGE, 'equiChangeButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_SAVE, 'saveButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_DELETE, 'deleteButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_RFID, 'rfidButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_REPORT, 'reportButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_PRINT, 'printButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_NEWITEM, 'newItemButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_NEWORDER, 'newOrderButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_NEWNOTIF, 'newNotifButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_SHOW_MY_MAPS_POSITION, 'showMyMapsPositionButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_MENU, 'alternateOptionsMenuButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_OBJ_STRUCTURE, 'objStructureButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_BARCODE, 'barcodeButtonOM');
			this._optionToElementIdMap.add(this.self.OPTION_PRINT_MULTIPLE_REPORTS, 'printMultipleReports');
			this._optionToElementIdMap.add(this.self.OPTION_SIGNING_BUTTON, 'signingReportsButton');
			this._optionToElementIdMap.add(this.self.OPTION_SAVE_REPORTS, 'saveReportsButton');
			this._optionToElementIdMap.add(this.self.OPTION_INTERNNOTE, 'internNoteButtonOM');

      this._optionToElementIdMap.add(this.self.OPTION_COMMIT, 'commitMovementsOM');
      this._optionToElementIdMap.add(this.self.OPTION_LGORT_CHANGE, 'changeLgortOM');

		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeOptionToElementIdMap of BaseToolbarController', ex);
		}
	},

	transferOptionsIntoView: function() {
		try {
			//evaluate, how much space will be required to display the options
			//if there is to less space, just show the item "menu"
			var requestedSpace = 0;

			if(this._currentSupportedOptions && this._currentSupportedOptions.length > 0) {
				requestedSpace = 50 * this._currentSupportedOptions.length;
			}

			var availableSpace = this.getView().getAvailableOptionsMenuWidth();

			var optionsToShow;
			if(requestedSpace > availableSpace) {
				optionsToShow = [ this.self.OPTION_MENU ];
			} else {
				optionsToShow = this._currentSupportedOptions;
			}

			//convert options to ids
			var arrayOfOptionsAsIds = [];

			if(optionsToShow) {
				Ext.Array.each(optionsToShow, function(option) {
					var id = this._optionToElementIdMap.get(option);

					if(id)
						arrayOfOptionsAsIds.push(id);
				}, this);
			}

			this.getView().setOptionMenuItems(arrayOfOptionsAsIds);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside transferOptionsIntoView of BaseToolbarController', ex);
		}
	},

	getOptionOfButton: function(button) {
		var retval = undefined;

		try {
			var toSearchFor = button.id;

			if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(toSearchFor) && this._optionToElementIdMap) {
				this._optionToElementIdMap.each(function(key, value) {
					if(toSearchFor === value) {
						retval = key;
						return false;
					}
				}, this);
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getOptionOfButton of BaseToolbarController', ex);
		}

		return retval;
	},

	getMainView: function() {
		try {
			if (this._mainView === null){
				this._mainView = AssetManagement.customer.core.Core.getMainView();
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMainView of BaseToolbarController', ex);
		}
		return this._mainView;
	},

	prepareAlternateOptionsMenu: function() {
		try {
			this._alternateOptionsMenu.getController().setMenuItems(this._currentSupportedOptions);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside prepareAlternateOptionsMenu of BaseToolbarController', ex);
		}
	},

	onMainMenuOptionSelected: function() {
		try {
            this.getMainView().getController().toggleMainMenuVisibility();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onMainMenuOptionSelected of BaseToolbarController', ex);
		}
	},

	onHomeOptionSelected: function() {
		try {
			AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_HOMESCREEN);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onHomeOptionSelected of BaseToolbarController', ex);
		}
	},

	onBackOptionSelected: function() {
		try{
			AssetManagement.customer.core.Core.navigateBack();
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBackOptionSelected of BaseToolbarController', ex);
		}
	},

	onBarcodeScanned: function(scanResult) {
		try {
			AssetManagement.customer.core.Core.getCurrentPage().getController().onOptionMenuItemSelected(this.self.OPTION_BARCODE, scanResult);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onBarcodeScanned of ToolbarViewController', ex);
		}
    }
});