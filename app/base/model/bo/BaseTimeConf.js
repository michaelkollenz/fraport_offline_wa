Ext.define('AssetManagement.base.model.bo.BaseTimeConf', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    requires: [
        'AssetManagement.customer.utils.DateTimeUtils',
        'AssetManagement.customer.utils.StringUtils',
        'AssetManagement.customer.helper.OxLogger'
    ],

    fields: [
        //key fields
        { name: 'aufnr', type: 'string' },
        { name: 'vornr', type: 'string' },
        { name: 'split', type: 'string', defaultValue: '000'},
        { name: 'rmzhl', type: 'string' },
		
        //ordinary fields
        { name: 'ltxa1', type: 'string' },
        { name: 'learr', type: 'string' },
        { name: 'learrText', type: 'string' },
        { name: 'txtsp', type: 'string' },
        { name: 'ismnw', type: 'string', defaultValue: '0'},
        { name: 'ismne', type: 'string' },
        { name: 'aueru', type: 'string' },
        { name: 'pernr', type: 'string' },
        { name: 'bemot', type: 'string' },
        { name: 'grund', type: 'string' },
        { name: 'ernam', type: 'string' },
        { name: 'arbpl', type: 'string' },
        { name: 'werks', type: 'string' },
        { name: 'idaur', type: 'string', defaultValue: '0'},
        { name: 'idaue', type: 'string' },
        { name: 'ausor', type: 'string' },
        { name: 'ofmnw', type: 'string' },
        { name: 'ofmne', type: 'string' },
        { name: 'stagr', type: 'string' },
        
        { name: 'actype', type: 'string' },
        { name: 'usr1', type: 'string' },
        { name: 'usr2', type: 'string' },
        { name: 'kstar', type: 'string' },
        { name: 'betrag', type: 'string' },
        { name: 'tcurr', type: 'string' },
		
        //date fields
        { name: 'ersda', type: 'date' },
        { name: 'isd',   type: 'date' },
        { name: 'ied',   type: 'date' },
        { name: 'budat', type: 'date' },
        { name: 'pausesz', type: 'date' },
        { name: 'pauseez', type: 'date' },
		
        //delta fields
        { name: 'updFlag',   type: 'string' },
        { name: 'mobileKey', type: 'string' },
        { name: 'childKey',  type: 'string' },
        { name: 'childKey2', type: 'string' },
		
        //references
        { name: 'activityType',    		type: 'auto' },
        { name: 'accountIndication',    type: 'auto' },
        { name: 'localLongtext',   		type: 'auto' },
        { name: 'backendLongtext',      type: 'auto' },
        { name: 'custActGroup', type: 'auto'}
    ],
	
    constructor: function(config) {
        this.callParent(arguments);

        try {
            if (config) {
                if (config.order) {
                    var order = config.order;
            	
                    this.set('aufnr', order.get('aufnr'));
                    this.set('bemot', order.get('bemot'));
                    this.set('mobileKey', order.get('mobileKey'));
                }
            
                if (config.operation) {
                    var operation = config.operation;
    	        
                    //override order dependend fields, because order may be omitted
                    this.set('aufnr', operation.get('aufnr'));
                    this.set('vornr', operation.get('vornr'));
                    this.set('split', operation.get('split'));
                    this.set('werks', operation.get('werks'));
                    this.set('arbpl', operation.get('arbpl'));
                    this.set('pernr', operation.get('pernr'));
    	        	
                    this.set('imsne', operation.get('daune'));
                    this.set('idaue', operation.get('daune'));

                    this.set('mobileKey', operation.get('mobileKey'));
                    this.set('childKey', operation.get('childKey'));
    	        	
                    this.set('learr', operation.get('larnt'));
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseTimeConf', ex);
        }
    },
    
    getWorkValueForDisplay: function() {
        var retval = ''
    		
        try {
            var asFloat = 0.0;
    	
            //first parse the value
            if(!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.get('ismnw'))) {
                asFloat = AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(this.get('ismnw'));
				
                if(asFloat === Number.NaN) {
                    asFloat = 0.0;
                }
            }
    		
            var unit = this.get('ismne').toUpperCase();
            var caseHours = unit === 'H' || unit === 'STD';
    		
            if (caseHours) {
                if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(unit))
                    unit = 'H';

                var hours = Math.floor(asFloat);
                var minutesAsFraction = asFloat - hours;
                var minutes = Math.round(minutesAsFraction * 60);

                hours = AssetManagement.customer.utils.StringUtils.padLeft(hours, '0', 2);
                minutes = AssetManagement.customer.utils.StringUtils.padLeft(minutes, '0', 2);

                retval = hours + ':' + minutes + ' ' + unit;
            } else {
                retval = Math.round(asFloat) + ' ' + unit;
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getWorkValueForDisplay of BaseTimeConf', ex);
        }
    	
        return retval;
    }
});