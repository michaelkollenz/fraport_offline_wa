Ext.define('AssetManagement.base.model.bo.BaseFuncLoc', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
		'AssetManagement.customer.utils.StringUtils'
	],
   
	fields: [
		//key fields
		{ name: 'tplnr',    type: 'string' },
		
		//ordinary fields
		{ name: 'stort',    type: 'string' },
		{ name: 'pltxt',    type: 'string' },
		{ name: 'submt',    type: 'string' },
		{ name: 'iwerk',    type: 'string' },
		{ name: 'baujj',    type: 'string' },
		{ name: 'baumm',    type: 'string' },
		{ name: 'beber',    type: 'string' },
		{ name: 'eqart',    type: 'string' },
		{ name: 'eqfnr',    type: 'string' },
		{ name: 'fltyp',    type: 'string' },
		{ name: 'herld',    type: 'string' },
		{ name: 'herst',    type: 'string' },
		{ name: 'posnr',    type: 'string' },
		{ name: 'invnr',    type: 'string' },
		{ name: 'kostl',    type: 'string' },
		{ name: 'msgrp',    type: 'string' },
		{ name: 'rbnr',     type: 'string' },
		{ name: 'tplma',    type: 'string' },
		{ name: 'typbz',    type: 'string' },
		{ name: 'gewrk',    type: 'string' },
		{ name: 'wergw',    type: 'string' },
		{ name: 'txtGewrk', type: 'string' },
		{ name: 'objnr',    type: 'string' },
		{ name: 'adrnr',    type: 'string' },
		{ name: 'stsma',    type: 'string' },
        { name: 'abckz',      type: 'string' },
        { name: 'groes',     type: 'string' },
        { name: 'brgew',      type: 'string' },
        { name: 'gewei',      type: 'string' },
        { name: 'usr01flag',  type: 'string' },
        { name: 'usr02flag',  type: 'string' },
        { name: 'usr03code',  type: 'string' },
        { name: 'usr04code',  type: 'string' },
        { name: 'usr05txt',   type: 'string' },
        { name: 'usr06txt',   type: 'string' },
        { name: 'usr07txt',   type: 'string' },
        { name: 'usr09num',   type: 'string' },
		
		//date fields
		{ name: 'ansdt',	type: 'date'},
		{ name: 'datab',    type: 'date'},
        { name: 'usr08date',type: 'date'},
		
		//delta fields
		{ name: 'updFlag',         type: 'string' },

		//references
		{ name: 'address',          	type: 'auto' },
		{ name: 'classifications',  	type: 'auto' },
		{ name: 'objectStatusList', 	type: 'auto' },
		{ name: 'measPoints',   		type: 'auto' },
		{ name: 'documents',        	type: 'auto' },
		{ name: 'partners',    			type: 'auto' },
		{ name: 'historicalOrders',    	type: 'auto' },
		{ name: 'historicalNotifs',    	type: 'auto' },
		{ name: 'localLongtext',    	type: 'auto' },
		{ name: 'backendLongtext',  	type: 'auto' },
		
 		//mapping fields
	    { 	name: 'postalCode',
	    	convert: function (notUsed, data) {
	        	return data.getPostalCode.call(data);
	        },
	        depends: ['address']
	    }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	},

	getPostalCode: function() {
		var retval = '';
		
		try {
			var address = this.get('address');
			
			if(address) {
				retval = address.get('postCode1');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseFuncLoc', ex);
		}
		
		return retval;
	},

	getDisplayIdentification : function() {
        var retval = null;	
	
	    try {
		    retval = this.get('strno');

		    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(retval))
			    retval = this.get('tplnr');       
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDisplayIdentification of BaseFuncLoc', ex);
		}
			
		return retval;
	}
});