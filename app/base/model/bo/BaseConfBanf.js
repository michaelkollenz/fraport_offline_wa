Ext.define('AssetManagement.base.model.bo.BaseConfBanf', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'scenario',         type: 'string' },
	    
	    //ordinary fields
        { name: 'mm_scenario' ,   	type: 'string' },
		{ name: 'bsart',     		type: 'string' },
		{ name: 'pstyp' ,     		type: 'string' },
		{ name: 'ekorg',          	type: 'string' },
		{ name: 'ekgrp' ,     		type: 'string' },
		{ name: 'knttp',      		type: 'string' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});