﻿Ext.define('AssetManagement.base.model.bo.BaseQPlosOper', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
    fields: [
	    //key fields
	    { name: 'aufpl', type: 'string' },
	    { name: 'aplzl', type: 'string' },
		{ name: 'vornr', type: 'string' },

	    //ordinary fields
	    { name: 'ltxa1', type: 'string' },
		
	    //delta fields
		{ name: 'updFlag', type: 'string' }

    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});