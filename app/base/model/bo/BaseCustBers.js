Ext.define('AssetManagement.base.model.bo.BaseCustBers', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields 
	    { name: 'rbnr',     type: 'string' },
	    { name: 'qkatart',  type: 'string' },
	    { name: 'qcodegrp', type: 'string' },
   	
        //delta fields
		{ name: 'updFlag',  type: 'string' }
	],

	constructor: function(config) {
		this.callParent(arguments);
	}
});