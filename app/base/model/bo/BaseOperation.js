Ext.define('AssetManagement.base.model.bo.BaseOperation', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    requires: [
        'AssetManagement.customer.utils.NumberFormatUtils',
        'AssetManagement.customer.helper.OxLogger'
    ],

    fields: [
        //key fields
        { name: 'aufnr', type: 'string' }, 
        { name: 'vornr', type: 'string' },
		
        //split extra key fields
        { name: 'bedid', type: 'string' }, 
        { name: 'bedzl', type: 'string' },
        { name: 'canum', type: 'string' },
		
        //ordinary fields
        { name: 'aufpl', type: 'string' },
        { name: 'aplzl', type: 'string' },
        { name: 'steus', type: 'string' },
        { name: 'ltxa1', type: 'string' },
        { name: 'larnt', type: 'string' },
        { name: 'pernr', type: 'string' },
        { name: 'dauno', type: 'string' },
        { name: 'daune', type: 'string' },
        { name: 'anzma', type: 'string' },
        { name: 'arbei', type: 'string' },
        { name: 'arbeh', type: 'string' },
        { name: 'arbpl', type: 'string' },
        { name: 'werks', type: 'string' },
        { name: 'istru', type: 'string' },
        { name: 'stsma', type: 'string' },
        { name: 'objnr', type: 'string' },
        { name: 'ktsch', type: 'string' },
        { name: 'obknr', type: 'string' },
        { name: 'obzae', type: 'string' },
		{ name: 'txtsp', type: 'string' },
        { name: 'ablad', type: 'string' },
        { name: 'sumnr', type: 'string' },

        //split extra ordinary fields
        { name: 'split',     type: 'string', defaultValue: '000' },
        { name: 'arbid',     type: 'string' },
        { name: 'sdaune',    type: 'string' },
        { name: 'sdauno',    type: 'string' },
        { name: 'sarbei',    type: 'string' },
        { name: 'sarbeh',    type: 'string' },
        { name: 'stext',     type: 'string' },
		
        //date fields
        { name: 'fsav', 	type: 'date'  },
        { name: 'fsed',	 	type: 'date'  },
        { name: 'ntan', 	type: 'date'  },
        { name: 'nten', 	type: 'date'  },
        { name: 'isd', 		type: 'date' },
        { name: 'ied', 		type: 'date' },
		
        //split extra date fields
        { name: 'fstadt' },
        { name: 'fenddt' },
		
        //dynamic fields
        { name: 'tConfSum', type: 'number', defaultValue: 0 },
		
        //delta fields
        { name: 'updFlag',   type: 'string' },
        { name: 'mobileKey', type: 'string' },
        { name: 'childKey',  type: 'string' },

        { name: 'usr01Flag', type: 'string' },
        { name: 'usr02Flag', type: 'string' },
        { name: 'usr03Code', type: 'string' },
        { name: 'usr04Code', type: 'string' },
        { name: 'usr05Txt', type: 'string' },

        { name: 'usr06Txt', type: 'string' },
        { name: 'usr07Txt', type: 'string' },
        { name: 'usr08Date', type: 'string' },
        { name: 'usr09Num', type: 'string' },
        
        //references
        { name: 'timeConfs',        type: 'auto' },
        { name: 'matConfs',         type: 'auto' },
        { name: 'objectStatusList', type: 'auto' }
    ],

    constructor : function(config) {
        this.callParent(arguments);

        try {
            if (config !== null && config !== undefined) {
                if (config.order) {
                    this.set('aufnr', config.order.get('aufnr'));
                    this.set('arbpl', config.order.get('vaplz'));
                    this.set('werks', config.order.get('wawrk'));
		  
                    this.set('mobileKey', config.order.get('mobileKey'));
		        	
                    //check if there is a aufpl to draw from already existing operations
                    var ordersOperations = config.order.get('operations');
		        	
                    if (ordersOperations && ordersOperations.getCount() > 0) {
                        this.set('aufpl', ordersOperations.getAt(0).get('aufpl'));
                    }
                }
            }
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOperation', ex);
        }
    },

    //initializes the operation object with default values e.g. Date and Time fields set to the current date
    initNewOperation: function() {
        try {
            this.set('aufnr', '');
            this.set('vornr', '');
            this.set('aufpl', '');
            this.set('aplzl', '');
            this.set('steus', '');
            this.set('ltxa1', '');	
            this.set('larnt', '');
            this.set('pernr', '');
            this.set('dauno', '0.0');
            this.set('daune', 'H');
            this.set('anzma', '');
            this.set('arbei', '0.0');
            this.set('arbeh', 'H');
            this.set('arbpl', '');
            this.set('werks', '');
            this.set('istru', '');
            this.set('stsma', '');
            this.set('objnr', '');
            this.set('ktsch', '');
            this.set('obknr', '');
            this.set('obzae', '');
            this.set('fsav', new Date());
            this.set('fsed', new Date());
            this.set('ntan', new Date());
            this.set('nten', new Date());
            this.set('isd', new Date());
            this.set('ied', new Date());
            this.set('updFlag', '');
            this.set('mobileKey', '');
            this.set('childKey', '');
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initNewOperation of BaseOperation', ex);
        }
    },
	
    getPlannedDoneString: function() {
        var retval = '00:00 H / 00:00 H (0)';
	
        try {
            var plannedPart = '';
            var donePart = '';
			
            var plannedUnit = '';
		
            //first build the planned part
            {
                //parse the planned value
                var plannedAsFloat = 0.0;
                if (!AssetManagement.customer.utils.StringUtils.isNullOrEmpty(this.get('arbei'))) {
                    plannedAsFloat = AssetManagement.customer.utils.NumberFormatUtils.parseUsingBackendLocale(this.get('arbei'));

                    if (plannedAsFloat === Number.NaN) {
                        plannedAsFloat = 0.0;
                    }
                }

                plannedUnit = this.get('arbeh').toUpperCase();

                if (plannedUnit === 'MIN' || plannedUnit === 'M') {
                    plannedUnit = 'MIN';
                    plannedPart = Math.round(plannedAsFloat) + ' ' + plannedUnit;
                } else {
                    if (AssetManagement.customer.utils.StringUtils.isNullOrEmpty(plannedUnit))
                        plannedUnit = 'H';

                    var hours = Math.floor(plannedAsFloat);
                    var minutesAsFraction = plannedAsFloat - hours;
                    var minutes = Math.round(minutesAsFraction * 60);

                    hours = AssetManagement.customer.utils.StringUtils.padLeft(hours, '0', 2);
                    minutes = AssetManagement.customer.utils.StringUtils.padLeft(minutes, '0', 2);

                    plannedPart = hours + ':' + minutes + ' ' + plannedUnit;
                }
            }
				
            //build the done part
            {
                var timeConfs = this.get('timeConfs');
                var timeConfCount = timeConfs ? timeConfs.getCount() : 0;
                var timeConfSum = this.get('tConfSum');

                //convert to operations planned unit
                if (plannedUnit === 'MIN') {
                    donePart = Math.round(timeConfSum * 60) + ' ' + plannedUnit + ' (' + timeConfCount + ')';
                } else {
                    var hours = Math.floor(timeConfSum);
                    var minutesAsFraction = timeConfSum - hours;
                    var minutes = Math.round(minutesAsFraction * 60);

                    hours = AssetManagement.customer.utils.StringUtils.padLeft(hours, '0', 2);
                    minutes = AssetManagement.customer.utils.StringUtils.padLeft(minutes, '0', 2);

                    donePart = hours + ':' + minutes + ' ' + plannedUnit + ' (' + timeConfCount + ')';
                }
            }
            
            retval = plannedPart + ' / ' + donePart;
        }
        catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPlannedDoneString of BaseOperation', ex);
        }
		
        return retval;
    }
});