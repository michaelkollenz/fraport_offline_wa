Ext.define('AssetManagement.base.model.bo.BaseOxBaseBusinessObject', {
    extend: 'Ext.data.Model',
    
    constructor: function(config) {
        this.callParent(arguments);
    	
        try {
            var myFields = this.getFields();	

            if (myFields) {
                Ext.Array.each(myFields, function(field) {
                    if (field.getType() === 'string') {
                        var initValue = this.get(field.getName());
			
                        //set is not called, to keep it as performant as possible
                        if (initValue === null || initValue === undefined)
                            this.data[field.getName()] = '';
                    }
                }, this);
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOxBaseBusinessObject', ex);
        }
    },

    set: function(fieldName, value) {
        try {
            var field = this.getFieldForFieldName(fieldName);
			
            if (field && field.getType() === 'string') {
                if (value === null || value === undefined)
                    arguments[1] = '';
            }
            
            this.callParent(arguments);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside set of BaseOxBaseBusinessObject', ex);
        }
    },
	
    getFieldForFieldName: function(fieldName) {
        var retval = null;
	
        try {
            if (fieldName) {
                var myFields = this.getFields();
				
                if (myFields) {
                    Ext.Array.each(myFields, function(field) {
                        if (field.getName() === fieldName) {
                            retval = field;
                            return false;
                        }
                    }, this);
                }
            }		
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFieldForFieldName of BaseOxBaseBusinessObject', ex);
        }
		
        return retval;
    },

    getRelevantTimeZone: function(){
        var retval = '';

        try {
            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
            retval = userInfo.get('timeZone');
        } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelevantTimeZone of BaseOxBaseBusinessObject', ex);
        }
            
        return retval;
    }
});