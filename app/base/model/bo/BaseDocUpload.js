Ext.define('AssetManagement.base.model.bo.BaseDocUpload', {
	extend: 'AssetManagement.customer.model.bo.File',
	
	fields: [
	    //used for doc upload
		{ name: 'scenario',     	type:'string'},
	    { name: 'userId',             type: 'string' },
	
	    //key fields
	    { name: 'docId',             type: 'string' },
	    
	    //ordinary fields
		{ name: 'refType',       type: 'string' },
		{ name: 'refObject',     type: 'string' },
        { name: 'docType' ,      type: 'string' },
		{ name: 'docSize' ,      type: 'string' },
		{ name: 'description' ,   type: 'string' },
		{ name: 'ordReport',     type: 'string' },
		{ name: 'spras',          type: 'string' },
		{ name: 'filename' ,      type: 'string' },
		{ name: 'email',          type: 'string' },
		{ name: 'recordname' ,    type: 'string' },
		{ name: 'recordnameBin', type: 'string' },
		{ name: 'orgFilename' ,  type: 'string' },
		
		//date fields
	    { name: 'timestamp' } ,
	    { name: 'syncTimestamp' },
		
        //delta fields
		{ name: 'updFlag',       type: 'string' },
		{ name: 'mobileKey', type: 'string' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});
