Ext.define('AssetManagement.base.model.bo.BaseCustNotifType', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'qmart' },
		
		//ordinary fields
		{ name: 'rbnr' },
		{ name: 'auart' },
		{ name: 'stsma' },
		{ name: 'fekat' },
		{ name: 'urkat' },
		{ name: 'makat' },
		{ name: 'mfkat' },
		{ name: 'otkat' },
		{ name: 'sakat' },
		{ name: 'artpr' },
		{ name: 'createAllowed' },
		{ name: 'qmartx' },
		{ name: 'smstsma' },
		{ name: 'partnerTransfer' },
        { name: 'artpr' },
        { name: 'close_amer' },

		
		//delta fields
		{ name: 'updFlag' }		
	],
    	
    constructor: function(config) {
        this.callParent(arguments);
    }
});