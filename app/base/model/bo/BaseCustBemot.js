Ext.define('AssetManagement.base.model.bo.BaseCustBemot', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
 
	fields: [			
        //key fields
		{ name: 'bemot' },
	
		//ordinary fields
		{ name: 'bemottxt' },
			
		//delta fields
		{ name: 'updFlag' }
	],
	

	constructor: function(config) {
		this.callParent(arguments);
	}
});