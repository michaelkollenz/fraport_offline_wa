Ext.define('AssetManagement.base.model.bo.BaseOrdComponent', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    requires: [
        'AssetManagement.customer.helper.OxLogger'
    ],

    fields: [
        //key fields
        { name: 'rsnum' ,           type:  'string' },
        { name: 'rspos' ,           type:  'string' },
		
        //ordinary fields  
        { name: 'aufnr',            type: 'string' },
        { name: 'xloek',            type: 'string' },
        { name: 'kzear',            type: 'string' },
        { name: 'matnr',            type: 'string' },
        { name: 'werks',            type: 'string' },
        { name: 'lgort',            type: 'string' },
        { name: 'bdmng',            type: 'string' },
        { name: 'meins' ,           type: 'string' },
        { name: 'enmng' ,           type: 'string' },
        { name: 'erfmg',            type: 'string' },
        { name: 'erfme' ,           type: 'string' },
        { name: 'bwart',            type: 'string' },
        { name: 'sgtxt' ,           type: 'string' },
        { name: 'vornr' ,           type: 'string' },
        { name: 'locEnmng',         type: 'string' },
        { name: 'postp' ,           type: 'string' },
        { name: 'vmeng',            type: 'string' },
	    { name: 'ltxsp',            type: 'string' },
        { name: 'idnlf',            type: 'string' },
        { name: 'infnr',            type: 'string' },

        { name: 'matxt',            type: 'string' },
        { name: 'ekorg',            type: 'string' },
        { name: 'ekrgp',            type: 'string' },
        { name: 'afnam',            type: 'string' },
        { name: 'saknr',            type: 'string' },
        { name: 'lifnr',            type: 'string' },
        { name: 'gpreis',           type: 'string' },
        { name: 'waers',            type: 'string' },
        { name: 'wempf',            type: 'string' },
        { name: 'ablad',            type: 'string' },
        { name: 'charg',            type: 'string' },
        
        //delta fields
        { name: 'updFlag',          type:  'string' },
        { name: 'mobileKey',        type:  'string' },
        { name: 'childKey',         type:  'string' },
        
        //references
        { name: 'material',        type: 'auto' }
    ],
		
    constructor : function(config) {
        this.callParent(arguments);

        try {
            if (config && config.order) {
                var order = config.order;
                this.set('aufnr', order.get('aufnr'));
                this.set('rsnum', order.get('rsnum'));
                this.set('werks', order.get('iwerk'));
                this.set('mobileKey', order.get('mobileKey'));
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseOrdComponent', ex);
        }
    }
});