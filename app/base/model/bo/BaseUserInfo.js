Ext.define('AssetManagement.base.model.bo.BaseUserInfo', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	mixins: ['Ext.mixin.Observable'],

	inheritableStatics: {
		_instance: null,
		
		getInstance: function(reset) {
			if(!this._instance || reset)
				this._instance = Ext.create('AssetManagement.customer.model.bo.UserInfo');
				
			return this._instance;
		},

		resetInstance: function() {
			this._instance = null;
		}
	},
	
	constructor: function(config) {
	    try {
			this.callParent(config);
			
		    this.mixins.observable.constructor.call(this, config);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseUserInfo', ex);
		}    
	},
   
	fields: [
		//key fields
		{ name: 'mobileUser', type: 'string' },
		
		//ordinary fields
		{ name: 'mobileScenario', type: 'string' },
		{ name: 'orderScenario', type: 'string' },
		{ name: 'orderWorkcenter', type: 'string' },
		{ name: 'orderPlantWrkc', type: 'string' },
		{ name: 'orderPernr', type: 'string' },
		{ name: 'orderPlangroup', type: 'string' },
		{ name: 'orderPlanPlant', type: 'string' },
		{ name: 'orderParRole', type: 'string' },
		{ name: 'orderPartnerNo', type: 'string' },
		{ name: 'notifScenario', type: 'string' },
		{ name: 'notifWorkcenter', type: 'string' },
		{ name: 'notifPlantWrkc', type: 'string' },
		{ name: 'notifPlanGroup', type: 'string' },
		{ name: 'notifPlanPlant', type: 'string' },
		{ name: 'notifPernr', type: 'string' },
		{ name: 'notifParRole', type: 'string' },
		{ name: 'notifPartnerNo', type: 'string' },
		{ name: 'variTpl', type: 'string' },
		{ name: 'changeTpl', type: 'string' },
		{ name: 'variEqui', type: 'string' },
		{ name: 'changeEqui', type: 'string' },
		{ name: 'variClass', type: 'string' },
		{ name: 'sprache', type: 'string' },
		{ name: 'lartDefault', type: 'string' },
		{ name: 'intern', type: 'string' },
		{ name: 'ekorg', type: 'string' },
		{ name: 'ekgrp', type: 'string' },
		{ name: 'arbplKtext', type: 'string' },
		{ name: 'keyfigureKokrs', type: 'string' },
		{ name: 'keyfigureGroup', type: 'string' },
		{ name: 'vkorg', type: 'string' },
		{ name: 'vtweg', type: 'string' },
		{ name: 'spart', type: 'string' },
		{ name: 'kschl', type: 'string' },
		{ name: 'kunnrConsi', type: 'string' },
		{ name: 'vkgrp', type: 'string' },
		{ name: 'vkbur', type: 'string' },
		{ name: 'kunnrAG', type: 'string' },
		{ name: 'kunnrWE', type: 'string' },
		{ name: 'lifnr', type: 'string' },
		{ name: 'varMati', type: 'string' },
		{ name: 'timeZone', type: 'string' },
		
		//control field
		{ name: 'isInitialized', type: 'auto' }
	],
    
    setInitialized: function(initialized) {
	    try {
    	    this.set('isInitialized', initialized);
    	
    	    this.fireEvent('initialized', initialized);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitialized of BaseUserInfo', ex);
        }
    }
});