Ext.define('AssetManagement.base.model.bo.BaseDoch', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [	   
	    //key fields
	    { name: 'id',          type: 'string' },
	   
        //ordinary fields
        { name: 'ref_object' ,        type: 'string' },
        { name: 'doc_type' ,          type: 'string' },
		{ name: 'ref_type',           type: 'string' },
		{ name: 'description',        type: 'string' },
		{ name: 'spras',              type: 'string' },
		{ name: 'filename' ,          type: 'string' },
		{ name: 'email',              type: 'string' },
        { name: 'orln',               type: 'string' },
        { name: 'dms_filename',       type: 'string' },
        { name: 'dms_description',    type: 'string' },   
        { name: 'cryp_salt',    type: 'string' },
			
        //delta fields
		{ name: 'updFlag',     type: 'string' },
        { name: 'mobileKey',   type: 'string' },

        //references
         { name: 'docds', type: 'auto' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});