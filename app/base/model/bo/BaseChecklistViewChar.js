Ext.define('AssetManagement.base.model.bo.BaseChecklistViewChar', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
	    //key fields
	    { name: 'prueflos', type: 'string' },
	    { name: 'vorglfnr', type: 'string' },
		{ name: 'merknr', type: 'string' },
        { name: 'zaehl', type: 'string' },

	    //ordinary fields
	    { name: 'kzeinstell', type: 'string' },
		{ name: 'steuerkz', type: 'string' },
		{ name: 'kurztext', type: 'string' },
		{ name: 'katab1', type: 'string' },
		{ name: 'katalgart1', type: 'string' },
		{ name: 'auswmenge1', type: 'string' },
		{ name: 'auswmgwrk1', type: 'string' },
		{ name: 'katab2', type: 'string' },
		{ name: 'katalgart2', type: 'string' },
		{ name: 'auswmenge2', type: 'string' },
		{ name: 'auswmgwrk2', type: 'string' },
		{ name: 'sollwert', type: 'string' },
		{ name: 'toleranzob', type: 'string' },
		{ name: 'toleranzun', type: 'string' },
		{ name: 'masseinhsw', type: 'string' },
        { name: 'char_type', type: 'string' },
		{ name: 'stellen', type: 'string' },
		{ name: 'vorglfnr', type: 'string' },
		{ name: 'closed', type: 'string' },
		{ name: 'category', type: 'auto' },

		{ name: 'checkBoxOK', type: 'auto' },
		{ name: 'checkBoxNOK', type: 'auto' },
		{ name: 'checkBoxNREL', type: 'auto' },

        
	    //delta fields
		{ name: 'updFlag', type: 'string' },

        //reference
        { name: 'meanValue', type: 'string' }
    ],

	constructor : function(config) {
		this.callParent(arguments);
	}
});
