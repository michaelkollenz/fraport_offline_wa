Ext.define('AssetManagement.base.model.bo.BaseEquiTyp', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [	    
	    //key fields
	    { name: 'SCENARIO',  type: 'string' },
	    { name: 'EQTYP' ,    type: 'string' },
       
	   	//ordinary fields
       	{ name: 'TXTBZ',      type:  'string' },
		
        //delta fields
		{ name: 'updFlag',   type:'string' }		
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});