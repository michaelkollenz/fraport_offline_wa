Ext.define('AssetManagement.base.model.bo.BaseClass', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'objnr',      type: 'string' },
	    { name: 'mafid',      type: 'string' },
	    { name: 'klart',      type: 'string' },
	    { name: 'adzhl' ,     type: 'string' },
        { name: 'clint' ,     type: 'string' },
      
        //ordinary fields
		{ name: 'classno' ,   type: 'string' },
		{ name: 'txtbz',      type: 'string' },
		{ name: 'mandt',      type: 'string' },
		{ name: 'userid',     type: 'string' },
		{ name: 'werk',       type: 'string' },
		{ name: 'zaehl',      type: 'string' },
			
        //delta fields
		{ name: 'updFlag',    type: 'string' },
        { name: 'mobileKey',  type: 'string' }
    ],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});