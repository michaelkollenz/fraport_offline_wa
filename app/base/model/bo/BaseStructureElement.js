 Ext.define('AssetManagement.base.model.bo.BaseStructureElement', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
	    'Ext.data.Store',
        'AssetManagement.customer.helper.OxLogger'
    ],

	fields: [
		//ordinary fields
		{ name: 'equipment', type: 'auto' },
		{ name: 'floc', type: 'auto' },
		{ name: 'material', type: 'auto' },
		{ name: 'childElements', type: 'auto' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}, 
	
	addChild: function(child){
		try {
			this.set('childElements', child); 
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside addChild of BaseStructureElement', ex);
		}
	}, 
	
	getElement: function(){
		try {
		  if (this.get('equipment') != null)
              return this.get('equipment');
          if (this.get('floc') != null)
              return this.get('floc') ;
          if (this.get('material') != null)
              return this.get('material');
              
          return null;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getElement of BaseStructureElement', ex);
		}
	}, 
	
	hasChild: function(){
		try {
			var hasChild = false; 
			
			if(this.get('childElements'))
				hasChild = true; 
		} catch(ex) {
			hasChild = false; 
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside hasChild of BaseStructureElement', ex);
		}
		
		return hasChild;
	}
 });