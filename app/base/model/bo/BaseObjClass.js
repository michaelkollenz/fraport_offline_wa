Ext.define('AssetManagement.base.model.bo.BaseObjClass', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields	 
	    { name: 'objnr', type: 'string' },
	    { name: 'mafid' ,type: 'string' },
		{ name: 'klart', type: 'string' },
		{ name: 'clint' ,type: 'string' },
		{ name: 'adzhl', type: 'string' },

		
        //delta fields
	    { name: 'updFlag',      type:  'string' },
        { name: 'mobileKey',    type:  'string' },

        //references
        { name: 'characts',     type:  'auto' }
    ],
		
    constructor: function(config) {
        this.callParent(arguments);
	}
});