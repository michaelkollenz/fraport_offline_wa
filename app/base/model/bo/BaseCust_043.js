Ext.define('AssetManagement.base.model.bo.BaseCust_043', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'scenario',         type:'string' },
	    { name: 'acgroup',          type: 'string' },
	    { name: 'actype',           type:'string' },
		
        //delta fields
		{ name: 'updFlag',          type:'string' }      
	],
	

	constructor : function(config) {
		this.callParent(arguments);
	}
});