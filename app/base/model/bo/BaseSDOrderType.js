Ext.define('AssetManagement.base.model.bo.BaseSDOrderType', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'scenario', type: 'string' },
		{ name: 'auart', type: 'string' },
		
		//ordinary fields
		{ name: 'augru', type: 'string' },
	
		//delta fields
		{ name: 'updFlag',   type: 'string' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});