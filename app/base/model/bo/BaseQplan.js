Ext.define('AssetManagement.base.model.bo.BaseQPlan', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
	    //key fields
	    { name: 'plnty', type: 'string' },
	    { name: 'plnnr', type: 'string' },
		{ name: 'plnal', type: 'string' },
		{ name: 'zaehl', type: 'string' },
		{ name: 'matnr', type: 'string' },
        { name: 'zkriz', type: 'string' },

        //ordinary fields
        { name: 'ktext', type: 'string' },
        { name: 'delkz', type: 'string' }
    ],

	constructor: function(config) {
		this.callParent(arguments);
	}
});