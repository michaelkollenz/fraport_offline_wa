﻿Ext.define('AssetManagement.base.model.bo.BaseCustAuswm', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
	    //key fields
	    { name: 'werks', type: 'string' },
	    { name: 'katalogart', type: 'string' },
		{ name: 'auswahlmge', type: 'string' },
		{ name: 'codegruppe', type: 'string' },
		{ name: 'code', type: 'string' },
		{ name: 'versionam', type: 'string' },

	    //ordinary fields
	    { name: 'bewertung', type: 'string' },


	    //delta fields
		{ name: 'updFlag', type: 'string' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});