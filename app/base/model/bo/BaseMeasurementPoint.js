Ext.define('AssetManagement.base.model.bo.BaseMeasurementPoint', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'point',      type: 'string' },
		
		//ordinary fields
		{ name: 'mpobj',      type:  'string' },
		{ name: 'psort',      type:  'string' },
		{ name: 'pttxt',      type:  'string' },
		{ name: 'locas',      type:  'string' },
		{ name: 'atinn',      type:  'string' },
		{ name: 'mrmin',      type:  'string' },
		{ name: 'mrmax',      type:  'string' },
		{ name: 'mrngu',      type:  'string' },
		{ name: 'desir',      type:  'string' },
		{ name: 'indct',      type:  'string' },
		{ name: 'indrv',      type:  'string' },
		{ name: 'lastRec',    type:  'string' },
		{ name: 'lastUnit',   type:  'string' },
		{ name: 'mptyp',      type:  'string' },
		{ name: 'codct',      type:  'string' },
		{ name: 'codgr',      type:  'string' },
            	{ name: 'mseht',      type:  'string' },
            	{ name: 'mseh6',      type:  'string' },
		
		//delta fields
		{ name: 'updFlag', type: 'string' },

	    { name: 'equipment',    type:  'auto' },
	    { name: 'funcLoc', type: 'auto' },
        { name: 'measDocs', type: 'auto' }
	],
		
	constructor: function(config) {
		this.callParent(arguments);
	}
});