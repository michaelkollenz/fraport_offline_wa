Ext.define('AssetManagement.base.model.bo.BaseBanfHeader', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	   	//key fields
	    { name: 'banfn',     		 	type:'string'  },
	
	    //ordinary fields
	    { name: 'bsart',       			type: 'string' },
		{ name: 'lifnr' ,      			type: 'string' },
		{ name: 'aufnr',      			type: 'string' },
		{ name: 'mobileKey_ref' ,      	type: 'string' },
		{ name: 'mobileKey',       		type: 'string' },
		
        //delta fields
		{ name: 'updFlag',     			type:'string'  }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});
		
		
		
		
		
		
		
		


















