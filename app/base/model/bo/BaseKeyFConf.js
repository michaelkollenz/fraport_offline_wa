Ext.define('AssetManagement.base.model.bo.BaseKeyFConf', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields 
	    { name: 'aufnr',       type:  'string' },
	    { name: 'vornr',       type:  'string' },
	    { name: 'split',       type:  'string' },
	    { name: 'rmzhl',       type:  'string' },

        //ordinary fields
        { name: 'arbpl',       type: 'string' },
		{ name: 'aueru',       type: 'string' },
		{ name: 'ausor',       type: 'string' },
		{ name: 'bemot',       type: 'string' },
		{ name: 'ernam',       type: 'string' },
		{ name: 'idaue',       type: 'string' },
		{ name: 'idaur',       type: 'string' },
		{ name: 'ismne',       type:  'string' },
		{ name: 'ismnw',       type: 'string' },
		{ name: 'learr',       type: 'string' },
		{ name: 'ltxa1',       type: 'string' },
		{ name: 'ofmne',       type: 'string' },
		{ name: 'ofmnw',       type: 'string' },
	    { name: 'pernr',       type: 'string' },
		{ name: 'txtsp',       type: 'string' },
		{ name: 'werks',       type:  'string' },   
		{ name: 'stagr',       type: 'string' },
	    { name: 'grund',       type: 'string' },
	    { name: 'bezei',       type: 'string' },
       
	    //date fields
        { name: 'ersda' },
        { name: 'ied' },
	    { name: 'isd' },
	    { name: 'budat' } ,


        { name: 'actype', type: 'string' },
        { name: 'usr1', type: 'string' },
        { name: 'usr2', type: 'string' },
        { name: 'pausesz', type: 'string' },
        { name: 'pauseez', type: 'string' },

        { name: 'kstar', type: 'string' },
        { name: 'betrag', type: 'string' },
        { name: 'tcurr', type: 'string' },
        { name: 'lgart', type: 'string' },

        //delta fields
		{ name: 'updFlag',       type:'string' },
        { name: 'mobilekey',     type: 'string' },
		{ name: 'childkey',      type:  'string' },
		{ name: 'childkey2',     type:  'string' }
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});