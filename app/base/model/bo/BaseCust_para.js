Ext.define('AssetManagement.base.model.bo.BaseCust_para', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'scenario',      type: 'string' },
	    { name: 'paragroup',     type: 'string' },
	    { name: 'fieldname',     type: 'string' },
	  
        //ordinary fields
        { name: 'fieldval1' ,    type: 'string' },
		{ name: 'fieldval2',     type: 'string' },
	
        //delta fields
		{ name: 'updFlag',       type: 'string' }       
	],

	constructor: function(config) {
		this.callParent(arguments);
	}
});