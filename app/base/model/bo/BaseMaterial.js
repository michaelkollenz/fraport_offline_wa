Ext.define('AssetManagement.base.model.bo.BaseMaterial', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'matnr' , type:  'string' },
		   
	    //ordinary fields
		    
		{ name: 'meins',  type:   'string' },
		{ name: 'maktx' , type:   'string' },
		{ name: 'labst' , type:   'string' },
		{ name: 'xchpf',  type:   'string' },
		{ name: 'brprs',  type:   'string' },
		{ name: 'ean11',  type:   'string' },
		{ name: 'bismt',  type:   'string' },
            	{ name: 'mtpos_mara',   type:   'string' },
            	{ name: 'sernp',        type:   'string' },
            	{ name: 'prdha',        type:   'string' },
            	{ name: 'mtart',        type:   'string' },
			
	    //delta fields
		{ name: 'updFlag', type: 'string' }	      
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});