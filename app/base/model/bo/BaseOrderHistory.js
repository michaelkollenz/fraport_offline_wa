Ext.define('AssetManagement.base.model.bo.BaseOrderHistory', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'aufnr',        type: 'string' },
			
		//ordinary fields
		{ name: 'auart',        type: 'string' },
		{ name: 'auartTxt',     type: 'string' },
		{ name: 'tplnr',        type: 'string' },
		{ name: 'equnr',        type: 'string' },
		{ name: 'bautl',        type:  'string' },
		{ name: 'qmnum',        type:  'string' },
		{ name: 'addat',        type:  'string' },
		{ name: 'priok',        type:  'string' },
		{ name: 'iwerk',        type: 'string' },
		{ name: 'ingpr',        type: 'string' },
		{ name: 'kunum',        type: 'string' },
		{ name: 'ktext',        type: 'string' },
		{ name: 'vaplz',        type:  'string' },
		{ name: 'wawrk',        type:  'string' },
		{ name: 'objnr',        type: 'string' },
		{ name: 'system_status', type:'string' },
		{ name: 'user_status',   type: 'string' },
		{ name: 'ilart',        type:  'string' },
		{ name: 'ilartTxt',     type:  'string' },
		{ name: 'city1',        type: 'string' },
		{ name: 'postalCode',   type:'string' },
			
		//date fields
		{ name: 'gltrp' },
		{ name: 'gstrp' },
			
	    //delta fields
		{ name: 'updFlag', type: 'string' }
	],
	
    constructor: function(config) {
        this.callParent(arguments);
    }
});