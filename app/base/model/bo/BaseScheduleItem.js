Ext.define('AssetManagement.base.model.bo.BaseScheduleItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
		//ordinary fields
		{ name: 'selected',  type: 'boolean' },
		
		//references
		{ name: 'operation' , 	type: 'auto'},
		{ name: 'order' , 		type: 'auto'},
		{ name: 'address' , 	type: 'auto'},
		{ name: 'equipment' , 	type: 'auto'},
		{ name: 'partnerAP' , 	type: 'auto'},
		{ name: 'notifTasks' , 	type: 'auto'},
		
		//mapping fields
		{ 
			name: 'postalCode',
			convert: function (notUsed, data) {
		    	return data.getPostalCode.call(data);
		    },
		    depends: ['address']
		},
		{ 
			name: 'startDateTime',
			convert: function (notUsed, data) {
		    	return data.getStartDateTime.call(data);
		    },
		    depends: ['operation']
		},
		{ 
			name: 'aufnrVornr',
			convert: function (notUsed, data) {
		    	return data.getAufnrVornr.call(data);
		    },
		    depends: ['operation']
		}
	],

	constructor : function(config) {
		this.callParent(arguments);
	},
	
	getPostalCode: function() {
		var retval = '';
	
		try {
			var address = this.get('address');
			
			if(address) {
				retval = address.get('postCode1');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseScheduleItem', ex);
		}
	
		return retval;
	},
	
	getStartDateTime: function() {
		var retval = '';
	
		try {
			var operation = this.get('operation');
			
			if(operation) {
				retval = operation.get('fsav');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStartDateTime of BaseScheduleItem', ex);
		}
	
		return retval;
	},
	
	getAufnrVornr: function() {
		var retval = '';
	
		try {
			var operation = this.get('operation');
			
			if(operation) {
				retval = operation.get('aufnr') + operation.get('vornr');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAufnrVornr of BaseScheduleItem', ex);
		}
	
		return retval;
	}
});