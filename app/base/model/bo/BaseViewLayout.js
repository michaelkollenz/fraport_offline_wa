﻿Ext.define('AssetManagement.base.model.bo.BaseViewLayout', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    mixins: ['Ext.mixin.Observable'],

    inheritableStatics: {
        _instance: null,
        _resetTriggered: false,

        getInstance: function (reset) {
            if (this._instance === null || this._instance === undefined || reset)
                this._instance = Ext.create('AssetManagement.customer.model.bo.ViewLayout');
            this.setResetTriggered(reset);

            return this._instance;
        },

        resetInstance: function () {
            this._instance = null;
        },

        getResetTriggered: function() {
            return this._resetTriggered;
        },

        setResetTriggered: function(reset) {
            this._resetTriggered = reset;
        }

    },

    constructor: function (config) {
        try {
            this.callParent(arguments);

            this.mixins.observable.constructor.call(this, config);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseViewLayout', ex);
        }
    },

    fields: [
	    { name: 'layoutsMap', type: 'auto' },

		//control field
		{ name: 'isInitialized', type: 'boolean', defaultValue: false }
    ],

    setInitialized: function (initialized) {
        try {
            this.set('isInitialized', initialized);

            this.fireEvent('initialized', initialized);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitialized of BaseViewLayout', ex);
        }
    },

    getViewFromHash: function (viewName) {
        var retval = null;
        try {
            var hashMap = this.get('hashMap');
            if (hashMap) {
                return retval = hashMap.get(viewName);
            }
            return false;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getViewNameFromHash of BaseViewLayout', ex);
        }
    }
});