Ext.define('AssetManagement.base.model.bo.BaseInventoryItem', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    requires: [
        'Ext.data.Store',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.CompletionManager'
    ],

    fields: [
	    //key fields
	    { name: 'fiscalyear', type: 'string' },
	    { name: 'physinventory', type: 'string' },
	    { name: 'item', type: 'string' },

        //ordinary fields
		{ name: 'material', type: 'string' },
        { name: 'batch', type: 'string' },
		{ name: 'zerocount', type: 'boolean' },
        { name: 'entryQnt', type: 'string' },
        { name: 'entryuom', type: 'string' },
        { name: 'entryuomiso', type: 'string' },
        { name: 'materialExternal', type: 'string' },
        { name: 'materialGuid', type: 'string' },
        { name: 'materialVersion', type: 'string' },

	    //delta fields
		{ name: 'updFlag', type: 'string' },
        { name: 'mobileKey', type: 'string' },

        //ordinary fields
        { name: 'selected', type: 'boolean' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});