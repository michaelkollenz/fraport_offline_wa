Ext.define('AssetManagement.base.model.bo.BaseCustReason', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
    
	fields: [
		//key fields
		{ name: 'grund' },
		{ name: 'werk' },
		
		//ordinary fields
		{ name: 'grdtx' },
		
		//delta fields
		{ name: 'updFlag' }		
	],
    	
    constructor: function(config) {
        this.callParent(arguments);
    }
});