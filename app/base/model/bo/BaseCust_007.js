Ext.define('AssetManagement.base.model.bo.BaseCust_007', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    // { name: 'mobileuser', type: 'string' },  
	    { name: 'qmart',        type: 'string' },
	    
	    //ordinary fields
        { name: 'qherk' ,       type: 'string' },
		{ name: 'qpart',        type: 'string' },
		{ name: 'insplotauto' , type: 'string' },
		{ name: 'refobject' ,   type: 'string' },
		{ name: 'detplant',     type: 'string' },
		
        //delta fields
		{ name: 'updFlag',      type: 'string' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});