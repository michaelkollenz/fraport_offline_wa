Ext.define('AssetManagement.base.model.bo.BaseNotifActivity', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [		
		//key fields		
		{ name: 'qmnum',  type: 'string' },
		{ name: 'fenum',  type: 'string', defaultValue: '0000' },
		{ name: 'manum',  type: 'string' },
		
		//ordinary fields
		{ name: 'bautl',  type: 'string' },
		{ name: 'matxt',  type: 'string' },
		{ name: 'mncod',  type: 'string' },
		{ name: 'mngrp',  type: 'string' },
		{ name: 'mnkat',  type: 'string' },
		{ name: 'mnver',  type: 'string', defaultValue: '000001' },
		{ name: 'parnr',  type: 'string' },
		{ name: 'parvw',  type: 'string' },
		{ name: 'urnum',  type: 'string' },
		{ name: 'erlnam', type: 'string' },
		{ name: 'qsmnum',  type: 'string' },
		{ name: 'stsma',  type: 'string' },
		{ name: 'kzmla',  type: 'string' },
		//date fields
		// erldt = ERLDAT + ERLZEIT
		// petdt = PETER + PETUR
		// pstdt = PSTER + PSTUR
		{ name: 'erldt' },
		{ name: 'petdt' },
		{ name: 'pstdt' },
		{ name: 'erzeit' },
		
		//delta fields
		{ name: 'updFlag',     type: 'string' },
		{ name: 'mobileKey',   type: 'string' },
		{ name: 'childKey',    type: 'string' },
		{ name: 'childKey2',   type: 'string' },

		//references
		{ name: 'notif',            type: 'auto' },
		{ name: 'notifItem',        type: 'auto' },
		{ name: 'objectStatus',     type: 'auto' },
		{ name: 'localLongtext',    type: 'auto' },
		{ name: 'backendLongtext',  type: 'auto' },
		{ name: 'activityCustCode', type: 'auto' }		
	],
    
	
    constructor: function(config) {
        this.callParent(arguments);
        
        try {
            var newDate = new Date();
            this.set('erldt', config.erldt ? config.erldt : newDate);
            this.set('petdt', config.petdt ? config.petdt : newDate);
            this.set('pstdt', config.pstdt ? config.pstdt : newDate);

            if(config.notif) {
            	this.set('qmnum', config.notif.get('qmnum'));
            	this.set('mobileKey', config.notif.get('mobileKey'));
            }
            
            if(config.notifItem) {
            	this.set('fenum', config.notifItem.get('fenum'));
            	this.set('childKey', config.notifItem.get('childKey'));
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNotifActivity', ex);
        }     
    }
});