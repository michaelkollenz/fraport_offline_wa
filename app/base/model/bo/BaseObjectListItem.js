Ext.define('AssetManagement.base.model.bo.BaseObjectListItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'aufnr',     type:'string' },
		{ name: 'obknr',     type: 'string' },
		{ name: 'obzae',     type: 'string' },
		
        //ordinary fields
		{ name: 'equnr',     type:  'string' },
		{ name: 'eqtxt',     type:  'string' },
		{ name: 'ihnum',     type: 'string' },
		{ name: 'qmtxt',     type: 'string' },
		{ name: 'tplnr',     type:  'string' },
		{ name: 'pltxt',     type: 'string' },
		{ name: 'matnr',     type:  'string' },
		{ name: 'maktx',     type: 'string' },
		{ name: 'bautl',     type:  'string' },
		{ name: 'bautx',     type: 'string' },
		{ name: 'sortf',     type: 'string' },
		{ name: 'bearb',     type: 'string' },
		{ name: 'sernr',     type:  'string' },
        { name: 'mobilekey', type:  'string' },
        { name: 'mobilekey_notif', type:  'string' },
		
        //delta fields
		{ name: 'updFlag',   type:  'string' },
		
		//references
        { name: 'qplos', type: 'auto' },
        { name: 'equipment', type: 'auto' },
        { name: 'funcLoc', type: 'auto' }
	],
    
    constructor: function(config) {
        this.callParent(arguments);
    }
});