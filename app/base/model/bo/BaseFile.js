//base class for any type of file
Ext.define('AssetManagement.base.model.bo.BaseFile', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'fileId',             type: 'string' },
	    
	    //ordinary fields
        { name: 'fileName' ,      type: 'string' },
		{ name: 'fileSize',     type: 'float' },
		{ name: 'fileType' ,      type: 'string' },
		{ name: 'fileOrigin' ,      type: 'string' },
		{ name: 'fileDescription' ,      type: 'string' },
		
		//date fields
	    { name: 'createdAt' },
	    { name: 'lastModified' },
	    
	    // references
        { name: 'contentAsArrayBuffer', type: 'auto' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});
