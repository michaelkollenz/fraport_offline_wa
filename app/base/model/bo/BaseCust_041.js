Ext.define('AssetManagement.base.model.bo.BaseCust_041', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'scenario',         type:'string' },
	    { name: 'actype',           type:'string' },
	    { name: 'target',           type:'string' },
	    
        //ordinary fields
	    { name: 'lstar',            type: 'string' },
        { name: 'kstar',            type: 'string' },
		{ name: 'stagr',            type: 'string' },
		{ name: 'regtyp',           type: 'string' },
		{ name: 'durationcalc',     type: 'string' },
        { name: 'timeinc',          type: 'string' },
        { name: 'leinh',            type: 'string' },

        //from cust040
		{ name: 'description',     type: 'string' },
		
        //delta fields
		{ name: 'updFlag',          type:'string' }      
	],
	

	constructor : function(config) {
		this.callParent(arguments);
	}
});