Ext.define('AssetManagement.base.model.bo.BaseCustCode', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'katalogart', type: 'string'},
		{ name: 'codegruppe', type: 'string' },
		{ name: 'code', type: 'string' },
		
		//ordinary fields
		{ name: 'codegrkurztext', type: 'string' },
		{ name: 'kurztext', type: 'string' },
		
		//delta fields
		{ name: 'updFlag', type: 'string' }
	],

    constructor: function(config) {
        this.callParent(arguments);
    }
});