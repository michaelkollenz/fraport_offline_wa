Ext.define('AssetManagement.base.model.bo.BaseOrder', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    requires: [
        'Ext.data.Store',
        'AssetManagement.customer.model.bo.SDOrderItem',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.CompletionManager'
    ],

    inheritableStatics: {
        SAP_OBJECT_TYPE: 'BUS2007'
    },

    fields: [
        //key fields
        { name: 'aufnr', type: 'string' },
		
        //ordinary fields
        { name: 'priok', type: 'string' },
        { name: 'equnr', type: 'string' },
        { name: 'bautl', type: 'string' },
        { name: 'iwerk', type: 'string' },
        { name: 'ingrp', type: 'string' },
        { name: 'pmobjty',  type: 'string' },
        { name: 'gewrk',    type: 'string' },
        { name: 'kunum', type: 'string' },
        { name: 'qmnum', type: 'string' },
        { name: 'serialnr', type: 'string' },
        { name: 'sermat', type: 'string' },
        { name: 'auart', type: 'string' },
        { name: 'ktext', type: 'string' },
        { name: 'vaplz', type: 'string' },
        { name: 'wawrk', type: 'string' },
        { name: 'tplnr', type: 'string' },
        { name: 'objnr', type: 'string' },
        { name: 'ilart', type: 'string' },
        { name: 'rsnum', type: 'string' },
        { name: 'fmat',  type: 'string' },
        { name: 'bemot', type: 'string' },
        { name: 'bemottxt', type: 'string' },
        { name: 'adrnra', type: 'string' },
        { name: 'stsma', type: 'string' },
        { name: 'gsber', type: 'string' },
        { name: 'prctr', type: 'string' },
        { name: 'vkorg', type: 'string' },
        { name: 'vtweg', type: 'string' },
        { name: 'spart', type: 'string' },
        { name: 'vkgrp', type: 'string' },
        { name: 'vkbur', type: 'string' },
        { name: 'bstkd', type: 'string' },
        { name: 'bstdk', type: 'string' },
        { name: 'matnr', type: 'string' },
        { name: 'menge', type: 'string' },
        { name: 'meins', type: 'string' },
        { name: 'faktf', type: 'string' },
        { name: 'artpr', type: 'string' },
        { name: 'konty', type: 'string' },
        { name: 'empge', type: 'string' },
        { name: 'anlzu', type: 'string' },
		{ name: 'ltext', type: 'string' },

        { name: 'tidnr', type: 'string' },
        { name: 'strno', type: 'string' },
		
        //date fields
        { name: 'gltrp' },
        { name: 'gstrp' },
        { name: 'fsav' },
        { name: 'fsed' },


        { name: 'usr01Flag', type: 'string' },
        { name: 'usr02Flag', type: 'string' },
        { name: 'usr03Code', type: 'string' },
        { name: 'usr04Code', type: 'string' },
        { name: 'usr05Txt', type: 'string' },

        { name: 'usr06Txt', type: 'string' },
        { name: 'usr07Txt', type: 'string' },
        { name: 'usr08Date', type: 'string' },
        { name: 'usr09Num', type: 'string' },

        { name: 'gstrs' },
        { name: 'gltrs' },
        { name: 'gluzp' },
        { name: 'gsuzp' },
		
        //delta fields
        { name: 'updFlag',   		type: 'string' },
        { name: 'mobileKey', 		type: 'string' },
		{ name: 'mobileKeyNotif', 	type: 'string' },

        //references
        { name: 'orderType',        type: 'auto' },
        { name: 'orderIlart',       type: 'auto' },
        { name: 'orderBemot',       type: 'auto' },
        { name: 'operations',       type: 'auto' },
        { name: 'objectStatusList', type: 'auto' },
        { name: 'localLongtext',    type: 'auto' },
        { name: 'backendLongtext',  type: 'auto' },
        { name: 'funcLoc',          type: 'auto' },
        { name: 'equipment',        type: 'auto' },
        { name: 'notif',            type: 'auto' },
        { name: 'address',          type: 'auto' },
        { name: 'objectList',       type: 'auto' },
        { name: 'partners',         type: 'auto' },
        { name: 'components',       type: 'auto' },
        { name: 'lumpSums',         type: 'auto' },
        { name: 'documents',        type: 'auto' },
        { name: 'qplos',            type: 'auto' },
        { name: 'sdOrders',  		type: 'auto' },
        { name: 'localStatus',      type: 'auto' },
        { name: 'prueflose',        type: 'auto' },
 	
        //mapping fields
        {
            name: 'postalCode',
            convert: function (notUsed, data) {
                return data.getPostalCode.call(data);
            },
            depends: ['address']
        },
        {
            name: 'ntan',
            type: 'auto',
            convert: function (notUsed, data) {
                return data.getNtan.call(data);
            },
            depends: ['operations']
        },
        {
            name: 'fsav',
            type: 'auto',
            convert: function (notUsed, data) {
                return data.getFsav.call(data);
            },
            depends: ['operations']
        },

        { name: 'nten' }
    ],

    constructor : function(config) {
        this.callParent(arguments);
    },
	
    getPostalCode: function() {
        var retval = '';
		
        try {
            var address = this.get('address');
			
            if (address) {
                retval = address.get('postCode1');
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseOrder', ex);
        }
		
        return retval;
    },

    getNtan: function() {
        var retval = '';

        try {
            var operation = AssetManagement.customer.manager.OperationManager.getEarliestOperation(this);

            if (operation) {

                var operationTime = operation.get('ntan');
                if(operationTime) {
                    retval = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(operationTime);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getNtan of BaseOrder', ex);
        }

        return retval;
    },

    getFsav: function() {
        var retval = '';

        try {
            var operation = AssetManagement.customer.manager.OperationManager.getEarliestOperation(this);

            if (operation) {

                var operationTime = operation.get('fsav');
                if(operationTime) {
                    retval = AssetManagement.customer.utils.DateTimeUtils.getDateStringForDisplay(operationTime);
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseOrder', ex);
        }

        return retval;
    },
	
    getAllSDItems: function() {
        var retval = null;
		
        try {
            var sdOrders = this.get('sdOrders');
		
            if (sdOrders && sdOrders.getCount() > 0) {
                var mergedStore = Ext.create('Ext.data.Store', {
                        model: 'AssetManagement.customer.model.bo.SDOrderItem',
                        autoLoad: false
                    });
				
                sdOrders.each(function(sdOrder) {
                    var orderBstnk = sdOrder.get('bstnk');
                    var ordersSDItems = sdOrder.get('items');
                    if (ordersSDItems && ordersSDItems.getCount() > 0) {
                        ordersSDItems.each(function(sdOrderItem) {
                            sdOrderItem.set('bstnk', orderBstnk);
                            mergedStore.add(sdOrderItem);
                        }, this);
                    }
                }, this);
            }
			
            retval = mergedStore;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllSDItems of BaseOrder', ex);
        }
		
        return retval;
    },
		
    //initializes the order object with default values e.g. Date and Time fields set to the current date
    initNewOrder: function() {
        try {
            this.set('aufnr', '');
            this.set('priok', '');
            this.set('equnr', '');
            this.set('bautl', '');
            this.set('iwerk', '');
            this.set('ingrp', '');
            this.set('pmobjty', '');
            this.set('gewrk', '');
            this.set('kunum', '');
            this.set('qmnum', '');
            this.set('serialnr', '');
            this.set('sermat', '');
            this.set('auart', '');
            this.set('ktext', '');
            this.set('vaplz', '');
            this.set('wawrk', '');
            this.set('tplnr', '');
            this.set('objnr', '');
            this.set('ilart', '');
            this.set('rsnum', '');
            this.set('fmat', '');
            this.set('bemot', '');
            this.set('bemottxt', '');
            this.set('adrnra', '');
            this.set('stsma', '');
            this.set('gsber', '');
            this.set('prctr', '');
            this.set('vkorg', '');
            this.set('vtweg', '');
            this.set('spart', '');
            this.set('vkgrp', '');
            this.set('vkbur', '');
            this.set('bstkd', '');
            this.set('bstdk', '');
            this.set('matnr', '');
            this.set('menge', '');
            this.set('meins', '');
            this.set('faktf', '');
            this.set('artpr', '');
            this.set('konty', '');
            this.set('empge', '');
            this.set('anlzu', '');
			
            this.set('tidnr', '');
            this.set('strno', '');
			
            this.set('fsav', new Date());
            this.set('fsed', new Date());
            this.set('ntan', new Date());
            this.set('nten', new Date());
            this.set('gltrp', new Date());
            this.set('gstrp', new Date());
            this.set('updFlag', '');
            this.set('mobileKey', '');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initNewOrder of BaseOrder', ex);
        }
    },

    getObjectAddress: function () {
        var retval = null;

        try {
            var orderAdress = this.get('address')

            if (orderAdress) {
                retval = orderAdress;
            } else {
                var myFuncLoc = this.get('funcLoc');

                if (myFuncLoc) {
                    retval = myFuncLoc.get('address');
                }

                if (!retval) {
                    var myEqui = this.get('equipment');

                    if (myEqui)
                        retval = myEqui.get('address');
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectAddress of BaseOrder', ex);
        }

        return retval;
    }
});
