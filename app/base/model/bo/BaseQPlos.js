﻿Ext.define('AssetManagement.base.model.bo.BaseQPlos', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    inheritableStatics: {
        SAP_OBJECT_TYPE: 'BUS2045'
    },

    fields: [
	    //key fields
	    { name: 'prueflos',     type: 'string' },
	    { name: 'qmnum',        type: 'string' },
		{ name: 'plnty',        type: 'string' },
        { name: 'plnnr',        type: 'string' },
        { name: 'plnal',        type: 'string' },

        //ordinary fields
		{ name: 'ktextlos',     type: 'string' },
		{ name: 'aufnr',        type: 'string' },
        { name: 'aufpl',        type: 'string' },
		{ name: 'werk',         type: 'string' },
        { name: 'stat35',       type: 'string' },
		{ name: 'qpart',        type: 'string' },
        { name: 'matnr',        type: 'string' },
        { name: 'werks',        type: 'string' },
        { name: 'completed',    type: 'string' },
        { name: 'zaehl',        type: 'string' },
        { name: 'equnr',         tyoe: 'string' },
		
		//delta fields
		{ name: 'updFlag',   type: 'string' },
		{ name: 'mobileKey', type: 'string' }, 
        { name: 'mobileKey_ref', type: 'string' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    },

    //public
    isCompleted: function () {
        var retval = false;

        try {
            var updateFlag = '';
            var charFound = false;
            var qplosOperStore = this.get('qplosOpers');

            if (qplosOperStore && qplosOperStore.getCount() > 0) {
                qplosOperStore.each(function (qplosOper) {
                    var qplosCharStore = qplosOper.get('qplosChars');

                    if (qplosCharStore && qplosCharStore.getCount() > 0) {
                        qplosCharStore.each(function (qplosChar) {
                            var charFound = true;
                            var qpresult = qplosChar.get('qpResult');
                            updateFlag = qpresult ? qpresult.get('updFlag') : '';
                            return false;
                        });
                    }

                    if (charFound)
                        return false;
                });
            }

            retval = updateFlag === 'I' || updateFlag === 'X';
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside isCompleted of BaseQPlos', ex);
        }

        return retval;
    }
});
