Ext.define('AssetManagement.base.model.bo.BaseWorkCenter', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
 
	fields: [		
		//key fields
		{ name: 'arbpl',     type: 'string' },
		{ name: 'werks',     type:  'string' },
		
        //ordinary fields
		{ name: 'ktext',     type:   'string' },
		{ name: 'veran',     type:   'string' },
		 
        //delta fields
		{ name: 'updFlag',   type:   'string' }		
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});