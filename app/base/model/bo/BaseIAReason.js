Ext.define('AssetManagement.base.model.bo.BaseIAReason', {
  extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

  fields: [

    //key fields
    { name: 'grund',        type: 'string' },

    //ordinary fields
    { name: 'spras',        type: 'string' },
    { name: 'text',        type: 'string' },
  ],

  constructor: function (config) {
    this.callParent(arguments);
  }
});