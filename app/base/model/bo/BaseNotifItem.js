Ext.define('AssetManagement.base.model.bo.BaseNotifItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	requires: [
        'AssetManagement.customer.model.bo.NotifTask',
        'AssetManagement.customer.model.bo.NotifActivity',
        'AssetManagement.customer.model.bo.NotifCause',
        'Ext.data.Store'
    ],

    
	fields: [
		//key fields
		{ name: 'qmnum', type: 'string' },
		{ name: 'fenum', type: 'string' },
		
		//ordinary fields
		{ name: 'bautl', type: 'string' },
		{ name: 'fetxt', type: 'string' },
		{ name: 'fecod', type: 'string' },
		{ name: 'fegrp', type: 'string' },
		{ name: 'fekat', type: 'string' },
		{ name: 'fever', type: 'string', defaultValue: '000001' },
		{ name: 'oteil', type: 'string' },
		{ name: 'otgrp', type: 'string' },
		{ name: 'otkat', type: 'string' },
		{ name: 'otver', type: 'string', defaultValue: '000001' },
		{ name: 'posnr', type: 'string' },
		{ name: 'anzfehler', type: 'string' },
        { name: 'kzmla', type: 'string' },
		{ name: 'axoenh', type: 'string' },
		
		//date fields
		{ name: 'erzeit' },
		
		//delta fields
		{ name: 'updFlag',    type: 'string' },
		{ name: 'mobileKey',  type: 'string' },
		{ name: 'childKey',   type: 'string' },

		//references
		{ name: 'notifItemActivities', type: 'auto' },
		{ name: 'notifItemCauses',     type: 'auto' },
		{ name: 'notifItemTasks',      type: 'auto' },
		{ name: 'localLongtext',       type: 'auto' },
		{ name: 'backendLongtext',     type: 'auto' },
		{ name: 'damageCustCode',      type: 'auto' },
		{ name: 'objectPartCustCode',  type: 'auto' }
	],    
	
    constructor: function(config) {
        this.callParent(arguments);
        
        try {
            this.set('erzeit', config.erzeit ? config.erzeit : new Date());
            
            this.set('notifItemCauses', Ext.create('Ext.data.Store', {
	            model: 'AssetManagement.customer.model.bo.NotifCause',
	            autoLoad: false
            }));

            this.set('notifItemTasks', Ext.create('Ext.data.Store', {
	            model: 'AssetManagement.customer.model.bo.NotifTask',
	            autoLoad: false
            }));

            this.set('notifItemActivities', Ext.create('Ext.data.Store', {
	            model: 'AssetManagement.customer.model.bo.NotifActivity',
	            autoLoad: false
            }));

            if(config.notif) {
	            this.set('qmnum', config.notif.get('qmnum'));
	            this.set('mobileKey', config.notif.get('mobileKey'));
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNotifItem', ex);
        }
    }
});