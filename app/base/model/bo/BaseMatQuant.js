Ext.define('AssetManagement.base.model.bo.BaseMatQuant', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields
	    { name: 'lgnum' ,       type:   'string' },
	    { name: 'werks',        type:   'string' },
		  { name: 'lgort' ,       type:   'string' },
	    { name: 'lgpla',        type:   'string' },
	    { name: 'lgtyp',        type:   'string' },
		  { name: 'matnr' ,       type:   'string' },

        //ordinary fields
      { name: 'maktx',        type:   'string' },
      { name: 'charg',        type:   'string' },
      { name: 'meins',        type:   'string' },
      { name: 'verme',        type:   'string' },
      { name: 'gesme',        type:   'string' },
      { name: 'lgpbe',        type:   'string' },

      //local fields
      { name: 'menge_ist',        type:   'string' }
	],

	constructor: function(config) {
		this.callParent(arguments);
	}
});