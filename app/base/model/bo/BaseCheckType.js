Ext.define('AssetManagement.base.model.bo.BaseCheckType', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'plnty',       type: 'string' },
	    { name: 'plnnr',       type: 'string' },
	    { name: 'plnkn',       type: 'string' },
	    { name: 'kzeinstell',  type: 'string' },
		{ name: 'merknr' ,     type: 'string' },
		{ name: 'zaehl',       type: 'string' },
	    
        //ordinary fields
        { name: 'kurztext' ,   type:  'string' },
		{ name: 'kzeinstell',  type:  'string' },
		{ name: 'matnr' ,      type:  'string' },
		{ name: 'merknr',      type:  'string' },
		
		{ name: 'tplnr',       type:  'string' },
		{ name: 'vornr' ,      type:  'string' },
		{ name: 'werk',        type:  'string' },
		{ name: 'zaehl',       type:  'string' },
		
        //delta fields
		{ name: 'updFlag',     type: 'string' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});
