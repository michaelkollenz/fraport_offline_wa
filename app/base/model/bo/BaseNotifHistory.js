Ext.define('AssetManagement.base.model.bo.BaseNotifHistory', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
		//key fields
		{ name: 'qmnum',               type:  'string' },
		
		//ordinary fields
		{ name: 'qmart',              type:  'string' },
		{ name: 'tplnr',              type:  'string' },
		{ name: 'equnr',              type:  'string' },
		{ name: 'bautl',              type:  'string' },
		{ name: 'qmtxt',              type:  'string' },
		{ name: 'aufnr',              type:  'string' },
		{ name: 'artpr',              type:  'string' },
		{ name: 'priok',              type:  'string' },
		{ name: 'qmnam',              type:  'string' },
		{ name: 'iwerk',              type:  'string' },
		{ name: 'ingrp',              type:  'string' },
		{ name: 'gewrk',              type:  'string' },
		{ name: 'swerk',              type:   'string' },
		{ name: 'ktext',              type:   'string' },
		{ name: 'system_status',       type:  'string' },
		{ name: 'user_status',         type:  'string' },
		{ name: 'obj_part_p1',          type:  'string' },
		{ name: 'obj_part_grp_p1',       type:  'string' },
		{ name: 'damage_p1',           type:  'string' },
		{ name: 'damage_grp_p1',        type:  'string' },
		{ name: 'item_text_p1',         type:  'string' },
		{ name: 'cause_p1',            type:  'string' },
		{ name: 'cause_grp_p1',         type:  'string' },
		{ name: 'cause_text_p1',        type:  'string' },
		{ name: 'fekat',              type:  'string' },
		{ name: 'urkat',              type:  'string' },
		{ name: 'otkat',              type:  'string' },
		{ name: 'postalCode',         type:   'string' },
		{ name: 'city1',              type:  'string' },
		{ name: 'qmartx',             type:   'string' },
		
		//date fields
		{ name: 'strmn' },
		{ name: 'strur' },
		{ name: 'ltrmn' },
		{ name: 'ltrur' },
		{ name: 'ausvn' },
		{ name: 'ausbs' },
		{ name: 'auztv' },
		{ name: 'auztb' },
		{ name: 'mzeit'},
		{ name: 'qmdat'},		
		
        //delta fields
		{ name: 'updFlag', type: 'string' }	
	],
	
    constructor: function(config) {
        this.callParent(arguments);
    }
});