Ext.define('AssetManagement.base.model.bo.BaseDocd', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [	    
	    //key fields
	    { name: 'id',          type: 'string' },
	    { name: 'linenumber',  type: 'string' },
	   
        //ordinary fields
        { name: 'line' ,       type: 'string' },
		
        //delta fields
		{ name: 'updFlag',     type: 'string' },
        { name: 'mobileKey',   type: 'string' }
	],

	constructor: function(config) {
		this.callParent(arguments);
	}
});