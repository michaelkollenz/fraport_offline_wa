Ext.define('AssetManagement.base.model.bo.BaseInventoryHead', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    requires: [
        'Ext.data.Store',
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.manager.CompletionManager'
    ],

    fields: [
	    //key fields	 
	    { name: 'fiscalyear', type: 'string' },
	    { name: 'physinventory', type: 'string' },

        //ordinary fields
        { name: 'adjustStatus', type: 'string' },
		{ name: 'countDate', type: 'string' },
		{ name: 'deleteStatus', type: 'string' },
		{ name: 'dirty', type: 'date' },
		{ name: 'docDate', type: 'string' },
		{ name: 'eventType', type: 'string' },
		{ name: 'fisPeriod', type: 'string' },
		{ name: 'freezebookinv', type: 'string' },
		{ name: 'groupingCrit', type: 'string' },
        { name: 'groupingType', type: 'string' },
		{ name: 'mobilekey', type: 'string' },
		{ name: 'nameStgeLoc', type: 'string' },
		{ name: 'nameWerk', type: 'string' },
		{ name: 'physInvNo', type: 'string' },
		{ name: 'physInvNoLong', type: 'string' },
        { name: 'physInvRef', type: 'string' },
		{ name: 'physInvRefLong', type: 'string' },
		{ name: 'plant', type: 'string' },
        { name: 'planDate', type: 'string' },
		{ name: 'postBlock', type: 'string' },
		{ name: 'pstngDate', type: 'string' },
		{ name: 'specStock', type: 'string' },
		{ name: 'stgeLoc', type: 'string' },

        //delta fields
		{ name: 'updFlag', type: 'string' },

        //references
        { name: 'item', type: 'string' },
        { name: 'items', type: 'auto' },

        //dynamic fields
        { name: 'itemSum', type: 'number', defaultValue: 0 }
    ],

    constructor: function (config) {
        this.callParent(arguments);

        try {

            if (config.inventory) {
                this.set('physinventory', config.inventory.get('physinventory'));
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNotifItem', ex);
        }
    }
});