Ext.define('AssetManagement.base.model.bo.BaseCust_002', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'mobileuser',     type: 'string' },
	    { name: 'werks',          type: 'string' },
	    { name: 'lgort',          type: 'string' },
	   
	    //ordinary fields
        { name: 'default_lgort' , type: 'string' },
		{ name: 'wrkbe',          type: 'string' },
		{ name: 'lgobe' ,         type: 'string' },
		
		
	    //delta fields
		{ name: 'updFlag',        type: 'string' }
   ],

	constructor : function(config) {
		this.callParent(arguments);
	}
});