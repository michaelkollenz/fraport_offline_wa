Ext.define('AssetManagement.base.model.bo.BaseCharactValue', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'atinn',       type: 'string' },
	    { name: 'atzhl',       type: 'string' },
	    { name: 'adzhl', type: 'string' },

	    //ordinary fields
        { name: 'atwrt' ,      type: 'string' },
	    { name: 'atstd',       type: 'string' },
	    { name: 'atwtb' ,      type: 'string' },
	    { name: 'atflv',       type:  'string'},
	    { name: 'atflb' ,      type: 'string' },
	    { name: 'atcod',       type:  'string'},
	    { name: 'atawe' ,      type: 'string' },
	    { name: 'ataw1',       type:  'string'},
	    { name: 'attlv' ,      type: 'string' },
	    { name: 'attlb',       type:  'string'},
		{ name: 'atidn',       type:  'string'},
		
		//Value comes from BaseCharact
        { name: 'atfor',       type: 'string' },
		
		//delta fields
	    { name: 'updFlag',     type: 'string' }
	],
	

	constructor: function(config) {
		this.callParent(arguments);
	}
});
