Ext.define('AssetManagement.base.model.bo.BaseDMSDocument', {
	extend: 'AssetManagement.customer.model.bo.File',

	fields: [	  
	    //key fields
	    { name: 'refType',        type: 'string' },
	    { name: 'refObject',      type: 'string' },
	    { name: 'documentType',    type: 'string' },
	    { name: 'documentNumber',  type: 'string' },
	    { name: 'docId',         type: 'string' },
	    { name: 'storageCategory', type: 'string' },
	    
        //ordinary fields
        { name: 'documentVersion', type: 'string' },
		{ name: 'documentPart',    type: 'string' },
		{ name: 'validFromDate',   type: 'string' },
		{ name: 'wsApplication',   type: 'string' },
		{ name: 'docFile',         type: 'string' },
		{ name: 'fileDescr',       type: 'string' },
		{ name: 'docDescr',        type: 'string' },
		{ name: 'language',        type: 'string' },
		{ name: 'laboratory',      type: 'string' },
	 	{ name: 'fileSize',        type: 'string' },
        //delta fields
		{ name: 'updFlag',         type: 'string' }       
	],
	

	constructor : function(config) {
		this.callParent(arguments);
	}
});