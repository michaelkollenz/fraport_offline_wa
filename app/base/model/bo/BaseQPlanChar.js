Ext.define('AssetManagement.base.model.bo.BaseQPlanChar', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    inheritableStatics: {
        CATEGORIES: {
            QUANTITATIVE: 1,
            QUALITATIVE_STANDARD: 2,
            QUALITATIVE_CHECKBOX: 3
        }
    },

	fields: [
	    //key fields
	    { name: 'plnty',        type:   'string' },
	    { name: 'plnnr',        type:   'string' },
		{ name: 'plnkn' ,       type:   'string' },
		{ name: 'kzeinstell',   type:   'string' },
		{ name: 'merknr',       type:   'string' },
		{ name: 'zaehl',        type:   'string' },

		
        //ordinary fields
        { name: 'gueltigab' ,   type:   'string' },
        { name: 'kurztext' ,    type:   'string' },
        { name: 'katab1' ,      type:   'string' },
        { name: 'katalgart1' ,  type:   'string' },
        { name: 'auswmenge1' ,  type:   'string' },
        { name: 'auswmgwrk1' ,  type:   'string' },
        { name: 'katab2' ,      type:   'string' },
        { name: 'katalgart2' ,  type:   'string' },
        { name: 'auswmenge2' ,  type:   'string' },
        { name: 'auswmgwrk2' ,  type:   'string' },
        { name: 'sollwert' ,    type:   'string' },
        { name: 'toleranzob' ,  type:   'string' },
        { name: 'toleranzun' ,  type:   'string' },
        { name: 'masseinhsw' ,  type:   'string' },
        { name: 'char_type' ,   type:   'string' },
        { name: 'stellen',      type:   'string' },
        { name: 'formel1',      type:   'string' },
        { name: 'dummy10',      type:   'string' },
        { name: 'pzlfh',        type:   'string' },
        { name: 'dokukz',       type:   'string' },
        { name: 'verwmerkm',    type:   'string' },

        { name: 'dummy20', type: 'string' },
        { name: 'dummy40', type: 'string' },


        { name: 'category', type: 'auto' },

	    //delta fields
		{ name: 'updFlag', type: 'string' },

        //reference
        { name: 'codes', type: 'auto' },
        { name: 'codeMeanings', type: 'auto' }, //contains store of Cust013 records

		// group fields and the group name
        { name: 'qpOpertxt', type: 'auto' }
	],

	constructor: function(config) {
		this.callParent(arguments);
	}
});