Ext.define('AssetManagement.base.model.bo.BaseCatsRec', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    fields: [
	    //key fields
	    { name: 'Counter',     type:'string' },
	  
	    //ordinary fields
        { name: 'pernnr' ,     type: 'string' },
	    { name: 'lstar',       type: 'string' },
		{ name: 'lstnr' ,      type: 'string' },
		{ name: 'rkostl',      type: 'string' },
		{ name: 'rproj' ,      type: 'string' },
		{ name: 'raufnr',      type: 'string' },
		{ name: 'rnplnr' ,     type: 'string' },
		{ name: 'rkdauf',      type: 'string' },
		{ name: 'rkdpos' ,     type: 'string' },
		{ name: 'awart',       type: 'string' },
		{ name: 'lgart' ,      type: 'string' },
		{ name: 'split',       type: 'string' },
		{ name: 'kokrs' ,      type: 'string' },
		{ name: 'meinh',       type: 'string' },
		{ name: 'tcurr' ,      type: 'string' },
		{ name: 'price',       type: 'string' },
		{ name: 'arbid' ,      type: 'string' },
		{ name: 'werks',       type: 'string' },
		{ name: 'bemot' ,      type: 'string' },
		{ name: 'statkeyfig',  type: 'string' },
		{ name: 'vornr' ,      type: 'string' },
		{ name: 'uvorn',       type: 'string' },
		{ name: 'arbpl' ,      type: 'string' },
		{ name: 'posid',       type: 'string' },
		{ name: 'catshours',   type:'string' },
		{ name: 'beguz',       type: 'string' },
		{ name: 'enduz' ,      type: 'string' },
		{ name: 'vtken',       type: 'string' },
		{ name: 'alldf',       type: 'string' },
		{ name: 'aueru',       type: 'string' },
		{ name: 'ltxa1' ,      type: 'string' },
		{ name: 'eruzu',       type: 'string' },
		{ name: 'catsamount' , type: 'string' },
		{ name: 'catsquantity', type: 'string' },
		{ name: 'taskcounter' , type:'string' },
		{ name: 'tasktype',    type: 'string' },
	   	{ name: 'tasklevel' ,  type: 'string' },
		{ name: 'updflag',     type: 'string' },
			
		//date fields
        { name: 'workDate' },
		{ name: 'begdt' },
		{ name: 'enddt' },
		{ name: 'pedd' } ,
				
        //delta fields
		{ name: 'updFlag',   type:'string' },
		{ name: 'mobileKey', type: 'string' },
		{ name: 'childkey',  type: 'string' },
		{ name: 'childkey2', type: 'string' }
	],
	
	constructor: function(config) {
	    this.callParent(arguments);
    }
});
		
		
		
		
		
		
		
		


















