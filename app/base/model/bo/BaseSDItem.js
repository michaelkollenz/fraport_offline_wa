Ext.define('AssetManagement.base.model.bo.BaseSDItem', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
		//key fields
		{ name: 'vbeln', type: 'string' },

		//ordinary fields
		{ name: 'posnr', type: 'string' },
		{ name: 'matnr', type: 'string' },
		{ name: 'zmeng', type: 'string' },
		{ name: 'zieme', type: 'string' },
		{ name: 'arktx', type: 'string' },
		{ name: 'abgru', type: 'string' },
        { name: 'sernr', type: 'string' },
        { name: 'charge', type: 'string' },
        { name: 'werks', type: 'string' },
        { name: 'lgort', type: 'string' },
        { name: 'usr1', type: 'string' },
        { name: 'usr2', type: 'string' },

		//delta fields
		{ name: 'updFlag', type: 'string' },
		{ name: 'mobileKey', type: 'string' },
		{ name: 'childKey', type: 'string' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});