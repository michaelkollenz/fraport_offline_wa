Ext.define('AssetManagement.base.model.bo.BaseUserDetails', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	mixins: ['Ext.mixin.Observable'],

	inheritableStatics: {
		_instance: null,
		
		getInstance: function(reset) {
			if(!this._instance)
				this._instance = Ext.create('AssetManagement.customer.model.bo.UserDetails');
				
			return this._instance;
		}
	},
   
	fields: [
		//key fields
		{ name: 'uname', type: 'string' },
		
		//ordinary fields
		{ name: 'email', type: 'string' },
		{ name: 'usersTitle', type: 'string' },
		{ name: 'firstname', type: 'string' },
		{ name: 'lastname', type: 'string' },
		{ name: 'fullName', type: 'string' },
		{ name: 'nickname', type: 'string' },
		{ name: 'telNumber', type: 'string' },
		{ name: 'telExtension', type: 'string' }
	]
});