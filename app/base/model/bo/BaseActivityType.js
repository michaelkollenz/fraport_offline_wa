Ext.define('AssetManagement.base.model.bo.BaseActivityType', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
			//key fields
			{ name: 'kokrs',  type: 'string' },
			{ name: 'lstar' , type:'string'},
			{ name: 'arbpl' , type:'string'},
			{ name: 'werks' , type: 'string'},
			
			//ordinary fields
			{ name: 'ktext',  type:'string' },
			{ name: 'leinh' , type: 'string' },
			
			//delta fields
			{ name: 'updFlag' , type: 'string'}
	],
    
	constructor : function(config) {
		this.callParent(arguments);
	}
});