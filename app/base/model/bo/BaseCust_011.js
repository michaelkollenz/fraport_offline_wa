Ext.define('AssetManagement.base.model.bo.BaseCust_011', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'scenario',         type: 'string' },
	    { name: 'auart',            type: 'string' },
	    //ordinary fields
        { name: 'txt' ,             type: 'string' },
		{ name: 'createallowed',    type: 'string' },
		{ name: 'steus' ,           type: 'string' },
		{ name: 'flagrelease' ,     type: 'string' },
		{ name: 'withmeasdoc',      type: 'string' },
		{ name: 'partnertransfer' , type: 'string' },
		{ name: 'qmart',            type: 'string' },
		{ name: 'ilart' ,           type: 'string' },
		{ name: 'service',          type: 'string' },
		{ name: 'pdfrepactive',     type: 'string' },
		{ name: 'stsma' ,           type: 'string' },
		{ name: 'vrg_stsma',        type: 'string' },
		{ name: 'artpr' ,           type: 'string' },
		{ name: 'qpart' ,           type: 'string' },
		
        //delta fields
		{ name: 'updFlag',          type: 'string' }
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});