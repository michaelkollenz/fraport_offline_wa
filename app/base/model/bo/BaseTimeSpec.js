Ext.define('AssetManagement.base.model.bo.BaseTimeSpec', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
		//key fields
		{ name: 'timespeckey',      type:  'string' },
	
        //ordinary fields
		{ name: 'businesspartner',  type:  'string' },
		{ name: 'pernr',            type:  'string' },
		{ name: 'timespectype',     type:  'string' },
		{ name: 'priority',         type:  'string' },
		{ name: 'description',      type:  'string' },
		{ name: 'isavailable',      type:  'string' },
		
		//date fields
        { name: 'startdate' },
		{ name: 'starttime' },
		{ name: 'enddate'},
		{ name: 'endtime' },
		
        //delta fields
		{ name: 'updFlag',    type: 'string' }
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});