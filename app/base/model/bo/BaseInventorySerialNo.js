﻿Ext.define('AssetManagement.base.model.bo.BaseInventorySerialNo', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
	    //key fields	 
	    { name: 'fiscalyear', type: 'string' },
	    { name: 'physinventory', type: 'string' },
	    { name: 'item', type: 'string' },
        { name: 'serialNo', type: 'string' },

        //ordinary fields
		{ name: 'dirty', type: 'date' },

        //delta fields
		{ name: 'updFlag', type: 'string' },

        //dynamic fields
	    { name: 'serialList', type: 'number', defaultValue: 0 }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});