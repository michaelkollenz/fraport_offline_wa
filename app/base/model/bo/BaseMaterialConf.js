Ext.define('AssetManagement.base.model.bo.BaseMaterialConf', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
        //key fields	 
        { name: 'mblnr',      type:  'string' },
        { name: 'mjahr',      type:  'string' },
        { name: 'aufnr',      type:  'string' },
	   
        //ordinary fields
        { name: 'move_type',  type: 'string' },
        { name: 'spec_stock', type: 'string' },
        { name: 'stck_type',  type: 'string' },
        { name: 'plant',      type: 'string' },
        { name: 'storloc',    type: 'string' },
        { name: 'customer',   type: 'string' },
        { name: 'matnr',      type: 'string' },
        { name: 'quantity',   type: 'string' },
        { name: 'unit',       type: 'string' },
        { name: 'rsnum',      type: 'string' },
        { name: 'rspos',      type: 'string' },
        { name: 'kzear',      type: 'string' },
        { name: 'batch',      type: 'string' },
        { name: 'equnr',      type: 'string' },
        { name: 'serialno',   type: 'string' },
        { name: 'maktx',      type: 'string' },
        { name: 'ref_doc_no', type: 'string' },
        { name: 'gr_rcpt', 	  type: 'string' },
        { name: 'bemot',      type: 'string' },
        { name: 'vornr',      type: 'string' },
        { name: 'valtype',    type: 'string' },
        
        //date fieds
        { name: 'isdd', 	  type: 'date'},

        //delta fields
        { name: 'updFlag',    type: 'string' },
        { name: 'mobileKey',  type: 'string' },
        
        //references
        { name: 'material',   			type: 'auto' },
        { name: 'accountIndication',   	type: 'auto' }
    ],
	
    constructor : function(config) {
        this.callParent(arguments);

        try {
            if (config) {
                if (config.order) {
                    var order = config.order;
	        		
                    this.set('aufnr', order.get('aufnr'));
                    this.set('plant', order.get('iwerk'));
                    this.set('bemot', order.get('bemot'));
                    this.set('mobileKey', order.get('mobileKey'));
                }
	        
                if (config.operation) {
                    var operation = config.operation;
		        
                    //override order dependend fields, because order may be omitted
                    this.set('aufnr', operation.get('aufnr'));
                    this.set('vornr', operation.get('vornr'));
                    this.set('mobileKey', operation.get('mobileKey'));
                }
            }
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseMaterialConf', ex);
        }
    }
});