Ext.define('AssetManagement.base.model.bo.BaseSDOrder', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
    ],

	fields: [
		//key fields
		{ name: 'vbeln', type: 'string' },
		
		//ordinary fields
		{ name: 'auart', type: 'string' },
		{ name: 'vkorg', type: 'string' },
		{ name: 'vtweg', type: 'string' },
		{ name: 'spart', type: 'string' },
		{ name: 'vkgrp', type: 'string' },
		{ name: 'vkbur', type: 'string' },
		{ name: 'augru', type: 'string' },
		{ name: 'bstnk', type: 'string' },
		{ name: 'aufnr', type: 'string' },
		{ name: 'qmnum', type: 'string' },
		{ name: 'kunnrAG', type: 'string' },
		{ name: 'kunnrWE', type: 'string' },
		{ name: 'addrNo', type: 'string' },
		{ name: 'formOfAddress', type: 'string' },
		{ name: 'name1', type: 'string' },
		{ name: 'name2', type: 'string' },
		{ name: 'name3', type: 'string' },
		{ name: 'name4', type: 'string' },
		{ name: 'coName', type: 'string' },
		{ name: 'city', type: 'string' },
		{ name: 'district', type: 'string' },
		{ name: 'cityNo', type: 'string' },
		{ name: 'postalCode1', type: 'string' },
		{ name: 'postalCode2', type: 'string' },
		{ name: 'postalCode3', type: 'string' },
		{ name: 'poBox', type: 'string' },
		{ name: 'poBoxCity', type: 'string' },
		{ name: 'delivDis', type: 'string' },
		{ name: 'street', type: 'string' },
		{ name: 'streetNo', type: 'string' },
		{ name: 'strAbbr', type: 'string' },
		{ name: 'houseNo', type: 'string' },
		{ name: 'strSuppl1', type: 'string' },
		{ name: 'strSuppl2', type: 'string' },
		{ name: 'location', type: 'string' },
		{ name: 'building', type: 'string' },
		{ name: 'floor', type: 'string' },
		{ name: 'roomNo', type: 'string' },
		{ name: 'country', type: 'string' },
		{ name: 'langu', type: 'string' },
		{ name: 'region', type: 'string' },
		{ name: 'lfgsk', type: 'string' },
        { name: 'vtime', type: 'string' },

	
		//date fields
		{ name: 'vdatu', type: 'date' },
		{ name: 'bstdk', type: 'date' },
		
		//delta fields
		{ name: 'updFlag',   type: 'string' },
		{ name: 'mobileKey', type: 'string' },
        { name: 'mobileKey_ref', type: 'string' },

		//references
		{ name: 'orderType',        type: 'auto' },
		{ name: 'items',        	type: 'auto' },
		{ name: 'order',        	type: 'auto' },
		{ name: 'notif',        	type: 'auto' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	},
	
	getDeliveryStatus: function() {
		var retval = '';
		
		try {
			switch(this.get('lfgsk')) {
				case 'A':
					retval = Locale.getMsg('notDelivered');
					break;
				case 'B':
					retval = Locale.getMsg('partiallyDelivered');
					break;
				case 'C':
					retval = Locale.getMsg('fullyDelivered');
					break;
				default:
					break;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeliveryStatus of BaseSDOrder', ex)
		}
		
		return retval;
	}
});