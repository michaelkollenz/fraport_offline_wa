Ext.define('AssetManagement.base.model.bo.BaseScenario', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	mixins: ['Ext.mixin.Observable'],

	requires: [
	    'Ext.data.Store',
        'AssetManagement.customer.model.bo.SDOrderItem',
        'AssetManagement.customer.helper.OxLogger'
	],
   
	fields: [
		//key fields
		{ name: 'mandt',                type: 'string' },
        { name: 'scenario',             type: 'string' },
        { name: 'userId',               type: 'string' },
        { name: 'default_scenario',     type: 'string' },
        { name: 'description',          type: 'string' },
		
		//control field
		{ name: 'isInitialized', type: 'auto' }
	],
    
	constructor: function (config) {
	    this.callParent(arguments);
	}
});