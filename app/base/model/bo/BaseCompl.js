﻿Ext.define('AssetManagement.base.model.bo.BaseCompl', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
		//key fields
		{ name: 'objnr' , type:'string'},
			
		//ordinary fields
		{ name: 'mobilekey' , type:'string'},
			
		//delta fields
		{ name: 'updFlag' , type: 'string'}
	],
    
    /*
        * creates a new compl-object with the provided data
        * sets the updFlag to I
        */
    constructor : function(config) {
        this.callParent(arguments);

        try {
            this.set('objnr', config.objnr);

            if(config.mobilekey)                    
                this.set('mobilekey', config.mobilekey);
            else
                this.set('mobilekey', '');

            this.set('updFlag', 'I');
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseCompl', ex);
        }
    }
});