Ext.define('AssetManagement.base.model.bo.BaseAddress', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
	   'AssetManagement.customer.utils.StringUtils'
	],
 
	fields: [
			//key fields
			{ name: 'adrnr' },
			
			//ordinary fields
			{ name: 'title',     type: 'string' },
			{ name: 'name1',     type: 'string' },
			{ name: 'name2',     type: 'string' },
			{ name: 'name3',     type: 'string' },
			{ name: 'name4',     type: 'string' },
			{ name: 'city1',     type: 'string' },
			{ name: 'city2',     type: 'string' },
			{ name: 'postCode1', type: 'string' },
			{ name: 'street',    type: 'string' },
			{ name: 'houseNum1', type: 'string' },
			{ name: 'country',   type: 'string' },
			{ name: 'region',    type: 'string' },
			{ name: 'telNumber', type: 'string' },
			{ name: 'telExtens', type: 'string' },
			{ name: 'faxNumber', type: 'string' },
			{ name: 'faxExtens', type: 'string' },
			{ name: 'telMobile', type: 'string' },
            { name: 'str_suppl1',type: 'string' },
			
			//delta fields
			{ name: 'updFlag', type: 'string' }
		],
    
	
    constructor: function(config) {
        this.callParent(arguments);
    },
    
    getName: function() {
	    var retval = '';
	    
	    try {
    	    retval = AssetManagement.customer.utils.StringUtils.concatenate([ this.get('name1'), this.get('name2'), this.get('name3'), this.get('name4') ], null, true);
    	} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getName of BaseAddress', ex);
		}  
    	
		return retval;
	},
	
	getStreet: function() {
		var retval = '';
	    
        try {
            retval = this.get('street');
        } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStreet of BaseAddress', ex);
        }  

        return retval;
	},
	
	getHouseNo: function() {
		var retval = '';
	    
        try {
            retval = this.get('houseNum1');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHouseNo of BaseAddress', ex);
        }  

        return retval;
	},
	
	getCity: function() {		
		var retval = '';
	    
        try {
            retval = AssetManagement.customer.utils.StringUtils.concatenate([ this.get('city1'), this.get('city2') ], null, true);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCity of BaseAddress', ex);
        }  

        return retval;
	},
	
	getPostalCode: function() {
		var retval = '';
	    
        try {
            retval = this.get('postCode1');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseAddress', ex);
        }  

        return retval;
	}
});