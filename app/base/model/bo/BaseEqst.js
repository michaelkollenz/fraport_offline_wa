Ext.define('AssetManagement.base.model.bo.BaseEqst', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'equnr',  type:  'string' },
	    
       //ordinary fields
        { name: 'werks' , type: 'string' },
		{ name: 'stlan',  type: 'string' },
		{ name: 'stlnr' , type: 'string' },
		{ name: 'stlal',  type: 'string' },
		
        //delta fields
		{ name: 'updFlag', type: 'string' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});