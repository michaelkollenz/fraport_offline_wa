﻿Ext.define('AssetManagement.base.model.bo.BaseLocalOrderStatus', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
		{ name: 'statusCode', type: 'int' },
		{ name: 'statusText', type: 'string' },
		{ name: 'statusIconPath', type: 'string' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});