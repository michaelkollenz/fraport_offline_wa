Ext.define('AssetManagement.base.model.bo.BaseKeyFigure', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [	   
	    //key fields
	    { name: 'stagr' , type:'string' },
	    
        //ordinary fields
        { name: 'msehi',  type:  'string' },
		{ name: 'bezei',  type:  'string' },
		{ name: 'kokrs',  type:  'string' },
		
        //delta fields
		{ name: 'updFlag', type: 'string' }       
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});