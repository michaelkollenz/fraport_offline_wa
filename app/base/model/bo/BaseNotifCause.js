Ext.define('AssetManagement.base.model.bo.BaseNotifCause', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'qmnum', type: 'string' },
		{ name: 'fenum', type: 'string' },
		{ name: 'urnum', type: 'string' },
		
		//ordinary fields
		{ name: 'bautl', type: 'string' },
		{ name: 'urtxt', type: 'string' },
		{ name: 'qurnum', type: 'string' },
		{ name: 'urcod', type: 'string' },
		{ name: 'urgrp', type: 'string' },
		{ name: 'urkat', type: 'string' },
		{ name: 'urver', type: 'string', defaultValue: '000001' },
		{ name: 'stsma', type: 'string' },
		{ name: 'kzmla', type: 'string' },
		//date fields
		{ name: 'erzeit' },
		
		//delta fields
		{ name: 'updFlag',   type: 'string' },
		{ name: 'mobileKey', type: 'string' },
		{ name: 'childKey',  type: 'string' },
		{ name: 'childKey2', type: 'string' },

		//references
		{ name: 'notif',           type: 'auto' },
		{ name: 'notifItem',       type: 'auto' },
		{ name: 'localLongtext',   type: 'auto' },
		{ name: 'backendLongtext', type: 'auto' },
		{ name: 'causeCustCode',   type: 'auto' }
	],
    
    constructor: function(config) {
        this.callParent(arguments);
        
        try {
            this.set('erzeit', config.erzeit ? config.erzeit : new Date());

            if(config.notif) {
	           this.set('qmnum', config.notif.get('qmnum'));
	           this.set('mobileKey', config.notif.get('mobileKey'));
            }

            if(config.notifItem) {
	           this.set('fenum', config.notifItem.get('fenum'));
	           this.set('childKey', config.notifItem.get('childKey'));
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseNotifCause', ex);
        }
    }
});