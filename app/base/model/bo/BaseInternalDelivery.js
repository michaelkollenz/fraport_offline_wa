Ext.define('AssetManagement.base.model.bo.BaseInternalDelivery', {
  extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

  requires: [
    'AssetManagement.customer.utils.StringUtils',
    'AssetManagement.customer.helper.OxLogger'
  ],

  fields: [
    //key fields
    {name: 'belnr', type: 'string'},

    //ordinary fields
    {name: 'segid', type: 'string'},
    {name: 'erdat', type: 'string'},
    {name: 'ertim', type: 'string'},
    {name: 'mblnr', type: 'string'},
    {name: 'mjahr', type: 'string'},
    {name: 'mblpo', type: 'string'},
    {name: 'ebeln', type: 'string'},
    {name: 'ebelp', type: 'string'},
    {name: 'usnam', type: 'string'},
    {name: 'txz01', type: 'string'},
    {name: 'menge', type: 'string'},
    {name: 'meins', type: 'string'},
    {name: 'empf', type: 'string'},
    {name: 'gebnr', type: 'string'},
    {name: 'raum', type: 'string'},
    {name: 'short', type: 'string'},
    {name: 'kostl', type: 'string'},
    {name: 'aufnr', type: 'string'},
    {name: 'pspnr', type: 'string'},
    {name: 'wlief', type: 'string'},
    {name: 'wuzeitv', type: 'string'},
    {name: 'wuzeitb', type: 'string'},
    {name: 'sgtxt', type: 'string'},
    {name: 'bereich', type: 'string'},
    {name: 'ia_status', type: 'string'},
    {name: 'grund', type: 'string'},
    {name: 'gtext', type: 'string'},
    {name: 'dienst', type: 'string'},
    {name: 'nummer', type: 'string'},
    {name: 'lfname', type: 'string'},
    {name: 'telefon', type: 'string'},
    {name: 'guid', type: 'string'},
    {name: 'email', type: 'string'},
    {name: 'prio', type: 'string'},
    {name: 'trans', type: 'string'},
    {name: 'xblock', type: 'string'},
    {name: 'vlgort', type: 'string'},
    {name: 'equnr', type: 'string'},
    {name: 'lidat', type: 'string'},
    {name: 'sequ', type: 'string'},
    {name: 'matnr', type: 'string'},
    {name: 'passcode', type: 'string'},
    {name: 'mobilekey', type: 'string'},
    {name: 'posid', type: 'string'},

    //delta fields
    {name: 'updFlag', type: 'string'}
  ],

  constructor: function (config) {
    this.callParent(arguments);
  }
});