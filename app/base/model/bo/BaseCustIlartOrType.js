Ext.define('AssetManagement.base.model.bo.BaseCustIlartOrType', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
		//key fields
		{ name: 'auart' },
		{ name: 'ilart' },
		
		//ordinary fields
		{ name: 'ilatx' },
		
		//delta fields
		{ name: 'updFlag' }
	],
 
    constructor: function(config) {
        this.callParent(arguments);
    }
});