Ext.define('AssetManagement.base.model.bo.BaseStsma', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'stsma',  type: 'string' },
		{ name: 'estat' , type:'string'},
			
		//ordinary fields
		{ name: 'txt04',  type:'string' },
		{ name: 'txt30' , type: 'string' },
		{ name: 'inist',  type:'string' },
		{ name: 'stonr' , type: 'string' },
		{ name: 'hsonr',  type:'string' },
		{ name: 'nsonr' , type: 'string' },
		{ name: 'linep',  type:'string' },
		{ name: 'statp' , type: 'string' },
		{ name: 'bersl' , type: 'string' },
			
		//delta fields
		{ name: 'updFlag' , type: 'string'}		
	],
      
    constructor: function(config) {
		this.callParent(arguments);
	}
});