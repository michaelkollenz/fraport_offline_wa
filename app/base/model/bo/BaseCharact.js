Ext.define('AssetManagement.base.model.bo.BaseCharact', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields
	    { name: 'clint',        type: 'string' },
	    { name: 'atinn',        type: 'string' },
		{ name: 'adzhl',        type: 'string' },
	    
	    //ordinary fields
	    { name: 'atnam' ,       type: 'string' },
		{ name: 'atfor',        type: 'string' },
		{ name: 'anzst' ,       type: 'string' },
		{ name: 'anzdz',        type: 'string' },
		{ name: 'atkle' ,       type: 'string' },
		{ name: 'aterf',        type: 'string' },
		{ name: 'atein' ,       type: 'string' },
		{ name: 'atint',        type: 'string' },
		{ name: 'atson' ,       type: 'string' },
		{ name: 'atinp',        type: 'string' },
		{ name: 'atvie' ,       type: 'string' },
		{ name: 'atbez',        type: 'string' },
		{ name: 'msehi' ,       type: 'string' },
		{ name: 'atwme',        type: 'string' },
		{ name: 'mseh6' ,       type: 'string' },
		{ name: 'mseht',        type: 'string' },
		{ name: 'qmerk',        type: 'string' },
		
	    //delta fields
		{ name: 'updFlag',      type:'string' },
		
		// references
	    { name: 'classValues',   type: 'auto' },
	    { name: 'charactValues', type: 'auto' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});		