Ext.define('AssetManagement.base.model.bo.BaseStpo', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'stlty',      type:  'string' },
		{ name: 'stlnr',      type:   'string' },
		{ name: 'stlkn',      type:  'string' },
		{ name: 'stpoz',      type:   'string' },
		{ name: 'idnrk',      type:  'string' },
		{ name: 'postp',      type:  'string' },
		{ name: 'posnr',      type:  'string' },
      
		//ordinary fields
		{ name: 'sortf',      type:  'string' },
		{ name: 'meins',      type:  'string' },
		{ name: 'menge',      type:  'string' },
		{ name: 'erskz',      type:  'string' },
		{ name: 'sanin',      type:  'string' },
		{ name: 'stkkz',      type:  'string' },
		{ name: 'datuv',      type:  'string' },
		
        //delta fields
		{ name: 'updFlag',    type:  'string' },
		
		//references
		{ name: 'material',        type: 'auto' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});