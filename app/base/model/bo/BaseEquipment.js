Ext.define('AssetManagement.base.model.bo.BaseEquipment', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'equnr',      type: 'string' },
		
		//ordinary fields
		{ name: 'tidnr',      type: 'string' },
		{ name: 'stort',      type: 'string' },
		{ name: 'eqktx',      type: 'string' },
		{ name: 'tplnr',      type: 'string' },
		{ name: 'eqtyp',      type: 'string' },
		{ name: 'eqart',      type: 'string' },
		{ name: 'objnr',      type: 'string' },
		{ name: 'invnr',      type: 'string' },
		{ name: 'herst',      type: 'string' },
		{ name: 'herld',      type: 'string' },
		{ name: 'serge',      type: 'string' },
		{ name: 'typbz',      type: 'string' },
		{ name: 'baujj',      type: 'string' },
		{ name: 'baumm',      type: 'string' },
		{ name: 'matnr',      type: 'string' },
		{ name: 'sernr',      type: 'string' },
		{ name: 'werk',       type: 'string' },
		{ name: 'lager',      type: 'string' },
		{ name: 'iwerk',      type: 'string' },
		{ name: 'submt',      type: 'string' },
		{ name: 'hequi',      type: 'string' },
		{ name: 'heqnr',      type: 'string' },
		{ name: 'ingrp',      type: 'string' },
		{ name: 'kund1',      type: 'string' },
		{ name: 'kund2',      type: 'string' },
		{ name: 'kund3',      type: 'string' },
		{ name: 'rbnr',       type: 'string' },
		{ name: 'spras',      type: 'string' },
		{ name: 'vkorg',      type: 'string' },
		{ name: 'vtextVkorg', type: 'string' },
		{ name: 'vtweg',      type: 'string' },
		{ name: 'vtextVtweg', type: 'string' },
		{ name: 'spart',      type: 'string' },
		{ name: 'vtextSpart', type: 'string' },
		{ name: 'msgrp',      type: 'string' },
		{ name: 'eqfnr',      type: 'string' },
		{ name: 'groes',      type: 'string' },
		{ name: 'mapar',      type: 'string' },
		{ name: 'gewrk',      type: 'string' },
		{ name: 'wergw',      type: 'string' },
		{ name: 'txtGewrk',   type: 'string' },
		{ name: 'adrnr',      type: 'string' },
		{ name: 'stsma',      type: 'string' },
        { name: 'abckz',      type: 'string' },
        { name: 'b_werk',     type: 'string' },
        { name: 'b_lager',    type: 'string' },
        { name: 'lbbsa',      type: 'string' },
        { name: 'b_charge',   type: 'string' },
        { name: 'sobkz',      type: 'string' },
        { name: 'kunnr',      type: 'string' },
        { name: 'lifnr',      type: 'string' },
        { name: 'kdauf',      type: 'string' },
        { name: 'kdpos',      type: 'string' },
        { name: 'brgew',      type: 'string' },
        { name: 'gewei',      type: 'string' },
        { name: 'usr01flag',  type: 'string' },
        { name: 'usr02flag',  type: 'string' },
        { name: 'usr03code',  type: 'string' },
        { name: 'usr04code',  type: 'string' },
        { name: 'usr05txt',   type: 'string' },
        { name: 'usr06txt',   type: 'string' },
        { name: 'usr07txt',   type: 'string' },
        { name: 'usr09num',   type: 'string' },
		
		//date fields
		{ name: 'ansdt', 	type: 'date'},
		{ name: 'inbdt', 	type: 'date'},
		{ name: 'gwldt',	type: 'date'},
		{ name: 'gwlen',	type: 'date' },
		{ name: 'gwldt_i',	type: 'date'},
		{ name: 'gwlen_i',  type: 'date' },
        { name: 'usr08date',type: 'date'},

		//delta fields
		{ name: 'updFlag',         type: 'string' },
		{ name: 'mobileKey',       type: 'string' },

		//references
		{ name: 'funcLoc',            	type: 'auto' },
		{ name: 'superiorEquipment',  	type: 'auto' },
		{ name: 'partners',           	type: 'auto' },
		{ name: 'address',            	type: 'auto' },
		{ name: 'objectStatusList',   	type: 'auto' },
		{ name: 'classifications',    	type: 'auto' },
		{ name: 'documents',          	type: 'auto' },
		{ name: 'measPoints',         	type: 'auto' },
		{ name: 'historicalOrders',     type: 'auto' },
		{ name: 'historicalNotifs',     type: 'auto' },
		{ name: 'localLongtext',      	type: 'auto' },
		{ name: 'backendLongtext',    	type: 'auto' },
		{ name: 'internNote',         	type: 'auto' },
		
		//mapping fields
	    { 	name: 'postalCode',
	    	convert: function (notUsed, data) {
	        	return data.getPostalCode.call(data);
	        },
	        depends: ['address']
	    }
	],
	
	getPostalCode: function() {
		var retval = '';
		
		try {
			var address = this.get('address');
			
			if(address) {
				retval = address.get('postCode1');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseEquipment', ex);
		}
		
		return retval;
	},
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});