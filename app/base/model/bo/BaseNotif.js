Ext.define('AssetManagement.base.model.bo.BaseNotif', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	requires: [
	    'Ext.data.Store',
        'AssetManagement.customer.model.bo.SDOrderItem',
        'AssetManagement.customer.helper.OxLogger'
    ],

	fields: [		
		//key fields
		{ name: 'qmnum',    type: 'string' },
		
		//ordinary fields
		{ name: 'qmtxt',    type: 'string' },
		{ name: 'aufnr',    type: 'string' },
		{ name: 'ktext',    type: 'string' },
		{ name: 'equnr',    type: 'string' },
		{ name: 'eqktx',    type: 'string' },
		{ name: 'tplnr',    type: 'string' },
		{ name: 'pltxt',    type: 'string' },
		{ name: 'qmart',    type: 'string' },
		{ name: 'qmartTxt', type: 'string' },
		{ name: 'artpr',    type: 'string' },
		{ name: 'priok',    type: 'string' },
		{ name: 'prueflos', type: 'string' },
		{ name: 'iwerk',    type: 'string' },
		{ name: 'bautl',    type: 'string' },
		{ name: 'ingrp',    type: 'string' },
		{ name: 'warpl',    type: 'string' },
		{ name: 'wapos',    type: 'string' },
		{ name: 'wppos',    type: 'string' },
		{ name: 'eqfnr',    type: 'string' },
		{ name: 'strno',    type: 'string' },
		{ name: 'stort',    type: 'string' },
		{ name: 'tidnr',    type: 'string' },
		{ name: 'msgrp',    type: 'string' },
		{ name: 'qmnam',    type: 'string' },
		{ name: 'rbnr',     type: 'string' },
		{ name: 'gewrk',    type: 'string' },
		{ name: 'swerk',    type: 'string' },
		{ name: 'flagPlos', type: 'string' },
		{ name: 'stsma',    type: 'string' },
		{ name: 'qmkat',    type: 'string' },
		{ name: 'qmgrp',    type: 'string' },
		{ name: 'qmcod',    type: 'string' },
		{ name: 'matnr',    type: 'string' },
		{ name: 'auswk',    type: 'string' },
		{ name: 'sernr',    type: 'string' },
		{ name: 'msaus',    type: 'string' },
		{ name: 'kzmla', 	type: 'string' },
		{ name: 'ktext', 	type: 'string' },

		
        { name: 'usr01Flag', type: 'string' },
        { name: 'usr02Flag', type: 'string' },
        { name: 'usr03Code', type: 'string' },
        { name: 'usr04Code', type: 'string' },
        { name: 'usr05Txt', type: 'string' },

        { name: 'usr06Txt', type: 'string' },
        { name: 'usr07Txt', type: 'string' },
        { name: 'usr08Date', type: 'string' },
        { name: 'usr09Num', type: 'string' },
		
		//date fields
		// qmdt = QMDAT + MZEIT
		// strdt = STRMN + STRUR
		// ltrdt = LTRMN + LTRUR
		// vondt = AUSVN + AUZTV
		// bisdt = AUSBS + AUZTB
		{ name: 'qmdt' },
		{ name: 'strdt' },
		{ name: 'ltrdt' },
		{ name: 'vondt' },
		{ name: 'bisdt'	},
		
		//delta fields
		{ name: 'updFlag',         type: 'string' },
		{ name: 'mobileKey',       type: 'string' },
		{ name: 'mobileKeyOrder',  type: 'string' },

		//references
		{ name: 'notifType',           type: 'auto' },
		{ name: 'codification',    	   type: 'auto' },
		{ name: 'priority',			   type: 'auto' },
		{ name: 'notifActivities',     type: 'auto' },
		{ name: 'notifItems',          type: 'auto' },
		{ name: 'notifTasks',          type: 'auto' },
		{ name: 'order',    		   type: 'auto' },
		{ name: 'equipment',       	   type: 'auto' },
		{ name: 'funcLoc',             type: 'auto' },
		{ name: 'objectStatusList',    type: 'auto' },
		{ name: 'partners',       	   type: 'auto' },
		{ name: 'documents',       	   type: 'auto' },
		{ name: 'localLongtext',       type: 'auto' },
		{ name: 'backendLongtext',     type: 'auto' },
		{ name: 'checklistStatus',     type: 'auto' },
		{ name: 'sdOrders',  		   type: 'auto' },
		
		//notifs does currently not have an own address field, so dependencies come from sub objects
	    { 	name: 'postalCode',
	    	convert: function (notUsed, data) {
	        	return data.getPostalCode.call(data);
	        },
	        depends: ['equipment', 'funcLoc']
	    }
	],
		
	constructor: function(config) {
		this.callParent(arguments);
	},
	
	getPostalCode: function() {
		var retval = '';
		
		try {
			var address = this.getObjectAddress();
			
			if(address) {
				retval = address.get('postCode1');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BaseNotif', ex);
		}
		
		return retval;
	},
	
	getObjectAddress: function() {
		var retval = null;
		
		try {
			var myFuncLoc = this.get('funcLoc');
			
			if(myFuncLoc) {
				retval = myFuncLoc.get('address');
			}
			
			if(!retval) {
				var myEqui = this.get('equipment');
				
				if(myEqui)
					retval = myEqui.get('address');
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getObjectAddress of BaseNotif', ex);
		}
		
		return retval;
	},
	
	getAllSDItems: function() {
		var retval = null;
		
		try {
			var sdOrders = this.get('sdOrders');
		
			if(sdOrders && sdOrders.getCount() > 0) {
		
				var mergedStore = Ext.create('Ext.data.Store', {
					model: 'AssetManagement.customer.model.bo.SDOrderItem',
					autoLoad: false
				});
				
				sdOrders.each(function(sdOrder) {
					var orderBstnk = sdOrder.get('bstnk');
					var ordersSDItems = sdOrder.get('items');
					if(ordersSDItems && ordersSDItems.getCount() > 0) {
						ordersSDItems.each(function(sdOrderItem) {
							sdOrderItem.set('bstnk', orderBstnk);
							mergedStore.add(sdOrderItem);
						}, this);
					}
				}, this);
			}
			
			retval = mergedStore;
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllSDItems of BaseNotif', ex);
		}
		
		return retval;
	},
	
	/**
	 * initilizes the notif object with default values e.g. Date and Time fields set to the current date
	 */
	initNewNotif: function() {
		try { 
		    this.set('qmnum', '');
		    this.set('qmtxt', '');
		    this.set('aufnr', '');
		    this.set('ktext', '');
		    this.set('equnr', '');
		    this.set('eqktx', '');
		    this.set('tplnr', '');
		    this.set('pltxt', '');
		    this.set('qmart', '');
		    this.set('qmartTxt', '');
		    this.set('artpr', '');
		    this.set('priok', '');
		    this.set('prueflos', '');
		    this.set('iwerk', '');
		    this.set('bautl', '');
		    this.set('ingrp', '');
		    this.set('warpl', '');
		    this.set('wapos', '');
		    this.set('wppos', '');
		    this.set('eqfnr', '');
		    this.set('strno', '');
		    this.set('stort', '');
		    this.set('tidnr', '');
		    this.set('msgrp', '');
		    this.set('qmnam', '');
		    this.set('rbnr', '');
		    this.set('gewrk', '');
		    this.set('swerk', '');
		    this.set('flagPlos', '');
		    this.set('stsma', '');
		    this.set('qmkat', '');
		    this.set('qmgrp', '');
		    this.set('qmcod', '');
		    this.set('matnr', '');
		    this.set('auswk', '');
		    this.set('sernr', '');
		    this.set('msaus', '');
		
		    this.set('qmdt', new Date());
		    this.set('strdt', new Date());
		    this.set('ltrdt', new Date());
		    this.set('vondt', new Date());
		    this.set('bisdt', new Date());
		
		    this.set('updFlag', '');
		    this.set('mobileKey', '');
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initNewNotif of BaseNotif', ex);
		}
	}
});