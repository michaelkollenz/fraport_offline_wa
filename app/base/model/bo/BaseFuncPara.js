Ext.define('AssetManagement.base.model.bo.BaseFuncPara', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	mixins: ['Ext.mixin.Observable'],

	inheritableStatics: {
		_instance: null,

		getInstance: function(reset) {
			if(this._instance === null || this._instance === undefined || reset)
				this._instance = Ext.create('AssetManagement.customer.model.bo.FuncPara');

			return this._instance;
		},

		resetInstance: function() {
			this._instance = null;
		}
	},

	constructor: function(config) {
	    try {
			this.callParent(arguments);

		    this.mixins.observable.constructor.call(this, config);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside constructor of BaseFuncPara', ex);
		}
	},

    fields: [
	    //OX_IH_EQUI
	    { name: 'equi_active',      		  	type: 'auto' },
	    { name: 'equi_create',      			type: 'auto' },
	    { name: 'equi_change',    				type: 'auto' },
	    { name: 'equi_chkl_active', 	 		type: 'auto' },
	    { name: 'equi_cl_active',   	    	type: 'auto' },
	    { name: 'equi_mp_active', 				type: 'auto' },
	    { name: 'equi_adr_active', 				type: 'auto' },
	    { name: 'equi_change_position', 		type: 'auto' },
	    { name: 'equi_default_section', 		type: 'auto' },
	    { name: 'equi_docs_active', 			type: 'auto' },
	    { name: 'equi_maps_active', 			type: 'auto' },
	    { name: 'equi_nfhist_active', 			type: 'auto' },
	    { name: 'equi_orhist_active', 			type: 'auto' },
	    { name: 'equi_os_active', 				type: 'auto' },
	    { name: 'equi_partner_active', 			type: 'auto' },
	    { name: 'equi_show_field_address', 		type: 'auto' },
	    { name: 'equi_show_field_loc', 			type: 'auto' },
	    { name: 'equi_show_field_serno', 		type: 'auto' },
	    { name: 'equi_show_field_sort', 		type: 'auto' },
	    { name: 'equi_show_field_tid', 			type: 'auto' },


	    //OX_IH_FLOC
	    { name: 'floc_active', 					type: 'auto' },
	    { name: 'floc_adr_active', 				type: 'auto' },
	    { name: 'floc_change', 					type: 'auto' },
	    { name: 'floc_chkl_active', 			type: 'auto' },
	    { name: 'floc_cl_active', 				type: 'auto' },
	    { name: 'floc_default_section', 		type: 'auto' },
	    { name: 'floc_docs_active', 			type: 'auto' },
	    { name: 'floc_maps_active', 			type: 'auto' },
	    { name: 'floc_mp_active', 				type: 'auto' },
	    { name: 'floc_nfhist_active', 			type: 'auto' },
	    { name: 'floc_orhist_active', 			type: 'auto' },
	    { name: 'floc_os_active', 				type: 'auto' },
	    { name: 'floc_partner_active', 			type: 'auto' },
	    { name: 'floc_show_field_loc', 			type: 'auto' },
	    { name: 'floc_show_field_sort',			type: 'auto' },


	    //OX_IH_LTXT
	    { name: 'c_intv_equi', 					type: 'auto' },
	    { name: 'c_ltxt_equi', 					type: 'auto' },
	    { name: 'c_ltxt_iflo', 					type: 'auto' },
	    { name: 'c_ltxt_notif_activity', 		type: 'auto' },
	    { name: 'c_ltxt_notif_head', 			type: 'auto' },
	    { name: 'c_ltxt_notif_item', 			type: 'auto' },
	    { name: 'c_ltxt_notif_item_activity', 	type: 'auto' },
	    { name: 'c_ltxt_notif_item_cause', 		type: 'auto' },
	    { name: 'c_ltxt_notif_item_task', 		type: 'auto' },
	    { name: 'c_ltxt_notif_task', 			type: 'auto' },
	    { name: 'c_ltxt_operation', 			type: 'auto' },
	    { name: 'c_ltxt_order', 				type: 'auto' },
	    { name: 'c_ltxt_tconf', 				type: 'auto' },
	    { name: 'intv_equi', 					type: 'auto' },
	    { name: 'ltxt_equi', 					type: 'auto' },
	    { name: 'ltxt_iflo', 					type: 'auto' },
	    { name: 'ltxt_notif_activity', 			type: 'auto' },
	    { name: 'ltxt_notif_head', 				type: 'auto' },
	    { name: 'ltxt_notif_item', 				type: 'auto' },
	    { name: 'ltxt_notif_item_activity', 	type: 'auto' },
		{ name: 'ltxt_notif_item_cause', 		type: 'auto' },
		{ name: 'ltxt_notif_item_task', 		type: 'auto' },
		{ name: 'ltxt_notif_task', 				type: 'auto' },
		{ name: 'ltxt_operation', 				type: 'auto' },
		{ name: 'ltxt_order', 					type: 'auto' },
        { name: 'intv_equi',                    type: 'auto' },

		//OX_IH_MAT
		{ name: 'banf_active', 					type: 'auto' },
		{ name: 'consi_active', 				type: 'auto' },
		{ name: 'material_active', 				type: 'auto' },
		{ name: 'mat_stl_active', 				type: 'auto' },
		{ name: 'nonstock_mat_active', 				type: 'auto' },

		//OX_IH_MCNF
		{ name: 'matconf_active', 				type: 'auto' },
		{ name: 'matconf_unit', 				type: 'auto' },

		//OX_IH_NOT
		{ name: 'coding_active', 				type: 'auto' },
		{ name: 'default_qmart', 				type: 'auto' },
		{ name: 'head_activity_active', 		type: 'auto' },
		{ name: 'head_activity_change', 		type: 'auto' },
		{ name: 'head_activity_create', 		type: 'auto' },
		{ name: 'head_item_active', 			type: 'auto' },
		{ name: 'head_item_create', 			type: 'auto' },
		{ name: 'head_item_change', 			type: 'auto' },
		{ name: 'head_item_obj_active', 		type: 'auto' },
		{ name: 'head_item_obj_change', 		type: 'auto' },
		{ name: 'head_task_active', 			type: 'auto' },
		{ name: 'head_task_change', 			type: 'auto' },
		{ name: 'head_task_create', 			type: 'auto' },
		{ name: 'item_activity_active', 		type: 'auto' },
		{ name: 'item_activity_change', 		type: 'auto' },
		{ name: 'item_activity_create', 		type: 'auto' },
		{ name: 'item_cause_active', 			type: 'auto' },
		{ name: 'item_cause_create', 			type: 'auto' },
		{ name: 'item_cause_change', 			type: 'auto' },
		{ name: 'item_task_active', 			type: 'auto' },
		{ name: 'item_task_change', 			type: 'auto' },
		{ name: 'item_task_create', 			type: 'auto' },
		{ name: 'notif_default_section', 		type: 'auto' },
		{ name: 'notif_documents_active', 		type: 'auto' },
		{ name: 'notif_partner_active', 		type: 'auto' },
		{ name: 'notif_show_field_objaddress', 	type: 'auto' },
		{ name: 'notif_show_field_sysstat', 	type: 'auto' },
		{ name: 'notif_show_field_userstat', 	type: 'auto' },
		{ name: 'noti_active', 					type: 'auto' },
		{ name: 'noti_change', 					type: 'auto' },
		{ name: 'noti_create', 					type: 'auto' },
		{ name: 'noti_cam_active', 				type: 'auto' },
		{ name: 'noti_show_field_ausbs', 		type: 'auto' },
		{ name: 'noti_show_field_ausvn', 		type: 'auto' },
		{ name: 'noti_show_field_ltrmn', 		type: 'auto' },
		{ name: 'noti_show_field_qmdat', 		type: 'auto' },
		{ name: 'noti_show_field_strmn', 		type: 'auto' },

		//OX_IH_ORD
		{ name: 'calendar_active', 				type: 'auto' },
		{ name: 'calendar_change', 				type: 'auto' },
		{ name: 'default_auart', 				type: 'auto' },
		{ name: 'default_steus', 				type: 'auto' },
		{ name: 'def_pmact_type', 				type: 'auto' },
		{ name: 'oper_change_active', 			type: 'auto' },
		{ name: 'oper_create_active', 			type: 'auto' },
		{ name: 'oper_default_time', 			type: 'auto' },
		{ name: 'ord_active', 					type: 'auto' },
		{ name: 'ord_change', 					type: 'auto' },
		{ name: 'ord_create', 					type: 'auto' },
		{ name: 'ord_cam_active', 				type: 'auto' },
		{ name: 'ord_comp_active', 				type: 'auto' },
		{ name: 'ord_comp_create', 				type: 'auto' },
		{ name: 'ord_default_section', 			type: 'auto' },
		{ name: 'ord_documents_active', 		type: 'auto' },
		{ name: 'ord_documents_create', 		type: 'auto' },
		{ name: 'ord_flatcharge_active', 		type: 'auto' },
		{ name: 'ord_lumpsums_active', 			type: 'auto' },
		{ name: 'ord_mconf_cconf', 				type: 'auto' },
		{ name: 'ord_notif_active', 			type: 'auto' },
		{ name: 'ord_obj_list_active',          type: 'auto' },
		{ name: 'ord_obj_list_change',          type: 'auto' },
		{ name: 'ord_obj_list_cl_active', 		type: 'auto' },
		{ name: 'ord_oper_cconf', 				type: 'auto' },
		{ name: 'ord_partner_active', 			type: 'auto' },
		{ name: 'ord_pdf_rep_active', 			type: 'auto' },
		{ name: 'ord_readmarker_active', 		type: 'auto' },
		{ name: 'ord_show_field_userstat', 		type: 'auto' },
		{ name: 'ord_show_field_vaplz', 		type: 'auto' },
		{ name: 'ord_show_field_wawrk', 		type: 'auto' },
		{ name: 'ord_split_active', 			type: 'auto' },
		{ name: 'ord_status_panel_active', 		type: 'auto' },
		{ name: 'ord_techc_active', 			type: 'auto' },
		{ name: 'ord_use_operlist', 			type: 'auto' },
		{ name: 'pdf_rep_active', 				type: 'auto' },
		{ name: 'priority_active', 				type: 'auto' },
		{ name: 'priority_change', 				type: 'auto' },
		{ name: 'schedule_active',              type: 'auto' },
        { name: 'integrated_conf',              type: 'auto' },
        { name: 'work_exp_group',               type: 'auto'},
        { name: 'general_exp_group',            type: 'auto'},

		//OX_IH_ORDF -> Filter Auftragsliste muss noch implementiert werden

		//OX_IH_PART
		{ name: 'partner_active', 				type: 'auto' },
		{ name: 'partner_change', 				type: 'auto' },

		//OX_IH_STAT
		{ name: 'equi_stat_active', 			type: 'auto' },
		{ name: 'equi_stat_change', 			type: 'auto' },
		{ name: 'floc_stat_active', 			type: 'auto' },
		{ name: 'floc_stat_change', 			type: 'auto' },
		{ name: 'notif_head_stat_active', 		type: 'auto' },
		{ name: 'notif_head_stat_change', 		type: 'auto' },
		{ name: 'notif_task_stat_active', 		type: 'auto' },
		{ name: 'notif_task_stat_change', 		type: 'auto' },
		{ name: 'oper_stat_active', 			type: 'auto' },
		{ name: 'oper_stat_change', 			type: 'auto' },
		{ name: 'order_stat_active', 			type: 'auto' },
		{ name: 'order_stat_change', 			type: 'auto' },
		{ name: 'ord_stat_mo_active', 			type: 'auto' },
		{ name: 'ord_stat_mo_change', 			type: 'auto' },
		{ name: 'ord_stat_oo_active', 			type: 'auto' },
		{ name: 'ord_stat_oo_change', 			type: 'auto' },

		//OX_IH_SYS
		{ name: 'attach_active',                type: 'auto' },
        { name: 'attach_max_file_size',         type: 'auto' },
        { name: 'attach_max_img_res',           type: 'auto' },
        { name: 'date_display_format',          type: 'auto' },
        { name: 'time_display_format',          type: 'auto' },
        { name: 'display_timezones',         	type: 'auto' },
		{ name: 'file_folder_ce', 				type: 'auto' },
		{ name: 'file_folder_w32', 				type: 'auto' },
		{ name: 'last_sync_to_be', 				type: 'auto' },
		{ name: 'message_display', 				type: 'auto' },
		{ name: 'number_format', 				type: 'auto' },
		{ name: 'residence_time', 				type: 'auto' },
		{ name: 'rfid_active', 					type: 'auto' },
		{ name: 'rfid_portname_ce', 			type: 'auto' },
		{ name: 'rfid_portname_w3', 			type: 'auto' },
		{ name: 'rfid_porttype_ce', 			type: 'auto' },
		{ name: 'rfid_porttype_w3', 			type: 'auto' },
		{ name: 'rfid_timeout', 				type: 'auto' },
		{ name: 'rfid_timeout_drc', 			type: 'auto' },
		{ name: 'rfid_timeout_drw', 			type: 'auto' },
		{ name: 'rfid_write', 					type: 'auto' },
		{ name: 'rfid_write_verif', 			type: 'auto' },
		{ name: 'scan_scenario', 				type: 'auto' },
		{ name: 'settings_active', 				type: 'auto' },
		{ name: 'statusmessage_ti', 			type: 'auto' },
		{ name: 'support_active', 				type: 'auto' },
		{ name: 'svm_temp_dir_ce', 				type: 'auto' },
		{ name: 'svm_temp_dir_w32', 			type: 'auto' },
		{ name: 'trace_filesize', 				type: 'auto' },
		{ name: 'logout_timeout',               type: 'auto' },

		//OX_IH_TCNF
		{ name: 'conf_account_active', 			type: 'auto' },
		{ name: 'conf_rep_active', 				type: 'auto' },
		{ name: 'default_timeunit', 			type: 'auto' },
		{ name: 'keyfigure_act', 				type: 'auto' },
		{ name: 'tconf_overview_expiration', 	type: 'auto' },
		{ name: 'tc_travel_active', 			type: 'auto' },
		{ name: 'timeconf_active', 				type: 'auto' },
		{ name: 'timer_active', 				type: 'auto' },
		{ name: 'timer_max_duration', 			type: 'auto' },
		{ name: 'timer_use_pernr', 				type: 'auto' },
		{ name: 'tncf_bemot_field', 			type: 'auto' },
		{ name: 'tncf_dial_int', 				type: 'auto' },
		{ name: 'tncf_grund_field', 			type: 'auto' },
		{ name: 'tncf_iedd_field', 				type: 'auto' },
		{ name: 'tncf_isdd_field', 				type: 'auto' },
		{ name: 'tncf_longtxt_field', 			type: 'auto' },
		{ name: 'tncf_pernr_field', 			type: 'auto' },
		{ name: 'tncf_wegz_field', 				type: 'auto' },

		//OX_INV
		{ name: 'inventory_active', 			type: 'auto' },
		{ name: 'equi_inv_active', 				type: 'auto' },

		//OX_CLASS
		{ name: 'class_change', 				type: 'auto' },

		//OX_IH_CAL
		{ name: 'calendar_color_blue', 			type: 'auto' },
		{ name: 'calendar_color_green', 		type: 'auto' },
		{ name: 'calendar_color_order', 		type: 'auto' },
		{ name: 'calendar_color_read', 			type: 'auto' },
		{ name: 'e0001', 						type: 'auto' },
		{ name: 'e0002', 						type: 'auto' },
		{ name: 'e0003', 						type: 'auto' },
		{ name: 'ox_cal_active', 				type: 'auto' },

		//OX_IH_CL
		{ name: 'create_positions', 			type: 'auto' },
		{ name: 'quant_btn_visible', 			type: 'auto' },
		{ name: 'report_active', 				type: 'auto' },
		{ name: 'chklactive', 					type: 'auto' },

		//OX_IH_USER
		{ name: 'use_coworkers', 				type: 'auto' },

		//OX_PUSH
		{ name: 'push_active', 					type: 'auto' },

		//OX_IH_CATS
		{ name: 'cats_active', 					type: 'auto' },


      //OX_WM
      { name: 'ia_active', 					type: 'auto' },
      { name: 'mm_active', 					type: 'auto' },
      { name: 'we_active', 					type: 'auto' },
      { name: 'wa_active', 					type: 'auto' },
      { name: 'bst_active', 					type: 'auto' },


      //Z_EXT_CONF
		{ name: 'ext_conf_material', 			type: 'auto' },

		//Z_IH_BANF
		{ name: 'sakto', 						type: 'auto' },
		{ name: 'matkl', 						type: 'auto' },

		//Z_IH_BANF
		{ name: 'ext_scen_active',              type: 'auto' },

		//control field
		{ name: 'isInitialized', type: 'boolean', defaultValue: false }
	],

	setInitialized: function(initialized) {
	    try {
    	    this.set('isInitialized', initialized);

    	    this.fireEvent('initialized', initialized);
        } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setInitialized of BaseFuncPara', ex);
	    }
	},
	getValue1For: function (paramName) {
	    var retval = '';
	    try {
	        var param = this.get(paramName);
	        if (this.get(paramName))
	            retval = param.get('fieldval1');
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getValue1For of BaseFuncPara', ex);
	    }
	    return retval;
	},
	getValue2For: function (paramName) {
	    var retval = '';
	    try {
	        var param = this.get(paramName);
	        if (this.get(paramName))
	            retval = param.get('fieldval2');
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getValue2For of BaseFuncPara', ex);
	    }
	    return retval;
	}
});