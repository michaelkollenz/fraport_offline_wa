Ext.define('AssetManagement.base.model.bo.BasePartnerAP', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
	   'AssetManagement.customer.utils.StringUtils'
	],

	fields: [
		//key fields
		{ name: 'parnr',        type: 'string' },
	
	    //ordinary fields
		{ name: 'kunnr',        type: 'string' },
		{ name: 'lastname',     type: 'string' },
		{ name: 'firstname',    type:'string' },
		{ name: 'titlep',       type:'string' },
		{ name: 'city',         type:  'string' },
		{ name: 'postlcod1',    type: 'string' },
		{ name: 'street',       type:  'string' },
		{ name: 'telnumbermob', type:'string' },
		{ name: 'tel1numbr',    type: 'string' },
		{ name: 'faxnumber',    type:'string' },
		{ name: 'functionVal',  type:'string' },
		{ name: 'email',        type:'string' },
		{ name: 'telextens',    type: 'string' },
		{ name: 'faxextens',    type:'string' },
            	{ name: 'time_zone',    type:'string' },
            	{ name: 'str_suppl1',    type:'string' },
			
	    //delta fields
		{ name: 'updFlag',   type:  'string' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	},
	
    getName: function() {
    	var retval = '';
	    
        try {
            retval = AssetManagement.customer.utils.StringUtils.concatenate([ this.get('firstname'), this.get('lastname') ], null, true);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getName of BasePartnerAP', ex);
        }  

        return retval;
	},
	
	getStreet: function() {
		var retval = '';
	    
        try {
            retval = this.get('street');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStreet of BasePartnerAP', ex);
        }  

        return retval;
	},
	
	getHouseNo: function() {
		return '';
	},
	
	getCity: function() {
		var retval = '';
	    
        try {
            retval = this.get('city');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCity of BasePartnerAP', ex);
        }  

        return retval;
	},
	
	getPostalCode: function() {
		var retval = '';
	    
        try {
            retval = this.get('postcode1');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BasePartnerAP', ex);
        }  

        return retval;
	}
});