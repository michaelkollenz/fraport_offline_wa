Ext.define('AssetManagement.base.model.bo.BaseCust_013', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'werks',           type:'string' },
	    { name: 'katalogart',      type: 'string' },
	    { name: 'auswahlmge',      type:'string' },
	    { name: 'funktion',        type: 'string' },
	    
        //ordinary fields
        { name: 'codegruppe' ,    type:'string' },
		{ name: 'code',           type: 'string' },
		
        //delta fields
		{ name: 'updFlag',        type:'string' }
	],
	

	constructor : function(config) {
		this.callParent(arguments);
	}
});