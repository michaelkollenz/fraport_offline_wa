Ext.define('AssetManagement.base.model.bo.BaseObjectStatus', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
		//key fields	 
		{ name: 'objnr' ,           type:   'string' },
		{ name: 'stat' ,            type:   'string' },
			
	    //ordinary fields
	    { name: 'chgnr',            type:  'string' },
		{ name: 'inact',            type:  'string' },
		{ name: 'changed',          type:  'string' },
		{ name: 'txt04',            type:  'string' },
		{ name: 'txt30',            type:  'string' },
		{ name: 'status_int',       type:  'string' },
		{ name: 'user_status_code', type:  'string' },
		{ name: 'user_status_desc', type:  'string' },
					
	    //delta fields
		{ name: 'updFlag',          type: 'string' },
	    { name: 'mobileKey',        type: 'string' },
	    { name: 'childKey',         type: 'string' },
	    { name: 'childKey2',        type: 'string' }			
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});