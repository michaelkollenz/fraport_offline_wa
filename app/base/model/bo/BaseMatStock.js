Ext.define('AssetManagement.base.model.bo.BaseMatStock', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields	 
	    { name: 'matnr' ,       type:   'string' },
	    { name: 'werks',        type:   'string' },
		{ name: 'lgort' ,       type:   'string' },
	
        //ordinary fields
		{ name: 'meins',        type:   'string' },
		
	    { name: 'labst' ,       type:   'string' },
		{ name: 'sernp',        type:   'string' },
		{ name: 'serpflicht',   type:   'string' },
		{ name: 'speme',  type:  'string' },
		
        //delta fields
		{ name: 'updFlag',      type: 'string' },
		
		//references
		{ name: 'material',     type: 'auto' }
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});