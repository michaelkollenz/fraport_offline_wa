Ext.define('AssetManagement.base.model.bo.BasePartner', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	requires: [
	   'AssetManagement.customer.utils.StringUtils'
	],

	fields: [
		//key fields
		{ name: 'objnr',       type:'string' },
		{ name: 'parvw',       type: 'string' },
		{ name: 'counter',     type: 'string' },

        //ordinary fields
		{ name: 'parnr',      type: 'string' },
		{ name: 'partnertype',type:'string' },
		{ name: 'adrnr',      type: 'string' },
		{ name: 'name1',      type: 'string' },
		{ name: 'name2',      type:  'string' },
		{ name: 'postcode1',  type:'string' },
		{ name: 'city1',      type:  'string' },
		{ name: 'street',     type:'string' },
		{ name: 'country',    type: 'string' },
		{ name: 'region',     type:'string' },
		{ name: 'telnumber',  type:'string' },
		{ name: 'faxnumber',  type:'string' },
		{ name: 'name3',      type: 'string' },
		{ name: 'name4',      type: 'string' },
		{ name: 'city2',      type:'string' },
		{ name: 'housenum1',  type:'string' },
		{ name: 'telextens',  type: 'string' },
		{ name: 'faxextens',  type: 'string' },
		{ name: 'aufnr',      type: 'string' },
		{ name: 'qmnum',      type:  'string' },
	    { name: 'equnr',      type: 'string' },
	    { name: 'tplnr',      type:  'string' },
	    { name: 'telnumbermob',      type:  'string' },
	    { name: 'email',      type: 'string' },
	    { name: 'timeZone',	  type:'string'},
        { name: 'str_suppl1',  type:'string'},
		{ name: 'title',  type:'string'},
		
        //delta fields
		{ name: 'updFlag',     type: 'string' },
	    { name: 'mobilekey',   type: 'string' } ,
	    
		//references
		{ name: 'address',        type: 'auto' }
    ],
    
    constructor: function(config) {
        this.callParent(arguments);
    },
    
    getName: function() {    
    	var retval = '';
	    
        try {
            retval = AssetManagement.customer.utils.StringUtils.concatenate([ this.get('name1'), this.get('name2'), this.get('name3'), this.get('name4') ], null, true);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getName of BasePartner', ex);
        }  

        return retval;	
	},
	
	getStreet: function() {
		var retval = '';
	    
        try {
            retval = this.get('street');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStreet of BasePartner', ex);
        }  

        return retval;
	},
	
	getHouseNo: function() {
		var retval = '';
	    
        try {
            retval = this.get('housenum1');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getHouseNo of BasePartner', ex);
        }  

        return retval;
	},
	
	getCity: function() {
		var retval = '';
	    
        try {
            retval = AssetManagement.customer.utils.StringUtils.concatenate([ this.get('city1'), this.get('city2') ], null, true);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCity of BasePartner', ex);
        }  

        return retval;
	},
	
	getPostalCode: function() {
		var retval = '';
	    
        try {
            retval = this.get('postcode1');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BasePartner', ex);
        }  

        return retval;
    },

    //override
    //public
    getRelevantTimeZone: function(){
        var retval = '';

        try {
            retval = this.get('timeZone');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelevantTimeZone of Partner', ex);
        }

        return retval;
    }
});