Ext.define('AssetManagement.base.model.bo.BaseMeasurementDoc', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields 
	    { name: 'point',        type:   'string' },
	    { name: 'mdocm',        type:   'string' },
	   
        //ordinary fields
        { name: 'mdtext',        type: 'string' },
		{ name: 'readr',         type:  'string' },
		{ name: 'recdc',         type:  'string' },
		{ name: 'unitr',         type:  'string' },
        { name: 'codct',         type:  'string' },
        { name: 'codgr',         type:  'string' },
        { name: 'vlcod',         type:  'string' },
		
        //date fields
		{ name:  'idate',        type: 'date' },
		{ name:  'itime',        type: 'date' },

        //delta fields
		{ name: 'updFlag',      type:  'string' }
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});