Ext.define('AssetManagement.base.model.bo.BaseCust_008', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    //{ name: 'mobileuser', type: 'string' },
	    { name: 'qpart',          type: 'string' },
	    
	    //ordinary fields
        { name: 'qkatartqual' ,   type: 'string' },
		{ name: 'nove',           type: 'string' },
		{ name: 'qkatartve' ,     type: 'string' },
		{ name: 'qvmenge' ,       type: 'string' },
		{ name: 'codegrve',       type: 'string' },
		{ name: 'codeve' ,        type: 'string' },
		{ name: 'codegrvefehler', type: 'string' },
		{ name: 'codevefehler' ,  type: 'string' },
		{ name: 'plosref',        type: 'string' },
		
        //delta fields
		{ name: 'updFlag',        type: 'string' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});