Ext.define('AssetManagement.base.model.bo.BaseCustomer', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
	    'AssetManagement.customer.utils.StringUtils',
	    'AssetManagement.customer.helper.OxLogger'
	],
	
	fields: [
		//key fields
		{ name: 'kunnr',        type: 'string' },
		{ name: 'vkorg',        type: 'string' },
		{ name: 'vtweg',        type: 'string' },
		{ name: 'spart',        type: 'string' },
		   
	    //ordinary fields 
	    { name: 'kdgrp',        type: 'string' },
		{ name: 'bzirk',        type: 'string' },
		{ name: 'vkgrp',        type: 'string' },
		{ name: 'vkbur',        type: 'string' },
		{ name: 'kvgr1',        type: 'string' },
		{ name: 'kvgr2',        type: 'string' },
		{ name: 'kvgr3',        type: 'string' },
		{ name: 'kvgr4',        type: 'string' },
		{ name: 'kvgr5',        type: 'string' },
		{ name: 'titlep',       type: 'string' },
		{ name: 'firstname',    type: 'string' },
		{ name: 'lastname',     type: 'string' },
		{ name: 'city',         type: 'string' },
		{ name: 'postlcod1',    type: 'string' },
		{ name: 'street',       type: 'string' },
		{ name: 'telnumbermob', type: 'string' },
		{ name: 'tel1numbr',    type: 'string' },
		{ name: 'faxnumber',    type: 'string' },
		{ name: 'funktion',     type: 'string' },
		{ name: 'email',        type: 'string' },
		{ name: 'telextens',    type: 'string' },
		{ name: 'faxextens',    type: 'string' },
        { name: 'user1',        type: 'string' },
		{ name: 'user2',        type: 'string' },
		{ name: 'timeZone',     type: 'string' },
            { name: 'str_suppl1',     type: 'string' },
	
	    //delta fields
		{ name: 'updFlag',      type: 'string' }
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	},
	
	getFullName: function() {
		var retval = '';
	
		try {
			retval = AssetManagement.customer.utils.StringUtils.concatenate([ this.get('titlep'), this.get('firstname'), this.get('lastname') ], ' ', true);
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getFullName of BaseCustomer', ex);
		}
		
		return retval;
	},

	getName: function () {
	    var retval = '';

	    try {
	        retval = this.getFullName();
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getName of BaseCustomer', ex);
	    }

	    return retval;
	},

	getStreet: function () {
	    var retval = '';

	    try {
	        retval = this.get('street');
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStreet of BaseCustomer', ex);
	    }

	    return retval;
	},

	getHouseNo: function () {
	    return '';
	},

	getCity: function () {
	    var retval = '';

	    try {
	        retval = this.get('city');
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCity of BaseCustomer', ex);
	    }

	    return retval;
	},

	getPostalCode: function () {
	    var retval = '';

	    try {
	        retval = this.get('postlcod1');
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getPostalCode of BasePartner', ex);
	    }

	    return retval;
	},

    //override
    //public
    getRelevantTimeZone: function(){
        var retval = '';

        try {
            retval = this.get('timeZone');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelevantTimeZone of Customer', ex);
        }

        return retval;
    }
});