Ext.define('AssetManagement.base.model.bo.BaseQPResult', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'insplot',       type: 'string' },
	    { name: 'inspoper',       type: 'string' },
	    { name: 'inspchar',       type: 'string' },
	    { name: 'equnr' ,        type: 'string' },
		{ name: 'tplnr',         type: 'string' },
			
        //ordinary fields
		{ name: 'closed',       type: 'string' },
		{ name: 'codegrp1' ,    type: 'string' },
		{ name: 'codegrp2',     type: 'string' },
		{ name: 'code1' ,       type: 'string' },
		{ name: 'code2',        type: 'string' },	
		{ name: 'inspector',    type: 'string' },	
		{ name: 'meanValue' ,   type: 'string' },
		{ name: 'qmnum',        type: 'string' },
		{ name: 'remark' ,      type: 'string' },
		{ name: 'evaluation', type: 'string' },


		{ name: 'matnr', type: 'string' },
		{ name: 'werks', type: 'string' },
		{ name: 'plnty', type: 'string' },
		{ name: 'plnnr', type: 'string' },
		{ name: 'plnal', type: 'string' },
		{ name: 'zkriz', type: 'string' },
		{ name: 'zaehl', type: 'string' },
		{ name: 'datuv', type: 'string' },
		{ name: 'char_attr', type: 'string' },
		{ name: 'usr01flag', type: 'string' },
		{ name: 'usr02flag', type: 'string' },
		{ name: 'usr03code', type: 'string' },
		{ name: 'usr04code', type: 'string' },
		{ name: 'usr05txt', type: 'string' },
		{ name: 'usr06txt', type: 'string' },
		{ name: 'usr07txt', type: 'string' },
		{ name: 'usr08date', type: 'string' },
		{ name: 'usr09num', type: 'string' },
			
		//date fields
	    { name:  'endDate' } ,
	    { name:  'endTime'  } ,
	    { name:  'startDate' } ,
	    { name:  'startTime' },
			
        //delta fields
		{ name: 'updFlag',     type: 'string' },
		{ name: 'mobileKey', type: 'string' },
    	{ name: 'childKey', type: 'string' },
    	{ name: 'childKey_char', type: 'string' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});
