Ext.define('AssetManagement.base.model.bo.BaseTpst', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
 
	fields: [
		//key fields
		{ name: 'tplnr',      type:'string' },
		{ name: 'werks',      type:'string' },
		{ name: 'stlan',      type:'string' },
		{ name: 'stlnr',      type:'string' },
		
        //ordinary fields
		{ name: 'stlal',      type:'string' },
	
        //delta fields
		{ name: 'updFlag',    type:'string' }		
	],
	
	constructor: function(config) {
		this.callParent(arguments);
	}
});