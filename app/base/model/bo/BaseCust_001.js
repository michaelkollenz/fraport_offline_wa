Ext.define('AssetManagement.base.model.bo.BaseCust_001', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [  
	    //key fields
	    { name: 'mobileuser',          		type: 'string' },
	    
	    //ordinary fields
        { name: 'order_scenario' ,      	type: 'string' },
		{ name: 'order_workcenter',     	type: 'string' },
		{ name: 'order_plant_wrkc' ,     	type: 'string' },
		{ name: 'order_pernr',          	type: 'string' },
		{ name: 'order_plangroup' ,     	type: 'string' },
		{ name: 'order_plan_plant',      	type: 'string' },
		{ name: 'order_par_role' ,       	type: 'string' },
		{ name: 'order_partner_no',      	type: 'string' },
		{ name: 'notif_scenario' ,      	type: 'string' },
		{ name: 'notif_workcenter',     	type: 'string' },
		{ name: 'notif_plant_wrkc',    		type: 'string' },
		{ name: 'notif_plangroup',     		type: 'string' },
		{ name: 'notif_plan_plant',    		type: 'string' },
		{ name: 'notif_pernr',     	   		type: 'string' },
		{ name: 'notif_par_role',      		type: 'string' },
		{ name: 'notif_partner_no',    		type: 'string' },
		{ name: 'vari_tpl',             	type: 'string' },
		{ name: 'change_tpl',           	type: 'string' },
		{ name: 'vari_equi',            	type: 'string' },
		{ name: 'change_equi' ,         	type: 'string' },
		{ name: 'lartdefault',           	type: 'string' },
		{ name: 'vari_class',           	type: 'string' },
		{ name: 'sprache' ,            		type: 'string' },
		{ name: 'intern' ,             		type: 'string' },
		{ name: 'ekorg',               		type: 'string' },
		{ name: 'ekgrp',               		type: 'string' },
		{ name: 'arbplktxt',           		type: 'string' },
		{ name: 'keyfigure_kokrs',      	type: 'string' },
		{ name: 'keyfigure_grp',        	type: 'string' },
		{ name: 'vari_workc',       		type: 'string' },
		{ name: 'vari_custom',        		type: 'string' },
		{ name: 'vkorg',               		type: 'string' },
		{ name: 'vtweg',               		type: 'string' },
		{ name: 'spart',               		type: 'string' },
		{ name: 'vkgrp',               		type: 'string' },
		{ name: 'vkbur',               		type: 'string' },
		{ name: 'kschl',               		type: 'string' },
		{ name: 'vari_vendor',         		type: 'string' },
		{ name: 'kunnr_consi',         		type: 'string' },
		{ name: 'kunnr_ag',            		type: 'string' },
		{ name: 'kunnr_we',            		type: 'string' },
		{ name: 'lifnr',            		type: 'string' },
        { name: 'vari_mat',            		type: 'string' },
        { name: 'tzone',            		type: 'string' },
        { name: 'waers',            		type: 'string' },
        { name: 'quot_allowed',            	type: 'string' },
		{ name: 'lartDefault',            	type: 'string' },
		{ name: 'kschl',            		type: 'string' },
		{ name: 'kunnr_consi',            	type: 'string' },
        

		//delta fields
		{ name: 'updflag',            	 	type: 'string' }
	],
	

	constructor : function(config) {
		this.callParent(arguments);
	}
});