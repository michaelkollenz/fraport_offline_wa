Ext.define('AssetManagement.base.model.bo.BaseSDOrderItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	requires: [
	    'AssetManagement.customer.helper.OxLogger'
    ],

	fields: [
		//key fields
		{ name: 'vbeln', type: 'string' },
		{ name: 'posnr', type: 'string' },
		
		//ordinary fields
		{ name: 'matnr', type: 'string' },
		{ name: 'zmeng', type: 'string' },
		{ name: 'zieme', type: 'string' },
		{ name: 'arktx', type: 'string' },
		{ name: 'abgru', type: 'string' },
		{ name: 'lfgsa', type: 'string' },
	
		//delta fields
		{ name: 'updFlag',   type: 'string' },
		{ name: 'mobileKey', type: 'string' },
		{ name: 'childKey', type: 'string' },
		{ name: 'usr1', type: 'string' },
		{ name: 'usr2', type: 'string' },
		
		//references
		{ name: 'material',        	type: 'auto' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	},
	
	getDeliveryStatus: function() {
		var retval = '';
		
		try {
			switch(this.get('lfgsa')) {
				case 'A':
					retval = Locale.getMsg('notDelivered');
					break;
				case 'B':
					retval = Locale.getMsg('partiallyDelivered');
					break;
				case 'C':
					retval = Locale.getMsg('fullyDelivered');
					break;
				default:
					break;
			}
		} catch(ex) {
			AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getDeliveryStatus of BaseSDOrderItem', ex)
		}
		
		return retval;
	}
});