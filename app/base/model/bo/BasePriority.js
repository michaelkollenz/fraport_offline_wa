Ext.define('AssetManagement.base.model.bo.BasePriority', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'artpr',        type:'string' },
		{ name: 'priok',        type:'string' },
	
	    //ordinary fields
		{ name: 'priokx',       type:'string' },
			
	    //delta fields
		{ name: 'updFlag',      type: 'string' } 		
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});