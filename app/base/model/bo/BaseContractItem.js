Ext.define('AssetManagement.base.model.bo.BaseContractItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'vbeln',    type: 'string' },
	    { name: 'posnr',    type: 'string' },
	   
	    //ordinary fields
        { name: 'matnr' ,   type: 'string' },
		{ name: 'arktx',    type: 'string' },
		{ name: 'pstyv' ,   type: 'string' },
		{ name: 'obknr',    type: 'string' },
		{ name: 'auart' ,   type: 'string' },
		{ name: 'vkorg',    type: 'string' },
		{ name: 'vtweg' ,   type: 'string' },
		{ name: 'spart',    type: 'string' },
		{ name: 'ktext',    type: 'string' },
		{ name: 'bstnk',    type: 'string' },
		
		 //delta fields
		{ name: 'updFlag',  type: 'string' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});