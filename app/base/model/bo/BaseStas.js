Ext.define('AssetManagement.base.model.bo.BaseStas', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
		//key fields
		{ name: 'stlty',      type:  'string' },
		{ name: 'stlnr',      type:  'string' },
		{ name: 'stlal',      type:   'string' },
		{ name: 'stlkn',      type:  'string' },
		{ name: 'stasz',      type:  'string' },
		
        //delta fields
		{ name: 'updFlag',    type:  'string' }
	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});