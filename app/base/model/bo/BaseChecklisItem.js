Ext.define('AssetManagement.base.model.bo.BaseChecklistItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields
	    { name: 'plnty',        type:  'string' },
	    { name: 'plnnr',        type:  'string' },
	    { name: 'plnkn',        type:   'string' },
	    { name: 'kzeinstell',   type:  'string' },
		{ name: 'merknr' ,      type:  'string' },
		{ name: 'zaehl',        type:  'string' },
		
        //ordinary fields
        { name: 'vornr' ,       type: 'string' },
		{ name: 'vorltx',       type: 'string' },
		{ name: 'headzaehl' ,   type: 'string' },
		{ name: 'operzaehl',    type:  'string' },
		{ name: 'gueltigab' ,   type: 'string' },
		{ name: 'kurztext',     type:  'string' },
		{ name: 'katab1' ,      type:  'string' },
		{ name: 'katalgart1',   type: 'string' },
		{ name: 'auswmenge1' ,  type: 'string' },
		{ name: 'auswmgwrk2',   type:  'string' },
		{ name: 'sollwert',     type: 'string' },
		{ name: 'toleranzob' ,  type: 'string' },
		{ name: 'toleranzun',   type: 'string' },
		{ name: 'masseinhsw' ,  type: 'string' },
		{ name: 'chartype',     type: 'string' },
		{ name: 'werks' ,       type: 'string' },
		{ name: 'headktext',    type: 'string' },
		{ name: 'stellen' ,     type: 'string' },
		{ name: 'longtext',     type: 'string' },
		
		//Value comes from BaseCharact
        { name: 'atfor',        type: 'string' },
		
        //delta fields
		{ name: 'updFlag',      type: 'string' }

	],

	constructor : function(config) {
		this.callParent(arguments);
	}
});
