Ext.define('AssetManagement.base.model.bo.BasePushMessage', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
  
	fields: [
		//key fields
		{ name: 'messageid',     type: 'string' },
		
        //ordinary fields
		{ name: 'message',       type:'string' },
		
        //delta fields
		{ name: 'updFlag',      type: 'string' },
		 
		//date fields
		{ name: 'datetime' }
	],
	
	constructor : function(config) {
		this.callParent(arguments);
	}
});