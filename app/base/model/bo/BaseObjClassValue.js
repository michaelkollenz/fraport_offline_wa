Ext.define('AssetManagement.base.model.bo.BaseObjClassValue', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields
	    { name: 'objnr',     type: 'string' },
	    { name: 'atinn',     type: 'string' },
	    { name: 'atzhl',     type: 'string' },
	    { name: 'mafid',     type: 'string' },
	    { name: 'adzhl',     type: 'string' },
	     
        //ordinary field
        { name: 'clint',     type: 'string' },
		{ name: 'klart',     type: 'string' },
		{ name: 'atwrt',     type: 'string' },
		{ name: 'atflv',     type: 'string' },
		{ name: 'atawe',     type: 'string' },
		{ name: 'atflb',     type: 'string' },
		{ name: 'ataw1',     type: 'string' },
		{ name: 'atcod',     type: 'string' },
		{ name: 'attlv',     type: 'string' },
		{ name: 'attlb',     type: 'string' },
		
        //delta fields
		{ name: 'updFlag',    type: 'string' },
        { name: 'mobileKey',   type: 'string' }
	],
		
    constructor: function(config) {
        this.callParent(arguments);
	}
});