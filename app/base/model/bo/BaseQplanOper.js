Ext.define('AssetManagement.base.model.bo.BaseQPlanOper', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

	fields: [
	    //key fields
	    { name: 'plnty',        type:   'string' },
	    { name: 'plnnr',        type:   'string' },
		{ name: 'plnal' ,       type:   'string' },
		{ name: 'plnkn' ,       type:   'string' },
		{ name: 'zaehl',        type:   'string' },

        //ordinary fields
        { name: 'vornr' ,       type:   'string' },
        { name: 'ltxa1' ,       type:   'string' }
	],

	constructor: function(config) {
		this.callParent(arguments);
	}
});