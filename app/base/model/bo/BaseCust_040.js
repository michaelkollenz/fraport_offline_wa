Ext.define('AssetManagement.base.model.bo.BaseCust_040', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
	fields: [
	    //key fields
	    { name: 'actype',           type: 'string' },
	    { name: 'spras',            type:'string' },
	    
        //ordinary fields
	    { name: 'description',      type: 'string' },
		
        //delta fields
		{ name: 'updFlag',          type:'string' }
	],
	

	constructor : function(config) {
		this.callParent(arguments);
	}
});