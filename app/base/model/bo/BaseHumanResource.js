﻿Ext.define('AssetManagement.base.model.bo.BaseHumanResource', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
	    //key fields
	    { name: 'pernr', type: 'string' },

        //ordinary fields
        { name: 'infty', type: 'string' },
	    { name: 'subty', type: 'string' },
	    { name: 'objps', type: 'string' },
        { name: 'sprps', type: 'string' },
        { name: 'endda', type: 'string' },
	    { name: 'begda', type: 'string' },
	    { name: 'seqnr', type: 'string' },
        { name: 'bukrs', type: 'string' },
        { name: 'werks', type: 'string' },
	    { name: 'persg', type: 'string' },
	    { name: 'persk', type: 'string' },
        { name: 'vdsk1', type: 'string' },
        { name: 'gsber', type: 'string' },
	    { name: 'btrtl', type: 'string' },
	    { name: 'juper', type: 'string' },
        { name: 'abkrs', type: 'string' },
        { name: 'ansvh', type: 'string' },
	    { name: 'kostl', type: 'string' },
	    { name: 'orgeh', type: 'string' },
        { name: 'plans', type: 'string' },
        { name: 'stell', type: 'string' },
	    { name: 'mstbr', type: 'string' },
	    { name: 'sacha', type: 'string' },
        { name: 'sachp', type: 'string' },

        { name: 'sachz', type: 'string' },
	    { name: 'sname', type: 'string' },
	    { name: 'ename', type: 'string' },
        { name: 'otype', type: 'string' },
        { name: 'sbmod', type: 'string' },
	    { name: 'kokrs', type: 'string' },
	    { name: 'fistl', type: 'string' },
        { name: 'geber', type: 'string' },
        { name: 'fkber', type: 'string' },
	    { name: 'grantNbr', type: 'string' },
	    { name: 'sgmnt', type: 'string' },
        { name: 'budget_pd', type: 'string'},
        { name: 'juper', type: 'string'},

        //delta fields
		{ name: 'updFlag', type: 'string' }
    ],

    constructor: function (config) {
        this.callParent(arguments);
    }
});