Ext.define('AssetManagement.base.model.dialogmodel.BaseScenarioDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
    data: {
        scenarios: null
	},
	
	resetData: function() {
		this.callParent();
	
		try {
			this.setData({
			    scenarios: null
			});
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseScenarioDialogViewModel', ex);
	    }
	}
});