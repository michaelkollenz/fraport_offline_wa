Ext.define('AssetManagement.base.model.dialogmodel.BaseSingleTextLineInputDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


	data: {
	    title: '',
        message: '',
        maxLength: 40,
        emptyAllowed: true,
	    onlyUpperCase: false
	},

	resetData: function () {
	    this.callParent();

	    try {
	        this.setData({
	            title: '',
                message: '',
                maxLength: 40,
                emptyAllowed: true,
                onlyUpperCase: false
	        });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSingleTextLineInputDialogViewModel', ex);
	    }
	}
});