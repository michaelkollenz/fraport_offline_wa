Ext.define('AssetManagement.base.model.dialogmodel.BaseMaterialPickerDialogViewModel', {
  extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


  data: {
    order: null,
    useComponents: true,
    useBillOfMaterial: true,
    useMatStocks: true,
    useAllMaterials: true,
    components: null,
    matStocks: null,
    billOfMaterial: null,
    tabIndex: null
  },

  resetData: function () {
    this.callParent();

    try {
      this.setData({
        order: null,
        useComponents: true,
        useBillOfMaterial: true,
        useMatStocks: true,
        useAllMaterials: true,
        components: null,
        matStocks: null,
        billOfMaterial: null,
        tabIndex: null
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseMaterialPickerDialogViewModel', ex);
    }

  }
});