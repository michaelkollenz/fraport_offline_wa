﻿Ext.define('AssetManagement.base.model.dialogmodel.BaseMultiValueCheckboxDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        objClass: null,
        charact: null,
        newValues: null,
        currentValues: null
        
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                objClass: null,
                charact: null,
                newValues: null,
                currentValues: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseMultiValueCheckboxDialogViewModel', ex);
        }

    }
});