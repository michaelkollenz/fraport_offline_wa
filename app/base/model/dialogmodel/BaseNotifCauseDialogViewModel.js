Ext.define('AssetManagement.base.model.dialogmodel.BaseNotifCauseDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        notif: null,
        notifItem: null,
        funcLoc: null,
        equipment: null,
        notifType: null,
        notifCause: null,
        causeCodeGroupsMap: null
    },

    resetData: function() {
        this.callParent();

        try {
            this.setData({
                notif: null,
                notifItem: null,
                funcLoc: null,
                equipment: null,
                notifType: null,
                notifCause: null,
                causeCodeGroupsMap: null
            });
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifCauseDialogViewModel', ex);
        }
	}
});