Ext.define('AssetManagement.base.model.dialogmodel.BaseAlertDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
    data: {
		message: '',
		icon: null
	},
	
	resetData: function() {
		this.callParent();
	
		try {
			this.setData({
				message: '',
				icon: null
			});
	    } catch(ex) {
		    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseAlertDialogViewModel', ex);
	    }
	}
});