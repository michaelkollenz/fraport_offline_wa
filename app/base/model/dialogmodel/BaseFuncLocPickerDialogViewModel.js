Ext.define('AssetManagement.base.model.dialogmodel.BaseFuncLocPickerDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
	data: {
		funcLocs: null
	},
	
	resetData: function() {
		this.callParent();
	
		try {
		    this.setData({
			    funcLocs: null
		    });
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseFuncLocPickerDialogViewModel', ex);
        }
	}
});