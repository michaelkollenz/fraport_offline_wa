Ext.define('AssetManagement.base.model.dialogmodel.BaseObjectListEditDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        notif: null,
        equi: null,
        objectListItem: null
	},
	
	resetData: function() {
		this.callParent();
	
		try {
            this.setData({
                notif: null,
                equi: null,
			    objectListItem: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseObjectListEditDialogViewModel', ex);
        }
	}
});