Ext.define('AssetManagement.base.model.dialogmodel.BasePartnerPickerDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        kunnr: "",
        parVw: ""
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                kunnr: "",
                parVw: ""
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BasePartnerPickerDialogViewModel', ex);
        }

    }
});