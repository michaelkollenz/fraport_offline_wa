Ext.define('AssetManagement.base.model.dialogmodel.BaseOxDialogViewModel', {
	extend: 'AssetManagement.customer.model.viewModel.OxViewModel',

	config: {
		initialized: false
	},

    resetData: function() {
	    try {
    	    this.setInitialized(false);
    	} catch(ex) {
    	    AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }    
	}
});