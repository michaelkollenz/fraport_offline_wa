Ext.define('AssetManagement.base.model.dialogmodel.BaseOnlineGoodsMovementDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',
    alias: 'viewmodel.BaseOnlineGoodsMovementDialogViewModel'
});