﻿Ext.define('AssetManagement.base.model.dialogmodel.BaseInternNoteDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        bo: null,
        internNote: ''
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                bo: null,
                internNote: ''
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseInternNoteDialogViewModel', ex);
        }
    }

});