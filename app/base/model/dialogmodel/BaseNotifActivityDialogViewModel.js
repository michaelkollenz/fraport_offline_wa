Ext.define('AssetManagement.base.model.dialogmodel.BaseNotifActivityDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        notif: null,
        notifItem: null,
        funcLoc: null,
        equipment: null,
        notifType: null,
        notifActivity: null,
        activityCodeGroupsMap: null
    },

    resetData: function() {
        this.callParent();

        try {
            this.setData({
                notif: null,
                notifItem: null,
                funcLoc: null,
                equipment: null,
                notifType: null,
                notifActivity: null,
                activityCodeGroupsMap: null
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifActivityDialogViewModel', ex);
        }
    }

});