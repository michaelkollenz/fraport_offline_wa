Ext.define('AssetManagement.base.model.dialogmodel.BaseSDOrderItemDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
		sdOrder: null,
		sdOrderItem: null,
		isEditMode: false
	},
		
	resetData: function() {
		this.callParent();
	
		try {
			this.setData({
				sdOrder: null,
				sdOrderItem: null,
				isEditMode: false
			});
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSDOrderItemDialogViewModel', ex);
        }
	}
});