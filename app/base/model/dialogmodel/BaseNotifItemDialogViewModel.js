Ext.define('AssetManagement.base.model.dialogmodel.BaseNotifItemDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
    data: {
		notif: null,
	    notifItem: null,
	    funcLoc: null,
	    equipment: null,
	    notifType: null,
	    damageCodeGroupsMap: null,
	    objectCodeGroupsMap: null
    },

    resetData: function() {
    	this.callParent();
       	
    	try {
    		this.setData({
    			notif: null,
    		    notifItem: null,
    		    funcLoc: null,
    		    equipment: null,
    		    notifType: null,
    		    damageCodeGroupsMap: null,
    		    objectCodeGroupsMap: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifItemDialogViewModel', ex);
        }
	}

});