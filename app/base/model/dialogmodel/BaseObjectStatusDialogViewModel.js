﻿Ext.define('AssetManagement.base.model.dialogmodel.BaseObjectStatusDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        object: null,
        objectStatusList: null,
        stsmaList: null,
        displayedlistWithOrdNr: null,
        displayedlistWithoutOrdNr: null
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                object: null,
                objectStatusList: null,
                stsmaList: null,
                displayedlistWithOrdNr: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseObjectStatusDialogViewModel', ex);
        }
    }
});