Ext.define('AssetManagement.base.model.dialogmodel.BaseCustomerPickerDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        customers: null,
        searchCustomer: null
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                customers: null,
                searchCustomer: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseCustomerPickerDialogViewModel', ex);
        }

    }
});