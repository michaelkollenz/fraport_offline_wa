Ext.define('AssetManagement.base.model.dialogmodel.BaseOperationDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
		workCenters: null,
		order: null,
		operation: null
	},
		
	resetData: function() {
		this.callParent();
			
		try {
			this.setData({
				workCenters: null,
				order: null,
				operation: null
			});
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseOperationDialogViewModel', ex);
        }
	}

});