Ext.define('AssetManagement.base.model.dialogmodel.BaseLongtextDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
    data: {
        bo: null,
        readOnly: false,
		backendLongtext: '',
		localLongtext: ''
	},
	
	resetData: function() {
		this.callParent();
	
		try {
			this.setData({
			    bo: null,
			    readOnly: false,
				backendLongtext: '',
				localLongtext: ''
			});
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseLongtextDialogViewModel', ex);
        }
	}

});