Ext.define('AssetManagement.base.model.dialogmodel.BaseOnlineDispositionDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',
    alias: 'viewmodel.BaseOnlineDispositionDialogViewModel'
});