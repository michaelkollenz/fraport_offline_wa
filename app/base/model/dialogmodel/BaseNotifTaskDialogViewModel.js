Ext.define('AssetManagement.base.model.dialogmodel.BaseNotifTaskDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        taskCodeGroupsMap: null,
        notif: null,
        notifItem: null,
        funcLoc: null,
        equipment: null,
        notifType: null,
        notifTask: null
    },

    resetData: function() {
        this.callParent();

        try {
            this.setData({
                taskCodeGroupsMap: null,
                notif: null,
                notifItem: null,
                funcLoc: null,
                equipment: null,
                notifType: null,
                notifTask: null
            });
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifTaskDialogViewModel', ex);
        }
	}
});