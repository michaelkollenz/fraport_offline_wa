Ext.define('AssetManagement.base.model.dialogmodel.BaseLgortPickerDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',
    alias: 'viewmodel.BaseLgortPickerDialogViewModel',


    data: {
        title: '',
        percentage: 0,
        message: '',
        currentlyCancelable: true,
        isSelectionMode: true,
        lgortStore: null,
        selectedLgort: ''
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                title: '',
                percentage: 0,
                message: '',
                currentlyCancelable: true,
                isSelectionMode: true,
                lgortStore: null,
                selectedLgort: ''
            });
        } catch (ex) {
            AssetManagement.helper.OxLogger.logException('Exception occurred inside resetData of BaseLgortPickerDialogViewModel', ex);
        }
    }
});