﻿Ext.define('AssetManagement.base.model.dialogmodel.BaseOnlineSearchDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',

    data: {
        title: '',
        percentage: 0,
        message: '',
        currentlyCancelable: true,
        mode: 0,
        searchCriteria: null,
        lastSearchCriteria: null,
        maxHitCount: 0
    },

    resetData: function () {
        try {
            //always save the last search criteria
            var formerLastSearchCriteria = this.get('lastSearchCriteria');

            this.callParent();

            this.setData({
                title: '',
                percentage: 0,
                message: '',
                currentlyCancelable: true,
                mode: 0,
                searchCriteria: null,
                lastSearchCriteria: formerLastSearchCriteria,
                maxHitCount: 0
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
        }
    }
});
