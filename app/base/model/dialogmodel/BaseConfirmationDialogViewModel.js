Ext.define('AssetManagement.base.model.dialogmodel.BaseConfirmationDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
    data: {
		title: '',
		message: '',
		asHtml: false,
		icon: null,
		hideDeclineButton: false,
		alternateConfirmText: '',
		alternateDeclineText: ''
	},
	
	resetData: function() {
		this.callParent();
			
		try {
			this.setData({
				title: '',
				message: '',
				asHtml: false,
				icon: null,
				hideDeclineButton: false,
				alternateConfirmText: '',
				alternateDeclineText: ''
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseConfirmationDialogViewModel', ex);
        }
	}
});