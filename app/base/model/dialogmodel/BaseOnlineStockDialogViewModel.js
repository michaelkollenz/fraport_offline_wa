Ext.define('AssetManagement.base.model.dialogmodel.BaseOnlineStockDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',
    alias: 'viewmodel.BaseOnlineStockDialogViewModel'
});