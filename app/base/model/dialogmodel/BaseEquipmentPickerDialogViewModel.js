Ext.define('AssetManagement.base.model.dialogmodel.BaseEquipmentPickerDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',

    
	data: {
		superiorTplnr: '',
		equipmentStore: null,
		searchEqui: null
	},
	
	resetData: function() {
		this.callParent();
	
		try {
			this.setData({
				superiorTplnr: '',
				equipmentStore: null,
				searchEqui: null
			});
    	} catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseEquipmentPickerDialogViewModel', ex);
        }
	}

});