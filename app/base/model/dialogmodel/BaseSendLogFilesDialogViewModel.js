Ext.define('AssetManagement.base.model.dialogmodel.BaseSendLogFilesDialogViewModel', {
	extend: 'AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel',


    data: {
        title: '',
        percentage: 0,
        mode: 0,
        errorDateTime: null,
        errorRemarks: '',
        includeDatabase: false
    },

    resetData: function () {
        this.callParent();
        
        try {
            this.setData({
                title: '',
                percentage: 0,
                mode: 0,
                errorDateTime: null,
                errorRemarks: '',
                includeDatabase: false
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSendLogFilesDialogViewModel', ex);
        }
    }
});
