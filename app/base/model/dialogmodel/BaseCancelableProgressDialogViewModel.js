Ext.define('AssetManagement.base.model.dialogmodel.BaseCancelableProgressDialogViewModel', {
    extend: 'AssetManagement.customer.model.dialogmodel.OxDialogViewModel',


    data: {
        title: '',
        percentage: 0,
        message: '',
        currentlyCancelable: true
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                title: '',
                percentage: 0,
                message: '',
                currentlyCancelable: true
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseCancelableProgressDialogViewModel', ex);
        }
    }
});