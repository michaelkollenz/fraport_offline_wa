Ext.define('AssetManagement.base.model.helper.BaseReturnMessage', {
	
	inheritableStatics: {
		/**
		 * Creates a new returnMessage
		 * values: 	m = Message [string]
		 * 			s = Success [bool]
		 * 			t = Type [0 = Success, 1 = Information, 2 = Error]
		 */
		returnMessage: function(m, s, t) {
			try {
			    this.message = m;
			    this.success = s;
			    this.type = t;
			    this.date = new Date();
		    } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside returnMessage of BaseReturnMessage', ex);
            }
		}
	}
});