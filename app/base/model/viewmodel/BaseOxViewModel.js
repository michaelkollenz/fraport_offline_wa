Ext.define('AssetManagement.base.model.viewModel.BaseOxViewModel', {
	extend: 'Ext.app.ViewModel',

    //protected
	getClassName: function () {
	    var retval = 'BaseOxViewModel';

	    try {
	        var className = Ext.getClassName(this);

	        if (AssetManagement.customer.utils.StringUtils.contains(className, '.')) {
	            var splitted = className.split('.');
	            retval = splitted[splitted.length - 1];
	        } else {
	            retval = className;
	        }
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getClassName of BaseOxViewModel', ex);
	    }

	    return retval;
	},

	//public
    //@override
	//get rid of all the unnecessary sencha logic
	setData: function(value) {
		try {
			this.data = value;
			this._data = value;
		} catch(ex) {
		    AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
		}
    },

	//public
    //@override
	//get rid of all the unnecessary sencha logic
	set: function(property, value) {
		try {
			this.data[property] = value;
		} catch(ex) {
		    AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
		}
    },
	
    //public
    //@override
	//get rid of all the unnecessary sencha logic
	get: function(property) {
    	var retval = null;
	
		try {
			retval = this.data[property];
		} catch(ex) {
		    AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
		}
		
		return retval;
    },

    //public
    getRelevantTimeZone: function(){
    	var retval = '';

    	try {
    		var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
			retval = userInfo.get('timeZone');
    	} catch(ex) {
    	    AssetManagement.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    	}

    	return retval;
    }
});