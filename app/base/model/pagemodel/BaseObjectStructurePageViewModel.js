 Ext.define('AssetManagement.base.model.pagemodel.BaseObjectStructurePageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

		
    data: {
	 	objStructureStore: null, 
	 	equipment: null, 
	 	funcLoc: null
    },

    resetData: function() {
    	this.callParent(); 
    	
    	try {
    		this.setData({
    			objStructureStore: null,
    			equipment: null, 
    		 	funcLoc: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseObjectStructurePageViewModel', ex);
        }
	}
});