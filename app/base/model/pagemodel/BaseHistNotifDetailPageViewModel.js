Ext.define('AssetManagement.base.model.pagemodel.BaseHistNotifDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	histNotif: null,
    	tpl: null,
    	equi: null,
    	ord: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			histNotif: null,
    		    tpl: null,
    		    equi: null,
    		    ord: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseHistNotifDetailPageViewModel', ex);
        } 		
	}

});