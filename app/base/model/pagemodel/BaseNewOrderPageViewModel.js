Ext.define('AssetManagement.base.model.pagemodel.BaseNewOrderPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        order: null,
        operation: null,
        orderTypes: null,
        ilartTypes: null,
        bemotTypes: null,
        equi: null,
        funcLoc: null,
        customer: null,
        partnerAp: null,
        partnerZr: null
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                order: null,
                operation: null,
                orderTypes: null,
                ilartTypes: null,
                bemotTypes: null,
                equi: null,
                funcLoc: null,
                customer: null,
                partnerAp: null,
                partnerZr: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNewOrderPageViewModel', ex);
        }
    }
});