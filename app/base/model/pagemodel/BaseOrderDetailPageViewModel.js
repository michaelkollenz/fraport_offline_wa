Ext.define('AssetManagement.base.model.pagemodel.BaseOrderDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
		order: null,
		bemotTypes: null,
		orderTypes: null,
		custPara: null,
		bemot: null,
		timeConfs: null,
		matConfs: null,
        prueflose: null
    },

    resetData: function() {
    	this.callParent(); 
    	
    	try {
    		this.setData({
    			order: null,
    			bemotTypes: null,
                orderTypes: null,
    			custPara: null,
    			bemot: null,
	    		timeConfs: null,
	    		matConfs: null,
                prueflose: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseOrderDetailPageViewModel', ex);
        }
	}
});