Ext.define('AssetManagement.base.model.pagemodel.BaseTimeConfPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	order: null, 
    	operation: null,
		orderTypes: null,
    	activityTypes: null,
    	accountIndications: null,
    	timeConf: null,
    	isEditMode: false
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			order: null, 
    			operation: null,
                orderTypes: null,
    			activityTypes: null,
    			accountIndications: null,
    			timeConf: null,
    			isEditMode: false
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseTimeConfPageViewModel', ex);
        }
	}
});