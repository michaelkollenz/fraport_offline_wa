Ext.define('AssetManagement.base.model.pagemodel.BaseComponentPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
		order: null,
		component: null,
		storLocs: null,
		chosenMaterial: null,
		isEditMode: false
    },

    resetData: function() {
    	this.callParent();
        	
    	try {
    		this.setData({
    			order: null,
    			component: null,
    			storLocs: null,
    			chosenMaterial: null,
    			isEditMode: false
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseComponentPageViewModel', ex);
        }
	}
});