Ext.define('AssetManagement.base.model.pagemodel.BaseDeliveryDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


  data: {
    deliveryStore: null,
    reasonStore: null,
    persNo: null,
    kostl: null
  },

  resetData: function () {
    this.callParent();

    try {
      this.setData({
        deliveryStore: null,
        reasonStore: null,
        persNo: null,
        kostl: null
      });
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseDeliveryDetailPageViewModel', ex);
        }
	}
});