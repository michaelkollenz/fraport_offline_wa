Ext.define('AssetManagement.base.model.pagemodel.BaseOrderListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	orderStore: null,
    	searchOrder: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			orderStore: null,
    			searchOrder: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseOrderListPageViewModel', ex);
        }
	}
});