Ext.define('AssetManagement.base.model.pagemodel.BaseSDOrderListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	sdOrderStore: null,
    	searchSDOrder: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			sdOrderStore: null,
    			searchSDOrder: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSDOrderListPageViewModel', ex);
        }
	}
});