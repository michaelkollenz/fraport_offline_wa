Ext.define('AssetManagement.base.model.pagemodel.BaseEquiDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
		equipment: null, 
		histOrders: null, 
		histNotifs: null,
		funcLoc: null,
		preparedClassifications: null
    },
    
     resetData: function() {
    	this.callParent();    	
    	
    	try {
    		this.setData({
    			equipment: null, 
    			histOrders: null, 
    			histNotifs: null, 
    			funcLoc: null,
    			preparedClassifications: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseEquiDetailPageViewModel', ex);
        }
	}

});