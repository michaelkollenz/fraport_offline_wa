Ext.define('AssetManagement.base.model.pagemodel.BaseEquipmentListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	equipmentStore: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			equipmentStore: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseEquipmentListPageViewModel', ex);
        }
	}
});