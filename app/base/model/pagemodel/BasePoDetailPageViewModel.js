Ext.define('AssetManagement.base.model.pagemodel.BasePoDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',
    alias: 'viewmodel.BasePoDetailPageViewModel',

    data: {
    	poItem: null,
        delivery: null,
        miProcess: null,
        selectedLgort: null,
		goodsMovementItemsStore: null
    },

    resetData: function() {
    	this.callParent();

    	try {
    		this.setData({
                poItem: null,
                delivery: null,
                selectedLgort: null,
                goodsMovementItemsStore: null
    		});
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BasePoDetailPageViewModel', ex);
        }
	}
});