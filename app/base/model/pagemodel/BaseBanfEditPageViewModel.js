Ext.define('AssetManagement.base.model.pagemodel.BaseBanfEditPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	banfStore: null,
    	cust001: null,
    	banf: null,
    	storLocs: null, 
    	selectedMaterial: null,
    	isEditMode: false
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			banfStore: null,
    	    	cust001: null,
    			banf: null, 
    			storLocs: null,
    			selectedMaterial: null,
    			isEditMode: false
    		});
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseBanfEditPageViewModel', ex);
        }
	}
});