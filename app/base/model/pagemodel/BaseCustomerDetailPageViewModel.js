Ext.define('AssetManagement.base.model.pagemodel.BaseCustomerDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
		customer: null
    },

    resetData: function() {
    	this.callParent();    	
    	
    	try {
    		this.setData({
    			customer: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseCustomerDetailPageViewModel', ex);
        }
	}

});