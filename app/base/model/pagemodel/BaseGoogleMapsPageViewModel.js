Ext.define('AssetManagement.base.model.pagemodel.BaseGoogleMapsPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


	//CURRENTLY A DUMMY MODEL AND NOT USED BY CONTROLLER AND VIEW
	
    data: {
    },

    resetData: function() {
    	this.callParent();
    
    	try {
		    this.setData({
		    });
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseGoogleMapsPageViewModel', ex);
        }
	}
});