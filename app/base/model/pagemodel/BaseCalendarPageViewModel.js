Ext.define('AssetManagement.base.model.pagemodel.BaseCalendarPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	orderStore: null,
    	cust001: null,
    	wrkcStore: null,
    	currentArbpl: null,
		plannedEventStore: null,
		unplannedEventStore: null,
		stsma: null
    },

    resetData: function() {
    	this.callParent();
     	
    	try {
    		this.setData({
    			orderStore: null
    		});
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseCalendarPageViewModel', ex);
        }
	}
});