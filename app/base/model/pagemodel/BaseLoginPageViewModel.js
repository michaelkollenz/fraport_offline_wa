Ext.define('AssetManagement.base.model.pagemodel.BaseLoginPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


	data: {
		mode: '',
		mandt: '',
		user: '',
		password: '',
		oldPassword: '',
		newPassword: '',
		newPasswordRepeat: '',
		statusMessage: '',
		languages: null
	},
	
	resetData: function() {
		this.callParent(); 
		
		try {
			this.setData({
				mode: '',
				mandt: '',
				user: '',
				password: '',
				oldPassword: '',
				newPassword: '',
				newPasswordRepeat: '',
				statusMessage: '',
				languages: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseLoginPageViewModel', ex);
        }
	}

});