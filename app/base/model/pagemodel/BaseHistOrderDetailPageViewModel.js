Ext.define('AssetManagement.base.model.pagemodel.BaseHistOrderDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	histOrder: null,
    	tpl: null,
    	equi: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			histOrder: null,
    		    tpl: null,
    		    equi: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseHistOrderDetailPageViewModel', ex);
        }		
	}

});