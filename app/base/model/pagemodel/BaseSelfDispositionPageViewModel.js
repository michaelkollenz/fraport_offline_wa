Ext.define('AssetManagement.base.model.pagemodel.BaseSelfDispositionPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


  data: {
    dispositionStore: null
  },

  resetData: function () {
    this.callParent();

    try {
      this.setData({
        dispositionStore: null
      });
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSelfDispositionPageViewModel', ex);
        }
	}
});