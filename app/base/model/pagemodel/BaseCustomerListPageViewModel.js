Ext.define('AssetManagement.base.model.pagemodel.BaseCustomerListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	customerStore: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			customerStore: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseCustomerListPageViewModel', ex);
        }
	}
});