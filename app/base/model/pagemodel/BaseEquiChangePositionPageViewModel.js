Ext.define('AssetManagement.base.model.pagemodel.BaseEquiChangePositionPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        equipment: null,
        superiorEqui: null,
        funcLoc: null,
        newPosition: '',
        changeDate: null
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                equipment: null,
                superiorEqui: null,
                funcLoc: null,
                newPosition: '',
                changeDate: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseEquiChangePositionPageViewModel', ex);
        }
    }
});