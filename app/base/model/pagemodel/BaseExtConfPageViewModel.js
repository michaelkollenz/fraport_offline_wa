Ext.define('AssetManagement.base.model.pagemodel.BaseExtConfPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
	data: {
		order: null,
		operation: null,
		extConf: null,
		banfHeader: null,
		banfPositions: null,
		cust017: null,
		custPara: null,
		extConfMaterials: null,
		selectedMaterial: null,
		hasFinalConf: false,
		isEditMode: false
	},
	
	 resetData: function() {
		this.callParent(); 
		
		try {
			this.setData({
				order: null,
				operation: null,
				extConf: null,
				banfHeader: null,
				banfPositions: null,
				cust017: null,
				custPara: null,
				extConfMaterials: null,
				selectedMaterial: null,
				hasFinalConf: false,
				isEditMode: false
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseExtConfPageViewModel', ex);
        }
	}
});