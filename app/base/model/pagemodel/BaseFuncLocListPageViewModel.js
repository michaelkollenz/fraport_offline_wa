Ext.define('AssetManagement.base.model.pagemodel.BaseFuncLocListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	funcLocStore: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			funcLocStore: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseFuncLocListPageViewModel', ex);
        }
	}
});