Ext.define('AssetManagement.base.model.pagemodel.BaseNotifDetailPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        notif: null
    },
    
    resetData: function() {
    	this.callParent(); 
    	
    	try {
    		this.setData({
    			notif: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifDetailPageViewModel', ex);
        }
	}
});