Ext.define('AssetManagement.base.model.pagemodel.BaseGoodsMovementListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',
    alias: 'viewmodel.BaseGoodsMovementListPageViewModel',

    data: {
    	poItemsStore: null,
        goodsMovementsStore: null,
        miProcess: null,
		selectedLgort: null,
      showStorageLocation: null
    }
});