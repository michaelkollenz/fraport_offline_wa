Ext.define('AssetManagement.base.model.pagemodel.BaseInventoryListPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        inventoryStore: null,
        searchInventory: null

    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                inventoryStore: null,
                searchInventory: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseInventoryListPageViewModel', ex);
        }
    }
});