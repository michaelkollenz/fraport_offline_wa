Ext.define('AssetManagement.base.model.pagemodel.BaseEquipmentEditPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
	data: {
		equipment: null 
	},
	
	 resetData: function() {
		this.callParent(); 
		
		try {
			this.setData({
				equipment: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseEquipmentEditPageViewModel', ex);
        }
	}


});