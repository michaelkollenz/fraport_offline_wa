Ext.define('AssetManagement.base.model.pagemodel.BaseIntegratedConfPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	order: null, 
    	operation: null,
    	activityTypes: null,
    	accountIndications: null,
    	cust_041: null,
    	cust_043: null,
        cust_001: null,
    	timeConf: null,
    	isEditMode: false, 
    	storLocs: null,
    	matConf: null,
    	material: null, 
    	viewMode: null, 
        notif: null, 
        notifLongtextBackend: null,
        notifLongtextClient: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			order: null, 
    			operation: null,
    			activityTypes: null,
    			accountIndications: null,
    	        cust_041: null,
    	        cust_043: null,   
                cust_001: null,
    			timeConf: null,
    			isEditMode: false,  
    	        storLocs: null,
    	        matConf: null,
    	        material: null,
    	        viewMode: null,
                notif: null, 
                notifLongtextBackend: null,
                notifLongtextClient: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseIntegratedConfPageViewModel', ex);
        }
	}
});