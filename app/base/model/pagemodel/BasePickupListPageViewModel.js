Ext.define('AssetManagement.base.model.pagemodel.BasePickupListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	deliveryStore: null,
    	searchDelivery: null,
      reasonStore: null
    },

    resetData: function() {
    	this.callParent();

    	try {
    		this.setData({
    			deliveryStore: null,
    			searchDelivery: null,
          reasonStore: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BasePickupListtPageViewModel', ex);
        }
	}
});