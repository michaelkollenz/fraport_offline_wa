Ext.define('AssetManagement.base.model.pagemodel.BaseMeasDocEditPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


	data: {
	    measPoint: null,
	    measDoc: null
	},

	resetData: function () {
	    this.callParent();

	    try {
	        this.setData({
	            measPoint: null,
	            measDoc: null
	        });
	    } catch (ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of MaterialListPageViewModel', ex);
	    }
	}

});