Ext.define('AssetManagement.base.model.pagemodel.BaseNotifItemDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


	data: {
	    item: null
	},
	
	resetData: function() {
		this.callParent(); 
		
		try {
			this.setData({
				item: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifItemDetailPageViewModel', ex);
        }
	}

});