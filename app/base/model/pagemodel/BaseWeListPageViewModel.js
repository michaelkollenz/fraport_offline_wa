Ext.define('AssetManagement.base.model.pagemodel.BaseWeListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',
    alias: 'viewmodel.BaseWeListPageViewModel',

    data: {
    	poItemsStore: null,
        goodsMovementItemsStore: null,
        delivery: null,
        miProcess: null,
        selectedLgort: null,
        poInput: '',
        reference: ''
    }
});