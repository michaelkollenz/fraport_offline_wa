Ext.define('AssetManagement.base.model.pagemodel.BaseNewEquiPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
		equipment: null,
		equipmentTypes: null,
		chosenFuncLoc: null,
		chosenHeadEquipment: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			equipment: null,
    			equipmentTypes: null,
    			chosenFuncLoc: null,
    			chosenHeadEquipment: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNewEquiPageViewModel', ex);
        }
	}
});