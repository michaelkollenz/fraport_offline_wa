Ext.define('AssetManagement.base.model.pagemodel.BaseMatConfPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	order: null,
    	operation: null,
    	accountIndications: null,
    	storLocs: null,
    	matConf: null,
    	chosenMaterial: null,
    	isEditMode: false
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			order: null,
    			operation: null,
    			accountIndications: null,
    			storLocs: null,
    			matConf: null,
    			chosenMaterial: null,
    			isEditMode: false
    		});
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseMatConfPageViewModel', ex);
        }
	}
});