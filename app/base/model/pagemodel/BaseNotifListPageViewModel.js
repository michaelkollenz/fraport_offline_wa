Ext.define('AssetManagement.base.model.pagemodel.BaseNotifListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	notifStore: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    			notifStore: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNotifListPageViewModel', ex);
        }
	}
});