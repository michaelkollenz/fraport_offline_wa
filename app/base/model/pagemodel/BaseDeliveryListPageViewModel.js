Ext.define('AssetManagement.base.model.pagemodel.BaseDeliveryListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
    	deliveryStore: null,
      reasonStore: null
    },

    resetData: function() {
    	this.callParent();

    	try {
    		this.setData({
    			deliveryStore: null,
          reasonStore: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseDeliveryListPageViewModel', ex);
        }
	}
});