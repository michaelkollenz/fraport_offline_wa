Ext.define('AssetManagement.base.model.pagemodel.BaseNewSDOrderPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
	data: {
		sdOrder: null,
		sdOrderTypes: null,
		order: null, 
		notif: null
	},
	
	resetData: function() {
		this.callParent(); 
		
		try {
			this.setData({
				sdOrder: null,
				sdOrderTypes: null,
				order: null, 
				notif: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNewSDOrderPageViewModel', ex);
        }
	}

});