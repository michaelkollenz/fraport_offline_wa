Ext.define('AssetManagement.base.model.pagemodel.BaseSDOrderDetailPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	sdOrder: null
    },

    resetData: function() {
    	this.callParent(); 
    	
    	try {
    		this.setData({
    			sdOrder: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSDOrderDetailPageViewModel', ex);
        }
	}
});