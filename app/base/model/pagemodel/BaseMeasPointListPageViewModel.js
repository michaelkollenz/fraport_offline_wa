﻿Ext.define('AssetManagement.base.model.pagemodel.BaseMeasPointListPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        measPointStore: null,
        searchMeasPoint: null
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                searchMeasPoint: null,
                measPointStore: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseMeasPointListPageViewModel', ex);
        }
    }
});