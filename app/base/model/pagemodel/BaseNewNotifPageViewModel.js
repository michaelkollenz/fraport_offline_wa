Ext.define('AssetManagement.base.model.pagemodel.BaseNewNotifPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


	data: {
		notif: null,
		operation: null,
		notifTypes: null,
		equi: null,
		funcLoc: null,
		customer: null,
		partnerAp: null,
		partnerZr: null,
		cust001: null,
        notifCauseCustCodesGroup: null
	},

	resetData: function() {
		this.callParent();

		try {
			this.setData({
				notif: null,
				operation: null,
				notifTypes: null,
				equi: null,
				funcLoc: null,
				customer: null,
				partnerAp: null,
				partnerZr: null,
				cust001: null,
                notifCauseCustCodesGroup: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseNewNotifPageViewModel', ex);
        }
	}
});