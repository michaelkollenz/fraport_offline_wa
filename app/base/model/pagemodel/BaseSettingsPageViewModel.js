Ext.define('AssetManagement.base.model.pagemodel.BaseSettingsPageViewModel', {
  extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

  data: {
    sapClient: null,
    sapSystem: null,
    gateway: null,
    gatewayPort: null,
    gatewayIdent: null,
    protocol: null
  },

  resetData: function () {
    this.callParent();

    try {
      this.setData({
        sapClient: null,
        sapSystem: null,
        gateway: null,
        gatewayPort: null,
        gatewayIdent: null,
        protocol: null
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseSettingsPageViewModel', ex);
    }
  }
});