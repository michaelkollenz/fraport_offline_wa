Ext.define('AssetManagement.base.model.pagemodel.BaseMatListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',
    alias: 'viewmodel.BaseMatListPageViewModel',

    data: {
    	materialStore: null,
      selectedLgort: null,
        materialInput: ''
    }
});