﻿Ext.define('AssetManagement.base.model.pagemodel.BaseChecklistPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        qplos: null,
        qplan: null,
        qmnum: '',
        parentBO: null,
        techObj: null,
        viewChars: null,
        readOnly: false,
        checklistHasResults: false
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                qplos: null,
                qplan: null,
                matnr: null,
                qmnum: '',
                parentBO: null,
                techObj: null, 
                viewChars: null,
                readOnly: false,
                checklistHasResults: false
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseChecklistPageViewModel', ex);
        }
    }
});