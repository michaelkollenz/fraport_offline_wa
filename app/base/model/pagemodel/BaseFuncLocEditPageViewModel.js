Ext.define('AssetManagement.base.model.pagemodel.BaseFuncLocEditPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
    	funcLoc: null 
	},
	
	 resetData: function() {
		this.callParent(); 
		
		try {
			this.setData({
				funcLoc: null
			});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseFuncLocEditPageViewModel', ex);
        }
	}

});