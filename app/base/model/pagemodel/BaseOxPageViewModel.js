Ext.define('AssetManagement.base.model.pagemodel.BaseOxPageViewModel', {
	extend: 'AssetManagement.customer.model.viewModel.OxViewModel',


	config: {
		initialized: false
	},

    resetData: function() {
	    try {
	    	this.setData({});
	    
    	    this.setInitialized(false);
	    } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseOxPageViewModel', ex);
        }     
	}
});