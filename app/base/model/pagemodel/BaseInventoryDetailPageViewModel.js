Ext.define('AssetManagement.base.model.pagemodel.BaseInventoryDetailPageViewModel', {
    extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',


    data: {
        inventory: null,
        plant: null,
        stgeLoc: null,
        searchItem: null,
        lastSelectedItem: null,
        listItems: null
    },

    resetData: function () {
        this.callParent();

        try {
            this.setData({
                inventory: null,
                plant: null,
                stgeLoc: null,
                searchItem: null,
                lastSelectedItem: null,
                listItems: null
            });
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseInventoryDetailPageViewModel', ex);
        }
    }
});