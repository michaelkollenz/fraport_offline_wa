Ext.define('AssetManagement.base.model.pagemodel.BaseWaCreatePageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',
    alias: 'viewmodel.BaseWaCreatePageViewModel',

    data: {
    	processStore: null,
    	relevantFields: null,
        miProcess: null,
        printKZ: null,
        wempf: null,
        checkValue: null,
      refDoc: null,
      title: null,
      availableItemsArray: null
    },

    resetData: function() {
    	this.callParent();

    	try {
    		this.setData({
            processStore: null,
            relevantFields: null,
            miProcess: null,
            printKZ: null,
            wempf: null,
            checkValue: null,
          refDoc: null,
          title: null,
          availableItemsArray: null
    		});
	    } catch(ex) {
	        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseWaCreatePageViewModel', ex);
        }
	}
});