Ext.define('AssetManagement.base.model.pagemodel.BaseMaterialListPageViewModel', {
	extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',

    
    data: {
	    matStocks: null
    },

    resetData: function() {
    	this.callParent();
    
    	try {
    		this.setData({
    	    	matStocks: null
    		});
	    } catch(ex) {
           AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseMaterialListPageViewModel', ex);
        }
	}
});