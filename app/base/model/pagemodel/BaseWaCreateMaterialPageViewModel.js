Ext.define('AssetManagement.base.model.pagemodel.BaseWaCreateMaterialPageViewModel', {
  extend: 'AssetManagement.customer.model.pagemodel.OxPageViewModel',
  alias: 'viewmodel.BaseWaCreateMaterialPageViewModel',

  data: {
    goodsMovementDefaults: null,
    goodsMovementItemsStore: null,
    miProcess: null,
    matnr: null,
    printKZ: null,
    printKZLastUserValue: null,
    menge: null,
    reasonStore: null,
    refDoc: null,
    title: null
  },

  resetData: function () {
    this.callParent();
    try {
      this.setData({
        goodsMovementDefaults: null,
        goodsMovementItemsStore: null,
        miProcess: null,
        matnr: null,
        printKZLastUserValue: null,
        menge: null,
        title: null,
        refDoc: null,
        reasonStore: null
      });
    } catch (ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside resetData of BaseWaCreateMaterialPageViewModel', ex);
    }
  }
});