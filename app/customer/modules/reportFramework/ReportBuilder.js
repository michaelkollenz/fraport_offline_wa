﻿//this class builds a report pdf - this may be a heavy task
//that is why this procedure has a lot of defer calls
//this procedure may be cancelled
//it also provides progress, error and cancelation events
Ext.define('AssetManagement.customer.modules.reportFramework.ReportBuilder', {
    extend:'AssetManagement.base.modules.reportFramework.BaseReportBuilder'
});
