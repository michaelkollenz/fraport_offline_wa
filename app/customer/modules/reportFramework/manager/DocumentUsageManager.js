﻿Ext.define('AssetManagement.customer.modules.reportFramework.manager.DocumentUsageManager', {
    extend:'AssetManagement.base.modules.reportFramework.manager.BaseDocumentUsageManager',

    inheritableStatics: {
        logos : ['pdf_logo.png']

    }
});
