/**
 * @class AssetManagement.customer.modules.calendar.CalendarPanel
 * @extends Ext.Panel
 * <p>This is the default container for Ext calendar views. It supports day, week and month views as well
 * as a built-in event edit form. The only requirement for displaying a calendar is passing in a valid
 * {@link #calendarStore} config containing records of type {@link AssetManagement.customer.modules.calendar.EventRecord EventRecord}. In order
 * to make the calendar interactive (enable editing, drag/drop, etc.) you can handle any of the various
 * events fired by the underlying views and exposed through the BaseCalendarPanel.</p>
 * {@link #layoutConfig} option if needed.</p>
 * @constructor
 * @param {Object} config The config object
 * @xtype calendarpanel
 */
Ext.define('AssetManagement.customer.modules.calendar.CalendarPanel', {
    extend: 'AssetManagement.base.modules.calendar.BaseCalendarPanel',
    alias: 'widget.calendarpanel'
});
