/**
 * @class AssetManagement.customer.modules.calendar.form.BaseEventWindow
 * @extends Ext.Window
 * <p>A custom window containing a basic edit form used for quick editing of events.</p>
 * <p>This window also provides custom events specific to the calendar so that other calendar components can be easily
 * notified when an event has been edited via this component.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.form.EventWindow', {
	extend: 'AssetManagement.base.modules.calendar.form.BaseEventWindow',
    alias: 'widget.eventeditwindow'
});