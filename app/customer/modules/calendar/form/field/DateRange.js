/**
 * @class Ext.form.field.BaseDateRange
 * @extends Ext.form.Field
 * <p>A combination field that includes start and end dates and times, as well as an optional all-day checkbox.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.form.field.DateRange', {
    extend: 'AssetManagement.base.modules.calendar.form.field.BaseDateRange',
    alias: 'widget.daterangefield'
});