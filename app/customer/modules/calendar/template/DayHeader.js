/**
 * @class AssetManagement.customer.modules.calendar.template.BaseDayHeader
 * @extends Ext.XTemplate
 * <p>This is the template used to render the all-day event container used in {@link AssetManagement.customer.modules.calendar.DayView DayView} and
 * {@link AssetManagement.customer.modules.calendar.WeekView WeekView}. Internally the majority of the layout logic is deferred to an instance of
 * {@link AssetManagement.customer.modules.calendar.BoxLayoutTemplate}.</p>
 * <p>This template is automatically bound to the underlying event store by the 
 * calendar components and expects records of type {@link AssetManagement.customer.modules.calendar.EventRecord}.</p>
 * <p>Note that this template would not normally be used directly. Instead you would use the {@link AssetManagement.customer.modules.calendar.DayViewTemplate}
 * that internally creates an instance of this template along with a {@link AssetManagement.customer.modules.calendar.DayBodyTemplate}.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.template.DayHeader', {
    extend: 'AssetManagement.base.modules.calendar.template.BaseDayHeader'
    

});