/**
 * @class AssetManagement.customer.modules.calendar.template.BaseMonth
 * @extends Ext.XTemplate
 * <p>This is the template used to render the {@link AssetManagement.customer.modules.calendar.template.BaseMonth MonthView}. Internally this class defers to an
 * instance of {@link Ext.calerndar.BoxLayoutTemplate} to handle the inner layout rendering and adds containing elements around
 * that to form the month view.</p> 
 * <p>This template is automatically bound to the underlying event store by the 
 * calendar components and expects records of type {@link AssetManagement.customer.modules.calendar.EventRecord}.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.template.Month', {
    extend: 'AssetManagement.base.modules.calendar.template.BaseMonth'

});