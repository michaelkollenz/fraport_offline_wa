/**
 * @class AssetManagement.customer.modules.calendar.template.BaseDayBody
 * @extends Ext.XTemplate
 * <p>This is the template used to render the scrolling body container used in {@link AssetManagement.customer.modules.calendar.DayView DayView} and
 * {@link AssetManagement.customer.modules.calendar.WeekView WeekView}. This template is automatically bound to the underlying event store by the
 * calendar components and expects records of type {@link AssetManagement.customer.modules.calendar.EventRecord}.</p>
 * <p>Note that this template would not normally be used directly. Instead you would use the {@link AssetManagement.customer.modules.calendar.DayViewTemplate}
 * that internally creates an instance of this template along with a {@link AssetManagement.customer.modules.calendar.DayHeaderTemplate}.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.template.DayBody', {
    extend: 'AssetManagement.base.modules.calendar.template.BaseDayBody'

});