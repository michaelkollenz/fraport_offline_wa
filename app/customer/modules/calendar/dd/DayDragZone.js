/*
 * Internal drag zone implementation for the calendar day and week views.
 */
Ext.define('AssetManagement.customer.modules.calendar.dd.DayDragZone', {
    extend: 'AssetManagement.base.modules.calendar.dd.BaseDayDragZone'
});