/*
 * @class AssetManagement.customer.modules.calendar.dd.BaseStatusProxy
 * A specialized drag proxy that supports a drop status icon and auto-repair. It also
 * contains a calendar-specific drag status message containing details about the dragged event's target drop date range.  
 * This is the default drag proxy used by all calendar views.
 * @constructor
 * @param {Object} config
 */
Ext.define('AssetManagement.customer.modules.calendar.dd.StatusProxy', {
    extend: 'AssetManagement.base.modules.calendar.dd.BaseStatusProxy'
});