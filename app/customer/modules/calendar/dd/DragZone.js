/*
 * Internal drag zone implementation for the calendar components. This provides base functionality
 * and is primarily for the month view -- DayViewDD adds day/week view-specific functionality.
 */
Ext.define('AssetManagement.customer.modules.calendar.dd.DragZone', {
    extend: 'AssetManagement.base.modules.calendar.dd.BaseDragZone'
});