/*
 * Internal drop zone implementation for the calendar day and week views.
 */
Ext.define('AssetManagement.customer.modules.calendar.dd.DayDropZone', {
    extend: 'AssetManagement.base.modules.calendar.dd.BaseDayDropZone'
});
