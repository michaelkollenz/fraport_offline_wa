
Ext.define('AssetManagement.customer.modules.calendar.data.EventModel', {
    extend: 'AssetManagement.base.modules.calendar.data.BaseEventModel'

},
function(){
	try {
        this.reconfigure();
	} catch(ex) {
		AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside function of EventModul', ex)
	}    
});
