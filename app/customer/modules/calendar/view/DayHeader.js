/**
 * @class AssetManagement.customer.modules.calendar.view.BaseDayHeader
 * @extends AssetManagement.customer.modules.calendar.MonthView
 * <p>This is the header area container within the day and week views where all-day events are displayed.
 * Normally you should not need to use this class directly -- instead you should use {@link AssetManagement.customer.modules.calendar.DayView DayView}
 * which aggregates this class and the {@link AssetManagement.customer.modules.calendar.DayBodyView DayBodyView} into the single unified view
 * presented by {@link AssetManagement.customer.modules.calendar.CalendarPanel BaseCalendarPanel}.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.view.DayHeader', {
    extend: 'AssetManagement.base.modules.calendar.view.BaseDayHeader',
    alias: 'widget.dayheaderview'
});
