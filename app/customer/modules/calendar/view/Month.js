/**
 * @class AssetManagement.customer.modules.calendar.view.BaseMonth
 * @extends AssetManagement.customer.modules.calendar.CalendarView
 * <p>Displays a calendar view by month. This class does not usually need ot be used directly as you can
 * use a {@link AssetManagement.customer.modules.calendar.CalendarPanel BaseCalendarPanel} to manage multiple calendar views at once including
 * the month view.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.view.Month', {
    extend: 'AssetManagement.base.modules.calendar.view.BaseMonth',
    alias: 'widget.monthview'
});
