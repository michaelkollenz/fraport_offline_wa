/**
 * @class AssetManagement.customer.modules.calendar.view.BaseWeek
 * @extends AssetManagement.customer.modules.calendar.DayView
 * <p>Displays a calendar view by week. This class does not usually need ot be used directly as you can
 * use a {@link AssetManagement.customer.modules.calendar.CalendarPanel BaseCalendarPanel} to manage multiple calendar views at once including
 * the week view.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.view.Week', {
    extend: 'AssetManagement.base.modules.calendar.view.BaseWeek',
    alias: 'widget.weekview'
});
