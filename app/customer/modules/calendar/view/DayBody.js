/**S
 * @class AssetManagement.customer.modules.calendar.view.BaseDayBody
 * @extends AssetManagement.customer.modules.calendar.view.BaseAbstractCalendar
 * <p>This is the scrolling container within the day and week views where non-all-day events are displayed.
 * Normally you should not need to use this class directly -- instead you should use {@link AssetManagement.customer.modules.calendar.DayView DayView}
 * which aggregates this class and the {@link AssetManagement.customer.modules.calendar.DayHeaderView DayHeaderView} into the single unified view
 * presented by {@link AssetManagement.customer.modules.calendar.CalendarPanel BaseCalendarPanel}.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.view.DayBody', {
    extend: 'AssetManagement.base.modules.calendar.view.BaseDayBody',
    alias: 'widget.daybodyview'

});
