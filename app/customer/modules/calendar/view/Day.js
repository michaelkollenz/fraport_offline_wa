/**
 * @class AssetManagement.customer.modules.calendar.view.Day
 * @extends Ext.container.Container
 * <p>Unlike other calendar views, is not actually a subclass of {@link AssetManagement.customer.modules.calendar.view.BaseAbstractCalendar BaseAbstractCalendar}.
 * Instead it is a {@link Ext.container.Container Container} subclass that internally creates and manages the layouts of
 * a {@link AssetManagement.customer.modules.calendar.DayHeaderView DayHeaderView} and a {@link AssetManagement.customer.modules.calendar.DayBodyView DayBodyView}. As such
 * DayView accepts any config values that are valid for DayHeaderView and DayBodyView and passes those through
 * to the contained views. It also supports the interface required of any calendar view and in turn calls methods
 * on the contained views as necessary.</p>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('AssetManagement.customer.modules.calendar.view.Day', {
    extend: 'AssetManagement.base.modules.calendar.view.BaseDay',
    alias: 'widget.dayview'
});
