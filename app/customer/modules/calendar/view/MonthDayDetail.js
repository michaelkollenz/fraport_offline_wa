/*
 * This is the view used internally by the panel that displays overflow events in the
 * month view. Anytime a day cell cannot display all of its events, it automatically displays
 * a link at the bottom to view all events for that day. When clicked, a panel pops up that
 * uses this view to display the events for that day.
 */
Ext.define('AssetManagement.customer.modules.calendar.view.MonthDayDetail', {
    extend: 'AssetManagement.base.modules.calendar.view.BaseMonthDayDetail',
    alias: 'widget.monthdaydetailview'
});
