﻿Ext.define('AssetManagement.customer.controller.ClientStateController', {
    extend: 'AssetManagement.base.controller.BaseClientStateController',

    inheritableStatics: {

        initializeRuntimeCustomizing: function (callback, callbackScope) {
            try {
                var newCustInitCallback = function (initialSyncWasAlreadyDone) {
                    var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort');
                    var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
                    if (initialSyncWasAlreadyDone && !selectedLgort && funcParaInstance.getValue1For('mm_active') == 'X') { //initial synch was done (we have data) and lgort not selected and we are in MM where lgort has to be selected
                        var successCallback = function (selectedLgort) {
                            try {
                                var mainView = AssetManagement.customer.core.Core.getMainView();
                                mainView.getViewModel().set('selectedLgort', selectedLgort);
                                callback.call(callbackScope, initialSyncWasAlreadyDone); //lgort is now selected, continue with the normal procedures
                                var homeScreenView = mainView.down('pageshomescreenpage');
                                if (homeScreenView) //if homeScreenView exists already, it needs to be refreshed once again after selecting the lgort
                                    homeScreenView.getController().refreshData();

                            } catch (ex) {
                                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onChangeLgortClicked-successCallback of HomeScreenPageController', ex);
                            }
                        };
                        var passedArguments = {
                            callback: successCallback,
                            scope: this
                        };
                        AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CHANGE_LGORT_DIALOG, passedArguments); //popup to select lgort
                    } else { //if no data yet (can't select lgort) or lgort !selected, continue with normal procedures
                        callback.call(callbackScope, initialSyncWasAlreadyDone);
                    }
                };
                var newArguments = [];
                newArguments[0] = newCustInitCallback
                newArguments[1] = this;

                this.callParent(newArguments);

            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeRuntimeCustomizing of BaseClientStateController', ex);

                if (callback)
                    callback.call(callbackScope, false);
            }
        },
        logOutUser: function (callback, callbackScope) {
            try {

                var mainView = AssetManagement.customer.core.Core.getMainView().getViewModel().set('selectedLgort', null);
                this.callParent(arguments);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeRuntimeCustomizing of BaseClientStateController', ex);

                if (callback)
                    callback.call(callbackScope, false);
            }
        }
    }

});