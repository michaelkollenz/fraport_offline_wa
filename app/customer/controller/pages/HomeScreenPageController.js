Ext.define('AssetManagement.customer.controller.pages.HomeScreenPageController', {
	extend: 'AssetManagement.base.controller.pages.BaseHomeScreenPageController',
	alias: 'controller.CustomerHomeScreenPageController',


	config: {
	    contextReadyLevel: 1
	},

    startBuildingPageContext: function (argumentsObject) {
        try {
            var selectedLgort = AssetManagement.customer.core.Core.getMainView().getViewModel().get('selectedLgort'); //take the global selectedLgort and assign to the viewModel of the homescreenpage
            this.getViewModel().set('selectedLgort', selectedLgort);
            this.waitForDataBaseQueue();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOrderButtonClick of BaseHomeScreenPageController', ex);
        }
    },

    onWEButtonClick: function(button, e, eOpts) {
        try {
            var selectedLgort = this.getViewModel().get('selectedLgort');
            if (selectedLgort) {
                //argumentobject needs to be transfered as empyt object, and not undefined.
                //If it was undefined, it would not be able to modify it for GoBack compatibility int he view.
                //For further explanation, see
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WE_LIST, { } );
            }else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onWEButtonClick of HomeScreenPageController', ex);
        }
    },

    onWAButtonClick: function(button, e, eOpts) {
        try {
            var selectedLgort = this.getViewModel().get('selectedLgort');
            if(selectedLgort) {
                AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_WA_CREATE);
            }else {
                var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
                AssetManagement.customer.core.Core.getMainView().showNotification(notification);
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onWAButtonClick of HomeScreenPageController', ex);
        }
    },

  onStockButtonClick: function(button, e, eOpts) {
    try {
      var selectedLgort = this.getViewModel().get('selectedLgort');
      if(selectedLgort) {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_MAT_STOCK);
      }else {
        var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
        AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStockButtonClick of HomeScreenPageController', ex);
    }
  },

  onIVButtonClick: function(button, e, eOpts) {
    try {
      // var selectedLgort = this.getViewModel().get('selectedLgort');
      // if(selectedLgort) {
      AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_INVENTORY_LIST);
      // }else {
      //   var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
      //   AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      // }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onWAButtonClick of HomeScreenPageController', ex);
    }
  },

  onDeliveryButtonClick: function(button, e, eOpts) {
    try {
      // var selectedLgort = this.getViewModel().get('selectedLgort');
      // if(selectedLgort) {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_DELIVERY);
      // }else {
      //   var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
      //   AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      // }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStockButtonClick of HomeScreenPageController', ex);
    }
  },

  onPickupButtonClick: function(button, e, eOpts) {
    try {
      // var selectedLgort = this.getViewModel().get('selectedLgort');
      // if(selectedLgort) {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_PICKUP);
      // }else {
      //   var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
      //   AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      // }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStockButtonClick of HomeScreenPageController', ex);
    }
  },

  onSelfDispositionButtonClick: function(button, e, eOpts) {
    try {
      // var selectedLgort = this.getViewModel().get('selectedLgort');
      // if(selectedLgort) {
        AssetManagement.customer.core.Core.setMode(AssetManagement.customer.controller.NavigationController.MODE_SELF_DISPOSITION);
      // }else {
      //   var notification = new AssetManagement.customer.model.helper.ReturnMessage.returnMessage(Locale.getMsg('selectLgort'), false, 2);
      //   AssetManagement.customer.core.Core.getMainView().showNotification(notification);
      // }
    } catch(ex) {
      AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onStockButtonClick of HomeScreenPageController', ex);
    }
  },

    onCreateOptionMenu: function() {
        try {
            this._optionMenuItems = [];

            var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();
          if(funcParaInstance.getValue1For('mm_active') == 'X') {
            this._optionMenuItems.push(AssetManagement.customer.controller.ToolbarController.OPTION_LGORT_CHANGE);
          }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onCreateOptionMenu of WeListPageController', ex);
        }
    },

    onOptionMenuItemSelected: function(optionID) {
        try {
            if(optionID === AssetManagement.customer.controller.ToolbarController.OPTION_LGORT_CHANGE) {
                this.onChangeLgortClicked();
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onOptionMenuItemSelected of WeListPageController', ex);
        }
    },

    onChangeLgortClicked: function () {
        try {
            var myModel = this.getViewModel();
            var me = this;

            var successCallback = function (selectedLgort) {
                try {
                    AssetManagement.customer.core.Core.getMainView().getViewModel().set('selectedLgort', selectedLgort);
                    myModel.set('selectedLgort', selectedLgort)
                    me.refreshView();

                } catch (ex) {
                    AssetManagement.helper.OxLogger.logException('Exception occurred inside onChangeLgortClicked-successCallback of HomeScreenPageController', ex);
                }
            };
            var passedArguments = {
                callback: successCallback,
                scope: this
            };

            AssetManagement.customer.core.Core.showDialog(AssetManagement.customer.controller.NavigationController.CHANGE_LGORT_DIALOG, passedArguments);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside onChangeLgortClicked of HomeScreenPageController', ex);
        }
    },

    buttonSize: function() {
        try {
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buttonSize of BaseHomeScreenPageController', ex);
        }
    }

});