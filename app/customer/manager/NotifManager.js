Ext.define('AssetManagement.customer.manager.NotifManager', {
    extend: 'AssetManagement.base.manager.BaseNotifManager'
    //inheritableStatics: {
    ////protected
    ////@override
    ////returns the primary managed database store of the manager
    ////must be implemented when manual completion is used
    //getDataBaseStore: function () {
    //    return 'D_NOTIHEAD';
    //},

    ////begin region for manual completion management

    ////protected
    ////@override
    ////returns, if the managed business object requires manual completion
    ////by default this is false
    ////override to switch on A/C/R logic (partial also possible)
    //requiresManualCompletion: function () {
    //    return true;
    //},

    ////protected
    ////@override
    ////sets the passed notif completed
    //performCompletion: function (notif, callback) {
    //    try {
    //        var me = this;
    //        var calleeName = arguments.callee.name;

    //        var saveNotifHeadCallBack = function (success) {
    //            try {
    //                if (success) {
    //                    me.setDepedentObjectsCompleted(notif, callback);
    //                } else {
    //                    callback(false);
    //                }
    //            } catch (ex) {
    //                AssetManagement.customer.helper.OxLogger.logException(calleeName + '-saveNotifHeadCallBack', me.getClassName(), ex);
    //                callback(false);
    //            }
    //        };

    //        var eventId = this.saveNotifHead(notif);

    //        if (eventId > -1) {
    //            AssetManagement.customer.controller.EventController.getInstance().registerOnEventForOneTime(eventId, saveNotifHeadCallBack);
    //        } else {
    //            callback(false);
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    //        callback(false);
    //    }
    //},

    ////protected
    ////sets the passed notif sub items completed
    //setDepedentObjectsCompleted: function (notif, callback) {
    //    if (!callback)
    //        return;

    //    try {
    //        var me = this;
    //        var calleName = arguments.callee.name;

    //        var eventController = AssetManagement.customer.controller.EventController.getInstance();

    //        var eventIds = [];
    //        var pendingRequests = 0;
    //        var errorOccurred = false;
    //        var reported = false;

    //        var completeFunction = function () {
    //            if (errorOccurred === true && reported === false || pendingRequests === 0 && errorOccurred === false) {
    //                reported = true;
    //                callback.call(this, !errorOccurred);
    //            }
    //        };

    //        //process callback for every
    //        var requestCallback = function (success) {
    //            try {
    //                errorOccurred = errorOccurred || !success;

    //                pendingRequests--;
    //            } catch (ex) {
    //                AssetManagement.customer.helper.OxLogger.logException(calleeName + '-requestCallback', me.getClassName(), ex);
    //            } finally {
    //                completeFunction();
    //            }
    //        };

    //        var qmnum = notif.get('qmnum');

    //        //begin notif items
    //        var notifItems = notif.get('notifItems');

    //        if (notifItems) {
    //            notifItems.each(function (notifItem) {
    //                eventIds.push(AssetManagement.customer.manager.NotifItemManager.tryMarkAsCompleted(notifItem));
    //            }, this);
    //        }

    //        var keyMap = Ext.create('Ext.util.HashMap');
    //        keyMap.add('QMNUM', qmnum);

    //        eventIds.push(AssetManagement.customer.manager.NotifItemManager.markPendingDeletionsAsReady(keyMap));
    //        //end notif items

    //        //register on all valid ids
    //        if (eventIds.length > 0) {
    //            Ext.Array.each(eventIds, function (eventId) {
    //                if (eventId > -1) {
    //                    pendingRequests++;

    //                    eventController.registerOnEventForOneTime(eventId, requestCallback);
    //                } else {
    //                    errorOccurred = true;
    //                    //do not stop, because if the first is an error, the other ones might get stuck in the database queue!
    //                }
    //            }, this);

    //            if (pendingRequests > 0) {
    //                AssetManagement.customer.core.Core.getDataBaseHelper().doExecuteCommandQueue();
    //            } else {
    //                completeFunction();
    //            }
    //        } else {
    //            //no dependent objects found at all
    //            completeFunction();
    //        }
    //    } catch (ex) {
    //        AssetManagement.customer.helper.OxLogger.logException(arguments.callee.name, this.getClassName(), ex);
    //        callback.call(this, false);
    //    }
    //}

    ////end region for manual completion management
    //}
});
