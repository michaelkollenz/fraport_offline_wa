Ext.define('AssetManagement.customer.manager.MiFieldManager', {
    extend: 'AssetManagement.base.manager.BaseOxBaseManager',
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.MiField',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store'
    ],

    inheritableStatics: {

        //public methods
        getAllMiField: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MiField',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildMiProcessStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiProcess of MiFieldManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MI_PROC_F', null);
                 AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_PROC_F', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiProcess of MiFieldManager', ex);
                retval = -1;
            }

            return retval;
        },

        getSpecificMiField: function (mi_process,useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var miProcess = null;

                var me = this;
                var successCallback = function (eventArgs) {
                  try {
                    var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                    me.buildMiProcessStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                    if (done) {
                      eventController.requestEventFiring(retval, theStore);
                    }
                  } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiProcess of MiFieldManager', ex);
                    eventController.requestEventFiring(retval, undefined);
                  }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('MI_PROCESS', mi_process);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MI_PROC_F', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_PROC_F', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMiProcess of MiFieldManager', ex);
                retval = -1;
            }

            return retval;
        },

        buildMiProcessStoreFromDataBaseQuery: function(store, eventArgs) {
            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var miProcess = this.buildMiProcessFromDbResultObject(cursor.value);
                        store.add(miProcess);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessStoreFromDataBaseQuery of MiFieldManager', ex);
            }
        },

        buildMiProcessFromDataBaseQuery: function( eventArgs) {
            var retval = null;
            try {
                if(eventArgs) {
                    if(eventArgs.target.result) {
                        retval = this.buildMiProcessFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessStoreFromDataBaseQuery of MiFieldManager', ex);
            }
            return retval;
        },

        //builds a single material conf. for a database record
        buildMiProcessFromDbResultObject: function(dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.MiField', {
                    mi_process:dbResult['MI_PROCESS'],
                    mi_descript:dbResult['MI_DESCRIPT'],
                    rel_field: dbResult['REL_FIELD'],
                    fieldname: dbResult['FIELDNAME'],
                    relevance: dbResult['RELEVANCE'],
                    check_value: dbResult['CHECK_VALUE']
                });
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessFromDbResultObject of MiFieldManager', ex);
            }
            return retval;
        }
    }
});