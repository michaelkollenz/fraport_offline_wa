Ext.define('AssetManagement.customer.manager.LgortManager', {
    extend: 'AssetManagement.customer.manager.OxBaseManager',

    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.Lgort',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.util.HashMap',
        'Ext.data.Store'
    ],

    inheritableStatics:{
        //public
        getLgorts: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var fromDataBase = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.Lgort',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;

                        me.buildLgortStoreFromDataBaseQuery.call(me, fromDataBase, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, fromDataBase);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStorageLocations of LgortManager', ex);
                        eventController.fireEvent(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MI_STORAGE_LOC', null);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_STORAGE_LOC', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getStorageLocations of LgortManager', ex);
                retval = -1;
            }
            return retval;
        },

        //private
        buildLgortStoreFromDataBaseQuery: function (store, eventArgs) {
            try {
                if (eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var lgort = this.buildLgortFromDbResultObject(cursor.value);
                        store.add(lgort);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildLgortStoreFromDataBaseQuery of LgortManager', ex);
            }
        },

        //private
        buildLgortFromDbResultObject: function (dbResult) {
            var retval = null;
            try {
                retval = Ext.create('AssetManagement.customer.model.bo.Lgort', {
                    werks: dbResult['WERKS'],
                    lgort: dbResult['LGORT'],
                    lgobe: dbResult['LGOBE'],
                    kokrs: dbResult['KOKRS'],
                    werksbez: dbResult['WERKS_BEZ']
                });

                retval.set('id', dbResult['WERKS'] + dbResult['LGORT']);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildLgortFromDbResultObject of LgortManager', ex);
            }

            return retval;
        }
    }
});