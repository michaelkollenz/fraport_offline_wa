Ext.define('AssetManagement.customer.manager.MiGrundManager', {
    extend: 'AssetManagement.base.manager.BaseOxBaseManager',
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.MiGrund',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store'
    ],

    inheritableStatics: {

        //public methods
        getAllMiGrund: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MiGrund',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildMiProcessStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiGrund of MiGrundManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MI_PROC_G', null);
                 AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_PROC_G', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiGrund of MiGrundManager', ex);
                retval = -1;
            }

            return retval;
        },

        getspecificMiGrund: function (bwart,useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();


              var theStore = Ext.create('Ext.data.Store', {
                model: 'AssetManagement.customer.model.bo.MiGrund',
                autoLoad: false
              });

                var me = this;
                var successCallback = function (eventArgs) {
                  try {
                    var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                    me.buildMiProcessStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                    if (done) {
                      eventController.requestEventFiring(retval, theStore);
                    }
                  } catch (ex) {
                    AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getspecificMiGrund of MiGrundManager', ex);
                    eventController.requestEventFiring(retval, undefined);
                  }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('bwart', bwart);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MI_PROC_G', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_PROC_G', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getspecificMiGrund of MiGrundManager', ex);
                retval = -1;
            }

            return retval;
        },

        buildMiProcessStoreFromDataBaseQuery: function(store, eventArgs) {
            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var miProcess = this.buildMiProcessFromDbResultObject(cursor.value);
                        store.add(miProcess);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessStoreFromDataBaseQuery of MiGrundManager', ex);
            }
        },

        buildMiProcessFromDataBaseQuery: function( eventArgs) {
            var retval = null;
            try {
                if(eventArgs) {
                    if(eventArgs.target.result) {
                        retval = this.buildMiProcessFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessStoreFromDataBaseQuery of MiGrundManager', ex);
            }
            return retval;
        },

        //builds a single material conf. for a database record
        buildMiProcessFromDbResultObject: function(dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.MiGrund', {
                  bwart:dbResult['BWART'],
                  grund:dbResult['GRUND'],
                  grtxt: dbResult['GRTXT']
                });
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessFromDbResultObject of MiGrundManager', ex);
            }
            return retval;
        }
    }
});