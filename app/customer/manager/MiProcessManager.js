Ext.define('AssetManagement.customer.manager.MiProcessManager', {
    extend: 'AssetManagement.base.manager.BaseOxBaseManager',
    requires: [
        'AssetManagement.customer.helper.OxLogger',
        'AssetManagement.customer.model.bo.MiProcess',
        'AssetManagement.customer.manager.KeyManager',
        'AssetManagement.customer.manager.MaterialManager',
        'AssetManagement.customer.helper.CursorHelper',
        'AssetManagement.customer.utils.StringUtils',
        'Ext.data.Store'
    ],

    inheritableStatics: {

        //public methods
        getAllMiProcess: function (useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var theStore = Ext.create('Ext.data.Store', {
                    model: 'AssetManagement.customer.model.bo.MiProcess',
                    autoLoad: false
                });

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        var done = !eventArgs || !eventArgs.target || !eventArgs.target.result;
                        me.buildMiProcessStoreFromDataBaseQuery.call(me, theStore, eventArgs);

                        if (done) {
                            eventController.requestEventFiring(retval, theStore);
                        }
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiProcess of MiProcessManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbKeyRange('C_MI_PROC', null);
                 AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_PROC', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiProcess of MiProcessManager', ex);
                retval = -1;
            }

            return retval;
        },

        getMiProcess: function (mi_process,useBatchProcessing) {
            var retval = -1;

            try {
                var eventController = AssetManagement.customer.controller.EventController.getInstance();
                retval = eventController.getNextEventId();

                var miProcess = null;

                var me = this;
                var successCallback = function (eventArgs) {
                    try {
                        miProcess = me.buildMiProcessFromDataBaseQuery(eventArgs);
                        eventController.requestEventFiring(retval, miProcess);
                    } catch (ex) {
                        AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getAllMiProcess of MiProcessManager', ex);
                        eventController.requestEventFiring(retval, undefined);
                    }
                };

                var keyMap = Ext.create('Ext.util.HashMap');
                keyMap.add('MI_PROCESS', mi_process);

                var keyRange = AssetManagement.customer.helper.DbKeyRangeHelper.getDbOnlyKeyRange('C_MI_PROC', keyMap);
                AssetManagement.customer.core.Core.getDataBaseHelper().query('C_MI_PROC', keyRange, successCallback, null, useBatchProcessing);
            } catch (ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getMiProcess of MiProcessManager', ex);
                retval = -1;
            }

            return retval;
        },

        buildMiProcessStoreFromDataBaseQuery: function(store, eventArgs) {
            try {
                if(eventArgs && eventArgs.target) {
                    var cursor = eventArgs.target.result;
                    if (cursor) {
                        var miProcess = this.buildMiProcessFromDbResultObject(cursor.value);
                        store.add(miProcess);

                        AssetManagement.customer.helper.CursorHelper.doCursorContinue(cursor);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessStoreFromDataBaseQuery of MiProcessManager', ex);
            }
        },

        buildMiProcessFromDataBaseQuery: function( eventArgs) {
            var retval = null;
            try {
                if(eventArgs) {
                    if(eventArgs.target.result) {
                        retval = this.buildMiProcessFromDbResultObject(eventArgs.target.result.value);
                    }
                }
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessStoreFromDataBaseQuery of MiProcessManager', ex);
            }
            return retval;
        },

        //builds a single material conf. for a database record
        buildMiProcessFromDbResultObject: function(dbResult) {
            var retval = null;

            try {
                retval = Ext.create('AssetManagement.customer.model.bo.MiProcess', {
                    mi_process:dbResult['MI_PROCESS'],
                    mi_descript:dbResult['MI_DESCRIPT'],
                    bwart: dbResult['BWART'],
                    bw_descript: dbResult['BW_DESCRIPT'],
                    gm_code: dbResult['GM_CODE'],
                    kzbew: dbResult['KZBEW'],
                    rel_field: dbResult['REL_FIELD'],
                    fieldname: dbResult['FIELDNAME'],
                    sobkz: dbResult['SOBKZ'],
                    sobkz_txt: dbResult['SOBKZ_TXT'],
                    busobject: dbResult['BUSOBJECT'],
                  planned_items: dbResult['PLANNED_ITEMS'],
                    kzgru: dbResult['KZGRU'],
                  man_entries: dbResult['MAN_ENTRIES']
                });
            } catch(ex) {
                AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildMiProcessFromDbResultObject of MiProcessManager', ex);
            }
            return retval;
        }
    }
});