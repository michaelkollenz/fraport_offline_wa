Ext.define('AssetManagement.customer.view.dialogs.CancelableProgressDialog', {
    extend: 'AssetManagement.base.view.dialogs.BaseCancelableProgressDialog',
    alias: 'widget.cancelableprogressdialog',


    config: {
        // supportsDynamicWidth: true,
        minWidth: 200
        // width: 300,
    }

});