Ext.define('AssetManagement.customer.view.MainView', {
    extend: 'AssetManagement.base.view.BaseMainView',
    alias: 'widget.mainview',


    generateMainMenuPanel: function() {
        var retval = null;

        try {
            var retval = Ext.create('Ext.panel.Panel', {
                id: 'mainMenuPanel',
                cls: 'oxMainMenuPanel',
                itemId: 'menuPanel',
                overflowY: 'auto',
                width: 0,
                header: false,
                manageHeight: false,
                title: 'Menu',
                plugins: 'responsive',
                responsiveConfig: {
                    'width <= 800': {
                        visible: false
                    },
                    'width > 800': {
                        visible: true
                    }
                },

                items: [
                    {
                        xtype: 'menu',
                        flex: 1,
                        floating: false,
                        itemId: 'menu',
                        id: 'mainMenu',
                        margin: '',
                        padding: '',
                        width: '',
                        bodyBorder: true,
                        manageHeight: false,
                        items: [
                            {
                                xtype: 'oxmainmenuitem',
                                border: 8,
                                cls: 'oxMainMenuItem',
                                frame: true,
                                height: 55,
                                itemId: 'WE',
                                id: 'weMenuItem',
                                // hidden: true,
                                bind: {
                                    html: '<div><img src="resources/icons/demand_requirement.png" /><div class="font">'+Locale.getMsg('WE')+'</div></div>'
                                }
                            },
                            {
                                xtype: 'menuseparator',
                                frame: true,
                                id: 'weMenuItemSep',
                                visible: false,
                                padding: 0
                            },
                            {
                                xtype: 'oxmainmenuitem',
                                border: 8,
                                cls: 'oxMainMenuItem',
                                frame: true,
                                height: 55,
                                itemId: 'WA',
                                id: 'waMenuItem',
                                // hidden: true,
                                bind: {
                                    html: '<div><img src="resources/icons/object_list.png" /><div class="font">'+Locale.getMsg('WA')+'</div></div>'
                                }
                            },
                            {
                                xtype: 'menuseparator',
                                frame: true,
                                id: 'waMenuItemSep',
                                visible: false,
                                padding: 0
                            },
                            {
                                xtype: 'oxmainmenuitem',
                                border: 8,
                                cls: 'oxMainMenuItem',
                                frame: true,
                                height: 55,
                                itemId: 'settings',
                                id: 'settingsMenuItem',
                                visible: false,
                                bind: {
                                    html: '<div><img src="resources/icons/settings.png" />   <div class="font">'+Locale.getMsg('settings')+'</div></div>'
                                }
                            },
                            {
                              xtype: 'menuseparator',
                              frame: true,
                              id: 'ivMenuItemSep',
                              visible: false,
                              padding: 0
                            },
                            {
                              xtype: 'oxmainmenuitem',
                              border: 8,
                              cls: 'oxMainMenuItem',
                              frame: true,
                              height: 55,
                              itemId: 'IV',
                              id: 'ivMenuItem',
                              visible: false,
                              bind: {
                                html: '<div><img src="resources/icons/inventory.png" />   <div class="font">'+Locale.getMsg('inventory')+'</div></div>'
                              }
                            },
                            {
                                xtype: 'menuseparator',
                                frame: true,
                                id: 'settingsMenuItemSep',
                                visible: false,
                                padding: 0
                            },
                            {
                                xtype: 'oxmainmenuitem',
                                border: 8,
                                cls: 'oxMainMenuItem',
                                frame: true,
                                height: 55,
                                id: 'sync',
                                itemId: 'sync',
                                id: 'syncMenuItem',
                                bind: {
                                    html: '<div><img src="resources/icons/btsync.png" />   <div class="font">'+Locale.getMsg('sync')+'</div></div>'
                                }
                            },
                            {
                                xtype: 'menuseparator',
                                frame: true,
                                id: 'syncMenuItemSep',
                                padding: 0
                            },
                            {
                                xtype: 'oxmainmenuitem',
                                border: 8,
                                cls: 'oxMainMenuItem',
                                frame: true,
                                height: 55,
                                itemId: 'stacktrace',
                                id: 'stacktraceMenuItem',
                                bind: {
                                    html: '<div><img src="resources/icons/stack_trace.png" />   <div class="font">'+Locale.getMsg('stackTrace')+'</div></div>'
                                }
                            },
                            {
                                xtype: 'menuseparator',
                                frame: true,
                                id: 'stacktraceMenuItemSep',
                                padding: 0
                            },
                            {
                                xtype: 'oxmainmenuitem',
                                border: 8,
                                cls: 'oxMainMenuItem',
                                frame: true,
                                height: 55,
                                itemId: 'logoff',
                                id: 'logoffMenuItem',
                                padding: '',
                                bind: {
                                    html: '<div><img src="resources/icons/logout.png" />   <div class="font">'+Locale.getMsg('logout')+'</div></div>'
                                }
                            }
                        ],
                        listeners: {
                            click: {
                                fn: this.getController().onMainMenuItemClick,
                                scope: this.getController()
                            }
                        }
                    }
                ]
            });
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateMainMenuPanel of BaseMainView', ex);
        }

        return retval;
    },


    manageSections: function() {
        try {
            var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();

          // if (funcParaInstance.getValue1For('mm_active') == 'X'){
          //   Ext.getCmp('weMenuItem').setHidden(true);
          //   Ext.getCmp('weMenuItemSep').setHidden(true);
          // }else{
          //   Ext.getCmp('weMenuItem').setHidden(false);
          //   Ext.getCmp('weMenuItemSep').setHidden(false);
          // }
          if(funcParaInstance.getValue1For('we_active') == 'X') {
            Ext.getCmp('weMenuItem').setHidden(false);
            Ext.getCmp('weMenuItemSep').setHidden(false);
          }else{
            Ext.getCmp('weMenuItem').setHidden(true);
            Ext.getCmp('weMenuItemSep').setHidden(true);
          }
          if(funcParaInstance.getValue1For('wa_active') == 'X') {
            Ext.getCmp('waMenuItem').setHidden(false);
            Ext.getCmp('waMenuItemSep').setHidden(false);
          }else{
            Ext.getCmp('waMenuItem').setHidden(true);
            Ext.getCmp('waMenuItemSep').setHidden(true);
          }
          // if(funcParaInstance.getValue1For('bst_active') == 'X') {
          //   bstButton.setVisible(true);
          // }else{
          //   bstButton.setVisible(false);
          // }
          if(funcParaInstance.getValue1For('inventory_active') == 'X') {
            Ext.getCmp('ivMenuItem').setHidden(false);
            Ext.getCmp('ivMenuItemSep').setHidden(false);
          }else{
            Ext.getCmp('ivMenuItem').setHidden(true);
            Ext.getCmp('ivMenuItemSep').setHidden(true);
          }

            //settings are not yet implemented, so do not show
            //if(funcParaInstance.get('settings_active') != true) {
            Ext.getCmp('settingsMenuItem').setHidden(true);
            Ext.getCmp('settingsMenuItemSep').setHidden(true);
            /*} else {
             Ext.getCmp('settingsMenuItem').setHidden(false);
             Ext.getCmp('settingsMenuItemSep').setHidden(false);
             }*/

            //'orderMenuItemSep','orderMenuItem',
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside manageSections of MainView', ex);
        }
    }
});
