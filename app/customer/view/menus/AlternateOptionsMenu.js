Ext.define('AssetManagement.customer.view.menus.AlternateOptionsMenu', {
	extend: 'AssetManagement.base.view.menus.BaseAlternateOptionsMenu',
    alias: 'menu.AlternateOptionsMenu',


    generateMenuItems: function() {
        try {
            var myItems = Ext.create('Ext.util.HashMap');



            var menuItem = Ext.create('Ext.menu.Item', {
                icon: 'resources/icons/documentOk.png',
                iconCls: 'oxMenuItemIcon',
                cls: 'oxMenuItemWithIcon',
                text: Locale.getMsg('commit'),
                id: 'commitMovementsAOM',
                listeners: {
                    click: 'onMenuItemSelected'
                }
            });
            myItems.add('commitMovementsAOM', menuItem);


            this._myMenuItems = myItems;

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateMenuItems of AlternateOptionsMenu', ex);
        }
    }


});