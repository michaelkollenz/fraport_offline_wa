Ext.define('AssetManagement.customer.view.pages.HomeScreenPage', {
    extend: 'AssetManagement.base.view.pages.BaseHomeScreenPage',
    alias: 'widget.pageshomescreenpage',



    controller: 'CustomerHomeScreenPageController',

    buildUserInterface: function() {
        try {
            var items = [
                {
                    xtype: 'container',
                    margin: '60 0 80 0',
                    layout: {
                        type: 'vbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'image',
                            height: 84,
                            width: 302,
                            src: 'resources/icons/home_screen_logo.png',
                            id: 'home_logo',
                            responsiveConfig: {
                                'width < 600': {
                                    height: 66,
                                    width: 237
                                },
                                'width > 1100': {
                                    height: 84,
                                    width: 302
                                },
                                'width > 600 && width < 1100': {
                                    height: 73,
                                    width: 262
                                }
                            },
                            plugins: [
                                {
                                    ptype: 'responsive'
                                }
                            ]
                        }
                        //CUSTOMER LOGO
//		                ,
//		                {
//			            xtype: 'component',
//			            height: 15
//			            },
//		                {
//		                    xtype: 'image',
//		                    height: 83,
//		                    width: 299,
//		                    src: 'resources/icons/customer_logo.jpg',
//		                    id: 'customer_logo',
//	                        responsiveConfig: {
//                            	'width < 600': {
//	                                height: 66,
//	                                width: 237
//		                        },
//		                        'width > 1100': {
//	                                height: 83,
//	                                width: 299
//		                        },
//		                        'width > 600 && width < 1100': {
//		                            height: 72,
//		                            width: 259
//		                        }
//			            	},
//			            	plugins: [
//			                    {
//			                        ptype: 'responsive'
//			                    }
//			                ]
//		                }
                    ],
                    responsiveConfig: {
                        'height < 600': {
                            margin: '30 0 40 0'
                        },
                        'height >= 600': {
                            margin: '60 0 80 0'
                        }
                    },
                    plugins: [
                        {
                            ptype: 'responsive'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    id: 'lgortContainer',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'center'
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    id: 'selectedLgortLabel',
                                    flex: 1
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    id: 'mmButtonContainer',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: {
                                type: 'vbox',
                                align: 'center'
                            },
                            items: [
                                {
                                    xtype: 'label',
                                    id: 'mmSelectedLgortLabel',
                                    flex: 1
                                },
                                {
                                    xtype: 'button',
                                    html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/MM/Wareneingang.png"></img></div><p class="schwarz">'+Locale.getMsg('WE')+'</p>',
                                    height: 200,
                                    id: 'mmHomeScreenWEButton',
                                    maxHeight: '200',
                                    maxWidth: '176',
                                    minHeight: '135',
                                    minWidth: '119',
                                    width: 176,
                                    text: '',
                                    listeners: {
                                        click: 'onWEButtonClick'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/MM/Warenausgang.png"></img></div><p class="schwarz">'+Locale.getMsg('WA')+'</p>',
                                    height: 200,
                                    id: 'mmHomeScreenWAButton',
                                    maxHeight: '200',
                                    maxWidth: '176',
                                    minHeight: '135',
                                    minWidth: '130',
                                    width: 176,
                                    text: '',
                                    listeners: {
                                        click: 'onWAButtonClick'
                                    }
                                },
                                {
                                  xtype: 'button',
                                  html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/Buttons_forklift_gruen.png"></img></div><p class="schwarz">'+Locale.getMsg('WA')+'</p>',
                                  height: 200,
                                  id: 'mmHomeScreenStockButton',
                                  maxHeight: '200',
                                  maxWidth: '176',
                                  minHeight: '135',
                                  minWidth: '130',
                                  width: 176,
                                  text: '',
                                  listeners: {
                                    click: 'onStockButtonClick'
                                  }
                                }
                            ]
                        }
                    ]
                },
              {
                xtype: 'container',
                id: 'iaButtonContainer',
                layout: {
                  type: 'hbox',
                  align: 'stretch'
                },
                items: [
                  {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                      type: 'vbox',
                      align: 'center'
                    },
                    items: [
                      {
                        xtype: 'label',
                        id: 'iaSelectedLgortLabel',
                        flex: 1
                      },
                      {
                        xtype: 'button',
                        html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/button_banf_home.png"></img></div><p class="schwarz">'+Locale.getMsg('delivery')+'</p>',
                        height: 200,
                        id: 'iaHomeScreenDeliveryButton',
                        maxHeight: '200',
                        maxWidth: '176',
                        minHeight: '135',
                        minWidth: '119',
                        width: 176,
                        text: '',
                        listeners: {
                          click: 'onDeliveryButtonClick'
                        }
                      },
                      {
                        xtype: 'button',
                        html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/Buttons_forklift_gruen.png"></img></div><p class="schwarz">'+Locale.getMsg('pickUp')+'</p>',
                        height: 200,
                        id: 'iaHomeScreenPickupButton',
                        maxHeight: '200',
                        maxWidth: '176',
                        minHeight: '135',
                        minWidth: '130',
                        width: 176,
                        text: '',
                        listeners: {
                          click: 'onPickupButtonClick'
                        }
                      },
                      {
                        xtype: 'button',
                        html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/documentOk.png"></img></div><p class="schwarz">'+Locale.getMsg('selfDisposition')+'</p>',
                        height: 200,
                        id: 'iaHomeScreenSelfDispositionButton',
                        maxHeight: '200',
                        maxWidth: '176',
                        minHeight: '135',
                        minWidth: '130',
                        width: 176,
                        text: '',
                        listeners: {
                          click: 'onSelfDispositionButtonClick'
                        }
                      }
                    ]
                  }
                ]
              },
              {
                xtype: 'container',
                id: 'ivButtonContainer',
                layout: {
                  type: 'hbox',
                  align: 'stretch'
                },
                items: [
                  {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                      type: 'vbox',
                      align: 'center'
                    },
                    items: [
                      {
                        xtype: 'button',
                        html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/inventory.png"></img></div><p class="schwarz">'+Locale.getMsg('inventory')+'</p>',
                        height: 200,
                        id: 'ivHomeScreenButton',
                        maxHeight: '200',
                        maxWidth: '176',
                        minHeight: '135',
                        minWidth: '119',
                        width: 176,
                        text: '',
                        listeners: {
                          click: 'onIVButtonClick'
                        }
                      }
                    ]
                  }
                ]
              },
                {
                    xtype: 'container',
                    id: 'syncButton',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'component',
                            height: 50
                        },
                        {
                            xtype: 'button',
                            html: '<div><img class="resizeButtonHomeScreen" src="resources/icons/Button_Syncx2.png"></img></div><p class="schwarz">'+Locale.getMsg('sync')+'</p><style type="text/css">\n<!--\n</style>',
                            id: 'homeScreenSyncButton',
                            height: 200,
                            width: 176,
                            maxHeight: '200',
                            maxWidth: '176',
                            minHeight: '135',
                            minWidth: '130',
                            itemId: 'sync',
                            listeners: {
                                click: 'onSyncButtonClick'
                            }
                        }
                    ]
                },
                {
                    xtype: 'component',
                    height: 20
                }

            ];

            this.add(items);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside buildUserInterface of HomeScreenPage', ex);
        }
    },

    //@override
    updatePageContent: function() {
        try {
            //read customizing parameter and remove unneccessary buttons
            //or show sync button, if there is no data for this user yet
            var userInfo = AssetManagement.customer.model.bo.UserInfo.getInstance();
            var scenario = userInfo.get('mobileScenario');
            var funcParaInstance = AssetManagement.customer.model.bo.FuncPara.getInstance();

            var mmButtonCont  = Ext.getCmp('mmButtonContainer');
            var iaButtonCont  = Ext.getCmp('iaButtonContainer');
            var lgortContainer  = Ext.getCmp('lgortContainer');
            var syncButton = Ext.getCmp('syncButton');
            var weButton  = Ext.getCmp('mmHomeScreenWEButton');
            var waButton  = Ext.getCmp('mmHomeScreenWAButton');
            var bstButton  = Ext.getCmp('mmHomeScreenStockButton');
            var invButton  = Ext.getCmp('ivHomeScreenButton');


            lgortContainer.setVisible(false);

            if(!userInfo.get('isInitialized')) {
                mmButtonCont.setHidden(true);
                iaButtonCont.setHidden(true);
                syncButton.setVisible(true);
            } else {
              syncButton.setVisible(false);
              iaButtonCont.setVisible(false);
              mmButtonCont.setVisible(false);
              if (funcParaInstance.getValue1For('mm_active') == 'X') {
                mmButtonCont.setHidden(false);
                lgortContainer.setHidden(false);
                iaButtonCont.setVisible(false);
              } else if (funcParaInstance.getValue1For('ia_active') == 'X') {
                iaButtonCont.setHidden(false);
                mmButtonCont.setVisible(false);
                lgortContainer.setVisible(false);
              }
            }
            if(funcParaInstance.getValue1For('we_active') == 'X') {
                weButton.setVisible(true);
              }else{
                weButton.setVisible(false);
            }
            if(funcParaInstance.getValue1For('wa_active') == 'X') {
              waButton.setVisible(true);
            }else{
              waButton.setVisible(false);
            }
            if(funcParaInstance.getValue1For('bst_active') == 'X') {
              bstButton.setVisible(true);
            }else{
              bstButton.setVisible(false);
            }
            if(funcParaInstance.getValue1For('inventory_active') == 'X') {
              invButton.setVisible(true);
            }else{
              invButton.setVisible(false);
            }

            var myLabel =Ext.getCmp('selectedLgortLabel');
            var selectedLgort = this.getViewModel().get('selectedLgort');
            if(myLabel){
              if(selectedLgort){
                myLabel.setText(selectedLgort.get('werks')+' / '+selectedLgort.get('lgort') + ' - ' +selectedLgort.get('lgobe') );
              }else{
                myLabel.setText(Locale.getMsg('selectLgort'));
              }
            }
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside updatePageContent of HomeScreenPage', ex);
        }
    }

});