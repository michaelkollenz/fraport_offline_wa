Ext.define('AssetManagement.customer.view.ToolbarView', {
    extend: 'AssetManagement.base.view.BaseToolbarView',
    alias: 'widget.toolbarview',





    generateOptionsMenuItems: function() {
        try {

            this.callParent(arguments);

            var optionMenuItems = this._optionsMenuItems;

            button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/documentOk.png"></img>',
                height: 50,
                id: 'commitMovementsOM',
                width: 50,
                icon: '',
                text: '',
                listeners: {
                    click: 'onOptionSelected'
                }
            });
            optionMenuItems.add('commitMovementsOM', button);


            button = Ext.create('Ext.button.Button', {
                html: '<img width="100%" src="resources/icons/funcloc.png"></img>',
                height: 50,
                id: 'changeLgortOM',
                width: 50,
                icon: '',
                text: '',
                listeners: {
                    click: 'onOptionSelected'
                }
            });
            optionMenuItems.add('changeLgortOM', button);

        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside generateOptionsMenuItems of ToolbarView', ex);
        }
    }
});