Ext.define('AssetManagement.customer.model.bo.MiField', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
        //key fields
        { name: 'mi_process',        type: 'string' },

        //ordinary fields
        { name: 'mi_descript',        type: 'string' },
        { name: 'rel_field',        type: 'string' },
        { name: 'fieldname',        type: 'string' },
        { name: 'relevance',        type: 'string' },
        { name: 'check_value',        type: 'string' }

    ],

    constructor: function(config) {
        this.callParent(arguments);
    }
});