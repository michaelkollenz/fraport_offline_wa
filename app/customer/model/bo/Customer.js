Ext.define('AssetManagement.customer.model.bo.Customer', {
	extend: 'AssetManagement.base.model.bo.BaseCustomer',
	
    fields: [],
   
    constructor: function(config) {
        this.callParent(arguments);
    },

     //override
    //public
    getRelevantTimeZone: function(){
        var retval = '';

        try {
            retval = this.get('timeZone');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelevantTimeZone of Customer', ex);
        }

        return retval;
    }
});