Ext.define('AssetManagement.customer.model.bo.Lgort', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
        //key fields
        { name: 'werks',        type: 'string' },
        { name: 'lgort',        type: 'string' },

        //ordinary fields
        { name: 'lgobe',        type: 'string' },
        { name: 'werksbez',     type: 'string' },
        { name: 'kokrs',        type: 'string' }

    ],

    constructor: function(config) {
        this.callParent(arguments);
    }
});