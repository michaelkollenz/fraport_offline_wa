Ext.define('AssetManagement.customer.model.bo.MiProcess', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
        //key fields
        { name: 'mi_process',        type: 'string' },

        //ordinary fields
        { name: 'mi_descript',        type: 'string' },
        { name: 'bwart',        type: 'string' },
        { name: 'bw_descript',        type: 'string' },
        { name: 'gm_code',        type: 'string' },
        { name: 'kzbew',        type: 'string' },
        { name: 'rel_field',        type: 'string' },
        { name: 'fieldname',        type: 'string' },
        { name: 'sobkz',        type: 'string' },
        { name: 'sobkz_txt',        type: 'string' },
        { name: 'busobject',        type: 'string' },
        { name: 'kzgru',        type: 'string' },
        { name: 'planned_items',        type: 'auto' },
        { name: 'man_entries',        type: 'auto' }

    ],

    constructor: function(config) {
        this.callParent(arguments);
    }
});