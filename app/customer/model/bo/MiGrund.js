Ext.define('AssetManagement.customer.model.bo.MiGrund', {
  extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

  fields: [
    //key fields
    {name: 'bwart', type: 'string'},
    {name: 'grund', type: 'string'},

    //ordinary fields
    {name: 'grtxt', type: 'string'}

  ],

  constructor: function (config) {
    this.callParent(arguments);
  }
});