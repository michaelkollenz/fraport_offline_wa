Ext.define('AssetManagement.customer.model.bo.Partner', {
	extend: 'AssetManagement.base.model.bo.BasePartner',
	
    fields: [],
    
    constructor: function(config) {
        this.callParent(arguments);
    },

    //override
    //public
    getRelevantTimeZone: function(){
        var retval = '';

        try {
            retval = this.get('timeZone');
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getRelevantTimeZone of Partner', ex);
        }
        
        return retval;
    }
});