Ext.define('AssetManagement.customer.model.bo.PoItem', {
	extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    fields: [
        //key fields
        { name: 'ebeln',        type: 'string' },
        { name: 'ebelp',        type: 'string' },

        //ordinary fields
        { name: 'txz01',        type: 'string' },
        { name: 'matnr',        type: 'string' },
        { name: 'werks',        type: 'string' },
        { name: 'lgort',        type: 'string' },
        { name: 'menge',        type: 'string' },
        { name: 'meins',        type: 'string' },
        { name: 'elikz',        type: 'auto' },
        { name: 'menge_del',    type: 'string' },
        //local only
        { name: 'item_text', type: 'string' },
        { name: 'menge_ist',    type: 'string' }
    

    ],
   
    constructor: function(config) {
        this.callParent(arguments);
    }
});