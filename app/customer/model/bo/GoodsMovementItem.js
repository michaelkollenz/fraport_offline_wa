Ext.define('AssetManagement.customer.model.bo.GoodsMovementItem', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',

    fields: [
        //key fields
        { name: 'matnr',        type: 'string' },

        //ordinary fields
        { name: 'werks',        type: 'string' },
        { name: 'lgort',        type: 'string' },
        { name: 'bwart',        type: 'string' },
        { name: 'charg',        type: 'string' },
        { name: 'menge',        type: 'string' },
        { name: 'meins',        type: 'string' },
        { name: 'ebeln',        type: 'string' },
        { name: 'ebelp',        type: 'string' },
        { name: 'wempf',        type: 'string' },
        { name: 'kdauf',        type: 'string' },
        { name: 'ablad',        type: 'string' },
        { name: 'kostl',        type: 'string' },
        { name: 'aufnr',        type: 'string' },
        { name: 'aufps',        type: 'string' },
        { name: 'rsnum',        type: 'string' },
        { name: 'rspos',        type: 'string' },
        { name: 'rsart',        type: 'string' },
        { name: 'ps_posid',     type: 'string' },
        { name: 'item_text',    type: 'string' },
        { name: 'kzbew',        type: 'string' },
        { name: 'lifnr',        type: 'string' },
        { name: 'sobkz',        type: 'string' },
        { name: 'move_reas',    type: 'string' },
        { name: 'umglo ',       type: 'string' },
        { name: 'umcha ',       type: 'string' },


        //local Fields
        { name: 'elikz',        type: 'auto' },
        { name: 'matkt',        type: 'string' },
        { name: 'prefilled',        type: 'auto' },
        { name: 'kzumw',        type: 'auto' }

    ],

    constructor: function(config) {
        this.callParent(arguments);
    }
});