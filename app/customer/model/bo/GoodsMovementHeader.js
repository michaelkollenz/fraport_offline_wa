Ext.define('AssetManagement.customer.model.bo.GoodsMovementHeader', {
    extend: 'AssetManagement.customer.model.bo.OxBusinessObject',
	
    fields: [

        { name: 'pstng_date',           type: 'string' },
        { name: 'doc_date',             type: 'string' },
        { name: 'ref_doc_no',           type: 'string' },
        { name: 'bill_of_lading',       type: 'string' },
        { name: 'gr_gi_slip_no',        type: 'string' },
        { name: 'pr_uname',             type: 'string' },
        { name: 'header_txt',           type: 'string' },
        { name: 'ver_gr_gi_slip',       type: 'string' },
        { name: 'ver_gr_gi_slipx',      type: 'string' },
        { name: 'ext_wms',              type: 'string' },
        { name: 'ref_doc_no_long',      type: 'string' },
        { name: 'bill_of_lading_long',  type: 'string' },
        { name: 'bar_code',             type: 'string' },
        { name: 'print_version',        type: 'string' },
        { name: 'gm_code',              type: 'string' }

    ],

    constructor: function(config) {
        this.callParent(arguments);
    }
});