Ext.define('AssetManagement.customer.model.dialogmodel.CancelableProgressDialogViewModel', {
    extend: 'AssetManagement.base.model.dialogmodel.BaseCancelableProgressDialogViewModel',
    alias: 'viewmodel.CancelableProgressDialogModel'
});