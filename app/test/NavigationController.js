Ext.define('AssetManagement.test.NavigationController', {
    extend: 'AssetManagement.base.controller.BaseNavigationController',
    //private
    initializeBrowserForwardBackwardHandling: function() {
        try {
            Ext.History.init();

            location.hash = 'requestBack';
            location.hash = '';
            location.hash = 'requestForward';

            //history.back();

            //queue the registration on the changed event, so it will get registered, after the initial changed events have been fired
            Ext.defer(Ext.History.on, 1000, Ext.History, [ 'change', this.onNavigationHashChanged, this ]);
        } catch(ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeBrowserForwardBackwardHandling of BaseNavigationController', ex);
        }
    }
});