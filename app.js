Ext.application({
    requires: [
	    'AssetManagement.customer.helper.OxLogger',
	    'AssetManagement.customer.controller.AppUpdateController',
	    'AssetManagement.customer.controller.AppStartUpLoadingController',
        'AssetManagement.customer.controller.NavigationController',
        'AssetManagement.customer.core.Core'
    ],

    views: [
        'AssetManagement.customer.view.MainView',
        'AssetManagement.customer.view.utils.OxGrid',
        'AssetManagement.customer.view.utils.OxGridRow'
    ],

    controllers: [
        'AssetManagement.customer.controller.NavigationController'
    ],

    name: 'AssetManagement',

    launch: function () {
        try {
            //start caching all logs for a persistent trace as long as the application is not yet ready to save those persistently
            AssetManagement.customer.helper.OxLogger.enableCachingForPersistentTrace();

            AssetManagement.customer.core.Core.setController(this.getController('AssetManagement.customer.controller.NavigationController'));


            var version = AssetManagement.customer.core.Core.getVersion()
            AssetManagement.customer.helper.OxLogger.logMessage('[CONTROL] Starting application - Version: ' + version);

            //begin with covering the update case
            AssetManagement.customer.controller.AppUpdateController.checkForAndHandleApplicationUpdate(function () {
                //after passing this step, the application may start
                AssetManagement.customer.controller.AppStartUpLoadingController.initializeApplication();
            }, this);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside launch of Main Application', ex);
        }
    }/*,

    getVersion: function () {
        var retval = null;

        try {
            retval = this._version;
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getVersion of Main Application', ex);
        }

        return retval;
    },

    getRequiredDatabaseVersion: function () {
        return this._requiredDatabaseVersion;
    },

    initializeViewPort: function () {
        try {
            this._mainView = Ext.create('AssetManagement.customer.view.MainView');
            AssetManagement.customer.controller.ClientStateController.initializeAutoLogout();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside initializeViewPort of Main Application', ex);
        }
    },

    setMode: function (mode, argumentsObject, removeCurrentPageFromHistory) {
        try {
            this.getController('AssetManagement.customer.controller.NavigationController').requestSetMode(mode, argumentsObject, removeCurrentPageFromHistory);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMode of Main Application', ex);
        }
    },

    navigateBack: function (ignoreResultingChangedEvent) {
        try {
            this.getController('AssetManagement.customer.controller.NavigationController').requestGoBack(ignoreResultingChangedEvent);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside navigateBack of Main Application', ex);
        }
    },

    showDialog: function (dialogType, argumentsObject) {
        try {
            this.getController('AssetManagement.customer.controller.NavigationController').requestShowDialog(dialogType, argumentsObject);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside showDialog of Main Application', ex);
        }
    },

    setMainToolbarOptions: function (arrayOfOptions) {
        try {
            this._mainView.getController().getMainToolbarController().setOptionsMenuItems(arrayOfOptions);
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside setMainToolbarOptions of Main Application', ex);
        }
    },

    _appConfig: null,
    _mainView: null,
    _dataBaseHelper: null,

    getAppConfig: function () {
        return this._appConfig;
    },

    setAppConfig: function (appConfig) {
        this._appConfig = appConfig;
    },

    getMainView: function () {
        return this._mainView;
    },

    setMainView: function (view) {
        this._mainView = view;
    },

    getDataBaseHelper: function () {
        return this._dataBaseHelper;
    },

    setDataBaseHelper: function (dataBaseHelper) {
        this._dataBaseHelper = dataBaseHelper;
    },

    getCurrentPage: function () {
        var reval = null;

        try {
            retval = this.getController('AssetManagement.customer.controller.NavigationController').getCurrentPage();
        } catch (ex) {
            AssetManagement.customer.helper.OxLogger.logException('Exception occurred inside getCurrentPage of Main Application', ex);
        }

        return retval;
    }*/
});