describe("Inventory-List Page", function() {
    var inventoryListPage = null;

    var inventoryListPageController = null;


    beforeAll(function() {
        // Test to see if the app loaded
        inventoryListPage = Ext.create('AssetManagement.customer.view.pages.InventoryListPage');
        inventoryListPageController  = Ext.create('AssetManagement.customer.controller.pages.InventoryListPageController');

    }); // before each

    it('is loaded', function() {
        expect(inventoryListPage != null).toBeTruthy();
    });

    it("controller is loaded", function() {
        expect(inventoryListPageController != null).toBeTruthy();
    });

    it("has search field for inventory", function() {
        var searchFieldInventory = Ext.getCmp('searchFieldInventory');
        expect(searchFieldInventory != null).toBeTruthy();
    });

    
    it('has criteria filter combobox', () => {
        var inventoryListPageFilterCriteriaCombobox = Ext.getCmp('inventoryListPageFilterCriteriaCombobox');
        expect(inventoryListPageFilterCriteriaCombobox != null).toBeTruthy();
    });

    it('has inventory grid panel', function() {
        var inventoryGridPanel = Ext.getCmp('inventoryGridPanel');
        expect(inventoryGridPanel != null).toBeTruthy();
    });

});
