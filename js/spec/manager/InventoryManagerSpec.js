describe('InventoryManager Test:', () => {
    var originalTimeout;
    beforeEach(function () {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
    });

    var eventController = null;
    afterEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    beforeAll(function () {
        eventController = AssetManagement.customer.controller.EventController.getInstance();
    });

    var getInventoriesSpec = it('- get Inventories - ', (done) => {
        setTimeout(function () {
            var getInventoriesSpecSuccessCallback = function (inventories) {
                expect(inventories.getCount() > 0).toBeTruthy();
                lastInventoriesInList = inventories.getAt(inventories.getCount() - 1);
                this.result.description = this.result.description + inventories.getCount() + " results";
                done();
            };

            var eventId = AssetManagement.customer.manager.InventoryManager.getInventories(false);
            eventController.registerOnEventForOneTime(eventId, getInventoriesSpecSuccessCallback, this);
        }.bind(getInventoriesSpec), 1000);
    });
});