/**
 * Created by i0060 on 27.12.2016.
 */
describe("OrderManager", function() {
    var originalTimeout;
    beforeEach(function() {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
    });

    var selectedOrder = null;

    var getOrdersSpec = it("getOrders - returned ", function(done) {
        setTimeout(function() {
            var eventController = AssetManagement.customer.controller.EventController.getInstance();
            var orderSuccessCallback = function(orderList) {
                if(orderList && orderList.getCount()>0) {
                    selectedOrder = orderList.getAt(0);
                }
                this.result.description = this.result.description + orderList.getCount() + " results"
                expect(orderList.getCount() > 1).toBeTruthy();
                done();
            };

            var eventId = AssetManagement.customer.manager.OrderManager.getOrders(false, false);
            eventController.registerOnEventForOneTime(eventId, orderSuccessCallback, this);
        }.bind(getOrdersSpec), 10);
    });

    describe("get Order with dependent data", function() {
        //needed to load only once the order
        var order = null;

        beforeEach(function (done) {
            if(!order) {
                setTimeout(function() {
                    //provide some static order number for testing if the previous spec hasn't been executed
                    var aufnr = /*selectedOrder ? selectedOrder.get('aufnr') : */'000004000166';

                    var eventController = AssetManagement.customer.controller.EventController.getInstance();
                    var orderSuccessCallback = function (returnedOrder) {
                        order = returnedOrder;
                        done();
                    };

                    var eventId = AssetManagement.customer.manager.OrderManager.getOrder(aufnr, false);
                    eventController.registerOnEventForOneTime(eventId, orderSuccessCallback, this);
                }, 10);
            } else {
                done();
            }
        });

        var loadedOrderAufnr = it('Order aufnr ', function (done) {
            setTimeout(function() {
                var aufnr = order.get('aufnr')
                this.result.description = this.result.description + aufnr;
                expect(order.get('aufnr') !== "").toBeTruthy();
                done();
            }.bind(loadedOrderAufnr), 0);
        });

        var loadedOrderAddress = it('Order address ', function (done) {
            setTimeout(function() {
                var address = order.get('address')
                if(address) {
                    this.result.description = this.result.description + address.getName();
                }

                expect(address).toBeTruthy();
                done();
            }.bind(loadedOrderAddress), 0);
        });

        var loadedOrderEquNr = it('Order equnr ', function (done) {
            setTimeout(function() {
                var equnr = order.get('equnr');

                this.result.description = this.result.description + equnr;

                expect(equnr !== null).toBeTruthy();
                done();
            }.bind(loadedOrderEquNr), 0);
        });

        var loadedOrderTplNr = it('Order tplnr ', function (done) {
            setTimeout(function() {
                var tplnr = order.get('tplnr');

                this.result.description = this.result.description + tplnr;

                expect(tplnr !== null).toBeTruthy();
                done();
            }.bind(loadedOrderTplNr), 0);
        });

        var loadedOrderNotif = it('Order notif with notifnr ', function (done) {
            setTimeout(function() {
                var notif = order.get('notif');
                if(notif) {
                    this.result.description = this.result.description + notif.get('qmnum');
                }
                expect(notif).toBeTruthy();
                done();
            }.bind(loadedOrderNotif), 0);
        });

        //check if there is at least one operation
        var loadedOrderOperations = it('Order operations count = ', function (done) {
            setTimeout(function() {
                var operations  = order.get('operations');
                var operationsCount = operations.getCount();
                this.result.description = this.result.description + operationsCount;
                expect(operationsCount > 0).toBeTruthy();
                done();
            }.bind(loadedOrderOperations), 0);
        });

    });

    afterEach(function() {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });
});




