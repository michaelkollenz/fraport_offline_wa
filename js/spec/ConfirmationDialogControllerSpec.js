
describe("Confirmation Dialog Test", function() {
    var confirmationDialog = null;

    var confirmationDialogController = null;


    beforeEach(function() {
        // Test to see if the app loaded
        confirmationDialog = Ext.create('AssetManagement.customer.view.dialogs.ConfirmationDialog');
        confirmationDialogController  = Ext.create('AssetManagement.customer.controller.dialogs.ConfirmationDialogController');




    }); // before each

    it('Confirmation dialog is loaded', function() {
        expect(confirmationDialog != null).toBeTruthy();
    });

    it("Confirmation dialog controller is loaded", function() {
        expect(confirmationDialogController != null).toBeTruthy();
    });

    it("Confirmation dialog is modal", function() {
        expect(confirmationDialog.modal != null).toBeTruthy();
    });

    it("Confirmation dialog has items", function() {
        expect(confirmationDialog.items.length >0).toBeTruthy();
    });

});
