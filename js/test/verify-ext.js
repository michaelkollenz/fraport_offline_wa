describe("Ext", function() {
    it("is defined", function() {
        expect(Ext).toBeDefined();
    });

    it("is version 6", function() {
        expect(Ext.getVersion().major).toEqual(6);
    });

});

