Ext.Loader.setConfig({
    enabled: true,
    disableCaching: false
});

Ext.Loader.setPath({
    'Ext': 'ext',
    'AssetManagement': 'app',
    'Ext.ux': 'ext/packages/ux/classic/src'
});
Ext.require('AssetManagement.test.NavigationController');
Ext.require('Ext.ux.IFrame');
Ext.require('Ext.data.Model');
Ext.require('AssetManagement.customer.helper.DbKeyRangeHelper');
Ext.require('AssetManagement.customer.controller.ClientStateController');
Ext.require('AssetManagement.customer.core.Core');
Ext.require('AssetManagement.customer.helper.DataBaseHelper');


Ext.application({

    name: 'AssetManagement',

    controllers: [
        'AssetManagement.test.NavigationController'
    ],

    launch: function () {
        AssetManagement.customer.core.Core.setController(this.getController('AssetManagement.test.NavigationController'));

        var onInitializedDB = function() {
            var databaseHelper = AssetManagement.customer.core.Core.getDataBaseHelper();

            if(databaseHelper)
                databaseHelper.removeListener('initialized', onInitializedDB, this);


        }



        var dataBaseConfigObject = {
            dataBaseName: AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseName(),
            dataBaseVersion: AssetManagement.customer.controller.ClientStateController.getCurrentMainDataBaseVersion(),
            jsonResourceNameOfStrctInfo: 'databasestructure.json',
            jsonResourceCustomerStrctInfo: 'customer.json',
            listeners: { initialized: { fn: onInitializedDB, scope: this} }
        };

        AssetManagement.customer.core.Core.setDataBaseHelper(Ext.create('AssetManagement.customer.helper.DataBaseHelper', dataBaseConfigObject));



        AssetManagement.customer.core.Core.setAppConfig(AssetManagement.customer.AppConfig.getInstance());
        var ac = AssetManagement.customer.core.Core.getAppConfig();
        AssetManagement.customer.controller.ClientStateController.setClientLocale(ac.getLocale());
        ac.setScenario("OX_AM_01");


    }
});



afterEach(function () {
    Ext.data.Model.cache = {};
});

