Dir.chdir(ARGV[0] + '/resources/xml/') do
  #changing to production mode
  Dir.glob('*.xml').each {
    |file|
    puts file + " setting debugmode to false"
    File.write(file,
               File.open(file, &:read)
                   .gsub("<DebugMode>true</DebugMode>","<DebugMode>false</DebugMode>"))
  }
end

Dir.chdir ARGV[0]

puts "gathering files"

#deleting all markdown and mac specific files
Dir.glob('**/*.md' || '**/*.DS_Store').each {
  |f|
  File.delete(f)
}

puts "deleted various unneeded files in build directory"

files = Dir.glob("**/*").select {|e| File.file?(e) unless e == "cache.appcache"}

puts "found " + files.length.to_s + " files in " + ARGV[0]

#remove files with whitespaces
files.delete_if {
  |f|
  f.include? " "
}

puts "removed files with whitespaces"

#remove jsp
# files.delete_if {
#     |f| f.end_with? ".jsp"
# }

# puts "removed jsp files"

#remove all config files that are specific to a system
files.delete_if {
  |f|
  f =~ /appConfig\S+\.xml/
}

puts "removed appConfig files that are specific to a system from appcache"

if File.exist?("cache.appcachet")
  File.delete "cache.appcache"
end

# write file
File.open('cache.appcache', 'w') { |f|
  f << "CACHE MANIFEST\n"
  f << "#" + Time.now.utc.to_s + "\n"
  f << " \n"
  f << "#########\n"
  f << " \n"
  f << "CACHE:\n"
}

File.open("cache.appcache", "a") do |f|
  f.puts files
end

File.open('cache.appcache', 'a') { |f|
  f << " \n"
  f << "#########\n"
  f << " \n"
  f << "NETWORK:\n"
  f << "*\n"
}

puts  "wrote " + files.length.to_s + " files to cache.appcache"

#fixing css bug with 100%+7px
file_name = File.join(ARGV[0], 'resources', 'css','oxando.css')
text = File.read(file_name)
new_contents = text.gsub(/100%\+/, "100% + ")

File.open(file_name, "w") {
  |file|
  file.puts new_contents
}

puts "fixed %+ bug"

def checkLangfiles(basedir)
  puts 'checking files'
  fileArr = []
  nameArr = []
  longestId = 10
  mainPath = basedir + '/resources/localization/'

  Dir.chdir(mainPath) do
    Dir.glob('*.txt').each {
      |file|
      IO.readlines(file).each{
        |x|
        next if x.to_s.strip.empty?
        next if x.to_s.include? "****"
        if (x.split('=').length == 1)
          fileArr.push({id: x.split('=')[0].strip, key: ""})
        else
          fileArr.push({id: x.split('=')[0].strip, key: x.split('=',2)[1].delete("\n")})
        end
        longestId = x.split('=')[0].strip.length > longestId ? x.split('=')[0].strip.length : longestId
      }
      nameArr.push({name: file, path:  mainPath + file, content: File.read(file).each_line, stuff: []})
    }
  end

  fileArr.uniq! {|x| x[:id]}

  fileArr.each{
    |x|
    nameArr.each{
      |y|
      next if y[:content].any? { |s| s.include?(x[:id]) }
      y[:stuff].push(x[:id])
    }
  }

  nameArr.each{
    |x|
    next if x[:stuff].length == 0
    newFilename = basedir + '/build/' +x[:name] + '.dif'
    File.open(newFilename,"w"){
      |f|
      f.write(x[:stuff].join("\n"))
    }
    puts ""
    puts ""
    puts "######################################################################"
    puts "language files do not match!!"
    puts "missing strings in " + newFilename
  }
end

def readVersion(basedir)
  appJs = basedir + '\\app.js'
  basecore = basedir + "\\app\\base\\core\\BaseCore.js"
  version = File.foreach(appJs).grep(/_version\: \'(.*)\',/)
  version = File.foreach(basecore).grep(/_version\: \'(.*)\',/) if !version.any?

  puts version
end

checkLangfiles(ARGV[1])
readVersion(ARGV[1])
